<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterPendataanSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        db::beginTransaction();
        try {
            //code...
            $data =
                [
                    [
                        'nama_pendataan' => 'Perekaman Objek',
                        'jenis_transaksi' => 1
                    ],
                    [
                        'nama_pendataan' => 'Pecah Objek',
                        'jenis_transaksi' => 2
                    ],
                ];
            DB::table('jenis_pendataan')->insert($data);
            DB::commit();
            // $this->info('Suksess insert data pendataan');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            // $this->info($th->getMessage());
        }
    }
}
