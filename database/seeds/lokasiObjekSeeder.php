<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class lokasiObjekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
            DB::table('lokasi_objek')->insert(['nama_lokasi' => 'Jalan Propinsi']);
            DB::table('lokasi_objek')->insert(['nama_lokasi' => 'Jalan Kabupaten']);
            DB::table('lokasi_objek')->insert(['nama_lokasi' => 'Jalan Kecamatan']);
            DB::table('lokasi_objek')->insert(['nama_lokasi' => 'Jalan Desa']);
            DB::table('lokasi_objek')->insert(['nama_lokasi' => 'Gang']);
        
    }
}
