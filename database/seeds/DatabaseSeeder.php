<?php

use App\Permission;
// use App\Role;
use App\Unitkerja;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    /*    public function run()
    {
        // $this->call(UserSeeder::class);
    } */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        DB::beginTransaction();
        try {
            //code...


            // define permissions
            $permissions = [
                'view_users',
                'add_users',
                'edit_users',
                'delete_users',
                'view_roles',
                'add_roles',
                'edit_roles',
                'delete_roles',
                'view_permissions',
                'add_permissions',
                'edit_permissions',
                'delete_permissions',
                'view_dhkp',
                'view_dhkp_perbandingan'
            ];
            foreach ($permissions as $perms) {
                Permission::firstOrCreate(['name' => $perms]);
                $this->command->info('Permission  ' . $perms . ' created.');
            }



            $roles = [
                'Super User',
                'Kepala Bidang',
                'Kasi Pelayanan',
                'Kasi PDI',
                'Kasi Pendataan dan Penilaian',
                'Realisasi',
                'Penagihan',
                'Unit Pelaksana Teknis (UPT)',
                'Kecamatan',
                'Desa'
            ];

            // role
            foreach ($roles as $rr) {
                Role::firstOrCreate(['name' => $rr]);
                $this->command->info('Role  ' . $rr . ' created.');
            }

            $role = Role::wherename('Super User')->first();
            $role->syncPermissions(Permission::all());
            $this->command->info('Admin granted all the permissions');

            // sueper admin
            $admin = User::create([
                'nama' => 'Kang Admin',
                'nik' => '1234567890123456',
                'username' => 'admin',
                'telepon' => '0987654321',
                'password' => Hash::make('pass')
            ]);
            $roles =Role::wherename('Super User')->first();
            $admin->syncRoles($roles);            

            // $admin->assignRole('Super User');
            $this->command->info('super user created.');

            // user Kecamatan
            // kecamatan
            user::create(['nama' => 'AMPEL GADING', 'nik' => '3507090', 'username' => '3507090', 'telepon' => '3507090', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'BANTUR', 'nik' => '3507040', 'username' => '3507040', 'telepon' => '3507040', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'BULULAWANG', 'nik' => '3507130', 'username' => '3507130', 'telepon' => '3507130', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'D A U', 'nik' => '3507270', 'username' => '3507270', 'telepon' => '3507270', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'DAMPIT', 'nik' => '3507070', 'username' => '3507070', 'telepon' => '3507070', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'DONOMULYO', 'nik' => '3507010', 'username' => '3507010', 'telepon' => '3507010', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'GEDANGAN', 'nik' => '3507050', 'username' => '3507050', 'telepon' => '3507050', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'GONDANGLEGI', 'nik' => '3507140', 'username' => '3507140', 'telepon' => '3507140', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'JABUNG', 'nik' => '3507230', 'username' => '3507230', 'telepon' => '3507230', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'KALIPARE', 'nik' => '3507020', 'username' => '3507020', 'telepon' => '3507020', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'KARANGPLOSO', 'nik' => '3507260', 'username' => '3507260', 'telepon' => '3507260', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'KASEMBON', 'nik' => '3507310', 'username' => '3507310', 'telepon' => '3507310', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'KEPANJEN', 'nik' => '3507150', 'username' => '3507150', 'telepon' => '3507150', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'KROMENGAN', 'nik' => '3507161', 'username' => '3507161', 'telepon' => '3507161', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'LAWANG', 'nik' => '3507240', 'username' => '3507240', 'telepon' => '3507240', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'NGAJUM', 'nik' => '3507170', 'username' => '3507170', 'telepon' => '3507170', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'NGANTANG', 'nik' => '3507300', 'username' => '3507300', 'telepon' => '3507300', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'P A G A K', 'nik' => '3507030', 'username' => '3507030', 'telepon' => '3507030', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'P A K I S', 'nik' => '3507220', 'username' => '3507220', 'telepon' => '3507220', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'P U J O N', 'nik' => '3507290', 'username' => '3507290', 'telepon' => '3507290', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'PAGELARAN', 'nik' => '3507141', 'username' => '3507141', 'telepon' => '3507141', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'PAKISAJI', 'nik' => '3507190', 'username' => '3507190', 'telepon' => '3507190', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'PONCOKUSUMO', 'nik' => '3507100', 'username' => '3507100', 'telepon' => '3507100', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'SINGOSARI', 'nik' => '3507250', 'username' => '3507250', 'telepon' => '3507250', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'SUMBERMANJING WETAN', 'nik' => '3507060', 'username' => '3507060', 'telepon' => '3507060', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'SUMBERPUCUNG', 'nik' => '3507160', 'username' => '3507160', 'telepon' => '3507160', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'T U R E N', 'nik' => '3507120', 'username' => '3507120', 'telepon' => '3507120', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'TAJINAN', 'nik' => '3507200', 'username' => '3507200', 'telepon' => '3507200', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'TIRTOYUDO', 'nik' => '3507080', 'username' => '3507080', 'telepon' => '3507080', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'TUMPANG', 'nik' => '3507210', 'username' => '3507210', 'telepon' => '3507210', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'W A G I R', 'nik' => '3507180', 'username' => '3507180', 'telepon' => '3507180', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'W A J A K', 'nik' => '3507110', 'username' => '3507110', 'telepon' => '3507110', 'password' => Hash::make('pass')]);
            user::create(['nama' => 'WONOSARI', 'nik' => '3507171', 'username' => '3507171', 'telepon' => '3507171', 'password' => Hash::make('pass')]);

            // $user = User::whereraw("username like '3507%'")->get();
            // $rk =Role::wherename('Kecamatan')->first();
            // $user->syncRoles($rk);
            // $user->assignRole('Kecamatan');
            $this->command->info('Users kecamatan created.');

            //  unit kerja
            Unitkerja::create(['KD_UNIT' => '3507', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => null, 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Bapenda ']);
            Unitkerja::create(['KD_UNIT' => '3507090', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. AMPEL GADING ']);
            Unitkerja::create(['KD_UNIT' => '3507040', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. BANTUR ']);
            Unitkerja::create(['KD_UNIT' => '3507130', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. BULULAWANG ']);
            Unitkerja::create(['KD_UNIT' => '3507270', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. D A U ']);
            Unitkerja::create(['KD_UNIT' => '3507070', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. DAMPIT ']);
            Unitkerja::create(['KD_UNIT' => '3507010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. DONOMULYO ']);
            Unitkerja::create(['KD_UNIT' => '3507050', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. GEDANGAN ']);
            Unitkerja::create(['KD_UNIT' => '3507140', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. GONDANGLEGI ']);
            Unitkerja::create(['KD_UNIT' => '3507230', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. JABUNG ']);
            Unitkerja::create(['KD_UNIT' => '3507020', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. KALIPARE ']);
            Unitkerja::create(['KD_UNIT' => '3507260', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. KARANGPLOSO ']);
            Unitkerja::create(['KD_UNIT' => '3507310', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. KASEMBON ']);
            Unitkerja::create(['KD_UNIT' => '3507150', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. KEPANJEN ']);
            Unitkerja::create(['KD_UNIT' => '3507161', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. KROMENGAN ']);
            Unitkerja::create(['KD_UNIT' => '3507240', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. LAWANG ']);
            Unitkerja::create(['KD_UNIT' => '3507170', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. NGAJUM ']);
            Unitkerja::create(['KD_UNIT' => '3507300', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. NGANTANG ']);
            Unitkerja::create(['KD_UNIT' => '3507030', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. P A G A K ']);
            Unitkerja::create(['KD_UNIT' => '3507220', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. P A K I S ']);
            Unitkerja::create(['KD_UNIT' => '3507141', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. PAGELARAN ']);
            Unitkerja::create(['KD_UNIT' => '3507190', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. PAKISAJI ']);
            Unitkerja::create(['KD_UNIT' => '3507100', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. PONCOKUSUMO ']);
            Unitkerja::create(['KD_UNIT' => '3507250', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. SINGOSARI ']);
            Unitkerja::create(['KD_UNIT' => '3507060', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. SUMBERMANJING WETAN ']);
            Unitkerja::create(['KD_UNIT' => '3507160', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. SUMBERPUCUNG ']);
            Unitkerja::create(['KD_UNIT' => '3507120', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. T U R E N ']);
            Unitkerja::create(['KD_UNIT' => '3507200', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. TAJINAN ']);
            Unitkerja::create(['KD_UNIT' => '3507080', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. TIRTOYUDO ']);
            Unitkerja::create(['KD_UNIT' => '3507210', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. TUMPANG ']);
            Unitkerja::create(['KD_UNIT' => '3507180', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. W A G I R ']);
            Unitkerja::create(['KD_UNIT' => '3507110', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. W A J A K ']);
            Unitkerja::create(['KD_UNIT' => '3507070006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. AMADANOM Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507080010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. AMPELGADING Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507100008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. ARGOSUKO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507060009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. ARGOTIRTO Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507090011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. ARGOYUWONO Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507020001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. ARJOSARI Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507020007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. ARJOWILANGUN Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507220012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. ASRIKATON Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507310002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. B A Y E M Kec. KASEMBON']);
            Unitkerja::create(['KD_UNIT' => '3507240004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. B E D A L I Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507100012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. B E L U N G Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507110007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. B L A Y U Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507170011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. BABADAN Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507170012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. BALESARI Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507110003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. BAMBANG Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507040001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. BANDUNGREJO Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507171008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. BANGELAN Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507010007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. BANJAREJO Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507141009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. BANJAREJO Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507220005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. BANJAREJO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507170008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. BANJARSARI Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507040005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. BANTUR Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507300006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. BANTUREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507070003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. BATURETNO Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507250007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. BATURETNO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507290001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. BENDOSARI Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507210013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. BENJOR Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507110004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. BRINGIN Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507141010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. BRONGKAL Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507070004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. BUMIREJO Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507250014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. CANDIRENGGO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507150009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. CEPOKOMULYO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507150015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. CURUNGREJO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507150012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. D I L E M Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507060011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. D R U J U Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507180011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. DALISODO Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507070007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. DAMPIT Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507100001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. DAWUHAN Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507250005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. DENGKOL Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507010003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. DONOMULYO Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507260009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. DONOWARIH Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507210015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. DUWET KRAJAN Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507230009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. GADINGKEMBAR Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507270009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. GADINGKULON Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507080007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. GADUNGSARI Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507050003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. GAJAHREJO Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507030007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. GAMPINGAN Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507140022', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '022', 'NAMA_UNIT' => 'Ds. GANJARAN Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507050005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. GEDANGAN Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507120005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. GEDOG KULON Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. GEDOG WETAN Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507190010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. GENENGAN Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507260007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. GIRIMOYO Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507050008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. GIRIMULYO Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507190003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. GLANGGANG Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507140016', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '016', 'NAMA_UNIT' => 'Ds. GONDANGLEGI KULON Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507140006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. GONDANGLEGI WETAN Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507100016', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '016', 'NAMA_UNIT' => 'Ds. GUBUG KLAKAH Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507250013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. GUNUNGREJO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507200008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. GUNUNGRONGGO Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507060008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. HARJOKUNCARAN Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507210009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. J E R U Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507230014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. JABUNG Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507070012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. JAMBANGAN Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507200003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. JAMBEARJO Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507100006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. JAMBESARI Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507161006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. JAMBUWER Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507150016', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '016', 'NAMA_UNIT' => 'Ds. JATIREJOYOSO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507190008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. JATISARI Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507180010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. JEDONG Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507150001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. JENGGOLO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507080005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. JOGOMULYO Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507300013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. JOMBOK Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507130006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. K A S R I Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507120015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. K E D O K Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507230013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. K E M I R I Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507210002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. K I D A L Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507060013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. K L E P U Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507130003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. K R E B E T Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507270001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. K U C U R Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507141003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. KADEMANGAN Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507020009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. KALIASRI Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507020005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. KALIPARE Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507020008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. KALIREJO Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507210003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. KAMBINGAN Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507141001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. KANIGORO Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507100005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. KARANGANYAR Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507160007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. KARANGKATES Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507100010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KARANGNONGKO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507040010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KARANGSARI Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507270003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. KARANGWIDORO Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507130008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. KASEMBON Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507060003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. KEDUNGBANTENG Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507150007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. KEDUNGPEDARINGAN Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507220004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. KEDUNGREJO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507010006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. KEDUNGSALAM Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507230010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KEMANTREN Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507190012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. KENDALPAYAK Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507080004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. KEPATIHAN Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507260002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. KEPUHARJO Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507240011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. KETINDAN Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507110010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KIDANGBANG Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507171001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. KLUWUT Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507170009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. KRANGGAN Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507130010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KREBETSENGGRONG Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507161004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. KROMENGAN Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507130009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. KUWOLU Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507250001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. LANGLANG Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507090001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. LEBAKHARJO Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507070009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. MAJANGTENGAH Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507220014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. MANGLIAWAN Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507150005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. MANGUNREJO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507010009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. MENTARAMAN Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507150018', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '018', 'NAMA_UNIT' => 'Ds. MOJOSARI Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507270008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. MULYOAGUNG Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507300008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. MULYOREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507100017', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '017', 'NAMA_UNIT' => 'Ds. N G A D A S Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507290007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. NGABAB Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507230002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. NGADIREJO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507300005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. NGANTRU Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507170007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. NGASEM Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507200006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. NGAWONGGO Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507110013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. NGEMBAL Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507260003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. NGENEP Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507210001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. NGINGIT Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507290006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. NGROTO Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507030005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. P A G A K Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507310003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. P A I T Kec. KASEMBON']);
            Unitkerja::create(['KD_UNIT' => '3507070011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. P O J O K Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507250010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. PAGENTAN Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507300001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. PAGERSARI Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507100007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. PAJARAN Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507190007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. PAKISAJI Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507220010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. PAKISJAJAR Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507220009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. PAKISKEMBAR Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507170006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PALAAN Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507070008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. PAMOTAN Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507180012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. PANDANLANDUNG Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507030002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. PANDANREJO Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507100003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. PANDANSARI Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507290004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. PANDESARI Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507140015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. PANGGUNGREJO Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507180004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. PARANGARGO Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507110006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PATOKPICIS Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507150008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. PENARUKAN Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507161005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. PENIWEN Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507180007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. PETUNGSEWU Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507270004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. PETUNGSEWU Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507171002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. PLANDI Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507100014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. PONCOKUSUMO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507310001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. PONDOKAGUNG Kec. KASEMBON']);
            Unitkerja::create(['KD_UNIT' => '3507040006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PRINGGODANI Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507080002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. PUJIHARJO Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507290003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. PUJON KIDUL Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507210005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. PULUNGDOWO Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507250011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. PURWOASRI Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507010010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. PURWODADI Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507080001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. PURWODADI Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507090006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PURWOHARJO Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507010002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. PURWOREJO Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507300004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. PURWOREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507200007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. PURWOSEKAR Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507020003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. PUTUKREJO Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507140023', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '023', 'NAMA_UNIT' => 'Ds. PUTUKREJO Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507070010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. R E M B U N Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507040007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. REJOSARI Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507040009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. REJOYOSO Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507060006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. RINGINKEMBAR Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507060010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. RINGINSARI Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507120008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. S E D A Y U Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. SANANKERTO Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. SANANREJO Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507050006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. SEGARAN Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507060014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. SEKARBANYU Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507220001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SEKARPURO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507270005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. SELOREJO Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507130014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. SEMPALWADAK Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507030004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. SEMPOL Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507160004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. SENGGRENG Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507150002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SENGGURUH Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507140018', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '018', 'NAMA_UNIT' => 'Ds. SEPANJANG Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507060015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. SIDOASRI Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507050004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. SIDODADI Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507240003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SIDODADI Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507230008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. SIDOMULYO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507180009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. SIDORAHAYU Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507141007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SIDOREJO Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507230006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. SIDOREJO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507090010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. SIMOJAYAN Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507050002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SINDUREJO Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507060001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SITIARJO Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507180003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SITIREJO Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507210007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SLAMET Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507230012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. SLAMPAREJO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507161001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SLOROK Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507090004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. SONOWANGI Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507240002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SRIGADING Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507040003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SRIGONCO Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507070002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SRIMULYO Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507130005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. SUDIMORO Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507110011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. SUKOANYAR Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507220007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SUKOANYAR Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507070001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUKODONO Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507230005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. SUKOPURO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507150014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. SUKORAHARJO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507080009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. SUKOREJO Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507140014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. SUKOSARI Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507020006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. SUKOWILANGUN Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507060007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SUMBERAGUNG Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507040002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SUMBERBENING Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507171006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. SUMBERDEM Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507030006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. SUMBEREJO Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507050007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SUMBEREJO Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507100002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SUMBEREJO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507140024', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '024', 'NAMA_UNIT' => 'Ds. SUMBERJAYA Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507030003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SUMBERKERTO Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507220003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SUMBERKRADENAN Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507060012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. SUMBERMANJING Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507030001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUMBERMANJING KULON Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507240007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SUMBERNGEPOH Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507010001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUMBEROTO Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507020004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. SUMBERPETUNG Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507240008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. SUMBERPORONG Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507160001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUMBERPUCUNG Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507270010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. SUMBERSEKAR Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507070005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '070', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. SUMBERSUKO Kec. DAMPIT']);
            Unitkerja::create(['KD_UNIT' => '3507180001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUMBERSUKO Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507200012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. SUMBERSUKO Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507080003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SUMBERTANGKIL Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507171005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. SUMBERTEMPUR Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507190004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. SUTOJAYAN Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507230003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. T A J I Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507120007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. T A L O K Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. T U R E N Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507200010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. TAJINAN Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507150011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. TALANGAGUNG Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507120016', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '016', 'NAMA_UNIT' => 'Ds. TALANGSUKO Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507090013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. TAMANASRI Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507250008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. TAMANHARJO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507080011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. TAMANKUNCARAN Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507090003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. TAMANSARI Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507080013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. TAMANSATRIYAN Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507060004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. TAMBAKASRI Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507200001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. TAMBAKASRI Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507060002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. TAMBAKREJO Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507120009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. TANGGUNG Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507200002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. TANGKILSARI Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507090009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. TAWANGAGUNG Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507260010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. TAWANGARGO Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507120002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. TAWANGREJENI Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507290008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. TAWANGSARI Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507260001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. TEGALGONDO Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507060005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '060', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. TEGALREJO Kec. SUMBERMANJING WETAN']);
            Unitkerja::create(['KD_UNIT' => '3507150004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. TEGALSARI Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507270006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. TEGALWERU Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507010004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. TEMPURSARI Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507160005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. TERNYANG Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507090005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. TIRTOMARTO Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507090008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. TIRTOMOYO Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507220015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. TIRTOMOYO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507080006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. TIRTOYUDO Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507030008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '030', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. TLOGOREJO Kec. P A G A K']);
            Unitkerja::create(['KD_UNIT' => '3507010005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. TLOGOSARI Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507080008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. TLOGOSARI Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507250017', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '017', 'NAMA_UNIT' => 'Ds. TOYOMARTO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507010008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '010', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. TULUNGREJO Kec. DONOMULYO']);
            Unitkerja::create(['KD_UNIT' => '3507300011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. TULUNGREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507210012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. TULUSBESAR Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507020002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '020', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. TUMPAKREJO Kec. KALIPARE']);
            Unitkerja::create(['KD_UNIT' => '3507050001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '050', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. TUMPAKREJO Kec. GEDANGAN']);
            Unitkerja::create(['KD_UNIT' => '3507210011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. TUMPANG Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507120017', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '017', 'NAMA_UNIT' => 'Ds. TUMPUKRENTENG Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507250002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. TUNJUNGTIRTO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507240009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. TURIREJO Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507120004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. UNDAAN Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507140020', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '020', 'NAMA_UNIT' => 'Ds. UREK-UREK Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507110012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. W A J A K Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507190009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. WADUNG Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507130012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. WANDANPURO Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507250004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. WATUGEDE Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507300012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. WATUREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507090002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. WIROTAMAN Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507290010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. WIYUREJO Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507080012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '080', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. WONOAGUNG Kec. TIRTOYUDO']);
            Unitkerja::create(['KD_UNIT' => '3507310004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. WONOAGUNG Kec. KASEMBON']);
            Unitkerja::create(['KD_UNIT' => '3507110002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. WONOAYU Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507190005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. WONOKERSO Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507040008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. WONOKERTO Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507100011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. WONOMULYO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507040004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '040', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. WONOREJO Kec. BANTUR']);
            Unitkerja::create(['KD_UNIT' => '3507100013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. WONOREJO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507240012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. WONOREJO Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507250006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. WONOREJO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507171007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. WONOSARI Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507100015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. WRINGINANOM Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507210008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. WRINGINSONGO Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507090007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SIDORENGGO Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507090012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '090', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. MULYOASRI Kec. AMPEL GADING']);
            Unitkerja::create(['KD_UNIT' => '3507100004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. NGADIRESO Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507100009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '100', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. NGEBRUG Kec. PONCOKUSUMO']);
            Unitkerja::create(['KD_UNIT' => '3507110001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUMBERPUTIH Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507110005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. DADAPAN Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507110008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. C O D O Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507110009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '110', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. SUKOLILO Kec. W A J A K']);
            Unitkerja::create(['KD_UNIT' => '3507120001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. KEMULAN Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SAWAHAN Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. J E R U Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507120012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '120', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. PAGEDANGAN Kec. T U R E N']);
            Unitkerja::create(['KD_UNIT' => '3507130001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SUKONOLO Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507130002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. GADING Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507130004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. BAKALAN Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507130007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. PRINGU Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507130011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. LUMBANGSARI Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507130013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '130', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. BULULAWANG Kec. BULULAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507140012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. SUKOREJO Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507140013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. BULUPITU Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507140017', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '017', 'NAMA_UNIT' => 'Ds. PUTAT KIDUL Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507140019', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '019', 'NAMA_UNIT' => 'Ds. PUTAT LOR Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507140021', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '140', 'KD_KELURAHAN' => '021', 'NAMA_UNIT' => 'Ds. KETAWANG Kec. GONDANGLEGI']);
            Unitkerja::create(['KD_UNIT' => '3507141002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. BALEARJO Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507141004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. S U W A R U Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507141005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. CLUMPRIT Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507141008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. PAGELARAN Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507141011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '141', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. KARANGSUKO Kec. PAGELARAN']);
            Unitkerja::create(['KD_UNIT' => '3507150003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. K E M I R I Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507150006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PANGGUNGREJO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507150010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KEPANJEN Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507150013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. ARDIREJO Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507150017', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '150', 'KD_KELURAHAN' => '017', 'NAMA_UNIT' => 'Ds. NGADILANGKUNG Kec. KEPANJEN']);
            Unitkerja::create(['KD_UNIT' => '3507160002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. JATIGUWI Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507160003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. SAMBIGEDE Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507160006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '160', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. NGEBRUK Kec. SUMBERPUCUNG']);
            Unitkerja::create(['KD_UNIT' => '3507161002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. JATIKERTO Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507161003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. NGADIREJO Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507161007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '161', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. KARANGREJO Kec. KROMENGAN']);
            Unitkerja::create(['KD_UNIT' => '3507170005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. NGAJUM Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507170010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KESAMBEN Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507170013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '170', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. MAGUAN Kec. NGAJUM']);
            Unitkerja::create(['KD_UNIT' => '3507171', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. WONOSARI ']);
            Unitkerja::create(['KD_UNIT' => '3507171003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. PLAOSAN Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507171004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '171', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. KEBOBANG Kec. WONOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507180002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. MENDALANWANGI Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507180005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. GONDOWANGI Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507180006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PANDANREJO Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507180008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '180', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. SUKODADI Kec. W A G I R']);
            Unitkerja::create(['KD_UNIT' => '3507190001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. PERMANU Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507190002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. KARANGPANDAN Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507190006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. KARANGDUREN Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507190011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '190', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. KEBONAGUNG Kec. PAKISAJI']);
            Unitkerja::create(['KD_UNIT' => '3507200004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. JATISARI Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507200005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. PANDANMULYO Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507200009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. GUNUNGSARI Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507200011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '200', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. RANDUGADING Kec. TAJINAN']);
            Unitkerja::create(['KD_UNIT' => '3507210004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. PANDANAJENG Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507210006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. B O K O R Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507210010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. MALANGSUKO Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507210014', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '210', 'KD_KELURAHAN' => '014', 'NAMA_UNIT' => 'Ds. D U W E T Kec. TUMPANG']);
            Unitkerja::create(['KD_UNIT' => '3507220002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. AMPELDENTO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507220006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. PUCANGSONGO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507220008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. SUMBERPASIR Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507220011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. BUNUT WETAN Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507220013', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '220', 'KD_KELURAHAN' => '013', 'NAMA_UNIT' => 'Ds. SAPTORENGGO Kec. P A K I S']);
            Unitkerja::create(['KD_UNIT' => '3507230001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. KENONGO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507230004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. PANDANSARI LOR Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507230007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. SUKOLILO Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507230011', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '011', 'NAMA_UNIT' => 'Ds. ARGOSARI Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507230015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '230', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. GUNUNGJATI Kec. JABUNG']);
            Unitkerja::create(['KD_UNIT' => '3507240001', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '001', 'NAMA_UNIT' => 'Ds. SIDOLUHUR Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507240005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. KALIREJO Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507240006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. MULYOARJO Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507240010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '240', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. LAWANG Kec. LAWANG']);
            Unitkerja::create(['KD_UNIT' => '3507250003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. BANJARARUM Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507250009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. LOSARI WETAN Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507250012', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '012', 'NAMA_UNIT' => 'Ds. KLAMPOK Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507250015', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '015', 'NAMA_UNIT' => 'Ds. ARDIMULYO Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507250016', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '250', 'KD_KELURAHAN' => '016', 'NAMA_UNIT' => 'Ds. RANDUAGUNG Kec. SINGOSARI']);
            Unitkerja::create(['KD_UNIT' => '3507260004', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '004', 'NAMA_UNIT' => 'Ds. N G I J O Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507260005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. AMPELDENTO Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507260008', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '260', 'KD_KELURAHAN' => '008', 'NAMA_UNIT' => 'Ds. B O C E K Kec. KARANGPLOSO']);
            Unitkerja::create(['KD_UNIT' => '3507270002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. KALISONGO Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507270007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '270', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. LANDUNGSARI Kec. D A U']);
            Unitkerja::create(['KD_UNIT' => '3507290', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => null, 'NAMA_UNIT' => 'Kec. P U J O N ']);
            Unitkerja::create(['KD_UNIT' => '3507290002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SUKOMULYO Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507290005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. PUJON LOR Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507290009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '290', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. MADIREDO Kec. P U J O N']);
            Unitkerja::create(['KD_UNIT' => '3507300002', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '002', 'NAMA_UNIT' => 'Ds. SIDODADI Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507300003', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '003', 'NAMA_UNIT' => 'Ds. BANJAREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507300007', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '007', 'NAMA_UNIT' => 'Ds. PANDANSARI Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507300009', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '009', 'NAMA_UNIT' => 'Ds. SUMBERAGUNG Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507300010', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '300', 'KD_KELURAHAN' => '010', 'NAMA_UNIT' => 'Ds. KAUMREJO Kec. NGANTANG']);
            Unitkerja::create(['KD_UNIT' => '3507310005', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => '005', 'NAMA_UNIT' => 'Ds. KASEMBON Kec. KASEMBON']);
            Unitkerja::create(['KD_UNIT' => '3507310006', 'KD_PROPINSI' => '35', 'KD_DATI2' => '07', 'KD_KECAMATAN' => '310', 'KD_KELURAHAN' => '006', 'NAMA_UNIT' => 'Ds. SUKOSARI Kec. KASEMBON']);
            
            DB::commit();

            $this->command->warn('Proses is  done, all is well :)');
        } catch (Exception $e) {
            DB::rollback();
            //throw $th;
            $this->command->warn($e->getMessage());
        }
        
    }

    /**
     * Create a user with given role
     *
     * @param $role
     */
    /* private function createUser($role)
    {
        $user = factory(User::class)->create();
        $user->assignRole($role->name);

        if ($role->name == 'Admin') {
            $this->command->info('Here is your admin details to login:');
            $this->command->warn($user->username);
            $this->command->warn('Password is "secret"');
        }
    } */
}
