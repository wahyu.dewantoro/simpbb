<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class usersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeder user TP / Kecamatan
        User::where('nik', '3507090')->update(['username' => 'tpampelgading', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507040')->update(['username' => 'tpbantur', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507130')->update(['username' => 'tpbululawang', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507270')->update(['username' => 'tpdau', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507070')->update(['username' => 'tpdampit', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507010')->update(['username' => 'tpdonomulyo', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507050')->update(['username' => 'tpgedangan', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507140')->update(['username' => 'tpgondanglegi', 'password' => Hash::make('jancok')]);
        User::where('nik', '3507230')->update(['username' => 'tpjabung', 'password' => Hash::make('masjohan')]);
        User::where('nik', '3507020')->update(['username' => 'tpkalipare', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507260')->update(['username' => 'tpkarangploso', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507310')->update(['username' => 'tpkasembon', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507150')->update(['username' => 'tpkepanjen', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507161')->update(['username' => 'tpkromengan', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507240')->update(['username' => 'tplawang', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507170')->update(['username' => 'tpngajum', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507300')->update(['username' => 'tpngantang', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507030')->update(['username' => 'tppagak', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507220')->update(['username' => 'tppakis', 'password' => Hash::make('3507220')]);
        User::where('nik', '3507290')->update(['username' => 'tppujon', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507141')->update(['username' => 'tppagelaran', 'password' => Hash::make('LaVillaStrangiato')]);
        User::where('nik', '3507190')->update(['username' => 'tppakisaji', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507100')->update(['username' => 'tpponcokusumo', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507250')->update(['username' => 'tpsingosari', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507060')->update(['username' => 'tpsumbermanjingwetan', 'password' => Hash::make('sumawe457')]);
        User::where('nik', '3507160')->update(['username' => 'tpsumberpuncung', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507120')->update(['username' => 'tpturen', 'password' => Hash::make('Nat.88')]);
        User::where('nik', '3507200')->update(['username' => 'tptajinan', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507080')->update(['username' => 'tptirtoyudo', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507210')->update(['username' => 'tptumpang', 'password' => Hash::make('tanyayangpunya')]);
        User::where('nik', '3507180')->update(['username' => 'tpwagir', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507110')->update(['username' => 'tpwajak', 'password' => Hash::make('passwordtp')]);
        User::where('nik', '3507171')->update(['username' => 'tpwonosari', 'password' => Hash::make('passwordtp')]);


        // seeder user desa
        

    }
}
