<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisPenguranganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'Pensiunan','jenis'=>'0','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'WP tidak mampu','jenis'=>'1','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'WP terkena musibah','jenis'=>'1','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'Badan Usaha Pailit','jenis'=>'1','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'Yayasan Sosial','jenis'=>'1','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'Pertanian,Perkebunan,Perikanan','jenis'=>'1','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'Veteran','jenis'=>'0','pengurangan'=>'0']);
        DB::table('jenis_pengurangan')->insert(['nama_pengurangan' => 'Pendidikan','jenis'=>'1','pengurangan'=>'0']);
    }
    
}
