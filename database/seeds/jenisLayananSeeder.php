<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class jenisLayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Pendaftaran baru','order'=>1]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Pembatalan','order'=>2]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Pembetulan (Luas)','order'=>3]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Keberatan','order'=>4]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Pengurangan','order'=>5]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Mutasi Pecah','order'=>6]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Mutasi Gabung','order'=>7]);
            DB::table('jenis_layanan')->insert(['nama_layanan' => 'Pembetulan (Ejaan/Data WP)','order'=>0]);
    }
}
