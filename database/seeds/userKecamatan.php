<?php

use App\Kecamatan;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class userKecamatan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $roles = Role::wherename('Kecamatan')->first();
            $lk = Kecamatan::get();
            foreach ($lk as $row) {

                $user = User::create(['update_profile' => Carbon::now(), 'nama' => $row->nm_kecamatan, 'nik' => $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan, 'username' => $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan, 'telepon' => '1233512070006', 'password' => hash::make('paskecamatan')]);
                DB::table('user_unit_kerja')->insert(['user_id' => $user->id, 'kd_unit' => $user->username]);
                $user->syncRoles($roles);
            }

            DB::commit();
            Log::info("Berhasil seeder user kecamatan");
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();

            Log::error($th);
        }
    }
}
