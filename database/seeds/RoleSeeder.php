<?php

use App\Permission;
use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
// use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Reset cached roles and permissions
    app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

/* 
    // this can be done as separate statements
    Role::create(['name' => 'Super User']);
    Role::create(['name' => 'Realisasi']);
    Role::create(['name' => 'Penagihan']);
    Role::create(['name' => 'Unit Pelaksana Teknis (UPT)']);
    Role::create(['name' => 'Kecamatan']);
    Role::create(['name' => 'Desa']);

    $role = Role::wherename('Super User')->first();
    $role->syncPermissions(Permission::all());
    $this->command->info('Admin granted all the permissions');


    // create user
    $user=User::create([
      'nama' => 'Kang MIN',
      'nik' => '234235',
      'username' => 'admin',
      'telepon' => '9087237237',
      'email' => 'kangmin@mail.com',
      'password' => Hash::make('pass')
    ]);



    // Get the roles
    $roles =Role::wherename('Desa')->first();   $user->syncRoles($roles); */
  }
}
