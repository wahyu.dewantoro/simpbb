<?php

use App\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        DB::beginTransaction();
        try {
            // define permissions
            $permissions = [
                "view_users",
                "view_roles",
                "view_realisasi_target_apbd",
                "view_realisasi_harian",
                "view_realisasi_detail_kecamatan",
                "view_realisasi_bulanan",
                "view_realisasi_baku",
                "view_rangking_kecamatan",
                "view_permissions",
                "view_dhkp_perbandingan_desa",
                "view_dhkp_perbandingan",
                "view_dhkp_desa",
                "view_dhkp",
                "view_billing_kolektif",
                "pdf_realisasi_target_apbd",
                "pdf_realisasi_harian",
                "pdf_realisasi_detail_kecamatan",
                "pdf_realisasi_bulanan",
                "pdf_realisasi_baku",
                "pdf_rangking_kecamatan",
                "pdf_dhkp_perbandingan_desa",
                "pdf_dhkp_perbandingan",
                "pdf_dhkp_desa",
                "pdf_dhkp",
                "excel_realisasi_target_apbd",
                "excel_realisasi_harian",
                "excel_realisasi_detail_kecamatan",
                "excel_realisasi_bulanan",
                "excel_realisasi_baku",
                "excel_rangking_kecamatan",
                "excel_dhkp_perbandingan_desa",
                "excel_dhkp_perbandingan",
                "excel_dhkp_desa",
                "excel_dhkp",
                "edit_users",
                "edit_roles",
                "edit_permissions",
                "delete_users",
                "delete_roles",
                "delete_permissions",
                "add_users",
                "add_roles",
                "add_permissions",
                "add_billing_kolektif",
            ];
            foreach ($permissions as $perms) {
                Permission::firstOrCreate(['name' => $perms]);
                $this->command->info('Permission  ' . $perms . ' created.');
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $this->command->info($e->getMessage());
        }
    }
}
