<?php

use App\Helpers\Dafnom;
use App\Jobs\GenerateDafnom;
use App\Jobs\GenerateDafnomlist;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class testingSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=DB::table('data_billing')->select(DB::raw("data_billing_id,kd_kecamatan"))
        ->whereraw("trunc(created_at)=trunc(sysdate)")->get();
        foreach($data as $row){
            $queue=$row->kd_kecamatan;
            dispatch(new GenerateDafnomlist(['id_billing'=>$row->data_billing_id]))->onQueue($queue);
            dispatch(new GenerateDafnom(['id_billing'=>$row->data_billing_id]))->onQueue($queue);
        }


    }
}
