<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class kelompokObjekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Kawasan Apartemen']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Perumahan']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Gudang']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Kantor']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Sawah']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Kebun']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Rumah']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'ruko']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Rumah Kos']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Hotel']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Rumah Makan']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Asset Kabupaten']);
            DB::table('kelompok_objek')->insert(['nama_kelompok' => 'Asset lain']);
        
    }
}
