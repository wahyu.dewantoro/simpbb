<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekonBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekon_batch', function (Blueprint $table) {
            $table->id();
            $table->integer('tempat_bayar_id');
            $table->date('tanggal_pembayaran');
            $table->string('file_rekon');
            $table->integer('created_by');
            $table->date('created_at');
            $table->integer('updated_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->date('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekon_batch');
    }
}
