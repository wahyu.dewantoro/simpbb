<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotaPerhitunganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_perhitungan', function (Blueprint $table) {
            $table->id();
            $table->string('nomor', 50);
            $table->date('tanggal');
            $table->string('kota');
            $table->string('pegawai');
            $table->string('nip');
            $table->string('jabatan');
            $table->string('pangkat');
            $table->char('kobil',18);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_perhitungan');
    }
}
