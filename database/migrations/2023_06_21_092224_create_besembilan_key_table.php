<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBesembilanKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('besembilan_key', function (Blueprint $table) {
            $table->id();
            $table->string('source');
            $table->string('keyid');
            $table->timestamps();
        });

        Schema::create('besembilan_mutasi', function (Blueprint $table) {
            $table->string('source');
            $table->date('dateTime');
            $table->text('description');
            $table->string('transactionCode');
            $table->bigInteger('amount');
            $table->char('flag',1);
            $table->string('ccy');
            $table->string('reffno');
            $table->timestamps();
        });

        /* "dateTime": "2023-06-19 18:03:08",
        "description": "MB:PBB 3507010006000404072021",
        "transactionCode": "1017",
        "amount": 3816.0,
        "flag": "C",
        "ccy": "IDR",
        "reffno": "JMB480300925Dnd" */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('besembilan_key');
        Schema::dropIfExists('besembilan_mutasi');
    }
}
