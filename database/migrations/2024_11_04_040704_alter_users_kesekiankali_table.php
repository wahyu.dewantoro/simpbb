<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersKesekiankaliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->char('wp_verifikasi_kode', 1)->nullable();
            $table->string('wp_verifikasi_at')->nullable();
            $table->unsignedBigInteger('wp_verifikasi_by')->nullable();
            $table->text('wp_verifikasi_keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('wp_verifikasi_kode');
            $table->dropColumn('wp_verifikasi_at');
            $table->dropColumn('wp_verifikasi_by');
            $table->dropColumn('wp_verifikasi_keterangan');
        });
    }
}
