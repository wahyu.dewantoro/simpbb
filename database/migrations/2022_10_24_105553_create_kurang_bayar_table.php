<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKurangBayarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurang_bayar', function (Blueprint $table) {
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->char('thn_pajak_sppt', 4);
            $table->string('surat_kota')->nullable();
            $table->string('surat_pegawai')->nullable();
            $table->string('surat_nip')->nullable();
            $table->string('surat_jabatan')->nullable();
            $table->string('surat_pangkat')->nullable();
            $table->date('surat_tanggal')->nullable();
            $table->string('surat_nomor')->nullable();
            $table->integer('nilai');
            $table->integer('created_by');
            $table->date('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurang_bayar');
    }
}
