<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryJatuhTempoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_jatuh_tempo', function (Blueprint $table) {
            $table->id();
            $table->integer('tahun', 4);
            $table->date('tanggal');
            $table->date('created_at');
            $table->integer('created_by')->nullable();
            $table->date('updated_at');
            $table->integer('updated_by')->nullable();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_jatuh_tempo');
    }
}
