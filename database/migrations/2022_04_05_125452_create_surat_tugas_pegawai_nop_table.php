<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTugasPegawaiNopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_tugas_pegawai', function (Blueprint $table) {
            $table->string('id');
            $table->string('surat_tugas_id');
            $table->string('nama');
            $table->string('nip')->nullable();
            $table->string('jabatan');
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();


            $table->primary('id');
            $table->foreign('surat_tugas_id')->nullable()->references('id')->on('surat_tugas');
        });

        Schema::create('surat_tugas_objek', function (Blueprint $table) {
            $table->string('id');
            $table->string('surat_tugas_id');

            $table->integer('layanan_objek_id')->nullable();

            // data objek
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->string('alamat')->nullable();
            $table->string('nm_kelurahan');
            $table->string('nm_kecamatan');

            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();


            $table->primary('id');
            $table->foreign('surat_tugas_id')->nullable()->references('id')->on('surat_tugas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_tugas_pegawai');
        Schema::dropIfExists('surat_tugas_objek');
    }
}
