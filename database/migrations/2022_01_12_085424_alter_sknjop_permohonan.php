<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSknjopPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sknjop_permohonan', function (Blueprint $table) {
            //
            $table->string('nomor_hp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sknjop_permohonan', function (Blueprint $table) {
            //
            $table->dropColumn('nomor_hp');
        });
    }
}
