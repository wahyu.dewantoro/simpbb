<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_billing', function (Blueprint $table) {
            $table->bigIncrements('data_billing_id');
            $table->char('kd_propinsi',2);
            $table->char('kd_dati2',2);
            $table->char('kd_kecamatan',3);
            $table->char('kd_kelurahan',3);
            $table->char('kd_blok',3);
            $table->char('no_urut',4);
            $table->char('kd_jns_op',1);
            $table->char('tahun_pajak',4);

            $table->char('kd_status',1);
            $table->string('nama_wp');
            $table->string('nama_kelurahan');
            $table->string('nama_kecamatan');
            $table->char('kobil',18);
            $table->timestamp('tgl_bayar')->nullable();
            $table->string('pengesahan')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->primary(['data_billing_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_billing');
    }
}
