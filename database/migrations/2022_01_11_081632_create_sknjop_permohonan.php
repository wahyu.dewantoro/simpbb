<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSknjopPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sknjop_permohonan', function (Blueprint $table) {
            $table->string('id');
            $table->char('nomor_layanan', 11);
            $table->date('tanggal_permohonan');
            $table->char('nik_pemohon', 16);
            $table->string('nama_pemohon');
            $table->string('alamat_pemohon');
            $table->string('kelurahan_pemohon');
            $table->string('kecamatan_pemohon');
            $table->string('dati2_pemohon');
            $table->string('propinsi_pemohon');
            $table->char('kd_status', 1);
            $table->date('verifikasi_at')->nullable();
            $table->integer('verifikasi_by')->nullable();

            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
        });


        Schema::table('sknjop', function ($table) {
            $table->string('sknjop_permohonan_id');
            $table->dropColumn('nik_pemohon');
            $table->dropColumn('nama_pemohon');
            $table->dropColumn('alamat_pemohon');
            $table->dropColumn('kelurahan_pemohon');
            $table->dropColumn('kecamatan_pemohon');
            $table->dropColumn('dati2_pemohon');
            $table->dropColumn('propinsi_pemohon');

            /* $table->dropColumn('comment_count');
            $table->dropColumn('view_count'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        // alter sknjop
        Schema::table('sknjop', function ($table) {
            $table->dropColumn('sknjop_permohonan_id');

            $table->char('nik_pemohon', 16);
            $table->string('nama_pemohon');
            $table->string('alamat_pemohon');
            $table->string('kelurahan_pemohon');
            $table->string('kecamatan_pemohon');
            $table->string('dati2_pemohon');
            $table->string('propinsi_pemohon');
        });

        // drop table 
        Schema::dropIfExists('sknjop_permohonan');
    }
}
