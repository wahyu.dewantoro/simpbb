<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VDafnom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement($this->dropView());
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function createView(): string
    {
        return 
        "CREATE VIEW V_DAFNOM
        (
           ID,
           KOBIL,
           KD_KECAMATAN,
           KD_KELURAHAN,
           NAMA_KECAMATAN,
           NAMA_KELURAHAN,
           KETERANGAN,
           TAHUN_PAJAK,
           STATUS,
           CREATED_BY,
           CREATED_AT,
           EXPIRED_AT,
           TGL_BAYAR,
           JML_NOP,
           PBB
        )
        AS
             SELECT A.DATA_BILLING_ID,
                    A.KOBIL,
                    A.KD_KECAMATAN,
                    A.KD_KELURAHAN,
                    A.NAMA_KECAMATAN,
                    A.NAMA_KELURAHAN,
                    A.NAMA_WP,
                    A.TAHUN_PAJAK,
                    A.KD_STATUS,
                    A.CREATED_BY,
                    TO_CHAR (A.CREATED_AT, 'DD-MM-YYYY HH24:MI:SS'),
                    TO_CHAR (A.EXPIRED_AT, 'DD-MM-YYYY HH24:MI:SS'),
                    TO_CHAR (A.TGL_BAYAR, 'DD-MM-YYYY HH24:MI:SS'),
                    COUNT (B.DATA_BILLING_ID),
                    SUM (B.TOTAL)
               FROM    DATA_BILLING A
                    JOIN
                       BILLING_KOLEKTIF B
                    ON A.DATA_BILLING_ID = B.DATA_BILLING_ID
           GROUP BY A.DATA_BILLING_ID,
                    A.KOBIL,
                    A.KD_KECAMATAN,
                    A.KD_KELURAHAN,
                    A.NAMA_KECAMATAN,
                    A.NAMA_KELURAHAN,
                    A.NAMA_WP,
                    A.TAHUN_PAJAK,
                    A.KD_STATUS,
                    A.CREATED_BY,
                    A.CREATED_AT,
                    A.EXPIRED_AT,
                    A.TGL_BAYAR";
        
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function dropView(): string
    {
        return
           "DROP VIEW IF EXISTS `view_user_data`";
    }
}
