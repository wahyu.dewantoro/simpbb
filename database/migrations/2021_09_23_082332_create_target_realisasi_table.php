<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetRealisasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_realisasi', function (Blueprint $table) {
            $table->integer('tahun');
            $table->char('kd_buku', 3);
            $table->char('kd_sektor', 2);
            $table->integer('awal');
            $table->integer('perubahan_1');
            $table->integer('perubahan_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_realisasi');
    }
}
