<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefLurahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_lurah', function (Blueprint $table) {
            $table->id();
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->string('nm_lurah');
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->integer('created_by');
            $table->date('created_at');
            $table->integer('updated_by')->nullable();
            $table->date('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_lurah');
    }
}
