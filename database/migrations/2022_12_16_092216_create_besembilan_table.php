<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBesembilanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('besembilan', function (Blueprint $table) {
            // $table->id();
            $table->date('datetime');
            $table->string('description');
            $table->string('transactionCode');
            $table->integer('amount');
            $table->char('flag', 1);
            $table->string('ccy');
            $table->string('reffno');
            $table->string('jenis');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('besembilan');
    }
}
