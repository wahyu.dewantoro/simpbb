<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelolaObjekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelola_usulan', function (Blueprint $table) {
            $table->id();
            $table->char('nik', 16);
            $table->date('tanggal_usulan');
            $table->string('keterangan');
            $table->char('verifikasi_kode',1)->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });

        Schema::create('kelola_usulan_objek', function (Blueprint $table) {
            $table->id();
            $table->integer('kelola_usulan_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->char('verifikasi_kode',1)->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();

            $table->foreign(['kelola_usulan_id'], 'fk_kelola_obj')->references(['id'])->on('kelola_usulan')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('kelola_usulan_objek');
        Schema::dropIfExists('kelola_usulan');
    }
}
