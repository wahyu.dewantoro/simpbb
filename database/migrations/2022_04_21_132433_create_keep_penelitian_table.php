<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeepPenelitianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keep_penelitian', function (Blueprint $table) {
            // $table->id();
            // $table->timestamps();

            $table->integer('layanan_objek_id');
            $table->integer('peneliti_by');
            $table->date('start_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keep_penelitian');
    }
}
