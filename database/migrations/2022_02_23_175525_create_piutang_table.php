<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiutangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piutang', function (Blueprint $table) {
            $table->char('kd_propinsi',2);
            $table->char('kd_dati2',2);
            $table->char('kd_kecamatan',3);
            $table->char('kd_kelurahan',3);
            $table->char('kd_blok',3);
            $table->char('no_urut',4);
            $table->char('kd_jns_op',1);
            $table->char('tahun',4);

            $table->string('alamat_objek');
            $table->string('wp');
            $table->string('alamat_wp');
            $table->float('pbb',12,2);
            $table->char('buku',2);
            $table->float('tagihan',12,2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piutang');
    }
}
