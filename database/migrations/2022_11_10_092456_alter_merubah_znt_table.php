<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMerubahZntTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perubahan_znt', function (Blueprint $table) {
            $table->char('penetapan', 1)->nullable();
        });
        Schema::table('perubahan_znt_objek', function (Blueprint $table) {
            $table->char('thn_pajak', 4)->nullable();
            $table->date('tgl_jatuh_tempo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perubahan_znt', function (Blueprint $table) {
            $table->dropColumn('penetapan');
        });
        Schema::table('perubahan_znt_objek', function (Blueprint $table) {
            $table->dropColumn('thn_pajak');
            $table->dropColumn('tgl_jatuh_tempo');
        });
    }
}
