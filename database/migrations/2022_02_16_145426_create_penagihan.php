<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenagihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penagihan', function (Blueprint $table) {
            $table->string('penagihan_id', 36);
            $table->char('nomor_penagihan', 11);
            $table->enum('kd_status',['0','1'])->default('0');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            $table->primary('penagihan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penagihan');
    }
}
