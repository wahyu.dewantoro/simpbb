<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTagihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_tagihan', function (Blueprint $table) {
            $table->id();
            $table->string('surat_kota');
            $table->string('surat_pegawai');
            $table->string('surat_nip');
            $table->string('surat_jabatan');
            $table->string('surat_pangkat');
            $table->date('surat_tanggal');
            $table->string('surat_nomor');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->char('nik',16);
            $table->string('nm_wp');
            $table->string('jalan_wp');
            $table->char('rt_wp',3)->nullable();
            $table->char('rw_wp',3)->nullable();
            $table->string('nm_kelurahan_wp');
            $table->string('nm_kecamatan_wp');
            $table->string('nm_dati2_wp');
            $table->string('nm_propinsi_wp');
            $table->integer('created_by');
            $table->date('created_at');
            $table->integer('updated_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->date('deleted_at')->nullable();
        });

        Schema::create('surat_tagihan_nop', function (Blueprint $table) {
            $table->id();
            $table->integer('surat_tagihan_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->char('thn_pajak_sppt', 4);
            $table->foreign('surat_tagihan_id')
                ->references('id')->on('surat_tagihan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('surat_tagihan_nop');
        Schema::dropIfExists('surat_tagihan');
    }
}
