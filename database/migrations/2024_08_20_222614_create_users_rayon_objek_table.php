<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersRayonObjekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_rayon_objek');
        Schema::create('user_rayon_objek', function (Blueprint $table) {
            $table->id();
            $table->integer('user_rayon_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 3);
            $table->char('verifikasi_kode', 1)->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->text('verifikasi_keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_rayon_objek');
        Schema::dropIfExists('user_rayon_objek');
    }
}
