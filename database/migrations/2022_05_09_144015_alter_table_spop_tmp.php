<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSpopTmp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_spop_tmp', function (Blueprint $table) {
            //
            $table->char('mulai_tahun', 4)->nullable();
            $table->date('tgl_jatuh_tempo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_spop_tmp', function (Blueprint $table) {
            //
            $table->dropColumn('mulai_tahun');
            $table->dropColumn('tgl_jatuh_tempo');
        });
    }
}
