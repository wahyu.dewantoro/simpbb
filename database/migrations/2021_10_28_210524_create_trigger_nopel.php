<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTriggerNopel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::create('trigger_nopel', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        }); */

        DB::statement("CREATE OR REPLACE TRIGGER nopel_trg
        BEFORE INSERT
        ON layanan
        FOR EACH ROW
     BEGIN
        IF :new.nomor_layanan IS NULL
        THEN
           SELECT    TO_CHAR (SYSDATE, 'yyyymmdd')
                  || LPAD (
                          NVL (
                             MAX (CAST (SUBSTR (nomor_layanan, 9, 3) AS NUMBER)),
                             0)
                        + 1,
                        3,
                        0)
             INTO :new.nomor_layanan
             FROM layanan
            WHERE nomor_layanan LIKE TO_CHAR (SYSDATE, 'yyyymmdd') || '%';
        END IF;
     END;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('trigger_nopel');
        DB::statement("TRIGGER nopel_trg");
    }
}
