<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenagihanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penagihan_detail', function (Blueprint $table) {
            $table->integer('penagihan_detail_id');
            $table->char('penagihan_id',11);
           
            $table->char('kd_propinsi',2);
            $table->char('kd_dati2',2);
            $table->char('kd_kecamatan',3);
            $table->char('kd_kelurahan',3);
            $table->char('kd_blok',3);
            $table->char('no_urut',4);
            $table->char('kd_jns_op',1);
            $table->char('tahun',4);

            $table->string('alamat_objek');
            $table->string('wp');
            $table->string('alamat_wp');
            $table->float('pbb',10,2);
            $table->char('buku',1);
            $table->float('tagihan',10,2);

            $table->timestamps();
            // $table->foreign('penagihan_id')->references('penagihan_id')->on('penagihan');
            $table->primary('penagihan_detail_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penagihan_detail');
    }
}
