<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesaLunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desa_lunas', function (Blueprint $table) {
            /* $table->id();
            $table->timestamps(); */
            $table->char('kd_kecamatan',3);
            $table->char('kd_kelurahan',3);
            $table->integer('tahun_pajak');
            $table->date('tanggal_lunas');
            $table->integer('created_by');
            $table->date('created_at');
            $table->integer('updated_by');
            $table->date('updated_at');
            $table->primary(['kd_kecamatan','kd_kelurahan','tahun_pajak']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desa_lunas');
    }
}
