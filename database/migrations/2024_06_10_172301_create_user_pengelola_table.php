<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPengelolaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pengelola', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('keterangan')->nullable();
            $table->string('nama_pengembang');
            $table->date('tgl_surat');
            $table->string('no_surat');
            $table->char('verifikasi_kode', 1)->nullable();
            $table->string('verifikasi_keterangan')->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->char('is_pengelola', 1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_pengelola');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_pengelola');
        });
    }
}
