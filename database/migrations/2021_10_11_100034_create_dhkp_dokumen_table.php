<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDhkpDokumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_report', function (Blueprint $table) {
            $table->string('id');
            $table->string('keterangan');
            $table->char('kd_kecamatan', 3)->nullable();
            $table->char('kd_kelurahan', 3)->nullable();
            $table->string('disk');
            $table->string('filename');
            $table->date('send_mail')->nullable();
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dhkp_dokumen');
    }
}
