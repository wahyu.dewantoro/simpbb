<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLayananObjekKesekian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layanan_objek', function (Blueprint $table) {
            //
            $table->char('is_tolak', 1)->nullable();
            $table->string('keterangan_tolak')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('layanan_objek', function (Blueprint $table) {
            $tbale->dropColumn('is_tolak');
            $tbale->dropColumn('keterangan_tolak');
        });
    }
}
