<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspptHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esppt_history', function (Blueprint $table) {
            $table->id();
            $table->char('nik',16);
            $table->string('nama');
            $table->string('telepon');
            $table->string('nop');
            $table->char('status',1);
            $table->string('keterangan')->nullable();
            $table->string('ip')->nullable();
            $table->string('browser')->nullable();
            $table->string('platform')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('esppt_history');
    }
}
