<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTblSpopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TABLE DAT_SUBJEK_PAJAK 
        (	SUBJEK_PAJAK_ID CHAR(30 BYTE) NOT NULL ENABLE NOVALIDATE, 
         NM_WP VARCHAR2(30 BYTE) DEFAULT 'PEMILIK' NOT NULL ENABLE NOVALIDATE, 
         JALAN_WP VARCHAR2(30 BYTE) NOT NULL ENABLE NOVALIDATE, 
         BLOK_KAV_NO_WP VARCHAR2(15 BYTE), 
         RW_WP CHAR(2 BYTE), 
         RT_WP CHAR(3 BYTE), 
         KELURAHAN_WP VARCHAR2(30 BYTE), 
         KOTA_WP VARCHAR2(30 BYTE), 
         KD_POS_WP VARCHAR2(5 BYTE), 
         TELP_WP VARCHAR2(20 BYTE), 
         NPWP VARCHAR2(15 BYTE), 
         STATUS_PEKERJAAN_WP CHAR(1 BYTE) DEFAULT '0' NOT NULL ENABLE NOVALIDATE, 
         KECAMATAN_WP VARCHAR2(50 BYTE), 
         PROPINSI_WP VARCHAR2(70 BYTE)
        )");

        DB::statement("CREATE TABLE TBL_SPOP 
             (    
              NO_FORMULIR CHAR(11 BYTE) NOT NULL ENABLE, 
              LAYANAN_OBJEK_ID number,
              JENIS_LAYANAN_ID number,
              JNS_TRANSAKSI CHAR(1 BYTE) DEFAULT '1' NOT NULL ENABLE, 
              NOP_PROSES CHAR(18 BYTE) NOT NULL ENABLE, 
              NOP_BERSAMA CHAR(18 BYTE), 
              NOP_ASAL CHAR(18 BYTE), 
              SUBJEK_PAJAK_ID CHAR(30 BYTE) NOT NULL ENABLE, 
              NM_WP VARCHAR2(30 BYTE) DEFAULT 'PEMILIK', 
              JALAN_WP VARCHAR2(30 BYTE) DEFAULT '', 
              BLOK_KAV_NO_WP VARCHAR2(15 BYTE) DEFAULT '', 
              RW_WP CHAR(2 BYTE), 
              RT_WP CHAR(3 BYTE), 
              KELURAHAN_WP VARCHAR2(30 BYTE), 
              KOTA_WP VARCHAR2(30 BYTE), 
              KD_POS_WP VARCHAR2(5 BYTE), 
              TELP_WP VARCHAR2(20 BYTE), 
              NPWP VARCHAR2(15 BYTE) DEFAULT '', 
              STATUS_PEKERJAAN_WP CHAR(1 BYTE) DEFAULT '0', 
              NO_PERSIL VARCHAR2(5 BYTE), 
              JALAN_OP VARCHAR2(30 BYTE), 
              BLOK_KAV_NO_OP VARCHAR2(15 BYTE), 
              RW_OP CHAR(2 BYTE), 
              RT_OP CHAR(3 BYTE), 
              KD_STATUS_CABANG NUMBER(1,0) DEFAULT 1, 
              KD_STATUS_WP CHAR(1 BYTE) DEFAULT '1', 
              KD_ZNT CHAR(2 BYTE), 
              LUAS_BUMI NUMBER(12,0) DEFAULT 0, 
              JNS_BUMI CHAR(1 BYTE) DEFAULT '1', 
              TGL_PENDATAAN_OP DATE, 
              NIP_PENDATA CHAR(18 BYTE), 
              TGL_PEMERIKSAAN_OP DATE, 
              NIP_PEMERIKSA_OP CHAR(18 BYTE), 
              TGL_PEREKAMAN_OP DATE DEFAULT sysdate, 
              NIP_PEREKAM_OP CHAR(18 BYTE), 
              CREATED_AT DATE NOT NULL ENABLE, 
              CREATED_BY NUMBER(10,0) NOT NULL ENABLE, 
              UPDATED_AT DATE NOT NULL ENABLE, 
              UPDATED_BY NUMBER(10,0) NOT NULL ENABLE, 
              DELETED_AT DATE, 
              DELETED_BY NUMBER(10,0), 
               PRIMARY KEY (NO_FORMULIR)  
             )");

        DB::statement("CREATE TABLE TBL_LSPOP
                (	
                    NO_FORMULIR CHAR(11 BYTE) NOT NULL ENABLE, 
                    LAYANAN_OBJEK_ID number,
                 JNS_TRANSAKSI CHAR(1 BYTE) DEFAULT '1' NOT NULL ENABLE, 
                 NOP CHAR(18 BYTE) NOT NULL ENABLE, 
                 NO_BNG NUMBER(3,0) NOT NULL ENABLE, 
                 KD_JPB CHAR(2 BYTE), 
                 THN_DIBANGUN_BNG CHAR(4 BYTE) DEFAULT TO_CHAR(SYSDATE,'YYYY'), 
                 THN_RENOVASI_BNG CHAR(4 BYTE), 
                 LUAS_BNG NUMBER(12,0) DEFAULT 0, 
                 JML_LANTAI_BNG NUMBER(3,0) DEFAULT 1, 
                 KONDISI_BNG CHAR(1 BYTE), 
                 JNS_KONSTRUKSI_BNG CHAR(1 BYTE), 
                 JNS_ATAP_BNG CHAR(1 BYTE), 
                 KD_DINDING CHAR(1 BYTE), 
                 KD_LANTAI CHAR(1 BYTE), 
                 KD_LANGIT_LANGIT CHAR(1 BYTE), 
                 DAYA_LISTRIK NUMBER(10,0), 
                 ACSPLIT NUMBER(10,0), 
                 ACWINDOW NUMBER(10,0), 
                 ACSENTRAL NUMBER(1,0), 
                 LUAS_KOLAM NUMBER(10,0), 
                 FINISHING_KOLAM CHAR(1 BYTE), 
                 LUAS_PERKERASAN_RINGAN NUMBER(10,0), 
                 LUAS_PERKERASAN_SEDANG NUMBER(10,0), 
                 LUAS_PERKERASAN_BERAT NUMBER(10,0), 
                 LUAS_PERKERASAN_DG_TUTUP NUMBER(10,0), 
                 LAP_TENIS_LAMPU_BETON NUMBER(10,0), 
                 LAP_TENIS_LAMPU_ASPAL NUMBER(10,0), 
                 LAP_TENIS_LAMPU_RUMPUT NUMBER(10,0), 
                 LAP_TENIS_BETON NUMBER(10,0), 
                 LAP_TENIS_ASPAL NUMBER(10,0), 
                 LAP_TENIS_RUMPUT NUMBER(10,0), 
                 LIFT_PENUMPANG NUMBER(10,0), 
                 LIFT_KAPSUL NUMBER(10,0), 
                 LIFT_BARANG NUMBER(10,0), 
                 TGG_BERJALAN_A NUMBER(10,0), 
                 TGG_BERJALAN_B NUMBER(10,0), 
                 PJG_PAGAR NUMBER(10,0), 
                 BHN_PAGAR CHAR(1 BYTE), 
                 HYDRANT NUMBER(1,0), 
                 SPRINKLER NUMBER(1,0), 
                 FIRE_ALARM NUMBER(1,0), 
                 JML_PABX NUMBER(10,0), 
                 SUMUR_ARTESIS NUMBER(10,0), 
                 NILAI_INDIVIDU NUMBER(12,0), 
                 JPB3_8_TINGGI_KOLOM NUMBER(10,0), 
                 JPB3_8_LEBAR_BENTANG NUMBER(10,0), 
                 JPB3_8_DD_LANTAI NUMBER(10,0), 
                 JPB3_8_KEL_DINDING NUMBER(10,0), 
                 JPB3_8_MEZZANINE NUMBER(10,0), 
                 JPB5_KLS_BNG CHAR(1 BYTE), 
                 JPB5_LUAS_KAMAR NUMBER(10,0), 
                 JPB5_LUAS_RNG_LAIN NUMBER(10,0), 
                 JPB7_JNS_HOTEL CHAR(1 BYTE), 
                 JPB7_BINTANG CHAR(1 BYTE), 
                 JPB7_JML_KAMAR NUMBER(4,0), 
                 JPB7_LUAS_KAMAR NUMBER(10,0), 
                 JPB7_LUAS_RNG_LAIN NUMBER(10,0), 
                 JPB13_KLS_BNG CHAR(1 BYTE), 
                 JPB13_JML NUMBER(4,0), 
                 JPB13_LUAS_KAMAR NUMBER(10,0), 
                 JPB13_LUAS_RNG_LAIN NUMBER(10,0), 
                 JPB15_LETAK_TANGKI CHAR(1 BYTE), 
                 JPB15_KAPASITAS_TANGKI NUMBER(10,0), 
                 JPB_LAIN_KLS_BNG CHAR(1 BYTE), 
                 TGL_PENDATAAN_BNG DATE, 
                 NIP_PENDATA_BNG CHAR(18 BYTE), 
                 TGL_PEMERIKSAAN_BNG DATE, 
                 NIP_PEMERIKSA_BNG CHAR(18 BYTE), 
                 TGL_PEREKAMAN_BNG DATE DEFAULT SYSDATE, 
                 NIP_PEREKAM_BNG CHAR(18 BYTE), 
                 CREATED_AT DATE NOT NULL ENABLE, 
              CREATED_BY NUMBER(10,0) NOT NULL ENABLE, 
              UPDATED_AT DATE NOT NULL ENABLE, 
              UPDATED_BY NUMBER(10,0) NOT NULL ENABLE, 
              DELETED_AT DATE, 
              DELETED_BY NUMBER(10,0), 
               PRIMARY KEY (NO_FORMULIR)  
                )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::dropifexists('tbl_spop');
        schema::dropifexists('tbl_lspop');
        schema::dropifexists('dat_subjek_pajak');
    }
}
