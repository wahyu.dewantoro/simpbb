<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryMutasiGabungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_mutasi_gabung', function (Blueprint $table) {
            $table->string('id');
            $table->integer('layanan_objek_id')->nullable();
            $table->char('NO_FORMULIR', 11)->nullable();
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->timestamps();
            $table->primary('id');
        });

        Schema::create('history_mutasi_gabung_detail', function (Blueprint $table) {
            $table->id();
            $table->string('history_mutasi_gabung_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_mutasi_gabung');
        Schema::dropIfExists('history_mutasi_gabung_detail');
    }
}
