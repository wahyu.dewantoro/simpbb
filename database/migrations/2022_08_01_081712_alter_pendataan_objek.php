<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPendataanObjek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendataan_objek', function (Blueprint $table) {
            //
            $table->char('mark_tolak', 1)->nullable();
            $table->string('keterangan_tolak')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pendataan_objek', function (Blueprint $table) {
            //
            $table->dropColumn('mark_tolak');
            $table->dropColumn('keterangan_tolak');
        });
    }
}
