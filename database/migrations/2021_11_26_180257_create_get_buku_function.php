<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateGetBukuFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::create('get_buku_function', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        }); */

        DB::statement("CREATE OR REPLACE FUNCTION get_buku (nominal NUMBER)
                                RETURN NUMBER
                            IS
                                tmpVar   NUMBER;
                            BEGIN
                                
                            
                                IF nominal <= 100000
                                THEN
                                tmpVar := 1;
                                ELSIF nominal > 100000 AND nominal <= 500000
                                THEN
                                tmpVar := 2;
                                ELSIF nominal > 500000 AND nominal <= 2000000
                                THEN
                                tmpVar := 3;
                                ELSIF nominal > 2000000 AND nominal <= 5000000
                                THEN
                                tmpVar := 4;
                                ELSE
                                tmpVar := 5;
                                END IF;
                            
                                RETURN tmpVar;
                            
                            END get_buku;
                            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP FUNCTION GET_BUKU");
    }
}
