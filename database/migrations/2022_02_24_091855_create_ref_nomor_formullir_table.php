<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefNomorFormullirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_formulir', function (Blueprint $table) {
            $table->id();
            $table->char('penelitian', 1);
            $table->string('Uraian');
            $table->char('tahun', 4);
            $table->char('bundel_min', 4);
            $table->char('bundel_max', 4);
            $table->timestamps();
        });

        Schema::create('ref_formulir_layanan', function (Blueprint $table) {
            $table->id();
            $table->integer('ref_formulir_id');
            $table->integer('jenis_layanan_id');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_formullir');
        Schema::dropIfExists('ref_formulir_layanan');
    }
}
