<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefKelurahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kelurahan', function (Blueprint $table) {
           /*  $table->id();
            $table->timestamps(); */
            $table->string('kode_unit');
            $table->string('kd_kecamatan');
            $table->string('kd_kelurahan');
            $table->string('nama');
            $table->primary(['kode_unit','kd_kecamatan','kd_kelurahan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_kelurahan');
    }
}
