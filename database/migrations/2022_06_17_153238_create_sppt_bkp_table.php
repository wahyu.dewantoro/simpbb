<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpptBkpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sppt_bkp', function (Blueprint $table) {
            $table->id();
            $table->integer('layanan_objek_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->char('thn_pajak_sppt', 4);
            $table->string('nm_wp_sppt')->nullable();
            $table->string('jln_wp_sppt')->nullable();
            $table->string('blok_kav_no_wp_sppt')->nullable();
            $table->string('rw_wp_sppt')->nullable();
            $table->string('rt_wp_sppt')->nullable();
            $table->string('kelurahan_wp_sppt')->nullable();
            $table->string('kota_wp_sppt')->nullable();
            $table->string('kd_pos_wp_sppt')->nullable();
            $table->string('npwp_sppt')->nullable();
            $table->string('no_persil_sppt')->nullable();
            $table->date('tgl_jatuh_tempo_sppt')->nullable();
            $table->integer('luas_bumi_sppt')->nullable();
            $table->integer('luas_bng_sppt')->nullable();
            $table->integer('njop_bumi_sppt')->nullable();
            $table->integer('njop_bng_sppt')->nullable();
            $table->integer('njop_sppt')->nullable();
            $table->integer('njoptkp_sppt')->nullable();
            $table->integer('pbb_terhutang_sppt')->nullable();
            $table->integer('pbb_yg_harus_dibayar_sppt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sppt_bkp');
    }
}
