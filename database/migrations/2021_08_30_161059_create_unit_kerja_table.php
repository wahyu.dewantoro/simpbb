<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_kerja', function (Blueprint $table) {
            $table->string('kd_unit');
            $table->char('kd_propinsi',2);
            $table->char('kd_dati2',2);
            $table->char('kd_kecamatan',3)->nullable();
            $table->char('kd_kelurahan',3)->nullable();
            $table->char('kd_upt',3)->nullable();
            $table->string('nama_unit');
            $table->primary(['kd_unit']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_kerja');
    }
}
