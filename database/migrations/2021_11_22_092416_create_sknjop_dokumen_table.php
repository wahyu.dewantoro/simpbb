<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSknjopDokumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sknjop_dokumen', function (Blueprint $table) {
            $table->id();
            $table->string('sknjop_id');
            $table->string('disk');
            $table->string('path');
            $table->string('filename');
            $table->timestamps();

            $table->foreign('sknjop_id')->references('id')->on('sknjop');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sknjop_dokumen');
    }
}
