<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingKolektif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_kolektif', function (Blueprint $table) {
            $table->bigIncrements('billing_kolektif_id');
            $table->char('kd_propinsi',2);
            $table->char('kd_dati2',2);
            $table->char('kd_kecamatan',3);
            $table->char('kd_kelurahan',3);
            $table->char('kd_blok',3);
            $table->char('no_urut',4);
            $table->char('kd_jns_op',1);
            $table->char('tahun_pajak',4);
            $table->integer('pbb');
            $table->string('nm_wp');
            $table->integer('pokok');
            $table->integer('denda');
            $table->integer('total');
            $table->timestamp('expired_at')->nullable();
            $table->string('pengesahan')->nullable();
            $table->primary(['billing_kolektif_id']);
            $table->bigInteger('data_billing_id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('data_billing_id')->references('data_billing_id')->on('data_billing')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_kolektif');
    }
}
