<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayananDokumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_dokumen', function (Blueprint $table) {
            $table->id();
            $table->char('nomor_layanan', 11);
            $table->string('tablename');
            $table->string('keyid');
            $table->string('disk');
            $table->string('filename');
            $table->string('nama_dokumen');
            $table->string('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('nomor_layanan')->references('nomor_layanan')->on('layanan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan_dokumen');
    }
}
