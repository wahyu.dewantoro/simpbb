<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBeberapaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // sppt penelitian
        Schema::table('sppt_penelitian', function (Blueprint $table) {
            $table->integer('data_billing_id')->nullable();
        });
        Schema::table('nota_perhitungan', function (Blueprint $table) {
            $table->integer('data_billing_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sppt_penelitian', function (Blueprint $table) {
            $table->dropColumn('data_billing_id');
        });

        Schema::table('nota_perhitungan', function (Blueprint $table) {
            $table->dropColumn('data_billing_id');
        });
        
    }
}
