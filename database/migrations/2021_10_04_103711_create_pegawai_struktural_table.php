<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaiStrukturalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai_struktural', function (Blueprint $table) {
            $table->id();
            $table->char('kode',2)->unique();
            $table->string('nip');
            $table->string('nama');
            $table->string('jabatan');
            $table->char('is_plt')->nullable();
            $table->string('file_crt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai_struktural');
    }
}
