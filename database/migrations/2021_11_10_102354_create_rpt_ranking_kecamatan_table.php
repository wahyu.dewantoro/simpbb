<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRptRankingKecamatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_ranking_kecamatan', function (Blueprint $table) {
            // $table->id();
            // $table->timestamps();
            $table->char('kd_kecamatan', 3);
            $table->string('nm_kecamatan');
            $table->integer('buku');
            $table->integer('objek');
            $table->integer('baku');
            $table->integer('pokok');
            $table->integer('denda');
            $table->integer('total');
            $table->date('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_ranking_kecamatan');
    }
}
