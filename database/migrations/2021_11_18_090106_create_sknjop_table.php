<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSknjopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sknjop', function (Blueprint $table) {
            $table->string('id');
            $table->char('nik_pemohon', 16);
            $table->string('nama_pemohon');
            $table->string('alamat_pemohon');
            $table->string('kelurahan_pemohon');
            $table->string('kecamatan_pemohon');
            $table->string('dati2_pemohon');
            $table->string('propinsi_pemohon');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->string('alamat_op');
            $table->string('nama_wp');
            $table->string('alamat_wp');
            $table->integer('luas_bumi');
            $table->integer('luas_bng');
            $table->integer('njop_bumi');
            $table->integer('njop_bng');
            $table->integer('njop_pbb');
            $table->char('kd_status',1)->nullable();
            $table->date('tanggal_sk')->nullable();
            $table->string('nomer_sk')->nullable();
            $table->string('nama_kabid')->nullable();
            $table->string('nip_kabid')->nullable();
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');
            $table->date('verifikasi_at')->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sknjop');
    }
}
