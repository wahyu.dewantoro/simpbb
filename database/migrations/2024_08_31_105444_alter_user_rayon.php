<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserRayon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_rayon', function (Blueprint $table) {
            //
            $table->date('tgl_surat')->nullable();
            $table->string('no_surat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_rayon', function (Blueprint $table) {
            //
            $table->dropColumn('tgl_surat');
            $table->dropColumn('no_surat');
        });
    }
}
