<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('alamat_user')->nullable();
            $table->string('kelurahan_user')->nullable();
            $table->string('kecamatan_user')->nullable();
            $table->string('dati2_user')->nullable();
            $table->string('propinsi_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropcolumn('alamat_user');
            $table->dropcolumn('kelurahan_user');
            $table->dropcolumn('kecamatan_user');
            $table->dropcolumn('dati2_user');
            $table->dropcolumn('propinsi_user');
        });
    }
}
