<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNotapethitunganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nota_perhitungan', function (Blueprint $table) {
            //
            $table->integer('layanan_objek_id')->nullable();
            $table->integer('pendataan_objek_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nota_perhitungan', function (Blueprint $table) {
            $table->dropColumn('layanan_objek_id');
            $table->dropColumn('pendataan_objek_id ');
        });
    }
}
