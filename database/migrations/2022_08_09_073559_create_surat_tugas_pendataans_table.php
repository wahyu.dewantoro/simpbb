<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSuratTugasPendataansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try {
            //code...

            Schema::create('srt_tgs_pendataan', function (Blueprint $table) {
                $table->string('id');
                $table->date('tanggal_mulai')->nullable();
                $table->date('tanggal_selesai')->nullable();
                $table->string('lokasi')->nullable();
                $table->string('surat_kota');
                $table->string('surat_pegawai');
                $table->string('surat_nip');
                $table->string('surat_jabatan');
                $table->string('surat_pangkat');
                $table->date('surat_tanggal');
                $table->string('surat_nomor');

                $table->date('created_at');
                $table->integer('created_by');
                $table->date('updated_at');
                $table->integer('updated_by');
                $table->date('deleted_at')->nullable();
                $table->integer('deleted_by')->nullable();
                $table->primary('id');
                // $table->timestamps();
            });

            Schema::create('srt_tgs_pendataan_pegawai', function (Blueprint $table) {
                $table->string('id');
                $table->string('srt_tgs_pendataan_id');
                $table->string('nama');
                $table->string('nip')->nullable();
                $table->string('pangkat_golongan')->nullable();
                $table->string('jabatan')->nullable();
                $table->foreign('srt_tgs_pendataan_id')->references('id')->on('srt_tgs_pendataan')->onDelete('cascade');
            });

            Schema::create('srt_tgs_pendataan_objek', function (Blueprint $table) {
                $table->string('id');
                $table->string('srt_tgs_pendataan_id');
                $table->char('kd_propinsi', 2);
                $table->char('kd_dati2', 2);
                $table->char('kd_kecamatan', 3);
                $table->char('kd_kelurahan', 3);
                $table->char('kd_blok', 3);
                $table->char('no_urut', 4);
                $table->char('kd_jns_op', 1);
                $table->string('jalan_op')->nullable();
                $table->string('blok_kav_no_op')->nullable();
                $table->string('rw_op')->nullable();
                $table->string('rt_op')->nullable();
                $table->string('nm_kelurahan')->nullable();
                $table->string('nm_kecamatan')->nullable();
                $table->string('nama_wp')->nullable();
                $table->char('kd_znt', 2)->nullable();
                $table->integer('luas_bumi')->nullable();
                $table->integer('luas_bng')->nullable();
                $table->string('keterangan')->nullable();
                $table->foreign('srt_tgs_pendataan_id')->references('id')->on('srt_tgs_pendataan')->onDelete('cascade');
            });
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('srt_tgs_pendataan_pegawai');
        Schema::dropIfExists('srt_tgs_pendataan_objek');
        Schema::dropIfExists('srt_tgs_pendataan');
    }
}
