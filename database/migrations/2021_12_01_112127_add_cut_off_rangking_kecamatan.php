<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCutOffRangkingKecamatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_ranking_kecamatan', function (Blueprint $table) {
            $table->date('cut_off')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_ranking_kecamatan', function (Blueprint $table) {
            //
            $table->dropColumn('cut_off');
        });
    }
}
