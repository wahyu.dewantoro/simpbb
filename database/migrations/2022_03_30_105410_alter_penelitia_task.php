<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPenelitiaTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penelitian_task', function (Blueprint $table) {
            //
            $table->dropColumn('jenis_objek');
            $table->dropColumn('kd_status');
            $table->dropColumn('created_at');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_at');
            $table->dropColumn('updated_by');

            $table->integer('layanan_objek_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penelitian_task', function (Blueprint $table) {
            //
            $table->char('jenis_objek', 1);
            $table->char('kd_status', 1);
            $table->date('created_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->integer('updated_by')->nullable();

            $table->dropColumn('layanan_objek_id');
        });
    }
}
