<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSuratTugas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('surat_tugas', function (Blueprint $table) {
            $table->string('nomor_surat')->nullable();
             $table->char('kd_status',1)->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->integer('verifikasi_by')->nullable();
        });

        Schema::table('surat_tugas_pegawai', function (Blueprint $table) {
            $table->integer('users_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_tugas', function (Blueprint $table) {
            $table->dropColumn('nomor_surat');
             $table->dropColumn('kd_status');
            $table->dropColumn('verifikasi_at');
            $table->dropColumn('verifikasi_by');
        });

        Schema::table('surat_tugas_pegawai', function (Blueprint $table) {
            $table->dropColumn('users_id');
        });
    }
}
