<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayananObjekTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_objek_temp', function (Blueprint $table) {
            $table->id();
            // data subyek
            $table->string('nama_badan')->nullable();
            $table->string('nomor_akta_badan')->nullable();
            $table->string('nomor_sbu_badan')->nullable();
            $table->string('nomor_kemenhum')->nullable();
            $table->char('nik_wp', 16)->nullable();
            $table->string('nama_wp')->nullable();
            $table->date('Tanggal_lahir_wp')->nullable();
            $table->string('alamat_wp')->nullable();
            $table->string('RT_wp')->nullable();
            $table->string('RW_wp')->nullable();
            $table->string('kelurahan_wp')->nullable();
            $table->string('kecamatan_wp')->nullable();
            $table->string('dati2_wp')->nullable();
            $table->string('propinsi_wp')->nullable();
            $table->integer('agama_wp')->nullable();

            // data objek
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->integer('luas_bumi')->nullable();
            $table->integer('luas_bng')->nullable();
            $table->integer('njop_bumi')->nullable();
            $table->integer('njop_bng')->nullable();
            $table->integer('kelompok_objek_id')->nullable();
            $table->string('kelompok_objek_nama')->nullable();
            $table->integer('lokasi_objek_id')->nullable();
            $table->string('lokasi_objek_nama')->nullable();
            $table->timestamps();

            $table->string('telp_wp')->nullable();
            $table->string('alamat_op')->nullable();
            $table->string('rt_op')->nullable();
            $table->string('rw_op')->nullable();

            $table->integer('sisa_pecah_total_gabung')->nullable();
            $table->integer('hasil_pecah_hasil_gabung')->nullable();
            $table->string('zona')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->integer('jenis_layanan_id');

            $table->foreign('jenis_layanan_id')->references('id')->on('jenis_layanan');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('kelompok_objek_id')->references('id')->on('kelompok_objek');
            $table->foreign('lokasi_objek_id')->references('id')->on('lokasi_objek');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan_objek_temp');
    }
}
