<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekonHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekon_history', function (Blueprint $table) {
            $table->id();
            $table->char('kd_propinsi',2);
            $table->char('kd_dati2',2);
            $table->char('kd_kecamatan',3);
            $table->char('kd_kelurahan',3);
            $table->char('kd_blok',3);
            $table->char('no_urut',4);
            $table->char('kd_jns_op',1);
            $table->char('thn_pajak_sppt',4);
            $table->integer('pokok')->nullable();
            $table->integer('denda')->nullable();
            $table->integer('jumlah')->nullable();
            $table->date('tanggal_pembayaran');
            $table->char('flag',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekon_history');
    }
}
