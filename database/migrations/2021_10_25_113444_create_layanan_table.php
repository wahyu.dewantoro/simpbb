<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan', function (Blueprint $table) {
            $table->char('nomor_layanan', 11);
            $table->date('tanggal_layanan');
            $table->integer('jenis_objek');
            $table->integer('jenis_layanan_id');
            $table->string('jenis_layanan_nama');
            $table->char('nik', 16);
            $table->string('nama');
            $table->string('alamat');
            $table->string('kelurahan');
            $table->string('kecamatan');
            $table->string('dati2');
            $table->string('propinsi');
            $table->string('nomor_telepon');
            $table->string('keterangan')->nullable();

            $table->string('email')->nullable();
            $table->char('pengurus', 1);
            
            $table->char('kd_status', 1);
            $table->date('verifikasi_at')->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->primary('nomor_layanan');
            $table->foreign('jenis_layanan_id')->references('id')->on('jenis_layanan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanan');
    }
}
