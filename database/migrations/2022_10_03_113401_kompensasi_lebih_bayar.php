<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KompensasiLebihBayar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kompensasi_lebih_bayar', function (Blueprint $table) {
            $table->id();
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->char('thn_pajak_sppt', 4);

            $table->string('nama_wp')->nullable();
            $table->string('alamat_wp')->nullable();
            $table->string('alamat_op')->nullable();
            $table->float('pbb_asli')->nullable();
            $table->float('pbb_bayar')->nullable();
            $table->float('selisih')->nullable();
            $table->timestamp('tgl_bayar')->nullable();
            $table->char('kd_status', 1);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kompensasi_lebih_bayar');
    }
}
