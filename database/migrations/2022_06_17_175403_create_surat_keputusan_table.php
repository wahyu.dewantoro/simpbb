<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratKeputusanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keputusan', function (Blueprint $table) {
            $table->id();
            $table->integer('layanan_objek_id');
            $table->string('jenis');
            $table->string('nomor');
            $table->date('tanggal');
            $table->string('kota');
            $table->string('pegawai');
            $table->string('nip');
            $table->string('jabatan');
            $table->string('pangkat');
            //  $table->timestamps();
        });

        Schema::create('surat_keputusan_lampiran', function (Blueprint $table) {
            $table->id();
            $table->integer('surat_keputusan_id');
            $table->char('is_last',1)->nullable();
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->string('alamat_op');
            $table->string('nm_wp');
            $table->string('alamat_wp');
            $table->integer('luas_bumi');
            $table->integer('njop_bumi');
            $table->integer('luas_bng')->nullable();
            $table->integer('njop_bng')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keputusan');
        Schema::dropIfExists('surat_keputusan_lampiran');
    }
}
