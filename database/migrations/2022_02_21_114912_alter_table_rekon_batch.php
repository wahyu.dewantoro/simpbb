<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableRekonBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rekon_batch', function (Blueprint $table) {
            //
            $table->dropColumn('tempat_bayar_id');
            $table->dropColumn('file_rekon');
            $table->char('kode_bank',3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rekon_batch', function (Blueprint $table) {
            //
            $table->integer('tempat_bayar_id');
            $table->string('file_rekon');
            $table->dropColumn('kode_bank');
        });
    }
}
