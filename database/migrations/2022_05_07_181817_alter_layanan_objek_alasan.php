<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLayananObjekAlasan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layanan_objek', function (Blueprint $table) {
            $table->string('alasan')->nullable();
        });
        Schema::table('layanan_objek_temp', function (Blueprint $table) {
            $table->string('alasan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('layanan_objek', function (Blueprint $table) {
            $table->dropColumn('alasan');
        });
        Schema::table('layanan_objek_temp', function (Blueprint $table) {
            $table->dropColumn('alasan');
        });
    }
}
