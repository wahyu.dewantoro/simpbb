<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MigPerubahanZntObjek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('perubahan_znt', function (Blueprint $table) {
            $table->id();
            $table->char('nomor_batch', 8);
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3)->nullable();
            $table->char('kd_znt_lama', 2);
            // $table->integer('nir_lama');
            $table->char('kd_znt_baru', 2);
            // $table->integer('nir_baru');
            $table->char('verifikasi_kode', 1)->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->integer('created_by');
            $table->date('created_at');
        });

        Schema::create('perubahan_znt_objek', function (Blueprint $table) {
            $table->id();
            $table->integer('perubahan_znt_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->foreign('perubahan_znt_id')->references('id')->on('perubahan_znt')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perubahan_znt_objek');
        Schema::dropIfExists('perubahan_znt');
    }
}
