<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersRayonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_rayon');
        Schema::create('user_rayon', function (Blueprint $table) {
            $table->id();
            $table->integer('users_id');
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->string('keterangan')->nullable();
            $table->char('verifikasi_kode', 1)->nullable();
            $table->string('verifikasi_keterangan')->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_rayon');
        Schema::dropIfExists('user_rayon');
    }
}
