<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKolomDatZnt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('oracle_satutujuh')->table('dat_znt', function (Blueprint $table) {
            $table->integer('lokasi_objek_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->date('created_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->index([
                'lokasi_objek_id',
                'created_by',
                'updated_by'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('oracle_satutujuh')->table('dat_znt', function (Blueprint $table) {
            $table->dropIndex([
                'lokasi_objek_id',
                'created_by',
                'updated_by'
            ]);

            $table->dropColumn('lokasi_objek_id');
            $table->dropColumn('created_by');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_by');
            $table->dropColumn('updated_at');
        });
    }
}
