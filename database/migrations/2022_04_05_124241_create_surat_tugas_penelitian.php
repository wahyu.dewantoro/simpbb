<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTugasPenelitian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('surat_tugas', function (Blueprint $table) {
            // $table->id();
            $table->string('id');
            $table->string('batch');
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->string('lokasi');
            $table->string('surat_kota');
            $table->string('surat_pegawai');
            $table->string('surat_nip');
            $table->string('surat_jabatan');
            $table->string('surat_pangkat');
            $table->date('surat_tanggal');

            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();


            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_tugas');
    }
}
