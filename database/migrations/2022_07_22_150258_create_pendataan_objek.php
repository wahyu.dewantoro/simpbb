<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendataanObjek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('jenis_pendataan', function (Blueprint $table) {
            $table->id();
            $table->string('nama_pendataan');
            $table->char('jenis_transaksi', 1);
        });

        Schema::create('pendataan_batch', function (Blueprint $table) {
            $table->id();
            $table->integer('jenis_pendataan_id');
            $table->char('nomor_batch', 10)->nullable();
            $table->date('created_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('verifikasi_at')->nullable();
            $table->integer('verifikasi_by')->nullable();
            $table->char('verifikasi_kode',1)->nullable();
            $table->string('verifikasi_deskripsi')->nullable();

            $table->foreign('jenis_pendataan_id')->references('id')->on('jenis_pendataan')->onDelete('cascade');
        });

        Schema::create('pendataan_objek', function (Blueprint $table) {
            $table->id();
            $table->integer('pendataan_batch_id');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4)->nullable();
            $table->char('kd_jns_op', 1)->nullable();
            $table->char('nop_asal', 18)->nullable();
            $table->char('SUBJEK_PAJAK_ID', 16)->nullable();    //CHAR(30 BYTE) NOT NULL ENABLE, 
            $table->string('NM_WP', 30)->nullable();  // VARCHAR2(30 BYTE) DEFAULT 'PEMILIK', 
            $table->string('JALAN_WP', 30)->nullable();  // VARCHAR2(30 BYTE) DEFAULT '', 
            $table->string('BLOK_KAV_NO_WP', 15)->nullable();  // VARCHAR2(15 BYTE) DEFAULT '', 
            $table->char('RW_WP', 2)->nullable();    //CHAR(2 BYTE), 
            $table->char('RT_WP', 3)->nullable();    //CHAR(3 BYTE), 
            $table->string('KELURAHAN_WP', 30)->nullable();  // VARCHAR2(30 BYTE), 
            $table->string('kecamatan_wp', 30)->nullable();  // VARCHAR2(30 BYTE), 

            $table->string('KOTA_WP', 30)->nullable();  // VARCHAR2(30 BYTE), 
            $table->string('propinsi_wp', 30)->nullable();  // VARCHAR2(30 BYTE), 

            $table->string('KD_POS_WP', 5)->nullable();  // VARCHAR2(5 BYTE), 
            $table->string('TELP_WP', 20)->nullable();  // VARCHAR2(20 BYTE), 
            $table->string('NPWP', 15)->nullable();  // VARCHAR2(15 BYTE) DEFAULT '', 
            $table->char('STATUS_PEKERJAAN_WP', 1)->nullable();    //CHAR(1 BYTE) DEFAULT '0', 
            $table->string('NO_PERSIL', 5)->nullable();  // VARCHAR2(5 BYTE), 
            $table->string('JALAN_OP', 30)->nullable();  // VARCHAR2(30 BYTE), 
            $table->string('BLOK_KAV_NO_OP', 15)->nullable();  // VARCHAR2(15 BYTE), 
            $table->char('RW_OP', 2)->nullable();    //CHAR(2 BYTE), 
            $table->char('RT_OP', 3)->nullable();    //CHAR(3 BYTE), 
            $table->integer('KD_STATUS_CABANG', 1)->nullable();  //  NUMBER(1,0) DEFAULT 1, 
            $table->char('KD_STATUS_WP', 1)->nullable();    //CHAR(1 BYTE) DEFAULT '1', 
            $table->char('KD_ZNT', 2)->nullable();    //CHAR(2 BYTE), 
            $table->integer('LUAS_BUMI')->nullable();  //  NUMBER(12,0) DEFAULT 0, 
            $table->char('JNS_BUMI', 1)->nullable();    //CHAR(1 BYTE) DEFAULT '1', 

            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');

            $table->foreign('pendataan_batch_id')->references('id')->on('pendataan_batch')->onDelete('cascade');
        });


        Schema::create('pendataan_objek_bng', function (Blueprint $table) {
            $table->id();
            $table->integer('pendataan_objek_id');

            $table->integer('NO_BNG');   // NUMBER(3,0) NOT NULL ENABLE, 
            $table->char('KD_JPB', 2);  // CHAR(2 BYTE), 
            $table->char('THN_DIBANGUN_BNG', 4)->nullable();  // CHAR(4 BYTE) DEFAULT TO_CHAR(SYSDATE,'YYYY'), 
            $table->char('THN_RENOVASI_BNG', 4)->nullable();  // CHAR(4 BYTE), 
            $table->integer('LUAS_BNG', 12)->nullable();   // NUMBER(12,0) DEFAULT 0, 
            $table->integer('JML_LANTAI_BNG', 3)->nullable();   // NUMBER(3,0) DEFAULT 1, 
            $table->char('KONDISI_BNG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->char('JNS_KONSTRUKSI_BNG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->char('JNS_ATAP_BNG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->char('KD_DINDING', 1)->nullable();  // CHAR(1 BYTE), 
            $table->char('KD_LANTAI', 1)->nullable();  // CHAR(1 BYTE), 
            $table->char('KD_LANGIT_LANGIT', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('DAYA_LISTRIK', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('ACSPLIT', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('ACWINDOW', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('ACSENTRAL', 1)->nullable();   // NUMBER(1,0), 
            $table->integer('LUAS_KOLAM', 10)->nullable();   // NUMBER(10,0), 
            $table->char('FINISHING_KOLAM', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('LUAS_PERKERASAN_RINGAN', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LUAS_PERKERASAN_SEDANG', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LUAS_PERKERASAN_BERAT', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LUAS_PERKERASAN_DG_TUTUP', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LAP_TENIS_LAMPU_BETON', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LAP_TENIS_LAMPU_ASPAL', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LAP_TENIS_LAMPU_RUMPUT', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LAP_TENIS_BETON', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LAP_TENIS_ASPAL', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LAP_TENIS_RUMPUT', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LIFT_PENUMPANG', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LIFT_KAPSUL', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('LIFT_BARANG', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('TGG_BERJALAN_A', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('TGG_BERJALAN_B', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('PJG_PAGAR', 10)->nullable();   // NUMBER(10,0), 
            $table->char('BHN_PAGAR', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('HYDRANT', 1)->nullable();   // NUMBER(1,0), 
            $table->integer('SPRINKLER', 1)->nullable();   // NUMBER(1,0), 
            $table->integer('FIRE_ALARM', 1)->nullable();   // NUMBER(1,0), 
            $table->integer('JML_PABX', 0)->nullable();   // NUMBER(10,0), 
            $table->integer('SUMUR_ARTESIS', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('NILAI_INDIVIDU', 12)->nullable();   // NUMBER(12,0), 
            $table->integer('JPB3_8_TINGGI_KOLOM', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB3_8_LEBAR_BENTANG', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB3_8_DD_LANTAI', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB3_8_KEL_DINDING', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB3_8_MEZZANINE', 10)->nullable();   // NUMBER(10,0), 
            $table->char('JPB5_KLS_BNG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('JPB5_LUAS_KAMAR', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB5_LUAS_RNG_LAIN', 10)->nullable();   // NUMBER(10,0), 
            $table->char('JPB7_JNS_HOTEL', 1)->nullable();  // CHAR(1 BYTE), 
            $table->char('JPB7_BINTANG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('JPB7_JML_KAMAR', 4)->nullable();   // NUMBER(4,0), 
            $table->integer('JPB7_LUAS_KAMAR', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB7_LUAS_RNG_LAIN', 10)->nullable();   // NUMBER(10,0), 
            $table->char('JPB13_KLS_BNG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('JPB13_JML', 4)->nullable();   // NUMBER(4,0), 
            $table->integer('JPB13_LUAS_KAMAR', 10)->nullable();   // NUMBER(10,0), 
            $table->integer('JPB13_LUAS_RNG_LAIN', 10)->nullable();   // NUMBER(10,0), 
            $table->char('JPB15_LETAK_TANGKI', 1)->nullable();  // CHAR(1 BYTE), 
            $table->integer('JPB15_KAPASITAS_TANGKI', 10)->nullable();   // NUMBER(10,0), 
            $table->char('JPB_LAIN_KLS_BNG', 1)->nullable();  // CHAR(1 BYTE), 
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at');
            $table->integer('updated_by');

            $table->foreign('pendataan_objek_id')->references('id')->on('pendataan_objek')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('pendataan_objek');


        Schema::dropIfExists('pendataan_objek_bng');
        Schema::dropIfExists('pendataan_objek');
        Schema::dropIfExists('pendataan_batch');
        Schema::dropIfExists('jenis_pendataan');
    }
}
