<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenelitianKantorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penelitian_kantor', function (Blueprint $table) {
            $table->id();
            $table->char('nomor_layanan', 11)->nullable();
            $table->integer('layanan_objek_id')->nullable();
            $table->integer('jenis_layanan_id')->nullable();
            $table->char('nik_wp', 16);
            $table->string('nama_wp');
            $table->string('telp_wp', 20)->nullable();
            $table->string('alamat_wp');
            $table->string('rt_wp');
            $table->string('rw_wp');
            $table->string('kelurahan_wp');
            $table->string('kecamatan_wp');
            $table->string('kode_pos_wp')->nullable();
            $table->string('kabupaten_wp');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->string('alamat_op');
            $table->string('rt_op');
            $table->string('rw_op');
            $table->string('kelurahan_op');
            $table->string('kecamatan_op');
            $table->string('kabupaten_op');
            $table->integer('luas_tanah');
            $table->integer('luas_bangunan');
            $table->string('uraian')->nullable();
            $table->char('jns_transaksi',1);
            
            $table->integer('created_by');
            $table->date('created_at');
            $table->integer('pemutakhiran_by')->nullable();
            $table->date('pemutakhiran_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->date('deleted_at')->nullable();
        });

        Schema::create('penelitian_lapangan', function (Blueprint $table) {
            $table->id();
            $table->char('nomor_layanan', 11)->nullable();
            $table->integer('layanan_objek_id')->nullable();
            $table->integer('jenis_layanan_id')->nullable();
            $table->char('nik_wp', 16);
            $table->string('nama_wp');
            $table->string('telp_wp', 20)->nullable();
            $table->string('alamat_wp');
            $table->string('rt_wp');
            $table->string('rw_wp');
            $table->string('kelurahan_wp');
            $table->string('kecamatan_wp');
            $table->string('kode_pos_wp')->nullable();
            $table->string('kabupaten_wp');
            $table->char('kd_propinsi', 2);
            $table->char('kd_dati2', 2);
            $table->char('kd_kecamatan', 3);
            $table->char('kd_kelurahan', 3);
            $table->char('kd_blok', 3);
            $table->char('no_urut', 4);
            $table->char('kd_jns_op', 1);
            $table->string('alamat_op');
            $table->string('rt_op');
            $table->string('rw_op');
            $table->string('kelurahan_op');
            $table->string('kecamatan_op');
            $table->string('kabupaten_op');
            $table->integer('luas_tanah');
            $table->integer('luas_bangunan');
            $table->string('uraian')->nullable();
            $table->char('jns_transaksi',1);
            
            $table->integer('created_by');
            $table->date('created_at');
            $table->integer('pemutakhiran_by')->nullable();
            $table->date('pemutakhiran_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->date('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penelitian_kantor');
        Schema::dropIfExists('penelitian_lapangan');
        
    }
}
