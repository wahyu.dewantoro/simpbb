<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPendataanObjekPenetapanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendataan_objek_penetapan', function (Blueprint $table) {
            //
            $table->char('is_nota', 1)->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pendataan_objek_penetapan', function (Blueprint $table) {
            $table->dropColumn('is_nota');
        });
    }
}
