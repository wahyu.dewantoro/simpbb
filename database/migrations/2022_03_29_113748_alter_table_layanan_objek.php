<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLayananObjek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('layanan_objek', function (Blueprint $table) {
            //
            $table->date('pemutakhiran_at')->nullable();
            $table->integer('pemutakhiran_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('layanan_objek', function (Blueprint $table) {
            //
            $table->dropColumn('pemutakhiran_at');
            $table->dropColumn('pemutakhiran_by');
        });
    }
}
