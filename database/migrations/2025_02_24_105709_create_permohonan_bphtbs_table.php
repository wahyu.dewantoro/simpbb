<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermohonanBphtbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_bphtbs', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_layanan_bphtb');
            $table->string('nama_pemohon');
            $table->string('nomor_pemohon')->nullable();
            $table->char('kd_propinsi', 2)->nullable();
            $table->char('kd_dati2', 2)->nullable();
            $table->char('kd_kecamatan', 3)->nullable();
            $table->char('kd_kelurahan', 3)->nullable();
            $table->char('kd_blok', 3)->nullable();
            $table->char('no_urut', 4)->nullable();
            $table->char('kd_jns_op', 1)->nullable();
            $table->string('alamat_objek')->nullable();
            $table->char('rt_objek', 3)->nullable();
            $table->char('rw_objek', 3)->nullable();
            $table->string('nm_kelurahan')->nullable();
            $table->string('nm_kecamatan')->nullable();
            $table->float('luas_bumi', 10, 2)->nullable();
            $table->float('luas_bng', 10, 2)->nullable();
            $table->text('penjual')->nullable();
            $table->string('jenis_perolehan')->nullable();
            $table->text('dokumen_lampiran')->nullable();
            $table->text('keterangan')->nullable();
            $table->char('status', 1)->nullable();
            $table->string('nomor_layanan_pbb')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permohonan_bphtbs');
    }
}
