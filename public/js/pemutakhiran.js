$(document).ready(function() {
     var base_url = window.location.origin+window.location.pathname  ;
    
    console.log(base_url);
    // $("#jns_penelitian").trigger("change");

    var arrayPekerjaan = {
        "1": "PNS",
        "2": "ABRI",
        "3": "PENSIUNAN",
        "4": "BADAN",
        "5": "LAINNYA"
    };

    var arrayStatusWp = {
        "1": "Pemilik",
        "2": "Penyewa",
        "3": "Pengelola",
        "4": "Pemakai",
        "5": "Sengketa"
    };

    var arrayJnsBumi = {
        "1": "Tanah + Bangunan",
        "2": "Kavling siap bangun",
        "3": "Tanah kosong",
        "4": "Fasilitas Umum"
    };

    var arrayTransaksi = {
        "1": "PEREKAMAN DATA",
        "2": "PEMUTAKHIRAN DATA",
        "3": "PENGHAPUSAN DATA",
        "4": "PENGHAPUSAN OP BERSAMA"
    };

    var arrayZnt;

    JenisBumi(arrayJnsBumi);
    trSpop(arrayTransaksi);

    function trSpop(arrayTransaksi) {
        isi = $("#jns_transaksi").val();

        // JIKA PEREKAMAN DATA
        if (isi == "1") {
            exis = existingNop($("#nop_proses").val());
            // console.log(exis);
            if (exis == false) {
                $("#nop_proses").focus();
                Swal.fire({
                    icon: "error",
                    title: "Peringatan",
                    text: "NOP " + $("#nop_proses").val() + " sudah terpakai",
                    allowOutsideClick: false,
                    allowEscapeKey: false
                });
            }
        }

        if (typeof arrayTransaksi[isi] === "undefined") {
            hasil = "";
            $("#jns_transaksi").val("");
        } else {
            hasil = arrayTransaksi[isi];
        }
        $("#jns_transaksi_keterangan").html(hasil);
    }

    $("#jns_transaksi").on("keyup", function() {
        trSpop(arrayTransaksi);
    });

    function JenisBumi(arrayJnsBumi) {
        idxpkj = $("#jns_bumi").val();
        if (typeof arrayJnsBumi[idxpkj] === "undefined") {
            hasil = "";
            $("#jns_bumi").val("");
        } else {
            hasil = arrayJnsBumi[idxpkj];
        }
        $("#jns_bumi_keterangan").html(hasil);

        if (idxpkj == 1) {
            getFormBangunan(1);
            $("#input_bng").show();
            $("#jml_bng")
                .val(1)
                .show();
        } else {
            getFormBangunan(0);
            $("#input_bng").hide();
            $("#jml_bng")
                .val("")
                .hide();
        }

        // $("#jml_bng").trigger("change");
    }

    $("#jns_bumi").on("keyup", function() {
        JenisBumi(arrayJnsBumi);
    });

    // $("#status_pekerjaan_wp").val('5').trigger('keyup')
    $("#status_pekerjaan_wp").on("keyup", function() {
        idxpkj = $("#status_pekerjaan_wp").val();

        if (typeof arrayPekerjaan[idxpkj] === "undefined") {
            hasil = "";
            $(this).val("");
        } else {
            hasil = arrayPekerjaan[idxpkj];
        }

        $("#status_pekerjaan_wp_keterangan").html(hasil);
    });

    $("#kd_status_wp").on("keyup", function() {
        idxpkj = $("#kd_status_wp").val();
        if (typeof arrayStatusWp[idxpkj] === "undefined") {
            hasil = "";
            $(this).val("");
        } else {
            hasil = arrayStatusWp[idxpkj];
        }

        $("#kd_status_wp_keterangan").html(hasil);
    });

    $.validator.addMethod(
        "angkaRegex",
        function(value, element) {
            return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
        },
        "Harus di isi dengan angka."
    );

    // $("#submit").click(function() {
    $("#myform").submit(function() {
        var form = $("#myform");
        form.validate({
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.addClass("invalid-feedback");
                element.closest(".form-group").append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass("is-invalid");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass("is-invalid");
            },
            rules: {
                jns_bumi: {
                    required: true,
                    digits: true,
                    angkaRegex: true,
                    maxlength: 1
                },
                jns_transaksi: {
                    required: true
                },
                nop_proses: {
                    required: true,
                    minlength: 24,
                    maxlength: 24
                },
                nop_asal: {
                    required: false
                },
                jalan_op: {
                    required: true
                },
                rt_op: {
                    required: false,
                    digits: true,
                    angkaRegex: true,
                    digits: true,
                    maxlength: 3
                },
                rw_op: {
                    required: false,
                    digits: true,
                    angkaRegex: true,
                    maxlength: 2
                },
                luas_bumi: {
                    required: true,
                    digits: true,
                    angkaRegex: true,
                    digits: true
                },
                kd_znt: {
                    required: true
                },
                kd_status_wp: {
                    required: true,
                    digits: true,
                    angkaRegex: true,
                    maxlength: 1
                },
                subjek_pajak_id: {
                    required: true,
                    digits: true,
                    angkaRegex: true,
                    minlength: 16,
                    maxlength: 16
                },
                nm_wp: {
                    required: true
                },
                jalan_wp: {
                    required: true
                },
                blok_kav_no_wp: {
                    required: false
                },
                rt_wp: {
                    digits: true,
                    angkaRegex: false,
                    required: true,
                    maxlength: 3
                },
                rw_wp: {
                    angkaRegex: true,
                    required: true,
                    maxlength: 2,
                    digits: true
                },
                kd_pos_wp: {
                    required: false,
                    digits: true,
                    angkaRegex: true,
                    maxlength: 5
                },
                kelurahan_wp: {
                    required: true
                },
                kecamatan_wp: {
                    required: true
                },
                kota_wp: {
                    required: true
                },
                propinsi_wp: {
                    required: true
                },
                telp_wp: {
                    digits: true,
                    angkaRegex: true,
                    required: false,
                    minlength: 10
                },
                npwp: {
                    digits: true,
                    angkaRegex: true,
                    required: false
                },
                status_pekerjaan_wp: {
                    required: true,
                    digits: true,
                    maxlength: 1
                }
            },
            messages: {
                jns_bumi: {
                    required: "Jenis Tanah harus di pilih."
                },
                jns_transaksi: {
                    required: "Jenis transaksi harus di pilih."
                },
                nop_proses: {
                    required: "NOP proses harus di isi.",
                    minlength: "NOP beum lengkap",
                    maxlength: "NOP beum lengkap"
                },
                jalan_op: {
                    required: "Jalan harusdi isi."
                },
                rt_op: {
                    required: "RT harus di isi."
                },
                rw_op: {
                    required: "RW harus di isi."
                },
                luas_bumi: {
                    required: "Luas bumi harus di isi."
                },
                kd_znt: {
                    required: "ZNT harus di isi."
                }
            },
            errorPlacement: function(error, element) {
                if (element.is(":radio")) {
                    error.appendTo(element.parents(".container-radio"));
                } else {
                    // This is the default behavior
                    error.insertAfter(element);
                }
            }
        });

        if (form.valid() === false) {
            return false;
        } else {
            isi = $("#jns_transaksi").val();
            if (isi == "1") {
                hasilcek = existingNop($("#nop_proses").val());
                if (hasilcek == false) {
                    Swal.fire({
                        icon: "error",
                        title: "Peringatan",
                        text:
                            "NOP " + $("#nop_proses").val() + " sudah terpakai",
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    });
                }
                return hasilcek;
            }
        }
    });

    $(".nop").on("keyup", function() {
        var nop = $(this).val();
        var convert = formatnop(nop);
        $(this).val(convert);
    });

    // NOP znt

    if ($("#nop_proses").val() != "") {
        vaalue = $("#nop_proses").val();
        var ba = vaalue.replace(/[^\d]/g, "");
        var keca = ba.substr(4, 3);
        var kela = ba.substr(7, 3);
        var bloka = ba.substr(10, 3);
        // getZnt(keca, kela, bloka)
        $("#nop_proses").trigger("keyup");

        // getAjuan($('#jns_penelitian').val())
    }

    if ($("#jns_penelitian").val() != "") {
        getAjuan($("#jns_penelitian").val());
    }

    function existingNop(nop) {
        var response = false;
        if (nop.length == 24) {
            var b = nop.replace(/[^\d]/g, "");
            var kec = b.substr(4, 3);
            var kel = b.substr(7, 3);
            var blok = b.substr(10, 3);
            var no_urut = b.substr(13, 4);
            var jns_op = b.substr(17, 1);
            $.ajax({
                url: base_url + "/api/cek-unique-nop",
                data: {
                    kd_kecamatan: kec,
                    kd_kelurahan: kel,
                    kd_blok: blok,
                    no_urut: no_urut,
                    kd_jns_op: jns_op
                },
                success: function(res) {
                    // return  res;
                    if (res == 1) {
                        response = false;
                    } else {
                        response = true;
                    }
                },
                error: function(er) {
                    response = false;
                }
            });
        }
        return response;
        // return response;
    }

    // nop_proses
    $("#nop_proses").on("keyup", function() {
        var nop = $(this).val();

        var convert = formatnop(nop);
        var value = convert;
        if (value.length >= 17) {
            var b = value.replace(/[^\d]/g, "");
            var kec = b.substr(4, 3);
            var kel = b.substr(7, 3);
            var blok = b.substr(10, 3);
            getZnt(kec, kel, blok);
        }

        // console.log(value.length)
        $(this).val(convert);
    });

    $("#jns_penelitian").on("change", function() {
        pen = $("#jns_penelitian").val();
        getAjuan(pen);
    });

    function getAjuan(penelitian) {
        var html = "";
        $.ajax({
            url: base_url + "/api/jenis-ajuan",
            data: {
                penelitian: penelitian
            },
            success: function(res) {
                var count = Object.keys(res).length;
                // console.log(res);
                html = '<option value="">Pilih</option>';
                $.each(res, function(k, v) {
                    var apd = '<option value="' + k + '"> ' + v + "</option>";
                    html += apd;
                });

                $("#jns_ajuan").html(html);
            },
            error: function(er) {
                $("#jns_ajuan").html('<option value="">Pilih</option>');
            }
        });
    }

    function getZnt(kec, kel, blok) {
        arrayZnt = "";

        // option='<option value="">Belum tersedia</option>'

        $.ajax({
            url: base_url + "/api/znt",
            data: {
                kecamatan: kec,
                kelurahan: kel,
                blok: blok
            },
            success: function(res) {
                // console.log(res);
                html = '<option value="">Pilih</option>';
                $.each(res, function(k, v) {
                    var apd = '<option value="' + v + '"> ' + v + "</option>";
                    html += apd;
                });

                $("#kd_znt").html(html);
            },
            error: function(er) {
                // arrayZnt = "";
                $("#kd_znt").html('<option value="">Belum tersedia</option>');
            }
        });
    }

    function getFormBangunan(jumlah_bng) {
        $("#formLampiran").html("");
        if (jumlah_bng != "" && jumlah_bng > 0) {
            $.ajax({
                url: base_url + "/api/form-lspop",
                data: {
                    index: jumlah_bng
                },
                dataType: "html",
                success: function(data) {
                    $("#formLampiran").html(data);
                }
            });
        }
    }

    // tambahan untuk bangunan
    $("#jml_bng").on("change", function() {
        jumlah_bng = $("#jml_bng").val();
        getFormBangunan(jumlah_bng);
    });

    /*    var counter_lam = 1;
                $(document).on('click', '#addLampiran', function(e) {


                    $.ajax({
                        url: "{{ url('api/form-lspop') }}",
                        data: {
                            index: counter_lam
                        },
                        dataType: 'html',
                        success: function(data) {
                            $('#formLampiran').append(data);
                        },
                    });
                    counter_lam++;
                });
     */

    $(document).on("click", ".hapus_lampiran", function(e) {
        $(this)
            .parents(".hapus")
            .remove();
    });

    // register jQuery extension
    jQuery.extend(jQuery.expr[":"], {
        focusable: function(el, index, selector) {
            return $(el).is("a, button, :input, [tabindex]");
        }
    });

    $(document).on("keypress", "input,select", function(e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(":focusable");
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });
});
