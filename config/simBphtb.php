<?php

return [
    'base_url' =>  env('BASE_URL_SIMBPHTB'),
    'username' => env('USERNAME_SIMBPHTB'),
    'password' => env('PASSWORD_SIMBPHTB'),
];
