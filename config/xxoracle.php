<?php
// konfigurasi database dev

// use Illuminate\Support\Facades\Log;

$app = env('APP_DATABASE', 'dev');
// $app = '';
// Log::info($app);
if ($app == 'dev') {
    return [
        'oracle' => [
            'driver' => 'oracle',
            'host' => '192.168.1.204',
            'port' => '1521',
            'database' => 'sim_pbb',
            'service_name' => 'bapenda204',
            'username' => 'sim_pbb',
            'password' => 'pbb204',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
        'oracle_dua' => [
            'driver' => 'oracle',
            'host' => '192.168.1.204',
            'port' => '1521',
            'database' => 'pbb',
            'service_name' => 'bapenda204',
            'username' => 'pbb',
            'password' => 'pbb204',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
        'oracle_spo' => [
            'driver' => 'oracle',
            'host' => '192.168.1.204',
            'port' => '1521',
            'database' => 'spo',
            'service_name' => 'bapenda204',
            'username' => 'spo',
            'password' => 'pbb204',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
        'oracle_satutujuh' => [
            'driver' => 'oracle',
            'host' => '192.168.1.204',
            'port' => '1521',
            'database' => 'pbb',
            'service_name' => 'bapenda204',
            'username' => 'pbb',
            'password' => 'pbb204',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
    ];
} else {
    // db Production
    return [
        'oracle' => [
            'driver' => 'oracle',
            'host' => '192.168.1.200',
            'port' => '1521',
            'database' => 'sim_pbb',
            'service_name' => 'orcl',
            'username' => 'sim_pbb',
            'password' => 'B4p3nd4PBB200',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
        'oracle_dua' => [
            'driver' => 'oracle',
            'host' => '192.168.1.200',
            'port' => '1521',
            'database' => 'pbb',
            'service_name' => 'orcl',
            'username' => 'pbb',
            'password' => 'B4p3nd4PBB200',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
        'oracle_spo' => [
            'driver' => 'oracle',
            'host' => '192.168.1.200',
            'port' => '1521',
            'database' => 'spo',
            'service_name' => 'orcl',
            'username' => 'spo',
            'password' => 'B4p3nd4PBB200',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
        // Z2184SDNHGF8121RT58
        'oracle_satutujuh' => [
            'driver' => 'oracle',
            'host' => '192.168.1.200',
            'port' => '1521',
            'database' => 'pbb',
            'service_name' => 'orcl',
            'username' => 'pbb',
            'password' => 'B4p3nd4PBB200',
            'charset' => '',
            'prefix' => '',
            'server_version' => '12c',
        ],
    ];
}
