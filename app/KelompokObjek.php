<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KelompokObjek extends Model
{
    //
    protected $guarded = [];
    protected $table = 'kelompok_objek';
    public $timestamps = false;

    public function layanan_objek()
    {
        return $this->hasMany('App\Models\Layanan_objek',  'kelompok_objek_id','id');
    }
}
