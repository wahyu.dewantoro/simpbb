<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $guarded = [];

    public function childs()
    {
        return $this->hasMany('App\Menu', 'parent_id', 'id')->orderBy('urut','asc');
    }

    public function Role()
    {
        return $this->belongsToMany('App\Role', 'role_has_menus', 'menu_id', 'role_id');
    }
}
