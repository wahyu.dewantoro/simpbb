<?php

namespace App;

use App\Models\Layanan;
use App\Models\Layanan_objek;
use Illuminate\Database\Eloquent\Model;

class PermohonanBphtb extends Model
{
    //
    protected $guarded = [];

    protected $appends = ['jenis_layanan_pbb', 'status_layanan_pbb'];



    public function Layanan()
    {
        return $this->hasOne(Layanan::class, 'nomor_layanan', 'nomor_layanan_pbb');
    }

    public function Objek()
    {
        return $this->hasMany(Layanan_objek::class, 'nomor_layanan', 'nomor_layanan_pbb');
    }


    public function VmonitorLayanan()
    {
        return $this->hasOne(VmonitorLayanan::class, 'nomor_layanan', 'nomor_layanan_pbb');
    }

    public function getJenisLayananPbbAttribute()
    {
        $jenis_layanan = optional($this->Layanan)->jenis_layanan_nama;
        return empty($this->nomor_layanan_pbb) ? 'Belum di mapping ke layanan PBB' : $jenis_layanan;
    }

    public function getStatusLayananPbbAttribute()
    {

        return $this->VmonitorLayanan->status ?? '';
    }
}
