<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RekonBatch extends Model
{
    protected $guarded = [];
    protected $table = 'rekon_batch';
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            // $item->id = Uuid::uuid1()->toString();
            $item->created_at = Carbon::now();
            $item->created_by = Auth::user()->id;
            // $item->updated_by = Auth::user()->id;
            // $item->updated_at = Carbon::now();
        });


        // static::updating(function ($item) {
        //     $item->updated_by = Auth::user()->id;
        //     $item->updated_at = Carbon::now();
        // });
    }

    public function Nop()
    {
        return $this->hasMany('App\RekonNop', 'rekon_batch_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function proses()
    {
        return $this->hasMany('App\RekonHistory', 'rekon_batch_id', 'id');
    }
}
