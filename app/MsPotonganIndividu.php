<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MsPotonganIndividu extends Model
{
    //
    protected $guarded = [];
    protected $connection = 'oracle_dua';

    public $incrementing = false;
    protected $table = 'ms_potongan_individu';
    public $timestamps = false;
}
