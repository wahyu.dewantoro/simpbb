<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class KecamatanLunas extends Model
{
    //

    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'kecamatan_lunas';
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->created_at = Carbon::now();
            $item->created_by = Auth()->user()->id;
            $item->updated_by = Auth()->user()->id;
            $item->updated_at = Carbon::now();
        });


        static::updating(function ($item) {
            $item->updated_by = Auth()->user()->id;
            $item->updated_at = Carbon::now();
        });
    }

    public function kecamatan()
    {
        return $this->hasOne('App\Kecamatan', 'kd_kecamatan', 'kd_kecamatan');
    }

}
