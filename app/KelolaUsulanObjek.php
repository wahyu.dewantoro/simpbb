<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KelolaUsulanObjek extends Model
{
    //

    protected $guarded = [];
    protected $table = 'kelola_usulan_objek';
    protected $appends = [
        'Nop',
        'Status',
        'WajibPajak'
    ];
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {

            // $userid = Auth()->user()->id;

            $is = Auth()->user()->id ?? '';
            if ($is != '') {
                $userid = Auth()->user()->id;
            } else {
                $userid = $item->created_by;
            }

            $dateNow = now();

            $item->created_at = $dateNow;
            $item->created_by = $userid;
            $item->updated_by = $userid;
            $item->updated_at = $dateNow;
        });
    }

    public function KelolaUsulan()
    {
        return $this->hasOne('App\KelolaUsulan', 'id', 'kelola_usulan_id');
    }
    // return $this->BelongsTo('App\KelompokObjek',  'kelompok_objek_id', 'id');

    public function getStatusAttribute(): string
    {
        $st = $this->verifikasi_kode;
        if ($st == '') {
            $status = 'Menunggu di verifikasi';
        } else if ($st == '1') {
            $status = 'Telah di verifikasi';
        } else {
            $status = 'Di tolak';
        }
        return $status;
    }

    public function getNopAttribute(): string
    {
        $nop = '';
        $nop .= $this->kd_propinsi;
        $nop .= $this->kd_dati2;
        $nop .= $this->kd_kecamatan;
        $nop .= $this->kd_kelurahan;
        $nop .= $this->kd_blok;
        $nop .= $this->no_urut;
        $nop .= $this->kd_jns_op;

        $nop = formatnop($nop);
        return $nop;
    }

    public function getWajibPajakAttribute(): string
    {
        // $nop = '';
        $kd_propinsi = $this->kd_propinsi;
        $kd_dati2 = $this->kd_dati2;
        $kd_kecamatan = $this->kd_kecamatan;
        $kd_kelurahan = $this->kd_kelurahan;
        $kd_blok = $this->kd_blok;
        $no_urut = $this->no_urut;
        $kd_jns_op = $this->kd_jns_op;

        $wh = "";
        $wh .= " kd_propinsi ='" . $kd_propinsi . "' and ";
        $wh .= " kd_dati2 ='" . $kd_dati2 . "' and ";
        $wh .= " kd_kecamatan ='" . $kd_kecamatan . "' and ";
        $wh .= " kd_kelurahan ='" . $kd_kelurahan . "' and ";
        $wh .= " kd_blok ='" . $kd_blok . "' and ";
        $wh .= " no_urut ='" . $no_urut . "' and ";
        $wh .= " kd_jns_op ='" . $kd_jns_op . "'  ";

        $cs = DB::connection("oracle_satutujuh")->select("select nm_wp
from dat_objek_pajak  a
join dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
where " . $wh);
        if ($cs) {
            $wp = $cs[0]->nm_wp;
        } else {
            $wp = "Objek tidak di temukan";
        }
        return $wp;
    }
}
