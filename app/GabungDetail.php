<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GabungDetail extends Model
{
    //
    protected $guarded = [];
    protected $table = 'history_mutasi_gabung_detail';
}
