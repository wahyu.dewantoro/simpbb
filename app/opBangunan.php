<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class opBangunan extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dat_op_bangunan';
    public $timestamps = false;
}
