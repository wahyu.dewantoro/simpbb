<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranMutasi extends Model
{
    //
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    public $incrementing = true;
    protected $table = 'pembayaran_sppt_mutasi_batch';
    public $timestamps = false;

    public function NopTahun()
    {
        return $this->hasMany('App\PembayaranMutasiData', 'psmb_id', 'id');
    }
}
