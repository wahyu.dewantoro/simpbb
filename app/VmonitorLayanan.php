<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VmonitorLayanan extends Model
{
    //
    protected $table = 'v_monitor_layanan';
    public $timestamps = false;
    protected $primaryKey = 'nomor_layanan';
}
