<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ObjekPajak extends Model
{
    //
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dat_objek_pajak';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op'];

    public function scopeNop($sql, $param)
    {
        return $sql->select(DB::raw('dat_objek_pajak.*,get_kecamatan(kd_kecamatan) nm_kecamatan,get_kelurahan(kd_kelurahan,kd_kecamatan) nm_kelurahan'))
            ->whereraw("kd_propinsi='" . $param['kd_propinsi'] . "' and kd_dati2='" . $param['kd_dati2'] . "' and kd_kecamatan='" . $param['kd_kecamatan'] . "' and kd_kelurahan='" . $param['kd_kelurahan'] . "' and kd_blok='" . $param['kd_blok'] . "' and no_urut='" . $param['no_urut'] . "' and kd_jns_op='" . $param['kd_jns_op'] . "'");
      
    }

    public function subjek()
    {
        return $this->hasOne('App\SubjekPajak', 'subjek_pajak_id', 'subjek_pajak_id');
    }


    public function kecamatan()
    {
        return $this->hasOne('App\Kecamatan', 'kd_kecamatan', 'kd_kecamatan');
    }

    public function scopePendataan($sql)
    {
        return $sql->join(db::raw("(select no_formulir,nm_wp,kd_znt,jns_bumi,nop_proses,nama created_name,created_by 
        from sim_pbb.tbl_spop a
        join sim_pbb.users b on a.created_by=b.id
        where layanan_objek_id is null 
        ) tbl_spop"), 'dat_objek_pajak.no_formulir_spop', '=', 'tbl_spop.no_formulir')
                    ->select(dB::raw("tbl_spop.no_formulir, nop_proses,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op ,nm_wp,jns_bumi,kd_znt,total_luas_bumi,total_luas_bng,njop_bumi,njop_bng"));
    }
}
