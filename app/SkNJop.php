<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class SkNJop extends Model
{
    //

    protected $guarded = [];
    protected $table = 'sknjop';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->id = Uuid::uuid1()->toString();
            $item->created_at = Carbon::now();
            $item->created_by = Auth::user()->id;
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });


        static::updating(function ($item) {
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });
    }

    public function scopePencarian($sql)
    {
        // $q = strtolower(request('cari'));
        // $sql = $sql->whereraw("(lower(NAMA_PEMOHON) like '%$q%' or lower(ALAMAT_PEMOHON) like '%$q%' or lower(KELURAHAN_PEMOHON) like '%$q%' or lower(KECAMATAN_PEMOHON) like '%$q%' or lower(DATI2_PEMOHON) like '%$q%' or lower(PROPINSI_PEMOHON) like '%$q%' or lower(NOMER_SK) like '%$q%' or lower(ALAMAT_OP) like '%$q%' or KD_DATI2 ||'.'|| KD_KECAMATAN||'.'|| KD_KELURAHAN||'.'||KD_BLOK||'-'||NO_URUT||'.'||KD_JNS_OP like '%$q%')");
        // return $sql;
        
    }

    public function dokumen()
    {
        return $this->hasOne('App\skNjopDokumen','sknjop_id','id');
    }


    
}
