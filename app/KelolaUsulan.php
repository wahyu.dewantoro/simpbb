<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class KelolaUsulan extends Model
{
    //
    protected $guarded = [];
    protected $table = 'kelola_usulan';
    // public $timestamps = false;
    protected $appends = [
        'Nama',
        'JumlahNop',
        'Status',
        'NamaDokumen',
        'UrlDokumen',

    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {


            $is = Auth()->user()->id ?? '';
            if ($is != '') {
                $userid = Auth()->user()->id;
            } else {
                $userid = $item->created_by;
            }


            $dateNow = now();
            $item->created_at = $dateNow;
            $item->created_by = $userid;
            $item->updated_by = $userid;
            $item->updated_at = $dateNow;
        });
    }

    public function scopeVerifying($query)
    {
        return $query->whereNull('verifikasi_kode');
    }


    public function KelolaUsulanObjek()
    {
        return $this->hasMany('App\KelolaUsulanObjek',  'kelola_usulan_id', 'id');
    }


    public function Dokumen()
    {
        return $this->hasOne('App\Dokumen', 'keyid', 'id')
            ->where('tablename', 'KELOLA_USULAN');
    }

    public function getUrlDokumenAttribute(): string
    {
        $url =  $this->dokumen->url ?? "";
        return $url != '' ? url($this->dokumen->url) : '';
    }

    public function getNamaDokumenAttribute(): string
    {
        $dok = $this->dokumen->nama_dokumen ?? '';
        return $dok;
    }


    public function Pemohon()
    {
        return $this->hasOne('App\User', 'nik', 'nik');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function getNamaAttribute(): string
    {
        return $this->Pemohon->nama ?? '';
    }


    public function getJumlahNopAttribute(): string
    {
        return $this->KelolaUsulanObjek()->count();
    }

    public function getStatusAttribute(): string
    {
        $st = $this->verifikasi_kode;
        if ($st == '') {
            $status = 'Sedang di verifikasi';
        } else if ($st == '1') {
            $status = 'Telah di verifikasi';
        } else {
            $status = 'Di tolak';
        }
        return $status;
    }
}
