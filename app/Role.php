<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends \Spatie\Permission\Models\Role
{
    protected $guarded = [];

    public function Menu()
    {
        return $this->belongsToMany('App\Menu', 'role_has_menus', 'role_id', 'menu_id');
    }
}
