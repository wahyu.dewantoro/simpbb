<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUnitKerja extends Model
{
    //
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'user_unit_kerja';
    public $timestamps = false;

    public function User()
    {
        return $this->belongsToMany('App\User', 'user_unit_kerja', 'user_id', 'kd_unit');
    }
}
