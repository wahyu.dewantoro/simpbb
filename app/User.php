<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Models\Impersonate;
use Laravel\Passport\HasApiTokens;
use Faker\Factory as Faker;

/**  ADD THIS LINE **/

use Alfa6661\AutoNumber\AutoNumberTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Role;

class User extends Authenticatable
//  implements MustVerifyEmail
{
    use Notifiable, HasRoles, Impersonate, HasApiTokens;
    use AutoNumberTrait, SoftDeletes;
    // HasApiTokens
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /* protected $fillable = [
        'name', 'email', 'password',
    ]; */

    protected $guarded = [];
    protected $dates = ['deleted_at']; // Pastikan kolom tanggal dikenali
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    public function is_admin()
    {
        return $this->hasAnyRole(['Administrator', 'Super User']);
    }

    public function is_wp()
    {
        return $this->hasAnyRole(['DEVELOPER/PENGEMBANG', 'Rayon', 'Wajib Pajak']);
    }


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // protected $with=['role'];

    // public function 
    public function Unitkerja()
    {
        return $this->belongsToMany('App\Unitkerja', 'user_unit_kerja', 'user_id', 'kd_unit');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $faker = Faker::create('id_ID');
            $item->telepon = $faker->phoneNumber;
            $item->email = preg_replace('/@example\..*/', '@sipanji.com', $faker->unique()->safeEmail);
        });
    }

    public function getAutoNumberOptions()
    {
        return [
            'nik' => [
                'format' => '3507' . '?', // Format kode yang akan digunakan.
                'length' => 12 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }

    public function activeOTP()
    {
        return $this->hasOne(UserOTP::class, 'user_id')->whereraw("sysdate<expired_at");
    }

    public function Pendukung()
    {
        return $this->hasOne('App\Dokumen', 'keyid', 'id')
            ->where('tablename', 'users');
    }

    public function UserPengelola()
    {
        return $this->hasOne('App\UsulanPengelola', 'user_id', 'id')->orderby('id', 'desc');
    }

    public function UserRayon()
    {
        return $this->HasMany(UserRayon::class, 'users_id', 'id')->orderby('id', 'desc');
    }

    // public function roles()
    // {
    //     return $this->belongsToMany(Role::class);
    // }
}
