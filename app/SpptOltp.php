<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpptOltp extends Model
{
    //
    protected $connection = 'oracle_spo';
    protected $guarded = [];
    protected $table = 'sppt_oltp';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = ['thn_pajak_sppt', 'kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op', 'thn_pajak_sppt'];
}
