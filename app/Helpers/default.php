<?php

use App\Helpers\InformasiObjek;
use App\Menu;
use App\models\SuratTugas;
use App\SkNJop;
use App\Sppt;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

function tglIndo($date)
{

  if (empty($date)) {
    return null;
  }

  \Carbon\Carbon::setLocale('id');
  $date = date('Y-m-d', strtotime($date));
  return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->isoFormat('D MMMM Y');
}

function tglHari($tgl)
{
  $day = date('D', strtotime($tgl));
  $dayList = array(
    'Sun' => 'Minggu',
    'Mon' => 'Senin',
    'Tue' => 'Selasa',
    'Wed' => 'Rabu',
    'Thu' => 'Kamis',
    'Fri' => 'Jumat',
    'Sat' => 'Sabtu'
  );
  return  $dayList[$day];
}

function user()
{

  return $user = Auth()->user();
}


function terbilang_cap($nilai)
{
  $nilai = abs($nilai);
  $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  $temp = "";
  if ($nilai < 12) {
    $temp = " " . $huruf[$nilai];
  } else if ($nilai < 20) {
    $temp = terbilang_cap($nilai - 10) . " Belas";
  } else if ($nilai < 100) {
    $temp = terbilang_cap($nilai / 10) . " Puluh" . terbilang_cap($nilai % 10);
  } else if ($nilai < 200) {
    $temp = " Seratus" . terbilang_cap($nilai - 100);
  } else if ($nilai < 1000) {
    $temp = terbilang_cap($nilai / 100) . " Ratus" . terbilang_cap($nilai % 100);
  } else if ($nilai < 2000) {
    $temp = " Seribu" . terbilang_cap($nilai - 1000);
  } else if ($nilai < 1000000) {
    $temp = terbilang_cap($nilai / 1000) . " Ribu" . terbilang_cap($nilai % 1000);
  } else if ($nilai < 1000000000) {
    $temp = terbilang_cap($nilai / 1000000) . " Juta" . terbilang_cap($nilai % 1000000);
  } else if ($nilai < 1000000000000) {
    $temp = terbilang_cap($nilai / 1000000000) . " Milyar" . terbilang_cap(fmod($nilai, 1000000000));
  } else if ($nilai < 1000000000000000) {
    $temp = terbilang_cap($nilai / 1000000000000) . " Trilyun" . terbilang_cap(fmod($nilai, 1000000000000));
  }
  return $temp;
}

function hasRole()
{
  $seconds = 6000;
  $role = Cache::remember(md5('role_name' . auth()->user()->id), $seconds, function () {
    return user()->roles()->pluck('name')->toarray();
  });

  return $role;
}

function hasPermissions($permissions)
{
  // return user()->hasAnyPermission($permissions);
  return true;
}


function bakuDesa($kd_kecamatan, $kd_kelurahan, $tahun = '')
{
  if ($tahun == '') {
    $tahun = date('Y');
  }
  /* select sum(pbb_yg_harus_dibayar_sppt-potongan)
FROM core_sppt sppt
LEFT JOIN
(SELECT a.kd_propinsi,
a.kd_dati2,
a.kd_kecamatan,
a.kd_kelurahan,
a.kd_blok,
a.no_urut,
a.kd_jns_op,
a.thn_pajak_sppt,
kd_status
FROM sim_pbb.sppt_penelitian a
LEFT JOIN SIM_PBB.DATA_BILLING b
ON a.data_billing_id = b.data_billing_id
WHERE kd_status = '0') aj
ON     aj.kd_propinsi = sppt.kd_propinsi
AND aj.kd_dati2 = sppt.kd_dati2
AND aj.kd_kecamatan = sppt.kd_kecamatan
AND aj.kd_kelurahan = sppt.kd_kelurahan
AND aj.kd_blok = sppt.kd_blok
AND aj.no_urut = sppt.no_urut
AND aj.kd_jns_op = sppt.kd_jns_op
AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
WHERE     sppt.thn_pajak_sppt = '2023'
AND aj.thn_pajak_sppt IS NULL
AND sppt.kd_kecamatan = '080'
AND status_pembayaran_sppt IN ('0', '1')
and sppt.kd_kelurahan='008'  
 AND get_buku (pbb_yg_harus_dibayar_sppt) IN (1,2) */
  $sppt = db::connection("oracle_satutujuh")->table(db::raw("(select sum(pbb_yg_harus_dibayar_sppt-potongan) baku
  FROM core_sppt sppt
  LEFT JOIN
  (SELECT a.kd_propinsi,
  a.kd_dati2,
  a.kd_kecamatan,
  a.kd_kelurahan,
  a.kd_blok,
  a.no_urut,
  a.kd_jns_op,
  a.thn_pajak_sppt,
  kd_status
  FROM sim_pbb.sppt_penelitian a
  LEFT JOIN SIM_PBB.DATA_BILLING b
  ON a.data_billing_id = b.data_billing_id
  WHERE kd_status = '0') aj
  ON     aj.kd_propinsi = sppt.kd_propinsi
  AND aj.kd_dati2 = sppt.kd_dati2
  AND aj.kd_kecamatan = sppt.kd_kecamatan
  AND aj.kd_kelurahan = sppt.kd_kelurahan
  AND aj.kd_blok = sppt.kd_blok
  AND aj.no_urut = sppt.no_urut
  AND aj.kd_jns_op = sppt.kd_jns_op
  AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
  WHERE     sppt.thn_pajak_sppt = '" . $tahun . "'
  AND aj.thn_pajak_sppt IS NULL
  AND sppt.kd_kecamatan = '" . $kd_kecamatan . "'
  and sppt.kd_kelurahan='" . $kd_kelurahan . "'  
  AND status_pembayaran_sppt IN ('0', '1')
   AND get_buku (pbb_yg_harus_dibayar_sppt) IN (1,2))"))->first();

  return $sppt->baku;

  /* $sppt = DB::connection("oracle_dua")->table("mv_baku_desa")->selectraw("sum(baku) baku ")->where('kd_kecamatan', $kd_kecamatan)
    ->where('kd_kelurahan', $kd_desa)->whereraw("buku in (1,2)")->first();

  return $sppt->baku; */
}


function bakuKecamatan($tahun, $kd_kecamatan)
{

  $sppt = DB::connection("oracle_dua")->table("mv_baku_kecamatan")->where('kd_kecamatan', $kd_kecamatan)->where('thn_pajak_sppt', $tahun)->whereraw("buku in (1,2)")->selectraw("sum(baku) baku")->first();

  return $sppt->baku;
}


function formatnop($angkab)
{
  $angkaa = $angkab;
  $c = "";
  $panjang = strlen($angkaa);

  if ($panjang <= 2) {
    $c = $angkaa;
  } else if ($panjang > 2 && $panjang <= 4) {
    $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2);
  } else if ($panjang > 4 && $panjang <= 7) {
    $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3);
  } else if ($panjang > 7 && $panjang <= 10) {
    $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3);
  } else if ($panjang > 1 && $panjang <= 14) {
    $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3) . '.' . substr($angkaa, 10, 3);
  } else if ($panjang > 14 && $panjang <= 17) {
    $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3) . '.' . substr($angkaa, 10, 3) . '-' . substr($angkaa, 13, 4);
  } else {
    $c = substr($angkaa,  0, 2) . '.' . substr($angkaa, 2, 2) . '.' . substr($angkaa, 4, 3) . '.' . substr($angkaa, 7, 3) . '.' . substr($angkaa, 10, 3) . '-' . substr($angkaa, 13, 4) . '.' . substr($angkaa, 17, 1);
  }
  return $c;
}

// function acak($string)
// {
//   return Crypt::encryptString($string);
// }

function rapikanacak($encrypted)
{
  return  Crypt::decryptString($encrypted);
}

function getNoSkNjop($tahun = null)
{
  $AWAL = 'A06709/KET-1/35.07.205';
  $tahun = $tahun ?? date('Y');
  $noUrutAkhir =  (int)  SkNJop::whereraw("to_char(created_at,'yyyy')='$tahun'")->max('nomer_sk');
  $no = 1;
  if ($noUrutAkhir) {
    $no = abs($noUrutAkhir + 1);
  }

  return sprintf("%04s", $no) . '/' . $AWAL  . '/' . $tahun;
}


function getNoSuratTugas($tahun = null)
{
  $AWAL = 'ST-VERLAP/35.07';
  $tahun = $tahun ?? date('Y');
  $noUrutAkhir =  (int)  SuratTugas::whereraw("to_char(verifikasi_at,'yyyy')='$tahun'")->max('nomor_surat');
  $no = 1;
  if ($noUrutAkhir) {
    $no = abs($noUrutAkhir + 1);
  }

  return sprintf("%04s", $no) . '/' . $AWAL  . '/' . $tahun;
}

function terbilang($nilai)
{
  $nilai = abs($nilai);
  $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  $temp = "";
  if ($nilai < 12) {
    $temp = " " . $huruf[$nilai];
  } else if ($nilai < 20) {
    $temp = terbilang($nilai - 10) . " belas";
  } else if ($nilai < 100) {
    $temp = terbilang($nilai / 10) . " puluh" . terbilang($nilai % 10);
  } else if ($nilai < 200) {
    $temp = " seratus" . terbilang($nilai - 100);
  } else if ($nilai < 1000) {
    $temp = terbilang($nilai / 100) . " ratus" . terbilang($nilai % 100);
  } else if ($nilai < 2000) {
    $temp = " seribu" . terbilang($nilai - 1000);
  } else if ($nilai < 1000000) {
    $temp = terbilang($nilai / 1000) . " ribu" . terbilang($nilai % 1000);
  } else if ($nilai < 1000000000) {
    $temp = terbilang($nilai / 1000000) . " juta" . terbilang($nilai % 1000000);
  } else if ($nilai < 1000000000000) {
    $temp = terbilang($nilai / 1000000000) . " milyar" . terbilang(fmod($nilai, 1000000000));
  } else if ($nilai < 1000000000000000) {
    $temp = terbilang($nilai / 1000000000000) . " trilyun" . terbilang(fmod($nilai, 1000000000000));
  }
  return $temp;
}

if (!function_exists('statusnjop')) {
  function statusnjop($st)
  {
    $status = ['<i class="fab fa-stack-overflow text-info"></i>  Proses', '<i class="far fa-check-circle text-success"></i>  Telah di proses', '<i class="far fa-times-circle text-danger"></i> Belum lunas 5 tahun'];

    return $status[$st];
  }
}


if (!function_exists('angka')) {
  function angka($nilai = 0, $prefix = 0)
  {
    $prefix = 0;
    $cr = explode('.', $nilai);
    if (isset($cr[1])) {
      $prefix = strlen($cr[1]);
    }

    return number_format($nilai, $prefix, ',', '.');
  }
}


// cek lunas 5 tahun
if (!function_exists('lunas_limas_tahun')) {
  function lunas_limas_tahun($kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op)
  {
    $param = [
      'kd_kecamatan' => $kd_kecamatan,
      'kd_kelurahan' => $kd_kelurahan,
      'kd_blok' => $kd_blok,
      'no_urut' => $no_urut,
      'kd_jns_op' => $kd_jns_op
    ];
    $cek = InformasiObjek::lunasLimaTahun($param);

    if ($cek->op == $cek->lunas) {
      $res = 1;
    } else {
      $res = 0;
    }

    // return 1;
    return $res;
  }
}

if (!function_exists('romawi')) {
  function romawi($angka)
  {
    switch ($angka) {
      case 1:
        return "I";
        break;
      case 2:
        return "II";
        break;
      case 3:
        return "III";
        break;
      case 4:
        return "IV";
        break;
      case 5:
        return "V";
        break;
      case 6:
        return "VI";
        break;
      case 7:
        return "VII";
        break;
      case 8:
        return "VIII";
        break;
      case 9:
        return "IX";
        break;
      case 10:
        return "X";
        break;
      case 11:
        return "XI";
        break;
      case 12:
        return "XII";
        break;
    }
  }
}


if (!function_exists('ArrayJenisBumi')) {
  function ArrayJenisBumi()
  {
    return  [1 => 'TANAH + BANGUNAN', 'KAVLING SIAP BANGUN', 'TANAH KOSONG', 'FASILITAS UMUM', 'NON AKTIF'];
  }
}

if (!function_exists('jenisBumi')) {
  function jenisBumi($angka)
  {
    $angka = (int)$angka;
    if ($angka == '') {
      return '';
    }
    // $array = [1 => 'TANAH + BANGUNAN', 'KAVLING SIAP BANGUN', 'TANAH KOSONG', 'FASILITAS UMUM', 'LAIN - LAIN'];
    $array = ArrayJenisBumi();
    return $array[$angka];
  }
}

if (!function_exists('jenisTransaksiTanah')) {
  function jenisTransaksiTanah($angka)
  {
    $angka = (int)$angka;
    if ($angka == '') {
      return '';
    }
    $ARRAY = [1 => 'PEREKAMAN DATA', 'PEMUTAKHIRAN DATA', 'PENGHAPUSAN DATA', 'PENGHAPUSAN STATUS OP BERSAMA'];
    return $ARRAY[$angka];
  }
}


if (!function_exists('jenisTransaksiBangunan')) {
  function jenisTransaksiBangunan($angka)
  {
    $angka = (int)$angka;
    if ($angka == '') {
      return '';
    }

    $ARRAY = [1 => 'PEREKAMAN DATA', 'PEMUTAKHIRAN DATA', 'PENGHAPUSAN DATA', 'PENILAIAN INDIVIDUAL'];
    return $ARRAY[$angka];
  }
}


function ArrayStatusWP()
{
  return  [
    '1' => 'PEMILIK',
    '2' => 'PENYEWA',
    '3' => 'PENGELOLA',
    '4' => 'PEMAKAI / PEMANFAAT',
    '5' => 'SENGKETA'
  ];
}

if (!function_exists('statusWP')) {
  function statusWP($angka)
  {
    $angka = (int)$angka;
    if ($angka == '') {
      return '';
    }

    $array = ArrayStatusWP();
    return $array[$angka] ?? '';
  }
}


function ArrayJenisPekerjaan()
{
  return [
    '1' => 'PNS',
    '2' => 'ABRI',
    '3' => 'PENSIUNAN',
    '4' => 'BADAN',
    '5' => 'LAINNYA'
  ];
}

function jenisPekerjaan($angka)
{
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = ArrayJenisPekerjaan();
  return $data[$angka];
}

function ArrayPenggunaanBangunan()
{
  return [
    1 =>
    'PERUMAHAN',
    'PERKANTORAN SWASTA',
    'PABRIK',
    'TOKO/APOTIK/PASAR/RUKO',
    'RUMAH SAKIT/KLINIK',
    'OLAHRAGA/REKREASI',
    'HOTEL/WISMA',
    'BENGKEL/GUDANG/PERTANIAN',
    'GEDUNG PEMERINTAH',
    'LAIN - LAIN',
    'BANGUNAN TIDAK KENA PAJAK',
    'BANGUNAN PARKIR',
    'APARTEMEN',
    'POMPA BENSIN',
    'TANGKI MINYAK',
    'GEDUNG SEKOLAH'
  ];
}

function penggunaanBangunan($angka)
{

  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  } else {

    $data = ArrayPenggunaanBangunan();
    return $data[$angka];
  }
}

function ArrayKondisiBng()
{
  return [
    1 => 'SANGAT BAIK',
    'BAIK',
    'SEDANG',
    'JELEK',
  ];
}

function kondisi($angka)
{
  $angka = (int)$angka;
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = ArrayKondisiBng();
  return $data[$angka];
}

function arrayKonstruksi()
{
  return [
    1 => 'BAJA',
    'BETON',
    'BATU BATA',
    'KAYU',
  ];
}

function kontruksi($angka)
{
  $angka = (int)$angka;
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = arrayKonstruksi();
  return $data[$angka];
}

function arrayAtap()
{
  return  [
    1 =>
    'DECRABON / BETON / GTG GLAZUR',
    'GTG BETON / ALUMUNIUM',
    'GTG BIASA / SIRAP',
    'ASBES',
    'SENG',
  ];
}

function atap($angka)
{
  $angka = (int)$angka;

  if ($angka == '') {
    return '';
  }
  $data = arrayAtap();
  return $data[$angka];
}


function arrayDinding()
{
  return [
    1 =>
    'KACA / ALUMINIUM',
    'BETON',
    'BATU BATA / CONBLOK',
    'KAYU',
    'SENG',
  ];
}

function dinding($angka)
{
  $angka = (int)trim($angka);
  if ($angka == '') {
    return null;
  }

  $data = arrayDinding();
  return $data[$angka] ?? null;
}

function arrayLantai()
{
  return [
    1 =>
    'MARMER',
    'KERAMIK',
    'TEROSO',
    'UBIN PC / PAPANA',
    'SEMEN',
  ];
}

function lantai($angka)
{
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = arrayLantai();
  return $data[$angka];
}

function arrayLangit()
{
  return [
    1 =>
    'AKUSTIK / JATI',
    'TRIPLEK / ASBES BAMBU',
    'TIDAK ADA ',
  ];
}

function langit($angka)
{
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = arrayLangit();

  return $data[$angka];
}

function jenisHotel($angka)
{
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = [
    1 =>
    'Non Resort',
    'Resort',
  ];
  return $data[$angka];
}

function bintangHotel($angka)
{
  $angka = (int)$angka;
  if ($angka == '') {
    return '';
  }
  $data = [
    1 =>
    'Bintang 5',
    'Bintang 4',
    'Bintang 3',
    'Bintang 1-2',
    'Non Bintang'
  ];
  return $data[$angka];
}

function sensorText($target)
{
  // awal replace
  $count = strlen($target) - 2;

  $b = substr($target, strlen($target) - 2, 2);

  $output = substr_replace($target, str_repeat('*', $count), 2, $count);
  return $output;
}


function formatNomor($formulir)
{
  return substr($formulir, 0, 4) . '.' . substr($formulir, 4, 4) . '.' . substr($formulir, 8, 3);
}

function randomString($length = 10)
{
  $characters = '0123456789';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}


function getNamaSubjek($nop)
{
  $kd_propinsi = substr($nop, 0, 2);
  $kd_dati2 = substr($nop, 2, 2);
  $kd_kecamatan = substr($nop, 4, 3);
  $kd_kelurahan = substr($nop, 7, 3);
  $kd_blok = substr($nop, 10, 3);
  $no_urut = substr($nop, 13, 4);
  $kd_jns_op = substr($nop, 17, 1);

  $get = DB::connection('oracle_dua')->table(DB::raw('dat_objek_pajak a'))
    ->join(db::raw('dat_subjek_pajak b'), 'a.subjek_pajak_id', '=', 'b.subjek_pajak_id')
    ->whereraw("
      kd_propinsi='$kd_propinsi' and 
      kd_dati2='$kd_dati2' and 
      kd_kecamatan='$kd_kecamatan' and 
      kd_kelurahan='$kd_kelurahan' and 
      kd_blok='$kd_blok' and 
      no_urut='$no_urut' and 
      kd_jns_op='$kd_jns_op' 
      ")->select('nm_wp')->first();

  return $get->nm_wp ?? null;
}


function showMenu($parent = 0)
{
  $role_id = Cache::remember(md5('role_menu_id' . Auth()->user()->id), 6000, function () {
    return     implode(',', Auth()->user()->roles()->pluck('id')->toarray());
  });

  $menus = cacheSelectQuery("select a.id,a.title,a.url,a.icon,a.parent_id,a.urut,count(b.parent_id) childs
  from menus a
  left join menus b on a.id=b.parent_id
  where a.id in (select distinct  menu_id from role_has_menus where role_id in (" . $role_id . ")) and a.parent_id='" . $parent . "'
  group by a.id,a.title,a.url,a.icon,a.parent_id,a.urut order by a.urut asc,a.id asc", 6000);
  return $menus;
}

function cacheSelectQuery($sql, $timeout = 60)
{
  return Cache::remember(md5($sql), $timeout, function () use ($sql) {
    return DB::select(DB::raw($sql));
  });
}

function arrayKolam()
{
  return [
    1 => 'DI PLESTER',
    'DENGAN PELAPIS'
  ];
}

function ArrayPagar()
{
  return [
    1 => 'Besi / Baja',
    'Batu Bata / Batako'
  ];
}

function ArrayTrueFalse()
{
  return [
    1 => 'Ada',
    'Tidak Ada'
  ];
}

function acak($string)
{
  return Crypt::encryptString($string);
}
function rapikan($string)
{
  return Crypt::decryptString($string);
}

function onlyNumber($string)
{
  return preg_replace('/[^0-9]/', '', $string);
}

function padding($string, $prefix, $length)
{
  return str_pad($string, $length, $prefix, STR_PAD_LEFT);
}

function KodePengesahan()
{
  $digits = 12;
  $i = 0; //counter
  $pin = ""; //our default pin is blank.
  while ($i < $digits) {
    $pin .= mt_rand(0, 9);
    $i++;
  }
  return '3507' . $pin;
}


function microtime_float()
{
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}

function typeKonstruksi()
{
  return [
    '1' => 'ringan',
    'sedang',
    'menengah',
    'berat',
    'sangat berat'
  ];
}


function typeparkiran()
{
  return [
    '1' => 'kecil',
    'sedang',
    'menengah',
    'besar',
    'sangat besar'
  ];
}

function splitnop($nop = 0)
{
  $res = onlyNumber($nop);
  // $res = str_replace('.', '', $nop);
  $result['kd_propinsi'] = substr($res, 0, 2) ?? '';
  $result['kd_dati2'] = substr($res, 2, 2) ?? '';
  $result['kd_kecamatan'] = substr($res, 4, 3) ?? '';
  $result['kd_kelurahan'] = substr($res, 7, 3) ?? '';
  $result['kd_blok'] = substr($res, 10, 3) ?? '';
  $result['no_urut'] = substr($res, 13, 4) ?? '';
  $result['kd_jns_op'] = substr($res, 17, 1) ?? '';
  return $result;
}


function pecahObjek($elements, $parentId = 0)
{
  $branch = array();
  foreach ($elements as $element) {
    if ($element['nop_asal'] == $parentId) {
      $children = pecahObjek($elements, $element['nop_asal']);
      if ($children) {
        $element['pecahan'] = $children;
      }
      $branch[] = $element;
    }
  }

  return $branch;
}


function RoleInternal()
{
  return [301, 61, 62, 63, 64, 65, 66, 67, 161, 181, 182];
}

function KategoriIventarisasi()
{
  return [
    // '00' =>'Blokir, usulan koreksi',
    '01' => 'WP Bandel',
    '02' => 'WP tidak di ketahui',
    '03' => 'Sengketa',
    '04' => 'Fasum',
    '05' => 'SPPT ganda',
    '06' => 'Objek tidak ada',
    '07' => 'Penyesuaian'
  ];
}

function jenisKobil($jns)
{

  switch ($jns) {
    case '6':
      # code...
      $jenis = 'Virtual Account';
      break;
    case '1':
      # code...
      $jenis = 'Kode Billing';
      break;
    case '7':
      # code...
      $jenis = 'QRIS';
      break;
    default:
      # code...
      $jenis = 'Nota Perhitungan';
      break;
  }
  return $jenis;

  /* $jenis = [
    'desa',
    'pendataan',
    'Penelitian',
    'pengaktifan',
    'STPD',
    'Virtual Account',
    'Qris'
  ]; */


  /*    1 => desa
    2 => pendataan
    3 => Penelitian
    4 => pengaktifan
    5 => STPD
	6 => Virtual Account
	7 => Qris */
}


function getNamaLokasiZnt($kd_kecamatan, $kd_kelurahan, $kd_znt)
{
  $hsl = DB::connection("oracle_satutujuh")->table(DB::raw("dat_znt a"))
    ->join(db::raw("sim_pbb.lokasi_objek b"), "a.lokasi_objek_id", "=", "b.id")
    ->whereraw("kd_kecamatan='$kd_kecamatan'
    and kd_kelurahan='$kd_kelurahan'
    and kd_znt='$kd_znt'")
    ->select("nama_lokasi")->first();
  return $hsl->nama_lokasi ?? '';
}


function getNopWp()
{
  $nik = Auth()->user()->nik;
  $user_id = Auth()->user()->id;
  // $is_pengelola = Auth()->user()->is_pengelola;

  $is_pengelola = Auth()->user()->hasrole(['DEVELOPER/PENGEMBANG', 'Rayon', 'Wajib Pajak']);

  $milik = DB::connection("oracle_satutujuh")->select("select a.kd_propinsi||a.kd_dati2||a.kd_kecamatan||a.kd_kelurahan||a.kd_blok||a.no_urut||a.kd_jns_op nop,trim(nm_wp) nm_wp,trim(a.subjek_pajak_id) nik
      from dat_objek_pajak a
      join dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
      where trim(a.subjek_pajak_id)='$nik'");

  $nop = [];
  foreach ($milik as $item) {
    $nop[] = ['nop' => $item->nop, 'nama' => $item->nm_wp];
  }

  if ($is_pengelola == true) {
    $kelola = DB::select("select distinct kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop,
    (select nm_wp
from pbb.dat_objek_pajak a
join pbb.dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
where a.kd_kecamatan=kelola_usulan_objek.kd_kecamatan and kd_kelurahan=kelola_usulan_objek.kd_kelurahan
and kd_blok=kelola_usulan_objek.kd_blok and no_urut=kelola_usulan_objek.no_urut
and kd_jns_op=kelola_usulan_objek.kd_jns_op) nm_wp
    from kelola_usulan_objek
    join kelola_usulan  on   kelola_usulan.id=kelola_usulan_objek.kelola_usulan_id
    where kelola_usulan.nik='" . $nik . "' and kelola_usulan_objek.verifikasi_kode='1'");

    foreach ($kelola as $item) {
      $nop[] = ['nop' => $item->nop, 'nama' => $item->nm_wp];
    }
  }


  return $nop;
}

function NomorWa($nomorTelepon)
{

  /*   if (!preg_match('/[^+0-9]/', trim($nohp))) {
    // cek apakah no hp karakter 1-3 adalah +62
    if (substr(trim($nohp), 0, 2) == '62') {
      $phone_number = trim($nohp);
    }
    // cek apakah no hp karakter 1 adalah 0
    elseif (substr(trim($nohp), 0, 1) == '0') {
      $phone_number = '62' . substr(trim($nohp), 1);
    } else {
      $phone_number = '';
    }
  }
 */
  // return $phone_number;
  // Menghapus spasi, tanda hubung, atau karakter non-digit
  $nomorTelepon = preg_replace('/[^0-9]/', '', $nomorTelepon);

  // Jika nomor dimulai dengan "0", ganti dengan "+62"
  if (substr($nomorTelepon, 0, 1) === '0') {
    $nomorTelepon = '+62' . substr($nomorTelepon, 1);
  }

  // Jika nomor sudah dalam format internasional (+62), biarkan
  // Jika nomor sudah menggunakan kode negara lain, biarkan

  return $nomorTelepon;
}

function formatNomorTelepon($nomorTelepon)
{
  // Menghapus spasi, tanda hubung, atau karakter non-digit
  $nomorTelepon = preg_replace('/[^0-9]/', '', $nomorTelepon);

  // Jika nomor dimulai dengan "0", ganti dengan "+62"
  if (substr($nomorTelepon, 0, 1) === '0') {
    $nomorTelepon = '+62' . substr($nomorTelepon, 1);
  }

  // Jika nomor sudah dalam format internasional (+62), biarkan
  // Jika nomor sudah menggunakan kode negara lain, biarkan

  return $nomorTelepon;
}


function generateRegistrationNumber($prefix, $bundle, $counter)
{
  // Format nomor urut dengan leading zero (tiga digit)
  $counter = str_pad($counter, 3, '0', STR_PAD_LEFT);

  // Format bundle dengan leading zero (tiga digit)
  $bundle = str_pad($bundle, 3, '0', STR_PAD_LEFT);

  // Gabungkan semua bagian menjadi satu string
  return $prefix . $bundle  . $counter;
}

function nextRegistrationNumber($currentNumber)
{
  // Pisahkan string menjadi bagian-bagian
  list($prefix, $bundle, $counter) = explode(' ', $currentNumber);

  // Konversi bundle dan counter menjadi integer
  $bundle = (int)$bundle;
  $counter = (int)$counter;

  // Logika untuk meningkatkan counter dan bundle
  if ($counter < 999) {
    $counter++;
  } else {
    $counter = 1;
    $bundle++;
  }

  // Kembalikan nomor registrasi baru
  return generateRegistrationNumber($prefix, $bundle, $counter);
}

function buatAkronim($teks)
{
  // Pecah string menjadi array kata-kata
  $kataKata = explode(" ", $teks);

  // Inisialisasi variabel untuk menyimpan huruf pertama dari setiap kata
  $akronim = "";

  // Loop untuk mengambil huruf pertama dari setiap kata
  foreach ($kataKata as $kata) {
    // Pastikan kata tidak kosong
    if (!empty($kata)) {
      $akronim .= strtoupper($kata[0]); // Tambahkan huruf pertama dalam huruf kapital
    }
  }

  return $akronim;
}

function formatTo16Digit($number)
{
  // Pastikan hanya angka
  $number = preg_replace('/\D/', '', $number);

  // Jika lebih dari 16 digit, ambil dari kanan
  if (strlen($number) > 16) {
    return substr($number, -16);
  }

  // Jika kurang dari 16 digit, tambahkan 0 di depan
  return str_pad($number, 16, '0', STR_PAD_LEFT);
}
