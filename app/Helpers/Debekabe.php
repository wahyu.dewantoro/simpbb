<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Debekabe
{
    public static function Resource($tahun)
    {
        return DB::connection("oracle_satutujuh")->table(DB::raw("hrg_resource a"))
            ->join(DB::raw("item_resource b"), function ($join) {
                $join->on('a.kd_group_resource', '=', 'b.kd_group_resource')
                    ->on('a.kd_resource', '=', 'b.kd_resource');
            })
            ->join(db::raw("group_resource c"), 'c.kd_group_resource', '=', 'b.kd_group_resource')
            ->select(db::raw("nm_group_resource,nm_resource,satuan_resource,hrg_resource"))
            ->whereraw("thn_hrg_resource='$tahun'")
            ->orderByraw("a.kd_group_resource,a.kd_resource asc");
    }
}
