<?php

namespace App\Helpers;

use App\Models\Layanan_objek;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LayananUpdate
{

  public static function updateDataDiri_sppt($set,$where){
    if(!($set&&$where)) return;
    $tahun=Carbon::now()->year;
    $data = DB::connection('oracle_satutujuh')
        ->update("UPDATE sppt
            SET nm_wp_sppt = '".$set['nama']."',
            jln_wp_sppt = '".$set['alamat']."',
            kelurahan_wp_sppt = '".$set['kelurahan']."',
            kota_wp_sppt = '".$set['dati2']."'
            WHERE     thn_pajak_sppt = '".$tahun."'
            AND kd_propinsi = ?
            AND kd_dati2 = ?
            AND kd_kecamatan = ?
            AND kd_kelurahan = ?
            AND kd_blok = ?
            AND no_urut = ?
            AND kd_jns_op = ?
            and status_pembayaran_sppt='0'", $where);
    $data=true;
    return $data;
  }
  public static function getOPlog($where){
    if(!$where) return;
    $cek_log =   DB::connection('oracle_satutujuh')->select("select * 
    from dat_subjek_pajak_log 
    where subjek_pajak_id in (
      select subjek_pajak_id 
        from dat_objek_pajak
        where  kd_propinsi = ?
          AND kd_dati2 = ?
          AND kd_kecamatan = ?
          AND kd_kelurahan = ?
          AND kd_blok = ?
          AND no_urut = ?
          AND kd_jns_op = ?)", $where);
      $result=['nama_wp'=>false,'alamat_wp'=>false,'nomor_telepon'=>false,'alamat_op'=>false];
      if($cek_log){
        $set=(array)current($cek_log);
        $result=['nama_wp'=>$set['nm_wp'],
              'alamat_wp'=>$set['jalan_wp'],
              'nomor_telepon'=>$set['telp_wp'],
              'alamat_op'=>$set['alamat_op'] //alamat_op
            ];
      }
      return $result;
  }
  public static function updateDataDiri_dat_subjek_pajak($where,$set=[],$pajak_id=false){
    if(!$where) return;
    $connction=DB::connection('oracle_satutujuh');
    $cek_log =  $connction->select("select subjek_pajak_id from dat_subjek_pajak_log where subjek_pajak_id in (select subjek_pajak_id 
    from dat_objek_pajak
    where  kd_propinsi = ?
      AND kd_dati2 = ?
      AND kd_kecamatan = ?
      AND kd_kelurahan = ?
      AND kd_blok = ?
      AND no_urut = ?
      AND kd_jns_op = ?)", $where);
    if($cek_log){
      // delete exist log
      $cek_log=(array)current($cek_log);
      $connction->delete('delete from dat_subjek_pajak_log where subjek_pajak_id=?',[$cek_log['subjek_pajak_id']]);
    }
    $selectSet="";
    if($set){
      foreach($set as $key=>$item){
        $selectSet.=",'".$item."' ".$key;
      }
    }
    $connction->insert("insert into dat_subjek_pajak_log 
    select '".$pajak_id."' subjek_pajak_id,
          dat_subjek_pajak.nm_wp,
          dat_subjek_pajak.jalan_wp,
          dat_subjek_pajak.blok_kav_no_wp,
          dat_subjek_pajak.rw_wp,
          dat_subjek_pajak.rt_wp,
          dat_subjek_pajak.kelurahan_wp,
          SUBSTR(dat_subjek_pajak.kota_wp,0,30) kota_wp,
          dat_subjek_pajak.kd_pos_wp,
          dat_subjek_pajak.telp_wp,
          dat_subjek_pajak.npwp,
          dat_subjek_pajak.status_pekerjaan_wp,
          SYSDATE".$selectSet."
    from dat_subjek_pajak 
    where subjek_pajak_id in (select subjek_pajak_id 
        from dat_objek_pajak
        where  kd_propinsi = ?
          AND kd_dati2 = ?
          AND kd_kecamatan = ?
          AND kd_kelurahan = ?
          AND kd_blok = ?
          AND no_urut = ?
          AND kd_jns_op = ?)", $where);
    $data=true;
    // $data = DB::connection('oracle_satutujuh')
    //     ->update("UPDATE dat_subjek_pajak 
    //     SET nm_wp='".$set['nama']."',
    //         jalan_wp = '".$set['alamat']."',
    //         kelurahan_wp = '".$set['kelurahan']."',
    //         kota_wp = '".$set['dati2']."',
    //         telp_wp = '".$set['nomor_telepon']."'
    //     where subjek_pajak_id in (select subjek_pajak_id 
    //     from dat_objek_pajak
    //     where  kd_propinsi = ?
    //       AND kd_dati2 = ?
    //       AND kd_kecamatan = ?
    //       AND kd_kelurahan = ?
    //       AND kd_blok = ?
    //       AND no_urut = ?
    //       AND kd_jns_op = ?)", $where);
    // $data=[];
    return $data;
  }
  public static function cekpembayaran($nop,$blok_urut){
    $result=true;
    $cekBayar=InformasiObjek::riwayatPembayaran([
          'kd_propinsi'=>$nop[0],
          'kd_dati2'=>$nop[1],
          'kd_kecamatan'=>$nop[2],
          'kd_kelurahan'=>$nop[3],
          'kd_blok'=>$blok_urut[0],
          'no_urut'=>$blok_urut[1],
          'kd_jns_op'=>$nop[5],
        ])->orderby('sppt.thn_pajak_sppt')->get();
    if($cekBayar->count()>0){
      $count=0;
      foreach($cekBayar->toArray() as $item){
          if($item->tgl_pembayaran_sppt!=""){
              $count++;
          }
      }
      // if($cekBayar->count()>5){
      //   $result=($count>=5)?true:false;
      // }else{
        $result=($cekBayar->count()!=$count)?false:true;
      // }
    }
    return $result;
  }
  public static function getPBB($nopI,$tahun){
    $nop=explode(".",$nopI);
    $nopz=explode("-",$nop[4]);
    // $sppt = DB::connection('oracle_dua')->table('sppt')
    //     ->selectraw("pbb_yg_harus_dibayar_sppt pbb ")
    //     ->whereraw("kd_propinsi ='" . $kd_propinsi . "'")
    //     ->whereraw("kd_dati2 ='" . $kd_dati2 . "'")
    //     ->whereraw("kd_kecamatan ='" . $kd_kecamatan . "'")
    //     ->whereraw("kd_kelurahan ='" . $kd_kelurahan . "'")
    //     ->whereraw("kd_blok ='" . $kd_blok . "'")
    //     ->whereraw("no_urut ='" . $no_urut . "'")
    //     ->whereraw("kd_jns_op ='" . $kd_jns_op . "' and  thn_pajak_sppt>=2014")
    //     ->orderby('THN_PAJAK_SPPT')->get();

    $sql="select so.pbb_yg_harus_dibayar_sppt pbb,denda
            FROM spo.sppt_oltp so
            WHERE so.THN_PAJAK_SPPT = '".$tahun."'
                AND so.KD_PROPINSI = '".$nop[0]."'
                AND so.KD_DATI2 = '".$nop[1]."'
                AND so.KD_KECAMATAN = '".$nop[2]."'
                AND so.KD_KELURAHAN = '".$nop[3]."'
                AND so.KD_BLOK = '".$nopz[0]."'
                AND so.NO_URUT = '".$nopz[1]."'
                AND so.KD_JNS_OP = '".$nop[5]."'
                ";
    return DB::connection('oracle')->select(db::raw($sql));
  }
  public static function cekNOPLayanan($nop,$blok_urut,$tahun=false){
    if(!$tahun){
      $tahun=Carbon::now()->year;
    }
    $cekBayar=Layanan_objek::where([
          'layanan_objek.kd_propinsi'=>$nop[0],
          'layanan_objek.kd_dati2'=>$nop[1],
          'layanan_objek.kd_kecamatan'=>$nop[2],
          'layanan_objek.kd_kelurahan'=>$nop[3],
          'layanan_objek.kd_blok'=>$blok_urut[0],
          'layanan_objek.no_urut'=>$blok_urut[1],
          'layanan_objek.kd_jns_op'=>$nop[5]
        ])
        ->whereRaw(Db::raw("EXTRACT(YEAR FROM layanan.created_at)='".$tahun."' 
            and layanan.deleted_at is null 
            and layanan_objek.is_tolak is null
            and layanan.JENIS_LAYANAN_ID !='10'
            and (layanan.JENIS_LAYANAN_ID !='6' or (layanan_objek.NOP_GABUNG IS NOT NULL AND  layanan.JENIS_LAYANAN_ID ='6')) 
            and (layanan_objek.is_tolak is null or layanan_objek.is_tolak='0')
            AND (layanan.JENIS_LAYANAN_ID != '7' OR (layanan_objek.NOP_GABUNG IS NULL AND layanan.JENIS_LAYANAN_ID = '7'))"))
        ->leftJoin('layanan',function($join){
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
        ->get();
    return ($cekBayar->count()>0)?true:false;
  }
  public static function riwayatPembayaran($reqNop){
    if(!$reqNop) return;
    $nop=explode(".",$reqNop['nop']);
    $blok_urut=explode("-",$nop[4]);
    $where=[
      'kd_propinsi'=>$nop[0],
      'kd_dati2'=>$nop[1],
      'kd_kecamatan'=>$nop[2],
      'kd_kelurahan'=>$nop[3],
      'kd_blok'=>$blok_urut[0],
      'no_urut'=>$blok_urut[1],
      'kd_jns_op'=>$nop[5],
    ];
    $listData=Layanan_conf::riwayatPembayaran($where)
              ->orderby('sppt.thn_pajak_sppt')
              ->get();
    $setData=[];
    if($listData->count()>0){
      $no=1;
      foreach($listData as $item){
        if($item->tgl_pembayaran_sppt==''){
          $setData[]=[
            '',
            $item->kd_propinsi . '.' . $item->kd_dati2 . '.' . $item->kd_kecamatan . '.' . $item->kd_kelurahan . '.' . $item->kd_blok . '-' . $item->no_urut . '.' . $item->kd_jns_op,
            $item->thn_pajak_sppt,
            $item->nm_wp_sppt,
            $item->pbb,
            $item->pokok_bayar??'0',
            $item->denda_bayar??'0',
            ($item->total_bayar!=0) ? $item->total_bayar:angka(str_replace('.','',$item->denda_bayar) + str_replace('.','',$item->pokok_bayar)),
            'Belum lunas.'
            // $item->tgl_pembayaran_sppt<>''? tglindo($item->tgl_pembayaran_sppt):''
          ];
          $no++;
        }
      }
    }
    $result=gallade::generateinTbody($setData,"Data Masih Kosong",9);
    return  $result;
  }
                 
}
