<?php

namespace App\Helpers;


use Carbon\Carbon;
class gallade
{

    public static function getMonth($number=false) {
        $month=[
            '1'=>'Januari',
            '2'=>'Februari',
            '3'=>'Maret',
            '4'=>'April',
            '5'=>'Mei',
            '6'=>'Juni',
            '7'=>'Juli',
            '8'=>'Agustus',
            '9'=>'September',
            '10'=>'Oktober',
            '11'=>'November',
            '12'=>'Desember'
        ]; 
        if(!in_array($number,array_keys($month))){
            return false;
        }
        return $month[$number];
    }
    public static function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 
        $bytes /= pow(1024, $pow);
        //$bytes /= (1 << (10 * $pow)); 
        return round($bytes, $precision) . ' ' . $units[$pow]; 
    }
    public static function generateinTbody($data=false,$null=false,$length='7'){
		$tropen="<tr>";
        if(!$data&&is_array($data)){
			$data=array($null);
            $tropen="<tr class='null'>";
		}
		$trclose="</tr>";
		$set="";
		foreach($data as $item){
			$attr="";
			if(isset($item["attr"])){
				$tropen="<tr ".$item["attr"].">";
				unset($item["attr"]);
			}
			$item=gallade::_generateTd($item,$length);
			$set.=$tropen.$item.$trclose;
		}
		$return=$set;
		return $return;

    }
    public static function money_float($str=0){
        return str_replace('.',"",$str);
    }
    public static function _generateTd($data=false,$length='7'){
        $return=false;
        if($data&&!is_array($data)){
            return "<td colspan='".$length."' class='dataTables_empty text-center'>".$data."</td>";
        }
        if($data&&is_array($data)){
            $trclose="</td>";
            $set="";
            foreach($data as $k=>$item){
                $tropen="<td data-name='".$k."'>";
                if(is_array($item)){
                    foreach ($item as $key => $val){
                        if ($key !== 'data'){
                            $tropen = str_replace('<td', '<td '.$key.'="'.$val.'"', $tropen);
                        }
                    }
                    $item=$item["data"];
                }
                $set.=$tropen.$item.$trclose;
            }
            $return=$set;
        }        
        return $return; 
    }
    public static function chunk($data=[],$chunk=100){ 
        return collect($data)->chunk($chunk);
    }
    public static function parseQuantity($qt=0){
        return number_format($qt,false,'','.');
    }
    public static function anchorInfo($url,$value,$attr,$disabled='',$type='info'){
        return "<a target='_BLANK' href='".$url."' type='button' class='btn btn-info btn-".$type." ".$disabled."' ".$attr.">".$value."</a>";
    }
    public static function buttonInfo($value,$attr,$type='info'){
        return "<button type='button' class='btn btn-".$type." btn-flat' ".$attr.">".$value."</button>";
    }
    public static function clean($string) {
        $string = str_replace(' ', '-', strtolower($string));
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
    }
    public static function parseInputFormHidden($data,$value,$index){
        if(!$data){
            return false;
        }
        $return="";
        foreach($data as $key=>$item){
            if(in_array($key,$value)){
                $return.='<input type="hidden" name="data['.$index.']['.$key.']" value="'.$item.'" class="form-style-none" >';
            }
        }
        return $return;
    }
    public static function floatValParse($input){
        return floatval(preg_replace('/\,/',"",$input));
    }
    public static function parseInputForm($data,$value,$index,$numberlist=[],$listing=false){
        if(!$data){
            return false;
        }
        foreach($data as $key=>$item){
            if(in_array($key,$value)){
                $text=($listing&&in_array($key,$numberlist))?gallade::parseQuantity($item):$item;
                $data[$key]='<input type="hidden" name="data['.$index.']['.$key.']" value="'.$item.'" class="form-style-none" >'.$text;
            }
        }
        return $data;
    }
    public static function setColumns($data=[])
    {
        $return=[];
        foreach($data as $item){
            $return[$item]=['data'=>$item];
        }
        return $return;
    }
    public static function import($data=[])
    {
        if(!$data||$data['no']=="0"||$data["no"]=="TOTAL"||$data["no"]=="Jumlah"||$data['no']=="NO"||$data["nop"]=="0"||$data["nop"]=="TOTAL"){
            return false;
        }
        $msg="";
        $status=true;
        foreach($data as $key=>$val){
            $val=preg_replace('/\s+/', '', $val);
            if($key=="nop"&&$val){
                $nop=explode(".",$val);
                if(!isset($nop[4])){
                    $msg.="Format NOP tidak sesuai.";
                    $status=false;
                }else{
                    $nopz=explode("-",$nop[4]);
                    if(strlen($data["nop"])!='24'||count($nop)!=6||count($nopz)!=2){
                        $msg.="Format NOP tidak sesuai.";
                        $status=false;
                    }
                }
            }else if($key=="tahun_pajak"&&$val){
                $thn= (int)$val;
                if($thn!=$val&&strlen($thn)!='4'&&!in_array($thn,["2020","2021","2022","2023"])){
                    $msg.="Format Tahun tidak sesuai.".$thn;
                    $status=false;
                }
            }else{
                if(!$val||$val=='0'){
                    $msg.="Data ".$key." kosong.";
                    $status=false;
                }
            }
        }
        $data["status"]=$status;
        $data["msg"]=$msg;
        return $data;
    }


}
