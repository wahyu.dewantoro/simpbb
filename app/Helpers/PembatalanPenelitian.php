<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PembatalanPenelitian
{
    public static function MutasiPecah($id, $nomor_formulir, $induk = "")
    {

        // cek nota dan status pembayaran
        $nota = DB::table('sppt_penelitian')
            ->join('data_billing', 'sppt_penelitian.data_billing_id', '=', 'data_billing.data_billing_id')
            ->select(db::raw("data_billing.data_billing_id,kd_status,sppt_penelitian.kd_propinsi,
            sppt_penelitian.kd_dati2,
            sppt_penelitian.kd_kecamatan,
            sppt_penelitian.kd_kelurahan,
            sppt_penelitian.kd_blok,
            sppt_penelitian.no_urut,
            sppt_penelitian.kd_jns_op,
            listagg(sppt_penelitian.thn_pajak_sppt,',') within group (order by sppt_penelitian.thn_pajak_sppt) thn_pajak_sppt"))
            ->whereraw("nomor_formulir='$nomor_formulir")
            ->groupByraw(db::raw("data_billing.data_billing_id,kd_status,sppt_penelitian.kd_propinsi,
            sppt_penelitian.kd_dati2,
            sppt_penelitian.kd_kecamatan,
            sppt_penelitian.kd_kelurahan,
            sppt_penelitian.kd_blok,
            sppt_penelitian.no_urut,
            sppt_penelitian.kd_jns_op"))
            ->first();

        if ($nota->count() > 0) {
            // ada nota
            if ($nota->kd_status == '0') {
                DB::table('sppt_penelitian')->where('nomor_formulir', $nomor_formulir)->delete();
                DB::table('nota_perhitungan')->where('data_billing_id', $nota->data_billing_id)->delete();
                DB::table('data_billing')->where('data_billing_id', $nota->data_billing_id)->update(['deleted_at' => Carbon::now()]);
                DB::table("sppt")->whereraw(db::raw("kd_propinsi='" . $nota->kd_propinsi . "' and 
                kd_dati2='" . $nota->kd_dati2 . "' and 
                kd_kecamatan='" . $nota->kd_kecamatan . "' and 
                kd_kelurahan='" . $nota->kd_kelurahan . "' and 
                kd_blok='" . $nota->kd_blok . "' and 
                no_urut='" . $nota->no_urut . "' and 
                kd_jns_op='" . $nota->kd_jns_op . "' and status_pembayaran_sppt<>'1' and thn_pajak_sppt in (" . $nota->thn_pajak_sppt . ")"))->update(['status_pembayaran_sppt' => '3']);
                
            }
        }
    }
}
