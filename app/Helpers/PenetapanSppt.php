<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

// App\Helpers\PenetapanSppt::proses()

class PenetapanSppt
{

    public static function pbbMinimal($tahun)
    {
        $seconds = 6000;

        $data = Cache::remember(md5('cache_pbb_minimal_' . $tahun), $seconds, function () use ($tahun) {
            $min = DB::connection("oracle_satutujuh")->table("PBB_MINIMAL")->select("nilai_pbb_minimal")
                ->whereraw("thn_pbb_minimal='$tahun'")->first();
            return $min->nilai_pbb_minimal ?? 0;
        });

        return $data;
    }

    public static function penentuanNjop($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun)
    {
        DB::connection("oracle_satutujuh")->statement("CALL PENENTUAN_NJOP('$kd_propinsi','$kd_dati2','$kd_kecamatan','$kd_kelurahan','$kd_blok','$no_urut','$kd_jns_op','$tahun','1')");
    }

    public static function getTotalNjopLama($nop)
    {
        $kd_propinsi = $nop['kd_propinsi'];
        $kd_dati2 = $nop['kd_dati2'];
        $kd_kecamatan = $nop['kd_kecamatan'];
        $kd_kelurahan = $nop['kd_kelurahan'];
        $kd_blok = $nop['kd_blok'];
        $no_urut = $nop['no_urut'];
        $kd_jns_op = $nop['kd_jns_op'];
        $tahun = $nop['tahun'];

        $cpl = DB::connection("oracle_satutujuh")->table("sppt")
            ->leftjoin("sppt_potongan_detail", function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan_detail.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan_detail.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan_detail.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan_detail.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan_detail.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan_detail.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan_detail.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan_detail.thn_pajak_sppt')
                    ->where('sppt_potongan_detail.jns_potongan', '=', '1');
            })
            ->where('sppt.thn_pajak_sppt', $tahun)
            ->where('sppt.kd_propinsi', $kd_propinsi)
            ->where('sppt.kd_dati2', $kd_dati2)
            ->where('sppt.kd_kecamatan', $kd_kecamatan)
            ->where('sppt.kd_kelurahan', $kd_kelurahan)
            ->where('sppt.kd_blok', $kd_blok)
            ->where('sppt.no_urut', $no_urut)
            ->where('sppt.kd_jns_op', $kd_jns_op)
            ->selectraw("pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0) pbb,njop_bumi_sppt njop_bumi,njop_bng_sppt njop_bng,njoptkp_sppt")->first();
        if ($cpl) {
            $pbb_last = $cpl->pbb ?? 0;
            $njkp_bumi_last = $cpl->njop_bumi ?? 0;
            $njkp_bng_last = $cpl->njop_bng ?? 0;
            $njoptkp_last = $cpl->njoptkp_sppt ?? 0;

            return [
                'pbb_last' => $pbb_last,
                'njkp_bumi_last' => $njkp_bumi_last,
                'njkp_bng_last' => $njkp_bng_last,
                'njoptkp_last' => $njoptkp_last,
            ];
        } else {
            return [];
        }
    }


    public static function cekGabung($nop)
    {
        $kd_propinsi = $nop['kd_propinsi'];
        $kd_dati2 = $nop['kd_dati2'];
        $kd_kecamatan = $nop['kd_kecamatan'];
        $kd_kelurahan = $nop['kd_kelurahan'];
        $kd_blok = $nop['kd_blok'];
        $no_urut = $nop['no_urut'];
        $kd_jns_op = $nop['kd_jns_op'];
        $tahun = $nop['tahun'];

        $gabung = DB::connection("oracle_satutujuh")->select(DB::raw("SELECT nop_asal
        FROM (  SELECT MAX (id) id,
                       MAX (TO_CHAR (created_at, 'yyyy')) tahun,
                          kd_propinsi
                       || kd_dati2
                       || kd_kecamatan
                       || kd_kelurahan
                       || kd_blok
                       || no_urut
                       || kd_jns_op
                          nop_proses
                  FROM sim_pbb.history_mutasi_gabung
                 WHERE     kd_propinsi = '$kd_propinsi'
                       AND kd_dati2 = '$kd_dati2'
                       AND kd_kecamatan = '$kd_kecamatan'
                       AND kd_kelurahan = '$kd_kelurahan'
                       AND kd_blok = '$kd_blok'
                       AND no_urut = '$no_urut'
                       AND kd_jns_op = '$kd_jns_op'
              GROUP BY    kd_propinsi
                       || kd_dati2
                       || kd_kecamatan
                       || kd_kelurahan
                       || kd_blok
                       || no_urut
                       || kd_jns_op) a
             JOIN
             (SELECT    kd_propinsi
                     || kd_dati2
                     || kd_kecamatan
                     || kd_kelurahan
                     || kd_blok
                     || no_urut
                     || kd_jns_op
                        nop_asal,
                     history_mutasi_gabung_id
                FROM sim_pbb.history_mutasi_gabung_detail) b
                ON a.id = b.history_mutasi_gabung_id"));

        $tmp = 0;

        $pbb_last = 0;
        $njkp_bumi_last = 0;
        $njkp_bng_last = 0;
        $njoptkp_last = 0;
        $is_gabung = 0;
        $na = '';
        // Log::info($gabung);
        foreach ($gabung as $row) {
            $is_gabung = 1;

            $nop_asal = $row->nop_asal;
            // log::info($nop_asal);

            $kd_propinsi_asal = substr($nop_asal, 0, 2);
            $kd_dati2_asal = substr($nop_asal, 2, 2);
            $kd_kecamatan_asal = substr($nop_asal, 4, 3);
            $kd_kelurahan_asal = substr($nop_asal, 7, 3);
            $kd_blok_asal = substr($nop_asal, 10, 3);
            $no_urut_asal = substr($nop_asal, 13, 4);
            $kd_jns_op_asal = substr($nop_asal, 17, 1);

            $prl_a = [
                'kd_propinsi' => $kd_propinsi_asal,
                'kd_dati2' => $kd_dati2_asal,
                'kd_kecamatan' => $kd_kecamatan_asal,
                'kd_kelurahan' => $kd_kelurahan_asal,
                'kd_blok' => $kd_blok_asal,
                'no_urut' => $no_urut_asal,
                'kd_jns_op' => $kd_jns_op_asal,
                'tahun' => $tahun
            ];
            $cpla = Self::getTotalNjopLama($prl_a);
            $pbb = $cpla['pbb_last'] ?? 0;
            $njkp_bumi = $cpla['njkp_bumi_last'] ?? 0;
            $njkp_bng = $cpla['njkp_bng_last'] ?? 0;
            $njoptkp = $cpla['njoptkp_last'] ?? 0;

            $hsl = ($njkp_bumi + $njkp_bng) - $njoptkp;
            if ($tmp < $hsl) {
                $tmp = $hsl;

                $na = $nop_asal;
                $pbb_last = $pbb;
                $njkp_bumi_last = $njkp_bumi;
                $njkp_bng_last = $njkp_bng;
                $njoptkp_last = $njoptkp;
            }
        }

        return [
            'is_gabung' => $is_gabung,
            'pbb_last' => $pbb_last,
            'njkp_bumi_last' => $njkp_bumi_last,
            'njkp_bng_last' => $njkp_bng_last,
            'njoptkp_last' => $njoptkp_last,
        ];
    }

    /*   public static function hitungHkpd($data)
    {
        
        $njop_baru = $data['njop_baru'] ?? 0;
        $njop_lama = $data['njop_lama'] ?? 0;
        $tahun = $data['tahun'];
        $hk = $data['hk'];

        if ($njop_baru != 0 && $njop_lama != 0) {
            $selisih_njop = $njop_baru - $njop_lama;
            $hkpd = round(100 -  (($selisih_njop / $njop_baru) * 100));

            if ($hkpd > 100) {
                $hkpd = 100;
            }
        } else {
            $hkpd = 100;
        }

        if ($hkpd == 100 && $hk != $hkpd) {
            $nhkpd = $hk;
        } else {
            $nhkpd = $hkpd;
        }

        $hkpd = ($nhkpd / 100);
        return [
            'hkpd' => $hkpd,
            'nhkpd' => $nhkpd
        ];
    } */

    public static function hitungHkpd($data)
    {
        $njopBaru = $data['njop_baru'] ?? 0;
        $njopLama = $data['njop_lama'] ?? 0;
        $tahun = $data['tahun'] ?? null; // Jika `tahun` penting, tambahkan validasi lebih lanjut.
        $hk = $data['hk'] ?? 0;

        // Default nilai HKPD
        $hkpd = 100;

        // Hitung selisih NJOP jika keduanya tidak nol
        if ($njopBaru > 0 && $njopLama > 0) {
            $selisihNjop = $njopBaru - $njopLama;
            $hkpd = round(100 - (($selisihNjop / $njopBaru) * 100));
            $hkpd = min($hkpd, 100); // Batas maksimum HKPD adalah 100
        }

        // Tentukan nilai NHKPD berdasarkan kondisi
        $nhkpd = ($hkpd === 100 && $hk !== $hkpd) ? $hk : $hkpd;

        return [
            'hkpd' => $nhkpd / 100, // Konversi ke desimal
            'nhkpd' => $nhkpd       // Nilai NHKPD final
        ];
    }



    public static function proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun, $tgl_terbit, $tgl_jatuh_tempo, $non_aktif = false)
    {

        // mencari nilai pbb tahun sebelumnya
        $pbb_last = 0;
        $njkp_bumi_last = 0;
        $njkp_bng_last = 0;
        $njoptkp_last = 0;
        $selisih_njop = 0;

        $tarif_lama = 0;

        // $ambil_nir = 0;
        if ($tahun >= 2024) {
            $prl = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'tahun' => $tahun - 1
            ];
            $cpl = Self::getTotalNjopLama($prl);
            log::info(['njop lama' => $cpl]);

            if (count($cpl) > 0) {
                $pbb_last = $cpl['pbb_last'] ?? 0;
                $njkp_bumi_last = $cpl['njkp_bumi_last'] ?? 0;
                $njkp_bng_last = $cpl['njkp_bng_last'] ?? 0;
                $njoptkp_last = $cpl['njoptkp_last'] ?? 0;
            } else {
                // mencari nop asal
                $nop_string = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
                $na = DB::connection("oracle_satutujuh")->table("dat_objek_pajak")
                    ->join("tbl_spop", "dat_objek_pajak.no_formulir_spop", "=", "tbl_spop.no_formulir")
                    ->select("nop_asal")
                    ->whereraw("dat_objek_pajak.kd_propinsi='$kd_propinsi' and 
                    dat_objek_pajak.kd_dati2='$kd_dati2' and 
                    dat_objek_pajak.kd_kecamatan='$kd_kecamatan' and 
                    dat_objek_pajak.kd_kelurahan='$kd_kelurahan' and 
                    dat_objek_pajak.kd_blok='$kd_blok' and 
                    dat_objek_pajak.no_urut='$no_urut' and 
                    dat_objek_pajak.kd_jns_op='$kd_jns_op' 
                    and tbl_spop.nop_proses='$nop_string'")
                    ->first();
                $nop_asal = $na->nop_asal ?? '';
                if ($nop_asal != '') {
                    $kd_propinsi_asal = substr($nop_asal, 0, 2);
                    $kd_dati2_asal = substr($nop_asal, 2, 2);
                    $kd_kecamatan_asal = substr($nop_asal, 4, 3);
                    $kd_kelurahan_asal = substr($nop_asal, 7, 3);
                    $kd_blok_asal = substr($nop_asal, 10, 3);
                    $no_urut_asal = substr($nop_asal, 13, 4);
                    $kd_jns_op_asal = substr($nop_asal, 17, 1);

                    $prl_a = [
                        'kd_propinsi' => $kd_propinsi_asal,
                        'kd_dati2' => $kd_dati2_asal,
                        'kd_kecamatan' => $kd_kecamatan_asal,
                        'kd_kelurahan' => $kd_kelurahan_asal,
                        'kd_blok' => $kd_blok_asal,
                        'no_urut' => $no_urut_asal,
                        'kd_jns_op' => $kd_jns_op_asal,
                        'tahun' => $tahun - 1
                    ];
                    $cpla = Self::getTotalNjopLama($prl_a);
                    $pbb_last = $cpla['pbb_last'] ?? 0;
                    $njkp_bumi_last = $cpla['njkp_bumi_last'] ?? 0;
                    $njkp_bng_last = $cpla['njkp_bng_last'] ?? 0;
                    $njoptkp_last = $cpla['njoptkp_last'] ?? 0;
                }


                // cek gabung
                $gbg = [
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'tahun' => $tahun - 1
                ];
                $cg = self::cekGabung($gbg);
                if ($cg['is_gabung'] == '1') {
                    $pbb_last = $cg['pbb_last'] ?? 0;
                    $njkp_bumi_last = $cg['njkp_bumi_last'] ?? 0;
                    $njkp_bng_last = $cg['njkp_bng_last'] ?? 0;
                    $njoptkp_last = $cg['njoptkp_last'] ?? 0;
                }
            }
        }

        // PENENTUAN_NJOP
        self::penentuanNjop($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun);
        $objek = self::dataObjek($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun);
        // Log::info($objek);
        $njkp_bumi = $objek->njop_bumi ?? 0;
        $njkp_bng = $objek->njop_bng ?? 0;
        // Log::info(json_encode($lg));

        $njop = $njkp_bumi + $njkp_bng;
        $njoptkp = $objek->nilai_njoptkp ?? 0;
        $lokasi = $objek->lokasi_objek_id ?? null;
        $njop_hitung = ($njop - $njoptkp);
        $ch = 0;
        // menentukan hkpd untuk mulai
        if ($tahun == 2024) {
            // hanya berlaku di 2024
            // cek hkpd individu
            $hi = DB::connection("oracle_satutujuh")->table("hkpd_objek_individu")
                ->select("hkpd")
                ->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_hkpd' => $tahun
                ])->first();


            // $ch = 0;
            if ($hi) {
                if ($hi->hkpd > 0) {
                    $ch = $hi->hkpd;
                }
            }

            $njop_last = (($njkp_bumi_last + $njkp_bng_last) - $njoptkp_last);
            $njop_baru = (($njkp_bumi + $njkp_bng) - $njoptkp);

            if ($ch > 0) {
                $nhkpd = $hi->hkpd;
                $hkpd = $nhkpd / 100;
            } else {
                $phkpd = [
                    'njop_baru' => $njop_baru,
                    'njop_lama' => $njop_last,
                    'tahun' => $tahun,
                    'hk' => $objek->nilai_hkpd
                ];
                $reshkpd = self::hitungHkpd($phkpd);

                $hkpd = $reshkpd['hkpd'];
                $nhkpd = $reshkpd['nhkpd'];
            }
        } else {
            $nhkpd = $objek->nilai_hkpd ?? 0;
            $hkpd = ($nhkpd / 100);
        }



        $njkp = round($hkpd *  $njop_hitung);
        $tarif = self::getTarif($njkp,  $tahun, $lokasi);
        $pbb = round(($tarif / 100) *  $njkp);
        $pbbMinimal = self::pbbMinimal($tahun);
        $pbb_bayar = $pbbMinimal > $pbb ? $pbbMinimal : $pbb;

        //cek perbandingan pbb 
        if ($tahun >= 2024) {
            $pm = $pbb_last;
            if ($pbb_last <= $pbbMinimal) {
                $pm = $pbb_last + round(($pbb_last * 0.4));
            }

            // ini untuk mengejar ketetapan tidak turun
            if ($pbb_bayar < $pm && $nhkpd < 100 && $ch == 0 && $tahun == '2024') {
                for ($i = $nhkpd; $i <= 100; $i++) {
                    $nhkpd = $i;
                    $hkpd = $nhkpd / 100;
                    $njkp = round($hkpd *  $njop_hitung);
                    $tarif = self::getTarif($njkp,  $tahun, $lokasi);
                    $pbb = round(($tarif / 100) *  $njkp);
                    $pbbMinimal = self::pbbMinimal($tahun);
                    $pbb_bayar = $pbbMinimal > $pbb ? $pbbMinimal : $pbb;
                    if ($pbb_bayar >= $pm) {
                        break;
                    }
                }
            }
        }

        $njop_sppt = $njop - $njoptkp;
        $sppt = [];
        $jns_bumi = $objek->jns_bumi;
        if ((in_array($jns_bumi, ['1', '2', '3']) && $non_aktif == false)
            || (in_array($jns_bumi, ['4', '5']) && $non_aktif == true)
        ) {
            $cek_sppt = DB::connection("oracle_satutujuh")->table("sppt")->selectRaw("status_pembayaran_sppt,siklus_sppt")
                ->where('thn_pajak_sppt', $tahun)
                ->where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->first();

            $baru = 1;
            $siklus = 1;
            $lunas = 0;
            if ($cek_sppt) {
                $baru = 0;
                $lunas = $cek_sppt->status_pembayaran_sppt == '1' ? '1' : '0';
                $siklus += $cek_sppt->siklus_sppt;
            }

            if ($baru == 1 && $lunas == 0) {
                $proses = "1";
            } else if ($baru == 0 && $lunas == 0) {
                $proses = "2";
            } else {
                $proses = "0";
            }

            // dd($proses);

            $sppt = [
                'KD_PROPINSI' => $kd_propinsi,
                'KD_DATI2' => $kd_dati2,
                'KD_KECAMATAN' => $kd_kecamatan,
                'KD_KELURAHAN' => $kd_kelurahan,
                'KD_BLOK' =>  $kd_blok,
                'NO_URUT' =>  $no_urut,
                'KD_JNS_OP' =>  $kd_jns_op,
                'THN_PAJAK_SPPT' =>  $tahun,
                'SIKLUS_SPPT' =>  $siklus,
                'KD_KANWIL' =>  '01',
                'KD_KANTOR' =>  '01',
                'KD_TP' =>  '01',
                'NM_WP_SPPT' =>  $objek->nm_wp,
                'JLN_WP_SPPT' =>  $objek->jalan_wp,
                'BLOK_KAV_NO_WP_SPPT' =>  $objek->blok_kav_no_wp,
                'RW_WP_SPPT' =>  $objek->rw_wp,
                'RT_WP_SPPT' => $objek->rt_wp,
                'KELURAHAN_WP_SPPT' =>  $objek->kelurahan_wp,
                'KOTA_WP_SPPT' => $objek->kota_wp,
                'KD_POS_WP_SPPT' =>  $objek->kd_pos_wp,
                'NPWP_SPPT' => null,
                'NO_PERSIL_SPPT' => $objek->no_persil,
                'KD_KLS_TANAH' => $objek->kd_kls_tanah,
                'THN_AWAL_KLS_TANAH' => $objek->thn_awal_kls_tanah,
                'KD_KLS_BNG' =>  $objek->kd_kls_bng,
                'THN_AWAL_KLS_BNG' =>  $objek->thn_awal_kls_bng,
                'TGL_JATUH_TEMPO_SPPT' => new Carbon($tgl_jatuh_tempo),
                'LUAS_BUMI_SPPT' =>  $objek->total_luas_bumi,
                'LUAS_BNG_SPPT' => $objek->total_luas_bng,
                'NJOP_BUMI_SPPT' => $objek->njop_bumi,
                'NJOP_BNG_SPPT' => $objek->njop_bng,
                'NJOP_SPPT' =>  $njop_sppt,
                'NJOPTKP_SPPT' => $njoptkp,
                'PBB_TERHUTANG_SPPT' => $pbb,
                'FAKTOR_PENGURANG_SPPT' => 0,
                'PBB_YG_HARUS_DIBAYAR_SPPT' =>  $pbb_bayar,
                'STATUS_PEMBAYARAN_SPPT' => '0',
                'STATUS_TAGIHAN_SPPT' => '0',
                'STATUS_CETAK_SPPT' => '0',
                'TGL_TERBIT_SPPT' =>  new Carbon($tgl_terbit),
                'TGL_CETAK_SPPT' => new Carbon($tgl_terbit),
                'NIP_PENCETAK_SPPT' =>  '060000000000000000',
                'TTE_UPLOAD_AT' =>  null,
                'TTE_FILE_URL' =>  null,
                'HKPD' =>  $nhkpd,
                'TARIF' => $tarif,
                'CREATED_BY' => auth()->user()->id ?? null,
                'CREATED_AT' => DB::raw("sysdate"),
            ];

            // log::info($sppt);
        }

        /* // mencari insentif nya 
        if ($tahun >= 2024) {
            if ($pbb_last < $pbb_bayar && $pbb_last > 0) {
                $insentif = abs($pbb_last - $pbb_bayar);
                if ($tahun >= 2024) {
                    if ($insentif < 2000) {
                        $insentif = 0;
                    }
                }
            } else {
                $insentif = 0;
            }

            if ($insentif > $pbb_bayar) {
                $insentif = 0;
            }
        } else {
            $insentif = 0;
        }

        $ha = $pbb_bayar - $insentif;
        $min_b = 20000;
        if ($ha < $min_b && $tahun >= 2024) {
            $insentif = abs($min_b - $pbb_bayar);
        } */

        // Mencari insentif
        $insentif = 0;

        if ($tahun >= 2024) {

            Log::info('pbb last :' . $pbb_last);
            // Cek jika PBB terakhir valid dan lebih kecil dari PBB yang dibayar
            if ($pbb_last > 0 && $pbb_last < $pbb_bayar) {
                $insentif = abs($pbb_last - $pbb_bayar);

                // Set insentif ke 0 jika kurang dari 2000 atau lebih besar dari PBB yang dibayar
                if ($insentif < 2000 || $insentif > $pbb_bayar) {
                    $insentif = 0;
                }
            }

            // Hitung hasil akhir
            $ha = $pbb_bayar - $insentif;

            $min_b = 20000;

            if ($tahun == 2025) {
                $min_b = 23000;
            }

            // Penyesuaian insentif jika hasil akhir kurang dari nilai minimum
            if ($ha < $min_b && $ha) {
                $insentif = abs($min_b - $pbb_bayar);
            }
        }


        $cek = Db::connection("oracle_satutujuh")->table("ms_potongan_individu")->where([
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'thn_pajak_sppt' => $tahun
        ])
            ->selectraw("count(1) cek")
            ->first();

        if ($insentif > 0) {
            $inspot = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'thn_pajak_sppt' => $tahun,
                'nilai_potongan' => $insentif,
                'created_at' => Carbon::now(),
                'created_by' => auth()->user()->id ?? null,
                'tgl_mulai' => new Carbon('2003-01-01'),
                'tgl_selesai' => new Carbon('9999-01-01'),
            ];


            if ($cek->cek == 0) {
                DB::connection("oracle_satutujuh")->table("ms_potongan_individu")
                    ->insert($inspot);
            } else {
                DB::connection("oracle_satutujuh")->table("ms_potongan_individu")
                    ->where([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $tahun
                    ])
                    ->update($inspot);
            }
        } else {
            if ($cek->cek > 0) {
                Db::connection("oracle_satutujuh")->table("ms_potongan_individu")->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_pajak_sppt' => $tahun,
                    // 'jns_potongan' => '1'
                ])->delete();
            }
        }


        DB::connection("oracle_satutujuh")->commit();
        if (isset($proses)) {
            switch ($proses) {
                case '1':
                    # insert
                    DB::connection("oracle_satutujuh")->table("sppt")->insert($sppt);
                    DB::connection("oracle_satutujuh")->statement("begin HITUNG_POTONGAN('" . $kd_kecamatan . "','" . $kd_kelurahan . "','" . $kd_blok . "','" . $no_urut . "','" . $kd_jns_op . "','" . $tahun . "'); end;");
                    break;

                case '2':
                    # update
                    DB::connection("oracle_satutujuh")->table("sppt")
                        ->where('thn_pajak_sppt', $tahun)
                        ->where('kd_propinsi', $kd_propinsi)
                        ->where('kd_dati2', $kd_dati2)
                        ->where('kd_kecamatan', $kd_kecamatan)
                        ->where('kd_kelurahan', $kd_kelurahan)
                        ->where('kd_blok', $kd_blok)
                        ->where('no_urut', $no_urut)
                        ->where('kd_jns_op', $kd_jns_op)
                        ->update($sppt);
                    DB::connection("oracle_satutujuh")->statement("begin HITUNG_POTONGAN('" . $kd_kecamatan . "','" . $kd_kelurahan . "','" . $kd_blok . "','" . $no_urut . "','" . $kd_jns_op . "','" . $tahun . "'); end;");
                    break;
                default:
                    # code...
                    break;
            }
        }

        // jika tidak sama denagn tahun berjalan
        if (date('Y') != $tahun) {
            self::penentuanNjop($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, date('Y'));
        }
        DB::connection("oracle_satutujuh")->commit();
        return $sppt;
    }

    public static function getTarif($njop,  $tahun, $lokasi_id = '')
    {
        if ($lokasi_id == 150 && $tahun >= 2024) {
            // lahan pertanian & peternakan
            $tarif = 0.04;
        } else {

            $seconds = 6000;
            $data = Cache::remember(md5('cache_tarif'), $seconds, function () {
                $data = DB::connection("oracle_satutujuh")->table('tarif')->selectRaw('thn_awal,thn_akhir,njop_min,njop_max,nilai_tarif')->get()->toArray();
                $tmp = [];
                foreach ($data as $row) {
                    $tmp[] = [
                        'thn_awal' => (int)$row->thn_awal,
                        'thn_akhir' => (int)$row->thn_akhir,
                        'njop_min' => (int)$row->njop_min,
                        'njop_max' => (int)$row->njop_max,
                        'nilai_tarif' => (float)$row->nilai_tarif,
                    ];
                }
                return $tmp;
            });


            // get max tarif di tahun 
            // $tahun = '2024';
            $tm = 0;
            foreach ($data as $row) {
                if ($row['thn_awal'] <= $tahun && $row['thn_akhir'] >= $tahun) {
                    if ($tm < $row['nilai_tarif']) {
                        $tm = $row['nilai_tarif'];
                    }
                }
            }
            $tarif = $tm;
            // $nilai = "9900000";

            foreach ($data as $row) {
                if ($row['thn_awal'] <= $tahun && $row['thn_akhir'] >= $tahun &&    $row['njop_min'] <= $njop && $row['njop_max'] >= $njop) {
                    $tarif = $row['nilai_tarif'];
                    break;
                }
            }
            // $this->info($tarif);


            /*        $tmp = $njop;

            $tarif = DB::connection("oracle_satutujuh")->table("TARIF")
                ->whereraw($tahun . " between thn_awal and thn_akhir and " . $tmp . " between njop_min and njop_max  ")->first();

            if ($tarif) {
                $tarif = $tarif->nilai_tarif;
            } else {
                $tarif = DB::connection("oracle_satutujuh")->table("TARIF")
                    ->selectraw("max(nilai_tarif) nilai_tarif")
                    ->whereraw($tahun . " between thn_awal and thn_akhir ")->first();
                $tarif = $tarif->nilai_tarif;
            } */
        }

        $prefix = 0;
        $cr = explode('.', $tarif);
        if (isset($cr[1])) {
            $prefix = strlen($cr[1]);
        }

        // Log::info('ori trf ' . $tarif);
        return number_format($tarif, $prefix);
    }

    public static function dataObjek($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun)
    {
        $sql = DB::connection("oracle_satutujuh")->table("DAT_OBJEK_PAJAK")
            ->join('dat_op_bumi', function ($join) {
                $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->join('dat_znt', function ($join) {
                $join->on('dat_znt.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('dat_znt.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('dat_znt.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('dat_znt.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('dat_znt.kd_znt', '=', 'dat_op_bumi.kd_znt');
            })
            ->leftjoin('dat_nir', function ($join) use ($tahun) {
                $join->on('dat_znt.kd_propinsi', '=', 'dat_nir.kd_propinsi')
                    ->on('dat_znt.kd_dati2', '=', 'dat_nir.kd_dati2')
                    ->on('dat_znt.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                    ->on('dat_znt.kd_kelurahan', '=', 'dat_nir.kd_kelurahan')
                    ->on('dat_znt.kd_znt', '=', 'dat_nir.kd_znt')
                    ->on('dat_nir.thn_nir_znt', '=', db::raw($tahun));
            })
            ->leftjoin('dat_subjek_pajak', 'dat_objek_pajak.subjek_pajak_id', '=', 'dat_subjek_pajak.subjek_pajak_id')
            ->leftjoin('DAT_SUBJEK_PAJAK_NJOPTKP', function ($join) use ($tahun) {
                $join->on('dat_subjek_pajak_njoptkp.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('dat_subjek_pajak_njoptkp.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('dat_subjek_pajak_njoptkp.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('dat_subjek_pajak_njoptkp.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('dat_subjek_pajak_njoptkp.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('dat_subjek_pajak_njoptkp.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('dat_subjek_pajak_njoptkp.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op')
                    ->on('dat_subjek_pajak_njoptkp.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
                    ->where('dat_subjek_pajak_njoptkp.thn_njoptkp', $tahun);
            })
            ->leftjoin('njoptkp', function ($join) {
                $join->on('dat_subjek_pajak_njoptkp.kd_propinsi', '=', 'njoptkp.kd_propinsi')
                    ->on('dat_subjek_pajak_njoptkp.kd_dati2', '=', 'njoptkp.kd_dati2')
                    ->on('njoptkp.thn_awal', '<=', 'dat_subjek_pajak_njoptkp.thn_njoptkp')
                    ->on('njoptkp.thn_akhir', '>=', 'dat_subjek_pajak_njoptkp.thn_njoptkp');
            })
            ->leftjoin('kelas_tanah', function ($join) use ($tahun) {
                $join->whereraw(" $tahun between  kelas_tanah.thn_awal_kls_tanah and kelas_tanah.thn_akhir_kls_tanah
                and kelas_tanah.nilai_min_tanah <= (case when dat_objek_pajak.total_luas_bumi>0 then dat_objek_pajak.njop_bumi/dat_objek_pajak.total_luas_bumi/1000 else 0 end) 
                and kelas_tanah.nilai_max_tanah >= (case when dat_objek_pajak.total_luas_bumi>0 then dat_objek_pajak.njop_bumi/dat_objek_pajak.total_luas_bumi/1000 else 0 end)
                and dat_objek_pajak.total_luas_bumi<>0 and dat_objek_pajak.njop_bumi<>0 and kelas_tanah.kd_kls_tanah!='XXX'");
            })
            ->leftjoin('kelas_bangunan', function ($join) use ($tahun) {
                $join->whereraw(" $tahun between  kelas_bangunan.thn_awal_kls_bng and kelas_bangunan.thn_akhir_kls_bng
                and kelas_bangunan.nilai_min_bng <= (case when dat_objek_pajak.total_luas_bng>0 then dat_objek_pajak.njop_bng/dat_objek_pajak.total_luas_bng/1000 else 0 end) 
                and kelas_bangunan.nilai_max_bng >= (case when dat_objek_pajak.total_luas_bng>0 then dat_objek_pajak.njop_bng/dat_objek_pajak.total_luas_bng/1000 else 0 end)
                and dat_objek_pajak.total_luas_bng<>0 and dat_objek_pajak.njop_bng<>0 and kelas_bangunan.kd_kls_bng !='XXX'");
            })
            ->selectRaw("DAT_OBJEK_PAJAK.no_persil,DAT_OBJEK_PAJAK.total_luas_bumi,DAT_OBJEK_PAJAK.total_luas_bng,DAT_OBJEK_PAJAK.njop_bumi,DAT_OBJEK_PAJAK.njop_bng,nm_wp,jalan_wp,blok_kav_no_wp,rw_wp,rt_wp,kelurahan_wp,kota_wp,kd_pos_wp,nvl(nilai_njoptkp,0)*1000 nilai_njoptkp,jns_bumi,
                            nvl(kd_kls_tanah,'XXX') kd_kls_tanah,nvl(thn_awal_kls_tanah,'1986') thn_awal_kls_tanah,nvl(nilai_per_m2_tanah,0) nilai_per_m2_tanah,
                            nvl(kd_kls_bng,'XXX') kd_kls_bng,nvl(thn_awal_kls_bng,'1986') thn_awal_kls_bng,nvl(nilai_per_m2_bng,0) nilai_per_m2_bng,dat_op_bumi.kd_znt,lokasi_objek_id,nvl(nilai_hkpd,100) nilai_hkpd")
            ->where('DAT_OBJEK_PAJAK.kd_propinsi', $kd_propinsi)
            ->where('DAT_OBJEK_PAJAK.kd_dati2', $kd_dati2)
            ->where('DAT_OBJEK_PAJAK.kd_kecamatan', $kd_kecamatan)
            ->where('DAT_OBJEK_PAJAK.kd_kelurahan', $kd_kelurahan)
            ->where('DAT_OBJEK_PAJAK.kd_blok', $kd_blok)
            ->where('DAT_OBJEK_PAJAK.no_urut', $no_urut)
            ->where('DAT_OBJEK_PAJAK.kd_jns_op', $kd_jns_op);
        // ->first();
        // log::info($sql->toSql());

        return $sql->first();
    }
}
