<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendWa
{

    public static function send($data)
    {
        $response = Http::withHeaders([

            'Authorization' => config('wablas.token')
        ])->post(config('wablas.url'), $data);

        $response = $response->body();
        Log::info($response);
        return $response;
    }

    public static function simple($data)
    {
        return Self::send($data);
    }
}
