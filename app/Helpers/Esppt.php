<?php

namespace App\Helpers;

// use App\ObjekPajak;
use App\pegawaiStruktural;
use App\Sppt;
use App\TempatBayar;
// use BaconQrCode\Encoder\QrCode;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use SnappyPdf as PDF;
use DOMPDF as dompdf;

use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;

class Esppt
{



    public static function Sppt()
    {
        return DB::connection("oracle_satutujuh")->table(db::raw("sppt"))
            ->join('ref_kelurahan', function ($join) {
                $join->on('sppt.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
            })
            ->join('ref_kecamatan', 'sppt.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
            ->join('dat_objek_pajak', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('sppt.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->leftjoin(db::raw("(select kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,nilai_potongan 
                                from sppt_potongan_detail where jns_potongan='1') sppt_potongan"), function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->leftjoin(db::raw("(select kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,nilai_potongan  nilai_kompensasi
                                from sppt_potongan_detail where jns_potongan='3') sppt_kompensasi"), function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_kompensasi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_kompensasi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_kompensasi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_kompensasi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_kompensasi.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_kompensasi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_kompensasi.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_kompensasi.thn_pajak_sppt');
            })
            ->select(db::raw("sppt.kd_propinsi,
            sppt.kd_dati2,
            sppt.kd_kecamatan,
            sppt.kd_kelurahan,
            sppt.kd_blok,
            sppt.no_urut,
            sppt.kd_jns_op,
            sppt.thn_pajak_sppt,
            nm_wp_sppt,
            jalan_op,
            blok_kav_no_op,
            rt_op,
            rw_op,
            no_persil_sppt,
            nm_kelurahan,
            nm_kecamatan,
            kd_status_wp,
            jln_wp_sppt,
            blok_kav_no_wp_sppt,
            rt_wp_sppt,
            rw_wp_sppt,
            kelurahan_wp_sppt,
            kota_wp_sppt,
            luas_bumi_sppt,
            kd_kls_tanah,
            njop_bumi_sppt,
            luas_bng_sppt,
            kd_kls_bng,
            njop_bng_sppt,
            njoptkp_sppt,
            nvl(sppt.hkpd,100) hkpd,
            nvl(sppt.tarif,
            get_tarifpbb ( (njop_bng_sppt + njop_bumi_sppt),
            njoptkp_sppt,
            sppt.thn_pajak_sppt))
            tarif,
            pbb_terhutang_sppt,
            tgl_jatuh_tempo_sppt,
            tgl_terbit_sppt,
            pbb_yg_harus_dibayar_sppt,
            nilai_potongan,
            nilai_kompensasi,
            nvl((select 1 from pengurang_piutang pp 
            where pp.kd_propinsi=sppt.kd_propinsi and
pp.kd_dati2=sppt.kd_dati2 and
pp.kd_kecamatan=sppt.kd_kecamatan and
pp.kd_kelurahan=sppt.kd_kelurahan and
pp.kd_blok=sppt.kd_blok and
pp.no_urut=sppt.no_urut and
pp.kd_jns_op=sppt.kd_jns_op and
pp.thn_pajak_sppt=sppt.thn_pajak_sppt 
and rownum=1
            ),status_pembayaran_sppt)   status_pembayaran_sppt,
            (SELECT CASE WHEN COUNT (1) > 0 THEN 0 ELSE 1 END baru
               FROM sppt a
              WHERE     a.kd_kecamatan = sppt.kd_kecamatan
                    AND a.kd_kelurahan = sppt.kd_kelurahan
                    AND a.kd_blok = sppt.kd_blok
                    AND a.no_urut = sppt.no_urut
                    AND a.kd_jns_op = sppt.kd_jns_op
                    AND a.thn_pajak_sppt < sppt.thn_pajak_sppt - 1)
               baru,
            (  SELECT LISTAGG (
                            a.thn_pajak_sppt
                         || ':'
                         || CASE
                               WHEN   pbb_yg_harus_dibayar_sppt
                                    - NVL (nilai_potongan, 0) <= 0
                               THEN
                                  'NIHIL'
                               ELSE
                                   CASE
                                           WHEN   (pbb_yg_harus_dibayar_sppt
                                                - NVL (nilai_potongan, 0)
                                                - SUM (
                                                     (  NVL (
                                                           jml_sppt_yg_dibayar,
                                                           0)
                                                      - NVL (denda_sppt, 0))) <=
                                                   0) or c.jns_koreksi='3'
                                           THEN
                                              'LUNAS'
                                           ELSE
                                              CAST (
                                                   pbb_yg_harus_dibayar_sppt
                                                 - NVL (nilai_potongan, 0)
                                                 - SUM (
                                                      (  NVL (
                                                            jml_sppt_yg_dibayar,
                                                            0)
                                                       - NVL (denda_sppt, 0))) AS VARCHAR2 (2000))
                                        END
                            END,
                         ', ')
                      WITHIN GROUP (ORDER BY a.thn_pajak_sppt DESC)
                         tunggakan
                 FROM sppt a
                      LEFT JOIN sppt_potongan b
                         ON     a.kd_propinsi = b.kd_propinsi
                            AND a.kd_dati2 = b.kd_dati2
                            AND a.kd_kecamatan = b.kd_kecamatan
                            AND a.kd_kelurahan = b.kd_kelurahan
                            AND a.kd_blok = b.kd_blok
                            AND a.no_urut = b.no_urut
                            AND a.kd_jns_op = b.kd_jns_op
                            AND a.thn_pajak_sppt = b.thn_pajak_sppt
                      LEFT JOIN pengurang_piutang  c
                         ON     a.kd_propinsi = c.kd_propinsi
                            AND a.kd_dati2 = c.kd_dati2
                            AND a.kd_kecamatan = c.kd_kecamatan
                            AND a.kd_kelurahan = c.kd_kelurahan
                            AND a.kd_blok = c.kd_blok
                            AND a.no_urut = c.no_urut
                            AND a.kd_jns_op = c.kd_jns_op
                            AND a.thn_pajak_sppt = c.thn_pajak_sppt
                         
                      LEFT JOIN pembayaran_sppt d
                         ON     a.kd_propinsi = d.kd_propinsi
                            AND a.kd_dati2 = d.kd_dati2
                            AND a.kd_kecamatan = d.kd_kecamatan
                            AND a.kd_kelurahan = d.kd_kelurahan
                            AND a.kd_blok = d.kd_blok
                            AND a.no_urut = d.no_urut
                            AND a.kd_jns_op = d.kd_jns_op
                            AND a.thn_pajak_sppt = d.thn_pajak_sppt
                WHERE     a.kd_propinsi = sppt.kd_propinsi
                      AND a.kd_dati2 = sppt.kd_dati2
                      AND a.kd_kecamatan = sppt.kd_kecamatan
                      AND a.kd_kelurahan = sppt.kd_kelurahan
                      AND a.kd_blok = sppt.kd_blok
                      AND a.no_urut = sppt.no_urut
                      AND a.kd_jns_op = sppt.kd_jns_op
                      AND a.thn_pajak_sppt < sppt.thn_pajak_sppt
                      AND a.thn_pajak_sppt >= sppt.thn_pajak_sppt - 5
             GROUP BY a.thn_pajak_sppt,
                      pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0),
                      c.jns_koreksi)
               tunggakan"));
    }


    public static function render($param)
    {
        $sppt = $param['sppt'];
        $bookmark = $param['bookmark'];
        $qrcode = $param['qrcode'];
        $urlpembayaran = config('app.site_pembayaran');
        $urlpembayaran .=  '/' . $sppt->kd_propinsi .  $sppt->kd_dati2 .  $sppt->kd_kecamatan .  $sppt->kd_kelurahan .  $sppt->kd_blok .  $sppt->no_urut .  $sppt->kd_jns_op . $sppt->thn_pajak_sppt;
        $qrbayar = base64_encode(QrCode::style('round')->size(30)->errorCorrection('L')->generate($urlpembayaran));

        // site_pembayaran
        $kaban = $param['kaban'];
        $tp = $param['tp'];
        $tunggakan = $param['tunggakan'];
        $jsppt = $param['jsppt'];
        $tipe = $param['tipe'];
        $pbb_minimal = PenetapanSppt::pbbMinimal($sppt->thn_pajak_sppt);
        $pdf = dompdf::loadview('sppt/esppt_v3_tes', compact('sppt',  'qrbayar', 'qrcode',  'kaban', 'tp', 'tunggakan', 'jsppt', 'bookmark', 'pbb_minimal'));

        $filename = $param['nama_file'] ?? $sppt->kd_propinsi . $sppt->kd_dati2 . $sppt->kd_kecamatan . $sppt->kd_kelurahan . $sppt->kd_blok . $sppt->no_urut . $sppt->kd_jns_op . $sppt->thn_pajak_sppt . '.pdf';
        if ($tipe == 'download') {
            return $pdf->download($filename);
        }
        if ($tipe == 'save') {
            if (Storage::disk('sppt')->exists($filename)) {
                Storage::disk('sppt')->delete($filename);
            }
            $disk = Storage::disk('sppt');
            if ($disk->put($filename, $pdf->output())) {
                return $disk->path($filename);
            }
        }

        return $pdf->stream($filename);
    }

    public static function generate($nop, $tipe = "stream", $jsppt = "1", $bookmark = "S A L I N A N")
    {
        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kecamatan = substr($nop, 4, 3);
        $kd_kelurahan = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);
        $thn_pajak_sppt = substr($nop, 18, 4);
        $sppt = self::Sppt()
            ->where(
                [
                    'sppt.kd_propinsi' => $kd_propinsi,
                    'sppt.kd_dati2' => $kd_dati2,
                    'sppt.kd_kecamatan' => $kd_kecamatan,
                    'sppt.kd_kelurahan' => $kd_kelurahan,
                    'sppt.kd_blok' => $kd_blok,
                    'sppt.no_urut' => $no_urut,
                    'sppt.kd_jns_op' => $kd_jns_op,
                    'sppt.thn_pajak_sppt' => $thn_pajak_sppt,
                ]
            )->first();

        // $status_pembayaran=0;
        if (!$sppt) {
            return null;
        }
        if ($tipe == 'stream' && $sppt->status_pembayaran_sppt == '0') {
            $bookmark = "Belum Bayar";
        }

        $tunggakan = [];

        if (($sppt->tunggakan ?? '') <> '') {
            $expt = explode(',', $sppt->tunggakan);
            foreach ($expt as  $tgk) {
                $expb = explode(':', $tgk);

                $dd = preg_replace("/[^a-zA-Z]+/", "", $expb[1]);

                if ($dd == '' && $expb[1] != '-') {
                    $tunggakan[trim($expb[0])] = 'Rp.' . angka($expb[1]) . ',-';
                } else {
                    $tunggakan[trim($expb[0])] =  $expb[1];
                }
            }
        }
        $cetak['tunggakan'] = $tunggakan;
        $cetak['sppt'] = $sppt;
        $cetak['tp'] = TempatBayar::select(DB::raw("LISTAGG(nama_tempat, ', ') WITHIN GROUP (ORDER BY id) nama_tempat"))->first();
        $url =   config('app.url') . '/esppt?nop=' . $nop;
        $cetak['kaban'] = pegawaiStruktural::whereraw("kode='01'")->first();
        $cetak['qrcode'] = base64_encode(QrCode::style('square')->size(50)->errorCorrection('L')->generate($url));
        $cetak['jsppt'] = $jsppt;
        $cetak['tipe'] = $tipe;
        // $bookmark = "";
        $cetak['bookmark'] = $bookmark;

        return self::render($cetak);
    }

    public static function generateMulti($param)
    {
        $buku = $param['buku'] ?? '';
        $kd_kecamatan = $param['kd_kecamatan'] ?? '';
        $kd_kelurahan = $param['kd_kelurahan'] ?? '';
        $kd_blok = $param['kd_blok'] ?? '';
        $no_urut = $param['no_urut'] ?? '';
        $thn_pajak_sppt = $param['thn_pajak_sppt'] ?? '';

        $sppt = self::Sppt();
        if ($thn_pajak_sppt <> '') {
            $sppt = $sppt->whereraw("sppt.thn_pajak_sppt = '$thn_pajak_sppt' ");
        }
        if ($kd_kecamatan <> '') {
            $sppt = $sppt->whereraw("sppt.kd_kecamatan = '$kd_kecamatan' ");
        }
        if ($kd_kelurahan <> '') {
            $sppt = $sppt->whereraw("sppt.kd_kelurahan = '$kd_kelurahan' ");
        }
        if ($kd_blok <> '') {
            $sppt = $sppt->whereraw("sppt.kd_blok = '$kd_blok' ");
        }
        if ($no_urut <> '') {
            $exp = explode('-', $no_urut);
            if (count($exp) > 1) {
                $sppt = $sppt->whereraw("sppt.no_urut >='" . padding($exp[0], 0, 4) . "'");
                $sppt = $sppt->whereraw("sppt.no_urut <='" . padding($exp[1], 0, 4) . "'");
            } else {
                $sppt = $sppt->whereraw("sppt.no_urut = '" . padding($no_urut, 0, 4) . "' ");
            }
        }

        if ($buku <> '') {
            if ($buku == '1') {
                $sppt = $sppt->whereraw("sppt.pbb_yg_harus_dibayar_sppt<=500000");
            } else {
                $sppt = $sppt->whereraw("sppt.pbb_yg_harus_dibayar_sppt>500000");
            }
        }

        $sppt = $sppt->orderbyraw("sppt.kd_propinsi,
        sppt.kd_dati2,
        sppt.kd_kecamatan,
        sppt.kd_kelurahan,
        sppt.kd_blok,
        sppt.no_urut,
        sppt.kd_jns_op")->get();
        $kaban = pegawaiStruktural::whereraw("kode='01'")->first();
        $tp = TempatBayar::select(DB::raw("LISTAGG(nama_tempat, ', ') WITHIN GROUP (ORDER BY id) nama_tempat"))->first();

        $filepdf = [];
        $oMerger = PDFMerger::init();
        foreach ($sppt as $row) {
            $cetak['sppt'] = $row;
            $tunggakan = [];
            if ($row->tunggakan <> '') {
                $expt = explode(',', $row->tunggakan);
                foreach ($expt as  $tgk) {
                    // $expb = explode(':', $tgk);

                    $expb = explode(':', $tgk);
                    $dd = preg_replace("/[^a-zA-Z]+/", "", $expb[1]);

                    if ($dd == '') {
                        $tunggakan[trim($expb[0])] = 'Rp.' . angka($expb[1]) . ',-';
                    } else {
                        $tunggakan[trim($expb[0])] =  $expb[1];
                    }

                    // $tunggakan[$expb[0]] = 'Rp.' . angka($expb[1]) . ',-';
                }
            }

            $cetak['tunggakan'] = $tunggakan;
            $cetak['tp'] = $tp;
            $nop = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op . $row->thn_pajak_sppt;
            $url =   config('app.url') . '/esppt?nop=' . $nop;
            $nama_file = time() . '_' . $nop . '.pdf';
            $cetak['kaban'] = $kaban;
            $cetak['qrcode'] = base64_encode(QrCode::style('square')->size(70)->errorCorrection('H')->generate($url));
            $cetak['jsppt'] = '0';
            $cetak['tipe'] = 'save';
            $cetak['nama_file'] = $nama_file;
            $cetak['bookmark'] = "";
            $res = self::render($cetak);
            $filepdf[] = $nama_file;
            $oMerger->addPDF($res);
        }

        $oMerger->merge();

        // $oMerger->setFileName($ff);
        $jum = 0;
        foreach ($filepdf as $file) {
            if (Storage::disk('sppt')->exists($file)) {
                Storage::disk('sppt')->delete($file);
            }
            $jum += 1;
        }


        $filename = "sppt_generate_by_" . auth()->user()->id . ".pdf";
        if (Storage::disk('sppt')->exists($filename)) {
            Storage::disk('sppt')->delete($filename);
        }

        Storage::disk('sppt')->put($filename, $oMerger->output());
        return $filename;
    }
    /* 

    public static function generateasd($nop, $tipe = "stream", $jsppt = "1")
    {
        $param['kd_propinsi'] = substr($nop, 0, 2);
        $param['kd_dati2'] = substr($nop, 2, 2);
        $param['kd_kecamatan'] = substr($nop, 4, 3);
        $param['kd_kelurahan'] = substr($nop, 7, 3);
        $param['kd_blok'] = substr($nop, 10, 3);
        $param['no_urut'] = substr($nop, 13, 4);
        $param['kd_jns_op'] = substr($nop, 17, 1);
        $param['tahun'] = substr($nop, 18, 4);


        $sppt = Sppt::select(DB::raw("sppt.*,
        (select case when count(1)>0 then 0 else 1 end baru
from sppt a where  a.kd_kecamatan=sppt.kd_kecamatan and
a.kd_kelurahan=sppt.kd_kelurahan and
a.kd_blok=sppt.kd_blok and
a.no_urut=sppt.no_urut and
a.kd_jns_op=sppt.kd_jns_op and
a.thn_pajak_sppt<sppt.thn_pajak_sppt ) baru
        ,(select nilai_potongan
        from sppt_potongan
        where kd_kecamatan=sppt.kd_kecamatan
        and kd_kelurahan=sppt.kd_kelurahan and kd_blok=sppt.kd_blok and no_urut=sppt.no_urut
        and kd_jns_op=sppt.kd_jns_op and thn_pajak_sppt=sppt.thn_pajak_sppt) nilai_potongan , tarifpbb(sppt.njop_sppt)     tarif,
        ( select trim(subjek_pajak_id)
        from dat_objek_pajak
        where kd_kecamatan=sppt.kd_kecamatan
        and kd_kelurahan=sppt.kd_kelurahan and kd_blok=sppt.kd_blok and no_urut=sppt.no_urut
        and kd_jns_op=sppt.kd_jns_op) nik
        "))->nop($param)->first();
        // dd($sppt);

        if ($sppt) {
            $tp = TempatBayar::select(DB::raw("LISTAGG(nama_tempat, ', ') WITHIN GROUP (ORDER BY id) nama_tempat"))->first();
            $objek = DB::connection('oracle_dua')->table(DB::raw('dat_objek_pajak'))
                ->select(DB::raw('dat_objek_pajak.*,get_kecamatan(kd_kecamatan) nm_kecamatan,get_kelurahan(kd_kelurahan,kd_kecamatan) nm_kelurahan'))
                ->whereraw("kd_propinsi='" . $param['kd_propinsi'] . "' and kd_dati2='" . $param['kd_dati2'] . "' and kd_kecamatan='" . $param['kd_kecamatan'] . "' and kd_kelurahan='" . $param['kd_kelurahan'] . "' and kd_blok='" . $param['kd_blok'] . "' and no_urut='" . $param['no_urut'] . "' and kd_jns_op='" . $param['kd_jns_op'] . "'")
                ->first();
            if (empty($objek)) {
                $objek = DB::connection('oracle_satutujuh')->table(DB::raw('dat_objek_pajak'))
                    ->select(DB::raw('dat_objek_pajak.*,get_kecamatan(kd_kecamatan) nm_kecamatan,get_kelurahan(kd_kelurahan,kd_kecamatan) nm_kelurahan'))
                    ->whereraw("kd_propinsi='" . $param['kd_propinsi'] . "' and kd_dati2='" . $param['kd_dati2'] . "' and kd_kecamatan='" . $param['kd_kecamatan'] . "' and kd_kelurahan='" . $param['kd_kelurahan'] . "' and kd_blok='" . $param['kd_blok'] . "' and no_urut='" . $param['no_urut'] . "' and kd_jns_op='" . $param['kd_jns_op'] . "'")
                    ->first();
            }
            $riwayat = InformasiObjek::riwayatPembayaran($param)
                ->where('status_pembayaran_sppt', '0')
                ->whereraw("sppt.thn_pajak_sppt <> '" . $param['tahun'] . "'")
                ->orderby('sppt.thn_pajak_sppt')
                ->get();
            // return $riwayat;
            $tunggakan = [];

            foreach ($riwayat as $key => $tgk) {
                $tunggakan[$tgk->thn_pajak_sppt] = 'Rp.' . angka(str_replace('.', '', $tgk->pokok_bayar)) . ',-';
            }
            $url =   config('app.url') . 'esppt?nop=' . $nop;
            $kaban = pegawaiStruktural::whereraw("kode='01'")->first();

            $filename = $sppt->thn_pajak_sppt . ' - ' . $sppt->kd_kecamatan . ' ' . $sppt->kd_kelurahan . ' ' . $sppt->kd_blok . ' ' . $sppt->no_urut . ' ' . $sppt->kd_jns_op . ' - ' . $sppt->thn_pajak_sppt . '.pdf';

            $qrcode = base64_encode(QrCode::style('square')->size(70)->errorCorrection('H')->generate($url));
            return view('sppt/esppt_v3', compact('sppt', 'objek', 'qrcode',  'kaban', 'tp', 'tunggakan', 'jsppt'));
            $pdf = dompdf::loadview('sppt/esppt_v3', compact('sppt', 'objek', 'qrcode',  'kaban', 'tp', 'tunggakan', 'jsppt'));
            if ($tipe == 'download') {
                return $pdf->download($filename);
            }
            if ($tipe == 'save') {
                if (Storage::disk('sppt')->exists($filename)) {
                    Storage::disk('sppt')->delete($filename);
                }
                $disk = Storage::disk('sppt');
                if ($disk->put($filename, $pdf->output())) {
                    return $disk->path($filename);
                }
            }

            return $pdf->stream();
        } else {
            return abort(404, 'Data tidak ditemukan');
        }
    } */
}
