<?php

namespace App\Helpers;

use App\Models\PendataanBatch;
use App\Models\PendataanObjek;
use App\ObjekPajak;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Pendataan
{

    public static function core()
    {
        return ObjekPajak::join(db::raw("(select no_formulir, nm_wp, jalan_wp, blok_kav_no_wp, rt_wp, rw_wp, kelurahan_wp, kota_wp, kd_pos_wp, telp_wp, kd_znt, jns_bumi, nop_proses, nama created_name, created_by, a.created_at
                        from sim_pbb.tbl_spop a
                        join sim_pbb.users b on a.created_by=b.id
                        where layanan_objek_id is null 
                        ) tbl_spop"), 'dat_objek_pajak.no_formulir_spop', '=', 'tbl_spop.no_formulir')
            ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
            ->join('ref_kelurahan', function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
            });
    }


    public static function ReportObjek($param = [])
    {
        $core = self::core()->selectraw("tbl_spop.no_formulir, nop_proses, dat_objek_pajak.kd_propinsi, dat_objek_pajak.kd_dati2, dat_objek_pajak.kd_kecamatan, dat_objek_pajak.kd_kelurahan, dat_objek_pajak.kd_blok, dat_objek_pajak.no_urut, dat_objek_pajak.kd_jns_op, nm_wp, jalan_op, blok_kav_no_op, rt_op, rw_op, nm_kelurahan, nm_kecamatan, jns_bumi, kd_znt, total_luas_bumi, total_luas_bng, njop_bumi, njop_bng, no_persil, created_name, created_by, created_at");
        return $core;
    }

    public static function Creators()
    {
        return  self::core()->selectraw("distinct created_by,created_name");
    }

    public static function ReportSubjek($param = [])
    {
        $core = self::core()->selectraw("tbl_spop.no_formulir, nop_proses, 
        dat_objek_pajak.kd_propinsi, dat_objek_pajak.kd_dati2, dat_objek_pajak.kd_kecamatan, dat_objek_pajak.kd_kelurahan, dat_objek_pajak.kd_blok, dat_objek_pajak.no_urut, dat_objek_pajak.kd_jns_op, nm_wp, jalan_op, blok_kav_no_op, rt_op, rw_op, nm_kelurahan, nm_kecamatan, jns_bumi,
        subjek_pajak_id, nm_wp, jalan_wp, blok_kav_no_wp, rt_wp, rw_wp, kelurahan_wp, kota_wp, kd_pos_wp, telp_wp, created_name, created_by, created_at");
        return $core;
    }

    public static function CloningSpopLspop($nop, $tipe = 'PD')
    {
        $kd_propinsi = $nop['kd_propinsi'];
        $kd_dati2 = $nop['kd_dati2'];
        $kd_kecamatan = $nop['kd_kecamatan'];
        $kd_kelurahan = $nop['kd_kelurahan'];
        $kd_blok = $nop['kd_blok'];
        $no_urut = $nop['no_urut'];
        $kd_jns_op = $nop['kd_jns_op'];
        $userid = user()->id ?? '1700';
        $now = Carbon::now();
        $keterangan = "Perbaikan data objek dan subjek pajak";
        $jns_pendataan = '6';

        $batchData = [
            'jenis_pendataan_id' => $jns_pendataan,
            'tipe' => $tipe,
            'verifikasi_at' => $now,
            'verifikasi_by' => $userid,
            'verifikasi_deskripsi' => $keterangan
        ];
        $batch = PendataanBatch::create($batchData);
        // $jns_bumi = $nop['jns_koreksi'] == '04' ? '4' : '5';


        $batal = DB::connection("oracle_dua")->select(DB::raw("SELECT 2 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,substr(A.SUBJEK_PAJAK_ID,1,16) SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI, case when total_luas_bng >0 then '1' else '3' end  JNS_BUMI FROM DAT_OBJEK_PAJAK A
            left JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
            left JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
            WHERE a.kd_propinsi='$kd_propinsi'
            and a.kd_dati2='$kd_dati2'
            AND  a.kd_kecamatan='$kd_kecamatan'
            AND a.kd_kelurahan='$kd_kelurahan'
            AND a.kd_blok='$kd_blok'
            AND a.no_urut='$no_urut'
            AND a.kd_jns_op='$kd_jns_op' "))[0];


        $status_wp = $batal->kd_status_wp ?? '5';
        $nop_asal = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
        $dat_obj = [
            'pendataan_batch_id' => $batch->id,
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' =>  $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'nop_asal' => $nop_asal,
            'subjek_pajak_id' => $batal->subjek_pajak_id ?? '',
            'nm_wp' => $batal->nm_wp ?? '',
            'jalan_wp' => $batal->jalan_wp ?? '',
            'blok_kav_no_wp' => $batal->blok_kav_no_wp ?? '',
            'rw_wp' => padding(($batal->rw_wp ?? ''), 0, 2),
            'rt_wp' => padding(($batal->rt_wp ?? ''), 0, 3),
            'kelurahan_wp' => ($batal->kelurahan_wp ?? ''),
            'kecamatan_wp' => '',
            'propinsi_wp' => '',
            'kota_wp' => $batal->kota_wp != '' ? substr($batal->kota_wp, 0, 30) : '',
            'kd_pos_wp' => ($batal->kd_pos_wp ?? ''),
            'telp_wp' => ($batal->telp_wp ?? ''),
            'npwp' => null,
            'status_pekerjaan_wp' => ($batal->status_pekerjaan_wp ?? ''),
            'no_persil' => ($batal->no_persil ?? ''),
            'jalan_op' => ($batal->jalan_op ?? ''),
            'blok_kav_no_op' => ($batal->blok_kav_no_op ?? ''),
            'rw_op' => padding(($batal->rw_op ?? ''), 0, 2),
            'rt_op' => padding(($batal->rt_op ?? ''), 0, 3),
            'kd_status_cabang' =>  null,
            'kd_status_wp' =>  $status_wp,
            'kd_znt' =>  $batal->kd_znt,
            'luas_bumi' => $batal->luas_bumi,
            'jns_bumi' => $batal->jns_bumi,
            'created_at' => $now,
            'created_by' => $userid,
            'updated_at' => $now,
            'updated_by' => $userid
        ];

        $objek = PendataanObjek::create($dat_obj);

        $spop = [];
        $nop_asal = onlyNumber($objek->nop_asal);
        $jtr = 2;
        $nop_proses = $nop_asal;

        $data = [
            'jenis_pendataan_id' => $objek->batch->jenis_pendataan_id,
            'pendataan_objek_id' => $objek->id,
            'nop_proses' => $nop_proses,
            'nop_asal' => $nop_asal,
            'jns_transaksi' => $jtr,
            'jalan_op' => $objek->jalan_op,
            'blok_kav_no_op' => $objek->blok_kav_no_op,
            'rt_op' => $objek->rt_op,
            'rw_op' => $objek->rw_op,
            'no_persil' => $objek->no_persil,
            'luas_bumi' => $objek->luas_bumi,
            'kd_znt' => $objek->kd_znt,
            'jns_bumi' => $objek->jns_bumi,
            'jml_bng' => $objek->jml_bng,
            'subjek_pajak_id' => $objek->subjek_pajak_id != '' ? $objek->subjek_pajak_id : substr($nop_asal, 2, 16),
            'nm_wp' => $objek->nm_wp <> '' ? $objek->nm_wp : 'WAJIB PAJAK',
            'jalan_wp' => $objek->jalan_wp <> '' ? $objek->jalan_wp : '-',
            'blok_kav_no_wp' => $objek->blok_kav_no_wp,
            'rt_wp' => $objek->rt_wp,
            'rw_wp' => $objek->rw_wp,
            'kelurahan_wp' => $objek->kelurahan_wp,
            'kecamatan_wp' => $objek->kecamatan_wp,
            'kota_wp' => $objek->kota_wp,
            'propinsi_wp' => $objek->propinsi_wp,
            'kd_pos_wp' => $objek->kd_pos_wp,
            'status_pekerjaan_wp' => $objek->status_pekerjaan_wp <> '' ? $objek->status_pekerjaan_wp : '5',
            'kd_status_wp' => $objek->kd_status_wp,
            'npwp' => $objek->npwp,
            'telp_wp' => $objek->telp_wp,
        ];

        $data['created_by'] = $objek->created_by;
        $data['created_at'] = DB::raw('sysdate');
        $data['keterangan'] = $keterangan;

        $req = new \Illuminate\Http\Request();
        $req->merge($data);
        $res = Pajak::prosesPendataan($req);
        return $objek;
    }
}
