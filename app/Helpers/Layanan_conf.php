<?php

namespace App\Helpers;

use Carbon\Carbon;
use Dotenv\Result\Result;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class Layanan_conf
{

    public static function generateNomorLayanan()
    {
        $sql = "SELECT TO_CHAR(SYSDATE, 'yyyymmdd')
        || LPAD (
              NVL (MAX (CAST (SUBSTR (nomor_layanan, 9, 3) AS NUMBER)), 0) + 1,
              3,
              0)
           AS nomor_layanan
            FROM (SELECT nomor_layanan FROM layanan
          UNION
          SELECT nomor_layanan FROM sknjop_permohonan)
            WHERE nomor_layanan LIKE TO_CHAR(SYSDATE, 'yyyymmdd') || '%'";

        $return = DB::connection('oracle')->select(DB::raw($sql));
        return current($return)->nomor_layanan ?? false;
    }

    public static function nomor_layanan($no = false)
    {

        return self::generateNomorLayanan();
        /*  $sql = "SELECT    TO_CHAR (SYSDATE, 'yyyymmdd')
            || LPAD (
                  NVL (MAX (CAST (SUBSTR (nomor_layanan, 9, 3) AS NUMBER)), 0) + 1,
                  3,
                  0)
               AS nomor_layanan
       FROM (SELECT nomor_layanan FROM layanan
             UNION
             SELECT nomor_layanan FROM sknjop_permohonan)
      WHERE nomor_layanan LIKE TO_CHAR (SYSDATE, 'yyyymmdd') || '%'";

        $return = DB::connection('oracle')->select(db::raw($sql));
        $nomor_layanan = current($return)->nomor_layanan ?? false;
        return $nomor_layanan; */
    }
    public static function pengaktifan_nop($no = false)
    {
        return self::generateNomorLayanan();
        /*  $sql = "SELECT    TO_CHAR (SYSDATE, 'yyyymmdd')
                || LPAD (
                    NVL (MAX (CAST (SUBSTR (nomor_layanan, 9, 3) AS NUMBER)), 0) + 1,
                    3,
                    0)
                AS nomor_layanan
        FROM (SELECT nomor_layanan FROM layanan
                UNION
                SELECT nomor_layanan FROM sknjop_permohonan)
        WHERE nomor_layanan LIKE TO_CHAR (SYSDATE, 'yyyymmdd') || '%'";

        $return = DB::connection('oracle')->select(db::raw($sql));
        $nomor_layanan = current($return)->nomor_layanan ?? false;
        return $nomor_layanan; */
    }

    public static function getAlamatNOP($setnop)
    {
        $return = '';
        if (!($setnop)) return $return;
        $nop = explode(".", $setnop);
        $blok_urut = explode("-", $nop[4]);
        $tglNow = Carbon::now();
        $tahun = $tglNow->year;
        //||' '||sp.kelurahan_wp_sppt||' '||sp.kota_wp_sppt 
        $sql = "SELECT 
                sp.jln_wp_sppt nop_alamat
            FROM
                spo.sppt@tospo205 sp
            WHERE sp.kd_propinsi='" . $nop[0] . "'
            AND sp.kd_dati2='" . $nop[1] . "'
            AND sp.kd_kecamatan='" . $nop[2] . "'
            AND sp.kd_kelurahan='" . $nop[3] . "'
            AND sp.kd_blok='" . $blok_urut[0] . "'
            AND sp.no_urut='" . $blok_urut[1] . "'
            AND kd_jns_op='" . $nop[5] . "'
            AND sp.thn_pajak_sppt='" . $tahun . "'";
        $result = DB::connection('oracle')->select(db::raw($sql));
        if ($result) {
            $return = current($result)->nop_alamat ?? false;
        }
        return $return;
    }
    public static function cek_nop($nop)
    {
        $nop = explode(".", $nop);
        $blok_urut = explode("-", $nop[4]);
        $kd_propinsi = $nop[0];
        $kd_dati2 = $nop[1];
        $kd_kecamatan = $nop[2];
        $kd_kelurahan = $nop[3];
        $kd_blok = $blok_urut[0];
        $no_urut = $blok_urut[1];
        $kd_jns_op = $nop[5];
        $tahun = date('Y');
        $objek = DB::table(DB::raw("spo.sppt sppt"))
            ->select(DB::raw("
            dat_subjek_pajak.subjek_pajak_id nik_wp,
            dat_subjek_pajak.nm_wp nama_wp,
            dat_objek_pajak.jalan_op nop_alamat,
            dat_objek_pajak.rt_op nop_rt,
            dat_objek_pajak.rw_op nop_rw,
            ref_kelurahan.nm_kelurahan nop_kelurahan,
            ref_kecamatan.nm_kecamatan nop_kecamatan,
            sppt.KD_PROPINSI,
            sppt.KD_DATI2,
            sppt.KD_KECAMATAN,
              sppt.KD_KELURAHAN,
               sppt.KD_BLOK,
               sppt.NO_URUT,
              sppt.KD_JNS_OP,
               sppt.THN_PAJAK_SPPT,
               sppt.SIKLUS_SPPT,
              sppt.KD_KANWIL,
               sppt.KD_KANTOR,
               sppt.KD_TP,
              sppt.NM_WP_SPPT,
               sppt.JLN_WP_SPPT,
               sppt.BLOK_KAV_NO_WP_SPPT,
              sppt.RW_WP_SPPT,
               sppt.RT_WP_SPPT,
               sppt.KELURAHAN_WP_SPPT,
              sppt.KOTA_WP_SPPT,
               sppt.KD_POS_WP_SPPT,
               sppt.NPWP_SPPT,
              sppt.NO_PERSIL_SPPT,
               sppt.KD_KLS_TANAH,
               sppt.THN_AWAL_KLS_TANAH,
              sppt.KD_KLS_BNG,
               sppt.THN_AWAL_KLS_BNG,
               sppt.TGL_JATUH_TEMPO_SPPT,
              sppt.LUAS_BUMI_SPPT,
               nvl(sppt.LUAS_BNG_SPPT,0) LUAS_BNG_SPPT,
               nvl(sppt.NJOP_BUMI_SPPT,0) NJOP_BUMI_SPPT,
              nvl(sppt.NJOP_BNG_SPPT,0) NJOP_BNG_SPPT,
               nvl(sppt.NJOP_SPPT,0) NJOP_SPPT,
               sppt.NJOPTKP_SPPT,
              sppt.PBB_TERHUTANG_SPPT,
               sppt.FAKTOR_PENGURANG_SPPT,
               sppt.PBB_YG_HARUS_DIBAYAR_SPPT,
              sppt.STATUS_PEMBAYARAN_SPPT,
               sppt.STATUS_TAGIHAN_SPPT,
               sppt.STATUS_CETAK_SPPT, 
              sppt.TGL_TERBIT_SPPT,
               sppt.TGL_CETAK_SPPT,
               sppt.NIP_PENCETAK_SPPT,(select trim(jalan_op ||' '||blok_kav_no_op ||' RT :'||RT_OP||' RW:'||RW_OP)  
            from spo.dat_objek_pajak
            where kd_kecamatan=sppt.kd_kecamatan
            and kd_kelurahan=sppt.kd_kelurahan
            and kd_blok=sppt.kd_blok
            and no_urut=sppt.no_urut
            and kd_jns_op=sppt.kd_jns_op) alamat_op,nm_kelurahan,nm_kecamatan,(
                select  case when count(1)=0 then 1 else case when sum(case when status_pembayaran_sppt='1' then 1 else 0 end) = count(1) then 1 else 0 end end
                       from spo.sppt a
                       WHERE     thn_pajak_sppt between cast(to_char(sysdate,'yyyy') as number)-4
                       and cast(to_char(sysdate,'yyyy') as number)
                       AND a.kd_propinsi=SPPT.KD_PROPINSI 
                       AND a.KD_DATI2=SPPT.KD_DATI2 
                       AND a.KD_KECAMATAN=SPPT.KD_KECAMATAN
                       AND a.KD_KELURAHAN=SPPT.KD_KELURAHAN
                       AND a.KD_BLOK=SPPT.KD_BLOK
                       AND a.NO_URUT=SPPT.NO_URUT
                       AND a.KD_JNS_OP=SPPT.KD_JNS_OP       
                       ) lunas_lima"))
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('sppt.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('sppt.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->On('sppt.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
            })
            ->join(db::raw("pbb.dat_objek_pajak dat_objek_pajak"), function ($join) {
                $join->On('dat_objek_pajak.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->On('dat_objek_pajak.kd_dati2', '=', 'sppt.kd_dati2')
                    ->On('dat_objek_pajak.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->On('dat_objek_pajak.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->On('dat_objek_pajak.kd_blok', '=', 'sppt.kd_blok')
                    ->On('dat_objek_pajak.no_urut', '=', 'sppt.no_urut')
                    ->On('dat_objek_pajak.kd_jns_op', '=', 'sppt.kd_jns_op');
            })
            ->join(db::raw("pbb.dat_subjek_pajak dat_subjek_pajak"), function ($join) {
                $join->On('dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id');
            })
            ->whereraw(DB::raw("thn_pajak_sppt='$tahun' and SPPT.KD_PROPINSI = '35'
        AND SPPT.KD_DATI2 = '07'
        AND SPPT.KD_KECAMATAN = '$kd_kecamatan'
        AND SPPT.KD_KELURAHAN = '$kd_kelurahan'
        AND SPPT.KD_BLOK = '$kd_blok'
        AND SPPT.NO_URUT = '$no_urut'
        AND SPPT.KD_JNS_OP = '$kd_jns_op'"))->first();
        return $objek;
    }
    public static function riwayatPembayaran($param)
    {
        $kd_propinsi = $param['kd_propinsi'];
        $kd_dati2 = $param['kd_dati2'];
        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $kd_blok = $param['kd_blok'];
        $no_urut = $param['no_urut'];
        $kd_jns_op = $param['kd_jns_op'];
        DB::connection('oracle_spo')->statement("ALTER SESSION SET NLS_NUMERIC_CHARACTERS =',.' ");

        $riwayat = DB::connection('oracle_spo')->table(DB::raw("spo.sppt sppt"))->select(DB::raw("
        sppt.status_pembayaran_sppt,  
        sppt.kd_propinsi,
        sppt.kd_dati2,
        sppt.kd_kecamatan,
        sppt.kd_kelurahan,
        ref_kecamatan.nm_kecamatan,
        ref_kelurahan.nm_kelurahan,
        sppt.kd_blok,
        sppt.no_urut,
        sppt.kd_jns_op,
        sppt.thn_pajak_sppt,
        sppt.luas_bumi_sppt,
         sppt.njop_bumi_sppt,
         sppt.luas_bng_sppt,
         sppt.njop_bng_sppt,
        sppt.nm_wp_sppt,
        TO_CHAR (pbb_yg_harus_dibayar_sppt, 'FM999G999G999G999') pbb,
        TO_CHAR (
            NVL (
               (SELECT jml_sppt_yg_dibayar - denda_sppt
                  FROM pbb.pembayaran_sppt b
                 WHERE     b.kd_propinsi = sppt.kd_propinsi
                       AND b.kd_dati2 = sppt.kd_dati2
                       AND b.kd_kecamatan = sppt.kd_kecamatan
                       AND b.kd_kelurahan = sppt.kd_kelurahan
                       AND b.kd_blok = sppt.kd_blok
                       AND b.no_urut = sppt.no_urut
                       AND b.kd_jns_op = sppt.kd_jns_op
                       AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                       AND ROWNUM = 1),
               pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0)),
            'FM999G999G999G999')
            pokok_bayar,
            TO_CHAR (
                NVL (
                   ( select * from (SELECT b.denda_sppt
                      FROM pbb.pembayaran_sppt b
                     WHERE     b.kd_propinsi = sppt.kd_propinsi
                           AND b.kd_dati2 = sppt.kd_dati2
                           AND b.kd_kecamatan = sppt.kd_kecamatan
                           AND b.kd_kelurahan = sppt.kd_kelurahan
                           AND b.kd_blok = sppt.kd_blok
                           AND b.no_urut = sppt.no_urut
                           AND b.kd_jns_op = sppt.kd_jns_op
                           AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                    UNION
                    SELECT b.denda_sppt
                      FROM spo.pembayaran_sppt b
                     WHERE     b.kd_propinsi = sppt.kd_propinsi
                           AND b.kd_dati2 = sppt.kd_dati2
                           AND b.kd_kecamatan = sppt.kd_kecamatan
                           AND b.kd_kelurahan = sppt.kd_kelurahan
                           AND b.kd_blok = sppt.kd_blok
                           AND b.no_urut = sppt.no_urut
                           AND b.kd_jns_op = sppt.kd_jns_op
                           AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                           ) where  ROWNUM = 1),
                spo.get_denda (sppt.pbb_yg_harus_dibayar_sppt,
                                sppt.tgl_jatuh_tempo_sppt,
                                SYSDATE)),
            'FM999G999G999G999')
            denda_bayar,
            TO_CHAR (
                NVL (
                   (select * from (SELECT jml_sppt_yg_dibayar
                      FROM pbb.pembayaran_sppt b
                     WHERE     b.kd_propinsi = sppt.kd_propinsi
                           AND b.kd_dati2 = sppt.kd_dati2
                           AND b.kd_kecamatan = sppt.kd_kecamatan
                           AND b.kd_kelurahan = sppt.kd_kelurahan
                           AND b.kd_blok = sppt.kd_blok
                           AND b.no_urut = sppt.no_urut
                           AND b.kd_jns_op = sppt.kd_jns_op
                           AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                    UNION
                    SELECT jml_sppt_yg_dibayar
                    FROM spo.pembayaran_sppt b
                    WHERE     b.kd_propinsi = sppt.kd_propinsi
                        AND b.kd_dati2 = sppt.kd_dati2
                        AND b.kd_kecamatan = sppt.kd_kecamatan
                        AND b.kd_kelurahan = sppt.kd_kelurahan
                        AND b.kd_blok = sppt.kd_blok
                        AND b.no_urut = sppt.no_urut
                        AND b.kd_jns_op = sppt.kd_jns_op
                        AND b.thn_pajak_sppt = sppt.thn_pajak_sppt)
                        where ROWNUM = 1),
                0),
            'FM999G999G999G999')
            total_bayar,
        (select max(tanggal) from ( 
            SELECT case when sppt.status_pembayaran_sppt=1 then  TRUNC (tgl_pembayaran_sppt)else null end tanggal
                FROM pbb.pembayaran_sppt b
                WHERE     b.kd_propinsi = sppt.kd_propinsi
                    AND b.kd_dati2 = sppt.kd_dati2
                    AND b.kd_kecamatan = sppt.kd_kecamatan
                    AND b.kd_kelurahan = sppt.kd_kelurahan
                    AND b.kd_blok = sppt.kd_blok
                    AND b.no_urut = sppt.no_urut
                    AND b.kd_jns_op = sppt.kd_jns_op
                    AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                UNION
                SELECT case when sppt.status_pembayaran_sppt=1 then  TRUNC (tgl_pembayaran_sppt)else null end
                FROM spo.pembayaran_sppt b
                WHERE     b.kd_propinsi = sppt.kd_propinsi
                    AND b.kd_dati2 = sppt.kd_dati2
                    AND b.kd_kecamatan = sppt.kd_kecamatan
                    AND b.kd_kelurahan = sppt.kd_kelurahan
                    AND b.kd_blok = sppt.kd_blok
                    AND b.no_urut = sppt.no_urut
                    AND b.kd_jns_op = sppt.kd_jns_op
                    AND b.thn_pajak_sppt = sppt.thn_pajak_sppt)
                )
                tgl_pembayaran_sppt,
            unflag_desc"))
            ->leftjoin(DB::raw("(select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,
        LISTAGG(unflag_desc, ', ') WITHIN GROUP (ORDER BY unflag_id) unflag_desc
        from pbb.unflag_history
        group by kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt) unflag_history"), function ($join) {
                $join->on('sppt.KD_PROPINSI', '=', 'unflag_history.KD_PROPINSI')
                    ->on('sppt.KD_DATI2', '=', 'unflag_history.KD_DATI2')
                    ->on('sppt.KD_KECAMATAN', '=', 'unflag_history.KD_KECAMATAN')
                    ->on('sppt.KD_KELURAHAN', '=', 'unflag_history.KD_KELURAHAN')
                    ->on('sppt.KD_BLOK', '=', 'unflag_history.KD_BLOK')
                    ->on('sppt.NO_URUT', '=', 'unflag_history.NO_URUT')
                    ->on('sppt.KD_JNS_OP', '=', 'unflag_history.KD_JNS_OP')
                    ->on('SPPT.THN_PAJAK_SPPT', '=', 'unflag_history.thn_pajak_sppt');
            })
            ->leftjoin(DB::raw("sppt_potongan sppt_potongan"), function ($join) {
                $join->on('sppt.KD_PROPINSI', '=', 'sppt_potongan.KD_PROPINSI')
                    ->on('sppt.KD_DATI2', '=', 'sppt_potongan.KD_DATI2')
                    ->on('sppt.KD_KECAMATAN', '=', 'sppt_potongan.KD_KECAMATAN')
                    ->on('sppt.KD_KELURAHAN', '=', 'sppt_potongan.KD_KELURAHAN')
                    ->on('sppt.KD_BLOK', '=', 'sppt_potongan.KD_BLOK')
                    ->on('sppt.NO_URUT', '=', 'sppt_potongan.NO_URUT')
                    ->on('sppt.KD_JNS_OP', '=', 'sppt_potongan.KD_JNS_OP')
                    ->on('SPPT.THN_PAJAK_SPPT', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->leftjoin(DB::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
            ->leftjoin(DB::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan');
            })
            ->whereraw("sppt.KD_PROPINSI = '$kd_propinsi'
            AND sppt.KD_DATI2 = '$kd_dati2'
            AND sppt.KD_KECAMATAN = '$kd_kecamatan'
            AND sppt.KD_KELURAHAN = '$kd_kelurahan'
            AND sppt.KD_BLOK = '$kd_blok'
            AND sppt.NO_URUT = '$no_urut'
            AND sppt.KD_JNS_OP = '$kd_jns_op'
            and sppt.thn_pajak_sppt>=2014");
        return $riwayat;
    }
    public static function dateCustom($tgl, $from = 'd-m-Y', $to = 'Y-m-d')
    {
        if (!$tgl) return;
        return Carbon::createFromFormat($from, $tgl)->format($to);
    }
}
