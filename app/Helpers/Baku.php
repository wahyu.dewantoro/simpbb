<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Matrix\Operators\Subtraction;

class Baku
{
    public static function getBuku($no = false)
    {
        $setBuku = [
            1 => ["min" => 0, "max" => 100000],
            2 => ["min" => 100001, "max" => 500000],
            3 => ["min" => 500001, "max" => 2000000],
            4 => ["min" => 2000001, "max" => 5000000],
            5 => ["min" => 5000001]
        ];
        return Arr::only($setBuku, [$no])[$no];
    }


    public static function coreSql($param)
    {
        $is_dhkp = $param['is_dhkp'] ?? true;

        // $is_tagihan = $param['is_tagihan'] ?? false; 
        $cut_bayar = $param['cut_bayar'] ?? true;


        $buku = $param['buku'] ?? [];
        $endDate = $param['cut'] ?? date('Ymd');
        $a = new Carbon($endDate);
        $ys = $a->format('Y');

        $b = new Carbon($endDate);
        $end = $b->format('Ymd');
        $start = $ys . '0101';

        if ($is_dhkp == false) {
            $start = '20030101';
        }

        $wb = "";

        $cut_pembayaran = " ";
        $cut_koreksi = " ";
        if ($cut_bayar == true) {
            $cut_pembayaran = " AND TRUNC (tgl_pembayaran_sppt) BETWEEN TO_DATE (
                                                             '" . $start . "',
                                                             'yyyymmdd')
                                                      AND TO_DATE (
                                                             '" . $end . "',
                                                             'yyyymmdd')";
            $cut_koreksi = "   AND pengurang_piutang.tgl_surat BETWEEN TO_DATE ('" . $start . "',
                                                        'yyyymmdd')
                                           AND TO_DATE ('" . $end . "',
                                                        'yyyymmdd')";
        }


        // jika ada buku
        if (count($buku)) {
            foreach ($buku as $b) {
                if ($b == 1) {
                    $wb .= "(sppt.pbb_yg_harus_dibayar_sppt between 0 and 100000 ) or";
                } else if ($b == 2) {
                    $wb .= "(sppt.pbb_yg_harus_dibayar_sppt between 100001 and 500000 ) or";
                } else if ($b == 3) {
                    $wb .= "(sppt.pbb_yg_harus_dibayar_sppt between 500001 and 2000000 ) or";
                } else if ($b == 4) {
                    $wb .= "(sppt.pbb_yg_harus_dibayar_sppt between 2000001 and 5000000 ) or";
                } else {
                    $wb .= "(sppt.pbb_yg_harus_dibayar_sppt>=5000001 ) or";
                }
            }

            if ($wb != '') {
                $wb = substr($wb, 0, -2);

                $wb = "  (" . $wb . ") and";
            }
        }



        $sql = "SELECT KD_PROPINSI,
       KD_DATI2,
       KD_KECAMATAN,
       KD_KELURAHAN,
       KD_BLOK,
       NO_URUT,
       KD_JNS_OP,
       THN_PAJAK_SPPT,
       TGL_TERBIT_SPPT,
       nvl(pbb_last,pbb) pbb_last,
      pbb - nvl(pbb_last,pbb) perubahan,
       PBB,
       POTONGAN,
       case when bayar > (pbb - potongan ) then (pbb - potongan ) else 
       BAYAR
       end bayar,
       bayar_denda,
       TGL_BAYAR,
      case when bayar>0 then 0 else  koreksi end  KOREKSI,
       TGL_KOREKSI,
       case when (nota - bayar) <0 then 0 else (nota - bayar ) end nota from (SELECT sppt.kd_propinsi,
       sppt.kd_dati2,
       sppt.kd_kecamatan,
       sppt.kd_kelurahan,
       sppt.kd_blok,
       sppt.no_urut,
       sppt.kd_jns_op,
       sppt.thn_pajak_sppt,
       tgl_terbit_sppt,
       (select pbb_yg_harus_dibayar_sppt 
from sim_pbb.sppt_bkp 
where rowid in (
select max(rowid) ri
from sim_pbb.sppt_bkp
where thn_pajak_sppt=sppt.thn_pajak_sppt
and kd_kecamatan=sppt.kd_kecamatan and kd_kelurahan=sppt.kd_kelurahan
and kd_blok=sppt.kd_blok and no_urut=sppt.no_urut and kd_jns_op=sppt.kd_jns_op)) pbb_last,
       sppt.pbb_yg_harus_dibayar_sppt pbb,
       NVL (nilai_potongan, 0) potongan,
       NVL (
          (SELECT SUM (jml_sppt_yg_dibayar - denda_sppt)
             FROM pembayaran_sppt
            WHERE     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
                  AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                  AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
                  AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
                  AND sppt.kd_blok = pembayaran_sppt.kd_blok
                  AND sppt.no_urut = pembayaran_sppt.no_urut
                  AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                  AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
                  " . $cut_pembayaran . "
                  ),
          0)
          bayar,
          NVL (
                            (SELECT SUM (denda_sppt)
                               FROM pembayaran_sppt
                              WHERE     sppt.kd_propinsi =
                                           pembayaran_sppt.kd_propinsi
                                    AND sppt.kd_dati2 =
                                           pembayaran_sppt.kd_dati2
                                    AND sppt.kd_kecamatan =
                                           pembayaran_sppt.kd_kecamatan
                                    AND sppt.kd_kelurahan =
                                           pembayaran_sppt.kd_kelurahan
                                    AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                    AND sppt.no_urut = pembayaran_sppt.no_urut
                                    AND sppt.kd_jns_op =
                                           pembayaran_sppt.kd_jns_op
                                    AND sppt.thn_pajak_sppt =
                                           pembayaran_sppt.thn_pajak_sppt
                                    " . $cut_pembayaran . "
                                           ),
                            0)
                            bayar_denda,
       (SELECT MAX (TRUNC (tgl_pembayaran_sppt))
          FROM pembayaran_sppt
         WHERE     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
               AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
               AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
               AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
               AND sppt.kd_blok = pembayaran_sppt.kd_blok
               AND sppt.no_urut = pembayaran_sppt.no_urut
               AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
               AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
              " . $cut_pembayaran . "
               )
          tgl_bayar,
      ( CASE
          WHEN tgl_surat IS NOT NULL
          THEN
             sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
          ELSE
             0
       END)
          koreksi,
       tgl_surat tgl_koreksi,
        CASE
                  WHEN sppt_nota.thn_pajak_sppt IS NOT NULL
                  THEN
                     sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                  ELSE
                     0
               END
                  nota
  FROM sppt
       LEFT JOIN sppt_potongan
          ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
             AND sppt.kd_dati2 = sppt_potongan.kd_dati2
             AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
             AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
             AND sppt.kd_blok = sppt_potongan.kd_blok
             AND sppt.no_urut = sppt_potongan.no_urut
             AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
             AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
        LEFT JOIN sppt_nota
                  ON     sppt.kd_propinsi = sppt_nota.kd_propinsi
                     AND sppt.kd_dati2 = sppt_nota.kd_dati2
                     AND sppt.kd_kecamatan = sppt_nota.kd_kecamatan
                     AND sppt.kd_kelurahan = sppt_nota.kd_kelurahan
                     AND sppt.kd_blok = sppt_nota.kd_blok
                     AND sppt.no_urut = sppt_nota.no_urut
                     AND sppt.kd_jns_op = sppt_nota.kd_jns_op
                     AND sppt.thn_pajak_sppt = sppt_nota.thn_pajak_sppt
       LEFT JOIN pengurang_piutang 
          ON     sppt.kd_propinsi = pengurang_piutang.kd_propinsi
             AND sppt.kd_dati2 = pengurang_piutang.kd_dati2
             AND sppt.kd_kecamatan = pengurang_piutang.kd_kecamatan
             AND sppt.kd_kelurahan = pengurang_piutang.kd_kelurahan
             AND sppt.kd_blok = pengurang_piutang.kd_blok
             AND sppt.no_urut = pengurang_piutang.no_urut
             AND sppt.kd_jns_op = pengurang_piutang.kd_jns_op
             AND sppt.thn_pajak_sppt = pengurang_piutang.thn_pajak_sppt
           " . $cut_koreksi . "
            WHERE   ";

        if ($is_dhkp == true) {
            $sql .= " TRUNC (tgl_terbit_sppt) <=  TO_DATE ('" . $end . "', 'yyyymmdd') and";
            $sql .= " TRUNC (tgl_terbit_sppt) >=TO_DATE ('" . $start . "', 'yyyymmdd') 
                and TO_CHAR (sppt.tgl_terbit_sppt, 'yyyy') = '" . $ys . "'
                AND sppt.thn_pajak_sppt = '" . $ys . "' and";
        } else {
            $ort = "";
            $tahun_terbit = $param['tahun_terbit'] ?? '';
            if ($tahun_terbit != '') {
                $ort .= " TO_CHAR (sppt.tgl_terbit_sppt, 'yyyy') ='" . $tahun_terbit . "' and";
            }


            $tahun_pajak = $param['tahun_pajak'] ?? '';
            if ($tahun_pajak != '') {
                $ort .= " sppt.thn_pajak_sppt='" . $tahun_pajak . "' and";
            }

            if ($ort != '') {
                $ort = substr($ort, 0, -3);
                $sql .= "  (" . $ort . ")  and";
            }
        }


        $kd_kecamatan = $param['kd_kecamatan'] ?? '';
        $kd_kelurahan = $param['kd_kelurahan'] ?? '';
        if ($kd_kecamatan != '') {
            $sql .= "  sppt.kd_kecamatan='" . $kd_kecamatan . "' and ";
        }

        if ($kd_kelurahan != '') {
            $sql .= "  sppt.kd_kelurahan='" . $kd_kelurahan . "' and";
        }

        if ($wb != '') {
            $sql .= " " . $wb;
        }

        if (substr($sql, -3) == 'and') {
            $sql = substr($sql, 0, -3);
        }

        $sql .= ")";
        return $sql;
    }

    public static function get($param)
    {
        $sql = self::coreSql($param);
        return DB::connection("oracle_satutujuh")->table(DB::raw("(" . $sql . ") tmp"));
    }

    public static function RekapBakuDesa($param)
    {
        $sql = self::coreSql($param);
        return DB::connection("oracle_satutujuh")->table(DB::raw("(" . $sql . ") tmp"))
            ->selectraw("KD_KECAMATAN,(select nm_kecamatan from ref_kecamatan where kd_kecamatan=tmp.kd_kecamatan) nm_kecamatan,
         KD_KELURAHAN, (select nm_kelurahan from ref_kelurahan where kd_kecamatan=tmp.kd_kecamatan and kd_kelurahan=tmp.kd_kelurahan) nm_kelurahan,
         get_buku(pbb) buku,
         count(thn_pajak_sppt) jumlah_op,
           SUM (pbb_last) baku_last,
         SUM (pbb) baku,
         SUM (potongan) potongan,
         SUM (bayar) bayar,
         SUM (koreksi) koreksi,
       case when  ( SUM (pbb) -
         SUM (potongan) -
         SUM (bayar) -
         SUM (koreksi))<0 then 0 else ( SUM (pbb) -
         SUM (potongan) -
         SUM (bayar) -
         SUM (koreksi)) end 
         sisa")
            ->whereRaw("nota=0")
            ->groupByRaw("KD_KECAMATAN,
         KD_KELURAHAN,get_buku(pbb)")->orderByRaw("kd_kecamatan,kd_kelurahan,get_buku(pbb)");
    }

    public static function RekapDesa($param)
    {
        $sql = self::coreSql($param);
        return DB::connection("oracle_satutujuh")->table(DB::raw("(" . $sql . ") tmp"))
            ->selectraw("KD_KECAMATAN,(select nm_kecamatan from ref_kecamatan where kd_kecamatan=tmp.kd_kecamatan) nm_kecamatan,
         KD_KELURAHAN, (select nm_kelurahan from ref_kelurahan where kd_kecamatan=tmp.kd_kecamatan and kd_kelurahan=tmp.kd_kelurahan) nm_kelurahan,
         count(thn_pajak_sppt) jumlah_op,
         SUM (pbb) baku,
         SUM (potongan) potongan,
         SUM (bayar) bayar,
         SUM (koreksi) koreksi,
       case when  ( SUM (pbb) -
         SUM (potongan) -
         SUM (bayar) -
         SUM (koreksi))<0 then 0 else ( SUM (pbb) -
         SUM (potongan) -
         SUM (bayar) -
         SUM (koreksi)) end 
         sisa")
            ->whereRaw("nota=0")
            ->groupByRaw("KD_KECAMATAN, KD_KELURAHAN")->orderByRaw("kd_kecamatan,kd_kelurahan");
    }

    public static function RekapDesaTunggakan($param)
    {
        $sql = self::coreSql($param);
        return DB::connection("oracle_satutujuh")->table(DB::raw("(" . $sql . ") tmp"))
            ->selectraw("KD_KECAMATAN,(select nm_kecamatan from ref_kecamatan where kd_kecamatan=tmp.kd_kecamatan) nm_kecamatan,
         KD_KELURAHAN, (select nm_kelurahan from ref_kelurahan where kd_kecamatan=tmp.kd_kecamatan and kd_kelurahan=tmp.kd_kelurahan) nm_kelurahan,
         count(thn_pajak_sppt) jumlah_op,
         SUM (pbb) baku,
         SUM (potongan) potongan,
         SUM (bayar) bayar,
         SUM (koreksi) koreksi,
       case when  ( SUM (pbb) -
         SUM (potongan) -
         SUM (bayar) -
         SUM (koreksi))<0 then 0 else ( SUM (pbb) -
         SUM (potongan) -
         SUM (bayar) -
         SUM (koreksi)) end 
         sisa")
            ->whereRaw("nota=0 and koreksi =0 and bayar<(pbb-potongan)")
            ->groupByRaw("KD_KECAMATAN, KD_KELURAHAN")->orderByRaw("kd_kecamatan,kd_kelurahan");
    }




    public static function RangkingKecamatan($param)
    {
        $endDate = $param['cut'] ?? date('Ymd');
        $a = new Carbon($endDate);
        $ys = $a->format('Y');
        $sql = self::RekapDesa($param)->toSql();
        $data = DB::connection("oracle_satutujuh")->table(db::raw("(" . $sql . ") tmp"))
            ->selectRaw("tmp.kd_kecamatan,
         tmp.nm_kecamatan,
         sum(jumlah_op) jumlah_op,
         SUM (baku) baku,
         sum(potongan) potongan,
         sum(bayar) bayar,
         sum(koreksi) koreksi,
         CASE
            WHEN (  (  SUM (bayar)
                     / (SUM (baku) - SUM (potongan) - SUM (koreksi)))
                  * 100) > 100
            THEN
               100
            ELSE
               round(  (SUM (bayar) / (SUM (baku) - SUM (potongan) - SUM (koreksi)))
                * 100,3)
         END
            persen,
         (SELECT tanggal_lunas
            FROM sim_pbb.kecamatan_lunas
           WHERE kd_kecamatan = tmp.kd_kecamatan AND tahun_pajak = '$ys')
            tanggal_lunas")
            ->groupbyraw("tmp.kd_kecamatan, tmp.nm_kecamatan");

        $sqldua = $data->toSql();

        return DB::connection("oracle_satutujuh")->table(db::raw("(" . $sqldua . ") tb"))
            ->orderByRaw("persen desc,tanggal_lunas asc nulls last");
    }


    public static function RangkingDesa($param)
    {

        $endDate = $param['cut'] ?? date('Ymd');

        $a = new Carbon($endDate);
        $ys = $a->format('Y');

        $sql = self::RekapDesa($param)->toSql();

        $data = DB::connection("oracle_satutujuh")->table(db::raw("(" . $sql . ") tmp"))
            ->selectRaw("tmp.*,
         case when (bayar / (baku - potongan - koreksi) * 100) >100 then 100 else round(bayar / (baku - potongan - koreksi) * 100,3) end persen,
         (SELECT tanggal_lunas
            FROM sim_pbb.desa_lunas
           WHERE     kd_kecamatan = tmp.kd_kecamatan
                 AND kd_kelurahan = tmp.kd_kelurahan
                 AND tahun_pajak = '$ys'
                 AND tanggal_lunas <= TO_DATE ('$endDate', 'yyyymmdd'))
            tanggal_lunas")
            ->orderbyraw("case when (bayar / (baku - potongan - koreksi) * 100) >100 then 100 else (bayar / (baku - potongan - koreksi) * 100) end DESC,
         (SELECT  to_date(to_char(tanggal_lunas,'yyyymmdd')||to_char(created_at,'HH24:MI:SS'),'yyyymmddHH24:MI:SS')
            FROM sim_pbb.desa_lunas
           WHERE     kd_kecamatan = tmp.kd_kecamatan
                 AND kd_kelurahan = tmp.kd_kelurahan
                 AND tahun_pajak = '$ys'
                 AND tanggal_lunas <= TO_DATE ('$endDate', 'yyyymmdd')) ASC NULLS LAST");

        return $data;
    }


    public static function CoreSqlRealisasi()
    {
        $sql = "
    SELECT pembayaran_sppt.kd_propinsi,
                           pembayaran_sppt.kd_dati2,
                           pembayaran_sppt.kd_kecamatan,
                           pembayaran_sppt.kd_kelurahan,
                           pembayaran_sppt.kd_blok,
                           pembayaran_sppt.no_urut,
                           pembayaran_sppt.kd_jns_op,
                           pembayaran_sppt.thn_pajak_sppt,
                           tgl_terbit_sppt,
                           TO_CHAR (tgl_terbit_sppt, 'yyyy') tahun_terbit,
                           get_buku (pbb_yg_harus_dibayar_sppt)
                              buku,
                           pbb_yg_harus_dibayar_sppt pbb,
                           jml_sppt_yg_dibayar - denda_sppt pokok,
                           denda_sppt denda,
                           jml_sppt_yg_dibayar jumlah,
                           TRUNC (tgl_pembayaran_sppt) tanggal_bayar
                      FROM pembayaran_sppt
                           LEFT JOIN sppt
                              ON     sppt.kd_propinsi =
                                        pembayaran_sppt.kd_propinsi
                                 AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                 AND sppt.kd_kecamatan =
                                        pembayaran_sppt.kd_kecamatan
                                 AND sppt.kd_kelurahan =
                                        pembayaran_sppt.kd_kelurahan
                                 AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                 AND sppt.no_urut = pembayaran_sppt.no_urut
                                 AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                 AND sppt.thn_pajak_sppt =
                                        pembayaran_sppt.thn_pajak_sppt";
        return $sql;
    }


    public static function Realisasi($param)
    {
        $is_tunggakan = $param['is_tunggakan'] ?? false;
        $cut_off = $param['cut'] ?? date('Ymd');
        $buku = $param['buku'] ?? '';
        $fn = new Carbon($cut_off);
        $fm = new Carbon($cut_off);
        $tahun = $fm->format('Y');

        $start = $tahun . '0101';
        $end = $fn->format('Ymd');



        $sql = self::CoreSqlRealisasi();
        $wh = "trunc(tgl_pembayaran_sppt) between to_date('" . $start . "','yyyymmdd') and to_date('" . $end . "','yyyymmdd') and";
        if ($is_tunggakan == false) {
            $wh .= " TO_CHAR (tgl_terbit_sppt, 'yyyy') ='" . $tahun . "' and";
        } else {
            $wh .= " TO_CHAR (tgl_terbit_sppt, 'yyyy') <'" . $tahun . "' and";
        }

        if ($buku != '') {
            $wh .= " get_buku (pbb_yg_harus_dibayar_sppt)='$buku' and";
        }


        if ($wh != '') {
            $wh = substr($wh, 0, -3);
        }
        $table = $sql . " where " . $wh;
        return DB::connection("oracle_satutujuh")->table(DB::raw("(" . $table . ") tmp"));
    }

    public static function RekapRealiasasi($param)
    {
        $is_tunggakan = $param['is_tunggakan'] ?? false;
        $cut_off = $param['cut'] ?? date('Ymd');
        $fn = new Carbon($cut_off);
        $fm = new Carbon($cut_off);
        $tahun = $fm->format('Y');

        $start = $tahun . '0101';
        $end = $fn->format('Ymd');



        $sql = self::CoreSqlRealisasi();
        $wh = "trunc(tgl_pembayaran_sppt) between to_date('" . $start . "','yyyymmdd') and to_date('" . $end . "','yyyymmdd') and";
        if ($is_tunggakan == false) {
            $wh .= " TO_CHAR (tgl_terbit_sppt, 'yyyy') ='" . $tahun . "' and";
        } else {
            $wh .= " TO_CHAR (tgl_terbit_sppt, 'yyyy') <'" . $tahun . "' and";
        }


        if ($wh != '') {
            $wh = substr($wh, 0, -3);
        }
        $table = $sql . " where " . $wh;


        return DB::connection("oracle_satutujuh")->table(DB::raw("(" . $table . ") tmp"))
            ->selectRaw("buku,
         SUM (pokok) pokok,
         SUM (denda) denda,
         SUM (jumlah) jumlah")
            ->groupByRaw("buku");
    }
}
