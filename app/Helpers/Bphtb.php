<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Bphtb
{

    public static function rekapKecamatan($tahun)
    {
        $res = DB::connection('bphtb')->table(DB::raw('rpt_sspd_final'))
            ->select(DB::raw("SUBSTR (nop, 5, 3) kd_kecamatan, nama_kec_op nm_kecamatan,count(1) jumlah"))
            ->groupByRaw(DB::raw("SUBSTR (nop, 5, 3),nama_kec_op"))
            ->orderby('nama_kec_op');
        return $res;
    }

    public static function ajuanKecamatan($tahun, $kd_kecamatan)
    {
        $res = DB::connection('bphtb')
            ->table(DB::raw("sppt"))
            ->select(DB::raw("rpt_sspd_final.NOP, nm_wp_sppt,TAHUN, JLN_WP_SPPT, BLOK_KAV_NO_WP_SPPT, RT_WP_SPPT, RW_WP_SPPT, KELURAHAN_WP_SPPT, KOTA_WP_SPPT, LUAS_BUMI_SPPT, LUAS_BNG_SPPT, NAMA_WP, ALAMAT_WP, RT_WP, RW_WP, NAMA_KEL, NAMA_KEC, NAMA_KAB, ALAMAT_OP, RT_OP, RW_OP, NAMA_KEL_OP, NAMA_KEC_OP, NAMA_KAB_OP, LUAS_TANAH, LUAS_BANGUNAN, tanggal TANGGAL_PERMOHONAN, bphtb_penelitian BPHTB"))
            ->join(DB::raw('rpt_sspd_final'), function ($join) {
                $join->on(DB::raw('sppt.thn_pajak_sppt'), '=', DB::raw('rpt_sspd_final.tahun'))
                    ->on(DB::raw('sppt.kd_propinsi'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 1, 2)"))
                    ->on(DB::raw('sppt.kd_dati2'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 3, 2)"))
                    ->on(DB::raw('sppt.kd_kecamatan'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 5, 3)"))
                    ->on(DB::raw('sppt.kd_kelurahan'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 8, 3)"))
                    ->on(DB::raw('sppt.kd_blok'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 11, 3)"))
                    ->on(DB::raw('sppt.no_urut'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 14, 4)"))
                    ->on(DB::raw('sppt.kd_jns_op'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 18, 1)"));
                // ->on(DB::raw('rpt_sspd_final.nop'), '=', DB::raw("sppt.kd_propinsi || sppt.kd_dati2 || sppt.kd_kecamatan || sppt.kd_kelurahan || sppt.kd_blok || sppt.no_urut || sppt.kd_jns_op"));
            })->join(
                DB::raw('bphtb_pembayaran'),
                function ($join) {
                    $join->on(db::raw("bphtb_pembayaran.id_billing"), '=', db::raw("rpt_sspd_final.id_billing"));
                }
            )
            ->whereraw("bphtb_pembayaran.id_billing is not null")->whereraw(DB::raw("SUBSTR (nop, 5, 3) ='$kd_kecamatan'"));
        return $res;
    }

    public static function ajuanNop($nop)
    {

        $res = DB::connection('bphtb')
            ->table(DB::raw("sppt"))
            ->select(DB::raw("rpt_sspd_final.NOP, nm_wp_sppt,TAHUN, JLN_WP_SPPT, BLOK_KAV_NO_WP_SPPT, RT_WP_SPPT, RW_WP_SPPT, KELURAHAN_WP_SPPT, KOTA_WP_SPPT, LUAS_BUMI_SPPT, LUAS_BNG_SPPT, NAMA_WP, ALAMAT_WP, RT_WP, RW_WP, NAMA_KEL, NAMA_KEC, NAMA_KAB, ALAMAT_OP, RT_OP, RW_OP, NAMA_KEL_OP, NAMA_KEC_OP, NAMA_KAB_OP, LUAS_TANAH, LUAS_BANGUNAN, tanggal TANGGAL_PERMOHONAN, bphtb_penelitian BPHTB"))
            ->join(DB::raw('rpt_sspd_final'), function ($join) {
                $join->on(DB::raw('sppt.thn_pajak_sppt'), '=', DB::raw('rpt_sspd_final.tahun'))
                    ->on(DB::raw('sppt.kd_propinsi'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 1, 2)"))
                    ->on(DB::raw('sppt.kd_dati2'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 3, 2)"))
                    ->on(DB::raw('sppt.kd_kecamatan'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 5, 3)"))
                    ->on(DB::raw('sppt.kd_kelurahan'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 8, 3)"))
                    ->on(DB::raw('sppt.kd_blok'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 11, 3)"))
                    ->on(DB::raw('sppt.no_urut'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 14, 4)"))
                    ->on(DB::raw('sppt.kd_jns_op'), '=', DB::raw("SUBSTR (rpt_sspd_final.nop, 18, 1)"));
                // ->on(DB::raw('rpt_sspd_final.nop'), '=', DB::raw("sppt.kd_propinsi || sppt.kd_dati2 || sppt.kd_kecamatan || sppt.kd_kelurahan || sppt.kd_blok || sppt.no_urut || sppt.kd_jns_op"));
            })->join(
                DB::raw('bphtb_pembayaran'),
                function ($join) {
                    $join->on(db::raw("bphtb_pembayaran.id_billing"), '=', db::raw("rpt_sspd_final.id_billing"));
                }
            )
            ->whereraw("rpt_sspd_final.nop = '$nop'");
        return $res;
    }
}
