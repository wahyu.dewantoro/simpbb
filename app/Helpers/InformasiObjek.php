<?php

namespace App\Helpers;

use App\Models\Layanan_objek;
use App\PembayaranMutasi;
use App\PembayaranMutasiData;
use App\Sppt;
use Illuminate\Support\Facades\DB;

class InformasiObjek
{
   public static function pembayaranSppt($nop)
   {
      $whereraw = "";
      $whereraw .= " sppt.kd_propinsi ='" . $nop['kd_propinsi'] . "' and ";
      $whereraw .= " sppt.kd_dati2 ='" . $nop['kd_dati2'] . "' and ";
      $whereraw .= " sppt.kd_kecamatan ='" . $nop['kd_kecamatan'] . "' and ";
      $whereraw .= " sppt.kd_kelurahan ='" . $nop['kd_kelurahan'] . "' and ";
      $whereraw .= " sppt.kd_blok ='" . $nop['kd_blok'] . "' and ";
      $whereraw .= " sppt.no_urut ='" . $nop['no_urut'] . "' and ";
      $whereraw .= " sppt.kd_jns_op ='" . $nop['kd_jns_op'] . "' ";



      // cek pelimpahan
      $last = " kd_propinsi ='" . $nop['kd_propinsi'] . "'  and ";
      $last .= " kd_dati2 ='" . $nop['kd_dati2'] . "'  and ";
      $last .= " kd_kecamatan ='" . $nop['kd_kecamatan'] . "'  and ";
      $last .= " kd_kelurahan ='" . $nop['kd_kelurahan'] . "'  and ";
      $last .= " kd_blok ='" . $nop['kd_blok'] . "'  and ";
      $last .= " no_urut ='" . $nop['no_urut'] . "'  and ";
      $last .= " kd_jns_op ='" . $nop['kd_jns_op'] . "'";




      $mutasi = PembayaranMutasiData::whereraw($last)->get();
      $wha = "";
      foreach ($mutasi as $row) {
         $wha .= "( sppt.kd_propinsi ='" . $row->kd_propinsi_asal . "' and ";
         $wha .= " sppt.kd_dati2 ='" . $row->kd_dati2_asal . "' and ";
         $wha .= " sppt.kd_kecamatan ='" . $row->kd_kecamatan_asal . "' and ";
         $wha .= " sppt.kd_kelurahan ='" . $row->kd_kelurahan_asal . "' and ";
         $wha .= " sppt.kd_blok ='" . $row->kd_blok_asal . "' and ";
         $wha .= " sppt.no_urut ='" . $row->no_urut_asal . "' and ";
         $wha .= " sppt.thn_pajak_sppt ='" . $row->thn_pajak_sppt . "' and ";
         $wha .= " sppt.kd_jns_op ='" . $row->kd_jns_op_asal . "' ) or";
      }

      if ($wha != '') {
         $wha = substr($wha, 0, -2);
      }

      $data = DB::connection("oracle_satutujuh")->table("sppt")
         ->leftjoin('sppt_potongan', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
               ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
               ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
         })
         // pengurang_piutang
         ->leftjoin(db::raw("pengurang_piutang sppt_koreksi"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
               ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
               ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            // ->whereraw("(sppt_koreksi.kd_kategori!='07' or sppt_koreksi.kd_kategori is null)");
         })
         ->leftjoin(
            db::raw("( SELECT a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.tahun_pajak thn_pajak_sppt, LISTAGG ( kobil, ', ') WITHIN GROUP (ORDER BY kobil) kobil
                                  FROM sim_pbb.billing_kolektif a
                                  JOIN sim_pbb.data_billing b ON a.data_billing_id=b.data_billing_id
                                  WHERE kd_status = '0' AND b.expired_at > SYSDATE and b.deleted_at is null
                              GROUP BY a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.tahun_pajak) sppt_penelitian"),
            function ($join) {
               $join->on('sppt.kd_propinsi', '=', 'sppt_penelitian.kd_propinsi')
                  ->on('sppt.kd_dati2', '=', 'sppt_penelitian.kd_dati2')
                  ->on('sppt.kd_kecamatan', '=', 'sppt_penelitian.kd_kecamatan')
                  ->on('sppt.kd_kelurahan', '=', 'sppt_penelitian.kd_kelurahan')
                  ->on('sppt.kd_blok', '=', 'sppt_penelitian.kd_blok')
                  ->on('sppt.no_urut', '=', 'sppt_penelitian.no_urut')
                  ->on('sppt.kd_jns_op', '=', 'sppt_penelitian.kd_jns_op')
                  ->on('sppt.thn_pajak_sppt', '=', 'sppt_penelitian.thn_pajak_sppt');
            }
         )
         ->leftjoin('pembayaran_sppt', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
               ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
               ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
         })
         ->whereraw($whereraw);

      if ($wha != "") {
         $data = $data->orWhereraw(db::raw("(" . $wha . ")"));
      }
      $data = $data->whereraw("sppt.thn_pajak_sppt>=2003");
      $data = $data->groupbyraw("sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.thn_pajak_sppt, nm_wp_sppt, sppt.pbb_yg_harus_dibayar_sppt, NVL (nilai_potongan, 0), sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt_koreksi.keterangan, sppt_koreksi.tgl_surat, SPPT_KOREKSI.NO_SURAT, CASE WHEN ( case when jns_koreksi='3' then '1' else status_pembayaran_sppt end  )!='1' THEN spo.get_denda ( pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt.tgl_jatuh_tempo_sppt, SYSDATE) ELSE NULL END,  case when jns_koreksi='3' then '1' else status_pembayaran_sppt end ,jns_koreksi, kobil,sppt_koreksi.no_transaksi,tgl_terbit_sppt,jns_koreksi")
         ->select(db::raw("sppt.kd_propinsi,
      sppt.kd_dati2,
      sppt.kd_kecamatan,
      sppt.kd_kelurahan,
      sppt.kd_blok,
      sppt.no_urut,
      sppt.kd_jns_op,
      sppt.thn_pajak_sppt,
      nm_wp_sppt,
      CASE WHEN jns_koreksi = '3' THEN '1' ELSE status_pembayaran_sppt END
         status_pembayaran_sppt,
      sppt.pbb_yg_harus_dibayar_sppt pbb,
      NVL (nilai_potongan, 0) potongan,
      sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb_akhir,
      CASE
         WHEN CASE
                 WHEN jns_koreksi = '3' THEN '1'
                 ELSE status_pembayaran_sppt
              END != '1'
         THEN
            spo.get_denda (
               pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0),
               sppt.tgl_jatuh_tempo_sppt,
               SYSDATE)
         ELSE
            NULL
      END
         denda,
      sppt_koreksi.keterangan,
      sppt_koreksi.tgl_surat,
      SPPT_KOREKSI.NO_SURAT,
      CASE WHEN jns_koreksi = '3' and  (select count(1) from pembayaran_sppt
        where  SPPT.KD_PROPINSI = PEMBAYARAN_SPPT.KD_PROPINSI
            AND SPPT.KD_DATI2 = PEMBAYARAN_SPPT.KD_DATI2
            AND SPPT.KD_KECAMATAN = PEMBAYARAN_SPPT.KD_KECAMATAN
            AND SPPT.KD_KELURAHAN = PEMBAYARAN_SPPT.KD_KELURAHAN
            AND SPPT.KD_BLOK = PEMBAYARAN_SPPT.KD_BLOK
            AND SPPT.NO_URUT = PEMBAYARAN_SPPT.NO_URUT
            AND SPPT.KD_JNS_OP = PEMBAYARAN_SPPT.KD_JNS_OP
            AND SPPT.THN_PAJAK_SPPT = PEMBAYARAN_SPPT.THN_PAJAK_SPPT
         ) =0 
      THEN 0 ELSE SUM (NVL (denda_sppt, 0)) END
         denda_bayar,
      CASE
         WHEN jns_koreksi = '3' and  (select count(1) from pembayaran_sppt
        where  SPPT.KD_PROPINSI = PEMBAYARAN_SPPT.KD_PROPINSI
            AND SPPT.KD_DATI2 = PEMBAYARAN_SPPT.KD_DATI2
            AND SPPT.KD_KECAMATAN = PEMBAYARAN_SPPT.KD_KECAMATAN
            AND SPPT.KD_KELURAHAN = PEMBAYARAN_SPPT.KD_KELURAHAN
            AND SPPT.KD_BLOK = PEMBAYARAN_SPPT.KD_BLOK
            AND SPPT.NO_URUT = PEMBAYARAN_SPPT.NO_URUT
            AND SPPT.KD_JNS_OP = PEMBAYARAN_SPPT.KD_JNS_OP
            AND SPPT.THN_PAJAK_SPPT = PEMBAYARAN_SPPT.THN_PAJAK_SPPT
         ) =0
         THEN
            sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
         ELSE
            SUM (jml_sppt_yg_dibayar)
      END
         jumlah_bayar,
      CASE
         WHEN jns_koreksi = '3' and  (select count(1) from pembayaran_sppt
        where  SPPT.KD_PROPINSI = PEMBAYARAN_SPPT.KD_PROPINSI
            AND SPPT.KD_DATI2 = PEMBAYARAN_SPPT.KD_DATI2
            AND SPPT.KD_KECAMATAN = PEMBAYARAN_SPPT.KD_KECAMATAN
            AND SPPT.KD_KELURAHAN = PEMBAYARAN_SPPT.KD_KELURAHAN
            AND SPPT.KD_BLOK = PEMBAYARAN_SPPT.KD_BLOK
            AND SPPT.NO_URUT = PEMBAYARAN_SPPT.NO_URUT
            AND SPPT.KD_JNS_OP = PEMBAYARAN_SPPT.KD_JNS_OP
            AND SPPT.THN_PAJAK_SPPT = PEMBAYARAN_SPPT.THN_PAJAK_SPPT
         ) =0
         THEN
            NULL
         ELSE
            LISTAGG (TO_CHAR (tgl_pembayaran_sppt, 'yyyy-mm-dd'), ',')
               WITHIN GROUP (ORDER BY tgl_pembayaran_sppt)
      END
         AS tgl_bayar,
      kobil,
      sppt_koreksi.no_transaksi no_koreksi,
      jns_koreksi,
      tgl_terbit_sppt"))
         /*  ->select(db::raw("sppt.kd_propinsi,
          sppt.kd_dati2,
          sppt.kd_kecamatan,
          sppt.kd_kelurahan,
          sppt.kd_blok,
          sppt.no_urut,
          sppt.kd_jns_op,
          sppt.thn_pajak_sppt,
          nm_wp_sppt,
          CASE WHEN jns_koreksi = '3' THEN '1' ELSE status_pembayaran_sppt END
             status_pembayaran_sppt,
          sppt.pbb_yg_harus_dibayar_sppt pbb,
          NVL (nilai_potongan, 0) potongan,
          sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb_akhir,
          CASE
             WHEN CASE
                     WHEN jns_koreksi = '3' THEN '1'
                     ELSE status_pembayaran_sppt
                  END != '1'
             THEN
                spo.get_denda (
                   pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0),
                   sppt.tgl_jatuh_tempo_sppt,
                   SYSDATE)
             ELSE
                NULL
          END
             denda,
          sppt_koreksi.keterangan,
          sppt_koreksi.tgl_surat,
          SPPT_KOREKSI.NO_SURAT,
          CASE WHEN jns_koreksi = '3' THEN 0 ELSE SUM (NVL (denda_sppt, 0)) END
             denda_bayar,
          CASE
             WHEN jns_koreksi = '3'
             THEN
                sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
             ELSE
                SUM (jml_sppt_yg_dibayar)
          END
             jumlah_bayar,
          CASE
             WHEN jns_koreksi = '3'
             THEN
                null
             ELSE
                LISTAGG (TO_CHAR (tgl_pembayaran_sppt, 'yyyy-mm-dd'), ',')
                   WITHIN GROUP (ORDER BY tgl_pembayaran_sppt)
          END
             AS tgl_bayar,
          kobil,
          sppt_koreksi.no_transaksi no_koreksi,jns_koreksi,
          tgl_terbit_sppt")) */
         ->orderby("sppt.thn_pajak_sppt", 'asc');

      // dd($data->tosql());
      return $data;
   }

   public static function lunasLimaTahun($param)
   {
      $tahun = date('Y');
      $sampai = $tahun - 1;
      $awal = $sampai - 5;

      /* $kd_propinsi = $param['kd_propinsi'];
        $kd_dati2 = $param['kd_dati2']; */
      $kd_kecamatan = $param['kd_kecamatan'];
      $kd_kelurahan = $param['kd_kelurahan'];
      $kd_blok = $param['kd_blok'];
      $no_urut = $param['no_urut'];
      $kd_jns_op = $param['kd_jns_op'];

      $sppt = Sppt::select(DB::raw("kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op, count(1) op,sum( case when   status_pembayaran_sppt=0 then 0 else 1 end) lunas"))
         ->whereraw(DB::raw("thn_pajak_sppt<=$sampai and thn_pajak_sppt>$awal and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'"))
         ->groupbyRaw("kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op")->first();
      return $sppt;
   }

   public static function riwayatPembayarandua($param)
   {
      $kd_propinsi = $param['kd_propinsi'];
      $kd_dati2 = $param['kd_dati2'];
      $kd_kecamatan = $param['kd_kecamatan'];
      $kd_kelurahan = $param['kd_kelurahan'];
      $kd_blok = $param['kd_blok'];
      $no_urut = $param['no_urut'];
      $kd_jns_op = $param['kd_jns_op'];
      // $tahun_hidden = DB::table(db::raw('data_billing a'))
      //           ->join(db::raw("billing_kolektif b"), 'a.data_billing_id', '=', 'b.data_billing_id')
      //           ->join(db::raw("(select distinct data_billing_id 
      //                           from sppt_penelitian
      //                           where kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut'  and kd_jns_op='$kd_jns_op' ) c"), 'c.data_billing_id', '=', 'b.data_billing_id')
      //           ->whereraw("a.kd_status='0'")->selectraw("distinct b.tahun_pajak")->pluck('tahun_pajak')->toArray();


      // DB::connection('oracle_spo')->statement("ALTER SESSION SET NLS_NUMERIC_CHARACTERS =',.' ");

      $riwayat = DB::connection('oracle_spo')->table(DB::raw("spo.sppt sppt"))->select(DB::raw("
         sppt.status_pembayaran_sppt   
      ,sppt.kd_propinsi,
        sppt.kd_dati2,
        sppt.kd_kecamatan,
        sppt.kd_kelurahan,
        sppt.kd_blok,
        sppt.no_urut,
        sppt.kd_jns_op,
        sppt.thn_pajak_sppt,
        sppt.luas_bumi_sppt,
         sppt.njop_bumi_sppt,
         sppt.luas_bng_sppt,
         sppt.njop_bng_sppt,
        sppt.nm_wp_sppt,
        TO_CHAR (pbb_yg_harus_dibayar_sppt, 'FM999G999G999G999') pbb,
        TO_CHAR (
           NVL (
              (SELECT jml_sppt_yg_dibayar - denda_sppt
                 FROM pbb.pembayaran_sppt b
                WHERE     b.kd_propinsi = sppt.kd_propinsi
                      AND b.kd_dati2 = sppt.kd_dati2
                      AND b.kd_kecamatan = sppt.kd_kecamatan
                      AND b.kd_kelurahan = sppt.kd_kelurahan
                      AND b.kd_blok = sppt.kd_blok
                      AND b.no_urut = sppt.no_urut
                      AND b.kd_jns_op = sppt.kd_jns_op
                      AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                      AND ROWNUM = 1),
              pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0)),
           'FM999G999G999G999')
           pokok_bayar,
        TO_CHAR (
           NVL (
              ( select * from (SELECT b.denda_sppt
                 FROM pbb.pembayaran_sppt b
                WHERE     b.kd_propinsi = sppt.kd_propinsi
                      AND b.kd_dati2 = sppt.kd_dati2
                      AND b.kd_kecamatan = sppt.kd_kecamatan
                      AND b.kd_kelurahan = sppt.kd_kelurahan
                      AND b.kd_blok = sppt.kd_blok
                      AND b.no_urut = sppt.no_urut
                      AND b.kd_jns_op = sppt.kd_jns_op
                      AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
                      ) where  ROWNUM = 1),
              sppt.pbb_yg_harus_dibayar_sppt,
                             sppt.tgl_jatuh_tempo_sppt,
                             SYSDATE)),
           'FM999G999G999G999')
           denda_bayar,
        TO_CHAR (
           NVL (
              (select * from (SELECT jml_sppt_yg_dibayar
                 FROM pbb.pembayaran_sppt b
                WHERE     b.kd_propinsi = sppt.kd_propinsi
                      AND b.kd_dati2 = sppt.kd_dati2
                      AND b.kd_kecamatan = sppt.kd_kecamatan
                      AND b.kd_kelurahan = sppt.kd_kelurahan
                      AND b.kd_blok = sppt.kd_blok
                      AND b.no_urut = sppt.no_urut
                      AND b.kd_jns_op = sppt.kd_jns_op
                      AND b.thn_pajak_sppt = sppt.thn_pajak_sppt
              )
                      where ROWNUM = 1),
              0),
           'FM999G999G999G999')
           total_bayar,
        (select * from ( 
        SELECT case when sppt.status_pembayaran_sppt=1 then  TRUNC (tgl_pembayaran_sppt)else null end
           FROM pbb.pembayaran_sppt b
          WHERE     b.kd_propinsi = sppt.kd_propinsi
                AND b.kd_dati2 = sppt.kd_dati2
                AND b.kd_kecamatan = sppt.kd_kecamatan
                AND b.kd_kelurahan = sppt.kd_kelurahan
                AND b.kd_blok = sppt.kd_blok
                AND b.no_urut = sppt.no_urut
                AND b.kd_jns_op = sppt.kd_jns_op
                AND b.thn_pajak_sppt = sppt.thn_pajak_sppt)
                where ROWNUM = 1)
           tgl_pembayaran_sppt,
        unflag_desc"))
         ->leftjoin(DB::raw("(select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,
         LISTAGG(unflag_desc, ', ') WITHIN GROUP (ORDER BY unflag_id) unflag_desc
         from pbb.unflag_history
         group by kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt) unflag_history"), function ($join) {
            $join->on('sppt.KD_PROPINSI', '=', 'unflag_history.KD_PROPINSI')
               ->on('sppt.KD_DATI2', '=', 'unflag_history.KD_DATI2')
               ->on('sppt.KD_KECAMATAN', '=', 'unflag_history.KD_KECAMATAN')
               ->on('sppt.KD_KELURAHAN', '=', 'unflag_history.KD_KELURAHAN')
               ->on('sppt.KD_BLOK', '=', 'unflag_history.KD_BLOK')
               ->on('sppt.NO_URUT', '=', 'unflag_history.NO_URUT')
               ->on('sppt.KD_JNS_OP', '=', 'unflag_history.KD_JNS_OP')
               ->on('SPPT.THN_PAJAK_SPPT', '=', 'unflag_history.thn_pajak_sppt');
         })
         ->leftjoin(DB::raw("sppt_potongan"), function ($join) {
            $join->on('sppt.KD_PROPINSI', '=', 'sppt_potongan.KD_PROPINSI')
               ->on('sppt.KD_DATI2', '=', 'sppt_potongan.KD_DATI2')
               ->on('sppt.KD_KECAMATAN', '=', 'sppt_potongan.KD_KECAMATAN')
               ->on('sppt.KD_KELURAHAN', '=', 'sppt_potongan.KD_KELURAHAN')
               ->on('sppt.KD_BLOK', '=', 'sppt_potongan.KD_BLOK')
               ->on('sppt.NO_URUT', '=', 'sppt_potongan.NO_URUT')
               ->on('sppt.KD_JNS_OP', '=', 'sppt_potongan.KD_JNS_OP')
               ->on('SPPT.THN_PAJAK_SPPT', '=', 'sppt_potongan.thn_pajak_sppt');
         })
         ->whereraw("sppt.KD_PROPINSI = '$kd_propinsi'
            AND sppt.KD_DATI2 = '$kd_dati2'
            AND sppt.KD_KECAMATAN = '$kd_kecamatan'
            AND sppt.KD_KELURAHAN = '$kd_kelurahan'
            AND sppt.KD_BLOK = '$kd_blok'
            AND sppt.NO_URUT = '$no_urut'
            AND sppt.KD_JNS_OP = '$kd_jns_op'");

      $riwayat = $riwayat->whereraw("sppt.thn_pajak_sppt>=2014");



      return $riwayat;
   }

   public static function riwayatPembayaranOther($param)
   {
      $kd_propinsi = $param['kd_propinsi'];
      $kd_dati2 = $param['kd_dati2'];
      $kd_kecamatan = $param['kd_kecamatan'];
      $kd_kelurahan = $param['kd_kelurahan'];
      $kd_blok = $param['kd_blok'];
      $no_urut = $param['no_urut'];
      $kd_jns_op = $param['kd_jns_op'];
      $where = "WHERE sppt.KD_PROPINSI = '$kd_propinsi'
      AND sppt.KD_DATI2 = '$kd_dati2'
      AND sppt.KD_KECAMATAN = '$kd_kecamatan'
      AND sppt.KD_KELURAHAN = '$kd_kelurahan'
      AND sppt.KD_BLOK = '$kd_blok'
      AND sppt.NO_URUT = '$no_urut'
      AND sppt.KD_JNS_OP = '$kd_jns_op'
      AND sppt.status_pembayaran_sppt='1'";
      $sql = "Select sppt.kd_propinsi,
            sppt.kd_dati2,
            sppt.kd_kecamatan,
            sppt.kd_kelurahan,
            sppt.kd_blok,
            sppt.no_urut,
            sppt.kd_jns_op,
            sppt.thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt-nvl(nilai_potongan, 0) pbb,
            LISTAGG(TO_CHAR(tgl_pembayaran_sppt, 'DD Month YYYY', 'nls_date_language = INDONESIAN'), ',') WITHIN GROUP (
            ORDER BY tgl_pembayaran_sppt) AS tanggal_bayar,
            LISTAGG(tgl_pembayaran_sppt, ',') WITHIN GROUP (
               ORDER BY tgl_pembayaran_sppt) AS tanggal_bayar_real,
            sum(jml_sppt_yg_dibayar-denda_sppt) total_bayar
         FROM
            sppt
         LEFT JOIN sppt_potongan 
            ON sppt.kd_propinsi = sppt_potongan.kd_propinsi
            AND sppt.kd_dati2 = sppt_potongan.kd_dati2
            AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
            AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
            AND sppt.kd_blok = sppt_potongan.kd_blok
            AND sppt.no_urut = sppt_potongan.no_urut
            AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
            AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
         JOIN pembayaran_sppt ON
            sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
            AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
            AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
            AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
            AND sppt.kd_blok = pembayaran_sppt.kd_blok
            AND sppt.no_urut = pembayaran_sppt.no_urut
            AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
            AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
         " . $where . "
         GROUP BY
            sppt.kd_propinsi,
            sppt.kd_dati2,
            sppt.kd_kecamatan,
            sppt.kd_kelurahan,
            sppt.kd_blok,
            sppt.no_urut,
            sppt.kd_jns_op,
            sppt.thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt-nvl(nilai_potongan, 0)
         ORDER BY
            sppt.thn_pajak_sppt asc";

      // HAVING
      // pbb_yg_harus_dibayar_sppt-nvl(nilai_potongan, 0) < sum(jml_sppt_yg_dibayar-denda_sppt)
      return DB::connection('oracle_satutujuh')->select($sql);
   }

   public static function riwayatPembayaran($param)
   {
      $kd_propinsi = $param['kd_propinsi'];
      $kd_dati2 = $param['kd_dati2'];
      $kd_kecamatan = $param['kd_kecamatan'];
      $kd_kelurahan = $param['kd_kelurahan'];
      $kd_blok = $param['kd_blok'];
      $no_urut = $param['no_urut'];
      $kd_jns_op = $param['kd_jns_op'];
      /* $tahun_hidden = DB::table(db::raw('data_billing a'))
                ->join(db::raw("billing_kolektif b"), 'a.data_billing_id', '=', 'b.data_billing_id')
                ->join(db::raw("(select distinct data_billing_id 
                                from sppt_penelitian
                                where kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut'  and kd_jns_op='$kd_jns_op' ) c"), 'c.data_billing_id', '=', 'b.data_billing_id')
                ->whereraw("a.kd_status='0'")->selectraw("distinct b.tahun_pajak")->pluck('tahun_pajak')->toArray(); */


      // DB::connection('oracle_satutujuh')->statement("ALTER SESSION SET NLS_NUMERIC_CHARACTERS =',.' ");

      $riwayat = DB::connection('oracle_satutujuh')->table(DB::raw("sppt"))->select(DB::raw("sppt.status_pembayaran_sppt,
      sppt.kd_propinsi,
      sppt.kd_dati2,
      sppt.kd_kecamatan,
      sppt.kd_kelurahan,
      ref_kecamatan.nm_kecamatan,
      ref_kelurahan.nm_kelurahan,
      sppt.kd_blok,
      sppt.no_urut,
      sppt.kd_jns_op,
      sppt.thn_pajak_sppt,
      sppt.luas_bumi_sppt,
      sppt.njop_bumi_sppt,
      sppt.luas_bng_sppt,
      sppt.njop_bng_sppt,
      sppt.nm_wp_sppt,
      TO_CHAR (pbb_yg_harus_dibayar_sppt, 'FM999G999G999G999') pbb,
      TO_CHAR (NVL (pokok_bayar (1,
                                 sppt.kd_propinsi,
                                 sppt.kd_dati2,
                                 sppt.kd_kecamatan,
                                 sppt.kd_kelurahan,
                                 sppt.kd_blok,
                                 sppt.no_urut,
                                 sppt.kd_jns_op,
                                 sppt.thn_pajak_sppt),
                    pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)),
               'FM999G999G999G999')
         pokok_bayar,
      TO_CHAR (
         NVL (
            pokok_bayar (2,
                         sppt.kd_propinsi,
                         sppt.kd_dati2,
                         sppt.kd_kecamatan,
                         sppt.kd_kelurahan,
                         sppt.kd_blok,
                         sppt.no_urut,
                         sppt.kd_jns_op,
                         sppt.thn_pajak_sppt),
            spo.get_denda (
               pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0),
               sppt.tgl_jatuh_tempo_sppt,
               SYSDATE)),
         'FM999G999G999G999')
         denda_bayar,
      TO_CHAR (NVL (pokok_bayar (3,
                                 sppt.kd_propinsi,
                                 sppt.kd_dati2,
                                 sppt.kd_kecamatan,
                                 sppt.kd_kelurahan,
                                 sppt.kd_blok,
                                 sppt.no_urut,
                                 sppt.kd_jns_op,
                                 sppt.thn_pajak_sppt),
                    0),
               'FM999G999G999G999')
         total_bayar,
      (SELECT 
                            max (tgl_pembayaran_sppt)
                         tanggal
                 FROM pbb.pembayaran_sppt b
                WHERE     b.kd_propinsi = sppt.kd_propinsi
                      AND b.kd_dati2 = sppt.kd_dati2
                      AND b.kd_kecamatan = sppt.kd_kecamatan
                      AND b.kd_kelurahan = sppt.kd_kelurahan
                      AND b.kd_blok = sppt.kd_blok
                      AND b.no_urut = sppt.no_urut
                      AND b.kd_jns_op = sppt.kd_jns_op
                      AND b.thn_pajak_sppt = sppt.thn_pajak_sppt)
         tgl_pembayaran_sppt,
      unflag_desc,
      case when status_pembayaran_sppt=2 then 
            (select pbb_yg_harus_dibayar_sppt 
from spo.sppt_oltp
 where thn_pajak_sppt=sppt.thn_pajak_sppt
and kd_kecamatan=sppt.kd_kecamatan and kd_Kelurahan=sppt.kd_kelurahan
and kd_blok=sppt.kd_blok and no_urut=sppt.no_urut)
        else null end 
         kurang_bayar,
      kobil"))
         ->leftjoin(DB::raw("(select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,
         LISTAGG(unflag_desc, ', ') WITHIN GROUP (ORDER BY unflag_id) unflag_desc
         from pbb.unflag_history
         group by kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt) unflag_history"), function ($join) {
            $join->on('sppt.KD_PROPINSI', '=', 'unflag_history.KD_PROPINSI')
               ->on('sppt.KD_DATI2', '=', 'unflag_history.KD_DATI2')
               ->on('sppt.KD_KECAMATAN', '=', 'unflag_history.KD_KECAMATAN')
               ->on('sppt.KD_KELURAHAN', '=', 'unflag_history.KD_KELURAHAN')
               ->on('sppt.KD_BLOK', '=', 'unflag_history.KD_BLOK')
               ->on('sppt.NO_URUT', '=', 'unflag_history.NO_URUT')
               ->on('sppt.KD_JNS_OP', '=', 'unflag_history.KD_JNS_OP')
               ->on('SPPT.THN_PAJAK_SPPT', '=', 'unflag_history.thn_pajak_sppt');
         })
         ->leftjoin(DB::raw("sppt_potongan sppt_potongan"), function ($join) {
            $join->on('sppt.KD_PROPINSI', '=', 'sppt_potongan.KD_PROPINSI')
               ->on('sppt.KD_DATI2', '=', 'sppt_potongan.KD_DATI2')
               ->on('sppt.KD_KECAMATAN', '=', 'sppt_potongan.KD_KECAMATAN')
               ->on('sppt.KD_KELURAHAN', '=', 'sppt_potongan.KD_KELURAHAN')
               ->on('sppt.KD_BLOK', '=', 'sppt_potongan.KD_BLOK')
               ->on('sppt.NO_URUT', '=', 'sppt_potongan.NO_URUT')
               ->on('sppt.KD_JNS_OP', '=', 'sppt_potongan.KD_JNS_OP')
               ->on('SPPT.THN_PAJAK_SPPT', '=', 'sppt_potongan.thn_pajak_sppt');
         })
         ->leftjoin(DB::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
         ->leftjoin(DB::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
            $join->on('ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
               ->on('ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan');
         })
         ->leftjoin(DB::raw("(select 
         a.kd_propinsi,
         a.kd_dati2,
         a.kd_kecamatan,
         a.kd_kelurahan,
         a.kd_blok,
         a.no_urut,
         a.kd_jns_op,
         a.tahun_pajak thn_pajak_sppt,
         LISTAGG (kobil, ', ') WITHIN GROUP (ORDER BY kobil) kobil
         from sim_pbb.billing_kolektif a 
         join sim_pbb.data_billing  b on a.data_billing_id=b.data_billing_id
         where kd_status='0' and b.expired_at>sysdate
         group by a.kd_propinsi,
         a.kd_dati2,
         a.kd_kecamatan,
         a.kd_kelurahan,
         a.kd_blok,
         a.no_urut,
         a.kd_jns_op,
         a.tahun_pajak) sppt_penelitian"), function ($join) {
            $join->on('sppt.KD_PROPINSI', '=', 'sppt_penelitian.KD_PROPINSI')
               ->on('sppt.KD_DATI2', '=', 'sppt_penelitian.KD_DATI2')
               ->on('sppt.KD_KECAMATAN', '=', 'sppt_penelitian.KD_KECAMATAN')
               ->on('sppt.KD_KELURAHAN', '=', 'sppt_penelitian.KD_KELURAHAN')
               ->on('sppt.KD_BLOK', '=', 'sppt_penelitian.KD_BLOK')
               ->on('sppt.NO_URUT', '=', 'sppt_penelitian.NO_URUT')
               ->on('sppt.KD_JNS_OP', '=', 'sppt_penelitian.KD_JNS_OP')
               ->on('SPPT.THN_PAJAK_SPPT', '=', 'sppt_penelitian.thn_pajak_sppt');
         })


         ->whereraw("sppt.KD_PROPINSI = '$kd_propinsi'
            AND sppt.KD_DATI2 = '$kd_dati2'
            AND sppt.KD_KECAMATAN = '$kd_kecamatan'
            AND sppt.KD_KELURAHAN = '$kd_kelurahan'
            AND sppt.KD_BLOK = '$kd_blok'
            AND sppt.NO_URUT = '$no_urut'
            AND sppt.KD_JNS_OP = '$kd_jns_op'");
      if (in_array('thn_pajak_sppt', array_keys($param))) {
         $riwayat = $riwayat->whereraw("sppt.thn_pajak_sppt=" . $param['thn_pajak_sppt']);
      }
      $tahun = '2014';
      if (in_array('tahun_range', array_keys($param))) {
         $tahun = $param['tahun_range'];
      }

      $riwayat = $riwayat->whereraw("sppt.thn_pajak_sppt>=" . $tahun);



      return $riwayat;
   }

   public static function ObjekMutasiPecah($kd_kecamatan, $kd_kelurahan)
   {
      return DB::connection("oracle_satutujuh")->select(DB::raw("SELECT dat_subjek_pajak.subjek_pajak_id nik,
                           TRIM (dat_subjek_pajak.jalan_wp)
                        || ' '
                        || dat_subjek_pajak.blok_kav_no_wp
                           alamat_wp,
                        dat_subjek_pajak.rw_wp,
                        dat_subjek_pajak.rt_wp,
                           dat_objek_pajak.kd_propinsi
                        || '.'
                        || dat_objek_pajak.kd_dati2
                        || '.'
                        || dat_objek_pajak.kd_kecamatan
                        || '.'
                        || dat_objek_pajak.kd_kelurahan
                        || '.'
                        || dat_objek_pajak.kd_blok
                        || '-'
                        || dat_objek_pajak.no_urut
                        || '.'
                        || dat_objek_pajak.kd_jns_op
                           nop_format,
                     nop_proses nop_asli,
                        nop_asal,
                     jenis
                           keterangan,
                        dat_objek_pajak.kd_propinsi,
                        dat_objek_pajak.kd_dati2,
                        dat_objek_pajak.kd_kecamatan,
                        dat_objek_pajak.kd_kelurahan,
                        dat_objek_pajak.kd_blok,
                        dat_objek_pajak.no_urut,
                        dat_objek_pajak.kd_jns_op,
                        dat_subjek_pajak.nm_wp,
                           dat_objek_pajak.jalan_op
                        || ' '
                        || dat_objek_pajak.blok_kav_no_op
                           alamat_op,
                        dat_objek_pajak.rw_op,
                        dat_objek_pajak.rt_op,
                        (SELECT nm_kecamatan
                           FROM ref_kecamatan
                        WHERE kd_kecamatan = dat_objek_pajak.kd_kecamatan)
                           kecamatan_op,
                        (SELECT nm_kelurahan
                           FROM ref_kelurahan
                        WHERE     kd_kecamatan =
                                       dat_objek_pajak.kd_kecamatan
                              AND kd_kelurahan =
                                       dat_objek_pajak.kd_kelurahan)
                           kelurahan_op,
                        total_luas_bumi,
                        total_luas_bng,
                        njop_bumi,
                        njop_bng,
                        CASE
                           WHEN dat_op_bumi.jns_bumi = '1'
                           THEN
                              'TANAH + BANGUNAN'
                           WHEN dat_op_bumi.jns_bumi = '2'
                           THEN
                              'KAVLING'
                           WHEN dat_op_bumi.jns_bumi = '3'
                           THEN
                              'TANAH KOSONG'
                           WHEN dat_op_bumi.jns_bumi = '4'
                           THEN
                              'FASILITAS UMUM'
                           WHEN dat_op_bumi.jns_bumi = '5'
                           THEN
                              'NON AKTIF'
                           ELSE
                              'LAIN - LAIN'
                        END
                           status_peta,tgl_perekaman_op
                     from dat_objek_pajak 
                     join v_core_pecahan vcp on vcp.no_formulir=dat_objek_pajak.no_formulir_spop
                     JOIN dat_subjek_pajak
                           ON dat_objek_pajak.subjek_pajak_id =
                                 dat_subjek_pajak.subjek_pajak_id
                        JOIN dat_op_bumi
                           ON     dat_op_bumi.kd_propinsi =
                                    dat_objek_pajak.kd_propinsi
                              AND dat_op_bumi.kd_dati2 =
                                    dat_objek_pajak.kd_dati2
                              AND dat_op_bumi.kd_kecamatan =
                                    dat_objek_pajak.kd_kecamatan
                              AND dat_op_bumi.kd_kelurahan =
                                    dat_objek_pajak.kd_kelurahan
                              AND dat_op_bumi.kd_blok =
                                    dat_objek_pajak.kd_blok
                              AND dat_op_bumi.no_urut =
                                    dat_objek_pajak.no_urut
                              AND dat_op_bumi.kd_jns_op =
                                    dat_objek_pajak.kd_jns_op
                                    where dat_objek_pajak.kd_kecamatan='$kd_kecamatan'
                                    and dat_objek_pajak.kd_kelurahan='$kd_kelurahan'
                                    START WITH nop_asal is null
                     CONNECT BY PRIOR nop_proses = nop_asal"));
   }

   public static function dataDanInformasi($nop)
   {

      $bangunan = [];
      $sppt = [];
      $show_sppt = [];
      $tahun_nota = [];
      $pemutakhiran = [];
      $pecah = [];
      $gabung = [];
      $gabungke = [];
      $tagihan = [];
      $maxsppt = '';

      $rescode = 400;
      $objek = Pajak::coreInfoObjek($nop);

      if ($objek != null) {
         $rescode = 200;
         $res = onlyNumber($nop);
         $kd_propinsi = substr($res, 0, 2);
         $kd_dati2 = substr($res, 2, 2);
         $kd_kecamatan = substr($res, 4, 3);
         $kd_kelurahan = substr($res, 7, 3);
         $kd_blok = substr($res, 10, 3);
         $no_urut = substr($res, 13, 4);
         $kd_jns_op = substr($res, 17, 1);

         $bangunan = DB::connection("oracle_satutujuh")->table(db::raw(' dat_op_bangunan'))
            ->select(db::raw("no_formulir_lspop no_formulir, jns_transaksi_bng jns_transaksi, tbl_lspop.nop,dat_op_bangunan.no_bng, dat_op_bangunan.kd_jpb, dat_op_bangunan.thn_dibangun_bng, dat_op_bangunan.thn_renovasi_bng, dat_op_bangunan.luas_bng, dat_op_bangunan.jml_lantai_bng, dat_op_bangunan.kondisi_bng, dat_op_bangunan.jns_konstruksi_bng, dat_op_bangunan.jns_atap_bng, dat_op_bangunan.kd_dinding, dat_op_bangunan.kd_lantai, dat_op_bangunan.kd_langit_langit, tbl_lspop.daya_listrik, tbl_lspop.acsplit, tbl_lspop.acwindow, tbl_lspop.acsentral, tbl_lspop.luas_kolam, tbl_lspop.finishing_kolam, tbl_lspop.luas_perkerasan_ringan, tbl_lspop.luas_perkerasan_sedang, tbl_lspop.luas_perkerasan_berat, tbl_lspop.luas_perkerasan_dg_tutup, tbl_lspop.lap_tenis_lampu_beton, tbl_lspop.lap_tenis_lampu_aspal, tbl_lspop.lap_tenis_lampu_rumput, tbl_lspop.lap_tenis_beton, tbl_lspop.lap_tenis_aspal, tbl_lspop.lap_tenis_rumput, tbl_lspop.lift_penumpang, tbl_lspop.lift_kapsul, tbl_lspop.lift_barang, tbl_lspop.tgg_berjalan_a, tbl_lspop.tgg_berjalan_b, tbl_lspop.pjg_pagar, tbl_lspop.bhn_pagar, tbl_lspop.hydrant, tbl_lspop.sprinkler, tbl_lspop.fire_alarm, tbl_lspop.jml_pabx, tbl_lspop.sumur_artesis, tbl_lspop.nilai_individu, tbl_lspop.jpb3_8_tinggi_kolom, tbl_lspop.jpb3_8_lebar_bentang, tbl_lspop.jpb3_8_dd_lantai, tbl_lspop.jpb3_8_kel_dinding, tbl_lspop.jpb3_8_mezzanine, tbl_lspop.jpb5_kls_bng, tbl_lspop.jpb5_luas_kamar, tbl_lspop.jpb5_luas_rng_lain, tbl_lspop.jpb7_jns_hotel, tbl_lspop.jpb7_bintang, tbl_lspop.jpb7_jml_kamar, tbl_lspop.jpb7_luas_kamar, tbl_lspop.jpb7_luas_rng_lain, tbl_lspop.jpb13_kls_bng, tbl_lspop.jpb13_jml, tbl_lspop.jpb13_luas_kamar, tbl_lspop.jpb13_luas_rng_lain, tbl_lspop.jpb15_letak_tangki, tbl_lspop.jpb15_kapasitas_tangki, tbl_lspop.jpb_lain_kls_bng, to_char(dat_op_bangunan.tgl_pendataan_bng,'yyyy-mm-dd') tgl_pendataan_bng, dat_op_bangunan.nip_pendata_bng, to_char(dat_op_bangunan.tgl_pemeriksaan_bng,'yyyy-mm-dd') tgl_pemeriksaan_bng, dat_op_bangunan.nip_pemeriksa_bng, to_char(dat_op_bangunan.tgl_perekaman_bng,'yyyy-mm-dd') tgl_perekaman_bng, dat_op_bangunan.nip_perekam_bng, tbl_lspop.rec_info"))
            // mv_tbl_lspop
            ->leftjoin(db::raw("tbl_lspop"), "tbl_lspop.no_formulir", "=", "dat_op_bangunan.no_formulir_lspop")
            ->whereraw("kd_propinsi ='" . $kd_propinsi . "'")
            ->whereraw("kd_dati2 ='" . $kd_dati2 . "'")
            ->whereraw("kd_kecamatan ='" . $kd_kecamatan . "'")
            ->whereraw("kd_kelurahan ='" . $kd_kelurahan . "'")
            ->whereraw("kd_blok ='" . $kd_blok . "'")
            ->whereraw("no_urut ='" . $no_urut . "'")
            ->whereraw("kd_jns_op ='" . $kd_jns_op . "' and jns_transaksi_bng <>'3'")
            ->get();
         $sppt = DB::connection('oracle_satutujuh')->table('sppt')
            ->leftjoin('pembayaran_sppt', function ($join) {
               $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
                  ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
                  ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
                  ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
                  ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
                  ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
                  ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
                  ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
            })
            ->leftjoin('sppt_potongan', function ($join) {
               $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                  ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                  ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                  ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                  ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                  ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                  ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                  ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->selectraw("sppt.kd_propinsi,sppt.kd_dati2,sppt.kd_kecamatan,sppt.kd_kelurahan,sppt.kd_blok,sppt.no_urut,sppt.kd_jns_op,sppt.thn_pajak_sppt,sppt.nm_wp_sppt, sppt.luas_bumi_sppt, sppt.njop_bumi_sppt, sppt.luas_bng_sppt, sppt.njop_bng_sppt, sppt.pbb_yg_harus_dibayar_sppt pbb,LISTAGG (TO_CHAR (tgl_pembayaran_sppt, 'yyyy-mm-dd'), ',') WITHIN GROUP (ORDER BY tgl_pembayaran_sppt) AS tgl_bayar,nvl(nilai_potongan,0) insentif ")
            ->groupByRaw("sppt.kd_propinsi,sppt.kd_dati2,sppt.kd_kecamatan,sppt.kd_kelurahan,sppt.kd_blok,sppt.no_urut,sppt.kd_jns_op,sppt.thn_pajak_sppt,sppt.nm_wp_sppt, sppt.luas_bumi_sppt, sppt.njop_bumi_sppt, sppt.luas_bng_sppt, sppt.njop_bng_sppt, sppt.pbb_yg_harus_dibayar_sppt,nvl(nilai_potongan,0)")
            ->whereraw("sppt.kd_propinsi ='" . $kd_propinsi . "'")
            ->whereraw("sppt.kd_dati2 ='" . $kd_dati2 . "'")
            ->whereraw("sppt.kd_kecamatan ='" . $kd_kecamatan . "'")
            ->whereraw("sppt.kd_kelurahan ='" . $kd_kelurahan . "'")
            ->whereraw("sppt.kd_blok ='" . $kd_blok . "'")
            ->whereraw("sppt.no_urut ='" . $no_urut . "'")
            ->whereraw("sppt.kd_jns_op ='" . $kd_jns_op . "' and  sppt.thn_pajak_sppt>=2003")
            ->orderby('sppt.THN_PAJAK_SPPT')->get();
         $show_sppt = true;

         if (($objek->jns_transaksi_op ?? '') == '3' || ($objek->jns_bumi ?? '') == '4' || ($objek->jns_bumi ?? '') == '5') {
            $add_desc = [
               'status' => 'Tidak Aktif',
               'keterangan' => ''
            ];
            $show_sppt = false;
         }
         $tahun_nota = [];

         // pemutakhiran
         $pemutakhiran = DB::table(db::raw("(
              select case when layanan.jenis_layanan_id=7 then   nvl(nop_gabung,to_char(layanan_objek.id)) else to_char(layanan_objek.id) end id , nama_layanan,trunc(tbl_spop.created_at) created_at,users.nama created_by,'PERMOHONAN' jenis
              from tbl_spop tbl_spop
              join users on users.id=tbl_spop.created_by
              join layanan_objek on layanan_objek.nomor_formulir=tbl_spop.no_formulir
              join layanan on layanan.nomor_layanan=layanan_objek.nomor_layanan
              join jenis_layanan on jenis_layanan.id=layanan.jenis_layanan_id
              where nop_proses='$res'
              union 
              select to_char(pendataan_objek.id) id,nama_pendataan nama_layanan,trunc(tbl_spop.created_at) created_at,nama created_by,'PENDATAAN' jenis
              from tbl_spop tbl_spop
              join pendataan_objek on pendataan_objek.id=tbl_spop.pendataan_objek_id
              join pendataan_batch on pendataan_batch.id=pendataan_objek.pendataan_batch_id
              join jenis_pendataan on jenis_pendataan.id=pendataan_batch.jenis_pendataan_id
              join users on users.id=tbl_spop.created_by
              where nop_proses='$res'
              union 
              select to_char(perubahan_znt.id) id,'Perubahan ZNT' nama_layanan,perubahan_znt.created_at, nama created_by,'Dari '||kd_znt_lama||' ke '||kd_znt_baru jenis
              from perubahan_znt 
              join perubahan_znt_objek on perubahan_znt.id=PERUBAHAN_ZNT_OBJEK.PERUBAHAN_ZNT_ID
              join users on users.id=perubahan_znt.created_by
              where perubahan_znt.verifikasi_kode='1' and
              perubahan_znt_objek.kd_propinsi='$kd_propinsi' and 
              perubahan_znt_objek.kd_dati2='$kd_dati2' and 
              perubahan_znt_objek.kd_kecamatan='$kd_kecamatan' and 
              perubahan_znt_objek.kd_kelurahan='$kd_kelurahan' and 
              perubahan_znt_objek.kd_blok='$kd_blok' and 
              perubahan_znt_objek.no_urut='$no_urut' and 
              perubahan_znt_objek.kd_jns_op='$kd_jns_op'
              )"))
            ->orderby('created_at', 'desc')
            ->get();

         // pecahan ke objek mana saja
         $pecah = DB::connection("oracle_satutujuh")->table('tbl_spop')
            ->join('dat_objek_pajak', 'dat_objek_pajak.no_formulir_spop', '=', 'tbl_spop.no_formulir')
            ->select('nop_proses', 'nop_asal', db::raw("to_char(tbl_spop.tgl_pendataan_op,'yyyy') tahun"))
            ->whereraw("nop_asal='$res' and nop_asal<>nop_proses")->get();
         $gabung = DB::select(db::raw("select nop_proses,   nop_asal, tahun
               from  (select max(id)  id,max(to_char(created_at,'yyyy')) tahun,kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop_proses
               from sim_pbb.history_mutasi_gabung
               where kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' 
               and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and  no_urut='$no_urut' and kd_jns_op='$kd_jns_op'
               group by kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op
               ) a
               join (select kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op  nop_asal,HISTORY_MUTASI_GABUNG_ID from sim_pbb.history_mutasi_gabung_detail) b on a.id=B.HISTORY_MUTASI_GABUNG_ID"));

         $gabungke = DB::table(db::raw("(select nop_proses,   nop_asal, tahun
                                  from  (select max(id)  id,max(to_char(created_at,'yyyy')) tahun,kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop_proses
                                  from sim_pbb.history_mutasi_gabung
                                  group by kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op
                                  ) a
                                  join (select kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op  nop_asal,HISTORY_MUTASI_GABUNG_ID from sim_pbb.history_mutasi_gabung_detail
                                  where kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' 
                                  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and  no_urut='$no_urut' and kd_jns_op='$kd_jns_op'
                                  ) b on a.id=B.HISTORY_MUTASI_GABUNG_ID)"))->first();


         $tagihan_nop = DB::connection("oracle_spo")->table('sppt_oltp')
            ->whereraw("kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' 
                                  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' 
                                  and  no_urut='$no_urut' and kd_jns_op='$kd_jns_op' and status_pembayaran_sppt<>'3'")
            ->orderBy("thn_pajak_sppt", "desc")
            ->selectraw("thn_pajak_sppt tahun,pbb_yg_harus_dibayar_sppt pokok,denda,pbb_yg_harus_dibayar_sppt + denda total,data_billing_id,status_pembayaran_sppt")->get();


         // tagihan nop_asal atas penelitian

         $cek_tagihan_asal = DB::table(DB::raw("data_billing a"))
            ->join(
               DB::raw('(SELECT layanan_objek_id, MAX(data_billing_id) as data_billing_id FROM nota_perhitungan GROUP BY layanan_objek_id)  b'),
               'a.data_billing_id',
               '=',
               'b.data_billing_id'
            )
            ->join('layanan_objek as c', 'c.id', '=', 'b.layanan_objek_id')
            ->join('layanan as d', 'd.nomor_layanan', '=', 'c.nomor_layanan')
            ->join(DB::raw("(select layanan_objek_id,max(nop_proses) nop_proses
                     from tbl_spop
                     group by layanan_objek_id) e"), 'e.layanan_objek_id', '=', 'b.layanan_objek_id')
            ->whereNull('a.tgl_bayar')
            ->where('a.expired_at', '<', DB::raw('SYSDATE')) // Jika bukan Oracle, ganti dengan NOW() atau CURRENT_TIMESTAMP
            ->whereRaw("TO_CHAR(a.created_at, 'yyyy') >= '2024'
               and nop_proses='$res'")
            ->orderByRaw('d.nomor_layanan asc ,b.layanan_objek_id asc')
            ->select([
               'b.layanan_objek_id',
               'd.jenis_layanan_id',
               'd.jenis_layanan_nama',
               DB::raw("nvl(c.nop_gabung,c.id) layanan_objek_parent"),
               DB::raw("to_char (a.created_at, 'yyyy') tahun_penelitian"),
               'nop_proses'
            ])
            ->first();

         $ArTagihan = [];
         if ($cek_tagihan_asal) {
            $layanan_objek_id = $cek_tagihan_asal->layanan_objek_id;
            $tahun = $cek_tagihan_asal->tahun_penelitian;

            $np = preg_replace('/[^0-9]/', '', $cek_tagihan_asal->nop_proses);
            $nop_proses = [
               'kd_propinsi' => substr($np, 0, 2),
               'kd_dati2' => substr($np, 2, 2),
               'kd_kecamatan' => substr($np, 4, 3),
               'kd_kelurahan' => substr($np, 7, 3),
               'kd_blok' => substr($np, 10, 3),
               'no_urut' => substr($np, 13, 4),
               'kd_jns_op' => substr($np, 17, 1)
            ];

            $jenis_layanan_id = $cek_tagihan_asal->jenis_layanan_id;

            // Ambil data layanan objek berdasarkan kondisi jenis_layanan_id
            $layananObjek = ($jenis_layanan_id != 7)
               ? Layanan_objek::where('id', $cek_tagihan_asal->layanan_objek_parent)->get()
               : Layanan_objek::where('nop_gabung', $cek_tagihan_asal->layanan_objek_parent)->get();

            // Format data menggunakan map untuk lebih ringkas
            $nop_asal = $layananObjek->map(function ($lo) {
               return [
                  'sppt_oltp.kd_propinsi' => $lo->kd_propinsi,
                  'sppt_oltp.kd_dati2' => $lo->kd_dati2,
                  'sppt_oltp.kd_kecamatan' => $lo->kd_kecamatan,
                  'sppt_oltp.kd_kelurahan' => $lo->kd_kelurahan,
                  'sppt_oltp.kd_blok' => $lo->kd_blok,
                  'sppt_oltp.no_urut' => $lo->no_urut,
                  'sppt_oltp.kd_jns_op' => $lo->kd_jns_op,
               ];
            });


            // mencari komposisi beban
            $persen = 100;
            if ($jenis_layanan_id == '6') {
               // mencari total luas induk, dari data pecahan
               $subQuery1 = DB::table('layanan_objek')
                  ->selectRaw('NVL(nop_gabung, id)')
                  ->where('id', $layanan_objek_id);

               $subQuery2 = DB::table('layanan_objek')
                  ->selectRaw('NVL(nop_gabung, id)')
                  ->where('id', $layanan_objek_id);

               $firstQuery = DB::table('layanan_objek as lo')
                  ->select([
                     'lo.id',
                     DB::raw("CASE 
                                        WHEN lo.nop_gabung IS NULL 
                                        THEN CASE 
                                                WHEN l.jenis_layanan_id = 6 THEN lo.sisa_pecah_total_gabung 
                                                ELSE lo.luas_bumi 
                                            END 
                                        ELSE lo.luas_bumi 
                                    END AS luas_bumi")
                  ])
                  ->join('layanan as l', 'l.nomor_layanan', '=', 'lo.nomor_layanan')
                  ->whereIn('lo.nop_gabung', $subQuery1)
                  ->orWhereIn('lo.id', $subQuery2);

               $nestedSubQuery = DB::table('layanan_objek')
                  ->selectRaw('CAST(nop_gabung AS NUMBER) AS nop_gabung')
                  ->where('id', $layanan_objek_id)
                  ->unionAll(
                     DB::table('layanan_objek')
                        ->selectRaw('CAST(id AS NUMBER)')
                        ->where('id', $layanan_objek_id)
                        ->whereNull('nop_gabung')
                  );

               $nestedJoin = DB::table('layanan_objek as a')
                  ->select([
                     'a.id',
                     'a.kd_propinsi',
                     'a.kd_dati2',
                     'a.kd_kecamatan',
                     'a.kd_kelurahan',
                     'a.kd_blok',
                     'a.no_urut',
                     'a.kd_jns_op'
                  ])
                  ->joinSub($nestedSubQuery, 'b', function ($join) {
                     $join->on('a.id', '=', 'b.nop_gabung');
                  });

               $mainJoin = DB::table('layanan_objek as a')
                  ->select([
                     'a.id',
                     'a.kd_propinsi',
                     'a.kd_dati2',
                     'a.kd_kecamatan',
                     'a.kd_kelurahan',
                     'a.kd_blok',
                     'a.no_urut',
                     'a.kd_jns_op'
                  ])
                  ->joinSub($nestedJoin, 'b', function ($join) {
                     $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                        ->on('a.kd_dati2', '=', 'b.kd_dati2')
                        ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                        ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                        ->on('a.kd_blok', '=', 'b.kd_blok')
                        ->on('a.no_urut', '=', 'b.no_urut')
                        ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                        ->whereRaw('a.id != b.id')
                        ->whereNotNull('a.nomor_formulir');
                  });

               $secondQuery = DB::table('layanan_objek as dd')
                  ->select(['dd.id', 'dd.luas_bumi'])
                  ->joinSub($mainJoin, 'ee', 'ee.id', '=', 'dd.nop_gabung');

               $luas_pecah = $firstQuery->unionAll($secondQuery)->get();

               $total_luas_bumi = 0;
               foreach ($luas_pecah as $row) {
                  $total_luas_bumi += $row->luas_bumi;
               }
               $persentase_pecah = [];
               $arra = [];
               foreach ($luas_pecah as $row) {
                  $arra[$row->id] = ['luas' => $row->luas_bumi, 'total' => $total_luas_bumi];
                  $persentase_pecah[$row->id] = round(($row->luas_bumi / $total_luas_bumi) * 100, 2);
               }
               //mencari tagihan dari induk untuk pecahan
               $persen = $persentase_pecah[$layanan_objek_id] ?? 100;
            }

            // mencari tagihan asal
            $kolom = $jenis_layanan_id == '6' ? 'sppt_oltp.pbb_terhutang_sppt' : 'sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT';
            $tagihan = DB::connection("oracle_spo")
               ->table('sppt_oltp')
               ->selectRaw("
                    sppt_oltp.kd_propinsi, sppt_oltp.kd_dati2, sppt_oltp.kd_kecamatan, 
                    sppt_oltp.kd_kelurahan, sppt_oltp.kd_blok, sppt_oltp.no_urut, sppt_oltp.kd_jns_op, 
                    sppt_oltp.thn_pajak_sppt AS tahun_pajak,
                    ROUND(" . $kolom . " * (" . $persen . " / 100)) AS pbb, 
                    sppt_oltp.nm_wp_sppt AS nm_wp,
                    ROUND(" . $kolom . " * (" . $persen . " / 100)) AS pokok,  
                    ROUND(sppt_oltp.denda * (" . $persen . " / 100)) AS denda,
                    ROUND(" . $kolom . " * (" . $persen . " / 100)) + ROUND(sppt_oltp.denda * (" . $persen . " / 100)) AS total
                ")

               ->whereRaw("sppt_oltp.thn_pajak_sppt <'" . $tahun . "'
                        AND  sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT>0 
                        and sppt_oltp.status_pembayaran_sppt!='3'");
            $tagihan = $tagihan->where(function ($sql) use ($nop_asal) {
               foreach ($nop_asal as $value) {
                  $sql->orWhere(function ($dep) use ($value) {
                     foreach ($value as $i => $a) {
                        # code...
                        $dep->where($i, $a);
                     }
                  });
               }
            });


            $tagihanfix = $tagihan->get();

            foreach ($tagihanfix as $tagihan) {
               if ($tagihan->pokok > 0) {
                  $ArTagihan[] = [
                     'kd_propinsi' => $tagihan->kd_propinsi,
                     'kd_dati2' => $tagihan->kd_dati2,
                     'kd_kecamatan' => $tagihan->kd_kecamatan,
                     'kd_kelurahan' => $tagihan->kd_kelurahan,
                     'kd_blok' => $tagihan->kd_blok,
                     'no_urut' => $tagihan->no_urut,
                     'kd_jns_op' => $tagihan->kd_jns_op,
                     'tahun_pajak' => $tagihan->tahun_pajak,
                     'pbb' => $tagihan->pbb,
                     'nm_wp' => $tagihan->nm_wp,
                     'pokok' => $tagihan->pokok,
                     'denda' => $tagihan->denda,
                     'total' => $tagihan->total
                  ];
               }
            }
         }


         $maxsppt = '';
         foreach ($sppt as $key => $value) {
            # code...
            $maxsppt = $value->thn_pajak_sppt;
         }
         $data = [
            'objek' => $objek,
            'bangunan' => $bangunan,
            'sppt' => $sppt,
            'show_sppt' => $show_sppt,
            'tahun_nota' => $tahun_nota,
            'pemutakhiran' => $pemutakhiran,
            'pecah' => $pecah,
            'gabung' => $gabung,
            'gabungke' => $gabungke,
            'tagihan' => $tagihan_nop,
            'tagihan_nop_asal' => $ArTagihan,
            'sppt_terakhir' => $maxsppt
         ];
         $success = true;
      }

      return $data;
      // 35.07.190.012.018.0007.0 
   }
}
