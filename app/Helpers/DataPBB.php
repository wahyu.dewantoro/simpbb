<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class DataPBB
{


   public static function sqlCoreBaku($tanggal)
   {
      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));
      $start = $tahun . '0101';
      $sql = "SELECT sppt.kd_propinsi,
                          sppt.kd_dati2,
                          sppt.kd_kecamatan,
                          sppt.kd_kelurahan,
                          sppt.kd_blok,
                          sppt.no_urut,
                          sppt.kd_jns_op,
                          sppt.thn_pajak_sppt tahun_pajak,
                          sppt.tgl_terbit_sppt,
                          pbb_yg_harus_dibayar_sppt pbb,
                          get_buku (pbb_yg_harus_dibayar_sppt) kd_buku,
                          NVL (nilai_potongan, 0) potongan,
                          CASE
                             WHEN    jns_koreksi IN ('3', '4')
                                OR sppt_pembatalan.thn_pajak_sppt IS NOT NULL
                             THEN
                                pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                             ELSE
                                0
                          END
                             koreksi,
                          CASE
                             WHEN bayar >
                                   pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                             THEN
                                pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                             ELSE
                                NVL (bayar, 0)
                          END
                             bayar,
                          jm,
                          denda
                    FROM sppt
                          LEFT JOIN sppt_potongan
                             ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                                AND sppt.kd_dati2 = sppt_potongan.kd_dati2
                                AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
                                AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
                                AND sppt.kd_blok = sppt_potongan.kd_blok
                                AND sppt.no_urut = sppt_potongan.no_urut
                                AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
                                AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
                          LEFT JOIN  pengurang_piutang
                             ON     sppt.kd_propinsi = pengurang_piutang.kd_propinsi
                                AND sppt.kd_dati2 = pengurang_piutang.kd_dati2
                                AND sppt.kd_kecamatan = pengurang_piutang.kd_kecamatan
                                AND sppt.kd_kelurahan = pengurang_piutang.kd_kelurahan
                                AND sppt.kd_blok = pengurang_piutang.kd_blok
                                AND sppt.no_urut = pengurang_piutang.no_urut
                                AND sppt.kd_jns_op = pengurang_piutang.kd_jns_op
                                AND sppt.thn_pajak_sppt = pengurang_piutang.thn_pajak_sppt
                        left join sppt_nota on sppt.kd_propinsi=sppt_nota.kd_propinsi and
                                    sppt.kd_dati2=sppt_nota.kd_dati2 and
                                    sppt.kd_kecamatan=sppt_nota.kd_kecamatan and
                                    sppt.kd_kelurahan=sppt_nota.kd_kelurahan and
                                    sppt.kd_blok=sppt_nota.kd_blok and
                                    sppt.no_urut=sppt_nota.no_urut and
                                    sppt.kd_jns_op=sppt_nota.kd_jns_op and
                                    sppt.thn_pajak_sppt=sppt_nota.thn_pajak_sppt
                          LEFT JOIN (SELECT DISTINCT kd_propinsi,
                                                      kd_dati2,
                                                      kd_kecamatan,
                                                      kd_kelurahan,
                                                      kd_blok,
                                                      no_urut,
                                                      kd_jns_op,
                                                      thn_pajak_sppt
                                       FROM sppt_pembatalan
                                       WHERE     sppt_pembatalan.tgl_mulai <= TO_DATE('$tanggal','yyyymmdd')
                                             AND sppt_pembatalan.tgl_selesai >= TO_DATE('$tanggal','yyyymmdd')) sppt_pembatalan
                             ON     sppt.kd_propinsi = sppt_pembatalan.kd_propinsi
                                AND sppt.kd_dati2 = sppt_pembatalan.kd_dati2
                                AND sppt.kd_kecamatan = sppt_pembatalan.kd_kecamatan
                                AND sppt.kd_kelurahan = sppt_pembatalan.kd_kelurahan
                                AND sppt.kd_blok = sppt_pembatalan.kd_blok
                                AND sppt.no_urut = sppt_pembatalan.no_urut
                                AND sppt.kd_jns_op = sppt_pembatalan.kd_jns_op
                                AND sppt.thn_pajak_sppt = sppt_pembatalan.thn_pajak_sppt
                          LEFT JOIN
                          (  SELECT kd_propinsi,
                                   kd_dati2,
                                   kd_kecamatan,
                                   kd_kelurahan,
                                   kd_blok,
                                   no_urut,
                                   kd_jns_op,
                                   thn_pajak_sppt,
                                   SUM (NVL (denda_sppt, 0)) denda,
                                   SUM (
                                      NVL (jml_sppt_yg_dibayar, 0) - NVL (denda_sppt, 0))
                                      bayar
                             FROM pembayaran_sppt
                             WHERE TRUNC (tgl_pembayaran_sppt) BETWEEN TO_DATE('$start','yyyymmdd')
                                                           AND TO_DATE('$tanggal','yyyymmdd')
                          GROUP BY kd_propinsi,
                                   kd_dati2,
                                   kd_kecamatan,
                                   kd_kelurahan,
                                   kd_blok,
                                   no_urut,
                                   kd_jns_op,
                                   thn_pajak_sppt) pembayaran_sppt
                             ON     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
                                AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
                                AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
                                AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                AND sppt.no_urut = pembayaran_sppt.no_urut
                                AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
                          LEFT JOIN
                          (  SELECT b.kd_propinsi,
                                   b.kd_dati2,
                                   b.kd_kecamatan,
                                   b.kd_kelurahan,
                                   b.kd_blok,
                                   b.no_urut,
                                   b.kd_jns_op,
                                   b.thn_pajak_sppt,
                                   COUNT (1) jm
                             FROM sim_pbb.data_billing a
                                   JOIN sim_pbb.sppt_penelitian b
                                      ON a.data_billing_id = b.data_billing_id
                             WHERE kd_status = '0'
                          GROUP BY b.kd_propinsi,
                                   b.kd_dati2,
                                   b.kd_kecamatan,
                                   b.kd_kelurahan,
                                   b.kd_blok,
                                   b.no_urut,
                                   b.kd_jns_op,
                                   b.thn_pajak_sppt) sppt_penelitian
                             ON     sppt.kd_propinsi = sppt_penelitian.kd_propinsi
                                AND sppt.kd_dati2 = sppt_penelitian.kd_dati2
                                AND sppt.kd_kecamatan = sppt_penelitian.kd_kecamatan
                                AND sppt.kd_kelurahan = sppt_penelitian.kd_kelurahan
                                AND sppt.kd_blok = sppt_penelitian.kd_blok
                                AND sppt.no_urut = sppt_penelitian.no_urut
                                AND sppt.kd_jns_op = sppt_penelitian.kd_jns_op
                                AND sppt.thn_pajak_sppt = sppt_penelitian.thn_pajak_sppt
                    WHERE    TO_CHAR (sppt.tgl_terbit_sppt, 'yyyy') = '$tahun'
                              and trunc(sppt.tgl_terbit_sppt)<=to_date('$tanggal','yyyymmdd')
                             AND sppt.thn_pajak_sppt = '$tahun'
                          AND sppt.tgl_terbit_sppt IS NOT NULL
                          AND sppt.status_pembayaran_sppt IN ('1', '0')
                          AND NVL (jm,0)=0
                          and sppt_nota.thn_pajak_sppt is null";
      return $sql;
   }



   public static function coreSppt()
   {
      return DB::connection("oracle_satutujuh")->table("sppt")
         ->leftjoin('ref_buku', function ($join) {
            $join->on('ref_buku.thn_awal', '<=', 'sppt.thn_pajak_sppt')
               ->on('ref_buku.thn_akhir', '>=', 'sppt.thn_pajak_sppt')
               ->on('ref_buku.nilai_min_buku', '<=', 'sppt.pbb_yg_harus_dibayar_sppt')
               ->on('ref_buku.nilai_max_buku', '>=', 'sppt.pbb_yg_harus_dibayar_sppt');
         })
         ->leftjoin('sppt_potongan', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
               ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
               ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
         })->leftjoin(DB::raw('pengurang_piutang sppt_koreksi'), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
               ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
               ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
         });
   }

   public static function penagihan()
   {
      $data = self::coreSppt()
         ->join('dat_op_bumi', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'dat_op_bumi.kd_blok')
               ->on('sppt.no_urut', '=', 'dat_op_bumi.no_urut')
               ->on('sppt.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
         })
         ->join('dat_objek_pajak', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'dat_objek_pajak.kd_blok')
               ->on('sppt.no_urut', '=', 'dat_objek_pajak.no_urut')
               ->on('sppt.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
         })
         ->leftjoin(db::raw("(  SELECT pembayaran_sppt.kd_propinsi,
                                     pembayaran_sppt.kd_dati2,
                                     pembayaran_sppt.kd_kecamatan,
                                     pembayaran_sppt.kd_kelurahan,
                                     pembayaran_sppt.kd_blok,
                                     pembayaran_sppt.no_urut,
                                     pembayaran_sppt.kd_jns_op,
                                     pembayaran_sppt.thn_pajak_sppt,
                                     SUM (jml_sppt_yg_dibayar - NVL (denda_sppt, 0)) terbayar
                                 FROM pembayaran_sppt
                                 GROUP BY pembayaran_sppt.kd_propinsi,
                                     pembayaran_sppt.kd_dati2,
                                     pembayaran_sppt.kd_kecamatan,
                                     pembayaran_sppt.kd_kelurahan,
                                     pembayaran_sppt.kd_blok,
                                     pembayaran_sppt.no_urut,
                                     pembayaran_sppt.kd_jns_op,
                                     pembayaran_sppt.thn_pajak_sppt) pembayaran_sppt"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
               ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
               ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
         })
         ->selectraw("sppt.kd_propinsi,
                         sppt.kd_dati2,
                         sppt.kd_kecamatan,
                         sppt.kd_kelurahan,
                         sppt.kd_blok,
                         sppt.no_urut,
                         sppt.kd_jns_op,
                         sppt.thn_pajak_sppt,
                         sppt.nm_wp_sppt,
                         sppt.jln_wp_sppt,
                         sppt.blok_kav_no_wp_sppt,
                         sppt.rt_wp_sppt,
                         sppt.rw_wp_sppt,
                         sppt.kelurahan_wp_sppt,
                         sppt.kota_wp_sppt,
                         status_pembayaran_sppt,
                         pbb_yg_harus_dibayar_sppt pbb,
                         NVL (nilai_potongan, 0) potongan,
                         terbayar,
                         sppt_koreksi.no_transaksi,
                         sppt_koreksi.keterangan,
                         hit_denda((pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)),tgl_jatuh_tempo_sppt,sysdate) es_denda,
                         kd_buku,nm_kecamatan,nm_kelurahan,no_stpd,
                         kobil,tgl_terbit_sppt, case when jns_transaksi_op='3' then '4' else jns_bumi end jns_bumi")
         ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
         ->join('ref_kelurahan', function ($join) {
            $join->on('ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
               ->on('ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan');
         })
         ->leftjoin(db::raw("( SELECT stn.kd_propinsi,
                                         stn.kd_dati2,
                                         stn.kd_kecamatan,
                                         stn.kd_kelurahan,
                                         stn.kd_blok,
                                         stn.no_urut,
                                         stn.kd_jns_op,
                                         stn.thn_pajak_sppt,
                                         LISTAGG (surat_nomor || ' (' || surat_tanggal || ') ', ', ')
                                         WITHIN GROUP (ORDER BY surat_tanggal)
                                         no_stpd
                                 FROM sim_pbb.surat_tagihan_nop stn
                                         JOIN sim_pbb.surat_tagihan st ON st.id = stn.surat_tagihan_id
                                         where st.deleted_at is null
                             GROUP BY stn.kd_propinsi,
                                         stn.kd_dati2,
                                         stn.kd_kecamatan,
                                         stn.kd_kelurahan,
                                         stn.kd_blok,
                                         stn.no_urut,
                                         stn.kd_jns_op,
                                         stn.thn_pajak_sppt) stpd"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'stpd.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'stpd.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'stpd.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'stpd.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'stpd.kd_blok')
               ->on('sppt.no_urut', '=', 'stpd.no_urut')
               ->on('sppt.kd_jns_op', '=', 'stpd.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'stpd.thn_pajak_sppt');
         })
         ->leftjoin(db::raw("(SELECT billing_kolektif.kd_propinsi,
         billing_kolektif.kd_dati2,
         billing_kolektif.kd_kecamatan,
         billing_kolektif.kd_kelurahan,
         billing_kolektif.kd_blok,
         billing_kolektif.no_urut,
         billing_kolektif.kd_jns_op,
         billing_kolektif.tahun_pajak thn_pajak_sppt,
         LISTAGG (
               kobil
            || ' ('
            || CASE
                  WHEN data_billing.kd_jns_op = '1' THEN 'Billing Kolektif'
                  WHEN data_billing.kd_jns_op = '4' THEN 'Pengaktifan'
                  ELSE 'Nota Perhitungan'
               END
            || ')',
            ', ')
         WITHIN GROUP (ORDER BY kobil)
            kobil
    FROM sim_pbb.billing_kolektif
          JOIN sim_pbb.data_billing data_billing
            ON     data_billing.data_billing_id =
                      billing_Kolektif.data_billing_id
               AND data_billing.deleted_at IS NULL
GROUP BY billing_kolektif.kd_propinsi,
         billing_kolektif.kd_dati2,
         billing_kolektif.kd_kecamatan,
         billing_kolektif.kd_kelurahan,
         billing_kolektif.kd_blok,
         billing_kolektif.no_urut,
         billing_kolektif.kd_jns_op,
         billing_kolektif.tahun_pajak) billing_kolektif"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'billing_kolektif.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'billing_kolektif.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'billing_kolektif.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'billing_kolektif.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'billing_kolektif.kd_blok')
               ->on('sppt.no_urut', '=', 'billing_kolektif.no_urut')
               ->on('sppt.kd_jns_op', '=', 'billing_kolektif.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'billing_kolektif.thn_pajak_sppt');
         })
         ->whereraw("sppt.status_pembayaran_sppt!='1'  and (jns_koreksi is null or jns_koreksi='1' or jns_koreksi='2')")
         //  and nvl(jns_koreksi,'0')!='3' 
         ->orderbyraw("sppt.kd_propinsi,
         sppt.kd_dati2,
         sppt.kd_kecamatan,
         sppt.kd_kelurahan,
         sppt.kd_blok,
         sppt.no_urut,
         sppt.kd_jns_op");

      /*  AND (   (    data_billing.kd_status = 0
                        AND data_billing.expired_at > SYSDATE)
                    OR (    data_billing.kd_status = '1'
                        AND data_billing.expired_at < SYSDATE)) */

      return $data;
   }

   public static function tunggakanTagihan()
   {
      $data = self::coreSppt()
         ->join('dat_op_bumi', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'dat_op_bumi.kd_blok')
               ->on('sppt.no_urut', '=', 'dat_op_bumi.no_urut')
               ->on('sppt.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
         })
         ->join('dat_objek_pajak', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'dat_objek_pajak.kd_blok')
               ->on('sppt.no_urut', '=', 'dat_objek_pajak.no_urut')
               ->on('sppt.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
         })
         ->join('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
         ->leftjoin(db::raw("(  SELECT pembayaran_sppt.kd_propinsi,
                                        pembayaran_sppt.kd_dati2,
                                        pembayaran_sppt.kd_kecamatan,
                                        pembayaran_sppt.kd_kelurahan,
                                        pembayaran_sppt.kd_blok,
                                        pembayaran_sppt.no_urut,
                                        pembayaran_sppt.kd_jns_op,
                                        pembayaran_sppt.thn_pajak_sppt,
                                        SUM (jml_sppt_yg_dibayar - NVL (denda_sppt, 0)) terbayar
                                    FROM pembayaran_sppt
                                    GROUP BY pembayaran_sppt.kd_propinsi,
                                        pembayaran_sppt.kd_dati2,
                                        pembayaran_sppt.kd_kecamatan,
                                        pembayaran_sppt.kd_kelurahan,
                                        pembayaran_sppt.kd_blok,
                                        pembayaran_sppt.no_urut,
                                        pembayaran_sppt.kd_jns_op,
                                        pembayaran_sppt.thn_pajak_sppt) pembayaran_sppt"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
               ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
               ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
         })
         ->selectraw("sppt.kd_propinsi,
                            sppt.kd_dati2,
                            sppt.kd_kecamatan,
                            sppt.kd_kelurahan,
                            sppt.kd_blok,
                            sppt.no_urut,
                            sppt.kd_jns_op,
                            sppt.thn_pajak_sppt,
                            nm_wp nm_wp_sppt,
                            jalan_wp jln_wp_sppt,
                            blok_kav_no_wp blok_kav_no_wp_sppt,
                            rt_wp rt_wp_sppt,
                            rw_wp rw_wp_sppt,
                            kelurahan_wp kelurahan_wp_sppt,
                            kota_wp kota_wp_sppt,
                            status_pembayaran_sppt,
                            pbb_yg_harus_dibayar_sppt pbb,
                            NVL (nilai_potongan, 0) potongan,
                            terbayar,
                            sppt_koreksi.no_transaksi,
                            sppt_koreksi.keterangan,
                            hit_denda((pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)),tgl_jatuh_tempo_sppt,sysdate) es_denda,
                            kd_buku,nm_kecamatan,nm_kelurahan,no_stpd,
                            kobil,tgl_terbit_sppt, case when jns_transaksi_op='3' then '4' else jns_bumi end jns_bumi")
         ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
         ->join('ref_kelurahan', function ($join) {
            $join->on('ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
               ->on('ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan');
         })
         ->leftjoin(db::raw("( SELECT stn.kd_propinsi,
                                            stn.kd_dati2,
                                            stn.kd_kecamatan,
                                            stn.kd_kelurahan,
                                            stn.kd_blok,
                                            stn.no_urut,
                                            stn.kd_jns_op,
                                            stn.thn_pajak_sppt,
                                            LISTAGG (surat_nomor || ' (' || to_char(surat_tanggal,'yyyy-mm-dd') || ') ', ', ')
                                            WITHIN GROUP (ORDER BY surat_tanggal)
                                            no_stpd
                                    FROM sim_pbb.surat_tagihan_nop stn
                                            JOIN sim_pbb.surat_tagihan st ON st.id = stn.surat_tagihan_id
                                            where st.deleted_at is null
                                GROUP BY stn.kd_propinsi,
                                            stn.kd_dati2,
                                            stn.kd_kecamatan,
                                            stn.kd_kelurahan,
                                            stn.kd_blok,
                                            stn.no_urut,
                                            stn.kd_jns_op,
                                            stn.thn_pajak_sppt) stpd"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'stpd.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'stpd.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'stpd.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'stpd.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'stpd.kd_blok')
               ->on('sppt.no_urut', '=', 'stpd.no_urut')
               ->on('sppt.kd_jns_op', '=', 'stpd.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'stpd.thn_pajak_sppt');
         })
         ->leftjoin(db::raw("(SELECT billing_kolektif.kd_propinsi,
            billing_kolektif.kd_dati2,
            billing_kolektif.kd_kecamatan,
            billing_kolektif.kd_kelurahan,
            billing_kolektif.kd_blok,
            billing_kolektif.no_urut,
            billing_kolektif.kd_jns_op,
            billing_kolektif.tahun_pajak thn_pajak_sppt,
            LISTAGG (
                  kobil
               || ' ('
               || CASE
                     WHEN data_billing.kd_jns_op = '1' THEN 'Billing Kolektif'
                     WHEN data_billing.kd_jns_op = '4' THEN 'Pengaktifan'
                     ELSE 'Nota Perhitungan'
                  END
               || ')',
               ', ')
            WITHIN GROUP (ORDER BY kobil)
               kobil
       FROM sim_pbb.billing_kolektif
             JOIN sim_pbb.data_billing data_billing
               ON     data_billing.data_billing_id =
                         billing_Kolektif.data_billing_id
                  AND data_billing.deleted_at IS NULL
                  AND (   (    data_billing.kd_status = 0
                           AND data_billing.expired_at > SYSDATE)
                       OR (    data_billing.kd_status = '1'
                           AND data_billing.expired_at < SYSDATE))
   GROUP BY billing_kolektif.kd_propinsi,
            billing_kolektif.kd_dati2,
            billing_kolektif.kd_kecamatan,
            billing_kolektif.kd_kelurahan,
            billing_kolektif.kd_blok,
            billing_kolektif.no_urut,
            billing_kolektif.kd_jns_op,
            billing_kolektif.tahun_pajak) billing_kolektif"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'billing_kolektif.kd_propinsi')
               ->on('sppt.kd_dati2', '=', 'billing_kolektif.kd_dati2')
               ->on('sppt.kd_kecamatan', '=', 'billing_kolektif.kd_kecamatan')
               ->on('sppt.kd_kelurahan', '=', 'billing_kolektif.kd_kelurahan')
               ->on('sppt.kd_blok', '=', 'billing_kolektif.kd_blok')
               ->on('sppt.no_urut', '=', 'billing_kolektif.no_urut')
               ->on('sppt.kd_jns_op', '=', 'billing_kolektif.kd_jns_op')
               ->on('sppt.thn_pajak_sppt', '=', 'billing_kolektif.thn_pajak_sppt');
         })
         ->whereraw("sppt.status_pembayaran_sppt!='1'  and  nvl(jns_koreksi,'0') not in ('3','2')")
         //  and nvl(jns_koreksi,'0')!='3' 
         // ((jns_koreksi is null or jns_koreksi='1') and
         ->orderbyraw("sppt.kd_propinsi,
            sppt.kd_dati2,
            sppt.kd_kecamatan,
            sppt.kd_kelurahan,
            sppt.kd_blok,
            sppt.no_urut,
            sppt.kd_jns_op");

      return $data;
   }
}
