<?php

namespace App\Helpers;

use App\Datnilaiindividu;
// use App\Formulir;
use App\GabungDetail;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Billing_kolektif;
use App\Models\Data_billing;
use App\Models\HistoryMutasiGabung;
use App\Models\JenisPengurangan;
use App\Models\Layanan;
use App\Models\Layanan_dokumen;
use App\Models\Layanan_objek;
use App\Models\Notaperhitungan;
use App\Models\SpptBkp;
use App\Models\TmpPotonganIndividu;
use App\ObjekPajak;
use App\OpBumi;
use App\pegawaiStruktural;
use App\PembayaranMutasi;
use App\PembayaranMutasiData;
use App\PembayaranSppt;
use App\Sppt;
use App\SubjekPajak;
use App\TblSpop;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Exists;
use PhpParser\Node\Stmt\TryCatch;

// use PhpParser\Node\Stmt\TryCatch;
// use Illuminate\Support\Str;

class Pajak
{



    public static function tahun()
    {
        return date('Y');
    }

    public static function nip()
    {
        return '000000000202203086';
    }

    public static function userid($userid = '')
    {
        if ($userid <> '') {
            return $userid;
        }

        return Auth()->user()->id ?? 1901;
    }

    public static function jenisLayanan(Request $request)
    {
        $layanan = $request->jenis_layanan <> NULL ? $request->jenis_layanan : $request->jenis_pelayanan;

        $expajuan = explode('_', $layanan);


        if (count($expajuan) == 1) {
            return $layanan;
            // return 'a';
        } else {
            // return 'b';
            return $expajuan[1] ?? null;
        }
    }

    public static function pendataanSpop(Request $request)
    {

        $jenislayanan = self::jenisLayanan($request);
        $tahun = self::tahun();
        $getnomor = self::NomorFormulir('1');
        $nomor_formulir = $getnomor['nomor'];
        $now = db::raw("sysdate");

        // select nop_asal 
        // from pembayaran_sppt_mutasi_batch
        // where nop_tujuan=''
        $nop_asal = $request->nop_asal;
        if ($request->jenis_pelayanan == '1') {
            $cm = PembayaranMutasi::where('nop_tujuan', onlyNumber($request->nop_proses))
                ->first();
            if ($cm) {
                $nop_asal = $cm->nop_asal;
            }
        }

        $spop = [
            'no_formulir' => $nomor_formulir,
            'jns_transaksi' => $request->jns_transaksi,
            'nop_proses' => onlyNumber($request->nop_proses),
            'nop_bersama' => null,
            'nop_asal' => onlyNumber($nop_asal),
            'subjek_pajak_id' => $request->subjek_pajak_id,
            'nm_wp' => $request->nm_wp,
            'jalan_wp' => $request->jalan_wp,
            'blok_kav_no_wp' => $request->blok_kav_no_wp,
            'rw_wp' => $request->rw_wp,
            'rt_wp' => $request->rt_wp,
            'kelurahan_wp' => strtoupper($request->kelurahan_wp) . ' - ' . strtoupper($request->kecamatan_wp),
            'kota_wp' => strtoupper($request->kota_wp) . ' - ' . strtoupper($request->propinsi_wp),
            'kd_pos_wp' => $request->kd_pos_wp,
            'telp_wp' => $request->telp_wp,
            'npwp' => $request->npwp,
            'status_pekerjaan_wp' => $request->status_pekerjaan_wp,
            'no_persil' => $request->no_persil,
            'jalan_op' => strtoupper($request->jalan_op),
            'blok_kav_no_op' =>  strtoupper($request->blok_kav_no_op),
            'rw_op' =>  $request->rw_op,
            'rt_op' =>  $request->rt_op,
            'kd_status_cabang' => 0,
            'kd_status_wp' => $request->kd_status_wp,
            'kd_znt' => $request->kd_znt,
            'luas_bumi' => $request->luas_bumi,
            'jns_bumi' => $request->jns_bumi,
            'tgl_pendataan_op' =>  $now,
            'nip_pendata' => self::nip(),
            'tgl_pemeriksaan_op' => $now,
            'nip_pemeriksa_op' => self::nip(),
            'tgl_perekaman_op' => $now,
            'nip_perekam_op' => self::nip(),
        ];
        // log::info(['param spop' => $spop]);
        $nop_proses = $spop['nop_proses'];
        // DB::connection('oracle_satutujuh')->table('tbl_spop')->where('nop_proses', $nop_proses)->whereraw("no_formulir like '" . substr(date('Y'), 2, 2) . "%'")->delete();

        // LOg::info($spop);
        $kd_propinsi = substr($nop_proses, 0, 2);
        $kd_dati2 = substr($nop_proses, 2, 2);
        $kd_kecamatan = substr($nop_proses, 4, 3);
        $kd_kelurahan = substr($nop_proses, 7, 3);
        $kd_blok = substr($nop_proses, 10, 3);
        $no_urut = substr($nop_proses, 13, 4);
        $kd_jns_op = substr($nop_proses, 17, 1);



        //  spop ke 1.7
        $cek = TblSpop::whereraw("trim(no_formulir) ='" . trim($nomor_formulir) . "'")->get();
        if ($cek->count() > 0) {
            TblSpop::whereraw("trim(no_formulir) ='" . trim($nomor_formulir) . "'")->update($spop);
        } else {
            TblSpop::insert(
                $spop
            );
        }
        // spop ke sim_pbb
        $spop['jenis_layanan_id'] = $jenislayanan;
        $spop['layanan_objek_id'] = $request->lhp_id ?? '';
        $spop['pendataan_objek_id'] = $request->pendataan_objek_id ?? '';
        $spop['created_at'] = db::raw("sysdate");
        $spop['created_by'] = self::userid($request->created_by ?? '');
        $spop['updated_at'] = db::raw("sysdate");
        $spop['updated_by'] = self::userid($request->created_by ?? '');

        $cekb = DB::table('tbl_spop')->whereraw("trim(no_formulir) ='" . trim($nomor_formulir) . "'")->get();
        // log::info($spop);
        if ($cekb->count() > 0) {
            DB::table('tbl_spop')->whereraw("trim(no_formulir) ='" . trim($nomor_formulir) . "'")->update($spop);
        } else {
            DB::table('tbl_spop')->insert($spop);
        }

        return $spop;
    }

    public static function pendatanSubjek(Request $request)
    {
        $spi = trim($request->subjek_pajak_id);

        $kelurahan_wp = strtoupper($request->kelurahan_wp);
        $kec = str_replace('-', ' ', $request->kecamatan_wp);
        $kec = trim($kec);
        if ($kec != '') {

            $kelurahan_wp .= ' - ' . strtoupper($kec);
        }

        $kota_wp = strtoupper($request->kota_wp);
        $prop = str_replace('-', ' ', $request->propinsi_wp);
        $prop = trim($prop);
        if ($prop != '') {
            $kota_wp .= ' - ' . strtoupper($prop);
        }

        $ts = [
            'subjek_pajak_id' => trim($request->subjek_pajak_id),
            'nm_wp' => strtoupper($request->nm_wp),
            'jalan_wp' => substr(strtoupper($request->jalan_wp), 0, 30),
            'blok_kav_no_wp' => $request->blok_kav_no_wp,
            'rw_wp' => strtoupper($request->rw_wp),
            'rt_wp' => strtoupper($request->rt_wp),
            'kelurahan_wp' => $kelurahan_wp,
            'kota_wp' => $kota_wp,
            'kd_pos_wp' => strtoupper($request->kd_pos_wp),
            'telp_wp' => strtoupper($request->telp_wp),
            'npwp' => $request->npwp,
            'status_pekerjaan_wp' => $request->status_pekerjaan_wp,
        ];

        // dat_subjek_pajak
        $a = DB::connection('oracle_satutujuh')->table('dat_subjek_pajak')->whereraw("trim(subjek_pajak_id)='" . $spi . "'")->get()->count();
        if ($a > 0) {
            // update
            DB::connection('oracle_satutujuh')->table('dat_subjek_pajak')->whereraw("trim(subjek_pajak_id)='" . $spi . "'")->update($ts);
        } else {
            // insert
            DB::connection('oracle_satutujuh')->table('dat_subjek_pajak')->insert($ts);
        }

        /* $b = DB::connection('oracle_dua')->table('dat_subjek_pajak')->whereraw("trim(subjek_pajak_id)='" . $spi . "'")->get()->count();
        if ($b > 0) {
            // update
            DB::connection('oracle_dua')->table('dat_subjek_pajak')->whereraw("trim(subjek_pajak_id)='" . $spi . "'")->update($ts);
        } else {
            // insert
            DB::connection('oracle_dua')->table('dat_subjek_pajak')->insert($ts);
        } */

        return $ts;
    }

    public static function pendataanLspop(Request $request)
    {

        $now = db::raw('sysdate');
        $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
        $kd_propinsi = substr($nop_proses, 0, 2);
        $kd_dati2 = substr($nop_proses, 2, 2);
        $kd_kecamatan = substr($nop_proses, 4, 3);
        $kd_kelurahan = substr($nop_proses, 7, 3);
        $kd_blok = substr($nop_proses, 10, 3);
        $no_urut = substr($nop_proses, 13, 4);
        $kd_jns_op = substr($nop_proses, 17, 1);

        $vObject = [
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op
        ];

        // $nfl = self::NomorFormulir('2');
        $arrayBng = [];
        $arrayLspop = [];
        $arrayFasilitas = [];
        $arrayJpb = [];

        // hapus  op bangunan dulu
        DB::connection('oracle_satutujuh')->table(DB::raw('dat_op_bangunan'))->where('kd_propinsi', $kd_propinsi)->where('kd_dati2', $kd_dati2)->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->where('kd_blok', $kd_blok)->where('no_urut', $no_urut)->where('kd_jns_op', $kd_jns_op)->delete();

        // delete lspop sebelumnya 
        // DB::connection('oracle_satutujuh')->table('tbl_lspop')->where('nop', $nop_proses)->whereraw("no_formulir like '" . substr(date('Y'), 2, 2) . "%'")->delete();
        /* if (config('app.env') != 'local') {
            DB::connection('oracle_dua')->table('tbl_lspop')->where('nop', $nop_proses)->whereraw("no_formulir like '" . substr(date('Y'), 2, 2) . "%'")->delete();
        } */

        // DB::table('tbl_lspop')->where('nop', $nop_proses)->whereraw("no_formulir like '" . substr(date('Y'), 2, 2) . "%'")->delete();

        if (isset($request->kd_jpb)) {
            DB::connection('oracle_satutujuh')->table(DB::raw('dat_op_bangunan'))
                ->where('kd_propinsi', $kd_propinsi)->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)->delete();
            for ($i = 0; $i < count($request->kd_jpb); $i++) {
                $kd_jpb = padding($request->kd_jpb[$i], 0, 2) ?? '01';


                $nfl = self::NomorFormulir('2');
                $nomorlspop = (int)$nfl['nomor'];

                if ($i % 2 == 0) {
                    $tambah = $i;
                } else {
                    $tambah = $i + 1;
                }

                $nilai_individu = 0;

                // log::info(['individu' => isset($request->nilai_individu[$i])]);
                if (isset($request->nilai_individu[$i])) {
                    if (preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]) > 0) {
                        $nilai_individu = (preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]) * $request->luas_bng[$i]) / 1000;
                    }
                }

                // log::info($request->jpb15_letak_tangki[$i]);
                $lspop = [
                    'no_formulir' => $nomorlspop,
                    'jns_transaksi' => $request->jns_transaksi_lspop[$i] ?? 1,
                    'nop' => $nop_proses,
                    'no_bng' => $i + 1,
                    'kd_jpb' => $kd_jpb,
                    'thn_dibangun_bng' => $request->thn_dibangun_bng[$i] ?? date('Y'),
                    'thn_renovasi_bng' => $request->thn_renovasi_bng[$i] ?? null,
                    'luas_bng' => $request->luas_bng[$i] ?? 0,
                    'jml_lantai_bng' => $request->jml_lantai_bng[$i] ?? '1',
                    'kondisi_bng' => $request->kondisi_bng[$i] ?? '2',
                    'jns_konstruksi_bng' => $request->jns_konstruksi_bng[$i] ?? '3',
                    'jns_atap_bng' => $request->jns_atap_bng[$i] ?? '3',
                    'kd_dinding' => $request->kd_dinding[$i] ?? '3',
                    'kd_lantai' => $request->kd_lantai[$i] ?? '2',
                    'kd_langit_langit' => $request->kd_langit_langit[$i] ?? '3',
                    'daya_listrik' => $request->daya_listrik[$i] ?? null,
                    'acsplit' => $request->acsplit[$i] ?? null,
                    'acwindow' => $request->acwindow[$i] ?? null,
                    'acsentral' => $request->acsentral[$i] ?? null,
                    'luas_kolam' => $request->luas_kolam[$i] ?? null,
                    'finishing_kolam' => $request->finishing_kolam[$i] ?? null,
                    'luas_perkerasan_ringan' => $request->luas_perkerasan_ringan[$i] ?? null,
                    'luas_perkerasan_sedang' => $request->luas_perkerasan_sedang[$i] ?? null,
                    'luas_perkerasan_berat' => $request->luas_perkerasan_berat[$i] ?? null,
                    'luas_perkerasan_dg_tutup' => $request->luas_perkerasan_dg_tutup[$i] ?? null,
                    'lap_tenis_lampu_beton' => $request->lap_tenis_lampu_beton[$i] ?? null,
                    'lap_tenis_lampu_aspal' => $request->lap_tenis_lampu_aspal[$i] ?? null,
                    'lap_tenis_lampu_rumput' => $request->lap_tenis_lampu_rumput[$i] ?? null,
                    'lap_tenis_beton' => $request->lap_tenis_beton[$i] ?? null,
                    'lap_tenis_aspal' => $request->lap_tenis_aspal[$i] ?? null,
                    'lap_tenis_rumput' => $request->lap_tenis_rumput[$i] ?? null,
                    'lift_penumpang' => $request->lift_penumpang[$i] ?? null,
                    'lift_kapsul' => $request->lift_kapsul[$i] ?? null,
                    'lift_barang' => $request->lift_barang[$i] ?? null,
                    'tgg_berjalan_a' => $request->tgg_berjalan_a[$i] ?? null,
                    'tgg_berjalan_b' => $request->tgg_berjalan_b[$i] ?? null,
                    'pjg_pagar' => $request->pjg_pagar[$i] ?? null,
                    'bhn_pagar' => $request->bhn_pagar[$i] ?? null,
                    'hydrant' => $request->hydrant_[$i] ?? null,
                    'sprinkler' => $request->sprinkler_[$i] ?? null,
                    'fire_alarm' => $request->fire_alarm_[$i] ?? null,
                    'jml_pabx' => $request->jml_pabx[$i] ?? null,
                    'sumur_artesis' => $request->sumur_artesis[$i] ?? null,
                    'nilai_individu' => $nilai_individu,
                    'jpb3_8_tinggi_kolom' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_tinggi_kolom[$i] : null,
                    'jpb3_8_lebar_bentang' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_lebar_bentang[$i] : null,
                    'jpb3_8_dd_lantai' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_dd_lantai[$i] : null,
                    'jpb3_8_kel_dinding' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_kel_dinding[$i] : null,
                    'jpb3_8_mezzanine' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_mezzanine[$i] : null,
                    'jpb5_kls_bng' => $kd_jpb == '05' ? $request->jpb5_kls_bng[$i] : null,
                    'jpb5_luas_kamar' => $kd_jpb == '05' ? $request->jpb5_luas_kamar[$i] : null,
                    'jpb5_luas_rng_lain' => $kd_jpb == '05' ? $request->jpb5_luas_rng_lain[$i] : null,
                    'jpb7_jns_hotel' => $kd_jpb == '07' ? $request->jpb7_jns_hotel[$i] : null,
                    'jpb7_bintang' => $kd_jpb == '07' ? $request->jpb7_bintang[$i] : null,
                    'jpb7_jml_kamar' => $kd_jpb == '07' ? $request->jpb7_jml_kamar[$i] : null,
                    'jpb7_luas_kamar' => $kd_jpb == '07' ? $request->jpb7_luas_kamar[$i] : null,
                    'jpb7_luas_rng_lain' => $kd_jpb == '07' ? $request->jpb7_luas_rng_lain[$i] : null,
                    'jpb13_kls_bng' => $kd_jpb == '13' ? $request->jpb13_kls_bng[$i] : null,
                    'jpb13_jml' => $kd_jpb == '13' ? $request->jpb13_jml[$i] : null,
                    'jpb13_luas_kamar' => $kd_jpb == '13' ? $request->jpb13_luas_kamar[$i] : null,
                    'jpb13_luas_rng_lain' => $kd_jpb == '13' ? $request->jpb13_luas_rng_lain[$i] : null,
                    'jpb15_letak_tangki' => $kd_jpb == '15' ? $request->jpb15_letak_tangki[$i] : '2',
                    'jpb15_kapasitas_tangki' => $kd_jpb == '15' ? $request->jpb15_kapasitas_tangki[$i] : '8000',
                    'jpb_lain_kls_bng' => in_array($kd_jpb, ['02', '04', '06', '09', '12', '16']) ? $request->jpb_lain_kls_bng[$i] : null,
                    'tgl_pendataan_bng' => $now,
                    'nip_pendata_bng' => self::nip(),
                    'tgl_pemeriksaan_bng' => $now,
                    'nip_pemeriksa_bng' => self::nip(),
                    'tgl_perekaman_bng' => $now,
                    'nip_perekam_bng' => self::nip()
                ];

                // log::info($lspop);

                // lspop
                DB::connection('oracle_satutujuh')->table(db::raw('tbl_lspop'))->insert($lspop);

                // $lspoppbb = $lspop;
                $lspop['layanan_objek_id'] = $request->lhp_id;
                $lspop['created_at'] = $now;
                $lspop['created_by'] = self::userid($request->created_by ?? '');
                $lspop['updated_at'] = $now;
                $lspop['updated_by'] = self::userid($request->created_by ?? '');

                // simpbb
                DB::table(db::raw('tbl_lspop'))->insert($lspop);

                $arrayLspop[] = $lspop;
                // $arrayLspopPbb[] = $lspoppbb;

                $arrayBng[] = [
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'no_bng' => $lspop['no_bng'] ?? null,
                    'kd_jpb' => $lspop['kd_jpb'] ?? null,
                    'no_formulir_lspop' => $lspop['no_formulir'],
                    'thn_dibangun_bng' => ($lspop['thn_renovasi_bng'] <> '' && $lspop['thn_renovasi_bng'] > $lspop['thn_dibangun_bng']) ? $lspop['thn_renovasi_bng'] : $lspop['thn_dibangun_bng'],
                    'luas_bng' => $lspop['luas_bng'] ?? 0,
                    'jml_lantai_bng' => $lspop['jml_lantai_bng'] <> '' ? $lspop['jml_lantai_bng'] : '1',
                    'kondisi_bng' => $lspop['kondisi_bng'] ?? null,
                    'jns_konstruksi_bng' => $lspop['jns_konstruksi_bng'] ?? 3,
                    'jns_atap_bng' => $lspop['jns_atap_bng'] ?? null,
                    'kd_dinding' => $lspop['kd_dinding'] ?? null,
                    'kd_lantai' => $lspop['kd_lantai'] ?? null,
                    'kd_langit_langit' => $lspop['kd_langit_langit'] ?? null,
                    'nilai_sistem_bng' => 0,
                    'jns_transaksi_bng' => $lspop['jns_transaksi'] ?? null,
                    'tgl_pendataan_bng' => $now,
                    'nip_pendata_bng' => self::nip(),
                    'tgl_pemeriksaan_bng' => $now,
                    'nip_pemeriksa_bng' => self::nip(),
                    'tgl_perekaman_bng' => $now,
                    'nip_perekam_bng' => self::nip()
                ];

                $arrayFasilitas = self::arrayFasilitas($vObject, $lspop);
                // log::info($lspop);
                $arrayJpb = self::arrayJpb($vObject, $lspop);
            }

            // proses bangunan,fasilitas,JPB
            // dat_op_bangunan
            DB::connection('oracle_satutujuh')->table(DB::raw('dat_op_bangunan'))->insert($arrayBng);


            // fasilitas
            // hapus  fasilitas dulu
            DB::connection('oracle_satutujuh')->table(DB::raw('dat_fasilitas_bangunan'))->where('kd_propinsi', $kd_propinsi)->where('kd_dati2', $kd_dati2)->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->where('kd_blok', $kd_blok)->where('no_urut', $no_urut)->where('kd_jns_op', $kd_jns_op)->delete();

            // DB::connection('oracle_satutujuh')->table(DB::raw('dat_fasilitas_bangunan'))->insert($arrayFasilitas);
            foreach ($arrayFasilitas as $dat_fasilitas) {
                DB::connection('oracle_satutujuh')->table(DB::raw('dat_fasilitas_bangunan'))->insert($dat_fasilitas);
            }


            foreach ($arrayJpb as $tablejpb => $rowdata) {
                DB::connection('oracle_satutujuh')->table(DB::raw($tablejpb))->where('kd_propinsi', $kd_propinsi)->where('kd_dati2', $kd_dati2)->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->where('kd_blok', $kd_blok)->where('no_urut', $no_urut)->where('kd_jns_op', $kd_jns_op)->delete();
                DB::connection('oracle_satutujuh')->table(DB::raw($tablejpb))->insert($rowdata);
            }

            Datnilaiindividu::where('kd_propinsi', $kd_propinsi)->where('kd_dati2', $kd_dati2)->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->where('kd_blok', $kd_blok)->where('no_urut', $no_urut)->where('kd_jns_op', $kd_jns_op)->delete();

            foreach ($arrayLspop as $garr) {
                $nilai_individu = $garr['nilai_individu'];
                // log::info('nilai individu :' . $nilai_individu);
                if ($nilai_individu > 0) {
                    $res = Datnilaiindividu::create([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'no_bng' => $garr['no_bng'],
                        'no_formulir_individu' => $garr['no_formulir'],
                        'nilai_individu' => $nilai_individu,
                        'tgl_penilaian_individu' => $now,
                        'nip_penilai_individu' => self::nip(),
                        'tgl_pemeriksaan_individu' => $now,
                        'nip_pemeriksa_individu' => self::nip(),
                        'tgl_rekam_nilai_individu' => $now,
                        'nip_perekam_individu' => self::nip()
                    ]);
                    // log::info($res);
                }
            }
        } else {
        }



        return [
            'lspop' => $arrayLspop,
            'bangunan' => $arrayBng,
            'fasilitas' => $arrayFasilitas,
            'jpb' => $arrayJpb
        ];
    }


    public static function pendataanObjekPajak(Request $request, $spop)
    {

        $now = Carbon::now();
        $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
        $kd_propinsi = substr($nop_proses, 0, 2);
        $kd_dati2 = substr($nop_proses, 2, 2);
        $kd_kecamatan = substr($nop_proses, 4, 3);
        $kd_kelurahan = substr($nop_proses, 7, 3);
        $kd_blok = substr($nop_proses, 10, 3);
        $no_urut = substr($nop_proses, 13, 4);
        $kd_jns_op = substr($nop_proses, 17, 1);

        // array objek pajak
        $vObject = [
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'subjek_pajak_id' => $spop['subjek_pajak_id'],
            'total_luas_bumi' => $spop['luas_bumi'],
            'no_formulir_spop' => $spop['no_formulir'],
            'no_persil' => $spop['no_persil'],
            'jalan_op' => $spop['jalan_op'],
            'blok_kav_no_op' => $spop['blok_kav_no_op'],
            'rw_op' => $spop['rw_op'],
            'rt_op' => $spop['rt_op'],
            'kd_status_cabang' => $spop['kd_status_cabang'],
            'kd_status_wp' => $spop['kd_status_wp'],
            'jns_transaksi_op' => $spop['jns_transaksi'],
            'tgl_pendataan_op' => $now,
            'tgl_pemeriksaan_op' => $now,
            'tgl_perekaman_op' => $now,
            'nip_pemeriksa_op' => self::nip(),
            'nip_pendata' => self::nip(),
            'nip_perekam_op' => self::nip()
        ];




        // array dat_op_bumi
        $vBumi = [
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'no_bumi' => 1,
            'kd_znt' => $request->kd_znt,
            'luas_bumi' => $spop['luas_bumi'],
            'jns_bumi' => $request->jns_bumi,
        ];

        $where = [
            ['kd_propinsi', '=', $kd_propinsi],
            ['kd_dati2', '=', $kd_dati2],
            ['kd_kecamatan', '=', $kd_kecamatan],
            ['kd_kelurahan', '=', $kd_kelurahan],
            ['kd_blok', '=', $kd_blok],
            ['no_urut', '=', $no_urut],
            ['kd_jns_op', '=', $kd_jns_op],
        ];


        // znt op
        $tahun_znt = date('Y');
        /*   $csppt = DB::connection("oracle_satutujuh")->table("SPPT")->select(db::raw("status_pembayaran_sppt"))
            ->where('thn_pajak_sppt', $tahun_znt)
            ->where($where)->first();
        if ($csppt != null) {
            if ($csppt->status_pembayaran_sppt == '1') {
                $tahun_znt = date('Y') + 1;
            }
        }
 */
        // dd($tahun_znt);

        $dataZnt = [
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'thn_znt' => $tahun_znt,
            'kd_znt' => $vBumi['kd_znt'],
            'created_at' => carbon::now(),
            'created_by' => Auth()->user()->id ?? '',
        ];

        $ba = DB::connection('oracle_satutujuh')->table(db::raw("dat_op_znt"))->where('thn_znt', $tahun_znt)
            ->where($where)->get()->count();

        // dd($ba);
        if ($ba > 0) {
            DB::connection('oracle_satutujuh')->table(db::raw("dat_op_znt"))->where('thn_znt', $tahun_znt)
                ->where($where)->update($dataZnt);
        } else {
            DB::connection('oracle_satutujuh')->table(db::raw("dat_op_znt"))
                ->insert($dataZnt);
        }

        // dat objek pajak
        $b = DB::connection('oracle_satutujuh')->table(db::raw("dat_objek_pajak"))->where($where)->get()->count();
        if ($b > 0) {
            DB::connection('oracle_satutujuh')->table(db::raw("dat_objek_pajak"))->where($where)->update($vObject);
        } else {
            DB::connection('oracle_satutujuh')->table(db::raw("dat_objek_pajak"))->insert($vObject);
        }


        // dat op bumi
        $a = DB::connection('oracle_satutujuh')->table(db::raw("dat_op_bumi"))->where($where)->get()->count();
        if ($a > 0) {
            DB::connection('oracle_satutujuh')->table(db::raw("dat_op_bumi"))->where($where)->update($vBumi);
        } else {
            DB::connection('oracle_satutujuh')->table(db::raw("dat_op_bumi"))->insert($vBumi);
        }


        return [
            'dat_op_bumi' => $vBumi,
            'dat_objek_pajak' => $vObject
        ];
    }

    public static function blokirNop($data)
    {
        $kd_propinsi = $data['kd_propinsi'];
        $kd_dati2 = $data['kd_dati2'];
        $kd_kecamatan = $data['kd_kecamatan'];
        $kd_kelurahan = $data['kd_kelurahan'];
        $kd_blok = $data['kd_blok'];
        $no_urut = $data['no_urut'];
        $kd_jns_op = $data['kd_jns_op'];
        $tahun = $data['thn_pajak_sppt'];


        $where = [
            ['kd_propinsi', '=', $kd_propinsi],
            ['kd_dati2', '=', $kd_dati2],
            ['kd_kecamatan', '=', $kd_kecamatan],
            ['kd_kelurahan', '=', $kd_kelurahan],
            ['kd_blok', '=', $kd_blok],
            ['no_urut', '=', $no_urut],
            ['kd_jns_op', '=', $kd_jns_op],
            ['thn_pajak_sppt', '=', $tahun],
            ['status_pembayaran_sppt' => '0']
        ];
        $res = DB::connection('oracle_satutujuh')->table('sppt')->where($where)->update(['status_pembayaran_sppt' => '3']);
        return $res;
    }


    public static function prePecah(Request $request)
    {

        $jenis_layanan_id = self::jenisLayanan($request);
        $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
        $nop_asal = preg_replace('/[^0-9]/', '', $request->nop_asal);
        $blokir = [];
        $kd_propinsi = substr($nop_asal, 0, 2);
        $kd_dati2 = substr($nop_asal, 2, 2);
        $kd_kecamatan = substr($nop_asal, 4, 3);
        $kd_kelurahan = substr($nop_asal, 7, 3);
        $kd_blok = substr($nop_asal, 10, 3);
        $no_urut = substr($nop_asal, 13, 4);
        $kd_jns_op = substr($nop_asal, 17, 1);
        if ($jenis_layanan_id == 6 && $nop_proses == $nop_asal) {
            //blokir semua tunggakan induk
            $where = [
                ['kd_propinsi', '=', $kd_propinsi],
                ['kd_dati2', '=', $kd_dati2],
                ['kd_kecamatan', '=', $kd_kecamatan],
                ['kd_kelurahan', '=', $kd_kelurahan],
                ['kd_blok', '=', $kd_blok],
                ['no_urut', '=', $no_urut],
                ['kd_jns_op', '=', $kd_jns_op],
                ['status_pembayaran_sppt', '=', '0']
            ];
            self::prosesBackupSppt($request, $nop_asal);

            DB::connection('oracle_satutujuh')->table('sppt')->where($where)->update(['status_pembayaran_sppt' => '3']);
            /* $csk = DB::connection('oracle_satutujuh')->table('sppt_koreksi')->where([
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'thn_pajak_sppt' => date('Y')
            ])->select(db::raw("count(1) res"))->first()->res; */

            /* if ($csk == 0) {
                $ntr = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                    ->select(db::raw("'KP'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'KP'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
                    ->whereraw("no_transaksi like 'KP'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi;
                DB::connection('oracle_satutujuh')->table('sppt_koreksi')
                    ->insert([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => date('Y'),
                        'keterangan' => 'Mutasi pecah',
                        'verifikasi_kode' => '1',
                        'no_surat' => $ntr,
                        'tgl_surat' => db::raw("sysdate"),
                        'verifikasi_by' => auth()->user()->id,
                        'verifikasi_keterangan' => 'Proses penelitian/pendataan',
                        'no_transaksi' => $ntr,
                        'created_at' => db::raw("sysdate"),
                        'created_by' => auth()->user()->id,
                    ]);
            } else {
                DB::connection('oracle_satutujuh')->table('sppt_koreksi')
                    ->where([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => date('Y')
                    ])
                    ->update([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => date('Y'),
                        'keterangan' => 'Mutasi pecah',
                        'verifikasi_kode' => '1',
                        'verifikasi_keterangan' => 'Proses penelitian/pendataan'

                    ]);
            } */
        }

        // nir induk
        $nir = DB::connection('oracle_satutujuh')->table('sppt')->select(db::raw("sppt.thn_pajak_sppt tahun, 
                                                                            (select * from ( select kd_znt 
                                                                            from dat_nir
                                                                            where kd_kecamatan=sppt.kd_kecamatan
                                                                            and kd_kelurahan=sppt.kd_kelurahan 
                                                                            and thn_nir_znt=sppt.thn_pajak_sppt 
                                                                            and kelas_tanah.nilai_min_tanah<=nir and kelas_tanah.nilai_max_tanah>=nir
                                                                            order by regexp_replace(kd_znt, '[^0-9]', '') nulls first) where rownum=1
                                                                            ) kd_znt"))
            ->join("KELAS_TANAH", function ($join) {
                $join->on('kelas_tanah.kd_Kls_tanah', '=', 'sppt.kd_kls_tanah')
                    ->on('kelas_tanah.thn_awal_kls_tanah', '=', 'sppt.thn_awal_kls_tanah');
            })
            ->where('kd_propinsi', $kd_propinsi)
            ->where('kd_dati2', $kd_dati2)
            ->where('kd_kecamatan', $kd_kecamatan)
            ->where('kd_kelurahan', $kd_kelurahan)
            ->where('kd_blok', $kd_blok)
            ->where('no_urut', $no_urut)
            ->where('kd_jns_op', $kd_jns_op)->pluck('kd_znt', 'tahun')->toarray();

        return $nir;
    }


    public static function prosesBackupSppt(Request $request, $nop)
    {
        $nop_proses = preg_replace('/[^0-9]/', '', $nop);
        $kd_propinsi = substr($nop_proses, 0, 2);
        $kd_dati2 = substr($nop_proses, 2, 2);
        $kd_kecamatan = substr($nop_proses, 4, 3);
        $kd_kelurahan = substr($nop_proses, 7, 3);
        $kd_blok = substr($nop_proses, 10, 3);
        $no_urut = substr($nop_proses, 13, 4);
        $kd_jns_op = substr($nop_proses, 17, 1);
        $tahun = self::tahun();
        $where = [
            ['kd_propinsi', '=', $kd_propinsi],
            ['kd_dati2', '=', $kd_dati2],
            ['kd_kecamatan', '=', $kd_kecamatan],
            ['kd_kelurahan', '=', $kd_kelurahan],
            ['kd_blok', '=', $kd_blok],
            ['no_urut', '=', $no_urut],
            ['kd_jns_op', '=', $kd_jns_op],
            ['thn_pajak_sppt', '=', $tahun]
        ];

        $gs = DB::connection('oracle_satutujuh')->table('sppt')->where($where)->first();
        if ($gs) {
            $bkp = [
                'layanan_objek_id' => trim($request->lhp_id),
                'kd_propinsi' => trim($gs->kd_propinsi),
                'kd_dati2' => trim($gs->kd_dati2),
                'kd_kecamatan' => trim($gs->kd_kecamatan),
                'kd_kelurahan' => trim($gs->kd_kelurahan),
                'kd_blok' => trim($gs->kd_blok),
                'no_urut' => trim($gs->no_urut),
                'kd_jns_op' => trim($gs->kd_jns_op),
                'thn_pajak_sppt' => trim($gs->thn_pajak_sppt),
                'nm_wp_sppt' => trim($gs->nm_wp_sppt),
                'jln_wp_sppt' => trim($gs->jln_wp_sppt),
                'blok_kav_no_wp_sppt' => trim($gs->blok_kav_no_wp_sppt),
                'rw_wp_sppt' => trim($gs->rw_wp_sppt),
                'rt_wp_sppt' => trim($gs->rt_wp_sppt),
                'kelurahan_wp_sppt' => trim($gs->kelurahan_wp_sppt),
                'kota_wp_sppt' => trim($gs->kota_wp_sppt),
                'kd_pos_wp_sppt' => trim($gs->kd_pos_wp_sppt),
                'npwp_sppt' => trim($gs->npwp_sppt),
                'no_persil_sppt' => trim($gs->no_persil_sppt),
                'tgl_jatuh_tempo_sppt' => trim($gs->tgl_jatuh_tempo_sppt),
                'luas_bumi_sppt' => trim($gs->luas_bumi_sppt),
                'luas_bng_sppt' => trim($gs->luas_bng_sppt),
                'njop_bumi_sppt' => trim($gs->njop_bumi_sppt),
                'njop_bng_sppt' => trim($gs->njop_bng_sppt),
                'njop_sppt' => trim($gs->njop_sppt),
                'njoptkp_sppt' => trim($gs->njoptkp_sppt),
                'pbb_terhutang_sppt' => trim($gs->pbb_terhutang_sppt),
                'pbb_yg_harus_dibayar_sppt' => trim($gs->pbb_yg_harus_dibayar_sppt)
            ];
            SpptBkp::create($bkp);
        }
        return $gs;
    }

    public static function pembatalanGabung(Request $request, $formulirspop)
    {
        $nomorspop = (int)$formulirspop + 2;

        if (isset($request->nop_pembatalan)) {
            // pembatalan nop gabung
            $newNop = preg_replace('/[^0-9]/', '', $request->nop_proses);
            $indukGabung = [
                'layanan_objek_id' => $request->lhp_id,
                'kd_propinsi' => substr($newNop, 0, 2),
                'kd_dati2' => substr($newNop, 2, 2),
                'kd_kecamatan' => substr($newNop, 4, 3),
                'kd_kelurahan' => substr($newNop, 7, 3),
                'kd_blok' => substr($newNop, 10, 3),
                'no_urut' => substr($newNop, 13, 4),
                'kd_jns_op' => substr($newNop, 17, 1),
            ];

            $induk = HistoryMutasiGabung::create($indukGabung);
            foreach ($request->nop_pembatalan as  $i => $row) {


                $getnomor = self::NomorFormulir('1');
                $nomorspop = $getnomor['nomor'];
                self::prosesBackupSppt($request, $row);
                $kd_propinsi = substr($row, 0, 2);
                $kd_dati2 = substr($row, 2, 2);
                $kd_kecamatan = substr($row, 4, 3);
                $kd_kelurahan = substr($row, 7, 3);
                $kd_blok = substr($row, 10, 3);
                $no_urut = substr($row, 13, 4);
                $kd_jns_op = substr($row, 17, 1);

                $detailInduk = [
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'history_mutasi_gabung_id' => $induk->id
                ];
                GabungDetail::create($detailInduk);

                $dataspoppembatalan = (array) DB::connection("oracle_satutujuh")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,0 LUAS_BUMI,JNS_BUMI 
                                        FROM DAT_OBJEK_PAJAK A
                                        JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                                        JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                                        WHERE a.kd_kecamatan='$kd_kecamatan'
                                        AND a.kd_kelurahan='$kd_kelurahan'
                                        AND a.kd_blok='$kd_blok'
                                        AND a.no_urut='$no_urut'
                                        AND a.kd_jns_op='$kd_jns_op'"))[0];
                $dataspoppembatalan['no_formulir'] = $nomorspop;
                $dataspoppembatalan['tgl_pendataan_op'] =  Carbon::now();
                $dataspoppembatalan['nip_pendata'] = self::nip();
                $dataspoppembatalan['tgl_pemeriksaan_op'] = Carbon::now();
                $dataspoppembatalan['nip_pemeriksa_op'] = self::nip();
                $dataspoppembatalan['tgl_perekaman_op'] = Carbon::now();
                $dataspoppembatalan['nip_perekam_op'] = self::nip();
                $spoppembatalan = array_change_key_case($dataspoppembatalan, CASE_UPPER);

                self::ProsesSpop($spoppembatalan);

                // spop simpbb
                $spoppembatalan_sim_pbb = $spoppembatalan;
                $spoppembatalan_sim_pbb['jenis_layanan_id'] = null;
                $spoppembatalan_sim_pbb['layanan_objek_id'] = $request->lhp_id;
                $spoppembatalan_sim_pbb['created_at'] = Carbon::now();
                $spoppembatalan_sim_pbb['created_by'] = Auth()->user()->id;
                $spoppembatalan_sim_pbb['updated_at'] = Carbon::now();
                $spoppembatalan_sim_pbb['updated_by'] = Auth()->user()->id;
                DB::table('tbl_spop')->insert($spoppembatalan_sim_pbb);

                // log::info($spoppembatalan);
                $vObject = [
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'subjek_pajak_id' => $dataspoppembatalan['subjek_pajak_id'],
                    'no_formulir_spop' => $dataspoppembatalan['no_formulir'],
                    'no_persil' => $dataspoppembatalan['no_persil'],
                    'jalan_op' => $dataspoppembatalan['jalan_op'],
                    'blok_kav_no_op' => $dataspoppembatalan['blok_kav_no_op'],
                    'rw_op' => $dataspoppembatalan['rw_op'],
                    'rt_op' => $dataspoppembatalan['rt_op'],
                    'kd_status_cabang' => $dataspoppembatalan['kd_status_cabang'],
                    'kd_status_wp' => $dataspoppembatalan['kd_status_wp'],
                    'jns_transaksi_op' => $dataspoppembatalan['jns_transaksi'],
                    'tgl_pendataan_op' => $dataspoppembatalan['tgl_pendataan_op'],
                    'tgl_pemeriksaan_op' => $dataspoppembatalan['tgl_pemeriksaan_op'],
                    'nip_pemeriksa_op' => $dataspoppembatalan['nip_pemeriksa_op']
                ];
                self::ProsesObjek($vObject);
                // unflag history


                /*   // cek dulu 
                $csk = DB::connection('oracle_satutujuh')->table('sppt_koreksi')->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_pajak_sppt' => date('Y')
                ])->select(db::raw("count(1) res"))->first()->res;

                if ($csk == 0) {
                    $ntr = PembatalanObjek::NoTransaksi('PL', '07');
                    DB::connection('oracle_satutujuh')->table('sppt_koreksi')
                        ->insert([
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'thn_pajak_sppt' => date('Y'),
                            'keterangan' => 'Mutasi gabung ke ' . formatnop(onlyNumber($request->nop_proses)),
                            'verifikasi_kode' => '1',
                            'no_surat' => $ntr,
                            'tgl_surat' => db::raw("sysdate"),
                            'verifikasi_by' => auth()->user()->id,
                            'verifikasi_keterangan' => 'Proses penelitian/pendataan',
                            'no_transaksi' => $ntr,
                            'created_at' => db::raw("sysdate"),
                            'created_by' => auth()->user()->id,
                            'jns_koreksi' => '1'
                        ]);
                } else {
                    DB::connection('oracle_satutujuh')->table('sppt_koreksi')
                        ->where([
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'thn_pajak_sppt' => date('Y')
                        ])
                        ->update([
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'thn_pajak_sppt' => date('Y'),
                            'keterangan' => 'Mutasi gabung ke ' . formatnop(onlyNumber($request->nop_proses)),
                            'verifikasi_kode' => '1',
                            'verifikasi_keterangan' => 'Proses penelitian/pendataan'
                        ]);
                } */

                // update sppt status=3
                /* DB::connection('oracle_satutujuh')->table('sppt')
                    ->where('kd_propinsi', $kd_propinsi)
                    ->where('kd_dati2', $kd_dati2)
                    ->where('kd_kecamatan', $kd_kecamatan)
                    ->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_blok', $kd_blok)
                    ->where('no_urut', $no_urut)
                    ->where('kd_jns_op', $kd_jns_op)
                    ->where('thn_pajak_sppt', date('Y'))->update(['status_pembayaran_sppt' => 3]); */

                // batalkan bila tidak ada pembayaran
                DB::connection('oracle_satutujuh')->table('sppt')
                    ->whereraw("status_pembayaran_sppt <>'1' and thn_pajak_sppt='" . date('Y') . "' and  kd_propinsi='$kd_propinsi' and
                                kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and
                                kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' ")->delete();


                $loi = $request->layanan_objek_batal[$i];
                $penelitian = Layanan_objek::findorfail($loi);
                $penelitian->update([
                    'nomor_formulir' => $dataspoppembatalan['no_formulir'],
                    'pemutakhiran_at' => Carbon::now(),
                    'pemutakhiran_by' => auth()->user()->id
                ]);
                DB::commit();
            }
        }
    }

    public static function NotaPerhitunganDua($nop, $tagihan)
    {
        $kd_propinsi = $nop['kd_propinsi'];
        $kd_dati2 = $nop['kd_dati2'];
        $kd_kecamatan = $nop['kd_kecamatan'];
        $kd_kelurahan = $nop['kd_kelurahan'];
        $kd_blok = $nop['kd_blok'];
        $no_urut = $nop['no_urut'];
        $kd_jns_op = $nop['kd_jns_op'];
        $nm_wp = $nop['nm_wp'];
        $layanan_objek_id = $nop['layanan_objek_id'];
        $year = self::tahun();


        $cl = DB::table('data_billing')
            ->whereIn('data_billing_id', function ($query) use ($layanan_objek_id) {
                $query->select('data_billing_id')
                    ->from('nota_perhitungan')
                    ->where('layanan_objek_id', $layanan_objek_id);
            })
            ->select('kd_status')
            ->orderByDesc('created_at')
            ->first();

        if ($cl && $cl->kd_status == '0') {
            DB::transaction(function () use ($layanan_objek_id) {
                DB::table('data_billing')
                    ->whereNull('deleted_at')
                    ->where('kd_status', '0')
                    ->whereIn('data_billing_id', function ($query) use ($layanan_objek_id) {
                        $query->select('data_billing_id')
                            ->from('nota_perhitungan')
                            ->where('layanan_objek_id', $layanan_objek_id);
                    })
                    ->update(['deleted_at' => DB::raw('SYSDATE')]);
            });
        }


        /*   $cl = DB::table('data_billing')->whereraw("data_billing_id in (select data_billing_id 
                                        from nota_perhitungan
                                        where layanan_objek_id='$layanan_objek_id')")
            ->select("kd_status")->orderByraw("created_at desc")->first();
        if ($cl) {
            if ($cl->kd_status == '0') {

                DB::statement(db::raw("begin update data_billing set deleted_at=sysdate
                                        where deleted_at is null and data_billing_id in (select data_billing_id 
                                        from nota_perhitungan
                                        where layanan_objek_id='$layanan_objek_id') and kd_status='0' ;  commit;
                                    end;"));
            }
        } */
        $statu = $cl->kd_status ?? 0;
        if ($statu == '0' or $cl == null) {
            $urut = DB::table('data_billing')
                ->select(db::raw("nvl(max( CAST(no_urut AS NUMBER)),0) nomer"))->where('tahun_pajak', $year)
                ->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_jns_op', 3)
                ->first();
            $kode = $urut->nomer;

            $no_urut = sprintf("%04s", $kode + 1);
            $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . '999' . $no_urut . '3';
            $lokasi = DB::connection('oracle_dua')->table('ref_kelurahan')
                ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                ->selectraw("nm_kecamatan,nm_kelurahan")
                ->whereraw("ref_kecamatan.KD_KECAMATAN ='$kd_kecamatan' and KD_KELURAHAN = '$kd_kelurahan'")->first();
            $kecamatan = $lokasi->nm_kecamatan;
            $kelurahan = $lokasi->nm_kelurahan;
            $ta = date('Y') . '1130';
            // $ta='20341231';
            if (strtotime(date('Ymd'))   >= strtotime(date('Y') . '1130')) {
                $ta = date('Y') . '1230';
            }

            $exp = new Carbon($ta);

            $setDatabiling = array(
                "KD_PROPINSI" => $kd_propinsi,
                "KD_DATI2" => $kd_dati2,
                "KD_KECAMATAN" => $kd_kecamatan,
                "KD_KELURAHAN" => $kd_kelurahan,
                "KD_BLOK" => '999',
                "NO_URUT" => $no_urut,
                "KD_JNS_OP" => '3',
                "TAHUN_PAJAK" => $year,
                "KD_STATUS" => "0",
                "NAMA_WP" => $nm_wp,
                "NAMA_KELURAHAN" => $kelurahan,
                "NAMA_KECAMATAN" => $kecamatan,
                "KOBIL" => $kobil,
                "lhp" => 1
            );
            $Data_billing = Data_billing::create($setDatabiling);

            $getId = $Data_billing->data_billing_id;
            $billing_kolektif = [];
            foreach ($tagihan as $tag) {
                $billing_kolektif[] = [
                    'data_billing_id' => $getId,
                    'kd_propinsi' => $tag['kd_propinsi'],
                    'kd_dati2' => $tag['kd_dati2'],
                    'kd_kecamatan' => $tag['kd_kecamatan'],
                    'kd_kelurahan' => $tag['kd_kelurahan'],
                    'kd_blok' => $tag['kd_blok'],
                    'no_urut' => $tag['no_urut'],
                    'kd_jns_op' => $tag['kd_jns_op'],
                    'tahun_pajak' => $tag['tahun_pajak'],
                    'pbb' => $tag['pbb'],
                    'nm_wp' => $tag['nm_wp'],
                    'pokok' => $tag['pokok'],
                    'denda' => $tag['denda'],
                    'total' => $tag['total'],
                    'expired_at' => $exp
                ];
            }
            Billing_kolektif::insert($billing_kolektif);

            // nota
            $peg = pegawaiStruktural::where('kode', '02')->first();
            $nota = Notaperhitungan::create([
                'tanggal' => Carbon::now(),
                'kota' => 'kepanjen',
                'pegawai' => $peg->nama,
                'nip' => $peg->nip,
                'jabatan' => $peg->jabatan,
                'pangkat' => '-',
                'kobil' => $kobil,
                'data_billing_id' => $getId,
                'layanan_objek_id' => $layanan_objek_id
            ]);
            // UPDATE EXPIRED
            Data_billing::where('kobil', $kobil)->update(['expired_at' => $exp]);
        } else {
            $nota = [];
        }
        return $nota;
    }


    public static function prosesNotaPerhitungan($nopData, $spop)
    {
        try {
            //code...
            $year = self::tahun();

            $urut = DB::table('data_billing')
                ->select(db::raw("nvl(max( CAST(no_urut AS NUMBER)),0) nomer"))->where('tahun_pajak', $year)
                ->where('kd_kecamatan', $nopData[0]['kd_kecamatan'])->where('kd_kelurahan', $nopData[0]['kd_kelurahan'])
                ->where('kd_jns_op', 2)
                ->first();
            $kode = $urut->nomer;
            $no_urut = sprintf("%04s", $kode + 1);
            // }
            $kobil = $nopData[0]['kd_propinsi'] . $nopData[0]['kd_dati2'] . $nopData[0]['kd_kecamatan'] . $nopData[0]['kd_kelurahan'] . '999' . $no_urut . "2";

            $lokasi = DB::connection('oracle_dua')->table('ref_kelurahan')
                ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                ->selectraw("nm_kecamatan,nm_kelurahan")
                ->whereraw("ref_kecamatan.KD_KECAMATAN ='" . $nopData[0]['kd_kecamatan'] . "' and KD_KELURAHAN = '" . $nopData[0]['kd_kelurahan'] . "'")->first();


            // $kecamatan = Kecamatan::find($nopData[0]['kd_kecamatan']);
            $kecamatan = $lokasi->nm_kecamatan;

            // $kelurahan = Kelurahan::where(['KD_KECAMATAN' => $nopData[0]['kd_kecamatan'], 'KD_KELURAHAN' => $nopData[0]['kd_kelurahan']]);
            $kelurahan = $lokasi->nm_kelurahan;

            $ta = date('Y') . '1130';
            if (strtotime(date('Ymd'))   >= strtotime(date('Y') . '1130')) {
                $ta = date('Y') . '1230';
            }

            $exp = new Carbon($ta);

            $setDatabiling = array(
                "KD_PROPINSI" => $nopData[0]['kd_propinsi'],
                "KD_DATI2" => $nopData[0]['kd_dati2'],
                "KD_KECAMATAN" => $nopData[0]['kd_kecamatan'],
                "KD_KELURAHAN" => $nopData[0]['kd_kelurahan'],
                "KD_BLOK" => '999',
                "NO_URUT" => $no_urut,
                "KD_JNS_OP" => 2,
                "TAHUN_PAJAK" => $year,
                "KD_STATUS" => "0",
                "NAMA_WP" => $spop['nm_wp'],
                "NAMA_KELURAHAN" => $kelurahan,
                "NAMA_KECAMATAN" => $kecamatan,
                "KOBIL" => $kobil,
                "lhp" => 1
            );
            $Data_billing = Data_billing::create($setDatabiling);
            // $Data_billing->save();
            $getId = $Data_billing->data_billing_id;

            $billing_kolektif = [];
            $spptpenelitian = [];
            $artahun = [];


            foreach ($nopData as $ar) {
                $artahun[] = $ar['tahun'];
            }

            // lookup oltp
            $oltp = DB::connection('oracle_spo')->table('sppt_oltp')
                ->select(db::raw("nm_wp_sppt,thn_pajak_sppt,pbb_yg_harus_dibayar_sppt pokok,get_denda (
                PBB_YG_HARUS_DIBAYAR_SPPT ,
                      tgl_jatuh_tempo_sppt,
                TRUNC (SYSDATE))   denda,pbb_yg_harus_dibayar_sppt + get_denda (
                    PBB_YG_HARUS_DIBAYAR_SPPT ,
                          tgl_jatuh_tempo_sppt,
                    TRUNC (SYSDATE)) total"))
                ->wherein('thn_pajak_sppt', $artahun)
                ->whereraw("kd_kecamatan='" . $nopData[0]['kd_kecamatan'] . "' 
            and kd_kelurahan='" . $nopData[0]['kd_kelurahan'] . "' and kd_blok='" . $nopData[0]['kd_blok'] . "' 
            and no_urut='" . $nopData[0]['no_urut'] . "' and kd_jns_op='" . $nopData[0]['kd_jns_op'] . "'")->get();
            // log::info($oltp);

            $oltp_pokok = [];
            $oltp_nm_wp_sppt = [];
            $oltp_denda = [];
            $oltp_total = [];

            $oltp_tahun = [];
            foreach ($oltp as $rk) {
                $oltp_tahun[] = $rk->thn_pajak_sppt;
                $oltp_pokok[$rk->thn_pajak_sppt] = $rk->pokok;
                $oltp_nm_wp_sppt[$rk->thn_pajak_sppt] = $rk->nm_wp_sppt;
                $oltp_denda[$rk->thn_pajak_sppt] = $rk->denda;
                $oltp_total[$rk->thn_pajak_sppt] = $rk->total;
            }



            foreach ($nopData as $rg) {
                if (in_array($rg['tahun'], $oltp_tahun)) {

                    $spptpenelitian[] = [
                        'nomor_formulir' => $spop['no_formulir'],
                        'kd_propinsi' => $rg['kd_propinsi'],
                        'kd_dati2' => $rg['kd_dati2'],
                        'kd_kecamatan' => $rg['kd_kecamatan'],
                        'kd_kelurahan' => $rg['kd_kelurahan'],
                        'kd_blok' => $rg['kd_blok'],
                        'no_urut' => $rg['no_urut'],
                        'kd_jns_op' => $rg['kd_jns_op'],
                        'thn_pajak_sppt' => $rg['tahun'],
                        'kobil' => $kobil,
                        'data_billing_id' => $getId
                    ];



                    $pokok = 0;
                    if (in_array($rg['tahun'], array_keys($oltp_pokok))) {
                        $pokok = $oltp_pokok[$rg['tahun']];
                    }

                    $denda = 0;
                    if (in_array($rg['tahun'], array_keys($oltp_denda))) {
                        $denda = $oltp_denda[$rg['tahun']];
                    }

                    $total = 0;
                    if (in_array($rg['tahun'], array_keys($oltp_total))) {
                        $total = $oltp_total[$rg['tahun']];
                    }

                    $nm_wp_sppt = 'WAJIB PAJAK';
                    if (in_array($rg['tahun'], array_keys($oltp_nm_wp_sppt))) {
                        $nm_wp_sppt = $oltp_nm_wp_sppt[$rg['tahun']];
                    }

                    $billing_kolektif[] = [
                        'data_billing_id' => $getId,
                        'kd_propinsi' => $rg['kd_propinsi'],
                        'kd_dati2' => $rg['kd_dati2'],
                        'kd_kecamatan' => $rg['kd_kecamatan'],
                        'kd_kelurahan' => $rg['kd_kelurahan'],
                        'kd_blok' => $rg['kd_blok'],
                        'no_urut' => $rg['no_urut'],
                        'kd_jns_op' => $rg['kd_jns_op'],
                        'tahun_pajak' => $rg['tahun'],
                        'pbb' => $pokok,
                        'nm_wp' => $nm_wp_sppt,
                        'pokok' => $pokok,
                        'denda' => $denda,
                        'total' => $total,
                        'expired_at' => $exp
                    ];
                }
            }

            // Billing_kolektif::create($billing_kolektif);
            if (!empty($billing_kolektif)) {
                // log::info(['bk' => $billing_kolektif]);
                Billing_kolektif::insert($billing_kolektif);
            }


            if (!empty($spptpenelitian)) {
                DB::table('sppt_penelitian')->insert($spptpenelitian);
            }

            // nota
            $peg = pegawaiStruktural::where('kode', '02')->first();
            $nota = Notaperhitungan::create([
                'tanggal' => Carbon::now(),
                'kota' => 'kepanjen',
                'pegawai' => $peg->nama,
                'nip' => $peg->nip,
                'jabatan' => $peg->jabatan,
                'pangkat' => 'PEMBINA',
                'kobil' => $kobil,
                'data_billing_id' => $getId
            ]);
            // UPDATE EXPIRED
            Data_billing::where('kobil', $kobil)->update(['expired_at' => $exp]);

            // log::info($nota);
        } catch (\Throwable $th) {
            log::info($th);
            $nota = [];
        }

        return $nota;
        // }
    }

    public static function prosesPendataan(Request $request)
    {
        $jenis_pendataan_id = $request->jenis_pendataan_id;
        $msg = "";
        if ($request->jenis_pendataan_id != 5) {

            DB::beginTransaction();
            try {
                //code...
                self::pendatanSubjek($request);
                $spop = self::pendataanSpop($request);

                self::pendataanObjekPajak($request, $spop);
                if ($request->jenis_pendataan_id != '7') {
                    self::pendataanLspop($request);
                }

                DB::commit();
                $pendataan = '1';
            } catch (\Throwable $th) {
                DB::rollBack();
                $pendataan = '0';
                $msg .= "Sistem gagal melakukan pendataan ";
                log::emergency($th);
            }

            if ($pendataan == 0) {
                $df = $spop['no_formulir'] ?? '';
                if ($df != '') {
                    DB::connection('oracle_satutujuh')->statement(db::raw("
                begin
                delete from dat_objek_pajak where no_formulir_spop='" . $spop['no_formulir'] . "';
                delete from tbl_spop where no_formulir='" . $spop['no_formulir'] . "';
                end;
            "));
                }

                DB::table('booking_nop')->where('nop_proses', $spop['nop_proses'])->delete();
            }

            // proses penilaian dan penetapan
            $penilaian = '0';
            if ($pendataan == '1') {


                DB::beginTransaction();
                try {
                    self::prosesBackupSppt($request, $request->nop_proses);
                    // penentuan NJOP
                    $objp = preg_replace('/[^0-9]/', '', $request->nop_proses);
                    $obj = [
                        'kd_propinsi' => substr($objp, 0, 2),
                        'kd_dati2' => substr($objp, 2, 2),
                        'kd_kecamatan' => substr($objp, 4, 3),
                        'kd_kelurahan' => substr($objp, 7, 3),
                        'kd_blok' => substr($objp, 10, 3),
                        'no_urut' => substr($objp, 13, 4),
                        'kd_jns_op' => substr($objp, 17, 1),
                    ];

                    $nopPenetapan = [];
                    if (isset($request->mulai_tahun)) {
                        // proses loop 
                        $Lastsppt = DB::connection('oracle_satutujuh')->table('sppt')
                            ->select(db::raw("kd_propinsi, kd_dati2, kd_kecamatan, 
                                kd_kelurahan, kd_blok, no_urut, 
                                kd_jns_op, thn_pajak_sppt, nm_wp_sppt, 
                                jln_wp_sppt, blok_kav_no_wp_sppt, rw_wp_sppt, 
                                rt_wp_sppt, kelurahan_wp_sppt, kota_wp_sppt, 
                                luas_bumi_sppt, luas_bng_sppt, njop_bumi_sppt, 
                                njop_bng_sppt, njop_sppt, njoptkp_sppt, 
                                pbb_terhutang_sppt, pbb_yg_harus_dibayar_sppt,status_pembayaran_sppt"))
                            ->whereraw("sppt.kd_kecamatan='" . $obj['kd_kecamatan'] . "' 
                                and sppt.kd_kelurahan='" . $obj['kd_kelurahan'] . "' 
                                and sppt.kd_blok='" . $obj['kd_blok'] . "' 
                                and sppt.no_urut='" . $obj['no_urut'] . "' 
                                and sppt.kd_jns_op='" . $obj['kd_jns_op'] . "' ")
                            ->wherein('sppt.thn_pajak_sppt', $request->mulai_tahun)
                            ->get();

                        $spptexists = [];
                        foreach ($Lastsppt as $row) {
                            $spptexists[$row->thn_pajak_sppt] = $row->status_pembayaran_sppt;
                        }

                        $sqlpenentuannjop = "";
                        $sqlpenetapan = "";
                        $del_potongan = "";
                        $identifikasi = "";



                        if (!empty($spptexists)) {
                            // bukan data baru
                            foreach ($request->mulai_tahun as $i => $tahun_sppt) {
                                if (in_array($tahun_sppt, array_keys($spptexists))) {

                                    // cek hkpd individu
                                    $hkpd_individu = $request->hkpd_individu[$i] ?? 0;

                                    if ($hkpd_individu > 0) {
                                        $v_kd_kecamatan = $obj['kd_kecamatan'];
                                        $v_kd_kelurahan = $obj['kd_kelurahan'];
                                        $v_kd_blok = $obj['kd_blok'];
                                        $v_no_urut = $obj['no_urut'];
                                        $v_kd_jns_op = $obj['kd_jns_op'];

                                        $inshkpd = [
                                            'kd_propinsi' => '35',
                                            'kd_dati2' => '07',
                                            'kd_kecamatan' => $v_kd_kecamatan,
                                            'kd_kelurahan' => $v_kd_kelurahan,
                                            'kd_blok' => $v_kd_blok,
                                            'no_urut' => $v_no_urut,
                                            'kd_jns_op' => $v_kd_jns_op,
                                            'hkpd' => $hkpd_individu,
                                            'thn_hkpd' => $tahun_sppt,
                                            'created_at' => Carbon::now(),
                                            'created_by' => auth()->user()->id
                                        ];



                                        $hkpd = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                            ->whereraw(" thn_hkpd='$tahun_sppt' and kd_kecamatan='$v_kd_kecamatan' and kd_kelurahan='$v_kd_kelurahan'
                                              and kd_blok='$v_kd_blok' and no_urut='$v_no_urut' and kd_jns_op='$v_kd_jns_op'")->count();

                                        if ($hkpd == 0) {

                                            DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")->insert($inshkpd);
                                            DB::connection("oracle_satutujuh")->commit();
                                        } else {
                                            DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                                ->whereraw(" thn_hkpd='$tahun_sppt' and kd_kecamatan='$v_kd_kecamatan' and kd_kelurahan='$v_kd_kelurahan'
                                            and kd_blok='$v_kd_blok' and no_urut='$v_no_urut' and kd_jns_op='$v_kd_jns_op'")
                                                ->update($inshkpd);
                                            DB::connection("oracle_satutujuh")->commit();
                                        }
                                    }

                                    // jika ada
                                    // cek lunas atau belum
                                    if ($spptexists[$tahun_sppt] <> '1') {
                                        // belum lunas , ditetapkan 
                                        if (date('Y') == $tahun_sppt || ($jenis_layanan_id ?? '') == '6') {
                                            $identifikasi = "Proses A";
                                            $jatuh_tempo = date('Ymd', strtotime($request->tgl_jatuh_tempo[$i]));


                                            // $sqlpenetapan .= " PENENTUAN_NJOP ('35', '07', '" . $obj['kd_kecamatan'] . "', '" . $obj['kd_kelurahan'] . "', '" . $obj['kd_blok'] . "', '" . $obj['no_urut'] . "', '" . $obj['kd_jns_op'] . "', '" . $tahun_sppt . "', '1' ); ";
                                            // $sqlpenetapan .= " PENETAPAN_NOP_20 ( '35', '07', '" . $obj['kd_kecamatan'] . "', '" . $obj['kd_kelurahan'] . "', '" . $obj['kd_blok'] . "', '" . $obj['no_urut'] . "', '" . $obj['kd_jns_op'] . "', '" . $tahun_sppt . "', to_date('" . date('Ymd') . "','yyyymmdd'),  to_date('" . $jatuh_tempo . "','yyyymmdd'), '" . self::nip() . "' );";
                                            $tgl_terbit = date('Ymd');
                                            PenetapanSppt::proses('35', '07', $obj['kd_kecamatan'], $obj['kd_kelurahan'], $obj['kd_blok'], $obj['no_urut'],  $obj['kd_jns_op'], $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                                            $penetapan = $obj;
                                            $penetapan['tahun'] = $tahun_sppt;
                                            $penetapan['nip'] = self::nip();
                                            $penetapan['terbit'] = date('Ymd');
                                            $penetapan['tempo'] = $jatuh_tempo;
                                            $nopPenetapan[] = $penetapan;
                                        } else {
                                            $jatuh_tempo = $request->tgl_jatuh_tempo[$i];
                                            $penetapan = $obj;
                                            $penetapan['tahun'] = $tahun_sppt;
                                            $penetapan['nip'] = self::nip();
                                            $penetapan['terbit'] = date('Ymd');
                                            $penetapan['tempo'] = $jatuh_tempo;
                                            $nopPenetapan[] = $penetapan;

                                            $identifikasi = "Proses AA";
                                            DB::connection('oracle_satutujuh')->table('sppt')
                                                ->whereraw("sppt.kd_kecamatan='" . $obj['kd_kecamatan'] . "' 
                                                and sppt.kd_kelurahan='" . $obj['kd_kelurahan'] . "' 
                                                and sppt.kd_blok='" . $obj['kd_blok'] . "' 
                                                and sppt.no_urut='" . $obj['no_urut'] . "' 
                                                and sppt.kd_jns_op='" . $obj['kd_jns_op'] . "' ")
                                                ->where('sppt.thn_pajak_sppt',  $tahun_sppt)->update(['status_pembayaran_sppt' => 0, 'tgl_jatuh_tempo_sppt' => new carbon($jatuh_tempo)]);
                                        }
                                    }
                                } else {
                                    $jatuh_tempo = date('Ymd', strtotime($request->tgl_jatuh_tempo[$i]));
                                    // $sqlpenetapan .= " PENENTUAN_NJOP ('35', '07', '" . $obj['kd_kecamatan'] . "', '" . $obj['kd_kelurahan'] . "', '" . $obj['kd_blok'] . "', '" . $obj['no_urut'] . "', '" . $obj['kd_jns_op'] . "', '" . $tahun_sppt . "', '1' ); ";
                                    // $sqlpenetapan .= " PENETAPAN_NOP_20 ( '35', '07', '" . $obj['kd_kecamatan'] . "', '" . $obj['kd_kelurahan'] . "', '" . $obj['kd_blok'] . "', '" . $obj['no_urut'] . "', '" . $obj['kd_jns_op'] . "', '" . $tahun_sppt . "', to_date('" . date('Ymd') . "','yyyymmdd'),  to_date('" . $jatuh_tempo . "','yyyymmdd'), '" . self::nip() . "' );";
                                    $tgl_terbit = date('Ymd');
                                    PenetapanSppt::proses('35', '07', $obj['kd_kecamatan'], $obj['kd_kelurahan'], $obj['kd_blok'], $obj['no_urut'],  $obj['kd_jns_op'], $tahun_sppt, $tgl_terbit, $jatuh_tempo);

                                    $penetapan = $obj;
                                    $penetapan['tahun'] = $tahun_sppt;
                                    $penetapan['nip'] = self::nip();
                                    $penetapan['terbit'] = date('Ymd');
                                    $penetapan['tempo'] = $jatuh_tempo;

                                    $nopPenetapan[] = $penetapan;
                                    $identifikasi = "Proses B";
                                }


                                $del_potongan .= " hitung_potongan('" . $obj['kd_kecamatan'] . "','" . $obj['kd_kelurahan'] . "','" . $obj['kd_blok'] . "','" . $obj['no_urut'] . "','" . $obj['kd_jns_op'] . "','" . $tahun_sppt . "');";
                            }
                        } else {

                            // dd($request->all());
                            // data baru


                            foreach ($request->mulai_tahun as $i => $tahun_sppt) {
                                $identifikasi = "Proses C";
                                $jatuh_tempo = date('Ymd', strtotime($request->tgl_jatuh_tempo[$i]));
                                $tgl_terbit = date('Ymd');
                                PenetapanSppt::proses('35', '07', $obj['kd_kecamatan'], $obj['kd_kelurahan'], $obj['kd_blok'], $obj['no_urut'],  $obj['kd_jns_op'], $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                                $penetapan = $obj;
                                $penetapan['tahun'] = $tahun_sppt;
                                $penetapan['nip'] = self::nip();
                                $penetapan['terbit'] = date('Ymd');
                                $penetapan['tempo'] = $jatuh_tempo;
                                $nopPenetapan[] = $penetapan;
                                $del_potongan .= " hitung_potongan('" . $obj['kd_kecamatan'] . "','" . $obj['kd_kelurahan'] . "','" . $obj['kd_blok'] . "','" . $obj['no_urut'] . "','" . $obj['kd_jns_op'] . "','" . $tahun_sppt . "');";
                            }
                        }

                        if ($sqlpenetapan <> '') {
                            DB::connection("oracle_satutujuh")->statement("begin 
                            " . $sqlpenetapan . "
                        end;");
                        }

                        if ($del_potongan <> '') {

                            DB::connection('oracle_satutujuh')->statement(db::raw("Begin " . $del_potongan . " commit; end;"));
                        }
                        // cek nota atau tidak
                        $is_nota = $request->is_nota ?? 0;


                        // log::info($nopPenetapan);
                        if (!empty($nopPenetapan) && in_array($jenis_pendataan_id, [3, 4, 6]) && $is_nota == '1') {
                            // log::info('pakai nota');
                            // self::prosesNotaPerhitungan($nopPenetapan, $spop);
                        } else {
                            // log::info('tidak nota');
                        }
                    } else {
                        $obj['tahun'] = self::tahun();
                        self::PenentuanNjop($obj);
                    }


                    DB::commit();
                    $penilaian = 1;
                } catch (\Throwable $th) {
                    //throw $th;
                    log::emergency($th);
                    DB::rollBack();
                    $msg .= "Sistem gagal melakukan penilaian ";
                }
            }
        } else {
            // proses pembatalan

            self::pendatanSubjek($request);
            $spop = self::pendataanSpop($request);
            self::pendataanObjekPajak($request, $spop);

            $pendataan = '1';
            $penilaian = '1';
            $msg = "Berhasil di proses";
        }

        $result = [
            'pendataan' => $pendataan,
            'penilaian' => $penilaian,
            'msg' => $msg
        ];

        return $result;
    }


    public static  function prosesPenelitianDua(Request $request)
    {   // pendataan 
        $msg = "";
        $jenis_pelayanan = $request->jenis_pelayanan;
        $nop_asal = onlyNumber($request->nop_asal);
        $nop_proses = onlyNumber($request->nop_proses);
        $tahun_penetapan = $request->mulai_tahun ?? [];


        $penetapan = [];
        foreach ($tahun_penetapan as $i => $it) {
            $penetapan[$it] = $request->tgl_jatuh_tempo[$i];
        }

        if ($jenis_pelayanan == '1') {
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);
            // cek data sppt yang di terbitkan tahun ini untuk data abaru
            DB::connection("oracle_satutujuh")->table("sppt")
                ->whereraw("to_char(tgl_terbit_sppt,'yyyy')='" . date('Y') . "'")
                ->whereRaw("sppt.kd_kecamatan='$kd_kecamatan'
                and sppt.kd_kelurahan='$kd_kelurahan'
                and sppt.kd_blok='$kd_blok' 
                and sppt.no_urut='$no_urut'
                and sppt.kd_jns_op='$kd_jns_op'")
                ->delete();
        }

        // proses pendataan 
        DB::beginTransaction();
        try {
            //code...
            self::pendatanSubjek($request);
            $spop = self::pendataanSpop($request);
            self::pendataanObjekPajak($request, $spop);
            self::pendataanLspop($request);
            Layanan_objek::whereid($request->lhp_id)->update([
                'nomor_formulir' => $spop['no_formulir'],
                'pemutakhiran_at' => Carbon::now(),
                'pemutakhiran_by' => auth()->user()->id
            ]);

            DB::commit();
            $pendataan = '1';
        } catch (\Throwable $th) {
            DB::rollBack();

            $pendataan = '0';
            $msg .= "Sistem gagal melakukan pendataan ";
            log::emergency($th);
        }

        // $pendataan = 1;
        $penilaian = '0';
        if ($pendataan == '1') {
            DB::beginTransaction();
            try {

                if ($jenis_pelayanan != '10') {
                    self::prosesBackupSppt($request, $request->nop_proses);
                }

                $kd_propinsi = substr($nop_proses, 0, 2);
                $kd_dati2 = substr($nop_proses, 2, 2);
                $kd_kecamatan = substr($nop_proses, 4, 3);
                $kd_kelurahan = substr($nop_proses, 7, 3);
                $kd_blok = substr($nop_proses, 10, 3);
                $no_urut = substr($nop_proses, 13, 4);
                $kd_jns_op = substr($nop_proses, 17, 1);

                $layanan_objek_id = $request->lhp_id;
                // penetapan nya
                $sql_penetapan = "";
                $jns_bumi = $request->jns_bumi;
                $ArTagihan = [];


                // proses khusus di setiap masing2 jenis permohonan
                switch ($jenis_pelayanan) {
                    case '6':
                        # Mutasi pecah
                        $kd_propinsi_asal = substr($nop_asal, 0, 2);
                        $kd_dati2_asal = substr($nop_asal, 2, 2);
                        $kd_kecamatan_asal = substr($nop_asal, 4, 3);
                        $kd_kelurahan_asal = substr($nop_asal, 7, 3);
                        $kd_blok_asal = substr($nop_asal, 10, 3);
                        $no_urut_asal = substr($nop_asal, 13, 4);
                        $kd_jns_op_asal = substr($nop_asal, 17, 1);
                        $layanan_objek_id = $request->lhp_id;


                        // mengiventaris semua piutang
                        // blokir semua tagihan untuk nop tersebut

                        $ck = DB::connection("oracle_satutujuh")
                            ->table('sppt')
                            ->leftJoin('pengurang_piutang as sppt_koreksi', function ($join) {
                                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
                            })
                            ->select(
                                'sppt.kd_propinsi',
                                'sppt.kd_dati2',
                                'sppt.kd_kecamatan',
                                'sppt.kd_kelurahan',
                                'sppt.kd_blok',
                                'sppt.no_urut',
                                'sppt.kd_jns_op',
                                'sppt.thn_pajak_sppt',
                                'sppt_koreksi.jns_koreksi',
                                'sppt_koreksi.no_surat'
                            )
                            ->where([
                                ['sppt.kd_propinsi', '=', $kd_propinsi_asal],
                                ['sppt.kd_dati2', '=', $kd_dati2_asal],
                                ['sppt.kd_kecamatan', '=', $kd_kecamatan_asal],
                                ['sppt.kd_kelurahan', '=', $kd_kelurahan_asal],
                                ['sppt.kd_blok', '=', $kd_blok_asal],
                                ['sppt.no_urut', '=', $no_urut_asal],
                                ['sppt.kd_jns_op', '=', $kd_jns_op_asal],
                            ])
                            ->where('sppt.status_pembayaran_sppt', '!=', 1)
                            ->whereNull('sppt_koreksi.jns_koreksi')
                            ->get();

                        // ambil nomoer transaksi untuk sppt_koreksi

                        $ntr = PembatalanObjek::NoTransaksi('PL', '07');
                        $koreksi = [];
                        $wblokir = '';
                        foreach ($ck as $tg) {
                            $wblokir = "(kd_propinsi = '" . $tg->kd_propinsi . "' and
                            kd_dati2 = '" . $tg->kd_dati2 . "' and
                            kd_kecamatan = '" . $tg->kd_kecamatan . "' and
                            kd_kelurahan = '" . $tg->kd_kelurahan . "' and
                            kd_blok = '" . $tg->kd_blok . "' and
                            no_urut = '" . $tg->no_urut . "' and
                            kd_jns_op = '" . $tg->kd_jns_op . "' and
                            thn_pajak_sppt = '" . $tg->thn_pajak_sppt . "' ) or";
                            $koreksi[] = [
                                'kd_propinsi' => $tg->kd_propinsi,
                                'kd_dati2' => $tg->kd_dati2,
                                'kd_kecamatan' => $tg->kd_kecamatan,
                                'kd_kelurahan' => $tg->kd_kelurahan,
                                'kd_blok' => $tg->kd_blok,
                                'no_urut' => $tg->no_urut,
                                'kd_jns_op' => $tg->kd_jns_op,
                                'thn_pajak_sppt' => $tg->thn_pajak_sppt,
                                'keterangan' => 'Iventarisir piutang untuk keperluan mutasi Pecah',
                                'verifikasi_kode' => '1',
                                'no_surat' => $ntr,
                                'tgl_surat' => db::raw("sysdate"),
                                'verifikasi_by' => auth()->user()->id,
                                'verifikasi_keterangan' => 'Proses penelitian/pendataan',
                                'no_transaksi' => $ntr,
                                'created_at' => db::raw("sysdate"),
                                'created_by' => auth()->user()->id,
                                'jns_koreksi' => '1',
                                'layanan_objek_id' => $request->lhp_id
                            ];
                        }

                        if ($wblokir != "") {
                            $wblokir = substr($wblokir, 0, -2);
                            DB::connection('oracle_satutujuh')->table('sppt')
                                ->whereraw($wblokir)
                                ->update(['status_pembayaran_sppt' => 3]);
                        }

                        if (!empty($koreksi)) {

                            foreach ($koreksi as $koreksi) {
                                $ff = [
                                    'kd_propinsi' => $koreksi['kd_propinsi'],
                                    'kd_dati2' => $koreksi['kd_dati2'],
                                    'kd_kecamatan' => $koreksi['kd_kecamatan'],
                                    'kd_kelurahan' => $koreksi['kd_kelurahan'],
                                    'kd_blok' => $koreksi['kd_blok'],
                                    'no_urut' => $koreksi['no_urut'],
                                    'kd_jns_op' => $koreksi['kd_jns_op'],
                                    'thn_pajak_sppt' => $koreksi['thn_pajak_sppt']
                                ];

                                $df = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                                    ->where($ff)->count();
                                if ($df == 0) {
                                    DB::connection('oracle_satutujuh')
                                        ->table('sppt_koreksi')
                                        ->insert($koreksi);
                                }
                            }
                        }


                        // backup dulu data potongan individu nya 

                        $cb = DB::table("potongan_individu_tmp")
                            ->where([
                                ['kd_propinsi', '=', $kd_propinsi_asal],
                                ['kd_dati2', '=', $kd_dati2_asal],
                                ['kd_kecamatan', '=', $kd_kecamatan_asal],
                                ['kd_kelurahan', '=', $kd_kelurahan_asal],
                                ['kd_blok', '=', $kd_blok_asal],
                                ['no_urut', '=', $no_urut_asal],
                                ['kd_jns_op', '=', $kd_jns_op_asal],
                            ])
                            ->whereRaw("thn_pajak_sppt = TO_CHAR(SYSDATE, 'YYYY')")
                            ->count();


                        if ($cb == 0) {
                            // Ambil nilai_potongan dari database Oracle
                            $ap = DB::connection('oracle_satutujuh')
                                ->table('MS_POTONGAN_INDIVIDU')
                                ->where([
                                    ['kd_propinsi', '=', $kd_propinsi_asal],
                                    ['kd_dati2', '=', $kd_dati2_asal],
                                    ['kd_kecamatan', '=', $kd_kecamatan_asal],
                                    ['kd_kelurahan', '=', $kd_kelurahan_asal],
                                    ['kd_blok', '=', $kd_blok_asal],
                                    ['no_urut', '=', $no_urut_asal],
                                    ['kd_jns_op', '=', $kd_jns_op_asal],
                                ])
                                ->whereRaw("thn_pajak_sppt = TO_CHAR(SYSDATE, 'YYYY')")
                                ->value('nilai_potongan'); // Ambil langsung nilai potongan

                            if ($ap !== null) {
                                $tmp_pot = [
                                    'kd_propinsi' => $kd_propinsi_asal,
                                    'kd_dati2' => $kd_dati2_asal,
                                    'kd_kecamatan' => $kd_kecamatan_asal,
                                    'kd_kelurahan' => $kd_kelurahan_asal,
                                    'kd_blok' => $kd_blok_asal,
                                    'no_urut' => $no_urut_asal,
                                    'kd_jns_op' => $kd_jns_op_asal,
                                    'thn_pajak_sppt' => date('Y'),
                                    'layanan_objek_id' => $layanan_objek_id,
                                    'nilai_potongan' => $ap,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                ];

                                DB::table('potongan_individu_tmp')->insert($tmp_pot);
                            }
                        }


                        // mencari total luas induk, dari data pecahan
                        $subQuery1 = DB::table('layanan_objek')
                            ->selectRaw('NVL(nop_gabung, id)')
                            ->where('id', $layanan_objek_id);

                        $subQuery2 = DB::table('layanan_objek')
                            ->selectRaw('NVL(nop_gabung, id)')
                            ->where('id', $layanan_objek_id);

                        $firstQuery = DB::table('layanan_objek as lo')
                            ->select([
                                'lo.id',
                                DB::raw("CASE 
                                            WHEN lo.nop_gabung IS NULL 
                                            THEN CASE 
                                                    WHEN l.jenis_layanan_id = 6 THEN lo.sisa_pecah_total_gabung 
                                                    ELSE lo.luas_bumi 
                                                END 
                                            ELSE lo.luas_bumi 
                                        END AS luas_bumi")
                            ])
                            ->join('layanan as l', 'l.nomor_layanan', '=', 'lo.nomor_layanan')
                            ->whereIn('lo.nop_gabung', $subQuery1)
                            ->orWhereIn('lo.id', $subQuery2);

                        $nestedSubQuery = DB::table('layanan_objek')
                            ->selectRaw('CAST(nop_gabung AS NUMBER) AS nop_gabung')
                            ->where('id', $layanan_objek_id)
                            ->unionAll(
                                DB::table('layanan_objek')
                                    ->selectRaw('CAST(id AS NUMBER)')
                                    ->where('id', $layanan_objek_id)
                                    ->whereNull('nop_gabung')
                            );

                        $nestedJoin = DB::table('layanan_objek as a')
                            ->select([
                                'a.id',
                                'a.kd_propinsi',
                                'a.kd_dati2',
                                'a.kd_kecamatan',
                                'a.kd_kelurahan',
                                'a.kd_blok',
                                'a.no_urut',
                                'a.kd_jns_op'
                            ])
                            ->joinSub($nestedSubQuery, 'b', function ($join) {
                                $join->on('a.id', '=', 'b.nop_gabung');
                            });

                        $mainJoin = DB::table('layanan_objek as a')
                            ->select([
                                'a.id',
                                'a.kd_propinsi',
                                'a.kd_dati2',
                                'a.kd_kecamatan',
                                'a.kd_kelurahan',
                                'a.kd_blok',
                                'a.no_urut',
                                'a.kd_jns_op'
                            ])
                            ->joinSub($nestedJoin, 'b', function ($join) {
                                $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                                    ->on('a.kd_dati2', '=', 'b.kd_dati2')
                                    ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                                    ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                                    ->on('a.kd_blok', '=', 'b.kd_blok')
                                    ->on('a.no_urut', '=', 'b.no_urut')
                                    ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                                    ->whereRaw('a.id != b.id')
                                    ->whereNotNull('a.nomor_formulir');
                            });

                        $secondQuery = DB::table('layanan_objek as dd')
                            ->select(['dd.id', 'dd.luas_bumi'])
                            ->joinSub($mainJoin, 'ee', 'ee.id', '=', 'dd.nop_gabung');

                        $luas_pecah = $firstQuery->unionAll($secondQuery)->get();

                        $total_luas_bumi = 0;
                        foreach ($luas_pecah as $row) {
                            $total_luas_bumi += $row->luas_bumi;
                        }

                        $persentase_pecah = [];
                        $arra = [];

                        foreach ($luas_pecah as $row) {
                            $arra[$row->id] = ['luas' => $row->luas_bumi, 'total' => $total_luas_bumi];
                            $persentase_pecah[$row->id] = round(($row->luas_bumi / $total_luas_bumi) * 100, 2);
                        }

                        //mencari tagihan dari induk untuk pecahan
                        $persen = $persentase_pecah[$layanan_objek_id] ?? 100;
                        $tahun = date('Y');
                        $tt = "";
                        foreach ($penetapan as $tp => $rr) {
                            $tt .= $tp . ',';
                        }

                        if ($tt != '') {
                            $tt = substr($tt, 0, -1);
                        }

                        // hapus tagihan induk yang terdahulu
                        DB::statement(DB::raw("update data_billing set deleted_at=sysdate
                                                where data_billing_id in (
                                                select db.data_billing_id
                                                from nota_perhitungan np
                                                join data_billing db on np.data_billing_id=db.data_billing_id
                                                join (
                                                SELECT a.id
                                                FROM layanan_objek a
                                                    JOIN (SELECT a.id,
                                                                    kd_propinsi,
                                                                    kd_dati2,
                                                                    kd_kecamatan,
                                                                    kd_kelurahan,
                                                                    kd_blok,
                                                                    no_urut,
                                                                    kd_jns_op
                                                            FROM layanan_objek a
                                                                    JOIN (
                                                                            SELECT cast(nop_gabung as number) nop_gabung
                                                                            FROM layanan_objek
                                                                        WHERE id = '$layanan_objek_id'
                                                                        union 
                                                                        select cast(id as number)
                                                                        from layanan_objek
                                                                        where id='$layanan_objek_id' and nop_gabung is null 
                                                                        ) b
                                                                    ON a.id = b.nop_gabung) b
                                                        ON     a.kd_propinsi = b.kd_propinsi
                                                            AND a.kd_dati2 = b.kd_dati2
                                                            AND a.kd_kecamatan = b.kd_kecamatan
                                                            AND a.kd_kelurahan = b.kd_kelurahan
                                                            AND a.kd_blok = b.kd_blok
                                                            AND a.no_urut = b.no_urut
                                                            AND a.kd_jns_op = b.kd_jns_op
                                                            AND a.id != b.id
                                                            AND a.nomor_formulir IS NOT NULL
                                                ) sd on sd.id=np.layanan_objek_id 
                                                where  db.deleted_at is null and kd_status='0' 
                                                and db.deleted_at is null 
                                                )"));

                        $tagihan = DB::connection("oracle_spo")
                            ->table('sppt_oltp')
                            ->selectRaw("
                                    sppt_oltp.kd_propinsi, sppt_oltp.kd_dati2, sppt_oltp.kd_kecamatan, 
                                    sppt_oltp.kd_kelurahan, sppt_oltp.kd_blok, sppt_oltp.no_urut, sppt_oltp.kd_jns_op, 
                                    sppt_oltp.thn_pajak_sppt AS tahun_pajak,
                                    ROUND(sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT * (? / 100)) AS pbb, 
                                    sppt_oltp.nm_wp_sppt AS nm_wp,
                                    ROUND(sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT * (? / 100)) AS pokok,  
                                    ROUND(sppt_oltp.denda * (? / 100)) AS denda,
                                    ROUND(sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT * (? / 100)) + ROUND(sppt_oltp.denda * (? / 100)) AS total
                                ", [$persen, $persen, $persen, $persen, $persen])
                            ->leftJoin('pbb.pengurang_piutang AS sppt_koreksi', function ($join) {
                                $join->on('sppt_oltp.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                                    ->on('sppt_oltp.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                                    ->on('sppt_oltp.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                                    ->on('sppt_oltp.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                                    ->on('sppt_oltp.kd_blok', '=', 'sppt_koreksi.kd_blok')
                                    ->on('sppt_oltp.no_urut', '=', 'sppt_koreksi.no_urut')
                                    ->on('sppt_oltp.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                                    ->on('sppt_oltp.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
                            })
                            ->leftJoin('pbb.ref_buku', function ($join) {
                                $join->on('ref_buku.thn_awal', '<=', 'sppt_oltp.thn_pajak_sppt')
                                    ->on('ref_buku.thn_akhir', '>=', 'sppt_oltp.thn_pajak_sppt')
                                    ->on('ref_buku.nilai_min_buku', '<=', 'sppt_oltp.pbb_yg_harus_dibayar_sppt')
                                    ->on('ref_buku.nilai_max_buku', '>=', 'sppt_oltp.pbb_yg_harus_dibayar_sppt');
                            })
                            ->leftJoin('pbb.ref_kelurahan', function ($join) {
                                $join->on('ref_kelurahan.kd_kecamatan', '=', 'sppt_oltp.kd_kecamatan')
                                    ->on('ref_kelurahan.kd_kelurahan', '=', 'sppt_oltp.kd_kelurahan');
                            })
                            ->leftJoin(DB::raw("(SELECT tahun_pajak, kd_kelurahan, kd_kecamatan FROM sim_pbb.desa_lunas)  dl"), function ($join) {
                                $join->on('dl.tahun_pajak', '=', 'sppt_oltp.thn_pajak_sppt')
                                    ->on('dl.kd_kecamatan', '=', 'sppt_oltp.kd_kecamatan')
                                    ->on('dl.kd_kelurahan', '=', 'sppt_oltp.kd_kelurahan');
                            })
                            ->whereRaw("sppt_koreksi.no_surat IS NULL")
                            ->whereRaw("status_pembayaran_sppt != '1' 
                                        AND sppt_oltp.kd_propinsi = ? 
                                        AND sppt_oltp.kd_dati2 = ? 
                                        AND sppt_oltp.kd_kecamatan = ? 
                                        AND sppt_oltp.kd_kelurahan = ? 
                                        AND sppt_oltp.kd_blok = ? 
                                        AND sppt_oltp.no_urut = ? 
                                        AND sppt_oltp.kd_jns_op = ? 
                                        AND sppt_oltp.thn_pajak_sppt != ? 
                                        AND (sppt_koreksi.jns_koreksi != '3' OR sppt_koreksi.jns_koreksi IS NULL)
                                        AND (
                                            CASE 
                                                WHEN dl.tahun_pajak IS NOT NULL AND kd_buku IN ('1', '2') AND TO_CHAR(tgl_terbit_sppt, 'YYYY') = sppt_oltp.thn_pajak_sppt 
                                                THEN '0' 
                                                ELSE CASE 
                                                    WHEN sppt_oltp.thn_pajak_sppt < NVL(thn_sismiop, '2003') 
                                                    THEN '0' 
                                                    ELSE '1' 
                                                END 
                                            END 
                                        ) = '1'
                                    ", [
                                $kd_propinsi_asal,
                                $kd_dati2_asal,
                                $kd_kecamatan_asal,
                                $kd_kelurahan_asal,
                                $kd_blok_asal,
                                $no_urut_asal,
                                $kd_jns_op_asal,
                                $tahun
                            ]);

                        if ($tt != '') {
                            $tagihan = $tagihan->whereraw("sppt_oltp.thn_pajak_sppt in (" . $tt . ")");
                        }
                        
                        $tagihan = $tagihan->get();

                        /*  $tagihan = DB::connection("oracle_spo")->table("sppt_oltp")
                            ->select([
                                "sppt_oltp.kd_propinsi",
                                "sppt_oltp.kd_dati2",
                                "sppt_oltp.kd_Kecamatan",
                                "sppt_oltp.kd_kelurahan",
                                "sppt_oltp.kd_blok",
                                "sppt_oltp.no_urut",
                                "sppt_oltp.kd_jns_op",
                                "sppt_oltp.thn_pajak_sppt as tahun_pajak",
                                DB::raw("ROUND(sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT * (? / 100)) as pbb"),
                                "sppt_oltp.nm_wp_sppt as nm_wp",
                                DB::raw("ROUND(sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT * (? / 100)) as pokok"),
                                DB::raw("ROUND(sppt_oltp.denda * (? / 100)) as denda"),
                                DB::raw("ROUND(sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT * (? / 100)) + ROUND(sppt_oltp.denda * (? / 100)) as total")
                            ])
                            ->leftJoin("pbb.pengurang_piutang as sppt_koreksi", function ($join) {
                                $join->on("sppt_oltp.kd_propinsi", "=", "sppt_koreksi.kd_propinsi")
                                    ->on("sppt_oltp.kd_dati2", "=", "sppt_koreksi.kd_dati2")
                                    ->on("sppt_oltp.kd_kecamatan", "=", "sppt_koreksi.kd_kecamatan")
                                    ->on("sppt_oltp.kd_kelurahan", "=", "sppt_koreksi.kd_kelurahan")
                                    ->on("sppt_oltp.kd_blok", "=", "sppt_koreksi.kd_blok")
                                    ->on("sppt_oltp.no_urut", "=", "sppt_koreksi.no_urut")
                                    ->on("sppt_oltp.kd_jns_op", "=", "sppt_koreksi.kd_jns_op")
                                    ->on("sppt_oltp.thn_pajak_sppt", "=", "sppt_koreksi.thn_pajak_sppt");
                            })
                            ->leftJoin("pbb.ref_buku", function ($join) {
                                $join->on("ref_buku.thn_awal", "<=", "sppt_oltp.thn_pajak_sppt")
                                    ->on("ref_buku.thn_akhir", ">=", "sppt_oltp.thn_pajak_sppt")
                                    ->on("ref_buku.nilai_min_buku", "<=", "sppt_oltp.pbb_yg_harus_dibayar_sppt")
                                    ->on("ref_buku.nilai_max_buku", ">=", "sppt_oltp.pbb_yg_harus_dibayar_sppt");
                            })
                            ->leftJoin("pbb.ref_kelurahan", function ($join) {
                                $join->on("ref_kelurahan.kd_kecamatan", "=", "sppt_oltp.kd_kecamatan")
                                    ->on("ref_kelurahan.kd_kelurahan", "=", "sppt_oltp.kd_kelurahan");
                            })
                            ->leftJoin(DB::raw("(SELECT tahun_pajak, kd_kelurahan, kd_kecamatan FROM sim_pbb.desa_lunas) as dl"), function ($join) {
                                $join->on("dl.tahun_pajak", "=", "sppt_oltp.thn_pajak_sppt")
                                    ->on("dl.kd_kecamatan", "=", "sppt_oltp.kd_kecamatan")
                                    ->on("dl.kd_kelurahan", "=", "sppt_oltp.kd_kelurahan");
                            })
                            ->where("status_pembayaran_sppt", "!=", "1")
                            ->where("sppt_oltp.kd_propinsi", $kd_propinsi_asal)
                            ->where("sppt_oltp.kd_dati2", $kd_dati2_asal)
                            ->where("sppt_oltp.kd_kecamatan", $kd_kecamatan_asal)
                            ->where("sppt_oltp.kd_kelurahan", $kd_kelurahan_asal)
                            ->where("sppt_oltp.kd_blok", $kd_blok_asal)
                            ->where("sppt_oltp.no_urut", $no_urut_asal)
                            ->where("sppt_oltp.kd_jns_op", $kd_jns_op_asal)
                            ->where("sppt_oltp.thn_pajak_sppt", "!=", $tahun)
                            ->whereRaw("(sppt_koreksi.jns_koreksi != '3' OR sppt_koreksi.jns_koreksi IS NULL)")

                            ->whereRaw("CASE 
                                        WHEN dl.tahun_pajak IS NOT NULL AND kd_buku IN ('1', '2') 
                                            AND TO_CHAR(tgl_terbit_sppt, 'yyyy') = sppt_oltp.thn_pajak_sppt 
                                            THEN '0' 
                                        ELSE 
                                            CASE 
                                                WHEN sppt_oltp.thn_pajak_sppt < NVL(thn_sismiop, '2003') 
                                                    THEN '0' 
                                                ELSE '1' 
                                            END 
                                        END = '1'")
                            ->setBindings([$persen, $persen, $persen, $persen]);

                        // Binding untuk operasi matematis
                        // ->get();


                      */

                        $ArTagihan = [];
                        foreach ($tagihan as $tagihan) {
                            if ($tagihan->pokok > 0) {
                                $ArTagihan[] = [
                                    'kd_propinsi' => $tagihan->kd_propinsi,
                                    'kd_dati2' => $tagihan->kd_dati2,
                                    'kd_kecamatan' => $tagihan->kd_kecamatan,
                                    'kd_kelurahan' => $tagihan->kd_kelurahan,
                                    'kd_blok' => $tagihan->kd_blok,
                                    'no_urut' => $tagihan->no_urut,
                                    'kd_jns_op' => $tagihan->kd_jns_op,
                                    'tahun_pajak' => $tagihan->tahun_pajak,
                                    'pbb' => $tagihan->pbb,
                                    'nm_wp' => $tagihan->nm_wp,
                                    'pokok' => $tagihan->pokok,
                                    'denda' => $tagihan->denda,
                                    'total' => $tagihan->total
                                ];
                            }
                        }


                        // pada akhirnya penetapan hanya di lakukan untuk tahun berjalan saja
                        $tahun_sppt = date('Y');

                        // identifikasi hkpd
                        $hkpd = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                            ->where("thn_hkpd", $tahun_sppt)
                            ->where("kd_kecamatan", $kd_kecamatan)
                            ->where("kd_kelurahan", $kd_kelurahan)
                            ->where("kd_blok", $kd_blok)
                            ->where("no_urut", $no_urut)
                            ->where("kd_jns_op", $kd_jns_op)
                            ->count();

                        if ($hkpd == 0) {
                            $lh = DB::connection("oracle_satutujuh")->table("sppt")->select("hkpd")
                                ->whereraw("thn_pajak_sppt='$tahun_sppt' and kd_kecamatan='$kd_kecamatan_asal' and kd_kelurahan='$kd_kelurahan_asal'
                                        and kd_blok='$kd_blok_asal' and no_urut='$no_urut_asal' and kd_jns_op='$kd_jns_op_asal'")
                                ->first();

                            if ($lh) {
                                $nilai = $lh->hkpd;
                                $inshkpd = [
                                    'kd_propinsi' => $kd_propinsi,
                                    'kd_dati2' => $kd_dati2,
                                    'kd_kecamatan' => $kd_kecamatan,
                                    'kd_kelurahan' => $kd_kelurahan,
                                    'kd_blok' => $kd_blok,
                                    'no_urut' => $no_urut,
                                    'kd_jns_op' => $kd_jns_op,
                                    'hkpd' => $nilai,
                                    'thn_hkpd' => $tahun_sppt,
                                    'created_at' => Carbon::now(),
                                    'created_by' => auth()->user()->id
                                ];

                                DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")->insert($inshkpd);
                                DB::connection("oracle_satutujuh")->commit();
                            }
                        }

                        $sql_penetapan .= " PENENTUAN_NJOP ('$kd_propinsi', '$kd_dati2', '$kd_kecamatan', '$kd_kelurahan', '$kd_blok', '$no_urut', '$kd_jns_op', '$tahun_sppt', '1' ); ";
                        if (isset($penetapan[$tahun_sppt]) && (!in_array($jns_bumi, [4, 5]))) {
                            $jatuh_tempo = date('Ymd', strtotime($penetapan[$tahun_sppt]));

                            $tgl_terbit = date('Ymd');
                            $ls = PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                            // Log::info($ls);
                            $sb = $ls['PBB_YG_HARUS_DIBAYAR_SPPT'] ?? 0;
                            $sql_penetapan .= "
                              delete from pbb.MS_POTONGAN_INDIVIDU where   thn_pajak_sppt='$tahun_sppt' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                              and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op';";

                            if ($sb > 0) {

                                $insentif_min = 0;
                                if ($tahun_sppt >= '2025') {
                                    $insentif_min = 2000;
                                }


                                // Mencari persen potongan sebelum pecah
                                $vb = DB::table('sppt_bkp as a')
                                    ->leftJoin('potongan_individu_tmp as b', function ($join) {
                                        $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                                            ->on('a.kd_dati2', '=', 'b.kd_dati2')
                                            ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                                            ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                                            ->on('a.kd_blok', '=', 'b.kd_blok')
                                            ->on('a.no_urut', '=', 'b.no_urut')
                                            ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                                            ->on('a.thn_pajak_sppt', '=', 'b.thn_pajak_sppt');
                                    })
                                    ->where([
                                        ['a.thn_pajak_sppt', '=', $tahun_sppt],
                                        ['a.kd_kecamatan', '=', $kd_kecamatan_asal],
                                        ['a.kd_kelurahan', '=', $kd_kelurahan_asal],
                                        ['a.kd_blok', '=', $kd_blok_asal],
                                        ['a.no_urut', '=', $no_urut_asal],
                                        ['a.kd_jns_op', '=', $kd_jns_op_asal],
                                    ])
                                    ->orderBy('a.created_at', 'asc')
                                    ->selectRaw('CASE 
                                                WHEN COALESCE(nilai_potongan, 0) = 0 THEN 0 
                                                ELSE ROUND((nilai_potongan / a.pbb_yg_harus_dibayar_sppt) * 100, 2) 
                                            END AS persen')
                                    ->first();

                                if (!$vb) {
                                    $vb = DB::connection('oracle_satutujuh')->table('sppt as a')
                                        ->leftJoin('sppt_potongan_detail as b', function ($join) {
                                            $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                                                ->on('a.kd_dati2', '=', 'b.kd_dati2')
                                                ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                                                ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                                                ->on('a.kd_blok', '=', 'b.kd_blok')
                                                ->on('a.no_urut', '=', 'b.no_urut')
                                                ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                                                ->on('a.thn_pajak_sppt', '=', 'b.thn_pajak_sppt')
                                                ->where('b.jns_potongan', '=', 1);
                                        })
                                        ->where([
                                            ['a.thn_pajak_sppt', '=', $tahun_sppt],
                                            ['a.kd_kecamatan', '=', $kd_kecamatan_asal],
                                            ['a.kd_kelurahan', '=', $kd_kelurahan_asal],
                                            ['a.kd_blok', '=', $kd_blok_asal],
                                            ['a.no_urut', '=', $no_urut_asal],
                                            ['a.kd_jns_op', '=', $kd_jns_op_asal],
                                        ])
                                        ->selectRaw('CASE 
                                                            WHEN COALESCE(nilai_potongan, 0) = 0 THEN 0 
                                                            ELSE ROUND((nilai_potongan / a.pbb_yg_harus_dibayar_sppt) * 100, 2) 
                                                    END AS persen')
                                        ->first();
                                }


                                if ($vb) {
                                    $np = round($vb->persen, 2);
                                } else {
                                    if ($insentif_min > 0) {
                                        $df = DB::connection("oracle_satutujuh")->table(DB::raw("( 
                                                        select  round((" . $insentif_min . "/ pbb.pbbmin(" . $tahun_sppt . ")) * 100) persen 
                                                        from dual) tmp"))->first();
                                        $np = $df->persen;
                                    } else {
                                        $np = 0;
                                    }
                                }
                                if ($np > 0) {

                                    $sql_penetapan .= "
                                                 insert into pbb.MS_POTONGAN_INDIVIDU 
                                                     select kd_propinsi,
                                            kd_dati2,
                                            kd_kecamatan,
                                            kd_kelurahan,
                                            kd_blok,
                                            no_urut,
                                            kd_jns_op,
                                            thn_pajak_sppt,
                                            case when (pbb_yg_harus_dibayar_sppt -        (CASE
                                                WHEN pbb.pbbmin (thn_pajak_sppt) < pbb_yg_harus_dibayar_sppt
                                                THEN
                                                    ROUND (pbb_yg_harus_dibayar_sppt * (" . $np . " / 100))
                                                ELSE
                                                    0
                                            END)) < ( pbb.pbbmin (thn_pajak_sppt)- " . $insentif_min . "  ) then 
                                                 (pbb_yg_harus_dibayar_sppt) - (pbb.pbbmin (thn_pajak_sppt) -  " . $insentif_min . ")
                                            else 
                                                    (CASE
                                                WHEN pbb.pbbmin (thn_pajak_sppt) < pbb_yg_harus_dibayar_sppt
                                                THEN
                                                    ROUND (pbb_yg_harus_dibayar_sppt * (" . $np . " / 100))
                                                ELSE
                                                    0
                                            END)
                                            end
                               nilai_potongan,sysdate-1 created_at,'" . Auth()->user()->id . "' created_by ,trunc(sysdate-1) tgl_mulai,trunc(to_date('31129999','ddmmyyyy')) tgl_selesai
                             from pbb.sppt
                             where thn_pajak_sppt='$tahun_sppt' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                             and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'; commit;";
                                }
                                $sql_penetapan .= " hitung_potongan('$kd_kecamatan','$kd_kelurahan','$kd_blok','$no_urut','$kd_jns_op','$tahun_sppt'); ";
                            }
                        }

                        if (in_array($jns_bumi, [4, 5])) {
                            DB::connection("oracle_satutujuh")->table("sppt")
                                ->where("kd_kecamatan", $kd_kecamatan_asal)
                                ->where("kd_kelurahan", $kd_kelurahan_asal)
                                ->where("kd_blok", $kd_blok_asal)
                                ->where("no_urut", $no_urut_asal)
                                ->whereRaw("thn_pajak_sppt = TO_CHAR(SYSDATE, 'YYYY')")
                                ->delete();
                        }

                        $wh_tagihan_dua = "status_pembayaran_sppt!='1' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan'  
                                            and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' and thn_pajak_sppt='" . date('Y') . "' ";
                        break;
                    case '7':
                        # Mutasi gabung
                        $nop_asal = $request->nop_pembatalan;
                        $wh = "";

                        // $hkpd_gabung = 0;

                        self::pembatalanGabung($request, $spop['no_formulir']);
                        foreach ($nop_asal as $nop_asal) {
                            $kd_propinsi_asal = substr($nop_asal, 0, 2);
                            $kd_dati2_asal = substr($nop_asal, 2, 2);
                            $kd_kecamatan_asal = substr($nop_asal, 4, 3);
                            $kd_kelurahan_asal = substr($nop_asal, 7, 3);
                            $kd_blok_asal = substr($nop_asal, 10, 3);
                            $no_urut_asal = substr($nop_asal, 13, 4);
                            $kd_jns_op_asal = substr($nop_asal, 17, 1);
                            $wh .= " (sppt.kd_propinsi='$kd_propinsi_asal' and sppt.kd_dati2='$kd_dati2_asal' and sppt.kd_kecamatan='$kd_kecamatan_asal'                                      and sppt.kd_kelurahan='$kd_kelurahan_asal' and sppt.kd_blok='$kd_blok_asal' and sppt.no_urut='$no_urut_asal'  and sppt.kd_jns_op='$kd_jns_op_asal') or";
                        }
                        $wh = substr($wh, 0, -2);
                        // dd(count($penetapan));
                        if (count($penetapan) > 0) {
                            // get hkpd nya
                            $get_hkpd = DB::connection("oracle_satutujuh")->table("sppt")
                                ->whereraw("(" . $wh . ") and thn_pajak_sppt='" . date('Y') . "'")
                                ->selectraw("max(hkpd) hkpd")->first();

                            if ($get_hkpd) {
                                if ($get_hkpd->hkpd > 0) {
                                    $idv = [
                                        'thn_hkpd' => date('Y'),
                                        'kd_propinsi' => $kd_propinsi,
                                        'kd_dati2' => $kd_dati2,
                                        'kd_kecamatan' => $kd_kecamatan,
                                        'kd_kelurahan' => $kd_kelurahan,
                                        'kd_blok' => $kd_blok,
                                        'no_urut' => $no_urut,
                                        'kd_jns_op' => $kd_jns_op,
                                        'hkpd' => $get_hkpd->hkpd,
                                        'created_at' => Carbon::now(),
                                        'created_by' => auth()->user()->id,
                                    ];

                                    $fk = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                        ->where([
                                            'thn_hkpd' => date('Y'),
                                            'kd_propinsi' => $kd_propinsi,
                                            'kd_dati2' => $kd_dati2,
                                            'kd_kecamatan' => $kd_kecamatan,
                                            'kd_kelurahan' => $kd_kelurahan,
                                            'kd_blok' => $kd_blok,
                                            'no_urut' => $no_urut,
                                            'kd_jns_op' => $kd_jns_op
                                        ])->count();

                                    if ($fk == 0) {
                                        DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                            ->insert($idv);
                                    } else {
                                        DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                            ->where([
                                                'thn_hkpd' => date('Y'),
                                                'kd_propinsi' => $kd_propinsi,
                                                'kd_dati2' => $kd_dati2,
                                                'kd_kecamatan' => $kd_kecamatan,
                                                'kd_kelurahan' => $kd_kelurahan,
                                                'kd_blok' => $kd_blok,
                                                'no_urut' => $no_urut,
                                                'kd_jns_op' => $kd_jns_op
                                            ])
                                            ->update($idv);
                                    }
                                }
                            }



                            $jt = $penetapan[max(array_keys($penetapan))];
                            // update jt
                            /*   $cjt = $ck = DB::connection("oracle_satutujuh")->select(db::raw("select sppt.kd_propinsi,
                                                            sppt.kd_dati2,
                                                            sppt.kd_kecamatan,
                                                            sppt.kd_kelurahan,
                                                            sppt.kd_blok,
                                                            sppt.no_urut,
                                                            sppt.kd_jns_op,
                                                            sppt.no_urut,
                                                            sppt.thn_pajak_sppt,jns_koreksi,no_surat,sppt.hkpd
                                                                    from sppt  
                                                                    left join sppt_koreksi on sppt.kd_propinsi=sppt_koreksi.kd_propinsi and
                                                            sppt.kd_dati2=sppt_koreksi.kd_dati2 and
                                                            sppt.kd_kecamatan=sppt_koreksi.kd_kecamatan and
                                                            sppt.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                                                            sppt.kd_blok=sppt_koreksi.kd_blok and
                                                            sppt.no_urut=sppt_koreksi.no_urut and
                                                            sppt.kd_jns_op=sppt_koreksi.kd_jns_op and
                                                            sppt.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt
                                                            where (" . $wh . ")  and 
                                                            status_pembayaran_sppt!='1'")); */
                            $cjt = DB::connection("oracle_satutujuh")
                                ->table('sppt')
                                ->select([
                                    'sppt.kd_propinsi',
                                    'sppt.kd_dati2',
                                    'sppt.kd_kecamatan',
                                    'sppt.kd_kelurahan',
                                    'sppt.kd_blok',
                                    'sppt.no_urut',
                                    'sppt.kd_jns_op',
                                    'sppt.thn_pajak_sppt',
                                    'sppt_koreksi.jns_koreksi',
                                    'sppt_koreksi.no_surat',
                                    'sppt.hkpd'
                                ])
                                ->leftJoin('sppt_koreksi', function ($join) {
                                    $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                                        ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                                        ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                                        ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                                        ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                                        ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                                        ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                                        ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
                                })
                                ->whereRaw($wh)
                                ->where('status_pembayaran_sppt', '!=', '1')
                                ->get();




                            foreach ($cjt as $rj) {
                                DB::connection("oracle_satutujuh")->table("sppt")
                                    ->where('kd_kecamatan', $rj->kd_kecamatan)
                                    ->where('kd_kelurahan', $rj->kd_kelurahan)
                                    ->where('kd_blok', $rj->kd_blok)
                                    ->where('no_urut', $rj->no_urut)
                                    ->where('kd_jns_op', $rj->kd_jns_op)
                                    ->where('thn_pajak_sppt', $rj->thn_pajak_sppt)
                                    ->update(['tgl_jatuh_tempo_sppt' => new carbon($jt)]);
                            }
                        }
                        // mengiventaris semua piutang
                        // blokir semua tagihan untuk nop tersebut
                        $ck = DB::connection("oracle_satutujuh")
                            ->table('sppt')
                            ->select([
                                'sppt.kd_propinsi',
                                'sppt.kd_dati2',
                                'sppt.kd_kecamatan',
                                'sppt.kd_kelurahan',
                                'sppt.kd_blok',
                                'sppt.no_urut',
                                'sppt.kd_jns_op',
                                'sppt.thn_pajak_sppt',
                                'pengurang_piutang.jns_koreksi',
                                'pengurang_piutang.no_surat'
                            ])
                            ->leftJoin('pengurang_piutang', function ($join) {
                                $join->on('sppt.kd_propinsi', '=', 'pengurang_piutang.kd_propinsi')
                                    ->on('sppt.kd_dati2', '=', 'pengurang_piutang.kd_dati2')
                                    ->on('sppt.kd_kecamatan', '=', 'pengurang_piutang.kd_kecamatan')
                                    ->on('sppt.kd_kelurahan', '=', 'pengurang_piutang.kd_kelurahan')
                                    ->on('sppt.kd_blok', '=', 'pengurang_piutang.kd_blok')
                                    ->on('sppt.no_urut', '=', 'pengurang_piutang.no_urut')
                                    ->on('sppt.kd_jns_op', '=', 'pengurang_piutang.kd_jns_op')
                                    ->on('sppt.thn_pajak_sppt', '=', 'pengurang_piutang.thn_pajak_sppt');
                            })
                            ->whereRaw($wh)
                            ->where('status_pembayaran_sppt', '!=', '1')
                            ->whereNull('pengurang_piutang.jns_koreksi') // Menggunakan whereNull untuk kondisi IS NULL
                            ->get();

                        /*  $ck = DB::connection("oracle_satutujuh")->select(db::raw("select sppt.kd_propinsi,
                                                                                    sppt.kd_dati2,
                                                                                    sppt.kd_kecamatan,
                                                                                    sppt.kd_kelurahan,
                                                                                    sppt.kd_blok,
                                                                                    sppt.no_urut,
                                                                                    sppt.kd_jns_op,
                                                                                    sppt.no_urut,
                                                                                    sppt.thn_pajak_sppt,jns_koreksi,no_surat
                                                                                            from sppt  
                                                                                            left join pengurang_piutang sppt_koreksi on sppt.kd_propinsi=sppt_koreksi.kd_propinsi and
                                                                                    sppt.kd_dati2=sppt_koreksi.kd_dati2 and
                                                                                    sppt.kd_kecamatan=sppt_koreksi.kd_kecamatan and
                                                                                    sppt.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                                                                                    sppt.kd_blok=sppt_koreksi.kd_blok and
                                                                                    sppt.no_urut=sppt_koreksi.no_urut and
                                                                                    sppt.kd_jns_op=sppt_koreksi.kd_jns_op and
                                                                                    sppt.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt
                                                                                    where (" . $wh . ")  and 
                                                                                    status_pembayaran_sppt!='1' 
                                                                                    and jns_koreksi is null")); */
                        // ambil nomoer transaksi untuk sppt_koreksi

                        $ntr = PembatalanObjek::NoTransaksi('PL', '07');
                        $koreksi = [];
                        $wblokir = "";
                        foreach ($ck as $tg) {

                            $wblokir .= "(kd_propinsi = '" . $tg->kd_propinsi . "' and
                            kd_dati2 = '" . $tg->kd_dati2 . "' and
                            kd_kecamatan = '" . $tg->kd_kecamatan . "' and
                            kd_kelurahan = '" . $tg->kd_kelurahan . "' and
                            kd_blok = '" . $tg->kd_blok . "' and
                            no_urut = '" . $tg->no_urut . "' and
                            kd_jns_op = '" . $tg->kd_jns_op . "' and
                            thn_pajak_sppt = '" . $tg->thn_pajak_sppt . "' ) or";
                            $koreksi[] = [
                                'kd_propinsi' => $tg->kd_propinsi,
                                'kd_dati2' => $tg->kd_dati2,
                                'kd_kecamatan' => $tg->kd_kecamatan,
                                'kd_kelurahan' => $tg->kd_kelurahan,
                                'kd_blok' => $tg->kd_blok,
                                'no_urut' => $tg->no_urut,
                                'kd_jns_op' => $tg->kd_jns_op,
                                'thn_pajak_sppt' => $tg->thn_pajak_sppt,
                                'keterangan' => 'Iventarisir piutang untuk keperluan mutasi Gabung',
                                'verifikasi_kode' => '1',
                                'no_surat' => $ntr,
                                'tgl_surat' => db::raw("sysdate"),
                                'verifikasi_by' => auth()->user()->id,
                                'verifikasi_keterangan' => 'Proses penelitian/pendataan',
                                'no_transaksi' => $ntr,
                                'created_at' => db::raw("sysdate"),
                                'created_by' => auth()->user()->id,
                                'jns_koreksi' => '1'
                            ];
                        }

                        if ($wblokir != '') {
                            $wblokir = substr($wblokir, 0, -2);
                            DB::connection('oracle_satutujuh')->table('sppt')
                                ->whereraw($wblokir)
                                ->update(['status_pembayaran_sppt' => 3]);
                        }

                        if (!empty($koreksi)) {

                            foreach ($koreksi as $kf) {
                                // log::info($kf);

                                $c = DB::connection("oracle_satutujuh")
                                    ->table('sppt_koreksi')
                                    ->where([
                                        'kd_propinsi' => $kf['kd_propinsi'],
                                        'kd_dati2' => $kf['kd_dati2'],
                                        'kd_kecamatan' => $kf['kd_kecamatan'],
                                        'kd_kelurahan' => $kf['kd_kelurahan'],
                                        'kd_blok' => $kf['kd_blok'],
                                        'no_urut' => $kf['no_urut'],
                                        'kd_jns_op' => $kf['kd_jns_op'],
                                        'thn_pajak_sppt' => $kf['thn_pajak_sppt']
                                    ])->count();
                                if ($c == 0) {
                                    DB::connection("oracle_satutujuh")
                                        ->table('sppt_koreksi')
                                        ->insert($kf);
                                }
                            }
                        }

                        $tahun = date('Y');
                        /* $tagihan = DB::connection("oracle_spo")->table(db::raw("(
                                                                                select sppt_oltp.*
                                                                                from sppt_oltp
                                                                                left join   pbb.pengurang_piutang sppt_koreksi on sppt_oltp.kd_propinsi=sppt_koreksi.kd_propinsi and
                                                                                sppt_oltp.kd_dati2=sppt_koreksi.kd_dati2 and
                                                                                sppt_oltp.kd_Kecamatan=sppt_koreksi.kd_Kecamatan and
                                                                                sppt_oltp.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                                                                                sppt_oltp.kd_blok=sppt_koreksi.kd_blok and
                                                                                sppt_oltp.no_urut=sppt_koreksi.no_urut and
                                                                                sppt_oltp.kd_jns_op=sppt_koreksi.kd_jns_op and
                                                                                sppt_oltp.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt and
                                                                                sppt_koreksi.jns_koreksi='3'
                                                                                and (sppt_koreksi.kd_kategori!='07' or sppt_koreksi.kd_kategori is null)
                                                                                where jns_koreksi is null
                                                                            ) sppt"))
                            ->select(db::raw("kd_propinsi,kd_dati2,kd_Kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt tahun_pajak,
                                                round(PBB_YG_HARUS_DIBAYAR_SPPT ) pbb,nm_wp_sppt nm_wp,round(PBB_YG_HARUS_DIBAYAR_SPPT) pokok   , round(denda) denda,round(PBB_YG_HARUS_DIBAYAR_SPPT ) + round(denda) total"))
                            ->whereraw("status_pembayaran_sppt!='1' and thn_pajak_sppt>=2003 and thn_pajak_sppt<$tahun");

                        $tagihan = $tagihan->whereraw("(" . $wh . ")");
                        $tagihan = $tagihan->get(); */

                        $tagihan = DB::connection("oracle_spo")
                            ->table('sppt_oltp')
                            ->selectRaw("
                                    kd_propinsi, kd_dati2, kd_Kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, 
                                    thn_pajak_sppt as tahun_pajak, 
                                    round(PBB_YG_HARUS_DIBAYAR_SPPT) as pbb, 
                                    nm_wp_sppt as nm_wp, 
                                    round(PBB_YG_HARUS_DIBAYAR_SPPT) as pokok,  
                                    round(denda) as denda, 
                                    round(PBB_YG_HARUS_DIBAYAR_SPPT) + round(denda) as total
                                ")
                            ->leftJoin('pbb.pengurang_piutang', function ($join) {
                                $join->on('sppt_oltp.kd_propinsi', '=', 'pbb.pengurang_piutang.kd_propinsi')
                                    ->on('sppt_oltp.kd_dati2', '=', 'pbb.pengurang_piutang.kd_dati2')
                                    ->on('sppt_oltp.kd_Kecamatan', '=', 'pbb.pengurang_piutang.kd_Kecamatan')
                                    ->on('sppt_oltp.kd_kelurahan', '=', 'pbb.pengurang_piutang.kd_kelurahan')
                                    ->on('sppt_oltp.kd_blok', '=', 'pbb.pengurang_piutang.kd_blok')
                                    ->on('sppt_oltp.no_urut', '=', 'pbb.pengurang_piutang.no_urut')
                                    ->on('sppt_oltp.kd_jns_op', '=', 'pbb.pengurang_piutang.kd_jns_op')
                                    ->on('sppt_oltp.thn_pajak_sppt', '=', 'pbb.pengurang_piutang.thn_pajak_sppt')
                                    ->where('pbb.pengurang_piutang.jns_koreksi', '=', '3')
                                    ->where(function ($query) {
                                        $query->where('pbb.pengurang_piutang.kd_kategori', '!=', '07')
                                            ->orWhereNull('pbb.pengurang_piutang.kd_kategori');
                                    });
                            })
                            ->whereNull('pbb.pengurang_piutang.jns_koreksi')
                            ->where('status_pembayaran_sppt', '!=', '1')
                            ->whereBetween('thn_pajak_sppt', [2003, $tahun - 1])
                            ->whereRaw("(" . $wh . ")")
                            ->get();


                        // dd($tagihan);

                        $ArTagihan = [];
                        foreach ($tagihan as $tagihan) {
                            if ($tagihan->pokok > 0 and $tagihan->tahun_pajak >= 2003) {
                                $ArTagihan[] = [
                                    'kd_propinsi' => $tagihan->kd_propinsi,
                                    'kd_dati2' => $tagihan->kd_dati2,
                                    'kd_kecamatan' => $tagihan->kd_kecamatan,
                                    'kd_kelurahan' => $tagihan->kd_kelurahan,
                                    'kd_blok' => $tagihan->kd_blok,
                                    'no_urut' => $tagihan->no_urut,
                                    'kd_jns_op' => $tagihan->kd_jns_op,
                                    'tahun_pajak' => $tagihan->tahun_pajak,
                                    'pbb' => $tagihan->pbb,
                                    'nm_wp' => $tagihan->nm_wp,
                                    'pokok' => $tagihan->pokok,
                                    'denda' => $tagihan->denda,
                                    'total' => $tagihan->total
                                ];
                            }
                        }


                        // pada akhirnya penetapan hanya di lakukan untuk tahun berjalan saja bila induk nya belum di bayar
                        $tahun_sppt = $tahun;
                        // log::info("gabung ".$penetapan[$tahun_sppt]);
                        if (isset($penetapan[$tahun_sppt]) == true) {
                            $jatuh_tempo = date('Ymd', strtotime($penetapan[$tahun_sppt]));
                            $tgl_terbit = date('Ymd');
                            PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                        } else {
                            $sql_penetapan .= " PENENTUAN_NJOP ('$kd_propinsi', '$kd_dati2', '$kd_kecamatan', '$kd_kelurahan', '$kd_blok', '$no_urut', '$kd_jns_op', '$tahun_sppt', '1' ); ";
                            $sql_penetapan .= " DELETE FROM SPPT WHERE THN_PAJAK_SPPT='$tahun_sppt' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' ; ";
                        }

                        $wh_tagihan_dua = "status_pembayaran_sppt!='1' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan'  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' and thn_pajak_sppt='" . date('Y') . "' ";

                        break;
                    case '10':
                        // pengaktifan
                        // di proses dengan nop yang baru
                        $kd_propinsi_asal = substr($nop_asal, 0, 2);
                        $kd_dati2_asal = substr($nop_asal, 2, 2);
                        $kd_kecamatan_asal = substr($nop_asal, 4, 3);
                        $kd_kelurahan_asal = substr($nop_asal, 7, 3);
                        $kd_blok_asal = substr($nop_asal, 10, 3);
                        $no_urut_asal = substr($nop_asal, 13, 4);
                        $kd_jns_op_asal = substr($nop_asal, 17, 1);
                        // insert ke table pengaktifan
                        // insert ke pengaktifan objek
                        $dataAktifkan = [
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'kd_propinsi_asal' => $kd_propinsi_asal,
                            'kd_dati2_asal' => $kd_dati2_asal,
                            'kd_kecamatan_asal' => $kd_kecamatan_asal,
                            'kd_kelurahan_asal' => $kd_kelurahan_asal,
                            'kd_blok_asal' => $kd_blok_asal,
                            'no_urut_asal' => $no_urut_asal,
                            'kd_jns_op_asal' => $kd_jns_op_asal,
                            'created_by' => auth()->user()->id,
                            'created_at' => now(),
                            'layanan_objek_id' => $layanan_objek_id
                        ];


                        //  cek if exits
                        $cpa = DB::connection("oracle_satutujuh")->table('pengaktifan_objek')
                            ->where(['layanan_objek_id' => $layanan_objek_id])->first();
                        if ($cpa == null) {
                            DB::connection("oracle_satutujuh")->table('pengaktifan_objek')->insert($dataAktifkan);
                        } else {
                            DB::connection("oracle_satutujuh")->table('pengaktifan_objek')
                                ->where(['layanan_objek_id' => $layanan_objek_id])
                                ->update($dataAktifkan);
                        }

                        $tahunPenetapan = [];
                        foreach ($penetapan as $i => $a) {
                            // perlu kroscek
                            $tahunPenetapan[] = $i;
                        }



                        // migrasikan pembayaran
                        // PEMBAYARAN_SPPT_MUTASI_BATCH
                        if ($nop_asal <> $nop_proses) {

                            // cek tahun yang ken 4,5,6 dari nop asal
                            $gh = DB::connection("oracle_satutujuh")->table('pegurang_piutang')
                                // ->whereraw("kd_kategori in ('04','05','06') ")
                                ->where([
                                    'kd_propinsi' => $kd_propinsi_asal,
                                    'kd_dati2' => $kd_dati2_asal,
                                    'kd_kecamatan' => $kd_kecamatan_asal,
                                    'kd_kelurahan' => $kd_kelurahan_asal,
                                    'kd_blok' => $kd_blok_asal,
                                    'no_urut' => $no_urut_asal,
                                    'kd_jns_op' => $kd_jns_op_asal
                                ])->pluck('thn_pajak_sppt')->toarray();

                            foreach ($gh as $i) {
                                $tahunPenetapan[] = $i;
                                $penetapan[$i] = '31 Aug ' . $i;
                            }

                            $bm = [
                                'deskripsi' => "Dari " . formatnop($nop_asal) . " ke " . formatnop($nop_proses),
                                'nop_asal' => $nop_asal,
                                'nop_tujuan' => $nop_proses,
                                'created_at' => now(),
                                'created_by' => auth()->user()->id,
                                'layanan_objek_id' => $layanan_objek_id
                            ];

                            // get data pembayaran nop asal
                            $ps = PembayaranSppt::where([
                                'kd_propinsi' => $kd_propinsi_asal,
                                'kd_dati2' => $kd_dati2_asal,
                                'kd_kecamatan' => $kd_kecamatan_asal,
                                'kd_kelurahan' => $kd_kelurahan_asal,
                                'kd_blok' => $kd_blok_asal,
                                'no_urut' => $no_urut_asal,
                                'kd_jns_op' => $kd_jns_op_asal,

                            ])
                                ->whereNotIn('thn_pajak_sppt', $tahunPenetapan)
                                ->select(DB::raw('distinct kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt'))
                                ->get();

                            $pm = PembayaranMutasi::where(['layanan_objek_id' => $layanan_objek_id])->first();
                            if ($pm) {
                                $pm->update($bm);
                            } else {
                                $pm = PembayaranMutasi::create($bm);
                            }



                            // PEMBAYARAN_SPPT_MUTASI_DATA
                            PembayaranMutasiData::where(['psmb_id' => $pm->id])->delete();

                            foreach ($ps as $item) {
                                $bmo = [
                                    'kd_propinsi' => $kd_propinsi,
                                    'kd_dati2' => $kd_dati2,
                                    'kd_kecamatan' => $kd_kecamatan,
                                    'kd_kelurahan' => $kd_kelurahan,
                                    'kd_blok' => $kd_blok,
                                    'no_urut' => $no_urut,
                                    'kd_jns_op' => $kd_jns_op,
                                    'kd_propinsi_asal' => $kd_propinsi_asal,
                                    'kd_dati2_asal' => $kd_dati2_asal,
                                    'kd_kecamatan_asal' => $kd_kecamatan_asal,
                                    'kd_kelurahan_asal' => $kd_kelurahan_asal,
                                    'kd_blok_asal' => $kd_blok_asal,
                                    'no_urut_asal' => $no_urut_asal,
                                    'kd_jns_op_asal' => $kd_jns_op_asal,
                                    'created_at' => now(),
                                    'created_by' => auth()->user()->id,
                                    'psmb_id' => $pm->id,
                                    'thn_pajak_sppt' => $item->thn_pajak_sppt
                                ];

                                PembayaranMutasiData::create($bmo);
                            }
                        }


                        // mencari hkpd individu nop asal

                        if (count($penetapan) > 0) {
                            // get hkpd nya
                            /*   $get_hkpd = DB::connection("oracle_satutujuh")->table("sppt")
                                ->whereraw("sppt.kd_propinsi='$kd_propinsi_asal' and sppt.kd_dati2='$kd_dati2_asal' and sppt.kd_kecamatan='$kd_kecamatan_asal'                                      and sppt.kd_kelurahan='$kd_kelurahan_asal' and sppt.kd_blok='$kd_blok_asal' and sppt.no_urut='$no_urut_asal'  and sppt.kd_jns_op='$kd_jns_op_asal'  and thn_pajak_sppt='" . date('Y') . "'")
                                ->selectraw("max(hkpd) hkpd")->first(); */
                            $get_hkpd = DB::connection("oracle_satutujuh")
                                ->table("sppt")
                                ->where([
                                    ['sppt.kd_propinsi', '=', $kd_propinsi_asal],
                                    ['sppt.kd_dati2', '=', $kd_dati2_asal],
                                    ['sppt.kd_kecamatan', '=', $kd_kecamatan_asal],
                                    ['sppt.kd_kelurahan', '=', $kd_kelurahan_asal],
                                    ['sppt.kd_blok', '=', $kd_blok_asal],
                                    ['sppt.no_urut', '=', $no_urut_asal],
                                    ['sppt.kd_jns_op', '=', $kd_jns_op_asal],
                                    ['thn_pajak_sppt', '=', date('Y')],
                                ])
                                ->selectRaw("MAX(hkpd) AS hkpd")
                                ->first();


                            if ($get_hkpd) {
                                if ($get_hkpd->hkpd > 0) {
                                    $idv = [
                                        'thn_hkpd' => date('Y'),
                                        'kd_propinsi' => $kd_propinsi,
                                        'kd_dati2' => $kd_dati2,
                                        'kd_kecamatan' => $kd_kecamatan,
                                        'kd_kelurahan' => $kd_kelurahan,
                                        'kd_blok' => $kd_blok,
                                        'no_urut' => $no_urut,
                                        'kd_jns_op' => $kd_jns_op,
                                        'hkpd' => $get_hkpd->hkpd,
                                        'created_at' => Carbon::now(),
                                        'created_by' => auth()->user()->id,
                                    ];

                                    $connection = DB::connection("oracle_satutujuh");
                                    $table = "HKPD_OBJEK_INDIVIDU";

                                    $conditions = [
                                        'thn_hkpd'    => date('Y'),
                                        'kd_propinsi' => $kd_propinsi,
                                        'kd_dati2'    => $kd_dati2,
                                        'kd_kecamatan' => $kd_kecamatan,
                                        'kd_kelurahan' => $kd_kelurahan,
                                        'kd_blok'     => $kd_blok,
                                        'no_urut'     => $no_urut,
                                        'kd_jns_op'   => $kd_jns_op
                                    ];

                                    $exists = $connection->table($table)->where($conditions)->exists();

                                    if ($exists) {
                                        $connection->table($table)->where($conditions)->update($idv);
                                    } else {
                                        $connection->table($table)->insert($idv);
                                    }

                                    /*    $fk = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                        ->where([
                                            'thn_hkpd' => date('Y'),
                                            'kd_propinsi' => $kd_propinsi,
                                            'kd_dati2' => $kd_dati2,
                                            'kd_kecamatan' => $kd_kecamatan,
                                            'kd_kelurahan' => $kd_kelurahan,
                                            'kd_blok' => $kd_blok,
                                            'no_urut' => $no_urut,
                                            'kd_jns_op' => $kd_jns_op
                                        ])->count();

                                    if ($fk == 0) {
                                        DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                            ->insert($idv);
                                    } else {
                                        DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                            ->where([
                                                'thn_hkpd' => date('Y'),
                                                'kd_propinsi' => $kd_propinsi,
                                                'kd_dati2' => $kd_dati2,
                                                'kd_kecamatan' => $kd_kecamatan,
                                                'kd_kelurahan' => $kd_kelurahan,
                                                'kd_blok' => $kd_blok,
                                                'no_urut' => $no_urut,
                                                'kd_jns_op' => $kd_jns_op
                                            ])
                                            ->update($idv);
                                    } */
                                }
                            }
                        }

                        // menetapkan
                        foreach ($penetapan as $tahun_sppt => $jatuh_tempo) {
                            $jatuh_tempo = date('Ymd', strtotime($penetapan[$tahun_sppt]));
                            $tgl_terbit = date('Ymd');
                            // kroscek sppt existing
                            $spptExists = Sppt::where(
                                [
                                    'kd_propinsi' => $kd_propinsi,
                                    'kd_dati2' => $kd_dati2,
                                    'kd_kecamatan' => $kd_kecamatan,
                                    'kd_kelurahan' => $kd_kelurahan,
                                    'kd_blok' => $kd_blok,
                                    'no_urut' => $no_urut,
                                    'kd_jns_op' => $kd_jns_op,
                                    'thn_pajak_sppt' => $tahun_sppt
                                ]
                            )
                                ->whereraw("to_char(tgl_terbit_sppt,'yyyy') != to_char(sysdate,'yyyy')")
                                ->exists();
                            if (!$spptExists) {
                                PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                            }
                        }

                        //  // membentuk nota perhitungan
                        $tahun = date('Y');
                        $ArTagihan = [];
                        if ($nop_asal <> $nop_proses) {
                            $tagihan = DB::connection("oracle_spo")->table(db::raw("(
                                                                                select sppt_oltp.*
                                                                                from sppt_oltp
                                                                                left join   pbb.pengurang_piutang sppt_koreksi on sppt_oltp.kd_propinsi=sppt_koreksi.kd_propinsi and
                                                                                sppt_oltp.kd_dati2=sppt_koreksi.kd_dati2 and
                                                                                sppt_oltp.kd_Kecamatan=sppt_koreksi.kd_Kecamatan and
                                                                                sppt_oltp.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                                                                                sppt_oltp.kd_blok=sppt_koreksi.kd_blok and
                                                                                sppt_oltp.no_urut=sppt_koreksi.no_urut and
                                                                                sppt_oltp.kd_jns_op=sppt_koreksi.kd_jns_op and
                                                                                sppt_oltp.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt 
                                                                                where jns_koreksi is null
                                                                            ) sppt"))
                                ->select(db::raw("kd_propinsi,kd_dati2,kd_Kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt tahun_pajak,
                                                round(PBB_YG_HARUS_DIBAYAR_SPPT) pbb,nm_wp_sppt nm_wp,round(PBB_YG_HARUS_DIBAYAR_SPPT) pokok   , round(denda) denda,round(PBB_YG_HARUS_DIBAYAR_SPPT ) + round(denda) total"))
                                ->whereraw("(status_pembayaran_sppt != '1' or  PBB_YG_HARUS_DIBAYAR_SPPT>0)
                                        and thn_pajak_sppt>=2003 and thn_pajak_sppt<$tahun")
                                ->whereraw("(sppt.kd_propinsi='$kd_propinsi_asal' and sppt.kd_dati2='$kd_dati2_asal' and sppt.kd_kecamatan='$kd_kecamatan_asal'
                                                and sppt.kd_kelurahan='$kd_kelurahan_asal' 
                                        and sppt.kd_blok='$kd_blok_asal' and sppt.no_urut='$no_urut_asal'  and sppt.kd_jns_op='$kd_jns_op_asal')");

                            // $tagihan = $tagihan->whereraw("(" . $wh . ")");
                            $tagihan = $tagihan->get();
                            // dd($tagihan);
                            foreach ($tagihan as $tagihan) {
                                if ($tagihan->pokok > 0 and $tagihan->tahun_pajak >= 2003) {
                                    $ArTagihan[] = [
                                        'kd_propinsi' => $tagihan->kd_propinsi,
                                        'kd_dati2' => $tagihan->kd_dati2,
                                        'kd_kecamatan' => $tagihan->kd_kecamatan,
                                        'kd_kelurahan' => $tagihan->kd_kelurahan,
                                        'kd_blok' => $tagihan->kd_blok,
                                        'no_urut' => $tagihan->no_urut,
                                        'kd_jns_op' => $tagihan->kd_jns_op,
                                        'tahun_pajak' => $tagihan->tahun_pajak,
                                        'pbb' => $tagihan->pbb,
                                        'nm_wp' => $tagihan->nm_wp,
                                        'pokok' => $tagihan->pokok,
                                        'denda' => $tagihan->denda,
                                        'total' => $tagihan->total
                                    ];
                                }
                            }
                        }

                        $wh_tagihan_dua = "status_pembayaran_sppt!='1' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan'  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' ";
                        break;
                    default:
                        # code...
                        $wt = "";
                        if (count($penetapan) > 0) {
                            $wi = "";
                            foreach ($penetapan as $tahun_sppt => $jatuh_tempo) {
                                $wi .= "'" . $tahun_sppt . "',";
                                if ($jenis_pelayanan == '1') {
                                    $jatuh_tempo = date('Ymd', strtotime($jatuh_tempo));
                                    $tgl_terbit = date('Ymd');
                                    PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                                } else {
                                    // pembetulan
                                    if ($tahun_sppt == date('Y')) {
                                        $jatuh_tempo = date('Ymd', strtotime($jatuh_tempo));
                                        $tgl_terbit = date('Ymd');

                                        // identifikasi hkpd
                                        $hkpd = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                                            ->whereraw(" thn_hkpd='$tahun_sppt' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                                            and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")->count();

                                        if ($hkpd == 0) {
                                            $kd_kecamatan_asal = $kd_kecamatan_asal ?? $kd_kecamatan;
                                            $kd_kelurahan_asal = $kd_kelurahan_asal ?? $kd_kelurahan;
                                            $kd_blok_asal = $kd_blok_asal ?? $kd_blok;
                                            $no_urut_asal = $no_urut_asal ?? $no_urut;
                                            $kd_jns_op_asal = $kd_jns_op_asal ?? $kd_jns_op;

                                            $lh = DB::connection("oracle_satutujuh")->table("sppt")->select("hkpd")
                                                ->whereraw("thn_pajak_sppt='$tahun_sppt' and kd_kecamatan='$kd_kecamatan_asal' and kd_kelurahan='$kd_kelurahan_asal'
                                                             and kd_blok='$kd_blok_asal' and no_urut='$no_urut_asal' and kd_jns_op='$kd_jns_op_asal'")
                                                ->first();
                                            if ($lh) {

                                                $nilai = $lh->hkpd;
                                                $inshkpd = [
                                                    'kd_propinsi' => $kd_propinsi,
                                                    'kd_dati2' => $kd_dati2,
                                                    'kd_kecamatan' => $kd_kecamatan,
                                                    'kd_kelurahan' => $kd_kelurahan,
                                                    'kd_blok' => $kd_blok,
                                                    'no_urut' => $no_urut,
                                                    'kd_jns_op' => $kd_jns_op,
                                                    'hkpd' => $nilai,
                                                    'thn_hkpd' => $tahun_sppt,
                                                    'created_at' => Carbon::now(),
                                                    'created_by' => auth()->user()->id
                                                ];
                                                if ($nilai > 0) {
                                                    DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")->insert($inshkpd);
                                                    DB::connection("oracle_satutujuh")->commit();
                                                }
                                            }
                                        }
                                        PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun_sppt, $tgl_terbit, $jatuh_tempo);
                                    }
                                }
                            }
                            $wi = substr($wi, 0, -1);
                            $wt = " in (" . $wi . ")";
                        } else {
                            $sql_penetapan .= " PENENTUAN_NJOP ('$kd_propinsi', '$kd_dati2', '$kd_kecamatan', '$kd_kelurahan', '$kd_blok', '$no_urut', '$kd_jns_op', '" . date('Y') . "', '1' ); ";
                            $wt = " >=2013";
                        }

                        if (!isset($penetapan[date('Y')])) {
                            $sql_penetapan .= " PENENTUAN_NJOP ('$kd_propinsi', '$kd_dati2', '$kd_kecamatan', '$kd_kelurahan', '$kd_blok', '$no_urut', '$kd_jns_op', '" . date('Y') . "', '1' ); ";
                        }

                        $wh_tagihan_dua = "status_pembayaran_sppt!='1' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan'  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'  and thn_pajak_sppt " . $wt;
                        break;
                }

                // eksekusi penetapan
                if ($sql_penetapan != '') {
                    DB::connection("oracle_satutujuh")->statement("begin 
                            " . $sql_penetapan . "
                        end;");
                }

                if (!in_array($jns_bumi, [4, 5]) == true) {
                    // ambil tagihan hasil penetapan
                    $tagihanDua = DB::connection("oracle_spo")->table(DB::raw("(
                        select sppt_oltp.*
                        from sppt_oltp
                        left join pbb.pengurang_piutang sppt_koreksi on sppt_oltp.kd_propinsi=sppt_koreksi.kd_propinsi and
                        sppt_oltp.kd_dati2=sppt_koreksi.kd_dati2 and
                        sppt_oltp.kd_Kecamatan=sppt_koreksi.kd_Kecamatan and
                        sppt_oltp.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                        sppt_oltp.kd_blok=sppt_koreksi.kd_blok and
                        sppt_oltp.no_urut=sppt_koreksi.no_urut and
                        sppt_oltp.kd_jns_op=sppt_koreksi.kd_jns_op and
                        sppt_oltp.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt 
                        where sppt_koreksi.tgl_surat is null
                    )  sppt_oltp"))
                        ->select(db::raw("sppt_oltp.kd_propinsi,
                    sppt_oltp.kd_dati2,
                    sppt_oltp.kd_Kecamatan,
                    sppt_oltp.kd_kelurahan,
                    sppt_oltp.kd_blok,
                    sppt_oltp.no_urut,
                    sppt_oltp.kd_jns_op,
                    sppt_oltp.thn_pajak_sppt  tahun_pajak,
                            PBB_YG_HARUS_DIBAYAR_SPPT  pbb,nm_wp_sppt nm_wp,PBB_YG_HARUS_DIBAYAR_SPPT pokok   , denda denda,PBB_YG_HARUS_DIBAYAR_SPPT+denda total"))
                        ->whereraw($wh_tagihan_dua)
                        ->get();

                    foreach ($tagihanDua as $tagihan) {
                        if ($tagihan->pokok > 0) {
                            $ArTagihan[] = [
                                'kd_propinsi' => $tagihan->kd_propinsi,
                                'kd_dati2' => $tagihan->kd_dati2,
                                'kd_kecamatan' => $tagihan->kd_kecamatan,
                                'kd_kelurahan' => $tagihan->kd_kelurahan,
                                'kd_blok' => $tagihan->kd_blok,
                                'no_urut' => $tagihan->no_urut,
                                'kd_jns_op' => $tagihan->kd_jns_op,
                                'tahun_pajak' => $tagihan->tahun_pajak,
                                'pbb' => $tagihan->pbb,
                                'nm_wp' => $tagihan->nm_wp,
                                'pokok' => $tagihan->pokok,
                                'denda' => $tagihan->denda,
                                'total' => $tagihan->total
                            ];
                        }
                    }
                }

                /*    DB::statement(db::raw("begin update data_billing set deleted_at=sysdate
                            where deleted_at is null and data_billing_id in (select data_billing_id 
                            from nota_perhitungan
                        where layanan_objek_id='$layanan_objek_id');  commit;end;"));
 */

                DB::transaction(function () use ($layanan_objek_id) {
                    DB::table('data_billing')
                        ->whereNull('deleted_at')
                        ->whereIn('data_billing_id', function ($query) use ($layanan_objek_id) {
                            $query->select('data_billing_id')
                                ->from('nota_perhitungan')
                                ->where('layanan_objek_id', $layanan_objek_id);
                        })
                        ->update(['deleted_at' => DB::raw('SYSDATE')]);
                });


                $layanan_objek_id = $request->lhp_id;

                $cl = DB::table('data_billing')
                    ->whereIn('data_billing_id', function ($query) use ($layanan_objek_id) {
                        $query->select('data_billing_id')
                            ->from('nota_perhitungan')
                            ->where('layanan_objek_id', $layanan_objek_id);
                    })
                    ->select('kd_status')
                    ->orderByDesc('created_at')
                    ->first();

                if ($cl && $cl->kd_status == '0') {
                    DB::transaction(function () use ($layanan_objek_id) {
                        DB::table('data_billing')
                            ->whereNull('deleted_at')
                            ->whereIn('data_billing_id', function ($query) use ($layanan_objek_id) {
                                $query->select('data_billing_id')
                                    ->from('nota_perhitungan')
                                    ->where('layanan_objek_id', $layanan_objek_id);
                            })
                            ->where('kd_status', '0')
                            ->update(['deleted_at' => DB::raw('SYSDATE')]);
                    });
                }

                /*  $cl = DB::table('data_billing')->whereraw("data_billing_id in (select data_billing_id 
                                                    from nota_perhitungan
                                                    where layanan_objek_id='$layanan_objek_id')")
                    ->select("kd_status")->orderByraw("created_at desc")->first();

                if ($cl) {
                    if ($cl->kd_status == '0') {
                        DB::statement(db::raw("begin update data_billing set deleted_at=sysdate
                                        where deleted_at is null and data_billing_id in (select data_billing_id 
                                        from nota_perhitungan
                                        where layanan_objek_id='$layanan_objek_id') and kd_status='0' ;  commit;
                                    end;"));
                    }
                } */

                // log::info($ArTagihan);
                if (count($ArTagihan) > 0) {
                    $nop_nota = [
                        'nm_wp' => $request->nm_wp,
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'layanan_objek_id' => $request->lhp_id
                    ];
                    self::NotaPerhitunganDua($nop_nota, $ArTagihan);
                }
                DB::commit();
                $penilaian = 1;
            } catch (\Throwable $th) {
                //throw $th;
                log::emergency($th);
                DB::rollBack();
                $msg .= "Sistem gagal melakukan Penetapan ";
                $penilaian = 0;
            }
        }
        $kode_status = 0;
        if ($penilaian == '1' && $pendataan == '1') {
            $kode_status = 1;
        }
        $result = [
            'pendataan' => $pendataan,
            'penilaian' => $penilaian,
            'msg' => $msg,
            'kode' => $kode_status
        ];
        return $result;
    }


    public static function coreCancel($idobjek)
    {
        $list = DB::table(db::raw("sppt_penelitian"))->join(db::raw('layanan_objek'), 'layanan_objek.nomor_formulir', '=', 'sppt_penelitian.nomor_formulir')
            ->whereraw("layanan_objek.id='" . $idobjek . "'")
            ->select(db::raw("distinct sppt_penelitian.nomor_formulir,sppt_penelitian.kd_kecamatan,sppt_penelitian.kd_kelurahan,sppt_penelitian.kd_blok,sppt_penelitian.no_urut,sppt_penelitian.kd_jns_op,thn_pajak_sppt,data_billing_id,kobil"))
            ->get();


        // delete from nota_perhitungan where layanan_objek_id='".$idobjek."';

        DB::table("nota_perhitungan")->where('layanan_objek_id', $idobjek)->delete();

        // log::info(['list' => $list]);
        $formulir = [];
        $idbilling = [];
        $wr = "";
        foreach ($list as $rd) {
            $formulir[] = $rd->nomor_formulir;
            $idbilling[] = $rd->data_billing_id;

            // kd_kematan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt
            $wr .= "(status_pembayaran_sppt!='1' and  thn_pajak_sppt='" . $rd->thn_pajak_sppt . "' and kd_kecamatan='" . $rd->kd_kecamatan . "' and kd_kelurahan='" . $rd->kd_kelurahan . "' and kd_blok='" . $rd->kd_blok . "' and no_urut='" . $rd->no_urut . "' and kd_jns_op='" . $rd->kd_jns_op . "') or";
        }

        // log::info($idbilling);

        if (!empty($idbilling)) {
            $ff = "";
            foreach ($formulir as $row) {
                $ff .= "'" . $row . "',";
            }

            $ff = substr($ff, 0, -1);

            $idbilling = implode(',', $idbilling);
            $formulir = implode(',', $formulir);



            DB::statement(db::raw("
                        BEGIN
                        update  billing_kolektif set deleted_at=sysdate where data_billing_id in  (" . $idbilling . ");
                        update  data_billing set deleted_at=sysdate  where data_billing_id in  (" . $idbilling . ");
                        update pbb.dat_objek_pajak set jns_transaksi_op='3'  where no_formulir_spop in (" . $ff . ");
                        delete from nota_perhitungan where data_billing_id in  (" . $idbilling . ");
                        delete from sppt_penelitian where nomor_formulir in (" . $formulir . ");
                        delete from nota_perhitungan where layanan_objek_id='" . $idobjek . "';
                        end;
            "));
        }

        if ($wr <> '') {
            $wr = substr($wr, 0, -2);
            DB::connection('oracle_satutujuh')->table(db::raw('sppt'))->whereraw($wr)->update(['status_pembayaran_sppt' => '3']);
        }
    }

    public static function cancelPenelitian(Request $request)
    {

        $list = DB::table(db::raw("sppt_penelitian"))->join(db::raw('layanan_objek'), 'layanan_objek.nomor_formulir', '=', 'sppt_penelitian.nomor_formulir')
            ->whereraw("layanan_objek.id='" . $request->lhp_id . "'")
            ->select(db::raw("distinct sppt_penelitian.nomor_formulir,sppt_penelitian.kd_kecamatan,sppt_penelitian.kd_kelurahan,sppt_penelitian.kd_blok,sppt_penelitian.no_urut,sppt_penelitian.kd_jns_op,thn_pajak_sppt,data_billing_id,kobil"))
            ->get();
        $formulir = [];
        $idbilling = [];
        $wr = "";
        foreach ($list as $rd) {
            $formulir[] = $rd->nomor_formulir;
            $idbilling[] = $rd->data_billing_id;

            // kd_kematan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt
            $wr .= "(thn_pajak_sppt='" . $rd->thn_pajak_sppt . "' and kd_kecamatan='" . $rd->kd_kecamatan . "' and kd_kelurahan='" . $rd->kd_kelurahan . "' and kd_blok='" . $rd->kd_blok . "' and no_urut='" . $rd->no_urut . "' and kd_jns_op='" . $rd->kd_jns_op . "') or";
        }

        if (!empty($idbilling)) {

            $ff = "";
            foreach ($formulir as $row) {
                $ff .= "'" . $row . "',";
            }

            $ff = substr($ff, 0, -1);

            $idbilling = implode(',', $idbilling);
            $formulir = implode(',', $formulir);


            $idobjek = $request->lhp_id;
            DB::statement(db::raw("
                        BEGIN
                        update  billing_kolektif set deleted_at=sysdate where data_billing_id in  (" . $idbilling . ");
                        update  data_billing set deleted_at=sysdate  where data_billing_id in  (" . $idbilling . ");
                        update pbb.dat_objek_pajak set jns_transaksi_op='3'  where no_formulir_spop in (" . $ff . ");
                        delete from nota_perhitungan where data_billing_id in  (" . $idbilling . ");
                        delete from sppt_penelitian where nomor_formulir in (" . $formulir . ");
                        delete from nota_perhitungan where layanan_objek_id='" . $idobjek . "';
                        end;
            "));


            /*  DB::statement(db::raw("
                BEGIN

                update  billing_kolektif set deleted_at=sysdate where data_billing_id in  (" . $idbilling . ");
                update  data_billing set deleted_at=sysdate  where data_billing_id in  (" . $idbilling . ");

                delete from nota_perhitungan where data_billing_id in  (" . $idbilling . ");
                delete from sppt_penelitian where nomor_formulir in (" . $formulir . ");
                update pbb.dat_objek_pajak set jns_transaksi_op='3' where nomor_formulir in (" . $formulir . ");
                commit;
                end;
            ")); */
            // DB::table('sppt_penelitian')->wherein('nomor_formulir', $formulir)->delete();
        }

        if ($wr <> '') {
            $wr = substr($wr, 0, -2);
            DB::connection('oracle_satutujuh')->table(db::raw('sppt'))->whereraw($wr)->delete();
        }


        return $list;
    }


    public static function prosesEditPenelitianDua(Request $request)
    {

        self::cancelPenelitian($request);
        return self::prosesPenelitianDua($request);
    }


    public static function prosesPenetapan($data)
    {
        try {
            //code...
            $V_PROP = $data['kd_propinsi'];
            $V_DAT = $data['kd_dati2'];
            $V_KEC = $data['kd_kecamatan'];
            $V_KEL = $data['kd_kelurahan'];
            $V_BLOK = $data['kd_blok'];
            $V_URUT = $data['no_urut'];
            $V_JNS = $data['kd_jns_op'];
            $V_THN = $data['tahun'];
            $V_NIP = $data['nip'];
            $TRB = $data['terbit'];
            $TJTT = $data['tempo'];

            DB::connection("oracle_satutujuh")->statement("DECLARE 
                            V_PROP VARCHAR2(32767);
                            V_DAT VARCHAR2(32767);
                            V_KEC VARCHAR2(32767);
                            V_KEL VARCHAR2(32767);
                            V_BLOK VARCHAR2(32767);
                            V_URUT VARCHAR2(32767);
                            V_JNS VARCHAR2(32767);
                            V_THN VARCHAR2(32767);
                            TRB DATE;
                            TJTT DATE;
                            V_NIP VARCHAR2(32767);

                            BEGIN 
                            V_PROP := '" . $V_PROP . "';
                            V_DAT := '" . $V_DAT . "';
                            V_KEC := '" . $V_KEC . "';
                            V_KEL := '" . $V_KEL . "';
                            V_BLOK := '" . $V_BLOK . "';
                            V_URUT := '" . $V_URUT . "';
                            V_JNS := '" . $V_JNS . "';
                            V_THN := '" . $V_THN . "';
                            TRB := to_date('" . $TRB . "','yyyymmdd');
                            TJTT :=to_date('" . $TJTT . "','yyyymmdd');
                            V_NIP := '" . $V_NIP . "';

                            PBB.PENETAPAN_NOP_20 ( V_PROP, V_DAT, V_KEC, V_KEL, V_BLOK, V_URUT, V_JNS, V_THN, TRB, TJTT, V_NIP );
                            END; 
                            ");

            $status = true;
        } catch (\Throwable $th) {
            //throw $th;
            log::emergency($th);
            $status = false;
        }
        return $status;
    }

    public static function ProsesSubjek($data)
    {
        $subjek_pajak_id = trim($data['subjek_pajak_id']) ?? null;
        $nm_wp = $data['nm_wp'] ?? null;
        $jalan_wp = $data['jalan_wp'] ?? null;
        $blok_kav_no_wp = $data['blok_kav_no_wp'] ?? null;
        $rw_wp = $data['rw_wp'] ?? null;
        $rt_wp = $data['rt_wp'] ?? null;
        $kelurahan_wp = $data['kelurahan_wp'] ?? null;
        $kecamatan_wp = $data['kecamatan_wp'] ?? null;
        $kota_wp = $data['kota_wp'] ?? null;
        $propinsi_wp = $data['propinsi_wp'] ?? null;
        $kd_pos_wp = $data['kd_pos_wp'] ?? null;
        $telp_wp = $data['telp_wp'] ?? null;
        $npwp = $data['npwp'] ?? null;
        $status_pekerjaan_wp = $data['status_pekerjaan_wp'] ?? null;

        // cek di 1.7
        $cek = SubjekPajak::whereraw("trim(subjek_pajak_id)='" . $subjek_pajak_id . "'")->get();
        // log::info($cek);
        if ($cek->count() > 0) {
            /*  if (config('app.env') != 'local') {
                DB::connection('oracle_dua')->table('dat_subjek_pajak')->whereraw("trim(subjek_pajak_id)='" . $subjek_pajak_id . "'")
                    ->update([
                        'nm_wp' => $nm_wp,
                        'jalan_wp' => $jalan_wp,
                        'blok_kav_no_wp' => $blok_kav_no_wp,
                        'rw_wp' => $rw_wp,
                        'rt_wp' => $rt_wp,
                        'kelurahan_wp' => $kelurahan_wp,
                        'kota_wp' => $kota_wp,
                        'kd_pos_wp' => $kd_pos_wp,
                        'telp_wp' => $telp_wp,
                        'npwp' => $npwp,
                        'status_pekerjaan_wp' => $status_pekerjaan_wp
                    ]);
            } */
            $taxSubject = SubjekPajak::whereraw("trim(subjek_pajak_id)='" . $subjek_pajak_id . "'")->update([
                'nm_wp' => $nm_wp,
                'jalan_wp' => $jalan_wp,
                'blok_kav_no_wp' => $blok_kav_no_wp,
                'rw_wp' => $rw_wp,
                'rt_wp' => $rt_wp,
                'kelurahan_wp' => $kelurahan_wp,
                'kota_wp' => $kota_wp,
                'kd_pos_wp' => $kd_pos_wp,
                'telp_wp' => $telp_wp,
                'npwp' => $npwp,
                'status_pekerjaan_wp' => $status_pekerjaan_wp
            ]);

            $taxSubject = SubjekPajak::whereraw("trim(subjek_pajak_id)='" . $subjek_pajak_id . "'")->first();
        } else {
            /* if (config('app.env') != 'local') {
                DB::connection('oracle_dua')->table("dat_subjek_pajak")->insert([
                    'subjek_pajak_id' => $subjek_pajak_id,
                    'nm_wp' => $nm_wp,
                    'jalan_wp' => $jalan_wp,
                    'blok_kav_no_wp' => $blok_kav_no_wp,
                    'rw_wp' => $rw_wp,
                    'rt_wp' => $rt_wp,
                    'kelurahan_wp' => $kelurahan_wp,
                    'kota_wp' => $kota_wp,
                    'kd_pos_wp' => $kd_pos_wp,
                    'telp_wp' => $telp_wp,
                    'npwp' => $npwp,
                    'status_pekerjaan_wp' => $status_pekerjaan_wp
                ]);
            } */

            $taxSubject = SubjekPajak::Create(
                [
                    'subjek_pajak_id' => $subjek_pajak_id,
                    'nm_wp' => $nm_wp,
                    'jalan_wp' => $jalan_wp,
                    'blok_kav_no_wp' => $blok_kav_no_wp,
                    'rw_wp' => $rw_wp,
                    'rt_wp' => $rt_wp,
                    'kelurahan_wp' => $kelurahan_wp,
                    'kota_wp' => $kota_wp,
                    'kd_pos_wp' => $kd_pos_wp,
                    'telp_wp' => $telp_wp,
                    'npwp' => $npwp,
                    'status_pekerjaan_wp' => $status_pekerjaan_wp
                ]
            );
        }
        return $taxSubject;
    }

    public static function ProsesSpop($spop)
    {
        $no_formulire = $spop['NO_FORMULIR'];
        $cek = TblSpop::whereraw("trim(no_formulir) ='" . trim($no_formulire) . "'")->get();
        if ($cek->count() > 0) {
            $spop = TblSpop::whereraw("trim(no_formulir) ='" . trim($no_formulire) . "'")->update($spop);
        } else {
            $spop = TblSpop::Create(
                $spop
            );
        }
        return  $spop;
    }

    public static function ProsesObjek($objek)
    {
        $kd_propinsi = $objek['kd_propinsi'];
        $kd_dati2 = $objek['kd_dati2'];
        $kd_kecamatan = $objek['kd_kecamatan'];
        $kd_kelurahan = $objek['kd_kelurahan'];
        $kd_blok = $objek['kd_blok'];
        $no_urut = $objek['no_urut'];
        $kd_jns_op = $objek['kd_jns_op'];

        $cek = ObjekPajak::whereraw("kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'")->first();
        if ($cek) {
            // update
            unset($objek['kd_propinsi']);
            unset($objek['kd_dati2']);
            unset($objek['kd_kecamatan']);
            unset($objek['kd_kelurahan']);
            unset($objek['kd_blok']);
            unset($objek['no_urut']);
            unset($objek['kd_jns_op']);

            $res = ObjekPajak::whereraw("kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'")->update($objek);

            /* if (config('app.env') != 'local') {
                DB::connection('oracle_satutujuh')->table('dat_objek_pajak')->whereraw("kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'")
                    ->update($objek);
            } */
        } else {
            // insert
            /* if (config('app.env') != 'local') {
                DB::connection('oracle_satutujuh')->table('dat_objek_pajak')->insert($objek);
            } */

            $res = ObjekPajak::create($objek);
        }
        return $res;
    }

    public static function ProsesBumi($objek)
    {
        $kd_propinsi = $objek['kd_propinsi'];
        $kd_dati2 = $objek['kd_dati2'];
        $kd_kecamatan = $objek['kd_kecamatan'];
        $kd_kelurahan = $objek['kd_kelurahan'];
        $kd_blok = $objek['kd_blok'];
        $no_urut = $objek['no_urut'];
        $kd_jns_op = $objek['kd_jns_op'];

        $cek = OpBumi::whereraw("kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'")->first();
        if ($cek) {
            // update
            unset($objek['kd_propinsi']);
            unset($objek['kd_dati2']);
            unset($objek['kd_kecamatan']);
            unset($objek['kd_kelurahan']);
            unset($objek['kd_blok']);
            unset($objek['no_urut']);
            unset($objek['kd_jns_op']);

            $res = OpBumi::whereraw("kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'")->update($objek);
        } else {
            $res = OpBumi::create($objek);
        }

        return $res;
    }

    public static function PenilaianBumi($objek)
    {
        // penilaian
        $vlc_kd_propinsi = $objek['kd_propinsi'];
        $vlc_kd_dati2 = $objek['kd_dati2'];
        $vlc_kd_kecamatan = $objek['kd_kecamatan'];
        $vlc_kd_kelurahan = $objek['kd_kelurahan'];
        $vlc_kd_blok = $objek['kd_blok'];
        $vlc_no_urut = $objek['no_urut'];
        $vlc_kd_jns_op = $objek['kd_jns_op'];
        $vln_no_bumi = $objek['no_bumi'];
        $vlc_kd_znt = $objek['kd_znt'];
        $vln_luas_bumi = $objek['luas_bumi'];
        $vlc_tahun = $objek['tahun'];
        // $vln_flag_update='1';
        $vln_nilai_bumi = 0;
        // try {
        //code...

        DB::connection('oracle_satutujuh')->statement("DECLARE 
            VLC_KD_PROPINSI CHAR(2);
            VLC_KD_DATI2 CHAR(2);
            VLC_KD_KECAMATAN CHAR(3);
            VLC_KD_KELURAHAN CHAR(3);
            VLC_KD_BLOK CHAR(3);
            VLC_NO_URUT CHAR(4);
            VLC_KD_JNS_OP CHAR(1);
            VLN_NO_BUMI NUMBER;
            VLC_KD_ZNT CHAR(2);
            VLN_LUAS_BUMI NUMBER;
            VLC_TAHUN CHAR(4);
            VLN_FLAG_UPDATE NUMBER;
            VLN_NILAI_BUMI NUMBER;
          
          BEGIN 
            VLC_KD_PROPINSI :='" . $vlc_kd_propinsi . "';
            VLC_KD_DATI2 :='" . $vlc_kd_dati2 . "';
            VLC_KD_KECAMATAN :='" . $vlc_kd_kecamatan . "';
            VLC_KD_KELURAHAN :='" . $vlc_kd_kelurahan . "';
            VLC_KD_BLOK :='" . $vlc_kd_blok . "';
            VLC_NO_URUT :='" . $vlc_no_urut . "';
            VLC_KD_JNS_OP :='" . $vlc_kd_jns_op . "';
            VLN_NO_BUMI :=" . $vln_no_bumi . ";
            VLC_KD_ZNT :='" . $vlc_kd_znt . "';
            VLN_LUAS_BUMI :=" . $vln_luas_bumi . ";
            VLC_TAHUN :='" . $vlc_tahun . "';
            VLN_FLAG_UPDATE :=1;
            VLN_NILAI_BUMI :='" . $vln_nilai_bumi . "';
          
            PENILAIAN_BUMI ( VLC_KD_PROPINSI, VLC_KD_DATI2, VLC_KD_KECAMATAN, VLC_KD_KELURAHAN, VLC_KD_BLOK, VLC_NO_URUT, VLC_KD_JNS_OP, VLN_NO_BUMI, VLC_KD_ZNT, VLN_LUAS_BUMI, VLC_TAHUN, VLN_FLAG_UPDATE, VLN_NILAI_BUMI );

            PBB.PENENTUAN_NJOP_BUMI ( VLC_KD_PROPINSI, VLC_KD_DATI2, VLC_KD_KECAMATAN, VLC_KD_KELURAHAN, VLC_KD_BLOK, VLC_NO_URUT, VLC_KD_JNS_OP, VLC_TAHUN, VLN_FLAG_UPDATE, VLN_NILAI_BUMI );
            commit;

          END; 
          ");

        return true;
    }

    public static function PenilaianBangunan($objek)
    {


        // penilaian
        $vlc_kd_propinsi = $objek['kd_propinsi'];
        $vlc_kd_dati2 = $objek['kd_dati2'];
        $vlc_kd_kecamatan = $objek['kd_kecamatan'];
        $vlc_kd_kelurahan = $objek['kd_kelurahan'];
        $vlc_kd_blok = $objek['kd_blok'];
        $vlc_no_urut = $objek['no_urut'];
        $vlc_kd_jns_op = $objek['kd_jns_op'];
        $vlc_tahun = $objek['tahun'];


        DB::connection('oracle_satutujuh')->statement("DECLARE 
                                                VTAHUN NUMBER;
                                                VKD_KEC VARCHAR2(32767);
                                                VKD_KEL VARCHAR2(32767);
                                                VKBLOK VARCHAR2(32767);
                                                VURUT VARCHAR2(32767);
                                                VJNSOP VARCHAR2(32767);
                                            
                                            BEGIN 
                                                VTAHUN :=" . $vlc_tahun . ";
                                                VKD_KEC :='" . $vlc_kd_kecamatan . "';
                                                VKD_KEL := '" . $vlc_kd_kelurahan . "';
                                                VKBLOK := '" . $vlc_kd_blok . "';
                                                VURUT :=  '" . $vlc_no_urut . "';
                                                VJNSOP :='" . $vlc_kd_jns_op . "';
                                            
                                                PBB.PENILAIAN_BNG_NOP ( VTAHUN, VKD_KEC, VKD_KEL, VKBLOK, VURUT, VJNSOP );
                                                COMMIT; 
                                            END; ");



        return true;
    }


    public static function PenentuanNjop($objek)
    {
        // try {
        $vlc_kd_propinsi = $objek['kd_propinsi'];
        $vlc_kd_dati2 = $objek['kd_dati2'];
        $vlc_kd_kecamatan = $objek['kd_kecamatan'];
        $vlc_kd_kelurahan = $objek['kd_kelurahan'];
        $vlc_kd_blok = $objek['kd_blok'];
        $vlc_no_urut = $objek['no_urut'];
        $vlc_kd_jns_op = $objek['kd_jns_op'];
        $vlc_tahun = $objek['tahun'];

        DB::connection('oracle_satutujuh')->statement("DECLARE 
                P_PROP CHAR(2);
                P_DATI2 CHAR(2);
                P_KEC CHAR(3);
                P_KEL CHAR(3);
                P_BLOK CHAR(3);
                P_URUT CHAR(4);
                P_JNS CHAR(1);
                P_THN CHAR(4);
                P_FLAG NUMBER;
                BEGIN 

                P_PROP :='" . $vlc_kd_propinsi . "';
                P_DATI2 :='" . $vlc_kd_dati2 . "';
                P_KEC :='" . $vlc_kd_kecamatan . "';
                P_KEL :='" . $vlc_kd_kelurahan . "';
                P_BLOK :='" . $vlc_kd_blok . "';
                P_URUT :='" . $vlc_no_urut . "';
                P_JNS :='" . $vlc_kd_jns_op . "';
                P_THN :='" . $vlc_tahun . "';
                P_FLAG := '1';
                PBB.PENENTUAN_NJOP ( P_PROP, P_DATI2, P_KEC, P_KEL, P_BLOK, P_URUT, P_JNS, P_THN, P_FLAG );
                COMMIT; 
                END;");

        return true;
    }


    public static function NomorFormulir($tipe, $tahun = null, $jtr = null)
    {

        $status = false;
        $nomor = null;
        $des = null;

        if ($tipe == '1') {
            // spop
            $res = DB::connection('oracle_satutujuh')->select(DB::raw("select to_char(sysdate,'yymmdd')||lpad(case when   max(substr(no_formulir,7,5)) is null then 1 else max(substr(no_formulir,7,5)) + 2  end,5,'0') nomor
            from (
                select no_formulir from tbl_spop
                where  mod(to_number(substr(no_formulir,7,5)),2)=1
                union all
                select no_formulir from tmp_formulir
                where  mod(to_number(substr(no_formulir,7,5)),2)=1
                )
            where no_formulir like to_char(sysdate,'yymmdd')||'%'"))[0];
            $status = true;
            $nomor = $res->nomor;
            $des = null;
        }

        if ($tipe == '2') {
            // lspop
            $res = DB::connection('oracle_satutujuh')->select(DB::raw("select to_char(sysdate,'yymmdd')||lpad(case when   max(substr(no_formulir,7,5)) is null then 2 else max(substr(no_formulir,7,5)) + 2  end,5,'0') nomor
            from (
                select no_formulir from tbl_lspop
                where  mod(to_number(substr(no_formulir,7,5)),2)=0
                union all
                select no_formulir from tmp_formulir
                where  mod(to_number(substr(no_formulir,7,5)),2)=0
                )
            where no_formulir like to_char(sysdate,'yymmdd')||'%'"))[0];
            $status = true;
            $nomor = $res->nomor;
            $des = null;
        }

        DB::connection('oracle_satutujuh')->table('tmp_formulir')->insert([
            'no_formulir' => $nomor
        ]);


        return [
            'status' => $status,
            'nomor' => $nomor,
            'des' => $des
        ];
    }

    public static function arrayFasilitas($vObject, $lspop)
    {
        $arrayFasilitas = [];
        $kd_jpb = $lspop['kd_jpb'];
        if ($lspop['daya_listrik'] <> '') {
            // listrik
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '44',
                'jml_satuan' => $lspop['daya_listrik'] <> '' ? $lspop['daya_listrik'] : 0
            ];
        }

        if ($lspop['acsplit'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '01',
                'jml_satuan' => $lspop['acsplit'] <> '' ? $lspop['acsplit'] : 0
            ];
        }
        if ($lspop['acwindow'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '02',
                'jml_satuan' => $lspop['acwindow'] <> '' ? $lspop['acwindow'] : 0
            ];
        }

        if ($lspop['acsentral']  && $lspop['acsentral'] <> '2') {
            switch ($kd_jpb) {
                case "02":
                    $facCode = "03";
                    break;
                case "07":
                    $facCode = "04";
                    break;
                case "04":
                    $facCode = "06";
                    break;
                case "05":
                    $facCode = "07";
                    break;
                case "13":
                    $facCode = "09";
                    break;
                default:
                    $facCode = "11";
                    break;
            }

            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $facCode,
                'jml_satuan' => 1 <> '' ? 1 : 0
            ];
        }

        // kolam
        if ($lspop['luas_kolam'] > 0 && $lspop['finishing_kolam'] <> '') {
            switch ($kd_jpb) {
                case "1":
                    $facCode = "12";
                    break;
                default:
                    $facCode = "13";
                    break;
            }

            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $facCode,
                'jml_satuan' => $lspop['luas_kolam'] <> '' ? $lspop['luas_kolam'] : 0
            ];
        }

        if ($lspop['luas_perkerasan_ringan'] <> '' && $lspop['luas_perkerasan_ringan'] > 0) {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '14',
                'jml_satuan' => $lspop['luas_perkerasan_ringan'] <> '' ? $lspop['luas_perkerasan_ringan'] : 0
            ];
        }
        if ($lspop['luas_perkerasan_sedang'] <> '' && $lspop['luas_perkerasan_sedang'] > 0) {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '15',
                'jml_satuan' => $lspop['luas_perkerasan_sedang'] <> '' ? $lspop['luas_perkerasan_sedang'] : 0
            ];
        }
        if ($lspop['luas_perkerasan_berat'] <> '' && $lspop['luas_perkerasan_berat'] > 0) {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '16',
                'jml_satuan' => $lspop['luas_perkerasan_berat'] <> '' ? $lspop['luas_perkerasan_berat'] : 0
            ];
        }
        if ($lspop['luas_perkerasan_dg_tutup'] <> '' && $lspop['luas_perkerasan_dg_tutup'] > 0) {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '17',
                'jml_satuan' => $lspop['luas_perkerasan_dg_tutup'] <> '' ? $lspop['luas_perkerasan_dg_tutup'] : 0
            ];
        }

        if ($lspop['lap_tenis_lampu_beton'] <> '') {
            $kode = $lspop['lap_tenis_lampu_beton'] > 1 ? '24' : '18';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['lap_tenis_lampu_beton'] <> '' ? $lspop['lap_tenis_lampu_beton'] : 0
            ];
        }

        if ($lspop['lap_tenis_lampu_aspal'] <> '') {
            $kode = $lspop['lap_tenis_lampu_aspal'] > 1 ? '25' : '19';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['lap_tenis_lampu_aspal'] <> '' ? $lspop['lap_tenis_lampu_aspal'] : 0
            ];
        }
        if ($lspop['lap_tenis_lampu_rumput'] <> '') {
            $kode = $lspop['lap_tenis_lampu_rumput'] > 1 ? '26' : '20';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['lap_tenis_lampu_rumput'] <> '' ? $lspop['lap_tenis_lampu_rumput'] : 0
            ];
        }
        if ($lspop['lap_tenis_beton'] <> '') {
            $kode = $lspop['lap_tenis_beton'] > 1 ? '27' : '21';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['lap_tenis_beton'] <> '' ? $lspop['lap_tenis_beton'] : 0
            ];
        }
        if ($lspop['lap_tenis_aspal'] <> '') {
            $kode = $lspop['lap_tenis_aspal'] > 1 ? '28' : '22';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['lap_tenis_aspal'] <> '' ? $lspop['lap_tenis_aspal'] : 0
            ];
        }
        if ($lspop['lap_tenis_rumput'] <> '') {
            $kode = $lspop['lap_tenis_rumput'] > 1 ? '29' : '23';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['lap_tenis_rumput'] <> '' ? $lspop['lap_tenis_rumput'] : 0
            ];
        }

        if ($lspop['lift_penumpang'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '30',
                'jml_satuan' => $lspop['lift_penumpang'] <> '' ? $lspop['lift_penumpang'] : 0
            ];
        }
        if ($lspop['lift_kapsul'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '31',
                'jml_satuan' => $lspop['lift_kapsul'] <> '' ? $lspop['lift_kapsul'] : 0
            ];
        }
        if ($lspop['lift_barang'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '32',
                'jml_satuan' => $lspop['lift_barang'] <> '' ? $lspop['lift_barang'] : 0
            ];
        }
        if ($lspop['tgg_berjalan_a'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '33',
                'jml_satuan' => $lspop['tgg_berjalan_a'] <> '' ? $lspop['tgg_berjalan_a'] : 0
            ];
        }
        if ($lspop['tgg_berjalan_b'] <> '') {
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => '34',
                'jml_satuan' => $lspop['tgg_berjalan_b'] <> '' ? $lspop['tgg_berjalan_b'] : 0
            ];
        }
        if ($lspop['pjg_pagar'] <> '' && $lspop['bhn_pagar'] <> '') {
            $kode = $lspop['bhn_pagar'] == '1' ? '35' : '36';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['tgg_berjalan_b'] <> '' ? $lspop['tgg_berjalan_b'] : 0
            ];
        }

        if ($lspop['hydrant'] <> '' || $lspop['hydrant'] <> '0') {
            $kode = '37';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['hydrant'] <> '' ? $lspop['hydrant'] : 0
            ];
        }
        if ($lspop['sprinkler'] <> '' || $lspop['sprinkler'] <> '0') {
            $kode = '38';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['sprinkler'] <> '' ? $lspop['sprinkler'] : 0
            ];
        }
        if ($lspop['fire_alarm'] <> '' || $lspop['fire_alarm'] <> '0') {
            $kode = '39';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['fire_alarm'] <> '' ? $lspop['fire_alarm'] : 0
            ];
        }
        if ($lspop['jml_pabx'] <> '') {
            $kode = '41';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['jml_pabx'] <> '' ? $lspop['jml_pabx'] : 0
            ];
        }
        if ($lspop['sumur_artesis'] <> '') {
            $kode = '42';
            $arrayFasilitas[] = [
                'kd_propinsi' => $vObject['kd_propinsi'],
                'kd_dati2' => $vObject['kd_dati2'],
                'kd_kecamatan' => $vObject['kd_kecamatan'],
                'kd_kelurahan' => $vObject['kd_kelurahan'],
                'kd_blok' => $vObject['kd_blok'],
                'no_urut' => $vObject['no_urut'],
                'kd_jns_op' => $vObject['kd_jns_op'],
                'no_bng' => $lspop['no_bng'],
                'kd_fasilitas' => $kode,
                'jml_satuan' => $lspop['sumur_artesis'] <> '' ? $lspop['sumur_artesis'] : 0
            ];
        }
        return $arrayFasilitas;
    }
    public static function arrayJpb($vObject, $lspop)
    {
        // return "Data JPB";
        $jpb = [];
        $kd_jpb = $lspop['kd_jpb'];
        $kd_propinsi = $vObject['kd_propinsi'];
        $kd_dati2 = $vObject['kd_dati2'];
        $kd_kecamatan = $vObject['kd_kecamatan'];
        $kd_kelurahan = $vObject['kd_kelurahan'];
        $kd_blok = $vObject['kd_blok'];
        $no_urut = $vObject['no_urut'];
        $kd_jns_op = $vObject['kd_jns_op'];

        $intjpb = (int)$kd_jpb;
        if (in_array($intjpb, ['12', '13', '14', '15', '16', '2', '3', '4', '5', '6', '7', '8', '9'])) {
            $idx = 'dat_jpb' . (int)$kd_jpb;
            $res = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'no_bng' => $lspop['no_bng']
            ];

            //jbb 15
            if ($intjpb == 15) {
                $res['letak_tangki_jpb15'] = $lspop['jpb15_letak_tangki'];
                $res['kapasitas_tangki_jpb15'] = $lspop['jpb15_kapasitas_tangki'];
            }

            // jpb 7 hotel/resport
            if ($intjpb == 7) {
                $res['jns_jpb7'] = $lspop['jpb7_jns_hotel'] ?? '1';
                $res['bintang_jpb7'] = $lspop['bintang_jpb7'] ?? 0;
                $res['jml_kmr_jpb7'] = $lspop['jml_kmr_jpb7'] ?? 0;
                $res['luas_kmr_jpb7_dgn_ac_sent'] = $lspop['luas_kmr_jpb7_dgn_ac_sent'] ?? 0;
                $res['luas_kmr_lain_jpb7_dgn_ac_sent'] = $lspop['luas_kmr_lain_jpb7_dgn_ac_sent'] ?? 0;
            }

            // jpb kelas lain
            $kl = ['2', '4', '6', '9', '12', '16'];
            if (in_array($intjpb, $kl)) {
                if ($intjpb == '12') {
                    $res['type_jpb12'] = $lspop['jpb_lain_kls_bng'] <> '' ? $lspop['jpb_lain_kls_bng'] : 1;
                } else {
                    $res['kls_jpb' . (int)$kd_jpb] = $lspop['jpb_lain_kls_bng'] <> '' ? $lspop['jpb_lain_kls_bng'] : 1;
                }
            }

            // jpb 3 atau 8
            if (in_array($intjpb, ['3', '8'])) {
                // jpb 3

                // if ($lspop['jpb3_8_dd_lantai'] <> '') {

                if ($lspop['jpb3_8_dd_lantai'] == '' || $lspop['jpb3_8_dd_lantai'] == 0) {
                    $ddl = 1;
                } else {
                    $ddl = $lspop['jpb3_8_dd_lantai'];
                }

                $ckt = DB::connection('oracle_satutujuh')->table('daya_dukung')->whereraw("daya_dukung_lantai_min_dbkb<=$ddl 
                    and daya_dukung_lantai_max_dbkb>=$ddl")->select('type_konstruksi')->first();

                // log::info($lspop['jpb3_8_dd_lantai']);

                $tp = 1;
                if ($ckt) {
                    $tp = $ckt->type_konstruksi ?? 1;
                }

                $jpb_38 = (int)$kd_jpb;
                $res['ting_kolom_jpb' . $jpb_38] = $lspop['jpb3_8_tinggi_kolom'] == '' ? '0' : $lspop['jpb3_8_tinggi_kolom'];
                $res['lbr_bent_jpb' . $jpb_38] = $lspop['jpb3_8_lebar_bentang'] == '' ? '0' : $lspop['jpb3_8_lebar_bentang'];
                $res['luas_mezzanine_jpb' . $jpb_38] = $lspop['jpb3_8_mezzanine'] == '' ? '0' : $lspop['jpb3_8_mezzanine'];
                $res['keliling_dinding_jpb' . $jpb_38] = $lspop['jpb3_8_kel_dinding'] == '' ? '0' : $lspop['jpb3_8_kel_dinding'];
                $res['daya_dukung_lantai_jpb' . $jpb_38] = $lspop['jpb3_8_dd_lantai'] == '' ? '0' : $lspop['jpb3_8_dd_lantai'];
                $res['type_konstruksi'] = $tp;
                // }
            }

            if ($kd_jpb == '05') {
                if ($lspop['jpb5_kls_bng'] <> '') {
                    $res['kls_jpb5'] = $lspop['jpb5_kls_bng'];
                    $res['luas_kmr_jpb5_dgn_ac_sent'] = $lspop['jpb5_luas_kamar'];
                    $res['luas_rng_lain_jpb5_dgn_ac_sent'] = $lspop['jpb5_luas_rng_lain'];
                }
            }
            $jpb = [$idx => $res];
        };

        log::info($jpb);
        return $jpb;
    }


    public static function prosesLspop($dataArray)
    {
        DB::beginTransaction();
        try {
            $np = $dataArray['dat_op_bangunan'][0];
            $kd_propinsi = $np['kd_propinsi'];
            $kd_dati2 = $np['kd_dati2'];
            $kd_kecamatan = $np['kd_kecamatan'];
            $kd_kelurahan = $np['kd_kelurahan'];
            $kd_blok = $np['kd_blok'];
            $no_urut = $np['no_urut'];
            $kd_jns_op = $np['kd_jns_op'];

            // lspop
            DB::connection('oracle_satutujuh')->table(db::raw('tbl_lspop'))->insert($dataArray['lspop']);
            // cek nilai individu lspop


            // hapus  op bangunan dulu
            DB::connection('oracle_satutujuh')->table(DB::raw('dat_op_bangunan'))
                ->where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->delete();


            // dat_op_bangunan
            DB::connection('oracle_satutujuh')->table(DB::raw('dat_op_bangunan'))->insert($dataArray['dat_op_bangunan']);
            // fasilitas

            // hapus  op bangunan dulu
            DB::connection('oracle_satutujuh')->table(DB::raw('dat_fasilitas_bangunan'))
                ->where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->delete();

            foreach ($dataArray['fasilitas'] as $dat_fasilitas) {
                DB::connection('oracle_satutujuh')->table(DB::raw('dat_fasilitas_bangunan'))->insert($dat_fasilitas);
            }


            // JPB

            foreach ($dataArray['jpb']  as $tablejpb => $rowdata) {
                DB::connection('oracle_satutujuh')->table(DB::raw($tablejpb))
                    ->where('kd_propinsi', $kd_propinsi)
                    ->where('kd_dati2', $kd_dati2)
                    ->where('kd_kecamatan', $kd_kecamatan)
                    ->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_blok', $kd_blok)
                    ->where('no_urut', $no_urut)
                    ->where('kd_jns_op', $kd_jns_op)
                    ->delete();

                DB::connection('oracle_satutujuh')->table(DB::raw($tablejpb))->insert($rowdata);
            }


            Datnilaiindividu::where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->delete();



            foreach ($dataArray['lspop'] as $garr) {
                $nilai_individu = $garr['nilai_individu'];
                // log::info('nilai individu :' . $nilai_individu);
                if ($nilai_individu > 0) {
                    $res = Datnilaiindividu::create([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'no_bng' => $garr['no_bng'],
                        'no_formulir_individu' => $garr['no_formulir'],
                        'nilai_individu' => $nilai_individu,
                        'tgl_penilaian_individu' => carbon::now(),
                        'nip_penilai_individu' => $garr['nip_perekam_bng'],
                        'tgl_pemeriksaan_individu' => carbon::now(),
                        'nip_pemeriksa_individu' => $garr['nip_perekam_bng'],
                        'tgl_rekam_nilai_individu' => carbon::now(),
                        'nip_perekam_individu' => $garr['nip_perekam_bng']
                    ]);
                    // log::info($res);
                }
            }
            //code...
            DB::commit();

            $res = [
                'status' => true,
                'desc' => 'Success'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            log::emergency($th);
            $res = [
                'status' => false,
                'desc' => $th->getMessage()
            ];
        }

        return $res;
    }

    public static function KodePengesahan()
    {
        $digits = 12;
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return '3504' . $pin;
    }

    public static function TagihanPenelitian($formulir)
    {
        $tagihan = DB::select(DB::raw("select distinct a.nm_wp_sppt,a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op,a.thn_pajak_sppt,c.denda,
        c.pokok,
        c.total,kobil,jln_wp_sppt,blok_kav_no_wp_sppt ,rw_wp_sppt,rt_wp_sppt,kelurahan_wp_sppt,kota_wp_sppt,a.luas_bumi_sppt,a.luas_bng_sppt
                                    from spo.sppt_oltp a
                                    join (select * from sppt_penelitian
                                    where nomor_formulir in ($formulir)) b on a.kd_propinsi=b.kd_propinsi
                                    and a.kd_dati2=b.kd_dati2 and a.kd_kecamatan=b.kd_kecamatan and a.kd_kelurahan=b.kd_kelurahan
                                    and a.kd_blok=b.kd_blok and a.no_urut=b.no_urut and a.kd_jns_op=b.kd_jns_op and a.thn_pajak_sppt=b.thn_pajak_sppt
                                    join billing_kolektif c on c.data_billing_id=b.data_billing_id
                                    and c.kd_propinsi=b.kd_propinsi
                                    and c.kd_dati2=b.kd_dati2 and c.kd_kecamatan=b.kd_kecamatan and c.kd_kelurahan=b.kd_kelurahan
                                    and c.kd_blok=b.kd_blok and c.no_urut=b.no_urut and c.kd_jns_op=b.kd_jns_op and c.tahun_pajak=b.thn_pajak_sppt
                                    order by a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.thn_pajak_sppt"));
        return $tagihan;
    }


    public static function getAlamatObjek($param)
    {
        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $kd_blok = $param['kd_blok'];
        $no_urut = $param['no_urut'];
        $kd_jns_op = $param['kd_jns_op'];
        $tahun = $param['tahun'];

        /* 
        $data = DB::connection('oracle_satutujuh')->table(DB::raw("dat_objek_pajak dop"))
            ->select(db::raw("jalan_op,
        blok_kav_no_op,
        rw_op,
        rt_op ,
        dob.kd_znt kd_znt_peta,
        nir*1000 nir_peta,
        nilai_per_m2_tanah*1000 njop_permeter"))
            ->leftjoin(db::raw("dat_op_bumi dob"), function ($join) {
                $join->on("DOP.KD_PROPINSI", "=", "DOB.KD_PROPINSI")
                    ->on("DOP.KD_DATI2", "=", "DOB.KD_DATI2")
                    ->on("DOP.KD_KECAMATAN", "=", "DOB.KD_KECAMATAN")
                    ->on("DOP.KD_KELURAHAN", "=", "DOB.KD_KELURAHAN")
                    ->on("DOP.KD_BLOK", "=", "DOB.KD_BLOK")
                    ->on("DOP.NO_URUT", "=", "DOB.NO_URUT")
                    ->on("DOP.KD_JNS_OP", "=", "DOB.KD_JNS_OP");
            })
            ->leftjoin(db::raw("dat_nir"), function ($join) {
                $join->on('dat_nir.kd_znt', '=', 'dob.kd_znt')
                    ->on('DOP.KD_PROPINSI', '=', 'dat_nir.KD_PROPINSI')
                    ->on('DOP.KD_DATI2', '=', 'dat_nir.KD_DATI2')
                    ->on('DOP.KD_KECAMATAN', '=', 'dat_nir.KD_KECAMATAN')
                    ->on('DOP.KD_KELURAHAN', '=', 'dat_nir.KD_KELURAHAN');
            })

            ->join('kelas_tanah', function ($join) {
                $join->on('kelas_tanah.thn_awal_kls_tanah', '<=', 'dat_nir.thn_nir_znt')
                    ->on('kelas_tanah.thn_akhir_kls_tanah', '>=', 'dat_nir.thn_nir_znt')
                    ->on('kelas_tanah.nilai_min_tanah', '<=', 'dat_nir.nir')
                    ->on('kelas_tanah.nilai_max_tanah', '>=', 'dat_nir.nir');
            })
            ->whereraw(db::raw("dop.kd_kecamatan = '$kd_kecamatan'
            AND dop.kd_kelurahan = '$kd_kelurahan'
            AND dop.kd_blok = '$kd_blok'
            AND dop.no_urut = '$no_urut'
            AND dop.kd_jns_op = '$kd_jns_op'
            and thn_nir_znt='$tahun' and lower(kd_kls_tanah) not like 'x%'"))->first(); */

        $data = DB::connection('oracle_dua')
            ->table('v_alamat_objek')
            ->whereRaw(
                "kd_kecamatan = ? 
                AND kd_kelurahan = ? 
                AND kd_blok = ? 
                AND no_urut = ? 
                AND kd_jns_op = ? 
                AND thn_nir_znt = ?",
                [$kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun]
            )
            ->first();

        if (empty($data)) {
            $data = DB::connection('oracle_dua')
                ->table('v_alamat_objek')
                ->whereRaw(
                    "kd_kecamatan = ? 
                    AND kd_kelurahan = ? 
                    AND kd_blok = ? 
                    AND no_urut = ? 
                    AND kd_jns_op = ?",
                    [$kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op]
                )
                ->orderBy("thn_nir_znt", "DESC")
                ->first();
        }
        // dd($data);
        return $data;
    }

    public static function loadPenelitian($penelitian)
    {
        $letak_op = [];
        $id = $penelitian->id;
        $nop = $penelitian->kd_propinsi . $penelitian->kd_dati2 . $penelitian->kd_kecamatan . $penelitian->kd_kelurahan . $penelitian->kd_blok . $penelitian->no_urut . $penelitian->kd_jns_op;
        $nopinduk = $penelitian->kd_propinsi . $penelitian->kd_dati2 . $penelitian->kd_kecamatan . $penelitian->kd_kelurahan . $penelitian->kd_blok . $penelitian->no_urut . $penelitian->kd_jns_op;

        $jns_pengurangan = [];
        if (optional($penelitian->layanan)->jenis_layanan_id == 5) {
            $jns_pengurangan = JenisPengurangan::where('jenis', 1)->get();
        }
        // dd($nop);
        if (optional($penelitian->layanan)->jenis_layanan_id <> '1') {
            if ($penelitian->nop_gabung <> '') {
                $opinduk = DB::table('layanan_objek')->select(DB::raw("id,kd_propinsi,
                kd_dati2,
                kd_kecamatan,
                kd_kelurahan,
                kd_blok,
                no_urut,
                kd_jns_op"))->where('id', $penelitian->nop_gabung)->first();

                $kd_propinsi = $opinduk->kd_propinsi;
                $kd_dati2 = $opinduk->kd_dati2;
                $kd_kecamatan = $opinduk->kd_kecamatan;
                $kd_kelurahan = $opinduk->kd_kelurahan;
                $kd_blok = $opinduk->kd_blok;
                $no_urut = $opinduk->no_urut;
                $kd_jns_op = $opinduk->kd_jns_op;

                $nop = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok;
                $nopinduk = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
            } else {

                $kd_propinsi = $penelitian->kd_propinsi;
                $kd_dati2 = $penelitian->kd_dati2;
                $kd_kecamatan = $penelitian->kd_kecamatan;
                $kd_kelurahan = $penelitian->kd_kelurahan;
                $kd_blok = $penelitian->kd_blok;
                $no_urut = $penelitian->no_urut;
                $kd_jns_op = $penelitian->kd_jns_op;
            }

            $th = date('Y');
            $ap = [
                'tahun' => $th,
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
            ];
            $letak_op = self::getAlamatObjek($ap);
        }

        // mutasi gabung
        $nop_pembatalan = [];
        if (optional($penelitian->layanan)->jenis_layanan_id == 7) {
            // nop pembatalan karena penggabungan
            $nop_pembatalan = Layanan_objek::whereraw("nomor_layanan ='" . $penelitian->nomor_layanan . "'  and nop_gabung='$id' ")->get()->toarray();


            // jika mutasi gabung
            if (optional($penelitian->layanan)->jenis_layanan_id == 7) {
                if (isset($nop_pembatalan[0])) {
                    $sd = $nop_pembatalan[0] ?? '';
                    $nop = ($nop_pembatalan[0]['kd_propinsi'] ?? '') .
                        ($nop_pembatalan[0]['kd_dati2'] ?? '') .
                        ($nop_pembatalan[0]['kd_kecamatan'] ?? '') .
                        ($nop_pembatalan[0]['kd_kelurahan'] ?? '') .
                        ($nop_pembatalan[0]['kd_blok'] ?? '');
                }
            }
        }

        $nop_pecah = [];
        $statuspecahan = [];
        if (optional($penelitian->layanan)->jenis_layanan_id <> 7) {
            $ida = $id;
            $idi = $opinduk->id ?? $id;


            $nop_pecah = DB::table('layanan_objek')
                ->leftjoin('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                ->leftjoin('kelompok_objek', 'kelompok_objek.id', '=', 'layanan_objek.kelompok_objek_id')
                ->leftjoin('lokasi_objek', 'lokasi_objek.id', '=', 'layanan_objek.lokasi_objek_id')
                ->leftjoin(db::raw("(select distinct nomor_formulir,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op
                    from sppt_Penelitian) hsl"), 'hsl.nomor_formulir', '=', 'layanan_objek.nomor_formulir')
                ->select(db::raw('layanan_objek.nomor_layanan,nama_wp,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_propinsi else hsl.kd_propinsi end kd_propinsi,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_dati2 else hsl.kd_dati2 end kd_dati2,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_kecamatan else hsl.kd_kecamatan end kd_kecamatan,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_kelurahan else hsl.kd_kelurahan end kd_kelurahan,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_blok else hsl.kd_blok end kd_blok,
                    case when  hsl.nomor_formulir is null then layanan_objek.no_urut else hsl.no_urut end no_urut,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_jns_op else hsl.kd_jns_op end kd_jns_op,
                    nama_kelompok,upper(nama_lokasi) nama_lokasi, CASE
                                        WHEN nop_gabung IS NULL 
                                        THEN  case when jenis_layanan_id=6 then sisa_pecah_total_gabung else luas_bumi end
                                        ELSE luas_bumi
                                    END luas_bumi,luas_bng,case when nop_gabung is null  then  luas_bumi else (select luas_bumi from layanan_objek tmp where id=layanan_objek.nop_gabung)  end luas_induk,nop_gabung'))->orderbyraw("nop_gabung asc nulls first");
            // mutasi pecah 


            if (optional($penelitian->layanan)->jenis_layanan_id == 6) {
                $nop_pecah = $nop_pecah->whereraw("layanan_objek.id='$ida'  or (nop_gabung='$idi' or layanan_objek.id='$idi')")->get();
                if ($penelitian->no_gabung == '') {
                    $statuspecahan = DB::select(DB::raw("select sum(jumlah) jumlah,sum(teliti) sudah
                                from (
                                select count(1)  jumlah,case when pemutakhiran_at is not null then 1  else 0 end teliti
                                from layanan_objek
                                where nop_gabung='$ida'
                                group by pemutakhiran_at
                                )"));
                }
            } else {
                $nop_pecah = $nop_pecah->whereraw("layanan_objek.id='$ida'")->get();
            }
        }



        $kd_propinsi = substr($nopinduk, 0, 2);
        $kd_dati2 = substr($nopinduk, 2, 2);
        $kd_kecamatan = substr($nopinduk, 4, 3);
        $kd_kelurahan = substr($nopinduk, 7, 3);
        $kd_blok = substr($nopinduk, 10, 3);
        $no_urut = substr($nopinduk, 13, 4);
        $kd_jns_op = substr($nopinduk, 17, 1);

        $riwayatpembayaran = [];
        // kecuali mutasi gabung
        if (optional($penelitian->layanan)->jenis_layanan_id != '7') {
            $start = 2003;
            // jika pendaftaran baru
            if (optional($penelitian->layanan)->jenis_layanan_id == '1') {
                $cm = DB::connection("oracle_satutujuh")->table("dat_peta_blok")
                    ->selectraw("count(1) cd")
                    ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")
                    ->first();
                if ($cm->cd > 2) {
                    // sismiop
                    $start = date('Y') - 4;
                } else {
                    $start = date('Y');
                }
            }

            // cek nop pelimpahan
            $np = PembayaranMutasiData::whereraw("KD_PROPINSI = '$kd_propinsi'
            AND KD_DATI2 = '$kd_dati2' AND KD_KECAMATAN = '$kd_kecamatan' AND KD_KELURAHAN = '$kd_kelurahan'
            AND KD_BLOK = '$kd_blok' AND NO_URUT = '$no_urut' AND KD_JNS_OP = '$kd_jns_op'")->selectraw("distinct kd_propinsi_asal kd_propinsi,
                        kd_dati2_asal kd_dati2,
                        kd_kecamatan_asal kd_kecamatan,
                        kd_kelurahan_asal kd_kelurahan,
                        kd_blok_asal kd_blok,
                        no_urut_asal no_urut,
                        kd_jns_op_asal kd_jns_op")->get();
            // dd($np);

            $awh = [[
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
            ]];

            $vwa = "(a.KD_PROPINSI = '$kd_propinsi'
                    AND a.KD_DATI2 = '$kd_dati2'
                    AND a.KD_KECAMATAN = '$kd_kecamatan'
                    AND a.KD_KELURAHAN = '$kd_kelurahan'
                    AND a.KD_BLOK = '$kd_blok'
                    AND a.NO_URUT = '$no_urut'
                    AND a.KD_JNS_OP = '$kd_jns_op') or";
            $vwb = "(KD_PROPINSI = '$kd_propinsi'
                    AND KD_DATI2 = '$kd_dati2'
                    AND KD_KECAMATAN = '$kd_kecamatan'
                    AND KD_KELURAHAN = '$kd_kelurahan'
                    AND KD_BLOK = '$kd_blok'
                    AND NO_URUT = '$no_urut'
                    AND KD_JNS_OP = '$kd_jns_op') or";

            foreach ($np as $np) {
                $vwa .= " (a.KD_PROPINSI = '" . $np->kd_propinsi . "'
                    AND a.KD_DATI2 = '" . $np->kd_dati2 . "'
                    AND a.KD_KECAMATAN = '" . $np->kd_kecamatan . "'
                    AND a.KD_KELURAHAN = '" . $np->kd_kelurahan . "'
                    AND a.KD_BLOK = '" . $np->kd_blok . "'
                    AND a.NO_URUT = '" . $np->no_urut . "'
                    AND a.KD_JNS_OP = '" . $np->kd_jns_op . "') or";

                $vwb .= " (KD_PROPINSI = '" . $np->kd_propinsi . "'
                    AND KD_DATI2 = '" . $np->kd_dati2 . "'
                    AND KD_KECAMATAN = '" . $np->kd_kecamatan . "'
                    AND KD_KELURAHAN = '" . $np->kd_kelurahan . "'
                    AND KD_BLOK = '" . $np->kd_blok . "'
                    AND NO_URUT = '" . $np->no_urut . "'
                    AND KD_JNS_OP = '" . $np->kd_jns_op . "') or";


                $awh[] =
                    [
                        'kd_propinsi' => $np->kd_propinsi,
                        'kd_dati2' => $np->kd_dati2,
                        'kd_kecamatan' => $np->kd_kecamatan,
                        'kd_kelurahan' => $np->kd_kelurahan,
                        'kd_blok' => $np->kd_blok,
                        'no_urut' => $np->no_urut,
                        'kd_jns_op' => $np->kd_jns_op,
                    ];
            }

            $vwa = substr($vwa, 0, -2);
            $vwb = substr($vwb, 0, -2);

            // cek tahun minimal
            $minth = DB::connection("oracle_satutujuh")->table("sppt");
            foreach ($awh as $rwh) {
                $minth =    $minth->orwhere(function ($sql) use ($rwh) {
                    $sql->whereraw("kd_propinsi ='" . $rwh['kd_propinsi'] . "' and 
                    kd_dati2 ='" . $rwh['kd_dati2'] . "' and
                    kd_kecamatan ='" . $rwh['kd_kecamatan'] . "' and
                    kd_kelurahan ='" . $rwh['kd_kelurahan'] . "' and
                    kd_blok ='" . $rwh['kd_blok'] . "' and
                    no_urut ='" . $rwh['no_urut'] . "' and
                    kd_jns_op ='" . $rwh['kd_jns_op'] . "'");
                });
            }
            $minth = $minth->select(db::raw("min(cast(thn_pajak_sppt as number) ) tahun"))->first();
            // dd($minth);

            if ($minth->tahun == null) {
                $query1 = DB::table('tbl_spop')
                    ->join('users', 'users.id', '=', 'tbl_spop.created_by')
                    ->join('layanan_objek', 'layanan_objek.nomor_formulir', '=', 'tbl_spop.no_formulir')
                    ->join('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                    ->join('jenis_layanan', 'jenis_layanan.id', '=', 'layanan.jenis_layanan_id')
                    ->where('nop_proses', $nop)
                    ->selectRaw("CASE WHEN layanan.jenis_layanan_id = 7 THEN NVL(nop_gabung, TO_CHAR(layanan_objek.id)) ELSE TO_CHAR(layanan_objek.id) END AS id")
                    ->addSelect([
                        'nama_layanan',
                        DB::raw('TRUNC(tbl_spop.created_at) AS created_at'),
                        'users.nama AS created_by',
                        DB::raw("'PERMOHONAN' AS jenis")
                    ]);

                $query2 = DB::table('tbl_spop')
                    ->join('pendataan_objek', 'pendataan_objek.id', '=', 'tbl_spop.pendataan_objek_id')
                    ->join('pendataan_batch', 'pendataan_batch.id', '=', 'pendataan_objek.pendataan_batch_id')
                    ->join('jenis_pendataan', 'jenis_pendataan.id', '=', 'pendataan_batch.jenis_pendataan_id')
                    ->join('users', 'users.id', '=', 'tbl_spop.created_by')
                    ->where('nop_proses', $nop)
                    ->selectRaw("TO_CHAR(pendataan_objek.id) AS id")
                    ->addSelect([
                        'jenis_pendataan.nama_pendataan AS nama_layanan',
                        DB::raw('TRUNC(tbl_spop.created_at) AS created_at'),
                        'users.nama AS created_by',
                        DB::raw("'PENDATAAN' AS jenis")
                    ]);

                $query3 = DB::table('perubahan_znt')
                    ->join('perubahan_znt_objek', 'perubahan_znt.id', '=', 'perubahan_znt_objek.perubahan_znt_id')
                    ->join('users', 'users.id', '=', 'perubahan_znt.created_by')
                    ->where('perubahan_znt.verifikasi_kode', '1')
                    ->where([
                        ['perubahan_znt_objek.kd_propinsi', '=', $kd_propinsi],
                        ['perubahan_znt_objek.kd_dati2', '=', $kd_dati2],
                        ['perubahan_znt_objek.kd_kecamatan', '=', $kd_kecamatan],
                        ['perubahan_znt_objek.kd_kelurahan', '=', $kd_kelurahan],
                        ['perubahan_znt_objek.kd_blok', '=', $kd_blok],
                        ['perubahan_znt_objek.no_urut', '=', $no_urut],
                        ['perubahan_znt_objek.kd_jns_op', '=', $kd_jns_op],
                    ])
                    ->selectRaw("TO_CHAR(perubahan_znt.id) AS id")
                    ->addSelect([
                        DB::raw("'Perubahan ZNT' AS nama_layanan"),
                        'perubahan_znt.created_at',
                        'users.nama AS created_by',
                        DB::raw("'Dari ' || kd_znt_lama || ' ke ' || kd_znt_baru AS jenis")
                    ]);

                // Menggabungkan query dengan UNION
                $results = $query1->union($query2)->union($query3)->get();
                // dd($results);
                if (count($results) > 0) {

                    $minth = (object) ['tahun' => date('Y')];
                }
            }

            if ($minth->tahun >= $start) {
                $start = $minth->tahun;
            }

            $riwayatpembayaran = db::table(db::raw("(
            SELECT $start + LEVEL - 1  tahun
            FROM dual
         CONNECT BY $start + LEVEL - 1 <= to_char(sysdate,'yyyy')
         ) ref_tahun"))
                ->select(db::raw("'$kd_propinsi' kd_propinsi,
         '$kd_dati2' kd_dati2,
         '$kd_kecamatan' kd_kecamatan,
         '$kd_kelurahan' kd_kelurahan,
         '$kd_blok' kd_blok,
         '$no_urut' no_urut,
         '$kd_jns_op' kd_jns_op,tahun thn_pajak_sppt ,( select case when pbb >pokok_bayar then null else TO_CHAR (tanggal, 'yyyy-mm-dd') end  tanggal 
         from (
         select a.pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0) pbb,sum(jml_sppt_yg_dibayar-denda_sppt) pokok_bayar,max(tgl_pembayaran_sppt) tanggal
         from  pbb.sppt a
         left join pbb.sppt_potongan c on a.kd_propinsi=c.kd_propinsi and
         a.kd_dati2=c.kd_dati2 and
         a.kd_kecamatan=c.kd_kecamatan and
         a.kd_kelurahan=c.kd_kelurahan and
         a.kd_blok=c.kd_blok and
         a.no_urut=c.no_urut and
         a.kd_jns_op=c.kd_jns_op and
         a.thn_pajak_sppt=c.thn_pajak_sppt
         left join pbb.pembayaran_sppt b on a.kd_propinsi=b.kd_propinsi and
         a.kd_dati2=b.kd_dati2 and
         a.kd_kecamatan=b.kd_kecamatan and
         a.kd_kelurahan=b.kd_kelurahan and
         a.kd_blok=b.kd_blok and
         a.no_urut=b.no_urut and
         a.kd_jns_op=b.kd_jns_op and
         a.thn_pajak_sppt=b.thn_pajak_sppt              
         WHERE (" . $vwa . ")
                        AND a.THN_PAJAK_SPPT =ref_tahun.tahun
                        group by a.pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0)
                        ) ) tgl_pembayaran_sppt,
         (SELECT no_transaksi
          FROM pbb.pengurang_piutang
         WHERE  (" . $vwb . ")
               AND THN_PAJAK_SPPT = ref_tahun.tahun
               AND ROWNUM = 1)
          koreksi
         "))->get();
        } else {

            $dk = DB::connection("oracle_satutujuh")->table(db::raw(" sppt a"))
                ->join(db::raw("( select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op
                                    from sim_pbb.layanan_objek
                                    where nop_gabung='" . $penelitian->id . "' ) b"), function ($join) {
                    $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                        ->on('a.kd_dati2', '=', 'b.kd_dati2')
                        ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                        ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                        ->on('a.kd_blok', '=', 'b.kd_blok')
                        ->on('a.no_urut', '=', 'b.no_urut')
                        ->on('a.kd_jns_op', '=', 'b.kd_jns_op');
                })
                ->leftjoin(db::raw("sppt_potongan d"), function ($join) {
                    $join->on('a.kd_propinsi', '=', 'd.kd_propinsi')
                        ->on('a.kd_dati2', '=', 'd.kd_dati2')
                        ->on('a.kd_kecamatan', '=', 'd.kd_kecamatan')
                        ->on('a.kd_kelurahan', '=', 'd.kd_kelurahan')
                        ->on('a.kd_blok', '=', 'd.kd_blok')
                        ->on('a.no_urut', '=', 'd.no_urut')
                        ->on('a.kd_jns_op', '=', 'd.kd_jns_op')
                        ->on('a.thn_pajak_sppt', '=', 'd.thn_pajak_sppt');
                })
                ->leftjoin(DB::raw("pembayaran_sppt c"), function ($join) {
                    $join->on('a.kd_propinsi', '=', 'c.kd_propinsi')
                        ->on('a.kd_dati2', '=', 'c.kd_dati2')
                        ->on('a.kd_kecamatan', '=', 'c.kd_kecamatan')
                        ->on('a.kd_kelurahan', '=', 'c.kd_kelurahan')
                        ->on('a.kd_blok', '=', 'c.kd_blok')
                        ->on('a.no_urut', '=', 'c.no_urut')
                        ->on('a.kd_jns_op', '=', 'c.kd_jns_op')
                        ->on('a.thn_pajak_sppt', '=', 'c.thn_pajak_sppt');
                })
                ->whereraw("a.thn_pajak_sppt='" . date('Y') . "'")
                ->selectraw("SUM (pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0))
                - SUM (NVL (jml_sppt_yg_dibayar, 0) - NVL (denda_sppt, 0)) cc")->first();



            if ($dk->cc != 0) {
                $riwayatpembayaran = collect([
                    (object)[
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => date('Y'),
                        'tgl_pembayaran_sppt' => null,
                        'koreksi' => null
                    ]
                ]);
            }
        }



        $refAjuan = Cache::remember(md5('layanankantor'), 3600, function () {
            return  DB::table(db::raw("ref_formulir"))
                ->select(db::raw('ref_formulir.id id_formulir,jenis_layanan_id,nama_layanan'))
                ->join(db::raw('ref_formulir_layanan'), 'ref_formulir_layanan.ref_formulir_id', '=', 'ref_formulir.id')
                ->join('jenis_layanan', 'jenis_layanan.id', '=', 'ref_formulir_layanan.jenis_layanan_id')
                ->where('penelitian', '1')
                ->orderBy('jenis_layanan_id')->get();
        });

        if (optional($penelitian->layanan)->jenis_layanan_id == '1') {
            $nop = substr($nop, 0, 10);
        }
        $berkas = Layanan_dokumen::where('nomor_layanan', $penelitian->nomor_layanan)->whereraw("keyid='$id'")->select(db::raw('nama_dokumen,keterangan,id,keyid,nomor_layanan,url'))->orderby('id', 'asc')->get()->toarray();
        switch (optional($penelitian->layanan)->jenis_layanan_id) {
            case '6':
                # code...
                $luas_bumi = $penelitian->nop_gabung <> '' ? $penelitian->luas_bumi : $penelitian->sisa_pecah_total_gabung;
                break;
            case '7':
                $luas_bumi = $penelitian->nop_gabung <> '' ? $penelitian->luas_bumi : $penelitian->sisa_pecah_total_gabung;

                break;
            default:
                # code...
                $luas_bumi = $penelitian->luas_bumi;
                break;
        }
        $jenisDokumen = Dokumen::list();

        return  [
            'jenis_pelayanan' => optional($penelitian->layanan)->jenis_layanan_id,
            'jenis_pelayanan_nama' => optional($penelitian->layanan)->jenis_layanan_nama,
            'jts' => optional($penelitian->layanan)->jenis_layanan_id == 2 ? 3 : 2,
            'luas_bumi' =>  $luas_bumi,
            'nomor_layanan' => $penelitian->nomor_layanan,
            'id' => $penelitian->id,
            'rt_op' => $penelitian->rt_op,
            'rw_op' => $penelitian->rw_op,
            'nik_wp' => $penelitian->nik_wp <> '' ? $penelitian->nik_wp : optional($penelitian->layanan)->nik,
            'nama_wp' => $penelitian->nama_wp <> '' ? $penelitian->nama_wp : optional($penelitian->layanan)->nama,
            'alamat_wp' => $penelitian->alamat_wp <> '' ? $penelitian->alamat_wp : optional($penelitian->layanan)->alamat,
            'rt_wp' => $penelitian->rt_wp,
            'rw_wp' => $penelitian->rw_wp,
            'kelurahan_wp' => $penelitian->kelurahan_wp <> '' ? $penelitian->kelurahan_wp : optional($penelitian->layanan)->kelurahan,
            'kecamatan_wp' => $penelitian->kecamatan_wp <> '' ? $penelitian->kecamatan_wp : optional($penelitian->layanan)->kecamatan,
            'dati2_wp' => $penelitian->dati2_wp <> '' ? $penelitian->dati2_wp : optional($penelitian->layanan)->dati2,
            'telp_wp' => $penelitian->telp_wp <> '' ? $penelitian->telp_wp : optional($penelitian->layanan)->nomor_telepon,
            'luas_bng' => $penelitian->luas_bng,
            'kelompok_objek_id' => $penelitian->kelompok_objek_id,
            'jenis_objek' => optional($penelitian->layanan)->jenis_objek,
            'propinsi_wp' => $penelitian->propinsi_wp,
            'nop_gabung' => $penelitian->nop_gabung,
            'berkas' => $berkas,
            'letak_op' => $letak_op,
            'nop_pecah' => $nop_pecah,
            'nop_pembatalan' => $nop_pembatalan,
            'refAjuan' => $refAjuan,
            'riwayatpembayaran' => $riwayatpembayaran,
            'nopinduk' => $nopinduk,
            'statuspecahan' => $statuspecahan,
            'jenisDokumen' => $jenisDokumen,
            'nop' => $nop,
            'jns_pengurangan_id' => $penelitian->jenis_pengurangan,
            'jns_pengurangan' => $jns_pengurangan,
            'is_online' => $penelitian->layanan->is_online
        ];
    }

    public static function getLastNop($kd_kecamatan, $kd_kelurahan, $kd_blok)
    {
        DB::statement(db::raw("delete from booking_nop where created_at< SYSDATE-1"));


        if ($kd_blok == '000') {
            // cek nop berlubang
            $carinop = '3507' . $kd_kecamatan . $kd_kelurahan . $kd_blok;
            $rawtable = "(SELECT min_a - 1 + LEVEL hasil
        FROM (SELECT NVL (MAX (TO_NUMBER (no_urut)), 1) max_a, 1 min_a
                FROM (SELECT no_urut
                        FROM pbb.dat_objek_pajak
                       WHERE     kd_kecamatan = '$kd_kecamatan'
                             AND kd_kelurahan = '$kd_kelurahan'
                             AND kd_blok = '$kd_blok'
                      UNION
                      SELECT SUBSTR (TRIM (nop_proses), 14, 4)
                        FROM booking_nop
                       WHERE TRIM (nop_proses) LIKE '" . $carinop . "%'))
                        CONNECT BY LEVEL <= max_a - min_a + 1
                        MINUS
                        SELECT TO_NUMBER (no_urut)
                            FROM (SELECT no_urut
                                    FROM pbb.dat_objek_pajak
                                WHERE     kd_kecamatan = '$kd_kecamatan'
                                        AND kd_kelurahan = '$kd_kelurahan'
                                        AND kd_blok = '$kd_blok'
                                UNION
                                SELECT SUBSTR (TRIM (nop_proses), 14, 4)
                                    FROM booking_nop
                                WHERE TRIM (nop_proses) LIKE '" . $carinop . "%'))";
            $cek_a = DB::table(db::raw($rawtable))->orderby('hasil', 'asc')->first();
            if (!empty($cek_a)) {
                $no_urut = sprintf("%04s", $cek_a->hasil);
            } else {
                $rb = "(select max(no_urut)+1 hasil from (
                select to_number(no_urut) no_urut
                from pbb.dat_objek_pajak 
                where kd_kecamatan='$kd_kecamatan'
                and kd_kelurahan='$kd_kelurahan'
                and kd_blok='$kd_blok' 
                union 
                select to_number(substr(trim(nop_proses),14,4)) no_urut
                from booking_nop
                where trim(nop_proses) like '" . $carinop . "%'
                ))";
                $cek_b = DB::table(db::raw($rb))->first();
                $no_urut = sprintf("%04s", $cek_b->hasil);
            }
            return $no_urut;
        } else {
            $checking = DB::connection('oracle_satutujuh')->table('dat_objek_pajak')
                ->whereraw(DB::raw("kd_propinsi='35' and kd_dati2='07' 
            and kd_kecamatan='$kd_kecamatan' 
            and kd_kelurahan='$kd_kelurahan' 
            and kd_blok='$kd_blok'"))
                ->select(DB::raw("nvl(max(to_number(no_urut)),0) last_number"))
                ->first();

            // select max(substr(nop_proses,14,4)) from tbl_spop_tmp
            $carinop = '3507' . $kd_kecamatan . $kd_kelurahan . $kd_blok;
            $cb = DB::table('booking_nop')->select(db::raw("max(substr(trim(nop_proses),14,4)) last_number"))->whereraw("trim(nop_proses) like '$carinop%'")->get();
            $a = $checking->last_number;
            $b = 0;
            if (!empty($cb)) {
                $b = $cb[0]->last_number;
            }
            $nomor = $a;
            if ($a <= $b) {
                $nomor = $b;
            }

            return $nomor + 1;
        }
    }

    public static function generateNOP($kd_kecamatan, $kd_kelurahan, $kd_blok, $userid = null)
    {
        if (user()) {
            $id_user = user()->id;
        } else {
            $id_user = $userid;
        }

        if ($kd_blok == '000') {
            $df = DB::connection("oracle_satutujuh")->table("dat_objek_pajak")
                ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'  and kd_blok='$kd_blok'")
                ->selectraw("max(cast(no_urut as number)) no_urut")->first();
            if ($df->no_urut == '9999') {
                $kd_blok = '001';
            }
        }

        // cek sismiop or sistep
        $cs = DB::connection("oracle_satutujuh")->table("ref_kelurahan")->select('thn_sismiop')
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")
            ->first();

        if ($cs->thn_sismiop == '') {
            $kd_jns_op = '7';
        } else {
            $kd_jns_op = '0';
        }

        // $kd_jns_op = ($kd_blok == '000') ? '7' : '0';

        $no_urut = padding(self::getLastNop($kd_kecamatan, $kd_kelurahan, $kd_blok), 0, 4);

        $nop_proses = '3507' . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;

        $book = [
            'nop_proses' => $nop_proses,
            'created_by' => $id_user,
            'created_at' => Carbon::now()
        ];
        db::table('booking_nop')->insert($book);
        return $nop_proses;
    }

    public static function prosesPotongan($param)
    {
        $request = new \Illuminate\Http\Request();
        DB::beginTransaction();

        // dd($param);

        $msg = "";
        try {
            $nop_proses = preg_replace('/[^0-9]/', '', $param['nop_proses']);
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);
            // $insentif = $param['insentif'] ?? '0';
            $insentif = 1;
            $dataspop = (array) DB::connection("oracle_dua")->select(DB::raw("SELECT  '" . $param['jenis_layanan'] . "' jenis_layanan,'" . $param['layanan_objek_id'] . "' lhp_id,2 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP, STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI FROM DAT_OBJEK_PAJAK A JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op WHERE a.kd_kecamatan='$kd_kecamatan' AND a.kd_kelurahan='$kd_kelurahan' AND a.kd_blok='$kd_blok' AND a.no_urut='$no_urut' AND a.kd_jns_op='$kd_jns_op' "))[0];
            $request->merge($dataspop);
            // dd($dataspop);
            $spop = Pajak::pendataanSpop($request);
            Pajak::pendataanObjekPajak($request, $spop);
            $wherenop = [['kd_propinsi', '=', $kd_propinsi], ['kd_dati2', '=', $kd_dati2], ['kd_kecamatan', '=', $kd_kecamatan], ['kd_kelurahan', '=', $kd_kelurahan], ['kd_blok', '=', $kd_blok], ['no_urut', '=', $no_urut], ['kd_jns_op', '=', $kd_jns_op]];
            $ceklunas = DB::connection('oracle_dua')->table(DB::raw('sppt'))
                ->selectraw("kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,status_pembayaran_sppt,pbb_yg_harus_dibayar_sppt")
                ->where($wherenop)
                ->where('thn_pajak_sppt', date('Y'))->first();

            $desta = "";
            if ($ceklunas) {
                if ($ceklunas->status_pembayaran_sppt == 0) {
                    // proses perhitungan pengurangan
                    $tipe_pengurangan = $param['tipe_pengurangan'] ?? 2;
                    $tahun_sppt = date('Y');

                    // cek insentif
                    if ($insentif == '0') {
                        DB::connection('oracle_satutujuh')
                            ->table(DB::raw("sppt_potongan_detail"))
                            ->whereRaw("thn_pajak_sppt='" . $tahun_sppt . "'
                            and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' 
                            and kd_blok='" . $kd_blok . "' 
                            and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'  and jns_potongan='1'")
                            ->delete();
                    }


                    $jp = JenisPengurangan::where('id', $param['jns_pengurangan'])->first();
                    $deskripsi_pengurangan =  $jp->nama_pengurangan;

                    $jns_pot = $jp->jenis;
                    $nilai_pengurangan = $param['nilai_pengurangan'] ?? $jp->pengurangan;

                    // $jns_pot == 1
                    if ($tipe_pengurangan == 1) {
                        // angka
                        $sebesar = angka($nilai_pengurangan);
                        $despen = 'Permohonan pengurangan ' . $deskripsi_pengurangan . ' sebesar Rp.' . $sebesar . '; ';
                        if ($param['keterangan']) {
                            $despen .= $param['keterangan'];
                        }

                        DB::connection("oracle_satutujuh")->statement(DB::raw("
                                    begin
                                    delete from sppt_potongan_detail where  thn_pajak_sppt='" . $tahun_sppt . "'
                                    and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' 
                                    and kd_blok='" . $kd_blok . "' 
                                    and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'  and jns_potongan='2';
                                 insert into sppt_potongan_detail 
                                 select a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op,'" . $tahun_sppt . "' thn_pajak_sppt,2 jns_potongan,  " . $param['layanan_objek_id'] . " loi  ,null , '" . $nilai_pengurangan . "' nilai_potongan,'" . $despen . "' keterangan,sysdate creatde_at ," . self::userid() . " created_by from (select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,pbb_yg_harus_dibayar_sppt pbb from sppt where thn_pajak_sppt='" . $tahun_sppt . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "' ) a;
                                 commit;
                                end;
                                "));
                    } else {
                        // potongan persentase
                        $pot = $nilai_pengurangan / 100;
                        $sebesar = $nilai_pengurangan . "%";
                        $despen = 'Permohonan pengurangan ' . $deskripsi_pengurangan . ' sebesar ' . $sebesar . '; ';
                        if ($param['keterangan']) {
                            $despen .= $param['keterangan'];
                        }
                        DB::connection("oracle_satutujuh")->statement(DB::raw("
                                    begin
                                    delete from sppt_potongan_detail where  thn_pajak_sppt='" . $tahun_sppt . "'
                                    and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' 
                                    and kd_blok='" . $kd_blok . "' 
                                    and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "'   and jns_potongan='2';
                                
                                    insert into sppt_potongan_detail
                                    select a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op,'" . $tahun_sppt . "' thn_pajak_sppt,2 jns_potongan,  " . $param['layanan_objek_id'] . " loi  ," . $nilai_pengurangan . " , round((a.pbb - a.potongan) * " . $pot . ")   nilai_potongan,'" . $despen . "' keterangan,sysdate creatde_at ," . self::userid() . " created_by from (select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,pbb_yg_harus_dibayar_sppt pbb,nvl((select nilai_potongan
                                            from sppt_potongan_detail
                                            where jns_potongan='1'
                                            and thn_pajak_sppt=sppt.thn_pajak_sppt  
                                            and kd_kecamatan=sppt.kd_kecamatan 
                                            and kd_kelurahan=sppt.kd_kelurahan 
                                            and kd_blok=sppt.kd_blok 
                                            and no_urut=sppt.no_urut 
                                            and kd_jns_op=sppt.kd_jns_op ),0) potongan from sppt where thn_pajak_sppt='" . $tahun_sppt . "' and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "' ) a;
                                commit;
                                end;
                                "));
                    }

                    if ($insentif == '0') {
                        DB::connection("oracle_satutujuh")->statement(DB::raw("begin 
                        pengurangan_tanpa_insentif('$kd_kecamatan','$kd_kelurahan','$kd_blok','$no_urut','$kd_jns_op','$tahun_sppt');
                        commit;
                        end;"));
                        // 
                    } else {
                        DB::connection("oracle_satutujuh")->statement(DB::raw("begin 
                        hitung_potongan('$kd_kecamatan','$kd_kelurahan','$kd_blok','$no_urut','$kd_jns_op','$tahun_sppt');
                        commit;
                        end;"));
                    }
                } else {
                    $desta .= " Objek telah melakukan pembayaran untuk tahun " . date('Y');
                }
            } else {
                $desta .= " Objek tidak ditemukan";
            }


            $penelitian = Layanan_objek::with('layanan')->findorfail($param['layanan_objek_id']);
            $penelitian->update([
                'nomor_formulir' => $spop['no_formulir'],
                'pemutakhiran_at' => DB::raw('sysdate'),
                'pemutakhiran_by' => self::userid(),
            ]);

            DB::commit();
            $msg = "Proses pengurangan pajak telah di proses. " . $desta;
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            log::emergency($th);
            $msg = "Proses pengurangan pajak gagal di proses";
        }

        return  $msg;
    }
    public static function ListNop()
    {
        return $objek = DB::connection('oracle_satutujuh')->table('DAT_OBJEK_PAJAK')
            ->join('dat_op_bumi', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
            })->select(db::raw("dat_objek_pajak.kd_propinsi,
    dat_objek_pajak.kd_dati2,
    dat_objek_pajak.kd_kecamatan,
    dat_objek_pajak.kd_kelurahan,
    dat_objek_pajak.kd_blok,
    dat_objek_pajak.no_urut,
    dat_objek_pajak.kd_jns_op"));
    }

    public static function RawSqlPenetapan($tahun, $terbit, $jatuhtempo, $nip)
    {
        return "select * from (select fix.* from (SELECT final.KD_PROPINSI,
        final.KD_DATI2,
        final.KD_KECAMATAN,
        final.KD_KELURAHAN,
        final.KD_BLOK,
        final.NO_URUT,
        final.KD_JNS_OP,
        final.THN_PAJAK_SPPT,
        final.SIKLUS_SPPT,
        final.KD_KANWIL,
        final.KD_KANTOR,
        final.KD_TP,
        final.NM_WP_SPPT,
        final.JLN_WP_SPPT,
        final.BLOK_KAV_NO_WP_SPPT,
        final.RW_WP_SPPT,
        final.RT_WP_SPPT,
        final.KELURAHAN_WP_SPPT,
        final.KOTA_WP_SPPT,
        final.KD_POS_WP_SPPT,
        final.NPWP_SPPT,
        final.NO_PERSIL_SPPT,
        final.KD_KLS_TANAH,
        final.THN_AWAL_KLS_TANAH,
        final.KD_KLS_BNG,
        final.THN_AWAL_KLS_BNG,
        final.TGL_JATUH_TEMPO_SPPT,
        final.LUAS_BUMI_SPPT,
        final.LUAS_BNG_SPPT,
        final.NJOP_BUMI_SPPT,
        final.NJOP_BNG_SPPT,
        (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT) - final.NJOPTKP
           njop_sppt,
        final.NJOPTKP njoptkp_sppt,
        ROUND (
             (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
              - final.NJOPTKP)
           * (nilai_tarif / 100))
           pbb_terhutang_sppt,
        0 faktor_pengurang_sppt,
        CASE
           WHEN NVL (nilai_pbb_minimal, 0) >
                   ROUND (
                        (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
                         - final.NJOPTKP)
                      * (nilai_tarif / 100))
           THEN
              NVL (nilai_pbb_minimal, 0)
           ELSE
              ROUND (
                   (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
                    - final.NJOPTKP)
                 * (nilai_tarif / 100))
        END
           pbb_yg_harus_dibayar_sppt,
        '0' status_pembayaran_sppt,
        '0' status_tagihan_sppt,
        '1' status_cetak_sppt,
        TO_DATE ('" . $terbit . "', 'ddmmyyyy') tgl_terbit_sppt,
        TRUNC (TO_DATE ('" . $terbit . "', 'ddmmyyyy') + 2) tgl_cetak_sppt,
        '" . $nip . "' nip_pencetak_sppt
    FROM (SELECT core.kd_propinsi,
                core.kd_dati2,
                core.kd_kecamatan,
                core.kd_kelurahan,
                core.kd_blok,
                core.no_urut,
                core.kd_jns_op,
                thn_pajak_sppt,
                siklus_sppt,
                kd_kanwil,
                kd_kantor,
                kd_tp,
                nm_wp_sppt,
                jln_wp_sppt,
                blok_kav_no_wp_sppt,
                rw_wp_sppt,
                rt_wp_sppt,
                kelurahan_wp_sppt,
                kota_wp_sppt,
                kd_pos_wp_sppt,
                npwp_sppt,
                no_persil_sppt,
                NVL (kd_kls_tanah, 'XXX') kd_kls_tanah,
                NVL (thn_awal_kls_tanah, '1986') thn_awal_kls_tanah,
                NVL (kd_kls_bng, 'XXX') kd_kls_bng,
                NVL (thn_awal_kls_bng, '1986') thn_awal_kls_bng,
                TO_DATE ('" . $jatuhtempo . "', 'ddmmyyyy') tgl_jatuh_tempo_sppt,
                total_luas_bumi luas_bumi_sppt,
                total_luas_bng luas_bng_sppt,
                total_luas_bumi * nilai_per_m2_tanah * 1000
                   njop_bumi_sppt,
                total_luas_bng * NVL (nilai_per_m2_bng, 0) * 1000
                   njop_bng_sppt,
                NVL (njoptkp, 0) njoptkp
           FROM (SELECT dat_objek_pajak.kd_propinsi,
                        dat_objek_pajak.kd_dati2,
                        dat_objek_pajak.kd_kecamatan,
                        dat_objek_pajak.kd_kelurahan,
                        dat_objek_pajak.kd_blok,
                        dat_objek_pajak.no_urut,
                        dat_objek_pajak.kd_jns_op,
                        '" . $tahun . "' thn_pajak_sppt,
                        1 siklus_sppt,
                        '01' kd_kanwil,
                        '01' kd_kantor,
                        '49' kd_tp,
                        nm_wp nm_wp_sppt,
                        jalan_wp jln_wp_sppt,
                        blok_kav_no_wp blok_kav_no_wp_sppt,
                        rw_wp rw_wp_sppt,
                        rt_wp rt_wp_sppt,
                        kelurahan_wp kelurahan_wp_sppt,
                        kota_wp kota_wp_sppt,
                        kd_pos_wp kd_pos_wp_sppt,
                        npwp npwp_sppt,
                        no_persil no_persil_sppt,
                        total_luas_bumi,
                        CASE
                           WHEN total_luas_bumi > 0
                           THEN
                              ROUND (
                                 njop_bumi / total_luas_bumi / 1000)
                           ELSE
                              0
                        END
                           njop_bumi_meter,
                        njop_bumi,
                        total_luas_bng,
                        CASE
                           WHEN total_luas_bng > 0
                           THEN
                              ROUND (njop_bng / total_luas_bng / 1000)
                           ELSE
                              NULL
                        END
                           njop_bng_meter,
                        njop_bng
                   FROM DAT_OBJEK_PAJAK
                        JOIN dat_op_bumi
                           ON     dat_objek_pajak.kd_propinsi =
                                     dat_op_bumi.kd_propinsi
                              AND dat_objek_pajak.kd_dati2 =
                                     dat_op_bumi.kd_dati2
                              AND dat_objek_pajak.kd_kecamatan =
                                     dat_op_bumi.kd_kecamatan
                              AND dat_objek_pajak.kd_kelurahan =
                                     dat_op_bumi.kd_kelurahan
                              AND dat_objek_pajak.kd_blok =
                                     dat_op_bumi.kd_blok
                              AND dat_objek_pajak.no_urut =
                                     dat_op_bumi.no_urut
                              AND dat_objek_pajak.kd_jns_op =
                                     dat_op_bumi.kd_jns_op
                        JOIN dat_subjek_pajak
                           ON dat_subjek_pajak.subjek_pajak_id =
                                 dat_objek_pajak.subjek_pajak_id
                  WHERE     jns_transaksi_op != '3'
                        AND jns_bumi IN ('1', '2', '3')) core
                LEFT JOIN kelas_tanah
                   ON     kelas_tanah.thn_awal_kls_tanah <=
                             core.thn_pajak_sppt
                      AND kelas_tanah.thn_akhir_kls_tanah >=
                             core.thn_pajak_sppt
                      AND kelas_tanah.nilai_min_tanah <
                             core.njop_bumi_meter
                      AND kelas_tanah.nilai_max_tanah >=
                             core.njop_bumi_meter
                      AND kelas_tanah.kd_kls_tanah != 'XXX'
                LEFT JOIN kelas_bangunan
                   ON     kelas_bangunan.thn_awal_kls_bng <=
                             core.thn_pajak_sppt
                      AND kelas_bangunan.thn_akhir_kls_bng >=
                             core.thn_pajak_sppt
                      AND kelas_bangunan.nilai_min_bng <
                             core.njop_bng_meter
                      AND kelas_bangunan.nilai_max_bng >=
                             core.njop_bng_meter
                      AND kelas_bangunan.kd_kls_bng != 'XXX'
                LEFT JOIN
                (SELECT dat_subjek_pajak_njoptkp.kd_propinsi,
                        dat_subjek_pajak_njoptkp.kd_dati2,
                        kd_kecamatan,
                        kd_kelurahan,
                        kd_blok,
                        no_urut,
                        kd_jns_op,
                        thn_njoptkp,
                        nilai_njoptkp * 1000 njoptkp
                   FROM dat_subjek_pajak_njoptkp
                        JOIN njoptkp
                           ON     NJOPTKP.THN_AWAL <=
                                     dat_subjek_pajak_njoptkp.thn_njoptkp
                              AND dat_subjek_pajak_njoptkp.thn_njoptkp <=
                                     NJOPTKP.thn_akhir) njoptkp
                   ON     njoptkp.kd_propinsi = core.kd_propinsi
                      AND njoptkp.kd_dati2 = core.kd_dati2
                      AND njoptkp.kd_kecamatan = core.kd_kecamatan
                      AND njoptkp.kd_kelurahan = core.kd_kelurahan
                      AND njoptkp.kd_blok = core.kd_blok
                      AND njoptkp.no_urut = core.no_urut
                      AND njoptkp.kd_jns_op = core.kd_jns_op
                      AND njoptkp.thn_njoptkp = core.thn_pajak_sppt
                      and core.total_luas_bng>0
                      )
        final
        JOIN tarif
           ON     tarif.thn_awal <= final.thn_pajak_sppt
              AND TARIF.THN_AKHIR >= final.thn_pajak_sppt
              and tarif.njop_min<= case when  (((final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)- final.NJOPTKP))  < 0 then 0 else (((final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)- final.NJOPTKP)) end
              and tarif.njop_max>= case when  (((final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)- final.NJOPTKP))  < 0 then 0 else (((final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)- final.NJOPTKP)) end
        LEFT JOIN pbb_minimal
           ON pbb_minimal.thn_pbb_minimal = final.thn_pajak_sppt) fix
           left join (select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op 
           from sppt
           where thn_pajak_sppt='" . $tahun . "'
           and status_pembayaran_sppt<>'0') bl on fix.kd_propinsi=bl.kd_propinsi and
           fix.kd_dati2=bl.kd_dati2 and
           fix.kd_kecamatan=bl.kd_kecamatan and
           fix.kd_kelurahan=bl.kd_kelurahan and
           fix.kd_blok=bl.kd_blok and
           fix.no_urut=bl.no_urut and
           fix.kd_jns_op=bl.kd_jns_op where bl.kd_propinsi is null) fix";
    }

    public static function coreInfoObjek($nop)
    {

        $res = onlyNumber($nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);
        return  DB::connection("oracle_satutujuh")->table('dat_objek_pajak')
            ->whereraw("dat_objek_pajak.kd_propinsi='$kd_propinsi'
            and dat_objek_pajak.kd_dati2='$kd_dati2' 
            and dat_objek_pajak.kd_kecamatan='$kd_kecamatan'
            and dat_objek_pajak.kd_kelurahan='$kd_kelurahan'
            and dat_objek_pajak.kd_blok='$kd_blok'
            and dat_objek_pajak.no_urut='$no_urut'
            and dat_objek_pajak.kd_jns_op='$kd_jns_op'")
            ->join('ref_kecamatan', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'ref_kecamatan.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'ref_kecamatan.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
            })
            ->join('ref_kelurahan', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'ref_kelurahan.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'ref_kelurahan.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
            })
            ->join('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
            ->leftjoin('dat_op_bumi', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
            })
            ->leftjoin('tbl_spop', function ($sql) use ($res) {
                $sql->on('tbl_spop.no_formulir', '=', 'dat_objek_pajak.no_formulir_spop')
                    ->whereraw("tbl_spop.nop_proses='$res'");
            })
            ->select(db::raw("dat_objek_pajak.kd_propinsi,
            dat_objek_pajak.kd_dati2,
            dat_objek_pajak.kd_kecamatan,
            dat_objek_pajak.kd_kelurahan,
            dat_objek_pajak.kd_blok,
            dat_objek_pajak.no_urut,
            dat_objek_pajak.kd_jns_op,
            dat_objek_pajak.jalan_op,
            dat_objek_pajak.no_persil,
            dat_objek_pajak.blok_kav_no_op,
            dat_objek_pajak.rt_op,
            dat_objek_pajak.rw_op,
            nm_kelurahan,
            nm_kecamatan,
            dat_subjek_pajak.nm_wp,
            dat_subjek_pajak.jalan_wp,
            dat_subjek_pajak.blok_kav_no_wp,
            dat_subjek_pajak.rt_wp,
            dat_subjek_pajak.rw_wp,
            dat_subjek_pajak.kelurahan_wp,
            dat_subjek_pajak.kota_wp,
            total_luas_bumi,
            total_luas_bng,
            njop_bumi,
            njop_bng,
            dat_op_bumi.kd_znt,
            dat_op_bumi.jns_bumi,
            dat_objek_pajak.kd_status_wp,
            jns_transaksi_op,
            dat_objek_pajak.subjek_pajak_id,nop_asal,dat_subjek_pajak.telp_wp"))
            ->first();
    }
}
