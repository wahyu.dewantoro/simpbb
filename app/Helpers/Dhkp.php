<?php

namespace App\Helpers;

use App\Sppt;
// use Illuminate\Filesystem\Cache;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Dhkp
{

    public static function DhkpWepe($param)
    {
        $tahun = $param['tahun'] ?? '';
        $kd_kecamatan = $param['kd_kecamatan'] ?? '';
        $kd_kelurahan = $param['kd_kelurahan'] ?? '';
        $buku = $param['buku'] ?? '';

        $wk = $kd_kelurahan <> '' ? " and sppt.kd_kelurahan='$kd_kelurahan' " : " ";

        $wb = " ";
        if ($buku <> '') {
            foreach ($buku as $buku) {
                $wb .= " (get_buku(sppt.pbb_yg_harus_dibayar_sppt)='$buku') or";
            }
            $wb = "and ( " . substr($wb, 0, -2) . " )";
        }

        $sppt = Sppt::select(DB::raw("sppt.kd_propinsi||'.'||sppt.kd_dati2||'.'||sppt.kd_kecamatan||'.'||sppt.kd_kelurahan||'.'||sppt.kd_blok||'-'||sppt.no_urut||'.'||sppt.kd_jns_op nop,nm_wp_sppt,  sppt.jln_wp_sppt,sppt.blok_kav_no_wp_sppt,sppt.rt_wp_sppt,sppt.rw_wp_sppt ,                        sppt.kelurahan_wp_sppt,sppt.kota_wp_sppt ,sppt.luas_bumi_sppt,sppt.luas_bng_sppt,sppt.pbb_yg_harus_dibayar_sppt,sppt.tgl_jatuh_tempo_sppt ,sppt.status_pembayaran_sppt,get_buku(sppt.pbb_yg_harus_dibayar_sppt) buku,jalan_op,blok_kav_no_op,rt_op,rw_op,
                        (select tgl_pembayaran_sppt
                        from pembayaran_sppt b
                        where b.kd_propinsi=sppt.kd_propinsi
                        and b.kd_dati2=sppt.kd_dati2
                        and b.kd_kecamatan=sppt.kd_kecamatan
                        and b.kd_kelurahan=sppt.kd_kelurahan
                        and b.kd_blok=sppt.kd_blok
                        and b.no_urut=sppt.no_urut
                        and b.kd_jns_op=sppt.kd_jns_op
                        and b.thn_pajak_sppt=sppt.thn_pajak_sppt
                        and rownum=1)   tgl_bayar
                        "))
            ->join('dat_objek_pajak', function ($join) {
                $join->on(db::raw('sppt.kd_propinsi'), '=', db::raw('dat_objek_pajak.kd_propinsi'))
                    ->on(db::raw('sppt.kd_dati2'), '=', db::raw('dat_objek_pajak.kd_dati2'))
                    ->on(db::raw('sppt.kd_kecamatan'), '=', db::raw('dat_objek_pajak.kd_kecamatan'))
                    ->on(db::raw('sppt.kd_kelurahan'), '=', db::raw('dat_objek_pajak.kd_kelurahan'))
                    ->on(db::raw('sppt.kd_blok'), '=', db::raw('dat_objek_pajak.kd_blok'))
                    ->on(db::raw('sppt.no_urut'), '=', db::raw('dat_objek_pajak.no_urut'))
                    ->on(db::raw('sppt.kd_jns_op'), '=', db::raw('dat_objek_pajak.kd_jns_op'));
            })->whereraw("thn_pajak_sppt='$tahun'
                    and sppt.kd_kecamatan='$kd_kecamatan'
                    " . $wk . " 
                    " . $wb . "
                    and status_pembayaran_sppt not in ('3')")->orderby('nm_wp_sppt')->get();
        return $sppt;
    }

    public static function RingkasanDhkpWepe($param)
    {
        if (isset($param['buku']) == '') {
            $buku = "1,2";
        } else {
            $buku = implode(',', $param['buku']);
        }

        $kd_kecamatan = $param['kd_kecamatan'] ?? '';
        $kd_kelurahan = $param['kd_kelurahan'] ?? '';
        $tahun = $param['tahun'] ?? date('Y');

        $wkc = "";
        if ($kd_kecamatan <> '') {
            $wkc = " and sppt.kd_kecamatan='$kd_kecamatan' ";
        }
        $wkl = "";
        if ($kd_kelurahan <> '') {
            $wkl = " and sppt.kd_kelurahan='$kd_kelurahan' ";
        }

        $sql = "SELECT nm_sektor,kecamatan,kelurahan, kd_kecamatan,kd_kelurahan,
        sum(op_lunas) op_lunas,
            sum(jumlah_lunas) jumlah_lunas,
            sum(op_belum) op_belum,
            sum(jumlah_belum) jumlah_belum,
               sum(op_total) op_total,
               sum(jumlah_total) jumlah_total
        from (
        select sppt.kd_kecamatan,
    sppt.kd_kelurahan,nm_sektor,nm_kecamatan kecamatan,nm_kelurahan kelurahan,case when status_pembayaran_sppt=1 then count(1) else 0 end op_lunas,
        case when status_pembayaran_sppt=1 then sum(pbb_yg_harus_dibayar_sppt) else 0 end jumlah_lunas,
         case when status_pembayaran_sppt<>1 then count(1) else 0 end op_belum,
        case when status_pembayaran_sppt<>1 then sum(pbb_yg_harus_dibayar_sppt) else 0 end jumlah_belum,
        count(1) op_total,sum(pbb_yg_harus_dibayar_sppt) jumlah_total 
        from (
            select sppt.kd_propinsi,
                           sppt.kd_dati2,
                           sppt.kd_kecamatan,
                           sppt.kd_kelurahan,
                           sppt.kd_blok,
                           sppt.no_urut,
                           sppt.kd_jns_op,
                           case when pbb_yg_harus_dibayar_sppt - NVL (potongan, 0)=0 then '1' else   sppt.status_pembayaran_sppt end status_pembayaran_sppt,
                           pbb_yg_harus_dibayar_sppt - NVL (potongan, 0) pbb_yg_harus_dibayar_sppt
                      FROM core_sppt sppt
                      LEFT JOIN
                           (SELECT a.kd_propinsi,
                                   a.kd_dati2,
                                   a.kd_kecamatan,
                                   a.kd_kelurahan,
                                   a.kd_blok,
                                   a.no_urut,
                                   a.kd_jns_op,
                                   a.thn_pajak_sppt,
                                   kd_status
                              FROM sim_pbb.sppt_penelitian a
                                   LEFT JOIN SIM_PBB.DATA_BILLING b
                                      ON a.data_billing_id = b.data_billing_id
                             WHERE kd_status = '0') aj
                              ON     aj.kd_propinsi = sppt.kd_propinsi
                                 AND aj.kd_dati2 = sppt.kd_dati2
                                 AND aj.kd_kecamatan = sppt.kd_kecamatan
                                 AND aj.kd_kelurahan = sppt.kd_kelurahan
                                 AND aj.kd_blok = sppt.kd_blok
                                 AND aj.no_urut = sppt.no_urut
                                 AND aj.kd_jns_op = sppt.kd_jns_op
                                 AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
                     WHERE    aj.thn_pajak_sppt IS NULL and
                       sppt.thn_pajak_sppt = '$tahun'
                            " . $wkc . "  " . $wkl . "
                           AND status_pembayaran_sppt IN ('0', '1')
                           AND buku IN ($buku)
                 )  sppt
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=sppt.kd_kecamatan
        join ref_kelurahan on sppt.kd_kecamatan=ref_kelurahan.kd_kecamatan and sppt.kd_kelurahan=ref_kelurahan.kd_kelurahan
        join ref_jns_sektor on ref_kelurahan.kd_sektor=REF_JNS_SEKTOR.KD_SEKTOR
        group by status_pembayaran_sppt,nm_kecamatan,nm_kelurahan,nm_sektor,sppt.kd_kecamatan,sppt.kd_kelurahan
        )
        group by kecamatan,kelurahan,nm_sektor,kd_kecamatan,kd_kelurahan order by kd_kecamatan,kd_kelurahan";
        // $data=Cache::remember(md5($sql), 500, function () use ($sql) {
        return DB::connection('oracle_dua')->select(db::raw($sql));
        // });
        // return $data;
    }


    public static function RekapDesa($tahun, $kd_kecamatan, $buku, $kd_kelurahan = null)
    {
        $wb = "";

        if (!empty($kd_kelurahan)) {
            $wb .= " and sppt.kd_kelurahan='$kd_kelurahan' ";
        }


        if ($buku <> '') {

            if ($buku == '1') {
                $wb .= "  AND get_buku (pbb_yg_harus_dibayar_sppt) IN ('1','2') ";
            } else {
                $wb .= "  AND get_buku (pbb_yg_harus_dibayar_sppt) IN ('3','4','5') ";
            }

            /* $imp = "";
            $imp = implode(',', $buku);
            $wb .= "  AND get_buku (pbb_yg_harus_dibayar_sppt) IN (" . $imp . ") "; */
        }

        return DB::connection('oracle_dua')->select(db::raw("select core.kd_kelurahan,
        nm_kelurahan,
        count(1) sppt,
         sum(bumi) bumi,
        sum(bangunan) bangunan,
        sum(pbb_yg_harus_dibayar_sppt) pbb,
        sum(bayar) bayar,
        sum(belum_bayar) belum_bayar 
        from (
        SELECT sppt.kd_propinsi,
                                   sppt.kd_dati2,
                                   sppt.kd_kecamatan,
                                   sppt.kd_kelurahan,
                                   sppt.kd_blok,
                                   sppt.no_urut,
                                   sppt.kd_jns_op,
                                   sppt.status_pembayaran_sppt,
                                   luas_bumi_sppt bumi,
                                    luas_bng_sppt bangunan,
                                   pbb_yg_harus_dibayar_sppt-potongan pbb_yg_harus_dibayar_sppt ,
                                       case when sppt.status_pembayaran_sppt=1 then   pbb_yg_harus_dibayar_sppt-potongan  else null end bayar, 
                                       case when sppt.status_pembayaran_sppt=0 then   pbb_yg_harus_dibayar_sppt-potongan  else null end belum_bayar
                              FROM core_sppt sppt
                              LEFT JOIN
                              (SELECT a.kd_propinsi,
                                      a.kd_dati2,
                                      a.kd_kecamatan,
                                      a.kd_kelurahan,
                                      a.kd_blok,
                                      a.no_urut,
                                      a.kd_jns_op,
                                      a.thn_pajak_sppt,
                                      kd_status
                                 FROM sim_pbb.sppt_penelitian a
                                      LEFT JOIN SIM_PBB.DATA_BILLING b
                                         ON a.data_billing_id = b.data_billing_id
                                WHERE kd_status = '0') aj
                                 ON     aj.kd_propinsi = sppt.kd_propinsi
                                    AND aj.kd_dati2 = sppt.kd_dati2
                                    AND aj.kd_kecamatan = sppt.kd_kecamatan
                                    AND aj.kd_kelurahan = sppt.kd_kelurahan
                                    AND aj.kd_blok = sppt.kd_blok
                                    AND aj.no_urut = sppt.no_urut
                                    AND aj.kd_jns_op = sppt.kd_jns_op
                                    AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
                             WHERE     sppt.thn_pajak_sppt = '$tahun'
                                    AND aj.thn_pajak_sppt IS NULL
                                   AND sppt.kd_kecamatan = '$kd_kecamatan'
                                   AND status_pembayaran_sppt IN ('0', '1')
                                   $wb
        ) core
        join ref_kelurahan on ref_kelurahan.kd_kelurahan=core.kd_kelurahan and core.kd_kecamatan=ref_kelurahan.kd_kecamatan
        group by core.kd_kelurahan,
        nm_kelurahan
        order by core.kd_kelurahan"));
    }

    public static function PerbandinganWepe($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $tahun_kemarin = $tahun - 1;
        $wk = $kd_kelurahan <> '' ? " and sppt.kd_kelurahan='$kd_kelurahan' " : "";

        $wb = "";
        if ($buku <> '') {

            foreach ($buku as $buku) {
                switch ($buku) {
                    case '1':
                        # code...
                        $wb .= " (sppt.pbb_yg_harus_dibayar_sppt <= 100000) or";
                        break;
                    case '2':
                        # code...
                        $wb .= " (sppt.pbb_yg_harus_dibayar_sppt > 100000 AND sppt.pbb_yg_harus_dibayar_sppt <= 500000) or";
                        break;
                    case '3':
                        # code...
                        $wb .= " (sppt.pbb_yg_harus_dibayar_sppt > 500000 AND sppt.pbb_yg_harus_dibayar_sppt <= 2000000) or";
                        break;
                    case '4':
                        # code...
                        $wb .= " (sppt.pbb_yg_harus_dibayar_sppt > 2000000 AND sppt.pbb_yg_harus_dibayar_sppt <= 5000000) or";
                        break;
                    default:
                        # code...
                        $wb .= " (sppt.pbb_yg_harus_dibayar_sppt > 5000000) or";
                        break;
                }
            }
            $wb = "and ( " . substr($wb, 0, -2) . " )";
        }
        return  DB::connection('oracle_satutujuh')
            ->table(db::raw('sppt'))
            ->select(db::raw("sppt.kd_propinsi||'.'||sppt.kd_dati2||'.'||sppt.kd_kecamatan||'.'||sppt.kd_kelurahan||'.'||sppt.kd_blok||'-'||sppt.no_urut||'.'||sppt.kd_jns_op nop, sppt.nm_wp_sppt nm_wp,sppt.pbb_yg_harus_dibayar_sppt pbb_sekarang,sppt_old.pbb_yg_harus_dibayar_sppt pbb_kemarin,sppt.pbb_yg_harus_dibayar_sppt - sppt_old.pbb_yg_harus_dibayar_sppt pbb_selisih"))
            ->leftjoin(DB::raw("(select  sppt.kd_propinsi,sppt.kd_dati2,sppt.kd_kecamatan,sppt.kd_kelurahan,sppt.kd_blok,sppt.no_urut,sppt.kd_jns_op, sppt.nm_wp_sppt,sppt.pbb_yg_harus_dibayar_sppt
            from sppt where sppt.thn_pajak_sppt='$tahun_kemarin'
            and sppt.kd_kecamatan='$kd_kecamatan'  
            $wb
            $wk
            and sppt.status_pembayaran_sppt not in ('3') ) SPPT_OLD"), function ($join) {
                $join->on(db::raw('sppt.kd_propinsi'), '=', db::raw('sppt_old.kd_propinsi'))
                    ->on(db::raw('sppt.kd_dati2'), '=', db::raw('sppt_old.kd_dati2'))
                    ->on(db::raw('sppt.kd_kecamatan'), '=', db::raw('sppt_old.kd_kecamatan'))
                    ->on(db::raw('sppt.kd_kelurahan'), '=', db::raw('sppt_old.kd_kelurahan'))
                    ->on(db::raw('sppt.kd_blok'), '=', db::raw('sppt_old.kd_blok'))
                    ->on(db::raw('sppt.no_urut'), '=', db::raw('sppt_old.no_urut'))
                    ->on(db::raw('sppt.kd_jns_op'), '=', db::raw('sppt_old.kd_jns_op'));
            })
            ->whereraw("sppt.thn_pajak_sppt='$tahun'
            and sppt.kd_kecamatan='$kd_kecamatan' 
            and sppt.status_pembayaran_sppt not in ('3') $wb
            $wk")->orderby(db::raw("sppt.nm_wp_sppt"));
    }

    public static function PerbandinganWepeRekap($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $wb = " ";
        if ($buku <> '') {
            foreach ($buku as $buku) {
                $wb .= " (get_buku(sppt.pbb_yg_harus_dibayar_sppt)='$buku') or";
            }
            $wb = "and ( " . substr($wb, 0, -2) . " )";
        }
        $tahun_kemarin = $tahun - 1;
        $wk = $kd_kelurahan <> 'all' ? " and sppt.kd_kelurahan='$kd_kelurahan' " : " ";
        return  DB::connection('oracle_satutujuh')
            ->select(DB::raw("select sppt.kd_kecamatan,
         sppt.kd_kelurahan,
         nm_kecamatan,
         nm_kelurahan,
         nm_sektor,
         COUNT (sppt.kd_propinsi) nop,
         SUM (sppt.pbb_yg_harus_dibayar_sppt) pbb_sekarang,
         SUM (sppt.insentif) insentif_sekarang,
         SUM (NVL (sppt_old.pbb_yg_harus_dibayar_sppt, 0)) pbb_kemarin,
         SUM (sppt_old.insentif) insentif_kemarin,
           (SUM (sppt.pbb_yg_harus_dibayar_sppt - nvl(sppt.insentif,0)))
         - (  SUM (NVL (sppt_old.pbb_yg_harus_dibayar_sppt, 0)  - nvl(sppt_old.insentif,0)))
            selisih    
            FROM  (
                select  a.thn_pajak_sppt,a.nm_wp_sppt,a.status_pembayaran_sppt,a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op, pbb_yg_harus_dibayar_sppt,nvl(nilai_potongan ,0) insentif
                from sppt  a
left join sppt_potongan_detail b on a.kd_propinsi=b.kd_propinsi and
          a.kd_dati2=b.kd_dati2 and
          a.kd_kecamatan=b.kd_kecamatan and
          a.kd_kelurahan=b.kd_kelurahan and
          a.kd_blok=b.kd_blok and
          a.no_urut=b.no_urut and
          a.kd_jns_op=b.kd_jns_op and a.thn_pajak_sppt=b.thn_pajak_sppt and
          b.jns_potongan='1'
            ) SPPT
                 LEFT JOIN
                 (SELECT sppt.kd_propinsi,
                         sppt.kd_dati2,
                         sppt.kd_kecamatan,
                         sppt.kd_kelurahan,
                         sppt.kd_blok,
                         sppt.no_urut,
                         sppt.kd_jns_op,
                         sppt.nm_wp_sppt,
                         sppt.pbb_yg_harus_dibayar_sppt,
                         insentif
                    FROM (select  a.thn_pajak_sppt,a.nm_wp_sppt,a.status_pembayaran_sppt,a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op, pbb_yg_harus_dibayar_sppt,nvl(nilai_potongan ,0) insentif
                    from sppt  a
    left join sppt_potongan b on a.kd_propinsi=b.kd_propinsi and
              a.kd_dati2=b.kd_dati2 and
              a.kd_kecamatan=b.kd_kecamatan and
              a.kd_kelurahan=b.kd_kelurahan and
              a.kd_blok=b.kd_blok and
              a.no_urut=b.no_urut and
              a.kd_jns_op=b.kd_jns_op and a.thn_pajak_sppt=b.thn_pajak_sppt) sppt
                   WHERE     sppt.thn_pajak_sppt = '$tahun_kemarin'
                         AND sppt.kd_kecamatan = '$kd_kecamatan'
                         $wk
                         AND sppt.status_pembayaran_sppt not in ('3')
                         $wb
                         ) SPPT_OLD
                    ON     sppt.kd_propinsi = sppt_old.kd_propinsi
                       AND sppt.kd_dati2 = sppt_old.kd_dati2
                       AND sppt.kd_kecamatan = sppt_old.kd_kecamatan
                       AND sppt.kd_kelurahan = sppt_old.kd_kelurahan
                       AND sppt.kd_blok = sppt_old.kd_blok
                       AND sppt.no_urut = sppt_old.no_urut
                       AND sppt.kd_jns_op = sppt_old.kd_jns_op
             join ref_kecamatan on ref_kecamatan.kd_kecamatan=sppt.kd_kecamatan
             join ref_kelurahan on ref_kelurahan.kd_kecamatan=sppt.kd_kecamatan and ref_kelurahan.kd_kelurahan=sppt.kd_kelurahan
             join ref_jns_sektor on ref_jns_sektor.kd_sektor=ref_kelurahan.kd_sektor
           WHERE     sppt.thn_pajak_sppt = '$tahun'
                 AND sppt.kd_kecamatan = '$kd_kecamatan'
                 AND sppt.status_pembayaran_sppt not in ('3')
                 $wk
                 $wb
        group by nm_kecamatan,nm_kelurahan,nm_sektor,sppt.kd_kecamatan,sppt.kd_kelurahan"));
    }


    public static function RekapPerbandinganDesa($tahun, $kd_kecamatan, $buku)
    {
        $tahun_kemarin = $tahun - 1;
        $wb = " ";
        $wp = " ";
        if ($buku <> '') {
            $q = "";
            $w = "";
            foreach ($buku as $buku) {
                $q .= " (get_buku(sppt.pbb_yg_harus_dibayar_sppt)='$buku') or";
                $w .= " (get_buku(nvl(a.jml_sppt_yg_dibayar,0)-a.denda_sppt )='$buku') or";
            }
            $wb .= " and ( " . substr($q, 0, -2) . " ) ";

            $wp .= " and ( " . substr($w, 0, -2) . " ) ";
        }

        // dd($wb);

        return  DB::connection('oracle_dua')->select(DB::raw("SELECT a.kd_kelurahan,
                                                        a.nm_kelurahan,
                                                        nvl(b.baku,0) baku_sekarang,
                                                        nvl(c.baku,0) baku_kemarin,
                                                        nvl(b.baku,0) - 
                                                        nvl(c.baku,0) baku_selisih,
                                                        nvl(d.realisasi,0) realisasi_sekarang,
                                                        nvl(e.realisasi,0) realisasi_kemarin,
                                                        nvl(d.realisasi,0) -
                                                        nvl(e.realisasi,0) realisasi_selisih
                                                    FROM ref_kelurahan a
                                                        LEFT JOIN
                                                        (  SELECT sppt.kd_kelurahan, SUM (sppt.pbb_yg_harus_dibayar_sppt) baku
                                                            FROM sppt
                                                            WHERE     sppt.thn_pajak_sppt = '$tahun'
                                                                AND sppt.kd_kecamatan = '$kd_kecamatan'
                                                                $wb
                                                                AND sppt.status_pembayaran_sppt not in ('3')
                                                        GROUP BY sppt.kd_kelurahan) b
                                                            ON a.kd_kelurahan = b.kd_kelurahan
                                                        LEFT JOIN
                                                        (  SELECT sppt.kd_kelurahan, SUM (nvl(sppt.pbb_yg_harus_dibayar_sppt,0)) baku
                                                            FROM sppt
                                                            WHERE     sppt.thn_pajak_sppt = '$tahun_kemarin'
                                                                AND sppt.kd_kecamatan = '$kd_kecamatan'
                                                                $wb
                                                                AND sppt.status_pembayaran_sppt not in ('3')
                                                        GROUP BY sppt.kd_kelurahan) c
                                                            ON c.kd_kelurahan = a.kd_kelurahan
                                                        LEFT JOIN
                                                        (  SELECT a.kd_kelurahan, SUM (nvl(a.jml_sppt_yg_dibayar,0)) realisasi
                                                            FROM pembayaran_sppt a
                                                            WHERE     TO_CHAR (a.tgl_pembayaran_sppt, 'yyyy') = '$tahun'
                                                                AND a.kd_kecamatan = '$kd_kecamatan'
                                                                $wp
                                                        GROUP BY a.kd_kelurahan) d
                                                            ON d.kd_kelurahan = a.kd_kelurahan
                                                        LEFT JOIN
                                                        (  SELECT a.kd_kelurahan, SUM (nvl(a.jml_sppt_yg_dibayar,0)) realisasi
                                                            FROM pembayaran_sppt a
                                                            WHERE     TO_CHAR (a.tgl_pembayaran_sppt, 'yyyy') = '$tahun_kemarin'
                                                                AND a.kd_kecamatan = '$kd_kecamatan'
                                                                $wp
                                                        GROUP BY a.kd_kelurahan) e
                                                            ON e.kd_kelurahan = a.kd_kelurahan
                                                    WHERE a.kd_kecamatan = '$kd_kecamatan'
                                                    order by a.kd_kelurahan asc"));
    }

    public static function ringkasanBaku($param)
    {
        $tahun = $param['tahun'];
        $kd_kecamatan = $param['kd_kecamatan'] ?? null;
        // $wa = " and a.status_pembayaran_sppt not in ('3') ";
        $wh = "";
        if ($kd_kecamatan <> '') {
            foreach ($kd_kecamatan as $i) {
                // $wa .= " a.kd_kecamatan='$i' or";
                $wh .= " a.kd_kecamatan='$i' or";
            }
            // $wa = " and ( " . substr($wa, 0, -2) . " ) ";
            $wh = " where ( " . substr($wh, 0, -2) . " ) ";
        }

        $sql = "select  a.kd_kecamatan,a.kd_kelurahan,a.nm_kecamatan,a.nm_kelurahan,a.kd_buku,count(b.thn_pajak_sppt) jumlah_op,sum(pbb_akhir) baku,
        sum(pokok) pokok,sum(denda) denda,sum(jumlah) total
        from (SELECT ref_kelurahan.kd_kecamatan,
                         ref_kelurahan.kd_kelurahan,
                         nm_kecamatan,
                         nm_kelurahan,
                         '1' kd_buku
                    FROM ref_kelurahan
                         JOIN ref_kecamatan
                            ON ref_kecamatan.kd_kecamatan = ref_kelurahan.kd_kecamatan
                  UNION
                  SELECT ref_kelurahan.kd_kecamatan,
                         ref_kelurahan.kd_kelurahan,
                         nm_kecamatan,
                         nm_kelurahan,
                         '2' kd_buku
                    FROM ref_kelurahan
                         JOIN ref_kecamatan
                            ON ref_kecamatan.kd_kecamatan = ref_kelurahan.kd_kecamatan
                  UNION
                  SELECT ref_kelurahan.kd_kecamatan,
                         ref_kelurahan.kd_kelurahan,
                         nm_kecamatan,
                         nm_kelurahan,
                         '3' kd_buku
                    FROM ref_kelurahan
                         JOIN ref_kecamatan
                            ON ref_kecamatan.kd_kecamatan = ref_kelurahan.kd_kecamatan
                  UNION
                  SELECT ref_kelurahan.kd_kecamatan,
                         ref_kelurahan.kd_kelurahan,
                         nm_kecamatan,
                         nm_kelurahan,
                         '4' kd_buku
                    FROM ref_kelurahan
                         JOIN ref_kecamatan
                            ON ref_kecamatan.kd_kecamatan = ref_kelurahan.kd_kecamatan
                  UNION
                  SELECT ref_kelurahan.kd_kecamatan,
                         ref_kelurahan.kd_kelurahan,
                         nm_kecamatan,
                         nm_kelurahan,
                         '5' kd_buku
                    FROM ref_kelurahan
                         JOIN ref_kecamatan
                            ON ref_kecamatan.kd_kecamatan = ref_kelurahan.kd_kecamatan) a
                            left join sppt_baku b on a.kd_kecamatan=b.kd_kecamatan and a.kd_kelurahan=b.kd_kelurahan
                             and a.kd_buku=b.kd_buku
                             and b.thn_pajak_sppt='" . $tahun . "'
            left join sppt_realisasi c on b.kd_propinsi=c.kd_propinsi and
        b.kd_dati2=c.kd_dati2 and
        b.kd_kecamatan=c.kd_kecamatan and
        b.kd_kelurahan=c.kd_kelurahan and
        b.kd_blok=c.kd_blok and
        b.no_urut=c.no_urut and
        b.thn_pajak_sppt=c.thn_pajak_sppt  
        " . $wh . "
        group by a.kd_kecamatan,a.kd_kelurahan,a.nm_kecamatan,a.nm_kelurahan,a.kd_buku
        order by a.kd_kecamatan,a.kd_kelurahan,a.nm_kecamatan,a.nm_kelurahan,a.kd_buku";
        /*  $sql = "
        select a.* ,jumlah_op,baku,pokok,denda,jumlah total
        from  (
        select ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan,nm_kecamatan,nm_kelurahan,'1' kd_buku
        from ref_kelurahan
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan
        union
        select ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan,nm_kecamatan,nm_kelurahan,'2' kd_buku
        from ref_kelurahan
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan
        union
        select ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan,nm_kecamatan,nm_kelurahan,'3' kd_buku
        from ref_kelurahan
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan
        union
        select ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan,nm_kecamatan,nm_kelurahan,'4' kd_buku
        from ref_kelurahan
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan
        union
        select ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan,nm_kecamatan,nm_kelurahan,'5' kd_buku
        from ref_kelurahan
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan) a
        left join (SELECT   sppt.kd_kecamatan,sppt.kd_kelurahan, kd_buku,
                           COUNT (sppt.kd_propinsi) jumlah_op,
                           SUM (pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)) baku,
                           SUM (NVL (jml_sppt_yg_dibayar, 0) - NVL (denda_sppt, 0)) pokok,
                           SUM (NVL (denda_sppt, 0)) denda,
                           SUM (NVL (jml_sppt_yg_dibayar, 0)) jumlah
                      FROM sppt
                           JOIN ref_buku
                              ON     sppt.thn_pajak_sppt BETWEEN ref_buku.thn_awal
                                                             AND ref_buku.thn_akhir
                                 AND sppt.pbb_yg_harus_dibayar_sppt BETWEEN ref_buku.nilai_min_buku
                                                                        AND ref_buku.nilai_max_buku
                           LEFT JOIN
                           (SELECT a.kd_propinsi,
                                   a.kd_dati2,
                                   a.kd_kecamatan,
                                   a.kd_kelurahan,
                                   a.kd_blok,
                                   a.no_urut,
                                   a.kd_jns_op,
                                   a.thn_pajak_sppt,
                                   kd_status
                              FROM sim_pbb.sppt_penelitian a
                                   LEFT JOIN SIM_PBB.DATA_BILLING b
                                      ON a.data_billing_id = b.data_billing_id
                             WHERE kd_status = '0') aj
                              ON     aj.kd_propinsi = sppt.kd_propinsi
                                 AND aj.kd_dati2 = sppt.kd_dati2
                                 AND aj.kd_kecamatan = sppt.kd_kecamatan
                                 AND aj.kd_kelurahan = sppt.kd_kelurahan
                                 AND aj.kd_blok = sppt.kd_blok
                                 AND aj.no_urut = sppt.no_urut
                                 AND aj.kd_jns_op = sppt.kd_jns_op
                                 AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
                           LEFT JOIN sppt_potongan
                              ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                                 AND sppt.kd_dati2 = sppt_potongan.kd_dati2
                                 AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
                                 AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
                                 AND sppt.kd_blok = sppt_potongan.kd_blok
                                 AND sppt.no_urut = sppt_potongan.no_urut
                                 AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
                                 AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
                           LEFT JOIN pembayaran_sppt
                              ON     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
                                 AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                 AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
                                 AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
                                 AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                 AND sppt.no_urut = pembayaran_sppt.no_urut
                                 AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                 AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
                     WHERE     sppt.thn_pajak_sppt = '$tahun'
                     ".$wh."
                           AND aj.thn_pajak_sppt IS NULL
                           AND sppt.status_pembayaran_sppt <> '3'
                  GROUP BY sppt.kd_kecamatan,sppt.kd_kelurahan, kd_buku
                  ) real on a.kd_kecamatan=real.kd_Kecamatan
                            and a.kd_kelurahan=real.kd_kelurahan
                               and a.kd_buku=real.kd_buku
                               order by a.kd_kecamatan,a.kd_kelurahan,a.kd_buku";*/

        // $timeout = 100;
        // $data = Cache::remember(md5($sql), $timeout, function () use ($sql) {
        return DB::connection("oracle_satutujuh")->select(db::raw($sql));
        // });

        // return $data;

        /* 
        $data = DB::connection("oracle_dua")->select(DB::raw("select a.* ,nm_kecamatan,nvl(op,0) op,nvl(baku,0) baku,nvl(pokok,0) pokok ,nvl(denda,0) denda,nvl(total,0) total
        from (
        select kd_kecamatan,kd_kelurahan,nm_kelurahan,1 buku 
        from ref_kelurahan
        union 
        select kd_kecamatan,kd_kelurahan,nm_kelurahan,2 buku 
        from ref_kelurahan
        union 
        select kd_kecamatan,kd_kelurahan,nm_kelurahan,3 buku 
        from ref_kelurahan
        union 
        select kd_kecamatan,kd_kelurahan,nm_kelurahan,4 buku 
        from ref_kelurahan
        union 
        select kd_kecamatan,kd_kelurahan,nm_kelurahan,5 buku 
        from ref_kelurahan ) a
        left join ref_kecamatan b on a.kd_kecamatan=b.kd_kecamatan
        left join (select * from MV_RINGKASAN_BAKU where thn_pajak_sppt='$tahun') c  on c.kd_kecamatan=a.kd_kecamatan and a.kd_kelurahan=c.kd_kelurahan and a.buku=c.buku
        order by a.kd_kecamatan,a.kd_kelurahan,a.buku asc")); */
        // return $data;
    }

    public static function exportExcelWepe($param)
    {

        $kd_kecamatan = $param['kd_kecamatan'] ?? '';
        $kd_kelurahan = $param['kd_kelurahan'] ?? '';
        $buku = $param['buku'] ?? '';
        $tahun = $param['tahun'] ?? date('Y');
        $and = "";

        if ($kd_kecamatan <> '') {
            $and .= "  and sppt.kd_kecamatan='$kd_kecamatan'";
        }

        if ($kd_kelurahan <> '') {
            $and .= "  and sppt.kd_kelurahan='$kd_kelurahan'";
        }

        if ($buku <> '') {
            $and .= " and sppt.buku in ($buku) ";
        }

        // $sppt = Sppt::
        $sppt = DB::connection('oracle_satutujuh')->table(db::raw('core_sppt sppt'))
            ->select(DB::raw("sppt.kd_propinsi||'.'||sppt.kd_dati2||'.'||sppt.kd_kecamatan||'.'||sppt.kd_kelurahan||'.'||sppt.kd_blok||'-'||sppt.no_urut||'.'||sppt.kd_jns_op nop,nm_wp_sppt, sppt.jln_wp_sppt,sppt.blok_kav_no_wp_sppt,sppt.rt_wp_sppt,sppt.rw_wp_sppt , sppt.kelurahan_wp_sppt,sppt.kota_wp_sppt ,sppt.luas_bumi_sppt,sppt.luas_bng_sppt,sppt.pbb_yg_harus_dibayar_sppt ,nvl(potongan,0) potongan,sppt.tgl_jatuh_tempo_sppt ,sppt.status_pembayaran_sppt, buku,jalan_op,blok_kav_no_op,rt_op,rw_op, tgl_bayar, last_pbb, hit_denda(pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt,sysdate) es_denda "))
            ->whereraw("sppt.thn_pajak_sppt='$tahun' AND sppt.status_pembayaran_sppt in ('0','1') " . $and)->orderbyraw("buku asc, sppt.kd_kecamatan asc,sppt.kd_kelurahan asc ,sppt.kd_blok asc,sppt.no_urut asc")->get();
        // ->whereraw("sppt.thn_pajak_sppt='$tahun' and sppt.status_pembayaran_sppt in ('0') " . $and)->orderby('sppt.nm_wp_sppt')->get();
        return $sppt;
    }

    /* public static function RekapAll($tahun)
    {
        $data = DB::connection('oracle_satutujuh')->table('sppt')
            ->leftjoin('sppt_potongan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->whereraw("sppt.status_pembayaran_sppt<>'3' and sppt.thn_pajak_sppt='$tahun'")
            ->select(db::raw("sppt.kd_kecamatan ,(select nm_kecamatan from ref_kecamatan where kd_Kecamatan=sppt.kd_Kecamatan) nm_kecamatan,sppt.kd_kelurahan,
            (select nm_kelurahan from ref_kelurahan where kd_kecamatan=sppt.kd_Kecamatan and kd_kelurahan=sppt.kd_Kelurahan) nm_kelurahan,
             count(1) jumlah_objek,
            sum(pbb_yg_harus_dibayar_sppt) pbb,sum(nvl(nilai_potongan,0)) potongan,sum(pbb_yg_harus_dibayar_sppt-nvl(nilai_potongan,0)) pbb_akhir"))
            ->groupByRaw("rollup(sppt.kd_kecamatan,sppt.kd_kelurahan)")
            ->orderByRaw("sppt.kd_kecamatan,sppt.kd_kelurahan");
        return $data;
    } */

    public static function RekapAll($tahun)
    {

        $data = DB::connection("oracle_satutujuh")->table('ref_kecamatan')
            ->join("ref_kelurahan", 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
            ->leftJoin(db::raw("(select kd_kecamatan,kd_kelurahan, count(1) jumlah_objek ,sum(pbb) pbb,sum(potongan) potongan,sum(pbb_akhir) pbb_akhir
            from sppt_baku
            where thn_pajak_sppt='$tahun'
            group by kd_kecamatan,kd_kelurahan) rekap"), function ($join) {
                $join->on('rekap.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->on('rekap.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
            })
            ->leftjoin(db::raw("(select dat_objek_pajak.kd_kecamatan,dat_objek_pajak.kd_kelurahan,count(1) op
       from dat_objek_pajak
       join dat_op_bumi on dat_objek_pajak.kd_propinsi=dat_op_bumi.kd_propinsi and 
       dat_objek_pajak.kd_dati2=dat_op_bumi.kd_dati2 and 
       dat_objek_pajak.kd_kecamatan=dat_op_bumi.kd_kecamatan and 
       dat_objek_pajak.kd_kelurahan=dat_op_bumi.kd_kelurahan and 
       dat_objek_pajak.kd_blok=dat_op_bumi.kd_blok and 
       dat_objek_pajak.no_urut=dat_op_bumi.no_urut and 
       dat_objek_pajak.kd_jns_op=dat_op_bumi.kd_jns_op 
       where
        total_luas_bumi>0 and  jns_transaksi_op != '3'
               AND jns_bumi IN ('1', '2', '3')
               group by dat_objek_pajak.kd_kecamatan,dat_objek_pajak.kd_kelurahan) jp"), function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'jp.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'jp.kd_kelurahan');
            })
            // and aj.kd_propinsi is null
            ->select(db::raw("ref_kecamatan.kd_kecamatan,nm_kecamatan,ref_kelurahan.kd_kelurahan,nm_kelurahan,jumlah_objek,pbb,potongan,pbb_akhir,op"))
            ->orderByRaw('ref_kecamatan.kd_kecamatan asc,ref_kelurahan.kd_kelurahan asc');
        return $data;
    }


    public static function RekapPerbandingan($tahun, $tahun_last)
    {
        $data = DB::connection("oracle_satutujuh")->table('ref_kecamatan')
            ->join("ref_kelurahan", 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
            ->leftJoin(
                db::raw("(select  sppt.kd_kecamatan ,sppt.kd_kelurahan,
                                    count(1) jumlah_objek,
                                sum(pbb_yg_harus_dibayar_sppt) pbb,sum(nvl(nilai_potongan,0)) potongan,sum(pbb_yg_harus_dibayar_sppt-nvl(nilai_potongan,0)) pbb_akhir
                                from sppt 
                                left join sppt_potongan on sppt.kd_propinsi=sppt_potongan.kd_propinsi and
                                sppt.kd_dati2=sppt_potongan.kd_dati2 and
                                sppt.kd_kecamatan=sppt_potongan.kd_kecamatan and
                                sppt.kd_kelurahan=sppt_potongan.kd_kelurahan and
                                sppt.kd_blok=sppt_potongan.kd_blok and
                                sppt.no_urut=sppt_potongan.no_urut and
                                sppt.kd_jns_op=sppt_potongan.kd_jns_op and
                                sppt.thn_pajak_sppt=sppt_potongan.thn_pajak_sppt
                                LEFT JOIN
                                            (SELECT a.kd_propinsi,
                                                    a.kd_dati2,
                                                    a.kd_kecamatan,
                                                    a.kd_kelurahan,
                                                    a.kd_blok,
                                                    a.no_urut,
                                                    a.kd_jns_op,
                                                    a.thn_pajak_sppt,
                                                    kd_status
                                                FROM sim_pbb.sppt_penelitian a
                                                    LEFT JOIN SIM_PBB.DATA_BILLING b
                                                        ON a.data_billing_id = b.data_billing_id
                                                WHERE kd_status = '0') aj
                                                ON     aj.kd_propinsi = sppt.kd_propinsi
                                                    AND aj.kd_dati2 = sppt.kd_dati2
                                                    AND aj.kd_kecamatan = sppt.kd_kecamatan
                                                    AND aj.kd_kelurahan = sppt.kd_kelurahan
                                                    AND aj.kd_blok = sppt.kd_blok
                                                    AND aj.no_urut = sppt.no_urut
                                                    AND aj.kd_jns_op = sppt.kd_jns_op
                                                    AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
                                where sppt.thn_pajak_sppt='$tahun'
                                AND aj.thn_pajak_sppt IS NULL
                                and sppt.status_pembayaran_sppt<>'3'
                                group by sppt.kd_kecamatan,sppt.kd_kelurahan) 
                                rekap"),
                function ($join) {
                    $join->on('rekap.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                        ->on('rekap.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
                }
            )

            ->leftJoin(
                db::raw("(select  sppt.kd_kecamatan ,sppt.kd_kelurahan,
                                    count(1) jumlah_objek,
                                sum(pbb_yg_harus_dibayar_sppt) pbb,sum(nvl(nilai_potongan,0)) potongan,sum(pbb_yg_harus_dibayar_sppt-nvl(nilai_potongan,0)) pbb_akhir
                                from sppt 
                                left join sppt_potongan on sppt.kd_propinsi=sppt_potongan.kd_propinsi and
                                sppt.kd_dati2=sppt_potongan.kd_dati2 and
                                sppt.kd_kecamatan=sppt_potongan.kd_kecamatan and
                                sppt.kd_kelurahan=sppt_potongan.kd_kelurahan and
                                sppt.kd_blok=sppt_potongan.kd_blok and
                                sppt.no_urut=sppt_potongan.no_urut and
                                sppt.kd_jns_op=sppt_potongan.kd_jns_op and
                                sppt.thn_pajak_sppt=sppt_potongan.thn_pajak_sppt
                                LEFT JOIN
                                            (SELECT a.kd_propinsi,
                                                    a.kd_dati2,
                                                    a.kd_kecamatan,
                                                    a.kd_kelurahan,
                                                    a.kd_blok,
                                                    a.no_urut,
                                                    a.kd_jns_op,
                                                    a.thn_pajak_sppt,
                                                    kd_status
                                                FROM sim_pbb.sppt_penelitian a
                                                    LEFT JOIN SIM_PBB.DATA_BILLING b
                                                        ON a.data_billing_id = b.data_billing_id
                                                WHERE kd_status = '0') aj
                                                ON     aj.kd_propinsi = sppt.kd_propinsi
                                                    AND aj.kd_dati2 = sppt.kd_dati2
                                                    AND aj.kd_kecamatan = sppt.kd_kecamatan
                                                    AND aj.kd_kelurahan = sppt.kd_kelurahan
                                                    AND aj.kd_blok = sppt.kd_blok
                                                    AND aj.no_urut = sppt.no_urut
                                                    AND aj.kd_jns_op = sppt.kd_jns_op
                                                    AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
                                where sppt.thn_pajak_sppt='$tahun_last'
                                
                                and sppt.status_pembayaran_sppt<>'3'
                                group by sppt.kd_kecamatan,sppt.kd_kelurahan) 
                                rekap_last"),
                // AND aj.thn_pajak_sppt IS NULL
                function ($join) {
                    $join->on('rekap_last.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                        ->on('rekap_last.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
                }
            )
            ->select(db::raw("ref_kecamatan.kd_kecamatan, nm_kecamatan, ref_kelurahan.kd_kelurahan, nm_kelurahan, rekap.jumlah_objek jumlah_objek_now, rekap.pbb pbb_now, rekap.potongan potongan_now, rekap.pbb_akhir pbb_akhir_now, rekap_last.jumlah_objek jumlah_objek_last, rekap_last.pbb pbb_last, rekap_last.potongan potongan_last, rekap_last.pbb_akhir pbb_akhir_last"))
            ->orderByRaw('ref_kecamatan.kd_kecamatan asc,ref_kelurahan.kd_kelurahan asc');
        return $data;
    }
}
