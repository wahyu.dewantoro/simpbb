<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Helpers\Buku;
use App\Helpers\gallade;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use App\Models\Data_billing;
use App\Models\Billing_kolektif;
use App\Models\Temp_Data;
use DOMPDF as PDF;
use PDFMerger;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Dafnom_export;
use App\Jobs\GenerateDafnom;
use PhpOffice\PhpSpreadsheet\Calculation\Statistical\Distributions\F;

class Dafnom
{
    public static function defaultDelayQueue($default = 30)
    {
        return $default;
    }
    public static function defaultQueuename($default = 'dafnom')
    {
        return $default;
    }
    public static function getDafnomData($filter = [], $limit = 500)
    {
        if (!$filter) {
            return false;
        }
        $startYear = 2003;
        $beforeYear = 2020;
        $pbbmax = Buku::getBuku(2)['max'];
        $whereAnd = "";
        if ($filter['blok']) {
            $whereAnd .= "AND so.kd_blok = '" . $filter['blok'] . "'";
        }
        /* $sql="SELECT so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op,so.nm_wp_sppt nm_wp,
                    so.JLN_WP_SPPT alamat,so.THN_PAJAK_SPPT tahun_pajak,
                    sum(pbb_yg_harus_dibayar_sppt) over(order by so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op) totalLoop,
                    so.pbb_yg_harus_dibayar_sppt pbb,
                    COALESCE(so.denda,0) denda
                FROM spo.sppt_oltp@tospo205 so
                WHERE so.status_pembayaran_sppt = 0
                    AND so.THN_PAJAK_SPPT = '".$filter['tahun_pajak']."'
                    AND so.KD_KECAMATAN = '".$filter['kecamatan']."'
                    AND so.KD_KELURAHAN = '".$filter['kelurahan']."'
                    ".$whereAnd."
                    AND so.pbb_yg_harus_dibayar_sppt <= '".$pbbmax."'
                and (so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op,so.THN_PAJAK_SPPT)
                not in (
                    select bk.kd_propinsi,bk.kd_dati2,bk.kd_kecamatan,bk.kd_kelurahan,bk.kd_blok,bk.no_urut,bk.kd_jns_op,bk.TAHUN_PAJAK THN_PAJAK_SPPT
                    from BILLING_KOLEKTIF BK
                    where bk.TAHUN_PAJAK = '".$filter['tahun_pajak']."'
                    AND bk.KD_KECAMATAN = '".$filter['kecamatan']."'
                    AND bk.KD_KELURAHAN = '".$filter['kelurahan']."'
                    AND (BK.DELETED_AT is null or BK.DELETED_AT='')
                )
                ORDER BY so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op
                "; */

        /* $sql="SELECT so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op,so.nm_wp_sppt nm_wp,
                    so.JLN_WP_SPPT alamat,so.THN_PAJAK_SPPT tahun_pajak,
                    sum(pbb_yg_harus_dibayar_sppt+denda) over(order by pbb_yg_harus_dibayar_sppt,so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op) totalLoop,
                    so.pbb_yg_harus_dibayar_sppt pbb,
                     denda
                FROM sppt_oltp@tospo205 so
                WHERE so.status_pembayaran_sppt = 0
                    AND so.THN_PAJAK_SPPT = '".$filter['tahun_pajak']."'
                    AND so.KD_KECAMATAN = '".$filter['kecamatan']."'
                    AND so.KD_KELURAHAN = '".$filter['kelurahan']."'
                    ".$whereAnd."
                    AND so.pbb_yg_harus_dibayar_sppt <= '".$pbbmax."'
                    ORDER BY pbb_yg_harus_dibayar_sppt,so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op
                "; */
        $sql = "SELECT so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op,so.nm_wp_sppt nm_wp,
                    so.JLN_WP_SPPT alamat,so.THN_PAJAK_SPPT tahun_pajak,
                    sum(pbb_yg_harus_dibayar_sppt+denda) over(order by pbb_yg_harus_dibayar_sppt,so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op) totalLoop,
                    so.pbb_yg_harus_dibayar_sppt pbb,
                     denda
                FROM spo.sppt_oltp so
                WHERE so.status_pembayaran_sppt = 0
                    AND so.THN_PAJAK_SPPT = '" . $filter['tahun_pajak'] . "'
                    AND so.KD_KECAMATAN = '" . $filter['kecamatan'] . "'
                    AND so.KD_KELURAHAN = '" . $filter['kelurahan'] . "'
                    " . $whereAnd . "
                    AND so.pbb_yg_harus_dibayar_sppt <= '" . $pbbmax . "'
                    ORDER BY pbb_yg_harus_dibayar_sppt,so.kd_propinsi,so.kd_dati2,so.kd_kecamatan,so.kd_kelurahan,so.kd_blok,so.no_urut,so.kd_jns_op
                ";
        //FETCH FIRST ".$limit." ROWS ONLY  
        if ($filter['rencana_bayar']) {
            $where = ($filter['option'] == '1') ? " subtable.totalloop<=" . $filter['rencana_bayar'] : " subtable.pbb+subtable.denda=" . $filter['rencana_bayar'];
            $sql = "select * from (" . $sql . ") subtable 
            WHERE " . $where . " and rownum <= " . $limit;
        }
        return DB::connection('oracle')->select(db::raw($sql));
    }
    public static function tempNOP($data = [])
    {
        $return = false;
        if ($data && is_array(($data))) {
            $saveTemp = [];
            foreach ($data as $item) {
                $nop = explode(".", $item["nop"]);
                $nopz = explode("-", $nop[4]);
                $saveTemp[] = [
                    'iduser' => Auth()->user()->id,
                    'kd_propinsi' => $nop['0'],
                    'kd_dati2' => $nop['1'],
                    'kd_kecamatan' => $nop['2'],
                    'kd_kelurahan' => $nop['3'],
                    'kd_blok' => $nopz['0'],
                    'no_urut' => $nopz['1'],
                    'kd_jns_op' => $nop['5'],
                    'tahun_pajak' => $item['tahun_pajak']
                ];
            }
            Temp_Data::insert($saveTemp);
            $return = true;
            // $temp_data_billing=new Temp_Data($saveTemp);
            // $temp_data_billing->save();
        }
    }
    public static function getAlltempNOP()
    {

        $hariini = date('Ymd');
        $user = Auth()->user()->id;
        /*   $sql="select distinct * from (SELECT '' no,
                    a.kd_propinsi || '.' || a.kd_dati2 || '.' || a.kd_kecamatan || '.' || a.kd_kelurahan || '.' || a.kd_blok || '-' || a.no_urut || '.' || a.kd_jns_op nop,
                    b.nm_wp_sppt nm_wp,
                    b.jln_wp_sppt alamat,
                    a.tahun_pajak,
                    b.PBB_YG_HARUS_DIBAYAR_SPPT pbb,
                    spo.get_denda( b.kd_dati2,
                        b.kd_kecamatan,
                        b.kd_kelurahan,
                        b.kd_blok,
                        b.no_urut,
                        b.kd_jns_op,
                        b.thn_pajak_sppt,
                        b.pbb_yg_harus_dibayar_sppt,
                        CASE
                            WHEN TO_CHAR (SYSDATE,
                            'yyyy') = '2021' THEN TO_DATE ('30122021',
                            'ddmmyyyy')
                            ELSE tgl_jatuh_tempo_sppt
                        END,
                        TRUNC (to_date('$hariini','yyyymmdd'))) Denda ,
                    CASE
                        WHEN b.thn_pajak_sppt IS NOT NULL THEN 1
                        ELSE 0
                    END data_ada,
                    b.status_pembayaran_sppt lunas_pbb, 
                    CASE WHEN data_billing.kd_status IS null THEN '0' ELSE '1'END billing_kolektif,
                    CASE WHEN data_billing.kd_status='1' THEN 'Masuk dafnom lain ('|| data_billing.kd_propinsi || '.' || data_billing.kd_dati2 || '.' || data_billing.kd_kecamatan || '.' || data_billing.kd_kelurahan || '.' || data_billing.kd_blok || '-' || data_billing.no_urut || '.' || data_billing.kd_jns_op||') - Lunas' ELSE 
                    CASE WHEN data_billing.kd_status='0' THEN 'Masuk dafnom lain ('|| data_billing.kd_propinsi || '.' || data_billing.kd_dati2 || '.' || data_billing.kd_kecamatan || '.' || data_billing.kd_kelurahan || '.' || data_billing.kd_blok || '-' || data_billing.no_urut || '.' || data_billing.kd_jns_op||') - Belum Lunas' ELSE '' END
                    END  keterangan
                FROM temp_data a
                LEFT outer JOIN  spo.sppt_oltp b
                    ON a.kd_propinsi = b.kd_propinsi
                    AND a.kd_dati2 = b.kd_dati2
                    AND a.kd_kecamatan = b.kd_kecamatan
                    AND a.kd_kelurahan = b.kd_kelurahan
                    AND a.kd_blok = b.kd_blok
                    AND a.no_urut = b.no_urut
                    AND a.kd_jns_op = b.kd_jns_op
                    AND a.tahun_pajak = b.thn_pajak_sppt
                    AND  get_buku(b.pbb_yg_harus_dibayar_sppt) IN (1, 2)
                LEFT OUTER JOIN billing_kolektif
                    ON billing_kolektif.kd_kecamatan = a.kd_kecamatan
                    AND billing_kolektif.kd_kelurahan = a.kd_kelurahan
                    AND billing_kolektif.kd_blok = a.kd_blok
                    AND billing_kolektif.no_urut = a.no_urut
                    AND billing_kolektif.kd_jns_op = a.kd_jns_op
                    AND billing_kolektif.tahun_pajak = a.tahun_pajak
                    AND billing_kolektif.deleted_at IS NULL 
                LEFT outer JOIN data_billing 
                    ON data_billing.data_billing_id = billing_kolektif.data_billing_id
                    AND data_billing.deleted_at IS NULL 
                    AND (data_billing.kd_status IS NULL OR data_billing.kd_status = 0)
                WHERE a.iduser = '".$user."'
                ORDER BY a.ID ) 
                ";
 */
        // $sql = "select distinct * from (
        //     SELECT null no,
        //                         a.kd_propinsi || '.' || a.kd_dati2 || '.' || a.kd_kecamatan || '.' || a.kd_kelurahan || '.' || a.kd_blok || '-' || a.no_urut || '.' || a.kd_jns_op nop,
        //                         b.nm_wp_sppt nm_wp,
        //                         b.jln_wp_sppt alamat,
        //                         a.tahun_pajak,
        //                         b.PBB_YG_HARUS_DIBAYAR_SPPT pbb,
        //                         b.denda,
        //                         CASE
        //                             WHEN b.thn_pajak_sppt IS NOT NULL THEN 1
        //                             ELSE 0
        //                         END data_ada,
        //                         b.status_pembayaran_sppt lunas_pbb, 
        //                         data_billing.kd_status,
        //                         CASE WHEN data_billing.kd_status IS null THEN '0' ELSE '1' END billing_kolektif,
        //                         CASE WHEN data_billing.kd_status='1' THEN 'Masuk dafnom lain ('|| data_billing.kd_propinsi || '.' || data_billing.kd_dati2 || '.' || data_billing.kd_kecamatan || '.' || data_billing.kd_kelurahan || '.' || data_billing.kd_blok || '-' || data_billing.no_urut || '.' || data_billing.kd_jns_op||') - Lunas' ELSE 
        //                         CASE WHEN data_billing.kd_status='0' THEN 'Masuk dafnom lain ('|| data_billing.kd_propinsi || '.' || data_billing.kd_dati2 || '.' || data_billing.kd_kecamatan || '.' || data_billing.kd_kelurahan || '.' || data_billing.kd_blok || '-' || data_billing.no_urut || '.' || data_billing.kd_jns_op||') - Belum Lunas' ELSE '' END
        //                         END  keterangan,
        //                         case when data_billing.expired_at <sysdate then ' Kobil Expired ' else '' end keterangan_tambahan 
        //                     FROM temp_data a
        //                     LEFT  JOIN  spo.sppt_oltp b
        //                         ON a.kd_propinsi = b.kd_propinsi
        //                         AND a.kd_dati2 = b.kd_dati2
        //                         AND a.kd_kecamatan = b.kd_kecamatan
        //                         AND a.kd_kelurahan = b.kd_kelurahan
        //                         AND a.kd_blok = b.kd_blok
        //                         AND a.no_urut = b.no_urut
        //                         AND a.kd_jns_op = b.kd_jns_op
        //                         AND a.tahun_pajak = b.thn_pajak_sppt
        //                         AND  b.pbb_terhutang_sppt<=500000
        //                     LEFT  JOIN billing_kolektif
        //                         ON billing_kolektif.kd_kecamatan = a.kd_kecamatan
        //                         AND billing_kolektif.kd_kelurahan = a.kd_kelurahan
        //                         AND billing_kolektif.kd_blok = a.kd_blok
        //                         AND billing_kolektif.no_urut = a.no_urut
        //                         AND billing_kolektif.kd_jns_op = a.kd_jns_op
        //                         AND billing_kolektif.tahun_pajak = a.tahun_pajak
        //                         AND billing_kolektif.deleted_at IS NULL 
        //                         AND billing_kolektif.expired_at > SYSDATE
        //                     LEFT  JOIN data_billing 
        //                         ON data_billing.data_billing_id = billing_kolektif.data_billing_id
        //                         AND data_billing.deleted_at IS NULL 
        //                         AND (
        //                             (    (data_billing.kd_status = 0 or data_billing.kd_status IS NULL )
        //                             AND data_billing.expired_at > SYSDATE)
        //                         OR (    data_billing.kd_status = 1
        //                             AND data_billing.expired_at < SYSDATE)
        //                         )
        //                     WHERE a.iduser = '$user'
        //                     ) order by nop asc
        //                     ";
        // // dd(['z1',$user]);
        // $result=DB::connection('oracle')->select(db::raw($sql));
        $sqlTemp="SELECT
                    DISTINCT *
                FROM
                    (
                    SELECT
                        NULL NO,
                        a.kd_propinsi || '.' || a.kd_dati2 || '.' || a.kd_kecamatan || '.' || a.kd_kelurahan || '.' || a.kd_blok || '-' || a.no_urut || '.' || a.kd_jns_op nop,
                        a.tahun_pajak,
                        data_billing.kd_status,
                        CASE
                            WHEN data_billing.kd_status IS NULL THEN '0'
                            ELSE '1'
                        END billing_kolektif,
                        CASE
                            WHEN data_billing.kd_status = '1' THEN 'Masuk dafnom lain (' || data_billing.kd_propinsi || '.' || data_billing.kd_dati2 || '.' || data_billing.kd_kecamatan || '.' || data_billing.kd_kelurahan || '.' || data_billing.kd_blok || '-' || data_billing.no_urut || '.' || data_billing.kd_jns_op || ') - Lunas'
                            ELSE 
                                                CASE
                                WHEN data_billing.kd_status = '0' THEN 'Masuk dafnom lain (' || data_billing.kd_propinsi || '.' || data_billing.kd_dati2 || '.' || data_billing.kd_kecamatan || '.' || data_billing.kd_kelurahan || '.' || data_billing.kd_blok || '-' || data_billing.no_urut || '.' || data_billing.kd_jns_op || ') - Belum Lunas'
                                ELSE ''
                            END
                        END keterangan,
                        CASE
                            WHEN data_billing.expired_at <sysdate THEN ' Kobil Expired '
                            ELSE ''
                        END keterangan_tambahan
                    FROM
                        temp_data a
                    LEFT JOIN billing_kolektif
                        ON billing_kolektif.kd_kecamatan = a.kd_kecamatan
                        AND billing_kolektif.kd_kelurahan = a.kd_kelurahan
                        AND billing_kolektif.kd_blok = a.kd_blok
                        AND billing_kolektif.no_urut = a.no_urut
                        AND billing_kolektif.kd_jns_op = a.kd_jns_op
                        AND billing_kolektif.tahun_pajak = a.tahun_pajak
                        AND billing_kolektif.deleted_at IS NULL
                        AND billing_kolektif.expired_at > SYSDATE
                    LEFT JOIN data_billing 
                        ON data_billing.data_billing_id = billing_kolektif.data_billing_id
                        AND data_billing.deleted_at IS NULL
                        AND (( (data_billing.kd_status = 0 OR data_billing.kd_status IS NULL )
                            AND data_billing.expired_at > SYSDATE)
                            OR ( data_billing.kd_status = 1 AND data_billing.expired_at < SYSDATE) )
                    WHERE
                        a.iduser = '$user')
                ORDER BY
                    nop ASC";
        $data=DB::select(db::raw($sqlTemp));
        $result=[];
        if(count($data)&&is_array($data)){
            foreach($data as $item){
                $item=(array) $item;
                $nop = explode(".", $item["nop"]);
                $nopz = explode("-", $nop[4]);
                $where="where b.kd_propinsi = '".$nop[0]."'
                                and b.kd_dati2 = '".$nop[1]."'
                                and b.kd_kecamatan = '".$nop[2]."'
                                and b.kd_kelurahan = '".$nop[3]."'
                                and b.kd_blok = '".$nopz[0]."'
                                and b.no_urut = '".$nopz[1]."'
                                and b.kd_jns_op = '".$nop[5]."'
                                and b.thn_pajak_sppt = '".$item['tahun_pajak']."'
                                AND b.pbb_terhutang_sppt <= 500000";
                $sqloltp="SELECT DISTINCT *
                    FROM
                        (
                        SELECT
                            b.nm_wp_sppt nm_wp,
                            b.jln_wp_sppt alamat,
                            b.PBB_YG_HARUS_DIBAYAR_SPPT pbb,
                            b.denda,
                            CASE
                                WHEN b.thn_pajak_sppt IS NOT NULL THEN 1
                                ELSE 0
                            END data_ada,
                            b.status_pembayaran_sppt lunas_pbb
                        FROM spo.sppt_oltp b
                        ".$where.")
                    ORDER BY
                        nm_wp ASC";
                $dataOltp=DB::connection('oracle')->select(db::raw($sqloltp));
                if(count($dataOltp)&&is_array($dataOltp)){
                    $result[]=(array)$item+(array)current($dataOltp);
                    // dd($result);
                }
            }
        }
        return $result;
    }
    public static function getNOPnonKolektif($data = [], $kd_jns_op = '')
    {
        if (!$data && !is_array($data)) {
            $return['nopnonkolektif'] = 0;
            $return['nopkolektif'] = 0;
        }
        $kelurahan = ($data['kelurahan'] && $data['kelurahan'] !== '-') ? "AND vnk.KD_KELURAHAN = '" . $data['kelurahan'] . "'" : "";
        $sql = "SELECT count(vnk.nm_wp_sppt) total
                FROM v_non_kolektif vnk
                WHERE vnk.THN_PAJAK_SPPT = '" . $data['tahun_pajak'] . "'
                    AND vnk.KD_KECAMATAN = '" . $data['kecamatan'] . "' " . $kelurahan;
        $get = DB::connection('oracle')->select(db::raw($sql));
        // ->whereRaw(DB::raw('lhp is null and data_billing.kd_jns_op='.$kd_jns_op.''))
        if (count($get) && is_array($get)) {
            $return['nopnonkolektif'] = gallade::parseQuantity($get[0]->total);
        }
        $where = [
            'billing_kolektif.kd_kecamatan' => $data['kecamatan'],
            'billing_kolektif.tahun_pajak' => $data['tahun_pajak']
        ];
        if ($data['kelurahan'] && $data['kelurahan'] !== '-') {
            $where['billing_kolektif.kd_kelurahan'] = $data['kelurahan'];
        }
        // if ($kd_jns_op && $kd_jns_op !== '-') {
        $where['data_billing.kd_jns_op'] = '1';
        // }
        $total_kobil = Billing_kolektif::where($where)
            ->join('data_billing', 'data_billing.data_billing_id', '=', 'billing_kolektif.data_billing_id')
            ->count();
        $return['nopkolektif'] = gallade::parseQuantity($total_kobil);
        return $return;
    }
    public static function makeExcelDafnom($data = [], $id = false)
    {
        $return = false;
        if ($data && $id) {
            $data = array_merge([['NO', 'NOP', 'NAMA', 'ALAMAT', 'TAHUN PAJAK', ' PBB', 'DENDA', 'TOTAL']], $data);
            $export = new Dafnom_export($data);
            $return = Excel::store($export, 'dafnom_import/' . $id . '.xlsx', 'dafnom_list_pdf');
        }
        return $return;
    }
    public static function makedaftarBillingKolektifPembayaran($id_billing = false)
    {
        if (!$id_billing) {
            return;
        }
        $data_billing = Data_billing::where(['data_billing_id' => $id_billing, 'kd_status' => '1'])->get();
        if (!($data_billing->count() > 0)) {
            return dispatch(new GenerateDafnom(['id_billing' => $id_billing]))
                ->delay(now()->addMinutes(Dafnom::defaultDelayQueue()))
                ->onQueue(Dafnom::defaultQueuename());
        }
        $filename = 'pembayaran/' . $id_billing;
        $isExists = Storage::disk('dafnom_list_pdf')->exists($filename . '.pdf');
        if ($id_billing && $isExists) {
            return $isExists;
        }
        ini_set('memory_limit', '-1');
        $data_billing = $data_billing->first();
        $onJoin = 'billing_kolektif.KD_PROPINSI
        AND psppt.KD_DATI2=billing_kolektif.KD_DATI2
        AND psppt.KD_KECAMATAN=billing_kolektif.KD_KECAMATAN
        AND psppt.KD_KELURAHAN=billing_kolektif.KD_KELURAHAN
        AND psppt.KD_BLOK=billing_kolektif.KD_BLOK
        AND psppt.NO_URUT=billing_kolektif.NO_URUT
        AND psppt.KD_JNS_OP=billing_kolektif.KD_JNS_OP
        AND psppt.THN_PAJAK_SPPT=billing_kolektif.TAHUN_PAJAK';
        $onJoinB = 'billing_kolektif.KD_PROPINSI
        AND spsppt.KD_DATI2=billing_kolektif.KD_DATI2
        AND spsppt.KD_KECAMATAN=billing_kolektif.KD_KECAMATAN
        AND spsppt.KD_KELURAHAN=billing_kolektif.KD_KELURAHAN
        AND spsppt.KD_BLOK=billing_kolektif.KD_BLOK
        AND spsppt.NO_URUT=billing_kolektif.NO_URUT
        AND spsppt.KD_JNS_OP=billing_kolektif.KD_JNS_OP
        AND spsppt.THN_PAJAK_SPPT=billing_kolektif.TAHUN_PAJAK';
        $select = [
            'billing_kolektif.kd_propinsi',
            'billing_kolektif.kd_dati2',
            'billing_kolektif.kd_kecamatan',
            'billing_kolektif.kd_kelurahan',
            'billing_kolektif.kd_kelurahan',
            'billing_kolektif.kd_blok',
            'billing_kolektif.no_urut',
            'billing_kolektif.kd_jns_op',
            'billing_kolektif.tahun_pajak',
            'billing_kolektif.nm_wp',
            'billing_kolektif.pokok',
            'billing_kolektif.denda',
            'billing_kolektif.total',
            'psppt.jln_wp_sppt',
            'spsppt.pengesahan',
            db::raw("case when ref_bank.NAMA_BANK is null then 'BANK JATIM' else ref_bank.NAMA_BANK end nama_bank")
        ];
        $billing_kolektif = Billing_kolektif::where('billing_kolektif.data_billing_id', $id_billing);
        $billing_kolektif = $billing_kolektif->select($select)
            ->leftJoin(db::raw('spo.sppt psppt'), 'psppt.KD_PROPINSI', '=', db::raw($onJoin))
            ->leftJoin(db::raw('spo.pembayaran_sppt spsppt'), 'spsppt.KD_PROPINSI', '=', db::raw($onJoinB))
            ->leftJoin(db::raw('pbb.ref_bank'), 'REF_BANK.KODE_BANK', '=', 'spsppt.KODE_BANK_BAYAR')
            ->get();
        $setGenerate['data_billing'] = $data_billing;
        $setGenerate['uri'] = env("APP_URL", "localhost") . 'e-sppt-ntpd';
        $no = 1;
        $i = 1;
        $count = $billing_kolektif->count();
        $oMerger = PDFMerger::init();
        foreach ($billing_kolektif->toArray() as $item) {
            $setGenerate['billing_kolektif'][] = (object)$item;
            if ($i % 2 == 0 || $count == $i) {
                $setGenerate['noPage'] = (object)[
                    'page' => $no,
                    'total' => ceil($count / 2)
                ];
                $filenametemp = Dafnom::makeKobilBayarPageTemp($setGenerate, $id_billing);
                if ($filenametemp) {
                    $oMerger->addPDF('storage/app/public/dafnom_list_pdf/' . $filenametemp, 'all');
                }
                $setGenerate['billing_kolektif'] = [];
                $no++;
            }
            $i++;
        }
        $oMerger->merge();
        $storage = Storage::disk('dafnom_list_pdf');
        $storage->put($filename . '.pdf', $oMerger->output());
        $storage->deleteDirectory($filename);
    }
    public static function makeKobilBayarPageTemp($data = [], $tempname = 'default')
    {
        if (!$data) {
            return false;
        }
        $list = ['billing_kolektif', 'data_billing', 'uri', 'noPage'];
        $data = Arr::only((array)$data, $list);
        $filename_temp = 'pembayaran/' . $tempname . '/' . $data['noPage']->page . '.pdf';
        $pdf = PDF::loadView('dafnom/_cetak_pembayaran_dafnom', $data)->setPaper('a4', 'portrait');
        Storage::disk('dafnom_list_pdf')->put($filename_temp, $pdf->output());
        return $filename_temp;
    }
    public static function makedaftarBillingKolektif($id_billing = false)
    {
        if (!$id_billing) {
            return;
        }
        $isExists = Storage::disk('dafnom_list_pdf')->exists('dafnom_list/' . $id_billing . '.pdf');
        // dd($isExists);
        if ($isExists) {
            return;
        }
        ini_set('memory_limit', '-1');
        $groupBilling = [
            'data_billing.data_billing_id',
            'data_billing.kd_propinsi',
            'data_billing.kd_dati2',
            'data_billing.kd_kecamatan',
            'data_billing.kd_kelurahan',
            'data_billing.kd_blok',
            'data_billing.no_urut',
            'data_billing.kd_jns_op',
            'data_billing.tahun_pajak',
            'data_billing.nama_wp',
            'data_billing.nama_kecamatan',
            'data_billing.nama_kelurahan',
            'data_billing.created_at',
            'data_billing.expired_at'
        ];
        $select_billing = array_merge($groupBilling, [DB::raw('sum(billing_kolektif.pbb) pbb,sum(billing_kolektif.denda) denda')]);
        $data_billing = Data_billing::where(["data_billing.data_billing_id" => $id_billing])
            ->select($select_billing)
            ->join('billing_kolektif', 'billing_kolektif.data_billing_id', '=', 'data_billing.data_billing_id')
            ->groupBy($groupBilling)
            ->get()->first();
        $onJoin = 'billing_kolektif.KD_PROPINSI
        AND psppt.KD_DATI2=billing_kolektif.KD_DATI2
        AND psppt.KD_KECAMATAN=billing_kolektif.KD_KECAMATAN
        AND psppt.KD_KELURAHAN=billing_kolektif.KD_KELURAHAN
        AND psppt.KD_BLOK=billing_kolektif.KD_BLOK
        AND psppt.NO_URUT=billing_kolektif.NO_URUT
        AND psppt.KD_JNS_OP=billing_kolektif.KD_JNS_OP
        AND psppt.THN_PAJAK_SPPT=billing_kolektif.TAHUN_PAJAK';
        $select = [
            'billing_kolektif.kd_propinsi',
            'billing_kolektif.kd_dati2',
            'billing_kolektif.kd_kecamatan',
            'billing_kolektif.kd_kelurahan',
            'billing_kolektif.kd_kelurahan',
            'billing_kolektif.kd_blok',
            'billing_kolektif.no_urut',
            'billing_kolektif.kd_jns_op',
            'billing_kolektif.tahun_pajak',
            'billing_kolektif.nm_wp',
            'billing_kolektif.pokok',
            'billing_kolektif.denda',
            'billing_kolektif.total',
            'billing_kolektif.pengesahan'
        ];
        $orderBy = DB::raw('billing_kolektif.kd_propinsi,
        billing_kolektif.kd_dati2,
        billing_kolektif.kd_kecamatan,
        billing_kolektif.kd_kelurahan,
        billing_kolektif.kd_blok,
        billing_kolektif.no_urut,
        billing_kolektif.kd_jns_op,
        billing_kolektif.nm_wp');
        $billing_kolektif = Billing_kolektif::where('billing_kolektif.data_billing_id', $id_billing)
            ->select($select)
            ->orderBy($orderBy)
            ->get();
        // return view('dafnom._cetak_dafnom',compact('data_billing','billing_kolektif'));
        $pdf = PDF::loadView('dafnom/_cetak_dafnom', compact('data_billing', 'billing_kolektif'))->setPaper('a4', 'portrait');
        $filename = 'dafnom_list/' . $id_billing . '.pdf';
        return  Storage::disk('dafnom_list_pdf')->put($filename, $pdf->output());
    }
    public static function cekNOP($data)
    {
        $nop = explode(".", $data["nop"]);
        $nopz = explode("-", $nop[4]);
        $sql = "select distinct * from (SELECT case when so.status_pembayaran_sppt='1' then 'Pbb Telah lunas' else 'belum lunas' end keterangan,
        so.status_pembayaran_sppt kd_status,
        so.nm_wp_sppt nm_wp,so.pbb_yg_harus_dibayar_sppt pbb,COALESCE(so.denda,0) denda
        FROM spo.sppt_oltp so
        WHERE so.KD_PROPINSI = '" . $nop['0'] . "'
            AND so.KD_DATI2 = '" . $nop['1'] . "'
            AND so.KD_KECAMATAN = '" . $nop['2'] . "'
            AND so.KD_KELURAHAN = '" . $nop['3'] . "'
            AND so.KD_BLOK = '" . $nopz['0'] . "' 
            AND so.NO_URUT = '" . $nopz['1'] . "' 
            AND so.KD_JNS_OP = '" . $nop['5'] . "' 
            AND so.THN_PAJAK_SPPT = '" . $data['tahun_pajak'] . "'
        UNION 
        SELECT 
            case when db.KD_STATUS ='1' then
        'Masuk dafnom lain ('||db.kd_propinsi||'.'||db.kd_dati2||'.'||db.kd_kecamatan||'.'||db.kd_kelurahan||'.'||db.kd_blok||'-'||db.no_urut||'.'||db.kd_jns_op||' ) - Lunas' else
        'Masuk dafnom lain ('||db.kd_propinsi||'.'||db.kd_dati2||'.'||db.kd_kecamatan||'.'||db.kd_kelurahan||'.'||db.kd_blok||'-'||db.no_urut||'.'||db.kd_jns_op||') - Belum Lunas' 
        end keterangan,
        '1' kd_status, 
        BK.nm_wp,bk.pbb,COALESCE(BK.denda,0) denda
        FROM BILLING_KOLEKTIF BK 
        left join DATA_BILLING db 
            ON db.DATA_BILLING_ID =bk.DATA_BILLING_ID 
        WHERE BK.KD_PROPINSI = '" . $nop['0'] . "' 
            AND BK.KD_DATI2 = '" . $nop['1'] . "' 
            AND BK.KD_KECAMATAN = '" . $nop['2'] . "' 
            AND BK.KD_KELURAHAN = '" . $nop['3'] . "' 
            AND BK.KD_BLOK = '" . $nopz['0'] . "'
            AND BK.NO_URUT = '" . $nopz['1'] . "' 
            AND BK.KD_JNS_OP = '" . $nop['5'] . "' 
            AND BK.TAHUN_PAJAK = '" . $data['tahun_pajak'] . "'
            AND (BK.DELETED_AT is null or BK.DELETED_AT=''))";
        //echo $sql."</br>";
        $data = DB::connection('oracle')->select(db::raw($sql));
        return $data;
    }
    public static function getDenda($nop = false, $thn_pajak = false)
    {
        $return = 0;
        if (!($nop && $thn_pajak)) {
            return $return;
        }
        $nop = explode(".", $nop);
        $nopz = explode("-", $nop[4]);
        $sql = "SELECT  spo.get_denda(kd_dati2, kd_kecamatan,
                    kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,
                    pbb_yg_harus_dibayar_sppt, tgl_jatuh_tempo_sppt, SYSDATE) denda
            FROM sppt_oltp so
            WHERE so.KD_PROPINSI='" . $nop['0'] . "'
            and so.KD_DATI2='" . $nop['1'] . "'
            and so.KD_KECAMATAN='" . $nop['2'] . "'
            and so.KD_KELURAHAN='" . $nop['3'] . "'
            and so.KD_BLOK='" . $nopz['0'] . "'
            and so.NO_URUT='" . $nopz['1'] . "'
            and so.KD_JNS_OP='" . $nop["5"] . "' 
                AND so.thn_pajak_sppt='" . $thn_pajak . "'";
        $data = DB::select(db::raw($sql));
        if (count($data) > 0) {
            $return = Arr::first($data)->denda;
        }
        return $return;
    }
}
