<?php

namespace App\Helpers;

use Illuminate\Support\Arr;

class Buku
{
    public static function getBuku($no=false){
        $setBuku=[
            1=>["min"=>0,"max"=>100000],
            2=>["min"=>100001,"max"=>500000],
            3=>["min"=>500001,"max"=>2000000],
            4=>["min"=>2000001,"max"=>5000000],
            5=>["min"=>5000001]
        ];
        return Arr::only($setBuku,[$no])[$no];
    }
}
