<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Realisasi
{


   public static function targetApbd($tanggal)
   {
      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));
      $sql = "select baku.*,pokok_berjalan,
    denda_berjalan,
    jumlah_berjalan,
    pokok_tunggakan,
    denda_tunggakan,
    jumlah_tunggakan,
    jumlah_tunggakan + jumlah_berjalan total_keseluruhan,
    round(((jumlah_tunggakan + jumlah_berjalan)/target)*100,2) persen_realisasi
    from (
    SELECT TRIM (kd_buku) buku,
             CASE
                WHEN kd_buku = 12 THEN 'Buku I dan II'
                ELSE 'Buku III, IV dan V'
             END
                nm_buku,
             SUM (
                nilai)
                target
        FROM target_realisasi
       WHERE tahun = '" . $tahun . "'
    GROUP BY kd_buku
    ) baku
    left join (
    select buku,sum(pokok_berjalan) pokok_berjalan,
    sum(denda_berjalan) denda_berjalan,
    sum(total_berjalan) jumlah_berjalan,
    sum(pokok_tunggakan) pokok_tunggakan,
    sum(denda_tunggakan) denda_tunggakan,
    sum(total_tunggakan) jumlah_tunggakan
    from ( 
    select buku, 
    case when tahun='" . $tahun . "' then sum(pokok) else 0 end pokok_berjalan,
    case when tahun='" . $tahun . "' then sum(denda) else 0 end denda_berjalan,
    case when tahun='" . $tahun . "' then sum(total) else 0 end total_berjalan,
    case when tahun!='" . $tahun . "' then sum(pokok) else 0 end pokok_tunggakan,
    case when tahun!='" . $tahun . "' then sum(denda) else 0 end denda_tunggakan,
    case when tahun!='" . $tahun . "' then sum(total) else 0 end total_tunggakan
    from (
    SELECT 
    to_char(tgl_terbit_sppt,'yyyy') tahun,
    CASE
       WHEN get_buku (pbb_yg_harus_dibayar_sppt) <= 2 THEN '12'
       ELSE '345'
    END
       buku,
    jml_sppt_yg_dibayar - NVL (denda_sppt, 0) pokok,
    NVL (denda_sppt, 0) denda,
    jml_sppt_yg_dibayar total
FROM pembayaran_sppt
    LEFT JOIN sppt
       ON     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
          AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
          AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
          AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
          AND sppt.kd_blok = pembayaran_sppt.kd_blok
          AND sppt.no_urut = pembayaran_sppt.no_urut
          AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
          AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
      where trunc(tgl_pembayaran_sppt)>=to_date('0101" . $tahun . "','ddmmyyyy')
      and trunc(tgl_pembayaran_sppt)<=to_date('" . $tanggal . "','yyyymmdd')
    ) pivot 
    group by buku,tahun ) pivot_final
    group by buku
    ) real on real.buku=baku.buku order by baku.buku";

      $data = DB::connection('oracle_dua')->select(DB::raw($sql));
      return $data;
   }

   public static function Baku($tanggal)
   {

      $user = Auth()->user();
      $role = $user->roles()->pluck('name')->toarray();
      $kd_kecamatan = "";

      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));


      $uk = $user->unitKerja()->first();
      $akec = "";
      $and = " ";
      if ($uk->kd_kecamatan <> '') {
         $and .= " and a..kd_kecamatan='" . $uk->kd_kecamatan . "' ";
      }

      $akel = "";
      if ($uk->kd_kelurahan <> '') {
         $and .= " and a.kd_kelurahan='" . $uk->kd_kelurahan . "' ";
      }

      $sql = "SELECT CASE WHEN ref_buku.kd_buku <= 2 THEN 'a' ELSE 'b' END jenis,
                     CASE
                        WHEN     ref_buku.kd_buku IS NULL
                           AND (CASE WHEN ref_buku.kd_buku <= 2 THEN 'a' ELSE 'b' END)
                                    IS NOT NULL
                        THEN
                           'Sub Total'
                        WHEN     ref_buku.kd_buku IS NULL
                           AND (CASE WHEN ref_buku.kd_buku <= 2 THEN 'a' ELSE 'b' END)
                                    IS NULL
                        THEN
                           'Total'
                        ELSE
                           TO_CHAR (ref_buku.kd_buku)
                     END
                        buku,
                     SUM (jumlah_op) jumlah_op,
                     SUM (baku) baku,
                     SUM (pokok) pokok,
                     SUM (denda) denda,
                     SUM (jumlah) jumlah,
                     (SUM (baku) - SUM (pokok)) sisa_baku,
                     ROUND (SUM (jumlah) / SUM (baku) * 100, 2) persen_realisasi
                     FROM ref_buku
                     LEFT JOIN
                     (  select kd_buku,count(a.pbb_akhir) jumlah_op,sum(pbb_akhir) baku,
                     sum(pokok) pokok,sum(denda) denda,sum(jumlah) jumlah
                     from sppt_baku a
                     left join sppt_realisasi b on a.kd_propinsi=b.kd_propinsi and
                     a.kd_dati2=b.kd_dati2 and
                     a.kd_kecamatan=b.kd_kecamatan and
                     a.kd_kelurahan=b.kd_kelurahan and
                     a.kd_blok=b.kd_blok and
                     a.no_urut=b.no_urut and
                     a.kd_jns_op=b.kd_jns_op and
                     a.thn_pajak_sppt=b.thn_pajak_sppt and b.tgl_bayar<=to_date('$tanggal','yyyymmdd')
                     where a.thn_pajak_sppt='$tahun'
                     group by kd_buku) real
                        ON ref_buku.kd_buku = real.kd_buku
                     GROUP BY ROLLUP (CASE WHEN ref_buku.kd_buku <= 2 THEN 'a' ELSE 'b' END,
        ref_buku.kd_buku)";

      $timeout = 100;
      $data = Cache::remember(md5($sql), $timeout, function () use ($sql) {
         return DB::connection('oracle_dua')->select(db::raw($sql));
      });

      return $data;
   }

   public static function Harian($tanggal, $kd_kecamatan = '', $kd_kelurahan = '')
   {
      $and = "";
      if ($kd_kecamatan <> '') {
         $and .= "and kd_kecamatan ='$kd_kecamatan' ";
      }

      if ($kd_kelurahan <> '') {
         $and .= "and kd_kelurahan ='$kd_kelurahan' ";
      }

      $tahun = date('Y', strtotime($tanggal));

      $pb = "pbb.pembayaran_sppt";
      /* if ($tanggal == date('Ymd')) {
         $pb = "spo.pembayaran_sppt";
      } */


      $sql = "SELECT CASE
               WHEN     (SELECT nm_kecamatan
                           FROM ref_kecamatan
                           WHERE kd_kecamatan = pivot_final.kd_kecamatan)
                           IS NOT NULL
                     AND buku IS NULL
               THEN
                  'SUB TOTAL'
               ELSE
                  CASE
                     WHEN     (SELECT nm_kecamatan
                                 FROM ref_kecamatan
                                 WHERE kd_kecamatan = pivot_final.kd_kecamatan)
                                 IS NULL
                           AND buku IS NULL
                     THEN
                        'TOTAL'
                     ELSE
                        (SELECT nm_kecamatan
                           FROM ref_kecamatan
                           WHERE kd_kecamatan = pivot_final.kd_kecamatan)
                  END
            END
               kecamatan,
            buku,
            SUM (pokok_berjalan) pokok_berjalan,
            SUM (denda_berjalan) denda_berjalan,
            SUM (jumlah_berjalan) jumlah_berjalan,
            SUM (pokok_tunggakan) pokok_tunggakan,
            SUM (denda_tunggakan) denda_tunggakan,
            SUM (jumlah_tunggakan) jumlah_tunggakan,
            SUM (jumlah_tunggakan + jumlah_berjalan) total_keseluruhan
            FROM (  SELECT kd_kecamatan,
                     buku,
                     CASE WHEN thn_pajak_sppt = '" . $tahun . "' THEN SUM (pokok) ELSE 0 END
                        pokok_berjalan,
                     CASE WHEN thn_pajak_sppt = '" . $tahun . "' THEN SUM (denda) ELSE 0 END
                        denda_berjalan,
                     CASE WHEN thn_pajak_sppt = '" . $tahun . "' THEN SUM (jumlah) ELSE 0 END
                        jumlah_berjalan,
                     CASE WHEN thn_pajak_sppt <> '" . $tahun . "' THEN SUM (pokok) ELSE 0 END
                        pokok_tunggakan,
                     CASE WHEN thn_pajak_sppt <> '" . $tahun . "' THEN SUM (denda) ELSE 0 END
                        denda_tunggakan,
                     CASE
                        WHEN thn_pajak_sppt <> '" . $tahun . "' THEN SUM (jumlah)
                        ELSE 0
                     END
                        jumlah_tunggakan
                  FROM (  SELECT kd_kecamatan,
                                 get_buku (jml_sppt_yg_dibayar - denda_sppt) buku,
                                 thn_pajak_sppt,
                                 SUM (jml_sppt_yg_dibayar - denda_sppt) pokok,
                                 SUM (denda_sppt) denda,
                                 SUM (jml_sppt_yg_dibayar) jumlah
                           FROM " . $pb . "
                           WHERE TRUNC (tgl_pembayaran_sppt) =
                                    TO_DATE ('" . $tanggal . "', 'yyyymmdd')
                        GROUP BY get_buku (jml_sppt_yg_dibayar - denda_sppt),
                                 thn_pajak_sppt,
                                 kd_kecamatan) pivot
            GROUP BY kd_kecamatan, buku, thn_pajak_sppt) pivot_final
            GROUP BY ROLLUP (kd_kecamatan, buku)
            ORDER BY kd_kecamatan ASC, buku ASC";

      return DB::connection("oracle_dua")->select(DB::raw($sql));
   }

   public static function Bulanan($bulan, $kd_kecamatan = '', $kd_kelurahan = '')
   {
      $and = "";
      if ($kd_kecamatan <> '') {
         $and .= "and kd_kecamatan ='$kd_kecamatan' ";
      }

      if ($kd_kelurahan <> '') {
         $and .= "and kd_kelurahan ='$kd_kelurahan' ";
      }

      $tahun = explode('-', $bulan)[1];
      $tanggal = date('Ymd', strtotime('01-' . $bulan));
      $sql = "SELECT 
               buku,
               SUM (pokok_berjalan) pokok_berjalan,
               SUM (denda_berjalan) denda_berjalan,
               SUM (jumlah_berjalan) total_berjalan,
               SUM (pokok_tunggakan) pokok_tunggakan,
               SUM (denda_tunggakan) denda_tunggakan,
               SUM (jumlah_tunggakan) total_tunggakan,
               SUM (jumlah_tunggakan + jumlah_berjalan) total_keseluruhan
            FROM (  SELECT 
                        buku,
                        CASE WHEN thn_pajak_sppt = '" . $tahun . "' THEN SUM (pokok) ELSE 0 END
                           pokok_berjalan,
                        CASE WHEN thn_pajak_sppt = '" . $tahun . "' THEN SUM (denda) ELSE 0 END
                           denda_berjalan,
                        CASE WHEN thn_pajak_sppt = '" . $tahun . "' THEN SUM (jumlah) ELSE 0 END
                           jumlah_berjalan,
                        CASE WHEN thn_pajak_sppt <> '" . $tahun . "' THEN SUM (pokok) ELSE 0 END
                           pokok_tunggakan,
                        CASE WHEN thn_pajak_sppt <> '" . $tahun . "' THEN SUM (denda) ELSE 0 END
                           denda_tunggakan,
                        CASE
                           WHEN thn_pajak_sppt <> '" . $tahun . "' THEN SUM (jumlah)
                           ELSE 0
                        END
                           jumlah_tunggakan
                     FROM (  SELECT kd_kecamatan,
                                    get_buku (jml_sppt_yg_dibayar - denda_sppt) buku,
                                    thn_pajak_sppt,
                                    SUM (jml_sppt_yg_dibayar - denda_sppt) pokok,
                                    SUM (denda_sppt) denda,
                                    SUM (jml_sppt_yg_dibayar) jumlah
                              FROM pembayaran_sppt
                              WHERE TRUNC (tgl_pembayaran_sppt) >=
                                       TO_DATE ('" . $tanggal . "', 'yyyymmdd') and 
                                       TRUNC (tgl_pembayaran_sppt) <=
                                       last_day(TO_DATE ('" . $tanggal . "', 'yyyymmdd')) " . $and . "
                           GROUP BY get_buku (jml_sppt_yg_dibayar - denda_sppt),
                                    thn_pajak_sppt,
                                    kd_kecamatan) pivot
               GROUP BY  buku, thn_pajak_sppt) pivot_final
            GROUP BY ROLLUP ( buku)
            ORDER BY  buku ASC";

      $data = DB::connection('oracle_dua')->select(DB::raw($sql));
      return $data;
   }

   public static function pembayaranHarian($start, $end = "")
   {
      $tanggal = date('d-m-Y', strtotime($start));
      $tanggal_dua = $end <> '' ? date('d-m-Y', strtotime($end)) : $tanggal;

      if ($tanggal == date('d-m-Y')) {
         $table = "pembayaran_sppt a";
      } else {
         $table = "pembayaran_sppt a";
      }

      $data = DB::connection('oracle_satutujuh')->table(DB::raw($table))
         ->select(DB::raw(" a.kd_propinsi
                           || a.kd_dati2
                           || a.kd_kecamatan
                           || a.kd_kelurahan
                           || a.kd_blok
                           || a.no_urut
                           || a.kd_jns_op
                              nop,
                              thn_pajak_sppt,
                           (select nm_wp_sppt from sppt where thn_pajak_sppt=a.thn_pajak_sppt
                              and kd_kecamatan=a.kd_kecamatan
                              and kd_kelurahan=a.kd_kelurahan
                              and kd_blok=a.kd_blok
                              and no_urut=a.no_urut
                              and kd_jns_op=a.kd_jns_op
                           ) nm_wp_sppt,
                           a.tgl_pembayaran_sppt,
                           a.jml_sppt_yg_dibayar - nvl(a.denda_sppt,0) pokok,
                           nvl(a.denda_sppt,0) denda,
                           a.jml_sppt_yg_dibayar total,
                              pengesahan,
                        nama_bank
                        ,(
                           select  kobil ||' / '||aa.tahun_pajak thn_pajak_kobil 
                     from sim_pbb.data_billing aa
                     join sim_pbb.billing_kolektif bb on aa.data_billing_id=bb.data_billing_id
                     where aa.deleted_at is null
                     and bb.kd_propinsi=a.kd_propinsi and
                     bb.kd_dati2=a.kd_dati2 and
                     bb.kd_kecamatan=a.kd_kecamatan and
                     bb.kd_kelurahan=a.kd_kelurahan and
                     bb.kd_blok=a.kd_blok and
                     bb.no_urut=a.no_urut and
                     bb.kd_jns_op=a.kd_jns_op and
                     bb.tahun_pajak=a.thn_pajak_sppt and 
                     trunc(aa.tgl_bayar)=TRUNC(a.tgl_pembayaran_sppt)
                     and aa.kode_bank=a.kode_bank
                     and aa.pengesahan=a.pengesahan  and rownum=1
                           )
                           kobil_tahun"));

      if ($tanggal == date('d-m-Y')) {
         $data = $data->join(DB::raw('pbb.ref_bank b'), function ($join) {
            $join->on('b.kode_bank', '=', DB::raw('trim(a.kode_bank)'));
         });
      } else {
         $data = $data->join(DB::raw('pbb.ref_bank b'), function ($join) {
            $join->on('b.kode_bank', '=', 'a.kode_bank');
         });
      }

      $data = $data->whereraw(DB::raw(" TRUNC (a.tgl_pembayaran_sppt) between TO_DATE ('$tanggal', 'dd-mm-yyyy')  and TO_DATE ('$tanggal_dua', 'dd-mm-yyyy')"));
      return $data;
   }

   public static function DetailKecamatan($tanggal, $buku)
   {
      $user = Auth()->user();
      $role = $user->roles()->pluck('name')->toarray();
      $kd_kecamatan = "";
      if (in_array('Kecamatan', $role)) {
         $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();

         foreach ($kec as $kec) {
            $kd_kecamatan .= " sppt.kd_kecamatan ='" . $kec . "' or";
         }

         $kd_kecamatan = " and ( " . substr($kd_kecamatan, 0, -2) . " )";
      }


      $wb = " ";
      if ($buku <> '') {
         foreach ($buku as $buku) {
            $wb .= " (get_buku(nvl(jml_sppt_yg_dibayar,0) - nvl(denda_sppt,0))='$buku') or";
         }
         $wb = "and ( " . substr($wb, 0, -2) . " )";
      }


      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('d-m-Y', strtotime($tanggal));

      $data = DB::connection('oracle_dua')->select(DB::raw("SELECT real.kd_propinsi, real.kd_dati2, real.kd_kecamatan, real.kd_kelurahan, real.kd_blok, real.no_urut, real.kd_jns_op, real.nm_wp_sppt, real.jln_wp_sppt, real.blok_kav_no_wp_sppt, real.rt_wp_sppt, real.rw_wp_sppt, real.kelurahan_wp_sppt, real.kota_wp_sppt, jalan_op, blok_kav_no_op, rw_op, rt_op, nm_kelurahan nm_kelurahan_op, nm_kecamatan nm_kecamatan_op, SUM (pokok_berjalan) pokok_berjalan, SUM (denda_berjalan) denda_berjalan, SUM (total_berjalan) total_berjalan, SUM (pokok_tunggakan) pokok_tunggakan, SUM (denda_tunggakan) denda_tunggakan, SUM (total_tunggakan) total_tunggakan FROM ( SELECT sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.nm_wp_sppt, sppt.jln_wp_sppt, sppt.blok_kav_no_wp_sppt, sppt.rt_wp_sppt, sppt.rw_wp_sppt, sppt.kelurahan_wp_sppt, sppt.kota_wp_sppt, CASE WHEN sppt.thn_pajak_sppt = " . $tahun . " THEN nvl(jml_sppt_yg_dibayar,0) - nvl(denda_sppt,0) ELSE 0 END pokok_berjalan, CASE WHEN sppt.thn_pajak_sppt < " . $tahun . " AND sppt.thn_pajak_sppt >= 2003 THEN nvl(jml_sppt_yg_dibayar,0) - nvl(denda_sppt,0) ELSE 0 END pokok_tunggakan, CASE WHEN sppt.thn_pajak_sppt = $tahun THEN nvl(denda_sppt,0) ELSE 0 END denda_berjalan, CASE WHEN sppt.thn_pajak_sppt < " . $tahun . " AND sppt.thn_pajak_sppt >= 2003 THEN nvl(denda_sppt,0) ELSE 0 END denda_tunggakan, CASE WHEN sppt.thn_pajak_sppt = " . $tahun . " THEN nvl(jml_sppt_yg_dibayar,0) ELSE 0 END total_berjalan, CASE WHEN sppt.thn_pajak_sppt < " . $tahun . " AND sppt.thn_pajak_sppt >= 2003 THEN nvl(jml_sppt_yg_dibayar,0) ELSE 0 END total_tunggakan FROM sppt JOIN  
     pembayaran_sppt ON sppt.kd_propinsi = pembayaran_sppt.kd_propinsi AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2 AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan AND sppt.kd_blok = pembayaran_sppt.kd_blok AND sppt.no_urut = pembayaran_sppt.no_urut AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op AND sppt.thn_pajak_sppt = PEMBAYARAN_SPPT.THN_PAJAK_SPPT 
            WHERE tgl_pembayaran_sppt >=to_date('" . $tanggal . "','dd-mm-yyyy') AND tgl_pembayaran_sppt < to_date('" . $tanggal . "','dd-mm-yyyy')+1
            " . $kd_kecamatan . " " . $wb . "
            GROUP BY sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.nm_wp_sppt, sppt.jln_wp_sppt, sppt.blok_kav_no_wp_sppt, sppt.rt_wp_sppt, sppt.rw_wp_sppt, sppt.kelurahan_wp_sppt, sppt.kota_wp_sppt, sppt.thn_pajak_sppt, jml_sppt_yg_dibayar - denda_sppt, denda_sppt, jml_sppt_yg_dibayar) real 
        join dat_objek_pajak dop on real.kd_propinsi=dop.kd_propinsi and real.kd_dati2=dop.kd_dati2 and real.kd_kecamatan=dop.kd_kecamatan and real.kd_kelurahan=dop.kd_kelurahan and real.kd_blok=dop.kd_blok and real.no_urut=dop.no_urut and real.kd_jns_op=dop.kd_jns_op 
        join ref_kecamatan on ref_kecamatan.kd_kecamatan=real.kd_kecamatan 
        join ref_kelurahan on ref_kelurahan.kd_kecamatan=real.kd_kecamatan and real.kd_kelurahan=ref_kelurahan.kd_kelurahan 
                    GROUP BY real.kd_propinsi, real.kd_dati2, real.kd_kecamatan, real.kd_kelurahan, real.kd_blok, real.no_urut, real.kd_jns_op, real.nm_wp_sppt, real.jln_wp_sppt, real.blok_kav_no_wp_sppt, real.rt_wp_sppt, real.rw_wp_sppt, real.kelurahan_wp_sppt, real.kota_wp_sppt, jalan_op, blok_kav_no_op, rw_op, rt_op, nm_kecamatan, nm_kelurahan"));

      return $data;
   }

   public static function RangkingKecamatan($tanggal, $buku)
   {
      $wa = " ";
      $wb = " ";

      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));
      $start = $tahun . '0101';

      switch ($buku) {
         case '1':
            # buku 1,2
            $wa .= " and a.kd_buku in ('1','2') ";
            break;
         case '2':
            # buku 1,2
            $wa .= " and a.kd_buku in ('3','4','5') ";
            break;
         default:
            # code...
            $wa = " ";
            break;
      }


      $core = DataPBB::sqlCoreBaku($tanggal);

      $sql = "SELECT a.kd_kecamatan,
                     nm_kecamatan,
                     tanggal_lunas,
                     count(1) op,
                     SUM (pbb - potongan) baku,
                     SUM (bayar) pokok,
                     SUM (denda) denda,
                     sum(bayar+denda) realisasi,
                     round(SUM (bayar)/SUM (pbb - potongan)  *100,3) persen_realisasi      
               FROM (" . $core . ") a
                              join ref_kecamatan b on a.kd_Kecamatan=b.kd_kecamatan
                              LEFT JOIN sim_pbb.kecamatan_lunas
                              ON     kecamatan_lunas.kd_kecamatan = b.kd_kecamatan
                                 AND tanggal_lunas BETWEEN TO_DATE ('$start', 'yyyymmdd')
                                                       AND TO_DATE ('$tanggal', 'yyyymmdd')
                     where  koreksi = 0          
                     " . $wa . "
               GROUP BY a.kd_kecamatan,nm_kecamatan,tanggal_lunas
               order by round(SUM (bayar)/SUM (pbb - potongan)  *100,3) desc,tanggal_lunas asc nulls last";
      $data = DB::connection('oracle_satutujuh')->select(db::raw($sql));
      return $data;

      /* $wa = " ";
      $wb = " ";

      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));
      if ($buku <> '') {
         foreach ($buku as $buku) {
            $wa .= " (buku='$buku') or";
            $wb .= " (get_buku(jml_sppt_yg_dibayar - denda_sppt)='$buku') or";
         }

         $wa = " ( " . substr($wa, 0, -2) . " )";
         $wb = " ( " . substr($wb, 0, -2) . " )";
      }


      $sql = "SELECT (SELECT nm_kecamatan
                  FROM ref_kecamatan
                  WHERE kd_kecamatan = baku.kd_kecamatan)
                  nm_kecamatan,
               baku.*,
               NVL (pokok, 0) pokok,
               NVL (denda, 0) denda,
               NVL (total, 0) realisasi,
               CASE WHEN ROUND (NVL (total, 0) / baku * 100, 3)>100 THEN
                           100
                  ELSE
                           ROUND (NVL (total, 0) / baku * 100, 3)
                  end
                  persen_realisasi
         FROM (SELECT mv_tmp_realisasi.kd_kecamatan, COUNT (1) op, SUM (pbb) baku
                  FROM mv_tmp_realisasi
                  LEFT JOIN
                           (SELECT a.kd_propinsi,
                                   a.kd_dati2,
                                   a.kd_kecamatan,
                                   a.kd_kelurahan,
                                   a.kd_blok,
                                   a.no_urut,
                                   a.kd_jns_op,
                                   a.thn_pajak_sppt,
                                   kd_status
                              FROM sim_pbb.sppt_penelitian a
                                   LEFT JOIN SIM_PBB.DATA_BILLING b
                                      ON a.data_billing_id = b.data_billing_id
                             WHERE kd_status = '0') aj
                              ON     aj.kd_propinsi = mv_tmp_realisasi.kd_propinsi
                                 AND aj.kd_dati2 = mv_tmp_realisasi.kd_dati2
                                 AND aj.kd_kecamatan = mv_tmp_realisasi.kd_kecamatan
                                 AND aj.kd_kelurahan = mv_tmp_realisasi.kd_kelurahan
                                 AND aj.kd_blok = mv_tmp_realisasi.kd_blok
                                 AND aj.no_urut = mv_tmp_realisasi.no_urut
                                 AND aj.kd_jns_op = mv_tmp_realisasi.kd_jns_op
                                 AND aj.thn_pajak_sppt = mv_tmp_realisasi.thn_pajak_sppt
                  WHERE mv_tmp_realisasi.thn_pajak_sppt = '" . $tahun . "' 
                  and aj.thn_pajak_sppt is null
                  and status_pembayaran_sppt <>'3'
                  AND " . $wa . "
         group by mv_tmp_realisasi.kd_Kecamatan
         ) baku
         left join (
         select mv_tmp_realisasi.kd_kecamatan,sum(pokok) pokok,sum(denda) denda,sum(jumlah) total
         from mv_tmp_realisasi
         LEFT JOIN
                           (SELECT a.kd_propinsi,
                                   a.kd_dati2,
                                   a.kd_kecamatan,
                                   a.kd_kelurahan,
                                   a.kd_blok,
                                   a.no_urut,
                                   a.kd_jns_op,
                                   a.thn_pajak_sppt,
                                   kd_status
                              FROM sim_pbb.sppt_penelitian a
                                   LEFT JOIN SIM_PBB.DATA_BILLING b
                                      ON a.data_billing_id = b.data_billing_id
                             WHERE kd_status = '0') aj
                              ON     aj.kd_propinsi = mv_tmp_realisasi.kd_propinsi
                                 AND aj.kd_dati2 = mv_tmp_realisasi.kd_dati2
                                 AND aj.kd_kecamatan = mv_tmp_realisasi.kd_kecamatan
                                 AND aj.kd_kelurahan = mv_tmp_realisasi.kd_kelurahan
                                 AND aj.kd_blok = mv_tmp_realisasi.kd_blok
                                 AND aj.no_urut = mv_tmp_realisasi.no_urut
                                 AND aj.kd_jns_op = mv_tmp_realisasi.kd_jns_op
                                 AND aj.thn_pajak_sppt = mv_tmp_realisasi.thn_pajak_sppt
         where mv_tmp_realisasi.thn_pajak_sppt='" . $tahun . "'
         and aj.thn_pajak_sppt is null
         and tgl_bayar<=to_date('" . $tanggal . "','yyyymmdd')
         AND " . $wa . "
         group by mv_tmp_realisasi.kd_Kecamatan
         ) real on baku.kd_kecamatan=real.kd_kecamatan
         order by persen_realisasi desc,(select tanggal_lunas 
         from sim_pbb.kecamatan_lunas
         where tahun_pajak='" . $tahun . "'
         and kd_Kecamatan=baku.kd_kecamatan ) asc,realisasi desc";

      $data = DB::connection('oracle_satutujuh')->select(db::raw($sql));
      return $data; */
   }

   public static function RangkingKelurahan($tanggal, $buku)
   {
      $wa = " ";
      $wb = " ";

      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));
      $start = $tahun . '0101';

      switch ($buku) {
         case '1':
            # buku 1,2
            $wa .= " and a.kd_buku in ('1','2') ";
            break;
         case '2':
            # buku 1,2
            $wa .= " and a.kd_buku in ('3','4','5') ";
            break;
         default:
            # code...
            $wa = " ";
            break;
      }


      $core = DataPBB::sqlCoreBaku($tanggal);

      $sql = "SELECT a.kd_kecamatan,
      nm_kecamatan,
       a.kd_kelurahan,
       nm_kelurahan,
       count(1) op,
       SUM (pbb - potongan) baku,
       SUM (bayar) pokok,
       SUM (denda) denda,
       sum(bayar+denda) realisasi,
        round(SUM (bayar)/SUM (pbb - potongan)  *100,3) persen_realisasi  ,tanggal_lunas    
  FROM (" . $core . ") a
               join ref_kecamatan b on a.kd_Kecamatan=b.kd_kecamatan
join ref_kelurahan c on c.kd_kecamatan=a.kd_kecamatan and c.kd_kelurahan=a.kd_kelurahan
left join sim_pbb.desa_lunas dl on dl.kd_kecamatan=a.kd_kecamatan and dl.kd_kelurahan=a.kd_kelurahan and dl.tahun_pajak='$tahun'
       where  koreksi = 0          
        " . $wa . "
GROUP BY a.kd_kecamatan, a.kd_kelurahan,nm_kecamatan,nm_kelurahan,tanggal_lunas
order by round(SUM (bayar)/SUM (pbb - potongan)  *100,3) desc,tanggal_lunas asc";
      $data = DB::connection('oracle_satutujuh')->select(db::raw($sql));
      return $data;
   }

   public static function RealisasiTunggakan($tanggal, $scope)
   {
      $tahun = date('Y', strtotime($tanggal));
      $tanggal = date('Ymd', strtotime($tanggal));
      $akhir = $tahun - 1;
      $awal = $akhir - 4;

      $kec = $scope['kd_kecamatan'] ?? '';
      $kel = $scope['kd_kelurahan'] ?? '';
      $wh = "";
      if ($kec <> '') {
         $wh .= " and sppt.kd_kecamatan='$kec'";
      }

      if ($kel <> '') {
         $wh .= " and sppt.kd_kelurahan='$kel'";
      }


      if ($kec <> '' && $kel <> '') {
         $wh = " and get_buku (
        pbb_yg_harus_dibayar_sppt
      - NVL (nilai_potongan, 0)) in (1,2)";
      }

      $sql = "SELECT jenis,
             CASE
                WHEN a.buku IS NULL AND jenis IS NOT NULL THEN 'SUB TOTAL'
                WHEN a.buku IS NULL AND jenis IS NULL THEN 'TOTAL'
                ELSE TO_CHAR (a.buku)
             END
                buku,
             SUM (JUMLAH_OP_BERJALAN) JUMLAH_OP_BERJALAN,
             SUM (JUMLAH_OP_TUNGGAKAN) JUMLAH_OP_TUNGGAKAN,
             SUM (JUMLAH_BAKU_BERJALAN) BAKU_BERJALAN,
             SUM (JUMLAH_BAKU_TUNGGAKAN) BAKU_TUNGGAKAN,
             SUM (REAL_POKOK_BERJALAN) REAL_POKOK_BERJALAN,
             SUM (REAL_POKOK_TUNGGAKAN) REAL_POKOK_TUNGGAKAN,
             SUM (REAL_DENDA_BERJALAN) REAL_DENDA_BERJALAN,
             SUM (REAL_DENDA_TUNGGAKAN) REAL_DENDA_TUNGGAKAN,
             SUM (REAL_JUMLAH_BERJALAN) REAL_JUMLAH_BERJALAN,
             SUM (REAL_JUMLAH_TUNGGAKAN) REAL_JUMLAH_TUNGGAKAN
        FROM (  SELECT buku,
                       SUM (CASE WHEN is_tunggakan = '0' THEN jumlah_op ELSE 0 END)
                          jumlah_op_berjalan,
                       SUM (CASE WHEN is_tunggakan = '1' THEN jumlah_op ELSE 0 END)
                          jumlah_op_tunggakan,
                       SUM (CASE WHEN is_tunggakan = '0' THEN baku ELSE 0 END)
                          jumlah_baku_berjalan,
                       SUM (CASE WHEN is_tunggakan = '1' THEN baku ELSE 0 END)
                          jumlah_baku_tunggakan,
                       SUM (CASE WHEN is_tunggakan = '0' THEN pokok ELSE 0 END)
                          real_pokok_berjalan,
                       SUM (CASE WHEN is_tunggakan = '1' THEN pokok ELSE 0 END)
                          real_pokok_tunggakan,
                       SUM (CASE WHEN is_tunggakan = '0' THEN denda ELSE 0 END)
                          real_denda_berjalan,
                       SUM (CASE WHEN is_tunggakan = '1' THEN denda ELSE 0 END)
                          real_denda_tunggakan,
                       SUM (CASE WHEN is_tunggakan = '0' THEN jumlah ELSE 0 END)
                          real_jumlah_berjalan,
                       SUM (CASE WHEN is_tunggakan = '1' THEN jumlah ELSE 0 END)
                          real_jumlah_tunggakan
                  FROM (  SELECT buku,
                                 is_tunggakan,
                                 COUNT (1) jumlah_op,
                                 SUM (
                                    CASE
                                       WHEN bayar < pbb AND bayar > 0 THEN bayar
                                       ELSE pbb
                                    END)
                                    baku,
                                 SUM (
                                    CASE
                                       WHEN bayar < pbb OR pbb = bayar THEN bayar
                                       ELSE pbb
                                    END)
                                    pokok,
                                 SUM (
                                    CASE WHEN bayar > pbb THEN bayar - pbb ELSE 0 END)
                                    denda,
                                 SUM (bayar) jumlah
                            FROM (SELECT sppt.kd_propinsi,
                                         sppt.kd_dati2,
                                         sppt.kd_kecamatan,
                                         sppt.kd_kelurahan,
                                         sppt.kd_blok,
                                         sppt.no_urut,
                                         sppt.kd_jns_op,
                                         CASE
                                            WHEN sppt.thn_pajak_sppt = '" . $tahun . "' THEN 0
                                            ELSE 1
                                         END
                                            is_tunggakan,
                                         get_buku (pbb_yg_harus_dibayar_sppt)
                                            buku,
                                           pbb_yg_harus_dibayar_sppt
                                         - NVL (nilai_potongan, 0)
                                            pbb,
                                         NVL (
                                            (SELECT jml_sppt_yg_dibayar
                                               FROM pembayaran_sppt
                                              WHERE     kd_propinsi = sppt.kd_propinsi
                                                    AND kd_dati2 = sppt.kd_dati2
                                                    AND kd_kecamatan =
                                                           sppt.kd_kecamatan
                                                    AND kd_kelurahan =
                                                           sppt.kd_kelurahan
                                                    AND kd_blok = sppt.kd_blok
                                                    AND no_urut = sppt.no_urut
                                                    AND kd_jns_op = sppt.kd_jns_op
                                                    AND thn_pajak_sppt =sppt.thn_pajak_sppt
                                                    and tgl_pembayaran_sppt <=TO_DATE ('$tanggal', 'yyyymmdd')
                                                    AND ROWNUM = 1),
                                            0)
                                            bayar
                                    FROM sppt
                                         LEFT JOIN sppt_potongan
                                            ON     sppt.kd_propinsi =
                                                      sppt_potongan.kd_propinsi
                                               AND sppt.kd_dati2 =
                                                      sppt_potongan.kd_dati2
                                               AND sppt.kd_kecamatan =
                                                      sppt_potongan.kd_kecamatan
                                               AND sppt.kd_kelurahan =
                                                      sppt_potongan.kd_kelurahan
                                               AND sppt.kd_blok = sppt_potongan.kd_blok
                                               AND sppt.no_urut = sppt_potongan.no_urut
                                               AND sppt.kd_jns_op =
                                                      sppt_potongan.kd_jns_op
                                               AND sppt.thn_pajak_sppt =
                                                      sppt_potongan.thn_pajak_sppt
                                   WHERE     status_pembayaran_sppt IN ('0', '1')
                                         AND sppt.thn_pajak_sppt >= '" . $awal . "'
                                         AND sppt.thn_pajak_sppt <= '" . $tahun . "'
                                         $wh
                                 ) core_rpt
                        GROUP BY buku, is_tunggakan) pivot
              GROUP BY buku, is_tunggakan) a
              JOIN (SELECT '1' buku, 'a' jenis FROM DUAL
                   UNION
                   SELECT '2' buku, 'a' jenis FROM DUAL
                   UNION
                   SELECT '3' buku, 'b' jenis FROM DUAL
                   UNION
                   SELECT '4' buku, 'b' jenis FROM DUAL
                   UNION
                   SELECT '5' buku, 'b' jenis FROM DUAL) b
                ON a.buku = b.buku
    GROUP BY ROLLUP (jenis, a.buku)";

      // $data = DB::connection('oracle_dua')->select(db::raw($sql));
      $timeout = 100;
      $data = Cache::remember(md5($sql), $timeout, function () use ($sql) {
         return DB::connection('oracle_dua')->select(db::raw($sql));
      });
      return $data;
   }
   public static function rekapHarian($ta, $tb)
   {
      $sql = DB::connection('oracle_satutujuh')->table(db::raw("(
         select tanggal,case when kode_bank='114' then sum(jumlah) else 0 end BJ,case when kode_bank='991' then sum(jumlah) else 0 end POS 
         from (
         select  trunc(tgl_pembayaran_sppt) tanggal,kode_bank,sum(jml_sppt_yg_dibayar) jumlah 
         from pembayaran_sppt
         where trunc(tgl_pembayaran_sppt)>=to_date('" . $ta . "','yyyymmdd')
         and trunc(tgl_pembayaran_sppt)<=to_date('" . $tb . "','yyyymmdd')
         group by  trunc(tgl_pembayaran_sppt),kode_bank
         )
         group by tanggal,kode_bank
         )"))->select(DB::raw("tanggal,sum(bj) bj,sum(pos) pos,sum(bj+pos) jumlah "))
         ->groupBy("tanggal")
         ->orderBy('tanggal');

      return $sql;

      /* return DB::connection('oracle_satutujuh')->select(db::raw("select tanggal,sum(bj) bj,sum(pos) pos,sum(bj+pos) jumlah 
      from (
      select tanggal,case when kode_bank='114' then sum(jumlah) else 0 end BJ,case when kode_bank='991' then sum(jumlah) else 0 end POS 
      from (
      select  trunc(tgl_pembayaran_sppt) tanggal,kode_bank,sum(jml_sppt_yg_dibayar) jumlah 
      from pembayaran_sppt
      where trunc(tgl_pembayaran_sppt)>=to_date('" . $ta . "','yyyymmdd')
      and trunc(tgl_pembayaran_sppt)<=to_date('" . $tb . "','yyyymmdd')
      group by  trunc(tgl_pembayaran_sppt),kode_bank
      )
      group by tanggal,kode_bank
      )
      group by tanggal
      order by tanggal")); */
   }
}
