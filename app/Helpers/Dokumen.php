<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    public static function list(){
        $result=[
            'FC KTP/SIM dan KK'=>'FC KTP/SIM dan KK',
            'SPOP dan LSPOP'=>'SPOP dan LSPOP',
            'Petok D'=>'Petok D',
            'FC akte jual beli/hibah'=>'FC akte jual beli/hibah',
            'FC Sertifikat / Kepemilikan'=>'FC Sertifikat / Kepemilikan',
            'Asli SPPT'=>'Asli SPPT',
            'Asli BBPD / Bukti Lunas'=>'Asli BBPD / Bukti Lunas',
            'FC IMB'=>'FC IMB',
            'FC SK Pensiun / Veteran'=>'FC SK Pensiun / Veteran',
            'FC SPPT / SSPD / Bukti lunas'=>'FC SPPT / SSPD / Bukti lunas',
            'FC SK Pengurangan'=>'FC SK Pengurangan',
            'FC SK Keberatan'=>'FC SK Keberatan',
            'FC SSPD BPHTB'=>'FC SSPD BPHTB',
            'Lain - lain'=>'Lain - lain',
            'NPWP/NPWPD'=>'NPWP/NPWPD',
            'Pengajuan Permohonan'=>'Pengajuan Permohonan',
            'Rincian Penghasil'=>'Rincian Penghasil',
            'Surat Kuasa'=>'Surat Kuasa',
            'SKTM'=>'SKTM',
            'Sket tanah'=>'Sket tanah',
            'Sket Lurah'=>'Sket Lurah',
            'SK Cagar budaya'=>'SK Cagar budaya',
        ];
        return $result;
    }
}
