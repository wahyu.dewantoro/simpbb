<?php

namespace App\Helpers;

use App\RptPiutang;
use App\Sppt;
use Illuminate\Support\Facades\DB;

class Piutang
{

    public static function Sql($kd_kecamatan = '', $kd_kelurahan = '', $tahun = '', $status_objek = '')
    {

        $where = " ";
        if ($kd_kecamatan <> '') {
            $where .= " and sppt.kd_kecamatan='$kd_kecamatan' ";
        }
        if ($kd_kelurahan <> '') {
            $where .= " and sppt.kd_kelurahan='$kd_kelurahan' ";
        }
        if ($tahun <> '') {
            $where .= " and sppt.thn_pajak_sppt='$tahun' ";
        }

        if ($status_objek <> '') {
            if ($status_objek == '1') {
                $where .= " and jns_bumi in ('1','2','3')";
            } else {
                $where .= " and jns_bumi not in ('1','2','3')";
            }
        }


        $tmp_table = "(SELECT    is_aktif,kd_propinsi
                        || '.'
                        || kd_dati2
                        || '.'
                        || kd_kecamatan
                        || '.'
                        || kd_kelurahan
                        || '.'
                        || kd_blok
                        || '.'
                        || no_urut
                        || '.'
                        || kd_jns_op
                        NOP ,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,
                                    kd_blok,no_urut,kd_jns_op,
                        nm_kecamatan,
                        nm_kelurahan,
                        alamat_op,
                        nm_wp_sppt,
                        alamat_wp,
                        status_pembayaran_sppt,
                        thn_pajak_sppt tahun_pajak,
                        pbb,
                        bayar,
                        pbb - bayar kurang_bayar
                    FROM (  SELECT sppt.kd_propinsi,
                                sppt.kd_dati2,
                                sppt.kd_kecamatan,
                                sppt.kd_kelurahan,
                                sppt.kd_blok,
                                sppt.no_urut,
                                sppt.kd_jns_op,
                                sppt.thn_pajak_sppt,
                                nm_kecamatan,
                                nm_kelurahan,
                                sppt.nm_wp_sppt,
                                    jln_wp_sppt
                                || ' '
                                || blok_kav_no_wp_sppt
                                || ' RT'
                                || rt_wp_sppt
                                || ' RW'
                                || rw_wp_sppt
                                || ' '
                                || kelurahan_wp_sppt
                                || ' '
                                || kota_wp_sppt
                                    alamat_wp,
                                    jalan_op
                                || ' '
                                || blok_kav_no_op
                                || ' RT '
                                || rt_op
                                || ' RW '
                                || rw_op
                                    alamat_op,
                                    status_pembayaran_sppt,
                                    case when jns_bumi in ('1','2','3') then '1' else '0' end is_aktif,
                                pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb,
                                SUM (NVL (jml_sppt_yg_dibayar, 0) - NVL (denda_sppt, 0)) bayar
                            FROM sppt
                                JOIN dat_objek_pajak
                                    ON     dat_objek_pajak.kd_propinsi = sppt.kd_propinsi
                                        AND dat_objek_pajak.kd_dati2 = sppt.kd_dati2
                                        AND dat_objek_pajak.kd_kecamatan = sppt.kd_kecamatan
                                        AND dat_objek_pajak.kd_kelurahan = sppt.kd_kelurahan
                                        AND dat_objek_pajak.kd_blok = sppt.kd_blok
                                        AND dat_objek_pajak.no_urut = sppt.no_urut
                                        AND dat_objek_pajak.kd_jns_op = sppt.kd_jns_op
                                        join dat_op_bumi on dat_objek_pajak.kd_propinsi = dat_op_bumi.kd_propinsi and 
                                        dat_objek_pajak.kd_dati2 = dat_op_bumi.kd_dati2 and 
                                        dat_objek_pajak.kd_kecamatan = dat_op_bumi.kd_kecamatan and 
                                        dat_objek_pajak.kd_kelurahan = dat_op_bumi.kd_kelurahan and 
                                        dat_objek_pajak.kd_blok = dat_op_bumi.kd_blok and 
                                        dat_objek_pajak.no_urut = dat_op_bumi.no_urut and 
                                        dat_objek_pajak.kd_jns_op = dat_op_bumi.kd_jns_op
                                JOIN ref_kecamatan
                                    ON ref_kecamatan.kd_kecamatan =
                                        dat_objek_pajak.kd_kecamatan
                                JOIN ref_kelurahan
                                    ON     ref_kelurahan.kd_kecamatan =
                                            dat_objek_pajak.kd_kecamatan
                                        AND ref_kelurahan.kd_kelurahan =
                                            dat_objek_pajak.kd_kelurahan
                                LEFT JOIN sppt_potongan
                                    ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                                        AND sppt.kd_dati2 = sppt_potongan.kd_dati2
                                        AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
                                        AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
                                        AND sppt.kd_blok = sppt_potongan.kd_blok
                                        AND sppt.no_urut = sppt_potongan.no_urut
                                        AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
                                        AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
                                LEFT JOIN sppt_koreksi
                                    ON     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                        AND sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                        AND sppt.kd_kecamatan = sppt_koreksi.kd_kecamatan
                                        AND sppt.kd_kelurahan = sppt_koreksi.kd_kelurahan
                                        AND sppt.kd_blok = sppt_koreksi.kd_blok
                                        AND sppt.no_urut = sppt_koreksi.no_urut
                                        AND sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                        AND sppt.thn_pajak_sppt = sppt_koreksi.thn_pajak_sppt
                                LEFT JOIN pembayaran_sppt
                                    ON     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
                                        AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                        AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
                                        AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
                                        AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                        AND sppt.no_urut = pembayaran_sppt.no_urut
                                        AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                        AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
                            WHERE sppt_koreksi.kd_propinsi IS NULL 
                                    " . $where . "
                        GROUP BY sppt.kd_propinsi,
                                sppt.kd_dati2,
                                sppt.kd_kecamatan,
                                sppt.kd_kelurahan,
                                sppt.kd_blok,
                                sppt.no_urut,
                                sppt.kd_jns_op,
                                sppt.thn_pajak_sppt,
                                sppt.nm_wp_sppt,
                                sppt.jln_wp_sppt,
                                sppt.blok_kav_no_wp_sppt,
                                sppt.rt_wp_sppt,
                                sppt.rw_wp_sppt,
                                sppt.kelurahan_wp_sppt,
                                sppt.kota_wp_sppt,
                                jalan_op,
                                blok_kav_no_op,
                                rt_op,
                                rw_op,
                                nm_kecamatan,
                                nm_kelurahan,
                                case when jns_bumi in ('1','2','3') then '1' else '0' end,
                                status_pembayaran_sppt,
                                pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)) tmp
                WHERE bayar = 0) tmp";
        return $tmp_table;
    }

    public static function PerNop($kd_kecamatan = '', $kd_kelurahan = '', $tahun = '', $status_objek = '')
    {
        return[];
        /*  $tab = self::sql($kd_kecamatan, $kd_kelurahan, $tahun, $status_objek);

        return DB::connection('oracle_satutujuh')->table(db::raw($tab))
            ->selectraw(" nop,status_pembayaran_sppt,nm_kecamatan,nm_kelurahan,alamat_op,nm_wp_sppt,alamat_wp,is_aktif,tahun_pajak,pbb,bayar,kurang_bayar"); */
    }


    public static function Rekap($kd_kecamatan = '', $tahun = '')
    {
        $and = "";
        if ($kd_kecamatan <> '') {
            $and = " and v_nop_piutang.kd_kecamatan='$kd_kecamatan'";
        }

        $sql = "select v_nop_piutang.kd_propinsi,v_nop_piutang.kd_dati2,v_nop_piutang.kd_kecamatan,nm_kecamatan,tahun_piutang,sum(ketetapan) ketetapan,sum(pengurang) pengurang,sum(pokok_terbayar) pokok_terbayar,sum(koreksi_piutang) koreksi_piutang 
        from v_nop_piutang
        join ref_kecamatan on v_nop_piutang.kd_kecamatan=ref_kecamatan.kd_kecamatan
        where tgl_terbit_sppt between to_date('0101" . $tahun . "','ddmmyyyy') 
        and last_day(to_date('0112" . $tahun . "','ddmmyyyy'))
        " . $and . "
        group by v_nop_piutang.kd_propinsi,v_nop_piutang.kd_dati2,v_nop_piutang.kd_kecamatan,tahun_piutang,nm_kecamatan";

        return DB::connection("oracle_satutujuh")->table(db::raw("(" . $sql . ") rekap"));
    }


    public static function rekapDesa($turut)
    {

        return [];
    }

    public static function rekapKecamatan($turut, $buku = '3,4,5')
    {

        return [];
    }

    public static function detailDesa($kd_kecamatan = '', $kd_kelurahan = '', $turut = 5)
    {

        return [];
    }

    public static function detailKecamatan($kd_kecamatan = '', $turut = 5, $buku = '3,4,5')
    {

        return [];
    }


    public static function piutangTahun($tahun)
    {
        return [];
    }

    public static function listKurangBayar()
    {

        return [];
    }
    public static function bukuBesar($tahun)
    {
        /*         $sql = "SELECT fix_a.kd_propinsi,
        fix_a.kd_dati2,
        fix_a.kd_kecamatan,
        fix_a.kd_kelurahan,
        fix_a.kd_blok,
        fix_a.no_urut,
        fix_a.kd_jns_op,
        nm_kecamatan,
        nm_kelurahan,
        jalan_op,
        blok_kav_no_op,
        rt_op,
        rw_op,
        nm_wp,
        jalan_wp,
        blok_kav_no_wp,
        rt_wp,
        rw_wp,
        kelurahan_wp,
        kota_wp,";
        $sqla = "";
        for ($ai = $tahun; $ai >= 2014; $ai--) {
            $sqla .= "nvl(pbb_" . $ai . ",0) pbb_" . $ai . " ,";
        }
        $sql .= substr($sqla, 0, -1);

        $sql .= "FROM (  SELECT kd_propinsi,
                  kd_dati2,
                  kd_kecamatan,
                  kd_kelurahan,
                  kd_blok,
                  no_urut,
                  kd_jns_op,";

        $sqlaa = "";
        for ($aia = $tahun; $aia >= 2014; $aia--) {
            $sqlaa .= "sum(pbb_" . $aia . ") pbb_" . $aia . " ,";
        }
        $sql .= substr($sqlaa, 0, -1);

        $sql .= "FROM (  SELECT kd_propinsi,
                            kd_dati2,
                            kd_kecamatan,
                            kd_kelurahan,
                            kd_blok,
                            no_urut,
                            kd_jns_op,";

        $sql_ = "";
        for ($i = $tahun; $i >= 2014; $i--) {
            $sql_ .= "CASE WHEN thn_pajak_sppt = " . $i . " THEN pbb ELSE NULL END pbb_" . $i . " ,";
        }
        $sql_ = substr($sql_, 0, -1);
        $sql .= $sql_;
        $sql .= "FROM (SELECT sppt.kd_propinsi,
                                    sppt.kd_dati2,
                                    sppt.kd_kecamatan,
                                    sppt.kd_kelurahan,
                                    sppt.kd_blok,
                                    sppt.no_urut,
                                    sppt.kd_jns_op,
                                    sppt.thn_pajak_sppt,
                                      pbb_yg_harus_dibayar_sppt
                                    - NVL (nilai_potongan, 0)
                                       pbb
                               FROM sppt
                                    LEFT JOIN sppt_potongan
                                       ON     sppt.kd_propinsi =
                                                 sppt_potongan.kd_propinsi
                                          AND sppt.kd_dati2 =
                                                 sppt_potongan.kd_dati2
                                          AND sppt.kd_kecamatan =
                                                 sppt_potongan.kd_kecamatan
                                          AND sppt.kd_kelurahan =
                                                 sppt_potongan.kd_kelurahan
                                          AND sppt.kd_blok = sppt_potongan.kd_blok
                                          AND sppt.no_urut = sppt_potongan.no_urut
                                          AND sppt.kd_jns_op =
                                                 sppt_potongan.kd_jns_op
                                          AND sppt.thn_pajak_sppt =
                                                 sppt_potongan.thn_pajak_sppt
                              WHERE     sppt.thn_pajak_sppt >= 2014
                                    AND status_pembayaran_sppt = '0') a
                   GROUP BY kd_propinsi,
                            kd_dati2,
                            kd_kecamatan,
                            kd_kelurahan,
                            kd_blok,
                            no_urut,
                            kd_jns_op,
                            thn_pajak_sppt,
                            pbb) b
         GROUP BY kd_propinsi,
                  kd_dati2,
                  kd_kecamatan,
                  kd_kelurahan,
                  kd_blok,
                  no_urut,
                  kd_jns_op) fix_a
        JOIN dat_objek_pajak
           ON     fix_a.kd_propinsi = dat_objek_pajak.kd_propinsi
              AND fix_a.kd_dati2 = dat_objek_pajak.kd_dati2
              AND fix_a.kd_kecamatan = dat_objek_pajak.kd_kecamatan
              AND fix_a.kd_kelurahan = dat_objek_pajak.kd_kelurahan
              AND fix_a.kd_blok = dat_objek_pajak.kd_blok
              AND fix_a.no_urut = dat_objek_pajak.no_urut
              AND fix_a.kd_jns_op = dat_objek_pajak.kd_jns_op
        JOIN dat_subjek_pajak
           ON dat_subjek_pajak.subjek_pajak_id =
                 dat_objek_pajak.subjek_pajak_id
        JOIN ref_kecamatan ON ref_kecamatan.kd_kecamatan = fix_a.kd_kecamatan
        JOIN ref_kelurahan
           ON     ref_kelurahan.kd_kecamatan = fix_a.kd_Kecamatan
              AND ref_kelurahan.kd_kelurahan = fix_a.kd_kelurahan
            WHERE ";
        $sql_ = "";
        for ($i = $tahun; $i >= 2014; $i--) {
            $sql_ .= " pbb_" . $tahun . " > 500000 or";
        }
        $sql .= substr($sql_, 0, -2);

        $sql .= "ORDER BY fix_a.kd_propinsi,
        fix_a.kd_dati2,
        fix_a.kd_kecamatan,
        fix_a.kd_kelurahan,
        fix_a.kd_blok,
        fix_a.no_urut,
        fix_a.kd_jns_op";

        return DB::connection("oracle_satutujuh")->select(db::raw($sql)); */
        return [];
    }

    public static function sqlCore($tanggal)
    {
        $sql = "SELECT sppt.kd_propinsi,
                                sppt.kd_dati2,
                                sppt.kd_kecamatan,
                                sppt.kd_kelurahan,
                                sppt.kd_blok,
                                sppt.no_urut,
                                sppt.kd_jns_op,
                                sppt.thn_pajak_sppt tahun_pajak,
                                sppt.tgl_terbit_sppt,
                                pbb_yg_harus_dibayar_sppt pbb,
                                NVL (nilai_potongan, 0) potongan,
                                CASE
                                WHEN (CASE
                                            WHEN bayar >
                                                    pbb_yg_harus_dibayar_sppt
                                                    - NVL (nilai_potongan, 0)
                                            THEN
                                                pbb_yg_harus_dibayar_sppt
                                            - NVL (nilai_potongan, 0)
                                            ELSE
                                            NVL (bayar, 0)
                                        END) > 0
                                THEN
                                    0
                                ELSE
                                    CASE
                                        WHEN    jns_koreksi IN ('3')
                                            OR sppt_pembatalan.thn_pajak_sppt IS NOT NULL
                                        THEN
                                            pbb_yg_harus_dibayar_sppt
                                            - NVL (nilai_potongan, 0)
                                        ELSE
                                            0
                                    END
                                END
                                koreksi,
                                CASE
                                WHEN bayar >
                                        pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                                THEN
                                    pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                                ELSE
                                    NVL (bayar, 0)
                                END
                                bayar
                        FROM sppt
                                LEFT JOIN sppt_potongan
                                ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                                    AND sppt.kd_dati2 = sppt_potongan.kd_dati2
                                    AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
                                    AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
                                    AND sppt.kd_blok = sppt_potongan.kd_blok
                                    AND sppt.no_urut = sppt_potongan.no_urut
                                    AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
                                    AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
                                LEFT JOIN sppt_koreksi
                                ON     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                    AND sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                    AND sppt.kd_kecamatan = sppt_koreksi.kd_kecamatan
                                    AND sppt.kd_kelurahan = sppt_koreksi.kd_kelurahan
                                    AND sppt.kd_blok = sppt_koreksi.kd_blok
                                    AND sppt.no_urut = sppt_koreksi.no_urut
                                    AND sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                    AND sppt.thn_pajak_sppt = sppt_koreksi.thn_pajak_sppt
                                    AND sppt_koreksi.jns_koreksi = '3'
                                    AND TRUNC (sppt_koreksi.tgl_surat) <=
                                            TO_DATE ('$tanggal', 'ddmmyyyy')
                                LEFT JOIN
                                (SELECT DISTINCT kd_propinsi,
                                                kd_dati2,
                                                kd_kecamatan,
                                                kd_kelurahan,
                                                kd_blok,
                                                no_urut,
                                                kd_jns_op,
                                                thn_pajak_sppt
                                FROM sppt_pembatalan
                                WHERE     sppt_pembatalan.tgl_mulai <=
                                            TO_DATE ('$tanggal', 'ddmmyyyy')
                                        AND sppt_pembatalan.tgl_selesai >=
                                            TO_DATE ('$tanggal', 'ddmmyyyy'))
                                sppt_pembatalan
                                ON     sppt.kd_propinsi = sppt_pembatalan.kd_propinsi
                                    AND sppt.kd_dati2 = sppt_pembatalan.kd_dati2
                                    AND sppt.kd_kecamatan = sppt_pembatalan.kd_kecamatan
                                    AND sppt.kd_kelurahan = sppt_pembatalan.kd_kelurahan
                                    AND sppt.kd_blok = sppt_pembatalan.kd_blok
                                    AND sppt.no_urut = sppt_pembatalan.no_urut
                                    AND sppt.kd_jns_op = sppt_pembatalan.kd_jns_op
                                    AND sppt.thn_pajak_sppt = sppt_pembatalan.thn_pajak_sppt
                                LEFT JOIN
                                (  SELECT kd_propinsi,
                                        kd_dati2,
                                        kd_kecamatan,
                                        kd_kelurahan,
                                        kd_blok,
                                        no_urut,
                                        kd_jns_op,
                                        thn_pajak_sppt,
                                        SUM (
                                            NVL (jml_sppt_yg_dibayar, 0) - NVL (denda_sppt, 0))
                                            bayar
                                    FROM pembayaran_sppt
                                    WHERE TRUNC (tgl_pembayaran_sppt) <=
                                            TO_DATE ('$tanggal', 'ddmmyyyy')
                                GROUP BY kd_propinsi,
                                        kd_dati2,
                                        kd_kecamatan,
                                        kd_kelurahan,
                                        kd_blok,
                                        no_urut,
                                        kd_jns_op,
                                        thn_pajak_sppt) pembayaran_sppt
                                ON     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
                                    AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                    AND sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
                                    AND sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
                                    AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                    AND sppt.no_urut = pembayaran_sppt.no_urut
                                    AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                    AND sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
                        WHERE     TRUNC (sppt.tgl_terbit_sppt) <=
                                    TO_DATE ('$tanggal', 'ddmmyyyy')
                                AND TRUNC (sppt.tgl_terbit_sppt) >=
                                    TO_DATE ('01012003', 'ddmmyyyy')
                                AND sppt.tgl_terbit_sppt IS NOT NULL";
        return $sql;
    }

    public static function RekapPerTahun($tanggal)
    {
        $from = "(" . self::sqlCore($tanggal) . ") tmp";
        $result = DB::connection("oracle_satutujuh")->table(DB::raw($from))
            ->select(db::raw("TO_CHAR (tgl_terbit_sppt, 'yyyy') tahun_piutang,
                        SUM (pbb) pbb,
                        SUM (potongan) potongan,
                        SUM (bayar) bayar,
                        SUM (koreksi) koreksi,
                        SUM (pbb - potongan - bayar - koreksi) saldo"))
            ->groupByRaw("TO_CHAR (tgl_terbit_sppt, 'yyyy')")
            ->orderByRaw("TO_CHAR (tgl_terbit_sppt, 'yyyy') ASC");
        return $result;
    }
}
