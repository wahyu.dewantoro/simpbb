<?php

namespace App\Helpers;

use App\Models\PendataanBatch;
use App\Models\PendataanObjek;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PembatalanObjek
{
    public static function get_username($user_id)
    {
        $user = DB::table('users')->where('userid', $user_id)->first();
        return (isset($user->username) ? $user->username : '');
    }



    public static function NoTransaksi($kode, $kd_kategori = '02')
    {

        return DB::connection("oracle_satutujuh")->table("sppt_koreksi")
            ->selectraw("'" . $kode . $kd_kategori . "'||to_char(sysdate,'yymmdd')||lpad(case when  max(cast(substr(trim(no_transaksi),11,5) as number)) is null then 1 else max(cast(substr(trim(no_transaksi),11,5) as number))+1 end,5,0) no_transaksi")
            ->whereraw("kd_kategori='$kd_kategori' and trunc(created_at)=trunc(sysdate)")
            ->first()
            ->no_transaksi;

        /* return  DB::connection("oracle_satutujuh")->table("sppt_koreksi")
            ->select(db::raw("'" . $kode . "'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'" . $kode . "'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
            ->whereraw("no_transaksi like '" . $kode . "'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi; */
    }



    public static function prosesPendaataan($nop, $tipe = 'PD')
    {
        $kd_propinsi = $nop['kd_propinsi'];
        $kd_dati2 = $nop['kd_dati2'];
        $kd_kecamatan = $nop['kd_kecamatan'];
        $kd_kelurahan = $nop['kd_kelurahan'];
        $kd_blok = $nop['kd_blok'];
        $no_urut = $nop['no_urut'];
        $kd_jns_op = $nop['kd_jns_op'];
        $userid = user()->id ?? '1700';
        $now = Carbon::now();
        $keterangan = KategoriIventarisasi()[$nop['jns_koreksi']];
        $jns_pendataan = '5';

        $batchData = [
            'jenis_pendataan_id' => $jns_pendataan,
            'tipe' => $tipe,
            'verifikasi_at' => $now,
            'verifikasi_by' => $userid,
            'verifikasi_deskripsi' => $keterangan
        ];
        $batch = PendataanBatch::create($batchData);
        $jns_bumi = $nop['jns_koreksi'] == '04' ? '4' : '5';


        $batal = DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,substr(A.SUBJEK_PAJAK_ID,1,16) SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,'" . $jns_bumi . "' JNS_BUMI FROM DAT_OBJEK_PAJAK A
            left JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
            left JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
            WHERE a.kd_propinsi='$kd_propinsi'
            and a.kd_dati2='$kd_dati2'
            AND  a.kd_kecamatan='$kd_kecamatan'
            AND a.kd_kelurahan='$kd_kelurahan'
            AND a.kd_blok='$kd_blok'
            AND a.no_urut='$no_urut'
            AND a.kd_jns_op='$kd_jns_op' "))[0];

        $status_wp = $nop['jns_koreksi'] == '03' ? '5' : $batal->kd_status_wp;
        $nop_asal = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
        $dat_obj = [
            'pendataan_batch_id' => $batch->id,
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' =>  $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'nop_asal' => onlyNumber($nop_asal),
            'subjek_pajak_id' => $batal->subjek_pajak_id ?? '',
            'nm_wp' => $batal->nm_wp ?? '',
            'jalan_wp' => $batal->jalan_wp ?? '',
            'blok_kav_no_wp' => $batal->blok_kav_no_wp ?? '',
            'rw_wp' => padding(($batal->rw_wp ?? ''), 0, 2),
            'rt_wp' => padding(($batal->rt_wp ?? ''), 0, 3),
            'kelurahan_wp' => ($batal->kelurahan_wp ?? ''),
            'kecamatan_wp' => '',
            'propinsi_wp' => '',
            'kota_wp' => $batal->kota_wp != '' ? substr($batal->kota_wp, 0, 30) : '',
            'kd_pos_wp' => ($batal->kd_pos_wp ?? ''),
            'telp_wp' => ($batal->telp_wp ?? ''),
            'npwp' => null,
            'status_pekerjaan_wp' => ($batal->status_pekerjaan_wp ?? ''),
            'no_persil' => ($batal->no_persil ?? ''),
            'jalan_op' => ($batal->jalan_op ?? ''),
            'blok_kav_no_op' => ($batal->blok_kav_no_op ?? ''),
            'rw_op' => padding(($batal->rw_op ?? ''), 0, 2),
            'rt_op' => padding(($batal->rt_op ?? ''), 0, 3),
            'kd_status_cabang' =>  null,
            'kd_status_wp' =>  $status_wp,
            'kd_znt' =>  $batal->kd_znt,
            'luas_bumi' => $batal->luas_bumi,
            'jns_bumi' => $batal->jns_bumi,
            'created_at' => $now,
            'created_by' => $userid,
            'updated_at' => $now,
            'updated_by' => $userid
        ];

        $objek = PendataanObjek::create($dat_obj);

        $spop = [];
        $nop_asal = onlyNumber($objek->nop_asal);
        $jtr = 2;
        $nop_proses = $nop_asal;

        $data = [
            'jenis_pendataan_id' => $objek->batch->jenis_pendataan_id,
            'pendataan_objek_id' => $objek->id,
            'nop_proses' => $nop_proses,
            'nop_asal' => $nop_asal,
            'jns_transaksi' => $jtr,
            'jalan_op' => $objek->jalan_op,
            'blok_kav_no_op' => $objek->blok_kav_no_op,
            'rt_op' => $objek->rt_op,
            'rw_op' => $objek->rw_op,
            'no_persil' => $objek->no_persil,
            'luas_bumi' => $objek->luas_bumi,
            'kd_znt' => $objek->kd_znt,
            'jns_bumi' => $objek->jns_bumi,
            'jml_bng' => $objek->jml_bng,
            'subjek_pajak_id' => $objek->subjek_pajak_id != '' ? $objek->subjek_pajak_id : substr($nop_asal, 2, 16),
            'nm_wp' => $objek->nm_wp <> '' ? $objek->nm_wp : 'WAJIB PAJAK',
            'jalan_wp' => $objek->jalan_wp <> '' ? $objek->jalan_wp : '-',
            'blok_kav_no_wp' => $objek->blok_kav_no_wp,
            'rt_wp' => $objek->rt_wp,
            'rw_wp' => $objek->rw_wp,
            'kelurahan_wp' => $objek->kelurahan_wp,
            'kecamatan_wp' => $objek->kecamatan_wp,
            'kota_wp' => $objek->kota_wp,
            'propinsi_wp' => $objek->propinsi_wp,
            'kd_pos_wp' => $objek->kd_pos_wp,
            'status_pekerjaan_wp' => $objek->status_pekerjaan_wp <> '' ? $objek->status_pekerjaan_wp : '5',
            'kd_status_wp' => $objek->kd_status_wp,
            'npwp' => $objek->npwp,
            'telp_wp' => $objek->telp_wp,
        ];

        $data['created_by'] = $objek->created_by;
        $data['created_at'] = db::raw('sysdate');
        $data['keterangan'] = $keterangan;

        $req = new \Illuminate\Http\Request();
        $req->merge($data);
        $res = Pajak::prosesPendataan($req);
        return $objek;
    }

    public static function getTunggakan($nop)
    {
        $kd_propinsi = $nop['kd_propinsi'];
        $kd_dati2 = $nop['kd_dati2'];
        $kd_kecamatan = $nop['kd_kecamatan'];
        $kd_kelurahan = $nop['kd_kelurahan'];
        $kd_blok = $nop['kd_blok'];
        $no_urut = $nop['no_urut'];
        $kd_jns_op = $nop['kd_jns_op'];
        $tunggakan = DB::connection("oracle_satutujuh")->table(db::raw("sppt"))
            ->leftjoin('sppt_koreksi', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            })
            ->leftjoin(DB::raw("(SELECT DISTINCT kd_propinsi,
            kd_dati2,
            kd_kecamatan,
            kd_kelurahan,
            kd_blok,
            no_urut,
            kd_jns_op,
            thn_pajak_sppt
FROM sppt_pembatalan
WHERE     sppt_pembatalan.tgl_mulai <=trunc(sysdate)
   AND sppt_pembatalan.tgl_selesai >=trunc(sysdate))  sppt_pembatalan"), function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_pembatalan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_pembatalan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_pembatalan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_pembatalan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_pembatalan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_pembatalan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_pembatalan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_pembatalan.thn_pajak_sppt');
            })
            ->leftjoin(db::raw("spo.sppt_oltp oltp"), function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'oltp.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'oltp.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'oltp.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'oltp.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'oltp.kd_blok')
                    ->on('sppt.no_urut', '=', 'oltp.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'oltp.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'oltp.thn_pajak_sppt');
            })
            ->select(db::raw("sppt.kd_propinsi,
                                sppt.kd_dati2,
                                sppt.kd_kecamatan,
                                sppt.kd_kelurahan,
                                sppt.kd_blok,
                                sppt.no_urut,
                                sppt.kd_jns_op,
                                sppt.thn_pajak_sppt,jns_koreksi,sppt.status_pembayaran_sppt,oltp.pbb_yg_harus_dibayar_sppt"))
            ->whereraw("sppt.kd_propinsi='$kd_propinsi' and 
                            sppt.kd_dati2='$kd_dati2' and 
                            sppt.kd_kecamatan='$kd_kecamatan' and 
                            sppt.kd_kelurahan='$kd_kelurahan' and 
                            sppt.kd_blok='$kd_blok' and 
                            sppt.no_urut='$no_urut' and 
                            sppt.kd_jns_op='$kd_jns_op' and 
                            sppt.status_pembayaran_sppt!='1' and 
                            sppt_koreksi.thn_pajak_sppt is null ")->get();
        // sppt_pembatalan.thn_pajak_sppt is null and 
        return $tunggakan;
    }

    public static function penyesuaian($postData)
    {
        $kd_propinsi = $postData['kd_propinsi'];
        $kd_dati2 = $postData['kd_dati2'];
        $kd_kecamatan = $postData['kd_kecamatan'];
        $kd_kelurahan = $postData['kd_kelurahan'];
        $kd_blok = $postData['kd_blok'];
        $no_urut = $postData['no_urut'];
        $kd_jns_op = $postData['kd_jns_op'];
        $keterangan = $postData['keterangan'] ?? KategoriIventarisasi()[$postData['jns_koreksi']];

        try {
            //code...
            DB::beginTransaction();

            // proses pendataan untuk penon aktifan
            // $objek = self::prosesPendaataan($postData);

            $now = Carbon::now();
            $no_trx = self::NoTransaksi('IV', $postData['jns_koreksi']);
            foreach ($postData['tahun'] as $tahun) {

                // cek sppt_koreksi
                $cs = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                    ->select(db::raw("count(1) jm"))
                    ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                                and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                                and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")->first();

                if ($cs->jm > 0) {
                    DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                        ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                                    and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                                    and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")
                        ->delete();
                }
                $ins = [
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_pajak_sppt' => $tahun,
                    'tgl_surat' => new Carbon($postData['tgl_surat']),
                    'no_surat' => $postData['no_surat'],
                    'jns_koreksi' => '2',
                    'keterangan' => $keterangan,
                    'no_transaksi' => $no_trx,
                    'created_at' => $now,
                    'created_by' => Auth()->user()->id,
                    'nm_kategori' => $keterangan,
                    'kd_kategori' => $postData['jns_koreksi'],
                    'verifikasi_kode' => 1,
                    'verifikasi_keterangan' => $keterangan,
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_at' => $now
                ];
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);

                DB::connection("oracle_satutujuh")->table("sppt")
                    ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                        and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                        and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' 
                        and status_pembayaran_sppt!='1'")
                    ->update(['tgl_terbit_sppt' => null]);
            }


            DB::commit();
            $msg = "berhasil di proses";
            $status = "1";
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th);
            DB::rollBack();
            $msg = $th->getMessage();
            $status = "0";

            $dd = null;
        }
        $respon = [
            'status' => $status,
            'msg' => $msg
        ];
        return response($respon, 200);
    }


    public static function iventarisasi($postData)
    {

        // return $postData;
        $tahun = $postData['tahun'] ?? date('Y');
        $kd_propinsi = $postData['kd_propinsi'];
        $kd_dati2 = $postData['kd_dati2'];
        $kd_kecamatan = $postData['kd_kecamatan'];
        $kd_kelurahan = $postData['kd_kelurahan'];
        $kd_blok = $postData['kd_blok'];
        $no_urut = $postData['no_urut'];
        $kd_jns_op = $postData['kd_jns_op'];
        $keterangan = $postData['keterangan'] ?? KategoriIventarisasi()[$postData['jns_koreksi']];

        try {
            //code...
            DB::beginTransaction();

            // proses pendataan untuk penon aktifan
            self::prosesPendaataan($postData);

            // cek sppt tahun berjalan
            $cst = DB::connection("oracle_satutujuh")
                ->table("sppt")->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op
                ])
                ->whereRaw("status_pembayaran_sppt not in ('1','3') and   thn_pajak_sppt<= $tahun")
                ->get();

            if ($cst->count() > 0) {

                $now = Carbon::now();
                $no_trx = self::NoTransaksi('IV', $postData['jns_koreksi']);
                foreach ($cst as $rr) {
                    $ck = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                        ->where([
                            'kd_propinsi' => $rr->kd_propinsi,
                            'kd_dati2' => $rr->kd_dati2,
                            'kd_kecamatan' => $rr->kd_kecamatan,
                            'kd_kelurahan' => $rr->kd_kelurahan,
                            'kd_blok' => $rr->kd_blok,
                            'no_urut' => $rr->no_urut,
                            'kd_jns_op' => $rr->kd_jns_op,
                            'thn_pajak_sppt' => $rr->thn_pajak_sppt
                        ])
                        ->first();
                    if ($ck == null) {

                        $ins = [
                            'kd_propinsi' => $rr->kd_propinsi,
                            'kd_dati2' => $rr->kd_dati2,
                            'kd_kecamatan' => $rr->kd_kecamatan,
                            'kd_kelurahan' => $rr->kd_kelurahan,
                            'kd_blok' => $rr->kd_blok,
                            'no_urut' => $rr->no_urut,
                            'kd_jns_op' => $rr->kd_jns_op,
                            'thn_pajak_sppt' => $rr->thn_pajak_sppt,
                            'tgl_surat' => new Carbon($postData['tgl_surat']),
                            'no_surat' => $postData['no_surat'],
                            'jns_koreksi' => '2',
                            'keterangan' => $keterangan,
                            'no_transaksi' => $no_trx,
                            'created_at' => $now,
                            'created_by' => Auth()->user()->id??null,
                            'nm_kategori' => $keterangan,
                            'kd_kategori' => $postData['jns_koreksi'],
                            'verifikasi_kode' => 1,
                            'verifikasi_keterangan' => $keterangan,
                            'verifikasi_by' => auth()->user()->id??null,
                            'verifikasi_at' => $now
                        ];
                        DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);

                        DB::connection("oracle_satutujuh")->table("sppt")
                            ->whereraw("thn_pajak_sppt='" . $rr->thn_pajak_sppt . "' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                        and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                        and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' 
                        and status_pembayaran_sppt!='1'")
                            ->update(['status_pembayaran_sppt' => '3']);
                    }
                }
            }

            DB::connection("oracle_satutujuh")->table("sppt")
                ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' 
                and status_pembayaran_sppt!='1'")
                ->update(['tgl_terbit_sppt' => null, 'status_pembayaran_sppt' => '3']);

            DB::commit();
            $msg = "berhasil di proses";
            $status = "1";
        } catch (\Throwable $th) {
            DB::rollBack();
            // Log::info($th);
            $msg = $th->getMessage();
            $status = "0";
        }
        $respon = [
            'status' => $status,
            'msg' => $msg
        ];
        return response($respon, 200);
    }

    public static function cancel($postData)
    {
        $kd_propinsi = $postData['kd_propinsi'];
        $kd_dati2 = $postData['kd_dati2'];
        $kd_kecamatan = $postData['kd_kecamatan'];
        $kd_kelurahan = $postData['kd_kelurahan'];
        $kd_blok = $postData['kd_blok'];
        $no_urut = $postData['no_urut'];
        $kd_jns_op = $postData['kd_jns_op'];

        $tahun = $postData['tahun'] ?? date('Y');
        $keterangan = $postData['keterangan'] ?? KategoriIventarisasi()[$postData['jns_koreksi']];
        // return $tunggakan;
        try {
            //code...
            DB::connection("oracle_satutujuh")->beginTransaction();
            $objek = self::prosesPendaataan($postData);

            $dd = [];
            $wh = "";
            $tunggakan = self::getTunggakan($postData);
            $pembatalan = [];
            foreach ($tunggakan as $key => $value) {
                # code...
                if (!in_array($value->jns_koreksi, ['1', '3'])) {
                    $thn_pajak_sppt = $value->thn_pajak_sppt;
                    $var['kd_propinsi'] = $kd_propinsi;
                    $var['kd_dati2'] = $kd_dati2;
                    $var['kd_kecamatan'] = $kd_kecamatan;
                    $var['kd_kelurahan'] = $kd_kelurahan;
                    $var['kd_blok'] = $kd_blok;
                    $var['no_urut'] = $no_urut;
                    $var['kd_jns_op'] = $kd_jns_op;
                    $var['thn_pajak_sppt'] = $thn_pajak_sppt;
                    $wh .= " ( sppt.kd_propinsi='" . $kd_propinsi . "' and 
                    sppt.kd_dati2='" . $kd_dati2 . "' and 
                    sppt.kd_kecamatan='" . $kd_kecamatan . "' and 
                    sppt.kd_kelurahan='" . $kd_kelurahan . "' and 
                    sppt.kd_blok='" . $kd_blok . "' and 
                    sppt.no_urut='" . $no_urut . "' and 
                    sppt.kd_jns_op='" . $kd_jns_op . "' and 
                    sppt.thn_pajak_sppt='" . $thn_pajak_sppt . "' ) or";

                    $ins = $var;
                    $notr = self::NoTransaksi('IV', $postData['jns_koreksi']);
                    $now = Carbon::now();
                    $ins['tgl_surat'] = new Carbon($postData['tgl_surat']);
                    $ins['no_surat'] = $notr;
                    $ins['jns_koreksi'] = '3';
                    $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                    $ins['keterangan'] = $keterangan;
                    $ins['no_transaksi'] = $notr;
                    $ins['created_at'] = $now;
                    $ins['created_by'] = Auth()->user()->id;
                    $ins['nm_kategori'] = $keterangan;
                    $ins['kd_kategori'] = $postData['jns_koreksi'];

                    $ins['verifikasi_kode'] = 1;
                    $ins['verifikasi_keterangan'] = $keterangan;
                    $ins['verifikasi_by'] = auth()->user()->id;
                    $ins['verifikasi_at'] = $now;

                    $dd[] = $ins;


                    $pembatalan[] = [
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $thn_pajak_sppt,
                        'pendataan_objek_id' => $objek->id ?? '',
                        'tgl_mulai' => new Carbon($postData['tgl_surat'])
                    ];
                }
            }

            if (count($dd) > 0) {
                $wh = substr($wh, 0, -2);
                $ws = str_replace('sppt.', '', $wh);
                if (count($pembatalan) > 0) {
                    DB::connection("oracle_satutujuh")->table('sppt_pembatalan')
                        ->whereraw($ws)
                        ->whereraw("tgl_selesai=to_date('31129999','ddmmyyyy')")
                        ->update(['tgl_selesai' => db::raw("sysdate-1")]);

                    DB::connection("oracle_satutujuh")->table('sppt_pembatalan')->insert($pembatalan);
                }
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->whereraw($ws)->delete();
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);
                DB::connection("oracle_satutujuh")->table('sppt')->whereraw($wh)->update(['status_pembayaran_sppt' => '3']);
            }

            DB::connection("oracle_satutujuh")->commit();
            $msg = "berhasil di proses";
            $status = "1";
        } catch (\Throwable $th) {
            Log::info($th);
            DB::connection("oracle_satutujuh")->rollBack();
            $msg = $th->getMessage();
            $status = "0";
            $dd = null;
        }
        $respon = [
            'status' => $status,
            'msg' => $msg
        ];
        return response($respon, 200);
    }

    public static function cancelSistep($postData)
    {
        $kd_propinsi = $postData['kd_propinsi'];
        $kd_dati2 = $postData['kd_dati2'];
        $kd_kecamatan = $postData['kd_kecamatan'];
        $kd_kelurahan = $postData['kd_kelurahan'];
        $kd_blok = $postData['kd_blok'];
        $no_urut = $postData['no_urut'];
        $kd_jns_op = $postData['kd_jns_op'];

        $tahun = date('Y');
        $keterangan = "Pembatalan karena objek di migrasi ke sismiop";
        // KategoriIventarisasi()[$postData['jns_koreksi']];
        // return $tunggakan;
        try {

            $dd = [];
            $wh = "";
            $tunggakan = self::getTunggakan($postData);
            // $thn_pajak_sppt = $postData['thn_pajak_sppt'];
            $pembatalan = [];
            foreach ($tunggakan as $key => $value) {
                # code...
                if (!in_array($value->jns_koreksi, ['1', '3'])) {
                    $thn_pajak_sppt = $value->thn_pajak_sppt;
                    $var['kd_propinsi'] = $kd_propinsi;
                    $var['kd_dati2'] = $kd_dati2;
                    $var['kd_kecamatan'] = $kd_kecamatan;
                    $var['kd_kelurahan'] = $kd_kelurahan;
                    $var['kd_blok'] = $kd_blok;
                    $var['no_urut'] = $no_urut;
                    $var['kd_jns_op'] = $kd_jns_op;
                    $var['thn_pajak_sppt'] = $thn_pajak_sppt;
                    $wh .= " ( sppt.kd_propinsi='" . $kd_propinsi . "' and 
                    sppt.kd_dati2='" . $kd_dati2 . "' and 
                    sppt.kd_kecamatan='" . $kd_kecamatan . "' and 
                    sppt.kd_kelurahan='" . $kd_kelurahan . "' and 
                    sppt.kd_blok='" . $kd_blok . "' and 
                    sppt.no_urut='" . $no_urut . "' and 
                    sppt.kd_jns_op='" . $kd_jns_op . "' and 
                    sppt.thn_pajak_sppt='" . $thn_pajak_sppt . "' ) or";

                    $ins = $var;
                    $notr = self::NoTransaksi('SM', '06');
                    $now = Carbon::now();
                    $ins['tgl_surat'] = new Carbon($postData['tgl_surat']);
                    $ins['no_surat'] = $notr;
                    $ins['jns_koreksi'] = '3';
                    $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                    $ins['keterangan'] = $keterangan;
                    $ins['no_transaksi'] = $notr;
                    $ins['created_at'] = $now;
                    $ins['created_by'] = Auth()->user()->id;
                    $ins['nm_kategori'] = $keterangan;
                    $ins['kd_kategori'] = '06';

                    $ins['verifikasi_kode'] = 1;
                    $ins['verifikasi_keterangan'] = $keterangan;
                    $ins['verifikasi_by'] = auth()->user()->id;
                    $ins['verifikasi_at'] = $now;

                    $dd[] = $ins;
                }
            }

            if (count($dd) > 0) {
                $wh = substr($wh, 0, -2);
                $ws = str_replace('sppt.', '', $wh);
                if (count($pembatalan) > 0) {
                    DB::connection("oracle_satutujuh")->table('sppt_pembatalan')
                        ->whereraw($ws)
                        ->whereraw("tgl_selesai=to_date('31129999','ddmmyyyy')")
                        ->update(['tgl_selesai' => db::raw("sysdate-1")]);
                    DB::connection("oracle_satutujuh")->table('sppt_pembatalan')->insert($pembatalan);
                }

                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->whereraw($ws)->delete();
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);
                DB::connection("oracle_satutujuh")->table('sppt')->whereraw($wh)->update(['status_pembayaran_sppt' => '3']);
            }

            DB::connection("oracle_satutujuh")->commit();
            $msg = "berhasil di proses";
            $status = "1";
        } catch (\Throwable $th) {
            Log::info($th);
            DB::connection("oracle_satutujuh")->rollBack();
            $msg = $th->getMessage();
            $status = "0";
            $dd = null;
        }
        $respon = [
            'status' => $status,
            'msg' => $msg
        ];
        return response($respon, 200);
    }
}

/* 

1	Di blokir Pembayaran / iventarisasi
	sppt tahun berjalan di batalkan (tgl_terbit bisa di hapus atau data sppt nya sekaligus di hapus)
	untuk tunggakan , di blokir iventarisasi
	Pada form ada opsi /checkbox untuk menon aktifkan objek nya
2	Pembatalan
	sppt tahun berjalan di batalkan (tgl_terbit bisa di hapus atau data sppt nya sekaligus di hapus)
	untuk tunggakan , di blokir seperti di koreksi sehingga piutang berkurang, namun suatu saat bila di aktifkan kembali masih bisa di terbitkan ulang 

*/