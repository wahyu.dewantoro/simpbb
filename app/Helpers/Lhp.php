<?php

namespace App\Helpers;

use App\Models\Notaperhitungan;
use App\Models\SpptBkp;
use App\Models\SuratKeputusan;
use App\Models\SuratKeputusanLampiran;
use App\pegawaiStruktural;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Lhp
{
    public static function All($penelitian = '')
    {
        $data = DB::table(db::raw('tbl_spop a'))
            ->select(db::raw('distinct a.created_at,a.no_formulir,a.layanan_objek_id,jns_transaksi,b.nomor_layanan,jenis_layanan_id,nama_layanan,nop_proses,nm_wp,a.kelurahan_wp,a.kota_wp,  nota,nomor_nota,tgl_bayar,case when surat_keputusan.id is not null then 1 else 0 end sk,
            trunc(sysdate)-trunc(a.created_at) usia_penelitian'))
            ->join(db::raw('layanan_objek b'), function ($join) {
                $join->on(db::raw('a.layanan_objek_id'), '=', db::raw('b.id'))
                    ->on(db::raw('a.no_formulir'), '=', db::raw('b.nomor_formulir'));
            })
            ->join(DB::raw("(select nomor_layanan,is_online from layanan ) l"), 'b.nomor_layanan', '=', 'l.nomor_layanan')
            ->leftjoin(db::raw('(select distinct nomor_layanan,penelitian,layanan_objek_id from penelitian_task) c'), function ($join) {
                $join->on(db::raw('c.nomor_layanan'), '=', db::raw('b.nomor_layanan'))
                    ->on(db::raw('c.layanan_objek_id'), '=', db::raw('b.id'));
            })
            ->join(db::raw('jenis_layanan d'), db::raw('d.id'), '=', db::raw('a.jenis_layanan_id'))
            ->leftJoin(db::raw("(select  nomor_formulir,b.data_billing_id ,tgl_bayar,count(1) nota
            from sppt_penelitian a
            join data_billing b on a.data_billing_id=b.data_billing_id
            group by  nomor_formulir,b.data_billing_id ,tgl_bayar
            ) cb"), 'cb.nomor_formulir', '=', 'a.no_formulir')
            ->leftJoin("surat_keputusan", 'surat_keputusan.layanan_objek_id', '=', 'a.layanan_objek_id')
            ->leftjoin(DB::raw("(
                select layanan_objek_id,kd_status, nomor nomor_nota
                from nota_perhitungan a
                join data_billing b on a.data_billing_id=b.data_billing_id and b.deleted_at is null ) np"), 'np.layanan_objek_id', '=', 'a.layanan_objek_id')
            ->whereraw("((jenis_layanan_id<>8 and ( ( np.layanan_objek_id is not null  ) or ( np.layanan_objek_id is  null ) )) or is_online is not null)")
            // ->whereraw("jenis_layanan_id<>8 and ( ( np.layanan_objek_id is not null and  kd_status='1' ) or ( np.layanan_objek_id is  null ) )")
            ->orderBy('a.created_at', 'desc');
        if ($penelitian <> '') {
            $data = $data->whereraw("penelitian='" . $penelitian . "'");
        }
        return $data;
    }

    public static function listSk()
    {
        $sql = self::All()->whereraw("  ( (nota is null and tgl_bayar is null )  or (nota is not null and tgl_bayar is not null)  )");

        $user = Auth()->user();
        if ($user) {
            $role = $user->roles()->pluck('name')->toarray();
            $input = 'kecamatan';
            $reskec = array_filter($role, function ($item) use ($input) {
                if (stripos($item, $input) !== false) {
                    return true;
                }
                return false;
            });

            $inputdesa = 'desa';
            $resdes = array_filter($role, function ($item) use ($inputdesa) {
                if (stripos($item, $inputdesa) !== false) {
                    return true;
                }
                return false;
            });




            if (count($reskec) > 0) {
                $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
                $kec = implode(',', $kec);
                $sql->whereraw(" substr(nop_proses,5,3) in ($kec) ");
            }


            if (count($resdes) > 0) {
                $res = $user->unitkerja()->selectraw('kd_kecamatan,kd_kelurahan')->first();
                $kec = $res->kd_kecamatan;
                $kel = $res->kd_kelurahan;

                $sql->whereraw(" substr(nop_proses,5,3) ='$kec' and  substr(nop_proses,8,3)='$kel' ");
            }
        }

        return $sql;
    }

    public static function getSk($id)
    {

        $sk = SuratKeputusan::with('TblSpop')->where('layanan_objek_id', $id)->first();
        $penelitian = self::getHasilNjop($id);
        $spop = $penelitian['core'];
        $hasil = $penelitian['hasil'];
        $last = $penelitian['last'];
        if ($sk == null) {
            DB::beginTransaction();
            try {
                $kaban = pegawaiStruktural::whereraw("kode='01'")->first();
                $jabatan = ($kaban->is_plt != '' ? 'PLT ' : '') . $kaban->jabatan;
                if ($spop->jenis_layanan_id == '2') {
                    $jenis = 'Pembatalan.PBB';
                } else
                if ($spop->jenis_layanan_id == '5') {
                    $jenis = 'Pengurangan.PBB';
                } else if ($spop->jenis_layanan_id = '1') {
                    $jenis = 'Pendaftaran Obyek Baru.PBB';
                } else {
                    $jenis = 'Pembetulan.PBB';
                }
                // dd($jenis);
                $sk = SuratKeputusan::create(
                    [
                        'layanan_objek_id' => $id,
                        'jenis' => $jenis,
                        'kota' => 'KEPANJEN',
                        'pegawai' => $kaban->nama,
                        'nip' => $kaban->nip,
                        'jabatan' => $jabatan,
                        'pangkat' => 'Pembina Utama Muda',
                        'tanggal' => new Carbon($spop->tanggal_penelitian)
                    ]
                );

                // data existing
                $alamat_op = "-";
                $alamat_wp = "-";
                if ($hasil) {
                    $alamat_op = $hasil->jalan_op . ' ' .  $hasil->blok_kav_no_op . '  RT ' . ($hasil->rt_op ?? '') . ' RW ' . ($hasil->rw_op ?? '');
                    $alamat_wp = $hasil->jalan_wp;
                    if ($hasil->blok_kav_no_wp) {
                        $alamat_wp .= ' ' . ($hasil->blok_kav_no_wp ?? '');
                    }
                    $alamat_wp .= ' RT ' . $hasil->rt_wp . ' RW ' . $hasil->rw_wp . ' <br>' . $hasil->kelurahan_wp . '<br> ' . $hasil->kota_wp;
                }
                SuratKeputusanLampiran::create([
                    'surat_keputusan_id' => $sk->id,
                    'is_last' => null,
                    'kd_propinsi' => $hasil->kd_propinsi,
                    'kd_dati2' => $hasil->kd_dati2,
                    'kd_kecamatan' => $hasil->kd_kecamatan,
                    'kd_kelurahan' => $hasil->kd_kelurahan,
                    'kd_blok' => $hasil->kd_blok,
                    'no_urut' => $hasil->no_urut,
                    'kd_jns_op' => $hasil->kd_jns_op,
                    'alamat_op' => $alamat_op,
                    'nm_wp' => $hasil->nm_wp ?? $spop->nm_wp,
                    'alamat_wp' => $alamat_wp ?? '-',
                    'luas_bumi' => $hasil->total_luas_bumi ?? 0,
                    'njop_bumi' => $hasil->njop_bumi ?? 0,
                    'luas_bng' => $hasil->total_luas_bng ?? 0,
                    'njop_bng' => $hasil->njop_bng ?? 0,
                ]);

                foreach ($last as $last) {
                    $alamat_wp_last = $last->jalan_wp . ' ' . $last->blok_kav_no_wp ?? '' . ' ' . $last->rw_wp . ' ' . $last->rt_wp .
                        ' <br>' . $last->kelurahan_wp . '<br> ' . $last->kota_wp;
                    SuratKeputusanLampiran::create([
                        'surat_keputusan_id' => $sk->id,
                        'is_last' => '1',
                        'kd_propinsi' => $last->kd_propinsi,
                        'kd_dati2' => $last->kd_dati2,
                        'kd_kecamatan' => $last->kd_kecamatan,
                        'kd_kelurahan' => $last->kd_kelurahan,
                        'kd_blok' => $last->kd_blok,
                        'no_urut' => $last->no_urut,
                        'kd_jns_op' => $last->kd_jns_op,
                        'alamat_op' => $alamat_op,
                        'nm_wp' => $last->nm_wp ?? '-',
                        'alamat_wp' => $alamat_wp_last,
                        'luas_bumi' => $last->total_luas_bumi ?? 0,
                        'njop_bumi' => $last->njop_bumi ?? 0,
                        'luas_bng' => $last->total_luas_bng ?? 0,
                        'njop_bng' => $last->njop_bng ?? 0,
                    ]);
                }
                DB::commit();

                $result = [
                    'is_error' => False,
                    'deskripsi' => 'Berhasil generate SK'
                ];
            } catch (\Throwable $th) {
                DB::rollBack();
                // Log::error($th);
                // abort(500);
                $result = [
                    'is_error' => true,
                    'deskripsi' => $th->getMessage()
                ];
            }
        } else {
            // update data sk
            DB::beginTransaction();
            try {
                //code...
                // data existing
                SuratKeputusanLampiran::where('surat_keputusan_id', $sk->id)
                    ->delete();
                $alamat_op = "-";
                $alamat_wp = "-";
                if ($hasil) {
                    $alamat_op = $hasil->jalan_op . ' ' .  $hasil->blok_kav_no_op . '  RT ' . ($hasil->rt_op ?? '') . ' RW ' . ($hasil->rw_op ?? '');
                    $alamat_wp = $hasil->jalan_wp;
                    // log::info($hasil);
                    if ($hasil->blok_kav_no_wp) {
                        $alamat_wp .= ' ' . ($hasil->blok_kav_no_wp ?? '');
                    }
                    $alamat_wp .= ' RT ' . $hasil->rt_wp . ' RW ' . $hasil->rw_wp . ' <br>' . $hasil->kelurahan_wp . '<br> ' . $hasil->kota_wp;
                }
                SuratKeputusanLampiran::create([
                    'surat_keputusan_id' => $sk->id,
                    'is_last' => null,
                    'kd_propinsi' => $hasil->kd_propinsi,
                    'kd_dati2' => $hasil->kd_dati2,
                    'kd_kecamatan' => $hasil->kd_kecamatan,
                    'kd_kelurahan' => $hasil->kd_kelurahan,
                    'kd_blok' => $hasil->kd_blok,
                    'no_urut' => $hasil->no_urut,
                    'kd_jns_op' => $hasil->kd_jns_op,
                    'alamat_op' => $alamat_op,
                    'nm_wp' => $hasil->nm_wp ?? $spop->nm_wp,
                    'alamat_wp' => $alamat_wp ?? '-',
                    'luas_bumi' => $hasil->total_luas_bumi ?? 0,
                    'njop_bumi' => $hasil->njop_bumi ?? 0,
                    'luas_bng' => $hasil->total_luas_bng ?? 0,
                    'njop_bng' => $hasil->njop_bng ?? 0,
                ]);

                foreach ($last as $last) {
                    $alamat_wp_last = $last->jalan_wp . ' ' . $last->blok_kav_no_wp ?? '' . ' ' . $last->rw_wp . ' ' . $last->rt_wp .
                        ' <br>' . $last->kelurahan_wp . '<br> ' . $last->kota_wp;
                    SuratKeputusanLampiran::create([
                        'surat_keputusan_id' => $sk->id,
                        'is_last' => '1',
                        'kd_propinsi' => $last->kd_propinsi,
                        'kd_dati2' => $last->kd_dati2,
                        'kd_kecamatan' => $last->kd_kecamatan,
                        'kd_kelurahan' => $last->kd_kelurahan,
                        'kd_blok' => $last->kd_blok,
                        'no_urut' => $last->no_urut,
                        'kd_jns_op' => $last->kd_jns_op,
                        'alamat_op' => $alamat_op,
                        'nm_wp' => $last->nm_wp ?? '-',
                        'alamat_wp' => $alamat_wp_last,
                        'luas_bumi' => $last->total_luas_bumi ?? 0,
                        'njop_bumi' => $last->njop_bumi ?? 0,
                        'luas_bng' => $last->total_luas_bng ?? 0,
                        'njop_bng' => $last->njop_bng ?? 0,
                    ]);
                }
                DB::commit();
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
            }
            $result = [
                'is_error' => False,
                'deskripsi' => 'SK sudah tersedia'
            ];
        }

        $response = [
            'sk' => $sk,
            'response' => $result
        ];

        return $response;
    }


    public static function getHasilNjop($id)
    {
        $spop = DB::table('tbl_spop')->selectraw("nm_wp,nop_proses,nop_asal,jenis_layanan_id,no_formulir,to_char(tbl_spop.created_at,'yyyy') tahun_penelitian,nomor_layanan,nop_gabung, trunc(layanan_objek.pemutakhiran_at) tanggal_penelitian")
            ->join('layanan_objek', function ($join) {
                $join->on('layanan_objek.nomor_formulir', '=', 'tbl_spop.no_formulir')
                    ->on('layanan_objek.id', '=', 'tbl_spop.layanan_objek_id');
            })
            ->where('layanan_objek_id', $id)
            ->orderby('tbl_spop.created_at', 'desc')
            ->first();
        // $tahun = $spop->tahun_penelitian - 1;
        $rowq = $spop->nop_proses;
        // dd($spop);

        $kd_kecamatan = substr($rowq, 4, 3);
        $kd_kelurahan = substr($rowq, 7, 3);
        $kd_blok = substr($rowq, 10, 3);
        $no_urut = substr($rowq, 13, 4);
        $kd_jns_op = substr($rowq, 17, 1);

        $prehas = DB::connection('oracle_dua')->table("data_hasil_penelitian")
            ->selectraw("trim(NO_FORMULIR) NO_FORMULIR, trim(KD_PROPINSI) KD_PROPINSI, trim(KD_DATI2) KD_DATI2, trim(KD_KECAMATAN) KD_KECAMATAN, trim(KD_KELURAHAN) KD_KELURAHAN, trim(KD_BLOK) KD_BLOK, trim(NO_URUT) NO_URUT, trim(KD_JNS_OP) KD_JNS_OP, trim(JALAN_OP) JALAN_OP, trim(BLOK_KAV_NO_OP) BLOK_KAV_NO_OP, trim(RW_OP) RW_OP, trim(RT_OP) RT_OP, trim(TOTAL_LUAS_BUMI) TOTAL_LUAS_BUMI, trim(NJOP_BUMI) NJOP_BUMI, trim(TOTAL_LUAS_BNG) TOTAL_LUAS_BNG, trim(NJOP_BNG) NJOP_BNG, trim(NM_WP) NM_WP, trim(JALAN_WP) JALAN_WP,trim(blok_kav_no_wp) blok_kav_no_wp, trim(RW_WP) RW_WP, trim(RT_WP) RT_WP, trim(KELURAHAN_WP) KELURAHAN_WP, trim(KOTA_WP) KOTA_WP ")
            ->whereraw("no_formulir='" . $spop->no_formulir . "'")
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")
            ->orderby('no_formulir', 'desc');
        // dd($prehas->first());
        if (empty($prehas->first())) {
            $prehas = DB::connection("oracle_satutujuh")->table('dat_objek_pajak')
                ->join('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
                ->selectraw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op
                                ,jalan_op,blok_kav_no_op,rw_op,rt_op,total_luas_bumi,njop_bumi,total_luas_bng,njop_bng,nm_wp,jalan_wp,blok_kav_no_wp,rw_wp,rt_wp,kelurahan_wp,kota_wp")
                ->whereraw("no_formulir_spop='" . $spop->no_formulir . "'")
                ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")
                ->orderby('no_formulir_spop', 'desc');
        }


        $hasil = $prehas->first();
        $corelast = $prehas->get();
        $last = $corelast;

        $row = $spop->nop_asal;
        $kd_dati2 = substr($row, 2, 2);
        $kd_kecamatan = substr($row, 4, 3);
        $kd_kelurahan = substr($row, 7, 3);
        $kd_blok = substr($row, 10, 3);
        $no_urut = substr($row, 13, 4);
        $kd_jns_op = substr($row, 17, 1);

        // dd($hasil);
        if ($hasil) {
            $last = SpptBkp::selectraw("sppt_bkp.kd_propinsi,sppt_bkp.kd_dati2,sppt_bkp.kd_kecamatan,sppt_bkp.kd_kelurahan,sppt_bkp.kd_blok,sppt_bkp.no_urut,sppt_bkp.kd_jns_op, '" . str_replace("'", "''", $hasil->jalan_op) . "' jalan_op,'" . $hasil->blok_kav_no_op . "' blok_kav_no_op,'" . $hasil->rw_op . "' rw_op,'" . $hasil->rt_op . "' rt_op,luas_bumi_sppt total_luas_bumi,njop_bumi_sppt njop_bumi,luas_bng_sppt total_luas_bng,njop_bng_sppt njop_bng,nm_wp_sppt,nm_wp_sppt  nm_wp,
                jln_wp_sppt  jalan_wp ,
                blok_kav_no_wp_sppt  blok_kav_no_wp ,
                rw_wp_sppt  rw_wp ,
                rt_wp_sppt  rt_wp ,
                kelurahan_wp_sppt  kelurahan_wp ,
                kota_wp_sppt  kota_wp")
                ->join(db::raw("(select LAYANAN_OBJEK_ID,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,max(id) id
            from SPPT_BKP
           group by LAYANAN_OBJEK_ID,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt) tmp"), 'tmp.id', '=', 'sppt_bkp.id')
                ->where('sppt_bkp.layanan_objek_id', $id);
            if ($spop->nop_gabung <> '') {
                $last = $last->orwhere('sppt_bkp.layanan_objek_id', $spop->nop_gabung);
            }
            $last = $last->get();

            if ($last->count() == 0) {
                $last = DB::connection('oracle_dua')->table("sppt")
                    ->selectraw(" kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op , '" . str_replace("'", "''", $hasil->jalan_op) . "' jalan_op,'" . $hasil->blok_kav_no_op . "' blok_kav_no_op,'" . $hasil->rw_op . "' rw_op,'" . $hasil->rt_op . "' rt_op,luas_bumi_sppt total_luas_bumi,njop_bumi_sppt njop_bumi,luas_bng_sppt total_luas_bng,njop_bng_sppt njop_bng,nm_wp_sppt,nm_wp_sppt  nm_wp ,
                            jln_wp_sppt  jalan_wp ,
                            blok_kav_no_wp_sppt  blok_kav_no_wp ,
                            rw_wp_sppt  rw_wp ,
                            rt_wp_sppt  rt_wp ,
                            kelurahan_wp_sppt  kelurahan_wp ,
                            kota_wp_sppt  kota_wp")
                    ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' and thn_pajak_sppt='" . date('Y') . "'")->get();
                if ($last->count() == 0) {
                    $last = $corelast;
                }
            }
        }

        return [
            'hasil' => $hasil,
            'last' => $last,
            'core' => $spop
        ];
    }

    public static function notaPerhitunganList()
    {
        $data = DB::table("nota_perhitungan")
            ->leftjoin("data_billing", function ($join) {
                $join->on('nota_perhitungan.data_billing_id', '=', 'data_billing.data_billing_id');
            })
            ->leftjoin("layanan_objek", function ($join) {
                $join->on('layanan_objek.id', '=', 'nota_perhitungan.layanan_objek_id')
                    ->whereraw("layanan_objek.pemutakhiran_at IS NOT NULL
                            AND layanan_objek.pemutakhiran_by IS NOT NULL");
            })
            ->leftjoin('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
            ->leftjoin(db::raw("tbl_spop tsa"), function ($join) {
                $join->on("tsa.layanan_objek_id", "=", "layanan_objek.id")
                    ->on("layanan_objek.nomor_formulir", "=", "tsa.no_formulir");
            })
            ->leftjoin('pendataan_objek', 'pendataan_objek.id', '=', 'nota_perhitungan.pendataan_objek_id')
            ->leftJoin('pendataan_batch', 'pendataan_batch.id', '=', 'pendataan_objek.pendataan_batch_id')
            /*  ->leftjoin(db::raw("(SELECT a.*
                        FROM tbl_spop a
             JOIN (  SELECT pendataan_objek_id, MAX (ROWID) ri
                       FROM tbl_spop
                      WHERE pendataan_objek_id IS NOT NULL
                   GROUP BY pendataan_objek_id) b
                ON a.ROWID = b.ri) tsb"), function ($join) {
                $join->on('tsb.pendataan_objek_id', '=', 'pendataan_objek.id')
                    ->on('layanan_objek.nomor_formulir', '=', 'tsb.no_formulir');
            }) */
            ->selectraw("nota_perhitungan.id,
                            jenis_objek,
                            nomor,
                            tanggal,
                            data_billing.kobil,
                            tahun_pajak,
                            tgl_bayar,
                            data_billing.kd_kecamatan,
                            data_billing.kd_kelurahan,
                            nvl (layanan_objek.nomor_layanan, nomor_batch) nomor_layanan,
                            nvl (nota_perhitungan.layanan_objek_id,
                                nota_perhitungan.pendataan_objek_id)
                            layanan_objek_id,
                            case
                            when nota_perhitungan.layanan_objek_id is not null
                            then
                                'PENELITIAN'
                            else
                                'PENDATAAN'
                            end
                            ket,
                            case
                            when nota_perhitungan.layanan_objek_id is not null then '1'
                            else '2'
                            end
                            jenis,
                            data_billing.kd_status,
                            tsa.nop_proses nop,
                            tahun_pajak thn_pajak_sppt,
                            (select sum(total) from billing_kolektif where data_billing_id=nota_perhitungan.data_billing_id)
                            nominal
            ")
            //  nvl (tsa.nop_proses, tsb.nop_proses) nop,
            ->whereraw("(tsa.nop_proses is not null ) and data_billing.deleted_at is null");
        return $data;
    }


    public static function notaListExport($request)
    {
        $sql = self::notaPerhitunganList();
        if ($request->has('kd_kecamatan')) {
            $kd_kecamatan = $request->kd_kecamatan;
            if ($kd_kecamatan != '') {
                $sql = $sql->whereraw("data_billing.kd_kecamatan ='$kd_kecamatan'");
            }
        }
        if ($request->has('kd_kelurahan')) {
            $kd_kelurahan = $request->kd_kelurahan;
            if ($kd_kelurahan != '') {
                $sql = $sql->whereraw("data_billing.kd_kelurahan ='$kd_kelurahan'");
            }
        }


        if ($request->has('jenis')) {
            $jenis = $request->jenis;
            if ($jenis <> '') {
                $sql->whereraw("case when nota_perhitungan.layanan_objek_id is not null then '1' else '2' end='$jenis'");
            }
        }

        if ($request->has('bayar')) {
            $bayar = $request->bayar;
            if ($bayar <> '') {
                $sql->whereraw("data_billing.kd_status='$bayar'");
            }
        }

        if ($request->has('jenis_objek')) {
            $jenis_objek = $request->jenis_objek;
            if ($jenis_objek <> '') {
                $sql->whereraw("(case when nota_perhitungan.layanan_objek_id is not null then '1' else '2' end)='$jenis_objek'");
            }
        }

        $sql = $sql->orderbyraw("nota_perhitungan.id desc");


        switch ($request->jenis_objek) {
            case '3':
                # code...
                $jenis_objek = "Kolektif";
                break;
            case '2':
                $jenis_objek = "Badan";
                break;
            case '1':
                $jenis_objek = "Individu";
                break;

            default:
                # code...
                $jenis_objek = "";
                break;
        }

        switch ($request->bayar) {
            case '1':
                $bayar = 'Lunas';
                break;
            case '0':
                $bayar = 'Belum Lunas';
                break;
            default:
                $bayar = 'Lunas & belum lunas';
                break;
        }

        switch ($request->jenis) {
            case '1':
                # code...
                $jenis = 'Pelayanan';
                break;
            case '2':
                # code...
                $jenis = 'Pendataan';
                break;
            default:
                $jenis = 'Pelayanan & Pendataan';
                break;
        }

        $data = $sql->get();

        return [
            'data' => $data,
            'jenis' => $jenis,
            'bayar' => $bayar,
            'jenis_objek' => $jenis_objek,

        ];
    }
}
