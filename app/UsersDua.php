<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Models\Impersonate;
use Laravel\Passport\HasApiTokens;
use Faker\Factory as Faker;

/**  ADD THIS LINE **/

use Alfa6661\AutoNumber\AutoNumberTrait;

class UsersDua extends Authenticatable
{
    use Notifiable, HasRoles, Impersonate, HasApiTokens;
    // use AutoNumberTrait;
    // HasApiTokens
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /* protected $fillable = [
        'name', 'email', 'password',
    ]; */
    protected $table = 'users';
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // protected $with=['role'];

    // public function 
    public function Unitkerja()
    {
        return $this->belongsToMany('App\Unitkerja', 'user_unit_kerja', 'user_id', 'kd_unit');
    }

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::creating(function ($item) {
    //         $faker = Faker::create('id_ID');
    //         $item->telepon = $faker->phoneNumber;
    //         $item->email = preg_replace('/@example\..*/', '@sipanji.com', $faker->unique()->safeEmail);
    //     });
    // }

    // public function getAutoNumberOptions()
    // {
    //     return [
    //         'nik' => [
    //             'format' => '3507' . '?', // Format kode yang akan digunakan.
    //             'length' => 12 // Jumlah digit yang akan digunakan sebagai nomor urut
    //         ]
    //     ];
    // }

    public function activeOTP()
    {
        return $this->hasOne(UserOTP::class, 'user_id')->where('expired_at', '>', 'now()');
    }
}
