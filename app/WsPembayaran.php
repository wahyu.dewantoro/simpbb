<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WsPembayaran extends Model
{
    //
    protected $connection = 'spo';
    protected $table = 'ws_pembayaran';

    public function tahun(){
        return $this->hasMany('App\WsPembayaranTahun', 'ws_pembayaran_id', 'id');
    }
    
}
