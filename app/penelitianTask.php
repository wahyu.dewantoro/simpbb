<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class penelitianTask extends Model
{
    //
    protected $guarded = [];
    protected $table = 'penelitian_task';
    public $timestamps = false;
    protected $primaryKey = 'id';

    /*    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $now = Carbon::now();
            $item->created_at = $now;
            // $item->created_by = AUth()->user()->id;
            // $item->updated_by = AUth()->user()->id;
            $item->updated_at = $now;
        });


        static::updating(function ($item) {
            $now = Carbon::now();
            $item->updated_at = $now;
        });
    }
 */
    public function layanan()
    {
        return $this->hasOne('App\Models\Layanan', 'nomor_layanan', 'nomor_layanan');
    }

    public function scopePenelitian($sql, $penelitian)
    {
        $res = $sql->join(DB::raw('layanan_objek'), function ($join) {
            $join->on('layanan_objek.nomor_layanan', '=', 'penelitian_task.nomor_layanan')
                ->on("layanan_objek.id", '=', 'penelitian_task.layanan_objek_id');
        })->join(DB::raw('layanan'), function ($join) {
            $join->on('layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan')
                ->on('layanan.kd_status', '<>', db::raw("'3'"))
                ->on('layanan.kd_status', '<>', db::raw("'2'"));
        })
            ->leftjoin(DB::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
            ->leftjoin(DB::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            // or (jenis_layanan_id='8' and is_online is not null)  
            ->whereraw(" layanan.deleted_at is null 
                    and is_tolak is null 
                    and jenis_layanan_id !='8'
                    and penelitian='$penelitian' 
                    and pemutakhiran_at is null
                    and  is_online is null 
                    and ( (jenis_layanan_id='7' and nop_gabung is null) or jenis_layanan_id!='7' ) ");
        return $res;
    }
}
