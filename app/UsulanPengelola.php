<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsulanPengelola extends Model
{
    //
    protected $guarded = [];
    protected $table = 'user_pengelola';

    protected $appends = [
        'status',
        'urldokumen',
        'namadokumen',
        'nama'
    ];

    public function scopeVerifying($query)
    {
        return $query->whereNull('verifikasi_kode');
    }

    public function getstatusattribute(): string
    {
        $kode = $this->verifikasi_kode;
        if ($kode == '1') {
            $status = 'Telah Diverifikasi';
        } else if ($kode == '0') {
            $status = 'Ditolak';
        } else {
            $status = 'Sedang Diverifikasi';
        }
        return $status;
    }

    public function Dokumen()
    {
        return $this->hasOne('App\Dokumen', 'keyid', 'id')
            ->where('tablename', 'user_pengelola');
    }

    public function geturldokumenAttribute(): string
    {
        $url =  $this->dokumen->url ?? "";
        return $url != '' ? url($this->dokumen->url) : '';
    }

    public function getnamadokumenAttribute(): string
    {
        $dok = $this->dokumen->nama_dokumen ?? '';
        return $dok;
    }



    public function user()
    {
        return $this->hasOne('App\user', 'id', 'user_id');
    }

    public function getnamaAttribute(): string
    {
        return $this->user->nama ?? '';
    }
}
