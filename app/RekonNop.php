<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekonNop extends Model
{

    protected $guarded = [];
    protected $table = 'rekon_nop';
}
