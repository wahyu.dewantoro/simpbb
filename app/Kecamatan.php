<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    //
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'ref_kecamatan';
    public $timestamps = false;
    protected $primaryKey = 'kd_kecamatan';

    public function scopeLogin($sql)
    {
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $input = 'kecamatan';
        $reskec = array_filter($role, function ($item) use ($input) {
            if (stripos($item, $input) !== false) {
                return true;
            }
            return false;
        });

        $inputdesa = 'desa';
        $resdes = array_filter($role, function ($item) use ($inputdesa) {
            if (stripos($item, $inputdesa) !== false) {
                return true;
            }
            return false;
        });


        if (count($reskec) > 0) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
            $sql->whereIn('kd_kecamatan', $kec);
        }


        if (count($resdes) > 0) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
            $sql->whereIn('kd_kecamatan', $kec);
        }
    }
}
