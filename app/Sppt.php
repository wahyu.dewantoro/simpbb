<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sppt extends Model
{
    use \Awobaz\Compoships\Compoships;

    //
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    protected $table = 'sppt';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = ['thn_pajak_sppt', 'kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op'];


    public function scopeLogin($sql)
    {
    }

    public function scopeNop($sql, $param)
    {
        // $sql = $sql->query();
        if (isset($param['tahun'])) {

            $sql = $sql->whereraw("thn_pajak_sppt='" . $param['tahun'] . "'");
        }
        if (isset($param['kd_propinsi'])) {
            $sql = $sql->whereraw("kd_propinsi='" . $param['kd_propinsi'] . "'");
        }
        if (isset($param['kd_dati2'])) {
            $sql = $sql->whereraw("kd_dati2='" . $param['kd_dati2'] . "'");
        }
        if (isset($param['kd_kecamatan'])) {
            $sql = $sql->whereraw("kd_kecamatan='" . $param['kd_kecamatan'] . "'");
        }
        if (isset($param['kd_kelurahan'])) {
            $sql = $sql->whereraw("kd_kelurahan='" . $param['kd_kelurahan'] . "'");
        }
        if (isset($param['kd_blok'])) {
            $sql = $sql->whereraw("kd_blok='" . $param['kd_blok'] . "'");
        }
        if (isset($param['no_urut'])) {
            $sql = $sql->whereraw("no_urut='" . $param['no_urut'] . "'");
        }
        if (isset($param['kd_jns_op'])) {
            $sql = $sql->whereraw("kd_jns_op='" . $param['kd_jns_op'] . "'");
        }

        return $sql;
    }

    public function kecamatan()
    {
        return $this->hasOne('App\Kecamatan', 'kd_kecamatan', 'kd_kecamatan');
    }

    public function mutasi()
    {
        return $this->hasMany('App\PembayaranMutasiData', [
            'kd_propinsi',
            'kd_dati2',
            'kd_kecamatan',
            'kd_kelurahan',
            'kd_blok',
            'no_urut',
            'kd_jns_op',
            'thn_pajak_sppt'
        ], [
            'kd_propinsi',
            'kd_dati2',
            'kd_kecamatan',
            'kd_kelurahan',
            'kd_blok',
            'no_urut',
            'kd_jns_op',
            'thn_pajak_sppt',
        ]);
    }
}
