<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    //
    protected $guarded = [];
    protected $table = 'dokumen';
    // public $timestamps = false;
    // protected $primaryKey = 'id';
}
