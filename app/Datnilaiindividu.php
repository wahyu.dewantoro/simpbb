<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datnilaiindividu extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dat_nilai_individu';
    public $timestamps = false;
}
