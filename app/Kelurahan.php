<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'ref_kelurahan';
    public $timestamps = false;
    protected $primaryKey = ['kd_kecamatan','kd_kelurahan'];
    //
    public function scopeLogin($sql)
    {
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        if (in_array('Desa', $role)) {
            $kel = $user->unitkerja()->pluck('kd_kelurahan')->toArray();
            $sql->whereIn('kd_kelurahan', $kel);
        }
    }
}
