<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpSaldo extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $table = 'tmp_saldo_cut_22';
}
