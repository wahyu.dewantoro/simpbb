<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserRayon extends Model
{
    //
    protected $guarded = [];
    protected $table = 'user_rayon';

    protected $appends  = [
        'nmkecamatan',
        'nmkelurahan',
        'status'

    ];

    public function GetNmkecamatanAttribute()
    {
        $kd_kecamatan = $this->kd_kecamatan;
        $cd = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first();
        return $cd->nm_kecamatan ?? "";
    }

    public function GetNmkelurahanAttribute()
    {
        $kd_kecamatan = $this->kd_kecamatan;
        $kd_kelurahan = $this->kd_kelurahan;
        $cd = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first();
        return $cd->nm_kelurahan ?? "";
    }

    public function user()
    {
        return $this->hasOne('App\user', 'id', 'users_id');
    }

    public function scopeVerifying($query)
    {
        return $query->whereNull('verifikasi_kode');
    }

    public function Dokumen()
    {
        return $this->hasOne('App\Dokumen', 'keyid', 'id')
            ->where('tablename', 'user_rayon');
    }


    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $now = Carbon::now();
            $item->created_at = $now;
            $item->updated_at = $now;
        });

        static::updating(function ($item) {
            $item->updated_at = Carbon::now();
        });
    }

    public function scopeVerifikasi($query)
    {
        return $query->whereraw("verifikasi_kode is null");
    }

    public function getstatusattribute(): string
    {
        $kode = $this->verifikasi_kode;
        if ($kode == '1') {
            $status = 'Telah Diverifikasi';
        } else if ($kode == '0') {
            $status = 'Ditolak';
        } else {
            $status = 'Sedang Diverifikasi';
        }
        return $status;
    }
}
