<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VirtualAccount extends Model
{
    //

    protected $guarded = [];
    protected $table = 'v_virtual_account';
    public $timestamps = false;
    public $incrementing = false;
}
