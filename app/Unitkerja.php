<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unitkerja extends Model
{
    //
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'unit_kerja';
    public $timestamps = false;
    protected $primaryKey = 'kd_unit';


    public function scopeBapenda($sql)
    {
        return $sql->whereraw("kd_unit='3507'");
    }

    public function scopeUpt($sql)
    {
        return $sql->whereraw("length(kd_unit)=10 and kd_upt is not null");
    }

    public function scopeKecamatan($sql)
    {
        return $sql->whereraw("length(kd_unit)=7 and kd_upt is not null");
    }

    public function scopeDesa($sql)
    {
        return $sql->whereraw("length(kd_unit)=10 and kd_upt is null");
    }
}
