<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LayananMandiriRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'jenis_layanan_id' => 'required',
            'nop' => 'required',
            'bukti_kepemilikan' => 'required|mimes:pdf',
            'ktp' => 'mimes:pdf|required',
        ];

        return $rule;
    }
}
