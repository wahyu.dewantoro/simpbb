<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegistrasiUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        // Aturan validasi dasar
        $rules = [
            'username' => 'required|unique:users,username',
            'nik' => 'required|string|max:16|unique:users,nik', // Maksimal 16 karakter
            'nama' => 'required',
            'telepon' => 'required|numeric',
            'password' => 'required|confirmed',
            'alamat_user' => 'required',
            'kelurahan_user' => 'required',
            'kecamatan_user' => 'required',
            'dati2_user' => 'required',
            'propinsi_user' => 'required',
            'file_ktp' => 'required|mimes:pdf,jpeg,png,jpg,gif|max:2048',
            'file_selfi' => 'required|mimes:pdf,jpeg,png,jpg,gif|max:2048',
        ];

        // Aturan tambahan untuk role tertentu
        $role = $this->input('role');
        if (in_array($role, ['Rayon', 'DEVELOPER/PENGEMBANG'])) {
            $rules['file_rekom'] = 'required|mimes:pdf,jpeg,png,jpg,gif|max:2048';
            $rules['tgl_surat'] = 'required|date'; // Validasi sebagai tanggal
            $rules['no_surat'] = 'required|string|max:255'; // Maksimal panjang 255 karakter
        }

        return $rules;

    }

    public function messages()
    {
        return [
            '*.required' => 'Wajib diisi.',
            'nik.unique' => 'NIK sudah terdaftar',
            'nik.digits' => 'NIK harus 16 angka',
            '*.numeric' => 'Harus di isi dengan angka',
            'password.confirmed' => 'Konfirmasi kata sandi tidak cocok.'
            // 'nik.required' => 'NIK Harus di isi',
        ];
    }
}
