<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class strukturalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'jabatan' => 'required',
            'nama' => 'required'
        ];

        if ($this->getMethod() == 'POST') {
            $rules += [
                'kode' => 'required|unique:pegawai_struktural'
            ];
        }
        return $rules;
    }
}
