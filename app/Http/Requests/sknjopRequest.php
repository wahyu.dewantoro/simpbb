<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class sknjopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nik_pemohon" =>'required',
            "nama_pemohon" =>'required',
            "alamat_pemohon" =>'required',
            "kelurahan_pemohon" =>'required',
            "kecamatan_pemohon" =>'required',
            "dati2_pemohon" =>'required',
            "propinsi_pemohon" =>'required',
            "nop" =>'required',
            "alamat_op" =>'required',
            "nama_wp" =>'required',
            "alamat_wp" =>'required',
            "luas_bumi" =>'required',
            "njop_bumi" =>'required',
            "luas_bng" =>'required',
            "njop_bng" =>'required',
            "njop_pbb" =>'required',
        ];
    }
}
