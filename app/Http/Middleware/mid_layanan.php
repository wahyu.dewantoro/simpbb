<?php

namespace App\Http\Middleware;

use App\Models\TutupLayanan;
use Closure;

class mid_layanan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $tutup = TutupLayanan::whereraw("tgl_mulai<=trunc(sysdate) 
        and tgl_selesai>=trunc(sysdate)")
            ->selectraw("min(tgl_mulai) tgl_mulai,max(tgl_selesai) tgl_selesai")->first();
        if ($tutup->tgl_mulai != '' && $tutup->tgl_selesai != '') {
            // return view('tutup_layanan', compact('tutup'));
            // dd('tutup layanan');
            return response()->view('tutup_layanan', compact('tutup'));
        } else {
            return $next($request);
        }
    }
}
