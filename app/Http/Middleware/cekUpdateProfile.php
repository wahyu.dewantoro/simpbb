<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class cekUpdateProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        // $user = User::where('email', $request->email)->first();
        /* if ($user->status == 'admin') {
            return redirect('admin/dashboard');
        } elseif ($user->status == 'mahasiswa') {
            return redirect('mahasiswa/dashboard');
        } */

        $user = Auth()->user();

        if (
            $user->hasRole('Wajib Pajak')
        ) {
            return redirect('logout');
        }
        return $next($request);

        /* if($user->update_profile==''){
            return redirect(url('users/'.$user->id.'/edit'));
        } */
    }
}
