<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class BasicAuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        // Cek apakah ada header Authorization
        if (!$request->hasHeader('Authorization')) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        // Ambil kredensial dari header Authorization (Basic base64_encode(username:password))
        $authHeader = $request->header('Authorization');
        $encodedCredentials = substr($authHeader, 6); // Hapus "Basic " dari string
        $decodedCredentials = base64_decode($encodedCredentials);

        // Pisahkan username dan password
        [$username, $password] = explode(':', $decodedCredentials, 2);

        // Cari user berdasarkan username

        /*         return response()->json([
            'username'=>$username,
            'password'=>$password
        ]);
 */
        $user = User::where('username', $username)->first();

        // return response()->json($user);


        // Validasi user dan password
        if (!$user || !Hash::check($password, $user->password)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        // Simpan user dalam request agar bisa digunakan di controller
        // $request->merge(['auth_user' => $user]);

        Auth::login($user);  // Simpan login ke session Laravel

        return $next($request);
    }
}
