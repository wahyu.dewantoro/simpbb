<?php

namespace App\Http\Middleware;

use App\Helpers\SendWa;
use App\Otp;
use App\Services\SendOtpOca;
use App\User;
use App\UserOTP;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class isOnmiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $SendOtpOca;
    public function __construct(SendOtpOca $SendOtpOca)
    {

        $this->SendOtpOca = $SendOtpOca;
    }

    public function handle($request, Closure $next)
    {
        // return $next($request);

        if (auth()->user()->email_verified_at != null) {
            return $next($request);
        } else {
            $user = User::find(Auth()->user()->id);
            // $user_id = $user->id;

            $otp = rand(100000, 999999);

            Otp::updateOrCreate(
                ['user_id' => $user->id],
                ['otp_code' => $otp, 'expires_at' => Carbon::now()->addMinutes(10)]
            );

            // Kirimkan OTP via WhatsApp atau email
            $phoneNumber = $user->telepon;
            $message = $otp;
            $this->SendOtpOca->sendMessage($phoneNumber, $message);

            $username = encrypt($user->id);
            // Logout user sementara
            Auth::logout();
            return redirect()->route('otp.verify.form', $username)->with('success', 'User registered successfully, OTP sent via WhatsApp.');

            // $userOtp = UserOTP::create([
            //     'user_id' => $user_id,
            //     'otp_code' => rand(100000, 999999),
            //     'expired_at' => Carbon::now()->addMinutes(3)
            // ]);
            // $sendWa = [
            //     'phone' => Auth()->user()->telepon,
            //     'message' => $userOtp->otp_code . ' ini adalah kode OTP anda , jangan bagikan kepada siapapun.'
            // ];
            // SendWa::simple($sendWa);

            // return redirect(route('otp-verification', encrypt($user_id)));
        }
        // return redirect('/');
    }
}
