<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class WajibPajakVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        // Cek apakah user memiliki role "Wajib Pajak" dan kode verifikasi bukan 1
        if ($user && $user->hasAnyRole('Wajib Pajak') == true && $user->wp_verifikasi_kode != 1) {
            if ($user->wp_verifikasi_kode == '') {
                return response()->view('notifverifikasi');
            } else {
                // return 'permohonan di tolak';
                return response()->view('notifverifikasi_ditolak');
            }
        }

        return $next($request);
    }
}
