<?php

namespace App\Http\Controllers;

// use App\Formulir;
// use App\Helpers\gallade;
// use App\Models\Jenis_layanan;
// use App\GabungDetail;
// use App\Models\HistoryMutasiGabung;
// use App\Helpers\InformasiObjek;
// use App\Models\Layanan;
// use App\Models\Layanan_dokumen;
// use App\Models\PendataanObjek;
// use App\Models\SuratKeputusan;
// use App\Models\SuratKeputusanLampiran;
// use App\Models\SuratTugas;
// use App\PenelitianKantor;
// use App\Unflag;
use App\Helpers\Dokumen;
use App\Helpers\Lhp;
use App\Helpers\Pajak;
use App\Helpers\PembatalanObjek;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\JenisPengurangan;
use App\Models\KeepPenelitian;
use App\Models\Layanan_dokumen;
use App\Models\Layanan_objek;
use App\Models\Lspop;
use App\Models\Spop;
use App\penelitianTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

// use DataTables;
// use DeepCopy\Filter\KeepFilter;
// use stdClass;

class penelitianKantorController extends Controller
{

    public function index(Request $request)
    {
        // test server commit
        if ($request->ajax()) {
            // tab hasil
            if ($request->tab == 'hasil') {
                $data = Lhp::all(1);
                if (request('nomor_layanan')) {
                    $data->whereRaw(DB::raw("b.nomor_layanan like '%" . request('nomor_layanan') . "%'"));
                }
                return  DataTables::of($data)
                    ->filterColumn('nop', function ($query, $keyword) {
                        $keyword = str_replace('-', '', str_replace('.', '', $keyword));
                        $keyword = strtolower($keyword);
                        $keyword = str_replace("'", "''", $keyword);
                        $query->whereRaw(DB::raw("a.nop_proses like '%$keyword%'"));
                    })
                    ->filterColumn('nomorlayanan', function ($query, $keyword) {
                        $keyword = str_replace('-', '', str_replace('.', '', $keyword));
                        $keyword = strtolower($keyword);
                        $keyword = str_replace("'", "''", $keyword);
                        $query->whereRaw(DB::raw("b.nomor_layanan like '%$keyword%'"));
                    })
                    // 
                    ->addColumn('pilih', function ($row) {
                        $nota = '';
                        if ($row->nota > 0) {
                            $nota = '<a class="dropdown-item cetak-tagihan"role="button"  data-id="' . acak($row->layanan_objek_id) . '" tabindex="0">Nota </a>';
                        }
                        $edit = '<a class="dropdown-item detail" href="' . route('penelitian.kantor.edit', $row->layanan_objek_id) . '">Edit</a>';
                        $sk = '';

                        if ($row->tgl_bayar <> '' || $sk == '1') {
                            $edit = '';
                        }


                        $btn = '<div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-info btn-sm btn-flat">Aksi</button>
                            <button type="button" class="btn btn-info btn-sm btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                ' . $edit . '
                                <a class="dropdown-item detail-objek" role="button"  data-id="' . $row->layanan_objek_id . '" tabindex="0">Detail</a>' . $nota . $sk .
                            '</div>
                            </div>';
                        return $btn;
                    })
                    ->addColumn('nopproses', function ($row) {
                        return formatnop($row->nop_proses);
                    })
                    ->addColumn('noformulir', function ($row) {
                        return formatNomor($row->no_formulir);
                    })
                    ->addColumn('nomorlayanan', function ($row) {
                        return formatNomor($row->nomor_layanan);
                    })
                    ->rawColumns(['pilih', 'noformulir', 'nomorlayanan', 'nopproses'])->make(true);
            }

            // tab task
            if ($request->tab == 'task') {
                $data = penelitianTask::penelitian(1)
                    ->select(db::raw("distinct jenis_objek,layanan_objek.id,layanan_objek.nomor_layanan,layanan_objek.created_at,case when nama_wp is null then  nama_badan else nama_wp end nama_wp,
                        layanan_objek.kd_propinsi,layanan_objek.kd_dati2,layanan_objek.kd_kecamatan,layanan_objek.kd_kelurahan,layanan_objek.kd_blok,layanan_objek.no_urut,layanan_objek.kd_jns_op,layanan.jenis_layanan_nama,
                            nm_kecamatan,nm_kelurahan,replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end alamat_op ,case when jenis_layanan_id=6 and nop_gabung is null then 'Induk' when jenis_layanan_id=6 and nop_gabung is not  null then 'Pecahan' else null end keterangan"))
                    ->orderbyraw(db::raw('jenis_objek desc,  trunc(layanan_objek.created_at) desc,layanan_objek.id asc'));
                if (request('nomor_layanan')) {
                    $data->whereRaw(DB::raw("layanan_objek.nomor_layanan like '%" . request('nomor_layanan') . "%'"));
                }
                return  Datatables::of($data)
                    ->filterColumn('pencarian', function ($query, $keyword) {
                        $keyword = strtolower($keyword);

                        $query->whereRaw(DB::raw("lower(layanan.jenis_layanan_nama) like '%$keyword%' 
                        or lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%' 
                         or ref_kecamatan.nm_kecamatan like '%$keyword%' or ref_kelurahan.nm_kelurahan like '%$keyword%' "));
                        $nopel = preg_replace('/[^0-9]/', '', $keyword);
                        if ($nopel <> '') {
                            $query->orwhereRaw(DB::raw("layanan_objek.nomor_layanan     like '%$nopel%' or  layanan_objek.kd_propinsi||layanan_objek.kd_dati2||layanan_objek.kd_kecamatan||layanan_objek.kd_kelurahan||layanan_objek.kd_blok||layanan_objek.no_urut||layanan_objek.kd_jns_op like '%$nopel%'"));
                        }
                    })->addColumn('nop', function ($data) {
                        return $data->kd_propinsi . '.' . $data->kd_dati2 . '.' . $data->kd_kecamatan . '.' . $data->kd_kelurahan . '.' . $data->kd_blok . '-' . $data->no_urut . '.' . $data->kd_jns_op;
                    })
                    ->addColumn('pilih', function ($row) {
                        return '<a data-toggle="tooltip" data-placement="top" title="Form penelitian"  href="' . url('penelitian/kantor/create') . '?id=' . $row->id . '&penelitian=1"> <i class="fas fa-clipboard-list text-primary fa-2x"></i> </a>';
                    })->rawColumns(['pilih', 'nop'])->make(true);
            }
        }

        // trash data keep
        $userId = Auth()->user()->id;
        KeepPenelitian::where('peneliti_by', $userId)->delete();
        db::table('booking_nop')->where('created_by', $userId)->delete();
        $nomor_layanan = $request->only('nomor_layanan');
        return view('penelitian.kantor_todo', compact('nomor_layanan'));
    }


    public function listNop($nopel)
    {
        // $layab
    }

    public function create(Request $request)
    {

        $userId = Auth()->user()->id;
        $id = $request->id ?? null;

        $penelitian = Layanan_objek::with('layanan')->where('id', $id)->wherenull('nomor_formulir')->first();

        $jenis_objek = $penelitian->layanan->jenis_objek;
        $nomor_layanan = $penelitian->nomor_layanan;
        // dd($penelitian);
        if ($penelitian && in_array($request->penelitian, [1, 2])) {
            $cek = KeepPenelitian::where('layanan_objek_id', $id)->whereraw("trunc(24*mod(sysdate - start_at,1))<2")->first();
            if (count((array)$cek) > 0) {
                if ($cek->peneliti_by <> $userId) {
                    $pesan = 'Berkas sedang di teliti oleh :<br> <strong>' . $cek->user->nama . '</strong>';
                    return view('penelitian.keep', compact('pesan'));
                }
            } else {
                $keep = [
                    'layanan_objek_id' => $id,
                    'peneliti_by' => $userId,
                    'start_at' => Carbon::now()
                ];
                KeepPenelitian::create($keep);
            }
            $jp = $penelitian->layanan->jenis_layanan_id;



            // return $penelitian;
            $kd_propinsi = $penelitian->kd_propinsi;
            $kd_dati2 = $penelitian->kd_dati2;
            $kd_kecamatan = $penelitian->kd_kecamatan;
            $kd_kelurahan = $penelitian->kd_kelurahan;
            $kd_blok = $penelitian->kd_blok;
            $no_urut = $penelitian->no_urut;
            $kd_jns_op = $penelitian->kd_jns_op;


            $penelitian = Pajak::loadPenelitian($penelitian);
            $data = [
                'method' => 'POST',
                'action' => url('penelitian/kantor'),
                'jns_penelitian' => $request->penelitian,
                'penelitian' => (object)$penelitian,
                'nop_pembatalan' => $penelitian['nop_pembatalan'],
                'nop_pecah' => $penelitian['nop_pecah'],
                'lo' => $penelitian['letak_op'],
                'refAjuan' => $penelitian['refAjuan'],
                'title' => 'Form Penelitian Kantor',
                'urlback' => $penelitian['is_online'] == '' ? url('penelitian/kantor') : url('layanan/pembetulan-online'),
                'urlpelimpahan' => url('penelitian/kantor-switch', $id),
                'induk' => $penelitian['nopinduk'],
                'berkas' => $penelitian['berkas']
            ];
            $jenisDokumen = $penelitian['jenisDokumen'];
            $riwayatpembayaran = $penelitian['riwayatpembayaran'];


            $statuspecahan = $penelitian['statuspecahan'];
            $nop = $penelitian['nop'];
            $is_admin = Auth()->user()->hasAnyRole('Super User', 'Administrator');
            $ganti_nop = '0';

            $civ = DB::connection("oracle_satutujuh")->table('pengurang_piutang')
                ->where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->count();
            if ($jp == '10' && $civ > 0) {
                // $ganti_nop = '1';
            }

            // return $jenis_objek;
            // dd($jenisDokumen);
            // dd($data);
            $dokbphtb = [];
            if ($jenis_objek == 5) {

                $dokbphtb = Layanan_dokumen::where('nomor_layanan', $nomor_layanan)->pluck('url', 'nama_dokumen')
                    ->toArray();
            }
            
            return view('spop.form', compact('data', 'dokbphtb', 'nop', 'riwayatpembayaran', 'statuspecahan', 'jenisDokumen', 'is_admin', 'ganti_nop'));
        }
        abort(404);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function cekObjek($nopel)
    {
        $ajuan = DB::table('layanan_objek')->where('nomor_layanan', $nopel)->get()->count();
        $penelitian = DB::table('penelitian_kantor')->where('nomor_layanan', $nopel)->get()->count();

        if ($ajuan == $penelitian) {
            return 1;
        } else {
            return 0;
        }
    }

    public function store(Request $request)
    {
        if ($request->jns_transaksi != '3') {
            if ($request->jenis_pelayanan == '5') {
                $param['layanan_objek_id'] = $request->lhp_id;
                $param['jenis_layanan'] = $request->jenis_layanan;
                $param['nop_proses'] = $request->nop_proses;
                $param['jns_pengurangan'] = $request->jns_pengurangan;
                $param['nilai_pengurangan'] = $request->nilai_pengurangan;
                $param['keterangan'] = $request->ket_pengurangan;
                $param['insentif'] = $request->insentif ?? '0';
                $param['tipe_pengurangan'] = $request->tipe_pengurangan;
                $flash['msg'] = Pajak::prosesPotongan($param);
                $flash['kode_status'] = 1;
            } else {
                // pmeutakhiran
                $flash = Pajak::prosesPenelitianDua($request);
            }
        } else {
            // pembatalan objek
            $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);

            DB::beginTransaction();
            $msg = "";
            try {
                //code...
                $dataspop = (array) DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI FROM DAT_OBJEK_PAJAK A
                JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                WHERE a.kd_kecamatan='$kd_kecamatan'
                AND a.kd_kelurahan='$kd_kelurahan'
                AND a.kd_blok='$kd_blok'
                AND a.no_urut='$no_urut'
                AND a.kd_jns_op='$kd_jns_op' "))[0];

                $request->merge($dataspop);
                $spop = Pajak::pendataanSpop($request);
                Pajak::pendataanObjekPajak($request, $spop);


                // treatment terhadap data tunggakan
                $arrayBatal = ['01', '02', '03'];

                $tahun = date('Y');
                $keterangan = KategoriIventarisasi()[$request->jns_koreksi];

                // iventarisasi tahun berjalan
                DB::connection("oracle_satutujuh")->table("sppt")
                    ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                    and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                    and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' 
                    and status_pembayaran_sppt!='1'")
                    ->update(['tgl_terbit_sppt' => null, 'status_pembayaran_sppt' => '3']);


                if (in_array($request->jns_koreksi, $arrayBatal)) {

                    // cek sppt_koreksi
                    $cs = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                        ->select(db::raw("count(1) jm"))
                        ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                        and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                        and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")->first();

                    if ($cs->jm > 0) {
                        DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                            ->whereraw("thn_pajak_sppt='$tahun' and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                        and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                        and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")
                            ->delete();
                    }

                    $now = Carbon::now();
                    $no_trx = PembatalanObjek::NoTransaksi('IV', $request->jns_koreksi);
                    $ins = [
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $tahun,
                        'tgl_surat' => $now,
                        'no_surat' => date('Ymd'),
                        'jns_koreksi' => '2',
                        'keterangan' => $keterangan,
                        'no_transaksi' => $no_trx,
                        'created_at' => $now,
                        'created_by' => Auth()->user()->id,
                        'nm_kategori' => $keterangan,
                        'kd_kategori' => $request->jns_koreksi,
                        'verifikasi_kode' => 1,
                        'verifikasi_keterangan' => $keterangan,
                        'verifikasi_by' => auth()->user()->id,
                        'verifikasi_at' => $now,
                        'layanan_objek_id' => $request->lhp_id
                    ];
                    DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);
                } else {

                    $dd = [];
                    $wh = "";
                    $postData = [
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                    ];
                    $tunggakan = PembatalanObjek::getTunggakan($postData);
                    $pembatalan = [];
                    foreach ($tunggakan as $key => $value) {
                        # code...
                        if (!in_array($value->jns_koreksi, ['1', '3'])) {

                            $thn_pajak_sppt = $value->thn_pajak_sppt;

                            $var['kd_propinsi'] = $kd_propinsi;
                            $var['kd_dati2'] = $kd_dati2;
                            $var['kd_kecamatan'] = $kd_kecamatan;
                            $var['kd_kelurahan'] = $kd_kelurahan;
                            $var['kd_blok'] = $kd_blok;
                            $var['no_urut'] = $no_urut;
                            $var['kd_jns_op'] = $kd_jns_op;
                            $var['thn_pajak_sppt'] = $thn_pajak_sppt;

                            $wh .= " ( sppt.kd_propinsi='" . $kd_propinsi . "' and 
                                        sppt.kd_dati2='" . $kd_dati2 . "' and 
                                        sppt.kd_kecamatan='" . $kd_kecamatan . "' and 
                                        sppt.kd_kelurahan='" . $kd_kelurahan . "' and 
                                        sppt.kd_blok='" . $kd_blok . "' and 
                                        sppt.no_urut='" . $no_urut . "' and 
                                        sppt.kd_jns_op='" . $kd_jns_op . "' and 
                                        sppt.thn_pajak_sppt='" . $thn_pajak_sppt . "' ) or";

                            $ins = $var;
                            $notr = PembatalanObjek::NoTransaksi('IV', $request->jns_koreksi);
                            $now = Carbon::now();
                            $ins['tgl_surat'] = $now;
                            $ins['no_surat'] = $notr;
                            $ins['jns_koreksi'] = '3';
                            $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                            $ins['keterangan'] = $keterangan;
                            $ins['no_transaksi'] = $notr;
                            $ins['created_at'] = $now;
                            $ins['created_by'] = Auth()->user()->id;
                            $ins['nm_kategori'] = $keterangan;
                            $ins['kd_kategori'] = $request->jns_koreksi;

                            $ins['verifikasi_kode'] = 1;
                            $ins['verifikasi_keterangan'] = $keterangan;
                            $ins['verifikasi_by'] = auth()->user()->id;
                            $ins['verifikasi_at'] = $now;
                            $ins['layanan_objek_id'] = $request->lhp_id ?? '';

                            $dd[] = $ins;

                            // if (in_array($postData['jns_koreksi'], ['03', '04', '05'])) {
                            $pembatalan[] = [
                                'kd_propinsi' => $kd_propinsi,
                                'kd_dati2' => $kd_dati2,
                                'kd_kecamatan' => $kd_kecamatan,
                                'kd_kelurahan' => $kd_kelurahan,
                                'kd_blok' => $kd_blok,
                                'no_urut' => $no_urut,
                                'kd_jns_op' => $kd_jns_op,
                                'thn_pajak_sppt' => $thn_pajak_sppt,
                                'layanan_objek_id' => $request->lhp_id ?? '',
                                'tgl_mulai' => $now
                            ];
                            // }
                        }
                    }

                    if (count($dd) > 0) {
                        $wh = substr($wh, 0, -2);

                        $ws = str_replace('sppt.', '', $wh);

                        if (count($pembatalan) > 0) {
                            DB::connection("oracle_satutujuh")->table('sppt_pembatalan')
                                ->whereraw($ws)
                                ->whereraw("tgl_selesai=to_date('31129999','ddmmyyyy')")
                                ->update(['tgl_selesai' => db::raw("sysdate-1")]);
                            DB::connection("oracle_satutujuh")->table('sppt_pembatalan')->insert($pembatalan);
                        }
                        DB::connection("oracle_satutujuh")->table('sppt_koreksi')->whereraw($ws)->delete();
                        DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);
                        DB::connection("oracle_satutujuh")->table('sppt')->whereraw($wh)->update(['status_pembayaran_sppt' => '3']);
                    }
                    // $res = PembatalanObjek::cancel($data);
                }



                $penelitian = Layanan_objek::with('layanan')->findorfail($request->lhp_id);
                $penelitian->update([
                    'nomor_formulir' => $spop['no_formulir'],
                    'pemutakhiran_at' => Carbon::now(),
                    'pemutakhiran_by' => auth()->user()->id,
                    'keterangan_pembatalan' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek'
                ]);

                DB::commit();
                $kode_status = '1';
                $msg = "Proses pembatalan objek telah di proses";
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                log::emergency($th);
                $type = "error";
                $kode_status = '0';
                $msg = "Proses pembatalan objek gagal di proses";
            }
            $flash['msg'] = $msg;
            $flash['kode_status'] = $kode_status;
        }
        // realese kepp
        KeepPenelitian::where('layanan_objek_id', $request->lhp_id)->delete();

        if ($flash['msg'] <> '') {
            $msg = $flash['msg'];
        } else {
            $msg = "Telah di proses";
        }

        $jenis_flash = 'errorr';
        if (($flash['kode_status'] ?? 0) == '1') {
            $jenis_flash = 'success';
        }

        return redirect(url('penelitian/kantor-hasil-penelitian', $request->lhp_id))->with([$jenis_flash => $msg]);
    }

    public function  hasilPenelitian($id)
    {
        $penelitian = Layanan_objek::whereraw("id ='$id'")->get();
        $formulir = '';
        foreach ($penelitian as $penelitian) {
            $formulir .= $penelitian->nomor_formulir . ',';
        }
        $formulir = substr($formulir, 0, -1);
        $tagihan = DB::table(db::raw("BILLING_KOLEKTIF bk "))
            ->join(db::raw("NOTA_PERHITUNGAN np"), db::raw('BK.DATA_BILLING_ID'), '=', db::raw('np.DATA_BILLING_ID'))
            ->join(db::raw("data_billing db"), db::raw('db.DATA_BILLING_ID'), '=', db::raw('np.DATA_BILLING_ID'))
            ->select(db::raw("bk.tahun_pajak thn_pajak_sppt,sum(pokok)	pokok,
            sum(denda)	denda,
            sum(total)	total"))
            ->where("np.layanan_objek_id", $id)
            ->wherenull('db.deleted_at')
            ->groupby("bk.tahun_pajak")
            ->orderbyraw("bk.tahun_pajak asc")
            ->get();

        $urlback = url('penelitian/kantor');
        return view('penelitian.hasil_penelitian', compact('urlback', 'id', 'tagihan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $objek = Spop::with([
            'jenisLayanan',
            'user',
            'layananObjek'
        ])->leftjoin('layanan_objek', function ($join) {
            $join->on('layanan_objek.nomor_formulir', '=', 'tbl_spop.no_formulir')
                ->on('layanan_objek.id', '=', 'tbl_spop.layanan_objek_id');
        })->selectraw("tbl_spop.*,keterangan_pembatalan")
            ->where('layanan_objek.id', $id)->first();

        // dd($objek);

        $bng = Lspop::with('user')->where('layanan_objek_id', $id)->get();
        $pecahobjek = [];
        $pecahbng = [];
        $berkasajuan = [];
        if ($objek->layananObjek != null) {
            $berkasajuan = Layanan_dokumen::where('nomor_layanan', $objek->layananObjek->nomor_layanan)->pluck('nama_dokumen', 'keterangan')->toarray();
            if ($objek->layananObjek->layanan->jenis_layanan_id == 6 && $objek->layananObjek->nop_gabung == '') {
                // $pid = $cek->nomor_layanan;
                $pecahobjek = Spop::with([
                    'jenisLayanan',
                    'user'
                ])->join('layanan_objek', function ($join) {
                    $join->on('layanan_objek.id', '=', 'tbl_spop.layanan_objek_id')
                        ->on('layanan_objek.nomor_formulir', '=', 'tbl_spop.no_formulir');
                })
                    ->select(db::raw("tbl_spop.*"))
                    ->where('nomor_layanan',  $objek->layananObjek->nomor_layanan)
                    ->where('nop_asal', $objek->nop_asal)
                    ->wherenotnull('nop_gabung')->get();

                $pecahbng = Lspop::with('user')
                    ->join('layanan_objek', 'layanan_objek.id', '=', 'tbl_lspop.layanan_objek_id')
                    ->select(db::raw("tbl_lspop.*"))
                    ->where('nomor_layanan',  $objek->layananObjek->nomor_layanan)->wherenotnull('nop_gabung')
                    ->get();
            }
        }
        $lhp = [];
        if (!in_array($objek->jenis_layanan_id, [2, 5])) {
            $lhp = Lhp::getHasilNjop($id);
        }
        // dd($lhp);

        return view('penelitian._show', compact('objek', 'bng', 'pecahobjek', 'pecahbng', 'berkasajuan', 'lhp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $penelitian = Layanan_objek::where('id', $id)->first();
        $jenis_objek = $penelitian->layanan->jenis_objek;
        $nomor_layanan = $penelitian->nomor_layanan;
        if ($penelitian) {
            $hsl = DB::table('tbl_spop')->where('layanan_objek_id', $id)->where('no_formulir', $penelitian->nomor_formulir)->first();
            $letak_op = [];
            $nop = $hsl->nop_proses;
            $nopinduk = $hsl->nop_asal;
            $kd_propinsi = substr($hsl->nop_proses, 0, 2);
            $kd_dati2 = substr($hsl->nop_proses, 2, 2);
            $kd_kecamatan = substr($hsl->nop_proses, 4, 3);
            $kd_kelurahan = substr($hsl->nop_proses, 7, 3);
            $kd_blok = substr($hsl->nop_proses, 10, 3);
            $no_urut = substr($hsl->nop_proses, 13, 4);
            $kd_jns_op = substr($hsl->nop_proses, 17, 1);
            $th = date('Y');

            $ap = [
                'tahun' => $th,
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
            ];
            if (!in_array($penelitian->layanan->jenis_layanan_id, [5, 2])) {
                $letak_op = Pajak::getAlamatObjek($ap);
            }

            // mutasi gabung
            $nop_pembatalan = [];
            if (!in_array($penelitian->layanan->jenis_layanan_id, [5, 2])) {
                if ($penelitian->layanan->jenis_layanan_id == 7) {
                    // nop pembatalan karena penggabungan
                    $nop_pembatalan = Layanan_objek::whereraw("nomor_layanan ='" . $penelitian->nomor_layanan . "'  and nop_gabung='$id' ")->get()->toarray();
                }
            }

            $nop_pecah = [];
            $statuspecahan = [];
            if (!in_array($penelitian->layanan->jenis_layanan_id, [5, 2, 7])) {
                $ida = $id;
                $idi = $penelitian->nop_gabung;
                if ($idi == '') {
                    $idi = $id;
                }

                $nop_pecah = DB::table('layanan_objek')
                    ->leftjoin('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                    ->leftjoin('kelompok_objek', 'kelompok_objek.id', '=', 'layanan_objek.kelompok_objek_id')
                    ->leftjoin('lokasi_objek', 'lokasi_objek.id', '=', 'layanan_objek.lokasi_objek_id')
                    ->leftjoin(db::raw("(select distinct nomor_formulir,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op
                    from sppt_Penelitian) hsl"), 'hsl.nomor_formulir', '=', 'layanan_objek.nomor_formulir')
                    ->select(db::raw('layanan_objek.nomor_layanan,nama_wp,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_propinsi else hsl.kd_propinsi end kd_propinsi,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_dati2 else hsl.kd_dati2 end kd_dati2,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_kecamatan else hsl.kd_kecamatan end kd_kecamatan,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_kelurahan else hsl.kd_kelurahan end kd_kelurahan,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_blok else hsl.kd_blok end kd_blok,
                    case when  hsl.nomor_formulir is null then layanan_objek.no_urut else hsl.no_urut end no_urut,
                    case when  hsl.nomor_formulir is null then layanan_objek.kd_jns_op else hsl.kd_jns_op end kd_jns_op,
                    nama_kelompok,upper(nama_lokasi) nama_lokasi, CASE
                                        WHEN nop_gabung IS NULL 
                                        THEN  case when jenis_layanan_id=6 then sisa_pecah_total_gabung else luas_bumi end
                                        ELSE luas_bumi
                                    END luas_bumi,luas_bng,case when nop_gabung is null  then  luas_bumi else (select luas_bumi from layanan_objek tmp where id=layanan_objek.nop_gabung)  end luas_induk,nop_gabung'));

                // mutasi pecah 


                if ($penelitian->layanan->jenis_layanan_id == 6) {
                    if ($penelitian->no_gabung == '') {
                        $statuspecahan = DB::select(DB::raw("select sum(jumlah) jumlah,sum(teliti) sudah
                                from (
                                select count(1)  jumlah,case when pemutakhiran_at is not null then 1  else 0 end teliti
                                from layanan_objek
                                where nop_gabung='$ida'
                                group by pemutakhiran_at
                                )"));
                    }

                    $nop_pecah = $nop_pecah->whereraw("layanan_objek.id='$ida'  or (nop_gabung='$idi' or layanan_objek.id='$idi')")->get();
                } else {
                    $nop_pecah = $nop_pecah->whereraw("layanan_objek.id='$ida'")->get();
                }
            }


            $riwayatpembayaran = [];
            if (!in_array($penelitian->layanan->jenis_layanan_id, [5, 2])) {
                // jika bukan pembatalan atau pengurangan 

                if (optional($penelitian->layanan)->jenis_layanan_id <> '1') {
                    // jika bukan pendaftaran baru
                    $kd_propinsi = substr($nopinduk, 0, 2);
                    $kd_dati2 = substr($nopinduk, 2, 2);
                    $kd_kecamatan = substr($nopinduk, 4, 3);
                    $kd_kelurahan = substr($nopinduk, 7, 3);
                    $kd_blok = substr($nopinduk, 10, 3);
                    $no_urut = substr($nopinduk, 13, 4);
                    $kd_jns_op = substr($nopinduk, 17, 1);
                }

                // dd(optional($penelitian->layanan)->jenis_layanan_id);
                if (optional($penelitian->layanan)->jenis_layanan_id == '1') {
                    $start = date('Y') - 4;
                    if ($kd_blok == '000') {
                        $start = date('Y');
                    }
                }

                // else if ($penelitian->layanan->jenis_layanan_id == 7) {
                //     $start = date('Y');
                // } 
                else {
                    $cst = DB::connection("oracle_satutujuh")->table("sppt")
                        ->whereraw(" KD_PROPINSI = '$kd_propinsi'
                            AND KD_DATI2 = '$kd_dati2'
                            AND KD_KECAMATAN = '$kd_kecamatan'
                            AND KD_KELURAHAN = '$kd_kelurahan'
                            AND KD_BLOK = '$kd_blok'
                            AND NO_URUT = '$no_urut'
                            AND KD_JNS_OP = '$kd_jns_op'")->select(db::raw("min(thn_pajak_sppt) thn_pajak_sppt"))->first();
                    $start = '2003';
                    if ($cst) {
                        $start = $cst->thn_pajak_sppt ?? '2003';
                    }
                }

                if ($penelitian->layanan->jenis_layanan_id == 7) {
                    $riwayatpembayaran = [];
                } else {
                    $riwayatpembayaran = db::table(db::raw("(
                        SELECT $start + LEVEL - 1  tahun
                        FROM dual
                    CONNECT BY $start + LEVEL - 1 <= to_char(sysdate,'yyyy')
                    ) ref_tahun"))
                        ->select(db::raw("'$kd_propinsi' kd_propinsi,
                                                '$kd_dati2' kd_dati2,
                        '$kd_kecamatan' kd_kecamatan,
                        '$kd_kelurahan' kd_kelurahan,
                        '$kd_blok' kd_blok,
                        '$no_urut' no_urut,
                        '$kd_jns_op' kd_jns_op,tahun thn_pajak_sppt ,( SELECT to_char(tgl_pembayaran_sppt,'yyyy-mm-dd') FROM pbb.pembayaran_sppt 
                        where KD_PROPINSI = '$kd_propinsi'
                        AND KD_DATI2 = '$kd_dati2'
                        AND KD_KECAMATAN = '$kd_kecamatan'
                        AND KD_KELURAHAN = '$kd_kelurahan'
                        AND KD_BLOK = '$kd_blok'
                        AND NO_URUT = '$no_urut'
                        AND KD_JNS_OP = '$kd_jns_op' AND THN_PAJAK_SPPT =ref_tahun.tahun AND rownum=1 ) tgl_pembayaran_sppt,
                        (SELECT no_transaksi
                        FROM pbb.pengurang_piutang
                        WHERE     KD_PROPINSI = '$kd_propinsi'
                            AND KD_DATI2 = '$kd_dati2'
                            AND KD_KECAMATAN = '$kd_kecamatan'
                            AND KD_KELURAHAN = '$kd_kelurahan'
                            AND KD_BLOK = '$kd_blok'
                            AND NO_URUT = '$no_urut'
                            AND KD_JNS_OP = '$kd_jns_op'
                            AND THN_PAJAK_SPPT = ref_tahun.tahun                    
                            AND ROWNUM = 1)
                        koreksi"))
                        ->get();
                }
            }

            $refAjuan = Cache::remember(md5('layanankantor_edit'), 3600, function () {
                return  DB::table(db::raw("ref_formulir"))
                    ->select(db::raw('ref_formulir.id id_formulir,jenis_layanan_id,nama_layanan'))
                    ->join(db::raw('ref_formulir_layanan'), 'ref_formulir_layanan.ref_formulir_id', '=', 'ref_formulir.id')
                    ->join('jenis_layanan', 'jenis_layanan.id', '=', 'ref_formulir_layanan.jenis_layanan_id')
                    ->where('penelitian', 1)
                    ->orderBy('jenis_layanan_id')->get();
            });



            $berkas = Layanan_dokumen::where('nomor_layanan', $penelitian->nomor_layanan)->where('keyid', $id)->select(db::raw('nama_dokumen,keterangan,id,keyid,nomor_layanan,url'))->orderby('id', 'asc')->get()->toarray();
            switch ($penelitian->layanan->jenis_layanan_id) {
                case '6':
                    # code...
                    $luas_bumi = $penelitian->nop_gabung <> '' ? $penelitian->luas_bumi : $penelitian->sisa_pecah_total_gabung;
                    break;
                case '7':
                    $luas_bumi = $penelitian->nop_gabung <> '' ? $penelitian->luas_bumi : $penelitian->sisa_pecah_total_gabung;
                    break;
                default:
                    # code...
                    $luas_bumi = $penelitian->luas_bumi;
                    break;
            }

            $exp_regional = explode('-', $hsl->kelurahan_wp);
            $exp_daerah = explode('-', $hsl->kota_wp);
            $bangunan = DB::table('tbl_lspop')->where('layanan_objek_id', $id)->get()->count();
            $jns_pengurangan = [];

            if (optional($penelitian->layanan)->jenis_layanan_id == 5) {
                $jns_pengurangan = JenisPengurangan::where('jenis', 1)->get();
            }
            $penelitian = [
                'nomor_formulir' => $hsl->no_formulir,
                'jenis_pelayanan' => $penelitian->layanan->jenis_layanan_id,
                'jts' => $penelitian->layanan->jenis_layanan_id == 2 ? 3 : 2,
                'luas_bumi' =>  $hsl->luas_bumi,
                'nomor_layanan' => $penelitian->nomor_layanan,
                'id' => $penelitian->id,
                'rt_op' => $hsl->rt_op,
                'rw_op' => $hsl->rw_op,
                'nik_wp' => $hsl->subjek_pajak_id <> '' ? $hsl->subjek_pajak_id : $penelitian->layanan->nik,
                'nama_wp' => $hsl->nm_wp <> '' ? $hsl->nm_wp : $penelitian->layanan->nama,
                'alamat_wp' => $hsl->jalan_wp . '|' . $hsl->blok_kav_no_wp <> '' ? $hsl->jalan_wp . '|' . $hsl->blok_kav_no_wp : $penelitian->layanan->alamat,
                'rt_wp' => $hsl->rt_wp,
                'rw_wp' => $hsl->rw_wp,
                'kelurahan_wp' => trim($exp_regional[0] ?? null),
                'kecamatan_wp' => trim($exp_regional[1] ?? null),
                'dati2_wp' => trim($exp_daerah[0] ?? null),
                'propinsi_wp' => trim($exp_daerah[1] ?? null),
                'telp_wp' => $hsl->telp_wp ?? '',
                'luas_bng' =>   $penelitian->luas_bng,
                'kelompok_objek_id' => $penelitian->kelompok_objek_id,
                'jenis_objek' => $penelitian->layanan->jenis_objek,
                'bangunan' => $bangunan,
                'nop_gabung' => $penelitian->nop_gabung,
                'jns_pengurangan' => $jns_pengurangan,
                'is_online' => $penelitian->layanan->is_online
            ];
            $data = [
                'method' => 'patch',
                'action' => route('penelitian.kantor.update', $id),
                'jns_penelitian' => 1,
                'penelitian' => (object)$penelitian,
                'nop_pembatalan' => $nop_pembatalan,
                'nop_pecah' => $nop_pecah,
                'lo' => $letak_op,
                'refAjuan' => $refAjuan,
                'title' => 'Form Penlitian Kantor',
                'urlback' =>  $penelitian['is_online'] == '' ? url('penelitian/kantor') : url('mandiri/layanan'),
                'urlpelimpahan' => '',
                'induk' => $nopinduk,
                'berkas' => $berkas,
            ];
            $jenisDokumen = Dokumen::list();

            $is_admin = Auth()->user()->hasAnyRole('Super User', 'Administrator');
            $dokbphtb = [];
            if ($jenis_objek == 5) {

                $dokbphtb = Layanan_dokumen::where('nomor_layanan', $nomor_layanan)->pluck('url', 'nama_dokumen')
                    ->toArray();
            }
            return view('spop.form', compact('data', 'dokbphtb', 'nop', 'riwayatpembayaran', 'statuspecahan', 'jenisDokumen', 'is_admin'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        // dd($request->all());
        if ($request->jns_transaksi != '3') {
            if ($request->jenis_pelayanan == '5') {
                $param['layanan_objek_id'] = $request->lhp_id;
                $param['jenis_layanan'] = $request->jenis_layanan;
                $param['nop_proses'] = $request->nop_proses;
                $param['jns_pengurangan'] = $request->jns_pengurangan;
                $param['nilai_pengurangan'] = $request->nilai_pengurangan;
                $param['keterangan'] = $request->ket_pengurangan;
                $param['insentif'] = $request->insentif ?? '0';
                $param['tipe_pengurangan'] = $request->tipe_pengurangan;

                $flash['msg'] = Pajak::prosesPotongan($param);
                $flash['kode_status'] = 1;
            } else {
                // pmeutakhiran
                $flash = Pajak::prosesEditPenelitianDua($request);
            }
        } else {
            // pembatalan objek

            // return $request->jns_koreksi;
            $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);

            $dd = [];
            $wh = "";
            $keterangan = KategoriIventarisasi()[$request->jns_koreksi];
            $postData = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
            ];
            $tunggakan = PembatalanObjek::getTunggakan($postData);

            // dd($tunggakan);
            // return $tunggakan;
            $pembatalan = [];
            foreach ($tunggakan as $key => $value) {
                # code...
                if (!in_array($value->jns_koreksi, ['1', '3'])) {
                    $thn_pajak_sppt = $value->thn_pajak_sppt;
                    $var['kd_propinsi'] = $kd_propinsi;
                    $var['kd_dati2'] = $kd_dati2;
                    $var['kd_kecamatan'] = $kd_kecamatan;
                    $var['kd_kelurahan'] = $kd_kelurahan;
                    $var['kd_blok'] = $kd_blok;
                    $var['no_urut'] = $no_urut;
                    $var['kd_jns_op'] = $kd_jns_op;
                    $var['thn_pajak_sppt'] = $thn_pajak_sppt;

                    $wh .= " ( sppt.kd_propinsi='" . $kd_propinsi . "' and 
                                        sppt.kd_dati2='" . $kd_dati2 . "' and 
                                        sppt.kd_kecamatan='" . $kd_kecamatan . "' and 
                                        sppt.kd_kelurahan='" . $kd_kelurahan . "' and 
                                        sppt.kd_blok='" . $kd_blok . "' and 
                                        sppt.no_urut='" . $no_urut . "' and 
                                        sppt.kd_jns_op='" . $kd_jns_op . "' and 
                                        sppt.thn_pajak_sppt='" . $thn_pajak_sppt . "' ) or";

                    $ins = $var;
                    $notr = PembatalanObjek::NoTransaksi('IV', $request->jns_koreksi);
                    $now = Carbon::now();
                    $ins['tgl_surat'] = $now;
                    $ins['no_surat'] = $notr;
                    $ins['jns_koreksi'] = '3';
                    $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                    $ins['keterangan'] = $keterangan;
                    $ins['no_transaksi'] = $notr;
                    $ins['created_at'] = $now;
                    $ins['created_by'] = Auth()->user()->id;
                    $ins['nm_kategori'] = $keterangan;
                    $ins['kd_kategori'] = $request->jns_koreksi;

                    $ins['verifikasi_kode'] = 1;
                    $ins['verifikasi_keterangan'] = $keterangan;
                    $ins['verifikasi_by'] = auth()->user()->id;
                    $ins['verifikasi_at'] = $now;
                    $ins['layanan_objek_id'] = $request->lhp_id ?? '';

                    $dd[] = $ins;

                    // if (in_array($postData['jns_koreksi'], ['03', '04', '05'])) {
                    $pembatalan[] = [
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $thn_pajak_sppt,
                        'layanan_objek_id' => $request->lhp_id ?? '',
                        'tgl_mulai' => $now
                    ];
                    // }
                }
            }
            // dd($dd);
            if (count($dd) > 0) {
                $wh = substr($wh, 0, -2);
                $ws = str_replace('sppt.', '', $wh);
                if (count($pembatalan) > 0) {
                    DB::connection("oracle_satutujuh")->table('sppt_pembatalan')
                        ->whereraw($ws)
                        ->whereraw("tgl_selesai=to_date('31129999','ddmmyyyy')")
                        ->update(['tgl_selesai' => db::raw("sysdate-1")]);

                    DB::connection("oracle_satutujuh")->table('sppt_pembatalan')->insert($pembatalan);
                }

                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->whereraw($ws)->delete();
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);

                DB::connection("oracle_satutujuh")->table('sppt')->whereraw($wh)->update(['status_pembayaran_sppt' => '3']);
            }

            $msg = "telah di proses";
            /* $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);


            DB::beginTransaction();
            $msg = "";
            try {
                //code...
                $dataspop = (array) DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP, STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI FROM DAT_OBJEK_PAJAK A
                JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                WHERE a.kd_kecamatan='$kd_kecamatan'
                AND a.kd_kelurahan='$kd_kelurahan'
                AND a.kd_blok='$kd_blok'
                AND a.no_urut='$no_urut'
                AND a.kd_jns_op='$kd_jns_op' "))[0];

                $request->merge($dataspop);
                // dd($request->all());

                $spop = Pajak::pendataanSpop($request);
                Pajak::pendataanObjekPajak($request, $spop);
                $wherenop = [
                    ['kd_propinsi', '=', $kd_propinsi],
                    ['kd_dati2', '=', $kd_dati2],
                    ['kd_kecamatan', '=', $kd_kecamatan],
                    ['kd_kelurahan', '=', $kd_kelurahan],
                    ['kd_blok', '=', $kd_blok],
                    ['no_urut', '=', $no_urut],
                    ['kd_jns_op', '=', $kd_jns_op]
                ];

                // blokir semua tagihan untuk nop tersebut
                $ck = DB::connection('oracle_satutujuh')->table(DB::raw('sppt'))
                    ->select(db::raw('thn_pajak_sppt,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op'))
                    ->where($wherenop)
                    ->where('status_pembayaran_sppt', '0')
                    ->get();
                // ambil nomoer transaksi untuk sppt_koreksi
                $ntr = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                    ->select(db::raw("'IV'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'IV'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
                    ->whereraw("no_transaksi like 'IV'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi;
                $koreksi = [];
                $wblokir = [];
                foreach ($ck as $tg) {
                    $wblokir[] = [
                        'kd_propinsi' => $tg->kd_propinsi,
                        'kd_dati2' => $tg->kd_dati2,
                        'kd_kecamatan' => $tg->kd_kecamatan,
                        'kd_kelurahan' => $tg->kd_kelurahan,
                        'kd_blok' => $tg->kd_blok,
                        'no_urut' => $tg->no_urut,
                        'kd_jns_op' => $tg->kd_jns_op,
                        'thn_pajak_sppt' => $tg->thn_pajak_sppt
                    ];
                    $koreksi[] = [
                        'kd_propinsi' => $tg->kd_propinsi,
                        'kd_dati2' => $tg->kd_dati2,
                        'kd_kecamatan' => $tg->kd_kecamatan,
                        'kd_kelurahan' => $tg->kd_kelurahan,
                        'kd_blok' => $tg->kd_blok,
                        'no_urut' => $tg->no_urut,
                        'kd_jns_op' => $tg->kd_jns_op,
                        'thn_pajak_sppt' => $tg->thn_pajak_sppt,
                        'keterangan' => 'Permohonan penonaktifan  Objek',
                        'verifikasi_kode' => '1',
                        'no_surat' => $ntr,
                        'tgl_surat' => db::raw("sysdate"),
                        'verifikasi_by' => auth()->user()->id,
                        'verifikasi_keterangan' => 'Proses penelitian/pendataan',
                        'no_transaksi' => $ntr,
                        'created_at' => db::raw("sysdate"),
                        'created_by' => auth()->user()->id,
                        'jns_koreksi' => '2'
                    ];
                }

                if (!empty($wblokir)) {
                    DB::connection('oracle_satutujuh')->table('sppt')
                        ->where($wblokir)
                        ->update(['status_pembayaran_sppt' => 3]);
                }

                if (!empty($koreksi)) {
                    DB::connection('oracle_satutujuh')
                        ->table('sppt_koreksi')
                        ->insert($koreksi);
                }

                $penelitian = Layanan_objek::with('layanan')->findorfail($request->lhp_id);
                $penelitian->update([
                    'nomor_formulir' => $spop['no_formulir'],
                    'pemutakhiran_at' => Carbon::now(),
                    'pemutakhiran_by' => auth()->user()->id,
                    'keterangan_pembatalan' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek'
                ]);

                DB::commit();

                $sk = SuratKeputusan::where('layanan_objek_id', $request->lhp_id)->select('id')->first();
                if ($sk) {
                    SuratKeputusanLampiran::where('surat_keputusan_id', $sk->id)->delete();
                    SuratKeputusan::where('layanan_objek_id', $request->lhp_id)->delete();
                }

                $msg = "Proses pembatalan objek telah di proses";
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                log::emergency($th);
                $type = "error";
                $msg = "Proses pembatalan objek gagal di proses";
            }
           */
            $flash['msg'] = $msg;
        }



        if ($flash['msg'] <> '') {
            $msg = $flash['msg'];
        } else {
            $msg = "Telah di proses";
        }

        return redirect(url('penelitian/kantor-hasil-penelitian', $request->lhp_id))->with(['success' => $msg]);
        $jenis_flash = 'errorr';
        if (($flash['kode_status'] ?? 0) == '1') {
            $jenis_flash = 'success';
        }

        return redirect(url('penelitian/kantor-hasil-penelitian', $request->lhp_id))->with([$jenis_flash => $msg]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function switchPenelitian($idObjek)
    {
        DB::beginTransaction();
        try {
            $layanan = Layanan_objek::find($idObjek);
            $nopel = $layanan->nomor_layanan;
            $id = $layanan->nop_gabung <> '' ? $layanan->nop_gabung : $layanan->id;

            $penelitiantask = penelitianTask::where('layanan_objek_id', $id)->where('nomor_layanan', $nopel)->first();
            $penelitiantask->update(['penelitian' => 2]);
            DB::commit();
            $flash = ['success' => 'Berhasil di limpahkan ke penelitian khusus'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            log::emergency($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(url('penelitian/kantor'))->with($flash);
    }

    public function SimpanTolak(Request $request, $id)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            //code...   
            Layanan_objek::where('id', $id)->update([
                'pemutakhiran_at' => Carbon::now(),
                'pemutakhiran_by' => Auth()->user()->id,
                'is_tolak' => 1,
                'keterangan_tolak' => $request->keterangan_tolak
            ]);

            DB::commit();
            $cl = DB::table("layanan_objek")->join('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                ->where('layanan_objek.id', $id)
                ->first();
            if ($cl->jenis_layanan_id == '4') {
                // Lhp::getSk($id);
                $urlsk = url('sk') . '/' . acak($id);
                $res = Http::get($urlsk);
            }


            return true;
        } catch (\Throwable $th) {
            //throw $th;
            log::emergency($th);
            db::rollBack();
            return false;
        }
    }

    public function ditolak(Request $request)
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }

        $jp = Jenis_layanan::orderby('order', 'asc')->pluck('nama_layanan', 'id')->toarray();
        // dd($jp);

        ksort($jp);
        // dd($jp);

        return view('penelitian.penolakan', compact('result', 'kecamatan', 'kelurahan', 'jp'));
    }

    public function  ditolakSk($id)
    {
        return $id;
    }
}
