<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\Kelurahan;
use App\Models\Penagihan;
use App\Models\PenagihanDetail;
use App\Models\Piutang;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PenagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        //Penagihan::where(['penagihan_id'=>'e62501b0-9456-11ec-895a-98f2b3257480'])->delete();
        // $setData=[
        //     'nomor_penagihan'=>'10000000001'
        // ];
        // $penagihan= new Penagihan($setData);
        // $penagihan->save();
        // $getPenagihan=Penagihan::where(['nomor_penagihan'=>'10000000001'])->select('penagihan_id')->get();
        // $id=$getPenagihan->first()->penagihan_id;
        // $file=public_path("tagihan_2016-2021.xlsx");
        // $array = Excel::toArray(false, $file);
        // unset($array[0][0]);
        // $temp_nop=[];
        // foreach($array[0] as $cell){
        //     $cellValidation=Validator::make($cell,[
        //         '1' => 'required'
        //     ]);
        //     if (!$cellValidation->fails()) {
        //         $nop=explode(".",$cell[1]);
        //         $blok_urut=explode("-",$nop[4]);
        //         //11 = 2021, 12= 2020, 13=2019, 14=2018, 15=2017, 16=2016
        //         $list=['11'=>'2021','12'=>'2020','13'=>'2019','14'=>'2018','15'=>'2017','16'=>'2016'];
        //         $set=[];
        //         $no=1;
        //         foreach($list as $key=>$setval){
        //             if($cell[$key]){
        //                 if(!Arr::exists($temp_nop,$cell[1].$setval)){
                            
        //                     $set=[
        //                         'kd_propinsi'=>$nop[0],
        //                         'kd_dati2'=>$nop[1],
        //                         'kd_kecamatan'=>$nop[2],
        //                         'kd_kelurahan'=>$nop[3],
        //                         'kd_blok'=>$blok_urut[0],
        //                         'no_urut'=>$blok_urut[1],
        //                         'kd_jns_op'=>$nop[5],
        //                         'tahun'=>$setval,
        //                         'alamat_objek'=>$cell[5],
        //                         'wp'=>$cell[7],
        //                         'alamat_wp'=>$cell[8],
        //                         'pbb'=>$cell[9],
        //                         'buku'=>$cell[10],
        //                         'tagihan'=>$cell[$key],
        //                     ];
        //                     $piutang= new Piutang($set);
        //                     $piutang->save();
        //                     // $set=[
        //                     //     'PENAGIHAN_DETAIL_ID'=>$no,
        //                     //     'penagihan_id'=>$id,
        //                     //     'kd_propinsi'=>$nop[0],
        //                     //     'kd_dati2'=>$nop[1],
        //                     //     'kd_kecamatan'=>$nop[2],
        //                     //     'kd_kelurahan'=>$nop[3],
        //                     //     'kd_blok'=>$blok_urut[0],
        //                     //     'no_urut'=>$blok_urut[1],
        //                     //     'kd_jns_op'=>$nop[5],
        //                     //     'tahun'=>$setval,
        //                     //     'alamat_objek'=>$cell[5],
        //                     //     'wp'=>$cell[7],
        //                     //     'alamat_wp'=>$cell[8],
        //                     //     'pbb'=>$cell[9],
        //                     //     'buku'=>$cell[10],
        //                     //     'tagihan'=>$cell[$key],
        //                     // ];
        //                     // $penagihanDetail= new PenagihanDetail($set);
        //                     // $penagihanDetail->save();
        //                     $temp_nop[]=$cell[1].$setval;
        //                     $no++;
        //                 }
        //             }
        //         }
        //     }
        // }
        
        $result=[];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan','asc')->get();
        $kelurahan = [];
        if($kecamatan->count()=='1'){
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
        }
        $tahun_pajak = [];
        $tahun = $request->tahun ?? date('Y');
        $start=2013;
        for($year= $tahun;$year>=$start;$year--){
            $tahun_pajak[$year]=$year;
        }
        return view('penagihan.index', compact('result','kecamatan','tahun_pajak','kelurahan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
