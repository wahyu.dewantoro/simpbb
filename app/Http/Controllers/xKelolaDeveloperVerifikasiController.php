<?php

namespace App\Http\Controllers;

use App\Traits\OcaWablas;
use App\User;
use App\UsulanPengelola;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KelolaDeveloperVerifikasiController extends Controller
{
    use OcaWablas;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = UsulanPengelola::oldest()->wherenull('verifikasi_kode')->paginate();
        // return $data;
        return view('kelola.developer.index_verifikasi', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = UsulanPengelola::findorfail($id);
        return view('kelola.developer.formverifikasi', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            //code...
            $upd = UsulanPengelola::find($id);
            $user_id = $upd->user_id;
            $keterangan=$request->verifikasi_keterangan;

            $upd->update([
                'verifikasi_kode' => $request->verifikasi_kode,
                'verifikasi_by' => Auth()->user()->id,
                'verifikasi_at' => now(),
                'verifikasi_keterangan'=>$keterangan
            ]);

            if ($request->verifikasi_kode == '1') {
                $is_pengelola = 1;
            } else {
                $is_pengelola = null;
            }

            $user = User::find($user_id);
            $user->update([
                'is_pengelola' => $is_pengelola
            ]);

            // dd($user);

            $telepon = $user->telepon ?? '';

            $nama = $user->nama ?? 'Wajib Pajak';
            $flash = [
                'success' => "berhasil di verifikasi"
            ];
            DB::commit();

            if ($request->verifikasi_kode == '1') {
                $this->NotifAcc($telepon, [$nama, '*PERMOHONAN Sebagai WP Developer*']);
            } else {
                // di tolak

                $this->NotifReject($telepon, [$nama, '*PERMOHONAN Sebagai WP Developer*', $keterangan ?? "Dokumen tidak sesuai"]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(route('kelola-developer-verifikasi.index'))->with($flash);
        // return redirect(url('kelola-verifikasi-data'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
