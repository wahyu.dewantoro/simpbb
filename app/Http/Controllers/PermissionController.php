<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:view_permissions')->only(['index']);
        $this->middleware('permission:add_permissions')->only(['create', 'store']);
        $this->middleware('permission:edit_permissions')->only(['edit', 'update']);
        $this->middleware('permission:delete_permissions')->only(['destroy']);
    }
    public function index(Request $request)
    {
        //
        $permissions = Permission::query();
        if ($request->cari) {
            $q = strtolower($request->cari);
            $permissions = $permissions->whereraw("(lower(name) like '%$q%')");
        }
        $permissions = $permissions->paginate(10);
        return view('permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::all();
        $form = (object)[
            'method' => 'post',
            'action' => route('permissions.store'),
            'title' => 'Create Permissions'
        ];
        return view('permissions.form', compact('form', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $this->validate($request, [
            'permission' => 'bail|required|min:2',
            'roles' => 'required|min:1'
        ]);



        DB::beginTransaction();
        try {
            //code...
            $permission = Permission::create(['name' => $request->permission]);
            // dd($permission);
            $roles = $request->get('roles', []);
            foreach ($roles as $role) {
                $rol = Role::where('name', $role)->first();
                $rol->givePermissionTo($permission->name);
            }

            DB::commit();

            $flash = [
                'success' => 'Permission ' . $permission->name . ' telah di tambahkan'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        // dd($flash);

        return redirect(route('permissions.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //

        // dd($permission->roles);
        $roles = Role::all();
        $form = (object)[
            'method' => 'patch',
            'action' => route('permissions.update', $permission),
            'title' => 'Edit Permissions',
            'permission' => $permission
        ];
        return view('permissions.form', compact('form', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        //
        $this->validate($request, [
            'permission' => 'bail|required|min:2',
            'roles' => 'required|min:1'
        ]);



        DB::beginTransaction();
        try {
            //code...
            // revoke all old permissions
            foreach ($permission->roles as $rl) {
                $rl->revokePermissionTo($permission->name);
            }

            $permission = $permission->update(['name' => $request->permission]);

            $roles = $request->get('roles', []);
            foreach ($roles as $role) {
                $rol = Role::where('name', $role)->first();
                $rol->givePermissionTo($request->permission);
            }

            DB::commit();

            $flash = [
                'success' => 'Permission berhasil di update'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $th->getMessage()
            ];
        }


        return redirect(route('permissions.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //

        try {
            foreach ($permission->roles as $rl) {
                $rl->revokePermissionTo($permission->name);
            }

            $permission->delete();
            $flash = ['success' => 'Permission ' . $permission->name . ' telah di hapus'];
        } catch (\Exception $th) {
            //throw $th;
            $flash = ['error' => $th->getMessage()];
        }

        return redirect(route('permissions.index'))->with($flash);
    }
}
