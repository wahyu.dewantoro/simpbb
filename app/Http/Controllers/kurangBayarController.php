<?php

namespace App\Http\Controllers;

use App\Helpers\Piutang;
use App\Models\KurangBayar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class kurangBayarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData($where)
    {


        $data = DB::connection("oracle_satutujuh")->table('sppt')
            ->leftjoin('sppt_potongan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->leftjoin('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
            ->leftjoin('ref_kelurahan', function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan');
            })->select(db::raw("sppt.kd_propinsi,
            sppt.kd_dati2,
            sppt.kd_kecamatan,
            sppt.kd_kelurahan,
            sppt.kd_blok,
            sppt.no_urut,
            sppt.kd_jns_op,
            sppt.thn_pajak_sppt ,
            nm_wp_sppt nm_wp,
            nm_kecamatan,
            nm_kelurahan,
            pbb_yg_harus_dibayar_sppt -  nvl(nilai_potongan,0) pbb,nvl((
            select sum(jml_sppt_yg_dibayar) from pembayaran_sppt
            where pembayaran_sppt.kd_propinsi=sppt.kd_propinsi and
            pembayaran_sppt.kd_dati2=sppt.kd_dati2 and
            pembayaran_sppt.kd_kecamatan=sppt.kd_kecamatan and
            pembayaran_sppt.kd_kelurahan=sppt.kd_kelurahan and
            pembayaran_sppt.kd_blok=sppt.kd_blok and
            pembayaran_sppt.no_urut=sppt.no_urut and
            pembayaran_sppt.kd_jns_op=sppt.kd_jns_op and
            pembayaran_sppt.thn_pajak_sppt=sppt.thn_pajak_sppt 
            ),0) bayar"))->where($where)->first();
        return $data;
    }

    public function listNop()
    {
        return  db::connection("oracle_spo")->select(db::raw("select  kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt
        from spo.sppt_oltp
        where status_pembayaran_sppt='2'
        and thn_pajak_sppt>=2014"));
    }

    public function index(Request $request)
    {

        $data = Piutang::listKurangBayar()->paginate(10);
        // dd($data->lastPage);
        return view('kurang_bayar.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if ($request->ajax()) {
            $where = [
                'sppt.kd_propinsi' => $request->kd_propinsi,
                'sppt.kd_dati2' => $request->kd_dati2,
                'sppt.kd_kecamatan' => $request->kd_kecamatan,
                'sppt.kd_kelurahan' => $request->kd_kelurahan,
                'sppt.kd_blok' => $request->kd_blok,
                'sppt.no_urut' => $request->no_urut,
                'sppt.kd_jns_op' => $request->kd_jns_op,
                'sppt.thn_pajak_sppt' => $request->thn_pajak_sppt,
            ];

            return json_encode($this->getData($where));
        }

        $data = [
            'method' => 'post',
            'action' => route('kurang-bayar.store'),
            'ln' => $this->listNop()
        ];

        // return $data['ln'];
        return view('kurang_bayar.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $obj = splitnop(onlyNumber($request->nop));
        $obj['thn_pajak_sppt'] = substr(onlyNumber($request->nop), 18, 4) ?? '';
        $obj = (object)$obj;
        

        DB::beginTransaction();
        try {
            // cek existing kurang bayar
            $cekjumlah = KurangBayar::where([
                'kd_propinsi' => $obj->kd_propinsi,
                'kd_dati2' => $obj->kd_dati2,
                'kd_kecamatan' => $obj->kd_kecamatan,
                'kd_kelurahan' => $obj->kd_kelurahan,
                'kd_blok' => $obj->kd_blok,
                'no_urut' => $obj->no_urut,
                'kd_jns_op' => $obj->kd_jns_op,
                'thn_pajak_sppt' => $obj->thn_pajak_sppt
            ])->get()->count();
            if ($cekjumlah == 0) {
                $data = [
                    'kd_propinsi' => onlyNumber($obj->kd_propinsi),
                    'kd_dati2' => onlyNumber($obj->kd_dati2),
                    'kd_kecamatan' => onlyNumber($obj->kd_kecamatan),
                    'kd_kelurahan' => onlyNumber($obj->kd_kelurahan),
                    'kd_blok' => onlyNumber($obj->kd_blok),
                    'no_urut' => onlyNumber($obj->no_urut),
                    'kd_jns_op' => onlyNumber($obj->kd_jns_op),
                    'thn_pajak_sppt' => onlyNumber($obj->thn_pajak_sppt),
                    'nilai' => onlyNumber($request->nilai),
                    'created_at' => Carbon::now(),
                    'created_by' => auth()->user()->id
                ];
                KurangBayar::insert($data);
                DB::commit();
                $flash = [
                    'success' => "Berhasil menambah data"
                ];
            } else {
                $flash = [
                    'warning' => "gagal menambahkan data , kerana NOP pada tahun pajak tersebit sudah di entrikan"
                ];
            }
        } catch (\Throwable $th) {
            $flash = ['errorr' => $th->getMessage()];
            db::rollBack();
        }

        return redirect(route('kurang-bayar.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wh = splitnop($id);
        $wh['thn_pajak_sppt'] = substr($id, 18, 4);

        $data = [
            'method' => 'patch',
            'action' => route('kurang-bayar.update', $id),
            'ln' => $this->listNop(),
            'kb' => KurangBayar::where($wh)->first()
        ];
        return view('kurang_bayar.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $wh = splitnop($id);
        $wh['thn_pajak_sppt'] = substr($id, 18, 4);
        
        DB::beginTransaction();
        try {
            
        $obj = splitnop(onlyNumber($request->nop));
        $obj['thn_pajak_sppt'] = substr(onlyNumber($request->nop), 18, 4) ?? '';
        $obj = (object)$obj;

            $data = [
                'kd_propinsi' => onlyNumber($obj->kd_propinsi),
                'kd_dati2' => onlyNumber($obj->kd_dati2),
                'kd_kecamatan' => onlyNumber($obj->kd_kecamatan),
                'kd_kelurahan' => onlyNumber($obj->kd_kelurahan),
                'kd_blok' => onlyNumber($obj->kd_blok),
                'no_urut' => onlyNumber($obj->no_urut),
                'kd_jns_op' => onlyNumber($obj->kd_jns_op),
                'thn_pajak_sppt' => onlyNumber($obj->thn_pajak_sppt),
                'nilai' => onlyNumber($request->nilai),
                'created_at' => Carbon::now(),
                'created_by' => auth()->user()->id
            ];
            KurangBayar::where($wh)->update($data);
            db::commit();
            $flash = ['success' => "Berhasil di update"];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('kurang-bayar.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wh = splitnop($id);
        $wh['thn_pajak_sppt'] = substr($id, 18, 4);
        // return $wh;
        DB::beginTransaction();
        try {
            KurangBayar::where($wh)->delete();
            db::commit();
            $flash = ['success' => "Berhasil di hapus"];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('kurang-bayar.index'))->with($flash);
    }
}
