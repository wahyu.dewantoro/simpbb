<?php

namespace App\Http\Controllers;

use App\Formulir;
use App\FormulirLayanan;
use App\Models\Jenis_layanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class nomorFormulirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahun = date('Y');
        $formulir = Formulir::where('tahun', $tahun)->get();
        return view('formulir.index', compact('formulir', 'tahun'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function jnsPenelitian()
    {
        return [
            1 => 'Penelitian Kantor',
            'Penelitian Lapangan'
        ];
    }

    public function create()
    {
        $data = [
            'title' => 'Tambah Nomor formulir',
            'action' => url('refrensi/formulir'),
            'method' => 'post'
        ];
        $jnsPenelitian = $this->jnsPenelitian();
        $jnsLayanan = Jenis_layanan::orderBy('order', 'asc')->pluck('nama_layanan', 'id')->toArray();
        // dd($jnsLayanan);

        // $formulir->layanan()->pluck('jenis_layanan_id')->toArray()
        return view('formulir.form', compact('data', 'jnsPenelitian', 'jnsLayanan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'penelitian' => 'required',
            'tahun' => 'required|digits:4',
            'bundel_min' => 'required|digits:4',
            'bundel_max' => 'required|digits:4',
            'uraian' => 'required'
        ]);

        $cl = "";

        if (isset($request->jenis_layanans)) {
            $ll = implode(',', $request->jenis_layanans);
            if (!empty($ll)) {
                $cl = "or jenis_layanan_id in (" . $ll . ")";
            }
        }
        $or = "  and penelitian ='".$request->penelitian."'  and ( (bundel_min<='" .  $request->bundel_min . "' and bundel_max>='" .  $request->bundel_max . "'   ) " . $cl . ")";

        $cek = DB::table('ref_formulir')
            ->leftjoin('ref_formulir_layanan', 'ref_formulir.id', '=', 'ref_formulir_layanan.ref_formulir_id')
            ->whereraw("tahun='" . $request->tahun . "' " . $or)
            ->select(DB::raw("count(1) jum"))->first();

        if ($cek->jum == '0') {
            DB::beginTransaction();
            try {
                //code...
                $a = Formulir::create($request->only(['penelitian', 'uraian', 'tahun', 'bundel']));
                if ($request->jenis_layanans) {
                    foreach ($request->jenis_layanans as $jenis_layanan_id) {
                        FormulirLayanan::create(
                            [
                                'ref_formulir_id' => $a->id,
                                'jenis_layanan_id' => $jenis_layanan_id
                            ]
                        );
                    }
                }
                DB::commit();

                $flash = [
                    'success' => 'Data berhasil disimpan'
                ];
            } catch (\Throwable $th) {
                DB::rollBack();
                Log::error($th);
                $flash = [
                    'error' => $th->getMessage()
                ];
            }
            $url = url('refrensi/formulir');
        } else {
            $flash = [
                'error' => 'Data duplikasi.'
            ];
            $url = url('refrensi/formulir/create');
        }
        return redirect($url)->with($flash);
    }


    function cekData($request)
    {
        return $request;
        $ll = implode(',', $request->jenis_layanans);
        $cek = DB::table('ref_formulir')
            ->join('ref_formulir_layanan', 'ref_formulir.id', '=', 'ref_formulir_layanan.ref_formulir_id')
            ->whereraw("tahun='" . $request->tahun . "' and ( (bundel_min<='" .  $request->bundel_min . "' and bundel_max>='" .  $request->bundel_max . "') or jenis_layanan_id in (" . $ll . "))")
            ->select(DB::raw("count(1) jum"))->first();

        return $cek->jum;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formulir = Formulir::findorfail($id);

        

        $data = [
            'title' => 'Edit Nomor formulir',
            'action' => url('refrensi/formulir', $id),
            'method' => 'patch',
            'formulir' => $formulir
        ];
        $jnsPenelitian = $this->jnsPenelitian();
        $jnsLayanan = Jenis_layanan::orderBy('order', 'asc')->pluck('nama_layanan', 'id')->toArray();
        return view('formulir.form', compact('data', 'jnsPenelitian', 'jnsLayanan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'penelitian' => 'required',
            'tahun' => 'required|digits:4',
            'bundel_min' => 'required|digits:4',
            'bundel_max' => 'required|digits:4',
            'uraian' => 'required'
        ]);

        $cl = "";

        if (isset($request->jenis_layanans)) {
            $ll = implode(',', $request->jenis_layanans);
            if (!empty($ll)) {
                $cl = "or jenis_layanan_id in (" . $ll . ")";
            }
        }
        $or = "  and penelitian ='".$request->penelitian."'  and ( (bundel_min<='" .  $request->bundel_min . "' and bundel_max>='" .  $request->bundel_max . "'   ) " . $cl . ")";

        $cek = DB::table('ref_formulir')
            ->leftjoin('ref_formulir_layanan', 'ref_formulir.id', '=', 'ref_formulir_layanan.ref_formulir_id')
            ->whereraw("ref_formulir_id<>'" . $id . "'  and  tahun='" . $request->tahun . "' " . $or)
            ->select(DB::raw("count(1) jum"))->first();

        if ($cek->jum == '0') {
            DB::beginTransaction();
            try {

                // update formulir
                $a = Formulir::find($id)->update($request->only(['penelitian', 'uraian', 'tahun', 'bundel_min', 'bundel_max']));

                // delete layanan
                FormulirLayanan::where('ref_formulir_id', $id)->delete();
                if ($request->jenis_layanans) {
                    

                    foreach ($request->jenis_layanans as $jenis_layanan_id) {
                        FormulirLayanan::create(
                            [
                                'ref_formulir_id' => $id,
                                'jenis_layanan_id' => $jenis_layanan_id
                            ]
                        );
                    }
                }
                DB::commit();

                $flash = [
                    'success' => 'Data berhasil disimpan'
                ];
            } catch (\Throwable $th) {
                DB::rollBack();
                Log::error($th);
                $flash = [
                    'error' => $th->getMessage()
                ];
            }
            $url = url('refrensi/formulir');
        } else {
            $flash = [
                'error' => 'Data duplikasi.'
            ];
            $url = url('refrensi/formulir/' . $id . '/edit');
        }
        return redirect($url)->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formulir = Formulir::findorfail($id);
        // dd($formulir);
        try {
            $formulir->delete();
            $flash = [
                'success' => 'Data berhasil dihapus'
            ];
        } catch (\Throwable $th) {
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect('refrensi/formulir')->with($flash);
    }
}
