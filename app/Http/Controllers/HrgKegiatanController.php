<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SnappyPdf as PDF;

class HrgKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function coreSql($tahun)
    {
        return DB::connection("oracle_satutujuh")->table('pekerjaan')
            ->select(db::raw("pekerjaan.kd_pekerjaan,
                        nm_pekerjaan,
                        pekerjaan_kegiatan.kd_kegiatan,
                        nm_kegiatan,
                         hrg_satuan"))
            ->leftJoin('pekerjaan_kegiatan', 'pekerjaan_kegiatan.kd_pekerjaan', '=', 'pekerjaan.kd_pekerjaan')
            ->leftJoin('hrg_satuan', function ($join) {
                $join->on('hrg_satuan.kd_pekerjaan', '=', 'pekerjaan_kegiatan.kd_pekerjaan')
                    ->on('hrg_satuan.kd_kegiatan', '=', 'pekerjaan_kegiatan.kd_kegiatan');
            })->where('thn_hrg_satuan', '=', $tahun)
            ->orderby('pekerjaan.kd_pekerjaan')
            ->get()->toArray();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = $this->coreSql($tahun);
            $grouped = collect($data)->groupBy('nm_pekerjaan');
            return view('ssh.satuan._index', compact('grouped'));
        }
        return view('ssh.satuan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportPdf(Request $request)
    {
        $tahun = $request->tahun;
        $data = $this->coreSql($tahun);
        $grouped = collect($data)->groupBy('nm_pekerjaan');
        $pages = view('ssh.satuan.exportPdf', compact('grouped', 'tahun'));
        $pdf = pdf::loadHTML($pages)
            ->setOption('enable-local-file-access', true)
            ->setPaper('A4');

        return $pdf->download();
    }
}
