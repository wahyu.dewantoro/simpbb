<?php

namespace App\Http\Controllers;

use App\Exports\LaporanPenelitianUserExcel;
use App\Exports\PenolakanExcel;
use App\Helpers\gallade;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\Layanan_objek;
use App\Models\Notaperhitungan;
use App\Models\SuratKeputusan;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Excel as ExcelExcel;
// use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel;

use SnappyPdf as PDF;

class LaporanPenelitian extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        $result = [];
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit')->first();
        $listUser = User::whereRaw(DB::raw("user_unit_kerja.kd_unit like '" . $user . "%'"))
            ->leftJoin('user_unit_kerja', function ($join) {
                $join->On('user_unit_kerja.user_id', '=', 'users.id');
            })
            ->orderBy('users.nama')
            ->get();
        // $Jenis_layanan = Jenis_layanan::all();

        $Jenis_layanan = DB::select(db::raw("select id,nama_layanan
            from jenis_layanan
            union
            select distinct jenis_layanan_id,jenis_layanan_nama
            from layanan
            where lower(jenis_layanan_nama) like '%online%'
            order by id asc"));
        return view('laporan_penelitian.user', compact('result', 'listUser', 'Jenis_layanan'));
    }
    public function user_search(Request $request)
    {
        $select = [
            // 'layanan_objek.kd_propinsi',
            // 'layanan_objek.kd_dati2',
            // 'layanan_objek.kd_kecamatan',
            // 'layanan_objek.kd_kelurahan',
            // 'layanan_objek.kd_blok',
            // 'layanan_objek.no_urut',
            // 'layanan_objek.kd_jns_op',
            DB::raw('TBL_SPOP.NOP_PROSES nop'),
            'layanan_objek.nomor_layanan',
            'layanan_objek.alamat_op',
            DB::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            db::raw('case when tbl_spop.luas_bumi is null then layanan_objek.luas_bumi else tbl_spop.luas_bumi end luas_bumi'),
            db::raw('case when tbl_lspop.luas_bng is null then layanan_objek.luas_bng else tbl_lspop.luas_bng end luas_bng'),
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            DB::raw("nvl(nama_pendek,username) peneliti"),
            db::raw("tbl_spop.nm_wp NAMA_WP")
        ];
        $dataSet = Layanan_objek::select($select);
        $listUser = $request->only('list_user');
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $dataSet->where(['layanan_objek.pemutakhiran_by' => $listUser['list_user']]);
            }
        }

        if ($request->layanan) {
            $setLayanan = $request->layanan;
            if ($setLayanan != '-') {
                $aexp = explode('|', $setLayanan);
                $dataSet->where('layanan.jenis_layanan_id', $aexp[0]);
                if ($aexp[1] == 'Pembetulan Data WP (online)') {
                    $dataSet->where(['layanan.jenis_layanan_nama' => $aexp[1]]);
                }
            }
        }

        /* if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $exp = explode('#', $request->layanan);
                $dataSet->where('layanan.jenis_layanan_id' , $exp[0]);
                if ($exp[1] == 'Pembetulan Data WP (online)') {
                    $dataSet->where(['layanan.jenis_layanan_nama' => $exp[1]]);
                }
            }
        } */
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['created_by' => Auth()->user()->id]);
        }
        $joinA = db::raw("layanan_objek.id AND tbl_spop.no_formulir=layanan_objek.nomor_formulir");
        $joinB = db::raw("layanan_objek.id AND tbl_lspop.no_formulir=layanan_objek.nomor_formulir");
        $dataSet = $dataSet->join('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join('tbl_spop', 'tbl_spop.layanan_objek_id', '=', $joinA, "left outer")
            ->join('tbl_lspop', 'tbl_lspop.layanan_objek_id', '=', $joinB, "left outer")
            ->join('jenis_layanan', function ($join) {
                $join->On('layanan.jenis_layanan_id', '=', 'jenis_layanan.id');
            })
            ->join('users', 'users.id', '=', 'layanan_objek.pemutakhiran_by')
            ->whereraw("layanan_objek.pemutakhiran_at is not null 
                        AND layanan.deleted_at is null
                        AND layanan_objek.is_tolak is null
                ")
            ->whereraw("(layanan_objek.nop_gabung IS NULL
                OR (layanan_objek.nop_gabung IS NOT NULL
                    AND layanan.jenis_layanan_id = 6))")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->editColumn('nop', function ($data) {
            return formatnop($data['nop']);
        })
            ->addIndexColumn()
            ->make(true);
    }
    public function user_cetak(Request $request)
    {

        // return $request->all();
        $output = $request->output ?? 'pdf';

        $listUser = $request->only('list_user');
        $userDefault = '-';
        $select = [

            DB::raw('TBL_SPOP.NOP_PROSES nop'),
            'layanan_objek.nomor_layanan',
            'layanan_objek.alamat_op',
            DB::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            db::raw('case when tbl_spop.luas_bumi is null then layanan_objek.luas_bumi else tbl_spop.luas_bumi end luas_bumi'),
            db::raw('case when tbl_lspop.luas_bng is null then layanan_objek.luas_bng else tbl_lspop.luas_bng end luas_bng'),
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            DB::raw("nvl(nama_pendek,username) peneliti"),
            db::raw("tbl_spop.nm_wp NAMA_WP")
        ];
        $dataSet = Layanan_objek::select($select);
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $userDefault = User::where(['id' => $listUser['list_user']])->pluck('nama')->first();
                $dataSet->where(['layanan_objek.pemutakhiran_by' => $listUser['list_user']]);
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['layanan.jenis_layanan_id' => onlyNumber($setLayanan['layanan'])]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['created_by' => Auth()->user()->id]);
        }
        $joinA = db::raw("layanan_objek.id AND tbl_spop.no_formulir=layanan_objek.nomor_formulir");
        $joinB = db::raw("layanan_objek.id AND tbl_lspop.no_formulir=layanan_objek.nomor_formulir");
        $dataSet = $dataSet->join('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join('tbl_spop', 'tbl_spop.layanan_objek_id', '=', $joinA, "left outer")
            ->join('tbl_lspop', 'tbl_lspop.layanan_objek_id', '=', $joinB, "left outer")
            ->join('jenis_layanan', function ($join) {
                $join->On('layanan.jenis_layanan_id', '=', 'jenis_layanan.id');
            })
            ->join('users', 'users.id', '=', 'layanan_objek.pemutakhiran_by')
            ->whereraw("layanan_objek.pemutakhiran_at is not null 
                        AND layanan.deleted_at is null
                        AND layanan_objek.is_tolak is null
                ")
            ->whereraw("(layanan_objek.nop_gabung IS NULL
                OR (layanan_objek.nop_gabung IS NOT NULL
                    AND layanan.jenis_layanan_id = 6))")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();

        if ($output == 'xlsx') {
            $arrayData = [];
            $no = 1;
            foreach ($dataSet as $item) {
                $arrayData[] = [
                    'no' => $no,
                    'nopel' => $item->nomor_layanan,
                    'nop' => formatnop($item->nop),
                    'wajib_pajak' => $item->nama_wp,
                    'alamat_op' => $item->alamat_op,
                    'jenis_layanan' => $item->jenis_layanan_nama,
                    'luas_bumi' => $item->luas_bumi,
                    'luas_bng' => $item->luas_bng,
                    'peneliti' => $item->peneliti
                ];

                $no++;
            }

            return Excel::download(new LaporanPenelitianUserExcel($arrayData), 'laporan.xlsx');
        } else {

            $result = [
                'nama' => $userDefault,
                'tanggal' => $tanggal,
                'data' => $dataSet
            ];

            $pages = View::make('laporan_penelitian.cetak.user', compact('result'));
            $pdf = pdf::loadHTML($pages)
                ->setPaper('A4');
            $pdf->setOption('enable-local-file-access', true);
            return $pdf->stream();
        }
    }
    public function kelurahan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $Jenis_layanan = Jenis_layanan::all();
        return view('laporan_penelitian.kelurahan', compact('result', 'kecamatan', 'kelurahan', 'Jenis_layanan'));
    }
    public function kelurahan_search(Request $request)
    {
        $select = [
            DB::raw('TBL_SPOP.NOP_PROSES nop'),
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            DB::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            db::raw('case when tbl_spop.luas_bumi is null then layanan_objek.luas_bumi else tbl_spop.luas_bumi end luas_bumi'),
            db::raw('case when tbl_lspop.luas_bng is null then layanan_objek.luas_bng else tbl_lspop.luas_bng end luas_bng'),
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['layanan.jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $onJ = db::raw('layanan_objek.kd_kecamatan and ref_kelurahan.kd_kelurahan=layanan_objek.kd_kelurahan');
        $dataSet = $dataSet->leftJoin('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
            ->leftJoin('tbl_spop', 'tbl_spop.layanan_objek_id', '=', 'layanan_objek.id')
            ->leftJoin('tbl_lspop', 'tbl_lspop.layanan_objek_id', '=', 'layanan_objek.id')
            ->leftJoin('jenis_layanan', 'layanan.jenis_layanan_id', '=', 'jenis_layanan.id')
            ->leftJoin('users', 'users.id', '=', 'layanan_objek.pemutakhiran_by')
            ->leftJoin(db::raw("spo.ref_kecamatan ref_kecamatan"), 'ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
            ->leftJoin(db::raw("spo.ref_kelurahan ref_kelurahan"), 'ref_kelurahan.kd_kecamatan', '=', $onJ)
            ->whereraw("layanan_objek.pemutakhiran_at is not null ")
            ->whereraw("layanan.deleted_at is null")
            ->whereraw("(layanan_objek.nop_gabung IS NULL
                OR (layanan_objek.nop_gabung IS NOT NULL
                    AND layanan.jenis_layanan_id = 6))")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->editColumn('nop', function ($data) {
            return formatnop($data['nop']);
        })
            ->addIndexColumn()
            ->make(true);
    }
    public function kelurahan_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = [
            DB::raw('TBL_SPOP.NOP_PROSES nop'),
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            DB::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            db::raw('case when tbl_spop.luas_bumi is null then layanan_objek.luas_bumi else tbl_spop.luas_bumi end luas_bumi'),
            db::raw('case when tbl_lspop.luas_bng is null then layanan_objek.luas_bng else tbl_lspop.luas_bng end luas_bng'),
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        $kdKecamatan = "-";
        $kdKelurahan = "-";
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['layanan.jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        // $User=Auth()->user();
        // $user=$User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        // if($user->count()=='1'&&$user->first()!='3507'){
        //     $dataSet->where(['created_by'=>Auth()->user()->id]);
        // }
        $onJ = db::raw('layanan_objek.kd_kecamatan and ref_kelurahan.kd_kelurahan=layanan_objek.kd_kelurahan');
        $dataSet = $dataSet->leftJoin('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
            ->leftJoin('tbl_spop', 'tbl_spop.layanan_objek_id', '=', 'layanan_objek.id')
            ->leftJoin('tbl_lspop', 'tbl_lspop.layanan_objek_id', '=', 'layanan_objek.id')
            ->leftJoin('jenis_layanan', 'layanan.jenis_layanan_id', '=', 'jenis_layanan.id')
            ->leftJoin('users', 'users.id', '=', 'layanan_objek.pemutakhiran_by')
            ->leftJoin(db::raw("spo.ref_kecamatan ref_kecamatan"), 'ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
            ->leftJoin(db::raw("spo.ref_kelurahan ref_kelurahan"), 'ref_kelurahan.kd_kecamatan', '=', $onJ)
            ->whereraw("layanan_objek.pemutakhiran_at is not null ")
            ->whereraw("layanan.deleted_at is null")
            ->whereraw("(layanan_objek.nop_gabung IS NULL
                OR (layanan_objek.nop_gabung IS NOT NULL
                    AND layanan.jenis_layanan_id = 6))")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('laporan_penelitian.cetak.kelurahan',compact('result'));
        $pages = View::make('laporan_penelitian.cetak.kelurahan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function bulan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun = date('Y');
        $start = 2013;
        $tahun_pajak = [];
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }
        $Jenis_layanan = Jenis_layanan::all();
        return view('laporan_penelitian.bulan', compact('result', 'kecamatan', 'kelurahan', 'tahun_pajak', 'Jenis_layanan'));
    }
    public function bulan_total(Request $request)
    {
        try {
            // $select=[
            //     DB::raw('count(1) penelitian'),
            //     DB::raw('count(1) luas_bumi'),
            //     DB::raw('count(1) luas_bangunan'),
            // ];
            $group = [
                "layanan_objek.kd_propinsi"
            ];
            $select = array_merge($group, [db::raw('count(*) penelitian,sum(layanan_objek.luas_bumi) luas_bumi,sum(layanan_objek.luas_bng) luas_bng')]);
            $dataSet = $this->search_bulanan($request, $group, $select)->first();
            $resultSet = Collect($dataSet->only(['penelitian', 'luas_bumi', 'luas_bng']))->transform(function ($res) {
                return gallade::parseQuantity($res);
            });
            $result = gallade::generateinTbody([$resultSet->toArray()], "Data Masih Kosong", 9);
            return response()->json(["data" => $result, "status" => true]);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    public function search_bulanan($request, $group, $select)
    {
        $dataSet = Jenis_layanan::select($select);
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['layanan.jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $search = $request->only(['kecamatan', 'kelurahan', "tahun", "bulan"]);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
            if ($search['tahun'] != "-" && $search['tahun'] != "") {
                $dataSet->whereRaw(Db::raw('EXTRACT(YEAR FROM layanan_objek.pemutakhiran_at)=' . $search['tahun']));
            }
            if ($search['bulan'] != "-" && $search['bulan'] != "") {
                $dataSet->whereRaw(Db::raw('EXTRACT(month FROM layanan_objek.pemutakhiran_at)=' . $search['bulan']));
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $dataSet = $dataSet->Join('layanan', function ($join) {
            $join->On('layanan.jenis_layanan_id', '=', 'jenis_layanan.id');
        })->Join('layanan_objek', function ($join) {
            $join->On('layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan_objek.pemutakhiran_at is not null")
            ->whereraw("layanan.deleted_at is null")
            ->groupBy($group)
            ->get();
        return $dataSet;
    }
    public function bulan_search(Request $request)
    {
        $group = [
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            "layanan.jenis_layanan_id",
            'layanan.jenis_layanan_nama',
        ];
        $select = array_merge($group, [db::raw('count(*) total,sum(layanan_objek.luas_bumi) luas_bumi,sum(layanan_objek.luas_bng) luas_bng')]);
        $dataSet = $this->search_bulanan($request, $group, $select);
        $datatables = DataTables::of($dataSet);
        return $datatables->addIndexColumn()
            ->make(true);
    }
    public function bulan_cetak(Request $request)
    {
        $group = [
            'layanan.jenis_layanan_nama',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
        ];
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = array_merge($group, [db::raw('count(*) total,sum(layanan_objek.luas_bumi) luas_bumi,sum(layanan_objek.luas_bng) luas_bng')]);
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan', "tahun", "bulan"]);
        $tahun = $search['tahun'];
        $bulan = 'Semua';
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['layanan.jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
            if ($search['tahun'] != "-" && $search['tahun'] != "") {
                $dataSet->whereRaw(Db::raw('EXTRACT(YEAR FROM layanan_objek.pemutakhiran_at)=' . $search['tahun']));
            }
            if ($search['bulan'] != "-" && $search['bulan'] != "") {
                $bulan = gallade::getMonth($search['bulan']);
                $dataSet->whereRaw(Db::raw('EXTRACT(month FROM layanan_objek.pemutakhiran_at)=' . $search['bulan']));
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                    $kelurahanDefault = $kecamatan->first()->nm_kelurahan;
                }
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan_objek.pemutakhiran_at is not null")
            ->whereraw("layanan.deleted_at is null")
            ->groupBy($group)
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'data' => $dataSet
        ];
        // return view('laporan_penelitian.cetak.bulan',compact('result'));
        $pages = View::make('laporan_penelitian.cetak.bulan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }

    public function nota_perhitungan()
    {
        $result = [];
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit')->first();
        $listUser = User::whereRaw(DB::raw("user_unit_kerja.kd_unit like '" . $user . "%'"))
            ->leftJoin('user_unit_kerja', function ($join) {
                $join->On('user_unit_kerja.user_id', '=', 'users.id');
            })
            ->orderBy('users.nama')
            ->get();
        return view('laporan_penelitian.nota_perhitungan', compact('result', 'listUser'));
    }
    public function nota_perhitungan_search(Request $request)
    {
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',

            'layanan.deleted_at',
            'layanan_objek.nomor_layanan',
            'layanan.jenis_layanan_nama',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            'layanan_objek.nop_gabung',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            db::raw("users.nama userdel")

        ];
        $dataSet = Layanan_objek::select($select);
        $listUser = $request->only('list_user');
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $dataSet->where(['layanan.deleted_by' => $listUser['list_user']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.deleted_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.deleted_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['layanan.deleted_by' => Auth()->user()->id]);
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->join('users', function ($join) {
                $join->On('users.id', '=', 'layanan.deleted_by');
            }, "left outer join")
            ->whereraw("layanan.deleted_at is not null")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                if ($data->jenis_layanan_id == '7') {
                    return "-- NOP GABUNG ---";
                }
            } else {
                if ($data->jenis_layanan_id == '6') {
                    return "-- NOP PECAH ---";
                }
            }
            if ($data->jenis_layanan_id == '1') {
                return "-- NOP BARU ---";
            }
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->addColumn('alamat', function ($data) {
                return $data['nm_kelurahan'] . "/" . $data['nm_kecamatan'];
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function nota_perhitungan_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $userDefault = '-';
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',

            'layanan.deleted_at',
            'layanan_objek.nomor_layanan',
            'layanan.jenis_layanan_nama',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            'layanan_objek.nop_gabung',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            db::raw("users.nama userdel")

        ];
        $dataSet = Layanan_objek::select($select);
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $userDefault = User::where(['id' => $listUser['list_user']])->pluck('nama')->first();
                $dataSet->where(['layanan.deleted_by' => $listUser['list_user']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.deleted_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.deleted_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['created_by' => Auth()->user()->id]);
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->join('users', function ($join) {
                $join->On('users.id', '=', 'layanan.deleted_by');
            }, "left outer join")
            ->whereraw("layanan.deleted_at is not null")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'nama' => $userDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('laporan_penelitian.cetak.nota_perhitungan',compact('result'));
        $pages = View::make('laporan_penelitian.cetak.nota_perhitungan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function belum_bayar()
    {
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }

        return view('laporan_penelitian.belum_bayar', compact('kecamatan', 'kelurahan'));
    }


    public function sudah_bayar()
    {
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }

        return view('laporan_penelitian.sudah_bayar', compact('kecamatan', 'kelurahan'));
    }
    public function sudah_bayar_search(Request $request)
    {
        $select = [
            'sppt_penelitian.kd_propinsi',
            'sppt_penelitian.kd_dati2',
            'sppt_penelitian.kd_kecamatan',
            'sppt_penelitian.kd_kelurahan',
            'sppt_penelitian.kd_blok',
            'sppt_penelitian.no_urut',
            'sppt_penelitian.kd_jns_op',
            'layanan.tanggal_layanan',
            'layanan_objek.nomor_layanan',
            'layanan.jenis_layanan_nama',
            'layanan_objek.nama_wp',
            'layanan_objek.telp_wp',
            'layanan_objek.alamat_op',
            'layanan_objek.nop_gabung',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            "billing_kolektif.nominal",
            "data_billing.kd_status",
            "data_billing.tgl_bayar",
            "data_billing.kobil"
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }

        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $layanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($layanan))) {
            if ($layanan['layanan'] != "-") {
                $dataSet->whereraw("layanan.jenis_layanan_id='" . $layanan['layanan'] . "'");
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            // $dataSet->where(['layanan.created_by'=>Auth()->user()->id]);
        }
        $joinB = DB::raw("(select data_billing_id, sum (total) nominal 
                from billing_kolektif 
                where deleted_at is null 
                group by data_billing_id ) billing_kolektif");
        $joinsppt = db::raw("(select distinct data_billing_id,nomor_formulir,
        sppt_penelitian.kd_propinsi,
        sppt_penelitian.kd_dati2,
        sppt_penelitian.kd_kecamatan,
        sppt_penelitian.kd_kelurahan,
        sppt_penelitian.kd_blok,
        sppt_penelitian.no_urut,
        sppt_penelitian.kd_jns_op  from sppt_penelitian) sppt_penelitian");

        $dataSet = $dataSet->join('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->join(
                $joinsppt,
                'sppt_penelitian.nomor_formulir',
                '=',
                'layanan_objek.nomor_formulir'
            )
            ->join('data_billing', 'data_billing.data_billing_id', '=', 'sppt_penelitian.data_billing_id')
            ->join($joinB, 'billing_kolektif.data_billing_id', '=', 'data_billing.data_billing_id') //,"left outer"
            ->whereraw("layanan.deleted_at is null and data_billing.kd_status='1'")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        // dd($dataSet->toArray());
        $datatables = DataTables::of($dataSet);
        return $datatables->editColumn('nop', function ($data) {
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->addColumn('raw_tag', function ($data) {
                return '';
                // $ket = "Belum dibayar";
                // if ($data->kd_status == '1') {
                //     $ket = "Telah dibayar pada " . tglIndo($data->tgl_bayar);
                // }
                // return '<small>' . $ket . '</small>';
            })
            ->addColumn('nominal', function ($data) {
                return angka($data->nominal);
            })
            ->addColumn('alamat', function ($data) {
                return $data['nm_kelurahan'] . "/" . $data['nm_kecamatan'];
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function sudah_bayar_cetak(Request $request)
    {
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $jlDefault = 'Semua';
        $select = [
            'sppt_penelitian.kd_propinsi',
            'sppt_penelitian.kd_dati2',
            'sppt_penelitian.kd_kecamatan',
            'sppt_penelitian.kd_kelurahan',
            'sppt_penelitian.kd_blok',
            'sppt_penelitian.no_urut',
            'sppt_penelitian.kd_jns_op',
            'layanan.tanggal_layanan',
            'layanan_objek.nomor_layanan',
            'layanan.jenis_layanan_nama',
            'layanan_objek.nama_wp',
            'layanan_objek.telp_wp',
            'layanan_objek.alamat_op',
            'layanan_objek.nop_gabung',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            "billing_kolektif.nominal",
            "data_billing.kd_status",
            "data_billing.tgl_bayar",
            "data_billing.kobil"
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        $layanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($layanan))) {
            if ($layanan['layanan'] != "-") {
                $dataSet->whereraw("layanan.jenis_layanan_id='" . $layanan['layanan'] . "'");
                $jlDefault = Jenis_layanan::where(['id' => $layanan['layanan']])->get()->first()->nama_layanan;
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            // $dataSet->where(['layanan.created_by'=>Auth()->user()->id]);
        }
        $joinB = DB::raw("(select data_billing_id, sum (total) nominal 
                from billing_kolektif 
                where deleted_at is null 
                group by data_billing_id ) billing_kolektif");
        $joinsppt = db::raw("(select distinct data_billing_id,nomor_formulir,
        sppt_penelitian.kd_propinsi,
        sppt_penelitian.kd_dati2,
        sppt_penelitian.kd_kecamatan,
        sppt_penelitian.kd_kelurahan,
        sppt_penelitian.kd_blok,
        sppt_penelitian.no_urut,
        sppt_penelitian.kd_jns_op  from sppt_penelitian) sppt_penelitian");

        $dataSet = $dataSet->join('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->join(
                $joinsppt,
                'sppt_penelitian.nomor_formulir',
                '=',
                'layanan_objek.nomor_formulir'
            )
            ->join('data_billing', 'data_billing.data_billing_id', '=', 'sppt_penelitian.data_billing_id')
            ->join($joinB, 'billing_kolektif.data_billing_id', '=', 'data_billing.data_billing_id') //,"left outer"
            ->whereraw("layanan.deleted_at is null and data_billing.kd_status='1'")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'jenis_layanan' => $jlDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('laporan_penelitian.cetak.sudah_bayar',compact('result'));
        $pages = View::make('laporan_penelitian.cetak.sudah_bayar', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function penolakan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('laporan_penelitian.penolakan', compact('result', 'kecamatan', 'kelurahan'));
    }
    public function penolakan_search(Request $request)
    {
        $select = [
            'layanan_objek.id',
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            "layanan_objek.keterangan_tolak",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas"),
            db::raw("peneliti.nama petugaspeneliti"),
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->leftJoin(db::raw('users peneliti'), function ($join) {
                $join->On('peneliti.id', '=', 'layanan_objek.pemutakhiran_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null and layanan_objek.is_tolak is not null");
        // ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'));

        if ($request->has('jenis_layanan')) {
            $jenis_layanan_id = $request->jenis_layanan;
            $dataSet = $dataSet->whereraw("layanan.jenis_layanan_id='" . $jenis_layanan_id . "'");
        }


        $dataSet = $dataSet->orderby("layanan_objek.pemutakhiran_at", "desc")->get();
        // echo $dataSet->toSql();
        // dd(['z']);
        $datatables = DataTables::of($dataSet);
        return $datatables
            ->addColumn('nomor_sk', function ($data) {
                $nomor_sk = '';
                if ($data->jenis_layanan_id == '4') {
                    $nm = SuratKeputusan::where('layanan_objek_id', $data->id)->first();
                    if ($nm) {
                        $nomor_sk = $nm->nomor;
                    }
                }
                return $nomor_sk;
            })
            ->addColumn('cetak_sk', function ($data) {

                $url = url('sk') . '/' . acak($data->id);
                return $url;
            })
            ->addColumn('nop', function ($data) {
                if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                    if ($data->jenis_layanan_id == '7') {
                        return "-- NOP GABUNG ---";
                    }
                } else {
                    if ($data->jenis_layanan_id == '6') {
                        return "-- NOP PECAH ---";
                    }
                }
                if ($data->jenis_layanan_id == '1') {
                    return "-- NOP BARU ---";
                }
                return $data['kd_propinsi'] . "." .
                    $data['kd_dati2'] . "." .
                    $data['kd_kecamatan'] . "." .
                    $data['kd_kelurahan'] . "." .
                    $data['kd_blok'] . "-" .
                    $data['no_urut'] . "." .
                    $data['kd_jns_op'];
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function penolakan_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            "layanan_objek.keterangan_tolak",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas"),
            db::raw("peneliti.nama petugaspeneliti"),
            db::raw("surat_keputusan.nomor nomor_sk"),
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        $kdKecamatan = "-";
        $kdKelurahan = "-";
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kecamatan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        // $User=Auth()->user();
        // $user=$User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        // if($user->count()=='1'&&$user->first()!='3507'){
        //     $dataSet->where(['created_by'=>Auth()->user()->id]);
        // }

        if ($request->has('jenis_layanan')) {
            $jenis_layanan_id = $request->jenis_layanan;
            $dataSet = $dataSet->whereraw("layanan.jenis_layanan_id='" . $jenis_layanan_id . "'");
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->leftJoin(db::raw('users peneliti'), function ($join) {
                $join->On('peneliti.id', '=', 'layanan_objek.pemutakhiran_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->leftjoin("surat_keputusan", function ($join) {
                $join->on("surat_keputusan.layanan_objek_id", "=", "layanan_objek.id");
                // ->whereraw("")
            })
            ->whereraw("layanan.deleted_at is null and layanan_objek.is_tolak is not null")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        if ($request->has('mode')) {
            $mode = $request->mode;
            if ($mode == 'excel') {
                // dd($dataSet);
                return Excel::download(new PenolakanExcel($dataSet), 'repot penolakan.xlsx');
            }
        }

        // return view('laporan_penelitian.cetak.penolakan',compact('result'));
        $pages = View::make('laporan_penelitian.cetak.penolakan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            // ->setPaper('letter', 'landscape')
            ->setPaper('A4', 'landscape');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function permohonan_tanpa_nota()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('laporan_penelitian.permohonan_tanpa_nota', compact('result', 'kecamatan', 'kelurahan'));
    }
    public function permohonan_tanpa_nota_search(Request $request)
    {
        $select = [
            DB::raw('TBL_SPOP.NOP_PROSES nop'),
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            DB::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            db::raw('case when tbl_spop.luas_bumi is null then layanan_objek.luas_bumi else tbl_spop.luas_bumi end luas_bumi'),
            db::raw('case when tbl_lspop.luas_bng is null then layanan_objek.luas_bng else tbl_lspop.luas_bng end luas_bng'),
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $dataSet = $dataSet->join('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join('tbl_spop', function ($join) {
                $join->On('tbl_spop.layanan_objek_id', '=', 'layanan_objek.id');
            })
            ->join('tbl_lspop', function ($join) {
                $join->On('tbl_lspop.layanan_objek_id', '=', 'layanan_objek.id');
            })
            ->join('jenis_layanan', function ($join) {
                $join->On('layanan.jenis_layanan_id', '=', 'jenis_layanan.id');
                // ->On('tbl_lspop.no_formulir', '=', 'layanan_objek.nomor_formulir');
            })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan_objek.pemutakhiran_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan_objek.pemutakhiran_at is not null")
            ->whereraw("layanan.deleted_at is null")
            // ->whereraw("(layanan_objek.nop_gabung IS NULL
            // OR (layanan_objek.nop_gabung IS NOT NULL
            //     AND layanan.jenis_layanan_id = 6))")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->editColumn('nop', function ($data) {
            return formatnop($data['nop']);
        })
            ->addIndexColumn()
            ->make(true);
    }
    public function permohonan_tanpa_nota_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = [
            DB::raw('TBL_SPOP.NOP_PROSES nop'),
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            DB::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            db::raw('case when tbl_spop.luas_bumi is null then layanan_objek.luas_bumi else tbl_spop.luas_bumi end luas_bumi'),
            db::raw('case when tbl_lspop.luas_bng is null then layanan_objek.luas_bng else tbl_lspop.luas_bng end luas_bng'),
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        $kdKecamatan = "-";
        $kdKelurahan = "-";
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan_objek.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan_objek.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan_objek.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        // $User=Auth()->user();
        // $user=$User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        // if($user->count()=='1'&&$user->first()!='3507'){
        //     $dataSet->where(['created_by'=>Auth()->user()->id]);
        // }
        $dataSet = $dataSet->join('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join('tbl_spop', function ($join) {
                $join->On('tbl_spop.layanan_objek_id', '=', 'layanan_objek.id');
            })
            ->join('tbl_lspop', function ($join) {
                $join->On('tbl_lspop.layanan_objek_id', '=', 'layanan_objek.id');
            })
            ->join('jenis_layanan', function ($join) {
                $join->On('layanan.jenis_layanan_id', '=', 'jenis_layanan.id');
                // ->On('tbl_lspop.no_formulir', '=', 'layanan_objek.nomor_formulir');
            })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan_objek.pemutakhiran_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan_objek.pemutakhiran_at is not null ")
            ->whereraw("layanan.deleted_at is null")
            ->whereraw("(layanan_objek.nop_gabung IS NULL
                OR (layanan_objek.nop_gabung IS NOT NULL
                    AND layanan.jenis_layanan_id = 6))")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('laporan_penelitian.cetak.permohonan_tanpa_nota',compact('result'));
        $pages = View::make('laporan_penelitian.cetak.permohonan_tanpa_nota', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
