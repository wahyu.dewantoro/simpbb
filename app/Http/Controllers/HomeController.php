<?php

namespace App\Http\Controllers;

// use Dompdf\Image\Cache;

use App\KelolaUsulan;
use App\User;
use App\UserRayon;
use App\UsulanPengelola;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('permission:home.index')->only(['index']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = Auth()->user();
        if ($user->is_wp()) {
            return redirect(url('users') . '/' . auth()->user()->id);
        }



        $verifikasi = [];
        $is_admin = $user->is_admin();
        if ($is_admin == true) {
            $verifikasi['rayon'] = UserRayon::Verifikasi()->count();
            $verifikasi['developer'] = UsulanPengelola::Verifying()->count();
            $verifikasi['objek'] = KelolaUsulan::Verifying()->count();
            $verifikasi['wajib_pajak'] = User::select('users.*', 'roles.name as role_name')
                ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                ->where('roles.name', 'Wajib Pajak')
                ->whereraw("wp_verifikasi_kode is null")
                // ->where('wp_verifikasi_kode','!)
                ->count();
            // return $verifikasi;
        }

        return view('home', compact('verifikasi', 'is_admin'));
    }

    public function createPDF(Request $request)
    {
        // set certificate file
        $certificate = 'file://' . base_path() . '/public/kaban.crt';

        // set additional information in the signature
        $info = array(
            'Name' => 'KEPALA BADAN PENDAPATAN MALANG',
            'Location' => 'KEPANJEN - MALANG',
            'Reason' => 'Digital Signature',
            'ContactInfo' => 'http://www.sipanji.id',
        );

        // set document signature
        PDF::setSignature($certificate, $certificate, 'SIPANJI', '', 2, $info, 'A');

        PDF::SetFont('helvetica', '', 12);
        PDF::SetTitle('Hello World');
        PDF::AddPage();

        // print a line of text
        $text = view('tcpdf');

        // add view content
        PDF::writeHTML($text, true, 0, true, 0);

        // add image for signature
        PDF::Image('sipanji.png', 180, 60, 15, 15, 'PNG');

        // define active area for signature appearance
        PDF::setSignatureAppearance(180, 60, 15, 15);

        // save pdf file
        // public_path('ds_'.time().'.pdf'), 'F'
        PDF::Output('ds_' . time() . '.pdf', 'D');

        PDF::reset();

        dd('pdf created');
    }
}
