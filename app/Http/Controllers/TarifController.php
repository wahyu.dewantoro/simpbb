<?php

namespace App\Http\Controllers;

use App\Helpers\gallade;
use App\Models\JenisPengurangan;
use App\Models\Tarif;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class TarifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Tarif';
        $data = [];
        $nowYear=Carbon::now()->year;
        return view('tarif.index', compact('title', 'data','nowYear'));
    }

    public function tarif_search(){
        $select=[
            'thn_awal',
            'thn_akhir',
            'kd_propinsi',
            'kd_dati2',
            'njop_min',
            'njop_max',
            'nilai_tarif',
        ];
        $dataSet=Tarif::select($select)
            ->whereRaw(DB::raw("kd_propinsi='35' order by thn_awal desc,thn_akhir desc,njop_min desc"))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('raw_tag',function($q){
                $btnDelete = gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Delete" data-delete=""','danger');
                $btn='<div class="btn-group">'.$btnDelete."</div>";
                return '-';
            })->editColumn('nilai_tarif',function($q){
                return number_format($q->nilai_tarif,strlen($q->nilai_tarif)-1);
            })->editColumn('njop_min',function($q){
                return gallade::parseQuantity($q->njop_min);
            })->editColumn('njop_max',function($q){
                return gallade::parseQuantity($q->njop_max);
            })
            ->addIndexColumn()   
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tarif_store(Request $request)
    {
        $return = ["msg" => "Proses gagal.", "status" => false];
        DB::beginTransaction();
        try {
            $list=['kd_propinsi','kd_dati2','thn_awal','thn_akhir','njop_min','njop_max','nilai_tarif'];
            $saveData=$request->only($list);
            Tarif::whereRaw('thn_akhir>'.$saveData['thn_awal'].' and thn_awal!='.$saveData['thn_awal'])->update(['thn_akhir'=>$saveData['thn_awal']-1]);
            // $saveData['nilai_tarif']=$saveData['nilai_tarif']/(10*strlen($saveData['nilai_tarif']));
            Tarif::updateOrCreate($saveData);
            $return =["msg" => "Proses Berhasil.", "status" => true];
            DB::commit();
        } catch (\Exception $th) {
            $return=["msg" => "Proses Berhasil.", "status" => $th->getMessage()];
            DB::rollback();
        }
        return response()->json($return);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $return = ["msg" => "Proses gagal.", "status" => false];
        $list=$request->only('id');
        if($list){
            DB::beginTransaction();
            try {
                $objek = Tarif::findorfail($list['id']);
                $objek->delete();
                $return =["msg" => "Proses Berhasil.", "status" => true];
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $msg = $e->getMessage();//"Proses tidak berhasil";//
                return response()->json(["msg" => $msg, "status" => false]);
            }
        }
        $return = ["msg" => "Proses berhasil.", "status" => true];
        return response()->json($return);
    }
}
