<?php

namespace App\Http\Controllers;

use App\ObjekPajak;
use App\Sppt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class objekController extends Controller
{
    //

    public function show(Request $request)
    {
        // $objek=ObjekPajak::get();
        $nop = $request->nop;
        $res = str_replace('.', '', $nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);

        $tahun = date('Y');
        $objek = DB::connection("oracle_satutujuh")->table("sppt")
            ->join("dat_objek_pajak", function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('sppt.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->leftJoin(db::raw(" (  SELECT sppt.KD_PROPINSI,
        sppt.KD_DATI2,
        sppt.KD_KECAMATAN,
        sppt.KD_KELURAHAN,
        sppt.KD_BLOK,
        sppt.NO_URUT,
        sppt.KD_JNS_OP,
        CASE
           WHEN COUNT (1) = 0
           THEN
              1
           ELSE
              CASE
                 WHEN SUM (
                         CASE
                            WHEN status_pembayaran_sppt = '1' or (pbb_yg_harus_dibayar_sppt -nvl(NILAI_POTONGAN,0))=0 THEN 1
                            ELSE 0
                         END) = COUNT (1)
                 THEN
                    1
                 ELSE
                    0
              END
        END
           lunas_lima
   FROM pbb.sppt
   left join pbb.sppt_potongan on  sppt.KD_PROPINSI=sppt_potongan.KD_PROPINSI and
sppt.KD_DATI2=sppt_potongan.KD_DATI2 and
sppt.KD_KECAMATAN=sppt_potongan.KD_KECAMATAN and
sppt.KD_KELURAHAN=sppt_potongan.KD_KELURAHAN and
sppt.KD_BLOK=sppt_potongan.KD_BLOK and
sppt.NO_URUT=sppt_potongan.NO_URUT and
sppt.KD_JNS_OP=sppt_potongan.KD_JNS_OP and 
sppt.THN_PAJAK_SPPT=SPPT_POTONGAN.THN_PAJAK_SPPT
  WHERE     sppt.thn_pajak_sppt BETWEEN   CAST (
                                        TO_CHAR (SYSDATE, 'yyyy') AS NUMBER)
                                   - 5
                               AND CAST (
                                      TO_CHAR (SYSDATE, 'yyyy') - 1 AS NUMBER)
        AND SPPT.KD_PROPINSI = '35'
        AND SPPT.KD_DATI2 = '07'
        AND SPPT.KD_KECAMATAN = '$kd_kecamatan'
        AND SPPT.KD_KELURAHAN = '$kd_kelurahan'
        AND SPPT.KD_BLOK = '$kd_blok'
        AND SPPT.NO_URUT = '$no_urut'
        AND SPPT.KD_JNS_OP = '$kd_jns_op'
GROUP BY sppt.KD_PROPINSI,
        sppt.KD_DATI2,
        sppt.KD_KECAMATAN,
        sppt.KD_KELURAHAN,
        sppt.KD_BLOK,
        sppt.NO_URUT,
        sppt.KD_JNS_OP) ln"), function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'ln.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'ln.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'ln.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'ln.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'ln.kd_blok')
                    ->on('sppt.no_urut', '=', 'ln.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'ln.kd_jns_op');
            })
            ->join(db::raw("ref_kecamatan"), function ($join) {
                $join->On('sppt.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
            })
            ->join(db::raw("ref_kelurahan"), function ($join) {
                $join->On('sppt.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->On('sppt.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
            })->select(db::raw(" sppt.KD_PROPINSI,
        sppt.KD_DATI2,
        sppt.KD_KECAMATAN,
        sppt.KD_KELURAHAN,
        sppt.KD_BLOK,
        sppt.NO_URUT,
        sppt.KD_JNS_OP,
        sppt.THN_PAJAK_SPPT,
        sppt.SIKLUS_SPPT,
        sppt.KD_KANWIL,
        sppt.KD_KANTOR,
        sppt.KD_TP,
        sppt.NM_WP_SPPT,
        sppt.JLN_WP_SPPT,
        sppt.BLOK_KAV_NO_WP_SPPT,
        sppt.RW_WP_SPPT,
        sppt.RT_WP_SPPT,
        sppt.KELURAHAN_WP_SPPT,
        sppt.KOTA_WP_SPPT,
        sppt.KD_POS_WP_SPPT,
        sppt.NPWP_SPPT,
        sppt.NO_PERSIL_SPPT,
        sppt.KD_KLS_TANAH,
        sppt.THN_AWAL_KLS_TANAH,
        sppt.KD_KLS_BNG,
        sppt.THN_AWAL_KLS_BNG,
        sppt.TGL_JATUH_TEMPO_SPPT,
        sppt.LUAS_BUMI_SPPT,
        NVL (sppt.LUAS_BNG_SPPT, 0) LUAS_BNG_SPPT,
        NVL (sppt.NJOP_BUMI_SPPT, 0) NJOP_BUMI_SPPT,
        NVL (sppt.NJOP_BNG_SPPT, 0) NJOP_BNG_SPPT,
        NVL (sppt.NJOP_SPPT, 0) NJOP_SPPT,
        sppt.NJOPTKP_SPPT,
        sppt.PBB_TERHUTANG_SPPT,
        sppt.FAKTOR_PENGURANG_SPPT,
        sppt.PBB_YG_HARUS_DIBAYAR_SPPT,
        sppt.STATUS_PEMBAYARAN_SPPT,
        sppt.STATUS_TAGIHAN_SPPT,
        sppt.STATUS_CETAK_SPPT,
        sppt.TGL_TERBIT_SPPT,
        sppt.TGL_CETAK_SPPT,
        sppt.NIP_PENCETAK_SPPT,
        TRIM (
              jalan_op
           || ' '
           || blok_kav_no_op
           || ' RT :'
           || RT_OP
           || ' RW:'
           || RW_OP)
           alamat_op,
           nm_kelurahan,
           nm_kecamatan,
        lunas_lima"))
            ->whereraw("sppt.thn_pajak_sppt = '$tahun'
        AND SPPT.KD_PROPINSI = '35'
        AND SPPT.KD_DATI2 = '07'
        AND SPPT.KD_KECAMATAN = '$kd_kecamatan'
        AND SPPT.KD_KELURAHAN = '$kd_kelurahan'
        AND SPPT.KD_BLOK = '$kd_blok'
        AND SPPT.NO_URUT = '$no_urut'
        AND SPPT.KD_JNS_OP = '$kd_jns_op'")
            ->first();
        return response()->json($objek);
    }

    public function cekPembayaran(Request $request)
    {
        $nop = $request->nop;
        $res = str_replace('.', '', $nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);
        $start = date('Y') - 1;
        $awal = 2003;
        $sampai = $start;

        $sppt = DB::connection('oracle_satutujuh')->table(DB::raw("sppt"))
            ->leftjoin('sppt_potongan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->select(DB::raw("	sppt.thn_pajak_sppt,
	pbb_yg_harus_dibayar_sppt,
	nvl(NILAI_POTONGAN,0) pot,
	case when (pbb_yg_harus_dibayar_sppt -nvl(NILAI_POTONGAN,0))=0 then '1' else status_pembayaran_sppt end kd_status,
CASE
		WHEN status_pembayaran_sppt = 1 or (pbb_yg_harus_dibayar_sppt -nvl(NILAI_POTONGAN,0))=0  THEN
		'Lunas' ELSE 'Belum' 
	END status "))
            ->where('sppt.kd_propinsi', $kd_propinsi)
            ->where('sppt.kd_dati2', $kd_dati2)
            ->where('sppt.kd_kecamatan', $kd_kecamatan)
            ->where('sppt.kd_kelurahan', $kd_kelurahan)
            ->where('sppt.kd_blok', $kd_blok)
            ->where('sppt.no_urut', $no_urut)
            ->where('sppt.kd_jns_op', $kd_jns_op)
            ->whereraw('sppt.thn_pajak_sppt  between ' . $awal . ' and ' . $sampai)
            ->orderby('sppt.thn_pajak_sppt', 'desc')->get();
        return response()->json($sppt);
    }
}
