<?php

namespace App\Http\Controllers;

use App\Services\SendOtpOca;
use Illuminate\Http\Request;

class WhatsAppController extends Controller
{
    //
    protected $SendOtpOca;

    public function __construct(SendOtpOca $SendOtpOca)
    {
        $this->SendOtpOca = $SendOtpOca;
    }

    public function sendMessage(Request $request)
    {
        $phoneNumber = '082330319913';
        $message = '8723872387237';

        $response = $this->SendOtpOca->sendMessage($phoneNumber, $message);

        return response()->json($response);
    }
}
