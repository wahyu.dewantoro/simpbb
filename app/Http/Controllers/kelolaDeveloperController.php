<?php

namespace App\Http\Controllers;

use App\KelolaUsulan;

use App\User;
use App\UsulanPengelola;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class kelolaDeveloperController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = UsulanPengelola::latest()->paginate();
        // dd($data);
        return view('kelola.developer.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = UsulanPengelola::findorfail($id);
        return view('kelola.developer.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexVerifikasi(Request $request)
    {
        /*  $usulannop = KelolaUsulan::wherenull('verifikasi_kode')->get();
        $usulandeveloper = UsulanPengelola::wherenull('verifikasi_kode')->get();
        return view('kelola.index-verifikasi', compact('usulannop', 'usulandeveloper')); */
    }

    public function formVerifikasi($id)
    {
        $data = UsulanPengelola::findorfail($id);
        return view('kelola.developer.formverifikasi', compact('data'));
    }

    public function formVerifikasiStore(Request $request, $id)
    {

        DB::beginTransaction();
        try {
            //code...
            $upd = UsulanPengelola::find($id);
            $user_id = $upd->user_id;
            $upd->update([
                'verifikasi_kode' => $request->verifikasi_kode,
                'verifikasi_by' => Auth()->user()->id,
                'verifikasi_at' => now()
            ]);

            if ($request->verifikasi_kode == '1') {
                $is_pengelola = 1;
            } else {
                $is_pengelola = null;
            }

            User::find($user_id)->update([
                'is_pengelola' => $is_pengelola
            ]);

            $flash = [
                'success' => "berhasil di verifikasi"
            ];
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('kelola-verifikasi-data'))->with($flash);
    }
}
