<?php

namespace App\Http\Controllers;

use App\Exports\DataRingkasanExcel;
use App\Exports\detailBayarExport;
use App\Helpers\InformasiObjek;
use App\Helpers\Pajak;
use App\Kecamatan;
use App\KelolaUsulanObjek;
use App\Models\Layanan_objek;
use App\Models\PendataanBatch;
use App\Models\PendataanObjek;
use App\Models\Spop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DOMPDF as PDF;
use Maatwebsite\Excel\Facades\Excel;
// use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf  as PDF;

use Yajra\Datatables\Datatables;

class objekPajakController extends Controller
{
    //


    public function index(Request $request)
    {
        // DB::commit();
        $nop = [];
        $is_wp = Auth()->user()->hasrole(['DEVELOPER/PENGEMBANG', 'Rayon', 'Wajib Pajak']);
        $nop = [];
        if ($is_wp == true) {
            $nop = getNopWp();
        }

        return view('objek.index', compact('nop', 'is_wp'));
    }


    public function dataBayar(
        $kd_propinsi,
        $kd_dati2,
        $kd_kecamatan,
        $kd_kelurahan,
        $kd_blok,
        $no_urut,
        $kd_jns_op
    ) {
        return  DB::connection('oracle_satutujuh')->select(db::raw("
        SELECT sppt.thn_pajak_sppt,
        trunc(sppt.tgl_terbit_sppt) tgl_terbit_sppt,
                   sppt.pbb_yg_harus_dibayar_sppt - nvl(sppt_potongan.nilai_potongan,0) pbb,
                 jml_sppt_yg_dibayar - denda_sppt pokok,
                 denda_sppt denda,
                 jml_sppt_yg_dibayar jumlah,
                 trunc(tgl_pembayaran_sppt) tgl_bayar,
                 pembayaran_sppt.pengesahan,
                 ROW_NUMBER ()
                    OVER (PARTITION BY sppt.thn_pajak_sppt ORDER BY tgl_pembayaran_sppt)
                    pembayaran_ke,
                    sppt_koreksi.no_transaksi no_koreksi, 
sppt_koreksi.keterangan keterangan_koreksi,
nop_proses nop_pecahan
            FROM sppt
                   left join pembayaran_sppt on sppt.kd_propinsi=pembayaran_sppt.kd_propinsi and 
        sppt.kd_dati2=pembayaran_sppt.kd_dati2 and 
        sppt.kd_kecamatan=pembayaran_sppt.kd_kecamatan and 
        sppt.kd_kelurahan=pembayaran_sppt.kd_kelurahan and 
        sppt.kd_blok=pembayaran_sppt.kd_blok and 
        sppt.no_urut=pembayaran_sppt.no_urut and 
        sppt.kd_jns_op=pembayaran_sppt.kd_jns_op and 
        sppt.thn_pajak_sppt=pembayaran_sppt.thn_pajak_sppt 
        left join sppt_potongan on sppt.kd_propinsi=sppt_potongan.kd_propinsi and 
        sppt.kd_dati2=sppt_potongan.kd_dati2 and 
        sppt.kd_kecamatan=sppt_potongan.kd_kecamatan and 
        sppt.kd_kelurahan=sppt_potongan.kd_kelurahan and 
        sppt.kd_blok=sppt_potongan.kd_blok and 
        sppt.no_urut=sppt_potongan.no_urut and 
        sppt.kd_jns_op=sppt_potongan.kd_jns_op and 
        sppt.thn_pajak_sppt=sppt_potongan.thn_pajak_sppt 
        left join sppt_koreksi on sppt.kd_propinsi=sppt_koreksi.kd_propinsi and 
sppt.kd_dati2=sppt_koreksi.kd_dati2 and 
sppt.kd_kecamatan=sppt_koreksi.kd_kecamatan and 
sppt.kd_kelurahan=sppt_koreksi.kd_kelurahan and 
sppt.kd_blok=sppt_koreksi.kd_blok and 
sppt.no_urut=sppt_koreksi.no_urut and 
sppt.kd_jns_op=sppt_koreksi.kd_jns_op and 
sppt.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt 

left join (select pengesahan,nop_proses,nop_asal
from sim_pbb.data_billing a
join sim_pbb.nota_perhitungan b on b.data_billing_id=a.data_billing_id
join sim_pbb.layanan_objek c on c.id=B.LAYANAN_OBJEK_ID
join sim_pbb.tbl_spop d on d.no_formulir=c.nomor_formulir) pch on pch.pengesahan=pembayaran_sppt.pengesahan
    and pch.nop_asal=sppt.kd_propinsi||sppt.kd_dati2||sppt.kd_kecamatan||sppt.kd_kelurahan||sppt.kd_blok||sppt.no_urut||sppt.kd_jns_op
           WHERE    sppt.kd_kecamatan = '$kd_kecamatan'
                 AND sppt.kd_kelurahan = '$kd_kelurahan'
                 AND sppt.kd_blok = '$kd_blok'
                 AND sppt.no_urut = '$no_urut'
                 AND sppt.kd_jns_op = '$kd_jns_op'
        ORDER BY sppt.thn_pajak_sppt, tgl_pembayaran_sppt, tgl_rekam_byr_sppt"));
    }

    public function riwayatPembayaran(Request $request)
    {
        if ($request->ajax()) {
            $nop = $request->nop;
            $objek = Pajak::coreInfoObjek($nop);
            $res = onlyNumber($nop);
            $kd_propinsi = substr($res, 0, 2);
            $kd_dati2 = substr($res, 2, 2);
            $kd_kecamatan = substr($res, 4, 3);
            $kd_kelurahan = substr($res, 7, 3);
            $kd_blok = substr($res, 10, 3);
            $no_urut = substr($res, 13, 4);
            $kd_jns_op = substr($res, 17, 1);

            $bayar = $this->dataBayar(
                $kd_propinsi,
                $kd_dati2,
                $kd_kecamatan,
                $kd_kelurahan,
                $kd_blok,
                $no_urut,
                $kd_jns_op
            );

            return view('objek.riwayatBayar_konten', compact('objek', 'nop', 'bayar'));
        }

        return view('objek.riwayatBayar');
    }


    public function riwayatPembayaranCetak(Request $request)
    {
        $nop = $request->nop;
        $objek = Pajak::coreInfoObjek($nop);
        $res = onlyNumber($nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);



        $bayar = $this->dataBayar(
            $kd_propinsi,
            $kd_dati2,
            $kd_kecamatan,
            $kd_kelurahan,
            $kd_blok,
            $no_urut,
            $kd_jns_op
        );

        $data = [
            'objek' => $objek,
            'nop' => $nop,
            'bayar' => $bayar
        ];

        return Excel::download(new detailBayarExport($data), 'detaail bayar ' . $nop . '.xlsx');

        // return view('objek.riwayatBayar_konten', compact('objek', 'nop', 'bayar'));
        // dd(count($bayar));
        // return onlyNumber($nop);
    }

    public function ringkasanObjek(Request $request)
    {
        $kecamatan = Kecamatan::orderby('kd_kecamatan')->get();
        return view('objek.ringkasan', compact('kecamatan'));
    }


    public function cetakNjop(Request $request)
    {
        // return $request->all();
        $nop = $request->nop;
        $res = onlyNumber($nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);

        $variable = [
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op
        ];
        $data = InformasiObjek::riwayatPembayaran($variable)->orderby('sppt.thn_pajak_sppt')->get();
        // data $sppt;
        $nop = formatnop($res);

        $pdf = PDF::loadView('objek/_cetak_njop', compact('data', 'nop'))->setPaper('a4', 'landscape');
        $filename = 'riwayat NJOP  ' . $nop . '.pdf';
        return $pdf->stream($filename);
    }


    public function sqlRingkasan($kd_kecamatan, $kd_kelurahan, $subjek_pajak = '')
    {
        $and = "";
        if ($kd_kelurahan != '') {
            $and = " AND dat_objek_pajak.kd_kelurahan = '$kd_kelurahan'";
        }

        if ($subjek_pajak != '') {
            $and = " AND lower(nm_wp) like '%" . $subjek_pajak . "%' ";
        }

        return   $table = "(SELECT dat_subjek_pajak.subjek_pajak_id nik,
        TRIM (jalan_wp) || ' ' || blok_kav_no_wp alamat_wp,
        rw_wp,
        rt_wp,
           dat_objek_pajak.kd_propinsi
        || '.'
        || dat_objek_pajak.kd_dati2
        || '.'
        || dat_objek_pajak.kd_kecamatan
        || '.'
        || dat_objek_pajak.kd_kelurahan
        || '.'
        || dat_objek_pajak.kd_blok
        || '-'
        || dat_objek_pajak.no_urut
        || '.'
        || dat_objek_pajak.kd_jns_op
           nop_format,
           dat_objek_pajak.kd_propinsi
        || dat_objek_pajak.kd_dati2
        || dat_objek_pajak.kd_kecamatan
        || dat_objek_pajak.kd_kelurahan
        || dat_objek_pajak.kd_blok
        || dat_objek_pajak.no_urut
        || dat_objek_pajak.kd_jns_op
           nop_asli,
        dat_objek_pajak.kd_propinsi,
        dat_objek_pajak.kd_dati2,
        dat_objek_pajak.kd_kecamatan,
        dat_objek_pajak.kd_kelurahan,
        dat_objek_pajak.kd_blok,
        dat_objek_pajak.no_urut,
        dat_objek_pajak.kd_jns_op,
        nm_wp,
        jalan_op || ' ' || blok_kav_no_op alamat_op,
        rw_op,
        rt_op,
        (SELECT nm_kecamatan
           FROM ref_kecamatan
          WHERE kd_kecamatan = dat_objek_pajak.kd_kecamatan)
           kecamatan_op,
        (SELECT nm_kelurahan
           FROM ref_kelurahan
          WHERE     kd_kecamatan = dat_objek_pajak.kd_kecamatan
                AND kd_kelurahan = dat_objek_pajak.kd_kelurahan)
           kelurahan_op,
           dat_op_bumi.kd_znt,
           nir*1000 nir,
           nama_lokasi,
        total_luas_bumi,
        total_luas_bng,
        njop_bumi,
        njop_bng,
        njop_bumi + njop_bng njop_pbb,
        NVL (nilai_njoptkp, 0) * 1000 njoptkp_pbb,
        CASE
           WHEN jns_bumi = '1' THEN 'TANAH + BANGUNAN'
           WHEN jns_bumi = '2' THEN 'KAVLING'
           WHEN jns_bumi = '3' THEN 'TANAH KOSONG'
           WHEN jns_bumi = '4' THEN 'FASILITAS UMUM'
           WHEN jns_bumi = '5' THEN 'NON AKTIF'
           ELSE 'LAIN - LAIN'
        END
           status_peta,
        CASE
           WHEN jns_transaksi_op = '1' THEN 'PEREKAMAN DATA'
           WHEN jns_transaksi_op = '2' THEN 'PEMUTAKHIRAN'
           WHEN jns_transaksi_op = '3' THEN 'PEMBATALAN / PENGAHPUSAN OBJEK'
           ELSE 'PENGHAPUSAN OP BERSAMA'
        END
           transaksi,
        get_tarifpbb (njop_bumi + njop_bng,
                      NVL (nilai_njoptkp, 0) * 1000,
                      TO_CHAR (SYSDATE, 'yyyy'))
           nilai_tarif,
          (  get_tarifpbb (njop_bumi + njop_bng,
                           NVL (nilai_njoptkp, 0) * 1000,
                           TO_CHAR (SYSDATE, 'yyyy'))
           / 100)
        * (njop_bumi + njop_bng - (NVL (nilai_njoptkp, 0) * 1000))
           pbb_tahun_berjalan
   FROM dat_objek_pajak
        JOIN dat_subjek_pajak
           ON dat_objek_pajak.subjek_pajak_id =
                 dat_subjek_pajak.subjek_pajak_id
        JOIN dat_op_bumi
           ON     dat_op_bumi.kd_propinsi = dat_objek_pajak.kd_propinsi
              AND dat_op_bumi.kd_dati2 = dat_objek_pajak.kd_dati2
              AND dat_op_bumi.kd_kecamatan = dat_objek_pajak.kd_kecamatan
              AND dat_op_bumi.kd_kelurahan = dat_objek_pajak.kd_kelurahan
              AND dat_op_bumi.kd_blok = dat_objek_pajak.kd_blok
              AND dat_op_bumi.no_urut = dat_objek_pajak.no_urut
              AND dat_op_bumi.kd_jns_op = dat_objek_pajak.kd_jns_op
          join dat_znt on dat_znt.kd_propinsi=dat_op_bumi.kd_propinsi and dat_znt.kd_dati2=dat_op_bumi.kd_dati2
          and dat_znt.kd_kecamatan=dat_op_bumi.kd_kecamatan
          and dat_znt.kd_kelurahan=DAT_OP_BUMI.KD_KELURAHAN and dat_znt.kd_znt=dat_op_bumi.kd_znt
          join dat_nir on dat_znt.kd_propinsi=dat_nir.kd_propinsi and
 dat_znt.kd_dati2=dat_nir.kd_dati2 and
 dat_znt.kd_kecamatan=dat_nir.kd_kecamatan and
 dat_znt.kd_kelurahan=dat_nir.kd_kelurahan and
 dat_znt.kd_znt=dat_nir.kd_znt and dat_nir.thn_nir_znt=to_char(sysdate,'yyyy')
          left join sim_pbb.lokasi_objek lo on lo.id=dat_znt.lokasi_objek_id
        LEFT JOIN dat_subjek_pajak_njoptkp
           ON     dat_objek_pajak.kd_propinsi =
                     DAT_SUBJEK_PAJAK_NJOPTKP.kd_propinsi
              AND dat_objek_pajak.kd_dati2 = DAT_SUBJEK_PAJAK_NJOPTKP.kd_dati2
              AND dat_objek_pajak.kd_kecamatan =
                     DAT_SUBJEK_PAJAK_NJOPTKP.kd_kecamatan
              AND dat_objek_pajak.kd_kelurahan =
                     DAT_SUBJEK_PAJAK_NJOPTKP.kd_kelurahan
              AND dat_objek_pajak.kd_blok = DAT_SUBJEK_PAJAK_NJOPTKP.kd_blok
              AND dat_objek_pajak.no_urut = DAT_SUBJEK_PAJAK_NJOPTKP.no_urut
              AND dat_objek_pajak.kd_jns_op =
                     DAT_SUBJEK_PAJAK_NJOPTKP.kd_jns_op
              AND dat_objek_pajak.subjek_pajak_id =
                     DAT_SUBJEK_PAJAK_NJOPTKP.subjek_pajak_id
              AND DAT_SUBJEK_PAJAK_NJOPTKP.THN_NJOPTKP =
                     TO_CHAR (SYSDATE, 'yyyy')
        LEFT JOIN njoptkp
           ON     njoptkp.kd_propinsi = dat_objek_pajak.kd_propinsi
              AND njoptkp.kd_dati2 = dat_objek_pajak.kd_dati2
              AND DAT_SUBJEK_PAJAK_NJOPTKP.THN_NJOPTKP >= njoptkp.thn_awal
              AND DAT_SUBJEK_PAJAK_NJOPTKP.THN_NJOPTKP <= njoptkp.thn_akhir
  WHERE     dat_objek_pajak.kd_propinsi = '35'
        AND dat_objek_pajak.kd_dati2 = '07'
        AND dat_objek_pajak.kd_kecamatan = '$kd_kecamatan'
        $and )";
    }

    public function showList(Request $request)
    {

        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $subjek_pajak = $request->subjek_pajak;
        if (isset($request->excel)) {
            // return $request->all();
            $table = $this->sqlRingkasan($kd_kecamatan, $kd_kelurahan, $subjek_pajak);
            $data = DB::connection('oracle_satutujuh')->table(db::raw($table));
            $data = $data->get();
            $array = [];
            foreach ($data as $row) {
                $alamat_op = $row->alamat_op;
                if ($row->rt_op != '') {
                    $alamat_op .= ' RT ' . $row->rt_op;
                }

                if ($row->rw_op != '') {
                    $alamat_op .= ' RW ' . $row->rw_op;
                }

                $alamat_wp = $row->alamat_wp;
                if ($row->rt_wp != '') {
                    $alamat_wp .= ' RT ' . $row->rt_wp;
                }

                if ($row->rw_wp != '') {
                    $alamat_wp .= ' RW ' . $row->rw_wp;
                }

                $keterangan = $row->transaksi . ' , ' . $row->status_peta;
                $array[] = [
                    $row->nop_format,
                    $alamat_op,
                    $row->nik,
                    $row->nm_wp,
                    $alamat_wp,
                    $row->kd_znt,
                    (int)$row->nir,
                    $row->nama_lokasi,
                    (int)$row->total_luas_bumi,
                    (int)$row->njop_bumi,
                    (int)$row->total_luas_bng,
                    (int)$row->njop_bng,
                    (int)$row->njop_pbb,
                    (int)$row->njoptkp_pbb,
                    angka($row->nilai_tarif),
                    (int)$row->pbb_tahun_berjalan,
                    $keterangan,
                ];
            }


            // return $array;
            return Excel::download(new DataRingkasanExcel($array), 'data ringkasan  ' . $kd_kecamatan . '_' . $kd_kelurahan . '.xlsx');
        }


        return view('objek.list_objek', compact('kd_kecamatan', 'kd_kelurahan', 'subjek_pajak'));
    }

    public function showListDatatables(Request $request)
    {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan ?? '';
        $subjek_pajak = $request->subjek_pajak ?? '';
        $table = $this->sqlRingkasan($kd_kecamatan, $kd_kelurahan, $subjek_pajak);

        $data = DB::connection('oracle_satutujuh')->table(db::raw($table));


        return Datatables::of($data)
            ->filterColumn('alamat_op', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw(DB::raw("lower(alamat_op) like '%$keyword%' or lower(kelurahan_op) like '%$keyword%' or lower(kecamatan_op) like '%$keyword%'"));
            })
            ->addcolumn('nop', function ($row) {
                return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
            })
            ->addcolumn('alamat_op', function ($row) {

                return $row->alamat_op . ' RT ' . $row->rt_op . ' RW ' . $row->rw_op . ' ' . $row->kelurahan_op . ' ' . $row->kecamatan_op;
            })
            ->addcolumn('detail', function ($row) {
                return "<button onclick='nopDetail(\"" . $row->nop_asli . "\")' class='btn btn-sm btn-info nop-detail' data-nop='" . $row->nop_asli . "'><i class='fas fa-binoculars'></i></button>";
            })->rawColumns(['nop', 'detail', 'alamat_op'])
            ->make(true);
    }

    public function show(Request $request)
    {
        $nop = $request->nop;
        $res = onlyNumber($nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);
        // mv_dat_objek_pajak
        $objek = DB::connection('oracle_satutujuh')->table('dat_objek_pajak')
            ->selectraw("dat_objek_pajak.*, 
                            ref_kecamatan.nm_kecamatan, ref_kelurahan.nm_kelurahan, dat_op_bumi.kd_znt, dat_op_bumi.jns_bumi, ( select unflag_desc from unflag_history uh
                            where      UH.KD_PROPINSI = dat_objek_pajak.kd_propinsi
                            AND uh.kd_dati2 = dat_objek_pajak.kd_dati2
                            AND uh.kd_kecamatan = dat_objek_pajak.kd_kecamatan
                            AND uh.kd_kelurahan = dat_objek_pajak.kd_kelurahan
                            AND uh.kd_blok = dat_objek_pajak.kd_blok
                            AND uh.no_urut = dat_objek_pajak.no_urut
                            AND uh.kd_jns_op = dat_objek_pajak.kd_jns_op and rownum=1) unflag_desc")
            ->join('ref_kelurahan', function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan');
            })
            ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
            ->leftjoin("dat_op_bumi", function ($join) {
                $join->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->whereraw("dat_objek_pajak.kd_propinsi ='" . $kd_propinsi . "'")
            ->whereraw("dat_objek_pajak.kd_dati2 ='" . $kd_dati2 . "'")
            ->whereraw("dat_objek_pajak.kd_kecamatan ='" . $kd_kecamatan . "'")
            ->whereraw("dat_objek_pajak.kd_kelurahan ='" . $kd_kelurahan . "'")
            ->whereraw("dat_objek_pajak.kd_blok ='" . $kd_blok . "'")
            ->whereraw("dat_objek_pajak.no_urut ='" . $no_urut . "'")
            ->whereraw("dat_objek_pajak.kd_jns_op ='" . $kd_jns_op . "'")
            ->get()
            ->first();
        // dd($objek);
        if ($objek) {
            // cek pengaktifan
            $cpa = DB::connection("oracle_satutujuh")->table('pengaktifan_objek')
                ->whereraw("kd_propinsi_asal ='" . $kd_propinsi . "'")
                ->whereraw("kd_dati2_asal ='" . $kd_dati2 . "'")
                ->whereraw("kd_kecamatan_asal ='" . $kd_kecamatan . "'")
                ->whereraw("kd_kelurahan_asal ='" . $kd_kelurahan . "'")
                ->whereraw("kd_blok_asal ='" . $kd_blok . "'")
                ->whereraw("no_urut_asal ='" . $no_urut . "'")
                ->whereraw("kd_jns_op_asal ='" . $kd_jns_op . "'")
                ->whereraw("kd_propinsi <>kd_propinsi_asal and 
                            kd_dati2 <>kd_dati2_asal and 
                            kd_kecamatan <>kd_kecamatan_asal and 
                            kd_kelurahan <>kd_kelurahan_asal and 
                            kd_blok <>kd_blok_asal and 
                            no_urut <>no_urut_asal and 
                            kd_jns_op <>kd_jns_op_asal")
                ->first();
            // dd($cpa);

            if ($cpa) {
                $deskripsi = 'NOP tersebut telah di migrasikan data nya ke nop ';
                $deskripsi .= $cpa->kd_propinsi . '.';
                $deskripsi .= $cpa->kd_dati2 . '.';
                $deskripsi .= $cpa->kd_kecamatan . '.';
                $deskripsi .= $cpa->kd_kelurahan . '.';
                $deskripsi .= $cpa->kd_blok . '.';
                $deskripsi .= $cpa->no_urut . '.';
                $deskripsi .= $cpa->kd_jns_op . '.';

                return view('404', compact('deskripsi'));
            }





            // data perekam
            $perekam = Spop::selectraw(" pendataan_objek_id, jns_transaksi jns_transaksi_op,nomor_layanan,nama_layanan,tbl_spop.created_at,nama,nomor_batch")
                ->leftjoin('layanan_objek', 'layanan_objek.id', '=', 'tbl_spop.layanan_objek_id')
                ->leftjoin('jenis_layanan', 'jenis_layanan.id', '=', 'tbl_spop.jenis_layanan_id')
                ->leftjoin('users', 'users.id', '=', 'tbl_spop.created_by')
                ->leftjoin('pendataan_objek', 'pendataan_objek.id', '=', 'tbl_spop.pendataan_objek_id')
                ->leftjoin('pendataan_batch', 'pendataan_batch.id', '=', 'pendataan_objek.pendataan_batch_id')
                ->where('no_formulir', trim($objek->no_formulir_spop))->first();

            //cek kobil bayar
            $tahun_nota = DB::table(db::raw('data_billing a'))
                ->join(db::raw("billing_kolektif b"), 'a.data_billing_id', '=', 'b.data_billing_id')
                ->join(db::raw("(select distinct data_billing_id 
                                from sppt_penelitian
                                where nomor_formulir='" . trim($objek->no_formulir_spop) . "') c"), 'c.data_billing_id', '=', 'b.data_billing_id')
                ->whereraw("a.kd_status='0'")->selectraw("distinct b.tahun_pajak")->pluck('tahun_pajak')->toArray();

            // mv_dat_subjek_pajak
            $subjek = DB::connection("oracle_dua")->table(DB::raw('dat_subjek_pajak'))
                ->whereraw("SUBJEK_PAJAK_ID='" . $objek->subjek_pajak_id . "'")->first();
            if ($subjek) {
                $subjek = DB::connection("oracle_satutujuh")->table(DB::raw('dat_subjek_pajak'))
                    ->whereraw("SUBJEK_PAJAK_ID='" . $objek->subjek_pajak_id . "'")->get()->first();
            }

            // mv_dat_op_bangunan
            $bangunan = DB::connection("oracle_satutujuh")->table(db::raw(' dat_op_bangunan'))
                ->select(db::raw("no_formulir_lspop no_formulir, jns_transaksi_bng jns_transaksi, tbl_lspop.nop,dat_op_bangunan.no_bng, dat_op_bangunan.kd_jpb, dat_op_bangunan.thn_dibangun_bng, dat_op_bangunan.thn_renovasi_bng, dat_op_bangunan.luas_bng, dat_op_bangunan.jml_lantai_bng, dat_op_bangunan.kondisi_bng, dat_op_bangunan.jns_konstruksi_bng, dat_op_bangunan.jns_atap_bng, dat_op_bangunan.kd_dinding, dat_op_bangunan.kd_lantai, dat_op_bangunan.kd_langit_langit, tbl_lspop.daya_listrik, tbl_lspop.acsplit, tbl_lspop.acwindow, tbl_lspop.acsentral, tbl_lspop.luas_kolam, tbl_lspop.finishing_kolam, tbl_lspop.luas_perkerasan_ringan, tbl_lspop.luas_perkerasan_sedang, tbl_lspop.luas_perkerasan_berat, tbl_lspop.luas_perkerasan_dg_tutup, tbl_lspop.lap_tenis_lampu_beton, tbl_lspop.lap_tenis_lampu_aspal, tbl_lspop.lap_tenis_lampu_rumput, tbl_lspop.lap_tenis_beton, tbl_lspop.lap_tenis_aspal, tbl_lspop.lap_tenis_rumput, tbl_lspop.lift_penumpang, tbl_lspop.lift_kapsul, tbl_lspop.lift_barang, tbl_lspop.tgg_berjalan_a, tbl_lspop.tgg_berjalan_b, tbl_lspop.pjg_pagar, tbl_lspop.bhn_pagar, tbl_lspop.hydrant, tbl_lspop.sprinkler, tbl_lspop.fire_alarm, tbl_lspop.jml_pabx, tbl_lspop.sumur_artesis, tbl_lspop.nilai_individu, tbl_lspop.jpb3_8_tinggi_kolom, tbl_lspop.jpb3_8_lebar_bentang, tbl_lspop.jpb3_8_dd_lantai, tbl_lspop.jpb3_8_kel_dinding, tbl_lspop.jpb3_8_mezzanine, tbl_lspop.jpb5_kls_bng, tbl_lspop.jpb5_luas_kamar, tbl_lspop.jpb5_luas_rng_lain, tbl_lspop.jpb7_jns_hotel, tbl_lspop.jpb7_bintang, tbl_lspop.jpb7_jml_kamar, tbl_lspop.jpb7_luas_kamar, tbl_lspop.jpb7_luas_rng_lain, tbl_lspop.jpb13_kls_bng, tbl_lspop.jpb13_jml, tbl_lspop.jpb13_luas_kamar, tbl_lspop.jpb13_luas_rng_lain, tbl_lspop.jpb15_letak_tangki, tbl_lspop.jpb15_kapasitas_tangki, tbl_lspop.jpb_lain_kls_bng, to_char(dat_op_bangunan.tgl_pendataan_bng,'yyyy-mm-dd') tgl_pendataan_bng, dat_op_bangunan.nip_pendata_bng, to_char(dat_op_bangunan.tgl_pemeriksaan_bng,'yyyy-mm-dd') tgl_pemeriksaan_bng, dat_op_bangunan.nip_pemeriksa_bng, to_char(dat_op_bangunan.tgl_perekaman_bng,'yyyy-mm-dd') tgl_perekaman_bng, dat_op_bangunan.nip_perekam_bng, tbl_lspop.rec_info"))
                // mv_tbl_lspop
                ->leftjoin(db::raw("tbl_lspop"), "tbl_lspop.no_formulir", "=", "dat_op_bangunan.no_formulir_lspop")
                ->whereraw("kd_propinsi ='" . $kd_propinsi . "'")
                ->whereraw("kd_dati2 ='" . $kd_dati2 . "'")
                ->whereraw("kd_kecamatan ='" . $kd_kecamatan . "'")
                ->whereraw("kd_kelurahan ='" . $kd_kelurahan . "'")
                ->whereraw("kd_blok ='" . $kd_blok . "'")
                ->whereraw("no_urut ='" . $no_urut . "'")
                ->whereraw("kd_jns_op ='" . $kd_jns_op . "' and jns_transaksi_bng <>'3'")
                ->get();

            $variable = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op
            ];

            $sppt = DB::connection('oracle_dua')->table('sppt')
                ->selectraw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,nm_wp_sppt, luas_bumi_sppt, njop_bumi_sppt, luas_bng_sppt, njop_bng_sppt, pbb_yg_harus_dibayar_sppt pbb ")
                ->whereraw("kd_propinsi ='" . $kd_propinsi . "'")
                ->whereraw("kd_dati2 ='" . $kd_dati2 . "'")
                ->whereraw("kd_kecamatan ='" . $kd_kecamatan . "'")
                ->whereraw("kd_kelurahan ='" . $kd_kelurahan . "'")
                ->whereraw("kd_blok ='" . $kd_blok . "'")
                ->whereraw("no_urut ='" . $no_urut . "'")
                ->whereraw("kd_jns_op ='" . $kd_jns_op . "' and  thn_pajak_sppt>=2014")
                ->orderby('THN_PAJAK_SPPT')->get();
            $add_desc = [
                'status' => 'Aktif',
                'keterangan' => ''
            ];
            $show_sppt = true;
            if ($objek->jns_transaksi_op == '3') {
                $add_desc = [
                    'status' => 'Tidak Aktif',
                    'keterangan' => $objek->unflag_desc
                ];
                $show_sppt = false;
            }
            $layananHistory = $this->layananHistory($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op);
            $pendataanHistory = $this->pendataanHistory($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op);
            return view('objek.show', compact('objek', 'subjek', 'bangunan', 'sppt', 'nop', 'perekam', 'tahun_nota', 'add_desc', 'layananHistory', 'pendataanHistory', 'show_sppt'));
        } else {
            $deskripsi = 'NOP tidak ditemukan dalam database.';
            return view('404', compact('deskripsi'));
        }
    }




    public function detailObjek(Request $request)
    {

        $nop = onlyNumber($request->nop);

        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kecamatan = substr($nop, 4, 3);
        $kd_kelurahan = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);
        // cek pengaktifan
        $cpa = DB::connection("oracle_satutujuh")->table('pengaktifan_objek')
            ->whereraw("kd_propinsi_asal ='" . $kd_propinsi . "'")
            ->whereraw("kd_dati2_asal ='" . $kd_dati2 . "'")
            ->whereraw("kd_kecamatan_asal ='" . $kd_kecamatan . "'")
            ->whereraw("kd_kelurahan_asal ='" . $kd_kelurahan . "'")
            ->whereraw("kd_blok_asal ='" . $kd_blok . "'")
            ->whereraw("no_urut_asal ='" . $no_urut . "'")
            ->whereraw("kd_jns_op_asal ='" . $kd_jns_op . "'")
            ->whereraw("kd_propinsi <>kd_propinsi_asal and 
            kd_dati2 <>kd_dati2_asal and 
            kd_kecamatan <>kd_kecamatan_asal and 
            kd_kelurahan <>kd_kelurahan_asal and 
            kd_blok <>kd_blok_asal and 
            no_urut <>no_urut_asal and 
            kd_jns_op <>kd_jns_op_asal")
            ->first();
        // dd($cpa);

        if ($cpa) {
            $deskripsi = 'NOP tersebut telah di migrasikan data nya ke nop ';
            $deskripsi .= $cpa->kd_propinsi . '.';
            $deskripsi .= $cpa->kd_dati2 . '.';
            $deskripsi .= $cpa->kd_kecamatan . '.';
            $deskripsi .= $cpa->kd_kelurahan . '.';
            $deskripsi .= $cpa->kd_blok . '.';
            $deskripsi .= $cpa->no_urut . '.';
            $deskripsi .= $cpa->kd_jns_op . '.';

            return '<div class="alert alert-danger alert-dismissible">
           <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
           ' . $deskripsi . '
           </div>';
        }

        // $objek = Pajak::coreInfoObjek($nop);
        $getData = InformasiObjek::dataDanInformasi($nop);
        $objek = $getData['objek'];
        if ($objek == null) {
            return '<div class="alert alert-warning alert-dismissible">
            <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
            Data objek tidak di temukan
            </div>';
        }
        $bangunan = $getData['bangunan'];
        $sppt = $getData['sppt'];
        $show_sppt = $getData['show_sppt'];
        $tahun_nota = $getData['tahun_nota'];
        $pemutakhiran = $getData['pemutakhiran'];
        $pecah = $getData['pecah'];
        $gabung = $getData['gabung'];
        $gabungke = $getData['gabungke'];

        return   view('objek.show_dua', compact('objek', 'bangunan', 'sppt', 'show_sppt', 'tahun_nota', 'nop', 'pemutakhiran', 'pecah', 'gabung', 'gabungke'));
    }

    private function pendataanHistory($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op)
    {
        $select = [
            'pendataan_batch.nomor_batch',
            'pendataan_objek.nm_wp',
            'pendataan_objek.luas_bumi',
            'jenis_pendataan.nama_pendataan',
            'pendataan_batch.verifikasi_at',
            'tbl_spop.nop_proses',
            DB::raw('verifikator.nama pegawai')
        ];
        $listPendataan = PendataanBatch::select($select)
            ->leftjoin('pendataan_objek', 'pendataan_objek.pendataan_batch_id', '=', 'pendataan_batch.id')
            ->leftjoin('tbl_spop', 'tbl_spop.pendataan_objek_id', '=', 'pendataan_objek.id')
            ->leftjoin('jenis_pendataan', 'jenis_pendataan.id', '=', 'pendataan_batch.jenis_pendataan_id')
            ->leftjoin(db::raw('users verifikator'), 'verifikator.id', '=', 'pendataan_batch.verifikasi_by')
            ->where([
                'tbl_spop.nop_proses' => $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op
            ])
            ->where(['pendataan_batch.verifikasi_kode' => '1'])
            ->get();
        return $listPendataan->toArray();
    }

    private function layananHistory($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op)
    {
        $joinA = db::raw("layanan_objek.id AND tbl_spop.no_formulir=layanan_objek.nomor_formulir");
        $historyList = Layanan_objek::whereRaw(
            "((layanan_objek.kd_propinsi='" . $kd_propinsi . "'
                            and  layanan_objek.kd_dati2='" . $kd_dati2 . "'
                            and  layanan_objek.kd_kecamatan='" . $kd_kecamatan . "'
                            and  layanan_objek.kd_kelurahan='" . $kd_kelurahan . "'
                            and  layanan_objek.kd_blok='" . $kd_blok . "'
                            and  layanan_objek.no_urut='" . $no_urut . "'
                            and  layanan_objek.kd_jns_op='" . $kd_jns_op . "')
                            or TBL_SPOP.NOP_PROSES='" . $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op . "')"
        )
            ->select([
                DB::raw('layanan_objek.*'),
                'tbl_spop.nop_proses',
                DB::raw('users.nama as nama_penelitian'),
                'layanan.jenis_layanan_nama',
                'layanan.jenis_layanan_id',
                'layanan.tanggal_layanan',
                'layanan.deleted_at'
            ])
            ->join('layanan', "layanan.nomor_layanan", "=", "layanan_objek.nomor_layanan")
            ->join('users', "users.id", "=", "layanan_objek.pemutakhiran_by")
            ->leftjoin('tbl_spop', 'tbl_spop.layanan_objek_id', '=', $joinA, "left outer")
            ->orderBy(DB::raw('layanan.tanggal_layanan,layanan_objek.id'), 'ASC')
            ->whereRaw('layanan.deleted_at is null')
            ->get();
        $layananHistory = Collect();

        $historyList->filter(function ($resData) use ($layananHistory, $joinA) {
            $listPecah = [];
            if ($resData->jenis_layanan_id == '6') { // induk
                if ($resData->nop_gabung != "" || $resData->nop_gabung != null) {
                    $listPecah = Layanan_objek::where(['layanan_objek.id' => $resData->nop_gabung])
                        ->select([DB::raw('layanan_objek.*'), 'tbl_spop.nop_proses'])
                        ->leftjoin('tbl_spop', 'tbl_spop.layanan_objek_id', '=', $joinA, "left outer")
                        ->get()->toArray();
                } else {
                    $listPecah = Layanan_objek::where(['layanan_objek.nop_gabung' => $resData->id])
                        ->select([DB::raw('layanan_objek.*'), 'tbl_spop.nop_proses'])
                        ->leftjoin('tbl_spop', 'tbl_spop.layanan_objek_id', '=', $joinA, "left outer")
                        ->get()->toArray();
                }

                $listCJ = [
                    'nama_penelitian' => $resData->nama_penelitian,
                    'jenis_layanan_nama' => $resData->jenis_layanan_nama,
                    'jenis_layanan_id' => $resData->jenis_layanan_id,
                    'tanggal_layanan' => $resData->tanggal_layanan,
                    'deleted_at' => $resData->deleted_at,
                ];

                $listFlatten = Collect([$listCJ])->crossJoin($listPecah);
                $listPecah = Collect([
                    [$resData->toArray()],
                    $this->mergeList($listFlatten)->toArray()
                ])->flatten(1)->toArray();
            } elseif ($resData->jenis_layanan_id == '7') {
            } else {
                $listPecah = [$resData->toArray()];
            }

            $layananHistory->push($listPecah);
        });
        return $layananHistory->flatten(1)->sortBy('id')->toArray();
    }

    private function mergeList($listFlat)
    {
        $listFlat->transform(function ($setFlat) {
            return array_merge($setFlat[0], $setFlat[1]);
        });
        return $listFlat;
    }

    public function mutasiPecah(Request $request)
    {
        if ($request->ajax()) {
            // return $request->all();
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $show = $request->show;
            $data = InformasiObjek::ObjekMutasiPecah($kd_kecamatan, $kd_kelurahan);
            $cetak = '0';
            return view('objek.mutasi_pecah_index', compact('data', 'cetak', 'show'));
        }

        $arRole = [310, 61, 182, 101, 62, 63, 64, 65, 81, 221];
        $show = 0;
        foreach (Auth()->user()->roles()->pluck('id')->toarray() as $i) {
            if (in_array($i, $arRole)) {
                $show = 1;
            }
        }

        $kecamatan = Kecamatan::login()->orderBy('kd_kecamatan', 'asc')->get();
        return view('objek.mutasi_pecah', compact('kecamatan', 'show'));
    }

    public function mutasiPecahPdf(Request $request)
    {

        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $show = '0';
        $lokasi = DB::connection("oracle_satutujuh")->table('ref_kecamatan')
            ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
            ->whereraw("ref_kelurahan.kd_kecamatan='$kd_kecamatan' and ref_kelurahan.kd_kelurahan='$kd_kelurahan'")
            ->first();
        $data = InformasiObjek::ObjekMutasiPecah($kd_kecamatan, $kd_kelurahan);
        $cetak = '1';
        $pdf = PDF::loadView('objek.mutasi_pecah_pdf', compact('data', 'cetak', 'lokasi', 'show'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
