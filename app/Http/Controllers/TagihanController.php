<?php

namespace App\Http\Controllers;

use App\Exports\DhkpWajibPajakExcel;
use App\Exports\excel_laporan_tagihan;
use App\Exports\PenagihanExcel;
use App\Helpers\Baku;
use App\Helpers\DataPBB;
use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class TagihanController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun_terbit = $request->tahun ?? '';
            $tahun_pajak = $request->tahun_pajak ?? "";
            $kd_kecamatan = $request->kd_kecamatan ?? null;
            $kd_kelurahan = $request->kd_kelurahan ?? null;

            $var_cetak = $request->getQueryString();

            if ($request->buku != '') {
                if ($request->buku == '1') {
                    $buku = [1, 2];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }

            $param = [
                'buku' => $buku,
                'is_dhkp' => false
            ];

            if ($kd_kecamatan != '') {
                $param['kd_kecamatan'] = $kd_kecamatan;
            }

            if ($kd_kelurahan != '') {
                $param['kd_kelurahan'] = $kd_kelurahan;
            }

            if ($tahun_pajak != '') {
                $param['tahun_pajak'] = $tahun_pajak;
            }
            if ($tahun_terbit != '') {
                $param['tahun_terbit'] = $tahun_terbit;
            }

            // return json_encode($param);
            $rekap = Baku::RekapDesa($param)->get();
            // return $rekap;
            return view('laporan_tagihan.index_table_dua', compact('rekap', 'param', 'var_cetak'));
        }

        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $preview = [];
        $original_name = "";
        $encript_name = "";
        $param = "";
        $data = (object)[
            'preview' => $preview,
            'param' => $param,
            'pdf' => $encript_name . '.pdf',
            'excel' => $encript_name . '.xlsx'
        ];

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start = 2003;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }

        $is_internal = Auth()->user()->roles()->pluck('is_internal')->toarray();
        array_unique($is_internal);
        $ordal = '0';
        // if (in_array('1', $is_internal)) {
        if (Auth()->user()->hasAnyRole(
            'ADMIN',
            'Kasi PDI',
            'Kasi Pelayanan',
            'Kepala Bidang',
            'Super User'
        )) {

            $ordal = '1';
        }

        return view('laporan_tagihan.index_dua', compact('kecamatan', 'data', 'role', 'tahun_pajak', 'kelurahan', 'ordal'));
    }

    public function indexDhkp(Request $request)
    {

        if ($request->ajax()) {
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan ?? null;
            $kd_kelurahan = $request->kd_kelurahan ?? null;

            if ($request->buku != '') {
                if ($request->buku == '1') {
                    $buku = [1, 2];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }


            if ($tahun == date('Y')) {
                $cut = date('Ymd');
            } else {
                $cut = $tahun . '1231';
            }

            $param = [
                'cut' => $cut,
                'buku' => $buku,
                'is_dhkp' => false
            ];

            if ($kd_kecamatan != '') {
                $param['kd_kecamatan'] = $kd_kecamatan;
            }

            if ($kd_kelurahan != '' or $kd_kelurahan != null) {
                $param['kd_kelurahan'] = $kd_kelurahan;
            }
            $param['cut_bayar'] = false;

            if ($tahun != '') {
                $param['tahun_terbit'] = $tahun;
            }

            $rekap = Baku::RekapDesaTunggakan($param)->get();
            // return $rekap;
            $var_cetak = $request->getQueryString();
            return view('laporan_tagihan.index_dhkp_dua_table', compact('rekap', 'param', 'var_cetak'));
        }

        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $preview = [];
        $original_name = "";
        $encript_name = "";
        $param = "";
        $data = (object)[
            'preview' => $preview,
            'param' => $param,
            'pdf' => $encript_name . '.pdf',
            'excel' => $encript_name . '.xlsx'
        ];

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start = 2003;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }

        $is_internal = Auth()->user()->roles()->pluck('is_internal')->toarray();
        array_unique($is_internal);
        $ordal = '0';
        // if (in_array('1', $is_internal)) {
        if (Auth()->user()->hasAnyRole(
            'ADMIN',
            'Kasi PDI',
            'Kasi Pelayanan',
            'Kepala Bidang',
            'Super User'
        )) {

            $ordal = '1';
        }

        return view('laporan_tagihan.index_dhkp_dua', compact('kecamatan', 'data', 'role', 'tahun_pajak', 'kelurahan', 'ordal'));

        // if ($request->ajax()) {

        //     $kd_kecamatan = $request->kd_kecamatan ?? '';
        //     $kd_kelurahan = $request->kd_kelurahan ?? '';
        //     $buku = $request->buku ?? '';
        //     $tahun_terbit = $request->tahun_terbit ?? '';
        //     $tahun_pajak = $request->tahun_pajak ?? '';
        //     if (

        //         $kd_kecamatan != '' ||
        //         $kd_kelurahan != '' ||
        //         $tahun_terbit != '' ||
        //         $tahun_pajak != ''
        //     ) {

        //         $thn = date('Y');
        //         $data = DataPBB::tunggakanTagihan()->whereraw("sppt.thn_pajak_sppt!='$thn' and ( pbb_yg_harus_dibayar_sppt- NVL (nilai_potongan, 0))>0");
        //         if ($kd_kecamatan != "") {
        //             $data = $data->where("sppt.kd_kecamatan", $kd_kecamatan);
        //         }

        //         if ($tahun_terbit != '') {
        //             $data = $data->whereraw("to_char(tgl_terbit_sppt,'yyyy')='$tahun_terbit'");
        //         }


        //         if ($tahun_pajak != '') {
        //             $data = $data->whereraw("sppt.thn_pajak_sppt='$tahun_pajak'");
        //         }

        //         if ($kd_kelurahan != '') {
        //             $data = $data->where("sppt.kd_kelurahan", $kd_kelurahan);
        //         }

        //         if ($buku != '') {
        //             if ($buku == '1') {
        //                 $data = $data->whereraw("kd_buku in ('1','2')");
        //             } else {
        //                 $data = $data->whereraw("kd_buku in ('3','4','5')");
        //             }
        //         }
        //     } else {
        //         $data = [];
        //     }
        //     $datatable = datatables::of($data)
        //         ->addColumn('nop', function ($row) {
        //             $nop = '';
        //             $nop .= $row->kd_propinsi . '.';
        //             $nop .= $row->kd_dati2 . '.';
        //             $nop .= $row->kd_kecamatan . '.';
        //             $nop .= $row->kd_kelurahan . '.';
        //             $nop .= $row->kd_blok . '-';
        //             $nop .= $row->no_urut . '.';
        //             $nop .= $row->kd_jns_op;
        //             return $nop;
        //         })
        //         ->addColumn('alamat_wp', function ($row) {
        //             $alamat = "";

        //             // $alamat .= $row->jln_wp_sppt . " ";
        //             // $alamat .= $row->blok_kav_no_wp_sppt . " ";
        //             $alamat .= $row->kelurahan_wp_sppt . " ";
        //             $alamat .= $row->kota_wp_sppt . " ";
        //             return $alamat;
        //         })
        //         ->addColumn('alamat_objek', function ($row) {
        //             return $row->nm_kelurahan . ' - ' . $row->nm_kecamatan;
        //         })
        //         ->addColumn('buku', function ($row) {
        //             return 'Buku ' . romawi($row->kd_buku);
        //         })
        //         ->editColumn('pbb', function ($row) {
        //             return angka($row->pbb);
        //         })
        //         ->editColumn('potongan', function ($row) {
        //             return angka($row->potongan);
        //         })
        //         ->addColumn('denda', function ($row) {
        //             return angka($row->es_denda);
        //         })
        //         ->addColumn('keterangan', function ($row) {
        //             /* $ket = $row->no_transaksi;
        //             $ket .= "<br>" . $row->keterangan; */
        //             return jenisBumi($row->jns_bumi);
        //         })
        //         ->rawColumns([
        //             'nop',
        //             'alamat_wp',
        //             'alamat_objek',
        //             'buku',
        //             'pbb',
        //             'potongan',
        //             'denda',
        //             'keterangan'
        //         ])
        //         ->make(true);
        //     return $datatable;
        // }
        // // $url = $request->url();
        // $arrTahun = [];
        // $tahun =  date('Y') - 1;
        // $start = 2003; //$tahun - 5;
        // for ($year = $tahun; $year >= $start; $year--) {
        //     $arrTahun[$year] = $year;
        // }

        // // dd($tahun_pajak);
        // $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        // $kelurahan = [];
        // if ($kecamatan->count() == '1') {
        //     $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
        //         ->login()->orderBy('nm_kelurahan', 'asc')
        //         ->select('nm_kelurahan', 'kd_kelurahan')
        //         ->get();
        // }
        // return view('laporan_tagihan/index_dhkp', compact('arrTahun', 'kecamatan', 'kelurahan'));
    }

    public function excelTunggakan(Request $request)
    {
        // return $request->all();
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan ?? null;
        $kd_kelurahan = $request->kelurahan ?? null;

        if ($request->buku != '') {
            if ($request->buku == '1') {
                $buku = [1, 2];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }


        if ($tahun == date('Y')) {
            $cut = date('Ymd');
        } else {
            $cut = $tahun . '1231';
        }

        $param = [
            'cut' => $cut,
            'buku' => $buku,
            'is_dhkp' => false
        ];

        if ($kd_kecamatan != '') {
            $param['kd_kecamatan'] = $kd_kecamatan;
        }

        if ($kd_kelurahan != '') {
            $param['kd_kelurahan'] = $kd_kelurahan;
        }
        $param['cut_bayar'] = false;

        if ($tahun != '') {
            $param['tahun_terbit'] = $tahun;
        }


        // return $param;
        $data =  Baku::get($param)
            ->join("dat_objek_pajak", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->join("dat_op_bumi", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
                // ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->join("sppt", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'sppt.kd_blok')
                    ->on('tmp.no_urut', '=', 'sppt.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->leftJoin("ref_iventarisasi", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'ref_iventarisasi.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'ref_iventarisasi.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'ref_iventarisasi.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'ref_iventarisasi.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'ref_iventarisasi.kd_blok')
                    ->on('tmp.no_urut', '=', 'ref_iventarisasi.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'ref_iventarisasi.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'ref_iventarisasi.thn_pajak_sppt');
            })
            ->select(DB::raw("tmp.*,nm_wp_sppt,jln_wp_sppt,blok_kav_no_wp_sppt,rw_wp_sppt,rt_wp_sppt,kelurahan_wp_sppt,kota_wp_sppt,
                            luas_bumi_sppt,luas_bng_sppt,tgl_jatuh_tempo_sppt,
                            GET_DENDA ((pbb-potongan),tgl_jatuh_tempo_sppt,sysdate) denda, get_buku(pbb) buku,jalan_op,blok_kav_no_op,rt_op,rw_op,
                            ref_iventarisasi.nm_kategori,jns_bumi"))
            ->whereRaw("nota=0 and koreksi =0 and bayar<(pbb-potongan)")
            //  and koreksi =0 and bayar<(pbb-potongan)")
            ->orderByRaw("nota nulls first,  tmp.kd_kecamatan,tmp.kd_kelurahan,tmp.kd_blok,tmp.no_urut")
            ->get();
        // return $data;
        $parseData = [];
        $no = 1;
        foreach ($data as $row) {
            $alamat_wp = "";
            if ($row->jln_wp_sppt != '') {
                $alamat_wp .= $row->jln_wp_sppt . " ";
            }
            if ($row->blok_kav_no_wp_sppt != '') {
                $alamat_wp .= $row->blok_kav_no_wp_sppt . " ";
            }

            if ($row->rt_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rt_wp_sppt . " ";
            }
            if ($row->rw_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rw_wp_sppt . " ";
            }
            if ($row->kelurahan_wp_sppt != '') {
                $alamat_wp .= $row->kelurahan_wp_sppt . " ";
            }
            if ($row->kota_wp_sppt != '') {
                $alamat_wp .= $row->kota_wp_sppt . " ";
            }

            $tgl_bayar = "";
            if ($row->tgl_bayar != '') {
                $tgl_bayar = tglIndo($row->tgl_bayar);
            }

            $tgl_koreksi = "";
            if ($row->tgl_koreksi != '') {
                $tgl_koreksi = tglIndo($row->tgl_koreksi);
            }

            $keterangan = "";
            if ($row->nm_kategori != '') {
                $keterangan .= $row->nm_kategori . ', ';
            }


            if ($keterangan != '') {
                $keterangan = substr($keterangan, 0, -2);
            }


            $alamat_op = "";

            if ($row->jalan_op != '') {
                $alamat_op .= $row->jalan_op . " ";
            }

            if ($row->blok_kav_no_op != '') {
                $alamat_op .= $row->blok_kav_no_op . " ";
            }

            if ($row->rt_op != '') {
                $alamat_op .= " RT " . $row->rt_op . " ";
            }

            if ($row->rw_op != '') {
                $alamat_op .= " RW " . $row->rw_op . " ";
            }

            $pbb_awal = $row->pbb_last;
            $pbb_perubahan = $row->perubahan;

            $parseData[] = [
                $no++,
                formatnop($row->kd_propinsi .
                    $row->kd_dati2 .
                    $row->kd_kecamatan .
                    $row->kd_kelurahan .
                    $row->kd_blok .
                    $row->no_urut .
                    $row->kd_jns_op),
                $alamat_op,
                $row->nm_wp_sppt,
                $alamat_wp,
                $row->thn_pajak_sppt,
                ($row->tgl_terbit_sppt != '' ?  tglIndo($row->tgl_terbit_sppt) : ''),
                'Buku ' . $row->buku,
                // $pbb_awal,
                // $pbb_perubahan,
                $row->pbb,
                $row->potongan,
                ($row->pbb - $row->potongan),
                $row->denda,
                $row->koreksi,
                $tgl_koreksi,
                $row->bayar,
                $tgl_bayar,
                jenisBumi($row->jns_bumi),
                $keterangan
            ];
        }
        return Excel::download(new PenagihanExcel($parseData), 'Tunggakan DHKP' . $kd_kecamatan . ' - ' . $kd_kelurahan . '.xlsx');
    }


    public function indexExcel(Request $request)
    {


        $tahun_terbit = $request->tahun ?? '';
        $tahun_pajak = $request->tahun_pajak ?? "";
        $kd_kecamatan = $request->kd_kecamatan ?? null;
        $kd_kelurahan = $request->kd_kelurahan ?? null;

        $var_cetak = $request->getQueryString();

        if ($request->buku != '') {
            if ($request->buku == '1') {
                $buku = [1, 2];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }

        $param = [
            // 'cut' => $cut,
            'buku' => $buku,
            'is_dhkp' => false
        ];

        if ($kd_kecamatan != '') {
            $param['kd_kecamatan'] = $kd_kecamatan;
        }

        if ($kd_kelurahan != '') {
            $param['kd_kelurahan'] = $kd_kelurahan;
        }

        if ($tahun_pajak != '') {
            $param['tahun_pajak'] = $tahun_pajak;
        }
        if ($tahun_terbit != '') {
            $param['tahun_terbit'] = $tahun_terbit;
        }



        $data =  Baku::get($param)
            ->join("dat_objek_pajak", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->join("dat_op_bumi", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
                // ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->join("sppt", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'sppt.kd_blok')
                    ->on('tmp.no_urut', '=', 'sppt.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->leftJoin("ref_iventarisasi", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'ref_iventarisasi.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'ref_iventarisasi.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'ref_iventarisasi.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'ref_iventarisasi.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'ref_iventarisasi.kd_blok')
                    ->on('tmp.no_urut', '=', 'ref_iventarisasi.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'ref_iventarisasi.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'ref_iventarisasi.thn_pajak_sppt');
            })
            ->select(DB::raw("tmp.*,nm_wp_sppt,jln_wp_sppt,blok_kav_no_wp_sppt,rw_wp_sppt,rt_wp_sppt,kelurahan_wp_sppt,kota_wp_sppt,
                                luas_bumi_sppt,luas_bng_sppt,tgl_jatuh_tempo_sppt,
                                GET_DENDA ((pbb-potongan),tgl_jatuh_tempo_sppt,sysdate) denda, get_buku(pbb) buku,jalan_op,blok_kav_no_op,rt_op,rw_op,
                                ref_iventarisasi.nm_kategori,jns_bumi
                                "))
            ->whereRaw("nota=0 and koreksi =0 and bayar<(pbb-potongan)")
            ->orderByRaw("nota nulls first,  tmp.kd_kecamatan,tmp.kd_kelurahan,tmp.kd_blok,tmp.no_urut")
            ->get();

        // return $data;

        $parseData = [];
        $no = 1;
        foreach ($data as $row) {
            $alamat_wp = "";
            if ($row->jln_wp_sppt != '') {
                $alamat_wp .= $row->jln_wp_sppt . " ";
            }
            if ($row->blok_kav_no_wp_sppt != '') {
                $alamat_wp .= $row->blok_kav_no_wp_sppt . " ";
            }

            if ($row->rt_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rt_wp_sppt . " ";
            }
            if ($row->rw_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rw_wp_sppt . " ";
            }
            if ($row->kelurahan_wp_sppt != '') {
                $alamat_wp .= $row->kelurahan_wp_sppt . " ";
            }
            if ($row->kota_wp_sppt != '') {
                $alamat_wp .= $row->kota_wp_sppt . " ";
            }

            $tgl_bayar = "";
            if ($row->tgl_bayar != '') {
                $tgl_bayar = tglIndo($row->tgl_bayar);
            }

            $tgl_koreksi = "";
            if ($row->tgl_koreksi != '') {
                $tgl_koreksi = tglIndo($row->tgl_koreksi);
            }

            $keterangan = "";
            if ($row->nm_kategori != '') {
                $keterangan .= $row->nm_kategori . ', ';
            }


            if ($keterangan != '') {
                $keterangan = substr($keterangan, 0, -2);
            }


            $alamat_op = "";

            if ($row->jalan_op != '') {
                $alamat_op .= $row->jalan_op . " ";
            }

            if ($row->blok_kav_no_op != '') {
                $alamat_op .= $row->blok_kav_no_op . " ";
            }

            if ($row->rt_op != '') {
                $alamat_op .= " RT " . $row->rt_op . " ";
            }

            if ($row->rw_op != '') {
                $alamat_op .= " RW " . $row->rw_op . " ";
            }

            $pbb_awal = $row->pbb_last;
            $pbb_perubahan = $row->perubahan;

            $parseData[] = [
                $no++,
                formatnop($row->kd_propinsi .
                    $row->kd_dati2 .
                    $row->kd_kecamatan .
                    $row->kd_kelurahan .
                    $row->kd_blok .
                    $row->no_urut .
                    $row->kd_jns_op),
                $alamat_op,
                $row->nm_wp_sppt,
                $alamat_wp,
                $row->thn_pajak_sppt,
                ($row->tgl_terbit_sppt != '' ?  tglIndo($row->tgl_terbit_sppt) : ''),
                'Buku ' . $row->buku,
                // $pbb_awal,
                // $pbb_perubahan,
                $row->pbb,
                $row->potongan,
                ($row->pbb - $row->potongan),
                $row->denda,
                $row->koreksi,
                $tgl_koreksi,
                $row->bayar,
                $tgl_bayar,
                jenisBumi($row->jns_bumi),
                $keterangan
            ];
        }
        // dd($parseData);
        // dd(count($parseData));


        // return $parseData;
        return Excel::download(new PenagihanExcel($parseData), 'Penagihan data ' . $kd_kecamatan . ' - ' . $kd_kelurahan . '.xlsx');
    }


    public function indexDhkpExcel(Request $request)
    {
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan ?? null;
        $kd_kelurahan = $request->kd_kelurahan ?? null;

        if ($request->buku != '') {
            if ($request->buku == '1') {
                $buku = [1, 2];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }


        if ($tahun == date('Y')) {
            $cut = date('Ymd');
        } else {
            $cut = $tahun . '1231';
        }

        $param = [
            'cut' => $cut,
            'buku' => $buku,
            'is_dhkp' => false
        ];

        if ($kd_kecamatan != '') {
            $param['kd_kecamatan'] = $kd_kecamatan;
        }

        if ($kd_kelurahan != '') {
            $param['kd_kelurahan'] = $kd_kelurahan;
        }
        $param['cut_bayar'] = false;

        if ($tahun != '') {
            $param['tahun_terbit'] = $tahun;
        }


        // return $param;
        $data =  Baku::get($param)
            ->join("dat_objek_pajak", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->join("dat_op_bumi", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
                // ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->join("sppt", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'sppt.kd_blok')
                    ->on('tmp.no_urut', '=', 'sppt.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->leftJoin("ref_iventarisasi", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'ref_iventarisasi.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'ref_iventarisasi.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'ref_iventarisasi.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'ref_iventarisasi.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'ref_iventarisasi.kd_blok')
                    ->on('tmp.no_urut', '=', 'ref_iventarisasi.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'ref_iventarisasi.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'ref_iventarisasi.thn_pajak_sppt');
            })
            ->select(DB::raw("tmp.*,nm_wp_sppt,jln_wp_sppt,blok_kav_no_wp_sppt,rw_wp_sppt,rt_wp_sppt,kelurahan_wp_sppt,kota_wp_sppt,
                            luas_bumi_sppt,luas_bng_sppt,tgl_jatuh_tempo_sppt,
                            GET_DENDA ((pbb-potongan),tgl_jatuh_tempo_sppt,sysdate) denda, get_buku(pbb) buku,jalan_op,blok_kav_no_op,rt_op,rw_op,
                            ref_iventarisasi.nm_kategori,jns_bumi
                            "))
            ->whereRaw("nota=0 and koreksi =0 and bayar<(pbb-potongan)")
            ->orderByRaw("nota nulls first,  tmp.kd_kecamatan,tmp.kd_kelurahan,tmp.kd_blok,tmp.no_urut")
            ->get();

        // return $data;

        $parseData = [];
        $no = 1;
        foreach ($data as $row) {
            $alamat_wp = "";
            if ($row->jln_wp_sppt != '') {
                $alamat_wp .= $row->jln_wp_sppt . " ";
            }
            if ($row->blok_kav_no_wp_sppt != '') {
                $alamat_wp .= $row->blok_kav_no_wp_sppt . " ";
            }

            if ($row->rt_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rt_wp_sppt . " ";
            }
            if ($row->rw_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rw_wp_sppt . " ";
            }
            if ($row->kelurahan_wp_sppt != '') {
                $alamat_wp .= $row->kelurahan_wp_sppt . " ";
            }
            if ($row->kota_wp_sppt != '') {
                $alamat_wp .= $row->kota_wp_sppt . " ";
            }

            $tgl_bayar = "";
            if ($row->tgl_bayar != '') {
                $tgl_bayar = tglIndo($row->tgl_bayar);
            }

            $tgl_koreksi = "";
            if ($row->tgl_koreksi != '') {
                $tgl_koreksi = tglIndo($row->tgl_koreksi);
            }

            $keterangan = "";
            if ($row->nm_kategori != '') {
                $keterangan .= $row->nm_kategori . ', ';
            }


            if ($keterangan != '') {
                $keterangan = substr($keterangan, 0, -2);
            }


            $alamat_op = "";

            if ($row->jalan_op != '') {
                $alamat_op .= $row->jalan_op . " ";
            }

            if ($row->blok_kav_no_op != '') {
                $alamat_op .= $row->blok_kav_no_op . " ";
            }

            if ($row->rt_op != '') {
                $alamat_op .= " RT " . $row->rt_op . " ";
            }

            if ($row->rw_op != '') {
                $alamat_op .= " RW " . $row->rw_op . " ";
            }

            $pbb_awal = $row->pbb_last;
            $pbb_perubahan = $row->perubahan;

            $parseData[] = [
                $no++,
                formatnop($row->kd_propinsi .
                    $row->kd_dati2 .
                    $row->kd_kecamatan .
                    $row->kd_kelurahan .
                    $row->kd_blok .
                    $row->no_urut .
                    $row->kd_jns_op),
                $alamat_op,
                $row->nm_wp_sppt,
                $alamat_wp,
                $row->thn_pajak_sppt,
                ($row->tgl_terbit_sppt != '' ?  tglIndo($row->tgl_terbit_sppt) : ''),
                'Buku ' . $row->buku,
                // $pbb_awal,
                // $pbb_perubahan,
                $row->pbb,
                $row->potongan,
                ($row->pbb - $row->potongan),
                $row->denda,
                $row->koreksi,
                $tgl_koreksi,
                $row->bayar,
                $tgl_bayar,
                jenisBumi($row->jns_bumi),
                $keterangan
            ];
        }
        // dd($parseData);
        // dd(count($parseData));


        // return $parseData;
        return Excel::download(new PenagihanExcel($parseData), 'Tunggakan DHKP' . $kd_kecamatan . ' - ' . $kd_kelurahan . '.xlsx');


        //         $tahun = $request->tahun ?? '';
        //         $kd_kecamatan = $request->kd_kecamatan ?? '';
        //         $kd_kelurahan = $request->kd_kelurahan ?? '';
        //         $buku = $request->buku ?? '';

        //         $thn = date('Y');
        //         $data = DataPBB::tunggakanTagihan()
        //             ->whereraw("sppt.thn_pajak_sppt!='$thn' and ( pbb_yg_harus_dibayar_sppt- NVL (nilai_potongan, 0))>0");
        //         $fn = "Tunggakan DHKP ";
        //         if ($kd_kecamatan != '') {
        //             $fn .= " " . $kd_kecamatan;
        //             $data = $data->where("sppt.kd_kecamatan", $kd_kecamatan);
        //         }

        //         $tahun_terbit = $request->tahun_terbit ?? '';
        //         if ($tahun_terbit != '') {
        //             $fn .= " terbit " . $tahun_terbit;
        //             $data = $data->whereraw("to_char(tgl_terbit_sppt,'yyyy')='$tahun_terbit'");
        //         }

        //         $tahun_pajak = $request->tahun_pajak ?? '';
        //         if ($tahun_pajak != '') {
        //             $fn .= " tahun pajak " . $tahun_pajak;
        //             $data = $data->whereraw("sppt.thn_pajak_sppt='$tahun_pajak'");
        //         }

        //         if ($kd_kelurahan != '') {
        //             $fn .= " " . $kd_kelurahan;
        //             $data = $data->where("sppt.kd_kelurahan", $kd_kelurahan);
        //         }

        //         if ($buku != '') {
        //             if ($buku == '1') {
        //                 $fn .= " Buku 1 dan 2";
        //                 $data = $data->whereraw("kd_buku in ('1','2')");
        //             } else {
        //                 $fn .= " Buku 3,4,5";
        //                 $data = $data->whereraw("kd_buku in ('3','4','5')");
        //             }
        //         }
        //         $data = $data->get();

        //         // dd($data);

        //         $array = [];
        //         foreach ($data as $i => $item) {
        //             $i += 1;

        //             // $keterangan = jenisBumi($item->jns_bumi);

        //             $status = in_array($item->jns_bumi, ['4', '5']) ? 'Non Aktif' : 'Aktif';

        //             $keterangan = "";
        //             /* if ($item->no_transaksi != '' && $item->no_transaksi != 'IV223112015') {

        //                 $keterangan .= " " . $item->no_transaksi;
        //             }
        // */
        //             if ($item->no_stpd != '') {
        //                 $keterangan .= " " . $item->no_stpd;
        //             }

        //             if ($item->kobil <> '') {
        //                 $keterangan .= ", " . $item->kobil;
        //             }
        //             $array[] = [
        //                 'no' => $i,
        //                 'nop' => $item->kd_propinsi . '.' . $item->kd_dati2 . '.' . $item->kd_kecamatan . '.' . $item->kd_kelurahan . '.' . $item->kd_blok .                '-' . $item->no_urut . '.' . $item->kd_jns_op,
        //                 'nm_wp_sppt' => $item->nm_wp_sppt,
        //                 'alamat_wp' => $item->jln_wp_sppt . " " . $item->blok_kav_no_wp_sppt . " " . ($item->rt_wp_sppt != '' ? 'RT ' . $item->rt_wp_sppt : '') . " " . ($item->rw_wp_sppt != '' ? 'RW ' . $item->rw_wp_sppt : '') . " " .  $item->kelurahan_wp_sppt . " " . $item->kota_wp_sppt,
        //                 'alamat_op' => $item->nm_kelurahan . ' ' . $item->nm_kecamatan,
        //                 'buku' => romawi($item->kd_buku),
        //                 'thn_pajak_sppt' => $item->thn_pajak_sppt,
        //                 // 'tgl_terbit_sppt' => tglIndo($item->tgl_terbit_sppt),
        //                 'pbb' => $item->pbb,
        //                 'potongan' => $item->potongan,
        //                 'es_denda' => $item->es_denda,
        //                 'keterangan' => $keterangan,
        //                 'status' => $status
        //             ];
        //             $i++;
        //         }


        //         // dd($array);

        //         $param = [
        //             'data' => $array,
        //             'jenis' => 2
        //         ];

        //         // , \Maatwebsite\Excel\Excel::CSV
        //         return Excel::download(new excel_laporan_tagihan($param), $fn . '.xlsx');
    }
}
