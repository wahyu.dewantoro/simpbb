<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Helpers\gallade;
use Carbon\Carbon;

class RekapJenisLayanan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        return view('layanan.rekap-jenis-layanan',compact('data'));
    }
    public function search(Request $request){
        $search=$request->only(['status','tgl']);
        if(!$search) return response()->json(['status' => false, 'msg' => "Data tidak ditemukan.",'data'=>[]]);
        $setData=[];
        $list = ['no', 'nama_layanan', 'total', 'aksi'];
        $listSelect='jenis_layanan.id,jenis_layanan.nama_layanan,count(layanan.NOMOR_LAYANAN) total';
        $getData=DB::table('jenis_layanan')
                ->select(DB::raw($listSelect));
        $whereRaw='jenis_layanan.id';
        if($search['tgl']!='-'){
            // $tgl=explode('-',str_replace(' ','',$search['tgl']));
            // $after=Carbon::createFromFormat('m/d/Y',$tgl[0])->format('d/m/Y');
            // $before=Carbon::createFromFormat('m/d/Y',$tgl[1])->format('d/m/Y');
            // if($after!=$before){
            //     $whereRaw.=" and layanan.created_at>=TO_DATE ('".$after."','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('".$before."','dd/mm/yyyy')";
            // }else{
            //     $whereRaw.=" and to_char(layanan.created_at,'dd/mm/yyyy')='".$after."'";
            // }
        }
        if($search['status']!='-'){
            $whereRaw.=" and layanan.kd_status='".$search['status']."'";
        }
        $getData=$getData->leftJoin('layanan', 'layanan.jenis_layanan_id', '=',db::raw($whereRaw))
                        ->groupBy(DB::raw('jenis_layanan.id,jenis_layanan.nama_layanan'))
                        ->orderBy('jenis_layanan.id')
                        ->get();
        if ($getData->count()>0) {
            $no='1';
            foreach($getData as $item){
                $btnDetail = gallade::buttonInfo('<i class="fas fa-search"></i>', 'title="Detail per Jenis Layanan" data-detail="' . $item->nama_layanan . '"');
                $result=[
                    'no'=>$no,
                    'nama_layanan'=>$item->nama_layanan,
                    'total'=>$item->total,
                    'aksi'=>$btnDetail,
                ];
                $setData[] = Arr::only($result, $list);
                $no++;
            }
        }
        $return = ["msg" => "Pencarian data berhasil.", "status" => true, 'data' => $setData];
        return response()->json($return);
    }
    public function detail(Request $request){

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
