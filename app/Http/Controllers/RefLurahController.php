<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\RefCamat;
use App\RefLurah;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class RefLurahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $basicQuery = RefLurah::join(db::raw("pbb.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->on('ref_kelurahan.kd_kelurahan', '=', 'ref_lurah.kd_kelurahan')
                    ->on('ref_kelurahan.kd_kecamatan', '=', 'ref_lurah.kd_kecamatan');
            })
                ->join(db::raw("pbb.ref_kecamatan ref_kecamatan"), 'ref_kecamatan.kd_kecamatan', '=', 'ref_lurah.kd_kecamatan')
                ->select(db::raw("ref_lurah.id,ref_lurah.kd_kecamatan,nm_kecamatan ,ref_lurah.kd_kecamatan,nm_kelurahan,nm_lurah,tgl_mulai,tgl_selesai,ref_lurah.kd_kelurahan "))
                ->orderbyraw("ref_lurah.kd_kecamatan,ref_lurah.kd_kelurahan asc");

            return DataTables::of($basicQuery)
                ->addColumn('aksi', function ($row) {
                    $edit = "";
                    $edit .= '<a data-id="' . $row->id . '"  data-nm_lurah="' . $row->nm_lurah . '" data-kd_kelurahan="' . $row->kd_kelurahan . '" data-kd_kecamatan="' . $row->kd_kecamatan . '" data-tgl_mulai="' . date('d M Y', strtotime($row->tgl_mulai)) . '" data-tgl_selesai="' . date('d M Y', strtotime($row->tgl_selesai)) . '"  class="edit" href="#" ><i class="fas fa-edit text-info"></i></a> ';
                    $edit .= '<a data-id="' . $row->id . '"  class="hapus" href="#" ><i class="fas fa-trash text-danger"></i></a>';
                    return $edit;
                })
                ->editColumn('tgl_mulai', function ($row) {
                    return tglIndo($row->tgl_mulai);
                })
                ->editColumn('tgl_selesai', function ($row) {
                    return tglIndo($row->tgl_selesai);
                })
                ->rawColumns(['aksi', 'tgl_mulai', 'tgl_selesai'])
                ->make(true);
        }

        $ref_kecamatan = Kecamatan::selectraw('kd_kecamatan, nm_kecamatan')->orderby('kd_kecamatan')->get();
        return view('lurah.index', compact('ref_kecamatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = [
                'nm_lurah' => $request->nm_lurah,
                'kd_kecamatan' => $request->kd_kecamatan,
                'kd_kelurahan' => $request->kd_kelurahan,
                'tgl_mulai' => new Carbon($request->tgl_mulai),
                'tgl_selesai' => new Carbon($request->tgl_selesai)
            ];
            RefLurah::create($data);
            DB::commit();
            $msg = "Berhasil menambah data";
        } catch (\Throwable $th) {
            DB::rollback();
            $msg = $th->getMessage();
        }
        return $msg;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = [
                'nm_lurah' => $request->nm_lurah_edit,
                'kd_kecamatan' => $request->kd_kecamatan_edit,
                'kd_kelurahan' => $request->kd_kelurahan_edit,
                'tgl_mulai' => new Carbon($request->tgl_mulai_edit),
                'tgl_selesai' => new Carbon($request->tgl_selesai_edit)
            ];
            RefLurah::where('id', $request->id)->update($data);
            DB::commit();
            $msg = "Berhasil edit data";
        } catch (\Throwable $th) {
            DB::rollback();
            $msg = $th->getMessage();
        }
        return $msg;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        db::beginTransaction();
        try {
            RefLurah::where('id', $id)->delete();
            DB::commit();
            $msg = "Berhasil di hapus";
        } catch (\Throwable $th) {
            DB::rollBack();
            $msg = $th->getMessage();
        }
        return $msg;
    }
}
