<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\Models\DatPetaBlok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class petaBLokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $tahun = date('Y');
            $data = DatPetaBlok::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                ->select(DB::raw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok, case when status_peta_blok='1' then 'SISMIOP' else 'SISTEP' end status_peta,status_peta_blok,(
                select count(distinct kd_blok) from dat_peta_znt where kd_kecamatan=DAT_PETA_BLOK.kd_kecamatan 
                and kd_kelurahan=DAT_PETA_BLOK.kd_kelurahan
                and kd_blok=DAT_PETA_BLOK.kd_blok
                ) pakai"))
                ->orderbyraw("kd_blok asc")
                ->get();
            return view('blokpeta/index_data', compact('data'));
        }

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        return view('blokpeta/index', compact('kecamatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function create()
    {
        //
    } */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            //code...

            // return $request->all();

            DatPetaBlok::create([
                'kd_propinsi' => '35',
                'kd_dati2' => '07',
                'kd_kecamatan' => $request->kd_kecamatan,
                'kd_kelurahan' => $request->kd_kelurahan,
                'kd_blok' => padding($request->kd_blok, '0', 3),
                'status_peta_blok' => $request->status_peta_blok
            ]);
            DB::connection('oracle_satutujuh')->commit();
            $status = 1;
            $msg = "Berhasil menambahkan blok";
        } catch (\Throwable $th) {
            DB::connection('oracle_satutujuh')->rollBack();

            $status = 0;
            $msg = $th->getMessage();
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // return $request->all();
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            //code...
            DatPetaBlok::where('kd_kecamatan', $request->kd_kecamatan)
                ->where('kd_kelurahan', $request->kd_kelurahan)
                ->where('kd_blok', $id)
                ->update([
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'kd_kecamatan' => $request->kd_kecamatan,
                    'kd_kelurahan' => $request->kd_kelurahan,
                    'kd_blok' => padding($request->kd_blok, '0', 3),
                    'status_peta_blok' => $request->status_peta_blok
                ]);
            DB::connection('oracle_satutujuh')->commit();
            $status = 1;
            $msg = "Berhasil merubah blok";
        } catch (\Throwable $th) {
            DB::connection('oracle_satutujuh')->rollBack();

            $status = 0;
            $msg = $th->getMessage();
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = explode('_', $id);
        if (count($cek) == 3) {
            $kd_kecamatan = $cek[0];
            $kd_kelurahan = $cek[1];
            $kd_blok = $cek[2];

            // hapus dat nir
            db::beginTransaction();
            try {

                DatPetaBlok::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_blok', $kd_blok)->delete();

                db::commit();
                $flash = [
                    'success' => 'Berhasil menghapus Blok'
                ];
            } catch (\Throwable $th) {
                //throw $th;
                db::rollBack();

                $flash = [
                    'error' => $th->getMessage()
                ];
            }

            return redirect(url('refrensi/blok-peta') . '?kd_kecamatan=' . $kd_kecamatan . '&kd_kelurahan=' . $kd_kelurahan)->with($flash);
        }
    }
}
