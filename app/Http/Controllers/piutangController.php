<?php

namespace App\Http\Controllers;

use App\Exports\detailPiutangExport;
use App\Exports\PiutangBukuBesarExcell;
use App\Exports\piutangTahunExcell;
use App\Exports\rekapPiutangExport;
use App\Helpers\Piutang;
use App\Kecamatan;
use App\Kelurahan;
use App\Sppt;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Stmt\Foreach_;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

// use BayAreaWebPro\SimpleCsv\SimpleCsv;

class piutangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            set_time_limit(1000);
            $tanggal = $request->tanggal ?? date('dmY');

            $tanggal = date('dmY', strtotime($tanggal));
            $data = Piutang::RekapPerTahun($tanggal);
            return  DataTables::of($data)
                ->addColumn('action', function ($row) {
                    return "-";
                    // tahun_piutang
                })
                ->rawcolumns(['action'])
                ->make(true);
            /*  $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $thn_pajak_sppt = $request->thn_pajak_sppt;
            $status_objek = $request->status_objek;

            $data = Piutang::PerNop($kd_kecamatan, $kd_kelurahan, $thn_pajak_sppt, $status_objek);
            return  DataTables::of($data)
                ->editColumn('pbb', function ($row) {
                    return angka($row->pbb);
                })
                ->editColumn('bayar', function ($row) {
                    return angka($row->bayar);
                })
                ->editColumn('is_aktif', function ($row) {
                    if ($row->is_aktif == '1') {
                        $r = 'Aktif';
                    } else {
                        $r = 'Non Aktif';
                    }
                    return $r;
                })
                ->editColumn('kurang_bayar', function ($row) {
                    return angka($row->kurang_bayar);
                })
                ->rawcolumns([
                    'pbb',
                    'bayar',
                    'kurang_bayar',
                    'is_aktif'
                ])
                ->make(true); */
        }

        /*    $tahun = [];
        $a = strtotime(date('Y') . '0831');
        $b = strtotime(date('Ymd'));
        if ($a > $b) {
            $st = date('Y') - 1;
        } else {
            $st = date('Y');
        }

        for ($i = $st; $i >= 2003; $i--) {
            $tahun[] = $i;
        }

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        } */
        return view('piutang.index');
    }


    public function rekap(Request $request)
    {

        if ($request->ajax()) {
            $kd_kecamatan = $request->kd_kecamatan;
            $tahun = $request->thn_pajak_sppt;

            $data = Piutang::Rekap($kd_kecamatan, $tahun);
            return  DataTables::of($data)
                ->editColumn('ketetapan', function ($row) {
                    return angka($row->ketetapan);
                })
                ->editColumn('pengurang', function ($row) {
                    return angka($row->pengurang);
                })
                ->editColumn('pokok_terbayar', function ($row) {
                    return angka($row->pokok_terbayar);
                })
                ->editColumn('koreksi_piutang', function ($row) {
                    return angka($row->koreksi_piutang);
                })
                ->addColumn('sisa', function ($row) {
                    $sisa =  $row->ketetapan - ($row->pengurang + $row->pokok_terbayar + $row->koreksi_piutang);

                    return angka($sisa);
                })
                ->rawcolumns([
                    'ketetapan',
                    'pengurang',
                    'pokok_terbayar',
                    'koreksi_piutang',
                    'sisa',
                ])
                ->make(true);
        }

        for ($i = date('Y') - 1; $i >= 2003; $i--) {
            $tahun[] = $i;
        }

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('piutang.indexRekap', compact('kecamatan', 'kelurahan', 'tahun'));
    }


    public function indexCetak(Request $request)
    {
        $turut = $request->turut ?? 5;
        $buku = $request->buku ?? '3,4,5';
        $param = ['turut' => $turut, 'buku' => $buku];
        return Excel::download(new rekapPiutangExport($param), 'rekap piutang ' . time() . '.xlsx');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kecamatan,  Request $request)
    {
        $turut = $request->turut ?? 5;
        $buku = $request->buku ?? '3,4,5';
        $piutang = Piutang::detailKecamatan($kecamatan, $turut, $buku)->simplepaginate(10);
        // dd($piutang);
        $kecamatan = Kecamatan::where('kd_kecamatan', $kecamatan)->first();
        $tahun = date('Y');
        return view('piutang.show', compact('piutang', 'tahun', 'kecamatan', 'turut', 'buku'));
    }

    public function showCetak(Request $request)
    {
        $param = [
            'kecamatan' => $request->kecamatan ?? null,
            'turut' => $request->turut ?? 5,
            'buku' => $request->buku ?? '3,4,5'
        ];
        return Excel::download(new detailPiutangExport($param), 'detail piutang ' . time() . '.xlsx');
    }

    public function test(Request $request)
    {
        $tahun = 2021;

        $core = Sppt::selectraw("kd_kecamatan,kd_kelurahan")->Whereraw("thn_pajak_sppt='$tahun' and status_pembayaran_sppt='0'")->distinct()->get();
        $filename = 'Piutang ' . $tahun . '.CSV';
        $columns = [['NOP', 'Alamat_OP', 'Kelurahan', 'Kecamatan', 'Tahun', 'Wajib_Pajak', 'Alamat', 'PBB', 'Buku']];
        foreach ($core as $rc) {
            $sppt = DB::connection("oracle_dua")->table(DB::raw('sppt a'))->select(DB::raw("a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op,jalan_op,blok_kav_no_op, nm_kelurahan,nm_kecamatan ,thn_pajak_sppt,nm_wp_sppt ,jln_wp_sppt,blok_kav_no_wp_sppt, kelurahan_wp_sppt,kota_wp_sppt,pbb_yg_harus_dibayar_sppt pbb,get_buku(pbb_yg_harus_dibayar_sppt) buku"))
                ->join(DB::raw('dat_objek_pajak b'), function ($join) {
                    $join->on(DB::raw('a.kd_propinsi'), '=', DB::raw('b.kd_propinsi'))
                        ->on(DB::raw('a.kd_dati2'), '=', DB::raw('b.kd_dati2'))
                        ->on(DB::raw('a.kd_kecamatan'), '=', DB::raw('b.kd_kecamatan'))
                        ->on(DB::raw('a.kd_kelurahan'), '=', DB::raw('b.kd_kelurahan'))
                        ->on(DB::raw('a.kd_blok'), '=', DB::raw('b.kd_blok'))
                        ->on(DB::raw('a.no_urut'), '=', DB::raw('b.no_urut'))
                        ->on(DB::raw('a.kd_jns_op'), '=', DB::raw('b.kd_jns_op'));
                })
                ->join(DB::raw('ref_kecamatan c'), function ($join) {
                    $join->on(DB::raw('a.kd_kecamatan'), '=', DB::raw('c.kd_kecamatan'));
                })
                ->join(DB::raw('ref_kelurahan d'), function ($join) {
                    $join->on(DB::raw('a.kd_kecamatan'), '=', DB::raw('d.kd_kecamatan'))
                        ->on(DB::raw('a.kd_kelurahan'), '=', DB::raw('d.kd_kelurahan'));
                })->whereraw("thn_pajak_sppt='$tahun' and status_pembayaran_sppt='0' and a.kd_Kecamatan='" . $rc->kd_kecamatan . "' and a.kd_kelurahan='" . $rc->kd_kelurahan . "'")->get();
            foreach ($sppt as $row) {
                $data['NOP'] = $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                $data['Alamat_OP'] = ($row->jalan_op ?? '') . ' ' . ($row->blok_kav_no_op ?? '');
                $data['Kelurahan'] = $row->nm_kelurahan ?? '';
                $data['Kecamatan'] = $row->nm_kecamatan ?? '';
                $data['Tahun'] = $row->thn_pajak_sppt ?? '';
                $data['Wajib_Pajak'] = $row->nm_wp_sppt ?? '';
                $data['Alamat'] = ($row->jln_wp_sppt ?? '') . ' ' . ($row->blok_kav_no_wp_sppt ?? '') . ' ' . ($row->kelurahan_wp_sppt ?? '') . ' ' . ($row->kota_wp_sppt ?? '');
                $data['PBB'] = $row->pbb;
                $data['Buku'] = $row->buku;
                $columns[] = array($data['NOP'], $data['Alamat_OP'], $data['Kelurahan'], $data['Kecamatan'], $data['Tahun'], $data['Wajib_Pajak'], $data['Alamat'], $data['PBB'], $data['Buku']);
            }
        }
    }

    private function sqlPivotPiutang($tahuna, $tahunb, $buku)
    {


        $scripta = "";
        $scriptb = "";

        switch ($buku) {
            case '1':
                # code...
                $wbuku = "('1', '2')";
                break;
            case '2':
                # code...
                $wbuku = "('3','4','5')";
                break;
            default:
                # code...
                $wbuku = "('1', '2','3','4','5')";
                break;
        }


        for ($i = $tahuna; $i <= $tahunb; $i++) {
            $scripta .= "SUM (pbb_" . $i . ") pbb_" . $i . " ,";
            $scriptb .= "CASE
            WHEN sppt.thn_pajak_sppt = '" . $i . "'
            THEN
               pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
            ELSE
               NULL
         END
            pbb_" . $i . ",";
        }
        $scripta = substr($scripta, 0, -1);
        $scriptb = substr($scriptb, 0, -1);





        $sql = "SELECT kd_propinsi,
        kd_dati2,
        kd_kecamatan,
        kd_kelurahan,
        kd_blok,
        no_urut,
        kd_jns_op,
        " . $scripta . "
   FROM (SELECT sppt.kd_propinsi,
                sppt.kd_dati2,
                sppt.kd_kecamatan,
                sppt.kd_kelurahan,
                sppt.kd_blok,
                sppt.no_urut,
                sppt.kd_jns_op,
                " . $scriptb . "
           FROM sppt
                LEFT JOIN sppt_potongan
                   ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                      AND sppt.kd_dati2 = sppt_potongan.kd_dati2
                      AND sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
                      AND sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
                      AND sppt.kd_blok = sppt_potongan.kd_blok
                      AND sppt.no_urut = sppt_potongan.no_urut
                      AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
                      AND sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
                LEFT JOIN
                (SELECT a.kd_propinsi,
                        a.kd_dati2,
                        a.kd_kecamatan,
                        a.kd_kelurahan,
                        a.kd_blok,
                        a.no_urut,
                        a.kd_jns_op,
                        a.thn_pajak_sppt,
                        kd_status
                   FROM sim_pbb.sppt_penelitian a
                        LEFT JOIN SIM_PBB.DATA_BILLING b
                           ON a.data_billing_id = b.data_billing_id
                  WHERE kd_status = '0') aj
                   ON     aj.kd_propinsi = sppt.kd_propinsi
                      AND aj.kd_dati2 = sppt.kd_dati2
                      AND aj.kd_kecamatan = sppt.kd_kecamatan
                      AND aj.kd_kelurahan = sppt.kd_kelurahan
                      AND aj.kd_blok = sppt.kd_blok
                      AND aj.no_urut = sppt.no_urut
                      AND aj.kd_jns_op = sppt.kd_jns_op
                      AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
                JOIN ref_buku
                   ON     sppt.thn_pajak_sppt BETWEEN ref_buku.thn_awal
                                                  AND ref_buku.thn_akhir
                      AND sppt.pbb_yg_harus_dibayar_sppt BETWEEN ref_buku.nilai_min_buku
                                                             AND ref_buku.nilai_max_buku
          WHERE     sppt.thn_pajak_sppt BETWEEN " . $tahuna . " AND " . $tahunb . "
                AND aj.thn_pajak_sppt IS NULL
                AND sppt.status_pembayaran_sppt IN ('0')
                AND REF_BUKU.KD_BUKU IN " . $wbuku . "  ) pv
GROUP BY kd_propinsi,
        kd_dati2,
        kd_kecamatan,
        kd_kelurahan,
        kd_blok,
        no_urut,
        kd_jns_op";

        return db::connection("oracle_satutujuh")->table(db::raw("(" . $sql . ") pivot"));
    }


    public function bukuBesar(Request $request)
    {
        ini_set('memory_limit', -1);
        $thn_awal = $request->thn_awal ?? date('Y') - 4;
        $thn_akhir = $request->thn_akhir ?? date('Y') - 1;
        $kd_buku = $request->kd_buku ?? '';
        $list_tahun = [];
        for ($i = date('Y'); $i >= 2003; $i--) {
            $list_tahun[] = $i;
        }

        $data = $this->sqlPivotPiutang($thn_awal, $thn_akhir, $kd_buku)
            ->paginate(10)
            ->appends(['thn_awal' => $thn_awal, 'thn_akhir' => $thn_akhir, 'kd_buku' => $kd_buku]);

        return view('piutang.buku_besar', compact('data', 'list_tahun', 'thn_awal', 'thn_akhir', 'kd_buku'));
    }

    public function  bukuBesarexcel(Request $request)
    {
        ini_set('memory_limit', -1);
        $thn_awal = $request->thn_awal ?? date('Y') - 4;
        $thn_akhir = $request->thn_akhir ?? date('Y') - 1;
        $kd_buku = $request->kd_buku ?? '';
        $data = $this->sqlPivotPiutang($thn_awal, $thn_akhir, $kd_buku)->get()->toArray();

        $param = [
            'data' => $data,
            'thn_awal' => $thn_awal,
            'thn_akhir' => $thn_akhir,
        ];
        return Excel::download(new PiutangBukuBesarExcell($param), 'Data piutang.xlsx');
    }
}
