<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function login()
    {
        if (Auth::attempt(['username' => request('username'), 'password' => request('password')])) {
            $user = Auth::user();
            $token=Carbon::now();
            $credential=$user->createToken($token);
            $response['status'] = true;
            $response['message'] = 'Berhasil login';
            $response['data']['token'] = 'Bearer ' . $credential->accessToken;

            return response()->json($response, 200);
        } else {
            $response['status'] = false;
            $response['message'] = 'Unauthorized';

            return response()->json($response, 401);
        }
    }
}
