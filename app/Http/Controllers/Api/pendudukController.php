<?php

namespace App\Http\Controllers\Api;

use App\Helpers\LayananUpdate;
use App\Http\Controllers\Controller;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class pendudukController extends Controller
{
    //

    public function index(Request $request)
    {
        // $response = Http::get('http://192.168.1.205/feeder200', [
        //     'nik' => $request->nik
        // ]);
        $where=$request->nik;
        $default=['nik'=>'',
                'no_kk'=>'',
                'nama_lgkp'=>'',
                'tmpt_lhr'=>'',
                'prop_name'=>'',
                'kab_name'=>'',
                'kec_name'=>'',
                'kel_name'=>'',
                'no_prop'=>'',
                'no_kab'=>'',
                'no_kec'=>'',
                'no_kel'=>'',
                'alamat'=>'',
                'no_rw'=>'',
                'no_rt'=>'',
                'rt_wp_baru'=>'',
                'rw_wp_baru'=>'',
                'jenis_pkrjn'=>'',
                'jenis_klmin'=>'',
                'tgl_lhr'=>'',
                'nomor_telepon'=>'',
                'agama_wp'=>'',
                'blok_kav_no_wp'=>'',
                'email'=>'',
                'rt_rw_wp'=>''
                
        ];
        $selectSP=[
            db::raw('dat_subjek_pajak.subjek_pajak_id nik'),
            db::raw('dat_subjek_pajak.nm_wp nama_lgkp'),
            db::raw('dat_subjek_pajak.jalan_wp alamat'),
            db::raw('dat_subjek_pajak.rw_wp no_rw'),
            db::raw('dat_subjek_pajak.rt_wp no_rt'),
            db::raw('dat_subjek_pajak.kelurahan_wp kel_name'),
            db::raw('dat_subjek_pajak.kota_wp kab_name'),
            db::raw('null kec_name'),
            // db::raw('dat_subjek_pajak.propinsi_wp prop_name'),
        ];
        $data17 = DB::connection('oracle_satutujuh')
            ->table(db::raw('dat_subjek_pajak'))
            ->select($selectSP)
            ->whereRaw(db::raw("dat_subjek_pajak.subjek_pajak_id=?"),$where)
            ->get();
        $result=['kode'=>false,'raw'=>$default,'keterangan'=>'Data tidak ditemukan.'];
        if($data17->count()){
            foreach($data17->first() as $key=>$item){
                if(in_array($key,array_keys($default))){
                    $default[$key]=$item;
                }
            }
            $rt_wp=sprintf("%03s", str_replace(" ","",$default['no_rt']));
            $rw_wp=sprintf("%02s", str_replace(" ","",$default['no_rw']));
            $default['rt_rw_wp']=$rt_wp."/".$rw_wp;
            $default['kab_name']=str_replace('-','.',$default['kab_name']);
            $kab_name=explode('.',$default['kab_name']);
            if(count($kab_name)>=2){
                $default['prop_name']=$kab_name[count($kab_name)-1];
                $default['kab_name']=$kab_name[0];
            }
            $default['kel_name']=str_replace('-','.',$default['kel_name']);
            $kel_name=explode('.',$default['kel_name']);
            if(count($kel_name)>=2){
                $default['kec_name']=$kel_name[count($kel_name)-1];
                $default['kel_name']=$kel_name[0];
            }
            $result=['kode'=>true,'raw'=>$default,'keterangan'=>'Data ditemukan.'];
        }else{
            $selectLayanan=[
                db::raw('layanan_objek.nik_wp nik'),
                db::raw('layanan_objek.nama_wp nama_lgkp'),
                db::raw('layanan_objek.propinsi_wp prop_name'),
                db::raw('layanan_objek.dati2_wp kab_name'),
                db::raw('layanan_objek.kecamatan_wp kec_name'),
                db::raw('layanan_objek.kelurahan_wp kel_name'),
                db::raw('layanan_objek.alamat_wp alamat'),
                db::raw('layanan_objek.rw_wp no_rw'),
                db::raw('layanan_objek.rt_wp no_rt'),
                db::raw('layanan_objek.rw_wp rw_wp_baru'),
                db::raw('layanan_objek.rt_wp rt_wp_baru'),
                'layanan_objek.agama_wp',
                'layanan.nomor_telepon',
                'layanan.email',
            ];
            $dataLayanan=Layanan_objek::select($selectLayanan)
                    ->join('layanan',function($join){
                        $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
                    })
                    ->where(['layanan_objek.nik_wp'=>$where])
                    ->get();
            if($dataLayanan->count()){
                // dd($dataLayanan->toArray());
                foreach($dataLayanan->first()->toArray() as $key=>$item){
                    if(in_array($key,array_keys($default))){
                        if($key=='alamat'){
                            $item=explode('-',$item);
                            if(isset($item[1])&&is_array($item)){
                                $default['blok_kav_no_wp']=$item[1];
                            }
                            $item=$item[0];
                        }
                        $default[$key]=$item;
                    }
                }
                $rt_wp=sprintf("%03s", str_replace(" ","",$default['no_rt']));
                $rw_wp=sprintf("%02s", str_replace(" ","",$default['no_rw']));
                $default['rt_rw_wp']=$rt_wp."/".$rw_wp;
                $default['kab_name']=str_replace('-','.',$default['kab_name']);
                $kab_name=explode('.',$default['kab_name']);
                if(count($kab_name)>=2){
                    $default['prop_name']=$kab_name[count($kab_name)-1];
                    $default['kab_name']=$kab_name[0];
                }
                $default['kel_name']=str_replace('-','.',$default['kel_name']);
                $kel_name=explode('.',$default['kel_name']);
                if(count($kel_name)>=2){
                    $default['kec_name']=$kel_name[count($kel_name)-1];
                    $default['kel_name']=$kel_name[0];
                }
                $result=['kode'=>true,'raw'=>$default,'keterangan'=>'Data ditemukan.'];
            }else{
                $selectL=[
                    db::raw('layanan.*'),
                    db::raw('nama nama_lgkp'),
                    db::raw('propinsi prop_name'),
                    db::raw('dati2 kab_name'),
                    db::raw('kecamatan kec_name'),
                    db::raw('kelurahan kel_name')
                ];
                $layanan=Layanan::select($selectL)
                        ->where(['nik'=>$where])
                        ->get();
                if($layanan->count()){
                    foreach($layanan->first()->toArray() as $key=>$item){
                        if(in_array($key,array_keys($default))){
                            $default[$key]=$item;
                        }
                    }
                    $rt_wp=sprintf("%03s", str_replace(" ","",$default['no_rt']));
                    $rw_wp=sprintf("%02s", str_replace(" ","",$default['no_rw']));
                    $default['rt_rw_wp']=$rt_wp."/".$rw_wp;
                    $default['kab_name']=str_replace('-','.',$default['kab_name']);
                    $kab_name=explode('.',$default['kab_name']);
                    if(count($kab_name)>=2){
                        $default['prop_name']=$kab_name[count($kab_name)-1];
                        $default['kab_name']=$kab_name[0];
                    }
                    $default['kel_name']=str_replace('-','.',$default['kel_name']);
                    $kel_name=explode('.',$default['kel_name']);
                    if(count($kel_name)>=2){
                        $default['kec_name']=$kel_name[count($kel_name)-1];
                        $default['kel_name']=$kel_name[0];
                    }
                    $result=['kode'=>true,'raw'=>$default,'keterangan'=>'Data ditemukan.'];
                }
            }
        }
        //return $response;
        // return response()->json($response);
        // return response()->json(json_decode($response));
        return response()->json($result);
    }
    public function nop(Request $request)
    {
        $setdata=$request->only('nop');
        $return['status']=false;
        $return['msg']='NOP tidak ditemukan.';
        try{
            if($setdata){
                $nop=explode(".",$setdata['nop']);
                $blok_urut=explode("-",$nop[4]);
                $tglNow=Carbon::now();
                $tahun=$tglNow->year;
                $sql="select  dat_subjek_pajak.subjek_pajak_id nik_wp,
                            total_luas_bumi luas_bumi,
                            total_luas_bng luas_bng,
                            njop_bumi,
                            njop_bng ,
                            (select kd_kls_tanah
                        from kelas_tanah
                        where thn_awal_kls_tanah <=".$tahun." and thn_akhir_kls_tanah>=".$tahun."
                        and  nilai_min_tanah<= ( njop_bumi/NULLIF(total_luas_bumi,0) /1000) and nilai_max_tanah>=( njop_bumi/NULLIF(total_luas_bumi,0) /1000)
                        and lower(kd_kls_tanah) not like 'x%')
                        kd_kls_tanah,
                            case when total_luas_bng=0  then null else                   
                            (select kd_kls_bng
                        from kelas_bangunan
                        where thn_awal_kls_bng <=".$tahun." and thn_akhir_kls_bng>=".$tahun."
                        and  nilai_min_bng<= ( njop_bng/NULLIF(total_luas_bng,0) /1000) and nilai_max_bng>=( njop_bng/NULLIF(total_luas_bng,0) /1000)
                        and lower(kd_kls_bng) not like 'x%') end 
                            kd_kls_bng,
                            dat_subjek_pajak.nm_wp nama_wp,
                            dat_subjek_pajak.jalan_wp alamat_wp,
                            dat_subjek_pajak.rt_wp,
                            dat_subjek_pajak.rw_wp,
                            dat_subjek_pajak.kelurahan_wp,
                            '' kecamatan_wp,
                            dat_subjek_pajak.telp_wp,
                            dat_subjek_pajak.kota_wp dati2_wp,
                            '' propinsi_wp,
                            dat_objek_pajak.jalan_op nop_alamat,
                            dat_objek_pajak.rt_op nop_rt,
                            dat_objek_pajak.rw_op nop_rw,
                            ref_kelurahan.nm_kelurahan nop_kelurahan,
                            ref_kecamatan.nm_kecamatan nop_kecamatan,
                            'KABUPATEN MALANG' nop_dati2,
                            'JAWA TIMUR' nop_propinsi,
                            dat_objek_pajak.jns_transaksi_op,
                            dob.jns_bumi
                from pbb.dat_objek_pajak dat_objek_pajak
                join pbb.dat_subjek_pajak dat_subjek_pajak
                            on dat_subjek_pajak.subjek_pajak_id=dat_objek_pajak.subjek_pajak_id
                LEFT JOIN dat_op_bumi dob 
                    ON dob.kd_propinsi=dat_objek_pajak.kd_propinsi
                    AND dob.kd_dati2=dat_objek_pajak.kd_dati2
                    AND dob.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                    AND dob.kd_kelurahan=dat_objek_pajak.kd_kelurahan
                    AND dob.kd_blok=dat_objek_pajak.kd_blok
                    AND dob.no_urut=dat_objek_pajak.no_urut
                    AND dob.kd_jns_op=dat_objek_pajak.kd_jns_op
                join pbb.ref_kecamatan ref_kecamatan
                            on ref_kecamatan.kd_propinsi=dat_objek_pajak.kd_propinsi
                            and ref_kecamatan.kd_dati2=dat_objek_pajak.kd_dati2
                            and ref_kecamatan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                        join pbb.ref_kelurahan ref_kelurahan
                            on ref_kelurahan.kd_propinsi=dat_objek_pajak.kd_propinsi
                            and ref_kelurahan.kd_dati2=dat_objek_pajak.kd_dati2
                            and ref_kelurahan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                            and ref_kelurahan.kd_kelurahan=dat_objek_pajak.kd_kelurahan
                            WHERE dat_objek_pajak.kd_propinsi='".$nop[0]."'
                        AND dat_objek_pajak.kd_dati2='".$nop[1]."'
                        AND dat_objek_pajak.kd_kecamatan='".$nop[2]."'
                        AND dat_objek_pajak.kd_kelurahan='".$nop[3]."'
                        AND dat_objek_pajak.kd_blok='".$blok_urut[0]."'
                        AND dat_objek_pajak.no_urut='".$blok_urut[1]."'
                        AND dat_objek_pajak.kd_jns_op='".$nop[5]."'";
                $result=DB::connection('oracle_satutujuh')->select(db::raw($sql));
                // dat_subjek_pajak.
                //     left outer join dat_objek_pajak
                //          on dat_objek_pajak.kd_propinsi=sp.kd_propinsi
                //         and dat_objek_pajak.kd_dati2=sp.kd_dati2
                //         and dat_objek_pajak.kd_kecamatan=sp.kd_kecamatan
                //         and dat_objek_pajak.kd_kelurahan=sp.kd_kelurahan
                //         and dat_objek_pajak.kd_blok=sp.kd_blok
                //         and dat_objek_pajak.no_urut=sp.no_urut
                //         and dat_objek_pajak.kd_jns_op=sp.kd_jns_op
                //     left outer join dat_subjek_pajak
                //         on dat_subjek_pajak.subjek_pajak_id=dat_objek_pajak.subjek_pajak_id
                if($result){
                    $return['data'] = (array)current($result);
                    $return['data']['dati2_wp']=str_replace('-','.',$return['data']['dati2_wp']);
                    $dati2=explode('.',$return['data']['dati2_wp']);
                    if(count($dati2)>=2){
                        $return['data']['propinsi_wp']=$dati2[count($dati2)-1];
                        $return['data']['dati2_wp']=$dati2[0];
                    }
                    $return['data']['kelurahan_wp']=str_replace('-','.',$return['data']['kelurahan_wp']);
                    $kelurahan_wp=explode('.',$return['data']['kelurahan_wp']);
                    if(count($kelurahan_wp)>=2){
                        $return['data']['kecamatan_wp']=$kelurahan_wp[count($kelurahan_wp)-1];
                        $return['data']['kelurahan_wp']=$kelurahan_wp[0];
                    }
                    $nop_alamat=explode('-',$return['data']['nop_alamat']);
                    $return['data']['blok_kav_no_op']='';
                    if(count($nop_alamat)>=2){
                        $return['data']['blok_kav_no_op']=$nop_alamat[count($nop_alamat)-1];
                        $return['data']['nop_alamat']=$nop_alamat[0];
                    }
                    $rt_wp=sprintf("%03s", str_replace(" ","",$return['data']['rt_wp']));
                    $rw_wp=sprintf("%02s", str_replace(" ","",$return['data']['rw_wp']));
                    $nop_rt=sprintf("%03s", str_replace(" ","",$return['data']['nop_rt']));
                    $nop_rw=sprintf("%02s", str_replace(" ","",$return['data']['nop_rw']));
                    $return['data']['rt_rw_wp']=$rt_wp.'/'.$rw_wp;
                    $return['data']['notelp_wp']=$return['data']['telp_wp'];
                    $return['data']['rt_rw_op']=$nop_rt.'/'.$nop_rw;
                    $return['data']['alamat_op']=$return['data']['nop_alamat'];
                    $return['data']['nik_wp']=substr($return['data']['nik_wp'],0,16);
                    // blok_kav_no_op
                    if(!$request->only('noTag')){
                        $return['tag']=[
                            'riwayat_pembayaran'=>LayananUpdate::riwayatPembayaran($setdata)
                        ];
                    }
                    $return['status']=true;
                    $return['msg']='NOP berhasil ditemukan.';
                    if($request->only('jenis')){
                        $jenis=$request->only('jenis');
                        if($jenis['jenis']!='10'){
                            if($return['data']['jns_transaksi_op']=='3'||in_array($return['data']['jns_bumi'],['4','5'])){
                                $return['status']=false;
                                $return['msg']='NOP tidak aktif, tidak bisa di ajukan.';
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $return['msg']=$e->getMessage();
            Log::info($return);
            $result=false;
        } 
        return response()->json($return);
    }
    public function nopBadan(Request $request)
    {
        $setdata=$request->only('nop');
        $return['status']=false;
        $return['msg']='NOP tidak ditemukan.';
        try{
            if($setdata){
                $nop=explode(".",$setdata['nop']);
                $blok_urut=explode("-",$nop[4]);
                $tglNow=Carbon::now();
                $tahun=$tglNow->year;
                $sql="select  dat_subjek_pajak.subjek_pajak_id nik_badan,
                            total_luas_bumi luas_bumi,
                            total_luas_bng luas_bng,
                            njop_bumi,
                            njop_bng ,
                            (select kd_kls_tanah
                        from kelas_tanah
                        where thn_awal_kls_tanah <=".$tahun." and thn_akhir_kls_tanah>=".$tahun."
                        and  nilai_min_tanah<= ( njop_bumi/NULLIF(total_luas_bumi,0) /1000) and nilai_max_tanah>=( njop_bumi/NULLIF(total_luas_bumi,0) /1000)
                        and lower(kd_kls_tanah) not like 'x%')
                        kd_kls_tanah,
                            case when total_luas_bng=0  then null else                   
                            (select kd_kls_bng
                        from kelas_bangunan
                        where thn_awal_kls_bng <=".$tahun." and thn_akhir_kls_bng>=".$tahun."
                        and  nilai_min_bng<= ( njop_bng/NULLIF(total_luas_bng,0) /1000) and nilai_max_bng>=( njop_bng/NULLIF(total_luas_bng,0) /1000)
                        and lower(kd_kls_bng) not like 'x%') end 
                            kd_kls_bng,
                            dat_subjek_pajak.nm_wp nama_badan,
                            '0000-0000' nomor_sbu_badan_badan,
                            '0000-0000' nomor_kemenhum_badan,
                            dat_subjek_pajak.jalan_wp alamat_wp,
                            dat_subjek_pajak.rt_wp,
                            dat_subjek_pajak.rw_wp,
                            dat_subjek_pajak.kelurahan_wp,
                            '' kecamatan_wp,
                            dat_subjek_pajak.telp_wp,
                            dat_subjek_pajak.kota_wp dati2_wp,
                            '' propinsi_wp,
                            dat_objek_pajak.jalan_op nop_alamat,
                            dat_objek_pajak.rt_op nop_rt,
                            dat_objek_pajak.rw_op nop_rw,
                            ref_kelurahan.nm_kelurahan nop_kelurahan,
                            ref_kecamatan.nm_kecamatan nop_kecamatan,
                            'KABUPATEN MALANG' nop_dati2,
                            'JAWA TIMUR' nop_propinsi,
                            dat_objek_pajak.jns_transaksi_op,
                            dob.jns_bumi
                from pbb.dat_objek_pajak dat_objek_pajak
                join pbb.dat_subjek_pajak dat_subjek_pajak
                            on dat_subjek_pajak.subjek_pajak_id=dat_objek_pajak.subjek_pajak_id
                LEFT JOIN dat_op_bumi dob 
                    ON dob.kd_propinsi=dat_objek_pajak.kd_propinsi
                    AND dob.kd_dati2=dat_objek_pajak.kd_dati2
                    AND dob.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                    AND dob.kd_kelurahan=dat_objek_pajak.kd_kelurahan
                    AND dob.kd_blok=dat_objek_pajak.kd_blok
                    AND dob.no_urut=dat_objek_pajak.no_urut
                    AND dob.kd_jns_op=dat_objek_pajak.kd_jns_op
                join pbb.ref_kecamatan ref_kecamatan
                            on ref_kecamatan.kd_propinsi=dat_objek_pajak.kd_propinsi
                            and ref_kecamatan.kd_dati2=dat_objek_pajak.kd_dati2
                            and ref_kecamatan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                        join pbb.ref_kelurahan ref_kelurahan
                            on ref_kelurahan.kd_propinsi=dat_objek_pajak.kd_propinsi
                            and ref_kelurahan.kd_dati2=dat_objek_pajak.kd_dati2
                            and ref_kelurahan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                            and ref_kelurahan.kd_kelurahan=dat_objek_pajak.kd_kelurahan
                            WHERE dat_objek_pajak.kd_propinsi='".$nop[0]."'
                        AND dat_objek_pajak.kd_dati2='".$nop[1]."'
                        AND dat_objek_pajak.kd_kecamatan='".$nop[2]."'
                        AND dat_objek_pajak.kd_kelurahan='".$nop[3]."'
                        AND dat_objek_pajak.kd_blok='".$blok_urut[0]."'
                        AND dat_objek_pajak.no_urut='".$blok_urut[1]."'
                        AND dat_objek_pajak.kd_jns_op='".$nop[5]."'";
                $result=DB::connection('oracle_satutujuh')->select(db::raw($sql));
                // dat_subjek_pajak.
                //     left outer join dat_objek_pajak
                //          on dat_objek_pajak.kd_propinsi=sp.kd_propinsi
                //         and dat_objek_pajak.kd_dati2=sp.kd_dati2
                //         and dat_objek_pajak.kd_kecamatan=sp.kd_kecamatan
                //         and dat_objek_pajak.kd_kelurahan=sp.kd_kelurahan
                //         and dat_objek_pajak.kd_blok=sp.kd_blok
                //         and dat_objek_pajak.no_urut=sp.no_urut
                //         and dat_objek_pajak.kd_jns_op=sp.kd_jns_op
                //     left outer join dat_subjek_pajak
                //         on dat_subjek_pajak.subjek_pajak_id=dat_objek_pajak.subjek_pajak_id
                if($result){
                    $return['data'] = (array)current($result);
                    $return['data']['dati2_wp']=str_replace('-','.',$return['data']['dati2_wp']);
                    $dati2=explode('.',$return['data']['dati2_wp']);
                    if(count($dati2)>=2){
                        $return['data']['propinsi_wp']=$dati2[count($dati2)-1];
                        $return['data']['dati2_wp']=$dati2[0];
                    }
                    $return['data']['kelurahan_wp']=str_replace('-','.',$return['data']['kelurahan_wp']);
                    $kelurahan_wp=explode('.',$return['data']['kelurahan_wp']);
                    if(count($kelurahan_wp)>=2){
                        $return['data']['kecamatan_wp']=$kelurahan_wp[count($kelurahan_wp)-1];
                        $return['data']['kelurahan_wp']=$kelurahan_wp[0];
                    }
                    $nop_alamat=explode('-',$return['data']['nop_alamat']);
                    $return['data']['blok_kav_no_op']='';
                    if(count($nop_alamat)>=2){
                        $return['data']['blok_kav_no_op']=$nop_alamat[count($nop_alamat)-1];
                        $return['data']['nop_alamat']=$nop_alamat[0];
                    }
                    $rt_wp=sprintf("%03s", str_replace(" ","",$return['data']['rt_wp']));
                    $rw_wp=sprintf("%02s", str_replace(" ","",$return['data']['rw_wp']));
                    $nop_rt=sprintf("%03s", str_replace(" ","",$return['data']['nop_rt']));
                    $nop_rw=sprintf("%02s", str_replace(" ","",$return['data']['nop_rw']));
                    $return['data']['rt_rw_wp']=$rt_wp.'/'.$rw_wp;
                    $return['data']['notelp_wp']=$return['data']['telp_wp'];
                    $return['data']['rt_rw_op']=$nop_rt.'/'.$nop_rw;
                    $return['data']['alamat_op']=$return['data']['nop_alamat'];
                    $return['data']['nik_badan']=substr($return['data']['nik_badan'],0,16);
                    // blok_kav_no_op
                    if(!$request->only('noTag')){
                        $return['tag']=[
                            'riwayat_pembayaran'=>LayananUpdate::riwayatPembayaran($setdata)
                        ];
                    }
                    $return['status']=true;
                    $return['msg']='NOP berhasil ditemukan.';
                    if($request->only('jenis')){
                        $jenis=$request->only('jenis');
                        if($jenis['jenis']!='10'){
                            if($return['data']['jns_transaksi_op']=='3'||in_array($return['data']['jns_bumi'],['4','5'])){
                                $return['status']=false;
                                $return['msg']='NOP tidak aktif, tidak bisa di ajukan.';
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $return['msg']=$e->getMessage();
            Log::info($return);
            $result=false;
        } 
        return response()->json($return);
    }
}
