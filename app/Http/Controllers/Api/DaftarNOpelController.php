<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DaftarNOpelController extends Controller
{
    //

    public function coreSql()
    {
        $sql = "(select distinct  nop_proses,a.nomor_layanan,jenis_layanan_nama 
                    from sim_pbb.layanan_objek a
                    join sim_pbb.layanan c on c.nomor_layanan=a.nomor_layanan
                    join tbl_spop b on a.nomor_formulir=b.no_formulir
                    where a.nomor_layanan not like 'ID%'
                    and jenis_layanan_nama!='Pembetulan (Ejaan/Data WP)') core";
        return $sql;
    }


    public function DaftarNopel(Request $request)
    {
        $jenis_layanan = $request->jenis_layanan ?? '';
        $tahun = $request->tahun ?? date('Y');

        if ($jenis_layanan == '') {
            return response()->json([
                'statusCode' => 404,
                'status' => 'error',
                'message' => 'Jenis layanan harus di isi'
            ], 404);
        }
        $table = $this->coreSql();
        $data = DB::table(DB::raw($table))
            ->where('jenis_layanan_nama', $jenis_layanan)
            ->whereraw("nomor_layanan like '$tahun%'")->orderby('nomor_layanan', 'desc')
            ->get();

        if ($data) {
            return response()->json([
                'statusCode' => 200,
                'status' => 'success',
                'message' => 'Data retrieved successfully',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'statusCode' => 404,
                'status' => 'error',
                'message' => 'Data not found'
            ], 404);
        }
    }

    public function JenisLayanan()
    {
        $table = $this->coreSql();
        $data = DB::table(DB::raw($table))->selectraw('distinct jenis_layanan_nama jenis_layanan')
            ->pluck('jenis_layanan')->toarray();

        if ($data) {
            return response()->json([
                'statusCode' => 200,
                'status' => 'success',
                'message' => 'Data retrieved successfully',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'statusCode' => 404,
                'status' => 'error',
                'message' => 'Data not found'
            ], 404);
        }
    }
}
