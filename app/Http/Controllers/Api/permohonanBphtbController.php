<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\PermohonanBphtb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use function PHPSTORM_META\map;

class permohonanBphtbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'nomor_layanan_bphtb' => 'required',
            'nomor_layanan_bphtb' => 'required|unique:permohonan_bphtbs,nomor_layanan_bphtb',
            'nama_pemohon' => 'required',
            'nomor_pemohon' => 'required',
            'kd_propinsi' => 'required',
            'kd_dati2' => 'required',
            'kd_kecamatan' => 'required',
            'kd_kelurahan' => 'required',
            'kd_blok' => 'required',
            'no_urut' => 'required',
            'kd_jns_op' => 'required',
            'alamat_objek' => 'required',
            'rt_objek' => 'required',
            'rw_objek' => 'required',
            'nm_kelurahan' => 'required',
            'nm_kecamatan' => 'required',
            'luas_bumi' => 'required',
            'luas_bng' => 'required',
            'penjual' => 'required',
            'jenis_perolehan' => 'required',
            'dokumen_lampiran' => 'required',
            'keterangan' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validasi gagal!',
                'errors' => $validator->errors()
            ], 422);
        }



        DB::beginTransaction();

        try {
            $data = $request->only([
                'nomor_layanan_bphtb',
                'nama_pemohon',
                'nomor_pemohon',
                'kd_propinsi',
                'kd_dati2',
                'kd_kecamatan',
                'kd_kelurahan',
                'kd_blok',
                'no_urut',
                'kd_jns_op',
                'alamat_objek',
                'rt_objek',
                'rw_objek',
                'nm_kelurahan',
                'nm_kecamatan',
                'luas_bumi',
                'luas_bng',
                'penjual',
                'jenis_perolehan',
                'keterangan'
            ]);

            $data['dokumen_lampiran'] = json_encode($request->dokumen_lampiran);

            $permohonan = PermohonanBphtb::create($data);

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil diterima',
                'data' => $permohonan
            ], 200);
        } catch (\Throwable $th) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Terjadi kesalahan saat menyimpan data',
                'error' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function returnHasil()
    {

        $base_url = config('services.sim_bphtb.base_url');
        $username = config('services.sim_bphtb.username');
        $password = config('services.sim_bphtb.password');
        // return [
        //     $base_url,
        //     $username,
        //     $password
        // ];

        $data = PermohonanBphtb::with(['VmonitorLayanan', 'Objek', 'Objek.Notaperhitungan', 'Objek.Notaperhitungan.DataBilling'])
            ->whereHas('VmonitorLayanan', function ($sql) {
                $sql->where('status', 'Telah di proses');
            })
            ->where('status', '1')
            ->get();


        // Optimasi dengan collection map()
        $return = $data->map(function ($item) {
            $response_pbb = collect($item->objek)->map(function ($lo) {
                $files = [];

                if (!empty(optional($lo->Notaperhitungan)->layanan_objek_id)) {
                    $files[] = [
                        'filename' => 'Nota Perhitungan',
                        'url' => url('penelitian/nota-perhitungan-pdf', acak($lo->Notaperhitungan->layanan_objek_id . '-1'))
                    ];

                    // if (!empty($lo->Notaperhitungan->DataBilling->tgl_bayar)) {
                    $files[] = [
                        'filename' => 'Surat Keputusan',
                        'url' => url('sk', acak($lo->Notaperhitungan->layanan_objek_id))
                    ];
                    // }
                }

                return $files;
            })->flatten(1); // Menggabungkan array dalam array

            return [
                'nomor_layanan' => $item->nomor_layanan_bphtb,
                'response_pbb' => json_encode($response_pbb)
            ];
        });


        foreach ($return as $payload) {

            /*             Log::info([
                $base_url . '/api/callbak-permohonan-pbb',
                $username,
                $password
            ]); */

            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])
                ->withBasicAuth($username, $password)
                ->post($base_url . '/api/callbak-permohonan-pbb', $payload);

            if ($response->successful()) {
                PermohonanBphtb::where('nomor_layanan_bphtb', $payload['nomor_layanan'])
                    ->update(['status' => '2']);
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => $return
        ], 200);

        // return response()->json($return);
    }
}
