<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Layanan_conf;
use App\Http\Controllers\Controller;
use App\pegawaiStruktural;
use App\Services\CreateSknjopService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SknjopApiController extends Controller
{
    //
    public function post(Request $request, CreateSknjopService $service)
    {
        $nop = $request->nop;
        $res = str_replace('.', '', $nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);

        $now = Carbon::now();
        $userid =Auth()->user()->id;
        $peg = pegawaiStruktural::where('kode', '02')->first();
        $nomor_layanan = Layanan_conf::nomor_layanan();
        $permohonan = $request->only(['nik_pemohon', 'nomor_hp', 'nama_pemohon', 'alamat_pemohon', 'kelurahan_pemohon', 'kecamatan_pemohon', 'dati2_pemohon', 'propinsi_pemohon']);
        $permohonan['nama_pemohon'] = html_entity_decode($request->nama_pemohon);
        $permohonan['nomor_layanan'] = $nomor_layanan;
        $permohonan['verifikasi_at'] = $now;
        $permohonan['verifikasi_by'] = $userid;

        $data = $request->only(['alamat_op', 'alamat_wp', 'luas_bumi', 'luas_bng', 'njop_bumi', 'njop_bng', 'njop_pbb']);

        $data['nama_wp'] = html_entity_decode($request->nama_wp);
        $data['kd_propinsi'] = $kd_propinsi;
        $data['kd_dati2'] = $kd_dati2;
        $data['kd_kecamatan'] = $kd_kecamatan;
        $data['kd_kelurahan'] = $kd_kelurahan;
        $data['kd_blok'] = $kd_blok;
        $data['no_urut'] = $no_urut;
        $data['kd_jns_op'] = $kd_jns_op;
        $data['nomer_sk'] = getNoSkNjop();
        $data['tanggal_sk'] = $now;
        $data['kd_status'] = '1';
        $data['verifikasi_at'] = $now;
        $data['verifikasi_by'] = $userid;
        // nama ttd
        $data['nama_kabid'] = $peg->nama;
        $data['nip_kabid'] = $peg->nip;

        $parameter = [
            'permohonan' => $permohonan,
            'data' => $data
        ];

        return response($parameter);

        $post = $service->handle($parameter);
        return response($post);
    }
}
