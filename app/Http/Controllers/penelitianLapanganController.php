<?php

namespace App\Http\Controllers;

use App\Helpers\gallade;
use App\Helpers\InformasiObjek;
// use App\Helpers\Layanan_conf;
use App\Helpers\Pajak;
// use App\KelompokObjek;
// use App\LokasiObjek;
use App\Models\Jenis_layanan;
use App\Models\KeepPenelitian;
use App\Models\Layanan;
use App\Models\Layanan_dokumen;
use App\Models\Layanan_objek;
use App\Models\Lspop;
use App\Models\Spop;
use App\PenelitianLapangan;
use App\penelitianTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Log;
use stdClass;

class penelitianLapanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // tab hasil
            if ($request->tab == 'hasil') {

                $data = DB::table(db::raw('tbl_spop a'))
                    ->select(db::raw('a.no_formulir,a.layanan_objek_id,jns_transaksi,b.nomor_layanan,nama_layanan,nop_proses,nm_wp,a.kelurahan_wp,a.kota_wp,(select count(1) from sppt_penelitian where nomor_formulir=b.nomor_formulir ) nota'))
                    ->join(db::raw('layanan_objek b'), db::raw('a.layanan_objek_id'), '=', db::raw('b.id'))
                    ->join(db::raw('penelitian_task c'), db::raw('c.layanan_objek_id'), '=', db::raw('b.id'))
                    ->join(db::raw('jenis_layanan d'), db::raw('d.id'), '=', db::raw('a.jenis_layanan_id'))
                    ->whereraw('penelitian=2')->orderBy('a.created_at', 'desc');
                return  Datatables::of($data)
                    ->filterColumn('nop', function ($query, $keyword) {
                        $keyword = str_replace('-', '', str_replace('.', '', $keyword));
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("a.nop_proses like '%$keyword%'"));
                    })
                    ->addColumn('pilih', function ($row) {
                        $nota = '';
                        if ($row->nota > 0) {
                            $nota = '<a class="dropdown-item cetak-tagihan"role="button"  data-id="' . $row->layanan_objek_id . '" tabindex="0">Nota Perhitungan</a>';
                        }

                        $btn = '<div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-info btn-sm btn-flat">Aksi</button>
                            <button type="button" class="btn btn-info btn-sm btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                            <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item detail-objek" role="button"  data-id="' . $row->layanan_objek_id . '" tabindex="0">Detail</a>
                                ' . $nota . '
                            </div>
                            </div>';

                        return $btn;
                    })
                    ->addColumn('nopproses', function ($row) {
                        return formatnop($row->nop_proses);
                    })
                    ->addColumn('noformulir', function ($row) {
                        return formatNomor($row->no_formulir);
                    })
                    ->addColumn('nomorlayanan', function ($row) {
                        return formatNomor($row->nomor_layanan);
                    })
                    ->rawColumns(['pilih', 'noformulir', 'nomorlayanan', 'nopproses'])->make(true);
            }

            // tab task
            if ($request->tab == 'task') {
                /* ->join(db::raw("(select b.layanan_objek_id
                    from surat_tugas a
                    join surat_tugas_objek b on a.id=b.surat_tugas_id
                    where a.kd_status='1') st"), 'st.layanan_objek_id', '=', 'layanan_objek.id') */
                $data = penelitianTask::penelitian(2)

                    ->select(db::raw("distinct layanan_objek.id,layanan_objek.nomor_layanan,layanan_objek.created_at,case when nama_wp is null then  nama_badan else nama_wp end nama_wp,
                        layanan_objek.kd_propinsi,layanan_objek.kd_dati2,layanan_objek.kd_kecamatan,layanan_objek.kd_kelurahan,layanan_objek.kd_blok,layanan_objek.no_urut,layanan_objek.kd_jns_op,layanan.jenis_layanan_nama,
                            nm_kecamatan,nm_kelurahan,replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end alamat_op ,case when jenis_layanan_id=6 and nop_gabung is null then 'Induk' when jenis_layanan_id=6 and nop_gabung is not  null then 'Pecahan' else null end keterangan"))
                    ->orderby('layanan_objek.nomor_layanan');
                return  Datatables::of($data)

                    ->filterColumn('nomor_layanan', function ($query, $keyword) {
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("layanan_objek.nomor_layanan     like '%$keyword%' or lower(layanan.jenis_layanan_nama) like '%$keyword%' "));
                    })
                    ->filterColumn('nama_wp', function ($query, $keyword) {
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%'"));
                    })
                    ->filterColumn('nama_nop', function ($query, $keyword) {
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%' or  (layanan_objek.kd_propinsi||'.'||layanan_objek.kd_dati2||'.'||layanan_objek.kd_kecamatan||'.'||layanan_objek.kd_kelurahan||'.'||layanan_objek.kd_blok||'-'||layanan_objek.no_urut||'.'||layanan_objek.kd_jns_op like '%$keyword%')"));
                    })
                    ->filterColumn('nop', function ($query, $keyword) {
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("layanan_objek.kd_propinsi||'.'||layanan_objek.kd_dati2||'.'||layanan_objek.kd_kecamatan||'.'||layanan_objek.kd_kelurahan||'.'||layanan_objek.kd_blok||'-'||layanan_objek.no_urut||'.'||layanan_objek.kd_jns_op like '%$keyword%'"));
                    })
                    ->filterColumn('alamat_op', function ($query, $keyword) {
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("(lower(replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end) like '%$keyword%' ) or ref_kecamatan.nm_kecamatan like '%$keyword%' or ref_kelurahan.nm_kelurahan like '%$keyword%' "));
                    })
                    ->addColumn('nop', function ($data) {
                        return $data->kd_propinsi . '.' . $data->kd_dati2 . '.' . $data->kd_kecamatan . '.' . $data->kd_kelurahan . '.' . $data->kd_blok . '-' . $data->no_urut . '.' . $data->kd_jns_op;
                    })
                    ->addColumn('pilih', function ($row) {
                        return '<a class="badge badge-primary" href="' . url('penelitian/lapangan/create') . '?id=' . $row->id . '&penelitian=2"> <i class="far fa-list-alt"></i> </a>';
                    })->rawColumns(['pilih', 'nop'])->make(true);
            }
        }

        // trash data keep
        $userId = Auth()->user()->id;
        KeepPenelitian::where('peneliti_by', $userId)->delete();
        db::table('booking_nop')->where('created_by', $userId)->delete();
        return view('penelitian.lapangan_todo');
    }


    public function listObjekPenelitian(Request $request)
    {
        $and = "";
        if ($request->id) {
            $and = " and  surat_tugas_id<>'" . $request->id . "'";
        }

        $data = penelitianTask::penelitian(2)->whereraw(DB::raw("layanan_objek_id not in (select layanan_objek_id 
        from surat_tugas_objek where deleted_at is null  $and ) "))
            ->select(db::raw("distinct layanan_objek.id,layanan_objek.nomor_layanan,layanan_objek.created_at,case when nama_wp is null then  nama_badan else nama_wp end nama_wp,
        layanan_objek.kd_propinsi,layanan_objek.kd_dati2,layanan_objek.kd_kecamatan,layanan_objek.kd_kelurahan,layanan_objek.kd_blok,layanan_objek.no_urut,layanan_objek.kd_jns_op,layanan.jenis_layanan_nama,
            nm_kecamatan,nm_kelurahan,replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end alamat_op ,case when jenis_layanan_id=6 and nop_gabung is null then 'Induk' when jenis_layanan_id=6 and nop_gabung is not  null then 'Pecahan' else null end keterangan"))
            ->orderbyraw(db::raw('layanan_objek.nomor_layanan asc,nop_gabung nulls last'));

        return  Datatables::of($data)
            ->filterColumn('nomor_layanan', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw(DB::raw("layanan_objek.nomor_layanan     like '%$keyword%' or lower(layanan.jenis_layanan_nama) like '%$keyword%' "));
            })
            ->filterColumn('nama_wp', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw(DB::raw("lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%'"));
            })
            ->filterColumn('nama_nop', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw(DB::raw("lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%' or  (layanan_objek.kd_propinsi||'.'||layanan_objek.kd_dati2||'.'||layanan_objek.kd_kecamatan||'.'||layanan_objek.kd_kelurahan||'.'||layanan_objek.kd_blok||'-'||layanan_objek.no_urut||'.'||layanan_objek.kd_jns_op like '%$keyword%')"));
            })
            ->filterColumn('nop', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw(DB::raw("layanan_objek.kd_propinsi||'.'||layanan_objek.kd_dati2||'.'||layanan_objek.kd_kecamatan||'.'||layanan_objek.kd_kelurahan||'.'||layanan_objek.kd_blok||'-'||layanan_objek.no_urut||'.'||layanan_objek.kd_jns_op like '%$keyword%'"));
            })
            ->filterColumn('alamat_op', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                $query->whereRaw(DB::raw("(lower(replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end) like '%$keyword%' ) or ref_kecamatan.nm_kecamatan like '%$keyword%' or ref_kelurahan.nm_kelurahan like '%$keyword%'"));
            })
            ->addColumn('nop', function ($data) {
                return $data->kd_propinsi . '.' . $data->kd_dati2 . '.' . $data->kd_kecamatan . '.' . $data->kd_kelurahan . '.' . $data->kd_blok . '-' . $data->no_urut . '.' . $data->kd_jns_op;
            })
            ->addColumn('pilih', function ($data) {
                return '
                    <input type="checkbox" id="checkall" class="pilihnop"
                    data-balloon="Pilih objek" data-balloon-pos="up"
                    data-alamat_op="' . str_replace(',', ' ', $data->alamat_op) . '"
                    data-id="' . $data->id . '"
                    data-nop="' . $data->kd_propinsi . $data->kd_dati2 . $data->kd_kecamatan . $data->kd_kelurahan . $data->kd_blok . $data->no_urut . $data->kd_jns_op . '"
                    data-nm_kelurahan="' . $data->nm_kelurahan . '"
                    data-nm_kecamatan="' . $data->nm_kecamatan . '"><br>
                    <a href="#" class="badge badge-warning pelimpahan" data-href="' . url('penelitian/lapangan-switch', $data->id) . '"><i class="far fa-share-square"></i> </a>
                    ';
            })->rawColumns(['pilih', 'nop'])->make(true);
    }

    public function searchpenelitian(Request $request)
    {
        $select = [
            'penelitian_lapangan.id',
            'penelitian_lapangan.nik_wp',
            'penelitian_lapangan.nama_wp',
            'penelitian_lapangan.kd_propinsi',
            'penelitian_lapangan.kd_dati2',
            'penelitian_lapangan.kd_kecamatan',
            'penelitian_lapangan.kd_kelurahan',
            'penelitian_lapangan.kd_blok',
            'penelitian_lapangan.no_urut',
            'penelitian_lapangan.kd_jns_op',
            'penelitian_lapangan.created_at',
            'penelitian_lapangan.pemutakhiran_at',
            'penelitian_lapangan.layanan_objek_id',
            db::raw('jenis_layanan.nama_layanan jenis_layanan_nama'),
            'penelitian_lapangan.nomor_layanan',
            'penelitian_lapangan.jenis_layanan_id',
        ];
        $dataSet = PenelitianLapangan::select($select)
            ->join('jenis_layanan', 'jenis_layanan.id', '=', 'penelitian_lapangan.jenis_layanan_id')
            ->orderBy('penelitian_lapangan.created_at', 'desc')
            ->where('penelitian_lapangan.jns_transaksi', '2')
            ->OrWhere('penelitian_lapangan.jns_transaksi', '0')
            ->OrWhereRaw(db::raw("(penelitian_lapangan.jns_transaksi = '1' and penelitian_lapangan.jenis_layanan_id='1')"))
            ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
            if ($data->pemutakhiran_at) {
                return '';
            }
            $btn = gallade::anchorInfo(
                url('penelitian/lapangan-detail', $data->id),
                "<i class='fas fa-binoculars text-info'></i>",
                'title="Edit hasil penelitian" data-pdf="true"',
                '',
                'default btn-xs'
            );
            $btnPemutakhiran = gallade::anchorInfo(
                url('pemutakhiran/spop/create') . "?penelitian=2&id=" . $data->id,
                "<i class='text-success fas fa-file-signature'></i>",
                'title="Pemutakhiran" data-pdf="true"',
                '',
                'default btn-xs'
            );
            return '<div class="btn-group">' . $btnPemutakhiran . $btn . "</div>";
        })
            ->addcolumn('raw_tag', function ($data) {
                $listObjek = ['1' => 'Pribadi', '2' => 'Badan', '3' => 'Kolektif'];
                $jenisObjek = '--';
                if (in_array($data->jenis_objek, array_keys($listObjek))) {
                    $jenisObjek = $listObjek[$data->jenis_objek];
                }
                return $data->jenis_layanan_nama . ' - <i class="text-info">' . $jenisObjek . '</i>';
            })
            ->addcolumn('action', function ($data) {
                $keterangan = "Non Ajuan";
                if ($data->nomor_layanan) {
                    $keterangan = "Layanan";
                }
                return $data->jenis_layanan_nama . ' - <i class="text-info">' . $keterangan . '</i>';
            })
            ->addcolumn('kd_status', function ($data) {
                $status = [
                    '0' => "<span class='badge badge-warning '>Proses</span>",
                    '1' => "<span class='badge badge-success '>Selesai</span>",
                ];
                return $status['0'];
            })
            ->addcolumn('nop', function ($data) {
                return $data->kd_propinsi . "." .
                    $data->kd_dati2 . "." .
                    $data->kd_kecamatan . "." .
                    $data->kd_kelurahan . "." .
                    $data->kd_blok . "-" .
                    $data->no_urut . "." .
                    $data->kd_jns_op;
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at;
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function search(Request $request)
    {
        $select = [
            'penelitian_task.nomor_layanan',
            'penelitian_task.jenis_layanan_nama',
            'penelitian_task.created_at',
            'penelitian_task.jenis_objek',
            'penelitian_task.kd_status'
        ];
        $dataSet = penelitianTask::select($select)
            ->where(['penelitian_task.penelitian' => '2'])
            ->orderBy('penelitian_task.created_at', 'desc')
            ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
            // if($data->jenis_objek=='1'){
            //     $btn="<a href='".url('penelitian/lapangan-kolektif', $data->nomor_layanan)."' target='_BLANK'><i class='text-success fas fa-file-signature'></i></a>";
            // }
            $btn = gallade::anchorInfo(
                url('penelitian/lapangan-form', $data->nomor_layanan),
                "<i class='fas fa-binoculars text-info'></i>",
                'title="Detail Ajuan" target="_BLANK"',
                '',
                'default btn-xs'
            );

            return $btn;
        })
            ->addcolumn('raw_tag', function ($data) {
                $listObjek = ['1' => 'Pribadi', '2' => 'Badan', '3' => 'Kolektif'];
                $jenisObjek = '--';
                if (in_array($data->jenis_objek, array_keys($listObjek))) {
                    $jenisObjek = $listObjek[$data->jenis_objek];
                }
                return $data->jenis_layanan_nama . ' - <i class="text-info">' . $jenisObjek . '</i>';
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function formLayanan(Request $request, $id = false)
    {
        if ($id) {
            $alllayanan = Layanan::select(
                db::raw('layanan.*'),
                db::raw('layanan_objek.*'),
                db::raw('layanan_objek.id layanan_objek_id'),
                db::raw('jenis_layanan.nama_layanan'),
                db::raw("layanan_objek.kd_propinsi||'.'||layanan_objek.kd_dati2||'.'||layanan_objek.kd_kecamatan||'.'||layanan_objek.kd_kelurahan||'.'||layanan_objek.kd_blok||'-'||layanan_objek.no_urut||'.'||layanan_objek.kd_jns_op nop"),
                'ref_kelurahan.nm_kelurahan',
                'ref_kecamatan.nm_kecamatan',
                db::raw("'KABUPATEN MALANG' kd_dati2"),
                db::raw("'JAWA TIMUR' kd_propinsi")
            )
                ->join('layanan_objek', 'layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan')
                ->join('jenis_layanan', 'jenis_layanan.id', '=', 'layanan.jenis_layanan_id')
                ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                    $join->On('layanan_objek.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
                })
                ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                    $join->On('layanan_objek.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                        ->On('layanan_objek.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
                })
                ->orderBy('nop_gabung', 'DESC')
                ->where(['layanan.nomor_layanan' => $id])->get();
            if (!$alllayanan->count()) {
                return redirect('penelitian/lapangan');
            }
            $layanan = $alllayanan->first();
            $setDokumen = [];
            $layanan_dokumen = Layanan_dokumen::where(['nomor_layanan' => $id])->get();
            if ($layanan_dokumen->count()) {
                $getDokumen = $layanan_dokumen->toArray();
                foreach ($getDokumen as $item) {
                    $setDokumen[] = [
                        '',
                        $item['filename'],
                        $item['nama_dokumen'],
                        $item['keterangan'],
                        url('laporan_input/berkas?nomor_layanan=' . $item['nomor_layanan'] . '&id=' . $item['id'])
                    ];
                }
            }
            // dd($setDokumen);
            // $kelompokObjek=[];
            // $lokasiObjek=[];
            // if($layanan->jenis_layanan_id=='1'){
            //     $kelompokObjek=KelompokObjek::all();
            //     $lokasiObjek=LokasiObjek::all();
            // }
            $data = [
                'title' => 'Penelitian lapangan data permohonan',
                'action' => url('penelitian/lapangan_store'),
                'back' => url('penelitian/lapangan'),
                'page' => gallade::clean($layanan->nama_layanan),
                'method' => 'post',
                'layanan' => $layanan,
                'ceknop' => false,
                'dokumen' => $setDokumen,
                // 'kelompokObjek'=>$kelompokObjek,
                // 'lokasiObjek'=>$lokasiObjek
            ];
            // dd($layanan->toArray());
            if ($layanan->jenis_layanan_id == '7') {
                $nopGabung = [];
                $no = 1;
                foreach ($alllayanan->toArray() as $item) {
                    if ($item['nop_gabung'] == '1') {
                        $nopGabung[] = [
                            'no' => $no,
                            'objek_id' => $item['id'],
                            'nop' => $item['nop'],
                            'luas_bumi' => $item['luas_bumi'],
                            'luas_bng' => $item['luas_bng'],
                            'alamat_op' => $item['alamat_op'],
                            'rtrw' => $item['rt_op'] . '/' . $item['rw_op'],
                            'kelurahan' => $item['nm_kelurahan'],
                            'kecamatan' => $item['nm_kecamatan']
                        ];
                        $no++;
                    }
                }
                $data['nopgabung'] = $nopGabung;
            }
            if ($layanan->jenis_layanan_id == '6') {
                $nopPecah = [];
                $no = 1;
                $getNop = explode('.', $layanan['nop']);
                foreach ($alllayanan->toArray() as $item) {
                    if ($item['nop_gabung'] == '1') {
                        $nopPecah[] = [
                            'no' => $no,
                            'nop_default' => $getNop[0] . '.' . $getNop[1] . '.' . $getNop[2] . '.' . $getNop[3],
                            'luas_bumi' => $item['luas_bumi'],
                            'luas_bng' => $item['luas_bng'],
                            'objek_id' => $item['id'],
                            'nik_wp' => $item['nik_wp'],
                            'nama_wp' => $item['nama_wp'],
                            'alamat_wp' => $item['alamat_wp'],
                            'kelurahan_wp' => $item['kelurahan_wp'],
                            'kecamatan_wp' => $item['kecamatan_wp'],
                            'dati2_wp' => $item['dati2_wp'],
                            'propinsi_wp' => $item['propinsi_wp'],
                        ];
                        $no++;
                    }
                }
                // dd($nopPecah);
                $data['noppecah'] = $nopPecah;
            }
            return view('penelitian.form_ver_lap', compact('data'));
        } else {
            $layanan = new stdClass;
            $layanan->nop = '';
            $layanan->nik_wp = '';
            $layanan->nama_wp = '';
            $layanan->alamat_wp = '';
            $layanan->nomor_telepon = '';
            $layanan->rt_wp = '';
            $layanan->rw_wp = '';
            $layanan->kelurahan_wp = '';
            $layanan->kecamatan_wp = '';
            $layanan->dati2_wp = '';
            $layanan->propinsi_wp = '';
            $layanan->alamat_op = '';
            $layanan->rt_op = '';
            $layanan->rw_op = '';
            $layanan->nm_kelurahan = '';
            $layanan->nm_kecamatan = '';
            $layanan->kd_dati2 = '';
            $layanan->kd_propinsi = '';
            $layanan->luas_bumi = '';
            $layanan->njop_bumi = '';
            $layanan->luas_bng = '';
            $layanan->njop_bng = '';
            $layanan->uraian = '';
            $data = [
                'layanan' => $layanan,
                'back' => url('penelitian/kantor'),
                'dokumen' => false,
                'ceknop' => true,
            ];
            $getjenisLayanan = Jenis_layanan::select(['nama_layanan', 'id'])
                ->whereIn('id', [1, 3, 7, 2, 9, 6])
                ->orderBy('nama_layanan')
                ->get();
            $jenisLayanan = [];
            if ($getjenisLayanan->count()) {
                foreach ($getjenisLayanan as $item) {
                    $jenisLayanan[] = ['nama' => $item->nama_layanan, 'tag' => gallade::clean($item->nama_layanan)];
                }
            }
            return view('penelitian.form_layanan_alltab', compact('jenisLayanan', 'data'));
        }
    }
    // public function formLayanan($id)
    // {
    //     $objek = Layanan_objek::with('layanan')->findorfail($id);
    //     $layanan = $objek->layanan;
    //     $alamat_op = DB::connection('oracle_satutujuh')->select(DB::raw("select jalan_op||' '||blok_kav_no_op alamat_op,rt_op,rw_op, nm_kecamatan,nm_kelurahan
    //     from dat_objek_pajak
    //     join ref_kecamatan on ref_kecamatan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
    //     join ref_kelurahan on ref_kelurahan.kd_kecamatan=dat_objek_pajak.kd_kecamatan and ref_kelurahan.kd_kelurahan=DAT_OBJEK_PAJAK.kd_kelurahan
    //     where dat_objek_pajak.kd_kecamatan='" . $objek->kd_kecamatan . "' and dat_objek_pajak.kd_kelurahan='" . $objek->kd_kelurahan . "'
    //     and DAT_OBJEK_PAJAK.KD_BLOK='" . $objek->kd_blok . "' and dat_objek_pajak.no_urut='" . $objek->no_urut . "' and dat_objek_pajak.kd_jns_op='" . $objek->kd_jns_op . "'"))[0];
    //     $data = [
    //         'title' => 'Penelitian Lapangan',
    //         'action' => url('penelitian/lapangan'),
    //         'method' => 'post',
    //         'layanan' => $layanan,
    //         'objek' => $objek,
    //         'alamat_op' => $alamat_op
    //     ];
    //     dd($objek);
    //     return view('penelitian.form_verlap', compact('data'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $userId = Auth()->user()->id;
        $id = $request->id ?? null;

        $penelitian = Layanan_objek::with('layanan')->where('id', $id)->wherenull('nomor_formulir')->first();
        $jenis_objek = $penelitian->layanan->jenis_objek;
        $nomor_layanan = $penelitian->nomor_layanan;

        if ($penelitian && in_array($request->penelitian, [1, 2])) {
            $cek = KeepPenelitian::where('layanan_objek_id', $id)->whereraw("trunc(24*mod(sysdate - start_at,1))<2")->first();
            if (count((array)$cek) > 0) {
                if ($cek->peneliti_by <> $userId) {
                    $pesan = 'Berkas sedang di teliti oleh :<br> <strong>' . $cek->user->nama . '</strong>';
                    return view('penelitian.keep', compact('pesan'));
                }
            } else {
                $keep = [
                    'layanan_objek_id' => $id,
                    'peneliti_by' => $userId,
                    'start_at' => Carbon::now()
                ];
                KeepPenelitian::create($keep);
            }

            $penelitian = Pajak::loadPenelitian($penelitian);
            // dd($penelitian);
            $data = [
                'method' => 'POST',
                'action' => url('penelitian/lapangan'),
                'jns_penelitian' => $request->penelitian,
                'penelitian' => (object)$penelitian,
                'nop_pembatalan' => $penelitian['nop_pembatalan'],
                'nop_pecah' => $penelitian['nop_pecah'],
                'lo' => $penelitian['letak_op'],
                'refAjuan' => $penelitian['refAjuan'],
                'title' => 'Form Penelitian Khusus',
                'urlback' => url('penelitian/lapangan'),
                'urlpelimpahan' => url('penelitian/lapangan-switch', $id),
                'induk' => $penelitian['nopinduk'],
                'berkas' => $penelitian['berkas']
            ];
            $jenisDokumen = $penelitian['jenisDokumen'];
            $riwayatpembayaran = $penelitian['riwayatpembayaran'];
            $statuspecahan = $penelitian['statuspecahan'];
            $nop = $penelitian['nop'];
            $is_admin = Auth()->user()->hasAnyRole('Super User', 'Administrator');

            $dokbphtb = [];
            if ($jenis_objek == 5) {

                $dokbphtb = Layanan_dokumen::where('nomor_layanan', $nomor_layanan)->pluck('url', 'nama_dokumen')
                    ->toArray();
            }

            return view('spop.form', compact('data', 'nop', 'dokbphtb', 'riwayatpembayaran', 'statuspecahan', 'jenisDokumen', 'is_admin'));
        }
        abort(404);

        /*   $userId = Auth()->user()->id;
        $id = $request->id ?? null;
        $penelitian = Layanan_objek::with('layanan')->where('id', $id)->wherenull('nomor_formulir')->first();
        if ($penelitian && in_array($request->penelitian, [1, 2])) {
            $cek = KeepPenelitian::where('layanan_objek_id', $id)->whereraw("trunc(24*mod(sysdate - start_at,1))<2")->first();
            if (count((array)$cek) > 0) {
                if ($cek->peneliti_by <> $userId) {
                    $pesan = 'Berkas sedang di teliti oleh :<br> <strong>' . $cek->user->nama . '</strong>';
                    return view('penelitian.keep', compact('pesan'));
                }
            } else {
                $keep = [
                    'layanan_objek_id' => $id,
                    'peneliti_by' => $userId,
                    'start_at' => Carbon::now()
                ];
                KeepPenelitian::create($keep);
            }


            $letak_op = [];
            $nop = $penelitian->kd_propinsi . $penelitian->kd_dati2 . $penelitian->kd_kecamatan . $penelitian->kd_kelurahan . $penelitian->kd_blok . $penelitian->no_urut . $penelitian->kd_jns_op;
            $nopinduk = $penelitian->kd_propinsi . $penelitian->kd_dati2 . $penelitian->kd_kecamatan . $penelitian->kd_kelurahan . $penelitian->kd_blok . $penelitian->no_urut . $penelitian->kd_jns_op;
            if ($penelitian->layanan->jenis_layanan_id <> '1') {
                if ($penelitian->nop_gabung <> '') {
                    // $opinduk = Layanan_objek::with('jpb', 'lokasi','pecah')->where('id', $penelitian->nop_gabung)->first();
                    $opinduk = DB::table('layanan_objek')->select(DB::raw("id,kd_propinsi,
                    kd_dati2,
                    kd_kecamatan,
                    kd_kelurahan,
                    kd_blok,
                    no_urut,
                    kd_jns_op"))->where('id', $penelitian->nop_gabung)->first();
                    $kd_propinsi = $opinduk->kd_propinsi;
                    $kd_dati2 = $opinduk->kd_dati2;
                    $kd_kecamatan = $opinduk->kd_kecamatan;
                    $kd_kelurahan = $opinduk->kd_kelurahan;
                    $kd_blok = $opinduk->kd_blok;
                    $no_urut = $opinduk->no_urut;
                    $kd_jns_op = $opinduk->kd_jns_op;

                    $nop = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok;
                    $nopinduk = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
                } else {

                    $kd_propinsi = $penelitian->kd_propinsi;
                    $kd_dati2 = $penelitian->kd_dati2;
                    $kd_kecamatan = $penelitian->kd_kecamatan;
                    $kd_kelurahan = $penelitian->kd_kelurahan;
                    $kd_blok = $penelitian->kd_blok;
                    $no_urut = $penelitian->no_urut;
                    $kd_jns_op = $penelitian->kd_jns_op;

                    if ($penelitian->layanan->jenis_layanan_id == 7) {
                        $nop = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok;
                    }
                }

                $letak_op  = DB::connection('oracle_satutujuh')->table(db::raw("dat_objek_pajak dop"))
                    ->select(db::raw("jalan_op,blok_kav_no_op,rw_op,rt_op ,(
                            select kd_znt
                            from pbb.dat_op_bumi where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan
                            and kd_blok=dop.kd_blok and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
                            ) kd_znt"))->whereraw("  kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'
                            and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' 
                            and kd_jns_op='" . $kd_jns_op . "'")->first();
            }


            // mutasi gabung
            $nop_pembatalan = [];
            if ($penelitian->layanan->jenis_layanan_id == 7) {
                // nop pembatalan karena penggabungan
                $nop_pembatalan = Layanan_objek::whereraw("nomor_layanan ='" . $penelitian->nomor_layanan . "'  and nop_gabung='$id' ")->get()->toarray();
            }


            $nop_pecah = [];
            if ($penelitian->layanan->jenis_layanan_id <> 7) {
                $ida = $id;
                $idi = $opinduk->id ?? $id;
                $nop_pecah = DB::table('layanan_objek')
                    ->leftjoin('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                    ->leftjoin('kelompok_objek', 'kelompok_objek.id', '=', 'layanan_objek.kelompok_objek_id')
                    ->leftjoin('lokasi_objek', 'lokasi_objek.id', '=', 'layanan_objek.lokasi_objek_id')
                    ->select(db::raw('layanan_objek.nomor_layanan,nama_wp,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,nama_kelompok,upper(nama_lokasi) nama_lokasi, CASE
                                        WHEN nop_gabung IS NULL 
                                        THEN  case when jenis_layanan_id=6 then sisa_pecah_total_gabung else luas_bumi end
                                        ELSE luas_bumi
                                    END luas_bumi,luas_bng,case when nop_gabung is null  then  luas_bumi else (select luas_bumi from layanan_objek tmp where id=layanan_objek.nop_gabung)  end luas_induk,nop_gabung'));

                // mutasi pecah 
                $statuspecahan = [];
            
                if ($penelitian->layanan->jenis_layanan_id == 6) {
                    $nop_pecah = $nop_pecah->whereraw("layanan_objek.id='$ida'  or (nop_gabung='$idi' or layanan_objek.id='$idi')")->get();
                    if ($penelitian->no_gabung == '') {
                        $statuspecahan = DB::select(DB::raw("select sum(jumlah) jumlah,sum(teliti) sudah
                                from (
                                select count(1)  jumlah,case when pemutakhiran_at is not null then 1  else 0 end teliti
                                from layanan_objek
                                where nop_gabung='$ida'
                                group by pemutakhiran_at
                                )"));
                    }
                } else {
                    $nop_pecah = $nop_pecah->whereraw("layanan_objek.id='$ida'")->get();
                }
            }

            

            $variable = [
                'kd_propinsi' => substr($nopinduk, 0, 2),
                'kd_dati2' => substr($nopinduk, 2, 2),
                'kd_kecamatan' => substr($nopinduk, 4, 3),
                'kd_kelurahan' => substr($nopinduk, 7, 3),
                'kd_blok' => substr($nopinduk, 10, 3),
                'no_urut' => substr($nopinduk, 13, 4),
                'kd_jns_op' => substr($nopinduk, 17, 1),
            ];
            $mulaitahun = date('Y') - 5;
            $riwayatpembayaran = InformasiObjek::riwayatPembayaran($variable)->whereraw("sppt.thn_pajak_sppt > $mulaitahun")->orderby('sppt.thn_pajak_sppt')->get();
            // dd($pembayaran);

            $refAjuan = DB::table(db::raw("ref_formulir"))
                ->select(db::raw('ref_formulir.id id_formulir,jenis_layanan_id,nama_layanan'))
                ->join(db::raw('ref_formulir_layanan'), 'ref_formulir_layanan.ref_formulir_id', '=', 'ref_formulir.id')
                ->join('jenis_layanan', 'jenis_layanan.id', '=', 'ref_formulir_layanan.jenis_layanan_id')
                ->where('penelitian', $request->penelitian)
                ->orderBy('jenis_layanan_id')->get();

            if ($penelitian->layanan->jenis_layanan_id == '1') {
                $nop = substr($nop, 0, 10);
            }

            $berkas = Layanan_dokumen::where('nomor_layanan', $penelitian->nomor_layanan)->where('keyid', $id)->select(db::raw('nama_dokumen,keterangan'))->get()->toarray();
            // dd($berkas);
            $data = [
                'method' => 'POST',
                'action' => url('penelitian/lapangan'),
                'jns_penelitian' => $request->penelitian,
                'penelitian' => $penelitian,
                'nop_pembatalan' => $nop_pembatalan,
                'nop_pecah' => $nop_pecah,
                'lo' => $letak_op,
                'refAjuan' => $refAjuan,
                'title' => 'Form Penlitian Khusus',
                'urlback' => url('penelitian/lapangan'),
                'urlpelimpahan' => url('penelitian/lapangan-switch', $id),
                'induk' => $nopinduk,
                'berkas' => $berkas
            ];
            // dd($riwayatpembayaran);
            return view('spop.form', compact('data', 'nop', 'riwayatpembayaran', 'statuspecahan'));
        }
        abort(404);
        */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request->all();
        if ($request->jns_transaksi != '3') {
            // pmeutakhiran

            // $flash = Pajak::prosesPenelitianDua($request);
            if ($request->jenis_pelayanan == '5') {
                $param['layanan_objek_id'] = $request->lhp_id;
                $param['jenis_layanan'] = $request->jenis_layanan;
                $param['nop_proses'] = $request->nop_proses;
                $param['jns_pengurangan'] = $request->jns_pengurangan;
                $param['nilai_pengurangan'] = $request->nilai_pengurangan;
                $param['keterangan'] = $request->ket_pengurangan;
                $flash['msg'] = Pajak::prosesPotongan($param);
            } else {
                // pmeutakhiran
                // dd($request->all());
                $flash = Pajak::prosesPenelitianDua($request);
            }
        } else {
            // pembatalan objek
            $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);


            DB::beginTransaction();
            $msg = "";
            try {
                //code...
                $dataspop = (array) DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP, STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI FROM DAT_OBJEK_PAJAK A
                JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                WHERE a.kd_kecamatan='$kd_kecamatan'
                AND a.kd_kelurahan='$kd_kelurahan'
                AND a.kd_blok='$kd_blok'
                AND a.no_urut='$no_urut'
                AND a.kd_jns_op='$kd_jns_op' "))[0];

                $request->merge($dataspop);
                // dd($request);

                $spop = Pajak::pendataanSpop($request);
                Pajak::pendataanObjekPajak($request, $spop);
                $wherenop = [
                    ['kd_propinsi', '=', $kd_propinsi],
                    ['kd_dati2', '=', $kd_dati2],
                    ['kd_kecamatan', '=', $kd_kecamatan],
                    ['kd_kelurahan', '=', $kd_kelurahan],
                    ['kd_blok', '=', $kd_blok],
                    ['no_urut', '=', $no_urut],
                    ['kd_jns_op', '=', $kd_jns_op]
                ];
                // proses rekam unflag_history
                $ceklunas = DB::connection('oracle_satutujuh')->table(DB::raw('sppt'))
                    ->selectraw("kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,status_pembayaran_sppt")
                    ->where($wherenop)
                    ->where('thn_pajak_sppt', date('Y'))->first();

                if ($ceklunas->status_pembayaran_sppt == 0) {
                    // blokir tahun berjalan
                    DB::connection('oracle_satutujuh')->table(DB::raw('sppt'))
                        ->where($wherenop)
                        ->where('thn_pajak_sppt', date('Y'))->update(['status_pembayaran_sppt' => 3]);

                    DB::connection('oracle_satutujuh')->table('unflag_history')
                        ->insert([
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'thn_pajak_sppt' => date('Y'),
                            'unflag_time' => Carbon::now(),
                            'unflag_desc' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek',
                            'unflag_mode' => 0
                        ]);
                }

                DB::connection('oracle_satutujuh')->table('unflag_history')
                    ->insert([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => date('Y'),
                        'unflag_time' => Carbon::now(),
                        'unflag_desc' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek',
                        'unflag_mode' => 0
                    ]);

                $penelitian = Layanan_objek::with('layanan')->findorfail($request->lhp_id);
                $penelitian->update([
                    'nomor_formulir' => $spop['no_formulir'],
                    'pemutakhiran_at' => Carbon::now(),
                    'pemutakhiran_by' => auth()->user()->id,
                    'keterangan_pembatalan' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek'
                ]);

                DB::commit();
                $msg = "Proses pembtalan objek telah di proses";
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                Log::error($th);
                $msg = "Proses pembtalan objek gagal di proses";
            }
            $flash['msg'] = $msg;
        }
        // realese kepp
        KeepPenelitian::where('layanan_objek_id', $request->lhp_id)->delete();

        if ($flash['msg'] <> '') {
            $msg = $flash['msg'];
        } else {
            $msg = "Telah di proses";
        }

        return redirect(url('penelitian/lapangan-hasil-penelitian', $request->lhp_id))->with(['success' => $msg]);
    }

    public function cekObjek($nopel)
    {
        $ajuan = DB::table('layanan_objek')->where('nomor_layanan', $nopel)->get()->count();
        $penelitian = DB::table('penelitian_lapangan')->where('nomor_layanan', $nopel)->get()->count();
        if ($ajuan <= $penelitian) {
            return 1;
        } else {
            return 0;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objek = Spop::with([
            'jenisLayanan',
            'user',
            'layananObjek'
        ])->where('layanan_objek_id', $id)->orderby('created_at', 'desc')->first();
        $bng = Lspop::with('user')->where('layanan_objek_id', $id)->get();

        $pecahobjek = [];
        $pecahbng = [];
        $berkasajuan = Layanan_dokumen::where('nomor_layanan', $objek->layananObjek->nomor_layanan)->pluck('nama_dokumen', 'keterangan')->toarray();
        if ($objek->layananObjek->layanan->jenis_layanan_id == 6 && $objek->layananObjek->nop_gabung == '') {
            // $pid = $cek->nomor_layanan;
            $pecahobjek = Spop::with([
                'jenisLayanan',
                'user'
            ])->join('layanan_objek', 'layanan_objek.id', '=', 'tbl_spop.layanan_objek_id')
                ->select(db::raw("tbl_spop.*"))
                ->where('nomor_layanan',  $objek->layananObjek->nomor_layanan)->wherenotnull('nop_gabung')->get();
            $pecahbng = Lspop::with('user')->join('layanan_objek', 'layanan_objek.id', '=', 'tbl_lspop.layanan_objek_id')
                ->select(db::raw("tbl_lspop.*"))
                ->where('nomor_layanan',  $objek->layananObjek->nomor_layanan)->wherenotnull('nop_gabung')
                ->get();
        }
        $lhp = Lhp::getHasilNjop($id);

        return view('penelitian._show', compact('objek', 'bng', 'pecahobjek', 'pecahbng', 'berkasajuan', 'lhp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());

        // $data['nomor_layanan'] = $request->nomor_layanan;
        // $data['layanan_objek_id'] = $request->layanan_objek_id;
        // $data['nik_wp'] = $request->nik_wp;
        // $data['nama_wp'] = $request->nama_wp;
        // $data['telp_wp'] = $request->telp_wp ?? '-';
        // $data['alamat_wp'] = $request->alamat_wp;
        // $data['rt_wp'] = $request->rt_wp;
        // $data['rw_wp'] = $request->rw_wp;
        // $data['kelurahan_wp'] = $request->kelurahan_wp;
        // $data['kecamatan_wp'] = $request->kecamatan_wp;
        // $data['kode_pos_wp'] = $request->kode_pos_wp;
        // $data['kabupaten_wp'] = $request->kabupaten_wp . "." . $request->propinsi_wp;
        // $data['uraian'] = $request->uraian;
        // $data['jenis_layanan_id'] = $request->jenis_layanan_id;
        // if (in_array($request->jenis_layanan_id, ['6', '7'])) {
        //     $nop = explode(".", $request->nop_induk);
        //     $blok_urut = explode("-", $nop[4]);
        //     $data['kd_propinsi'] = $nop[0];
        //     $data['kd_dati2'] = $nop[1];
        //     $data['kd_kecamatan'] = $nop[2];
        //     $data['kd_kelurahan'] = $nop[3];
        //     $data['kd_blok'] = $blok_urut[0];
        //     $data['no_urut'] = $blok_urut[1];
        //     $data['kd_jns_op'] = $nop[5];
        //     $data['alamat_op'] = $request->alamat_op_induk;
        //     $data['rt_op'] = $request->rt_op_induk;
        //     $data['rw_op'] = $request->rw_op_induk;
        //     $data['kelurahan_op'] = $request->kelurahan_op_induk;
        //     $data['kecamatan_op'] = $request->kecamatan_op_induk;
        //     $data['kabupaten_op'] = $request->kabupaten_op_induk;
        //     $data['luas_tanah'] = $request->luas_bumi_induk;
        //     $data['luas_bangunan'] = $request->luas_bng_induk;
        //     $data['jns_transaksi'] = '2';
        // } else {
        //     $nop = explode(".", $request->nop);
        //     $blok_urut = explode("-", $nop[4]);
        //     $data['kd_propinsi'] = $nop[0];
        //     $data['kd_dati2'] = $nop[1];
        //     $data['kd_kecamatan'] = $nop[2];
        //     $data['kd_kelurahan'] = $nop[3];
        //     $data['kd_blok'] = $blok_urut[0];
        //     $data['no_urut'] = $blok_urut[1];
        //     $data['kd_jns_op'] = $nop[5];
        //     $data['alamat_op'] = $request->alamat_op;
        //     $data['rt_op'] = $request->rt_op;
        //     $data['rw_op'] = $request->rw_op;
        //     $data['kelurahan_op'] = $request->kelurahan_op;
        //     $data['kecamatan_op'] = $request->kecamatan_op;
        //     $data['kabupaten_op'] = $request->kabupaten_op;
        //     $data['luas_tanah'] = $request->luas_bumi;
        //     $data['luas_bangunan'] = $request->luas_bng;
        // }
        // DB::beginTransaction();
        // try {
        //     //code...
        //     $penelitian = PenelitianLapangan::where(['id' =>  $request->id])->update($data);
        //     if (in_array($request->jenis_layanan_id, ['6'])) {
        //         foreach ($request->nop_pecah as $item) {
        //             $nopD = explode(".", $item['nop_baru']);
        //             $blok_urutD = explode("-", $nopD[4]);
        //             $dataSet = [
        //                 'kd_propinsi' => $nopD[0],
        //                 'kd_dati2' => $nopD[1],
        //                 'kd_kecamatan' => $nopD[2],
        //                 'kd_kelurahan' => $nopD[3],
        //                 'kd_blok' => (is_numeric($blok_urutD[0])) ? $blok_urutD[0] : '000',
        //                 'no_urut' => (is_numeric($blok_urutD[1])) ? $blok_urutD[1] : '000',
        //                 'kd_jns_op' => (is_numeric($nopD[5])) ? $nopD[5] : '0',
        //                 'luas_tanah' => $item['luas_bumi'],
        //                 'luas_bangunan' => $item['luas_bng']
        //             ];
        //             PenelitianLapangan::where(['id' =>  $item['objek_id']])->update($dataSet);
        //         }
        //     }
        //     DB::commit();
        //     $pesan = ['success' => "berhasil di proses"];
        // } catch (\Throwable $th) {
        //     DB::rollBack();
        //     Log::alert($th);
        //     $pesan = ['error' => $th->getMessage()];
        // }
        // return redirect(url('penelitian/lapangan'))->with($pesan);
    }


    public function hasilPenelitian($id)
    {
        $penelitian = Layanan_objek::whereraw("id ='$id'")->get();
        $formulir = '';
        foreach ($penelitian as $penelitian) {
            $formulir .= $penelitian->nomor_formulir . ',';
        }

        $formulir = substr($formulir, 0, -1);
        // tagihan
        $tagihan = [];
        if ($formulir <> '') {
            $tagihan = Pajak::TagihanPenelitian($formulir);
        }

        $urlback = url('penelitian/lapangan');
        return view('penelitian.hasil_penelitian', compact('urlback', 'id', 'tagihan'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function switchPenelitian($idObjek)
    {
        DB::beginTransaction();
        try {
            $layanan = Layanan_objek::find($idObjek);
            $nopel = $layanan->nomor_layanan;
            $id = $layanan->nop_gabung <> '' ? $layanan->nop_gabung : $layanan->id;

            $penelitiantask = penelitianTask::where('layanan_objek_id', $id)->where('nomor_layanan', $nopel)->first();
            $penelitiantask->update(['penelitian' => 1]);
            DB::commit();
            $flash = ['success' => 'Berhasil di limpahkan ke penelitian kantor'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect()->back()->with($flash);
    }
}
