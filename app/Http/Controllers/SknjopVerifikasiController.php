<?php

namespace App\Http\Controllers;

use App\Jobs\BatchSknjop;
use App\pegawaiStruktural;
use App\SkNJop;
use App\sknjoppermohonan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SknjopVerifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sknjop = sknjoppermohonan::where('kd_status', '0')->wherenull('deleted_at')->orderby('created_at', 'desc')->paginate(10);
        return view('sknjop.verifikasi-index', compact('sknjop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permohonan = sknjoppermohonan::with('nop')->findorfail($id);
        return view('sknjop.show-verifikasi', compact('permohonan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permohonan = sknjoppermohonan::findorfail($id);

        DB::beginTransaction();
        try {

            // loping nopsk njop
            $peg = pegawaiStruktural::where('kode', '02')->first();


            $jumlah_op = 0;
            $tolak = 0;
            $nomersk = getNoSkNjop();
            $exp = explode('/', $nomersk);
            $str = $exp[0];
            // dd($nomersk);

            foreach ($request->nop_cetak as $i => $row) {
                $jumlah_op += 1;
                $res = $request->nop_cetak[$i];
                $kd_propinsi = substr($res, 0, 2);
                $kd_dati2 = substr($res, 2, 2);
                $kd_kecamatan = substr($res, 4, 3);
                $kd_kelurahan = substr($res, 7, 3);
                $kd_blok = substr($res, 10, 3);
                $no_urut = substr($res, 13, 4);
                $kd_jns_op = substr($res, 17, 1);

                $sknjop = SkNJop::where('sknjop_permohonan_id', $id)->whereraw(DB::raw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'"))->first();
                $sknjop->kd_status = $request->status_cetak[$i];
                $sknjop->verifikasi_at = Carbon::now();
                $sknjop->verifikasi_by = Auth()->user()->id;
                if ($request->status_cetak[$i] == '1') {

                    $urut = sprintf("%04s", abs($str + $i));
                    $nomer_jadi = $urut . '/' . $exp[1] . '/' . $exp[2] . '/' . $exp[3] . '/' . $exp[4];
                    // nomer awal sprintf("%04s", $no) 
                    // data sk/ penomoran
                    $sknjop->nomer_sk = $nomer_jadi;

                    $sknjop->tanggal_sk = Carbon::now();
                    // nama ttd
                    $sknjop->nama_kabid = $peg->nama;
                    $sknjop->nip_kabid = $peg->nip;
                } else {
                    $tolak += 1;
                }
                $sknjop->save();
            }

            // update permohonan
            if ($tolak == $jumlah_op) {
                $sp = 2;
            } else {
                $sp = 1;
            }

            $permohonan->kd_status = $sp;
            $permohonan->verifikasi_at = Carbon::now();
            $permohonan->verifikasi_by = Auth()->user()->id;
            $permohonan->save();
            DB::commit();

            dispatch(new BatchSknjop($id))->onQueue('sknjop');

            $pesan = ['success' => "telah di proses"];

            if ($sp == 1) {
                $url = url('sknjop-cetak-batch/' . $id);
            } else {
                $url = url('sknjop-verifikasi');
            }
        } catch (\Throwable $th) {
            DB::rollback();
            $pesan = ['success' => $th->getMessage()];
            $url = url('sknjop-verifikasi');
        }
        return redirect($url)->with($pesan);
        // return [$pesan, $url];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
