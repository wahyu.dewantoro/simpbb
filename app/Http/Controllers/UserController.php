<?php

namespace App\Http\Controllers;

// use App\Authorizable;

use App\Dokumen as AppDokumen;
use App\Helpers\Dokumen;
use App\Kecamatan;
use App\KelolaUsulan;
use App\KelolaUsulanObjek;
use App\Kelurahan;
use App\Role;
use App\Unitkerja;
use App\User;
use App\UserRayon;
use App\UserUnitKerja;
use App\UsulanPengelola;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Stmt\UseUse;

class UserController extends Controller
{
    // use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:view_users')->only(['index']);
        $this->middleware('permission:add_users')->only(['create', 'store']);
        // $this->middleware('permission:edit_users')->only(['edit', 'update']);
        $this->middleware('permission:delete_users')->only(['destroy']);
    }
    public function index(Request $request)
    {
        $result = User::with(['unitkerja', 'roles']);

        // Cek jika ada input pencarian
        if ($request->has('cari')) {
            $q = strtolower($request->cari);
            // Gunakan query builder untuk menghindari raw SQL yang rentan terhadap SQL Injection
            $result = $result->where(function ($query) use ($q) {
                $query->whereRaw('lower(nama) like ?', ["%$q%"])
                    ->orWhereRaw('lower(nik) like ?', ["%$q%"])
                    ->orWhereRaw('lower(username) like ?', ["%$q%"])
                    ->orWhereRaw('lower(telepon) like ?', ["%$q%"])
                    ->orWhereRaw('lower(email) like ?', ["%$q%"]);
            });
        }
        // Pastikan data diurutkan berdasarkan nama dan tidak termasuk soft-deleted
        $result = $result->orderBy('nama', 'asc')->paginate(10);

        return view('user.index_table', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');
        $title = "Add Users";
        $unitkerja = Unitkerja::get();
        $form = (object)[
            'action' => url('users'),
            'method' => 'post',
            'user' => []
        ];
        $selecteduk = "";
        return view('user.form', compact('roles', 'title', 'form', 'unitkerja', 'selecteduk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nama' => 'bail|required|min:5',
            'nama_pendek' => 'bail|required',
            // 'nik' => 'bail|required|numeric|digits:16',
            // 'telepon' => 'bail|required|min:10|numeric',
            'roles' => 'required|min:1',
            'kd_unit' => 'required',
            // 'email' => 'required|email',
            'username' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);



        try {
            //code...
            DB::beginTransaction();
            // hash password
            $request->merge(['password' => bcrypt($request->get('password'))]);

            $data = $request->except('roles', 'permissions', '_token', '_method', 'kd_unit');
            $data['email_verified_at'] = now();
            $user = User::create($data);
            UserUnitKerja::updateOrCreate(['user_id' => $user->id], ['kd_unit' => $request->kd_unit]);
            $this->syncPermissions($request, $user);
            DB::commit();

            $flash = [
                'success' => 'Data berhasil disimpan'
            ];
        } catch (\Exception $th) {
            DB::rollback();

            $flash = [
                'error' => $th->getMessage()
            ];
        }
        // dd($flash);

        return redirect()->route('users.index')->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengguna = User::with('Pendukung', 'UserRayon')->findorfail($id);
        $is_wp = $pengguna->is_wp();
        $rayon = $pengguna->UserRayon()->latest()->get();
        // UserPengelola
        $developer = $pengguna->UserPengelola()->latest()->get();

        // return $developer;
        return view('user.profile', compact('pengguna', 'is_wp', 'rayon', 'developer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::find($id);
        $sr = $user->roles()->pluck('name')->toArray();
        $roles = Role::pluck('name', 'id');
        $title = "Edit Users";
        $form = (object)[
            'action' => route('users.update', $id),
            'method' => 'patch',
            'user' => $user
        ];
        $unitkerja = Unitkerja::get();
        $selecteduk = $user->UnitKerja()->first()->kd_unit ?? '';

        return view('user.form', compact('user', 'title', 'form', 'sr', 'roles', 'unitkerja', 'selecteduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //code...
        $validate = [
            'nama' => 'bail|required',
            'nama_pendek' => 'bail|required',
            'kd_unit' => 'required',
            'username' => 'unique:users,username,' . $id
        ];



        if (Auth()->user()->hasRole('Super User')) {
            // 'roles' => 'required|min:1',
            $validate['roles'] = 'required|min:1';
        }

        $this->validate($request, $validate);
        // dd($request->all());

        DB::beginTransaction();
        try {
            // Get the user
            $user = User::findOrFail($id);
            // Update user        
            $user->fill($request->except('roles', 'kd_unit', 'permissions', '_token', '_method'));

            // update kd_unit
            $cek = UserUnitKerja::where('user_id', $id)->count();
            if ($cek == 1) {
                UserUnitKerja::where('user_id', $id)->update([
                    'kd_unit' => $request->kd_unit
                ]);
            } else {
                UserUnitKerja::insert(['user_id' => $id, 'kd_unit' => $request->kd_unit]);
            }

            // 


            // check for password change
            if ($request->get('password')) {
                $user->password = bcrypt($request->get('password'));
            }

            // if ($user->update_profile == '') {
            $user->update_profile = new Carbon();
            // }

            if (Auth()->user()->hasRole('Super User')) {
                // Handle the user roles
                $this->syncPermissions($request, $user);
            }

            $user->save();
            // Unable to create user
            $flash = [
                'success' => 'Data user telah di perbarui'
            ];
            DB::commit();
            Cache::flush();
        } catch (\Exception $th) {
            //throw $th;
            Log::error($th);
            DB::rollback();
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(route('users.show', $id))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        // Cek apakah user yang akan dihapus adalah user yang sedang login
        if (Auth::id() == $id) {
            return redirect()->back()->withErrors([
                'error' => 'Deletion of currently logged-in user is not allowed.'
            ]);
        }
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id); // Cari user berdasarkan ID

            if ($user->delete()) {
                DB::commit();
                session()->flash('success', 'User has been deleted successfully.');
            } else {
                DB::rollBack();
                session()->flash('error', 'User could not be deleted.');
            }
        } catch (\Exception $e) {
            DB::commit();
            // Tangani jika terjadi kesalahan
            session()->flash('error', 'An error occurred while trying to delete the user: ' . $e->getMessage());
        }

        // Redirect kembali dengan pesan flash
        return redirect(url('users'));
    }


    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);
        $roles = Role::whereIn('name', $roles)->get();
        if (!$user->hasAllRoles($roles)) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);
        return $user;
    }

    public function Impersonate(Request $request)
    {
        $result = User::with('roles')->whereraw("id <> '" . Auth()->user()->id . "' ");
        if ($request->cari) {
            $q = strtolower($request->cari);
            $result = $result->whereraw("(lower(nama) like '%$q%'
            or lower(nik) like '%$q%'
            or lower(username) like '%$q%'
            or lower(telepon) like '%$q%'
            or lower(email) like '%$q%')");
        }
        $result = $result->latest()->paginate(10);
        return view('user.impersonate', compact('result'));
    }

    public function ProsesImpersonate(User $user)
    {
        Auth::user()->impersonate($user);

        return redirect(url('users', $user->id))->with(['success' => 'Impersonated as ' . $user->nama]);
    }

    public function LeaveImpersonate()
    {
        Auth::user()->leaveImpersonation();
        return redirect(url('users'))->with(['success' => 'Impersonated Leaved']);
    }

    public function reset_password($id)
    {

        if (Auth::user()->id == $id) {
            $flash = ['error' => 'Deletion of currently logged in user is not allowed :('];
            return redirect()->back()->with($flash);
        }
        $update = User::where('id', $id);
        $data = [
            'password' => bcrypt("pass"),
        ];

        if ($update->update($data)) {
            // flash()->success('User has been deleted');
            $flash = ['success' => 'Password user has been reset'];
        } else {
            // flash()->success('User not deleted');
            $flash = ['error' => 'Password user can not reset'];
        }

        return redirect()->back()->with($flash);
    }

    public function edit_password($id = null)
    {
        //
        $id = $id == '' ? Auth()->user()->id : $id;
        // $user = User::find($id);
        $title = "Change Password";
        $form = (object)[
            'action' => url('update_password', $id),
            'method' => 'post',
            // 'user' => $user
        ];
        return view('user.change_password', compact('title', 'form'));
    }

    public function update_password(Request $request, $id)
    {
        $user = User::find($id);
        $password = $request->password_lama;
        $password_baru = bcrypt($request->password_baru);
        DB::beginTransaction();
        try {
            if (Hash::check($password, $user->password)) {
                $change_pwd = User::where('id', $id);
                $data = [
                    'password' => $password_baru
                ];

                $change_pwd->update($data);
                $flash = [
                    'success' => 'Password Berhasil di update.'
                ];
                $kode = 1;
                DB::commit();
                $usr = User::find($id);
                event(new Registered($usr));
            } else {
                $flash = [
                    'error' => 'Password lama salah !'
                ];
                $kode = 0;
            }
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
            $kode = 0;
        }
        return redirect(route('users.show', $id))->with($flash);
    }

    // upgrade user menmjadi rayon/developer

    public function upgradeUser()
    {
        $disk = 'public';
        $pengguna = Auth()->user();
        $roles = Role::whereraw("name in ('DEVELOPER/PENGEMBANG','Rayon')")
            ->orderby('id')->pluck('name')->toarray();
        $kecamatan = Kecamatan::orderby('kd_kecamatan')->pluck('nm_kecamatan', 'kd_kecamatan')->toarray();
        $kelurahan = Kelurahan::pluck('nm_kelurahan')->toarray();
        $ac_kecamatan = [];
        foreach ($kecamatan as $i) {
            $ac_kecamatan[] = $i;
        }
        $ar_kel =  array_unique($kelurahan);
        $ac_kelurahan = [];
        foreach ($ar_kel as $i) {
            $ac_kelurahan[] = $i;
        }
        $ac_kelurahan = json_encode($ac_kelurahan);
        $ac_kecamatan = json_encode($ac_kecamatan);
        return view('user.upgrade_akun', compact('pengguna', 'roles', 'kecamatan', 'ac_kecamatan', 'ac_kelurahan'));
    }


    public function upgradeUserStore(Request $request)
    {
        $disk = 'public';
        $user = Auth()->user();
        DB::beginTransaction();
        try {


            // if rayon
            if ($request->role == 'Rayon') {
                $rayon = [
                    'users_id' => $user->id,
                    'kd_kecamatan' => $request->kd_kecamatan,
                    'kd_kelurahan' => $request->kd_kelurahan,
                    'no_surat' => $request->no_surat,
                    'tgl_surat' => new Carbon($request->tgl_surat)
                ];
                $ur = UserRayon::create($rayon);
                $table_name = "user_rayon";
                $table_id = $ur->id;
            }



            // if developer
            if ($request->role == 'DEVELOPER/PENGEMBANG') {
                $dataDeveloper = [
                    'user_id' => $user->id,
                    'nama_pengembang' => $request->nama_pengembang,
                    'no_surat' => $request->no_surat,
                    'tgl_surat' => new Carbon($request->tgl_surat)
                ];
                $up = UsulanPengelola::create($dataDeveloper);
                $table_name = "user_pengelola";
                $table_id = $up->id;
            }


            if ($request->file('file_rekom') && ($request->role == 'DEVELOPER/PENGEMBANG' || $request->role == 'Rayon')) {
                // file_rekom
                $file_rekom = $request->file('file_rekom');
                $filename_rekom = time() . '_rekom.' . $file_rekom->getClientOriginalExtension();

                if (Storage::disk($disk)->exists($filename_rekom)) {
                    Storage::disk($disk)->delete($filename_rekom);
                }
                $upload_file_rekom = Storage::disk($disk);
                if ($upload_file_rekom->put($filename_rekom, File::get($file_rekom))) {
                    $url_file_rekom = Storage::disk($disk)->url($filename_rekom);
                    $url_file_rekom = str_replace(config('app.url'), '', $url_file_rekom);
                    AppDokumen::create([
                        'tablename' => $table_name,
                        'keyid' => $table_id,
                        'filename' => $filename_rekom,
                        'disk' => $disk,
                        'nama_dokumen' => 'Dokumen Pendukung',
                        'keterangan' => 'Dokumen Pendukung',
                        'url' => $url_file_rekom
                    ]);
                }
            }
            $usd = User::find($user->id);
            $usd->syncRoles([$request->role]);
            DB::commit();

            $flash = [
                'success' => 'Akun berhasil di upgrade, dan akan segera di verifikasi !'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            // dd($th);
            $flash = ['error' => "Maaf, terjadi kesalahan teknis saat memproses data Anda. Silakan coba lagi atau hubungi tim dukungan jika masalah terus berlanjut."];
            // $redirectTo = url('register');
        }
        $redirectTo = url('users', $user->id);
        return redirect($redirectTo)->with($flash);
    }
}
