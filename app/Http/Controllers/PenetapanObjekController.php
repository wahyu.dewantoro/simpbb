<?php

namespace App\Http\Controllers;

use App\Helpers\Esppt;
use App\Helpers\Pajak;
use App\Helpers\PenetapanSppt;
use App\Jobs\generateFileSppt;
use App\Jobs\PenetapanMasalJob;
use App\Jobs\SpptTTEJob;
use App\Kecamatan;
use App\Models\HisPenetapan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Svg\Tag\Rect;
use DOMPDF as dompdf;

class PenetapanObjekController extends Controller
{
   //
   public function index(Request $request)
   {
      if ($request->ajax()) {

         if ($request->checklist) {
            $tahun = $request->tahun;
            $data = db::connection("oracle_satutujuh")->select(db::raw("select 'Nilai NIR' Jenis,(select count(1) from dat_nir where thn_nir_znt='" . $tahun . "' ) value,1 urut
            from dual
            union 
            select 'Harga Resource' Jenis,(select count(1) from HRG_RESOURCE where thn_hrg_resource='" . $tahun . "' ) value,2 urut
            from dual
            union 
            select 'Nilai fasilitas' Jenis,(select count(1) from fas_non_dep where thn_non_dep='" . $tahun . "' ) value,3 urut
            from dual
            union
            select 'DBKB Material' Jenis,(select count(1) from DBKB_MATERIAL
            where thn_dbkb_material='" . $tahun . "') value,4 urut
            from dual
            union
            select 'DBKB Standard'  jenis, (select count(1) from   DBKB_STANDARD where thn_DBKB_STANDARD ='" . $tahun . "' ) value , 5 urut from dual union
            select 'DBKB Daya dukung'  jenis, (select count(1) from   DBKB_DAYA_DUKUNG where thn_DBKB_DAYA_DUKUNG ='" . $tahun . "' ) value , 6 urut from dual union
            select 'DBKB MEZANIN'  jenis, (select count(1) from   DBKB_MEZANIN where thn_DBKB_MEZANIN ='" . $tahun . "' ) value , 7 urut from dual union
            select 'DBKB JPB2'  jenis, (select count(1) from   DBKB_JPB2 where thn_DBKB_JPB2 ='" . $tahun . "' ) value , 8 urut from dual union
            select 'DBKB JPB3'  jenis, (select count(1) from   DBKB_JPB3 where thn_DBKB_JPB3 ='" . $tahun . "' ) value , 9 urut from dual union
            select 'DBKB JPB4'  jenis, (select count(1) from   DBKB_JPB4 where thn_DBKB_JPB4 ='" . $tahun . "' ) value , 10 urut from dual union
            select 'DBKB JPB5'  jenis, (select count(1) from   DBKB_JPB5 where thn_DBKB_JPB5 ='" . $tahun . "' ) value , 11 urut from dual union
            select 'DBKB JPB6'  jenis, (select count(1) from   DBKB_JPB6 where thn_DBKB_JPB6 ='" . $tahun . "' ) value , 12 urut from dual union
            select 'DBKB JPB7'  jenis, (select count(1) from   DBKB_JPB7 where thn_DBKB_JPB7 ='" . $tahun . "' ) value , 13 urut from dual union
            select 'DBKB JPB8'  jenis, (select count(1) from   DBKB_JPB8 where thn_DBKB_JPB8 ='" . $tahun . "' ) value , 14 urut from dual union
            select 'DBKB JPB9'  jenis, (select count(1) from   DBKB_JPB9 where thn_DBKB_JPB9 ='" . $tahun . "' ) value , 15 urut from dual union
            select 'DBKB JPB12'  jenis, (select count(1) from   DBKB_JPB12 where thn_DBKB_JPB12 ='" . $tahun . "' ) value , 16 urut from dual union
            select 'DBKB JPB13'  jenis, (select count(1) from   DBKB_JPB13 where thn_DBKB_JPB13 ='" . $tahun . "' ) value , 17 urut from dual union
            select 'DBKB JPB14'  jenis, (select count(1) from   DBKB_JPB14 where thn_DBKB_JPB14 ='" . $tahun . "' ) value , 18 urut from dual union
            select 'DBKB JPB15'  jenis, (select count(1) from   DBKB_JPB15 where thn_DBKB_JPB15 ='" . $tahun . "' ) value , 19 urut from dual union
            select 'DBKB JPB16'  jenis, (select count(1) from   DBKB_JPB16 where thn_DBKB_JPB16 ='" . $tahun . "' ) value , 20 urut from dual  
            order by urut"));

            $data = array_chunk($data, 5);
            return view('penetapan.checklist_penetapan', compact('data'));
         }

         if ($request->ubah_tahun) {
            $tahun = $request->tahun;
            $jatuhtempo = date_format(date_create("31-08-" . $tahun), "d M Y");
            $terbit = date_format(date_create("02-01-" . $tahun), "d M Y");

            return [
               'jatuhtempo' => $jatuhtempo,
               'terbit' => $terbit
            ];
         }
      }


      $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
      $tahun = date('Y');
      if (in_array(date('m'), ['11', '12'])) {
         $tahun = date('Y') + 1;
      }
      $jatuhtempo = date_format(date_create("31-08-" . $tahun), "d M Y");
      $terbit = date_format(date_create("02-01-" . $tahun), "d M Y");
      return view('penetapan.index', compact('kecamatan', 'tahun', 'jatuhtempo', 'terbit'));
   }



   public function create(Request $request)
   {
      // return $request->all();
      $kd_blok = $request->kd_blok;
      $kd_kecamatan = $request->kd_kecamatan;
      $kd_kelurahan = $request->kd_kelurahan;
      $no_urut = $request->no_urut;
      $tahun_pajak = $request->tahun_pajak;
      $tgl_jatuh_tempo = $request->tgl_jatuh_tempo;
      $tgl_terbit = $request->tgl_terbit;


      $objek = DB::connection('oracle_satutujuh')->table('DAT_OBJEK_PAJAK')
         ->join('dat_op_bumi', function ($join) {
            $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
               ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
               ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
               ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
               ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
               ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
               ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
         })->select(db::raw("dat_objek_pajak.kd_propinsi,
        dat_objek_pajak.kd_dati2,
        dat_objek_pajak.kd_kecamatan,
        dat_objek_pajak.kd_kelurahan,
        dat_objek_pajak.kd_blok,
        dat_objek_pajak.no_urut,
        dat_objek_pajak.kd_jns_op"))
         ->whereraw("dat_objek_pajak.kd_kecamatan ='" . $kd_kecamatan . "' and dat_objek_pajak.kd_kelurahan='" . $kd_kelurahan . "'");


      if (!empty($kd_blok)) {
         $objek = $objek->whereraw("dat_objek_pajak.kd_blok='" . $kd_blok . "'");
      }

      if (!empty($no_urut)) {
         $objek = $objek->whereraw("dat_objek_pajak.no_urut='" . $no_urut . "'");
      }

      return $objek->get();
   }

   public function store(Request $request)
   {
      // $terbit = date('dmY', strtotime($request->tgl_terbit));
      // $jatuhtempo = date('dmY', strtotime($request->tgl_jatuh_tempo));
      $terbit = $request->tgl_terbit;
      $jatuhtempo = $request->tgl_jatuh_tempo;
      $tahun = $request->tahun_pajak;
      $kd_propinsi = '35';
      $kd_dati2 = '07';
      $kd_kecamatan = $request->kd_kecamatan;
      $kd_kelurahan = $request->kd_kelurahan;
      $kd_blok = $request->kd_blok;
      $no_urut = $request->no_urut;
      $kd_jns_op = $request->kd_jns_op;
      $nip = '000000000000001003';


      // sql penilaiain
      $objek = DB::connection('oracle_satutujuh')->table('DAT_OBJEK_PAJAK')
         ->join('dat_op_bumi', function ($join) {
            $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
               ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
               ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
               ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
               ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
               ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
               ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
         })
         ->leftjoin('dat_nir',function($join) use ($tahun) {
            $join->on('dat_nir.kd_propinsi','=','dat_op_bumi.kd_propinsi')
                   ->on('dat_nir.kd_dati2','=','dat_op_bumi.kd_dati2')
                   ->on('dat_nir.kd_kecamatan','=','dat_op_bumi.kd_kecamatan')
                   ->on('dat_nir.kd_kelurahan','=','dat_op_bumi.kd_kelurahan')
                   ->on('dat_nir.kd_znt','=','dat_op_bumi.kd_znt')
                   ->where('dat_nir.thn_nir_znt',$tahun);
           })
         ->select(db::raw("dat_objek_pajak.kd_propinsi,
            dat_objek_pajak.kd_dati2,
            dat_objek_pajak.kd_kecamatan,
            dat_objek_pajak.kd_kelurahan,
            dat_objek_pajak.kd_blok,
            dat_objek_pajak.no_urut,
            dat_objek_pajak.kd_jns_op"))
         ->whereraw(" total_luas_bumi>0 and  jns_transaksi_op != '3'
        AND jns_bumi IN ('1', '2', '3') and dat_nir.kd_znt is not null");


      if ($kd_kecamatan != '') {
         $objek = $objek->where('dat_objek_pajak.kd_kecamatan', $kd_kecamatan);
      }
      if ($kd_kelurahan != '') {
         $objek = $objek->where('dat_objek_pajak.kd_kelurahan', $kd_kelurahan);
      }
      if ($kd_blok != '') {
         $objek = $objek->where('dat_objek_pajak.kd_blok', $kd_blok);
      }
      if ($no_urut != '') {
         $objek = $objek->where('dat_objek_pajak.no_urut',  padding($no_urut, 0, 4));
      }


      // $potongan = "";
      foreach ($objek->get() as $rk) {
         /* $penilaian = "";
         $penilaian .= " PBB.PENENTUAN_NJOP ( '35','07', '" . $rk->kd_kecamatan . "', '" . $rk->kd_kelurahan . "','" . $rk->kd_blok . "', '" . $rk->no_urut . "','" . $rk->kd_jns_op . "', '" . $tahun . "', 1 ); commit; ";
         $penilaian .= " PBB.PENETAPAN_NOP_20 ( '35','07', '" . $rk->kd_kecamatan . "', '" . $rk->kd_kelurahan . "','" . $rk->kd_blok . "', '" . $rk->no_urut . "','" . $rk->kd_jns_op . "', '" . $tahun . "', to_date('" . $terbit . "','ddmmyyyy'), to_date('" . $jatuhtempo . "','ddmmyyyy'), '" . $nip . "' ); commit; ";
         $penilaian .= " PBB.HITUNG_POTONGAN('" . $rk->kd_kecamatan . "','" . $rk->kd_kelurahan . "','" . $rk->kd_blok . "','" . $rk->no_urut . "','" . $rk->kd_jns_op . "','" . $tahun . "'); commit; ";
         $full = "Begin  " . $penilaian . "   end;";
         DB::connection("oracle_satutujuh")->statement(db::raw($full));
 */
         $kd_propinsi = $rk->kd_propinsi;
         $kd_dati2 = $rk->kd_dati2;
         $kd_kecamatan = $rk->kd_kecamatan;
         $kd_kelurahan = $rk->kd_kelurahan;
         $kd_blok = $rk->kd_blok;
         $no_urut = $rk->no_urut;
         $kd_jns_op = $rk->kd_jns_op;
         $tahun = $tahun;
         $tgl_terbit = $terbit;
         $tgl_jatuh_tempo = $jatuhtempo;


         PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun, $tgl_terbit, $tgl_jatuh_tempo);
      }



      return ['msg' => "Penetapan masal berhasil"];
   }


   public function NopPenetapan($tahun, $terbit, $jatuhtempo, $nip)
   {
      $data = DB::connection("oracle_satutujuh")->table(db::raw("(SELECT final.KD_PROPINSI,
        final.KD_DATI2,
        final.KD_KECAMATAN,
        final.KD_KELURAHAN,
        final.KD_BLOK,
        final.NO_URUT,
        final.KD_JNS_OP,
        final.THN_PAJAK_SPPT,
        final.SIKLUS_SPPT,
        final.KD_KANWIL,
        final.KD_KANTOR,
        final.KD_TP,
        final.NM_WP_SPPT,
        final.JLN_WP_SPPT,
        final.BLOK_KAV_NO_WP_SPPT,
        final.RW_WP_SPPT,
        final.RT_WP_SPPT,
        final.KELURAHAN_WP_SPPT,
        final.KOTA_WP_SPPT,
        final.KD_POS_WP_SPPT,
        final.NPWP_SPPT,
        final.NO_PERSIL_SPPT,
        final.KD_KLS_TANAH,
        final.THN_AWAL_KLS_TANAH,
        final.KD_KLS_BNG,
        final.THN_AWAL_KLS_BNG,
        final.TGL_JATUH_TEMPO_SPPT,
        final.LUAS_BUMI_SPPT,
        final.LUAS_BNG_SPPT,
        final.NJOP_BUMI_SPPT,
        final.NJOP_BNG_SPPT,
        (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT) - final.NJOPTKP
           njop_sppt,
        final.NJOPTKP njoptkp_sppt,
        ROUND (
             (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
              - final.NJOPTKP)
           * (nilai_tarif / 100))
           pbb_terhutang_sppt,
        0 faktor_pengurang_sppt,
        CASE
           WHEN NVL (nilai_pbb_minimal, 0) >
                   ROUND (
                        (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
                         - final.NJOPTKP)
                      * (nilai_tarif / 100))
           THEN
              NVL (nilai_pbb_minimal, 0)
           ELSE
              ROUND (
                   (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
                    - final.NJOPTKP)
                 * (nilai_tarif / 100))
        END
           pbb_yg_harus_dibayar_sppt,
        '0' status_pembayaran_sppt,
        '0' status_tagihan_sppt,
        '1' status_cetak_sppt,
        TO_DATE ('" . $terbit . "', 'ddmmyyyy') tgl_terbit_sppt,
        TRUNC (TO_DATE ('" . $terbit . "', 'ddmmyyyy') + 2) tgl_cetak_sppt,
        '" . $nip . "' nip_pencetak_sppt
   FROM (SELECT core.kd_propinsi,
                core.kd_dati2,
                core.kd_kecamatan,
                core.kd_kelurahan,
                core.kd_blok,
                core.no_urut,
                core.kd_jns_op,
                thn_pajak_sppt,
                siklus_sppt,
                kd_kanwil,
                kd_kantor,
                kd_tp,
                nm_wp_sppt,
                jln_wp_sppt,
                blok_kav_no_wp_sppt,
                rw_wp_sppt,
                rt_wp_sppt,
                kelurahan_wp_sppt,
                kota_wp_sppt,
                kd_pos_wp_sppt,
                npwp_sppt,
                no_persil_sppt,
                NVL (kd_kls_tanah, 'XXX') kd_kls_tanah,
                NVL (thn_awal_kls_tanah, '1986') thn_awal_kls_tanah,
                NVL (kd_kls_bng, 'XXX') kd_kls_bng,
                NVL (thn_awal_kls_bng, '1986') thn_awal_kls_bng,
                TO_DATE ('" . $jatuhtempo . "', 'ddmmyyyy') tgl_jatuh_tempo_sppt,
                total_luas_bumi luas_bumi_sppt,
                total_luas_bng luas_bng_sppt,
                total_luas_bumi * nilai_per_m2_tanah * 1000
                   njop_bumi_sppt,
                total_luas_bng * NVL (nilai_per_m2_bng, 0) * 1000
                   njop_bng_sppt,
                NVL (njoptkp, 0) njoptkp
           FROM (SELECT dat_objek_pajak.kd_propinsi,
                        dat_objek_pajak.kd_dati2,
                        dat_objek_pajak.kd_kecamatan,
                        dat_objek_pajak.kd_kelurahan,
                        dat_objek_pajak.kd_blok,
                        dat_objek_pajak.no_urut,
                        dat_objek_pajak.kd_jns_op,
                        '" . $tahun . "' thn_pajak_sppt,
                        1 siklus_sppt,
                        '01' kd_kanwil,
                        '01' kd_kantor,
                        '49' kd_tp,
                        nm_wp nm_wp_sppt,
                        jalan_wp jln_wp_sppt,
                        blok_kav_no_wp blok_kav_no_wp_sppt,
                        rw_wp rw_wp_sppt,
                        rt_wp rt_wp_sppt,
                        kelurahan_wp kelurahan_wp_sppt,
                        kota_wp kota_wp_sppt,
                        kd_pos_wp kd_pos_wp_sppt,
                        npwp npwp_sppt,
                        no_persil no_persil_sppt,
                        total_luas_bumi,
                        CASE
                           WHEN total_luas_bumi > 0
                           THEN
                              ROUND (
                                 njop_bumi / total_luas_bumi / 1000)
                           ELSE
                              0
                        END
                           njop_bumi_meter,
                        njop_bumi,
                        total_luas_bng,
                        CASE
                           WHEN total_luas_bng > 0
                           THEN
                              ROUND (njop_bng / total_luas_bng / 1000)
                           ELSE
                              NULL
                        END
                           njop_bng_meter,
                        njop_bng
                   FROM DAT_OBJEK_PAJAK
                        JOIN dat_op_bumi
                           ON     dat_objek_pajak.kd_propinsi =
                                     dat_op_bumi.kd_propinsi
                              AND dat_objek_pajak.kd_dati2 =
                                     dat_op_bumi.kd_dati2
                              AND dat_objek_pajak.kd_kecamatan =
                                     dat_op_bumi.kd_kecamatan
                              AND dat_objek_pajak.kd_kelurahan =
                                     dat_op_bumi.kd_kelurahan
                              AND dat_objek_pajak.kd_blok =
                                     dat_op_bumi.kd_blok
                              AND dat_objek_pajak.no_urut =
                                     dat_op_bumi.no_urut
                              AND dat_objek_pajak.kd_jns_op =
                                     dat_op_bumi.kd_jns_op
                        JOIN dat_subjek_pajak
                           ON dat_subjek_pajak.subjek_pajak_id =
                                 dat_objek_pajak.subjek_pajak_id
                  WHERE     jns_transaksi_op != '3'
                        AND jns_bumi IN ('1', '2', '3')) core
                LEFT JOIN kelas_tanah
                   ON     kelas_tanah.thn_awal_kls_tanah <=
                             core.thn_pajak_sppt
                      AND kelas_tanah.thn_akhir_kls_tanah >=
                             core.thn_pajak_sppt
                      AND kelas_tanah.nilai_min_tanah <
                             core.njop_bumi_meter
                      AND kelas_tanah.nilai_max_tanah >=
                             core.njop_bumi_meter
                      AND kelas_tanah.kd_kls_tanah != 'XXX'
                LEFT JOIN kelas_bangunan
                   ON     kelas_bangunan.thn_awal_kls_bng <=
                             core.thn_pajak_sppt
                      AND kelas_bangunan.thn_akhir_kls_bng >=
                             core.thn_pajak_sppt
                      AND kelas_bangunan.nilai_min_bng <
                             core.njop_bng_meter
                      AND kelas_bangunan.nilai_max_bng >=
                             core.njop_bng_meter
                      AND kelas_bangunan.kd_kls_bng != 'XXX'
                LEFT JOIN
                (SELECT dat_subjek_pajak_njoptkp.kd_propinsi,
                        dat_subjek_pajak_njoptkp.kd_dati2,
                        kd_kecamatan,
                        kd_kelurahan,
                        kd_blok,
                        no_urut,
                        kd_jns_op,
                        thn_njoptkp,
                        nilai_njoptkp * 1000 njoptkp
                   FROM dat_subjek_pajak_njoptkp
                        JOIN njoptkp
                           ON     NJOPTKP.THN_AWAL <=
                                     dat_subjek_pajak_njoptkp.thn_njoptkp
                              AND dat_subjek_pajak_njoptkp.thn_njoptkp <=
                                     NJOPTKP.thn_akhir) njoptkp
                   ON     njoptkp.kd_propinsi = core.kd_propinsi
                      AND njoptkp.kd_dati2 = core.kd_dati2
                      AND njoptkp.kd_kecamatan = core.kd_kecamatan
                      AND njoptkp.kd_kelurahan = core.kd_kelurahan
                      AND njoptkp.kd_blok = core.kd_blok
                      AND njoptkp.no_urut = core.no_urut
                      AND njoptkp.kd_jns_op = core.kd_jns_op
                      AND njoptkp.thn_njoptkp = core.thn_pajak_sppt)
        final
        JOIN tarif
           ON     tarif.thn_awal <= final.thn_pajak_sppt
              AND TARIF.THN_AKHIR >= final.thn_pajak_sppt
              AND tarif.njop_min <=
                     (  (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
                         - final.NJOPTKP))
              AND tarif.njop_max >=
                       (  (final.NJOP_BUMI_SPPT + final.NJOP_BNG_SPPT)
                        - final.NJOPTKP)
        LEFT JOIN pbb_minimal
           ON pbb_minimal.thn_pbb_minimal = final.thn_pajak_sppt) fix"))
         ->leftjoin('sppt', function ($join) {
            $join->on('fix.kd_propinsi', '=', 'sppt.kd_propinsi')
               ->on('fix.kd_dati2', '=', 'sppt.kd_dati2')
               ->on('fix.kd_kecamatan', '=', 'sppt.kd_kecamatan')
               ->on('fix.kd_kelurahan', '=', 'sppt.kd_kelurahan')
               ->on('fix.kd_blok', '=', 'sppt.kd_blok')
               ->on('fix.no_urut', '=', 'sppt.no_urut')
               ->on('fix.kd_jns_op', '=', 'sppt.kd_jns_op')
               ->on('fix.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
         })->select(db::raw("CASE
            WHEN sppt.status_pembayaran_sppt IS NULL THEN 1
            WHEN sppt.status_pembayaran_sppt <> 1 THEN 2
            ELSE 0
         END
            proses,
         fix.*"));
      return $data;
   }
}
