<?php

namespace App\Http\Controllers;

// use App\Helpers\gallade;
// use App\Models\JenisPengurangan;
// use App\Models\Tarif;
// use Illuminate\Support\Facades\Log;
// use Illuminate\Support\Str;

use App\Exports\dataNirExport;
use App\Exports\template_upload_nir_excel;
use App\Imports\NopRekonImport;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\DAT_NIR;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use DOMPDF as PDF;


class DataNirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Data NIR';
        $data = [];
        $nowYear = Carbon::now()->year;
        $rangeZ = 6;
        $nowYearFrom = $nowYear;
        $lasYear = $nowYear - $rangeZ;
        $listYear = [];
        $listYearFrom = [];
        for ($i = $lasYear + $rangeZ + 1; $i < $nowYear + $rangeZ; $i++) {
            $listYear[$i] = $i;
            $listYearFrom[$nowYearFrom] = $nowYearFrom;
            $nowYearFrom = $nowYearFrom - 1;
        }
        $kecamatan = Kecamatan::login()->orderBy('kd_kecamatan', 'asc')->get();
        if (!$kecamatan->count()) {
            $kecamatan = Kecamatan::orderBy('kd_kecamatan', 'asc')->get();
        }
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('kd_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('data_nir.index', compact('title', 'data', 'listYear', 'listYearFrom', 'kecamatan', 'kelurahan'));
    }

    public function cetak(Request $request)
    {

        // return $request->all();
        $select = [
            'dat_nir.kd_propinsi',
            'dat_nir.kd_dati2',
            'dat_nir.kd_kecamatan',
            'dat_nir.kd_kelurahan',
            'dat_nir.kd_znt',
            'dat_nir.thn_nir_znt',
            'dat_nir.kd_kanwil',
            'dat_nir.kd_kantor',
            'dat_nir.jns_dokumen',
            'dat_nir.no_dokumen',
            'dat_nir.nir',
            'dat_nir.nilai_hkpd',
            'ref_kecamatan.nm_kecamatan',
            'ref_kelurahan.nm_kelurahan',
            'nama_lokasi',
            Db::raw("  (select count(1) from dat_op_bumi where dat_op_bumi.kd_propinsi=dat_nir.kd_propinsi and 
            dat_op_bumi.kd_dati2=dat_nir.kd_dati2 and 
            dat_op_bumi.kd_kecamatan=dat_nir.kd_kecamatan and 
            dat_op_bumi.kd_kelurahan=dat_nir.kd_kelurahan and 
            dat_op_bumi.kd_znt=dat_nir.kd_znt ) jum_op
            ,su.nama created_by,dat_nir.created_at,
                            suu.nama updated_by,dat_nir.updated_at,            
                            (select count(1) from dat_nir_log where 
                kd_kecamatan=dat_nir.kd_kecamatan and kd_kelurahan=dat_nir.kd_kelurahan
                 and kd_znt=dat_nir.kd_znt 
                and thn_nir_znt=dat_nir.thn_nir_znt
            ) ju")
        ];
        $now = Carbon::now()->year;
        $join = 'dat_nir.kd_kecamatan and ref_kelurahan.kd_kelurahan=dat_nir.kd_kelurahan';
        $dataSet = DAT_NIR::select($select)
            ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'dat_nir.kd_kecamatan', "left outer")
            ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', DB::raw($join), "left outer")
            ->leftjoin('dat_znt', function ($join) {
                $join->on('dat_znt.kd_znt', '=', 'dat_nir.kd_znt')
                    ->on('dat_znt.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                    ->on('dat_znt.kd_kelurahan', '=', 'dat_nir.kd_kelurahan');
            })
            ->leftjoin('lokasi_objek', function ($join) {
                $join->on('lokasi_objek.id', '=', 'dat_znt.lokasi_objek_id');
            })
            ->leftjoin(DB::raw("sim_pbb.users su "), "su.id", '=', "dat_nir.created_by")
            ->leftjoin(DB::raw("sim_pbb.users suu"), "suu.id", '=', "dat_nir.updated_by")
            ->whereRaw("dat_nir.kd_propinsi='35' ")
            ->orderbyraw("dat_nir.thn_nir_znt desc,
                dat_nir.kd_propinsi desc,
                dat_nir.kd_dati2 desc,
                dat_nir.kd_kecamatan asc,
                dat_nir.kd_kelurahan asc,
                dat_nir.kd_znt asc");

        if ($request->has('tahun')) {
            $tahun = $request->get('tahun');
            if (!empty($tahun)) {
                $dataSet = $dataSet->whereraw("dat_nir.thn_nir_znt='$tahun'");
            }
        }
        if ($request->has('kecamatan')) {
            $kecamatan = $request->get('kecamatan');
            if ($kecamatan != '' && $kecamatan != 'null') {
                $dataSet = $dataSet->whereraw("dat_nir.kd_kecamatan='$kecamatan'");
            }
        }
        if ($request->has('kelurahan')) {
            $kelurahan = $request->kelurahan;
            if ($kelurahan != '' && $kelurahan != 'null') {
                $dataSet = $dataSet->whereraw("dat_nir.kd_kelurahan='$kelurahan'");
            }
        }
        $dataSet = $dataSet->get();

        // dd($dataSet);
        $dataArray = [];
        $no = 1;
        foreach ($dataSet as $row) {
            $dataArray[] = [
                'no' => $no,
                "nm_kecamatan" => $row->nm_kecamatan,
                "nm_kelurahan" => $row->nm_kelurahan,
                "kd_znt" => $row->kd_znt,
                "lokasi" => $row->nama_lokasi,
                'jum_op' => $row->jum_op,
                "tahun" => $row->thn_nir_znt,
                "nir" => $row->nir * 1000,
                "hkpd" => $row->nilai_hkpd,
                "created" => $row->created_by . "<br>" . $row->created_at . " <br>" . ($row->ju + 1) . "x upload"
            ];
            $no++;
        }
        // return $dataArray;
        $pdf = PDF::loadView('data_nir/cetak_pdf', compact('dataArray'))->setPaper('a4', 'landscape');
        $filename = 'data nir.pdf';
        return $pdf->download($filename);
    }

    public function upload()
    {
        $kecamatan = Kecamatan::orderby('kd_kecamatan')->get();
        $kelurahan = [];
        return view('data_nir/formUpload', compact('kecamatan'));
    }

    public function uploadPreview(Request $request)
    {


        $file = $request->file;
        $array = Excel::toArray(new NopRekonImport(), $file);
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $array = $array[0];


        // cek sismiop/sistep
        $cs = DB::connection("oracle_satutujuh")->table("dat_znt")->selectRaw("case when regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end jenis ,count(kd_znt) jm")
            ->groupByRaw("case when regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end")
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")->pluck('jm', 'jenis')->toArray();

        if (isset($cs['sismiop']) == true) {
            $is_sistep = false;
        } else {
            $is_sistep = true;
        }

        return view('data_nir/uploadPreview', compact('array', 'tahun', 'kd_kecamatan', 'kd_kelurahan', 'is_sistep'));
    }

    // public function uploadStore

    public function search(Request $request)
    {
        $select = [
            'dat_nir.kd_propinsi',
            'dat_nir.kd_dati2',
            'dat_nir.kd_kecamatan',
            'dat_nir.kd_kelurahan',
            'dat_nir.kd_znt',
            'dat_nir.thn_nir_znt',
            'dat_nir.kd_kanwil',
            'dat_nir.kd_kantor',
            'dat_nir.jns_dokumen',
            'dat_nir.no_dokumen',
            'dat_nir.nir',
            'dat_nir.nilai_hkpd',
            'ref_kecamatan.nm_kecamatan',
            'ref_kelurahan.nm_kelurahan',
            'nama_lokasi',
            Db::raw("  (select count(1) from dat_op_bumi where dat_op_bumi.kd_propinsi=dat_nir.kd_propinsi and 
            dat_op_bumi.kd_dati2=dat_nir.kd_dati2 and 
            dat_op_bumi.kd_kecamatan=dat_nir.kd_kecamatan and 
            dat_op_bumi.kd_kelurahan=dat_nir.kd_kelurahan and 
            dat_op_bumi.kd_znt=dat_nir.kd_znt ) jum_op,su.nama created_by,dat_nir.created_at
            ,suu.nama updated_by,dat_nir.updated_at,(select count(1) from dat_nir_log where 
                kd_kecamatan=dat_nir.kd_kecamatan and kd_kelurahan=dat_nir.kd_kelurahan
                 and kd_znt=dat_nir.kd_znt 
                and thn_nir_znt=dat_nir.thn_nir_znt
            ) ju")
        ];
        $now = Carbon::now()->year;
        $join = 'dat_nir.kd_kecamatan and ref_kelurahan.kd_kelurahan=dat_nir.kd_kelurahan';
        $dataSet = DAT_NIR::select($select)
            ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'dat_nir.kd_kecamatan', "left outer")
            ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', DB::raw($join), "left outer")
            ->leftjoin('dat_znt', function ($join) {
                $join->on('dat_znt.kd_znt', '=', 'dat_nir.kd_znt')
                    ->on('dat_znt.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                    ->on('dat_znt.kd_kelurahan', '=', 'dat_nir.kd_kelurahan');
            })
            ->leftjoin('lokasi_objek', function ($join) {
                $join->on('lokasi_objek.id', '=', 'dat_znt.lokasi_objek_id');
            })
            ->leftjoin(DB::raw("sim_pbb.users su "), "su.id", '=', "dat_nir.created_by")
            ->leftjoin(DB::raw("sim_pbb.users suu"), "suu.id", '=', "dat_nir.updated_by")
            ->whereRaw("dat_nir.kd_propinsi='35' ")
            ->orderbyraw("dat_nir.thn_nir_znt desc,
                dat_nir.kd_propinsi desc,
                dat_nir.kd_dati2 desc,
                dat_nir.kd_kecamatan asc,
                dat_nir.kd_kelurahan asc,
                dat_nir.kd_znt asc");

        $datatables = DataTables::of($dataSet);
        return $datatables
            ->filter(function ($query) use ($request) {
                if ($request->has('tahun')) {
                    $tahun = $request->get('tahun');
                    if (!empty($tahun)) {
                        $query->whereraw("dat_nir.thn_nir_znt='$tahun'");
                    }
                }
                if ($request->has('kecamatan')) {
                    $kecamatan = $request->get('kecamatan');
                    if (!empty($kecamatan) && $kecamatan != "-") {
                        $query->whereraw("dat_nir.kd_kecamatan='$kecamatan'");
                    }
                }
                if ($request->has('kelurahan')) {
                    $kelurahan = $request->get('kelurahan');
                    if (!empty($kelurahan) && $kelurahan != "-") {
                        $query->whereraw("dat_nir.kd_kelurahan='$kelurahan'");
                    }
                }
            })
            ->addColumn('raw_tag', function ($q) {
                return '-';
            })->addColumn('kode', function ($q) {
                return $q->kd_propinsi . "." . $q->kd_dati2 . "." . $q->kd_kecamatan . "." . $q->kd_kelurahan;
            })
            ->editColumn('nilai_hkpd', function ($q) {
                return $q->nilai_hkpd . '%';
            })
            ->editColumn('nir', function ($q) {
                return angka($q->nir * 1000);
            })
            ->editColumn('nama_lokasi', function ($q) {
                return $q->nama_lokasi != '' ? $q->nama_lokasi : '';
            })
            ->addColumn('created', function ($q) {

                return $q->created_by . '<br><small>' . $q->created_at . '</small><br>' . ($q->ju + 1) . ' x upload';
            })
            ->addColumn('updated', function ($q) {

                return $q->updated_by . '<br><small>' . $q->updated_at . '</small>';
            })
            ->addIndexColumn()
            ->rawColumns(['nilai_hkpd', 'created', 'updated'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function templateExcel(Request $request)
    {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        // cek sismiop/sistep
        $cs = DB::connection("oracle_satutujuh")->table("dat_znt")->selectRaw("case when regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end jenis ,count(kd_znt) jm")
            ->groupByRaw("case when regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end")
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")->pluck('jm', 'jenis')->toarray();

        // dd($cs);

        if (isset($cs['sismiop']) == true) {
            $wh = "and regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null ";
            $is_sistep = false;
        } else {
            $wh = "";
            $is_sistep = true;
        }

        $sql = "select dat_znt.kd_kecamatan,dat_znt.kd_kelurahan,dat_znt.kd_znt,nama_lokasi
        ,count(dat_op_bumi.kd_znt) objek
        from dat_znt 
        left join lokasi_objek on DAT_ZNT.LOKASI_OBJEK_ID=lokasi_objek.id
        left join dat_op_bumi on dat_op_bumi.kd_propinsi=dat_znt.kd_propinsi and 
dat_op_bumi.kd_dati2=dat_znt.kd_dati2 and 
dat_op_bumi.kd_kecamatan=dat_znt.kd_kecamatan and 
dat_op_bumi.kd_kelurahan=dat_znt.kd_kelurahan and 
dat_op_bumi.kd_znt=dat_znt.kd_znt 
        where dat_znt.kd_kecamatan='$kd_kecamatan' 
        and dat_znt.kd_kelurahan='$kd_kelurahan'
        " . $wh . "
        group by  dat_znt.kd_kecamatan,dat_znt.kd_kelurahan,dat_znt.kd_znt ,nama_lokasi
       order by dat_znt.kd_znt";
        //    dd($sql);
        $datanir = DB::connection("oracle_satutujuh")->select(db::raw($sql));
        $filename = "Template upload nir " . $kd_kecamatan . " " . $kd_kelurahan . ".xlsx";

        $array = [];
        foreach ($datanir as $row) {
            $array[] = [
                'kd_kecamatan' => $row->kd_kecamatan,
                'kd_kelurahan' => $row->kd_kelurahan,
                'kd_znt  ' => $row->kd_znt,
                'objek' => $row->objek,
                'nama_lokasi' => $row->nama_lokasi
            ];
        }
        return Excel::download(new template_upload_nir_excel($array), $filename);
    }

    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;

            // cek sismiop/sistep
            $cs = DB::connection("oracle_satutujuh")->table("dat_znt")->selectRaw("case when regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end jenis ,count(kd_znt) jm")
                ->groupByRaw("case when regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end")
                ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")->pluck('jm', 'jenis')->toArray();

            $ct = DB::connection("oracle_satutujuh")->table("dat_nir")
                ->selectRaw("max(thn_nir_znt) tahun_max")
                ->whereRaw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")
                ->first();

            $tahun_max = $ct->tahun_max ?? date('Y');
            $tl = "select kd_kecamatan,kd_kelurahan,kd_znt,nir nir_lama,nilai_hkpd nilai_hkpd_lama
            from dat_nir
            where kd_kecamatan='$kd_kecamatan'
            and kd_kelurahan='$kd_kelurahan' 
            and thn_nir_znt='$tahun_max'";

            if (isset($cs['sismiop']) == true) {
                $wh = "and regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') is null ";
                $is_sistep = false;
            } else {
                $wh = "";
                $is_sistep = true;
            }

            $sql = "select dat_znt.kd_znt,nama_lokasi,nvl(nir,nir_lama)  *1000 nilai_nir,nvl(nilai_hkpd,nilai_hkpd_lama) nilai_hkpd 
            from dat_znt 
            left join lokasi_objek on DAT_ZNT.LOKASI_OBJEK_ID=lokasi_objek.id
            left join dat_nir on dat_nir.kd_kecamatan=dat_znt.kd_kecamatan
                            and dat_nir.kd_kelurahan=dat_znt.kd_kelurahan
                            and dat_nir.kd_znt=dat_znt.kd_znt
                            and dat_nir.thn_nir_znt='$tahun'
            left join (" . $tl . ") tl on tl.kd_kecamatan=dat_znt.kd_kecamatan
            and tl.kd_kelurahan=dat_znt.kd_kelurahan
            and tl.kd_znt=dat_znt.kd_znt
            where dat_znt.kd_kecamatan='$kd_kecamatan' 
            and dat_znt.kd_kelurahan='$kd_kelurahan'
            " . $wh . "
           order by dat_znt.kd_znt";


            $datanir = DB::connection("oracle_satutujuh")->select(db::raw($sql));
            return view('data_nir/_formnir', compact('datanir', 'is_sistep'));
        }
        $kecamatan = Kecamatan::selectraw('kd_kecamatan,nm_kecamatan')->orderby('kd_kecamatan')->get();
        return view('data_nir/formnir', compact('kecamatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // return $request->all();

        $data = [];
        foreach ($request->nir as $znt => $nir) {
            $nn = onlyNumber($nir) == '' ? 0 : (onlyNumber($nir) / 1000);
            $data[] = [
                'thn_nir_znt' => $request->tahun,
                'kd_znt' => $znt,
                'nir' => $nn,
                'nilai_hkpd' => onlyNumber($request->nilai_hkpd[$znt]),
                'kd_propinsi' => '35',
                'kd_dati2' => '07',
                'kd_kecamatan' => $request->kd_kecamatan,
                'kd_kelurahan' => $request->kd_kelurahan,
                'jns_dokumen' => '1',
                'no_dokumen' => '00000000000',
                'kd_kanwil' => '01',
                'kd_kantor' => '01',
                'created_at' => Carbon::now(),
                'created_by' => Auth()->user()->id,
                'updated_at' => Carbon::now(),
                'updated_by' => Auth()->user()->id
            ];
        }


        // return $data;
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...

            foreach ($data as $row) {
                $cek = DB::connection("oracle_satutujuh")
                    ->table("dat_nir")->whereraw("thn_nir_znt='" . $row['thn_nir_znt'] . "' and kd_kecamatan='" . $row['kd_kecamatan'] . "' 
                        and kd_kelurahan='" . $row['kd_kelurahan'] . "' and kd_znt='" . $row['kd_znt'] . "'")
                    ->get()->count();
                if ($cek == 0) {
                    // insert
                    DB::connection("oracle_satutujuh")
                        ->table("dat_nir")->insert($row);
                } else {
                    //update
                    DB::connection("oracle_satutujuh")
                        ->table("dat_nir")->whereraw("thn_nir_znt='" . $row['thn_nir_znt'] . "' and kd_kecamatan='" . $row['kd_kecamatan'] . "' 
                        and kd_kelurahan='" . $row['kd_kelurahan'] . "' and kd_znt='" . $row['kd_znt'] . "'")
                        ->update($row);
                }
            }
            Db::connection("oracle_satutujuh")->commit();
            $msg = ['success' => "berhasil memproses data NIR"];
        } catch (\Throwable $th) {
            //throw $th;
            Db::connection("oracle_satutujuh")->rollBack();
            Log::info($th);
            $msg = ['error' => $th->getMessage()];
        }
        // return $data;    
        return redirect(route('refrensi.data_nir.index'))->with($msg);
    }

    public function copystore(Request $request)
    {
        $return = ["msg" => "Proses gagal.", "status" => false];
        DB::beginTransaction();
        try {
            $data = $request->only(['tahun_pajak', 'dari_tahun']);
            if ($data) {

                // proses
                DB::connection('oracle_satutujuh')->statement(db::raw("insert into dat_nir  (kd_propinsi,
                kd_dati2,
                kd_kecamatan,
                kd_kelurahan,
                kd_znt,
                thn_nir_znt,
                kd_kanwil,
                kd_kantor,
                jns_dokumen,
                no_dokumen,
                nir,
                created_by,
                created_at,nilai_hkpd)
                select a.* 
                from (
                select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_znt,'" . $data['tahun_pajak'] . "' thn_nir_znt,kd_kanwil,kd_kantor,jns_dokumen,to_char(sysdate,'yyyymmddhh24i')  no_dokumen,nir,'" . Auth()->user()->id . "' created_by,sysdate created_at,nilai_hkpd
                from dat_nir
                where thn_nir_znt='" . $data['dari_tahun'] . "'
                ) a 
                left join (
                select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_znt
                from dat_nir 
                where thn_nir_znt='" . $data['tahun_pajak'] . "') b on a.kd_propinsi=b.kd_propinsi and
                a.kd_dati2=b.kd_dati2 and
                a.kd_kecamatan=b.kd_kecamatan and
                a.kd_kelurahan=b.kd_kelurahan and
                a.kd_znt=b.kd_znt 
                where b.kd_propinsi is  null"));
                $return = ["msg" => "Proses Berhasil.", "status" => true];
            }
            DB::commit();
            $return = ["msg" => "Proses Berhasil.", "status" => true];
        } catch (\Exception $th) {
            DB::rollback();
            $return = ["msg" => $th->getMessage(), "status" => false];
        }
        return response()->json($return);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
