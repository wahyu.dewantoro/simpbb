<?php

namespace App\Http\Controllers;

use App\espptHistory;
use App\Helpers\Esppt;
use App\Helpers\gallade;
use App\Helpers\InformasiObjek;
use App\Kecamatan;
use App\Models\Layanan;
// use App\Helpers\InformasiObjek;
// use App\Http\Requests\pencarianSpptRequest;
use App\ObjekPajak;
use App\Sppt;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use DOMPDF as PDFF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use DataTables;

// use Jenssegers\Agent\Agent;

class SpptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('permission:view_sppt')->only(['index']);
        // $this->middleware('permission:add_desa_lunas')->only(['create', 'store','edit', 'update','destroy']);
        // $this->middleware('permission:edit_users')->only(['edit', 'update']);
        // $this->middleware('permission:delete_users')->only(['destroy']);
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $param['tahun'] = $request->tahun_pajak;
            $nop = $request->nop;
            $res = str_replace('.', '', $nop);
            $param['kd_propinsi'] = substr($res, 0, 2);
            $param['kd_dati2'] = substr($res, 2, 2);
            $param['kd_kecamatan'] = substr($res, 4, 3);
            $param['kd_kelurahan'] = substr($res, 7, 3);
            $param['kd_blok'] = substr($res, 10, 3);
            $param['no_urut'] = substr($res, 13, 4);
            $param['kd_jns_op'] = substr($res, 17, 1);
            $sppt = Sppt::nop($param)->first();
            $objek = ObjekPajak::nop($param)->first();
            if (empty($sppt)) {
                return "<p><b>Data tidak ditemukan</b></p>";
            }
            return view('sppt/_detail', compact('sppt', 'objek'));
        }
        return view('sppt/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sspd($nop)
    {
        $nop = onlyNumber($nop);

        $url =   config('app.url') . 'informasi/sspd/cetak/' . $nop;
        $qrcode = base64_encode(QrCode::format('svg')->generate($url));
        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kecamatan = substr($nop, 4, 3);
        $kd_kelurahan = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);
        $thn_pajak_sppt = substr($nop, 18, 4);

        $pembayaran = DB::connection("oracle_satutujuh")->table("pembayaran_sppt")
            ->leftjoin('dat_objek_pajak', function ($join) {
                $join->on('pembayaran_sppt.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('pembayaran_sppt.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('pembayaran_sppt.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('pembayaran_sppt.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('pembayaran_sppt.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('pembayaran_sppt.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('pembayaran_sppt.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->leftjoin('ref_kecamatan', function ($join) {
                $join->on('pembayaran_sppt.kd_propinsi', '=', 'ref_kecamatan.kd_propinsi')
                    ->on('pembayaran_sppt.kd_dati2', '=', 'ref_kecamatan.kd_dati2')
                    ->on('pembayaran_sppt.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
            })
            ->leftjoin('ref_kelurahan', function ($join) {
                $join->on('pembayaran_sppt.kd_propinsi', '=', 'ref_Kelurahan.kd_propinsi')
                    ->on('pembayaran_sppt.kd_dati2', '=', 'ref_Kelurahan.kd_dati2')
                    ->on('pembayaran_sppt.kd_kecamatan', '=', 'ref_Kelurahan.kd_kecamatan')
                    ->on('pembayaran_sppt.kd_kelurahan', '=', 'ref_Kelurahan.kd_kelurahan');
            })
            ->leftjoin('sppt', function ($join) {
                $join->on('pembayaran_sppt.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('pembayaran_sppt.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('pembayaran_sppt.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('pembayaran_sppt.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('pembayaran_sppt.kd_blok', '=', 'sppt.kd_blok')
                    ->on('pembayaran_sppt.no_urut', '=', 'sppt.no_urut')
                    ->on('pembayaran_sppt.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->on('pembayaran_sppt.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->leftjoin(DB::raw("sim_pbb.data_billing"), function ($join) {
                $join->on('pembayaran_sppt.pengesahan', '=', 'data_billing.pengesahan')
                    ->on('pembayaran_sppt.kode_bank', '=', 'data_billing.kode_bank');
            })
            ->leftjoin('ref_bank', 'ref_bank.kode_bank', '=', db::raw('nvl(pembayaran_sppt.kode_bank,114)'))
            ->whereraw(" pembayaran_sppt.kd_propinsi='$kd_propinsi' and pembayaran_sppt.kd_dati2='$kd_dati2' 
            and pembayaran_sppt.kd_kecamatan='$kd_kecamatan' and pembayaran_sppt.kd_kelurahan='$kd_kelurahan' 
            and pembayaran_sppt.kd_blok='$kd_blok' and pembayaran_sppt.no_urut='$no_urut' 
            and pembayaran_sppt.kd_jns_op='$kd_jns_op' and pembayaran_sppt.thn_pajak_sppt='$thn_pajak_sppt' ")
            ->select(db::raw("pembayaran_sppt.kd_propinsi||'.'||
            pembayaran_sppt.kd_dati2||'.'||
            pembayaran_sppt.kd_kecamatan||'.'||
            pembayaran_sppt.kd_kelurahan||'.'||
            pembayaran_sppt.kd_blok||'-'||
            pembayaran_sppt.no_urut||'.'||
            pembayaran_sppt.kd_jns_op nop,
            pembayaran_sppt.thn_pajak_sppt,nm_wp_sppt,
            jalan_op,blok_kav_no_op,nm_kecamatan,nm_kelurahan,tgl_pembayaran_sppt,jml_sppt_yg_dibayar,pembayaran_sppt.denda_sppt,pembayaran_sppt.pengesahan,nama_bank,
            kobil,tahun_pajak
            "))
            ->get();
        $pdf = PDFF::loadView('sppt/esspd', compact('pembayaran', 'qrcode'))->setPaper('a5', 'landscape');
        $filename = 'esspd' . time() . '.pdf';
        //return $pdf->download($filename); // directdownload
        return $pdf->stream($filename);  // directstream
    }


    public function show(Request $request, $nop)
    {
        switch ($request->jenis) {
            case '0':
                $jenis = 0;
                break;
            default:
                # code...
                $jenis = 1;
                break;
        }

        return Esppt::generate($nop, 'stream', $jenis);
    }


    public function show_tanda_terima($nop)
    {
        $param['kd_propinsi'] = substr($nop, 0, 2);
        $param['kd_dati2'] = substr($nop, 2, 2);
        $param['kd_kecamatan'] = substr($nop, 4, 3);
        $param['kd_kelurahan'] = substr($nop, 7, 3);
        $param['kd_blok'] = substr($nop, 10, 3);
        $param['no_urut'] = substr($nop, 13, 4);
        $param['kd_jns_op'] = substr($nop, 17, 1);
        $param['thn_pajak_sppt'] = substr($nop, 18, 4);
        $riwayat = InformasiObjek::riwayatPembayaran($param)
            ->whereraw("trunc(tgl_jatuh_tempo_sppt)<trunc(sysdate)")
            ->orderby('sppt.thn_pajak_sppt')
            ->get();
        $uri = env("APP_URL", "localhost") . 'informasi/sppt/cetak_tanda_terima';
        // return view('sppt._cetak_pembayaran',compact('riwayat','uri'));
        $pdf = PDFF::loadView('sppt/_cetak_pembayaran', compact('riwayat', 'uri'))->setPaper('a4', 'portrait');
        $filename = 'tanda_terima_pembayaran' . time() . '.pdf';
        //return $pdf->download($filename); // directdownload
        return $pdf->stream($filename);  // directstream
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function prosePencarian(Request $request)
    {

        $rules = ['captcha' => 'required|captcha', 'nop' => 'required', 'nik' => 'required'];

        //d$request->validate($rules, ['captcha.captcha' => 'captcha invalid']);
        // dd($request->all());
        $sppt = null;
        $objek = null;
        $riwayat = null;
        $nop = null;
        $status_pencarian = 0;

        $rpc = str_replace(' ', '', $request->nop);
        $nop = null;
        $nik = $request->nik ?? null;
        $keterangan = "";
        if (!empty($request->nop)) {
            $lunas_tahun_kemarin = "";
            $objek = false;
            $data = null;
            preg_match_all('!\d+!', $rpc, $nop);
            $nop = $nop[0];
            $tahun = date('Y');

            $param['tahun'] = $tahun - 1;
            $param['kd_propinsi'] = $nop[0] ?? null;
            $param['kd_dati2'] = $nop[1] ?? null;
            $param['kd_kecamatan'] = $nop[2] ?? null;
            $param['kd_kelurahan'] = $nop[3] ?? null;
            $param['kd_blok'] = $nop[4] ?? null;
            $param['no_urut'] = $nop[5] ?? null;
            $param['kd_jns_op'] = $nop[6] ?? null;
            // cek sppt tahun sebelum nya 
            $cs = Sppt::nop($param)->first();
            if ($cs) {
                if ($cs->status_pembayaran_sppt == '1') {
                    $lunas_tahun_kemarin = 1;
                } else {
                    $lunas_tahun_kemarin = 0;
                }
            }
            // cek datau
            $sppt = DB::connection('oracle_dua')->table(DB::raw('sppt'))
                ->join(DB::raw('dat_objek_pajak'), function ($join) {
                    $join->on(DB::raw('sppt.kd_propinsi'), '=', DB::raw('dat_objek_pajak.kd_propinsi'))
                        ->on(DB::raw('sppt.kd_dati2'), '=', DB::raw('dat_objek_pajak.kd_dati2'))
                        ->on(DB::raw('sppt.kd_kecamatan'), '=', DB::raw('dat_objek_pajak.kd_kecamatan'))
                        ->on(DB::raw('sppt.kd_kelurahan'), '=', DB::raw('dat_objek_pajak.kd_kelurahan'))
                        ->on(DB::raw('sppt.kd_blok'), '=', DB::raw('dat_objek_pajak.kd_blok'))
                        ->on(DB::raw('sppt.no_urut'), '=', DB::raw('dat_objek_pajak.no_urut'))
                        ->on(DB::raw('sppt.kd_jns_op'), '=', DB::raw('dat_objek_pajak.kd_jns_op'));
                })->select(DB::raw('subjek_pajak_id,SPPT.*'))
                ->whereraw(DB::raw("sppt.kd_propinsi='" . $param['kd_propinsi'] . "' and 
                sppt.kd_dati2='" . $param['kd_dati2'] . "' and 
                sppt.kd_kecamatan='" . $param['kd_kecamatan'] . "' and 
                sppt.kd_kelurahan='" . $param['kd_kelurahan'] . "' and 
                sppt.kd_blok='" . $param['kd_blok'] . "' and 
                sppt.no_urut='" . $param['no_urut'] . "' and 
                sppt.kd_jns_op='" . $param['kd_jns_op'] . "' and  thn_pajak_sppt='$tahun'"))->first();

            // dd($sppt);

            $arr = [];
            if ($sppt <> null) {
                $objek = true;
                $status_pencarian = 1;
                $data = $sppt;
                $arr = ['-'];
                if ($nik <> trim($sppt->subjek_pajak_id)) {
                    // status 2 untuk data nik tidak sama
                    $status_pencarian = 2;
                    $detail = '';
                    $arr = [
                        "Data subjek pajak yang terekam di database tidak sesuai dengan yang anda inputkan.",
                        "Apabila data yang di masukan merupakan sudah benar sesuai dengan data objek, silahkan ajukan pembetulan data melalui <a href='#'>Form pembetulan online</a>"
                    ];
                } else {
                    $detail = $this->getNOPWP($nop[0] . '.' . $nop[1] . '.' . $nop[2] . '.' . $nop[3] . '.' . $nop[4] . '.' . $nop[5] . '.' . $nop[6]);
                }
            }
            if (empty($sppt)) {
                $arr = [
                    "Data objek <b>TIDAK TERSEDIA</b> di dalam database BAPENDA",
                    "Apabila data yang di masukan merupakan sudah benar sesuai dengan data objek, silahkan mengajukan permohonan di kantor BAPENDA"
                ];
            }
            // $arr=[];
            // $status_pencarian=1;
            $layanan_history=$this->layananHistory($param);
            // dd($layanan_history->toArray());
            $response = [
                'objek' => $objek,
                'lunas_tahun_kemarin' => $lunas_tahun_kemarin,
                'data' => $data,
                'keterangan' => $arr,
                'status' => $status_pencarian,
                'detail' => $detail ?? '',
                'layanan' => $layanan_history,
            ];
            // save history
            $history = [
                'nik' => $request->nik ?? date("YmdHis"),
                'nama' => $request->nama,
                'telepon' => time(),
                'nop' => implode('', $nop),
                'status' => $status_pencarian,
                'keterangan' => implode(', ', $arr),
                'ip' => $request->ip()
            ];
            // dd($history);
            espptHistory::create($history);
        } else {
            $response = [
                'objek' => false,
                'lunas_tahun_kemarin' => false,
                'data' => false,
                'status' => $status_pencarian
            ];
        }
        return view('sppt/_pencarian', compact('response'));
    }

    private static function layananHistory($param){
        $group=['layanan.nomor_layanan',
                'layanan.jenis_layanan_id',
                'layanan.jenis_layanan_nama',
                'layanan.nama',
                'layanan.keterangan', 
                'layanan.created_at', 
                'layanan.kd_status',
                'layanan.jenis_objek',
                'jenis_pengurangan.jenis',
                'jenis_pengurangan.nama_pengurangan'
            ];
            $selectLayanan=array_merge($group,[db::raw("sum(CASE WHEN layanan_objek.pemutakhiran_at IS NULL THEN 0 ELSE 1 END) pemutakhiran_count,
            sum(CASE WHEN layanan_objek.is_tolak = '1' THEN 1 ELSE 0 END) tolak_count,
            sum(CASE WHEN layanan_objek.id IS NULL THEN 0 ELSE 1 END) objek_count,
            sum(CASE WHEN layanan_objek.id IS NOT NULL AND layanan.jenis_layanan_id ='6' THEN 1 ELSE 0 END) pecah_count,
            sum(CASE WHEN layanan_objek.id IS NOT NULL AND layanan.jenis_layanan_id ='7' and layanan_objek.nop_gabung is null  THEN 1 ELSE 0 END) gabung_count")]);
            
            $layanan=Layanan::select($selectLayanan);
            $strWhereLayanan="(layanan_objek.kd_propinsi='".$param['kd_propinsi']."'
                AND layanan_objek.kd_dati2='".$param['kd_dati2']."'
                AND layanan_objek.kd_kecamatan='".$param['kd_kecamatan']."'
                AND layanan_objek.kd_kelurahan='".$param['kd_kelurahan']."'
                AND layanan_objek.kd_blok='".$param['kd_blok']."'
                AND layanan_objek.no_urut='".$param['no_urut']."'
                AND layanan_objek.kd_jns_op='".$param['kd_jns_op']."')";
            $layanan->whereRaw($strWhereLayanan);
            $layanan_history=$layanan->leftJoin('layanan_objek',function($join){
                    $join->On('layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan');
                })
                ->join('jenis_pengurangan','jenis_pengurangan.id',"=","layanan_objek.jenis_pengurangan",'left outer')
                ->whereRaw("Layanan.jenis_objek!='4'")
                ->where('layanan.jenis_layanan_id','8')
                ->whereRaw("layanan.jenis_layanan_nama like '%(online)%'")
                ->orderBy('Layanan.created_at', 'desc')
                ->groupBy($group)
                ->get();
            $layanan_history->transform(function($res){
                $res->option=gallade::anchorInfo('laporan_input/tanda_terima?nomor_layanan=' . $res->nomor_layanan, 
                '<i class="fas fa-file-pdf"></i>', 'title="Cetak Tanda Terima" data-pdf="true"','','primary');
                $his=date('H:i:s', strtotime($res->created_at));
                $res->created_at='<p class="p-0 m-0">'.tglIndo($res->created_at).'</p><i>'.$his.'</i>';
                if($res->jenis_layanan_id=='5'){
                    $res->jenis_layanan_nama=$res->jenis_layanan_nama." [".$res->nama_pengurangan."]";
                }
                $listObjek=['1'=>'Pribadi','2'=>'Badan','3'=>'Kolektif','4'=>'Pendataaan','5'=>'Permohonan BPHTB'];
                $jenisObjek='--';
                if(in_array($res->jenis_objek,array_keys($listObjek))){
                    $jenisObjek=$listObjek[$res->jenis_objek];
                }
                $res->nomor_layanan='<p class="p-0 m-0">'.$res->nomor_layanan.'</p><i class="text-info">'.$jenisObjek.'</i>';
                $countData=$res->objek_count;
                if($res->jenis_layanan_id=='6'){
                    $countData=$res->pecah_count;
                }
                if($res->jenis_layanan_id=='7'){
                    $countData=$res->gabung_count;
                }
                if($res->pemutakhiran_count>=$countData&&$res->kd_status!='1'&&$res->jenis_layanan_id!='8'){
                    $res->kd_status='1';
                }
                $status=[
                    // '0'=>"<span class='badge badge-warning '>Verifikai</span>",
                    '0'=>"<span class='badge badge-warning '>Penelitian</span>",
                    '1'=>"<span class='badge badge-success '>Selesai</span>",
                    '2'=>"<span class='badge badge-warning '>Berkas belum Masuk</span>",
                    '3'=>"<span class='badge badge-danger '>Berkas ditolak </span>",
                    // '3'=>"<span class='badge badge-primary '>Penelitian</span>",
                    // '3'=>"<span class='badge badge-primary '>Selesai</span>",
                ];
                $setstatus="<span class='badge badge-warning '>--</span>";
                if(in_array($res->kd_status,array_keys($status))){
                    $setstatus=$status[$res->kd_status];
                }
                if($res->tolak_count>0){
                    if($res->jenis_objek=='3'){
                        $setstatus.="<span class='badge badge-danger '>[".$res->tolak_count."] Ajuan di tolak</span>";
                    }else{
                        $setstatus="<span class='badge badge-danger '> Ajuan di tolak</span>";
                    }
                }
                $res->kd_status=$setstatus;
                return $res;
            });
        return $layanan_history;
    }
    public function pencarianSppt(Request $request)
    {



        $sppt = null;
        $objek = null;
        $riwayat = null;
        $nop = null;
        $status_pencarian = 0;



        if ($request->ajax()) {
            $rules = ['captcha' => 'required|captcha'];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // return redirect(url('page-sppt'))->whith(['error'=>'Captcha yang di entrikan tidak sesuai']);
                return redirect(url('page-sppt'))->with(['error' => 'Captcha yang di entrikan tidak sesuai']);
                // 
            }


            $rpc = str_replace(' ', '', $request->nop);
            $nop = null;
            $nik = $request->nik ?? null;
            $keterangan = "";
            if (!empty($request->nop)) {
                $lunas_tahun_kemarin = "";
                $objek = false;
                $data = null;
                preg_match_all('!\d+!', $rpc, $nop);
                $nop = $nop[0];
                $tahun = date('Y');

                $param['tahun'] = $tahun - 1;
                $param['kd_propinsi'] = $nop[0];
                $param['kd_dati2'] = $nop[1];
                $param['kd_kecamatan'] = $nop[2];
                $param['kd_kelurahan'] = $nop[3];
                $param['kd_blok'] = $nop[4];
                $param['no_urut'] = $nop[5];
                $param['kd_jns_op'] = $nop[6];
                // cek sppt tahun sebelum nya 
                $cs = Sppt::nop($param)->first();
                if ($cs) {
                    if ($cs->status_pembayaran_sppt == '1') {
                        $lunas_tahun_kemarin = 1;
                    } else {
                        $lunas_tahun_kemarin = 0;
                    }
                }

                // cek datau
                $sppt = DB::connection('oracle_dua')->table(DB::raw('sppt'))
                    ->join(DB::raw('dat_objek_pajak'), function ($join) {
                        $join->on(DB::raw('sppt.kd_propinsi'), '=', DB::raw('dat_objek_pajak.kd_propinsi'))
                            ->on(DB::raw('sppt.kd_dati2'), '=', DB::raw('dat_objek_pajak.kd_dati2'))
                            ->on(DB::raw('sppt.kd_kecamatan'), '=', DB::raw('dat_objek_pajak.kd_kecamatan'))
                            ->on(DB::raw('sppt.kd_kelurahan'), '=', DB::raw('dat_objek_pajak.kd_kelurahan'))
                            ->on(DB::raw('sppt.kd_blok'), '=', DB::raw('dat_objek_pajak.kd_blok'))
                            ->on(DB::raw('sppt.no_urut'), '=', DB::raw('dat_objek_pajak.no_urut'))
                            ->on(DB::raw('sppt.kd_jns_op'), '=', DB::raw('dat_objek_pajak.kd_jns_op'));
                    })->select(DB::raw('subjek_pajak_id,SPPT.*'))
                    ->whereraw(DB::raw("sppt.kd_propinsi='" . $param['kd_propinsi'] . "' and 
                    sppt.kd_dati2='" . $param['kd_dati2'] . "' and 
                    sppt.kd_kecamatan='" . $param['kd_kecamatan'] . "' and 
                    sppt.kd_kelurahan='" . $param['kd_kelurahan'] . "' and 
                    sppt.kd_blok='" . $param['kd_blok'] . "' and 
                    sppt.no_urut='" . $param['no_urut'] . "' and 
                    sppt.kd_jns_op='" . $param['kd_jns_op'] . "' and  thn_pajak_sppt='$tahun'"))->first();

                // dd($sppt);

                $arr = [];
                if ($sppt <> null) {
                    $objek = true;
                    $status_pencarian = 1;
                    $data = $sppt;
                    if (trim($nik)  <> trim($sppt->subjek_pajak_id)) {
                        // status 2 untuk data nik tidak sama
                        $status_pencarian = 2;
                        $arr[] = "Data subjek pajak yang terekam di database tidak sesuai dengan yang anda inputkan.";
                        $arr[] = "Apabila data yang di masukan merupakan sudah benar sesuai dengan data objek, bisa mendapatakan E-SPPT melalui kantor Desa atau  silahkan ajukan pembetulan data melalui <a href='#'>Form pembetulan online</a>";
                    }
                }

                if (empty($sppt)) {
                    $arr[] = "Data objek <b>TIDAK TERSEDIA</b> di dalam database BAPENDA";
                    $arr[] = "Apabila data yang di masukan merupakan sudah benar sesuai dengan data objek, silahkan mengajukan permohonan di kantor BAPENDA!";
                }

                $response = [
                    'objek' => $objek,
                    'lunas_tahun_kemarin' => $lunas_tahun_kemarin,
                    'data' => $data,
                    'keterangan' => $arr,
                    'status' => $status_pencarian
                ];

                /*   $status = 1;
                if (!empty($arr)) {
                    $status = 0;
                } */

                // save history
                $history = [
                    'nik' => $request->nik ?? date("YmdHis"),
                    'nama' => $request->nama,
                    'telepon' => $request->telepon,
                    'nop' => implode('', $nop),
                    'status' => $status_pencarian,
                    'keterangan' => implode(', ', $arr),
                    'ip' => $request->ip()
                ];

                espptHistory::create($history);
            } else {
                $response = [
                    'objek' => false,
                    'lunas_tahun_kemarin' => false,
                    'data' => false,
                    'status' => $status_pencarian
                ];
            }
            return view('sppt/_pencarian', compact('response'));
        }


        $cari = [
            'E - SPPT',
            'Riwayat Pembayaran'
        ];
        return view("sppt/pencarian_new", compact('sppt', 'objek', 'cari', 'riwayat', 'nop'));
    }

    private function getNOPWP($nop = false)
    {
        if (!$nop) {
            return false;
        }
        $res = str_replace('.', '', $nop);
        $variable['kd_propinsi'] = substr($res, 0, 2);
        $variable['kd_dati2'] = substr($res, 2, 2);
        $variable['kd_kecamatan'] = substr($res, 4, 3);
        $variable['kd_kelurahan'] = substr($res, 7, 3);
        $variable['kd_blok'] = substr($res, 10, 3);
        $variable['no_urut'] = substr($res, 13, 4);
        $variable['kd_jns_op'] = substr($res, 17, 1);

        $tahun_nota = [];
        $data = [];
        try {
            //code...

            $objek = DB::connection('oracle_dua')->table(db::raw(" dat_objek_pajak"))
                ->select('no_formulir_spop')
                ->whereraw("kd_propinsi ='" . $variable['kd_propinsi'] . "'")
                ->whereraw("kd_dati2 ='" . $variable['kd_dati2'] . "'")
                ->whereraw("kd_kecamatan ='" . $variable['kd_kecamatan'] . "'")
                ->whereraw("kd_kelurahan ='" . $variable['kd_kelurahan'] . "'")
                ->whereraw("kd_blok ='" . $variable['kd_blok'] . "'")
                ->whereraw("no_urut ='" . $variable['no_urut'] . "'")
                ->whereraw("kd_jns_op ='" . $variable['kd_jns_op'] . "'")
                ->first();
            if ($objek) {
                //cek kobil bayar
                $tahun_nota = DB::table(db::raw('data_billing a'))
                    ->join(db::raw("billing_kolektif b"), 'a.data_billing_id', '=', 'b.data_billing_id')
                    ->join(db::raw("(select distinct data_billing_id
                            from sppt_penelitian
                            where nomor_formulir='" . trim($objek->no_formulir_spop) . "') c"), 'c.data_billing_id', '=', 'b.data_billing_id')
                    ->whereraw("a.kd_status='0'")->selectraw("distinct b.tahun_pajak")->pluck('tahun_pajak')->toArray();
            }
            $variable['tahun_range'] = Carbon::now()->year - 2;
            $data = InformasiObjek::riwayatPembayaran($variable)->orderby('sppt.thn_pajak_sppt')->get();
        } catch (Exception $e) {
            Log::error($e);
            // $data = $e->getMessage();
        }
        return view('tracking_pembayaran/_detail_tt', compact('data', 'nop', 'tahun_nota'));
    }

    public function FormCetak(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        return view('penetapan.formCetak', compact('kecamatan'));
    }


    public function generateSppt(Request $request)
    {

        // return $request->all();

        $kd_kecamatan = $request->kd_kecamatan ?? '';
        $kd_kelurahan = $request->kd_kelurahan ?? '';
        $kd_blok = $request->kd_blok ?? '';
        $no_urut = padding(($request->no_urut ?? ''), 0, 4);
        $tahun = $request->tahun_pajak;

        $param = [
            'buku' => $request->buku,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut <> '0000' ? $no_urut : '',
            'thn_pajak_sppt' => $tahun
        ];

        $filename = Esppt::generateMulti($param);
        $resfile = url('preview-esppt') . '?nama_file=' . $filename;

        return "<iframe  src='" . $resfile . "' width='100%' height='650px' ></iframe>";
    }
}
