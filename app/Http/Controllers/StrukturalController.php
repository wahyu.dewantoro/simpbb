<?php

namespace App\Http\Controllers;

use App\Http\Requests\strukturalRequest;
use App\pegawaiStruktural;
use Exception;
use Illuminate\Http\Request;

class StrukturalController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view_struktural')->only(['index']);
        /* $this->middleware('permission:add_users')->only(['create', 'store']);
        $this->middleware('permission:edit_users')->only(['edit', 'update']);
        $this->middleware('permission:delete_users')->only(['destroy']); */
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pegawai = pegawaiStruktural::orderby('kode')->get();
        return view('struktural/index', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'action' => url('refrensi/struktural'),
            'method' => 'post'
        ];
        return view('struktural.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(strukturalRequest $request)
    {
        //
        try {
            $data = [

                "kode" => $request->kode,
                "jabatan" => $request->jabatan,
                "is_plt" => $request->is_plt,
                "nama" => $request->nama,
                "nip" => $request->nip,
            ];
            pegawaiStruktural::create($data);
            $flash = [
                'success' => 'Berhasil di tambahkan'
            ];
        } catch (Exception $e) {
            //throw $th;
            $flash = [
                'error' => $e->getMessage()
            ];
        }

        return   redirect('refrensi/struktural')->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pegawai = pegawaiStruktural::findorfail($id);
        $data = [
            'action' => route('refrensi.struktural.update', $id),
            'method' => 'PATCH',
            'struktural' => $pegawai
        ];
        return view('struktural.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(strukturalRequest $request, $id)
    {
        //
        try {
            $data = [
                "kode" => $request->kode,
                "jabatan" => $request->jabatan,
                "is_plt" => $request->is_plt,
                "nama" => $request->nama,
                "nip" => $request->nip,
                "pangkat" => $request->pangkat
            ];
            $peg = pegawaiStruktural::findorfail($id);
            $peg->update($data);
            $flash = [
                'success' => 'Berhasil di update'
            ];
        } catch (Exception $e) {
            //throw $th;
            $flash = [
                'error' => $e->getMessage()
            ];
        }

        return   redirect('refrensi/struktural')->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            pegawaiStruktural::findorfail($id)->delete();
            $flash = [
                'success' => 'Berhasil di update'
            ];
        } catch (Exception $e) {
            //throw $th;
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        return   redirect('refrensi/struktural')->with($flash);
    }
}
