<?php

namespace App\Http\Controllers;

use App\KelolaUsulan;
use App\KelolaUsulanObjek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KelolaObjekVerifikasi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KelolaUsulan::Verifying()->paginate();
        return view('kelola.verifikasi.index_objek', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = KelolaUsulan::find($id);
        // return $data;
        $jn = DB::table('kelola_usulan')->join('kelola_usulan_objek', 'kelola_usulan.id', '=', 'kelola_usulan_objek.kelola_usulan_id')
            ->select(db::raw("count(1) jumlah"))
            ->whereraw("kelola_usulan_objek.verifikasi_kode='1' and kelola_usulan.created_by='" . $data->Pemohon->id . "'")
            ->first();


        $jum_op = $jn->jumlah;

        $perusahaan = $data->Pemohon->UserPengelola()->first() ?? [];
        // return $perusahaan;
        $rayon = $data->Pemohon->UserRayon->first() ?? [];

        return view('kelola.form-verifikasi', compact('data', 'perusahaan', 'rayon', 'jum_op'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // return $request->all();
        DB::beginTransaction();
        try {

            $kode = '1';
            $keterangan = "telah di verifikasi oleh petugas";
            $acc = $request->id_nop;
            $obj = KelolaUsulanObjek::where('kelola_usulan_id', $id)->pluck('id')->toarray();

            foreach ($obj as $i) {
                $kode_v = $acc[$i] ?? '0';
                $o = KelolaUsulanObjek::find($i);
                $o->update([
                    'verifikasi_kode' => $kode_v,
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_at' => now()
                ]);
            }
            //code...
            $ku = KelolaUsulan::find($id);
            $ku->update(['verifikasi_kode' => $kode, 'keterangan_verifikasi' => $keterangan, 'verifikasi_by' => auth()->user()->id, 'verifikasi_at' => now()]);

            $telepon = $ku->user->telepon ?? '';
            $nama = $ku->user->nama ?? 'Wajib Pajak';
            // $user=User::
            DB::commit();
            $flash = [
                'success' => 'Berhasil verifikasi'
            ];

            // if ($kode == '1') {
            //     $this->NotifAcc($telepon, [$nama, '*USULAN PENGELOLAAN NOP*']);
            // } else {
            //     // di tolak
            //     $this->NotifReject($telepon, [$nama, '*USULAN PENGELOLAAN NOP*', $keterangan ?? "Dokumen tidak sesuai"]);
            // }
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(route('kelola-objek-verifikasi.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
