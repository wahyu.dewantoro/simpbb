<?php

namespace App\Http\Controllers;

use App\Formulir;
use App\Helpers\Pajak;
use App\Kelurahan;
use App\Models\Layanan_objek;
use App\PenelitianKantor;
use App\PenelitianLapangan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;
use Illuminate\Support\Facades\View;
use SnappyPdf as PDF;

class spopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function cetakSpop($formulir)
    {
        // return $formulir;
        $objek = DB::table('tbl_spop')->where('no_formulir', $formulir)->first();
        $row = $objek->nop_proses;
        $kd_propinsi = substr($row, 0, 2);
        $kd_dati2 = substr($row, 2, 2);
        $kd_kecamatan = substr($row, 4, 3);
        $kd_kelurahan = substr($row, 7, 3);
        $kd_blok = substr($row, 10, 3);
        $no_urut = substr($row, 13, 4);
        $kd_jns_op = substr($row, 17, 1);
        // dd(storage_path());
        $kelurahan=Kelurahan::where('kd_kecamatan',$kd_kecamatan)->where('kd_kelurahan',$kd_kelurahan)->first();

        
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(public_path('spop.docx'));

        $templateProcessor->setValues([
            'no_formulir' => $objek->no_formulir,
            'layanan_objek_id' => $objek->layanan_objek_id,
            'jenis_layanan_id' => $objek->jenis_layanan_id,
            'jns_transaksi' => jenisTransaksiTanah($objek->jns_transaksi),
            'nop_proses' => formatnop($objek->nop_proses),
            'nop_bersama' => $objek->nop_bersama,
            'nop_asal' => $objek->nop_asal <> '' ? formatnop($objek->nop_asal) : null,
            'subjek_pajak_id' => $objek->subjek_pajak_id,
            'nm_wp' => $objek->nm_wp,
            'jalan_wp' => $objek->jalan_wp,
            'blok_kav_no_wp' => $objek->blok_kav_no_wp,
            'rw_wp' => $objek->rw_wp,
            'rt_wp' => $objek->rt_wp,
            'kelurahan_wp' => $objek->kelurahan_wp,
            'kota_wp' => $objek->kota_wp,
            'kd_pos_wp' => $objek->kd_pos_wp,
            'telp_wp' => $objek->telp_wp,
            'npwp' => $objek->npwp,
            'status_pekerjaan_wp' => jenisPekerjaan($objek->status_pekerjaan_wp),
            'no_persil' => $objek->no_persil,
            'jalan_op' => $objek->jalan_op,
            'blok_kav_no_op' => $objek->blok_kav_no_op,
            'kelurahan_op'=>$kelurahan->nm_kelurahan,
            'rw_op' => $objek->rw_op,
            'rt_op' => $objek->rt_op,
            'kd_status_cabang' => $objek->kd_status_cabang,
            'status_wp' => statusWP($objek->kd_status_wp),
            'kd_znt' => $objek->kd_znt,
            'luas_bumi' => $objek->luas_bumi,
            'jns_bumi' => jenisBumi($objek->jns_bumi),
            'tgl_pendataan_op' => $objek->tgl_pendataan_op,
            'nip_pendata' => $objek->nip_pendata,
            'tgl_pemeriksaan_op' => $objek->tgl_pemeriksaan_op,
            'nip_pemeriksa_op' => $objek->nip_pemeriksa_op,
            'tgl_perekaman_op' => $objek->tgl_perekaman_op,
            'nip_perekam_op' => $objek->nip_perekam_op,
        ]);

        header("Content-Disposition: attachment; filename=spop_" . $formulir . ".docx");

        $templateProcessor->saveAs('php://output');

        /* // return view('spop._pdf', compact('objek'));
        $pages[] = View::make('spop._pdf', compact('objek'));

        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        // ->setOption('margin-left', '20')
        // ->setOption('margin-right', '20')
        // ->setOption('margin-top', '10')
        // ->setOption('margin-bottom', '10');
        $filename = time() . ' tagihan ' . '.pdf';
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream($filename); */
    }

    public function index()
    {
        // return view('spop.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
