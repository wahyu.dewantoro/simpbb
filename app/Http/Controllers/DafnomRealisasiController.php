<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kecamatan;
use App\Models\Data_billing;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Helpers\gallade;

class DafnomRealisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan','asc')->get();
        $kelurahan = [];
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start=$tahun-5;
        for($year= $tahun;$year>=$start;$year--){
            $tahun_pajak[$year]=$year;
        }
        $result = ['data','data'];
        return view('dafnom.realisasi', compact('result','kecamatan','tahun_pajak'));
    }
    public function search(Request $request){
        $result=response()->json(['status'=>false,'msg'=>"Data tidak ditemukan."]);
        $request=$request->only(['kecamatan','kelurahan','tahun_pajak']);
        $where['data_billing.kd_kecamatan']=$request['kecamatan'];
        $where['data_billing.tahun_pajak']=$request['tahun_pajak'];
        if($request['kelurahan']&&$request['kelurahan']!="-"){
            $where['data_billing.kd_kelurahan']=$request['kelurahan'];
        }
        $limit=15;
        $page=0;
        $select=['data_billing.data_billing_id',
                'data_billing.kd_propinsi',
                'data_billing.kd_dati2',
                'data_billing.kd_kecamatan',
                'data_billing.kd_kelurahan',
                'data_billing.kd_blok',
                'data_billing.no_urut',
                'data_billing.kd_jns_op',
                'data_billing.kobil',
                'data_billing.nama_wp',
                'data_billing.nama_kecamatan',
                'data_billing.nama_kelurahan',
                'data_billing.nama_kecamatan',
                'data_billing.nama_kecamatan',
                'data_billing.nama_kecamatan',
                'data_billing.nama_kecamatan',
                'data_billing.created_at',
                'data_billing.kd_status'];
        $getData=Data_billing::where($where)
                ->select($select)
                ->orderBy('data_billing.created_at','desc')
                ->skip($page)->take($limit)
                ->get();
        if($getData->count()){
            $setData=[];
            $list=['no','kobil','nama_wp','nama_kecamatan','nama_kelurahan','created_at','kd_status','aksi'];
            $no=1;
            $result=[];
            foreach($getData->toArray() as $item){
                $result=[
                    'no'=>'',
                    'kobil'=>$item['kobil'],
                    'nama_wp'=>$item['nama_wp'],
                    'nama_kecamatan'=>$item['nama_kecamatan'],
                    'nama_kelurahan'=>$item['nama_kelurahan'],
                    'created_at'=>$item['created_at'],
                    'kd_status'=>($item['kd_status'])?"<span class='label label-success'>Lunas</span>":"<span class='label label-warning'>Belum Lunas</span>",
                    'aksi'=>['class'=>'text-center',
                        'data'=>'<div class="btn-group">
                        <button type="button" class="btn btn-default" title="Cetak Excel"><i class="fas fa-file-excel"></i></button>
                        <button type="button" class="btn btn-default"  title="Cetak Pdf"><i class="fas fa-file-pdf"></i></button>
                        <button type="button" class="btn btn-default"  title="Detail Dafnom" ><i class="fas fa-file-alt"></i></button>
                      </div>']
                ];
                $setData[]=Arr::only($result, $list);
            }
            $setData=gallade::generateinTbody($setData,"Data Masih Kosong");
            $return=response()->json(["msg"=>"Pencarian data berhasil.","status"=>true,'data'=>$setData]);
        }
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
