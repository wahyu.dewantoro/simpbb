<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Helpers\gallade;
use App\Models\Data_billing;
use App\Models\Billing_kolektif;
use Facade\FlareClient\Http\Response;
use DOMPDF as PDF;
use App\Helpers\Dafnom;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Jobs\GenerateDafnom;
use Yajra\DataTables\Facades\DataTables;

class DataDafnomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start = 2013; //$tahun - 5;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }
        $result = ['data', 'data'];
        return view('dafnom.data_dafnom', compact('result', 'kecamatan', 'tahun_pajak', 'kelurahan'));
    }
    public function search(Request $request)
    {
        $return = ['status' => false, 'msg' => "Data tidak ditemukan.", 'data' => [], 'extra' => ["nopnonkolektif" => 0, 'nopkolektif' => 0]];


        $kd_jns_op = $request->kd_jns_op ?? '1';
        if ($kd_jns_op=='1') {
            $request = $request->only(['kecamatan', 'kelurahan', 'tahun_pajak']);
            $where['data_billing.kd_kecamatan'] = $request['kecamatan'] ?? '';
            $where['data_billing.tahun_pajak'] = $request['tahun_pajak'] ?? '';
            if ($request['kelurahan'] && $request['kelurahan'] != "-") {
                $where['data_billing.kd_kelurahan'] = $request['kelurahan'];
            }
            // $limit=100;
            // $page=0;


            $group = [
                'data_billing.data_billing_id',
                'data_billing.kd_propinsi',
                'data_billing.kd_dati2',
                'data_billing.kd_kecamatan',
                'data_billing.kd_kelurahan',
                'data_billing.kd_blok',
                'data_billing.no_urut',
                'data_billing.kd_jns_op',
                'data_billing.nama_wp',
                'data_billing.nama_kecamatan',
                'data_billing.nama_kelurahan',
                'data_billing.created_at',
                'data_billing.kd_status',
                'data_billing.expired_at'
            ];
            $select = array_merge($group, [
                DB::raw("sum(billing_kolektif.pbb) pbb,
                    sum(COALESCE(billing_kolektif.denda,0)) denda,
                    count(billing_kolektif.data_billing_id) total_nop,
                    CASE WHEN data_billing.expired_at <sysdate then '1' else '0' end kd_expired
                    ")
            ]);
            $getData = Data_billing::where($where)
                ->whereRaw(DB::raw('lhp is null and data_billing.kd_jns_op='.$kd_jns_op.''))
                ->select($select)
                ->orderBy('data_billing.created_at', 'desc')
                ->join('billing_kolektif', 'billing_kolektif.data_billing_id', '=', 'data_billing.data_billing_id')
                ->groupBy($group)
                // ->skip($page)->take($limit)
                ->get();
            if ($getData->count()) {
                $setData = [];
                $list = ['no', 'kobil', 'nama_wp', 'pbb', 'nama_kecamatan', 'nama_kelurahan', 'created_at', 'kd_status', 'aksi'];
                $no = 1;
                $result = [];

                $cekUser = auth()->user();
                // dd([!$cekUser->hasRole('Desa'),!$cekUser->hasRole('UNIT PELAKSANA TEKNIS (UPT) PPD')]);
                foreach ($getData->toArray() as $item) {
                    $btnDelte = "";
                    if (!$cekUser->hasRole('Desa') && !$cekUser->hasRole('UNIT PELAKSANA TEKNIS (UPT) PPD') && $kd_jns_op == '1') {
                        $btnDelte = ($item['kd_status'] != '1') ? '<a href="javascript:;" data-delete="' . $item['data_billing_id'] . '" class="btn btn-sm btn-danger btn-flat" title="Delete"><i class="far fa-trash-alt"></i></a>' : "";
                    }
                    $btnPdf = gallade::anchorInfo(url('data_dafnom/pdf_cetak/' . $item['data_billing_id']), '<i class="fas fa-file-pdf"></i>', 'title="Cetak Pdf" data-pdf="true"');
                    $btnDetail = "";
                    if ($kd_jns_op == '1') {
                        $btnDetail = gallade::buttonInfo('<i class="fas fa-search"></i>', 'title="Detail Dafnom" data-detail="' . $item['data_billing_id'] . '"');
                    }


                    $status = ($item['kd_status']) ? "<span class='btn btn-xs btn-outline btn-success'>Lunas</span>" : "<span class='btn btn-xs btn-outline btn-warning'>Belum Lunas</span>";
                    if ($item['kd_expired'] == '1' && !$item['kd_status']) {
                        $status = "<span class='btn btn-xs btn-outline btn-danger'>Expired</span>";
                    }
                    $result = [
                        'no' =>  $no,
                        'kobil' => $item['kd_propinsi'] . "." .
                            $item['kd_dati2'] . "." .
                            $item['kd_kecamatan'] . "." .
                            $item['kd_kelurahan'] . "." .
                            $item['kd_blok'] . "-" .
                            $item['no_urut'] . "." .
                            $item['kd_jns_op'],
                        'nama_kecamatan' => $item['nama_kecamatan'],
                        'nama_kelurahan' => $item['nama_kelurahan'],
                        'nama_wp' => $item['nama_wp'],
                        'pbb' => gallade::parseQuantity($item['pbb'] + $item['denda']),
                        'created_at' => $item['created_at'],
                        'kd_status' => $status,
                        'aksi' => '<div class="btn-group">
                        ' . $btnPdf . $btnDetail . $btnDelte . '
                      </div>'
                    ];
                    // if($item['denda']>0){
                    //     $result['pbb'].="(+ ".gallade::parseQuantity($item['denda']).")";
                    // }
                    $no++;
                    $setData[] = Arr::only($result, $list);
                }
                $return = ["msg" => "Pencarian data berhasil.", "status" => true, 'data' => $setData];
            }

            // if ($kd_jns_op != '') {
            //     $request['kd_jns_op'] = $request->kd_jns_op;
            // }

            $return['extra'] = Dafnom::getNOPnonKolektif($request);
            $return = response()->json($return);
            return $return;
        } else {
            $kelurahan = ($request['kelurahan'] && $request['kelurahan'] !== '-') ? "AND vnk.KD_KELURAHAN = '" . $request['kelurahan'] . "'" : "";
            $sql = "SELECT vnk.*
                    FROM v_non_kolektif vnk
                    WHERE vnk.THN_PAJAK_SPPT = '" . $request['tahun_pajak'] . "'
                        AND vnk.KD_KECAMATAN = '" . $request['kecamatan'] . "' " . $kelurahan;
            $getData = DB::connection('oracle')->select(db::raw($sql));
            $datatable=DataTables::of($getData)
                ->addColumn('kobil', function ($row) {
                    return "NOP:".$row->kd_propinsi.".".
                    $row->kd_propinsi.".".
                    $row->kd_dati2.".".
                    $row->kd_kecamatan.".".
                    $row->kd_kelurahan.".".
                    $row->kd_blok."-".
                    $row->no_urut.".";
                    $row->kd_jns_op;
                })
                ->addColumn('nama_kecamatan', function ($row) {
                    return $row->kota_wp_sppt;
                })
                ->addColumn('nama_kelurahan', function ($row) {
                    return $row->kelurahan_wp_sppt;
                })
                ->addColumn('nama_wp', function ($row) {
                    return $row->nm_wp_sppt;
                })
                ->editColumn('pbb', function ($row) {
                    return gallade::parseQuantity($row->jml_sppt_yg_dibayar);
                })
                ->editColumn('kd_status', function ($item) {
                    $status = "<span class='btn btn-xs btn-outline btn-success'>Lunas</span>";
                    return $status;
                })
                ->addColumn('aksi', function ($item) use ($kd_jns_op) {
                    return '<div class="btn-group"></div>';
                })
                ->addColumn('created_at', function ($row) {
                    return "Tgl Pembayaran: ".$row->tgl_pembayaran_sppt;
                })
                ->rawColumns(['aksi', 'pbb', 'kd_status', 'nop', 'kobil'])
                ->with([
                    'extra'=>Dafnom::getNOPnonKolektif($request),
                    'status'=>true
                ])
                ->make(true);
            return $datatable;
            // dd($get);
    //         $where['data_billing.kd_jns_op'] = $kd_jns_op;


    //         $pencarian = $request->pencarian ?? '';


    //         $group = [
    //             'data_billing.data_billing_id',
    //             'data_billing.kd_propinsi',
    //             'data_billing.kd_dati2',
    //             'data_billing.kd_kecamatan',
    //             'data_billing.kd_kelurahan',
    //             'data_billing.kd_blok',
    //             'data_billing.no_urut',
    //             'data_billing.kd_jns_op',
    //             'data_billing.nama_wp',
    //             'data_billing.nama_kecamatan',
    //             'data_billing.nama_kelurahan',
    //             'data_billing.created_at',
    //             'data_billing.kd_status',
    //             'data_billing.expired_at',
    //             'data_billing.kobil',
    //             db::raw('BILLING_KOLEKTIF.kd_propinsi||BILLING_KOLEKTIF.kd_dati2||BILLING_KOLEKTIF.kd_kecamatan||BILLING_KOLEKTIF.kd_kelurahan||BILLING_KOLEKTIF.kd_blok||BILLING_KOLEKTIF.no_urut||BILLING_KOLEKTIF.kd_jns_op')
    //         ];
    //         $select = array_merge($group, [
    //             DB::raw("
    //             'BILLING_KOLEKTIF.kd_propinsi||
    //             BILLING_KOLEKTIF.kd_dati2||
    //             BILLING_KOLEKTIF.kd_kecamatan||
    //             BILLING_KOLEKTIF.kd_kelurahan||
    //             BILLING_KOLEKTIF.kd_blok||
    //             BILLING_KOLEKTIF.no_urut||
    //             BILLING_KOLEKTIF.kd_jns_op nop,                
    //             sum(billing_kolektif.pbb) pbb,
    //                 sum(COALESCE(billing_kolektif.denda,0)) denda,
    //                 CASE WHEN data_billing.expired_at <sysdate then '1' else '0' end kd_expired
    //                 ")
    //         ]);
    //         $getData = Data_billing::where($where)
    //             ->whereRaw(DB::raw('lhp is null'))
    //             ->select(db::raw(" DATA_BILLING.DATA_BILLING_ID,
    //             DATA_BILLING.KD_PROPINSI,
    //             DATA_BILLING.KD_DATI2,
    //             DATA_BILLING.KD_KECAMATAN,
    //             DATA_BILLING.KD_KELURAHAN,
    //             DATA_BILLING.KD_BLOK,
    //             DATA_BILLING.NO_URUT,
    //             DATA_BILLING.KD_JNS_OP,
    //             DATA_BILLING.NAMA_WP,
    //             DATA_BILLING.NAMA_KECAMATAN,
    //             DATA_BILLING.NAMA_KELURAHAN,
    //             DATA_BILLING.CREATED_AT,
    //             DATA_BILLING.KD_STATUS,
    //             DATA_BILLING.EXPIRED_AT,
    //             DATA_BILLING.KOBIL,
    //             BILLING_KOLEKTIF.kd_propinsi||
    //    BILLING_KOLEKTIF.kd_dati2||
    //    BILLING_KOLEKTIF.kd_kecamatan||
    //    BILLING_KOLEKTIF.kd_kelurahan||
    //    BILLING_KOLEKTIF.kd_blok||
    //    BILLING_KOLEKTIF.no_urut||
    //    BILLING_KOLEKTIF.kd_jns_op nop,
    //             SUM (billing_kolektif.pbb) pbb,
    //             SUM (COALESCE (billing_kolektif.denda, 0)) denda,
    //             CASE WHEN data_billing.expired_at < SYSDATE THEN '1' ELSE '0' END
    //                kd_expired"))
    //             ->orderBy('data_billing.created_at', 'desc')
    //             ->join('billing_kolektif', 'billing_kolektif.data_billing_id', '=', 'data_billing.data_billing_id')
    //             ->groupByraw("DATA_BILLING.DATA_BILLING_ID,
    //             DATA_BILLING.KD_PROPINSI,
    //             DATA_BILLING.KD_DATI2,
    //             DATA_BILLING.KD_KECAMATAN,
    //             DATA_BILLING.KD_KELURAHAN,
    //             DATA_BILLING.KD_BLOK,
    //             DATA_BILLING.NO_URUT,
    //             DATA_BILLING.KD_JNS_OP,
    //             DATA_BILLING.NAMA_WP,
    //             DATA_BILLING.NAMA_KECAMATAN,
    //             DATA_BILLING.NAMA_KELURAHAN,
    //             DATA_BILLING.CREATED_AT,
    //             DATA_BILLING.KD_STATUS,
    //             DATA_BILLING.EXPIRED_AT,
    //             DATA_BILLING.KOBIL,
    //             BILLING_KOLEKTIF.kd_propinsi||
    //    BILLING_KOLEKTIF.kd_dati2||
    //    BILLING_KOLEKTIF.kd_kecamatan||
    //    BILLING_KOLEKTIF.kd_kelurahan||
    //    BILLING_KOLEKTIF.kd_blok||
    //    BILLING_KOLEKTIF.no_urut||
    //    BILLING_KOLEKTIF.kd_jns_op")
    //             ->where("data_billing.kd_jns_op", $kd_jns_op);
    //         if ($pencarian != '') {
    //             $pencarian = onlyNumber($pencarian);
    //             $getData = $getData->whereraw("(kobil like '%$pencarian%' or   BILLING_KOLEKTIF.kd_propinsi||BILLING_KOLEKTIF.kd_dati2||BILLING_KOLEKTIF.kd_kecamatan||BILLING_KOLEKTIF.kd_kelurahan||BILLING_KOLEKTIF.kd_blok||BILLING_KOLEKTIF.no_urut||BILLING_KOLEKTIF.kd_jns_op like '%$pencarian%' )");
    //         }
    //         return DataTables::of($getData)
    //             ->editColumn('kobil', function ($row) {
    //                 return ($row->kobil);
    //             })
    //             ->editColumn('nop', function ($row) {
    //                 return formatnop($row->nop);
    //             })
    //             ->editColumn('pbb', function ($row) {
    //                 return $row->pbb + $row->denda;
    //             })
    //             ->editColumn('kd_status', function ($item) {
    //                 $status = ($item->kd_status) ? "<span class='btn btn-xs btn-outline btn-success'>Lunas</span>" : "<span class='btn btn-xs btn-outline btn-warning'>Belum Lunas</span>";
    //                 if ($item->kd_expired == '1' && !$item->kd_status) {
    //                     $status = "<span class='btn btn-xs btn-outline btn-danger'>Expired</span>";
    //                 }
    //                 return $status;
    //             })
    //             ->addColumn('aksi', function ($item) use ($kd_jns_op) {
    //                 $btnDelte = "";

    //                 $cekUser = Auth()->user();
    //                 if (!$cekUser->hasRole('Desa') && !$cekUser->hasRole('UNIT PELAKSANA TEKNIS (UPT) PPD') && $kd_jns_op == '') {
    //                     $btnDelte = ($item->kd_status != '1') ? '<a href="javascript:;" data-delete="' . $item->data_billing_id . '" class="btn btn-sm btn-danger btn-flat" title="Delete"><i class="far fa-trash-alt"></i></a>' : "";
    //                 }
    //                 $btnPdf = gallade::anchorInfo(url('data_dafnom/pdf_cetak/' . $item->data_billing_id), '<i class="fas fa-file-pdf"></i>', 'title="Cetak Pdf" data-pdf="true"');
    //                 $btnDetail = "";
    //                 if ($kd_jns_op == '') {
    //                     $btnDetail = gallade::buttonInfo('<i class="fas fa-search"></i>', 'title="Detail Dafnom" data-detail="' . $item->data_billing_id . '"');
    //                 }
    //                 return '<div class="btn-group">
    //                 ' . $btnPdf . $btnDetail . $btnDelte . '
    //               </div>';
    //             })
    //             ->rawColumns(['aksi', 'pbb', 'kd_status', 'nop', 'kobil'])
    //             ->make(true);
        }
    }

    public function delete(Request $request)
    {
        $return = response()->json(["msg" => "gagal menghapus data.", "status" => false]);
        $request = $request->only(['id']);
        if ($request['id']) {
            DB::beginTransaction();
            try {
                $check = Data_billing::where(['data_billing_id' => $request['id'], 'kd_status' => '1'])->count();
                if (!$check) {
                    Data_billing::where(['data_billing_id' => $request['id']])->delete();
                    billing_kolektif::where(['data_billing_id' => $request['id']])->delete();
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $msg = "Terjadi Kesalahan, Harap Hubungi Admin"; //$e->getMessage();//
                return response()->json(["msg" => $msg, "status" => false]);
            }
            $return = response()->json(["msg" => "Berhasil menghapus data.", "status" => true]);
        }
        return $return;
    }
    public function pdf_cetak($id_billing = false)
    {
        $chekOrcreate = Dafnom::makedaftarBillingKolektif($id_billing);
        $url = Storage::disk('dafnom_list_pdf')->url('dafnom_list/' . $id_billing . '.pdf');

        return redirect($url);

        // dd($url);

        // $pathToFile = 'storage/app/public/dafnom_list_pdf/dafnom_list/' . $id_billing . '.pdf';
        // return response()->file($pathToFile);
    }
    public function espptntpd(Request $request)
    {
        $sendData = $request->only(['ntpd', 'kobil', 'nop']);
        $sql = "SELECT bk.BILLING_KOLEKTIF_ID,bk.DATA_BILLING_ID
        FROM BILLING_KOLEKTIF bk 
        LEFT OUTER JOIN DATA_BILLING db
            ON db.DATA_BILLING_ID=bk.DATA_BILLING_ID 
        WHERE db.KOBIL ='" . $sendData['kobil'] . "'
        AND bk.KD_PROPINSI||bk.KD_DATI2||bk.KD_KECAMATAN||bk.KD_KELURAHAN||bk.KD_BLOK||bk.NO_URUT||bk.KD_JNS_OP='" . $sendData['nop'] . "'";
        $getData = DB::connection('oracle')->select(db::raw($sql));
        if (!count($getData)) {
            return abort(404, 'Data tidak ditemukan');
        }
        $getData = Arr::first($getData);
        return $this->pdf($getData->data_billing_id, $getData->billing_kolektif_id);
    }
    public function pdf($id_billing = false, $idkolektif = false)
    {
        if ($id_billing && !$idkolektif) {
            $chekOrcreate = Dafnom::makedaftarBillingKolektifPembayaran($id_billing);
            $pathToFile = 'storage/app/public/dafnom_list_pdf/pembayaran/' . $id_billing . '.pdf';
            return response()->file($pathToFile);
        }
        if ($id_billing && $idkolektif) {
            ini_set('memory_limit', '-1');
            $data_billing = Data_billing::where(['data_billing_id' => $id_billing, 'kd_status' => '1'])->get();
            if (!($data_billing->count() > 0)) {
                return response()->json(["msg" => "Data billing belum lunas.", "status" => false]);
            }
            $data_billing = $data_billing->first();
            $onJoin = 'billing_kolektif.KD_PROPINSI
            AND psppt.KD_DATI2=billing_kolektif.KD_DATI2
            AND psppt.KD_KECAMATAN=billing_kolektif.KD_KECAMATAN
            AND psppt.KD_KELURAHAN=billing_kolektif.KD_KELURAHAN
            AND psppt.KD_BLOK=billing_kolektif.KD_BLOK
            AND psppt.NO_URUT=billing_kolektif.NO_URUT
            AND psppt.KD_JNS_OP=billing_kolektif.KD_JNS_OP
            AND psppt.THN_PAJAK_SPPT=billing_kolektif.TAHUN_PAJAK';
            $onJoinB = 'billing_kolektif.KD_PROPINSI
            AND spsppt.KD_DATI2=billing_kolektif.KD_DATI2
            AND spsppt.KD_KECAMATAN=billing_kolektif.KD_KECAMATAN
            AND spsppt.KD_KELURAHAN=billing_kolektif.KD_KELURAHAN
            AND spsppt.KD_BLOK=billing_kolektif.KD_BLOK
            AND spsppt.NO_URUT=billing_kolektif.NO_URUT
            AND spsppt.KD_JNS_OP=billing_kolektif.KD_JNS_OP
            AND spsppt.THN_PAJAK_SPPT=billing_kolektif.TAHUN_PAJAK';
            $select = [
                'billing_kolektif.kd_propinsi',
                'billing_kolektif.kd_dati2',
                'billing_kolektif.kd_kecamatan',
                'billing_kolektif.kd_kelurahan',
                'billing_kolektif.kd_kelurahan',
                'billing_kolektif.kd_blok',
                'billing_kolektif.no_urut',
                'billing_kolektif.kd_jns_op',
                'billing_kolektif.tahun_pajak',
                'billing_kolektif.nm_wp',
                'billing_kolektif.pokok',
                'billing_kolektif.denda',
                'billing_kolektif.total',
                'psppt.jln_wp_sppt',
                'billing_kolektif.pengesahan',
                db::raw("case when ref_bank.NAMA_BANK is null then 'BANK JATIM' else ref_bank.NAMA_BANK end nama_bank")
            ];
            $billing_kolektif = Billing_kolektif::where('billing_kolektif.data_billing_id', $id_billing);
            if ($idkolektif) {
                $billing_kolektif = $billing_kolektif->where('billing_kolektif.BILLING_KOLEKTIF_ID', $idkolektif);
            }
            $billing_kolektif = $billing_kolektif->select($select)
                ->leftJoin(db::raw('spo.sppt psppt'), 'psppt.KD_PROPINSI', '=', db::raw($onJoin))
                ->leftJoin(db::raw('spo.pembayaran_sppt@tospo205 spsppt'), 'spsppt.KD_PROPINSI', '=', db::raw($onJoinB))
                ->leftJoin(db::raw('pbb.ref_bank'), 'REF_BANK.KODE_BANK', '=', 'spsppt.KODE_BANK_BAYAR')
                ->get();
            $uri = env("APP_URL", "localhost") . 'e-sppt-ntpd';
            $noPage = false;
            // return view('dafnom._cetak_pembayaran_dafnom',compact('data_billing','billing_kolektif','uri'));
            $pdf = PDF::loadView('dafnom/_cetak_pembayaran_dafnom', compact('data_billing', 'billing_kolektif', 'uri', 'noPage'))->setPaper('a4', 'portrait');
            $filename = 'Dafnom_pembayaran' . time() . '.pdf';
            //return $pdf->download($filename); // directdownload
            return $pdf->stream($filename);  // directstream
        }
    }
    public function detail(Request $request)
    {
        $request = $request->only('id');
        $return = response()->json(["msg" => "Detail data, bermasalah.", "status" => false]);
        if ($request) {
            $data_billing = Data_billing::with('billing_kolektif')->find($request['id']);
            $result['jumlah_nop'] = $data_billing->billing_kolektif->count();
            $result['status'] = ($data_billing->kd_status) ? "<span class='btn btn-sm btn-success btn-sm'>Lunas</span>" : "<span class='btn btn-sm btn-sm btn-warning'>Belum Lunas</span>";
            $result['tgl_kobil'] = $data_billing->created_at;
            $result['nama_kecamatan'] = $data_billing->nama_kecamatan;
            $result['nama_kelurahan'] = $data_billing->nama_kelurahan;
            $result['kode_billing'] = $data_billing->kd_propinsi . "." .
                $data_billing->kd_dati2 . "." .
                $data_billing->kd_kecamatan . "." .
                $data_billing->kd_kelurahan . "." .
                $data_billing->kd_blok . "-" .
                $data_billing->no_urut . "." .
                $data_billing->kd_jns_op;
            //gallade::anchorInfo('data_dafnom/excel/'.$request['id'],'<i class="fas fa-file-excel"></i> Export Dafnom Excel','title="Cetak Excel" data-excel="true"').
            $disabled = ($data_billing->kd_status) ? "" : "disabled";
            $result['option-tools'] = gallade::anchorInfo('data_dafnom/pdf/' . $request['id'], '<i class="fas fa-file-pdf"></i> Export PDF Semua data NOP', 'title="Cetak Pdf" data-pdf="true"', $disabled);
            $setData_billing = $data_billing->billing_kolektif->toArray();
            $setData = [];
            $total_pbb = 0;
            $total_denda = 0;
            $list = ['no', 'nop', 'nm_wp', 'pbb', 'denda', 'aksi'];
            foreach ($setData_billing as $item) {
                //$btnExcel=gallade::anchorInfo('data_dafnom/excel/'.$request['id']."/".$item['billing_kolektif_id'],'<i class="fas fa-file-excel"></i>','title="Cetak Excel" data-excel="true"');
                $btnPdf = gallade::anchorInfo('data_dafnom/pdf/' . $request['id'] . "/" . $item['billing_kolektif_id'], '<i class="fas fa-file-pdf"></i>', 'title="Cetak Pdf" data-pdf="true"', $disabled);
                $resultData = [
                    'no' => '',
                    'nop' => $item['kd_propinsi'] . "." .
                        $item['kd_dati2'] . "." .
                        $item['kd_kecamatan'] . "." .
                        $item['kd_kelurahan'] . "." .
                        $item['kd_blok'] . "-" .
                        $item['no_urut'] . "." .
                        $item['kd_jns_op'],
                    'nm_wp' => $item['nm_wp'],
                    'pbb' => gallade::parseQuantity($item['pbb']),
                    'denda' => gallade::parseQuantity($item['denda']),
                    'aksi' => [
                        'class' => 'text-center',
                        'data' => '<div class="btn-group">' . $btnPdf . '
                      </div>'
                    ]
                ];
                $total_pbb = $total_pbb + $item['pbb'];
                $total_denda = $total_denda + $item['denda'];
                $setData[] = Arr::only($resultData, $list);
            }
            $result['append-data'] = gallade::generateinTbody($setData, "Data Masih Kosong");
            $result['total_pbb'] = gallade::parseQuantity($total_pbb);
            $result['total_denda'] = gallade::parseQuantity($total_denda);
            $return = response()->json(["msg" => "Detail data dafnom ditemukan.", "status" => true, 'data' => $result]);
        }
        return $return;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
