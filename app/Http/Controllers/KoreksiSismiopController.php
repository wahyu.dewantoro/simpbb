<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KoreksiSismiopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::connection("oracle_satutujuh")->table('sppt_Koreksi')
                ->leftjoin('sim_pbb.users', 'users.id', '=', 'sppt_Koreksi.created_by')
                ->leftjoin(DB::raw("sim_pbb.users c"), 'c.id', '=', 'sppt_koreksi.verifikasi_by')
                ->whereRaw("jns_koreksi = '3' and no_transaksi like 'SM%' and verifikasi_at is not null")
                ->selectRaw("no_transaksi,kd_propinsi,kd_dati2,sppt_koreksi.kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,keterangan,sppt_koreksi.created_at,users.nama,verifikasi_keterangan,c.nama verifikator,jns_koreksi,verifikasi_kode")
                ->orderBy("created_at", "desc");
            $search = Strtolower($request->search);
            if ($search != '') {
                $angka = onlyNumber($search);
                $wh = "lower(no_surat) like '%$search%' or lower(no_transaksi) like '%$search%' ";
                if ($angka) {
                    $wh .= "or sppt_koreksi.kd_propinsi||sppt_koreksi.kd_dati2||sppt_koreksi.kd_kecamatan||sppt_koreksi.kd_kelurahan||sppt_koreksi.kd_blok||sppt_koreksi.no_urut||sppt_koreksi.kd_jns_op like '%$angka%'";
                }
                $data = $data->whereraw('(' . $wh . ')');
            }

            return  DataTables::of($data)
                ->addColumn('aksi', function ($row) {
                    $btn = '<button data-kd_propinsi="' . $row->kd_propinsi . '" data-kd_dati2="' . $row->kd_dati2 . '" data-kd_kecamatan="' . $row->kd_kecamatan . '" data-kd_kelurahan="' . $row->kd_kelurahan . '" data-kd_blok="' . $row->kd_blok . '" data-no_urut="' . $row->no_urut . '" data-kd_jns_op="' . $row->kd_jns_op . '" data-thn_pajak_sppt="' . $row->thn_pajak_sppt . '" class="buka btn btn-sm btn-warning"><i class="far fa-trash-alt"></i></button>';
                    return $btn;
                })
                ->editcolumn('jns_koreksi', function ($row) {
                    switch ($row->jns_koreksi) {
                        case '4':
                            # code...
                            $var = "Iventaris";
                            break;

                        default:
                            # code...
                            $var = "Blokir Pembayaran";
                            break;
                    }
                    return $var;
                })
                ->editcolumn('verifikasi_kode', function ($row) {

                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $res = "Telah di setujui";

                            break;
                        case '0':
                            # code...
                            $res = "Tidak di setujui";
                            break;
                        default:
                            # code...
                            $res = "Belum di verifikasi";
                            break;
                    }

                    return $res;
                })
                ->addColumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '.' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->editColumn('keterangan', function ($row) {
                    if ($row->keterangan == '') {
                        $ket = '-';
                    } else {
                        $ket = $row->keterangan;
                    }
                    return $ket;
                })
                ->rawColumns(['aksi', 'jns_koreksi', 'verifikasi_kode', 'keterangan', 'nop'])->make(true);
        }

        return view('koreksi.sismiop-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if ($request->ajax()) {
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;

            $data = DB::connection("oracle_satutujuh")->select(db::raw("select thn_pajak_sppt,count(1) objek,sum(pbb_yg_harus_dibayar_sppt) jumlah 
                                                from sppt
                                                where kd_Kecamatan='$kd_kecamatan' 
                                                and kd_kelurahan='$kd_kelurahan'
                                                and kd_jns_op='7'
                                                and status_pembayaran_sppt='0'
                                                and tgl_terbit_sppt is not null
                                                group by thn_pajak_sppt
                                                order by thn_pajak_sppt asc"));

            $html = "";
            foreach ($data as $row) {

                $html .= "<tr>
                            <td class='text-center'>".$row->thn_pajak_sppt."</td>
                            <td class='text-center'>".$row->objek."</td>
                            <td class='text-right'>".angka($row->jumlah)."</td>
                        </tr>";
            }
            return $html;
        }

        $data = [
            'action' => route('koreksi-sismiop.store'),
            'method' => 'post'
        ];
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('koreksi.sismiop-form', compact('data', 'kecamatan', 'kelurahan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
