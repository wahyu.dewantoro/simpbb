<?php

namespace App\Http\Controllers;

use App\KelolaUsulan;
use App\KelolaUsulanObjek;
use App\Traits\OcaWablas;
use App\User;
use App\UserRayon;
use App\UsulanPengelola;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KelolaNopVerifikasiController extends Controller
{
    use OcaWablas;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return $request->all();
        $jenis = $request->jenis ?? 'all';

        $search = strtolower($request->search ?? '');


        if ($jenis == 'all' || $jenis == 'developer') {
            $developer = UsulanPengelola::Verifying()->selectraw("id,tgl_surat,no_surat,nama_pengembang,'UsulanPengelola' model");
            if ($search != '') {

                $developer = $developer->whereraw("lower(nama_pengembang) like '%" . $search . "%'");
            }
        } else {
            $developer = [];
        }


        if ($jenis == 'all' || $jenis == 'rayon') {
            $rayon = UserRayon::Verifying()->select(DB::raw("id,tgl_surat,no_surat,(select 'Rayon ' ||nm_kelurahan
                                                            from pbb.ref_kelurahan 
                                                            join pbb.ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan
                                                            where ref_kelurahan.kd_kecamatan=user_rayon.kd_kecamatan and ref_kelurahan.kd_kelurahan=user_rayon.kd_kelurahan
                                                            ) nam_rayon,'UserRayon' model"));
            if ($search != '') {

                $rayon = $rayon->whereraw("lower((select 'Rayon ' ||nm_kelurahan
                                                            from pbb.ref_kelurahan 
                                                            join pbb.ref_kecamatan on ref_kecamatan.kd_kecamatan=ref_kelurahan.kd_kecamatan
                                                            where ref_kelurahan.kd_kecamatan=user_rayon.kd_kecamatan and ref_kelurahan.kd_kelurahan=user_rayon.kd_kelurahan
                                                            )) like '%" . $search . "%'");
            }
        } else {
            $rayon = [];
        }

        if ($jenis == 'all' || $jenis == 'wp') {
            $user = User::selectraw("users.id,users.created_at tgl_surat,'Wajib Pajak' no_surat,nama nama_pengembang,'user' model")
                ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                ->where('roles.name', 'Wajib Pajak')
                ->whereraw("wp_verifikasi_kode is null");
            if ($search != '') {

                $user = $user->whereraw("lower(nama) like '%" . $search . "%'");
            }
        } else {
            $user = [];
        }


        if ($jenis == 'all') {
            $data = $developer->union($rayon)
                ->union($user)
                ->orderby('id', 'desc')
                ->get();
        }

        if ($jenis == 'rayon') {
            $data = $rayon->get();
        }

        if ($jenis == 'developer') {
            $data = $developer->get();
        }

        if ($jenis == 'wp') {
            $data = $user->get();
        }






        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;
        $items = $data->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginatedResults = new LengthAwarePaginator($items, $data->count(), $perPage, $currentPage, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);


        $data = $paginatedResults;
        // ->paginate();
        // return $data;
        return view('kelola.verifikasi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $is_admin = Auth()->user()->is_admin();
        if ($is_admin == false) {
            abort(404);
        }

        // return 
        $model = decrypt($request->model);
        // return $model;
        $dokumen = null;
        if ($model == 'UsulanPengelola') {
            $dataVerif = UsulanPengelola::find($id);
            $dokumen = $dataVerif->user->Pendukung()->get();
        } else if ($model == 'UserRayon') {
            $dataVerif = UserRayon::find($id);
            $dokumen = $dataVerif->user->Pendukung()->get();
        } else if ($model == 'user') {
            $dataVerif = User::find($id);
            $dokumen = $dataVerif->pendukung()->get();
        } else {
            abort(404);
        }

        // return $dokumen;

        $data = [
            'method' => 'PATCH',
            'action' => route('kelola-nop-verifikasi.update', $id),
            'data' => $dataVerif,
            'model' => $model,
            'dokumen' => $dokumen
        ];

        //    return $data['dokumen'];
        return view('kelola.verifikasi.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $is_admin = Auth()->user()->is_admin();
        if ($is_admin == false) {
            abort(404);
        }


        // return $request->all();

        $model = ($request->model);
        if ($model == 'UsulanPengelola') {
            $dataVerif = UsulanPengelola::find($id);
            $user_id = $dataVerif->user_id;
            $layanan = 'Usulan sebagai user Developer';
            // $rm = 'DEVELOPER/PENGEMBANG';
        } else if ($model == 'UserRayon') {
            // $rm = 'Rayon';
            $dataVerif = UserRayon::find($id);
            $user_id = $dataVerif->users_id;
            $layanan = "Usulan sebagai user Rayon";
        } else if ($model == 'user') {
            $dataVerif = User::find($id);
            $user_id = $id;
            $layanan = "Usulan sebagai user Wajib Pajak";
        } else {
            abort(404);
        }

        DB::beginTransaction();
        try {
            //code...
            if ($model != 'user') {
                $dataVerif->update([
                    'verifikasi_kode' => $request->verifikasi_kode,
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_at' => now(),
                    'verifikasi_keterangan' => $request->verifikasi_keterangan
                ]);
            } else {
                $dataVerif->update([
                    'wp_verifikasi_kode' => $request->verifikasi_kode,
                    'wp_verifikasi_by' => auth()->user()->id,
                    'wp_verifikasi_at' => now(),
                    'wp_verifikasi_keterangan' => $request->verifikasi_keterangan
                ]);
            }


            $user = User::find($user_id);
            $kode = $request->verifikasi_kode;
            if ($kode == '1') {
                $user->update(['is_pengelola' => '1']);
            }

            if ($kode == '0' && $model != 'user') {
                // assign ke role wajib pajak                
                $user->syncRoles(['Wajib Pajak']);
                $user->update(['wp_verifikasi_kode' => '1', 'wp_verifikasi_at' => now(), 'wp_verifikasi_by' => auth()->user()->id]);
            }

            $telepon = $user->telepon;
            $nama = $user->nama;
            $keterangan = $request->verifikasi_keterangan ?? '';

            if ($kode == '1') {
                $this->NotifAcc($telepon, [$nama, '*' . $layanan . '*']);
            } else {
                // di tolak
                $this->NotifReject($telepon, [$nama, '*' . $layanan . '*', $keterangan ?? "Dokumen tidak sesuai"]);
            }
            DB::commit();

            $flash = [
                'success' => 'Berhasil verifikasi'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(route('kelola-nop-verifikasi.index'))->with($flash);


        /* 

        // return $dataVerif;

        // return $request->all();
        DB::beginTransaction();
        try {
            
            $kode = '1';
            $keterangan = "telah di verifikasi oleh petugas";
            $acc = $request->id_nop;
            $obj = KelolaUsulanObjek::where('kelola_usulan_id', $id)->pluck('id')->toarray();

            foreach ($obj as $i) {
                $kode_v = $acc[$i] ?? '0';
                $o = KelolaUsulanObjek::find($i);
                $o->update([
                    'verifikasi_kode' => $kode_v,
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_at' => now()
                ]);
            }
            //code...
            $ku = KelolaUsulan::find($id);
            $ku->update(['verifikasi_kode' => $kode, 'keterangan_verifikasi' => $keterangan, 'verifikasi_by' => auth()->user()->id, 'verifikasi_at' => now()]);

            $telepon = $ku->user->telepon ?? '';
            $nama = $ku->user->nama ?? 'Wajib Pajak';
            // $user=User::
            DB::commit();
            $flash = [
                'success' => 'Berhasil verifikasi'
            ];

            if ($kode == '1') {
                $this->NotifAcc($telepon, [$nama, '*USULAN PENGELOLAAN NOP*']);
            } else {
                // di tolak
                $this->NotifReject($telepon, [$nama, '*USULAN PENGELOLAAN NOP*', $keterangan ?? "Dokumen tidak sesuai"]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(route('kelola-nop-verifikasi.index'))->with($flash); */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
