<?php

namespace App\Http\Controllers;

use App\Exports\dataQrisExport;
use App\Models\Qris as ModelsQris;
use App\Qris;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Qris::orderby('expired_date', 'desc');

        $search = strtolower(($request->search ?? ''));
        $status = $request->status ?? '99';
        $a = Carbon::now()->subDay(6)->format('m/d/Y');
        $b = Carbon::now()->format('m/d/Y');


        if ($request->has('daterange')) {
            $daterange = $request->daterange;
        } else {
            $daterange = $a . ' - ' . $b;
        }

        $appends = [];
        if ($search != '' or $status != '' or $daterange != '') {

            if ($daterange != '') {
                $exp = explode(' - ', $daterange);
                $start = new Carbon($exp[0]);
                $start = $start->format('Ymd');
                $end = new Carbon($exp[1]);
                $end = $end->format('Ymd');
                $data = $data->whereraw(" (trunc(transactiondate) between to_date('$start','yyyymmdd') and to_date('$end','yyyymmdd'))");
                $appends['daterange'] = $daterange;
            }

            if ($search != '') {
                $nop = onlyNumber($search);
                $ff = "lower(nama_wp) like '%$search%' or lower(pjsp) like '%$search%' or";
                if ($nop != '') {
                    $ff .= " kobil like '%$nop%' or nop like '%$nop%' or bill_number like '%$nop%' or";
                }

                $ff = substr($ff, 0, -2);
                $data = $data->whereraw("(" . $ff . ")");
                $appends['search'] = $search;
            }

            switch ($status) {
                case '1':
                    # code...
                    $wh = "(amount_pay is not null)";
                    break;
                case '2':
                    $wh = "(amount_pay is null and expired_date>sysdate)";
                    break;
                case '3':
                    $wh = "(amount_pay is null and expired_date<sysdate)";
                    break;
                default:
                    # code...
                    $wh = "";
                    break;
            }
            if ($wh != '') {
                $data = $data->whereraw($wh);
                $appends['status'] = $status;
            }
        }

        $data = $data->paginate(10)->appends($appends);

        return view('qris/index', compact('data', 'daterange'));
    }

    public function cetak(Request $request)
    {
        $data = Qris::orderby('expired_date', 'desc');

        $search = strtolower(($request->search ?? ''));
        $status = $request->status ?? '99';
        $a = Carbon::now()->subDay(6)->format('m/d/Y');
        $b = Carbon::now()->format('m/d/Y');


        if ($request->has('daterange')) {
            $daterange = $request->daterange;
        } else {
            $daterange = $a . ' - ' . $b;
        }

        $appends = [];
        if ($search != '' or $status != '' or $daterange != '') {

            if ($daterange != '') {
                $exp = explode(' - ', $daterange);
                $start = new Carbon($exp[0]);
                $start = $start->format('Ymd');
                $end = new Carbon($exp[1]);
                $end = $end->format('Ymd');
                $data = $data->whereraw(" (trunc(transactiondate) between to_date('$start','yyyymmdd') and to_date('$end','yyyymmdd'))");
                $appends['daterange'] = $daterange;
            }

            if ($search != '') {
                $nop = onlyNumber($search);
                $ff = "lower(nama_wp) like '%$search%' or lower(pjsp) like '%$search%' or";
                if ($nop != '') {
                    $ff .= " kobil like '%$nop%' or nop like '%$nop%' or bill_number like '%$nop%' or";
                }

                $ff = substr($ff, 0, -2);
                $data = $data->whereraw("(" . $ff . ")");
                $appends['search'] = $search;
            }

            switch ($status) {
                case '1':
                    # code...
                    $wh = "(amount_pay is not null)";
                    break;
                case '2':
                    $wh = "(amount_pay is null and expired_date>sysdate)";
                    break;
                case '3':
                    $wh = "(amount_pay is null and expired_date<sysdate)";
                    break;
                default:
                    # code...
                    $wh = "";
                    break;
            }
            if ($wh != '') {
                $data = $data->whereraw($wh);
                $appends['status'] = $status;
            }
        }
        $data = $data->get();
        return Excel::download(new dataQrisExport($data), 'pembayaran qris.xlsx');
        // return view('qris/cetak_excel', compact('data', 'daterange'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $qr_value = decrypt($id);
        $qris = ModelsQris::where('qr_value', $qr_value)->first();

        return view('qris.flayer', compact('qr_value', 'qris'));
        //  return ' <img src="data:image/png;base64, '.$qrbayar.'" style="width:55px">';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
