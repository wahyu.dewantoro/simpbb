<?php

namespace App\Http\Controllers;

use App\Models\PemutihanPajak;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PemutihanPajakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = PemutihanPajak::get();
        return view('pemutihan/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = [
            'action' => route('refrensi.pemutihan.store'),
            'method' => 'post'
        ];
        return view('pemutihan/form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $pemutihan = [
                'nama_program' => $request->nama_program,
                'deskripsi' => $request->deskripsi,
                'tgl_mulai' => new Carbon($request->tgl_mulai),
                'tgl_selesai' => new Carbon($request->tgl_selesai),
            ];
            PemutihanPajak::create($pemutihan);
            DB::commit();
            $flash = ['success' => 'Berhasil di simpan'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.pemutihan.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pemutihan = PemutihanPajak::find($id);
        $data = [
            'action' => route('refrensi.pemutihan.update', $id),
            'method' => 'patch',
            'pemutihan' => $pemutihan
        ];
        return view('pemutihan/form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


        DB::beginTransaction();
        try {
            $pemutihan = [
                'nama_program' => $request->nama_program,
                'deskripsi' => $request->deskripsi,
                'tgl_mulai' => new Carbon($request->tgl_mulai),
                'tgl_selesai' => new Carbon($request->tgl_selesai),
            ];
            PemutihanPajak::where('id', $id)->update($pemutihan);
            DB::commit();
            $flash = ['success' => 'Berhasil di update'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.pemutihan.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {

            PemutihanPajak::where('id', $id)->delete();
            DB::commit();
            $flash = ['success' => 'Berhasil di hapus'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.pemutihan.index'))->with($flash);
    }
}
