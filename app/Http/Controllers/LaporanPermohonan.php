<?php

namespace App\Http\Controllers;

use App\Exports\LaporanUserExport;
use App\Helpers\gallade;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\Layanan_objek;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Excel as ExcelExcel;
use Maatwebsite\Excel\Facades\Excel;
use SnappyPdf as PDF;

class LaporanPermohonan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        $result = [];
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit')->first();
        $listUser = User::whereRaw(db::raw("user_unit_kerja.kd_unit like '" . $user . "%'"))
            ->leftJoin('user_unit_kerja', function ($join) {
                $join->On('user_unit_kerja.user_id', '=', 'users.id');
            })
            ->orderBy('users.nama')
            ->get();
        $Jenis_layanan = Jenis_layanan::all();
        return view('layanan.laporan.user', compact('result', 'listUser', 'Jenis_layanan'));
    }
    public function user_search(Request $request)
    {
        $select = [
            db::raw('DISTINCT layanan_objek.id'),
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            'layanan_objek.alamat_op',
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan_objek.pemutakhiran_at",
            "layanan.jenis_layanan_id",
            "layanan.jenis_objek",
            "layanan.kd_status",
            "penelitian_task.penelitian",
            db::raw("nvl(users.nama_pendek,username) petugas")
        ];
        $dataSet = Layanan_objek::select($select);
        $listUser = $request->only('list_user');
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $dataSet->where(['created_by' => $listUser['list_user']]);
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['created_by' => Auth()->user()->id]);
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join('penelitian_task', function ($join) {
                $join->On('penelitian_task.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                    ->On('penelitian_task.layanan_objek_id', '=', 'layanan_objek.id');
            })
            ->join('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->whereraw("layanan.deleted_at is null and layanan.jenis_layanan_id!='8'")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();

        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                if ($data->jenis_layanan_id == '7') {
                    return "-- NOP GABUNG ---";
                }
            } else {
                if ($data->jenis_layanan_id == '6') {
                    return "-- NOP PECAH ---";
                }
            }
            if ($data->jenis_layanan_id == '1') {
                return "-- NOP BARU ---";
            }
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->editColumn('penelitian', function ($data) {
                $list = [
                    '1' => 'Penelitian Kantor',
                    '2' => 'Penelitian Khusus'
                ];
                if (!in_array($data->penelitian, array_keys($list))) {
                    return '';
                }
                return $list[$data->penelitian];
            })
            ->editColumn('jenis_layanan_nama', function ($data) {
                $jenisObjek = "";
                // $list=[
                //     "1"=>"Pribadi",
                //     "2"=>"Badan",
                //     "3"=>"Kolektif"
                // ];
                // if(in_array($data->jenis_objek,array_keys($list))){
                //     $jenisObjek=" [".$list[$data->jenis_objek]."]";
                // }
                return $data->jenis_layanan_nama . $jenisObjek;
            })
            ->editColumn('kd_status', function ($data) {
                if ($data->pemutakhiran_at != null && $data->pemutakhiran_at != "" && $data->kd_status != '1' && $data->jenis_layanan_id != '8') {
                    $data->kd_status = '1';
                }
                $status = [
                    '0' => "<span class='badge badge-warning '>Penelitian</span>",
                    '1' => "<span class='badge badge-success '>Selesai</span>",
                    '2' => "<span class='badge badge-warning '>Berkas belum Masuk</span>",
                ];
                $setstatus = "<span class='badge badge-warning '>--</span>";
                if (in_array($data->kd_status, array_keys($status))) {
                    $setstatus = $status[$data->kd_status];
                }
                return $setstatus;
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function user_cetak(Request $request)
    {

        $tipe = $request->tipe ?? 'pdf';
        $listUser = $request->only('list_user');
        $userDefault = '-';
        $select = [
            db::raw('DISTINCT layanan_objek.id'),
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            'layanan_objek.alamat_op',
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            "layanan_objek.nop_gabung",
            "layanan_objek.pemutakhiran_at",
            "layanan.jenis_layanan_id",
            "layanan.jenis_objek",
            "layanan.kd_status",
            "penelitian_task.penelitian",
            DB::raw("case when penelitian_task.penelitian = 1 then 'Penelitian Kantor' else 'Penelitian Khusus' end penelitian_nama"),
            DB::raw("nvl(users.nama_pendek,username) petugas")
        ];
        $dataSet = Layanan_objek::select($select);
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $userDefault = User::where(['id' => $listUser['list_user']])->pluck('nama')->first();
                $dataSet->where(['created_by' => $listUser['list_user']]);
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['created_by' => Auth()->user()->id]);
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join('penelitian_task', function ($join) {
                $join->On('penelitian_task.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                    ->On('penelitian_task.layanan_objek_id', '=', 'layanan_objek.id');
            })
            ->join('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->whereraw("layanan.deleted_at is null and layanan.jenis_layanan_id!='8'")
            ->orderBy(DB::raw('penelitian_task.penelitian,layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();


        if ($tipe == 'excel') {

            $statusLabel = [
                '0' => "Penelitian",
                '1' => "Selesai",
                '2' => "Berkas belum Masuk",
            ];

            $dataArray = [];
            $no = 1;
            $total_luas = 0;
            $total_bng = 0;

            foreach ($dataSet as $item) {
                // Format NOP
                $nop = "{$item->kd_propinsi}.{$item->kd_dati2}.{$item->kd_kecamatan}.{$item->kd_kelurahan}.{$item->kd_blok}-{$item->no_urut}.{$item->kd_jns_op}";

                if ($item->jenis_layanan_id == '7') {
                    $nop = "-NOP GABUNG-";
                } elseif ($item->jenis_layanan_id == '6') {
                    $nop = "-NOP PECAH-";
                } elseif ($item->jenis_layanan_id == '1') {
                    $nop = "-NOP BARU-";
                }

                // Menentukan status
                $setstatus = "--";
                if (!empty($item->pemutakhiran_at) && $item->kd_status != '1' && $item->jenis_layanan_id != '8') {
                    $item->kd_status = '1';
                }

                if (array_key_exists($item->kd_status, $statusLabel)) {
                    $setstatus = $statusLabel[$item->kd_status];
                }

                // Memasukkan data ke array
                $dataArray[] = [
                    $no,
                    $item->nomor_layanan,
                    $nop,
                    $item->nama_wp,
                    $item->alamat_op,
                    $item->jenis_layanan_nama,
                    $item->luas_bumi,
                    $item->luas_bng,
                    $setstatus,
                    $item->petugas,
                ];

                // Update total luas
                $total_luas += $item->luas_bumi;
                $total_bng += $item->luas_bng;
                $no++;
            }

            // Menambahkan total di akhir array
            $dataArray[] = [
                '',
                '',
                '',
                '',
                'Total',
                '',
                $total_luas,
                $total_bng,
                '',
                '',
            ];

            return Excel::download(new LaporanUserExport($dataArray), 'laporan_layanan.xlsx');
            return response()->json($dataArray);
        } else {
            $result = [
                'nama' => $userDefault,
                'tanggal' => $tanggal,
                'data' => $dataSet
            ];
            // return view('layanan.cetak.laporan.user',compact('result'));
            $pages = View::make('layanan.cetak.laporan.user', compact('result'));
            $pdf = pdf::loadHTML($pages)
                ->setPaper('A4');
            $pdf->setOption('enable-local-file-access', true);
            return $pdf->stream();
        }
    }
    public function kelurahan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $Jenis_layanan = Jenis_layanan::all();
        return view('layanan.laporan.kelurahan', compact('result', 'kecamatan', 'kelurahan', 'Jenis_layanan'));
    }
    public function kelurahan_search(Request $request)
    {
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            "layanan_objek.pemutakhiran_at",
            "layanan.kd_status",
            "layanan_objek.is_tolak",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $orderBy = DB::raw('layanan_objek.nomor_layanan,
        layanan_objek.id,
        layanan_objek.kd_propinsi,
        layanan_objek.kd_dati2,
        layanan_objek.kd_kecamatan,
        layanan_objek.kd_kelurahan,
        layanan_objek.kd_blok,
        layanan_objek.no_urut,
        layanan_objek.kd_jns_op,
        layanan.jenis_layanan_id');
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null")
            ->orderBy($orderBy)
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                if ($data->jenis_layanan_id == '7') {
                    return "-- NOP GABUNG ---";
                }
            } else {
                if ($data->jenis_layanan_id == '6') {
                    return "-- NOP PECAH ---";
                }
            }
            if ($data->jenis_layanan_id == '1') {
                return "-- NOP BARU ---";
            }
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->editColumn('usernameinput', function ($data) {
                $petugas = "";
                if ($data->petugas != $data->usernameinput) {
                    $petugas = $data->petugas;
                }
                return $data->usernameinput . "/" . $petugas;
            })
            ->editColumn('kd_status', function ($data) {
                if ($data->pemutakhiran_at != null && $data->pemutakhiran_at != "" && $data->kd_status != '1' && $data->jenis_layanan_id != '8') {
                    $data->kd_status = '1';
                }
                $ext_txt = "<span class='badge badge-success '>Selesai</span>";
                if ($data->is_tolak == '1') {
                    $ext_txt = "<span class='badge badge-danger'>Di tolak</span>";
                }
                $status = [
                    '0' => "<span class='badge badge-warning '>Penelitian</span>",
                    '1' => $ext_txt,
                    '2' => "<span class='badge badge-warning '>Berkas belum Masuk</span>",
                ];
                $setstatus = "<span class='badge badge-warning '>--</span>";
                if (in_array($data->kd_status, array_keys($status))) {
                    $setstatus = $status[$data->kd_status];
                }
                return $setstatus;
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function kelurahan_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            "layanan_objek.pemutakhiran_at",
            "layanan.kd_status",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        $kdKecamatan = "-";
        $kdKelurahan = "-";
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kecamatan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        // $User=Auth()->user();
        // $user=$User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        // if($user->count()=='1'&&$user->first()!='3507'){
        //     $dataSet->where(['created_by'=>Auth()->user()->id]);
        // }
        $orderBy = DB::raw('layanan_objek.nomor_layanan,
        layanan_objek.id,
        layanan_objek.kd_propinsi,
        layanan_objek.kd_dati2,
        layanan_objek.kd_kecamatan,
        layanan_objek.kd_kelurahan,
        layanan_objek.kd_blok,
        layanan_objek.no_urut,
        layanan_objek.kd_jns_op,
        layanan.jenis_layanan_id');
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null")
            ->orderBy($orderBy)
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('layanan.cetak.laporan.kelurahan',compact('result'));
        $pages = View::make('layanan.cetak.laporan.kelurahan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function bulan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun = date('Y');
        $start = 2013;
        $tahun_pajak = [];
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }
        $Jenis_layanan = Jenis_layanan::all();
        return view('layanan.laporan.bulan', compact('result', 'kecamatan', 'kelurahan', 'tahun_pajak', 'Jenis_layanan'));
    }
    public function bulan_cetak(Request $request)
    {
        $group = [
            'layanan.jenis_layanan_nama',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
        ];
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = array_merge($group, [db::raw('count(*) total,sum(layanan_objek.luas_bumi) luas_bumi,sum(layanan_objek.luas_bng) luas_bng')]);
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan', "tahun", "bulan"]);
        $tahun = $search['tahun'];
        $bulan = 'Semua';
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
            if ($search['tahun'] != "-" && $search['tahun'] != "") {
                $dataSet->whereRaw(Db::raw('EXTRACT(YEAR FROM layanan.created_at)=' . $search['tahun']));
            }
            if ($search['bulan'] != "-" && $search['bulan'] != "") {
                $bulan = gallade::getMonth($search['bulan']);
                $dataSet->whereRaw(Db::raw('EXTRACT(month FROM layanan.created_at)=' . $search['bulan']));
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                    $kelurahanDefault = $kecamatan->first()->nm_kelurahan;
                }
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null")
            ->groupBy($group)
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'data' => $dataSet
        ];
        // return view('layanan.cetak.laporan.bulan',compact('result'));
        $pages = View::make('layanan.cetak.laporan.bulan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function bulan_search(Request $request)
    {
        $group = [
            'layanan.jenis_layanan_nama',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
        ];
        $select = array_merge($group, [db::raw('count(*) total,sum(layanan_objek.luas_bumi) luas_bumi,sum(layanan_objek.luas_bng) luas_bng')]);
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan', "tahun", "bulan"]);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
            if ($search['tahun'] != "-" && $search['tahun'] != "") {
                $dataSet->whereRaw(Db::raw('EXTRACT(YEAR FROM layanan.created_at)=' . $search['tahun']));
            }
            if ($search['bulan'] != "-" && $search['bulan'] != "") {
                $dataSet->whereRaw(Db::raw('EXTRACT(month FROM layanan.created_at)=' . $search['bulan']));
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null")
            ->groupBy($group)
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addIndexColumn()
            ->make(true);
    }
    public function hapus_permohonan()
    {
        $result = [];
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit')->first();
        $listUser = User::whereRaw(DB::raw("user_unit_kerja.kd_unit like '" . $user . "%'"))
            ->leftJoin('user_unit_kerja', function ($join) {
                $join->On('user_unit_kerja.user_id', '=', 'users.id');
            })
            ->orderBy('users.nama')
            ->get();
        $Jenis_layanan = Jenis_layanan::all();
        return view('layanan.laporan.hapus_permohonan', compact('result', 'listUser', 'Jenis_layanan'));
    }
    public function hapus_permohonan_search(Request $request)
    {
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan.keterangan',
            'layanan.deleted_at',
            'layanan_objek.nomor_layanan',
            'layanan.jenis_layanan_nama',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            'layanan_objek.nop_gabung',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            db::raw("users.nama userdel")

        ];
        $dataSet = Layanan_objek::select($select);
        $listUser = $request->only('list_user');
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $dataSet->where(['layanan.deleted_by' => $listUser['list_user']]);
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.deleted_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.deleted_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['layanan.deleted_by' => Auth()->user()->id]);
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->join('users', function ($join) {
                $join->On('users.id', '=', 'layanan.deleted_by');
            }, "left outer join")
            ->whereraw("layanan.deleted_at is not null")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                if ($data->jenis_layanan_id == '7') {
                    return "-- NOP GABUNG ---";
                }
            } else {
                if ($data->jenis_layanan_id == '6') {
                    return "-- NOP PECAH ---";
                }
            }
            if ($data->jenis_layanan_id == '1') {
                return "-- NOP BARU ---";
            }
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->addColumn('alamat', function ($data) {
                return $data['nm_kelurahan'] . "/" . $data['nm_kecamatan'];
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function hapus_permohonan_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $userDefault = '-';
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',

            'layanan.deleted_at',
            'layanan_objek.nomor_layanan',
            'layanan.jenis_layanan_nama',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            'layanan_objek.nop_gabung',
            "layanan.jenis_layanan_id",
            "ref_kecamatan.nm_kecamatan",
            "ref_kelurahan.nm_kelurahan",
            db::raw("users.nama userdel")

        ];
        $dataSet = Layanan_objek::select($select);
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $userDefault = User::where(['id' => $listUser['list_user']])->pluck('nama')->first();
                $dataSet->where(['layanan.deleted_by' => $listUser['list_user']]);
            }
        }
        $setLayanan = $request->only(['layanan']);
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $dataSet->where(['jenis_layanan_id' => $setLayanan['layanan']]);
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.deleted_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.deleted_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.deleted_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        if ($user->count() == '1' && $user->first() != '3507') {
            $dataSet->where(['created_by' => Auth()->user()->id]);
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->join('users', function ($join) {
                $join->On('users.id', '=', 'layanan.deleted_by');
            }, "left outer join")
            ->whereraw("layanan.deleted_at is not null")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'nama' => $userDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('layanan.cetak.hapus_permohonan',compact('result'));
        $pages = View::make('layanan.cetak.hapus_permohonan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    public function induk_pecah_minus()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('layanan.laporan.induk_pecah_minus', compact('result', 'kecamatan', 'kelurahan'));
    }
    public function induk_pecah_minus_search(Request $request)
    {
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.sisa_pecah_total_gabung',
            'layanan_objek.hasil_pecah_hasil_gabung',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas"),
            db::raw("case when layanan_objek.pemutakhiran_at is null then 'Penelitian' else 'Done' end raw_tag")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $setInduk = $request->only(['induk']);
        $whereInduk = "";
        if (in_array('induk', array_keys($setInduk))) {
            if ($setInduk['induk'] == '2') {
                $whereInduk = "and (layanan_objek.sisa_pecah_total_gabung>layanan_objek.luas_bumi or layanan_objek.hasil_pecah_hasil_gabung<0)";
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null 
                        and layanan.jenis_layanan_id='6' 
                        " . $whereInduk . "
                        and layanan_objek.nop_gabung is null")
            ->orderByraw("layanan_objek.pemutakhiran_at desc nulls first")
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                if ($data->jenis_layanan_id == '7') {
                    return "-- NOP GABUNG ---";
                }
            } else {
                if ($data->jenis_layanan_id == '6') {
                    return "-- NOP PECAH ---";
                }
            }
            if ($data->jenis_layanan_id == '1') {
                return "-- NOP BARU ---";
            }
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->editColumn('raw_tag', function ($data) {
                return "<span class='badge badge-primary '>" . $data->raw_tag . "</span>";
            })
            ->editColumn('usernameinput', function ($data) {
                $petugas = "";
                if ($data->petugas != $data->usernameinput) {
                    $petugas = $data->petugas;
                }
                return $data->usernameinput . "/" . $petugas;
            })
            ->editColumn('sisa_pecah_total_gabung', function ($data) {
                if ($data->sisa_pecah_total_gabung > 0) {
                    $result = $data->luas_bumi - $data->sisa_pecah_total_gabung;
                } else {
                    $result = $data->hasil_pecah_hasil_gabung;
                }
                return $result;
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function induk_pecah_minus_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            db::raw("'Kec. ' || ref_kecamatan.nm_kecamatan || ' Ds. ' ||  ref_kelurahan.nm_kelurahan || ' ' ||  layanan_objek.alamat_op as alamat_op"),
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.sisa_pecah_total_gabung',
            'layanan_objek.hasil_pecah_hasil_gabung',
            'layanan_objek.nama_wp',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas"),
            db::raw("case when layanan_objek.pemutakhiran_at is null then 'Penelitian' else 'Done' end raw_tag")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        $kdKecamatan = "-";
        $kdKelurahan = "-";
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kecamatan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        // $User=Auth()->user();
        // $user=$User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        // if($user->count()=='1'&&$user->first()!='3507'){
        //     $dataSet->where(['created_by'=>Auth()->user()->id]);
        // }
        $setInduk = $request->only(['induk']);
        $whereInduk = "";
        $jenisInduk = "Semua";
        if (in_array('induk', array_keys($setInduk))) {
            if ($setInduk['induk'] == '2') {
                $jenisInduk = "Induk Pecah Minus";
                $whereInduk = "and (layanan_objek.sisa_pecah_total_gabung>layanan_objek.luas_bumi or layanan_objek.hasil_pecah_hasil_gabung<0)";
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan');
            })
            ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                    ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
            })
            ->whereraw("layanan.deleted_at is null 
                        and layanan.jenis_layanan_id='6' 
                        " . $whereInduk . "
                        and layanan_objek.nop_gabung is null")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet,
            'jenisInduk' => $jenisInduk
        ];
        // return view('layanan.cetak.laporan.induk_pecah_minus',compact('result'));
        $pages = View::make('layanan.cetak.laporan.induk_pecah_minus', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }

    public function daftar_pembetulan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('layanan.laporan.daftar_pembetulan', compact('result', 'kecamatan', 'kelurahan'));
    }
    public function daftar_pembetulan_search(Request $request)
    {
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            'layanan_objek.alamat_op',
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
            }
        }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->whereraw("layanan.deleted_at is null and layanan.jenis_layanan_id='8' ")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            if ($data->nop_gabung == "" || $data->nop_gabung == null) {
                if ($data->jenis_layanan_id == '7') {
                    return "-- NOP GABUNG ---";
                }
            } else {
                if ($data->jenis_layanan_id == '6') {
                    return "-- NOP PECAH ---";
                }
            }
            if ($data->jenis_layanan_id == '1') {
                return "-- NOP BARU ---";
            }
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->editColumn('usernameinput', function ($data) {
                $petugas = "";
                if ($data->petugas != $data->usernameinput) {
                    $petugas = $data->petugas;
                }
                return $data->usernameinput;
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function daftar_pembetulan_cetak(Request $request)
    {
        $listUser = $request->only('list_user');
        $kecamatanDefault = 'Semua';
        $kelurahanDefault = 'Semua';
        $select = [
            'layanan_objek.kd_propinsi',
            'layanan_objek.kd_dati2',
            'layanan_objek.kd_kecamatan',
            'layanan_objek.kd_kelurahan',
            'layanan_objek.kd_blok',
            'layanan_objek.no_urut',
            'layanan_objek.kd_jns_op',
            'layanan_objek.nomor_layanan',
            'layanan_objek.alamat_op',
            'layanan.jenis_layanan_nama',
            'layanan_objek.luas_bumi',
            'layanan_objek.luas_bng',
            'layanan_objek.nama_wp',
            'layanan_objek.alamat_op',
            "layanan_objek.nop_gabung",
            "layanan.jenis_layanan_id",
            db::raw("users.nama usernameinput"),
            db::raw("petugas.nama petugas")
        ];
        $dataSet = Layanan_objek::select($select);
        $search = $request->only(['kecamatan', 'kelurahan']);
        $kdKecamatan = "-";
        $kdKelurahan = "-";
        if ($search) {
            if ($search['kecamatan'] != "-" && $search['kecamatan'] != "") {
                $dataSet->where(['layanan_objek.kd_kecamatan' => $search['kecamatan']]);
                $kecamatan = Kecamatan::where(['kd_kecamatan' => $search['kecamatan']])->get();
                if ($kecamatan->count() == '1') {
                    $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                }
                if ($search['kelurahan'] != "-" && $search['kelurahan'] != "") {
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $search['kelurahan']]);
                    $kelurahan = Kelurahan::where(['kd_kecamatan' => $search['kecamatan'], 'kd_kelurahan' => $search['kelurahan']])->get();
                    if ($kecamatan->count() == '1') {
                        $kelurahanDefault = $kelurahan->first()->nm_kelurahan;
                    }
                }
            }
        } else {
            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            if ($kecamatan->count() == '1') {
                $kecamatanDefault = $kecamatan->first()->nm_kecamatan;
                $dataSet->where(['layanan_objek.kd_kecamatan' => $kecamatan->first()->kd_kecamatan]);
                $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan', 'asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
                if ($kelurahan->count() == '1') {
                    $kelurahanDefault = $kecamatan->first()->nm_kelurahan;
                    $dataSet->where(['layanan_objek.kd_kelurahan' => $kelurahan->first()->kd_kelurahan]);
                }
            }
        }
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $dataSet->whereraw("layanan.created_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and layanan.created_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')");
                $tanggal = $before . "-" . $after;
            } else {
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
                $tanggal = $after;
            }
        } else {
            $tanggal = 'Semua';
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $dataSet->whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
                $tanggal = $now;
            }
        }
        // $User=Auth()->user();
        // $user=$User->Unitkerja()->pluck('UNIT_KERJA.kd_unit');
        // if($user->count()=='1'&&$user->first()!='3507'){
        //     $dataSet->where(['created_by'=>Auth()->user()->id]);
        // }
        $dataSet = $dataSet->leftJoin('layanan', function ($join) {
            $join->On('layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan');
        })
            ->leftJoin('users', function ($join) {
                $join->On('users.id', '=', 'layanan.created_by');
            })
            ->leftJoin(db::raw('users petugas'), function ($join) {
                $join->On('petugas.id', '=', 'layanan.updated_by');
            })
            ->whereraw("layanan.deleted_at is null and layanan.jenis_layanan_id='8' ")
            ->orderBy(DB::raw('layanan_objek.nomor_layanan,layanan_objek.id'))
            ->get();
        $result = [
            'kecamatan' => $kecamatanDefault,
            'kelurahan' => $kelurahanDefault,
            'tanggal' => $tanggal,
            'data' => $dataSet
        ];
        // return view('layanan.cetak.laporan.kelurahan',compact('result'));
        $pages = View::make('layanan.cetak.laporan.daftar_pembetulan', compact('result'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
