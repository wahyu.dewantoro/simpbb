<?php

namespace App\Http\Controllers;

use App\Helpers\gallade;
use App\Models\Penagihan_piutang;
use App\Models\Piutang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class RealisasiPenagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $file=public_path("penagihan_piutang.xls");
        // ini_set('memory_limit', '-1');
        // $array = Excel::toArray(false, $file);
        // unset($array[0][0]);
        // $temp_nop=[];
        // foreach($array[0] as $cell){
        //     $cellValidation=Validator::make($cell,[
        //         '1' => 'required'
        //     ]);
        //     if (!$cellValidation->fails()) {
        //         $nop=explode(".",$cell[1]);
        //         $blok_urut=explode("-",$nop[4]);
        //         $setData=[ 
        //             'kd_propinsi'=>$nop[0],
        //             'kd_dati2'=>$nop[1],
        //             'kd_kecamatan'=>$nop[2],
        //             'kd_kelurahan'=>$nop[3],
        //             'kd_blok'=>$blok_urut[0],
        //             'no_urut'=>$blok_urut[1],
        //             'kd_jns_op'=>$nop[5]
        //         ];
        //         $penagihanPiutang= new Penagihan_piutang($setData);
        //         $penagihanPiutang->save();
        //     }
        // }
        $result=[];
        return view('penagihan.realisasi_piutang', compact('result'));   
    }
    public function search(Request $request){
        $select=[
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_dati2',
            'penagihan_piutang.kd_kecamatan',
            'ref_kecamatan.nm_kecamatan',
            // DB::raw("count(piutang.kd_propinsi) objek"),
            DB::raw("sum(CASE WHEN piutang.tahun='2021' THEN 1 ELSE 0 END) objek2021"),
            DB::raw("sum(CASE WHEN piutang.tahun='2020' THEN 1 ELSE 0 END) objek2020"),
            DB::raw("sum(CASE WHEN piutang.tahun='2019' THEN 1 ELSE 0 END) objek2019"),
            DB::raw("sum(CASE WHEN piutang.tahun='2018' THEN 1 ELSE 0 END) objek2018"),
            DB::raw("sum(CASE WHEN piutang.tahun='2017' THEN 1 ELSE 0 END) objek2017"),
            DB::raw("sum(CASE WHEN piutang.tahun='2016' THEN 1 ELSE 0 END) objek2016"),
            DB::raw("sum(CASE WHEN piutang.tahun='2021' THEN piutang.pbb ELSE 0 END) pbb2021"),
            DB::raw("sum(CASE WHEN piutang.tahun='2020' THEN piutang.pbb ELSE 0 END) pbb2020"),
            DB::raw("sum(CASE WHEN piutang.tahun='2019' THEN piutang.pbb ELSE 0 END) pbb2019"),
            DB::raw("sum(CASE WHEN piutang.tahun='2018' THEN piutang.pbb ELSE 0 END) pbb2018"),
            DB::raw("sum(CASE WHEN piutang.tahun='2017' THEN piutang.pbb ELSE 0 END) pbb2017"),
            DB::raw("sum(CASE WHEN piutang.tahun='2016' THEN piutang.pbb ELSE 0 END) pbb2016")
            ];
        $groupBy=[
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_dati2',
            'penagihan_piutang.kd_kecamatan',
            'ref_kecamatan.nm_kecamatan'
        ];
        $joinPS="penagihan_piutang.kd_propinsi
            and pembayaran_sppt.kd_propinsi=penagihan_piutang.kd_propinsi
            and pembayaran_sppt.kd_dati2=penagihan_piutang.kd_dati2
            and pembayaran_sppt.kd_kecamatan=penagihan_piutang.kd_kecamatan
            and pembayaran_sppt.kd_kelurahan=penagihan_piutang.kd_kelurahan
            and pembayaran_sppt.kd_blok=penagihan_piutang.kd_blok
            and pembayaran_sppt.no_urut=penagihan_piutang.no_urut
            and pembayaran_sppt.kd_jns_op=penagihan_piutang.kd_jns_op
            and pembayaran_sppt.thn_pajak_sppt=penagihan_piutang.tahun
        ";
        $joinP="penagihan_piutang.kd_propinsi
            and piutang.kd_propinsi=penagihan_piutang.kd_propinsi
            and piutang.kd_dati2=penagihan_piutang.kd_dati2
            and piutang.kd_kecamatan=penagihan_piutang.kd_kecamatan
            and piutang.kd_kelurahan=penagihan_piutang.kd_kelurahan
            and piutang.kd_blok=penagihan_piutang.kd_blok
            and piutang.no_urut=penagihan_piutang.no_urut
            and piutang.kd_jns_op=penagihan_piutang.kd_jns_op
        ";
        $join="penagihan_piutang.kd_propinsi
            and ref_kecamatan.kd_propinsi=penagihan_piutang.kd_propinsi
            and ref_kecamatan.kd_dati2=penagihan_piutang.kd_dati2
            and ref_kecamatan.kd_kecamatan=penagihan_piutang.kd_kecamatan";
        $dataSet=Penagihan_piutang::select($select)
                ->join(db::raw('pbb.ref_kecamatan ref_kecamatan'),'ref_kecamatan.kd_propinsi','=',db::raw($join))
                ->leftJoin('piutang', 'piutang.kd_propinsi', '=', db::raw($joinP))
                // ->leftJoin(db::raw('pbb.pembayaran_sppt pembayaran_sppt'),'pembayaran_sppt.kd_propinsi','=',db::raw($joinPS))
                ->groupBy($groupBy)
                ->orderBy('ref_kecamatan.nm_kecamatan')
                ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
                        $kec=$data->kd_propinsi.'.'.
                        $data->kd_dati2.'.'.
                        $data->kd_kecamatan;
                        return gallade::anchorInfo(url('realisasi_penagihan_piutang/detail',[$kec]),"<i class='fas fa-search'></i>",'title="Detail Data"');
                    })
                    ->editColumn('pbb2016', function($data){
                        return gallade::parseQuantity($data->pbb2016);
                    })   
                    ->editColumn('pbb2017', function($data){
                        return gallade::parseQuantity($data->pbb2017);
                    })   
                    ->editColumn('pbb2018', function($data){
                        return gallade::parseQuantity($data->pbb2018);
                    })   
                    ->editColumn('pbb2019', function($data){
                        return gallade::parseQuantity($data->pbb2019);
                    })   
                    ->editColumn('pbb2020', function($data){
                        return gallade::parseQuantity($data->pbb2020);
                    })   
                    ->editColumn('pbb2021', function($data){
                        return gallade::parseQuantity($data->pbb2021);
                    })   
                ->make(true);
    }

    public function detail(Request $request,$id){
        $result=[];
        $idkec=explode('.',$id);
        $getKecamatan=DB::table(db::raw('pbb.ref_kecamatan ref_kecamatan'))
                ->select(['ref_kecamatan.nm_kecamatan'])
                ->where([
                    'ref_kecamatan.kd_propinsi'=>$idkec[0],
                    'ref_kecamatan.kd_dati2'=>$idkec[1],
                    'ref_kecamatan.kd_kecamatan'=>$idkec[2]
                ])
                ->get();
        $kecamatan=($getKecamatan->first()->nm_kecamatan)?$getKecamatan->first()->nm_kecamatan:'';
        return view('penagihan.realisasi_piutang_detail', compact('result','id','kecamatan'));   
    }
    public function detailkelurahan(Request $request,$id){
        $result=[];
        $idkec=explode('.',$id);
        $kec=$idkec[0].'.'.$idkec[1].'.'.$idkec[2];
        $getKecamatan=DB::table(db::raw('pbb.ref_kecamatan ref_kecamatan'))
                ->select(['ref_kecamatan.nm_kecamatan'])
                ->where([
                    'ref_kecamatan.kd_propinsi'=>$idkec[0],
                    'ref_kecamatan.kd_dati2'=>$idkec[1],
                    'ref_kecamatan.kd_kecamatan'=>$idkec[2]
                ])
                ->get();
        $getKelurahan=DB::table(db::raw('pbb.ref_kelurahan ref_kelurahan'))
                ->select(['ref_kelurahan.nm_kelurahan'])
                ->where([
                    'ref_kelurahan.kd_propinsi'=>$idkec[0],
                    'ref_kelurahan.kd_dati2'=>$idkec[1],
                    'ref_kelurahan.kd_kecamatan'=>$idkec[2],
                    'ref_kelurahan.kd_kelurahan'=>$idkec[3]
                ])
                ->get();
        $kecamatan=($getKecamatan->first()->nm_kecamatan)?$getKecamatan->first()->nm_kecamatan:'';
        $kelurahan=($getKelurahan->first()->nm_kelurahan)?$getKelurahan->first()->nm_kelurahan:'';
        return view('penagihan.realisasi_piutang_detail_kelurahan', compact('result','id','kecamatan','kelurahan','kec'));   
    }
    public function searchdetail(Request $request,$id){
        $select=[
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_dati2',
            'penagihan_piutang.kd_kecamatan',
            'penagihan_piutang.kd_kelurahan',
            'ref_kelurahan.nm_kelurahan',
            DB::raw("sum(CASE WHEN piutang.tahun='2021' THEN 1 ELSE 0 END) objek2021"),
            DB::raw("sum(CASE WHEN piutang.tahun='2020' THEN 1 ELSE 0 END) objek2020"),
            DB::raw("sum(CASE WHEN piutang.tahun='2019' THEN 1 ELSE 0 END) objek2019"),
            DB::raw("sum(CASE WHEN piutang.tahun='2018' THEN 1 ELSE 0 END) objek2018"),
            DB::raw("sum(CASE WHEN piutang.tahun='2017' THEN 1 ELSE 0 END) objek2017"),
            DB::raw("sum(CASE WHEN piutang.tahun='2016' THEN 1 ELSE 0 END) objek2016"),
            DB::raw("sum(CASE WHEN piutang.tahun='2021' THEN piutang.pbb ELSE 0 END) pbb2021"),
            DB::raw("sum(CASE WHEN piutang.tahun='2020' THEN piutang.pbb ELSE 0 END) pbb2020"),
            DB::raw("sum(CASE WHEN piutang.tahun='2019' THEN piutang.pbb ELSE 0 END) pbb2019"),
            DB::raw("sum(CASE WHEN piutang.tahun='2018' THEN piutang.pbb ELSE 0 END) pbb2018"),
            DB::raw("sum(CASE WHEN piutang.tahun='2017' THEN piutang.pbb ELSE 0 END) pbb2017"),
            DB::raw("sum(CASE WHEN piutang.tahun='2016' THEN piutang.pbb ELSE 0 END) pbb2016")
            ];
        $groupBy=[
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_dati2',
            'penagihan_piutang.kd_kecamatan',
            'penagihan_piutang.kd_kelurahan',
            'ref_kelurahan.nm_kelurahan'
        ];
        $joinP="penagihan_piutang.kd_propinsi
            and piutang.kd_dati2=penagihan_piutang.kd_dati2
            and piutang.kd_kecamatan=penagihan_piutang.kd_kecamatan
            and piutang.kd_kelurahan=penagihan_piutang.kd_kelurahan
            and piutang.kd_blok=penagihan_piutang.kd_blok
            and piutang.no_urut=penagihan_piutang.no_urut
            and piutang.kd_jns_op=penagihan_piutang.kd_jns_op
        ";
        $join="penagihan_piutang.kd_propinsi
        and ref_kelurahan.kd_propinsi=penagihan_piutang.kd_propinsi
        and ref_kelurahan.kd_dati2=penagihan_piutang.kd_dati2
        and ref_kelurahan.kd_kecamatan=penagihan_piutang.kd_kecamatan
        AND ref_kelurahan.kd_kelurahan = penagihan_piutang.kd_kelurahan";
        $idkec=explode('.',$id);
        $dataSet=Penagihan_piutang::select($select)
                ->leftJoin('piutang', 'piutang.kd_propinsi', '=', db::raw($joinP))
                ->join(db::raw('pbb.ref_kelurahan ref_kelurahan'),'ref_kelurahan.kd_propinsi','=',db::raw($join))
                ->where([
                    'penagihan_piutang.kd_propinsi'=>$idkec[0],
                    'penagihan_piutang.kd_dati2'=>$idkec[1],
                    'penagihan_piutang.kd_kecamatan'=>$idkec[2]
                ])
                ->orderBy('ref_kelurahan.nm_kelurahan')
                ->groupBy($groupBy)
                ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
                            $kel=$data->kd_propinsi.'.'.
                            $data->kd_dati2.'.'.
                            $data->kd_kecamatan.'.'.$data->kd_kelurahan;
                            return gallade::anchorInfo(url('realisasi_penagihan_piutang/detail_kelurahan',[$kel]),"<i class='fas fa-search'></i>",'title="Detail Data"');
                    })
                    ->addColumn('nop', function ($data) {
                        return  $kec=$data->kd_propinsi.'.'.
                        $data->kd_dati2.'.'.
                        $data->kd_kecamatan.'.'.
                        $data->kd_kelurahan.'.'.
                        $data->kd_blok.'-'.
                        $data->no_urut.'.'.
                        $data->kd_jns_op;
                    })
                    ->editColumn('pbb2021', function ($data) {
                        return gallade::parseQuantity($data->pbb2021);
                    })   
                    ->editColumn('pbb2020', function ($data) {
                        return gallade::parseQuantity($data->pbb2020);
                    })   
                    ->editColumn('pbb2019', function ($data) {
                        return gallade::parseQuantity($data->pbb2019);
                    })   
                    ->editColumn('pbb2018', function ($data) {
                        return gallade::parseQuantity($data->pbb2018);
                    })   
                    ->editColumn('pbb2017', function ($data) {
                        return gallade::parseQuantity($data->pbb2017);
                    })   
                    ->editColumn('pbb2016', function ($data) {
                        return gallade::parseQuantity($data->pbb2016);
                    })   
                    ->addIndexColumn()
                ->make(true);
    }
    public function searchdetailkelurahan(Request $request,$id){
        $select=[
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_dati2',
            'penagihan_piutang.kd_kecamatan',
            'penagihan_piutang.kd_kelurahan',
            'penagihan_piutang.kd_blok',
            'penagihan_piutang.no_urut',
            'penagihan_piutang.kd_jns_op',
            'piutang.wp',
            DB::raw("sum(CASE WHEN piutang.tahun='2021' THEN piutang.pbb ELSE 0 END) pbb2021"),
            DB::raw("sum(CASE WHEN piutang.tahun='2020' THEN piutang.pbb ELSE 0 END) pbb2020"),
            DB::raw("sum(CASE WHEN piutang.tahun='2019' THEN piutang.pbb ELSE 0 END) pbb2019"),
            DB::raw("sum(CASE WHEN piutang.tahun='2018' THEN piutang.pbb ELSE 0 END) pbb2018"),
            DB::raw("sum(CASE WHEN piutang.tahun='2017' THEN piutang.pbb ELSE 0 END) pbb2017"),
            DB::raw("sum(CASE WHEN piutang.tahun='2016' THEN piutang.pbb ELSE 0 END) pbb2016")
            ];
        $groupBy=[
            'penagihan_piutang.kd_propinsi',
            'penagihan_piutang.kd_dati2',
            'penagihan_piutang.kd_kecamatan',
            'penagihan_piutang.kd_kelurahan',
            'penagihan_piutang.kd_blok',
            'penagihan_piutang.no_urut',
            'penagihan_piutang.kd_jns_op',
            'piutang.wp',
        ];
        $joinP="penagihan_piutang.kd_propinsi
            and piutang.kd_dati2=penagihan_piutang.kd_dati2
            and piutang.kd_kecamatan=penagihan_piutang.kd_kecamatan
            and piutang.kd_kelurahan=penagihan_piutang.kd_kelurahan
            and piutang.kd_blok=penagihan_piutang.kd_blok
            and piutang.no_urut=penagihan_piutang.no_urut
            and piutang.kd_jns_op=penagihan_piutang.kd_jns_op
        ";
        $idkec=explode('.',$id);
        $dataSet=Penagihan_piutang::select($select)
                ->leftJoin('piutang', 'piutang.kd_propinsi', '=', db::raw($joinP))
                ->where([
                    'penagihan_piutang.kd_propinsi'=>$idkec[0],
                    'penagihan_piutang.kd_dati2'=>$idkec[1],
                    'penagihan_piutang.kd_kecamatan'=>$idkec[2],
                    'penagihan_piutang.kd_kelurahan'=>$idkec[3]
                ])
                ->groupBy($groupBy)
                ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
                        return '';        
                    })
                    ->addColumn('nop', function ($data) {
                        return  $kec=$data->kd_propinsi.'.'.
                        $data->kd_dati2.'.'.
                        $data->kd_kecamatan.'.'.
                        $data->kd_kelurahan.'.'.
                        $data->kd_blok.'-'.
                        $data->no_urut.'.'.
                        $data->kd_jns_op;
                    })
                    ->editColumn('pbb2021', function ($data) {
                        return gallade::parseQuantity($data->pbb2021);
                    })   
                    ->editColumn('pbb2020', function ($data) {
                        return gallade::parseQuantity($data->pbb2020);
                    })   
                    ->editColumn('pbb2019', function ($data) {
                        return gallade::parseQuantity($data->pbb2019);
                    })   
                    ->editColumn('pbb2018', function ($data) {
                        return gallade::parseQuantity($data->pbb2018);
                    })   
                    ->editColumn('pbb2017', function ($data) {
                        return gallade::parseQuantity($data->pbb2017);
                    })   
                    ->editColumn('pbb2016', function ($data) {
                        return gallade::parseQuantity($data->pbb2016);
                    })   
                    ->addIndexColumn()
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
