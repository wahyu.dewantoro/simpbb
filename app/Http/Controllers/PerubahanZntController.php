<?php

namespace App\Http\Controllers;

use App\Helpers\Pajak;
use App\Helpers\PenetapanSppt;
use App\Models\PerubahanZnt;
use App\Models\PerubahanZntObjek;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class PerubahanZntController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PerubahanZnt::join(db::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'perubahan_znt.kd_kecamatan')
                ->join(db::raw("pbb.ref_kelurahan ref_kelurahan"), function ($join) {
                    $join->on('ref_kelurahan.kd_kecamatan', '=', 'perubahan_znt.kd_kecamatan')
                        ->on('ref_kelurahan.kd_kelurahan', '=', 'perubahan_znt.kd_kelurahan');
                })
                ->select(db::raw("perubahan_znt.*,nm_kecamatan,nm_kelurahan,(select count(1) from perubahan_znt_objek where perubahan_znt_id=perubahan_znt.id ) objek"))
                ->orderbyraw('verifikasi_kode asc nulls first ,created_at desc');


            $jenis = $request->jenis;
            if ($jenis == '2') {
                $data = $data->whereraw("perubahan_znt.verifikasi_at is null");
            }

            return  DataTables::of($data)
                /*   ->filter(function ($query) use ($request) {
                    if ($request->has('status')) {
                        $status = $request->status;
                        // Log::info('status =' . $status);
                        if (is_null($status) == false) {
                            if ($status == 'x') {
                                $query->whereraw("verifikasi_kode is null");
                            } else {
                                $query->whereraw("verifikasi_kode='$status'");
                            }
                        }
                    }
                }) */
                ->filterColumn('pencarian_data', function ($query, $string) {
                    $keyword = strtolower($string);
                    $wh = "(nomor_batch like '%" . $keyword . "%' or lower(nm_kecamatan) like '%" . $keyword . "%' or lower(nm_kelurahan) like '%" . $keyword . "%'  or lower(kd_znt_lama) like '%" . $keyword . "%'   or lower(kd_znt_baru) like '%" . $keyword . "%' )";

                    $query->whereRaw(DB::raw($wh));
                })
                ->addColumn('status', function ($row) {
                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $st = "<i class='far fa-check-circle text-success'></i> Disetujui";
                            break;
                        case '0':
                            # code...
                            $st = "<i class='fas fa-exclamation-circle text-danger'></i> Tidak Disetujui";
                            break;
                        default:
                            # code...
                            $st = "<i class='fas fa-user-clock text-warning'></i> Belum di verifikasi";
                            break;
                    }

                    return $st;
                })
                ->addColumn('pilih', function ($row) {
                    $aksi = "";
                    if ($row->verifikasi_at == '') {
                        $aksi .= " <a  role='button' data-toggle='tooltip'  data-href='" . route('pendataan.merubah-znt.destroy', $row->id) . "' data-placement='top' title='Hapus perubahan'   class='hapus' ><i class='fas fa-trash-alt text-danger '></i></a>";
                    }
                    $aksi .= " <a href='" . route('pendataan.merubah-znt.show', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Detail objek'   class='nop-detail' ><i class='fas text-success fa-binoculars'></i></a>";

                    return $aksi;
                })
                ->addColumn('pilih2', function ($row) {
                    $aksi = "";

                    $aksi .= " <a href='" . route('pendataan.merubah-znt.verifikasi', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Detail objek'   class='nop-detail' ><i class='fas text-success fa-binoculars'></i></a>";

                    return $aksi;
                })
                ->rawColumns(['pilih', 'pilih2', 'status'])->make(true);
        }

        $verifikator = 0;
        return view('pendataan.znt.index', compact('verifikator'));
    }

    public function verifikasiIndex(Request $request)
    {
        return view('pendataan.znt.indexVerfikasi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $kd_blok = $request->kd_blok;
            $kd_znt = $request->kd_znt;
            $tahun = date('Y');

            $tipe = $request->tipe;
            if ($tipe == 'objek') {
                $res = DB::connection('oracle_satutujuh')->table('dat_op_bumi')
                    ->join('dat_objek_pajak', function ($join) {
                        $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                            ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                            ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                            ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                            ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                            ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                            ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
                    })
                    ->join('dat_nir', function ($join) {
                        $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_nir.kd_propinsi')
                            ->on('dat_op_bumi.kd_dati2', '=', 'dat_nir.kd_dati2')
                            ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                            ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_nir.kd_kelurahan')
                            ->on('dat_op_bumi.kd_znt', '=', 'dat_nir.kd_znt');
                    })
                    ->whereraw("dat_op_bumi.kd_Kecamatan='" . $kd_kecamatan . "' and dat_op_bumi.kd_kelurahan='" . $kd_kelurahan . "'
                and jns_bumi in ('1','2','3') and jns_transaksi_op in ('1','2') and dat_nir.thn_nir_znt='$tahun'");
                if ($kd_blok <> '') {
                    $res = $res->whereraw("dat_op_bumi.kd_blok='" . $kd_blok . "'");
                }


                $objek = $res->select(db::raw('dat_op_bumi.*'))->whereraw("dat_op_bumi.kd_znt='$kd_znt'")->orderby("dat_op_bumi.no_urut")->get();
                return view('pendataan/znt/_form_objek', compact('objek'));
            }

            if ($tipe == 'znt') {
                $array = DB::connection("oracle_satutujuh")->select(db::raw("select dat_nir.kd_znt,nir*1000 nir 
                from dat_peta_znt
                join dat_nir on dat_peta_znt.kd_kecamatan=dat_nir.kd_Kecamatan
                                        and dat_peta_znt.kd_kelurahan=dat_nir.kd_kelurahan
                                        and dat_nir.kd_znt=dat_peta_znt.kd_znt 
                                        where  dat_nir.thn_nir_znt='" . $tahun . "'
                                        and dat_peta_znt.kd_kecamatan='" . $kd_kecamatan . "'
                                        and dat_peta_znt.kd_kelurahan='" . $kd_kelurahan . "'
                                        and dat_peta_znt.kd_blok='" . $kd_blok . "'
                                        order by REGEXP_REPLACE (dat_nir.kd_znt, '[^0-9]', '')  nulls first,nir asc"));
                $result = array_map(function ($array) {
                    return (array)$array;
                }, $array);

                return    response()->json($result);
            }
        }

        $data = [
            'action' => route('pendataan.merubah-znt.store'),
            'method' => 'post',
            'kecamatan' => DB::connection('oracle_satutujuh')->table('ref_kecamatan')->select(db::raw('kd_Kecamatan,nm_kecamatan'))->orderby('kd_kecamatan')->get()
        ];
        return view('pendataan.znt.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {

            $data = new PerubahanZnt;
            $data->kd_propinsi = '35';
            $data->kd_dati2 = '07';
            $data->kd_kecamatan = $request->kd_kecamatan;
            $data->kd_kelurahan = $request->kd_kelurahan;
            $data->kd_blok = $request->kd_blok;
            $data->kd_znt_lama = $request->kd_znt_lama;
            $data->kd_znt_baru   = $request->kd_znt_baru;
            $data->keterangan = $request->keterangan;
            $data->save();
            // 
            // $core = PerubahanZnt::insert($data);

            $objek = [];
            foreach ($request->objek as $obj) {
                $aro = splitnop($obj);
                $aro['perubahan_znt_id'] = $data->id;
                $objek[] = $aro;
            }
            PerubahanZntObjek::insert($objek);
            DB::commit();
            $flash = ['success' => 'Perubahan ZNT berhasil di simpan , perubahan akan terjadi apabila sudah di verfikasi oleh KASI'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('pendataan.merubah-znt.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDataDetail($id)
    {
        $data = PerubahanZnt::select(db::raw("perubahan_znt.*,nm_kecamatan,nm_kelurahan,dat_nir.nir*1000 nir_lama, dat_nir_baru.nir*1000 nir_baru,( select nama from users where id=perubahan_znt.created_by) pegawai"))
            ->join(db::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'perubahan_znt.kd_kecamatan')
            ->join(db::raw("pbb.ref_kelurahan ref_kelurahan"), function ($join) {
                $join->on('ref_kelurahan.kd_kecamatan', '=', 'perubahan_znt.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'perubahan_znt.kd_kelurahan');
            })
            ->leftjoin(db::raw("pbb.dat_nir"), function ($join) {
                $join->on(db::raw("dat_nir.kd_propinsi"), '=', DB::raw("perubahan_znt.kd_propinsi"))
                    ->on(db::raw("dat_nir.kd_dati2"), '=', DB::raw("perubahan_znt.kd_dati2"))
                    ->on(db::raw("dat_nir.kd_kecamatan"), '=', DB::raw("perubahan_znt.kd_kecamatan"))
                    ->on(db::raw("dat_nir.kd_kelurahan"), '=', DB::raw("perubahan_znt.kd_kelurahan"))
                    ->on(db::raw("dat_nir.kd_znt"), '=', DB::raw("perubahan_znt.kd_znt_lama"))
                    ->on(db::raw("dat_nir.thn_nir_znt"), '=', DB::raw("to_char(perubahan_znt.created_at,'yyyy')"));
            })
            ->leftjoin(db::raw("pbb.dat_nir dat_nir_baru"), function ($join) {
                $join->on(db::raw("dat_nir_baru.kd_propinsi"), '=', DB::raw("perubahan_znt.kd_propinsi"))
                    ->on(db::raw("dat_nir_baru.kd_dati2"), '=', DB::raw("perubahan_znt.kd_dati2"))
                    ->on(db::raw("dat_nir_baru.kd_kecamatan"), '=', DB::raw("perubahan_znt.kd_kecamatan"))
                    ->on(db::raw("dat_nir_baru.kd_kelurahan"), '=', DB::raw("perubahan_znt.kd_kelurahan"))
                    ->on(db::raw("dat_nir_baru.kd_znt"), '=', DB::raw("perubahan_znt.kd_znt_baru"))
                    ->on(db::raw("dat_nir_baru.thn_nir_znt"), '=', DB::raw("to_char(perubahan_znt.created_at,'yyyy')"));
            })
            ->find($id);

        $objek = PerubahanZntObjek::join(db::raw('pbb.dat_op_bumi'), function ($join) {
            $join->on('dat_op_bumi.kd_propinsi', '=', 'perubahan_znt_objek.kd_propinsi')
                ->on('dat_op_bumi.kd_dati2', '=', 'perubahan_znt_objek.kd_dati2')
                ->on('dat_op_bumi.kd_kecamatan', '=', 'perubahan_znt_objek.kd_kecamatan')
                ->on('dat_op_bumi.kd_kelurahan', '=', 'perubahan_znt_objek.kd_kelurahan')
                ->on('dat_op_bumi.kd_blok', '=', 'perubahan_znt_objek.kd_blok')
                ->on('dat_op_bumi.no_urut', '=', 'perubahan_znt_objek.no_urut')
                ->on('dat_op_bumi.kd_jns_op', '=', 'perubahan_znt_objek.kd_jns_op');
        })->select(db::raw("perubahan_znt_objek.*,luas_bumi"))->whereraw("perubahan_znt_id='" . $id . "'")->get();

        return ['objek' => $objek, 'data' => $data];
    }


    public function show($id)
    {
        $core = $this->getDataDetail($id);
        $data = $core['data'];
        $objek = $core['objek'];

        switch ($data->verifikasi_kode) {
            case '1':
                # code...
                $st = "<i class='far fa-check-circle text-success'></i> Disetujui";
                break;
            case '0':
                # code...
                $st = "<i class='fas fa-exclamation-circle text-danger'></i> Tidak Disetujui";
                break;
            default:
                # code...
                $st = "<i class='fas fa-user-clock text-warning'></i> Belum di verifikasi";
                break;
        }
        return view('pendataan.znt.show', compact('data', 'objek', 'st'));
    }


    public function verifikasi($id)
    {
        $core = $this->getDataDetail($id);

        // return $core;
        if ($core['data']->verifikasi_kode == '') {
            $data = $core['data'];
            $objek = $core['objek'];
            switch ($data->verifikasi_kode) {
                case '1':
                    # code...
                    $st = "<i class='far fa-check-circle text-success'></i> Disetujui";
                    break;
                case '0':
                    # code...
                    $st = "<i class='fas fa-exclamation-circle text-danger'></i> Tidak Disetujui";
                    break;
                default:
                    # code...
                    $st = "<i class='fas fa-user-clock text-warning'></i> Belum di verifikasi";
                    break;
            }
            return view('pendataan.znt.verifikasi', compact('data', 'objek', 'st'));
        }
        abort(404);
    }


    public function verifikasiStore(Request $request, $id)
    {
        // return $request->all();
        // 
        DB::beginTransaction();
        try {
            //code...

            $status = $request->st;
            if ($status == '1') {
                $penetapan = $request->penetapan ?? '0';
                if ($penetapan == '1') {
                    $tahunPerubahan = date('Y');
                } else {
                    $tahunPerubahan = date('Y') + 1;
                }


                // insert /update ke dat_op_znt
                $arb = DB::select(DB::raw("select b.kd_propinsi,
                                                    b.kd_dati2,
                                                    b.kd_Kecamatan,
                                                    b.kd_kelurahan,
                                                    b.kd_blok,
                                                    b.no_urut,
                                                    b.kd_jns_op,
                                                    '$tahunPerubahan' thn_znt,
                                                    kd_znt_baru kd_znt,
                                                    nomor_batch,
                                                    created_at,
                                                    created_by,
                                                    no_bumi,
                                                    luas_bumi
                                            FROM perubahan_znt a
                                                    JOIN perubahan_znt_objek b ON a.id = b.perubahan_znt_id
                                                    JOIN pbb.dat_op_bumi c
                                                    ON     b.kd_propinsi = c.kd_propinsi
                                                        AND b.kd_dati2 = c.kd_dati2
                                                        AND b.kd_Kecamatan = c.kd_Kecamatan
                                                        AND b.kd_kelurahan = c.kd_kelurahan
                                                        AND b.kd_blok = c.kd_blok
                                                        AND b.no_urut = c.no_urut
                                                        AND b.kd_jns_op = c.kd_jns_op
                                                    where perubahan_Znt_id=$id"));


                foreach ($arb as $row) {
                    $ck = DB::connection("oracle_satutujuh")
                        ->table("DAT_OP_ZNT")
                        ->whereraw("kd_propinsi ='$row->kd_propinsi' and   kd_dati2 ='$row->kd_dati2' and    kd_kecamatan ='$row->kd_kecamatan' and kd_kelurahan ='$row->kd_kelurahan' and
                                    kd_blok ='$row->kd_blok' and  no_urut ='$row->no_urut' and  kd_jns_op ='$row->kd_jns_op' and thn_znt ='$row->thn_znt'")
                        ->Select(DB::raw("count(1) jumlah"))
                        ->first();
                    $dataZnt = [
                        'kd_propinsi' => $row->kd_propinsi,
                        'kd_dati2' => $row->kd_dati2,
                        'kd_kecamatan' => $row->kd_kecamatan,
                        'kd_kelurahan' => $row->kd_kelurahan,
                        'kd_blok' => $row->kd_blok,
                        'no_urut' => $row->no_urut,
                        'kd_jns_op' => $row->kd_jns_op,
                        'thn_znt' => $row->thn_znt,
                        'kd_znt' => $row->kd_znt,
                        'nomor_batch' => $row->nomor_batch,
                        'created_at' => new Carbon($row->created_at),
                        'created_by' => $row->created_by,
                    ];

                    // dd($dataZnt);
                    if ($ck->jumlah == 0) {
                        // insert
                        DB::connection("oracle_satutujuh")->table("DAT_OP_ZNT")->insert($dataZnt);
                    } else {
                        // update
                        DB::connection("oracle_satutujuh")
                            ->table("DAT_OP_ZNT")
                            ->whereraw("kd_propinsi ='$row->kd_propinsi' and
                                            kd_dati2 ='$row->kd_dati2' and
                                            kd_kecamatan ='$row->kd_kecamatan' and
                                            kd_kelurahan ='$row->kd_kelurahan' and
                                            kd_blok ='$row->kd_blok' and
                                            no_urut ='$row->no_urut' and
                                            kd_jns_op ='$row->kd_jns_op' and
                                            thn_znt ='$row->thn_znt'")
                            ->update($dataZnt);
                    }
                }

                // penilaiain

                array_map(function ($array) {
                    // return $array->sql;
                    DB::connection("oracle_satutujuh")->statement("   DECLARE 
                        VLC_KD_PROPINSI CHAR(2);
                        VLC_KD_DATI2 CHAR(2);
                        VLC_KD_KECAMATAN CHAR(3);
                        VLC_KD_KELURAHAN CHAR(3);
                        VLC_KD_BLOK CHAR(3);
                        VLC_NO_URUT CHAR(4);
                        VLC_KD_JNS_OP CHAR(1);
                        VLN_NO_BUMI NUMBER;
                        VLC_KD_ZNT CHAR(2);
                        VLN_LUAS_BUMI NUMBER;
                        VLC_TAHUN CHAR(4);
                        VLN_FLAG_UPDATE NUMBER;
                        VLN_NILAI_BUMI NUMBER;
                      
                      BEGIN 
                       
                        VLC_KD_PROPINSI := '" . $array->kd_propinsi . "';
                        VLC_KD_DATI2 := '" . $array->kd_dati2 . "';
                        VLC_KD_KECAMATAN :='" . $array->kd_kecamatan . "';
                        VLC_KD_KELURAHAN := '" . $array->kd_kelurahan . "';
                        VLC_KD_BLOK := '" . $array->kd_blok . "';
                        VLC_NO_URUT := '" . $array->no_urut . "';
                        VLC_KD_JNS_OP :='" . $array->kd_jns_op . "';
                        VLN_NO_BUMI :='" . $array->no_bumi . "';
                        VLC_KD_ZNT := '" . $array->kd_znt . "';
                        VLN_LUAS_BUMI := '" . $array->luas_bumi . "';
                        VLC_TAHUN := '" . date('Y') . "';
                        VLN_FLAG_UPDATE := 1;
                        VLN_NILAI_BUMI := NULL;
                      
                        PBB.PENILAIAN_BUMI ( VLC_KD_PROPINSI, VLC_KD_DATI2, VLC_KD_KECAMATAN, VLC_KD_KELURAHAN, VLC_KD_BLOK, VLC_NO_URUT, VLC_KD_JNS_OP, VLN_NO_BUMI, VLC_KD_ZNT, VLN_LUAS_BUMI, VLC_TAHUN, VLN_FLAG_UPDATE, VLN_NILAI_BUMI );
                        COMMIT; 
                      END;");
                }, $arb);


                // cek penetapan
                if ($request->penetapan) {
                    $penetapan = $request->penetapan;
                    if ($penetapan == '1') {
                        /*    $terbit = date('dmY');
                        $jatuhtempo = date('dmY', strtotime($request->tgl_jatuh_tempo));
                        $tahun = $request->thn_pajak;
                        $nip = '000000000000001003';
 */
                        $ln = DB::table(db::raw("perubahan_znt_objek"))
                            ->whereraw("perubahan_znt_id=$id")->get();

                        foreach ($ln as $rk) {
                            $kd_propinsi = $rk->kd_propinsi;
                            $kd_dati2 = $rk->kd_dati2;
                            $kd_kecamatan = $rk->kd_kecamatan;
                            $kd_kelurahan = $rk->kd_kelurahan;
                            $kd_blok = $rk->kd_blok;
                            $no_urut = $rk->no_urut;
                            $kd_jns_op = $rk->kd_jns_op;
                            $tahun = $request->thn_pajak ?? date('Y');

                            $tgl_terbit = date('Ymd');
                            $tgl_jatuh_tempo = $tahun . '0831';

                            PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun, $tgl_terbit, $tgl_jatuh_tempo);
                        }

                        // dd($ln);



                        /*  $getWhere = DB::table(db::raw("perubahan_znt_objek"))
                            ->join(db::raw("pbb.sppt"), function ($join) {
                                $join->on('perubahan_znt_objek.kd_propinsi', '=', 'sppt.kd_propinsi')
                                    ->on('perubahan_znt_objek.kd_dati2', '=', 'sppt.kd_dati2')
                                    ->on('perubahan_znt_objek.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                                    ->on('perubahan_znt_objek.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                                    ->on('perubahan_znt_objek.kd_blok', '=', 'sppt.kd_blok')
                                    ->on('perubahan_znt_objek.no_urut', '=', 'sppt.no_urut')
                                    ->on('perubahan_znt_objek.kd_jns_op', '=', 'sppt.kd_jns_op');
                            })->select(db::raw("LISTAGG('(kd_propinsi='''||perubahan_znt_objek.kd_propinsi||''' and kd_dati2='''||perubahan_znt_objek.kd_dati2||''' and kd_kecamatan='''||perubahan_znt_objek.kd_kecamatan||''' and kd_kelurahan='''||perubahan_znt_objek.kd_kelurahan||''' and kd_blok='''||perubahan_znt_objek.kd_blok||''' and no_urut='''||perubahan_znt_objek.no_urut||''' and kd_jns_op='''||perubahan_znt_objek.kd_jns_op||''')' , ' or ') WITHIN GROUP (ORDER BY perubahan_znt_objek.kd_propinsi asc) AS whe"))
                            ->whereraw("perubahan_znt_id=$id and thn_pajak_sppt='$tahun'
                                    and status_pembayaran_sppt='0'")->get();

              

                        if ($getWhere->whe <> '') {

                            $sql_core = Pajak::RawSqlPenetapan($tahun, $terbit, $jatuhtempo, $nip);

                            $sql_penetapan = "delete from sppt_potongan where thn_pajak_sppt='$tahun' and (" . $getWhere->whe . ");";
                            $sql_penetapan .= "delete from sppt where thn_pajak_sppt='$tahun' and (" . $getWhere->whe . ");";
                            $sql_penetapan .= "insert into sppt  " . $sql_core . ' where ' . $getWhere->whe . "; ";

                            $sql_penetapan .= "update sim_pbb.perubahan_znt_objek set thn_pajak='$tahun',tgl_jatuh_tempo=to_date('" . $jatuhtempo . "','ddmmyyyy')  where perubahan_znt_id='$id' and (" . $getWhere->whe . ");";

                            DB::connection("oracle_satutujuh")->statement(db::raw("begin " .
                                $sql_penetapan
                                . " commit; end;"));
                        } */
                    }
                }
            }

            PerubahanZnt::find($id)->update([
                'penetapan' => $request->penetapan ?? '',
                'verifikasi_kode' => $status,
                'keterangan' => $request->keterangan,
                'verifikasi_by' => Auth()->user()->id,
                'verifikasi_at' => Carbon::now()
            ]);

            DB::commit();
            $flash = ['success' => 'Data berhasil di verifikasi'];
        } catch (\Throwable $th) {
            DB::rollBack();
            log::error($th->getMessage());
            $flash = ['error' => $th->getMessage()];
        }

        return redirect(route('pendataan.merubah-znt.verifikasi.index'))->with($flash);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            //code...
            PerubahanZnt::find($id)->delete();
            db::commit();
            $flash = ['success' => 'Data berhasil di hapus'];
        } catch (\Throwable $th) {
            $flash = ['error' => $th->getMessage()];
            Db::rollBack();
        }
        return redirect(route('pendataan.merubah-znt.index'))->with($flash);
    }
}
