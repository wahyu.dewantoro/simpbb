<?php

namespace App\Http\Controllers;

use App\Exports\cetak_dhkp_excel;
use App\Exports\DhkpExport;
use App\Exports\DhkpPerbandinganExport;
use App\Exports\DhkpRekapDesaExport;
use App\Exports\DhkpRekapPerbandinganDesaExport;
use App\Exports\DhkpRingkasanBaku;
use App\Exports\DhkpWajibPajakExcel;
use App\FileGenerator;
use App\fileReport;
use App\Helpers\Baku;
use App\Helpers\Dhkp;
use App\Jobs\DhkpFileGenerator;
use App\Kecamatan;
use App\Kelurahan;
use App\pegawaiStruktural;
use App\RefCamat;
use App\Sppt;
use Barryvdh\Snappy\Facades\SnappyPdf as FacadesSnappyPdf;
// use Barryvdh\DomPDF\PDF;
use DOMPDF as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use SplFileInfo;
use Yajra\Datatables\Datatables;
use ZipArchive;
use DOMPDF as dompdf;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use SnappyPdf;
// use Webklex\PDFMerger\PDFMerger;
use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;

// use PhpOffice\PhpSpreadsheet\Writer\Pdf\Tcpdf;


class DhkpController extends Controller
{
    //
    public function ringkasanBaku(Request $request)
    {

        if ($request->ajax()) {
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan ?? null;
            $kd_kelurahan = $request->kd_kelurahan ?? null;

            if ($request->buku != '') {
                if ($request->buku == '1') {
                    $buku = [1, 2];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }


            if ($tahun == date('Y')) {
                $cut = date('Ymd');
            } else {
                $cut = $tahun . '1231';
            }

            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];

            if ($kd_kecamatan != '') {
                $param['kd_kecamatan'] = $kd_kecamatan;
            }

            if ($kd_kelurahan != '') {
                $param['kd_kelurahan'] = $kd_kelurahan;
            }


            $rekap = Baku::RekapBakuDesa($param)->get();
            // return $rekap;
            // ringkasan_baku_dua
            return view('dhkp.ringkasan_baku_dua_table', compact('rekap', 'param'));
        }

        // return view('dhkp.ringkasan_baku_dua', compact('param'));
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $preview = [];
        $original_name = "";
        $encript_name = "";
        $param = "";
        $data = (object)[
            'preview' => $preview,
            'param' => $param,
            'pdf' => $encript_name . '.pdf',
            'excel' => $encript_name . '.xlsx'
        ];

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start = 2003;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }

        $is_internal = Auth()->user()->roles()->pluck('is_internal')->toarray();
        array_unique($is_internal);
        $ordal = '0';
        // if (in_array('1', $is_internal)) {
        if (Auth()->user()->hasAnyRole(
            'ADMIN',
            'Kasi PDI',
            'Kasi Pelayanan',
            'Kepala Bidang',
            'Super User'
        )) {

            $ordal = '1';
        }

        return view('dhkp.ringkasan_baku_dua', compact('kecamatan', 'data', 'role', 'tahun_pajak', 'kelurahan', 'ordal'));
    }

    public function ringkasanBakuExcel(Request $request)
    {
        ini_set('memory_limit', '-1');
        $param['tahun'] = $request->tahun_pajak ?? date('Y');
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        if (in_array('Kecamatan', $role)) {
            $param['kd_kecamatan'] = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
        }
        // dd($param);
        return Excel::download(new DhkpRingkasanBaku($param), 'dhkp ringkasan ' . time() . '.xlsx');
    }

    public function ringkasanBakuPdf(Request $request)
    {
        $param['tahun'] = $request->tahun_pajak ?? date('Y');
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        if (in_array('Kecamatan', $role)) {
            $param['kd_kecamatan'] = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
        }
        $dhkp = Dhkp::ringkasanBaku($param);
        $tahun = $param['tahun'];
        $pdf = PDF::loadView('dhkp/ringkasan_baku_pdf', compact('dhkp', 'tahun'))->setPaper('a4', 'landscape');
        $filename = 'Ringkasan DHKP ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan ?? null;
            $kd_kelurahan = $request->kd_kelurahan ?? null;

            if ($request->buku != '') {
                if ($request->buku == '1') {
                    $buku = [1, 2];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }


            if ($tahun == date('Y')) {
                $cut = date('Ymd');
            } else {
                $cut = $tahun . '1231';
            }

            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];

            if ($kd_kecamatan != '') {
                $param['kd_kecamatan'] = $kd_kecamatan;
            }

            if ($kd_kelurahan != '') {
                $param['kd_kelurahan'] = $kd_kelurahan;
            }

            $param['cut_bayar'] = false;
            $rekap = Baku::RekapDesa($param)->get();
            return view('dhkp.index_dua', compact('rekap', 'param'));
        }

        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $preview = [];
        $original_name = "";
        $encript_name = "";
        $param = "";
        $data = (object)[
            'preview' => $preview,
            'param' => $param,
            'pdf' => $encript_name . '.pdf',
            'excel' => $encript_name . '.xlsx'
        ];

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y')+1;
        $start = 2003;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }

        $is_internal = Auth()->user()->roles()->pluck('is_internal')->toarray();
        array_unique($is_internal);
        $ordal = '0';
        // if (in_array('1', $is_internal)) {
        if (Auth()->user()->hasAnyRole(
            'ADMIN',
            'Kasi PDI',
            'Kasi Pelayanan',
            'Kepala Bidang',
            'Super User'
        )) {

            $ordal = '1';
        }

        return view('dhkp.index', compact('kecamatan', 'data', 'role', 'tahun_pajak', 'kelurahan', 'ordal'));
    }

    public function cetakExcel(Request $request)
    {
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan ?? null;
        $kd_kelurahan = $request->kd_kelurahan ?? null;
        if ($request->buku != '') {
            if ($request->buku == '1') {
                $buku = [1, 2];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }


        if ($tahun == date('Y')) {
            $cut = date('Ymd');
        } else {
            $cut = $tahun . '1231';
        }

        // $cut = "20231231";
        // $buku = [1,2];

        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];

        if ($kd_kecamatan != '') {
            $param['kd_kecamatan'] = $kd_kecamatan;
        }

        if ($kd_kelurahan != '') {
            $param['kd_kelurahan'] = $kd_kelurahan;
        }
        $param['cut_bayar'] = false;

        $data =  Baku::get($param)
            ->join("dat_objek_pajak", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('tmp.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->join("sppt", function ($join) {
                $join->on('tmp.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('tmp.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('tmp.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('tmp.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('tmp.kd_blok', '=', 'sppt.kd_blok')
                    ->on('tmp.no_urut', '=', 'sppt.no_urut')
                    ->on('tmp.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->on('tmp.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
            })
            ->select(DB::raw("tmp.*,nm_wp_sppt,jln_wp_sppt,blok_kav_no_wp_sppt,rw_wp_sppt,rt_wp_sppt,kelurahan_wp_sppt,kota_wp_sppt,
                                luas_bumi_sppt,luas_bng_sppt,tgl_jatuh_tempo_sppt,
                                GET_DENDA ((pbb-potongan),tgl_jatuh_tempo_sppt,sysdate) denda, get_buku(pbb) buku,jalan_op,blok_kav_no_op,rt_op,rw_op
                                "))
            ->whereRaw("nota=0")
            ->orderByRaw("nota nulls first,  tmp.kd_kecamatan,tmp.kd_kelurahan,tmp.kd_blok,tmp.no_urut")
            ->get();

        // return $data;

        $parseData = [];
        $no = 1;
        foreach ($data as $row) {
            $alamat_wp = "";
            if ($row->jln_wp_sppt != '') {
                $alamat_wp .= $row->jln_wp_sppt . " ";
            }
            if ($row->blok_kav_no_wp_sppt != '') {
                $alamat_wp .= $row->blok_kav_no_wp_sppt . " ";
            }

            if ($row->rt_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rt_wp_sppt . " ";
            }
            if ($row->rw_wp_sppt != '') {
                $alamat_wp .= " RW " . $row->rw_wp_sppt . " ";
            }
            if ($row->kelurahan_wp_sppt != '') {
                $alamat_wp .= $row->kelurahan_wp_sppt . " ";
            }
            if ($row->kota_wp_sppt != '') {
                $alamat_wp .= $row->kota_wp_sppt . " ";
            }

            $tgl_bayar = "";
            if ($row->tgl_bayar != '') {

                $tgl_bayar = tglIndo($row->tgl_bayar);
            }

            $tgl_koreksi = "";
            if ($row->tgl_koreksi != '') {
                $tgl_koreksi = tglIndo($row->tgl_koreksi);
            }

            $keterangan = "";
            if ($row->nota > 0) {
                $keterangan .= " Ada nota perhitungan";
            }

            $alamat_op = "";

            if ($row->jalan_op != '') {
                $alamat_op .= $row->jalan_op . " ";
            }

            if ($row->blok_kav_no_op != '') {
                $alamat_op .= $row->blok_kav_no_op . " ";
            }

            if ($row->rt_op != '') {
                $alamat_op .= " RT " . $row->rt_op . " ";
            }

            if ($row->rw_op != '') {
                $alamat_op .= " RW " . $row->rw_op . " ";
            }

            $pbb_awal = $row->pbb_last;
            $pbb_perubahan = $row->perubahan;

            $parseData[] = [
                $no++,
                formatnop($row->kd_propinsi .
                    $row->kd_dati2 .
                    $row->kd_kecamatan .
                    $row->kd_kelurahan .
                    $row->kd_blok .
                    $row->no_urut .
                    $row->kd_jns_op),
                $alamat_op,
                $row->nm_wp_sppt,
                $alamat_wp,
                $row->luas_bumi_sppt,
                $row->luas_bng_sppt,
                'Buku ' . $row->buku,
                $pbb_awal,
                $pbb_perubahan,
                $row->pbb,
                $row->potongan,
                ($row->pbb - $row->potongan),
                $row->denda,
                $row->koreksi,
                $tgl_koreksi,
                $row->bayar,
                $tgl_bayar,
                $keterangan
            ];
        }
        // dd($parseData);
        // dd(count($parseData));


        // return $parseData;
        return Excel::download(new DhkpWajibPajakExcel($parseData), 'dhkp wajib pajak ' . $kd_kecamatan . ' - ' . $kd_kelurahan . ' tahun ' . $tahun . '.xlsx');


        // return $param;


        // ini_set('memory_limit', '-1');
        // $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;
        // $kelurahan = Kelurahan::where('kd_kecamatan', $request->kd_kecamatan)->where('kd_kelurahan', $request->kd_kelurahan)->first()->nm_kelurahan ?? '';
        // return Excel::download(new DhkpExport($request->tahun, $request->kd_kecamatan, $request->kd_kelurahan, $request->buku), 'dhkp ' . $kecamatan . ' - ' . $kelurahan . ' tahun ' . $request->tahun . '.xlsx');
    }

    public function cetakPdf(Request $request)
    {
        ini_set('max_execution_time', '0'); // for infinite time of execution 

        $buku = $request->buku;
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;
        $kelurahan = Kelurahan::where('kd_kecamatan', $request->kd_kecamatan)->where('kd_kelurahan', $request->kd_kelurahan)->first()->nm_kelurahan ?? '';

        $variable = [
            'tahun' => $tahun,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'buku' => $buku,
        ];
        $dhkp = Dhkp::DhkpWepe($variable);
        $buku = implode(',', $buku);
        $pdf = PDF::loadView('dhkp/_cetak_pdf', compact('dhkp', 'buku', 'tahun', 'kelurahan', 'kecamatan'))->setPaper('a4', 'landscape');
        $filename = 'DHKP wajib pajak ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }

    public function rekapDesa(Request $request)
    {
        if ($request->ajax()) {
            /*  $param = $request->getQueryString();
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $buku = $request->buku;
            $preview = Dhkp::RekapDesa($tahun, $kd_kecamatan, $buku, $kd_kelurahan);
            $data = (object)[
                'preview' => $preview,
                'param' => $param
            ]; */

            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan ?? null;
            $kd_kelurahan = $request->kd_kelurahan ?? null;

            if ($request->buku != '') {
                if ($request->buku == '1') {
                    $buku = [1, 2];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }


            if ($tahun == date('Y')) {
                $cut = date('Ymd');
            } else {
                $cut = $tahun . '1231';
            }

            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];

            if ($kd_kecamatan != '') {
                $param['kd_kecamatan'] = $kd_kecamatan;
            }

            if ($kd_kelurahan != '') {
                $param['kd_kelurahan'] = $kd_kelurahan;
            }

            $filter = $request->getQueryString();


            $cn = implode("", $buku) . $kd_kecamatan . $kd_kelurahan . $cut . "dhkp_rekap_desa";
            $seconds = 5000;
            $rekap = Cache::remember($cn, $seconds, function () use ($param) {
                return  Baku::RekapDesa($param)->get();
            });

            // dd($rekap);

            return view('dhkp.rekap_desa_dua', compact('rekap', 'filter'));
        }
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        return view('dhkp/rekap_desa', compact('kecamatan'));
    }

    public function rekapDesaCetak(Request $request)
    {
        ini_set('memory_limit', '-1');
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan ?? null;
        $kd_kelurahan = $request->kd_kelurahan ?? null;

        if ($request->buku != '') {
            if ($request->buku == '1') {
                $buku = [1, 2];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }


        if ($tahun == date('Y')) {
            $cut = date('Ymd');
        } else {
            $cut = $tahun . '1231';
        }

        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];

        if ($kd_kecamatan != '') {
            $param['kd_kecamatan'] = $kd_kecamatan;
        }

        if ($kd_kelurahan != '') {
            $param['kd_kelurahan'] = $kd_kelurahan;
        }

        $filter = $request->getQueryString();


        $cn = implode("", $buku) . $kd_kecamatan . $kd_kelurahan . $cut . "dhkp_rekap_desa";
        $seconds = 5000;
        $rekap = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::RekapDesa($param)->get();
        });


        $parseData = [];
        foreach ($rekap as $row) {
            # code...
            $parseData[] = [
                $row->kd_kecamatan . ' - ' . $row->nm_kecamatan,
                $row->kd_kelurahan . ' - ' . $row->nm_kelurahan,
                $row->jumlah_op,
                $row->baku,
                $row->potongan,
                $row->bayar,
                $row->koreksi,
                $row->sisa
            ];
        }

        $filename = 'Rekap DHKP desa  ' . $request->tahun . '.xlsx';
        return Excel::download(new DhkpRekapDesaExport($parseData), $filename);

        // return $parseData;
        // return $rekap;


        /*   $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;
        $kelurahan = "";
        if (!empty($request->kd_kelurahan)) {
            $kelurahan = Kelurahan::where('kd_kecamatan', $request->kd_kecamatan)->where('kd_kelurahan', $request->kd_kelurahan)->first()->nm_kelurahan;
        }
        $buku = $request->buku;
        switch ($buku) {
            case '1':
                # code...
                $jns_buku = 'Buku 1 & 2';
                break;
            case '2':
                # code...
                $jns_buku = 'Buku 3, 4 dan 5';
                break;

            default:
                # code...
                $jns_buku = "";
                break;
        }
        $filename = 'dhkp rekap per desa kecamatan ' . $jns_buku . ' ' . $kecamatan . '  ' . $kelurahan . ' tahun  pajak ' . $request->tahun . '.xlsx';
        return Excel::download(new DhkpRekapDesaExport($request->tahun, $request->kd_kecamatan, $request->buku, $request->kd_kelurahan), $filename); */
    }

    public function rekapDesaCetakPdf(Request $request)
    {
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan ?? null;
        $kd_kelurahan = $request->kd_kelurahan ?? null;

        if ($request->buku != '') {
            if ($request->buku == '1') {
                $buku = [1, 2];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }


        if ($tahun == date('Y')) {
            $cut = date('Ymd');
        } else {
            $cut = $tahun . '1231';
        }

        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];

        if ($kd_kecamatan != '') {
            $param['kd_kecamatan'] = $kd_kecamatan;
        }

        if ($kd_kelurahan != '') {
            $param['kd_kelurahan'] = $kd_kelurahan;
        }

        $filter = $request->getQueryString();


        $cn = implode("", $buku) . $kd_kecamatan . $kd_kelurahan . $cut . "dhkp_rekap_desa";
        $seconds = 5000;
        $rekap = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::RekapDesa($param)->get();
        });


        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan;

        $kelurahan = "";
        if ($kd_kelurahan != '') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan;
            //    dd($kelurahan);
            // ->first()->nm_kelurahan;
        }

        switch ($request->buku) {
            case '1':
                # code...
                $jns_buku = 'Buku 1 & 2';
                break;
            case '2':
                # code...
                $jns_buku = 'Buku 3, 4 dan 5';
                break;

            default:
                # code...
                $jns_buku = "";
                break;
        }
        $filename = 'dhkp rekap per desa kecamatan ' . $jns_buku . ' ' . $kecamatan . '  ' . $kelurahan . ' tahun  pajak ' . $request->tahun . '.pdf';
        $pdf = PDF::loadView('dhkp/rekap_desa_pdf', compact('rekap', 'buku', 'tahun', 'kecamatan', 'kelurahan', 'jns_buku'))->setPaper('a4', 'landscape');

        return $pdf->download($filename);
        // return $rekap;

        /*   $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan;
        $buku = $request->buku;
        $kd_kelurahan = $request->kd_kelurahan;
        $preview = Dhkp::RekapDesa($tahun, $kd_kecamatan, $buku, $kd_kelurahan);
        $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;

        $kelurahan = "";
        if (!empty($request->kd_kelurahan)) {
            $kelurahan = Kelurahan::where('kd_kecamatan', $request->kd_kecamatan)->where('kd_kelurahan', $request->kd_kelurahan)->first()->nm_kelurahan;
        }

        switch ($buku) {
            case '1':
                # code...
                $jns_buku = 'Buku 1 & 2';
                break;
            case '2':
                # code...
                $jns_buku = 'Buku 3, 4 dan 5';
                break;

            default:
                # code...
                $jns_buku = "";
                break;
        }
        $filename = 'dhkp rekap per desa kecamatan ' . $jns_buku . ' ' . $kecamatan . '  ' . $kelurahan . ' tahun  pajak ' . $request->tahun . '.pdf';
        $pdf = PDF::loadView('dhkp/rekap_desa_pdf', compact('preview', 'buku', 'tahun', 'kecamatan', 'kelurahan', 'jns_buku'))->setPaper('a4', 'landscape');

        return $pdf->download($filename); */
    }

    public function rekapDesaPerbandingan(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        if ($request->tahun && $request->kd_kecamatan) {
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan;
            $buku = $request->buku;
            $dhkp = Dhkp::RekapPerbandinganDesa($tahun, $kd_kecamatan, $buku);
            $preview = $dhkp;
        } else {
            $preview = [];
        }
        $data = (object)[
            'preview' => $preview,
        ];

        // dd($data->preview);
        return view('dhkp/rekap_desa_perbandingan', compact('kecamatan', 'data'));
    }

    public function rekapDesaPerbanCetak(Request $request)
    {
        ini_set('memory_limit', '-1');
        $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;
        return Excel::download(new DhkpRekapPerbandinganDesaExport($kecamatan, $request->tahun, $request->kd_kecamatan, $request->buku), 'rekap dhkp  perbandingan per desa kecamatan ' . $kecamatan . ' tahun  pajak ' . $request->tahun . '.xlsx');
    }

    public function rekapDesaPerbanCetakPdf(Request $request)
    {
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan;
        $buku = $request->buku;
        $dhkp = Dhkp::RekapPerbandinganDesa($tahun, $kd_kecamatan, $buku);
        $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;
        $pdf = PDF::loadView('dhkp/rekap_desa_perbandingan_pdf', compact('dhkp', 'buku', 'tahun', 'kecamatan'))->setPaper('a4', 'landscape');
        $filename = 'Rekap desa dhkp ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }

    public function DesaPerbandingan(Request $request)
    {
        $param = '';
        if ($request->tahun && $request->kd_kecamatan) {
            $param = $request->getQueryString();
            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan == null ? 'all' : $request->kd_kelurahan;
            $buku = $request->buku;
            $preview = Dhkp::PerbandinganWepeRekap($tahun, $kd_kecamatan, $kd_kelurahan, $buku);
        } else {
            $preview = [];
        }

        $data = [
            'preview' => $preview,
        ];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        return view('dhkp/perbandingan_dhkp', compact('kecamatan', 'data', 'param'));
    }

    public function DesaPerbandinganPdf(Request $request)
    {
        // ini_set('memory_limit', '-1');
        $tahun = $request->tahun;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $buku = $request->buku;
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan;
        $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan;
        $preview = Dhkp::PerbandinganWepe($tahun, $kd_kecamatan, $kd_kelurahan, $buku)->get();

        // dd($preview);

        $pdf = PDF::loadView('dhkp/_cetak_perbadingan_desa_pdf', [
            'data' => $preview,
            'tahun' => $tahun,
            'buku' => $buku,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan
        ])->setPaper('a4', 'landscape');
        $filename = 'Rekap dhkp perbandingan desa  ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }

    public function DesaPerbandinganExcel(Request $request)
    {
        ini_set('memory_limit', '-1');
        $kecamatan = Kecamatan::where('kd_kecamatan', $request->kd_kecamatan)->first()->nm_kecamatan;
        $kelurahan = Kelurahan::where('kd_kecamatan', $request->kd_kecamatan)->where('kd_kelurahan', $request->kd_kelurahan)->first()->nm_kelurahan;
        $buku = $request->buku;
        return Excel::download(new DhkpPerbandinganExport($request->tahun, $request->kd_kecamatan, $request->kd_kelurahan, $buku), 'perbandingan DHKP desa ' . $kelurahan . ' kecamatan ' . $kecamatan . ' tahun  pajak ' . $request->tahun . '.xlsx');
    }

    public function dokumenDhkp(Request $request)
    {
        $data = fileReport::Dhkp()
            ->select(db::raw("file_report.*,nm_kecamatan,nm_kelurahan"))
            ->leftjoin('spo.ref_kecamatan', function ($join) {
                $join->on('file_report.kd_kecamatan', '=', 'spo.ref_kecamatan.kd_kecamatan');
            })
            ->leftjoin('spo.ref_kelurahan', function ($join) {
                $join->on('file_report.kd_kecamatan', '=', 'spo.ref_kelurahan.kd_kecamatan')
                    ->on('file_report.kd_kelurahan', '=', 'spo.ref_kelurahan.kd_kelurahan');
            });
        if ($request->cari) {
            $q = strtolower($request->cari);
            $data = $data->whereraw("lower(nm_kecamatan) like '%$q%' or lower(nm_kelurahan) like '%$q%'");
        }
        $data = $data->orderby('created_at', 'desc')->paginate(10);
        return view('dhkp/dokumen', compact('data'));
    }

    public function dokumenDhkpDownload()
    {

        $zip = new ZipArchive;
        $ZipfileName = Auth()->user()->id . 'dhkp.zip';
        Storage::disk('reports')->delete($ZipfileName);
        if ($zip->open(Storage::disk('reports')->path($ZipfileName), ZipArchive::CREATE) === TRUE) {
            // $files = \File::files(Storage::disk('dokumen')->path(''));
            $data = fileReport::Dhkp()->get();
            foreach ($data as $rk) {
                $filename = $rk->filename;

                $file = new SplFileInfo(Storage::disk('reports')->path($filename));
                $filename = basename($file);
                $zip->addFile($file, $filename);
            }

            $zip->close();
        }

        return response()->download(Storage::disk('reports')->path($ZipfileName));
    }

    public function FormCetak(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        return view('dhkp.formCetak', compact('kecamatan'));
    }

    public function FormCetakRender(Request $request)
    {
        $tahun_pajak = $request->tahun_pajak ?? date('Y');
        $buku = $request->buku;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;


        switch ($buku) {
            case '1':
                # code...
                $jns = 'I, II';
                $kd_buku = '1,2';
                break;
            case '2':
                # code...
                $kd_buku = '3,4,5';
                $jns = 'III, IV, V';
                break;
            default:
                # code...
                $kd_buku = '1,2,3,4,5';
                $jns = 'I, II, III, IV, V';
                break;
        }


        $param = [
            'tahun' => $tahun_pajak,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'buku' => $kd_buku
        ];

        $data = Dhkp::exportExcelWepe($param);

        $pbb = 0;
        $potongan = 0;

        foreach ($data as $qq) {
            $pbb += $qq->pbb_yg_harus_dibayar_sppt;
            $potongan += $qq->potongan;
        }


        $pagesppt = $data->chunk(30);
        $tempat = DB::connection("oracle_satutujuh")->table('ref_kecamatan')
            ->join('ref_kelurahan', 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
            ->whereraw("ref_kelurahan.kd_Kecamatan='$kd_kecamatan' and ref_kelurahan.kd_kelurahan='$kd_kelurahan'")
            ->first();
        $kaban = pegawaiStruktural::whereraw("kode='01'")->first();
        $camat = RefCamat::whereraw("trunc(sysdate) between tgl_mulai and tgl_selesai
        and kd_kecamatan='$kd_kecamatan'")
            ->selectraw("nm_camat,nip_camat")
            ->first();

        $filepdf = [];
        $oMerger = PDFMerger::init();

        $total_page = count($pagesppt);
        // cover
        $filecover = 'coverk_dhkp_' . auth()->user()->id . '.pdf';
        $pdfcover = PDF::loadView('dhkp/header_dhkp', compact('tempat', 'tahun_pajak', 'jns'))
            ->setPaper('A4', 'landscape');

        if (Storage::disk('sppt')->exists($filecover)) {
            Storage::disk('sppt')->delete($filecover);
        }

        $diska = Storage::disk('sppt');
        $diska->put($filecover, $pdfcover->output());
        $oMerger->addPDF($diska->path($filecover), 'all', 'L');
        $filepdf[] = $filecover;

        $halaman = 1;
        $no_urut = 1;
        foreach ($pagesppt as $i => $sppt) {
            $last = false;
            if ($total_page == $halaman) {
                $last = true;
            }
            $filehalaman = 'cetak_dhkp_' . $i . '_' . auth()->user()->id . '.pdf';
            $pdf = PDF::loadView('dhkp/cetak_baku_pdf', compact('sppt', 'tempat', 'kaban', 'jns', 'tahun_pajak', 'camat', 'halaman', 'no_urut', 'last', 'pbb', 'potongan'))
                ->setPaper('A4', 'landscape');

            if (Storage::disk('sppt')->exists($filehalaman)) {
                Storage::disk('sppt')->delete($filehalaman);
            }
            $disk = Storage::disk('sppt');
            $disk->put($filehalaman, $pdf->output());
            $oMerger->addPDF($disk->path($filehalaman), 'all', 'L');
            $filepdf[] = $filehalaman;
            $halaman++;
            $no_urut += 30;
        }
        $oMerger->merge();

        foreach ($filepdf as $file) {
            if (Storage::disk('sppt')->exists($file)) {
                Storage::disk('sppt')->delete($file);
            }
        }

        $filename = 'dhkp_' . auth()->user()->id . '.pdf';
        if (Storage::disk('sppt')->exists($filename)) {
            Storage::disk('sppt')->delete($filename);
        }

        Storage::disk('sppt')->put($filename, $oMerger->output());
        $resfile = url('preview-esppt') . '?nama_file=' . $filename;
        return "<iframe  src='" . $resfile . "' width='100%' height='650px' ></iframe>";
    }

    public function prosesExcel(Request $request)
    {
        $tahun_pajak = $request->tahun_pajak ?? date('Y');
        $buku = $request->buku;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;


        switch ($buku) {
            case '1':
                # code...
                $jns = 'I, II';
                $kd_buku = '1,2';
                break;
            case '2':
                # code...
                $kd_buku = '3,4,5';
                $jns = 'III, IV, V';
                break;
            default:
                # code...
                $kd_buku = '1,2,3,4,5';
                $jns = 'I, II, III, IV, V';
                break;
        }


        $param = [
            'tahun' => $tahun_pajak,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'buku' => $kd_buku
        ];

        $sppt = Dhkp::exportExcelWepe($param);
        // dd($data);
        $arrayData = [];

        $no = 1;
        $a = 0;
        $b = 0;
        $c = 0;


        foreach ($sppt as $row) {
            $alamat = $row->jalan_op . ' ' . $row->blok_kav_no_op . ' RT ' . $row->rt_op . ' RW ' . $row->rw_op;
            $arrayData[] = [
                $no,
                $row->nop,
                substr($alamat, 0, 33),
                trim($row->nm_wp_sppt),
                ($row->luas_bumi_sppt),
                ($row->luas_bng_sppt),
                ($row->pbb_yg_harus_dibayar_sppt),
                ($row->potongan),
                (($row->pbb_yg_harus_dibayar_sppt - $row->potongan)),
            ];

            $a += $row->pbb_yg_harus_dibayar_sppt;
            $b += $row->potongan;
            $c += ($row->pbb_yg_harus_dibayar_sppt - $row->potongan);

            $no++;
        }


        // dd($arrayData);
        return Excel::download(new cetak_dhkp_excel($arrayData), 'data DHKP ' . $jns . '.xlsx');
    }
}
