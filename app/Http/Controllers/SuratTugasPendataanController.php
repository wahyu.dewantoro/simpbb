<?php

namespace App\Http\Controllers;

use App\Models\StpObjek;
use App\Models\StpPegawai;
use App\Models\SuratTugasPendataan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use Yajra\DataTables\DataTables;
use SnappyPdf as PDF;

class SuratTugasPendataanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {


            $data = SuratTugasPendataan::selectraw("id,tanggal_mulai,surat_nomor, surat_tanggal, lokasi, (SELECT LISTAGG (nama, ', ') WITHIN GROUP (ORDER BY id) FROM srt_tgs_pendataan_pegawai WHERE srt_tgs_pendataan_id=srt_tgs_pendataan.id) pegawai, (SELECT COUNT (1) FROM srt_tgs_pendataan_objek WHERE srt_tgs_pendataan_id=srt_tgs_pendataan.id) jum_objek")->orderby('id', 'desc');
            return  DataTables::of($data)
                ->filterColumn('pencarian_data', function ($query, $string) {
                    $keyword = strtolower($string);
                    // $nopproses = onlyNumber($string);
                    $wh = "lower(lokasi) like '%$keyword%' or lower(surat_nomor) like '%$keyword%'  or 
                    lower((SELECT LISTAGG (nama, ', ') WITHIN GROUP (ORDER BY id) FROM srt_tgs_pendataan_pegawai WHERE srt_tgs_pendataan_id=srt_tgs_pendataan.id)) like '%$keyword%'                 
                     ";
                    $query->whereRaw(DB::raw($wh));
                })
                ->addColumn('tanggal_indo', function ($row) {
                    return tglIndo($row->surat_tanggal);
                })
                ->addColumn('tanggal_pendataan', function ($row) {
                    return tglIndo($row->tanggal_mulai);
                })
                ->addColumn('pilih', function ($row) {
                    $aksi = "";
                    $aksi .= " <a href='" . route('surat-tugas.edit', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Edit Surat Tugas'   class='' ><i class='fas text-success fa-edit'></i></a>";

                    $aksi .= " <a  role='button' data-toggle='tooltip'  data-href='" . route('surat-tugas.delete', $row->id) . "' data-placement='top' title='Hapus Surat Tuugas'   class='hapus' ><i class='fas fa-trash-alt text-danger '></i></a>";

                    $aksi .= " <a href='" . route('surat-tugas.show', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Detail Surat Tugas'   class='nop-detail' ><i class='fas text-success fa-binoculars'></i></a>";


                    return $aksi;
                })
                ->rawColumns(['pilih',  'tanggal_pendataan', 'tanggal_indo'])->make(true);
        }

        return view('pendataan.surattugas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'action' => route('surat-tugas.store'),
            'method' => 'post'
        ];
        return view('pendataan.surattugas.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tanggal_mulai' => 'required',
            'nama' => 'required',
            'nama.*'  => 'required',
            'kd_propinsi' => 'required',
            'kd_propinsi.*'  => 'required'
        ], [
            'tanggal_mulai.required' => 'Tanggal harus di isi.',
            'nama.required' => 'Pegawai harus di sertakan.',
            'kd_propinsi.required' => 'Objek harus di sertakan.'
        ]);
        $core = [];
        DB::beginTransaction();
        try {
            //code...
            $lokasi = [];
            foreach ($request->nm_kelurahan as $i => $nm_kelurahan) {
                $lokasi[$request->nm_kecamatan[$i]][] = $nm_kelurahan;
            }

            $vdesa = [];
            $arkec = [];
            foreach ($lokasi as $kec =>  $desa) {
                foreach ($desa as $i => $desa) {
                    $vdesa[$kec][] = 'Ds ' . $desa;
                }
                $arkec[] = implode(', ', array_unique($vdesa[$kec])) . ' Kec ' . $kec;
            }
            $strlokasi = implode('; ', $arkec);

            $core = SuratTugasPendataan::create(
                [
                    'tanggal_mulai' => new Carbon($request->tanggal_mulai),
                    'lokasi' => $strlokasi
                ]
            );
            $pegawai = [];
            foreach ($request->nama as $i => $nama) {
                $pegawai[] = [
                    'id' => Uuid::uuid1()->toString(),
                    'srt_tgs_pendataan_id' => $core->id,
                    'nama' => $request->nama[$i],
                    'nip' => $request->nip[$i],
                    'pangkat_golongan' => $request->pangkat_golongan[$i],
                    'jabatan' => $request->jabatan[$i],
                ];
            }
            StpPegawai::insert($pegawai);

            $obj = [];
            foreach ($request->kd_propinsi as $i => $prop) {
                $obj[] = [
                    'id' => Uuid::uuid1()->toString(),
                    'srt_tgs_pendataan_id' => $core->id,
                    'kd_propinsi' => $request->kd_propinsi[$i],
                    'kd_dati2' => $request->kd_dati2[$i],
                    'kd_kecamatan' => $request->kd_kecamatan[$i],
                    'kd_kelurahan' => $request->kd_kelurahan[$i],
                    'kd_blok' => $request->kd_blok[$i],
                    'no_urut' => $request->no_urut[$i],
                    'kd_jns_op' => $request->kd_jns_op[$i],
                    'nama_wp' => $request->nm_wp[$i],
                    'jalan_op' => $request->jalan_op[$i],
                    'blok_kav_no_op' => $request->blok_kav_no_op[$i],
                    'rw_op' => $request->rw_op[$i],
                    'rt_op' => $request->rt_op[$i],
                    'nm_kelurahan' => $request->nm_kelurahan[$i],
                    'nm_kecamatan' => $request->nm_kecamatan[$i],
                    'luas_bumi' => $request->total_luas_bumi[$i],
                    'luas_bng' => $request->total_luas_bng[$i],
                    'kd_znt' => $request->kd_znt[$i]
                ];
            }
            StpObjek::insert($obj);

            DB::commit();

            $flash = [
                'success' => 'Berhasil membuat surat tugas'
            ];
        } catch (\Throwable $th) {
            Log::error($th);
            DB::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(route('surat-tugas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $surat = SuratTugasPendataan::with(['pegawai', 'objek'])->find($id);
        return view('pendataan.surattugas.show', compact('surat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = SuratTugasPendataan::with(['pegawai', 'objek'])->find($id);
        $data = [
            'action' => route('surat-tugas.update', $id),
            'method' => 'patch',
            'surat' => $res
        ];
        return view('pendataan.surattugas.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'tanggal_mulai' => 'required',
            'nama' => 'required',
            'nama.*'  => 'required',
            'kd_propinsi' => 'required',
            'kd_propinsi.*'  => 'required'
        ], [
            'tanggal_mulai.required' => 'Tanggal harus di isi.',
            'nama.required' => 'Pegawai harus di sertakan.',
            'kd_propinsi.required' => 'Objek harus di sertakan.'
        ]);





        DB::beginTransaction();
        try {
            //code...
            $lokasi = [];
            foreach ($request->nm_kelurahan as $i => $nm_kelurahan) {
                $lokasi[$request->nm_kecamatan[$i]][] = $nm_kelurahan;
            }

            $vdesa = [];
            $arkec = [];
            foreach ($lokasi as $kec =>  $desa) {
                foreach ($desa as $i => $desa) {
                    $vdesa[$kec][] = 'Ds ' . $desa;
                }
                $arkec[] = implode(', ', array_unique($vdesa[$kec])) . ' Kec ' . $kec;
            }
            $strlokasi = implode('; ', $arkec);

            SuratTugasPendataan::where('id', $id)->update(
                [
                    'tanggal_mulai' => new Carbon($request->tanggal_mulai),
                    'lokasi' => $strlokasi
                ]
            );
            $pegawai = [];
            foreach ($request->nama as $i => $nama) {
                $pegawai[] = [
                    'id' => Uuid::uuid1()->toString(),
                    'srt_tgs_pendataan_id' => $id,
                    'nama' => $request->nama[$i],
                    'nip' => $request->nip[$i],
                    'pangkat_golongan' => $request->pangkat_golongan[$i],
                    'jabatan' => $request->jabatan[$i],
                ];
            }
            StpPegawai::where('srt_tgs_pendataan_id', $id)->delete();
            StpPegawai::insert($pegawai);

            $obj = [];
            foreach ($request->kd_propinsi as $i => $prop) {
                $obj[] = [
                    'id' => Uuid::uuid1()->toString(),
                    'srt_tgs_pendataan_id' => $id,
                    'kd_propinsi' => $request->kd_propinsi[$i],
                    'kd_dati2' => $request->kd_dati2[$i],
                    'kd_kecamatan' => $request->kd_kecamatan[$i],
                    'kd_kelurahan' => $request->kd_kelurahan[$i],
                    'kd_blok' => $request->kd_blok[$i],
                    'no_urut' => $request->no_urut[$i],
                    'kd_jns_op' => $request->kd_jns_op[$i],
                    'nama_wp' => $request->nm_wp[$i],
                    'jalan_op' => $request->jalan_op[$i],
                    'blok_kav_no_op' => $request->blok_kav_no_op[$i],
                    'rw_op' => $request->rw_op[$i],
                    'rt_op' => $request->rt_op[$i],
                    'nm_kelurahan' => $request->nm_kelurahan[$i],
                    'nm_kecamatan' => $request->nm_kecamatan[$i],
                    'luas_bumi' => $request->total_luas_bumi[$i],
                    'luas_bng' => $request->total_luas_bng[$i],
                    'kd_znt' => $request->kd_znt[$i]
                ];
            }
            StpObjek::where('srt_tgs_pendataan_id', $id)->delete();
            StpObjek::insert($obj);

            DB::commit();

            $flash = [
                'success' => 'Berhasil update surat tugas'
            ];
        } catch (\Throwable $th) {
            Log::error($th);
            DB::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(route('surat-tugas.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        db::beginTransaction();
        try {
            //code...
            SuratTugasPendataan::where('id', $id)->delete();
            db::commit();
            $flash = [
                'success' => 'Berhasil hapus data'
            ];
        } catch (\Throwable $th) {
            Log::error($th);
            DB::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(route('surat-tugas.index'))->with($flash);
    }

    public function cetak($id)
    {
        $st = SuratTugasPendataan::with(['pegawai', 'objek'])->find($id);
        $pages = view('pendataan/surattugas/cetak_sk', compact('st'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4')
            ->setOption('margin-left', '19')
            ->setOption('margin-right', '15')
            ->setOption('margin-top', '12.5')
            ->setOption('margin-bottom', '20');
        $filename = 'surat tugas ' . str_replace('/', '_', $st->surat_nomor) . '.pdf';
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->download($filename);
    }

    public function cetakLampiran($id)
    {
        $st = SuratTugasPendataan::with(['pegawai', 'objek'])->find($id);
        $pages = view('pendataan/surattugas/lampiran_sk', compact('st'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4')
            ->setOption('margin-left', '5')
            ->setOption('margin-right', '5')
            ->setOption('margin-top', '5')
            ->setOption('margin-bottom', '5')
            ->setOption('orientation', 'landscape');
        $filename = 'lampiran ' . str_replace('/', '_', $st->surat_nomor) . '.pdf';
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->download($filename);
    }
}
