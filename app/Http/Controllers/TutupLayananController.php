<?php

namespace App\Http\Controllers;

use App\Models\TutupLayanan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TutupLayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TutupLayanan::get();
        return view('tutuplayanan/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = [
            'action' => route('refrensi.tutup-layanan.store'),
            'method' => 'post'
        ];
        return view('tutuplayanan/form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = [
                'deskripsi' => $request->deskripsi,
                'tgl_mulai' => new Carbon($request->tgl_mulai),
                'tgl_selesai' => new Carbon($request->tgl_selesai),
                'created_by' => auth()->user()->id,
                'created_at' => carbon::now()
            ];
            TutupLayanan::create($data);
            DB::commit();
            $flash = ['success' => 'Berhasil di simpan'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.tutup-layanan.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tl = TutupLayanan::find($id);
        $data = [
            'action' => route('refrensi.tutup-layanan.update', $id),
            'method' => 'patch',
            'tl' => $tl
        ];
        return view('tutuplayanan/form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = [
                'deskripsi' => $request->deskripsi,
                'tgl_mulai' => new Carbon($request->tgl_mulai),
                'tgl_selesai' => new Carbon($request->tgl_selesai),
                'updated_by' => auth()->user()->id,
                'updated_at' => carbon::now()
            ];
            TutupLayanan::find($id)->update($data);
            DB::commit();
            $flash = ['success' => 'Berhasil di simpan'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.tutup-layanan.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            TutupLayanan::find($id)->delete();
            DB::commit();
            $flash = ['success' => 'Berhasil di hapus'];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.tutup-layanan.index'))->with($flash);
    }
}
