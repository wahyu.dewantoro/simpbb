<?php

namespace App\Http\Controllers;

use App\Helpers\Layanan_conf;
use App\Http\Requests\LayananMandiriRequest;
use App\Jobs\disposisiPenelitian;
use App\Models\Jenis_layanan;
use App\Models\JenisPengurangan;
use App\Models\Layanan;
use App\Models\Layanan_dokumen;
use App\Models\Layanan_objek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class LayananMandiriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = strtolower(($request->search ?? ''));
        $data = Layanan::join('layanan_objek', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
            ->select([
                'layanan.nomor_layanan',
                'tanggal_layanan',
                'jenis_layanan_nama',
                'nama',
                'kd_propinsi',
                'kd_dati2',
                'kd_kecamatan',
                'kd_kelurahan',
                'kd_blok',
                'no_urut',
                'kd_jns_op',
                DB::raw("case when is_tolak is null then 
                case when nomor_formulir is not null then 'Telah di proses' else 'Belum diproses' end
              else 'Ditolak' end status_permohonan")
            ])
            ->whereraw("is_online is not null and layanan.deleted_at is null")
            ->orderby('layanan.tanggal_layanan', 'desc');


        if (Auth()->user()->hasRole('Wajib Pajak')) {
            $data = $data->where('layanan.created_by', Auth()->user()->id);
        }

        $appends = [];
        if ($search != '') {
            $search = trim(strtolower($search));
            $angka = onlyNumber($search);
            $data = $data->whereraw("(
                lower(layanan.nomor_layanan) like '%$angka%' or
                lower(tanggal_layanan) like '%$search%' or
                lower(jenis_layanan_nama) like '%$search%' or
                lower(nama) like '%$search%' or 
                kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op  like '%$angka%' 
                            )");
            $appends = ['search' => $search];
        }
        $data = $data->paginate()->appends($appends);

        return view('mandiri.index', compact('data'));

        // return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $nop = getNopWp();
        $data = [
            'action' => route('mandiri.layanan.store'),
            'method' => 'post',
            'jenis_layanan' => Jenis_layanan::orderby('order')->wherein('id', [3, 8, 5])->pluck('nama_layanan', 'id')->toarray(),
            'jenis_pengurangan' => JenisPengurangan::orderby('id', 'asc')->pluck('nama_pengurangan', 'id')->toarray(),
            'nop' => $nop
        ];
        // return $data;

        return view('mandiri.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LayananMandiriRequest $request)
    {
        //

        // return $request->all();

        $nomor_layanan = Layanan_conf::nomor_layanan();

        $jl = Jenis_layanan::find($request->jenis_layanan_id);

        // jenis_pengurangan_id
        // keterangan
        $nop = onlyNumber($request->nop);
        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kecamatan = substr($nop, 4, 3);
        $kd_kelurahan = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);

        // return $request->all();
        $lo = [
            'nomor_layanan' => $nomor_layanan,
            'tanggal_layanan' => now(),
            'jenis_layanan_id' => $request->jenis_layanan_id,
            'jenis_layanan_nama' => $jl->nama_layanan,
            'nik' => sprintf('%016d', substr(auth()->user()->nik, -16, 16)),
            'nama' => Auth()->user()->nama,
            'alamat' => Auth()->user()->alamat_user,
            'kelurahan' => Auth()->user()->kelurahan_user,
            'kecamatan' => Auth()->user()->kecamatan_user,
            'dati2' => Auth()->user()->dati2_user,
            'propinsi' => Auth()->user()->propinsi_user,
            'nomor_telepon' => $request->nomor_telepon,
            'keterangan' => $jl->nama_layanan,
            'created_at' => now(),
            'created_by' => auth()->user()->id,
            'jenis_objek' => '1',
            'is_online' => '1'
        ];

        $dlo = [
            'nomor_layanan' => $lo['nomor_layanan'],
            'nik_wp' =>  sprintf('%016d', substr($request->subjek_pajak_id, -16, 16)),
            'nama_wp' => $request->nama_subjek,
            'alamat_wp' => $request->alamat,
            'rt_wp' => '00',
            'rw_wp' => '00',
            'kelurahan_wp' => $request->kelurahan,
            'kecamatan_wp' => $request->kecamatan,
            'dati2_wp' => $request->dati2,
            'propinsi_wp' => $request->propinsi,
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'luas_bumi' => $request->luas_bumi,
            'luas_bng' => $request->luas_bng,
            'njop_bumi' => $request->luas_bumi * $request->njop_bumi,
            'njop_bng' => $request->luas_bng * $request->njop_bng,
            // 'is_online' => '1'
        ];

        // return $request->all();
        // return ['layanan' => $lo, 'layanan_objek' => $dlo];
        // 
        DB::beginTransaction();
        try {
            //code...
            Layanan::create($lo);
            $obj = Layanan_objek::create($dlo);

            // upload dokumen pendukung
            $disk = 'public';

            $bukti_kepemilikan_file = $request->file('bukti_kepemilikan');
            $filename_bukti_kepemilikan = $obj->id . '_dokumen_kepemilikan.' . $bukti_kepemilikan_file->getClientOriginalExtension();
            if (Storage::disk($disk)->exists($filename_bukti_kepemilikan)) {
                Storage::disk($disk)->delete($filename_bukti_kepemilikan);
            }

            $upload_bukti_kepemilikan = Storage::disk($disk);
            if ($upload_bukti_kepemilikan->put($filename_bukti_kepemilikan, File::get($bukti_kepemilikan_file))) {
                $url_bukti_kepemilikan_file = Storage::disk($disk)->url($filename_bukti_kepemilikan);
                $url_bukti_kepemilikan_file = str_replace(config('app.url'), '', $url_bukti_kepemilikan_file);
                $ld = Layanan_dokumen::create([
                    'nomor_layanan' => $lo['nomor_layanan'],
                    'tablename' => 'LAYANAN_OBJEK',
                    'keyid' => $obj->id,
                    'filename' => $filename_bukti_kepemilikan,
                    'disk' => $disk,
                    'nama_dokumen' => 'Bukti Kepemilikan',
                    'keterangan' => 'Bukti Kepemilikan',
                    'url' => $url_bukti_kepemilikan_file
                ]);
                // dd($ld);
            }

            $ktp_file = $request->file('ktp');
            $filename_ktp = $obj->id . '_dokumen_ktp.' . $ktp_file->getClientOriginalExtension();
            if (Storage::disk($disk)->exists($filename_ktp)) {
                Storage::disk($disk)->delete($filename_ktp);
            }

            $upload_ktp = Storage::disk($disk);
            if ($upload_ktp->put($filename_ktp, File::get($ktp_file))) {
                $url_ktp_file = Storage::disk($disk)->url($filename_ktp);
                $url_ktp_file = str_replace(config('app.url'), '', $url_ktp_file);
                $ld = Layanan_dokumen::create([
                    'nomor_layanan' => $lo['nomor_layanan'],
                    'tablename' => 'LAYANAN_OBJEK',
                    'keyid' => $obj->id,
                    'filename' => $filename_ktp,
                    'disk' => $disk,
                    'nama_dokumen' => 'KTP',
                    'keterangan' => 'KTP',
                    'url' => $url_ktp_file
                ]);
            }


            $lt = [
                'nomor_layanan' => $nomor_layanan,
                'jenis_layanan_nama' => $lo['jenis_layanan_nama'],
                'penelitian' => '1',
                'layanan_objek_id' => $obj->id
            ];
            Db::table('penelitian_task')->insert($lt);
            DB::commit();
            // dispatch(new disposisiPenelitian($nomor_layanan))->onQueue('layanan');
            $with = [
                'success' => 'Permohonan berhasil di buat'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $with = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(route('mandiri.layanan.index'))->with($with);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $layanan = Layanan::find($id);
        return view('mandiri.show', compact('layanan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            Layanan::where('nomor_layanan', $id)
                ->update([
                    'deleted_at' => now(),
                    'deleted_by' => auth()->user()->id
                ]);

            DB::commit();

            $request->session()->flash('success', 'Permohonan berhasil dihapus');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $request->session()->flash('error', $th->getMessage());
        }

        return true;
    }
}
