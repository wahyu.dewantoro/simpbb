<?php

namespace App\Http\Controllers;

use App\Imports\NopRekonImport;
use App\Jobs\RekonPembayaranJob;
use App\RekonBatch;
use App\rekonHistory;
use App\RekonNop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class RekonsiliasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batch = RekonBatch::orderbyraw('rekon_batch.created_at desc')->paginate(10);
        return view('rekon.index', compact('batch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tp = Cache::remember(md5('ref_tmp_byr'), 2000, function () {
            return DB::select(db::raw("select *
                from (
                select '114' kode_bank,	'BANK JATIM' NAMA_BANK from dual
                union
                select '991',	'Kantor POS' from dual)"));
        });
        // dd($tp);
        return view('rekon.form', compact('tp'));
    }

    public function previewUpload(Request $request)
    {
        // dd($request->all());

        $default = response()->json(['status' => false, 'msg' => "Format tidak sesuai"]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $ext = $file->extension();
            // return $ext;
            if (!in_array($ext, ["xlsx", "csv", "xls"])) {
                return $default;
            }

            $array = Excel::toArray(new NopRekonImport(), $file);

            // dd($array[0]);

            $data = [];
            $postdate = date('Y-m-d', strtotime($request->tanggal));
            $tgl = date('Y-m-d', strtotime($request->tanggal));
            foreach ($array[0] as $row) {
                $res = trim(onlyNumber($row['nop']));
                if ($res <> '' && $row['tahun_pajak'] <> '') {
                    $datanop['kd_propinsi'] = substr($res, 0, 2) ?? null;
                    $datanop['kd_dati2'] = substr($res, 2, 2) ?? null;
                    $datanop['kd_kecamatan'] = substr($res, 4, 3) ?? null;
                    $datanop['kd_kelurahan'] = substr($res, 7, 3) ?? null;
                    $datanop['kd_blok'] = substr($res, 10, 3) ?? null;
                    $datanop['no_urut'] = substr($res, 13, 4) ?? null;
                    $datanop['kd_jns_op'] = substr($res, 17, 1) ?? null;

                    $datanop['thn_pajak_sppt'] = $row['tahun_pajak'];
                    $datanop['pokok'] = (int)$row['pokok'];
                    $datanop['denda'] = (int)$row['denda'];
                    $datanop['jumlah'] = (int)(onlyNumber($row['pokok'])) + (int)(onlynumber($row['denda']));

                    $datanop['tanggal_pembayaran'] = $tgl;
                    if ($tgl <> $postdate || (strlen($res) <> 18 && substr($res, -1, 1) != 7   && substr($res, 0, 2) == '35')) {

                        $datanop['status'] = 0;
                        if ($tgl <> $postdate) {
                            $datanop['keterangan'] = 'Tanggal Tidak sama';
                        } else if (strlen($res) <> 18  && substr($res, -1, 1) != 7 && substr($res, 0, 2) == '35') {
                            $datanop['keterangan'] = 'Digit NOP harus 18';
                        } else {
                            $datanop['keterangan'] = '';
                        }
                    } else {
                        $datanop['status'] = 1;
                        $datanop['keterangan'] = 'Bisa di proses';
                    }
                    $data[] = $datanop;
                }
            }
            $kode_tp = $request->kode_tp;
            return view('rekon/preview', compact('kode_tp', 'postdate', 'data'));
        }


        return response()->json($default);
    }




    public function getOltp($param)
    {

        $kd_propinsi = $param['kd_propinsi'];
        $kd_dati2 = $param['kd_dati2'];
        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $kd_blok = $param['kd_blok'];
        $no_urut = $param['no_urut'];
        $kd_jns_op = $param['kd_jns_op'];
        $thn_pajak_sppt = $param['thn_pajak_sppt'];
        $tanggal = $param['tanggal_pembayaran'];
        if ($kd_blok <> '999') {
            $cek = DB::connection('oracle_spo')->table('sppt_oltp')
                ->selectRaw(" kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,pbb_terhutang_sppt-faktor_pengurang_sppt pokok")
                ->whereraw("kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' 
                            and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' 
                            and kd_blok='$kd_blok' and no_urut='$no_urut' 
                            and kd_jns_op='$kd_jns_op' and thn_pajak_sppt='$thn_pajak_sppt'")->first();
        } else {
            // kobil
            $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
            $cek = DB::connection('oracle')->table('billing_kolektif')->join('data_billing', 'data_billing.data_billing_id', '=', 'billing_kolektif.data_billing_id')
                ->selectraw("sum(pokok) pokok,sum(denda) denda ")
                ->whereraw("kobil='" . onlyNumber($kobil) . "' and data_billing.tahun_pajak='$thn_pajak_sppt'")->first();
        }
        return [
            'pokok' => (int)($cek->pokok ?? 0)
        ];
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            // batch insert
            $batch = [
                'tanggal_pembayaran' => new Carbon($request->tanggal_batch),
                'kode_bank' => $request->kode_bank
            ];
            $rb = RekonBatch::create($batch);
            $batchnop = [];
            $kd_propinsi =  explode('|', $request->kd_propinsi);
            $kd_dati2 =  explode('|', $request->kd_dati2);
            $kd_kecamatan =  explode('|', $request->kd_kecamatan);
            $kd_kelurahan =  explode('|', $request->kd_kelurahan);
            $kd_blok =  explode('|', $request->kd_blok);
            $no_urut =  explode('|', $request->no_urut);
            $kd_jns_op =  explode('|', $request->kd_jns_op);
            $thn_pajak_sppt =  explode('|', $request->thn_pajak_sppt);
            $tanggal_pembayaran =  explode('|', $request->tanggal_pembayaran);
            $pokok =  explode('|', trim($request->pokok));
            $denda =  explode('|', trim($request->denda));
            $jumlah =  explode('|', trim($request->jumlah));

            foreach ($kd_propinsi as $i => $row) {
                $datanop['rekon_batch_id'] = $rb->id ?? null;
                $datanop['kd_propinsi'] = $kd_propinsi[$i] ?? null;
                $datanop['kd_dati2'] = $kd_dati2[$i] ?? null;
                $datanop['kd_kecamatan'] = $kd_kecamatan[$i] ?? null;
                $datanop['kd_kelurahan'] = $kd_kelurahan[$i] ?? null;
                $datanop['kd_blok'] = $kd_blok[$i] ?? null;
                $datanop['no_urut'] = $no_urut[$i] ?? null;
                $datanop['kd_jns_op'] = $kd_jns_op[$i] == '' ? '-' : $kd_jns_op[$i];
                $datanop['thn_pajak_sppt'] = $thn_pajak_sppt[$i] ?? null;
                $datanop['tanggal_pembayaran'] = isset($tanggal_pembayaran[$i]) ? date('Ymd', strtotime($tanggal_pembayaran[$i])) : null;

                $datanop['pokok'] = $pokok[$i];
                $datanop['denda'] = $denda[$i];
                $datanop['jumlah'] = $jumlah[$i];
                $datanop['created_at'] = $rb->created_at ?? null;
                $batchnop[] = $datanop;
            }
            // dd($batch);
            // dd(4batc);
            $insert_data = collect($batchnop);
            $chunks = $insert_data->chunk(500);
            foreach ($chunks as $chunk) {
                RekonNop::insert($chunk->toArray());
            }

            DB::commit();
            Artisan::call('rekon:pembayaran', ['--tp' => $request->kode_bank, '--tanggal' => date('Y-m-d', strtotime($request->tanggal_batch))]);
            $response = [
                'status' => true,
                'msg' => 'data telah di import dan rekon sedang berlangsung'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $response = [
                'status' => false,
                'msg' => $th->getMessage()
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tanggal)
    {
        $history = rekonHistory::whereraw("tanggal_pembayaran = to_date('" . $tanggal . "','yyyymmdd')")->get();
        return view('rekon.show', compact('history', 'tanggal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
