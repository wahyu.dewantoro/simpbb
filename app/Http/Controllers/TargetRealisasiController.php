<?php

namespace App\Http\Controllers;

use App\DesaLunas;
use App\Kecamatan;
use App\Kelurahan;
use App\TargetRealisasi;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TargetRealisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tahun = date('Y');
        /* $data =  DB::select(DB::raw("SELECT case when KD_BUKU='12' then 'Buku I,II' else 'Buku III,IV,V' end BUKU,
        case when KD_SEKTOR ='10' then 'Perdesaan' else 'Perkotaan' end SEKTOR,
        AWAL, PERUBAHAN_1, PERUBAHAN_2
        from TARGET_REALISASI
        where TAHUN='$tahun'
        order by KD_BUKU asc,KD_SEKTOR asc")); */
        $data=TargetRealisasi::select(DB::raw("case when KD_BUKU='12' then 'Buku 1 & 2' else 'Buku 3,4, & 5' end BUKU,
        case when KD_SEKTOR ='10' then 'Pedesaan' else 'Perkotaan' end SEKTOR,
        AWAL, PERUBAHAN_1, PERUBAHAN_2,tahun,kd_buku,kd_sektor"))
        ->where('tahun',$tahun)->orderbyraw('kd_buku,kd_sektor')->get();
        return view('target_realisasi/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tahun = date('Y');
        $data = [
            'title' => 'Realisasi Target APBD Tahun ' . $tahun,
            'method' => 'post',
            'action' => route('realisasi.targetrealisasi.store'),
            'tahun' => $tahun,
            'buku' => ['12' => 'Buku 1 & 2', '345' => 'Buku 3,4 & 5'],
            'sektor' => ['10' => 'Pedesaan', '20' => 'Perkotaan']
        ];
        
        return view('target_realisasi.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {

            // cek existing
            $cek = TargetRealisasi::whereraw("trim(kd_buku)='$request->kd_buku'")->whereraw("trim(kd_sektor)='$request->kd_sektor'")->whereraw("trim(tahun)='$request->tahun'")->get();
            // dd($cek);
            if ($cek) {
                TargetRealisasi::whereraw("trim(kd_buku)='$request->kd_buku'")->whereraw("trim(kd_sektor)='$request->kd_sektor'")->whereraw("trim(tahun)='$request->tahun'")->delete();
            }

            TargetRealisasi::updateOrCreate(
                ['kd_buku' => $request->kd_buku, 'kd_sektor' => $request->kd_sektor, 'tahun' => $request->tahun],
                [
                    'awal' => str_replace('.', '', $request->awal),
                    'perubahan_1' => str_replace('.', '', $request->perubahan_1 ?? '0'),
                    'perubahan_2' => str_replace('.', '', $request->perubahan_2 ?? '0'),
                ]
            );
            DB::commit();
            $flash = [
                'success' => 'Berhasil menambahkan Realisasi Target APBD'
            ];
        } catch (\Throwable $e) {
            DB::rollBack();


            $flash = [
                'error' => $e
            ];
        }
        // dd($flash);
        return redirect(route('realisasi.targetrealisasi.index'))->with($flash);

        /*  DB::beginTransaction();
        $tahun = $request->tahun;
        $jenis = $request->jenis;
        $perdesaan_12 = $request->perdesaan_12;
        $perkotaan_12 = $request->perkotaan_12;
        $perdesaan_345 = $request->perdesaan_345;
        $perkotaan_345 = $request->perkotaan_345;
        // dd($request);
        try {
            //code...
            $cek = TargetRealisasi::where('TAHUN', '=', $tahun)->count();
            if ($cek == 0) {
                if ($jenis == 1) {
                    $a = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '12', 'KD_SEKTOR' => '10', 'AWAL' => str_replace('.', '', $perdesaan_12),
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => '0'
                    ]);
                    $b = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '12', 'KD_SEKTOR' => '20', 'AWAL' => str_replace('.', '', $perkotaan_12),
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => '0'
                    ]);
                    $c = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '345', 'KD_SEKTOR' => '10', 'AWAL' => str_replace('.', '', $perdesaan_345),
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => '0'
                    ]);
                    $d = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '345', 'KD_SEKTOR' => '20', 'AWAL' => str_replace('.', '', $perkotaan_345),
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => '0'
                    ]);
                } else if ($jenis == 2) {
                    $a = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '12', 'KD_SEKTOR' => '10', 'AWAL' => '0',
                        'PERUBAHAN_1' => str_replace('.', '', $perdesaan_12), 'PERUBAHAN_2' => '0'
                    ]);
                    $b = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '12', 'KD_SEKTOR' => '20', 'AWAL' => '0',
                        'PERUBAHAN_1' => str_replace('.', '', $perkotaan_12), 'PERUBAHAN_2' => '0'
                    ]);
                    $c = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '345', 'KD_SEKTOR' => '10', 'AWAL' => '0',
                        'PERUBAHAN_1' => str_replace('.', '', $perdesaan_345), 'PERUBAHAN_2' => '0'
                    ]);
                    $d = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '345', 'KD_SEKTOR' => '20', 'AWAL' => '0',
                        'PERUBAHAN_1' => str_replace('.', '', $perkotaan_345), 'PERUBAHAN_2' => '0'
                    ]);
                } else if ($jenis == 3) {
                    $a = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '12', 'KD_SEKTOR' => '10', 'AWAL' => '0',
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => str_replace('.', '', $perdesaan_12)
                    ]);
                    $b = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '12', 'KD_SEKTOR' => '20', 'AWAL' => '0',
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => str_replace('.', '', $perkotaan_12)
                    ]);
                    $c = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '345', 'KD_SEKTOR' => '10', 'AWAL' => '0',
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => str_replace('.', '', $perdesaan_345)
                    ]);
                    $d = TargetRealisasi::create([
                        'TAHUN' => $tahun, 'KD_BUKU' => '345', 'KD_SEKTOR' => '20', 'AWAL' => '0',
                        'PERUBAHAN_1' => '0', 'PERUBAHAN_2' => str_replace('.', '', $perkotaan_345)
                    ]);
                }
            } else {
                if ($jenis == 1) {
                    $a = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '12')->where('KD_SEKTOR', '10')->update(['AWAL' => str_replace('.', '', $perdesaan_12)]);
                    $b = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '12')->where('KD_SEKTOR', '20')->update(['AWAL' => str_replace('.', '', $perkotaan_12)]);
                    $c = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '345')->where('KD_SEKTOR', '10')->update(['AWAL' => str_replace('.', '', $perdesaan_345)]);
                    $d = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '345')->where('KD_SEKTOR', '20')->update(['AWAL' => str_replace('.', '', $perkotaan_345)]);
                } else if ($jenis == 2) {
                    $a = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '12')->where('KD_SEKTOR', '10')->update(['PERUBAHAN_1' => str_replace('.', '', $perdesaan_12)]);
                    $b = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '12')->where('KD_SEKTOR', '20')->update(['PERUBAHAN_1' => str_replace('.', '', $perkotaan_12)]);
                    $c = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '345')->where('KD_SEKTOR', '10')->update(['PERUBAHAN_1' => str_replace('.', '', $perdesaan_345)]);
                    $d = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '345')->where('KD_SEKTOR', '20')->update(['PERUBAHAN_1' => str_replace('.', '', $perkotaan_345)]);
                } else if ($jenis == 3) {
                    $a = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '12')->where('KD_SEKTOR', '10')->update(['PERUBAHAN_2' => str_replace('.', '', $perdesaan_12)]);
                    $b = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '12')->where('KD_SEKTOR', '20')->update(['PERUBAHAN_2' => str_replace('.', '', $perkotaan_12)]);
                    $c = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '345')->where('KD_SEKTOR', '10')->update(['PERUBAHAN_2' => str_replace('.', '', $perdesaan_345)]);
                    $d = TargetRealisasi::where('TAHUN', $tahun)->where('KD_BUKU', '345')->where('KD_SEKTOR', '20')->update(['PERUBAHAN_2' => str_replace('.', '', $perkotaan_345)]);
                }
            }

            $flash = [
                'success' => 'Berhasil menambahkan Realisasi Target APBD'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        } */
        // dd($flash);
        return redirect(route('realisasi.targetrealisasi.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $exp=explode('_',$id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $param=explode('.',$id);
        $tahun=$param[0];
        $buku=$param[1];
        $sektor=$param[2];

        $target = TargetRealisasi::whereraw("trim(kd_buku)='$buku'")->whereraw("trim(kd_sektor)='$sektor'")->whereraw("trim(tahun)='$tahun'")->first();
        $data = [
            'title' => 'Realisasi Target APBD Tahun ' . $tahun,
            'method' => 'post',
            'action' => route('realisasi.targetrealisasi.store'),
            'tahun' => $tahun,
            'buku' => ['12' => 'Buku 1 & 2', '345' => 'Buku 3,4 & 5'],
            'sektor' => ['10' => 'Pedesaan', '20' => 'Perkotaan'],
            'target'=>$target
        ];
        return view('target_realisasi.form', compact('data'));
     /*    //
        $exp = explode('_', $id);
        $lunas = DesaLunas::where('kd_kecamatan', $exp[0])->where('kd_kelurahan', $exp[1])->where('tahun_pajak', $exp[2])->firstorfail();

        // dd($lunas);
        $data = [
            'title' => 'Edit Desa Lunas',
            'method' => 'patch',
            'action' => route('realisasi.desalunas.update', $id),
            'lunas' => $lunas
        ];
        $kecamatan = Kecamatan::get();
        $kelurahan = Kelurahan::where('kd_kecamatan',  $exp[0])->get();
        // dd($kelurahan);
        return view('desa_lunas.form', compact('data', 'kecamatan', 'kelurahan')); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();
        try {
            //code...
            $exp = explode('_', $id);
            $lunas = DesaLunas::where('kd_kecamatan', $exp[0])->where('kd_kelurahan', $exp[1])->where('tahun_pajak', $exp[2]);
            $data = [
                'kd_kecamatan' => $request->kd_kecamatan,
                'kd_kelurahan' => $request->kd_kelurahan,
                'tahun_pajak' => $request->tahun_pajak,
                'tanggal_lunas' => new carbon($request->tanggal_lunas)
            ];

            $lunas->update($data);
            $flash = [
                'success' => 'berhasil di update.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        // dd($flash);
        return redirect(route('realisasi.desalunas.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $exp = explode('_', $id);
            $lunas = DesaLunas::where('kd_kecamatan', $exp[0])->where('kd_kelurahan', $exp[1])->where('tahun_pajak', $exp[2]);
            $lunas->delete();
            $flash = [
                'success' => 'berhasil di hapus data.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        return redirect(route('realisasi.desalunas.index'))->with($flash);
    }
}
