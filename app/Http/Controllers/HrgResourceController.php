<?php

namespace App\Http\Controllers;

use App\Exports\exportHargaResource;
use App\Helpers\Debekabe;
use App\Models\HrgResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use DOMPDF as dompdf;
use Maatwebsite\Excel\Facades\Excel;

class HrgResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function coreSql($tahun)
    {
        return  Debekabe::Resource($tahun)->get()->toArray();
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = $this->coreSql($tahun);
            $grouped = collect($data)->groupBy('nm_group_resource');
            return view('ssh.resource._index', compact('grouped'));
        }


        return view('ssh.resource.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DB::connection("oracle_satutujuh")
                ->table(DB::raw("item_resource b"))
                ->join(db::raw("group_resource c"), 'c.kd_group_resource', '=', 'b.kd_group_resource')
                ->orderByraw("b.kd_group_resource,b.kd_resource asc")
                ->select(db::raw("b.kd_group_resource,
                nm_group_resource,
                b.kd_resource,
                nm_resource,
                satuan_resource,
                (SELECT hrg_resource
                   FROM hrg_resource
                  WHERE     kd_group_resource = b.kd_group_resource
                        AND kd_resource = b.kd_resource
                        AND thn_hrg_resource = '" . ($tahun - 1) . "')
                   hrg_tahun_lalu,
                (SELECT hrg_resource
                   FROM hrg_resource
                  WHERE     kd_group_resource = b.kd_group_resource
                        AND kd_resource = b.kd_resource
                        AND thn_hrg_resource = '" . $tahun . "')
                   hrg_tahun_skrg"))
                ->get()->toArray();

            $grouped = collect($data)->groupBy('nm_group_resource');
            return view('ssh.resource._form', compact('grouped', 'tahun'));
        }
        return view('ssh.resource.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $var_data = $request->except(['_token', '_method', 'tahun']);
        // dd($var_data);


        $data = [];
        foreach ($request->thn_hrg_resource as $i => $itm) {
            if (is_null($request->hrg_resource[$i]) == false) {
                $data[] = [
                    'JNS_DOKUMEN' => 2,
                    'kd_propinsi' => $request->kd_propinsi[$i],
                    'kd_dati2' => $request->kd_dati2[$i],
                    'kd_kanwil' => $request->kd_kanwil[$i],
                    'kd_kantor' => $request->kd_kantor[$i],
                    'no_dokumen' => $request->no_dokumen[$i],
                    'thn_hrg_resource' => $request->thn_hrg_resource[$i],
                    'kd_group_resource' => $request->kd_group_resource[$i],
                    'kd_resource' => $request->kd_resource[$i],
                    'hrg_resource' => onlyNumber($request->hrg_resource[$i]) / 1000,
                ];
            }
        }

        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            HrgResource::where('thn_hrg_resource', $request->tahun)->delete();

            if (!empty($data)) {
                foreach ($data as $row) {
                    HrgResource::create($row);
                }
            }

            $flash = ['success' => 'Berhasil di proses untuk tahun ' . $request->tahun];
            DB::connection("oracle_satutujuh")->commit();

            DB::connection("oracle_satutujuh")->statement("call PEMBETUKAN_DBKB('" . $request->tahun . "','35','07')");
        } catch (\Throwable $th) {
            Log::error($th);
            $flash = ['error' => $th->getMessage()];
            DB::connection("oracle_satutujuh")->rollBack();
        }

        return redirect(route('ssh.harga-resource.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function cetak($tahun, $tipe)
    {
        // return [$tahun, $tipe];

        if ($tipe == 'pdf') {
            return $this->cetakPdf($tahun);
        } else if ($tipe == 'excel') {
            return     $this->cetakExcel($tahun);
        } else {
            abort(404);
        }
    }

    public function cetakPdf($tahun)
    {
        $data = $this->coreSql($tahun);
        $grouped = collect($data)->groupBy('nm_group_resource');

        $pdf = dompdf::loadview('ssh.resource.export_pdf', compact('grouped', 'tahun'));
        return $pdf->stream();
    }

    public function cetakExcel($tahun)
    {
        $data = $this->coreSql($tahun);
        $grouped = collect($data)->groupBy('nm_group_resource');
        return Excel::download(new exportHargaResource($grouped), 'harga resource tahun '.$tahun.'.xlsx');
    }
}
