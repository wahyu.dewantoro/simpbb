<?php

namespace App\Http\Controllers;

use App\Models\DbkbJpbDuaBelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbJpbDuaBelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbJpbDuaBelas::where('thn_dbkb_jpb12', $tahun)->get();
            $read = 1;
            return view('dbkb.jepebeduabelas._index', compact('data', 'read'));
        }
        return view('dbkb.jepebeduabelas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbJpbDuaBelas::where('thn_dbkb_jpb12', $tahun)->get();
            if ($data->count() == 0) {
                $data = DbkbJpbDuaBelas::where('thn_dbkb_jpb12', $tahun - 1)->get();
            }
            $read = 0;
            return view('dbkb.jepebeduabelas._index', compact('data', 'read'));
        }
        return view('dbkb/jepebeduabelas/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            //code...

            $arfas = $request->except(['thn_dbkb_jpb12', '_method', '_token']);
            // $angka = [];
            foreach ($arfas as $i => $item) {
                DbkbJpbDuaBelas::where([
                    'thn_dbkb_jpb12' => $request->thn_dbkb_jpb12,
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'type_dbkb_jpb12' => str_replace('nilai_', '', $i)
                ])->delete();

                $angka = (int)$item / 1000;
                DbkbJpbDuaBelas::create([
                    'thn_dbkb_jpb12' => $request->thn_dbkb_jpb12,
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'type_dbkb_jpb12' => str_replace('nilai_', '', $i),
                    'nilai_dbkb_jpb12' => $angka
                ]);
            }
            // dd($angka);

            $flash = ['success' => 'Berhasil memproses JPB 12 pada tahun ' . $request->thn_dbkb_jpb12];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.jepebe-dua-belas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
