<?php

namespace App\Http\Controllers;

use App\Models\DbkbDayaDukung;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbDayaDukungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbDayaDukung::where('thn_dbkb_daya_dukung', $tahun)->get();
            $read = 1;
            return view('dbkb.daya_dukung._index', compact('data', 'read'));
        }
        return view('dbkb.daya_dukung.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbDayaDukung::where('thn_dbkb_daya_dukung', $tahun)->get();
            if ($data->count() == 0) {
                $data = DbkbDayaDukung::where('thn_dbkb_daya_dukung', $tahun - 1)->get();
            }
            $read = 0;
            return view('dbkb.daya_dukung._index', compact('data', 'read'));
        }
        return view('dbkb/daya_dukung/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            //code...

            $arfas = $request->except(['thn_dbkb_daya_dukung', '_method', '_token']);
            // $angka = [];
            foreach ($arfas as $i => $item) {
                DbkbDayaDukung::where([
                    'thn_dbkb_daya_dukung' => $request->thn_dbkb_daya_dukung,
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'type_konstruksi' => str_replace('nilai_', '', $i)
                ])->delete();

                $angka = (int)$item / 1000;
                DbkbDayaDukung::create([
                    'thn_dbkb_daya_dukung' => $request->thn_dbkb_daya_dukung,
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'type_konstruksi' => str_replace('nilai_', '', $i),
                    'nilai_dbkb_daya_dukung' => $angka
                ]);
            }
            // dd($angka);

            $flash = ['success' => 'Berhasil memproses daya dukung pada tahun ' . $request->thn_dbkb_daya_dukung];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.daya-dukung.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
