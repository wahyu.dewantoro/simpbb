<?php

namespace App\Http\Controllers;

use App\Exports\ExcelNotaPerhitungan;
use App\Helpers\Lhp;
use App\Helpers\Pajak;
use App\Helpers\PembatalanPenelitian;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Data_billing;
use App\Models\Jenis_layanan;
use App\Models\Layanan_objek;
use App\Models\Notaperhitungan;
use App\pegawaiStruktural;
use App\TempatBayar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use SnappyPdf as PDF;
use DOMPDF;
use PhpOffice\PhpWord\Writer\PDF\DomPDF as PDFDomPDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yajra\DataTables\DataTables;

class LhpController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // role
            $role = Auth()->user()->roles()->pluck('id')->toarray();
            $data = Lhp::all();
            if (request('nomor_layanan')) {
                $data->whereRaw(DB::raw("b.nomor_layanan like '%" . request('nomor_layanan') . "%'"));
            }
            return  Datatables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->has('tahun')) {
                        $tahun = $request->get('tahun');
                        if (!empty($tahun)) {
                            $query->whereraw("substr(b.nomor_layanan,1,4)='$tahun'");
                        }
                    }
                    if ($request->has('jenis_layanan_id')) {
                        $jenis_layanan_id = $request->get('jenis_layanan_id');
                        if (!empty($jenis_layanan_id)) {
                            $query->whereraw("jenis_layanan_id='$jenis_layanan_id'");
                        }
                    }
                    if ($request->has('keyword')) {
                        $text = $request->keyword;
                        $keyword = onlyNumber($text);
                        $keyword = strtolower($keyword);
                        $query->whereRaw(DB::raw("(a.nop_proses like '%$keyword%' or b.nomor_layanan like '%$keyword%')"));
                    }
                })
                ->filterColumn('nop', function ($query, $keyword) {
                    $keyword = onlyNumber($keyword);
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("a.nop_proses like '%$keyword%'"));
                })
                ->filterColumn('nomorlayanan', function ($query, $keyword) {
                    $keyword = onlyNumber($keyword);
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("b.nomor_layanan like '%$keyword%'"));
                })
                // 
                ->addColumn('pilih', function ($row) use ($role) {
                    $btn = '<a data-toggle="tooltip" data-placement="top" title="Detail Penelitian" class=" detail-objek" role="button"  data-id="' . $row->layanan_objek_id . '" tabindex="0"><i class="fas fa-search  text-primary"></i></a>';

                    if (in_array(61, $role) || in_array(62, $role)) {
                        if ($row->usia_penelitian <= 1000) {
                            $btn .= ' <a  data-toggle="tooltip" data-placement="top" title="Edit Penelitian" class="detail " href="' . route('penelitian.kantor.edit', $row->layanan_objek_id) . '"><i class=" text-info  far fa-edit"></i></a> ';
                        }
                    } else {
                        // $btn .= ' <a data-toggle="tooltip" data-placement="top" title="Surat Keputusan" class="  cetak-sk" role="button"  data-url="' . url('sk', acak($row->layanan_objek_id)) . '" data-id="' . $row->layanan_objek_id . '" tabindex="0"><i class="text-success  fas fa-file-alt"></i></a> ';
                    }

                    if ($row->nota > 0) {
                        $btn .= ' <a data-toggle="tooltip" data-placement="top" title="Nota Perhitungan"  class=" cetak-tagihan"role="button"  data-id="' . acak($row->layanan_objek_id) . '" tabindex="0"><i class="fas fa-receipt  text-warning"></i></a> ';
                    }
                    return $btn;
                })
                ->addColumn('nopproses', function ($row) {
                    return formatnop($row->nop_proses);
                })
                ->addColumn('noformulir', function ($row) {
                    return formatNomor($row->no_formulir);
                })
                ->addColumn('nomorlayanan', function ($row) {
                    return formatNomor($row->nomor_layanan);
                })
                ->rawColumns(['pilih', 'noformulir', 'nomorlayanan', 'nopproses'])->make(true);
        }
        $nomor_layanan = $request->only('nomor_layanan');
        $jnslayanan = Jenis_layanan::orderby('order')->get();
        return view('penelitian.index_hasil_penelitian', compact('nomor_layanan', 'jnslayanan'));
    }


    public function notaPerhitunganPdf($ide)
    {
        $mentah = rapikan($ide);
        $id = explode('-', $mentah)[0];
        $jenis = explode('-', $mentah)[1] ?? '1';

        $nota = DB::table(db::raw("NOTA_PERHITUNGAN np "))
            ->join(db::raw("DATA_BILLING db"), "db.DATA_BILLING_ID", "=", "np.DATA_BILLING_ID")
            ->leftjoin(db::raw("(SELECT lo.id,ts.nop_proses nop,ts.nm_wp,jalan_wp,ts.BLOK_KAV_NO_WP ,ts.rt_wp,ts.rw_wp,ts.KELURAHAN_WP ,ts.KOTA_WP 
        FROM TBL_SPOP ts 
        JOIN LAYANAN_OBJEK lo ON ts.NO_FORMULIR=lo.NOMOR_FORMULIR ) lo"), "lo.id", "=", "np.LAYANAN_OBJEK_ID")
            ->select(db::raw("np.*,db.expired_at ,db.tgl_bayar ,db.tahun_pajak,
            lo.nop nop ,
            lo.nm_wp nm_wp ,
            lo.jalan_wp jalan_wp ,
            lo.blok_kav_no_wp blok_kav_no_wp ,
            lo.rt_wp rt_wp ,
            lo.rw_wp rw_wp ,
            lo.kelurahan_wp kelurahan_wp ,
            lo.kota_wp kota_wp "))->wherenull('db.deleted_at');
        if ($jenis == '2') {
            // pendataan
            $nota = $nota->whereraw("PENDATAAN_OBJEK_ID ='$id'");
        } else {
            // permohonan
            $nota = $nota->whereraw("layanan_objek_id ='$id'");
        }
        $nota = $nota->first();
        // dd($nota);


        // $tagihan = Pajak::TagihanPenelitian($nota->nomor_formulir);
        $tagihan = DB::table(db::raw("BILLING_KOLEKTIF bk "))
            ->join(db::raw("NOTA_PERHITUNGAN np"), db::raw('BK.DATA_BILLING_ID'), '=', db::raw('np.DATA_BILLING_ID'))
            ->join(db::raw("data_billing db"), db::raw('db.DATA_BILLING_ID'), '=', db::raw('np.DATA_BILLING_ID'))
            ->select(db::raw("bk.tahun_pajak thn_pajak_sppt,sum(pokok) pokok,sum(denda) denda,sum(total) total"))
            ->wherenull('db.deleted_at')
            ->groupBy("bk.tahun_pajak")
            ->orderbyraw("bk.tahun_pajak asc");

        if ($jenis == '2') {
            // pendataan
            $tagihan = $tagihan->whereraw("PENDATAAN_OBJEK_ID ='$id'");
        } else {
            // permohonan
            $tagihan = $tagihan->whereraw("layanan_objek_id ='$id'");
        }

        $tagihan = $tagihan->get();

        $tp = TempatBayar::select(DB::raw("LISTAGG(nama_tempat, ', ') WITHIN GROUP (ORDER BY nama_tempat) nama_tempat"))->first();
        $urla =   config('app.url') . 'penelitian/nota-perhitungan-pdf/' . $ide;
        $url = base64_encode(QrCode::format('svg')->generate($urla));
        $pages = view('penelitian/_perhitungan_pdf', compact('tagihan', 'tp', 'nota', 'url'));
        // $pdf = PDFDomPDF::loadHTML($pages);
        $pdf = DOMPDF::loadHTML($pages)->setPaper('a4');
        /* ->setOption('enable-local-file-access', true)
            ->setOption('margin-top', '15')
            ->setOption('margin-left', '20')
            ->setOption('margin-right', '20')
            ->setPaper('A4'); */
        $filename = time() . ' tagihan ' . '.pdf';
        return $pdf->stream($filename);
    }

    public function notaPerhitunganExcel(Request $request)
    {

        // $request = $request->all();

/* 
        $res = Lhp::notaListExport($request);
        return $res; */
        // $res = Lhp::notaListExport($request);
        // $data = $res['data'];
        /* $jenis = $res['jenis'];
        $bayar = $res['bayar'];
        $jenis_objek = $res['jenis_objek']; */
        switch ($request->jenis_objek) {
            case '3':
                # code...
                $jenis_objek = "Kolektif";
                break;
            case '2':
                $jenis_objek = "Badan";
                break;
            case '1':
                $jenis_objek = "Individu";
                break;

            default:
                # code...
                $jenis_objek = "";
                break;
        }


        switch ($request->bayar) {
            case '1':
                $bayar = 'Lunas';
                break;
            case '0':
                $bayar = 'Belum Lunas';
                break;
            default:
                $bayar = 'Lunas & belum lunas';
                break;
        }

        switch ($request->jenis) {
            case '1':
                # code...
                $jenis = 'Pelayanan';
                break;
            case '2':
                # code...
                $jenis = 'Pendataan';
                break;
            default:
                $jenis = 'Pelayanan & Pendataan';
                break;
        }

        return Excel::download(new ExcelNotaPerhitungan($request), 'Nota perhitungan ' . $jenis . ' ' . $bayar . '  ' . $jenis_objek . '.xlsx');
    }


    public function notaPerhitunganprint(Request $request)
    {
        $res = Lhp::notaListExport($request);
        $data = $res['data'];
        $jenis = $res['jenis'];
        $bayar = $res['bayar'];
        $jenis_objek = $res['jenis_objek'];
        $pages = view('penelitian/nota-perhitungan-print', compact('data', 'jenis', 'bayar', 'jenis_objek'));
        $pdf = pdf::loadHTML($pages)
            ->setOption('enable-local-file-access', true)
            ->setOption('orientation', 'landscape')
            ->setPaper('A4');
        $filename = time() . ' tagihan ' . '.pdf';
        return $pdf->download($filename);
    }

    public function notaPerhitungan(Request $request)
    {
        if ($request->ajax()) {
            $sql = Lhp::notaPerhitunganList();
            if ($request->has('kd_kecamatan')) {
                $kd_kecamatan = $request->kd_kecamatan;
                if ($kd_kecamatan != '') {
                    $sql = $sql->whereraw("data_billing.kd_kecamatan ='$kd_kecamatan'");
                }
            }

            if ($request->has('kd_kelurahan')) {
                $kd_kelurahan = $request->kd_kelurahan;
                if ($kd_kelurahan != '') {
                    $sql = $sql->whereraw("data_billing.kd_kelurahan ='$kd_kelurahan'");
                }
            }

            if ($request->has('jenis')) {
                $jenis = $request->jenis;
                if ($jenis <> '') {
                    $sql->whereraw("case when nota_perhitungan.layanan_objek_id is not null then '1' else '2' end='$jenis'");
                }
            }

            if ($request->has('bayar')) {
                $bayar = $request->bayar;
                if ($bayar <> '') {
                    $sql->whereraw("data_billing.kd_status='$bayar'");
                }
            }

            if ($request->has('jenis_objek')) {
                $jenis_objek = $request->jenis_objek;
                if ($jenis_objek <> '') {
                    $sql->whereraw("(case when nota_perhitungan.layanan_objek_id is not null then '1' else '2' end)='$jenis_objek'");
                }
            }
            if($request->has('search')){
                $search=$request->search;
                if($search['value']){
                    $keyword = strtolower($search['value']);
                    $wh = "nomor like '%".$keyword."%' ";
                    $nopproses = onlyNumber($search['value']);
                    if ($nopproses != '') {
                        $wh .= " or data_billing.kobil like '%$nopproses%'";
                        $wh .= " or nvl(layanan_objek.nomor_layanan, nomor_batch) like '%$nopproses%'";
                        $wh .= " or tsa.nop_proses like '%$nopproses%'";
                    }
                    $sql->whereRaw("(" . $wh . ")");
                }
            }
            $sql = $sql->orderbyraw("nota_perhitungan.id desc");
            // echo $sql->toSql();
            // dd('z');
            // dd(request('search'));
            // ->filterColumn('pencarian_data', function ($query, $string) {
            //     $keyword = strtolower($string);
            //     $wh = "nomor like '%$keyword%' ";
            //     $nopproses = onlyNumber($string);
            // if ($nopproses != '') {
            //     $wh .= " or data_billing.kobil like '%$nopproses%'";
            //     $wh .= " or nvl(layanan_objek.nomor_layanan, nomor_batch) like '%$nopproses%'";
            //     $wh .= " or nvl(tsa.nop_proses, tsb.nop_proses) like '%$nopproses%'";
            // }
            // $query->whereRaw($wh);
            // })
            $role = Auth()->user()->roles()->pluck('id')->toarray();
            return  DataTables::of($sql)
                ->addColumn('tanggal_surat', function ($row) {
                    return tglIndo($row->tanggal);
                })
                ->addColumn('pilih', function ($row) use ($role) {

                    $btn = ' <a data-toggle="tooltip" data-placement="top" title="Nota Perhitungan"  class=" cetak-tagihan"role="button"  data-id="' . acak($row->layanan_objek_id . "-" . $row->jenis) . '" tabindex="0"><i class="fas fa-receipt  text-warning"></i></a> ';
                    if ($row->tgl_bayar != '') {
                        $btn .= ' <a data-toggle="tooltip" data-placement="top" title="Surat Keputusan" class="  cetak-sk" role="button"  data-url="' . url('sk', acak($row->layanan_objek_id)) . '" data-id="' . $row->layanan_objek_id . '" tabindex="0"><i class="text-success  fas fa-file-alt"></i></a> ';
                        if ($row->thn_pajak_sppt != '') {
                            $urs = url('informasi/sppt/cetak', $row->nop . $row->tahun_pajak);
                            $btn .= ' <a data-toggle="tooltip" data-placement="top" title="SPPT" class="  cetak-sppt" role="button"  data-url="' . $urs  . '"  tabindex="0"><i class="text-info  fas fa-file-alt"></i></a> ';
                        }
                    } else {


                        if (in_array(61, $role) || in_array(62, $role)) {
                            $hapus = '1';
                        } else {
                            $hapus = '0';
                        }

                        if ($hapus == '1') {
                            $btn .= ' <a data-toggle="tooltip" data-placement="top" title="Hapus" class="  hapus" role="button"  data-id="' . $row->id . '" tabindex="0"><i class="fas fa-trash-alt text-danger"></i></a> ';
                        }
                    }
                    return $btn;
                })
                ->addColumn('keterangan', function ($row) {
                    return $row->jenis_objek == '1' ? 'Individu' : 'Kolektif';

                    // $ket="";
                    // $jns = $row->jenis_objek == '1' ? 'Individu' : 'Kolektif';
                    // return "<small>" . $row->ket . "<br> " . $jns ." </small>";
                })
                ->editColumn('nop', function ($row) {
                    $nop = $row->nop;
                    return formatnop($nop);
                })
                ->editColumn('kobil', function ($row) {
                    $ket = "Belum dibayar";
                    if ($row->kd_status == '1') {
                        $ket = "Lunas : " . tglIndo($row->tgl_bayar);
                    }
                    return $row->kobil . "<br><small class='text-info'>" . $ket . "</small>";
                })
                ->editColumn('nomor', function ($row) {
                    return $row->nomor . '<br><small class="text-info">' . tglIndo($row->tanggal) . '</small>';
                })
                ->addColumn('nomorlayanan', function ($row) {
                    $jns = $row->ket;
                    return formatNomor($row->nomor_layanan) . "<br><small class='text-primary'>" . $jns . "</small>";
                })
                ->rawColumns(['pilih', 'nomorlayanan', 'tanggal_surat', 'keterangan', 'kobil', 'nop', 'nomor'])->make(true);
        }

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }

        return view('penelitian.nota_index', compact('kecamatan', 'kelurahan'));
    }

    public function notaPerhitunganhapus($id)
    {
        // return $id;

        $ck = Notaperhitungan::whereid($id)->first();
        if ($ck == null) {
            abort('404');
        }

        DB::beginTransaction();
        try {
            //code...
            Data_billing::where('data_billing_id', $ck->data_billing_id)
                ->update(['deleted_at' => Carbon::now()]);

            db::commit();
            $flash = ['success' => 'Berhasil di hapus'];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = ['error' => $th->getMessage()];
        }

        return redirect(url('penelitian/nota-perhitungan'))->with($flash);
    }

    public function cancelPenelitian($id)
    {


        $data = DB::table('layanan_objek')->join('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
            ->select(db::raw("layanan_objek.id,jenis_layanan_id,nop_gabung,nomor_formulir"))
            ->whereraw("layanan_objek.id='$id'")->first();

        $jenis_layanan_id = $data->jenis_layanan_id;
        switch ($jenis_layanan_id) {
            case '6':
                // mutasi Pecah
                $induk = $data->nop_gabung;
                return  PembatalanPenelitian::MutasiPecah($id, $data->nomor_formulir, $induk);
                break;
            default:
                # code...
                break;
        }

        return $data->jenis_layanan_id;
    }
}
