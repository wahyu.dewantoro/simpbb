<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\gallade;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class DafnomRiwayatController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $isDownload=$request->only('download');
        if($isDownload){
            $pathToFile = 'storage/app/public/dafnom_list_pdf/dafnom_import/' . $isDownload['download'];
            return response()->file($pathToFile);
        }
        $localStorage=Storage::disk('dafnom_list_pdf');
        $getList=$localStorage->files('dafnom_import');
        $fileList=[];
        foreach($getList as $file){
            $fileIdentity=explode('/',$file);
            $btnExcel=gallade::anchorInfo('riwayat?download='.$fileIdentity[1],'<i class="fas fa-file-excel"></i>','title="Download File"');
            $btnDetail=gallade::buttonInfo('<i class="fas fa-search"></i>', 'title="Detail Dafnom" data-detail="' . $fileIdentity[1] . '"');
            $fileList[]= [
                'no'=>'',
                'name'=>$fileIdentity[1],
                'Modified'=>Carbon::createFromTimestamp($localStorage->lastModified($file))->format('Y-m-d H:i:s'),
                'size'=>gallade::formatBytes($localStorage->size($file),2),
                'option'=>'<div class="btn-group">'.$btnExcel.$btnDetail.'</div>'
            ];
        }
        $setdata['data']=gallade::generateinTbody($fileList,"File Masih Kosong/File tidak ditemukan");
        return view('dafnom.dafnom_riwayat', compact('setdata'));
    }
}
