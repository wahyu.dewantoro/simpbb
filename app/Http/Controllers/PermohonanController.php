<?php

namespace App\Http\Controllers;

use App\Helpers\Layanan_conf;
use App\Helpers\Pajak;
use App\Models\KeepPenelitian;
use App\Models\Layanan;
use App\Models\Layanan_dokumen;
use App\Models\Layanan_objek;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class PermohonanController extends Controller
{
    //

    public function ajuanOnline(Request $request)
    {
        if ($request->ajax()) {
            $data = Layanan_objek::join(DB::raw('layanan'), 'layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan')
                ->leftjoin(DB::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                ->leftjoin(DB::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
                    $join->on('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek.kd_kecamatan')
                        ->on('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek.kd_kelurahan');
                })
                ->select(db::raw("distinct layanan_objek.id,layanan_objek.nomor_layanan,layanan_objek.created_at,case when nama_wp is null then  nama_badan else nama_wp end nama_wp,
                        layanan_objek.kd_propinsi,layanan_objek.kd_dati2,layanan_objek.kd_kecamatan,layanan_objek.kd_kelurahan,layanan_objek.kd_blok,layanan_objek.no_urut,layanan_objek.kd_jns_op,layanan.jenis_layanan_nama,
                            nm_kecamatan,nm_kelurahan,replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end alamat_op ,case when jenis_layanan_id=6 and nop_gabung is null then 'Induk' when jenis_layanan_id=6 and nop_gabung is not  null then 'Pecahan' else null end keterangan"))
                ->whereraw("layanan.deleted_at is null and layanan.is_online is not null and nomor_formulir is null and is_tolak is null");
            // jenis_layanan_id=8 and jenis_layanan_nama like '%online%' 


            return  Datatables::of($data)
                ->filterColumn('nomor_layanan', function ($query, $keyword) {
                    $keyword = strtolower($keyword);
                    $nopel = preg_replace('/[^0-9]/', '', $keyword);
                    $query->whereRaw(DB::raw("layanan_objek.nomor_layanan     like '%$nopel%' or lower(layanan.jenis_layanan_nama) like '%$keyword%' "));
                })
                ->filterColumn('nama_wp', function ($query, $keyword) {
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%'"));
                })
                ->filterColumn('nama_nop', function ($query, $keyword) {
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("lower(case when nama_wp is null then  nama_badan else nama_wp end) like '%$keyword%' or  (layanan_objek.kd_propinsi||'.'||layanan_objek.kd_dati2||'.'||layanan_objek.kd_kecamatan||'.'||layanan_objek.kd_kelurahan||'.'||layanan_objek.kd_blok||'-'||layanan_objek.no_urut||'.'||layanan_objek.kd_jns_op like '%$keyword%')"));
                })
                ->filterColumn('nop', function ($query, $keyword) {
                    $keyword = preg_replace('/[^0-9]/', '', $keyword);
                    $query->whereRaw(DB::raw("layanan_objek.kd_propinsi||layanan_objek.kd_dati2||layanan_objek.kd_kecamatan||layanan_objek.kd_kelurahan||layanan_objek.kd_blok||layanan_objek.no_urut||layanan_objek.kd_jns_op like '%$keyword%'"));
                })
                ->filterColumn('alamat_op', function ($query, $keyword) {
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("(lower(replace(upper(alamat_op),'|','') ||  case when rt_op is not null or rt_op<>''then     ' RT ' ||rt_op else null end   || case when rw_op is not null or rw_op<>''then     ' RW ' ||rw_op else null end) like '%$keyword%' ) or ref_kecamatan.nm_kecamatan like '%$keyword%' or ref_kelurahan.nm_kelurahan like '%$keyword%' "));
                })
                ->addColumn('nop', function ($data) {
                    return $data->kd_propinsi . '.' . $data->kd_dati2 . '.' . $data->kd_kecamatan . '.' . $data->kd_kelurahan . '.' . $data->kd_blok . '-' . $data->no_urut . '.' . $data->kd_jns_op;
                })
                ->addColumn('pilih', function ($data) {
                    // $btn = '<a class="badge badge-danger tolak" data-id="' . $row->id . '" href="#"> <i class="fas fa-times"></i> </a>';
                    // $btn .= '<a class="badge badge-primary" href="' . url('layanan/pembetulan-online/' . $row->id) . ' "> <i class="far fa-list-alt"></i> </a>';
                    // $btn = '';
                    return '<a data-toggle="tooltip" data-placement="top" title="Form penelitian"  href="' . url('penelitian/kantor/create') . '?id=' . $data->id . '&penelitian=1"> <i class="fas fa-clipboard-list text-primary fa-2x"></i> </a>';
                    // return $btn;
                    // return $data->id;
                })->rawColumns(['pilih', 'nop'])->make(true);
        }
        return view('layanan.online.index');
    }

    public function ajuanOnlineVerifikasi($id)
    {

        $userId = Auth()->user()->id;
        // $id = $request->id ?? null;

        $penelitian = Layanan_objek::with('layanan')->where('id', $id)->wherenull('nomor_formulir')->first();
        $lampiran = Layanan_dokumen::where('nomor_layanan', $penelitian->nomor_layanan)->get();

        if ($penelitian) {
            $cek = KeepPenelitian::where('layanan_objek_id', $id)->whereraw("trunc(24*mod(sysdate - start_at,1))<2")->first();
            if (count((array)$cek) > 0) {
                if ($cek->peneliti_by <> $userId) {
                    $pesan = 'Berkas sedang di teliti oleh :<br> <strong>' . $cek->user->nama . '</strong>';
                    return view('penelitian.keep', compact('pesan'));
                }
            } else {
                $keep = [
                    'layanan_objek_id' => $id,
                    'peneliti_by' => $userId,
                    'start_at' => Carbon::now()
                ];
                KeepPenelitian::create($keep);
            }

            $penelitian = Pajak::loadPenelitian($penelitian);
            // dd($penelitian);
            $data = [
                'method' => 'PATCH',
                'action' => url('layanan/pembetulan-online', $id),
                'jns_penelitian' => '1',
                'penelitian' => (object)$penelitian,
                'nop_pembatalan' => $penelitian['nop_pembatalan'],
                'nop_pecah' => $penelitian['nop_pecah'],
                'lo' => $penelitian['letak_op'],
                'refAjuan' => $penelitian['refAjuan'],
                'title' => 'Form Penlitian Pembetulan Online',
                'urlback' => url('layanan/pembetulan-online'),
                'urlpelimpahan' => '',
                'induk' => $penelitian['nopinduk'],
                'berkas' => $penelitian['berkas']
            ];
            $jenisDokumen = $penelitian['jenisDokumen'];
            $riwayatpembayaran = $penelitian['riwayatpembayaran'];
            $statuspecahan = $penelitian['statuspecahan'];
            $nop = $penelitian['nop'];
            $is_admin = Auth()->user()->hasAnyRole('Super User', 'Administrator');
            return view('spop.form', compact('data', 'nop', 'riwayatpembayaran', 'statuspecahan', 'jenisDokumen', 'lampiran', 'is_admin'));
        }

        abort(404);
    }

    public function ajuanOnlineVerifikasiReject($id)
    {
        $layanan = Layanan::where('nomor_layanan', $id)->first();
        $nomortujuan = $layanan->nomor_telepon;
        $pesan = "*INFORMASI* \n Permohonanan pembetulan data yang di ajukan oleh Bapak/Ibu *" . $layanan->nama . "* dengan nomor pelayanan *" . $id . "* tidak dapat di layani atau di tolak karena persyaratan yang disertakan tidak valid. \n\n SIPANJI *#NOREPLY*";
        Layanan::where('nomor_layanan', $id)->update(['kd_status' => '3']);
        Http::post(config('app.whatsapp_send_url'), [
            'number' => $nomortujuan,
            'message' => $pesan,
        ]);
        return redirect(url('layanan/pembetulan-online'));
    }

    public function storeajuanOnlineVerifikasi(Request $request, $id)
    {

        // dd($request->all());
        if ($request->jns_transaksi != '3') {
            // pmeutakhiran
            // $flash = Pajak::prosesPenelitian($request);
            $flash = Pajak::prosesPenelitianDua($request);
            // log::info($flash);
        } else {
            // pembatalan objek
            $nop_proses = preg_replace('/[^0-9]/', '', $request->nop_proses);
            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);


            DB::beginTransaction();
            $msg = "";
            try {
                //code...
                $dataspop = (array) DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI FROM DAT_OBJEK_PAJAK A
                    JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                    JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                    WHERE a.kd_kecamatan='$kd_kecamatan'
                    AND a.kd_kelurahan='$kd_kelurahan'
                    AND a.kd_blok='$kd_blok'
                    AND a.no_urut='$no_urut'
                    AND a.kd_jns_op='$kd_jns_op' "))[0];

                $request->merge($dataspop);
                // dd($request->all());

                $spop = Pajak::pendataanSpop($request);
                Pajak::pendataanObjekPajak($request, $spop);
                $wherenop = [
                    ['kd_propinsi', '=', $kd_propinsi],
                    ['kd_dati2', '=', $kd_dati2],
                    ['kd_kecamatan', '=', $kd_kecamatan],
                    ['kd_kelurahan', '=', $kd_kelurahan],
                    ['kd_blok', '=', $kd_blok],
                    ['no_urut', '=', $no_urut],
                    ['kd_jns_op', '=', $kd_jns_op]
                ];
                // proses rekam unflag_history
                $ceklunas = DB::connection('oracle_satutujuh')->table(DB::raw('sppt'))
                    ->selectraw("kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt,status_pembayaran_sppt")
                    ->where($wherenop)
                    ->where('thn_pajak_sppt', date('Y'))->first();

                if ($ceklunas->status_pembayaran_sppt == 0) {
                    // blokir tahun berjalan
                    DB::connection('oracle_satutujuh')->table(DB::raw('sppt'))
                        ->where($wherenop)
                        ->where('thn_pajak_sppt', date('Y'))->update(['status_pembayaran_sppt' => 3]);

                    DB::connection('oracle_satutujuh')->table('unflag_history')
                        ->insert([
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'thn_pajak_sppt' => date('Y'),
                            'unflag_time' => Carbon::now(),
                            'unflag_desc' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek',
                            'unflag_mode' => 0
                        ]);
                }

                DB::connection('oracle_satutujuh')->table('unflag_history')
                    ->insert([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => date('Y'),
                        'unflag_time' => Carbon::now(),
                        'unflag_desc' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek',
                        'unflag_mode' => 0
                    ]);

                $penelitian = Layanan_objek::with('layanan')->findorfail($request->lhp_id);
                $penelitian->update([
                    'nomor_formulir' => $spop['no_formulir'],
                    'pemutakhiran_at' => Carbon::now(),
                    'pemutakhiran_by' => auth()->user()->id,
                    'keterangan_pembatalan' => $request->alasan <> '' ? $request->alasan : 'Pembatalan objek'
                ]);

                DB::commit();
                $msg = "Proses pembtalan objek telah di proses";
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                log::emergency($th);
                $msg = "Proses pembtalan objek gagal di proses";
            }
            $flash['msg'] = $msg;
        }
        // realese kepp
        KeepPenelitian::where('layanan_objek_id', $request->lhp_id)->delete();

        if ($flash['msg'] <> '') {
            $msg = $flash['msg'];
        } else {
            $msg = "Telah di proses";
        }

        return redirect(url('penelitian/kantor-hasil-penelitian', $request->lhp_id))->with(['success' => $msg]);




        /* DB::beginTransaction();
        try {
            // ambilnop

            $lo = Layanan_objek::where('id', $id)->first();

            $kd_propinsi = $lo->kd_propinsi;
            $kd_dati2 = $lo->kd_dati2;
            $kd_kecamatan = $lo->kd_kecamatan;
            $kd_kelurahan = $lo->kd_kelurahan;
            $kd_blok = $lo->kd_blok;
            $no_urut = $lo->no_urut;
            $kd_jns_op = $lo->kd_jns_op;

            $dataspop = (array) DB::connection("oracle_satutujuh")->select(DB::raw("SELECT 2 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_ASAL
                        ,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI 
                       FROM DAT_OBJEK_PAJAK A
                        JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                        JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                        WHERE a.kd_kecamatan='$kd_kecamatan'
                        AND a.kd_kelurahan='$kd_kelurahan'
                        AND a.kd_blok='$kd_blok'
                        AND a.no_urut='$no_urut'
                        AND a.kd_jns_op='$kd_jns_op' "))[0];
            $dataspop['lhp_id'] = $lo->id;
            $dataspop['jenis_layanan'] = 'x_8';
            $request->merge($dataspop);
            Pajak::pendatanSubjek($request);
            $spop = Pajak::pendataanSpop($request);
            Pajak::pendataanObjekPajak($request, $spop);

            Layanan_objek::where('id', $lo->id)
                ->update([
                    'nomor_formulir' => $spop['no_formulir'],
                    'pemutakhiran_at' => Carbon::now(),
                    'pemutakhiran_by' => Pajak::userid()
                ]);

            $cek = DB::connection('oracle_satutujuh')->table(DB::raw("sppt"))->select('status_pembayaran_sppt')
                ->whereraw("thn_pajak_sppt='" . date('Y') . "'
                and kd_kecamatan='" . $kd_kecamatan . "'
                and kd_kelurahan='" . $kd_kelurahan . "'
                and kd_blok='" . $kd_blok . "'
                and no_urut='" . $no_urut . "'
                and kd_jns_op='" . $kd_jns_op . "'
            ")->first();

            $obj = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'nip' => Pajak::nip(),
                'terbit' => date('Ymd'),
                'tempo' => date('dmY', strtotime('3009' . date('Y'))),
                'tahun' => date('Y')
            ];
            if ($cek) {
                if ($cek->status_pembayaran_sppt <> '1') {
                    // Pajak::PenentuanNjop($obj);
                    Pajak::prosesPenetapan($obj);
                }
            } else {
                // Pajak::PenentuanNjop($obj);
                Pajak::prosesPenetapan($obj);
            }

            DB::commit();
            $flash = [
                'success' => 'Telah di proses'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            log::emergency($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('layanan/pembetulan-online'))->with($flash); */
    }

    public function saveLampiran(Request $request)
    {
        $nomor_layanan = $request->nomor_layanan;
        $layanan_objek_id = $request->layanan_objek_id;
        $nama_dokumen = $request->nama_dokumen;
        $keterangan = $request->keterangan;

        DB::beginTransaction();
        try {
            //code...   
            Layanan_dokumen::create([
                'nomor_layanan' => $nomor_layanan,
                'tablename' => 'LAYANAN_OBJEK',
                'keyid' => $layanan_objek_id,
                'filename' => '-',
                'disk' => '-',
                'nama_dokumen' => $nama_dokumen,
                'keterangan' => $keterangan
            ]);
            DB::commit();
            $status = "Berhasil di tambahkan";
        } catch (\Throwable $th) {
            log::emergency($th);
            $status = $th->getMessage();
        }

        $res['keterangan'] = $status;
        $res['data'] = Layanan_dokumen::where('nomor_layanan', $nomor_layanan)
            ->where('keyid', $layanan_objek_id)->selectraw('nama_dokumen,keterangan,keyid,nomor_layanan')->orderby('id', 'asc')->get();

        return $res;
    }

    public function hapusLampiran(Request $request)
    {
        $id = $request->id;
        $dokumen = Layanan_dokumen::where('id', $id)->first();
        $nomor_layanan = $dokumen->nomor_layanan;
        $keyid = $dokumen->keyid;

        try {
            DB::beginTransaction();
            Layanan_dokumen::where('id', $id)->delete();
            DB::commit();
            $status = "berhasil di hapus";
        } catch (\Throwable $th) {
            //throw $th;
            log::emergency($th);
            DB::rollBack();
            $status = $th->getMessage();
        }

        $res['keterangan'] = $status;
        $res['data'] = Layanan_dokumen::where('nomor_layanan', $nomor_layanan)
            ->where('keyid', $keyid)->selectraw('nama_dokumen,keterangan,keyid,nomor_layanan,id')->orderby('id', 'asc')->get();
        return $res;
    }
}
