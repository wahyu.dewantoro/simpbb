<?php

namespace App\Http\Controllers;

use App\Helpers\PenetapanSppt;
use App\Models\Billing_kolektif;
use App\Models\DAT_OBJEK_PAJAK;
use App\Models\Data_billing;
use App\Models\SuratTagihan;
use App\Models\SuratTagihanNop;
use App\pegawaiStruktural;
use App\TempatBayar;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Log;
use PDO;

class SuratTagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = SuratTagihan::join('users', 'surat_tagihan.created_by', '=', 'users.id')
                ->select(db::raw("surat_tagihan.id,surat_nomor,surat_tanggal,kd_propinsi||kd_dati2||surat_tagihan.kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop,nm_wp,(select LISTAGG(thn_pajak_sppt, ', ') WITHIN GROUP (ORDER BY thn_pajak_sppt) AS tahun
                from surat_tagihan_nop
               where surat_tagihan_id=surat_tagihan.id) tagihan,nama created_by"))
                ->whereraw("surat_tagihan.deleted_at is null")
                ->orderby("surat_tagihan.id", "desc");
            return  datatables()::of($data)
                ->filterColumn('nop', function ($query, $keyword) {
                    $keyword = onlyNumber($keyword);
                    $sql = "(kd_propinsi||kd_dati2||surat_tagihan.kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op)  like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                })
                ->editColumn('nop', function ($row) {
                    return formatnop($row->nop);
                })
                ->editColumn('surat_tanggal', function ($row) {
                    return tglIndo($row->surat_tanggal);
                })
                ->addColumn('aksi', function ($row) {
                    $des = url('surat-tagihan-hapus', $row->id);
                    $act = "";
                    $cetak = route('surat-tagihan.cetak', $row->id);
                    $act .= '<a data-href="' . $cetak . '" class="cetak-sk"><i class="fas fa-file-pdf text-warning"></i></a> ';
                    $act .= "<a href='" . url('surat-tagihan-kobil', $row->id) . "' data-title='Kode billing'><i class='text-success fas fa-money-check-alt'></i></a>";
                    if (Auth()->user()->hasRole('Super User') == true) {
                        $act .= ' <a class="hapus" data-url="' . $des . '"><i class="fas fa-trash text-danger"></i></a>';
                    }
                    return $act;
                })
                ->rawColumns(['nop', 'aksi', 'surat_tanggal'])->make(true);
        }
        return view('tagihan.index');
    }

    public function cetakPdf($id)
    {
        $st = SuratTagihan::whereId($id)->wherenull('deleted_at')
            ->select(db::raw("surat_tagihan.*,(select LISTAGG(thn_pajak_sppt, ', ') WITHIN GROUP (ORDER BY thn_pajak_sppt) AS tahun
            from surat_tagihan_nop
           where surat_tagihan_id=surat_tagihan.id) tahun_tagihan,(select jalan_op from pbb.dat_objek_pajak where kd_propinsi=surat_tagihan.kd_propinsi and
           kd_dati2=surat_tagihan.kd_dati2 and
           kd_kecamatan=surat_tagihan.kd_kecamatan and
           kd_kelurahan=surat_tagihan.kd_kelurahan and
           kd_blok=surat_tagihan.kd_blok and
           no_urut=surat_tagihan.no_urut and
           kd_jns_op=surat_tagihan.kd_jns_op) jalan_op,(select nm_kelurahan from pbb.ref_Kelurahan where kd_propinsi=surat_tagihan.kd_propinsi and
           kd_dati2=surat_tagihan.kd_dati2 and
           kd_kecamatan=surat_tagihan.kd_kecamatan and
           kd_kelurahan=surat_tagihan.kd_kelurahan) nm_kelurahan,(select nm_kecamatan from pbb.ref_kecamatan where kd_propinsi=surat_tagihan.kd_propinsi and
           kd_dati2=surat_tagihan.kd_dati2 and
           kd_kecamatan=surat_tagihan.kd_kecamatan ) nm_kecamatan"))
            ->first();
        if ($st == null) {
            abort(404);
        }

        $sppt = SuratTagihanNop::join(db::raw("pbb.sppt"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'surat_tagihan_nop.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'surat_tagihan_nop.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'surat_tagihan_nop.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'surat_tagihan_nop.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'surat_tagihan_nop.kd_blok')
                ->on('sppt.no_urut', '=', 'surat_tagihan_nop.no_urut')
                ->on('sppt.kd_jns_op', '=', 'surat_tagihan_nop.kd_jns_op')
                ->on('sppt.thn_pajak_sppt', '=', 'surat_tagihan_nop.thn_pajak_sppt');
        })
            ->leftjoin(db::raw("pbb.sppt_potongan"), function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })->select(db::raw("sppt.thn_pajak_sppt,
            pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb"))
            ->wheresurat_tagihan_id($id)->get();
        $tp = TempatBayar::selectraw("listagg(nama_tempat,', ') within group(order by id) tp")->first();
        $url =   config('app.url') . '/surat-tagihan-cetak/' . $id;

        $page = view('tagihan.cetak', compact('st', 'sppt', 'url', 'tp'));
        $basename = 'surat_tagihan_' . time();
        $filename = $basename . '.pdf';


        $pdf = SnappyPdf::loadHTML($page)
            ->setOption('enable-local-file-access', true)
            ->setPaper('A4');
        return $pdf->stream($filename);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $nop = $request->nop;
            $res = onlyNumber($nop);

            $kd_propinsi = substr($res, 0, 2);
            $kd_dati2 = substr($res, 2, 2);
            $kd_kecamatan = substr($res, 4, 3);
            $kd_kelurahan = substr($res, 7, 3);
            $kd_blok = substr($res, 10, 3);
            $no_urut = substr($res, 13, 4);
            $kd_jns_op = substr($res, 17, 1);


            $where['kd_propinsi'] = $kd_propinsi;
            $where['kd_dati2'] = $kd_dati2;
            $where['kd_kecamatan'] = $kd_kecamatan;
            $where['kd_kelurahan'] = $kd_kelurahan;
            $where['kd_blok'] = $kd_blok;
            $where['no_urut'] = $no_urut;
            $where['kd_jns_op'] = $kd_jns_op;

            // cek sppt
            $cek_sppt = DB::connection("oracle_satutujuh")->select(db::raw("select tahun, ( select count(1)  
                                                                                            from sppt
                                                                                        where     sppt.kd_propinsi = '$kd_propinsi'
                                                                                                and sppt.kd_dati2 = '$kd_dati2'
                                                                                                and sppt.kd_kecamatan = '$kd_kecamatan'
                                                                                                and sppt.kd_kelurahan = '$kd_kelurahan'
                                                                                                and sppt.kd_blok = '$kd_blok'
                                                                                                and sppt.no_urut = '$no_urut'
                                                                                                and sppt.kd_jns_op = '$kd_jns_op'
                                                                                                and  sppt.thn_pajak_sppt=a.tahun
                                                                                ) jum
                                                                                from (    select   (select min (thn_pajak_sppt) thn_min
                                                                                            from sppt
                                                                                        where     sppt.kd_propinsi = '$kd_propinsi'
                                                                                                and sppt.kd_dati2 = '$kd_dati2'
                                                                                                and sppt.kd_kecamatan = '$kd_kecamatan'
                                                                                                and sppt.kd_kelurahan = '$kd_kelurahan'
                                                                                                and sppt.kd_blok = '$kd_blok'
                                                                                                and sppt.no_urut = '$no_urut'
                                                                                                and sppt.kd_jns_op = '$kd_jns_op')
                                                                                    + level
                                                                                    - 1
                                                                                        tahun
                                                                                from dual
                                                                                connect by   (select min (thn_pajak_sppt) thn_min
                                                                                            from sppt
                                                                                        where     sppt.kd_propinsi = '$kd_propinsi'
                                                                                                and sppt.kd_dati2 = '$kd_dati2'
                                                                                                and sppt.kd_kecamatan = '$kd_kecamatan'
                                                                                                and sppt.kd_kelurahan = '$kd_kelurahan'
                                                                                                and sppt.kd_blok = '$kd_blok'
                                                                                                and sppt.no_urut = '$no_urut'
                                                                                                and sppt.kd_jns_op = '$kd_jns_op')
                                                                                    + level
                                                                                    - 1 <= to_char (sysdate, 'yyyy')) a"));

            // cek ketetapan kosong
            $ps = [];
            foreach ($cek_sppt as $row) {
                if ($row->jum == 0) {
                    $ps[] = $row->tahun;
                }
            }

            if (count($ps) > 0) {
                foreach ($ps as $tahun) {
                    PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun, carbon::now(), carbon::now(), true);
                    // hapus tgl terbit
                    DB::connection("oracle_satutujuh")->table('sppt')
                        ->whereraw("sppt.kd_propinsi = '$kd_propinsi'
                    and sppt.kd_dati2 = '$kd_dati2'
                    and sppt.kd_kecamatan = '$kd_kecamatan'
                    and sppt.kd_kelurahan = '$kd_kelurahan'
                    and sppt.kd_blok = '$kd_blok'
                    and sppt.no_urut = '$no_urut'
                    and sppt.kd_jns_op = '$kd_jns_op' and thn_pajak_sppt='$tahun' ")->update(['tgl_terbit_sppt' => null]);
                }
            }

            $op = DAT_OBJEK_PAJAK::join('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
                ->select(DB::raw("dat_objek_pajak.kd_propinsi,dat_objek_pajak.kd_dati2,dat_objek_pajak.kd_kecamatan,dat_objek_pajak.kd_kelurahan,dat_objek_pajak.kd_blok,dat_objek_pajak.no_urut,dat_objek_pajak.kd_jns_op,dat_objek_pajak.subjek_pajak_id,
            nm_wp,jalan_wp||' '||blok_kav_no_wp alamat_wp,rt_wp,rw_wp,kelurahan_wp,kota_wp"))
                ->where($where)->first();

            $exp = explode('-', $op->kelurahan_wp);
            $nm_kelurahan_wp = $exp[0] ?? '-';
            $nm_kecamatan_wp = $exp[1] ?? '-';

            $exd = explode('-', $op->kota_wp);
            $nm_dati2_wp = $exd[0] ?? '-';
            $nm_propinsi_wp = $exd[1] ?? '-';


            $sppt = DB::connection("oracle_satutujuh")->table('sppt')
                ->leftjoin('sppt_potongan', function ($join) {
                    $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                        ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                        ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                        ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                        ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                        ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                        ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
                })
                ->leftjoin('pengurang_piutang', function ($join) {
                    $join->on('sppt.kd_propinsi', '=', 'pengurang_piutang.kd_propinsi')
                        ->on('sppt.kd_dati2', '=', 'pengurang_piutang.kd_dati2')
                        ->on('sppt.kd_kecamatan', '=', 'pengurang_piutang.kd_kecamatan')
                        ->on('sppt.kd_kelurahan', '=', 'pengurang_piutang.kd_kelurahan')
                        ->on('sppt.kd_blok', '=', 'pengurang_piutang.kd_blok')
                        ->on('sppt.no_urut', '=', 'pengurang_piutang.no_urut')
                        ->on('sppt.thn_pajak_sppt', '=', 'pengurang_piutang.thn_pajak_sppt');
                    // ->whereraw("sppt_koreksi.jns_koreksi  in ('1','3')");
                })
                ->select(db::raw(" sppt.thn_pajak_sppt,pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb,status_pembayaran_sppt,no_transaksi"))
                ->whereraw("sppt.KD_PROPINSI = '" . $where['kd_propinsi'] . "' and sppt.KD_DATI2 = '" . $where['kd_dati2'] . "' 
                            and sppt.KD_KECAMATAN = '" . $where['kd_kecamatan'] . "' and sppt.KD_KELURAHAN = '" . $where['kd_kelurahan'] . "' 
                            and sppt.KD_BLOK = '" . $where['kd_blok'] . "' 
                            and sppt.NO_URUT = '" . $where['no_urut'] . "' and sppt.KD_JNS_OP = '" . $where['kd_jns_op'] . "'
                            and status_pembayaran_sppt !='1'
                            and (  sppt.thn_pajak_sppt!=to_char(sysdate,'yyyy') 
                            or ( sppt.thn_pajak_sppt=to_char(sysdate,'yyyy') and trunc(sppt.tgl_jatuh_tempo_sppt)<trunc(sysdate) )
                           )
                            and pengurang_piutang.thn_pajak_sppt is null ")
                ->get();

            $tagihan = [];
            foreach ($sppt as $row) {
                if ($row->no_transaksi == '' && $row->status_pembayaran_sppt != '') {
                    $aa = [
                        $row->thn_pajak_sppt => $row->pbb
                    ];
                    $tagihan[$row->thn_pajak_sppt] = $row->pbb;
                }
            }
            // return $tagihan;
            $stpd = SuratTagihan::whereraw("KD_PROPINSI = '" . $where['kd_propinsi'] . "' and KD_DATI2 = '" . $where['kd_dati2'] . "' 
            and KD_KECAMATAN = '" . $where['kd_kecamatan'] . "' and KD_KELURAHAN = '" . $where['kd_kelurahan'] . "' 
            and KD_BLOK = '" . $where['kd_blok'] . "' 
            and NO_URUT = '" . $where['no_urut'] . "' and KD_JNS_OP = '" . $where['kd_jns_op'] . "' and deleted_at is null and to_char(created_at,'yyyy')=to_char(sysdate,'yyyy')")
                ->selectraw("surat_nomor")
                ->first();
            $response = [
                'kd_propinsi' => $op->kd_propinsi,
                'kd_dati2' => $op->kd_dati2,
                'kd_kecamatan' => $op->kd_kecamatan,
                'kd_kelurahan' => $op->kd_kelurahan,
                'kd_blok' => $op->kd_blok,
                'no_urut' => $op->no_urut,
                'kd_jns_op' => $op->kd_jns_op,
                'nik' => substr($op->subjek_pajak_id, 0, 16),
                'nm_wp' => $op->nm_wp,
                'jalan_wp' => $op->alamat_wp,
                'rt_wp' => $op->rt_wp == '' ? '0' : $op->rt_wp,
                'rw_wp' => $op->rw_wp == '' ? '0' : $op->rw_wp,
                'nm_kelurahan_wp' => $nm_kelurahan_wp == '' ? '-' : $nm_kelurahan_wp,
                'nm_kecamatan_wp' => $nm_kecamatan_wp == '' ? '-' : $nm_kecamatan_wp,
                'nm_dati2_wp' => $nm_dati2_wp == '' ? '-' : $nm_dati2_wp,
                'nm_propinsi_wp' => $nm_propinsi_wp == '' ? '-' : $nm_propinsi_wp,
                'tagihan' => $tagihan,
                'stpd' => $stpd->surat_nomor ?? ''
            ];
            return $response;
        }


        $data = [
            'action' => route('surat-tagihan.store'),
            'method' => 'post'
        ];
        return view('tagihan.form', compact('data'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (count($request->thn_pajak_sppt) == 0) {
            return redirect(route('surat-tagihan.create'))->with(['errorr' => 'Tidak ada tagihan']);
        }
        $surat = $request->only([
            'kd_propinsi',
            'kd_dati2',
            'kd_kecamatan',
            'kd_kelurahan',
            'kd_blok',
            'no_urut',
            'kd_jns_op',
            'nik',
            'nm_wp',
            'jalan_wp',
            'rt_wp',
            'rw_wp',
            'nm_kelurahan_wp',
            'nm_kecamatan_wp',
            'nm_dati2_wp',
            'nm_propinsi_wp',
        ]);

        if ($request->nm_kelurahan_wp == null) {
            $surat['nm_kelurahan_wp'] = '-';
        }
        if ($request->nm_kecamatan_wp == null) {
            $surat['nm_kecamatan_wp'] = '-';
        }


        $peg = pegawaiStruktural::where('kode', '01')->first();
        $srt = SuratTagihan::select(db::raw("max(nvl(cast(replace(replace(surat_nomor,'/STPD-PBB/35.07.205/'|| TO_CHAR (SYSDATE, 'yyyy'),''),'973/','') as number),0))+1 nomor"))
            ->whereraw("to_char(created_at,'yyyy')=to_char(sysdate,'yyyy')")->first();
        $surat['surat_kota'] = 'KEPANJEN';
        $surat['surat_pegawai'] = $peg->nama;
        $surat['surat_nip'] = $peg->nip;
        $surat['surat_jabatan'] = $peg->jabatan;
        $surat['surat_pangkat'] = '-';
        $surat['surat_tanggal'] = Carbon::now();
        $surat['surat_nomor'] = '973/' . $srt->nomor . '/STPD-PBB/35.07.205/' . date('Y');


        // dd($detail);
        DB::beginTransaction();
        try {
            $ins = SuratTagihan::create($surat);
            $detail = [];
            foreach ($request->thn_pajak_sppt as  $i => $item) {
                $detail[] = [
                    'surat_tagihan_id' => $ins->id,
                    'kd_propinsi' => $request->kd_propinsi,
                    'kd_dati2' => $request->kd_dati2,
                    'kd_kecamatan' => $request->kd_kecamatan,
                    'kd_kelurahan' => $request->kd_kelurahan,
                    'kd_blok' => $request->kd_blok,
                    'no_urut' => $request->no_urut,
                    'kd_jns_op' => $request->kd_jns_op,
                    'thn_pajak_sppt' => $item
                ];
            }
            SuratTagihanNop::insert($detail);
            DB::connection("oracle_satutujuh")->table("sppt")
                ->wherein('thn_pajak_sppt', $request->thn_pajak_sppt)
                ->whereraw("status_pembayaran_sppt='3'")
                ->update(['status_pembayaran_sppt' => '0']);
            DB::commit();
            $flash = [
                'success' => "berhasil membuat surat tagihan dengan nomor " . $ins->surat_nomor
            ];
        } catch (Exception $e) {
            // dd($e);

            db::rollBack();
            $flash['error'] = $e->getMessage();
        }
        return redirect(route('surat-tagihan.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            //code...
            SuratTagihan::where('id', $id)->update(['deleted_at' => Carbon::now(), 'deleted_by' => Auth()->user()->id]);
            DB::commit();
            $flash = ['success' => 'Berhasil hapus data'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            $flash = ['errorr' => $th->getMessage()];
        }

        return redirect(route('surat-tagihan.index'))->with($flash);
    }

    public function formKobil($id)
    {
        $st = SuratTagihan::whereId($id)->wherenull('deleted_at')
            ->select(db::raw("surat_tagihan.*,
            (SELECT data_billing_id
                  FROM data_billing
                 WHERE     deleted_at IS NULL
                       AND (   (kd_status = 0 AND expired_at > SYSDATE)
                            OR (kd_status = 1 AND expired_at < SYSDATE))
                       AND data_billing_id = surat_tagihan.data_billing_id)
                  id_kobil,
            (select LISTAGG(thn_pajak_sppt, ', ') WITHIN GROUP (ORDER BY thn_pajak_sppt) AS tahun
            from surat_tagihan_nop
           where surat_tagihan_id=surat_tagihan.id) tahun_tagihan,(select jalan_op from pbb.dat_objek_pajak where kd_propinsi=surat_tagihan.kd_propinsi and
           kd_dati2=surat_tagihan.kd_dati2 and
           kd_kecamatan=surat_tagihan.kd_kecamatan and
           kd_kelurahan=surat_tagihan.kd_kelurahan and
           kd_blok=surat_tagihan.kd_blok and
           no_urut=surat_tagihan.no_urut and
           kd_jns_op=surat_tagihan.kd_jns_op) jalan_op,(select nm_kelurahan from pbb.ref_Kelurahan where kd_propinsi=surat_tagihan.kd_propinsi and
           kd_dati2=surat_tagihan.kd_dati2 and
           kd_kecamatan=surat_tagihan.kd_kecamatan and
           kd_kelurahan=surat_tagihan.kd_kelurahan) nm_kelurahan,(select nm_kecamatan from pbb.ref_kecamatan where kd_propinsi=surat_tagihan.kd_propinsi and
           kd_dati2=surat_tagihan.kd_dati2 and
           kd_kecamatan=surat_tagihan.kd_kecamatan ) nm_kecamatan"))
            ->first();
        if ($st == null) {
            abort(404);
        }

        $sppt = SuratTagihanNop::join(db::raw("spo.sppt_oltp sppt"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'surat_tagihan_nop.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'surat_tagihan_nop.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'surat_tagihan_nop.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'surat_tagihan_nop.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'surat_tagihan_nop.kd_blok')
                ->on('sppt.no_urut', '=', 'surat_tagihan_nop.no_urut')
                ->on('sppt.kd_jns_op', '=', 'surat_tagihan_nop.kd_jns_op')
                ->on('sppt.thn_pajak_sppt', '=', 'surat_tagihan_nop.thn_pajak_sppt');
        })

            ->leftjoin(DB::raw("(SELECT DISTINCT b.kd_propinsi,
                                                b.kd_dati2,
                                                b.kd_kecamatan,
                                                b.kd_kelurahan,
                                                b.kd_blok,
                                                b.no_urut,
                                                b.kd_jns_op,
                                                b.tahun_pajak thn_pajak_sppt
                                    FROM data_billing a
                                    JOIN billing_kolektif b
                                        ON a.data_billing_id = b.data_billing_id
                                    WHERE     a.deleted_at IS NULL
                                    AND (   (a.kd_status = 0 AND a.expired_at > SYSDATE)
                                            OR (a.kd_status = 1 AND a.expired_at < SYSDATE))) bb"), function ($join) {
                $join->on('surat_tagihan_nop.kd_propinsi', '=', 'bb.kd_propinsi')
                    ->on('surat_tagihan_nop.kd_dati2', '=', 'bb.kd_dati2')
                    ->on('surat_tagihan_nop.kd_kecamatan', '=', 'bb.kd_kecamatan')
                    ->on('surat_tagihan_nop.kd_kelurahan', '=', 'bb.kd_kelurahan')
                    ->on('surat_tagihan_nop.kd_blok', '=', 'bb.kd_blok')
                    ->on('surat_tagihan_nop.no_urut', '=', 'bb.no_urut')
                    ->on('surat_tagihan_nop.kd_jns_op', '=', 'bb.kd_jns_op')
                    ->on('surat_tagihan_nop.thn_pajak_sppt', '=', 'bb.thn_pajak_sppt');
            })
            ->select(db::raw("sppt.thn_pajak_sppt,
            pbb_terhutang_sppt pbb,
            faktor_pengurang_sppt potongan,
            denda,
            case when bb.thn_pajak_sppt is not null then '1' else null end nota,
            case when status_pembayaran_sppt='1' then '1' else null end lunas"))
            ->wheresurat_tagihan_id($id)->get();
        // return $sppt;
        $kobil = [];
        if ($st->id_kobil != '') {
            $kobil = DB::select(DB::raw(" select data_billing.kobil,data_billing.tahun_pajak tahun_kobil,
            billing_kolektif.tahun_pajak thn_pajak_sppt,pokok,denda,total,data_billing.expired_at,kd_status
            from data_billing 
            join billing_kolektif on data_billing.data_billing_id=billing_Kolektif.data_billing_id
            where data_billing.data_billing_id='" . $st->id_kobil . "'
            order by billing_kolektif.tahun_pajak asc"));
        }


        $jumlah_pokok = array_map(function ($kobil) {
            return $kobil->pokok;
        }, $kobil);

        $jumlah_denda = array_map(function ($kobil) {
            return $kobil->denda;
        }, $kobil);

        $jumlah_total = array_map(function ($kobil) {
            return $kobil->total;
        }, $kobil);

        $jumlah_pokok = array_sum($jumlah_pokok);
        $jumlah_denda = array_sum($jumlah_denda);
        $jumlah_total = array_sum($jumlah_total);


        return view('tagihan.kodeBilling', compact('sppt', 'st', 'kobil', 'jumlah_pokok', 'jumlah_denda', 'jumlah_total'));
    }

    public function formKobilStore(Request $request, $id)
    {

        DB::beginTransaction();
        try {
            //code...

            $st = SuratTagihan::where('id', $id)->first();
            $kd_propinsi = $st->kd_propinsi;
            $kd_dati2 = $st->kd_dati2;
            $kd_kecamatan = $st->kd_kecamatan;
            $kd_kelurahan = $st->kd_kelurahan;
            $kd_blok = $st->kd_blok;
            $no_urut = $st->no_urut;
            $kd_jns_op = $st->kd_jns_op;

            $wt = "(";
            foreach ($request->thn_pajak_sppt as $thn_pajak_sppt) {
                $wt .= " thn_pajak_sppt='$thn_pajak_sppt' or";
            }

            $wt = substr($wt, 0, -2);
            $wt = $wt . ')';


            $tagihan = DB::connection("oracle_spo")->table(DB::raw("(
            select sppt_oltp.*
            from sppt_oltp
            left join pbb.sppt_koreksi on sppt_oltp.kd_propinsi=sppt_koreksi.kd_propinsi and
            sppt_oltp.kd_dati2=sppt_koreksi.kd_dati2 and
            sppt_oltp.kd_Kecamatan=sppt_koreksi.kd_Kecamatan and
            sppt_oltp.kd_kelurahan=sppt_koreksi.kd_kelurahan and
            sppt_oltp.kd_blok=sppt_koreksi.kd_blok and
            sppt_oltp.no_urut=sppt_koreksi.no_urut and
            sppt_oltp.kd_jns_op=sppt_koreksi.kd_jns_op and
            sppt_oltp.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt and
            sppt_koreksi.jns_koreksi='3'
            where jns_koreksi is null
        )  sppt_oltp"))
                ->select(db::raw("sppt_oltp.kd_propinsi,
                                    sppt_oltp.kd_dati2,
                                    sppt_oltp.kd_Kecamatan,
                                    sppt_oltp.kd_kelurahan,
                                    sppt_oltp.kd_blok,
                                    sppt_oltp.no_urut,
                                    sppt_oltp.kd_jns_op,
                                    sppt_oltp.thn_pajak_sppt  tahun_pajak,
                            PBB_YG_HARUS_DIBAYAR_SPPT  pbb,nm_wp_sppt nm_wp,PBB_YG_HARUS_DIBAYAR_SPPT pokok   , denda denda,PBB_YG_HARUS_DIBAYAR_SPPT+denda total"))
                ->whereraw("status_pembayaran_sppt!='1'  and kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan'  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op' and $wt ")
                ->get();
            // return $tagihan;

            $arTagihan = [];
            foreach ($tagihan as $tagihan) {
                $arTagihan[] = [
                    'kd_propinsi' => $tagihan->kd_propinsi,
                    'kd_dati2' => $tagihan->kd_dati2,
                    'kd_kecamatan' => $tagihan->kd_kecamatan,
                    'kd_kelurahan' => $tagihan->kd_kelurahan,
                    'kd_blok' => $tagihan->kd_blok,
                    'no_urut' => $tagihan->no_urut,
                    'kd_jns_op' => $tagihan->kd_jns_op,
                    'tahun_pajak' => $tagihan->tahun_pajak,
                    'pbb' => $tagihan->pbb,
                    'nm_wp' => $tagihan->nm_wp,
                    'pokok' => $tagihan->pokok,
                    'denda' => $tagihan->denda,
                    'total' => $tagihan->total
                ];
            }

            if (count($arTagihan) > 0) {
                $nso = DB::connection("oracle_satutujuh")->table("dat_subjek_pajak")
                    ->join('dat_objek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
                    ->select("nm_wp")
                    ->whereraw("kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and kd_kecamatan='$kd_kecamatan'  and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")
                    ->first();
                $nm_wp = $nso->nm_wp ?? 'Wajib Pajak';

                // return $nm_wp;
                $year = date('Y');
                $urut = DB::table('data_billing')
                    ->select(db::raw("nvl(max( CAST(no_urut AS NUMBER)),0) nomer"))->where('tahun_pajak', $year)
                    ->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_jns_op', '5')
                    ->first();
                $kode = $urut->nomer;

                $no_urut = sprintf("%04s", $kode + 1);
                $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . '999' . $no_urut . '5';
                $lokasi = DB::connection('oracle_dua')->table('ref_kelurahan')
                    ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->selectraw("nm_kecamatan,nm_kelurahan")
                    ->whereraw("ref_kecamatan.KD_KECAMATAN ='$kd_kecamatan' and KD_KELURAHAN = '$kd_kelurahan'")->first();
                $kecamatan = $lokasi->nm_kecamatan;
                $kelurahan = $lokasi->nm_kelurahan;
                $setDatabiling = array(
                    "KD_PROPINSI" => $kd_propinsi,
                    "KD_DATI2" => $kd_dati2,
                    "KD_KECAMATAN" => $kd_kecamatan,
                    "KD_KELURAHAN" => $kd_kelurahan,
                    "KD_BLOK" => '999',
                    "NO_URUT" => $no_urut,
                    "KD_JNS_OP" => '5',
                    "TAHUN_PAJAK" => $year,
                    "KD_STATUS" => "0",
                    "NAMA_WP" => $nm_wp,
                    "NAMA_KELURAHAN" => $kelurahan,
                    "NAMA_KECAMATAN" => $kecamatan,
                    "KOBIL" => $kobil
                );
                $Data_billing = Data_billing::create($setDatabiling);
                // $Data_billing="";
                $getId = $Data_billing->data_billing_id;
                $url_cetak = url('data_dafnom/pdf_cetak') . '/' . $getId;
                $kode_billing = $kobil;
                $billing_kolektif = [];
                $tglNow = Carbon::now();
                $montNow = $tglNow->month;
                $next = $tglNow->addDays(1);
                if ($montNow != $next->month) {
                    $next = Carbon::now()->endOfMonth();
                }

                foreach ($arTagihan as $tag) {
                    $billing_kolektif[] = [
                        'data_billing_id' => $getId,
                        'kd_propinsi' => $tag['kd_propinsi'],
                        'kd_dati2' => $tag['kd_dati2'],
                        'kd_kecamatan' => $tag['kd_kecamatan'],
                        'kd_kelurahan' => $tag['kd_kelurahan'],
                        'kd_blok' => $tag['kd_blok'],
                        'no_urut' => $tag['no_urut'],
                        'kd_jns_op' => $tag['kd_jns_op'],
                        'tahun_pajak' => $tag['tahun_pajak'],
                        'pbb' => $tag['pbb'],
                        'nm_wp' => $tag['nm_wp'],
                        'pokok' => $tag['pokok'],
                        'denda' => $tag['denda'],
                        'total' => $tag['total'],
                        'expired_at' => new carbon($Data_billing->expired_at ?? '')
                    ];
                }
                Billing_kolektif::insert($billing_kolektif);

                // update 
                SuratTagihan::where('id', $id)
                    ->update(['data_billing_id' => $Data_billing->data_billing_id]);
            }
            DB::commit();
            $notif = ['success' => 'Berhasil membuat kobil'];
        } catch (\Throwable $th) {
            //throw $th;
            $notif = ['error' => $th->getMessage()];
            DB::rollBack();
            Log::info($th);
        }

        return redirect(url('surat-tagihan-kobil', $id))->with($notif);
    }
}
