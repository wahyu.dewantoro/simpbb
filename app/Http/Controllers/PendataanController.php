<?php

namespace App\Http\Controllers;

use AlterPendataanObjek;
use App\Exports\ListPendataan;
use App\Exports\PendataanObjekExcel;
use App\Exports\PendataanSubjekExcel;
use App\Helpers\Pajak;
use App\Helpers\PembatalanObjek;
use App\Helpers\Pendataan;
use App\Imports\PendataanImport;
use App\Jobs\pendataanObjek;
use App\Kecamatan;
use App\Models\DAT_OBJEK_PAJAK;
use App\Models\JenisPendataan;
use App\Models\PendataanBatch;
use App\Models\PendataanObjek as ModelsPendataanObjek;
use App\Models\PendataanObjekBng;
use App\ObjekPajak;
use App\RekonBatch;
use App\TblSpop;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use PDO;
use Yajra\DataTables\DataTables;

use SnappyPdf as PDF;

class PendataanController extends Controller
{
    //

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PendataanBatch::ListPendataan()
                ->orderby('pendataan_batch.id', 'desc');
            return  DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->has('status')) {
                        $status = $request->status;
                        // Log::info('status =' . $status);
                        if (is_null($status) == false) {
                            if ($status == 'x') {
                                $query->whereraw("verifikasi_kode is null");
                            } else {
                                $query->whereraw("verifikasi_kode='$status'");
                            }
                        }
                    }

                    if ($request->has('jenis_pendataan_id')) {
                        $jenis_pendataan_id = $request->jenis_pendataan_id;
                        if (is_null($jenis_pendataan_id) == false) {
                            $query->whereraw("jenis_pendataan_id='$jenis_pendataan_id'");
                        }
                    }

                    if ($request->has('created_by')) {
                        $created_by = $request->get('created_by');
                        if (!empty($created_by)) {
                            $query->whereraw("pendataan_batch.created_by='$created_by'");
                        }
                    }
                    if ($request->has('tanggal')) {
                        $tanggal = $request->get('tanggal');
                        if (!empty($tanggal)) {
                            $query->whereraw("trunc(pendataan_batch.created_at)= trunc(to_date('$tanggal','dd-mm-yyyy')) ");
                        }
                    }
                })
                ->filterColumn('pencarian_data', function ($query, $string) {
                    $keyword = strtolower($string);
                    $nopproses = onlyNumber($string);
                    $wh = "lower(nama_pendataan) like '%$keyword%' or lower(users.nama) like '%$keyword%'  ";
                    if ($nopproses != '') {
                        $wh .= " or pendataan_batch.nomor_batch like '%$nopproses%'  ";
                    }
                    $query->whereRaw(DB::raw($wh));
                })
                ->addColumn('tanggal_indo', function ($row) {
                    return tglIndo($row->tanggal);
                })
                ->addColumn('status', function ($row) {
                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $st = "<i class='far fa-check-circle text-success'></i> Disetujui";
                            break;
                        case '2':
                            # code...
                            $st = "<i class='far fa-check-circle text-success'></i> Verifikasi PETA";
                            break;

                        case '0':
                            # code...
                            $st = "<i class='fas fa-exclamation-circle text-danger'></i> Tidak Disetujui";
                            break;
                        default:
                            # code...
                            $st = "<i class='fas fa-user-clock text-warning'></i> Belum di verifikasi";
                            break;
                    }

                    return $st;
                })
                ->addColumn('pilih', function ($row) {
                    $aksi = "";
                    if ($row->verifikasi_at == '') {
                        $aksi .= " <a  role='button' data-toggle='tooltip'  data-href='" . route('pendataan.delete', $row->id) . "' data-placement='top' title='Hapus Pendataan'   class='hapus' ><i class='fas fa-trash-alt text-danger '></i></a>";
                    }
                    $aksi .= " <a href='" . route('pendataan.show', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Detail Pendataan'   class='nop-detail' ><i class='fas text-success fa-binoculars'></i></a>";

                    return $aksi;
                })
                ->rawColumns(['pilih', 'status', 'tanggal_indo'])->make(true);
        }
        $pembuat = PendataanBatch::join('users', 'pendataan_batch.created_by', '=', 'users.id')
            ->selectraw("distinct created_by,nama")->get();
        $jp = JenisPendataan::get();

        return view('pendataan.index', compact('pembuat', 'jp'));
    }

    public function cetak_listpendataan(Request $request)
    {
        $data = PendataanBatch::ListPendataan()
            ->orderby('pendataan_batch.id', 'desc')
            ->where(function ($query) use ($request) {
                if ($request->has('status')) {
                    $status = $request->status;
                    if (is_null($status) == false) {
                        if ($status == 'x') {
                            $query->whereraw("verifikasi_kode is null");
                        } else {
                            $query->whereraw("verifikasi_kode='$status'");
                        }
                    }
                }
                if ($request->has('jenis_pendataan_id')) {
                    $jenis_pendataan_id = $request->jenis_pendataan_id;
                    if (is_null($jenis_pendataan_id) == false) {
                        $query->whereraw("jenis_pendataan_id='$jenis_pendataan_id'");
                    }
                }
                if ($request->has('created_by')) {
                    $created_by = $request->get('created_by');
                    if (!empty($created_by)) {
                        $query->whereraw("pendataan_batch.created_by='$created_by'");
                    }
                }
                if ($request->has('tanggal')) {
                    $tanggal = $request->get('tanggal');
                    if (!empty($tanggal)) {
                        $query->whereraw("trunc(pendataan_batch.created_at)= trunc(to_date('$tanggal','dd-mm-yyyy')) ");
                    }
                }
            })->get();
        // dd($data->toArray());
        $search = [
            'jenis' => 'Semua',
            'status' => 'Semua',
            'pegawai' => 'Seluruh Pegawai',
            'tanggal' => 'Semua',
        ];
        if ($request->has('status')) {
            if ($request->status) {
                $list = [
                    "x" => "Belum di verifikasi",
                    "2" => "Verifikasi Peta",
                    "1" => "Disetujui",
                    "0" => "Tidak Disetujui"
                ];
                if (in_array($request->status, array_keys($list))) {
                    $search['status'] = $list[$request->status];
                }
            }
        }
        if ($request->has('jenis_pendataan_id')) {
            if ($request->jenis_pendataan_id) {
                $list = [
                    "1" => "SISMIOP",
                    "2" => "Mutasi Pecah",
                    "3" => "Penilaian Individu",
                    "4" => "Pendataan Baru",
                    "5" => "Pembatalan",
                    "6" => "Pembetulan"
                ];
                if (in_array($request->jenis_pendataan_id, array_keys($list))) {
                    $search['jenis'] = $list[$request->jenis_pendataan_id];
                }
            }
        }
        if ($request->has('created_by')) {
            if ($request->created_by) {
                $getPegawai = User::select(['nama'])->where(['id' => $request->created_by])->get();
                if ($getPegawai->count()) {
                    $search['pegawai'] = $getPegawai->first()->nama;
                }
            }
        }
        if ($request->has('tanggal')) {
            if ($request->tanggal) {
                $search['tanggal'] = $request->tanggal;
            }
        }
        $export = new ListPendataan($data->toArray(), $search);
        return Excel::download($export, 'daftar_nop_pendataan.xlsx');
    }

    public function create()
    {
        $jenis = JenisPendataan::whereraw("id not in (1,2,5)")->get();
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $data = [
            'method' => 'POST',
            'action' => route('pendataan.store'),
            'title' => 'Pembetulan / Mutasi Objek',
            'jtr' => [1 => 'Perekaman', 'Pemutakhiran', 'Pembatalan']
        ];
        return view('pendataan.form', compact('data', 'jenis', 'kecamatan'));
    }


    public function edit($id)
    {
        $jenis = JenisPendataan::get();
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $objek = ModelsPendataanObjek::find($id);
        $arr_penetapan = $objek->penetapan()->select(db::raw("LISTAGG(mulai_tahun||'|'||to_char(tgl_jatuh_tempo,'YYYY-mm-dd'), ',') WITHIN GROUP (ORDER BY mulai_tahun) mulai_tahun"))->first();
        $data = [
            'method' => 'POST',
            'action' => route('pendataan.update', $id),
            'title' => 'Pendataan Objek',
            'jtr' => [1 => 'Perekaman', 'Pemutakhiran', 'Pembatalan'],
            'objek' => $objek,
            'arr_penetapan' => $arr_penetapan->mulai_tahun
        ];
        // dd($data['arr_penetapan']);
        //  return $data['objek']->bng->count();
        return view('pendataan.form', compact('data', 'jenis', 'kecamatan'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        db::beginTransaction();
        try {
            $userid = user()->id;
            $now = Carbon::now();

            $jns_pendataan = $request->jns_pendataan;
            $batchData = [
                'jenis_pendataan_id' => $jns_pendataan,
                'tipe' => 'PD'
            ];
            $batch = PendataanBatch::create($batchData);
            // objek
            $res = [];

            if (in_array($jns_pendataan, [1, 3, 5, 6])) {
                $angkaa = onlyNumber($request->nop_asal);
                $kd_kecamatan = substr($angkaa, 4, 3);
                $kd_kelurahan = substr($angkaa, 7, 3);
                $kd_blok =  substr($angkaa, 10, 3);
                $no_urut = substr($angkaa, 13, 4);
                $kd_jns_op = substr($angkaa, 17, 1);
            } else {
                $kd_kecamatan = $request->kd_kecamatan;
                $kd_kelurahan = $request->kd_kelurahan;
                $kd_blok = padding($request->kd_blok, 0, 3);
                $no_urut = null;
                $kd_jns_op = null;
            }

            // $batal = [];
            if ($jns_pendataan == 5) {
                $batal = DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,substr(A.SUBJEK_PAJAK_ID,1,16) SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,5 JNS_BUMI FROM DAT_OBJEK_PAJAK A
                JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                WHERE a.kd_kecamatan='$kd_kecamatan'
                AND a.kd_kelurahan='$kd_kelurahan'
                AND a.kd_blok='$kd_blok'
                AND a.no_urut='$no_urut'
                AND a.kd_jns_op='$kd_jns_op' "))[0];
            }


            // $num = 4;
            // $num_padded = sprintf("%02d", $num);
            // echo $num_padded; // returns 04

            $subjek_pajak_id = sprintf("%016d", onlyNumber($request->subjek_pajak_id ?? ($batal->subjek_pajak_id ?? '')));

            $dat_obj = [
                'pendataan_batch_id' => $batch->id,
                'kd_propinsi' => '35',
                'kd_dati2' => '07',
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' =>  padding($kd_blok, 0, 3),
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'nop_asal' => onlyNumber($request->nop_asal),
                'subjek_pajak_id' => $subjek_pajak_id,
                'nm_wp' => $request->nm_wp ?? ($batal->nm_wp ?? ''),
                'jalan_wp' => $request->jalan_wp ?? ($batal->jalan_wp ?? ''),
                'blok_kav_no_wp' => $request->blok_kav_no_wp ?? ($batal->blok_kav_no_wp ?? ''),
                'rw_wp' => padding($request->rw_wp ?? ($batal->rw_wp ?? ''), 0, 2),
                'rt_wp' => padding($request->rt_wp ?? ($batal->rt_wp ?? ''), 0, 3),
                'kelurahan_wp' => $request->kelurahan_wp ?? ($batal->kelurahan_wp ?? ''),
                'kecamatan_wp' => $request->kecamatan_wp ?? '',
                'propinsi_wp' => $request->propinsi_wp ?? '',
                'kota_wp' => $request->kota_wp ?? ($batal->kota_wp ?? ''),
                'kd_pos_wp' => $request->kd_pos_wp ?? ($batal->kd_pos_wp ?? ''),
                'telp_wp' => $request->telp_wp ?? ($batal->telp_wp ?? ''),
                'npwp' => $request->npwp,
                'status_pekerjaan_wp' => $request->status_pekerjaan_wp ?? ($batal->status_pekerjaan_wp ?? ''),
                'no_persil' => $request->no_persil ?? ($batal->no_persil ?? ''),
                'jalan_op' => $request->jalan_op ?? ($batal->jalan_op ?? ''),
                'blok_kav_no_op' => $request->blok_kav_no_op ?? ($batal->blok_kav_no_op ?? ''),
                'rw_op' => padding($request->rw_op ?? ($batal->rw_op ?? ''), 0, 2),
                'rt_op' => padding($request->rt_op ?? ($batal->rt_op ?? ''), 0, 3),
                'kd_status_cabang' => $request->kd_status_cabang ?? null,
                'kd_status_wp' => $request->kd_status_wp ?? ($batal->kd_status_wp ?? ''),
                'kd_znt' => $request->kd_znt ?? $batal->kd_znt,
                'luas_bumi' => $request->luas_bumi ?? $batal->luas_bumi,
                'jns_bumi' => $request->jns_bumi ?? $batal->jns_bumi,
                'created_at' => $now,
                'created_by' => $userid,
                'updated_at' => $now,
                'updated_by' => $userid
            ];
            // dd($dat_obj);
            // die();
            $objek = ModelsPendataanObjek::create($dat_obj);

            if (!empty($request->mulai_tahun)) {
                $penetapan = [];
                foreach ($request->mulai_tahun as $ai => $mulai_tahun) {
                    $penetapan[] = [
                        'pendataan_objek_id' => $objek->id,
                        'mulai_tahun' => $mulai_tahun,
                        'tgl_jatuh_tempo' => new carbon($request->tgl_jatuh_tempo[$ai]),
                        'is_nota' => $request->is_nota,
                        'hkpd_individu' => $request->hkpd_individu[$ai]
                    ];
                }

                if (count($penetapan) > 0) {
                    DB::table('pendataan_objek_penetapan')->insert($penetapan);
                }
            }


            if (!empty($request->jns_transaksi_lspop)) {
                foreach ($request->jns_transaksi_lspop as $i => $lspop) {
                    $nilai_individu = 0;
                    if (isset($request->nilai_individu[$i])) {
                        if (preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]) > 0) {
                            $nilai_individu = preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]);
                        }
                    }
                    $kd_jpb = padding($request->kd_jpb[$i], 0, 2) ?? '01';
                    $bng = [
                        'pendataan_objek_id' => $objek->id,
                        'no_bng' => $i + 1,
                        'kd_jpb' => $kd_jpb,
                        'thn_dibangun_bng' => $request->thn_dibangun_bng[$i] ?? date('Y'),
                        'thn_renovasi_bng' => $request->thn_renovasi_bng[$i] ?? null,
                        'luas_bng' => $request->luas_bng[$i] ?? 0,
                        'jml_lantai_bng' => $request->jml_lantai_bng[$i] ?? '1',
                        'kondisi_bng' => $request->kondisi_bng[$i] ?? '2',
                        'jns_konstruksi_bng' => $request->jns_konstruksi_bng[$i] ?? '3',
                        'jns_atap_bng' => $request->jns_atap_bng[$i] ?? '3',
                        'kd_dinding' => $request->kd_dinding[$i] ?? '3',
                        'kd_lantai' => $request->kd_lantai[$i] ?? '2',
                        'kd_langit_langit' => $request->kd_langit_langit[$i] ?? '3',
                        'daya_listrik' => $request->daya_listrik[$i] ?? null,
                        'acsplit' => $request->acsplit[$i] ?? null,
                        'acwindow' => $request->acwindow[$i] ?? null,
                        'acsentral' => $request->acsentral[$i] ?? null,
                        'luas_kolam' => $request->luas_kolam[$i] ?? null,
                        'finishing_kolam' => $request->finishing_kolam[$i] ?? null,
                        'luas_perkerasan_ringan' => $request->luas_perkerasan_ringan[$i] ?? null,
                        'luas_perkerasan_sedang' => $request->luas_perkerasan_sedang[$i] ?? null,
                        'luas_perkerasan_berat' => $request->luas_perkerasan_berat[$i] ?? null,
                        'luas_perkerasan_dg_tutup' => $request->luas_perkerasan_dg_tutup[$i] ?? null,
                        'lap_tenis_lampu_beton' => $request->lap_tenis_lampu_beton[$i] ?? null,
                        'lap_tenis_lampu_aspal' => $request->lap_tenis_lampu_aspal[$i] ?? null,
                        'lap_tenis_lampu_rumput' => $request->lap_tenis_lampu_rumput[$i] ?? null,
                        'lap_tenis_beton' => $request->lap_tenis_beton[$i] ?? null,
                        'lap_tenis_aspal' => $request->lap_tenis_aspal[$i] ?? null,
                        'lap_tenis_rumput' => $request->lap_tenis_rumput[$i] ?? null,
                        'lift_penumpang' => $request->lift_penumpang[$i] ?? null,
                        'lift_kapsul' => $request->lift_kapsul[$i] ?? null,
                        'lift_barang' => $request->lift_barang[$i] ?? null,
                        'tgg_berjalan_a' => $request->tgg_berjalan_a[$i] ?? null,
                        'tgg_berjalan_b' => $request->tgg_berjalan_b[$i] ?? null,
                        'pjg_pagar' => $request->pjg_pagar[$i] ?? null,
                        'bhn_pagar' => $request->bhn_pagar[$i] ?? null,
                        'hydrant' => $request->hydrant_[$i] ?? null,
                        'sprinkler' => $request->sprinkler_[$i] ?? null,
                        'fire_alarm' => $request->fire_alarm_[$i] ?? null,
                        'jml_pabx' => $request->jml_pabx[$i] ?? null,
                        'sumur_artesis' => $request->sumur_artesis[$i] ?? null,
                        'nilai_individu' => $nilai_individu,
                        'jpb3_8_tinggi_kolom' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_tinggi_kolom[$i] : null,
                        'jpb3_8_lebar_bentang' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_lebar_bentang[$i] : null,
                        'jpb3_8_dd_lantai' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_dd_lantai[$i] : null,
                        'jpb3_8_kel_dinding' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_kel_dinding[$i] : null,
                        'jpb3_8_mezzanine' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_mezzanine[$i] : null,
                        'jpb5_kls_bng' => $kd_jpb == '05' ? $request->jpb5_kls_bng[$i] : null,
                        'jpb5_luas_kamar' => $kd_jpb == '05' ? $request->jpb5_luas_kamar[$i] : null,
                        'jpb5_luas_rng_lain' => $kd_jpb == '05' ? $request->jpb5_luas_rng_lain[$i] : null,
                        'jpb7_jns_hotel' => $kd_jpb == '07' ? $request->jpb7_jns_hotel[$i] : null,
                        'jpb7_bintang' => $kd_jpb == '07' ? $request->jpb7_bintang[$i] : null,
                        'jpb7_jml_kamar' => $kd_jpb == '07' ? $request->jpb7_jml_kamar[$i] : null,
                        'jpb7_luas_kamar' => $kd_jpb == '07' ? $request->jpb7_luas_kamar[$i] : null,
                        'jpb7_luas_rng_lain' => $kd_jpb == '07' ? $request->jpb7_luas_rng_lain[$i] : null,
                        'jpb13_kls_bng' => $kd_jpb == '13' ? $request->jpb13_kls_bng[$i] : null,
                        'jpb13_jml' => $kd_jpb == '13' ? $request->jpb13_jml[$i] : null,
                        'jpb13_luas_kamar' => $kd_jpb == '13' ? $request->jpb13_luas_kamar[$i] : null,
                        'jpb13_luas_rng_lain' => $kd_jpb == '13' ? $request->jpb13_luas_rng_lain[$i] : null,
                        'jpb15_letak_tangki' => $kd_jpb == '15' ? $request->jpb15_letak_tangki[$i] : null,
                        'jpb15_kapasitas_tangki' => $kd_jpb == '15' ? $request->jpb15_kapasitas_tangki[$i] : null,
                        'jpb_lain_kls_bng' => in_array($kd_jpb, ['02', '04', '06', '09', '12', '16']) ? $request->jpb_lain_kls_bng[$i] : null,
                        'created_at' => $now,
                        'created_by' => $userid,
                        'updated_at' => $now,
                        'updated_by' => $userid
                    ];
                    PendataanObjekBng::create($bng);
                }
            }

            db::commit();
            $flash = [
                'success' => 'Data pendataan berhasil di tambah'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
            Log::error($th);
        }
        return   redirect(url('pendataan'))->with($flash);
    }

    public function update($id, Request $request)
    {

        db::beginTransaction();
        try {
            $userid = user()->id;
            $now = Carbon::now();
            $jns_pendataan = $request->jns_pendataan;

            $res = [];

            /*   if ($jns_pendataan == '3' || $jns_pendataan == '1') {
                $angkaa = onlyNumber($request->nop_asal);
                $kd_kecamatan = substr($angkaa, 4, 3);
                $kd_kelurahan = substr($angkaa, 7, 3);
                $kd_blok =  substr($angkaa, 10, 3);
                $no_urut = substr($angkaa, 13, 4);
                $kd_jns_op = substr($angkaa, 17, 1);
            } else {
                $kd_kecamatan = $request->kd_kecamatan;
                $kd_kelurahan = $request->kd_kelurahan;
                $kd_blok = padding($request->kd_blok, 0, 3);
                $no_urut = null;
                $kd_jns_op = null;
            } */

            if (in_array($jns_pendataan, [1, 3, 5, 6])) {
                $angkaa = onlyNumber($request->nop_asal);
                $kd_kecamatan = substr($angkaa, 4, 3);
                $kd_kelurahan = substr($angkaa, 7, 3);
                $kd_blok =  substr($angkaa, 10, 3);
                $no_urut = substr($angkaa, 13, 4);
                $kd_jns_op = substr($angkaa, 17, 1);
            } else {
                $kd_kecamatan = $request->kd_kecamatan;
                $kd_kelurahan = $request->kd_kelurahan;
                $kd_blok = padding($request->kd_blok, 0, 3);
                $no_urut = null;
                $kd_jns_op = null;
            }



            $dat_obj = [
                'kd_propinsi' => '35',
                'kd_dati2' => '07',
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' =>  padding($kd_blok, 0, 3),
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'nop_asal' => onlyNumber($request->nop_asal),
                'subjek_pajak_id' => $request->subjek_pajak_id,
                'nm_wp' => $request->nm_wp,
                'jalan_wp' => $request->jalan_wp,
                'blok_kav_no_wp' => $request->blok_kav_no_wp,
                'rw_wp' => padding($request->rw_wp, 0, 2),
                'rt_wp' => padding($request->rt_wp, 0, 3),
                'kelurahan_wp' => $request->kelurahan_wp,
                'kecamatan_wp' => $request->kecamatan_wp,
                'propinsi_wp' => $request->propinsi_wp,
                'kota_wp' => $request->kota_wp,
                'kd_pos_wp' => $request->kd_pos_wp,
                'telp_wp' => $request->telp_wp,
                'npwp' => $request->npwp,
                'status_pekerjaan_wp' => $request->status_pekerjaan_wp,
                'no_persil' => $request->no_persil,
                'jalan_op' => $request->jalan_op,
                'blok_kav_no_op' => $request->blok_kav_no_op,
                'rw_op' => padding($request->rw_op, 0, 2),
                'rt_op' => padding($request->rt_op, 0, 3),
                'kd_status_cabang' => $request->kd_status_cabang ?? null,
                'kd_status_wp' => $request->kd_status_wp,
                'kd_znt' => $request->kd_znt,
                'luas_bumi' => $request->luas_bumi,
                'jns_bumi' => $request->jns_bumi,
                'created_at' => $now,
                'created_by' => $userid,
                'updated_at' => $now,
                'updated_by' => $userid,
                'count_edit' => db::raw("count_edit+1")
            ];
            // return $dat_obj;
            $objek = ModelsPendataanObjek::where('id', $id)->update($dat_obj);

            DB::table('pendataan_objek_penetapan')->where('pendataan_objek_id', $id)->delete();

            if (!empty($request->mulai_tahun)) {
                $penetapan = [];
                foreach ($request->mulai_tahun as $ai => $mulai_tahun) {
                    $penetapan[] = [
                        'pendataan_objek_id' => $id,
                        'mulai_tahun' => $mulai_tahun,
                        'tgl_jatuh_tempo' => new carbon($request->tgl_jatuh_tempo[$ai]),
                        'is_nota' => $request->is_nota
                    ];
                }

                if (count($penetapan) > 0) {
                    DB::table('pendataan_objek_penetapan')->insert($penetapan);
                }
            }

            PendataanObjekBng::where('pendataan_objek_id', $id)->delete();
            if (!empty($request->jns_transaksi_lspop)) {
                foreach ($request->jns_transaksi_lspop as $i => $lspop) {

                    $nilai_individu = 0;
                    if (isset($request->nilai_individu[$i])) {
                        if (preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]) > 0) {
                            // $nilai_individu = (preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]) * $request->luas_bng[$i]) / 1000;
                            $nilai_individu = preg_replace('/[^0-9]/', '', $request->nilai_individu[$i]);
                        }
                    }
                    $kd_jpb = padding($request->kd_jpb[$i], 0, 2) ?? '01';
                    $bng = [
                        'pendataan_objek_id' => $id,
                        'no_bng' => $i + 1,
                        'kd_jpb' => $kd_jpb,
                        'thn_dibangun_bng' => $request->thn_dibangun_bng[$i] ?? date('Y'),
                        'thn_renovasi_bng' => $request->thn_renovasi_bng[$i] ?? null,
                        'luas_bng' => $request->luas_bng[$i] ?? 0,
                        'jml_lantai_bng' => $request->jml_lantai_bng[$i] ?? '1',
                        'kondisi_bng' => $request->kondisi_bng[$i] ?? '2',
                        'jns_konstruksi_bng' => $request->jns_konstruksi_bng[$i] ?? '3',
                        'jns_atap_bng' => $request->jns_atap_bng[$i] ?? '3',
                        'kd_dinding' => $request->kd_dinding[$i] ?? '3',
                        'kd_lantai' => $request->kd_lantai[$i] ?? '2',
                        'kd_langit_langit' => $request->kd_langit_langit[$i] ?? '3',
                        'daya_listrik' => $request->daya_listrik[$i] ?? null,
                        'acsplit' => $request->acsplit[$i] ?? null,
                        'acwindow' => $request->acwindow[$i] ?? null,
                        'acsentral' => $request->acsentral[$i] ?? null,
                        'luas_kolam' => $request->luas_kolam[$i] ?? null,
                        'finishing_kolam' => $request->finishing_kolam[$i] ?? null,
                        'luas_perkerasan_ringan' => $request->luas_perkerasan_ringan[$i] ?? null,
                        'luas_perkerasan_sedang' => $request->luas_perkerasan_sedang[$i] ?? null,
                        'luas_perkerasan_berat' => $request->luas_perkerasan_berat[$i] ?? null,
                        'luas_perkerasan_dg_tutup' => $request->luas_perkerasan_dg_tutup[$i] ?? null,
                        'lap_tenis_lampu_beton' => $request->lap_tenis_lampu_beton[$i] ?? null,
                        'lap_tenis_lampu_aspal' => $request->lap_tenis_lampu_aspal[$i] ?? null,
                        'lap_tenis_lampu_rumput' => $request->lap_tenis_lampu_rumput[$i] ?? null,
                        'lap_tenis_beton' => $request->lap_tenis_beton[$i] ?? null,
                        'lap_tenis_aspal' => $request->lap_tenis_aspal[$i] ?? null,
                        'lap_tenis_rumput' => $request->lap_tenis_rumput[$i] ?? null,
                        'lift_penumpang' => $request->lift_penumpang[$i] ?? null,
                        'lift_kapsul' => $request->lift_kapsul[$i] ?? null,
                        'lift_barang' => $request->lift_barang[$i] ?? null,
                        'tgg_berjalan_a' => $request->tgg_berjalan_a[$i] ?? null,
                        'tgg_berjalan_b' => $request->tgg_berjalan_b[$i] ?? null,
                        'pjg_pagar' => $request->pjg_pagar[$i] ?? null,
                        'bhn_pagar' => $request->bhn_pagar[$i] ?? null,
                        'hydrant' => $request->hydrant_[$i] ?? null,
                        'sprinkler' => $request->sprinkler_[$i] ?? null,
                        'fire_alarm' => $request->fire_alarm_[$i] ?? null,
                        'jml_pabx' => $request->jml_pabx[$i] ?? null,
                        'sumur_artesis' => $request->sumur_artesis[$i] ?? null,
                        'nilai_individu' => $nilai_individu,
                        'jpb3_8_tinggi_kolom' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_tinggi_kolom[$i] : null,
                        'jpb3_8_lebar_bentang' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_lebar_bentang[$i] : null,
                        'jpb3_8_dd_lantai' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_dd_lantai[$i] : null,
                        'jpb3_8_kel_dinding' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_kel_dinding[$i] : null,
                        'jpb3_8_mezzanine' => ($kd_jpb == '03' || $kd_jpb == '08') ? $request->jpb3_8_mezzanine[$i] : null,
                        'jpb5_kls_bng' => $kd_jpb == '05' ? $request->jpb5_kls_bng[$i] : null,
                        'jpb5_luas_kamar' => $kd_jpb == '05' ? $request->jpb5_luas_kamar[$i] : null,
                        'jpb5_luas_rng_lain' => $kd_jpb == '05' ? $request->jpb5_luas_rng_lain[$i] : null,
                        'jpb7_jns_hotel' => $kd_jpb == '07' ? $request->jpb7_jns_hotel[$i] : null,
                        'jpb7_bintang' => $kd_jpb == '07' ? $request->jpb7_bintang[$i] : null,
                        'jpb7_jml_kamar' => $kd_jpb == '07' ? $request->jpb7_jml_kamar[$i] : null,
                        'jpb7_luas_kamar' => $kd_jpb == '07' ? $request->jpb7_luas_kamar[$i] : null,
                        'jpb7_luas_rng_lain' => $kd_jpb == '07' ? $request->jpb7_luas_rng_lain[$i] : null,
                        'jpb13_kls_bng' => $kd_jpb == '13' ? $request->jpb13_kls_bng[$i] : null,
                        'jpb13_jml' => $kd_jpb == '13' ? $request->jpb13_jml[$i] : null,
                        'jpb13_luas_kamar' => $kd_jpb == '13' ? $request->jpb13_luas_kamar[$i] : null,
                        'jpb13_luas_rng_lain' => $kd_jpb == '13' ? $request->jpb13_luas_rng_lain[$i] : null,
                        'jpb15_letak_tangki' => $kd_jpb == '15' ? $request->jpb15_letak_tangki[$i] : null,
                        'jpb15_kapasitas_tangki' => $kd_jpb == '15' ? $request->jpb15_kapasitas_tangki[$i] : null,
                        'jpb_lain_kls_bng' => in_array($kd_jpb, ['02', '04', '06', '09', '12', '16']) ? $request->jpb_lain_kls_bng[$i] : null,
                        'created_at' => $now,
                        'created_by' => $userid,
                        'updated_at' => $now,
                        'updated_by' => $userid
                    ];
                    PendataanObjekBng::create($bng);
                }
            }
            // }

            // dd($res);

            db::commit();
            $flash = [
                'success' => 'Data pendataan berhasil di edit'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
            Log::error($th);
        }
        $batch = ModelsPendataanObjek::select('pendataan_batch_id')->where('id', $id)->first();
        return   redirect(url('pendataan/show', $batch->pendataan_batch_id))->with($flash);
    }


    public function formImport()
    {
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $jenis = JenisPendataan::whereraw("id in (1,2,6)")->get();
        return view('pendataan/form-import', compact('kecamatan', 'jenis'));
    }

    public function previewImport(Request $request)
    {
        $default = response()->json(['status' => false, 'msg' => "Format tidak sesuai"]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $ext = $file->extension();

            // dd($request->all());

            // return $ext;
            if (!in_array($ext, ["xlsx", "csv", "xls"])) {
                return $default;
            }
            $array = Excel::toArray(new PendataanImport, $file);
            $data = [];
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $prefix = '3507' . $request->kd_kecamatan . $request->kd_kelurahan;

            if ($array[0]) {
                // $data = $array[0];
                $ct = "";
                $urut = "";
                $znt = "";

                $kec = "";
                $kel = "";
                $blok = "";


                $statusdata = '0';
                $ketznt = "";

                // Log::info($array);

                foreach ($array[0] as $i => $row) {
                    $st = $row['status_tanah'];
                    // if ($row['kd_blok'] != '' && $row['znt'] != ''  && $row['subjek_pajak_id'] != '' && $row['status_tanah'] != '' && $row['status_kepemilikan'] != '') {
                    if ((in_array($st, [4, 5]))  || (in_array($st, [1, 2, 3]) &&  $row['subjek_pajak_id'] != '' && $row['kd_blok'] != '')) {
                        $keterangan = "";
                        $ind = onlyNumber(trim($row['induk']));
                        $na = onlyNumber(trim($row['nop_asal']));
                        $nop_jadi = "";
                        if ($request->jenis_pendataan_id == '1') {
                            $nop_jadi = '3507' . $request->kd_kecamatan . $request->kd_kelurahan . padding(onlyNumber(trim($row['kd_blok'])), 0, 3) . padding(onlyNumber(trim($row['no'])), 0, 4) . '0';
                            $ind = $nop_jadi;
                            $na = $nop_jadi;
                        }

                        $luas_bumi = $st == 5 ? 0 : trim($row['luas_bumi']);
                        $pemilik = $row['status_kepemilikan'] == '' ? 1 : $row['status_kepemilikan'];
                        $result = [
                            'induk' => trim($ind <> '' ? 1 : null),
                            'nop_asal' => trim($na),
                            'kd_blok' => trim($ind <> '' ? substr($na, 10, 3)  : padding(onlyNumber(trim($row['kd_blok'])), 0, 3)),
                            'jalan_op' => trim(strtoupper(trim($row['jalan_op']))),
                            'blok_kav_no_op' => trim(strtoupper(trim($row['blok_kav_no_op']))),
                            'no_persil' => trim(trim($row['no_persil'])),
                            'rt_op' => trim(padding(onlyNumber(trim($row['rt_op'])), 0, 3)),
                            'rw_op' => trim(padding(onlyNumber(trim($row['rw_op'])), 0, 2)),
                            'luas_bumi' => trim($luas_bumi),
                            'znt' => trim(trim($row['znt'])),
                            'status_tanah' => trim(onlyNumber(trim($row['status_tanah']))),
                            'status_kepemilikan' => trim(onlyNumber(trim($pemilik))),
                            'subjek_pajak_id' => trim(onlyNumber(trim($row['subjek_pajak_id']))),
                            'nm_wp' => trim(strtoupper(trim($row['nm_wp']))),
                            'jalan_wp' => trim(strtoupper(trim($row['jalan_wp']))),
                            'blok_kav_no_wp' => trim(trim($row['blok_kav_no_wp'])),
                            'rt_wp' => trim(padding(onlyNumber(trim($row['rt_wp'])), 0, 3)),
                            'rw_wp' => trim(padding(onlyNumber(trim($row['rw_wp'])), 0, 2)),
                            'pekerjaan' => trim(onlyNumber(trim($row['pekerjaan']))),
                            'kelurahan_wp' => trim(strtoupper(trim($row['kelurahan_wp']))),
                            'kecamatan_wp' => trim(strtoupper(trim($row['kecamatan_wp']))),
                            'kota_wp' => trim(strtoupper(trim($row['kota_wp']))),
                            'propinsi_wp' => trim(strtoupper(trim($row['propinsi_wp']))),
                            'kd_pos_wp' => trim(onlyNumber(trim($row['kd_pos_wp']))),
                            'telp_wp' => trim(onlyNumber(trim($row['telp_wp']))),
                            'npwp' => trim(onlyNumber(trim($row['npwp']))),
                            'kd_jpb' => trim(padding(onlyNumber(trim($row['kd_jpb'])), 0, 2)),
                            'luas_bng' => trim(trim($row['luas_bangunan'])),
                            'thn_dibangun_bng' => trim(onlyNumber(trim($row['thn_dibangun']))),
                            'thn_renovasi_bng' => trim(onlyNumber(trim($row['thn_direnovasi']))),
                            'jml_lantai_bng' => trim(onlyNumber(trim($row['jumlah_lantai']))),
                            'daya_listrik' => trim(onlyNumber(trim($row['daya_listrik']))),
                            'kondisi_bng' => trim(onlyNumber(trim($row['kondisi']))),
                            'jns_konstruksi_bng' => trim(onlyNumber(trim($row['konstruksi']))),
                            'jns_atap_bng' => trim(onlyNumber(trim($row['atap']))),
                            'kd_dinding' => trim(onlyNumber(trim($row['dinding']))),
                            'kd_lantai' => trim(onlyNumber(trim($row['lantai']))),
                            'kd_langit_langit' => trim(onlyNumber(trim($row['langit2']))),
                        ];

                        // if (in_array($st, [1, 2, 3])) {
                        // if (($result['znt'] <> $znt || $result['kd_blok'] <> $blok || $znt == '')) {
                        $cj = DB::connection('oracle_satutujuh')->table(DB::raw('DAT_NIR a'))
                            ->join(db::raw('dat_peta_znt b'), function ($join) {
                                $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')->on('a.kd_dati2', '=', 'b.kd_dati2')->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')->on('a.kd_znt', '=', 'b.kd_znt');
                            })
                            ->select('a.kd_znt')->whereraw("thn_nir_znt='" . date('Y') . "' and a.kd_kecamatan='$kd_kecamatan' and a.kd_kelurahan='$kd_kelurahan' and b.kd_blok='" . $result['kd_blok'] . "' and a.kd_ZNT='" . $result['znt'] . "'")->first();
                        if ($cj) {
                            $statusdata = 1;
                            $ketznt_tmp = "";
                        } else {
                            $statusdata = 0;
                            $ketznt_tmp = "ZNT " . $result['znt'] . " tidak terdaftar pada kode blok " . $result['kd_blok'] . "<br>";
                        }
                        // }else{
                        //     Log::info('masalah disini');
                        // }

                        $ketznt = $ketznt_tmp;

                        $keterangan .= $ketznt;


                        //nop_asal
                        if ($result['nop_asal'] <> '') {
                            if (strlen($result['nop_asal']) <> 18) {
                                $statusdata = 0;
                                $keterangan .= "NOP asal harus 18 digit;<br>";
                            }
                        }

                        if (strlen($result['jalan_op']) > 30) {
                            $statusdata = 0;
                            $keterangan .= "Jalan Objek melebihi batas karakter yang di tentukan sebanyak 30 karakter;<br>";
                        }
                        if (strlen($result['blok_kav_no_op']) > 15) {
                            $statusdata = 0;
                            $keterangan .= "BLOK/KAV/NO Objek melebihi batas karakter yang di tentukan sebanyak 15 karakter;<br>";
                        }
                        if (strlen($result['no_persil']) > 5) {
                            $statusdata = 0;
                            $keterangan .= "No persil melebihi batas karakter yang di tentukan sebanyak 5 karakter;<br>";
                        }
                        if (strlen($result['rt_op']) > 3) {
                            $statusdata = 0;
                            $keterangan .= "RT Objek melebihi batas karakter yang di tentukan sebanyak 3 karakter;<br>";
                        }
                        if (strlen($result['rw_op']) > 2) {
                            $statusdata = 0;
                            $keterangan .= "RW Objek melebihi batas karakter yang di tentukan sebanyak 2 karakter;<br>";
                        }
                        if (strlen($result['luas_bumi']) > 12) {
                            $statusdata = 0;
                            $keterangan .= "Luas Bumi melebihi batas karakter yang di tentukan sebanyak 12 karakter;<br>";
                        }
                        if (strlen($result['znt']) > 2) {
                            $statusdata = 0;
                            $keterangan .= "ZNT melebihi batas karakter yang di tentukan sebanyak 2 karakter;<br>";
                        }
                        if (strlen($result['status_tanah']) > 1) {
                            $statusdata = 0;
                            $keterangan .= "Status Tanah melebihi batas karakter yang di tentukan sebanyak 1 karakter;<br>";
                        }
                        if (strlen($result['status_kepemilikan']) > 1) {
                            $statusdata = 0;
                            $keterangan .= "Kepemilikan melebihi batas karakter yang di tentukan sebanyak 1 karakter;<br>";
                        }
                        if (strlen($result['subjek_pajak_id']) > 16) {
                            $statusdata = 0;
                            $keterangan .= "NIK melebihi batas karakter yang di tentukan sebanyak 16 karakter;<br>";
                        }
                        if (strlen($result['nm_wp']) > 30) {
                            $statusdata = 0;
                            $keterangan .= "Nama WP melebihi batas karakter yang di tentukan sebanyak 30 karakter;<br>";
                        }
                        if (strlen($result['jalan_wp']) > 30) {
                            $statusdata = 0;
                            $keterangan .= "Jalan WP melebihi batas karakter yang di tentukan sebanyak 30 karakter;<br>";
                        }
                        if (strlen($result['blok_kav_no_wp']) > 15) {
                            $statusdata = 0;
                            $keterangan .= "BLOK/KAV/NO WP melebihi batas karakter yang di tentukan sebanyak 15 karakter;<br>";
                        }
                        if (strlen($result['rt_wp']) > 3) {
                            $statusdata = 0;
                            $keterangan .= "RT WP melebihi batas karakter yang di tentukan sebanyak 3 karakter;<br>";
                        }
                        if (strlen($result['rw_wp']) > 2) {
                            $statusdata = 0;
                            $keterangan .= "RW WP melebihi batas karakter yang di tentukan sebanyak 2 karakter;<br>";
                        }
                        if (strlen($result['pekerjaan']) > 1) {
                            $statusdata = 0;
                            $keterangan .= "Pekerjaan melebihi batas karakter yang di tentukan sebanyak 1 karakter;<br>";
                        }
                        if (strlen($result['kelurahan_wp']) > 100) {
                            $statusdata = 0;
                            $keterangan .= "Kelurahan WP' melebihi batas karakter yang di tentukan sebanyak 100 karakter;<br>";
                        }
                        if (strlen($result['kota_wp']) > 100) {
                            $statusdata = 0;
                            $keterangan .= "Kota WP' melebihi batas karakter yang di tentukan sebanyak 100 karakter;<br>";
                        }
                        if (strlen($result['propinsi_wp']) > 100) {
                            $statusdata = 0;
                            $keterangan .= "Propinsi' melebihi batas karakter yang di tentukan sebanyak 100 karakter;<br>";
                        }
                        if (strlen($result['kd_pos_wp']) > 5) {
                            $statusdata = 0;
                            $keterangan .= "Kode Pos melebihi batas karakter yang di tentukan sebanyak 5 karakter;<br>";
                        }
                        if (strlen($result['telp_wp']) > 20) {
                            $statusdata = 0;
                            $keterangan .= "Telepon melebihi batas karakter yang di tentukan sebanyak 20 karakter;<br>";
                        }
                        if (strlen($result['npwp']) > 15) {
                            $statusdata = 0;
                            $keterangan .= "NPWP melebihi batas karakter yang di tentukan sebanyak 15 karakter;<br>";
                        }


                        if ($request->jenis_pendataan_id == 1) {
                            $sn = splitnop($result['nop_asal']);
                            $co = DAT_OBJEK_PAJAK::where($sn)->first();
                            if ($co) {
                                $statusdata = 0;
                                $keterangan .= "NOP sudah terpakai di objek lain;<br>";
                            }
                        }

                        if ($result['induk'] == '1') {
                            $result['nop_proses'] = $result['nop_asal'];
                            $result['jns_transaksi'] = '2';
                        } else {
                            $result['nop_proses'] = $prefix . $result['kd_blok'];
                            $result['jns_transaksi'] = '1';
                        }


                        $result['status'] = $statusdata; //== 1 ? '1' : '0';
                        $result['keterangan'] = $keterangan;
                        $data[] = $result;

                        /*                         if ($statusdata == 0 && $keterangan == '') {
                            Log::info($statusdata);
                            Log::info($data);
                            dd($data);
                        }
 */


                        $znt = $result['znt'];
                        $blok = $result['kd_blok'];
                    }
                }
            }


            return view('pendataan/preview', compact('data', 'prefix', 'request'));
        }
        return response()->json($default);
    }


    public function storeImport(Request $request)
    {
        // dd($request->all());

        db::beginTransaction();
        try {
            $userid = user()->id;
            $now = Carbon::now();
            $batchData = [
                'tipe' => 'PD',
                'jenis_pendataan_id' => $request->jenis_pendataan_id,
            ];
            $batch = PendataanBatch::create($batchData);
            // objek
            $res = [];
            foreach ($request->nop_proses as $i => $row) {
                // $kd_blok = substr($request->nop_proses[$i], 10, 3);
                $angkaa = $request->nop_proses[$i];
                $dat_obj = [
                    'pendataan_batch_id' => $batch->id ?? null,
                    'kd_propinsi' => substr($angkaa,  0, 2),
                    'kd_dati2' => substr($angkaa, 2, 2),
                    'kd_kecamatan' => substr($angkaa, 4, 3),
                    'kd_kelurahan' => substr($angkaa, 7, 3),
                    'kd_blok' =>  substr($angkaa, 10, 3),
                    'no_urut' => substr($angkaa, 13, 4) ?? null,
                    'kd_jns_op' => substr($angkaa, 17, 1) ?? null,
                    'nop_asal' => $request->nop_asal[$i],
                    'subjek_pajak_id' => $request->subjek_pajak_id[$i],
                    'nm_wp' => $request->nm_wp[$i],
                    'jalan_wp' => $request->jalan_wp[$i],
                    'blok_kav_no_wp' => $request->blok_kav_no_wp[$i],
                    'rw_wp' => $request->rw_wp[$i],
                    'rt_wp' => $request->rt_wp[$i],
                    'kelurahan_wp' => $request->kelurahan_wp[$i],
                    'kecamatan_wp' => $request->kecamatan_wp[$i],
                    'propinsi_wp' => $request->propinsi_wp[$i],
                    'kota_wp' => $request->kota_wp[$i],
                    'kd_pos_wp' => $request->kd_pos_wp[$i],
                    'telp_wp' => $request->telp_wp[$i],
                    'npwp' => $request->npwp[$i],
                    'status_pekerjaan_wp' => $request->status_pekerjaan_wp[$i],
                    'no_persil' => $request->no_persil[$i],
                    'jalan_op' => $request->jalan_op[$i],
                    'blok_kav_no_op' => $request->blok_kav_no_op[$i],
                    'rw_op' => $request->rw_op[$i],
                    'rt_op' => $request->rt_op[$i],
                    'kd_status_cabang' => $request->kd_status_cabang[$i] ?? null,
                    'kd_status_wp' => $request->kd_status_wp[$i],
                    'kd_znt' => $request->kd_znt[$i],
                    'luas_bumi' => $request->luas_bumi[$i],
                    'jns_bumi' => $request->jns_bumi[$i],
                    'created_at' => $now,
                    'created_by' => $userid,
                    'updated_at' => $now,
                    'updated_by' => $userid
                ];
                $objek = ModelsPendataanObjek::create($dat_obj);

                if (!empty($request->kd_jpb[$i]) &&  $request->jns_bumi[$i] == '1') {
                    $bng = [
                        'pendataan_objek_id' => $objek->id,
                        'no_bng' => 1,
                        'kd_jpb' => $request->kd_jpb[$i],
                        'thn_dibangun_bng' => $request->thn_dibangun_bng[$i],
                        'thn_renovasi_bng' => $request->thn_renovasi_bng[$i] > 0 ? $request->thn_renovasi_bng[$i] : null,
                        'luas_bng' => ($request->luas_bng[$i] <> '' or $request->luas_bng[$i] > 0) ? $request->luas_bng[$i] : '100',
                        'jml_lantai_bng' => ($request->jml_lantai_bng[$i] <> '' or $request->jml_lantai_bng[$i] > 0) ? $request->jml_lantai_bng[$i] : '1',
                        'kondisi_bng' => $request->kondisi_bng[$i],
                        'jns_konstruksi_bng' => $request->jns_konstruksi_bng[$i],
                        'jns_atap_bng' => $request->jns_atap_bng[$i],
                        'kd_dinding' => $request->kd_dinding[$i],
                        'kd_lantai' => $request->kd_lantai[$i],
                        'kd_langit_langit' => $request->kd_langit_langit[$i],
                        'daya_listrik' => $request->daya_listrik[$i],
                        'created_at' => $now,
                        'created_by' => $userid,
                        'updated_at' => $now,
                        'updated_by' => $userid
                    ];
                    PendataanObjekBng::create($bng);
                }
            }

            // dd($res);

            db::commit();
            $flash = [
                'success' => 'Data pendataan berhasil di upload'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
            Log::error($th);
        }

        return   redirect(url('pendataan'))->with($flash);
    }

    public function reportObjek(Request $request)
    {
        if ($request->ajax()) {
            $data = Pendataan::ReportObjek()->orderby('dat_objek_pajak.tgl_perekaman_op', 'desc');
            return  DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->has('kd_kecamatan')) {
                        $kec = $request->get('kd_kecamatan');
                        if (!empty($kec)) {
                            $query->whereraw("dat_objek_pajak.kd_kecamatan='$kec'");
                        }
                    }

                    if ($request->has('kd_kelurahan')) {
                        $kel = $request->get('kd_kelurahan');
                        if (!empty($kel)) {
                            $query->whereraw("dat_objek_pajak.kd_kelurahan='$kel'");
                        }
                    }
                    if ($request->has('created_by')) {
                        $created_by = $request->get('created_by');
                        if (!empty($created_by)) {
                            $query->whereraw("created_by='$created_by'");
                        }
                    }
                    if ($request->has('tanggal')) {
                        $tanggal = $request->get('tanggal');
                        if (!empty($tanggal)) {
                            $query->whereraw("trunc(created_at)= trunc(to_date('$tanggal','dd-mm-yyyy')) ");
                        }
                    }
                })
                ->addColumn('pilih', function ($row) {
                    return  "<a  role='button' data-toggle='tooltip' data-placement='top' title='Detail Objek'  onclick='nopDetail(\"" . $row->nop_proses . "\")' class='nop-detail' data-nop='" . $row->nop_proses . "'><i class='fas text-success fa-binoculars'></i></a>";
                })

                ->addColumn('noformulir', function ($row) {
                    return formatNomor($row->no_formulir);
                })
                ->addColumn('nop', function ($row) {
                    return formatnop($row->nop_proses);
                })
                ->rawColumns(['pilih', 'noformulir', 'nop'])->make(true);
        }
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $creators = Pendataan::Creators()->get();
        return view('pendataan/report-objek', compact('kecamatan', 'creators'));
    }

    public function reportObjekExcel(Request $request)
    {
        $param = $request->only('kd_kecamatan', 'kd_kelurahan', 'created_by', 'tanggal');
        $nm_kec = DB::connection('oracle_dua')->table('ref_kecamatan')->where('kd_kecamatan', $param['kd_kecamatan'])->select('nm_kecamatan')->first()->nm_kecamatan ?? null;
        $nm_kel = DB::connection('oracle_dua')->table('ref_kelurahan')->where('kd_kecamatan', $param['kd_kecamatan'])->where('kd_kelurahan', $param['kd_kelurahan'])->first()->nm_kelurahan ?? null;

        $nm_pendata = User::where('id', $param['created_by'])->first()->nama ?? null;
        $param['nm_kec'] = $nm_kec;
        $param['nm_kel'] = $nm_kel;
        $param['nm_pendata'] = $nm_pendata;
        $tanggal = $param['tanggal'] <> '' ? tglindo($param['tanggal']) : '';

        return Excel::download(new PendataanObjekExcel($param), 'Pendataan Objek ' . $nm_kel . ' ' . $nm_kec . ' ' . $nm_pendata . ' ' . $tanggal . '.xlsx');
    }


    public function reportSubjek(Request $request)
    {
        if ($request->ajax()) {
            $data = Pendataan::ReportSubjek()->orderby('dat_objek_pajak.tgl_perekaman_op', 'desc');
            return  DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->has('kd_kecamatan')) {
                        $kec = $request->get('kd_kecamatan');
                        if (!empty($kec)) {
                            $query->whereraw("dat_objek_pajak.kd_kecamatan='$kec'");
                        }
                    }

                    if ($request->has('kd_kelurahan')) {
                        $kel = $request->get('kd_kelurahan');
                        if (!empty($kel)) {
                            $query->whereraw("dat_objek_pajak.kd_kelurahan='$kel'");
                        }
                    }
                    if ($request->has('created_by')) {
                        $created_by = $request->get('created_by');
                        if (!empty($created_by)) {
                            $query->whereraw("created_by='$created_by'");
                        }
                    }
                    if ($request->has('tanggal')) {
                        $tanggal = $request->get('tanggal');
                        if (!empty($tanggal)) {
                            $query->whereraw("trunc(created_at)= trunc(to_date('$tanggal','dd-mm-yyyy')) ");
                        }
                    }
                })
                ->addColumn('pilih', function ($row) {
                    return  "<a  role='button' data-toggle='tooltip' data-placement='top' title='Detail Objek'  onclick='nopDetail(\"" . $row->nop_proses . "\")' class='nop-detail' data-nop='" . $row->nop_proses . "'><i class='fas text-success fa-binoculars'></i></a>";
                })
                ->addColumn('noformulir', function ($row) {
                    return formatNomor($row->no_formulir);
                })
                ->addColumn('nop', function ($row) {
                    return formatnop($row->nop_proses);
                })
                ->addColumn('kelurahan', function ($row) {
                    $res = explode('-', $row->kelurahan_wp);
                    return $res[0];
                })
                ->addColumn('kecamatan', function ($row) {
                    $res = explode('-', $row->kelurahan_wp);
                    return $res[1] ?? null;
                })
                ->addColumn('kota', function ($row) {
                    $res = explode('-', $row->kota_wp);
                    return $res[0];
                })
                ->addColumn('propinsi', function ($row) {
                    $res = explode('-', $row->kota_wp);
                    return $res[1] ?? null;
                })
                ->rawColumns(['pilih', 'noformulir', 'kelruahan', 'kecamatan', 'nop'])->make(true);
        }
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();
        $creators = Pendataan::Creators()->get();
        // dd($creators);
        return view('pendataan/report-subjek', compact('kecamatan', 'creators'));
    }

    public function reportSubjekExcel(Request $request)
    {
        $param = $request->only('kd_kecamatan', 'kd_kelurahan', 'created_by', 'tanggal');
        $nm_kec = DB::connection('oracle_dua')->table('ref_kecamatan')->where('kd_kecamatan', $param['kd_kecamatan'])->select('nm_kecamatan')->first()->nm_kecamatan ?? null;
        $nm_kel = DB::connection('oracle_dua')->table('ref_kelurahan')->where('kd_kecamatan', $param['kd_kecamatan'])->where('kd_kelurahan', $param['kd_kelurahan'])->first()->nm_kelurahan ?? null;

        $nm_pendata = User::where('id', $param['created_by'])->first()->nama ?? null;
        $param['nm_kec'] = $nm_kec;
        $param['nm_kel'] = $nm_kel;
        $param['nm_pendata'] = $nm_pendata;
        $tanggal = $param['tanggal'] <> '' ? tglindo($param['tanggal']) : '';

        return Excel::download(new PendataanSubjekExcel($param), 'Pendataan Subjek Pajak ' . $nm_kel . ' ' . $nm_kec . ' ' . $nm_pendata . ' ' . $tanggal . '.xlsx');
    }
    // 

    public function queryListObjek($id)
    {
        return  ModelsPendataanObjek::with('spop')->where('pendataan_batch_id', $id)
            ->selectraw(" count_edit,pendataan_objek.id, mark_tolak, nop_asal, pendataan_objek.kd_propinsi, pendataan_objek.kd_dati2, pendataan_objek.kd_kecamatan, pendataan_objek.kd_kelurahan, pendataan_objek.kd_blok, pendataan_objek.no_urut, pendataan_objek.kd_jns_op, nm_kecamatan, nm_kelurahan, nm_wp, jalan_op, blok_kav_no_op, rt_op, rw_op, pendataan_objek.kd_znt, luas_bumi, nir * 1000 nir, jns_bumi, nvl(( select sum(luas_bng) from pendataan_objek_bng
                             where pendataan_objek_id=pendataan_objek.id),0) luas_bng")
            ->join(db::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'pendataan_objek.kd_kecamatan')
            ->join(db::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
                $join->on('ref_kelurahan.kd_kelurahan', '=', 'pendataan_objek.kd_kelurahan')
                    ->on('ref_kelurahan.kd_kecamatan', '=', 'pendataan_objek.kd_kecamatan');
            })
            ->join(db::raw('pbb.dat_nir dat_nir'), function ($join) {
                $join->on('dat_nir.kd_znt', '=', 'pendataan_objek.kd_znt')
                    ->on('pendataan_objek.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                    ->on('pendataan_objek.kd_kelurahan', '=', 'dat_nir.kd_kelurahan')
                    ->on("dat_nir.thn_nir_znt", '=', DB::raw("to_char(pendataan_objek.created_at,'yyyy')"));
            })->orderby('pendataan_objek.id', 'asc')
            ->get();
    }

    public function show($id)
    {
        $batch = PendataanBatch::ListPendataan($id)
            ->first();
        $objek = $this->queryListObjek($id);
        return view('pendataan/show', compact('batch', 'objek'));
    }

    public function indexVerifikasi(Request $request)
    {
        if ($request->ajax()) {
            $data = PendataanBatch::listPendataan()
                ->whereraw("((verifikasi_at is null and jenis_pendataan_id!=1) or (verifikasi_kode=2 and jenis_pendataan_id=1))")
                ->orderbyraw(db::raw("to_char(pendataan_batch.created_at,'yyyy-mm-dd') desc"));

            return  DataTables::of($data)
                ->filterColumn('pencarian_data', function ($query, $string) {
                    $keyword = strtolower($string);
                    $nopproses = onlyNumber($string);
                    $wh = "lower(nama_pendataan) like '%$keyword%' or lower(users.nama) like '%$keyword%'  ";
                    // if ($nopproses != '') {
                    $wh .= " or lower(pendataan_batch.nomor_batch) like '%$keyword%'  ";
                    // }
                    $query->whereRaw(DB::raw($wh));
                })
                ->addColumn('tanggal_indo', function ($row) {
                    return tglIndo($row->tanggal);
                })
                ->addColumn('status', function ($row) {
                    // return tglIndo($row->tanggal);
                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $st = "Disetujui";
                            break;
                        case '2':
                            # code...
                            $st = "Verifikasi PETA";
                            break;
                        case '0':
                            # code...
                            $st = "Tidak Disetujui";
                            break;
                        default:
                            # code...
                            $st = "Belum di verifikasi";
                            break;
                    }
                    return $st;
                })
                ->addColumn('pilih', function ($row) {
                    return "<a href='" . route('pendataan.verifikasi.form', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Verifikasi Pendataan'   class='nop-detail' ><i class='fas text-success fa-binoculars'></i></a>";
                })
                ->rawColumns(['pilih', 'status', 'tanggal_indo'])->make(true);
        }
        return view('pendataan.index-verifikasi');
    }

    public function indexVerifikasiPeta(Request $request)
    {
        if ($request->ajax()) {
            $data = PendataanBatch::listPendataan()
                ->wherenull('verifikasi_at')
                ->where('jenis_pendataan_id', 1)
                ->orderbyraw(db::raw("to_char(pendataan_batch.created_at,'yyyy-mm-dd') desc"));

            return  DataTables::of($data)
                ->filterColumn('pencarian_data', function ($query, $string) {
                    $keyword = strtolower($string);
                    $nopproses = onlyNumber($string);
                    $wh = "lower(nama_pendataan) like '%$keyword%' or lower(users.nama) like '%$keyword%'  ";
                    if ($nopproses != '') {
                        $wh .= " or pendataan_batch.nomor_batch like '%$nopproses%'  ";
                    }
                    $query->whereRaw(DB::raw($wh));
                })
                ->addColumn('tanggal_indo', function ($row) {
                    return tglIndo($row->tanggal);
                })
                ->addColumn('status', function ($row) {
                    // return tglIndo($row->tanggal);
                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $st = "Disetujui";
                            break;
                        case '2':
                            # code...
                            $st = "Verifikasi PETA";
                            break;
                        case '0':
                            # code...
                            $st = "Tidak Disetujui";
                            break;
                        default:
                            # code...
                            $st = "Belum di verifikasi";
                            break;
                    }

                    return $st;
                })
                ->addColumn('pilih', function ($row) {
                    return "<a href='" . route('pendataan.verifikasi.peta.form', $row->id) . "'  data-toggle='tooltip' data-placement='top' title='Verifikasi Pendataan'   class='nop-detail' ><i class='fas text-success fa-binoculars'></i></a>";
                })
                ->rawColumns(['pilih', 'status', 'tanggal_indo'])->make(true);
        }
        return view('pendataan.index-verifikasi-peta');
    }

    public function showVerifikasi($id)
    {
        $batch = PendataanBatch::ListPendataan($id)
            ->first();
        $objek = $this->queryListObjek($id);
        // dd($objek);


        return view('pendataan/form-verifikasi', compact('batch', 'objek'));
    }


    public function showVerifikasiPeta($id)
    {
        $batch = PendataanBatch::ListPendataan($id)->first();
        $objek = $this->queryListObjek($id);
        return view('pendataan/form-verifikasi-peta', compact('batch', 'objek'));
    }

    public function storeVerifikasiPeta(Request $request)
    {
        // return $request->all();
        db::beginTransaction();
        try {
            if ($request->verifikasi_kode == '0') {
                if (!empty($request->mark_tolak)) {
                    ModelsPendataanObjek::wherein('id', $request->mark_tolak)->update(['mark_tolak' => '1']);
                }
            }

            $batch = [
                'verifikasi_deskripsi' => $request->verifikasi_deskripsi,
                'verifikasi_kode' => $request->verifikasi_kode,
                'verifikasi_by' => Auth()->user()->id,
                'verifikasi_at' => db::raw('sysdate')
            ];

            PendataanBatch::where('id', $request->id)->update($batch);
            db::commit();
            // dd($data);
            $flash =
                [
                    'success' => 'Data pendataan berhasil di verifikasi'
                ];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(route('pendataan.verifikasi.peta'))->with($flash);
    }

    public function storeVerifikasi(Request $request)
    {
        // return $request->all();
        db::beginTransaction();
        try {
            //code...
            if ($request->verifikasi_kode == '1') {
                $objek = ModelsPendataanObjek::with(['penetapan', 'batch'])
                    ->where('pendataan_batch_id', $request->id)
                    ->orderby('id', 'asc')->get();
                $i = 0;

                // dd($objek);
                foreach ($objek as $row) {
                    $kd_propinsi = $row->kd_propinsi;
                    $kd_dati2 = $row->kd_dati2;
                    $kd_kecamatan = $row->kd_kecamatan;
                    $kd_kelurahan = $row->kd_kelurahan;
                    $kd_blok = $row->kd_blok;
                    $no_urut = $row->no_urut;
                    $kd_jns_op = $row->kd_jns_op;
                    $nop_asal = onlyNumber($row->nop_asal);
                    if ($no_urut == '') {
                        $nop_proses = Pajak::generateNOP($kd_kecamatan, $kd_kelurahan, $kd_blok);
                        $jtr = 1;

                        $i++;
                    } else {

                        if ($row->batch->jenis_pendataan_id == 1) {
                            $jtr = 1;
                        } else {
                            $jtr = 2;
                        }

                        $nop_proses = $nop_asal;
                    }
                    $data = [
                        'jenis_pendataan_id' => $row->batch->jenis_pendataan_id,
                        'pendataan_objek_id' => $row->id,
                        'nop_proses' => $nop_proses,
                        'nop_asal' => $nop_asal,
                        'jns_transaksi' => $jtr,
                        'jalan_op' => $row->jalan_op,
                        'blok_kav_no_op' => $row->blok_kav_no_op,
                        'rt_op' => $row->rt_op,
                        'rw_op' => $row->rw_op,
                        'no_persil' => $row->no_persil,
                        'luas_bumi' => $row->luas_bumi,
                        'kd_znt' => $row->kd_znt,
                        'jns_bumi' => $row->jns_bumi,
                        'jml_bng' => $row->jml_bng,
                        'subjek_pajak_id' => $row->subjek_pajak_id != '' ? $row->subjek_pajak_id : substr($nop_asal, 2, 16),
                        'nm_wp' => $row->nm_wp <> '' ? $row->nm_wp : 'WAJIB PAJAK',
                        'jalan_wp' => $row->jalan_wp <> '' ? $row->jalan_wp : '-',
                        'blok_kav_no_wp' => $row->blok_kav_no_wp,
                        'rt_wp' => $row->rt_wp,
                        'rw_wp' => $row->rw_wp,
                        'kelurahan_wp' => $row->kelurahan_wp,
                        'kecamatan_wp' => $row->kecamatan_wp,
                        'kota_wp' => $row->kota_wp,
                        'propinsi_wp' => $row->propinsi_wp,
                        'kd_pos_wp' => $row->kd_pos_wp,
                        'status_pekerjaan_wp' => $row->status_pekerjaan_wp <> '' ? $row->status_pekerjaan_wp : '5',
                        'kd_status_wp' => $row->kd_status_wp,
                        'npwp' => $row->npwp,
                        'telp_wp' => $row->telp_wp,
                    ];

                    if ($row->batch->jenis_pendataan_id == 1) {
                        $mulai_tahun = [];
                        $tgl_jatuh_tempo = [];
                        // $mulai_tahun[] = date('Y');
                        // $tgl_jatuh_tempo[] = date('Y') . '0831';
                        $data['is_nota'] = 0;
                        $data['mulai_tahun'] = $mulai_tahun;
                        $data['tgl_jatuh_tempo'] = $tgl_jatuh_tempo;
                    } else {

                        if ($row->penetapan()->get()->count() > 0) {
                            $mulai_tahun = [];
                            $tgl_jatuh_tempo = [];
                            $hkpd_individu = [];
                            foreach ($row->penetapan()->get() as $rk) {
                                $mulai_tahun[] = $rk->mulai_tahun;
                                $tgl_jatuh_tempo[] = $rk->tgl_jatuh_tempo;
                                $hkpd_individu[] = $rk->hkpd_individu;
                            }
                            $data['is_nota'] = $rk->is_nota;
                            $data['mulai_tahun'] = $mulai_tahun;
                            $data['tgl_jatuh_tempo'] = $tgl_jatuh_tempo;
                            $data['hkpd_individu'] = $hkpd_individu;
                        }
                    }


                    if ($row->jns_bumi == 1) {
                        $bng = PendataanObjekBng::where('pendataan_objek_id', $row->id)->get();
                        $created_at = [];

                        $no_bng = [];
                        $kd_jpb = [];
                        $thn_dibangun_bng = [];
                        $thn_renovasi_bng = [];
                        $luas_bng = [];
                        $jml_lantai_bng = [];
                        $kondisi_bng = [];
                        $jns_konstruksi_bng = [];
                        $jns_atap_bng = [];
                        $kd_dinding = [];
                        $kd_lantai = [];
                        $kd_langit_langit = [];
                        $daya_listrik = [];
                        $acsplit = [];
                        $acwindow = [];
                        $acsentral = [];
                        $luas_kolam = [];
                        $finishing_kolam = [];
                        $luas_perkerasan_ringan = [];
                        $luas_perkerasan_sedang = [];
                        $luas_perkerasan_berat = [];
                        $luas_perkerasan_dg_tutup = [];
                        $lap_tenis_lampu_beton = [];
                        $lap_tenis_lampu_aspal = [];
                        $lap_tenis_lampu_rumput = [];
                        $lap_tenis_beton = [];
                        $lap_tenis_aspal = [];
                        $lap_tenis_rumput = [];
                        $lift_penumpang = [];
                        $lift_kapsul = [];
                        $lift_barang = [];
                        $tgg_berjalan_a = [];
                        $tgg_berjalan_b = [];
                        $pjg_pagar = [];
                        $bhn_pagar = [];
                        $hydrant = [];
                        $sprinkler = [];
                        $fire_alarm = [];
                        $jml_pabx = [];
                        $sumur_artesis = [];
                        $nilai_individu = [];
                        $jpb3_8_tinggi_kolom = [];
                        $jpb3_8_lebar_bentang = [];
                        $jpb3_8_dd_lantai = [];
                        $jpb3_8_kel_dinding = [];
                        $jpb3_8_mezzanine = [];
                        $jpb5_kls_bng = [];
                        $jpb5_luas_kamar = [];
                        $jpb5_luas_rng_lain = [];
                        $jpb7_jns_hotel = [];
                        $jpb7_bintang = [];
                        $jpb7_jml_kamar = [];
                        $jpb7_luas_kamar = [];
                        $jpb7_luas_rng_lain = [];
                        $jpb13_kls_bng = [];
                        $jpb13_jml = [];
                        $jpb13_luas_kamar = [];
                        $jpb13_luas_rng_lain = [];
                        $jpb15_letak_tangki = [];
                        $jpb15_kapasitas_tangki = [];
                        $jpb_lain_kls_bng = [];
                        foreach ($bng as $bng) {
                            $no_bng[] = $bng->no_bng;
                            $kd_jpb[] = $bng->kd_jpb;
                            $thn_dibangun_bng[] = $bng->thn_dibangun_bng;
                            $thn_renovasi_bng[] = $bng->thn_renovasi_bng;
                            $luas_bng[] = $bng->luas_bng;
                            $jml_lantai_bng[] = $bng->jml_lantai_bng;
                            $kondisi_bng[] = $bng->kondisi_bng;
                            $jns_konstruksi_bng[] = $bng->jns_konstruksi_bng;
                            $jns_atap_bng[] = $bng->jns_atap_bng;
                            $kd_dinding[] = $bng->kd_dinding;
                            $kd_lantai[] = $bng->kd_lantai;
                            $kd_langit_langit[] = $bng->kd_langit_langit;
                            $daya_listrik[] = $bng->daya_listrik;
                            $acsplit[] = $bng->acsplit;
                            $acwindow[] = $bng->acwindow;
                            $acsentral[] = $bng->acsentral;
                            $luas_kolam[] = $bng->luas_kolam;
                            $finishing_kolam[] = $bng->finishing_kolam;
                            $luas_perkerasan_ringan[] = $bng->luas_perkerasan_ringan;
                            $luas_perkerasan_sedang[] = $bng->luas_perkerasan_sedang;
                            $luas_perkerasan_berat[] = $bng->luas_perkerasan_berat;
                            $luas_perkerasan_dg_tutup[] = $bng->luas_perkerasan_dg_tutup;
                            $lap_tenis_lampu_beton[] = $bng->lap_tenis_lampu_beton;
                            $lap_tenis_lampu_aspal[] = $bng->lap_tenis_lampu_aspal;
                            $lap_tenis_lampu_rumput[] = $bng->lap_tenis_lampu_rumput;
                            $lap_tenis_beton[] = $bng->lap_tenis_beton;
                            $lap_tenis_aspal[] = $bng->lap_tenis_aspal;
                            $lap_tenis_rumput[] = $bng->lap_tenis_rumput;
                            $lift_penumpang[] = $bng->lift_penumpang;
                            $lift_kapsul[] = $bng->lift_kapsul;
                            $lift_barang[] = $bng->lift_barang;
                            $tgg_berjalan_a[] = $bng->tgg_berjalan_a;
                            $tgg_berjalan_b[] = $bng->tgg_berjalan_b;
                            $pjg_pagar[] = $bng->pjg_pagar;
                            $bhn_pagar[] = $bng->bhn_pagar;
                            $hydrant[] = $bng->hydrant;
                            $sprinkler[] = $bng->sprinkler;
                            $fire_alarm[] = $bng->fire_alarm;
                            $jml_pabx[] = $bng->jml_pabx;
                            $sumur_artesis[] = $bng->sumur_artesis;
                            $nilai_individu[] = $bng->nilai_individu;
                            $jpb3_8_tinggi_kolom[] = $bng->jpb3_8_tinggi_kolom;
                            $jpb3_8_lebar_bentang[] = $bng->jpb3_8_lebar_bentang;
                            $jpb3_8_dd_lantai[] = $bng->jpb3_8_dd_lantai;
                            $jpb3_8_kel_dinding[] = $bng->jpb3_8_kel_dinding;
                            $jpb3_8_mezzanine[] = $bng->jpb3_8_mezzanine;
                            $jpb5_kls_bng[] = $bng->jpb5_kls_bng;
                            $jpb5_luas_kamar[] = $bng->jpb5_luas_kamar;
                            $jpb5_luas_rng_lain[] = $bng->jpb5_luas_rng_lain;
                            $jpb7_jns_hotel[] = $bng->jpb7_jns_hotel;
                            $jpb7_bintang[] = $bng->jpb7_bintang;
                            $jpb7_jml_kamar[] = $bng->jpb7_jml_kamar;
                            $jpb7_luas_kamar[] = $bng->jpb7_luas_kamar;
                            $jpb7_luas_rng_lain[] = $bng->jpb7_luas_rng_lain;
                            $jpb13_kls_bng[] = $bng->jpb13_kls_bng;
                            $jpb13_jml[] = $bng->jpb13_jml;
                            $jpb13_luas_kamar[] = $bng->jpb13_luas_kamar;
                            $jpb13_luas_rng_lain[] = $bng->jpb13_luas_rng_lain;
                            $jpb15_letak_tangki[] = $bng->jpb15_letak_tangki;
                            $jpb15_kapasitas_tangki[] = $bng->jpb15_kapasitas_tangki;
                            $jpb_lain_kls_bng[] = $bng->jpb_lain_kls_bng;


                            $created_at[] = $bng->created_at;
                            // $nilai_individu[]=$bng->nilai_individu;
                        }

                        $data['no_bng'] = $no_bng;
                        $data['kd_jpb'] = $kd_jpb;
                        $data['thn_dibangun_bng'] = $thn_dibangun_bng;
                        $data['thn_renovasi_bng'] = $thn_renovasi_bng;
                        $data['luas_bng'] = $luas_bng;
                        $data['jml_lantai_bng'] = $jml_lantai_bng;
                        $data['kondisi_bng'] = $kondisi_bng;
                        $data['jns_konstruksi_bng'] = $jns_konstruksi_bng;
                        $data['jns_atap_bng'] = $jns_atap_bng;
                        $data['kd_dinding'] = $kd_dinding;
                        $data['kd_lantai'] = $kd_lantai;
                        $data['kd_langit_langit'] = $kd_langit_langit;
                        $data['daya_listrik'] = $daya_listrik;
                        $data['acsplit'] = $acsplit;
                        $data['acwindow'] = $acwindow;
                        $data['acsentral'] = $acsentral;
                        $data['luas_kolam'] = $luas_kolam;
                        $data['finishing_kolam'] = $finishing_kolam;
                        $data['luas_perkerasan_ringan'] = $luas_perkerasan_ringan;
                        $data['luas_perkerasan_sedang'] = $luas_perkerasan_sedang;
                        $data['luas_perkerasan_berat'] = $luas_perkerasan_berat;
                        $data['luas_perkerasan_dg_tutup'] = $luas_perkerasan_dg_tutup;
                        $data['lap_tenis_lampu_beton'] = $lap_tenis_lampu_beton;
                        $data['lap_tenis_lampu_aspal'] = $lap_tenis_lampu_aspal;
                        $data['lap_tenis_lampu_rumput'] = $lap_tenis_lampu_rumput;
                        $data['lap_tenis_beton'] = $lap_tenis_beton;
                        $data['lap_tenis_aspal'] = $lap_tenis_aspal;
                        $data['lap_tenis_rumput'] = $lap_tenis_rumput;
                        $data['lift_penumpang'] = $lift_penumpang;
                        $data['lift_kapsul'] = $lift_kapsul;
                        $data['lift_barang'] = $lift_barang;
                        $data['tgg_berjalan_a'] = $tgg_berjalan_a;
                        $data['tgg_berjalan_b'] = $tgg_berjalan_b;
                        $data['pjg_pagar'] = $pjg_pagar;
                        $data['bhn_pagar'] = $bhn_pagar;
                        $data['hydrant'] = $hydrant;
                        $data['sprinkler'] = $sprinkler;
                        $data['fire_alarm'] = $fire_alarm;
                        $data['jml_pabx'] = $jml_pabx;
                        $data['sumur_artesis'] = $sumur_artesis;
                        $data['nilai_individu'] = $nilai_individu;
                        $data['jpb3_8_tinggi_kolom'] = $jpb3_8_tinggi_kolom;
                        $data['jpb3_8_lebar_bentang'] = $jpb3_8_lebar_bentang;
                        $data['jpb3_8_dd_lantai'] = $jpb3_8_dd_lantai;
                        $data['jpb3_8_kel_dinding'] = $jpb3_8_kel_dinding;
                        $data['jpb3_8_mezzanine'] = $jpb3_8_mezzanine;
                        $data['jpb5_kls_bng'] = $jpb5_kls_bng;
                        $data['jpb5_luas_kamar'] = $jpb5_luas_kamar;
                        $data['jpb5_luas_rng_lain'] = $jpb5_luas_rng_lain;
                        $data['jpb7_jns_hotel'] = $jpb7_jns_hotel;
                        $data['jpb7_bintang'] = $jpb7_bintang;
                        $data['jpb7_jml_kamar'] = $jpb7_jml_kamar;
                        $data['jpb7_luas_kamar'] = $jpb7_luas_kamar;
                        $data['jpb7_luas_rng_lain'] = $jpb7_luas_rng_lain;
                        $data['jpb13_kls_bng'] = $jpb13_kls_bng;
                        $data['jpb13_jml'] = $jpb13_jml;
                        $data['jpb13_luas_kamar'] = $jpb13_luas_kamar;
                        $data['jpb13_luas_rng_lain'] = $jpb13_luas_rng_lain;
                        $data['jpb15_letak_tangki'] = $jpb15_letak_tangki;
                        $data['jpb15_kapasitas_tangki'] = $jpb15_kapasitas_tangki;
                        $data['jpb_lain_kls_bng'] = $jpb_lain_kls_bng;
                    }
                    $data['created_by'] = $row->created_by;
                    $data['created_at'] = db::raw('sysdate');
                    // dd($data);

                    // pendataanObjek::dispatch($data)->onQueue('pendataan');

                    $req = new \Illuminate\Http\Request();
                    $req->merge($data);

                    // Log::info($req);
                    // return $req;
                    Pajak::prosesPendataan($req);

                    if ($row->jns_bumi == '5') {
                        // NON aktif kan semua tunggakan yang belum di bayar
                        $ck = DB::connection("oracle_satutujuh")->select(db::raw("select sppt.kd_propinsi,
                            sppt.kd_dati2,
                            sppt.kd_kecamatan,
                            sppt.kd_kelurahan,
                            sppt.kd_blok,
                            sppt.no_urut,
                            sppt.kd_jns_op,
                            sppt.no_urut,
                            sppt.thn_pajak_sppt,jns_koreksi,no_surat
                                    from sppt  
                                    left join sppt_koreksi on sppt.kd_propinsi=sppt_koreksi.kd_propinsi and
                            sppt.kd_dati2=sppt_koreksi.kd_dati2 and
                            sppt.kd_kecamatan=sppt_koreksi.kd_kecamatan and
                            sppt.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                            sppt.kd_blok=sppt_koreksi.kd_blok and
                            sppt.no_urut=sppt_koreksi.no_urut and
                            sppt.kd_jns_op=sppt_koreksi.kd_jns_op and
                            sppt.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt
                            where sppt.kd_propinsi='$kd_propinsi' and 
                            sppt.kd_dati2='$kd_dati2' and 
                            sppt.kd_kecamatan='$kd_kecamatan' and 
                            sppt.kd_kelurahan='$kd_kelurahan' and 
                            sppt.kd_blok='$kd_blok' and 
                            sppt.no_urut='$no_urut' and 
                            sppt.kd_jns_op='$kd_jns_op' and 
                            status_pembayaran_sppt!='1' 
                            and jns_koreksi is null"));

                        // ambil nomoer transaksi untuk sppt_koreksi
                        $ntr = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                            ->select(db::raw("'IV'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'IV'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
                            ->whereraw("no_transaksi like 'IV'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi;
                        $koreksi = [];
                        foreach ($ck as $tg) {


                            DB::connection('oracle_satutujuh')->table('sppt')
                                ->where([
                                    'kd_propinsi' => $tg->kd_propinsi,
                                    'kd_dati2' => $tg->kd_dati2,
                                    'kd_kecamatan' => $tg->kd_kecamatan,
                                    'kd_kelurahan' => $tg->kd_kelurahan,
                                    'kd_blok' => $tg->kd_blok,
                                    'no_urut' => $tg->no_urut,
                                    'kd_jns_op' => $tg->kd_jns_op,
                                    'thn_pajak_sppt' => $tg->thn_pajak_sppt
                                ])
                                ->update(['status_pembayaran_sppt' => 3]);

                            $koreksi[] = [
                                'kd_propinsi' => $tg->kd_propinsi,
                                'kd_dati2' => $tg->kd_dati2,
                                'kd_kecamatan' => $tg->kd_kecamatan,
                                'kd_kelurahan' => $tg->kd_kelurahan,
                                'kd_blok' => $tg->kd_blok,
                                'no_urut' => $tg->no_urut,
                                'kd_jns_op' => $tg->kd_jns_op,
                                'thn_pajak_sppt' => $tg->thn_pajak_sppt,
                                'keterangan' => 'Permohonan penonaktifan  Objek',
                                'verifikasi_kode' => '1',
                                'no_surat' => $ntr,
                                'tgl_surat' => db::raw("sysdate"),
                                'verifikasi_by' => auth()->user()->id,
                                'verifikasi_keterangan' => 'Proses penelitian/pendataan',
                                'no_transaksi' => $ntr,
                                'created_at' => db::raw("sysdate"),
                                'created_by' => $row->created_by,
                                'jns_koreksi' => '1'
                            ];
                        }
                        if (!empty($koreksi)) {
                            DB::connection('oracle_satutujuh')
                                ->table('sppt_koreksi')
                                ->insert($koreksi);
                        }
                    } else {
                        // hapus data penonaktifan selain koreksi piutang
                        $dp = DB::connection("oracle_satutujuh")->select(db::raw("select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan ,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt 
                            from sppt_koreksi
                            where kd_propinsi='$kd_propinsi' and 
                            kd_dati2='$kd_dati2' and 
                            kd_kecamatan='$kd_kecamatan' and 
                            kd_kelurahan='$kd_kelurahan' and 
                            kd_blok='$kd_blok' and 
                            no_urut='$no_urut' and 
                            kd_jns_op='$kd_jns_op'
                            and jns_koreksi<>'3'"));

                        foreach ($dp as $db) {
                            try {
                                //code...
                                $where = [
                                    'kd_propinsi' => $db->kd_propinsi,
                                    'kd_dati2' => $db->kd_dati2,
                                    'kd_kecamatan' => $db->kd_kecamatan,
                                    'kd_kelurahan' => $db->kd_kelurahan,
                                    'kd_blok' => $db->kd_blok,
                                    'no_urut' => $db->no_urut,
                                    'kd_jns_op' => $db->kd_jns_op,
                                    'thn_pajak_sppt' => $db->thn_pajak_sppt
                                ];
                                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->where($where)->delete();
                                DB::connection("oracle_satutujuh")->table('sppt')->where($where)->update(['status_pembayaran_sppt' => '0']);
                                DB::connection("oracle_satutujuh")->commit();
                            } catch (\Throwable $th) {
                                db::connection("oracle_satutujuh")->rollBack();
                            }
                        }
                    }

                    $spop[] = $data['nop_proses'];
                }
            }

            if ($request->verifikasi_kode == '0') {
                if (!empty($request->mark_tolak)) {
                    ModelsPendataanObjek::wherein('id', $request->mark_tolak)->update(['mark_tolak' => '1']);
                }
            }

            $batch = [
                'verifikasi_deskripsi' => $request->verifikasi_deskripsi,
                'verifikasi_kode' => $request->verifikasi_kode,
                'verifikasi_by' => Auth()->user()->id,
                'verifikasi_at' => db::raw('sysdate')
            ];

            PendataanBatch::where('id', $request->id)->update($batch);
            db::commit();
            // dd($data);
            $flash =
                [
                    'status' => 1,
                    'msg' => 'Data pendataan berhasil di verifikasi'
                ];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            Log::error($th);
            $flash =
                [
                    'status' => 0,
                    'msg' => $th->getMessage()
                ];
        }

        return $flash;
        // return redirect(route('pendataan.verifikasi'))->with($flash);
    }

    public function destroy($id)
    {
        db::beginTransaction();
        try {
            $cek = PendataanBatch::where('id', $id)->wherenull('verifikasi_at')->first();
            if ($cek) {

                PendataanBatch::where('id', $id)->wherenull('verifikasi_at')->delete();
                $flash = [
                    'success' => 'Draft pendataan berhasil di dihapus'
                ];
            } else {
                $flash = [
                    'warning' => 'Data tidak ditemukan'
                ];
            }


            db::commit();
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(route('pendataan.index'))->with($flash);
    }


    public function showModal($id)
    {
        $objek = ModelsPendataanObjek::with('bng')->where('id', $id)
            ->selectraw("pendataan_objek.id,mark_tolak,nop_asal,pendataan_objek.kd_propinsi,pendataan_objek.kd_dati2,pendataan_objek.kd_kecamatan,pendataan_objek.kd_kelurahan,pendataan_objek.kd_blok,pendataan_objek.no_urut,pendataan_objek.kd_jns_op, nm_wp, no_persil, jalan_op, blok_kav_no_op, rw_op, rt_op, pendataan_objek.kd_znt, luas_bumi, jns_bumi, nm_kecamatan,nm_kelurahan,nir * 1000 nir, subjek_pajak_id,jalan_wp,blok_kav_no_wp,
                        rw_wp,rt_wp,kelurahan_wp,kota_wp,kd_pos_wp,telp_wp,npwp,status_pekerjaan_wp,kd_status_cabang,kd_status_wp,kecamatan_wp,propinsi_wp")
            ->join(db::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'pendataan_objek.kd_kecamatan')
            ->join(db::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
                $join->on('ref_kelurahan.kd_kelurahan', '=', 'pendataan_objek.kd_kelurahan')
                    ->on('ref_kelurahan.kd_kecamatan', '=', 'pendataan_objek.kd_kecamatan');
            })
            ->join(db::raw('pbb.dat_nir dat_nir'), function ($join) {
                $join->on('dat_nir.kd_znt', '=', 'pendataan_objek.kd_znt')
                    ->on('pendataan_objek.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                    ->on('pendataan_objek.kd_kelurahan', '=', 'dat_nir.kd_kelurahan')
                    ->on("dat_nir.thn_nir_znt", '=', DB::raw("to_char(pendataan_objek.created_at,'yyyy')"));
            })
            ->first();

        return view('pendataan.show_detail', compact('objek'));
    }

    public function draftPdf($id)
    {
        $batch = PendataanBatch::ListPendataan($id)
            ->first();
        $objek = ModelsPendataanObjek::with('spop')->where('pendataan_batch_id', $id)
            ->selectraw("pendataan_objek.id,mark_tolak,nop_asal,pendataan_objek.kd_propinsi,pendataan_objek.kd_dati2,pendataan_objek.kd_kecamatan,pendataan_objek.kd_kelurahan,pendataan_objek.kd_blok,pendataan_objek.no_urut,pendataan_objek.kd_jns_op, nm_wp, no_persil, jalan_op, blok_kav_no_op, rw_op, rt_op, pendataan_objek.kd_znt, luas_bumi, jns_bumi, nm_kecamatan,nm_kelurahan,nir * 1000 nir, nop_asal,subjek_pajak_id,jalan_wp,blok_kav_no_wp,
                        rw_wp,rt_wp,kelurahan_wp,kota_wp,kd_pos_wp,telp_wp,npwp,status_pekerjaan_wp,no_persil,kd_status_cabang,kd_status_wp,kecamatan_wp,propinsi_wp")
            ->join(db::raw('pbb.ref_kecamatan ref_kecamatan'), 'ref_kecamatan.kd_kecamatan', '=', 'pendataan_objek.kd_kecamatan')
            ->join(db::raw('pbb.ref_kelurahan ref_kelurahan'), function ($join) {
                $join->on('ref_kelurahan.kd_kelurahan', '=', 'pendataan_objek.kd_kelurahan')
                    ->on('ref_kelurahan.kd_kecamatan', '=', 'pendataan_objek.kd_kecamatan');
            })
            ->join(db::raw('pbb.dat_nir dat_nir'), function ($join) {
                $join->on('dat_nir.kd_znt', '=', 'pendataan_objek.kd_znt')
                    ->on('pendataan_objek.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                    ->on('pendataan_objek.kd_kelurahan', '=', 'dat_nir.kd_kelurahan')
                    ->on("dat_nir.thn_nir_znt", '=', DB::raw("to_char(pendataan_objek.created_at,'yyyy')"));
            })
            ->get();

        // return $objek;
        // 
        $pages = view('pendataan/draft_pdf', compact('batch', 'objek'));

        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4')
            ->setOption('margin-left', '5')
            ->setOption('margin-right', '5')
            ->setOption('margin-top', '5')
            ->setOption('margin-bottom', '5')
            ->setOption('orientation', 'landscape');
        $filename = 'darft pendataan batch ' . $batch->nomor_batch . '.pdf';
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream($filename);
    }

    public function formPembatalanSistep(Request $request)
    {
        if ($request->ajax()) {
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;

            $bj = DB::connection("oracle_satutujuh")->table("dat_op_bumi")
                ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_jns_op='7' and jns_bumi not in ('4','5')")
                ->selectraw("count(1) jumlah")->first();
            return $bj->jumlah ?? 0;
        }

        $jns_pendataan_id = '5';
        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();

        return view('pendataan/form-pembatalan-sistep', compact('kecamatan', 'jns_pendataan_id'));
    }

    public function storePembatalanSistep(Request $request)
    {


        $kdkecamatan = $request->kd_kecamatan;
        $kdkelurahan = $request->kd_kelurahan;
        $no_surat = $request->no_surat;
        $tgl_surat = $request->tgl_surat;


        DB::beginTransaction();
        try {

            $keterangan = "Pembatalan karena objek di migrasi ke sismiop";
            $tunggakan = DB::connection("oracle_satutujuh")->select(DB::raw("select sppt.kd_propinsi,
                                                            sppt.kd_dati2,
                                                            sppt.kd_kecamatan,
                                                            sppt.kd_kelurahan,
                                                            sppt.kd_blok,
                                                            sppt.no_urut,
                                                            sppt.kd_jns_op,
                                                            sppt.thn_pajak_sppt,
                                                            jns_koreksi,
                                                            sppt.status_pembayaran_sppt,
                                                            oltp.pbb_yg_harus_dibayar_sppt
                                                    from sppt
                                                            left join sppt_koreksi
                                                            on     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                                                and sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                                                and sppt.kd_kecamatan = sppt_koreksi.kd_kecamatan
                                                                and sppt.kd_kelurahan = sppt_koreksi.kd_kelurahan
                                                                and sppt.kd_blok = sppt_koreksi.kd_blok
                                                                and sppt.no_urut = sppt_koreksi.no_urut
                                                                and sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                                                and sppt.thn_pajak_sppt = sppt_koreksi.thn_pajak_sppt
                                                            left join sppt_pembatalan
                                                            on     sppt.kd_propinsi = sppt_pembatalan.kd_propinsi
                                                                and sppt.kd_dati2 = sppt_pembatalan.kd_dati2
                                                                and sppt.kd_kecamatan = sppt_pembatalan.kd_kecamatan
                                                                and sppt.kd_kelurahan = sppt_pembatalan.kd_kelurahan
                                                                and sppt.kd_blok = sppt_pembatalan.kd_blok
                                                                and sppt.no_urut = sppt_pembatalan.no_urut
                                                                and sppt.kd_jns_op = sppt_pembatalan.kd_jns_op
                                                                and sppt.thn_pajak_sppt = sppt_pembatalan.thn_pajak_sppt
                                                                and tgl_mulai <= sysdate
                                                                and tgl_selesai >= sysdate
                                                            left join spo.sppt_oltp oltp
                                                            on     sppt.kd_propinsi = oltp.kd_propinsi
                                                                and sppt.kd_dati2 = oltp.kd_dati2
                                                                and sppt.kd_kecamatan = oltp.kd_kecamatan
                                                                and sppt.kd_kelurahan = oltp.kd_kelurahan
                                                                and sppt.kd_blok = oltp.kd_blok
                                                                and sppt.no_urut = oltp.no_urut
                                                                and sppt.kd_jns_op = oltp.kd_jns_op
                                                                and sppt.thn_pajak_sppt = oltp.thn_pajak_sppt
                                                    where     sppt.kd_propinsi = '35'
                                                            and sppt.kd_dati2 = '07'
                                                            and sppt.kd_kecamatan = '$kdkecamatan'
                                                            and sppt.kd_kelurahan = '$kdkelurahan'
                                                            and sppt.kd_jns_op = '7'
                                                            and sppt.status_pembayaran_sppt != '1'
                                                            and sppt_pembatalan.thn_pajak_sppt is null
                                                            and sppt_koreksi.thn_pajak_sppt is null"));
            $now = Carbon::now();
            $userid = Auth()->user()->id;
            $dd = [];
            $wh = "";
            // return $tunggakan;

            foreach ($tunggakan as $key => $value) {
                # code...
                if (!in_array($value->jns_koreksi, ['1', '3'])) {
                    $thn_pajak_sppt = $value->thn_pajak_sppt;
                    // $ins = $var;
                    $ins['kd_propinsi'] = $value->kd_propinsi;
                    $ins['kd_dati2'] = $value->kd_dati2;
                    $ins['kd_kecamatan'] = $value->kd_kecamatan;
                    $ins['kd_kelurahan'] = $value->kd_kelurahan;
                    $ins['kd_blok'] = $value->kd_blok;
                    $ins['no_urut'] = $value->no_urut;
                    $ins['kd_jns_op'] = $value->kd_jns_op;
                    $ins['thn_pajak_sppt'] = $thn_pajak_sppt;
                    $notr = PembatalanObjek::NoTransaksi('SM', '06');
                    // $now = Carbon::now();
                    $ins['tgl_surat'] = new Carbon($tgl_surat);
                    $ins['no_surat'] = $no_surat;
                    $ins['jns_koreksi'] = '3';
                    $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                    $ins['keterangan'] = $keterangan;
                    $ins['no_transaksi'] = $notr;
                    $ins['created_at'] = $now;
                    $ins['created_by'] = Auth()->user()->id;
                    $ins['nm_kategori'] = $keterangan;
                    $ins['kd_kategori'] = '06';

                    $ins['verifikasi_kode'] = 1;
                    $ins['verifikasi_keterangan'] = $keterangan;
                    $ins['verifikasi_by'] = auth()->user()->id;
                    $ins['verifikasi_at'] = $now;
                    $dd[] = $ins;
                }
            }


            $bj = DB::connection("oracle_satutujuh")->table("dat_op_bumi")
                ->whereraw("kd_kecamatan='$kdkecamatan' and kd_kelurahan='$kdkelurahan' and kd_jns_op='7' and jns_bumi not in ('4','5')")->get();
            $wha = "";
            foreach ($bj as $value) {
                $wha .= " ( kd_propinsi='" . $value->kd_propinsi . "' and 
                kd_dati2='" . $value->kd_dati2 . "' and 
                kd_kecamatan='" . $value->kd_kecamatan . "' and 
                kd_kelurahan='" . $value->kd_kelurahan . "' and 
                kd_blok='" . $value->kd_blok . "' and 
                no_urut='" . $value->no_urut . "' and 
                kd_jns_op='" . $value->kd_jns_op . "') or";
            }

            // return $dd;
            if (count($dd) > 0) {
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);
            }
            // dd($dd);

            // dd($postData);
            if ($wh != "") {
                DB::connection("oracle_satutujuh")->statement("update sppt set status_pembayaran_sppt='3' where   (" . substr($wh, 0, -2) . ")");
            }

            if ($wha != "") {
                DB::connection("oracle_satutujuh")->statement("update dat_op_bumi set jns_bumi='5' where  jns_bumi not in ('4','5') and  (" . substr($wha, 0, -2) . ")");
            }
            DB::commit();
            $with = ['success' => 'Telah berhasil di batalkan'];
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);

            DB::rollBack();
            $with = ['error' => $th->getMessage()];
    
        }
    
        return redirect(url('pendataan/penangguhan'))->with($with);
    }
}
