<?php

namespace App\Http\Controllers;

use App\Dokumen;
use App\Imports\NopRekonImport;
use App\KelolaUsulan;
use App\KelolaUsulanObjek;
use App\Models\DAT_OBJEK_PAJAK;
use App\Models\Layanan_dokumen;
use App\Traits\OcaWablas;
use App\User;
use App\UsulanPengelola;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Calculation\TextData\Format;

class KelolaNopController extends Controller
{
    use OcaWablas;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return AUth()->user()->is_wp();

        // use Illuminate\Support\Facades\DB;
        $user_id = Auth()->user()->id;
        $data = DB::table('kelola_usulan_objek')
            ->select(
                'verifikasi_kode',
                'verifikasi_at',
                'kelola_usulan_objek.kd_propinsi',
                'kelola_usulan_objek.kd_dati2',
                'kelola_usulan_objek.kd_kecamatan',
                'kelola_usulan_objek.kd_kelurahan',
                'kelola_usulan_objek.kd_blok',
                'kelola_usulan_objek.no_urut',
                'kelola_usulan_objek.kd_jns_op',
                'jalan_op',
                'blok_kav_no_op',
                'rt_op',
                'rw_op',
                'total_luas_bumi',
                'total_luas_bng',
                'nm_wp',
                DB::raw('pbb_yg_harus_dibayar_sppt - COALESCE(nilai_potongan, 0) AS pbb'),
                'status_pembayaran_sppt'
            )
            ->join('pbb.dat_objek_pajak', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'kelola_usulan_objek.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'kelola_usulan_objek.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'kelola_usulan_objek.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'kelola_usulan_objek.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'kelola_usulan_objek.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'kelola_usulan_objek.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'kelola_usulan_objek.kd_jns_op');
            })
            ->join('pbb.dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
            ->leftJoin('pbb.sppt', function ($join) {
                $join->on('kelola_usulan_objek.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('kelola_usulan_objek.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('kelola_usulan_objek.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('kelola_usulan_objek.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('kelola_usulan_objek.kd_blok', '=', 'sppt.kd_blok')
                    ->on('kelola_usulan_objek.no_urut', '=', 'sppt.no_urut')
                    ->on('kelola_usulan_objek.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->where('sppt.thn_pajak_sppt', '=', date('Y'));
            })
            ->leftJoin('pbb.sppt_potongan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.THN_PAJAK_SPPT');
            })
            ->orderBy('kelola_usulan_objek.created_at', 'desc');
        if (Auth()->user()->is_wp() == true) {
            $data = $data->whereraw(" kelola_usulan_id in (select id from kelola_usulan  where KELOLA_USULAN.CREATED_BY = '$user_id')");
        }

        $data = $data->paginate();
        $is_admin = Auth()->user()->is_admin();
        return view('kelola.index', compact('data', 'is_admin'));

        /*    return $result;
        // batas source lama


        $data = KelolaUsulan::with(['user', 'KelolaUsulanObjek']);
        $is_wp = Auth()->user()->hasAnyRole(['Wajib Pajak', 'Rayon', 'DEVELOPER/PENGEMBANG']);
        if ($is_wp == true) {
            $data = $data->where('kelola_usulan.created_by', Auth()->user()->id);
        }
        $data = $data->orderbyraw('verifikasi_kode asc nulls first,id desc')->paginate();
        // return $data;

        $is_admin = Auth()->user()->hasAnyRole('Administrator', 'Super User');
        return view('kelola.index', compact('data', 'is_admin')); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth()->user()->hasAnyRole(['Wajib Pajak', 'Rayon', 'DEVELOPER/PENGEMBANG']) == false) {
            abort(404);
        }

        $data = [
            'action' => url('kelola-nop-preview'),
            'method' => 'post'
        ];
        // return $data;
        return view('kelola.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function previewNop(Request $request)
    {
        if (Auth()->user()->hasAnyRole(['Wajib Pajak', 'Rayon', 'DEVELOPER/PENGEMBANG']) == false) {
            abort(404);
        }

        $ry = Auth()->user()->UserRayon()->first();

        // return ->kd_kecamatan;
        // dd(Auth()->user()->UserRayon()->first());

        $kd_kecamatan = $ry->kd_kecamatan ?? '';
        $kd_kelurahan = $ry->kd_kelurahan ?? '';


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $ext = $file->extension();
            // return $ext;
            if (!in_array($ext, ["xlsx", "csv", "xls"])) {
                return redirect(url('kelola-nop/creare'));
            }

            $array = Excel::toArray(new NopRekonImport(), $file);
            $data = [];

            foreach ($array[0] as $row) {
                $res = trim(onlyNumber($row['nop']));
                if ($res <> '') {
                    // $v_kd_kecamatan = substr($res, 4, 3);
                    // $v_kd_kelurahan = substr($res, 7, 3);

                    $v_kd_propinsi = substr($res, 0, 2);
                    $v_kd_dati2 = substr($res, 2, 2);
                    $v_kd_kecamatan = substr($res, 4, 3);
                    $v_kd_kelurahan = substr($res, 7, 3);
                    $v_kd_blok = substr($res, 10, 3);
                    $v_no_urut = substr($res, 13, 4);
                    $v_kd_jns_op = substr($res, 17, 1);
                    $nama_wp = '';
                    $datanop['nop'] = formatnop($res);
                    if (strlen($res) <> 18) {

                        $datanop['status'] = 0;
                        $datanop['keterangan'] = 'Digit NOP harus 18';
                    } else {
                        $objek = DAT_OBJEK_PAJAK::where(
                            [
                                'kd_propinsi' => $v_kd_propinsi,
                                'kd_dati2' => $v_kd_dati2,
                                'kd_kecamatan' => $v_kd_kecamatan,
                                'kd_kelurahan' => $v_kd_kelurahan,
                                'kd_blok' => $v_kd_blok,
                                'no_urut' => $v_no_urut,
                                'kd_jns_op' => $v_kd_jns_op
                            ]
                        )->first();

                        if ($objek != null) {

                            $nama_wp = $objek->NmWp ?? '';
                            if (
                                $kd_kecamatan <> '' &&
                                $kd_kelurahan <> ''
                            ) {
                                // rayon
                                if (
                                    $kd_kecamatan <> $v_kd_kecamatan &&
                                    $kd_kelurahan <> $v_kd_kelurahan
                                ) {
                                    $status = 0;
                                    $keterangan = "NOP tidak sesuai wilayah";
                                } else {
                                    $status = 1;
                                    $keterangan = 'Bisa di proses';
                                }
                            } else {
                                // pengembang  
                                $status = 1;
                                $keterangan = 'Bisa di proses';
                            }
                        } else {
                            $status = 0;
                            $keterangan = "Objek tidak ditemukan";
                        }



                        $datanop['status'] = $status;
                        $datanop['nm_wp'] = $nama_wp;
                        $datanop['keterangan'] = $keterangan;
                    }
                    $data[] = $datanop;
                }
            }

            // return $data;
            $dataForm = [
                'action' => url('kelola-nop'),
                'method' => 'post',
                'nop' => $data
            ];


            return view('kelola.form-preview', compact('dataForm'));
            // dd($data);

        } else {
            return redirect(url('kelola-nop/creare'));
        }
    }

    public function store(Request $request)
    {
        if (Auth()->user()->hasAnyRole(['Rayon', 'DEVELOPER/PENGEMBANG']) == false) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $post = [
                'nik' => $request->nik,
                'tanggal_usulan' => now(),
                // 'verifikasi_kode' => '1',
                // 'verifikasi_by' => Auth()->user()->id,
                // 'verifikasi_at' => now(),
                'keterangan' => 'Usulan pengelolaan NOP',
            ];

            $kn = KelolaUsulan::create($post);

            foreach ($request->nop as $nop) {
                $res = onlyNumber($nop);
                $kd_propinsi = substr($res, 0, 2);
                $kd_dati2 = substr($res, 2, 2);
                $kd_kecamatan = substr($res, 4, 3);
                $kd_kelurahan = substr($res, 7, 3);
                $kd_blok = substr($res, 10, 3);
                $no_urut = substr($res, 13, 4);
                $kd_jns_op = substr($res, 17, 1);

                $obj = [
                    'kelola_usulan_id' => $kn->id,
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    // 'verifikasi_kode' => '1',
                    // 'verifikasi_by' => Auth()->user()->id,
                    // 'verifikasi_at' => now(),
                ];
                KelolaUsulanObjek::create($obj);
            }
            $flash = [
                'success' => "berhasiil menambahkan"
            ];
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::alert($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('users', AUth()->user()->id))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = KelolaUsulan::with('user')->find($id);
        //  return $data;
        $perusahaan = $data->user->UserPengelola()->first() ?? [];
        // return $perusahaan;index
        return view('kelola.show', compact('data', 'perusahaan'));
    }

    public function verifikasi($id)
    {
        $is_admin = Auth()->user()->hasAnyRole('Administrator', 'Super User');
        if ($is_admin == false) {
            abort(404);
        }

        $data = KelolaUsulan::with('user')->find($id);
        $perusahaan = $data->user->UserPengelola->first() ?? [];
        // return $perusahaan;

        return view('kelola.form-verifikasi', compact('data', 'perusahaan'));
    }


    public function verifikasiStore(Request $request, $id)
    {
        // return $request->all();
        // $oca = 


        // return $oca;
        $is_admin = Auth()->user()->hasAnyRole('Administrator', 'Super User');
        if ($is_admin == false) {
            abort(404);
        }
        DB::beginTransaction();
        try {
            $kode = $request->status_verifikasi;
            $keterangan = $request->keterangan;
            $obj = KelolaUsulanObjek::where('kelola_usulan_id', $id)->pluck('id')->toarray();

            foreach ($obj as $i) {
                $o = KelolaUsulanObjek::find($i);
                $o->update([
                    'verifikasi_kode' => $kode,
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_at' => now()
                ]);
            }
            //code...
            $ku = KelolaUsulan::find($id);
            $ku->update(['verifikasi_kode' => $kode, 'keterangan_verifikasi' => $keterangan, 'verifikasi_by' => auth()->user()->id, 'verifikasi_at' => now()]);

            $telepon = $ku->user->telepon ?? '';
            $nama = $ku->user->nama ?? 'Wajib Pajak';
            // $user=User::
            DB::commit();
            $flash = [
                'success' => 'Berhasil verifikasi'
            ];

            if ($kode == '1') {
                $this->NotifAcc($telepon, [$nama, '*USULAN PENGELOLAAN NOP*']);
            } else {
                // di tolak
                $this->NotifReject($telepon, [$nama, '*USULAN PENGELOLAAN NOP*', $keterangan ?? "Dokumen tidak sesuai"]);
            }
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(url('kelola-verifikasi-data'))->with($flash);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ide)
    {
        return decrypt($ide);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storePermohonanPengelola(Request $request, $id)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            //code...
            $data = [
                'user_id' => $id,
                'keterangan' => $request->keterangan,
                'nama_pengembang' => $request->nama_pengembang,
                'tgl_surat' => new Carbon($request->tgl_surat),
                'no_surat' => $request->no_surat,
                'verifikasi_kode' => '1'
            ];

            $usulan = UsulanPengelola::create($data);
            $disk = 'public';
            $file = $request->file('file');
            $filename = time() . '.' . $file->getClientOriginalExtension();


            if (Storage::disk($disk)->exists($filename)) {
                Storage::disk($disk)->delete($filename);
            }
            $upload_file = Storage::disk($disk);
            if ($upload_file->put($filename, File::get($file))) {
                $url_file = Storage::disk($disk)->url($filename);
                $url_file = str_replace(config('app.url'), '', $url_file);
                Dokumen::create([
                    'tablename' => 'usulan_pengelola',
                    'keyid' => $usulan->id,
                    'filename' => $filename,
                    'disk' => $disk,
                    'nama_dokumen' => 'Scan Permohonan',
                    'keterangan' => 'Scan Permohonan',
                    'url' => $url_file
                ]);
            }

            Db::commit();
            $flash = [
                'success' => 'Usulan berhasil di buat dan akan segera di verifikasi oleh petugas'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return Redirect(url('users', $id))->with($flash);
    }
}
