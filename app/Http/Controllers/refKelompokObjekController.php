<?php

namespace App\Http\Controllers;

use App\KelompokObjek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use PhpParser\Node\Stmt\TryCatch;

class refKelompokObjekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view_kelompok_objek')->only(['index']);
        $this->middleware('permission:add_kelompok_objek')->only(['create', 'store']);
        $this->middleware('permission:edit_kelompok_objek')->only(['edit', 'update']);
        $this->middleware('permission:delete_kelompok_objek')->only(['destroy']);
    }

    public function index()
    {
        //
        $title = 'Kelompok Objek';
        $data = KelompokObjek::orderby('id')->get();
        return view('kelobjek.index', compact('title', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = [
            'method' => 'post',
            'action' => route('refrensi.kelompokobjek.store'),
        ];
        $title = 'Form Kelompok Objek';

        return view('kelobjek.form', compact('title', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $data = ['nama_kelompok' => $request->nama_kelompok];
            KelompokObjek::create($data);
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di tambahkan'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.kelompokobjek.index'))->with($pesan);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
        $objek = KelompokObjek::findorfail($id);
        $data = [
            'method' => 'patch',
            'action' => route('refrensi.kelompokobjek.update', $id),
            'objek' => $objek
        ];
        $title = 'Form Kelompok Objek';

        return view('kelobjek.form', compact('title', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $objek = KelompokObjek::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            $data = ['nama_kelompok' => $request->nama_kelompok];
            $objek->update($data);
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di update'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.kelompokobjek.index'))->with($pesan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $objek = KelompokObjek::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            // $data = ['nama_kelompok' => $request->nama_kelompok];
            $objek->delete();
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di hapus'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.kelompokobjek.index'))->with($pesan);
    }
}
