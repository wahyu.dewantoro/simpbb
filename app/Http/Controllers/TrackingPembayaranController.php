<?php

namespace App\Http\Controllers;

use App\Helpers\InformasiObjek;
use App\KelolaUsulanObjek;
use App\Sppt;
use DOMPDF as PDF;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TrackingPembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function sqlDataPembayaran($nop)
    {
        $data = InformasiObjek::pembayaranSppt($nop);
        return $data;
    }


    public function apiRiwayatPembayaran(Request $request)
    {
        // Validasi request
        $request->validate([
            'kd_propinsi' => 'required|string',
            'kd_dati2' => 'required|string',
            'kd_kecamatan' => 'required|string',
            'kd_kelurahan' => 'required|string',
            'kd_blok' => 'required|string',
            'no_urut' => 'required|string',
            'kd_jns_op' => 'required|string',
        ]);

        // Mapping request ke variabel
        $params = [
            'kd_propinsi' => $request->kd_propinsi,
            'kd_dati2' => $request->kd_dati2,
            'kd_kecamatan' => $request->kd_kecamatan,
            'kd_kelurahan' => $request->kd_kelurahan,
            'kd_blok' => $request->kd_blok,
            'no_urut' => $request->no_urut,
            'kd_jns_op' => $request->kd_jns_op,
        ];

        // Ambil data dari query
        $data = $this->sqlDataPembayaran($params)->get();

        // Return response standar
        if ($data->isEmpty()) {
            return response()->json([
                'status' => false,
                'message' => 'Data tidak ditemukan',
                'data' => []
            ], 404);
        }

        return response()->json([
            'status' => true,
            'message' => 'Data ditemukan',
            'data' => $data
        ], 200);
    }


    public function index(Request $request)
    {
        $is_wp = Auth()->user()->hasrole(['DEVELOPER/PENGEMBANG', 'Rayon', 'Wajib Pajak']);
        if ($request->ajax()) {
            $nop = $request->nop;
            $res = onlyNumber($nop);
            $variable['kd_propinsi'] = substr($res, 0, 2);
            $variable['kd_dati2'] = substr($res, 2, 2);
            $variable['kd_kecamatan'] = substr($res, 4, 3);
            $variable['kd_kelurahan'] = substr($res, 7, 3);
            $variable['kd_blok'] = substr($res, 10, 3);
            $variable['no_urut'] = substr($res, 13, 4);
            $variable['kd_jns_op'] = substr($res, 17, 1);

            $data = $this->sqlDataPembayaran($variable)->get();
            // dd($data);
            return view('tracking_pembayaran._detail', compact('data', 'nop'));
        }
        $nop = [];
        if ($is_wp == true) {
            $nop = getNopWp();
        }

        return view('tracking_pembayaran.index', compact('nop', 'is_wp'));
    }

    public function cetak(Request $request)
    {
        $nop = $request->nop;
        $res = str_replace('.', '', $nop);
        $variable['kd_propinsi'] = substr($res, 0, 2);
        $variable['kd_dati2'] = substr($res, 2, 2);
        $variable['kd_kecamatan'] = substr($res, 4, 3);
        $variable['kd_kelurahan'] = substr($res, 7, 3);
        $variable['kd_blok'] = substr($res, 10, 3);
        $variable['no_urut'] = substr($res, 13, 4);
        $variable['kd_jns_op'] = substr($res, 17, 1);
        $data = $this->sqlDataPembayaran($variable)->get();


        // dd($data);
        $nop = formatnop($res);
        $pdf = PDF::loadView('tracking_pembayaran/_cetak_pdf', compact('data', 'nop'))->setPaper('a4', 'landscape');
        $filename = 'riwayat pembayaran  ' . $res . '.pdf';
        return $pdf->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
