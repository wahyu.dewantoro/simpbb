<?php

namespace App\Http\Controllers;

use App\Models\SuratTugas;
use App\Models\SuratTugasObjek;
use App\Models\SuratTugasPegawai;
use App\pegawaiStruktural;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\DataTables;
use SnappyPdf as PDF;

class suratTugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = SuratTugas::wherenull('deleted_at')->orderbyraw('created_at desc');
            return  Datatables::of($data)
                ->addColumn('tanggal_peneitian', function ($st) {
                    $tanggal = tglIndo($st->tanggal_mulai);
                    $tanggalb = tglIndo($st->tanggal_selesai);
                    $res = $tanggal;
                    if ($tanggal <> $tanggalb) {
                        if (date('m', strtotime($tanggal)) <> date('m', strtotime($tanggalb))) {
                            $res = $tanggal . ' - ' . $tanggalb;
                        } else {
                            $res = date('d', strtotime($tanggal)) . ' - ' . $tanggalb;
                        }
                    }
                    return $res;
                })
                ->addColumn('pegawai', function ($data) {
                    $res = $data->pegawai()->get()->pluck('nama')->toarray();
                    return implode(', ', $res);
                })
                ->addColumn('status', function ($data) {
                    switch ($data->kd_status) {
                        case '1':
                            # code...
                            $status = 'Di Setujui';
                            break;
                        case '0':
                            # code...
                            $status = 'Di Tolak';
                            break;
                        default:
                            # code...
                            $status = 'Proses verifikasi';
                            break;
                    }
                    return $status;
                })
                ->addColumn('aksi', function ($data) {
                    $aksi = "";
                    if ($data->kd_status == '') {
                        $aksi .= "<a  href='" . route('penelitian.surat-tugas.edit', $data->id) . "' ><i class='text-success fas fa-edit'></i></a> ";
                        $aksi .= "<a class='hapus'  role='button' tabindex='0' data-id='" . $data->id . "'><i class='text-danger fas fa-trash-alt'></i></a> ";
                    }


                    if ($data->kd_status == '1') {
                        $aksi .= "<a target='_blank' href='" . url('penelitian/surat-tugas-cetak') . "?id=" . $data->id . "'><i class='text-warning far fa-file-pdf'></i></a>";
                    }


                    return $aksi;
                })->rawColumns(['aksi', 'tanggal_peneitian', 'pegawai', 'status'])
                ->make(true);
        }
        $tab = $request->tab ?? 'list';
        $verifikasi = 0;
        if (Auth()->user()->hasRole('Super User') || Auth()->user()->hasRole('Kepala Bidang')) {
            $verifikasi = 1;
        }
        return view('surattugas.index', compact('tab', 'verifikasi'));
    }

    public function verifikasiSuratTugas(Request $request)
    {
        $data = SuratTugas::wherenull('deleted_at')->wherenull('verifikasi_at');
        return  Datatables::of($data)
            ->addColumn('tanggal_peneitian', function ($st) {
                $tanggal = tglIndo($st->tanggal_mulai);
                $tanggalb = tglIndo($st->tanggal_selesai);
                $res = $tanggal;
                if ($tanggal <> $tanggalb) {
                    if (date('m', strtotime($tanggal)) <> date('m', strtotime($tanggalb))) {
                        $res = $tanggal . ' - ' . $tanggalb;
                    } else {
                        $res = date('d', strtotime($tanggal)) . ' - ' . $tanggalb;
                    }
                }
                return $res;
            })
            ->addColumn('pegawai', function ($data) {
                $res = $data->pegawai()->get()->pluck('nama')->toarray();
                return implode(', ', $res);
            })
            ->addColumn('status', function ($data) {
                switch ($data->kd_status) {
                    case '1':
                        # code...
                        $status = 'Di Setujui';
                        break;
                    case '0':
                        # code...
                        $status = 'Di Tolak';
                        break;
                    default:
                        # code...
                        $status = 'Proses verifikasi';
                        break;
                }
                return $status;
            })
            ->addColumn('aksi', function ($data) {
                $aksi = "";
                $aksi .= "<a class='show-verifikasi'  role='button' tabindex='0' data-id='" . $data->id . "'><i class='text-info fas fa-binoculars'></i></a>";
                return $aksi;
            })->rawColumns(['aksi', 'tanggal_peneitian', 'pegawai', 'status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function verifikasiSuratTugasShow(Request $request)
    {
        $id = $request->id;
        $data = SuratTugas::findorfail($id);
        return view('surattugas._verifikasi', compact('data'));
        // return 
    }



    public function create()
    {

        $staff = DB::table(db::raw('users a'))
            ->join(db::raw('model_has_roles b'), 'b.model_id', '=', 'a.id')
            ->join(db::raw('roles c'), 'c.id', '=', 'b.role_id')
            ->whereraw("lower(c.name) like '%staff%'")
            ->select(DB::raw('a.id,upper(a.nama) nama '))->orderby('nama')->pluck('nama', 'id')->toArray();

        $data = [
            'action' => url('penelitian/surat-tugas'),
            'method' => 'post',
            'staff' => $staff
        ];
        return view('surattugas.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->list_nop);
        DB::beginTransaction();
        try {
            $objek = [];
            $lokasi = [];
            $robj = explode(',', $request->list_nop);
            foreach ($robj as $obj) {
                $i = explode('#', $obj);
                // dd($i[4]);
                $res = str_replace('.', '', $i[1]);
                $kd_propinsi = substr($res, 0, 2);
                $kd_dati2 = substr($res, 2, 2);
                $kd_kecamatan = substr($res, 4, 3);
                $kd_kelurahan = substr($res, 7, 3);
                $kd_blok = substr($res, 10, 3);
                $no_urut = substr($res, 13, 4);
                $kd_jns_op = substr($res, 17, 1);


                $objek[] = [
                    'layanan_objek_id' => $i[0],
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'alamat' => $i[2],
                    'nm_kelurahan' => $i[3],
                    'nm_kecamatan' => $i[4],
                ];
                $lokasi[] = $i[4];
            }

            $petugas = [];
            for ($i = 0; $i < $request->jml; $i++) {

                $en = explode('_', $request->nama[$i]);

                $petugas[] = [
                    'users_id' => $en[0],
                    'nama' => $en[1],
                    'nip' => $request->nip[$i],
                    'jabatan' => $request->jabatan[$i]
                ];
            }

            $peg = pegawaiStruktural::where('kode', '02')->first();
            $lokasi = array_unique($lokasi);
            // dd($lokasi);
            $surat = [
                'tanggal_mulai' => new Carbon($request->tanggal_mulai),
                'tanggal_selesai' => new Carbon($request->tanggal_selesai),
                'lokasi' => implode(', ', $lokasi),
                'surat_kota' => 'KEPANJEN',
                'surat_pegawai' => $peg->nama,
                'surat_nip' => $peg->nip,
                'surat_jabatan' => $peg->jabatan,
                'surat_pangkat' => 'Pembina',
                'surat_tanggal' => Carbon::now()
            ];

            $st = SuratTugas::create($surat);
            // pegawai
            foreach ($petugas as $rpetugas) {
                $rpetugas['surat_tugas_id'] = $st->id;

                SuratTugasPegawai::create($rpetugas);
            }

            foreach ($objek as $robjek) {
                $robjek['surat_tugas_id'] = $st->id;
                SuratTugasObjek::create($robjek);
            }

            DB::commit();
            $flash = [
                'success' => 'Berhasil di simpan'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(url('penelitian/surat-tugas'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $surat = SuratTugas::findorfail($id);
        $staff = DB::table(db::raw('users a'))
            ->join(db::raw('model_has_roles b'), 'b.model_id', '=', 'a.id')
            ->join(db::raw('roles c'), 'c.id', '=', 'b.role_id')
            ->whereraw("lower(c.name) like '%staff%'")
            ->select(DB::raw('a.id,upper(a.nama) nama '))->orderby('nama')->pluck('nama', 'id')->toArray();

        $data = [
            'action' => route('penelitian.surat-tugas.update', $id),
            'method' => 'patch',
            'staff' => $staff,
            'surat' => $surat
        ];
        return view('surattugas.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            //code...
            // looping objek
            $obj = explode(',', $request->list_nop);
            $objek = [];
            $lokasi = [];
            foreach ($obj as $obj) {
                $i = explode('#', $obj);
                $res = str_replace('.', '', $i[1]);
                $kd_propinsi = substr($res, 0, 2);
                $kd_dati2 = substr($res, 2, 2);
                $kd_kecamatan = substr($res, 4, 3);
                $kd_kelurahan = substr($res, 7, 3);
                $kd_blok = substr($res, 10, 3);
                $no_urut = substr($res, 13, 4);
                $kd_jns_op = substr($res, 17, 1);

                $lokasi[] = $i[4];
                $objek[] = [
                    'layanan_objek_id' => $i[0],
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'alamat' => $i[2],
                    'nm_kelurahan' => $i[3],
                    'nm_kecamatan' => $i[4],
                ];
            }

            $petugas = [];
            for ($i = 0; $i < $request->jml; $i++) {

                $en = explode('_', $request->nama[$i]);

                $petugas[] = [
                    'users_id' => $en[0],
                    'nama' => $en[1],
                    'nip' => $request->nip[$i],
                    'jabatan' => $request->jabatan[$i]
                ];
            }

            $peg = pegawaiStruktural::where('kode', '02')->first();
            $lokasi = array_unique($lokasi);
            $surat = [
                'tanggal_mulai' => new Carbon($request->tanggal_mulai),
                'tanggal_selesai' => new Carbon($request->tanggal_selesai),
                'lokasi' => implode(', ', $lokasi),
                'surat_kota' => 'KEPANJEN',
                'surat_pegawai' => $peg->nama,
                'surat_nip' => $peg->nip,
                'surat_jabatan' => $peg->jabatan,
                'surat_pangkat' => 'Pembina',
                'surat_tanggal' => Carbon::now()
            ];


            $st = SuratTugas::find($id)->update($surat);

            // $st = SuratTugas::create($surat);

            SuratTugasPegawai::where('surat_tugas_id', $id)->delete();

            foreach ($petugas as $rpetugas) {
                $rpetugas['surat_tugas_id'] = $id;

                SuratTugasPegawai::create($rpetugas);
            }

            SuratTugasObjek::where('surat_tugas_id', $id)->delete();
            foreach ($objek as $robjek) {
                $robjek['surat_tugas_id'] = $id;
                SuratTugasObjek::create($robjek);
            }

            DB::commit();
            $flash = [
                'success' => 'Berhasil di update'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(url('penelitian/surat-tugas'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // return $id;
        DB::beginTransaction();
        try {
            //code...
            SuratTugasObjek::where('surat_tugas_id', $id)->delete();
            SuratTugasPegawai::where('surat_tugas_id', $id)->delete();
            SuratTugas::where('id', $id)->delete();
            DB::commit();
            $flash = [
                'status' => 1,
                'msg' => 'Data berhasil di hapus'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            db::rollBack();
            $flash = [
                'status' => 0,
                'msg' => 'Data gagal di hapus'
            ];
        }

        return $flash;
        // return redirct(url('penelitian/surat-tugas'))->with($flash);
    }

    public function cetakSuratTugas(Request $request)
    {
        $id = $request->id ?? null;
        $st = SuratTugas::find($id);
        if ($st) {
            // dd($st->pegawai()->get());
            //  return view('surattugas._pdf',compact('st'));
            $pages = [];
            $pages[] = View::make('surattugas._pdf', compact('st'));
            $pdf = pdf::loadHTML($pages)
                ->setPaper('A4')
                ->setOption('margin-left', '19')
                ->setOption('margin-right', '15')
                ->setOption('margin-top', '12.5')
                ->setOption('margin-bottom', '20');
            $filename = 'surat tugas batch' . $st->batch . '.pdf';
            $pdf->setOption('enable-local-file-access', true);
            return $pdf->stream();
        }

        abort(404);
    }

    public function verifikasiSuratTugasPost(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->id;
            SuratTugas::find($id)->update([
                'verifikasi_at' => Carbon::now(),
                'verifikasi_by' => auth()->user()->id,
                'surat_tanggal' => Carbon::now(),
                'kd_status' => $request->kd_status,
                'nomor_surat' => getNoSuratTugas()
            ]);
            DB::commit();
            $flash = [
                'success' => 'Berhasil di verifikasi'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => 'Gagal di verifikasi'
            ];
        }
        return redirect(url('penelitian/surat-tugas') . '?tab=verifikasi')->with($flash);
    }
}
