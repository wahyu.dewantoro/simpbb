<?php

namespace App\Http\Controllers;

use App\Jobs\PelimpahanBayarJob;
use App\Models\Data_billing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UnflagController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $nop = $request->nop;
            $kobil = onlyNumber($nop);
            $kd_propinsi = substr($kobil, 0, 2);
            $kd_dati2 = substr($kobil, 2, 2);
            $kd_kecamatan = substr($kobil, 4, 3);
            $kd_kelurahan = substr($kobil, 7, 3);
            $kd_blok = substr($kobil, 10, 3);
            $no_urut = substr($kobil, 13, 4);
            $kd_jns_op = substr($kobil, 17, 1);
            $tahun = $request->tahun;

            $tanggal_bayar=$request->tanggal_bayar;

            if ($request->jenis == 'inquiry') {

                $data = 0;
                $status = 0;
                $pokok = '';
                $denda = '';


                // jika kobil cek ke data_billing
                if ($kd_blok == '999') {
                    $bill = Data_billing::whereraw("kobil ='$kobil' and tahun_pajak='$tahun'")->selectraw("kd_status,expired_at, case when expired_at <sysdate then 1  else 0 end  is_expired")->first();
                    if ($bill) {
                        if ($bill->kd_status == 0 && $bill->is_expired == 1) {
                            Data_billing::whereraw("kobil ='$kobil' ")
                                ->update(
                                    ['expired_at' => Carbon::now()->addDays(2)]
                                );
                        }
                    }
                }

                $usr = explode('|', $request->kd_bank);
                $username = $usr[0];
                $password = $usr[1];
                $response = Http::withBasicAuth($username, $password)
                    ->post(config('app.service_inquiry'), [
                        "Nop" => $kobil,
                        "MasaPajak" => $tahun,
                        "DateTime" => date('Y-m-d H:i:s',strtotime($tanggal_bayar)),
                        "Merchant" => "6010",
                        "KodeInstitusi" => "001011",
                        "NoHp" => null,
                        "Email" => null
                    ]);
                log::info("FLAG:".$response);
                // dd($response);
                if ($response['Status']['IsError'] == 'False') {
                    foreach ($response['Tagihan'] as $tg) {
                        if ($tg['Tahun'] == $tahun) {
                            $data = 1;
                            $status = 1;
                            $pokok = $tg['Pokok'];
                            $denda = $tg['Denda'];
                        }
                    }
                }

                return [
                    'pokok' => (int) $pokok,
                    'denda' => (int) $denda,
                    'data' => $data,
                    'status' => $status
                ];
            }

            if ($request->jenis == 'reversal') {

                $data = 0;
                $status = 0;
                $pokok = '';
                $denda = '';

                $res = DB::connection('oracle_satutujuh')->select(DB::raw("select jml_sppt_yg_dibayar - denda_sppt pokok , 
                                            denda_sppt denda
                                            from pembayaran_sppt where thn_pajak_sppt='$tahun' and kd_kecamatan='$kd_kecamatan' 
                    and kd_kelurahan='$kd_kelurahan' and kd_blok='$kd_blok' 
                    and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'"));

                if ($res) {
                    $data = 1;
                    $pokok = $res[0]->pokok;
                    $denda = $res[0]->denda;
                    $status = 1;
                }

                return [
                    'pokok' => (int) $pokok,
                    'denda' => (int) $denda,
                    'data' => (int)$data,
                    'status' => (int)$status
                ];
            }
        }

        
        $tp = [];
        $akunws = DB::connection('oracle_spo')->table("ws_user")->selectraw("username,password_md5")->whereraw("id<>22")->get();
        $tab = $request->tab ?? 'flag';
        return view('unflag/index', compact('tp', 'tab', 'akunws'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $tipe = $request->tipe;

        if ($tipe == 1) {
            $add = "?tab=flag";
        } else {
            $add = "?tab=unflag";
        }

        $nop = $request->nop;
        $kobil = onlyNumber($nop);

        $usr = explode('|', $request->kd_bank);
        $username = $usr[0];
        $password = $usr[1];


        if ($tipe == 1) {
            // flagging
            $response = Http::withBasicAuth($username, $password)
                ->post(
                    config('app.service_payment'),
                    [
                        "Nop" => $kobil,
                        "Merchant" => '6010',
                        "DateTime" => date('Y-m-d H:i:s', strtotime($request->tanggal_bayar . ' ' . date('H:i:s'))),
                        "Reference" => '907402',
                        "TotalBayar" => $request->pokok + $request->denda,
                        "KodeInstitusi" => '001011',
                        "NoHp" => null,
                        "Email" => null,
                        "Tagihan" => [
                            ["Tahun" => $request->thn_pajak]
                        ]
                    ]
                );
            

            // pelimpahan_bayar
            $param['nop'] = $kobil;
            $param['tahun'] = $request->thn_pajak;
            $param['jenis'] = 1;
            dispatch(new PelimpahanBayarJob($param))->onQueue('rekon');
            
            return  redirect(url('realisasi/unflag') . $add)->with(['success' => $response['Status']['ErrorDesc']]);
        }


        if ($tipe == 0) {

          
            $response = Http::withBasicAuth($username, $password)
                ->post(
                    config('app.service_reversal'),
                    [
                        "Nop" => $kobil,
                        "DateTime" => date('Y-m-d H:i:s'),
                        "Reference" => '907402',
                        "Tagihan" => [
                            ["Tahun" => $request->thn_pajak]
                        ]
                    ]
                );

            $param['nop'] = $kobil;
            $param['tahun'] = $request->thn_pajak;
            $param['jenis'] = 2;
            dispatch(new PelimpahanBayarJob($param))->onQueue('rekon');

            return  redirect(url('realisasi/unflag') . $add)->with(['success' => $response['Status']['ErrorDesc']]);
        }

        die();

        DB::beginTransaction();
        try {
            //code...
            $tipe = $request->tipe;
            $exp = explode('.', $request->nop);
            if ($tipe == 1) {
                // flagging
                $nop = $request->nop;
                $kobil = onlyNumber($nop);
                $tahun = $request->thn_pajak;

                $usr = explode('|', $request->kd_bank);
                $username = $usr[0];
                $password = $usr[1];


                $response = Http::withBasicAuth($username, $password)
                    ->post(
                        config('app.service_payment'),
                        [
                            "Nop" => $kobil,
                            "Merchant" => '6010',
                            "DateTime" => date('Y-m-d H:i:s', strtotime($request->tanggal_bayar . ' ' . date('H:i:s'))),
                            "Reference" => '907402',
                            "TotalBayar" => $request->pokok + $request->denda,
                            "KodeInstitusi" => '001011',
                            "NoHp" => null,
                            "Email" => null,
                            "Tagihan" => [
                                ['Tahun' => $request->thn_pajak]
                            ]
                        ]
                    );

               
            }

            if ($tipe == 0) {
                $byr = [
                    'kd_propinsi' => $exp[0],
                    'kd_dati2' => $exp[1],
                    'kd_kecamatan' => $exp[2],
                    'kd_kelurahan' => $exp[3],
                    'kd_blok' => $exp[4],
                    'no_urut' => $exp[5],
                    'kd_jns_op' => $exp[6],
                    'thn_pajak_sppt' => $request->thn_pajak
                ];
                DB::connection('oracle_satutujuh')->table('pembayaran_sppt')
                    ->whereraw("kd_kecamatan='" . $byr['kd_kecamatan'] . "' and kd_kelurahan='" . $byr['kd_kelurahan'] . "'
                         and kd_blok='" . $byr['kd_blok'] . "' and no_urut='" . $byr['no_urut'] . "' and kd_jns_op='" . $byr['kd_jns_op'] . "' and thn_pajak_sppt='" . $byr['thn_pajak_sppt'] . "'")
                    ->delete();

                DB::connection('oracle_satutujuh')->table('sppt')
                    ->whereraw("kd_kecamatan='" . $byr['kd_kecamatan'] . "' and kd_kelurahan='" . $byr['kd_kelurahan'] . "'
                         and kd_blok='" . $byr['kd_blok'] . "' and no_urut='" . $byr['no_urut'] . "' and kd_jns_op='" . $byr['kd_jns_op'] . "' and thn_pajak_sppt='" . $byr['thn_pajak_sppt'] . "'")
                    ->update(['status_pembayaran_sppt' => '0']);

                DB::connection('oracle_spo')->table('pembayaran_sppt')
                    ->whereraw("kd_kecamatan='" . $byr['kd_kecamatan'] . "' and kd_kelurahan='" . $byr['kd_kelurahan'] . "'
                         and kd_blok='" . $byr['kd_blok'] . "' and no_urut='" . $byr['no_urut'] . "' and kd_jns_op='" . $byr['kd_jns_op'] . "' and thn_pajak_sppt='" . $byr['thn_pajak_sppt'] . "'")
                    ->delete();

                DB::commit();
                $flash = [
                    'success'  => 'Berhasil unflag NOP ' . $request->nop
                ];
            }
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error'  => 'gagal flag/unflag NOP ' . $request->nop
            ];
        }

        if ($tipe == 1) {
            $add = "?tab=flag";
        } else {
            $add = "?tab=unflag";
        }

        return  redirect(url('realisasi/unflag') . $add)->with($flash);
    }

    
}
