<?php

namespace App\Http\Controllers;

use App\Models\DbkbJpbLimasBelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbJpbLimaBelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbJpbLimasBelas::where('thn_dbkb_jpb15', $tahun)->orderbyraw("jns_tangki_dbkb_jpb15 desc,kapasitas_min_dbkb_jpb15 asc")->get();
            $read = 1;
            return view('dbkb.jepebelimabelas._index', compact('data', 'read'));
        }
        return view('dbkb.jepebelimabelas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbJpbLimasBelas::where('thn_dbkb_jpb15', $tahun)->orderbyraw("jns_tangki_dbkb_jpb15 desc,kapasitas_min_dbkb_jpb15 asc")->get();
            if ($data->count() == 0) {
                $data = DbkbJpbLimasBelas::where('thn_dbkb_jpb15', $tahun - 1)->orderbyraw("jns_tangki_dbkb_jpb15 desc,kapasitas_min_dbkb_jpb15 asc")->get();
            }
            $read = 0;
            return view('dbkb.jepebelimabelas._index', compact('data', 'read'));
        }
        return view('dbkb/jepebelimabelas/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request->all();
        //
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            //code...
            $tahun = $request->thn_dbkb_jpb15;
            DbkbJpbLimasBelas::where('thn_dbkb_jpb15', $tahun)->delete();
            $data = [];
            foreach ($request->jns_tangki_dbkb_jpb15 as $i => $itm) {
                $data[] = [
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'thn_dbkb_jpb15' => $tahun,
                    'jns_tangki_dbkb_jpb15' => $request->jns_tangki_dbkb_jpb15[$i],
                    'kapasitas_min_dbkb_jpb15' => $request->kapasitas_min_dbkb_jpb15[$i],
                    'kapasitas_max_dbkb_jpb15' => $request->kapasitas_max_dbkb_jpb15[$i],
                    'nilai_dbkb_jpb15' => is_null($request->nilai_dbkb_jpb15[$i]) == '1' ? 0 : $request->nilai_dbkb_jpb15[$i] / 1000,
                ];
                
            }
            DbkbJpbLimasBelas::insert($data);
            // return $data;
            // die();

            $flash = ['success' => 'Berhasil memproses DBKB JPB 15 pada tahun ' . $tahun];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.jepebe-lima-belas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
