<?php

namespace App\Http\Controllers;

use App\Helpers\gallade;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\Layanan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class VerifikasiLayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if($kecamatan->count()=='1'){
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start = $tahun - 5;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }
        return view('layanan.verifikasi-layanan',compact('kecamatan','kelurahan','tahun_pajak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $search=$request->only(['status','jenis_objek','tgl']);
        $select=['nomor_layanan', 'jenis_layanan_nama', 'nama', 'keterangan', 'created_at', 'kd_status','jenis_objek'];
        $layanan=Layanan::select($select);
        $layanan->whereIn('jenis_layanan_id',['3','8']);
        $dataSet=$layanan->orderBy('Layanan.created_at', 'desc')->get();

        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
                $btnDetail = gallade::buttonInfo('<i class="fas fa-search"></i>', 'title="Detail Layanan Input" data-detail="' . $data->nomor_layanan. '"');
                $btnPdf = gallade::anchorInfo('laporan_input/cetak_pdf?nomor_layanan=' . $data->nomor_layanan, '<i class="fas fa-file-pdf"></i>', 'title="Cetak Pdf" data-pdf="true"');
                return '<div class="btn-group">' .$btnDetail. $btnPdf."</div>";
            })
            ->editColumn('nomor_layanan',function($data){
                $listObjek=['1'=>'Pribadi','2'=>'Badan','3'=>'Kolektif'];
                $jenisObjek=(isset($listObjek[$data->jenis_objek]))?$listObjek[$data->jenis_objek]:'--';
                return '<p class="p-0 m-0">'.$data->nomor_layanan.'</p><i class="text-info">'.$jenisObjek.'</i>';
            })
            ->editColumn('created_at',function($data){
                $his=date('H:i:s', strtotime($data->created_at));
                return '<p class="p-0 m-0">'.tglIndo($data->created_at).'</p><i>'.$his.'</i>';
            })
            ->editColumn('kd_status',function($data){
                return ($data->kd_status) ? "<span class='badge badge-success '>Selesai</span>" : "<span class='badge badge-warning '>Proses</span>";
            })
            ->addIndexColumn()   
            ->removeColumn(['penagihan_id'])
            ->make(true);
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
