<?php

namespace App\Http\Controllers;

use App\Helpers\Layanan_conf;
use App\Http\Requests\sknjopRequest;
use App\Jobs\BatchSknjop;
use App\Jobs\genareteSkNJOP;
// use App\Jobs\verifikasiSKNJOP;
use App\pegawaiStruktural;
use App\Services\CreateSknjopService;
use App\SkNJop;
use App\skNjopDokumen;
use App\sknjoppermohonan;
use Carbon\Carbon;
// use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Ramsey\Uuid\Uuid;
use SnappyPdf as PDF;
use Illuminate\Support\Facades\Log;

class sknjopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* $user=Auth()->user();
        $res=$user->hasRole('Wajib Pajak');
        dd($res); */

        $sknjop = sknjoppermohonan::with('nop.dokumen')->wepe()->wherenull('deleted_at')->pencarian()->orderby('created_at', 'desc')->paginate(10);
        return view('sknjop.index', compact('sknjop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = [
            'action' => route('sknjop.store'),
            'method' => 'post',
        ];
        return view('sknjop.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function storeKolektif(Request $request)
    {
        // dd($request->all());
        $peg = pegawaiStruktural::where('kode', '02')->first();
        DB::beginTransaction();
        try {
            //code...
            $nomor_layanan = Layanan_conf::nomor_layanan();
            $permohonan['nik_pemohon'] = $request->nik_pemohon_kolektif;
            $permohonan['nomor_hp'] = $request->nomor_hp_kolektif;
            $permohonan['nama_pemohon'] = html_entity_decode($request->nama_pemohon_kolektif);

            $permohonan['alamat_pemohon'] = $request->alamat_pemohon_kolektif;
            $permohonan['kelurahan_pemohon'] = $request->kelurahan_pemohon_kolektif;
            $permohonan['kecamatan_pemohon'] = $request->kecamatan_pemohon_kolektif;
            $permohonan['dati2_pemohon'] = $request->dati2_pemohon_kolektif;
            $permohonan['propinsi_pemohon'] = $request->propinsi_pemohon_kolektif;
            $permohonan['nomor_layanan'] = $nomor_layanan;
            // $permohonan['kd_status'] = 1;
            $permohonan['verifikasi_at'] = Carbon::now();
            $permohonan['verifikasi_by'] = Auth()->user()->id;
            $permohonan = sknjoppermohonan::create($permohonan);

            $batch = [];
            $nomersk = getNoSkNjop();
            $exp = explode('/', $nomersk);
            $str = $exp[0];
            foreach ($request->nop_kolektif as $i => $row) {
                $nop = $request->nop_kolektif[$i];
                $res = str_replace('.', '', $nop);
                $kd_propinsi = substr($res, 0, 2);
                $kd_dati2 = substr($res, 2, 2);
                $kd_kecamatan = substr($res, 4, 3);
                $kd_kelurahan = substr($res, 7, 3);
                $kd_blok = substr($res, 10, 3);
                $no_urut = substr($res, 13, 4);
                $kd_jns_op = substr($res, 17, 1);

                $urut = sprintf("%04s", abs($str + $i));
                $nomer_jadi = $urut . '/' . $exp[1] . '/' . $exp[2] . '/' . $exp[3] . '/' . $exp[4];

                $batch[] = [
                    'id' => Uuid::uuid1()->toString(),
                    'created_at' => Carbon::now(),
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => Carbon::now(),
                    'sknjop_permohonan_id' => $permohonan->id,
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'alamat_op' => $request->alamat_op_kolektif[$i],
                    'nama_wp' => html_entity_decode($request->nama_wp_kolektif[$i]),
                    'alamat_wp' => $request->alamat_wp_kolektif[$i],
                    'luas_bumi' => $request->luas_bumi_kolektif[$i],
                    'njop_bumi' => $request->njop_bumi_kolektif[$i],
                    'luas_bng' => $request->luas_bng_kolektif[$i],
                    'njop_bng' => $request->njop_bng_kolektif[$i],
                    'njop_pbb' => $request->njop_pbb_kolektif[$i],
                    'sknjop_permohonan_id' => $permohonan->id,
                    'nomer_sk' => $nomer_jadi,
                    'tanggal_sk' => Carbon::now(),
                    'kd_status' => $request->status_cetak[$i],
                    'verifikasi_at' => Carbon::now(),
                    'verifikasi_by' => Auth()->user()->id,
                    'nama_kabid' => $peg->nama,
                    'nip_kabid' => $peg->nip
                ];
            }
            SkNJop::insert($batch);
            DB::commit();
            dispatch(new BatchSknjop($permohonan->id))->onQueue('sknjop');
            $pesan = ['success' => 'Permohonan SK NJOP telah di submit'];
            $redirect = url('sknjop') . '/' . $permohonan->id;
        } catch (\Throwable $th) {
            //throw $th;

            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
            $redirect = url('sknjop');
        }

        return redirect($redirect)->with($pesan);
    }


    public function store(sknjopRequest $request, CreateSknjopService $service)
    {

        

        $nop = $request->nop;
        $res = str_replace('.', '', $nop);
        $kd_propinsi = substr($res, 0, 2);
        $kd_dati2 = substr($res, 2, 2);
        $kd_kecamatan = substr($res, 4, 3);
        $kd_kelurahan = substr($res, 7, 3);
        $kd_blok = substr($res, 10, 3);
        $no_urut = substr($res, 13, 4);
        $kd_jns_op = substr($res, 17, 1);

        $now = Carbon::now();
        $userid = Auth()->user()->id;
        $peg = pegawaiStruktural::where('kode', '02')->first();
        $nomor_layanan = Layanan_conf::nomor_layanan();
        $permohonan = $request->only(['nik_pemohon', 'nomor_hp', 'nama_pemohon', 'alamat_pemohon', 'kelurahan_pemohon', 'kecamatan_pemohon', 'dati2_pemohon', 'propinsi_pemohon']);
        $permohonan['nama_pemohon'] = html_entity_decode($request->nama_pemohon);
        $permohonan['nomor_layanan'] = $nomor_layanan;
        // $permohonan['kd_status'] = '1';
        $permohonan['verifikasi_at'] = $now;
        $permohonan['verifikasi_by'] = $userid;

        $data = $request->only(['alamat_op', 'alamat_wp', 'luas_bumi', 'luas_bng', 'njop_bumi', 'njop_bng', 'njop_pbb']);



        $data['nama_wp'] = html_entity_decode($request->nama_wp);
        $data['kd_propinsi'] = $kd_propinsi;
        $data['kd_dati2'] = $kd_dati2;
        $data['kd_kecamatan'] = $kd_kecamatan;
        $data['kd_kelurahan'] = $kd_kelurahan;
        $data['kd_blok'] = $kd_blok;
        $data['no_urut'] = $no_urut;
        $data['kd_jns_op'] = $kd_jns_op;
        $data['nomer_sk'] = getNoSkNjop();
        $data['tanggal_sk'] = $now;
        $data['kd_status'] = '1';
        $data['verifikasi_at'] = $now;
        $data['verifikasi_by'] = $userid;
        // nama ttd
        $data['nama_kabid'] = $peg->nama;
        $data['nip_kabid'] = $peg->nip;

        $parameter= [
            'permohonan' => $permohonan,
            'data' => $data
        ];
        
        $post = $service->handle($parameter);
        
        $pesan = [$post['type'] => $post['desc']];
        $redirect = $post['url'];
        return redirect($redirect)->with($pesan);
        //  return redirect()->back()->withSuccess($message);

/* 
        die();

        DB::beginTransaction();
        try {

            $permohonan = sknjoppermohonan::create($permohonan);
            $data['sknjop_permohonan_id'] = $permohonan->id;
            SkNJop::create($data);
            DB::commit();

            dispatch(new BatchSknjop($permohonan->id))->onQueue('sknjop');
            $pesan = ['success' => 'Permohonan SK NJOP telah di submit'];
            $redirect = url('sknjop') . '/' . $permohonan->id;
        } catch (\Exception $th) {


            DB::rollback();
            Log::alert($th);
            $pesan = ['error' => $th->getMessage()];
            $redirect = url('sknjop');
        }

        return redirect($redirect)->with($pesan); */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permohonan = sknjoppermohonan::with('nop')->findorfail($id);
        return view('sknjop.show', compact('permohonan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sknjop = SkNJop::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            $sknjop->update(['deleted_at' => Carbon::now(), 'deleted_by' => Auth()->user()->id]);
            DB::commit();
            $pesan = ['success' => 'Permohonan SK NJOP telah di hapus'];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['danger' => $th->getMessage()];
        }

        return redirect(url('sknjop'))->with($pesan);
    }

    public function generatePdf($id)
    {

        $sk = SkNJop::with(['dokumen'])->findOrFail($id);
        if (count((array)$sk->dokumen) == 0) {
            return $this->renderPdf($id);
        } else {
            $filename = $sk->dokumen->path;
            $disk = $sk->dokumen->disk;
            if (!Storage::disk($disk)->exists($filename)) {
                return $this->renderPdf($id);
            }
            $file = Storage::disk($disk)->get($filename);
            $type = Storage::disk($disk)->mimeType($filename);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        }
    }


    public function renderPdf($id)
    {
        $sknjop = SkNJop::findorfail($id);
        $url = config('app.url') . 'sknjop-pdf/' . $id;
        $pages = View::make('sknjop/_pdf', compact('sknjop', 'url'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $path = 'sk' . time() . '.pdf';
        $pdf->setOption('enable-local-file-access', true);
        $disk = Storage::disk('sknjop');
        if ($disk->put($path, $pdf->output())) {
            $file = [
                'sknjop_id' => $id,
                'disk' => 'sknjop',
                'path' => $path,
                'filename' => str_replace('/', ' ', $sknjop->nomer_sk)
            ];
            $res = skNjopDokumen::create($file);
        }

        return $pdf->stream();
    }

    public function previewCetak($id)
    {
        $filepdf = url('sknjop-pdf') . '/' . $id;

        // return $this->tandaterima($id);
        return view('sknjop.previewcetak', compact('filepdf'));
    }

    public function tandaterima($id)
    {
        $permohonan = sknjoppermohonan::findorfail($id);
        // return view('sknjop.tandaterima', compact('permohonan'));
        $pages = View::make('sknjop.tandaterima', compact('permohonan'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $path = 'tanda terima ' . $permohonan->nomor_layanan . '.pdf';

        $pdf->setOption('enable-local-file-access', true);

        return $pdf->stream();
    }
    public function tandaterimaPreview($id)
    {
        return $this->tandaterima($id);
        /* $title = 'Cetak Tanda Terima';
        $filepdf = url('sknjop-tanda-terima') . '/' . $id;
        return view('sknjop.previewcetak', compact('filepdf', 'title')); */
    }

    public function batchRender($id)
    {

        // dd($id);
        // return 'Render sknjop';
        $filename = $id . '.pdf';
        $disk = 'sknjop';
        if (!Storage::disk($disk)->exists($filename)) {
            $nop = SkNJop::whereraw("sknjop_permohonan_id='" . $id . "'")->get();
            $disk = Storage::disk('sknjop');
            $mp = array();
            foreach ($nop as $sknjop) {
                $url = config('app.url') . 'sknjop-pdf/' . $sknjop->id;
                $pages = View::make('sknjop/_pdf', compact('sknjop', 'url'));
                $mp[] = $pages;
                $pdf = pdf::loadHTML($pages)
                    ->setPaper('A4');
                $pdf->setOption('enable-local-file-access', true);
            }

            if (!empty($mp)) {
                $pdfmulti = pdf::loadHTML($mp);
                $disk->put($filename, $pdfmulti->output());

                // return $pdfmulti->stream();

                $file = Storage::disk('sknjop')->get($filename);
                $type = Storage::disk('sknjop')->mimeType($filename);
                $response = Response::make($file, 200);
                $response->header("Content-Type", $type);
                return $response;
            } else {
                abort(404, 'Dokumen tidak ditemukan');
            }
        } else {
            $file = Storage::disk($disk)->get($filename);
            $type = Storage::disk($disk)->mimeType($filename);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        }
    }

    public function batchPreview($id)
    {
        return $this->batchRender($id);
    }
}
