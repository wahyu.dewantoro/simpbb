<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Otp;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class OTPVerificationController extends Controller
{
    public function showVerificationForm($id)
    {
        // return $id = decrypt($id);
        return view('auth.otp_verify', compact('id'));
    }

    public function verifyOtp(Request $request)
    {
       
        $request->validate([
            'user_id' => 'required',
            // 'otp_code' => 'required',
            'input1' => 'required',
            'input2' => 'required',
            'input3' => 'required',
            'input4' => 'required',
            'input5' => 'required',
            'input6' => 'required',
        ]);

        // return $user_id = decrypt($request->user_id);

        $kode_otp = "";
        $kode_otp .= $request->input1;
        $kode_otp .= $request->input2;
        $kode_otp .= $request->input3;
        $kode_otp .= $request->input4;
        $kode_otp .= $request->input5;
        $kode_otp .= $request->input6;

        // return $kode_otp;


        $otp = Otp::where('user_id', decrypt($request->user_id))
            ->where('otp_code', $kode_otp)
            ->first();

        if (!$otp) {
            return back()->withErrors(['error' => 'Invalid OTP.']);
        }

        if (Carbon::now()->greaterThan($otp->expires_at)) {
            // return back()->withErrors(['error' => 'OTP has expired.']);
            $status = false;
        } else {
            // return $otp;
            $user = User::find($otp->user_id);
            $user->update(['email_verified_at' => now()]);
            $status = true;
        }

        $otp->delete(); // Hapus OTP setelah validasi berhasil

        // return $status;

        if ($status == true) {
            // Auth::l($user);
            Auth::login($user);

            $redirect=url('users',$user->id);
            // return redirect('home');
            return redirect($redirect)->with('success', 'OTP validated successfully.');
        } else {
            return redirect(url('login'))->withErrors(['otp_code' => 'OTP has expired.']);
        }
        // return redirect()->route('login')->with('success', 'OTP validated successfully. You can now log in.');
    }
}
