<?php

namespace App\Http\Controllers\Auth;

use App\Dokumen;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrasiUserRequest;
use App\Kecamatan;
use App\Kelurahan;
use App\Otp;
use App\Providers\RouteServiceProvider;
use App\Role;
use App\Services\SendOtpOca;
use App\User;
use App\UserRayon;
use App\UsersDua;
use App\UsulanPengelola;
use Carbon\Carbon;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $SendOtpOca;
    public function __construct(SendOtpOca $SendOtpOca)
    {
        $this->middleware('guest');
        $this->SendOtpOca = $SendOtpOca;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    public function showRegistrationForm()
    {
        $roles = Role::whereraw("name in ('DEVELOPER/PENGEMBANG','Rayon','Wajib Pajak')")
            ->orderby('id')->pluck('name')->toarray();
        $kecamatan = Kecamatan::orderby('kd_kecamatan')->pluck('nm_kecamatan', 'kd_kecamatan')->toarray();
        $kelurahan = Kelurahan::pluck('nm_kelurahan')->toarray();
        $ac_kecamatan = [];
        foreach ($kecamatan as $i) {
            $ac_kecamatan[] = $i;
        }
        $ar_kel =  array_unique($kelurahan);
        $ac_kelurahan = [];
        foreach ($ar_kel as $i) {
            $ac_kelurahan[] = $i;
        }
        $ac_kelurahan = json_encode($ac_kelurahan);
        $ac_kecamatan = json_encode($ac_kecamatan);
        return view('register', compact('roles', 'kecamatan', 'ac_kecamatan', 'ac_kelurahan'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama' => 'bail|required|min:2',
            'nik' => 'bail|required|numeric|digits:16',
            'telepon' => 'bail|required|min:10|numeric',
            'email' => 'required|email',
            'username' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            "nik" => date('Ymdhis'),
            "nama" => $data['nama'],
            "telepon" => $data['telepon'],
            "email" => $data['email'],
            "username" => $data['username'],
            "password" => bcrypt($data['password']),
            "update_profile" => Carbon::now()
        ]);
    }

    public function verifOtp(Request $request) {}

    public function register(RegistrasiUserRequest $request)
    {

        $disk = 'public';
        $file_ktp = $request->file('file_ktp');
        $filename_ktp = time() . '_ktp.' . $file_ktp->getClientOriginalExtension();

        $file_selfie_ktp = $request->file('file_selfi');
        $filename_selfie_ktp = time() . '_selfie_ktp.' . $file_selfie_ktp->getClientOriginalExtension();



        DB::beginTransaction();
        try {
            $exp = explode(' ', $request->nama);
            $phone_number = $request->telepon;
            $data = [
                "nik" => $request->nik,
                "nama" => $request->nama,
                "username" => $request->username,
                "telepon" => $phone_number,
                "email" => $request->email,
                "nama_pendek" => $exp[0] ?? $request->nama,
                "password" => bcrypt($request->password),
                "alamat_user" => $request->alamat_user,
                "kelurahan_user" => $request->kelurahan_user,
                "kecamatan_user" => $request->kecamatan_user,
                "dati2_user" => $request->dati2_user,
                "propinsi_user" => $request->propinsi_user,
            ];
            $user = UsersDua::create($data);

            if (Storage::disk($disk)->exists($filename_ktp)) {
                Storage::disk($disk)->delete($filename_ktp);
            }
            $upload_file_ktp = Storage::disk($disk);
            if ($upload_file_ktp->put($filename_ktp, File::get($file_ktp))) {
                $url_file_ktp = Storage::disk($disk)->url($filename_ktp);
                $url_file_ktp = str_replace(config('app.url'), '', $url_file_ktp);
                Dokumen::create([
                    'tablename' => 'users',
                    'keyid' => $user->id,
                    'filename' => $filename_ktp,
                    'disk' => $disk,
                    'nama_dokumen' => 'scan KTP',
                    'keterangan' => 'scan KTP',
                    'url' => $url_file_ktp
                ]);
            }


            if (Storage::disk($disk)->exists($filename_selfie_ktp)) {
                Storage::disk($disk)->delete($filename_selfie_ktp);
            }
            $upload_file_selfie_ktp = Storage::disk($disk);
            if ($upload_file_selfie_ktp->put($filename_selfie_ktp, File::get($file_selfie_ktp))) {
                $url_file_selfie_ktp = Storage::disk($disk)->url($filename_selfie_ktp);
                $url_file_selfie_ktp = str_replace(config('app.url'), '', $url_file_selfie_ktp);
                Dokumen::create([
                    'tablename' => 'users',
                    'keyid' => $user->id,
                    'filename' => $filename_selfie_ktp,
                    'disk' => $disk,
                    'nama_dokumen' => 'scan selfie KTP',
                    'keterangan' => 'scan selfie KTP',
                    'url' => $url_file_selfie_ktp
                ]);
            }

            // if rayon
            if ($request->role == 'Rayon') {
                $rayon = [
                    'users_id' => $user->id,
                    'kd_kecamatan' => $request->kd_kecamatan,
                    'kd_kelurahan' => $request->kd_kelurahan,
                    'no_surat' => $request->no_surat,
                    'tgl_surat' => new Carbon($request->tgl_surat)
                ];
                $ur = UserRayon::create($rayon);
                $table_name = "user_rayon";
                $table_id = $ur->id;
            }



            // if developer
            if ($request->role == 'DEVELOPER/PENGEMBANG') {
                $dataDeveloper = [
                    'user_id' => $user->id,
                    'nama_pengembang' => $request->nama_pengembang,
                    'no_surat' => $request->no_surat,
                    'tgl_surat' => new Carbon($request->tgl_surat)
                ];
                $up = UsulanPengelola::create($dataDeveloper);
                $table_name = "user_pengelola";
                $table_id = $up->id;
            }


            if ($request->file('file_rekom') && ($request->role == 'DEVELOPER/PENGEMBANG' || $request->role == 'Rayon')) {
                // file_rekom
                $file_rekom = $request->file('file_rekom');
                $filename_rekom = time() . '_rekom.' . $file_rekom->getClientOriginalExtension();

                if (Storage::disk($disk)->exists($filename_rekom)) {
                    Storage::disk($disk)->delete($filename_rekom);
                }
                $upload_file_rekom = Storage::disk($disk);
                if ($upload_file_rekom->put($filename_rekom, File::get($file_rekom))) {
                    $url_file_rekom = Storage::disk($disk)->url($filename_rekom);
                    $url_file_rekom = str_replace(config('app.url'), '', $url_file_rekom);
                    Dokumen::create([
                        'tablename' => $table_name,
                        'keyid' => $table_id,
                        'filename' => $filename_rekom,
                        'disk' => $disk,
                        'nama_dokumen' => 'Dokumen Pendukung',
                        'keterangan' => 'Dokumen Pendukung',
                        'url' => $url_file_rekom
                    ]);
                }
            }
            $usd = User::find($user->id);
            $usd->assignRole($request->role);
            $otp = rand(100000, 999999);

            Otp::create([
                'user_id' => $user->id,
                'otp_code' => $otp,
                'expires_at' => Carbon::now()->addMinutes(10),
            ]);

            // $phone_number
            $phoneNumber = $phone_number;
            $message = $otp;
            $this->SendOtpOca->sendMessage($phoneNumber, $message);

            DB::commit();
            // $this->guard()->login($user);
            $id = encrypt($user->id);
            $redirectTo = route('otp.verify.form', $id);

            $flash = [
                'success' => 'Akun berhasil di registrasi !'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            Log::error($th);
            // dd($th);
            $flash = ['error' => "Gagal melakukan registrasi"];
            $redirectTo = url('register');
        }
        return redirect($redirectTo)->with($flash);
    }
}
