<?php

namespace App\Http\Controllers;

use App\Exports\DaftarSK;
use App\Helpers\Lhp;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\Layanan_objek;
use App\Models\SuratKeputusan;
use App\Models\SuratKeputusanLampiran;
use App\pegawaiStruktural;
use App\Sppt;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use DOMPDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yajra\DataTables\DataTables;

class SuratKeputusanController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = Lhp::listSk();
            $jenis_sk = $request->jenis_sk ?? '-';
            $kecamatan = $request->kecamatan ?? '-';
            $kelurahan = $request->kelurahan ?? '-';

            if ($jenis_sk != "-") {
                $data->where(['a.jenis_layanan_id' => $jenis_sk]);
            }

            if ($kecamatan != "-") {
                $data->where(['b.kd_kecamatan' => $kecamatan]);
            }
            if ($kelurahan != "-") {
                $data->where(['b.kd_kelurahan' => $kelurahan]);
            }


            $tanggal_mulai = date('Ymd', strtotime($request->tanggal_mulai));
            $tanggal_akhir = date('Ymd', strtotime($request->tanggal_akhir));
            $data->whereraw("trunc(a.created_at) between  to_date('$tanggal_mulai','yyyymmdd') and to_date('$tanggal_akhir','yyyymmdd') and (kd_status='1' or nomor_nota is null) ");


            return  Datatables::of($data)
                ->filterColumn('nop', function ($query, $keyword) {
                    $keyword = str_replace('-', '', str_replace('.', '', $keyword));
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("a.nop_proses like '%$keyword%'"));
                })
                ->filterColumn('nomorlayanan', function ($query, $keyword) {
                    $keyword = str_replace('-', '', str_replace('.', '', $keyword));
                    $keyword = strtolower($keyword);
                    $query->whereRaw(DB::raw("b.nomor_layanan like '%$keyword%'"));
                })
                // 
                ->addColumn('pilih', function ($row) {
                    $aksi =  ' <a data-toggle="tooltip" data-placement="top" title="Surat Keputusan" class="cetak-sk" role="button"  data-url="' . url('sk', acak($row->layanan_objek_id)) . '" data-id="' . $row->layanan_objek_id . '" tabindex="0"><i class="text-success fa-2x fas fa-file-alt"></i></a> ';
                    if ($row->jenis_layanan_id == '1') {
                        $tahun = substr($row->nomor_layanan, 0, 4);
                        $var = onlyNumber($row->nop_proses) . $tahun;
                        $url = url('informasi/sppt/cetak', $var);
                        $aksi .=  ' <a data-toggle="tooltip"  class="cetak-sk" role="button"  data-url="' . $url . '" data-id="' . $row->layanan_objek_id . '" tabindex="0" data-placement="top" title="SPPT" ><i class="text-warning fa-2x far fa-file-pdf"></i></a> ';
                    }


                    return $aksi;
                })
                ->addColumn('nopproses', function ($row) {
                    return formatnop($row->nop_proses);
                })
                ->addColumn('noformulir', function ($row) {
                    return formatNomor($row->no_formulir);
                })
                ->addColumn('nomorlayanan', function ($row) {
                    return formatNomor($row->nomor_layanan);
                })
                ->rawColumns(['pilih', 'noformulir', 'nomorlayanan', 'nopproses'])->make(true);
        }
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $nomor_layanan = $request->only('nomor_layanan');
        $Jenis_layanan = Jenis_layanan::all();
        return view('suratkeputusan.index', compact('nomor_layanan', 'kecamatan', 'kelurahan', 'Jenis_layanan'));
    }

    public function cetakSK($ide)
    {
        $url =   config('app.url') . '/sk/' . $ide;
        $id = rapikan($ide);
        // dd($id);
        $ct = Layanan_objek::find($id);
        if ($ct->is_tolak == '1' && $ct->layanan->jenis_layanan_id == '4') {

            $tanggal = new Carbon($ct->pemutakhiran_at);
            // cek sk existing
            $sk = SuratKeputusan::where('layanan_objek_id', $id)->first();


            $tahun = $tanggal->year;
            // return $ct->pemutakhiran_at->is_format('yyyy');
            $kd_propinsi = $ct->kd_propinsi;
            $kd_dati2 = $ct->kd_dati2;
            $kd_kecamatan = $ct->kd_kecamatan;
            $kd_kelurahan = $ct->kd_kelurahan;
            $kd_blok = $ct->kd_blok;
            $no_urut = $ct->no_urut;
            $kd_jns_op = $ct->kd_jns_op;

            $sppt = Sppt::where(
                [
                    'thn_pajak_sppt' => $tahun,
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                ]
            )->first();


            $alamat_op = $ct->alamat_op;
            if ($ct->rt_op != '') {
                $alamat_op .= 'RT ' . $ct->rt_op;
            }

            if ($ct->rw_op != '') {
                $alamat_op .= 'RW ' . $ct->rw_op;
            }

            $dm = DB::connection("oracle_satutujuh")->table("ref_kelurahan")
                ->join("ref_kecamatan", 'ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                ->selectraw("nm_kecamatan,nm_kelurahan")
                ->whereraw("ref_kelurahan.kd_kecamatan='" . $kd_kecamatan . "' and ref_kelurahan.kd_kelurahan='" . $kd_kelurahan . "'")
                ->first();

            $alamat_op .= $dm->nm_kelurahan . ' ' . $dm->nm_kecamatan;
            $alamat_wp = '';

            if ($sppt->jln_wp_sppt != '') {
                $alamat_wp .= " " . $sppt->jln_wp_sppt;
            }
            if ($sppt->blok_kav_wp_sppt != '') {
                $alamat_wp .= " " . $sppt->blok_kav_wp_sppt;
            }
            if ($sppt->rt_wp_sppt != '') {
                $alamat_wp .= " RT " . $sppt->rt_wp_sppt;
            }
            if ($sppt->rw_wp_sppt != '') {
                $alamat_wp .= " RW " . $sppt->rw_wp_sppt;
            }
            if ($sppt->kelurahan_wp_sppt != '') {
                $alamat_wp .= "<br> " . $sppt->kelurahan_wp_sppt;
            }
            if ($sppt->kota_wp_sppt != '') {
                $alamat_wp .= "<br> " . $sppt->kota_wp_sppt;
            }


            if ($sk == null) {
                $kaban = pegawaiStruktural::whereraw("kode='01'")->first();
                $jabatan = ($kaban->is_plt != '' ? 'PLT ' : '') . $kaban->jabatan;
                $jenis = 'Keberatan.PBB';

                $sk = SuratKeputusan::create(
                    [
                        'layanan_objek_id' => $id,
                        'jenis' => $jenis,
                        'kota' => 'KEPANJEN',
                        'pegawai' => $kaban->nama,
                        'nip' => $kaban->nip,
                        'jabatan' => $jabatan,
                        'pangkat' => 'Pembina Utama Muda',
                        'tanggal' => $tanggal
                    ]
                );


                SuratKeputusanLampiran::create([
                    'surat_keputusan_id' => $sk->id,
                    'is_last' => null,
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'alamat_op' => $alamat_op,
                    'nm_wp' => $sppt->nm_wp_sppt,
                    'alamat_wp' => $alamat_wp ?? '-',
                    'luas_bumi' => $sppt->luas_bumi_sppt,
                    'njop_bumi' =>  $sppt->njop_bumi_sppt / $sppt->luas_bumi_sppt,
                    'luas_bng' => $sppt->luas_bng_sppt,
                    'njop_bng' => $sppt->luas_bng_sppt > 0 ? ($sppt->njop_bng_sppt / $sppt->luas_bng_sppt)  : 0,
                ]);
            }

            $sk_id = $sk->id;
            $sk = SuratKeputusan::find($sk_id);
            $pages = view('suratkeputusan/penolakan', compact('sk', 'url', 'sppt', 'alamat_wp', 'alamat_op'));
            // return $pages;


            $basename = 'Surat keputusan keberatan ' . $sk->LayananObjek->nomor_layanan;
            $filename = $basename . '.pdf';
            $pdf = SnappyPdf::loadHTML($pages)
                ->setOption('enable-local-file-access', true)

                ->setPaper('A4');
            return $pdf->stream($filename);
        }

        $datask = Lhp::getSk($id);

        $sk = $datask['sk'];
        // dd($sk);
        if ($datask['response']['is_error'] == true) {
            abort(500);
        }

        $jenis_layanan_id = $sk->TblSpop->jenis_layanan_id;
        if (in_array($jenis_layanan_id, [8, 10, 9,  3,  6, 7])) {
            // pemutkhiran
            $jenis_sk = '1';
        } else if (in_array($jenis_layanan_id, [4])) {
            // keberatan
            $jenis_sk = '4';
        } else if (in_array($jenis_layanan_id, [1])) {
            // objek baru
            $jenis_sk = '3';
        } else if (in_array($jenis_layanan_id, [5])) {
            // pengurangan
            $jenis_sk = '2';
        } else {
            // pembatalan
            $jenis_sk = '';
        }


        if ($jenis_sk == '1') {
            if ($jenis_layanan_id <> '7') {

                $last = $sk->last()->first();
                // dd($sk->hasil());
                $pages = view('suratkeputusan/pembetulan', compact('sk', 'url', 'last'));
            } else {
                $last = $sk->last()->get();
                $pages = view('suratkeputusan/pembetulan_gabung', compact('sk', 'url', 'last'));
            }
        } else if ($jenis_sk == '4') {
            $last = $sk->last()->first();
            $pages = view('suratkeputusan/keberatan', compact('sk', 'url', 'last'));
        } else if ($jenis_sk == '3') {
            $last = $sk->last()->first();
            $pages = view('suratkeputusan/data_baru', compact('sk', 'url', 'last'));
        } else if ($jenis_sk == '2') {
            $tahun = date('Y', strtotime($sk->tanggal));
            // dd($sk);
            $row = $sk->TblSpop->nop_proses;
            $kd_propinsi = substr($row, 0, 2);
            $kd_dati2 = substr($row, 2, 2);
            $kd_kecamatan = substr($row, 4, 3);
            $kd_kelurahan = substr($row, 7, 3);
            $kd_blok = substr($row, 10, 3);
            $no_urut = substr($row, 13, 4);
            $kd_jns_op = substr($row, 17, 1);

            $spo = DB::connection('oracle_satutujuh')->table(db::raw("sppt a"))
                ->leftjoin(db::raw("sppt_potongan_detail b"), function ($join) {
                    $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                        ->on('a.kd_dati2', '=', 'b.kd_dati2')
                        ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                        ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                        ->on('a.kd_blok', '=', 'b.kd_blok')
                        ->on('a.no_urut', '=', 'b.no_urut')
                        ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                        ->on('a.thn_pajak_sppt', '=', 'b.thn_pajak_sppt')
                        ->on('b.jns_potongan', '=', db::raw('2'));
                })
                ->leftjoin(db::raw("sppt_potongan_detail c"), function ($join) {
                    $join->on('a.kd_propinsi', '=', 'c.kd_propinsi')
                        ->on('a.kd_dati2', '=', 'c.kd_dati2')
                        ->on('a.kd_kecamatan', '=', 'c.kd_kecamatan')
                        ->on('a.kd_kelurahan', '=', 'c.kd_kelurahan')
                        ->on('a.kd_blok', '=', 'c.kd_blok')
                        ->on('a.no_urut', '=', 'c.no_urut')
                        ->on('a.kd_jns_op', '=', 'c.kd_jns_op')
                        ->on('a.thn_pajak_sppt', '=', 'c.thn_pajak_sppt')
                        ->on('c.jns_potongan', '=', db::raw('1'));
                })
                ->select(db::raw("a.nm_wp_sppt,pbb_yg_harus_dibayar_sppt ,c.nilai_potongan insentif,b.nilai_potongan, b.persentase"))
                ->whereraw("a.thn_pajak_sppt='$tahun' and a.kd_propinsi='$kd_propinsi' and
                a.kd_dati2='$kd_dati2' and
                a.kd_kecamatan='$kd_kecamatan' and
                a.kd_kelurahan='$kd_kelurahan' and
                a.kd_blok='$kd_blok' and
                a.no_urut='$no_urut' and
                a.kd_jns_op='$kd_jns_op'")->first();
            // return $spo;
            // dd($spo);
            $pages = view('suratkeputusan/pengurangan', compact('sk', 'url', 'spo'));
        } else {

            $tahun = date('Y', strtotime($sk->tanggal));
            // dd($sk);
            $row = $sk->TblSpop->nop_proses;
            $kd_propinsi = substr($row, 0, 2);
            $kd_dati2 = substr($row, 2, 2);
            $kd_kecamatan = substr($row, 4, 3);
            $kd_kelurahan = substr($row, 7, 3);
            $kd_blok = substr($row, 10, 3);
            $no_urut = substr($row, 13, 4);
            $kd_jns_op = substr($row, 17, 1);


            $rk = Layanan_objek::select('keterangan_pembatalan')->find($sk->layanan_objek_id);
            $keterangan = "Pembatalan objek";
            if ($rk) {
                $keterangan = $rk->keterangan_pembatalan;
            }

            $pages = view('suratkeputusan/pembatalan', compact('sk', 'url', 'keterangan'));
        }


        $basename = 'Surat keputusan' . time();
        $filename = $basename . '.pdf';
        $pdf = SnappyPdf::loadHTML($pages)
            ->setOption('enable-local-file-access', true)

            ->setPaper('A4');
        return $pdf->stream($filename);
    }

    public function cetak(Request $request)
    {
        try {
            $data = Lhp::listSk();
            $jenis_sk = $request->jenis_sk ?? '-';
            if ($jenis_sk == 'null') {
                $jenis_sk = '-';
            }
            $kecamatan = $request->kecamatan ?? '-';
            if ($kecamatan == 'null') {
                $kecamatan = '-';
            }
            $kelurahan = $request->kelurahan ?? '-';
            if ($kelurahan == 'null') {
                $kelurahan = '-';
            }

            if ($jenis_sk != "-") {
                $data = $data->where(['a.jenis_layanan_id' => $jenis_sk]);
            }

            if ($kecamatan != "-") {
                $data = $data->where(['b.kd_kecamatan' => $kecamatan]);
            }
            if ($kelurahan != "-") {
                $data = $data->where(['b.kd_kelurahan' => $kelurahan]);
            }

            // dd($data->tosql());

            $tanggal_mulai = date('Ymd', strtotime($request->tanggal_mulai));
            $tanggal_akhir = date('Ymd', strtotime($request->tanggal_akhir));
            // dd("trunc(a.created_at) between  to_date('$tanggal_mulai','yyyymmdd') and to_date('$tanggal_akhir','yyyymmdd') ");

            $data->whereraw("trunc(a.created_at) between  to_date('$tanggal_mulai','yyyymmdd') and to_date('$tanggal_akhir','yyyymmdd') ");

            $kota_wp = "Semua Kecamatan";
            $kelurahan_wp = "Semua Kelurahan";
            if ($kecamatan != '-' || $kelurahan != '-') {
                $getkelkecOP = DB::connection('oracle_dua')->table('ref_kecamatan')
                    ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                    ->leftJoin(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) use ($kelurahan) {
                        $join->On('ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                            ->where(['ref_kelurahan.kd_kelurahan' => $kelurahan]);
                    })
                    ->where(function ($q) use ($kecamatan) {
                        $q->where(['ref_kecamatan.kd_kecamatan' => $kecamatan]);
                    })
                    ->get()->first();
                $kelurahan_wp = $getkelkecOP->nm_kelurahan ?? "Semua Kelurahan";
                $kota_wp = $getkelkecOP->nm_kecamatan ?? "Semua Kecamatan";
            }
            $export = new DaftarSK($data->get()->toArray(), [
                'kota_wp' => $kota_wp,
                'kelurahan_wp' => $kelurahan_wp,
            ]);
            return Excel::download($export, 'daftar_SK.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }

    public function ContohSkTte() {}
}
