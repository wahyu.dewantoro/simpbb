<?php

namespace App\Http\Controllers;

use App\Models\Billing_kolektif;
use App\Models\Data_billing;
use App\Services\PaymentService;
// use App\Qris;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class TagihanSpptController extends Controller
{
    //
    protected $PaymentService;

    public function __construct(PaymentService $PaymentService)
    {
        $this->PaymentService = $PaymentService;
    }

    public function historyTagihan()
    {
        // return 'data';
        $user_id = Auth()->user()->id;
        $data = Data_billing::wherein('kd_jns_op', [6, 7])->where("created_by", $user_id)
            ->get();

        $response = [];
        foreach ($data as $row) {
            $response[] = [
                'kobil' => $row->kobil,
                'tahun_pajak' => $row->tahun_pajak,
                'nm_wp' => $row->nm_wp,
                'pokok' => $row->PokokNominal,
                'denda' => $row->DendaNominal,
                'total' => $row->TotalNominal,
                'status' => $row->PaymentStatus,
                'jenis' => $row->JenisBilling
            ];
        }

        return response()->json($response);
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $nop = onlyNumber($request->nop);
            $tanggal = Carbon::now()->format('Y-m-d H:i:s');
            $param = [
                'Nop' => $nop,
                'MasaPajak' => date('Y'),
                'DateTime' => $tanggal,
                'Merchant' => '6010',
                'KodeInstitusi' => '001011',
                'NoHp' => '08123456789',
                'Email' => 'abc@mail.com'
            ];

            $body = $this->PaymentService->Inquiry($param);
            // return $response;

            $hasil = [
                'Nop' => $body['data']['Nop'] ?? "",
                'Nama' => $body['data']['Nama'] ?? "",
                'Tagihan' => $body['data']['Tagihan'] ?? [],
                'Status' => $body['data']['Status'] ?? "",
                // 'nomor_va' => 'asd'
            ];
            // return $hasil;

            // return $param;
            return view('tagihan-sppt._index', compact('hasil'));
        }

        $user_id = Auth()->user()->id;
        $datas = Data_billing::wherein('kd_jns_op', [6, 7])->where("created_by", $user_id)
            ->latest()
            ->get();

        // return $datas;
        $data = [];
        foreach ($datas as $row) {
            $ad = $row->billing_kolektif()->first();
            $nop = "";
            $nop .= $ad->kd_propinsi;
            $nop .= $ad->kd_dati2;
            $nop .= $ad->kd_kecamatan;
            $nop .= $ad->kd_kelurahan;
            $nop .= $ad->kd_blok;
            $nop .= $ad->no_urut;
            $nop .= $ad->kd_jns_op;


            $data[] = [
                'data_billing_id' => $row->data_billing_id,
                'kobil' => $row->kobil,
                'nop' => formatnop($nop),
                'tahun_pajak' => $row->tahun_pajak,
                'nm_wp' => $row->nama_wp,
                'pokok' => $row->PokokNominal,
                'denda' => $row->DendaNominal,
                'total' => $row->TotalNominal,
                'status' => $row->PaymentStatus,
                'jenis' => $row->JenisBilling,
                'qr_value' => encrypt($row->qris_value),
                'expired_at' => $row->expired_at,
                'nomor_va' => $row->VirtualAccount->nomor_va ?? '-',
                'bill_number' => $row->Qris->bill_number ?? "-"
            ];
        }


        // dd($data);
        return view('tagihan-sppt.index', compact('data'));
    }

    public function registerPembayaran(Request $request)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            //code...

            $nop = $request->nop[0];
            $kd_propinsi = substr($nop, 0, 2);
            $kd_dati2 = substr($nop, 2, 2);
            $kd_kecamatan = substr($nop, 4, 3);
            $kd_kelurahan = substr($nop, 7, 3);
            $kd_jns_op = $request->metode == '1' ? '6' : '7';
            $tahun = max($request->tahun);
            $kd_blok = '999';
            $noUrutAkhir = Data_billing::whereraw("tahun_pajak='$tahun' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_jns_op='$kd_jns_op'")
                ->max('no_urut');

            $no_urut = sprintf("%04s", abs($noUrutAkhir + 1));
            $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
            $rk = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
                ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                ->selectraw("nm_kecamatan,nm_kelurahan")
                ->whereraw("ref_kelurahan.kd_kelurahan='$kd_kelurahan' and ref_kecamatan.kd_kecamatan='$kd_kecamatan'")
                ->first();

            $nama_wp = $request->nama;
            if ($kd_jns_op == '6') {
                // va
                $ta = date('Y-m-d') . ' 23:59:00';
                $expired_at = new Carbon($ta);
            } else {
                // qris
                $expc = Carbon::now()->addMinutes(30);
                $expired_at = $expc->format('Y-m-d H:i:s');
            }
            $dataBilling = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'tahun_pajak' => $tahun,
                'nama_wp' => $nama_wp,
                'nama_kelurahan' => $rk->nm_kelurahan,
                'nama_kecamatan' => $rk->nm_kecamatan,
                'kobil' => $kobil,
                'expired_at' => $expired_at
            ];

            $billing = Data_billing::create($dataBilling);
            $data_billing_id = $billing->data_billing_id;


            // UPDATE EXPIRED
            Data_billing::where('data_billing_id', $data_billing_id)->update(['expired_at' => $expired_at]);

            $item = [];
            $totalTagihan = 0;
            foreach ($request->gettahun as $k => $i) {
                $nop_proses = $request->nop[0];

                $kd_propinsi = substr($nop_proses, 0, 2);
                $kd_dati2 = substr($nop_proses, 2, 2);
                $kd_kecamatan = substr($nop_proses, 4, 3);
                $kd_kelurahan = substr($nop_proses, 7, 3);
                $kd_blok = substr($nop_proses, 10, 3);
                $no_urut = substr($nop_proses, 13, 4);
                $kd_jns_op = substr($nop_proses, 17, 1);

                $item[] = [
                    'data_billing_id' => $data_billing_id,
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'tahun_pajak' => $i,
                    'pbb' => $request->pokok[$i],
                    'nm_wp' => $nama_wp,
                    'pokok' => $request->pokok[$i],
                    'denda' => $request->denda[$i],
                    'total' => $request->total[$i],
                    'expired_at' => $expired_at
                ];

                $totalTagihan += $request->total[$i];
            }
            // dd($item);
            Billing_kolektif::insert($item);
            if ($request->metode == '1') {
                // virtual
                $posva['data_billing_id'] = $data_billing_id;
                $posva['nama_wp'] = $billing['nama_wp'];
                $expired = carbon::now();
                $expired = $expired->format('Ymd');
                $posva['total'] = $totalTagihan;
                $posva['TanggalExp'] = $expired;
                $posva['berita'] = implode(', ', $request->tahun);
                $va = $this->postingVa($posva);

                if ($va['code'] == '00') {
                    DB::commit();
                    // $va['url'] = url('virtual-account', $data_billing_id);
                } else {
                    DB::rollBack();
                    // $va['url'] = url('/');
                }
            } else {
                $bilnumber = $billing->kobil . $billing->tahun_pajak;
                $hck = hash('sha256', config('qris.merchantpan') . $bilnumber . config('qris.terminaluser') . config('qris.merchanthashkey'));
                $qris = [
                    'merchantPan' => config('qris.merchantpan'),
                    'hashcodeKey' =>  $hck,
                    'billNumber' => $bilnumber,
                    'purposetrx' => 'Pembayaran PBB',
                    'storelabel' => config('qris.storelabel'),
                    'customerlabel' => config('qris.customerlabel'),
                    'terminalUser' => config('qris.terminaluser'),
                    'expiredDate' => $expired_at,
                    'amount' => $totalTagihan,
                    'data_billing_id' => $data_billing_id
                ];
                // dd();
                $va = $this->postingQris($qris);

                if ($va['code'] == '00') {
                    DB::commit();

                    $va['url'] = url('qris', $data_billing_id);
                } else {
                    DB::rollBack();
                    $va['url'] = url('/');
                }
            }


            $desc = "Telah di generete, silahkan melakukan pembayaran";
            $tipe = "success";
            // DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::rollBack();
            $desc = $th->getMessage();
            $tipe = "success";
            $data_billing_id = $th->getMessage();
        }


        return redirect(route('tagihan-sppt.index'))->with([$tipe => $desc]);

        /*     
        DB::beginTransaction();
        try {
            $n_total = 0;
            foreach ($request->total as $nt) {
                $n_total += $nt;
            }

            // return $n_total;
            // die();

            $nop = $request->nop[0];
            $kd_propinsi = substr($nop, 0, 2);
            $kd_dati2 = substr($nop, 2, 2);
            $kd_kecamatan = substr($nop, 4, 3);
            $kd_kelurahan = substr($nop, 7, 3);
            $kd_jns_op = $n_total <= 10000000 ? '7' : '6';
            $tahun = date('Y');
            $kd_blok = '999';
            $noUrutAkhir = DB::table("data_billing")
                ->whereraw("tahun_pajak='$tahun' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_jns_op='$kd_jns_op'")
                ->max('no_urut');

            $no_urut = sprintf("%04s", abs($noUrutAkhir + 1));
            $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
            $rk = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
                ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                ->selectraw("nm_kecamatan,nm_kelurahan")
                ->whereraw("ref_kelurahan.kd_kelurahan='$kd_kelurahan' and ref_kecamatan.kd_kecamatan='$kd_kecamatan'")
                ->first();

            $nama_wp = $request->nm_wp[0];
            if ($kd_jns_op == '6') {
                // va
                $ta = date('Y-m-d') . ' 23:59:00';
                $expired_at = new Carbon($ta);
            } else {
                // qris
                $expc = Carbon::now()->addMinutes(30);
                $expired_at = $expc->format('Y-m-d H:i:s');
            }

            $dataBilling = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'tahun_pajak' => $tahun,
                'nama_wp' => $nama_wp,
                'nama_kelurahan' => $rk->nm_kelurahan,
                'nama_kecamatan' => $rk->nm_kecamatan,
                'kobil' => $kobil,
                'expired_at' => $expired_at
            ];

            // return $dataBilling;
            //code...

            $billing = Data_billing::create($dataBilling);

            $data_billing_id = $billing->data_billing_id;

            // UPDATE EXPIRED
            Data_billing::where('data_billing_id', $data_billing_id)->update(['expired_at' => $expired_at]);

            $item = [];
            $totalTagihan = 0;
            foreach ($request->tahun_pajak as  $i => $k) {
                $nop_proses = $request->nop[0];

                $kd_propinsi = substr($nop_proses, 0, 2);
                $kd_dati2 = substr($nop_proses, 2, 2);
                $kd_kecamatan = substr($nop_proses, 4, 3);
                $kd_kelurahan = substr($nop_proses, 7, 3);
                $kd_blok = substr($nop_proses, 10, 3);
                $no_urut = substr($nop_proses, 13, 4);
                $kd_jns_op = substr($nop_proses, 17, 1);

                $item[] = [
                    'data_billing_id' => $data_billing_id,
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'tahun_pajak' => $k,
                    'pbb' => $request->pokok[$i],
                    'nm_wp' => $nama_wp,
                    'pokok' => $request->pokok[$i],
                    'denda' => $request->denda[$i],
                    'total' => $request->total[$i],
                    'expired_at' => $expired_at
                ];

                $totalTagihan += $request->total[$i];
            }
            // dd($item);
            Billing_kolektif::insert($item);

            if ($request->metode == '1') {
                // virtual
                $posva['data_billing_id'] = $data_billing_id;
                $posva['nama_wp'] = $billing['nama_wp'];
                $expired = carbon::now();
                $expired = $expired->format('Ymd');
                $posva['total'] = $totalTagihan;
                $posva['TanggalExp'] = $expired;
                $posva['berita'] = implode(', ', $request->tahun_pajak);
                $va = $this->postingVa($posva);

                if ($va['code'] == '00') {
                    DB::commit();
                    $va['url'] = url('virtual-account', $data_billing_id);
                } else {
                    DB::rollBack();
                    $va['url'] = url('/');
                }
            } else {
                $bilnumber = $billing->kobil . $billing->tahun_pajak;
                $hck = hash('sha256', config('qris.merchantpan') . $bilnumber . config('qris.terminaluser') . config('qris.merchanthashkey'));
                $qris = [
                    'merchantPan' => config('qris.merchantpan'),
                    'hashcodeKey' =>  $hck,
                    'billNumber' => $bilnumber,
                    'purposetrx' => 'Pembayaran PBB',
                    'storelabel' => config('qris.storelabel') ?? '-',
                    'customerlabel' => config('qris.customerlabel') ?? '-',
                    'terminalUser' => config('qris.terminaluser') ?? '-',
                    'expiredDate' => $expired_at,
                    'amount' => $totalTagihan,
                    'data_billing_id' => $data_billing_id
                ];
                // dd();
                $va = $this->postingQris($qris);

                if ($va['code'] == '00') {
                    DB::commit();

                    $va['url'] = url('qris', $data_billing_id);
                } else {
                    DB::rollBack();
                    $va['url'] = url('/');
                }
            }
            DB::commit();
            $desc = "Telah di generete, silahkan melakukan pembayaran";
            $tipe = "success";
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            // dd($th);
            $va['desc'] = $th->getMessage();
            $va['code'] = '99';
            $va['url'] = url('/');
            DB::rollBack();
            $desc = $th->getMessage();
            $tipe = "success";
            $data_billing_id = $th->getMessage();
        } */

        // return $data_billing_id;

        return redirect(route('tagihan-sppt.index'))->with([$tipe => $desc]);
    }

    private function nomorVa()
    {
        $prefix_a = config('virtual.prefix');
        $prefix_b = config('virtual.tipe');
        $prefix_c = date('y');

        $cari = $prefix_a . $prefix_b . $prefix_c;
        $getlast = DB::table(db::raw("virtual_account"))
            ->selectraw("nvl(max(cast( substr(nomor_va,11,3) as number)),0) bundel,nvl(max( cast(substr(nomor_va,14,3) as number)),0) urut")
            ->whereraw("nomor_va like '" . $cari . "%' 
                 and cast( substr(nomor_va,11,3) as number) in (select cast( substr(nomor_va,11,3) as number) from  virtual_account
                    where nomor_va like '" . $cari . "%')
            ")->first();
        $urt = (int)($getlast->urut ?? 0) + 1;


        $cd = $getlast->bundel == 0 ? 1 : $getlast->bundel;
        $bdl = (int)$cd;

        $prefix = $prefix_a . $prefix_b . $prefix_c;
        $bundle = $bdl; // Bundle awal
        $counter = $urt; // Counter awal

        // Generate nomor registrasi pertama
        return  generateRegistrationNumber($prefix, $bundle, $counter);


        /*    if ($urt > 999) {
            $bdl = $bdl + 1;
            $urt = 001;
        }

        if ($bdl > 999) {
            $prefix_c = $prefix_c + 1;
            $urt = 001;
            $bdl = 001;
        }

        $urut = sprintf("%03d", ($urt));
        $bundel = sprintf("%03d", ($bdl));
        $prefix_c = sprintf("%02d", ($prefix_c));
        return $prefix_a . $prefix_b . $prefix_c . $bundel . $urut; */
    }


    public function postingVa($data)
    {
        $client = new Client();
        $posting = [
            'VirtualAccount' => $this->nomorVa(),
            'Nama' => $data['nama_wp'],
            'TotalTagihan' => $data['total'],
            'TanggalExp' => $data['TanggalExp'],
            'Berita1' =>  substr($data['berita'], 0, 30) != false ? (substr($data['berita'], 0, 30)) : '',
            'Berita2' => substr($data['berita'], 30, 30) != false ? (substr($data['berita'], 30, 30)) : '',
            'Berita3' =>  substr($data['berita'], 60, 30) != false ? (substr($data['berita'], 60, 30)) : '',
            'Berita4' =>  substr($data['berita'], 90, 30) != false ? (substr($data['berita'], 90, 30)) : '',
            'Berita5' =>  substr($data['berita'], 120, 30) != false ? (substr($data['berita'], 120, 30)) : '',
            'FlagProses' => '1',
        ];
        // dd($posting);

        // return $posting;

        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->post(config('virtual.url'), $posting);
        $body = $response->json();

        // $response = $client->POST(config('virtual.url'),  ['json' => $posting]);
        // Log::info($response->getBody());
        // $code = $response->getStatusCode();
        // $body = json_decode($response->getBody()->getContents());
        $status = $body['Status']['ResponseCode'] ?? '99';
        Log::info($status);
        if ($status == '00') {
            $add = [
                'nomor_va' => $posting['VirtualAccount'],
                'data_billing_id' => $data['data_billing_id'],
                'nama' => $posting['Nama'],
                'total_tagihan' => $posting['TotalTagihan'],
                'tanggal_exp' => new carbon($posting['TanggalExp']),
                'berita_1' => $posting['Berita1'],
                'berita_2' => $posting['Berita2'],
                'berita_3' => $posting['Berita3'],
                'berita_4' => $posting['Berita4'],
                'berita_5' => $posting['Berita5'],
                'flag_proses' => $posting['FlagProses']
            ];
            DB::table('virtual_account')->insert($add);
        }

        return [
            'code' => $status,
            'desc' => $body['Status']['ErrorDesc'] ?? 'Gagal memposting virtual account'
        ];
    }


    public function postingQris($data)
    {
        $post = $data;
        unset($post['data_billing_id']);
        $client = new Client();
        $response = $client->POST(config('qris.url'),  ['json' => $post]);
        $body = json_decode($response->getBody()->getContents());

        $status = $body->responsCode;

        if ($status == '00') {
            $postqris = [
                'data_billing_id' => $data['data_billing_id'],
                'bill_number' => $body->billNumber,
                'total_amount' => $body->totalAmount,
                'qr_value' => $body->qrValue,
                'amount' => $body->amount,
                'expired_date' => $body->expiredDate,
                'nmid' => $body->nmid,
                'merchant_pan' => $body->merchantPan,
                'invoice_number' => $body->invoice_number,
                'status' => $body->status,
                'merchant_name' => $body->merchantName
            ];
            // Qris::insert($postqris);
            DB::table("qris")->insert($postqris);
        }

        return [
            'code' => $status,
            'desc' => $status == '00' ? 'Berhasil posting QRIS' : 'Gagal memposting virtual account'
        ];
    }
}
