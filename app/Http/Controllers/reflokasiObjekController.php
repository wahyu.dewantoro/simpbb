<?php

namespace App\Http\Controllers;

use App\LokasiObjek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class reflokasiObjekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view_lokasi_objek')->only(['index']);
        $this->middleware('permission:add_lokasi_objek')->only(['create', 'store']);
        $this->middleware('permission:edit_lokasi_objek')->only(['edit', 'update']);
        $this->middleware('permission:delete_lokasi_objek')->only(['destroy']);
    }
    public function index()
    {
        //
        $title = 'Lokasi Objek';
        $data = LokasiObjek::orderby('id')->get();
        return view('lokobjek.index', compact('title', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'method' => 'post',
            'action' => route('refrensi.lokasiobjek.store'),
        ];
        $title = 'Form Lokasi Objek';

        return view('lokobjek.form', compact('title', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $data = ['nama_lokasi' => $request->nama_lokasi];
            // LokasiObjek::create($data);
            Db::connection("oracle_satutujuh")->table("lokasi_objek")->insert($data);
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di tambahkan'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.lokasiobjek.index'))->with($pesan);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objek = LokasiObjek::findorfail($id);
        $data = [
            'method' => 'patch',
            'action' => route('refrensi.lokasiobjek.update', $id),
            'objek' => $objek
        ];
        $title = 'Form Kelompok Objek';

        return view('lokobjek.form', compact('title', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $objek = LokasiObjek::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            $data = ['nama_lokasi' => $request->nama_lokasi];

            DB::connection("oracle_satutujuh")->table("lokasi_objek")->where('id',$id)->update($data);
            // $objek->update($data);
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di update'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.lokasiobjek.index'))->with($pesan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $objek = LokasiObjek::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            // $data = ['nama_lokasi' => $request->nama_lokasi];
            // $objek->delete();
            DB::connection("oracle_satutujuh")->table('lokasi_objek')->where('id', $id)->delete();
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di hapus'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.lokasiobjek.index'))->with($pesan);
    }
}
