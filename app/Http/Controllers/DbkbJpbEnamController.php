<?php

namespace App\Http\Controllers;

use App\Models\DbkbJpb6;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbJpbEnamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbJpb6::where('thn_dbkb_jpb6', $tahun)->orderbyraw('kls_dbkb_jpb6')->get();
            $read = 1;
            return view('dbkb.jepebeenam._index', compact('data', 'read'));
        }
        return view('dbkb.jepebeenam.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DbkbJpb6::where('thn_dbkb_jpb6', $tahun)->orderbyraw('kls_dbkb_jpb6')->get();
            if ($data->count() == 0) {
                $data = DbkbJpb6::where('thn_dbkb_jpb6', $tahun - 1)->orderbyraw('kls_dbkb_jpb6')->get();
            }
            $read = 0;
            return view('dbkb.jepebeenam._index', compact('data', 'read'));
        }
        return view('dbkb/jepebeenam/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'thn_dbkb_jpb6' => $request->thn_dbkb_jpb6, 'kls_dbkb_jpb6' => '1', 'nilai_dbkb_jpb6' => $request->nilai_1 / 1000],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'thn_dbkb_jpb6' => $request->thn_dbkb_jpb6, 'kls_dbkb_jpb6' => '2', 'nilai_dbkb_jpb6' => $request->nilai_2 / 1000],

        ];

        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            DbkbJpb6::where(['thn_dbkb_jpb6' => $request->thn_dbkb_jpb6])->delete();

            // DbkbJpb6::insert($data);
            foreach ($data as $ins) {
                DbkbJpb6::create($ins);
            }

            $flash = ['success' => 'Berhasil memproses DBKB JPB6 pada tahun ' . $request->thn_dbkb_jpb6];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.jepebe-enam.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
