<?php

namespace App\Http\Controllers;

use App\Models\JenisPengurangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JenisPenguranganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Jenis Pengurangan';
        $data = JenisPengurangan::orderby('id')->get();
        return view('jenis_pengurangan.index', compact('title', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'method' => 'post',
            'action' => route('refrensi.jenispengurangan.store'),
        ];
        $title = 'Form Jenis Pengurangan';

        return view('jenis_pengurangan.form', compact('title', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            //code...
            $data = [
                'nama_pengurangan' => $request->nama_pengurangan,
                'jenis' => $request->jenis,
                'pengurangan' => $request->pengurangan
            ];
            JenisPengurangan::create($data);
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di tambahkan'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.jenispengurangan.index'))->with($pesan);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objek = JenisPengurangan::findorfail($id);
        $data = [
            'method' => 'patch',
            'action' => route('refrensi.jenispengurangan.update', $id),
            'objek' => $objek
        ];
        $title = 'Form Jenis Pengurangan';

        return view('jenis_pengurangan.form', compact('title', 'data'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objek = JenisPengurangan::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            $data = [
                'nama_pengurangan' => $request->nama_pengurangan,
                'jenis' => $request->jenis,
                'pengurangan' => $request->pengurangan
            ];
            $objek->update($data);
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di update'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.jenispengurangan.index'))->with($pesan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objek = JenisPengurangan::findorfail($id);
        DB::beginTransaction();
        try {
            //code...
            // $data = ['nama_kelompok' => $request->nama_kelompok];
            $objek->delete();
            DB::commit();
            $pesan = [
                'success' => 'Berhasil di hapus'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $pesan = ['error' => $th->getMessage()];
        }
        return redirect(route('refrensi.jenispengurangan.index'))->with($pesan);
    }
}
