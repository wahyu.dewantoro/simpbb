<?php

namespace App\Http\Controllers;

use App\PembayaranMutasi;
use App\PembayaranMutasiData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PembayaranMutasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function table()
    {
        return "(select a.kd_propinsi||
        a.kd_dati2||
        a.kd_kecamatan||
        a.kd_kelurahan||
        a.kd_blok||
        a.no_urut||
        kd_jns_op nop,thn_pajak_sppt,kd_propinsi_asal||
        kd_dati2_asal||
        kd_kecamatan_asal||
        kd_kelurahan_asal||
        kd_blok_asal||
        no_urut_asal||
        kd_jns_op_asal nop_asal,b.nama created_by,a.created_at
        from pembayaran_sppt_mutasi a
        join sim_pbb.users b on a.created_by=b.id
        order by created_at desc)";
    }

    public function index()
    {
        // $table = $this->table();

        // $data = DB::connection("oracle_satutujuh")->table(DB::raw($table))->paginate(10);
        $data = PembayaranMutasi::with('NopTahun')->paginate();
        // return $data;
        return view('pembayaran_mutasi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $nop_asal = $request->nop_asal;
            $psmb_id = $request->psmb_id;
            $nop_proses = preg_replace('/[^0-9]/', '', $nop_asal);
            $selected = [];
            if ($psmb_id != '') {
                $na = PembayaranMutasi::find($psmb_id);
                $na = $na->nop_asal;

                if ($na == $nop_proses) {
                    $selected = PembayaranMutasiData::where('psmb_id', $psmb_id)->pluck('thn_pajak_sppt')->toArray();
                }
            }


            $kd_propinsi = substr($nop_proses, 0, 2);
            $kd_dati2 = substr($nop_proses, 2, 2);
            $kd_kecamatan = substr($nop_proses, 4, 3);
            $kd_kelurahan = substr($nop_proses, 7, 3);
            $kd_blok = substr($nop_proses, 10, 3);
            $no_urut = substr($nop_proses, 13, 4);
            $kd_jns_op = substr($nop_proses, 17, 1);

            $data = DB::connection("oracle_satutujuh")->select(DB::raw("select thn_pajak_sppt,  sum(jml_sppt_yg_dibayar - nvl(denda_sppt,0)) pokok, sum(nvl(denda_sppt,0)) denda, sum(jml_sppt_yg_dibayar) jumlah 
            from pembayaran_sppt
            where
                kd_propinsi='$kd_propinsi'
                and kd_dati2='$kd_dati2'
            and kd_kecamatan='$kd_kecamatan'
            and kd_kelurahan='$kd_kelurahan'
            and kd_blok='$kd_blok'
            and no_urut='$no_urut'
            and kd_jns_Op='$kd_jns_op'
            group by thn_pajak_sppt
            order by thn_pajak_sppt asc"));
            return view('pembayaran_mutasi._form', compact('data', 'selected'));
        }
        $data = [
            'action' => route('pembayaran-mutasi.store'),
            'method' => 'post'
        ];
        return view('pembayaran_mutasi.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request->all();
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...
            $nop_asal = $request->nop_asal;
            $nop_tujuan = $request->nop_tujuan;
            $dataBatch = [
                'deskripsi' => "Dari " . $nop_asal . " ke " . $nop_tujuan,
                'nop_asal' =>  preg_replace('/[^0-9]/', '', $nop_asal),
                'nop_tujuan' =>  preg_replace('/[^0-9]/', '', $nop_tujuan),
                'created_by' => auth()->user()->id,
                'created_at' => now()
            ];

            $batch = PembayaranMutasi::create($dataBatch);
            $nop_asal = preg_replace('/[^0-9]/', '', $nop_asal);
            $nop_tujuan = preg_replace('/[^0-9]/', '', $nop_tujuan);

            $datas = [];
            foreach ($request->thn_pajak_sppt as $thn_pajak_sppt) {
                $data['psmb_id'] = $batch->id;
                $data['kd_propinsi_asal'] = substr($nop_asal, 0, 2);
                $data['kd_dati2_asal'] = substr($nop_asal, 2, 2);
                $data['kd_kecamatan_asal'] = substr($nop_asal, 4, 3);
                $data['kd_kelurahan_asal'] = substr($nop_asal, 7, 3);
                $data['kd_blok_asal'] = substr($nop_asal, 10, 3);
                $data['no_urut_asal'] = substr($nop_asal, 13, 4);
                $data['kd_jns_op_asal'] = substr($nop_asal, 17, 1);

                $data['kd_propinsi'] = substr($nop_tujuan, 0, 2);
                $data['kd_dati2'] = substr($nop_tujuan, 2, 2);
                $data['kd_kecamatan'] = substr($nop_tujuan, 4, 3);
                $data['kd_kelurahan'] = substr($nop_tujuan, 7, 3);
                $data['kd_blok'] = substr($nop_tujuan, 10, 3);
                $data['no_urut'] = substr($nop_tujuan, 13, 4);
                $data['kd_jns_op'] = substr($nop_tujuan, 17, 1);
                $data['thn_pajak_sppt'] = $thn_pajak_sppt;
                $data['created_by'] = auth()->user()->id;
                $data['created_at'] = now();
                $datas[] = $data;
            }

            // DB::connection("oracle_satutujuh")->table('pembayaran_sppt_mutasi')->insert($datas);
            PembayaranMutasiData::insert($datas);
            DB::connection("oracle_satutujuh")->commit();
            $flash = [
                'success' => 'Berhasil mutasi data pembayaran'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::connection("oracle_satutujuh")->rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return  redirect(route('pembayaran-mutasi.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PembayaranMutasi $pembayaran_mutasi)
    {
        $data = [
            'action' => route('pembayaran-mutasi.update', $pembayaran_mutasi),
            'method' => 'patch',
            'mutasi' => $pembayaran_mutasi
        ];
        return view('pembayaran_mutasi.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PembayaranMutasi $pembayaran_mutasi)
    {
        //

        // return $pembayaran_mutasi;
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...
            $nop_asal = $request->nop_asal;
            $nop_tujuan = $request->nop_tujuan;
            $dataBatch = [
                'deskripsi' => "Dari " . $nop_asal . " ke " . $nop_tujuan,
                'nop_asal' =>  preg_replace('/[^0-9]/', '', $nop_asal),
                'nop_tujuan' =>  preg_replace('/[^0-9]/', '', $nop_tujuan),
                'created_by' => auth()->user()->id,
                'created_at' => now()
            ];

            $pembayaran_mutasi->update($dataBatch);
            $nop_asal = preg_replace('/[^0-9]/', '', $nop_asal);
            $nop_tujuan = preg_replace('/[^0-9]/', '', $nop_tujuan);
            $datas = [];
            foreach ($request->thn_pajak_sppt as $thn_pajak_sppt) {
                $data['psmb_id'] = $pembayaran_mutasi->id;
                $data['kd_propinsi_asal'] = substr($nop_asal, 0, 2);
                $data['kd_dati2_asal'] = substr($nop_asal, 2, 2);
                $data['kd_kecamatan_asal'] = substr($nop_asal, 4, 3);
                $data['kd_kelurahan_asal'] = substr($nop_asal, 7, 3);
                $data['kd_blok_asal'] = substr($nop_asal, 10, 3);
                $data['no_urut_asal'] = substr($nop_asal, 13, 4);
                $data['kd_jns_op_asal'] = substr($nop_asal, 17, 1);

                $data['kd_propinsi'] = substr($nop_tujuan, 0, 2);
                $data['kd_dati2'] = substr($nop_tujuan, 2, 2);
                $data['kd_kecamatan'] = substr($nop_tujuan, 4, 3);
                $data['kd_kelurahan'] = substr($nop_tujuan, 7, 3);
                $data['kd_blok'] = substr($nop_tujuan, 10, 3);
                $data['no_urut'] = substr($nop_tujuan, 13, 4);
                $data['kd_jns_op'] = substr($nop_tujuan, 17, 1);
                $data['thn_pajak_sppt'] = $thn_pajak_sppt;
                $data['created_by'] = auth()->user()->id;
                $data['created_at'] = now();
                $datas[] = $data;
            }

            // DB::connection("oracle_satutujuh")->table('pembayaran_sppt_mutasi')->insert($datas);
            PembayaranMutasiData::where('psmb_id', $pembayaran_mutasi->id)->delete();
            PembayaranMutasiData::insert($datas);
            DB::connection("oracle_satutujuh")->commit();
            $flash = [
                'success' => 'Berhasil update  mutasi data pembayaran'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::connection("oracle_satutujuh")->rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return  redirect(route('pembayaran-mutasi.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PembayaranMutasi $pembayaran_mutasi)
    {
        //
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...
            $id = $pembayaran_mutasi->id;

            PembayaranMutasiData::where('psmb_id', $id)->delete();
            $pembayaran_mutasi->delete();
            DB::connection("oracle_satutujuh")->commit();
            $flash = ['success' => 'Berhasil di hapus'];
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            $flash = ['errorr' => $th->getMessage()];
            DB::connection("oracle_satutujuh")->rollBack();
        }


        return  redirect(route('pembayaran-mutasi.index'))->with($flash);
        // dd($pembayaran_mutasi);
    }
}
