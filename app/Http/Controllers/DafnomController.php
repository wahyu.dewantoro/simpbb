<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data_billing;
use App\Models\Temp_Data;
use App\Models\Billing_kolektif;
use App\Helpers\Dafnom;
use App\Helpers\gallade;
use App\Kecamatan;
use App\Kelurahan;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use App\Helpers\Buku;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Jobs\GenerateDafnomlist;
use App\Jobs\GenerateDafnom;
use Illuminate\Support\Facades\Log;

class DafnomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan','asc')->get();
        $kelurahan = [];
        $userkelurahan=true;
        if($kecamatan->count()=='1'){
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan','asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
            if($kelurahan->count()=='1'){
                $userkelurahan=false;
            }
        }
        $tahun_pajak = [];
        $tahun = $request->tahun ?? date('Y');
        $start=2013;
        for($year= $tahun;$year>=$start;$year--){
            $tahun_pajak[$year]=$year;
        }
        $result = ['data','data'];
        return view('dafnom.index', compact('result','kecamatan','tahun_pajak','kelurahan','userkelurahan'));
        
    }
    public function cek_pembuatan_dafnom(Request $request){
        $return=response()->json(['status'=>false,'msg'=>"Data tidak ditemukan."]);
        $validator= Validator::make($request->all(),[
            'tahun_pajak' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required'
        ],["required"=>'Kolom :attribute harus di isi.']);
        if ($validator->fails()) {
            $msg="<ol><li>".implode('</li><li>',$validator->errors()->all())."</li></ol>";
            return $return=response()->json(['status'=>false,'msg'=>$msg]);
        }
        $request['rencana_bayar']=gallade::money_float($request['rencana_bayar']);
        $get=Dafnom::getDafnomData($request->only(['tahun_pajak','kecamatan','kelurahan','blok','rencana_bayar','option']),500);
        if(count($get)&&is_array($get)){
            $list=['no','nop','nm_wp','pbb','denda','total'];
            $input=['nop','nm_wp','pbb','denda','total'];
            $hiddenList=['alamat','pbb','tahun_pajak'];
            $no=1;
            $setData=[];
            $setDataFalse=[];
            $allTotal=0;
            foreach($get as $item){
                $setitem=[
                    "no"=>gallade::parseInputFormHidden(Arr::only((array)$item, $hiddenList),$hiddenList,$no),
                    "nop"=>$item->kd_propinsi.".".
                    $item->kd_dati2.".".
                    $item->kd_kecamatan.".".
                    $item->kd_kelurahan.".".
                    $item->kd_blok."-".
                    $item->no_urut.".".
                    $item->kd_jns_op
                ];
                $setitem=$setitem+(array)$item;
                $setitem["total"]=$setitem['denda']+$setitem['pbb'];
                $setData[]=gallade::parseInputForm(Arr::only($setitem, $list),$input,$no,['pbb','denda','total'],true);
                $allTotal=$allTotal+$setitem["total"];
                $no++;
            }
            $result["jumlah_nop"]=gallade::parseQuantity(count($setData));
            $result["jumlah_nop_false"]=0;
            $result["akumulasi"]=gallade::parseQuantity($allTotal);
            $result["akumulasi_false"]=0;
            $result["body_preview_nop_dafnom"]=gallade::generateinTbody($setData,"Data Masih Kosong");
            $result["body_preview_nop_dafnom_false"]=gallade::generateinTbody($setDataFalse,"Data Masih Kosong");
            $result['status']=true;
            $return=response()->json($result);
        }
        return $return;
    }
    public function preview(Request $request){
        $default=response()->json(['status'=>false,'msg'=>"File tidak sesuai."]);
        try{
            if($request->hasFile('import_excel')){
                $default=response()->json(['status'=>false,'msg'=>"Format tidak sesuai"]);
                Temp_Data::where(['iduser' => Auth()->user()->id])->delete();
                $file=$request->file('import_excel');
                $path = $file->getRealPath();
                $ext=$file->extension();
                if(!in_array($ext,["xlsx","csv","xls","zip"])){
                    Log::info('Format File upload not valid : ' .$ext);
                    return $default;
                }
                ini_set('memory_limit','-1');
                $array = Excel::toArray(false, $file);
                unset($array[0][0]);
                $setData=[];
                $setDataFalse=[];
                $nofalse=$no='1';
                $pbbTotal=$pbbTotal_false=0;
                $list=array('no','nop','nm_wp','tahun_pajak','pbb','denda','total');
                $input=array('nop','nm_wp','tahun_pajak','pbb','denda','total');
                $listValid=array('no','nop','nm_wp','alamat','tahun_pajak','pbb','msg');
                /// reduce data input empty
                $limit=500;
                $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
                $kelurahan = Kelurahan::login()->orderBy('nm_kelurahan', 'asc')->get();
                if($kecamatan->count()=='1'){
                    $sampleID=current($array[0]);
                    $nop=explode(".", $sampleID[1]);
                    if(isset($nop[2])){
                        if($kecamatan->first()->kd_kecamatan!=$nop[2]){
                            return response()->json(['status'=>false,'msg'=>"File Excel tidak berada di lokasi Kecamatan ".$kecamatan->first()->nm_kecamatan.". [".$kecamatan->first()->kd_kecamatan."!=".$nop[2]."]"]);
                        }
                    }
                    if($kelurahan->count()=='1'){
                        $sampleIDx=current($array[0]);
                        $nop=explode(".", $sampleIDx[1]);
                        if($kelurahan->first()->kd_kelurahan!=$nop[3]){
                            return response()->json(['status'=>false,'msg'=>"File Excel tidak berada di lokasi Kelurahan ".$kelurahan->first()->nm_kelurahan.". [".$kelurahan->first()->kd_kelurahan."!=".$nop[3]."]"]);
                        }
                    }
                }
                $checkDupliacte=collect();
                $temp_nop=[];
                $pbbmax=Buku::getBuku(2)['max'];
                $length=1;
                $listToTemp=[];
                $tahunLimit=false;
                foreach($array[0] as $cell){
                    $returnSet=array(
                        "no"=>$cell[0]??0,
                        "nop"=>$cell[1]??0,
                        "nm_wp"=>$cell[2]??0,
                        "alamat"=>$cell[3]??0,
                        "tahun_pajak"=>$cell[4]??0,
                        "pbb"=>$cell[5]??0
                    );
                    $validator=gallade::import($returnSet);
                    if($validator){
                        
                        if($length>$limit){
                            return response()->json(['status'=>false,'msg'=>"Data Melebihi kapasitas Proses Validasi Kode Biling \n  Maksimal Proses Validasi ".$limit." data."]);
                        }
                        if($tahunLimit===false){
                            $tahunLimit=$returnSet['tahun_pajak'];
                        }
                        if($tahunLimit!=$returnSet['tahun_pajak']){
                            return response()->json(["msg"=>"dalam satu billing kolektif hanya untuk 1 tahun yang sama","status"=>false]);
                        }
                        if(Arr::exists($temp_nop,$validator['nop'].$validator['tahun_pajak'])&&$validator["status"]){
                            $validator["status"]=false;
                            $validator['msg']='Data Nop lebih dari satu.';
                        }
                        $temp_nop[$validator['nop'].$returnSet['tahun_pajak']]=$validator['nop'].$returnSet['tahun_pajak']; 
                        if($validator["status"]){
                            //$getData=Dafnom::cekNOP(Arr::only($validator, array("nop","tahun_pajak")));
                            $listToTemp[]=Arr::only($validator, array("nop","tahun_pajak"));
                        }
                        if(!$validator["status"]){
                            if(!is_numeric($validator["pbb"])){
                                $validator["pbb"]=(int)$validator["pbb"]??0;
                            }
                            $validator["denda"]=0;
                            $validator["total"]= $validator["denda"]+ $validator["pbb"];
                            
                            $validator["no"]=$nofalse;
                            $pbbTotal_false=$pbbTotal_false+$validator["total"];
                            $validator["pbb"]=gallade::parseQuantity($validator["pbb"]);
                            $setDataFalse[]=Arr::only($validator, $listValid);
                            $nofalse++;
                        }
                        $length++;
                    }
                }
                
                // dd($listToTemp);
                $toTempNOP=Dafnom::tempNOP($listToTemp);

                // dd($toTempNOP);
                $getAllData=Dafnom::getAlltempNOP();
                // dd($getAllData);
                if(count($getAllData)>0&&is_array($getAllData)){
                    foreach($getAllData as $validatorSet){
                        $validatorSet=(array) $validatorSet;
                        $validatorSet["total"]=$validatorSet["pbb"]+$validatorSet["denda"];
                        $validatorSet["no"]='';
                        if($validatorSet['data_ada']=='1'){
                            $validatorSet["status"]=true;
                            if($validatorSet['lunas_pbb']!='0'){
                                $validatorSet["status"]=false;
                                $validatorSet["msg"]='NOP Sudah Lunas';
                                if($validatorSet['lunas_pbb']=='2'){
                                    $validatorSet["msg"]='NOP Kurang Bayar';
                                }
                            }
                            if($validatorSet['billing_kolektif']!='0'){
                                //??'Masuk Billing Kolektif.'
                                if($validatorSet['keterangan_tambahan']!=""&&$validatorSet['kd_status']!='1'){
                                    $validatorSet['keterangan']=$validatorSet['keterangan']." : ".$validatorSet['keterangan_tambahan'];
                                }
                                $validatorSet["status"]=false;
                                $validatorSet["msg"]=$validatorSet['keterangan'];
                            }
                            if($pbbmax<$validatorSet['pbb']){
                                $validatorSet["status"]=false;
                                $validatorSet["msg"]='Nominal Pajak tidak termasuk dalam Buku 1,2';
                            }
                        }else{
                            $validatorSet["status"]=false;
                            $validatorSet["msg"]='Nop tidak Valid atau tagihan tidak di temukan.';
                        }
                        
                        if(!$validatorSet["status"]){
                            $validatorSet["no"]=$nofalse;
                            $pbbTotal_false=$pbbTotal_false+$validatorSet["total"];
                            $validatorSet["pbb"]=gallade::parseQuantity($validatorSet["pbb"]);
                            $setDataFalse[]=Arr::only($validatorSet, $listValid);
                            $nofalse++;
                        }else{
                            $hiddenList=['alamat'];
                            $validatorSet["no"]=$no.gallade::parseInputFormHidden(Arr::only($validatorSet, $hiddenList),$hiddenList,$no);
                            $validatorSet['unique']=$validatorSet['nop'].$validatorSet['tahun_pajak'];
                            $checkDupliacte[$no]=(object) $validatorSet;
                            $setData[$no]=gallade::parseInputForm(Arr::only($validatorSet, $list),$input,$no,['pbb','denda','total'],true);
                            $no++;
                            $pbbTotal=$pbbTotal+$validatorSet["total"];
                        }
                    }
                }
                $removeDuplicate=$checkDupliacte->duplicates('unique');
                if(count($removeDuplicate)>0){
                    foreach($removeDuplicate->toArray() as $key=>$value){
                        $pbbTotal_false=$pbbTotal_false+$checkDupliacte[$key]->pbb;
                        $newSet=(array)$checkDupliacte[$key];
                        $newSet['no']=$nofalse;
                        $newSet['pbb']=gallade::parseQuantity($checkDupliacte[$key]->pbb);
                        $newSet['msg']='Data Nop lebih dari satu.';
                        $setDataFalse[]=Arr::only($newSet, $listValid);
                        $nofalse++;
                        unset($setData[$key]);
                    }
                }
                if(count($setData)>$limit){
                return response()->json(['status'=>false,'msg'=>"Data Melebihi kapasitas Proses Validasi Kode Biling \n  Maksimal Proses Validasi ".$limit." data."]);
                }
                $return["jumlah_nop"]=count($setData);
                $return["jumlah_nop_false"]=count($setDataFalse);
                $return["akumulasi"]=gallade::parseQuantity($pbbTotal);
                $return["akumulasi_false"]=gallade::parseQuantity($pbbTotal_false);
                $return["body_preview_dafnom"]=gallade::generateinTbody($setData,"Data Masih Kosong");
                $return["body_preview_dafnom_false"]=gallade::generateinTbody($setDataFalse,"Data Masih Kosong");
                $return['status']=true;
                $default=$return;
            }
            return response()->json($default);
        } catch (\Exception $e) {
            $msg='Proses Validasi berkendala.';//$e->getMessage();
            Log::error('Error : ' .$e->getMessage());
            return response()->json(["msg"=>$msg,"status"=>false]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function cek_no_urut($tahun,$kecamatan,$kelurahan,$cekStart=false){
         if($cekStart){
            $fr = "(SELECT min_a - 1 + LEVEL hasil
                            FROM (SELECT NVL (MAX (TO_NUMBER (no_urut)), 0) max_a, 1 min_a
                                    FROM data_billing
                                WHERE     tahun_pajak = '" . $tahun . "'
                                        AND kd_kecamatan = '" . $kecamatan . "'
                                        AND kd_kelurahan = '" . $kelurahan . "'
                                        AND kd_jns_op = '1')
                    CONNECT BY LEVEL <= max_a - min_a + 1
                    MINUS
                    SELECT TO_NUMBER (no_urut)
                        FROM data_billing
                    WHERE     tahun_pajak = '$tahun'
            AND kd_kecamatan = '" . $kecamatan . "'
            AND kd_kelurahan = '" . $kelurahan . "'
            AND kd_jns_op = '1')";
            $cr = DB::table(db::raw($fr))->first();
            $urut='1';
            if (!empty($cr)) {
                $urut=$cr->hasil;
            } 
            $no_urut = sprintf("%04s",$urut);
        }else{ 
            $sql="SELECT nvl(max(no_urut),0) nomer
                FROM data_billing
                WHERE tahun_pajak='".$tahun."'
                AND kd_kecamatan='".$kecamatan."'
                AND kd_kelurahan='".$kelurahan."'
                AND kd_jns_op='1'";
            $urut=DB::connection('oracle')->select(db::raw($sql));
            $urut=current($urut);
            // $urut = Data_billing::select(db::raw("nvl(max(no_urut),0) nomer"))
            //     ->where('tahun_pajak', $tahun)
            //     ->where('kd_kecamatan', $kecamatan)
            //     ->where('kd_kelurahan', $kelurahan)
            //     ->where('kd_jns_op', 1)
            //     ->first()
            //     ->withTrashed();
            $kode = $urut->nomer;
            $no_urut = sprintf("%04s", $kode + 1);
            if(strlen($no_urut)=='5'){
                $no_urut=$this->cek_no_urut($tahun,$kecamatan,$kelurahan,true);
            }
        }
        return $no_urut;
    }
    public function store(Request $request)
    {
        $request=$request->only(['data']);
        if(!$request['data']){
            return response()->json(["msg"=>"Terjadi kesalahan pengiriman data","status"=>false]);
        }
        $setBilling=current($request['data']);
        $nopData=explode(".",$setBilling['nop']);
        $year=$setBilling['tahun_pajak'];
       
        $kecamatan=Kecamatan::find($nopData[2]);
        $kecamatan=($kecamatan)?$kecamatan->nm_kecamatan:"Kecamatan Kosong";
        $kelurahan=Kelurahan::where(['KD_KECAMATAN'=>$nopData[2],'KD_KELURAHAN'=>$nopData[3]]);
        $kelurahan=($kelurahan->count())?$kelurahan->get()->first()->nm_kelurahan:"Kelurahan Kosong";
        $total_awal=count($request['data']);
        ini_set('memory_limit','-1');
        $getId=false;
        DB::beginTransaction();
        try {
            $no_urut=$this->cek_no_urut($year,$nopData[2],$nopData[3]);
            $kobil=$nopData[0].$nopData[1].$nopData[2].$nopData[3].'999'.$no_urut."1";
            
            $cekTahunLimit=Collect($request['data'])->transform(function($res){
                return $res['tahun_pajak'];
            });
            if($cekTahunLimit->unique()->count()>1){
                return response()->json(["msg"=>"dalam satu billing kolektif hanya untuk 1 tahun yang sama","status"=>false]);
            }
            $cek=Data_billing::where([
                'kd_propinsi'=>'35',
                'kd_dati2'=>'07',
                'kd_kecamatan'=>$kecamatan,
                'kd_kelurahan'=>$kelurahan,
                'kd_blok'=>"999",
                'no_urut'=>$no_urut,
                'kd_jns_op'=>"1",
            ])->get();
            if($cek->count()>0){
                $no_urut=$this->cek_no_urut($year,$nopData[2],$nopData[3]);
            }
            $setDatabiling=array(
                "KD_PROPINSI"=>$nopData[0],
                "KD_DATI2"=>$nopData[1],
                "KD_KECAMATAN"=>$nopData[2],
                "KD_KELURAHAN"=>$nopData[3], 
                "KD_BLOK"=>'999',
                "NO_URUT"=>$no_urut, 
                "KD_JNS_OP"=>1,
                "TAHUN_PAJAK"=>$setBilling["tahun_pajak"],
                "KD_STATUS"=>"0", 
                "NAMA_WP"=>  'Billing Kolektif UPT (DS '.$kelurahan.', KEC '.$kecamatan.') Jml NOP: '.$total_awal,
                "NAMA_KELURAHAN"=>$kelurahan,
                "NAMA_KECAMATAN"=>$kecamatan,
                "KOBIL"=>$kobil,
            );
            $Data_billing=new Data_billing($setDatabiling);
            $Data_billing->save();
            $getId=$Data_billing->data_billing_id;
            $billing_kolektif=[];
            $excelItem=[];
            $list=array("kd_propinsi","kd_dati2","kd_kecamatan","kd_kelurahan","kd_blok","no_urut","kd_jns_op","tahun_pajak","pbb","nm_wp","pokok","denda","total");
            $no=1;
            foreach($request['data'] as $item){
                $nop=explode(".",$item["nop"]);
                $blok_urut=explode("-",$nop[4]);
                $item["kd_propinsi"]=$nop[0];
                $item["kd_dati2"]=$nop[1];
                $item["kd_kecamatan"]=$nop[2];
                $item["kd_kelurahan"]=$nop[3];
                $item["kd_blok"]=$blok_urut[0];
                $item["no_urut"]=$blok_urut[1];
                $item["kd_jns_op"]=$nop[5];
                $cek_where=Arr::only($item,["kd_propinsi","kd_dati2","kd_kecamatan","kd_kelurahan","kd_blok","no_urut","kd_jns_op","tahun_pajak"]);
                //penyesuaian
                $cek=Billing_kolektif::select(['billing_kolektif.data_billing_id'])
                        ->join('data_billing','data_billing.data_billing_id',"=","billing_kolektif.data_billing_id")
                        ->where([
                            "billing_kolektif.kd_propinsi"=>$item["kd_propinsi"],
                            "billing_kolektif.kd_dati2"=>$item["kd_dati2"],
                            "billing_kolektif.kd_kecamatan"=>$item["kd_kecamatan"],
                            "billing_kolektif.kd_kelurahan"=>$item["kd_kelurahan"],
                            "billing_kolektif.kd_blok"=>$item["kd_blok"],
                            "billing_kolektif.no_urut"=>$item["no_urut"],
                            "billing_kolektif.kd_jns_op"=>$item["kd_jns_op"],
                            "billing_kolektif.tahun_pajak"=>$item["tahun_pajak"]
                            ])
                        ->whereRaw('(data_billing.deleted_at IS NULL and TO_DATE(data_billing.expired_at) >SYSDATE )')
                        ->get();
                if($cek->count()==0){
                    $excelItem[]=[
                        'no'=>$no,
                        'nop'=>$item['nop'],
                        "nm_wp"=>$item['nm_wp'],
                        "alamat"=>$item['alamat'],
                        "tahun_pajak"=>$item['tahun_pajak'],
                        "pbb"=>$item['pbb'],
                        "denda"=>$item['denda'],
                        "total"=>$item['total'],
                    ];
                    //array_merge(['no'=>$no],$item);
                    //Dafnom::getDenda($item["nop"],$item["tahun_pajak"]);
                    //$denda=(in_array('denda',$item))?$item['denda']:0;
                    $item["pokok"]=$item["pbb"]; 
                    $item["denda"]=$item["denda"];
                    $item["total"]=$item["total"];
                    $billing_kolektif[]=new Billing_kolektif(Arr::only($item, $list));
                    $no++;
                }
            }
            $Data_billing->billing_kolektif()->saveMany($billing_kolektif);
            $createExcel=Dafnom::makeExcelDafnom($excelItem,$kobil."_".$getId);
            if($total_awal!=count($billing_kolektif)){
                $total_awal=count($billing_kolektif);
                $update=["NAMA_WP"=>  'Billing Kolektif UPT (DS '.$kelurahan.', KEC '.$kecamatan.') Jml NOP: '.$total_awal];
                Data_billing::where(['data_billing_id'=>$getId])->update($update);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            //"Terjadi Kesalahan, Harap Hubungi Admin";//
            $msg=$e->getMessage();
            return response()->json(["msg"=>$msg,"status"=>false]);
        }
        Temp_Data::where(['iduser' => Auth()->user()->id])->delete();
        $return=["msg"=>"Proses pembuatan billing kolektif bermasalah","status"=>false];
        if($getId){
            $return=["msg"=>"Proses pembuatan billing kolektif berhasil. Kobil : ".$kobil,"status"=>true];
            dispatch(new GenerateDafnomlist(['id_billing'=>$getId]))->onQueue(Dafnom::defaultQueuename());
            dispatch(new GenerateDafnom(['id_billing'=>$getId]))->delay(now()->addMinutes(Dafnom::defaultDelayQueue()))->onQueue(Dafnom::defaultQueuename());
        }
        return response()->json($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
