<?php

namespace App\Http\Controllers;

use App\Exports\PotonganObjekExcel;
use App\Imports\MsPotonganIndividuImport;
use App\Models\MsPotongan;
use App\Models\MsPotonganScope;
use App\MsPotonganIndividu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use phpseclib\Crypt\RC2;
use Yajra\DataTables\Facades\DataTables;

class MsPotonganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? '';
        $potongan = MsPotongan::select(DB::raw("tahun,kd_buku,jenis, case when jenis='1' then 'Potongan Persentase' else 'Potongan Selisih tahun berjalan dengan tahun sebelumnya' end  keterangan,persentase,to_char(tgl_mulai,'yyyy-mm-dd') tgl_mulai,to_char(tgl_selesai,'yyyy-mm-dd') tgl_selesai"));
        if ($cari != '') {
            $potongan = $potongan->where("tahun", $cari);
        }
        $potongan = $potongan->OrderByraw(DB::raw("tahun desc,kd_buku asc"))->paginate(10);

        $tab = $request->tab ?? '';

        return view('ms_potongan.index', compact('potongan', 'tab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function KelurahanCollection()
    {

        $data = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
            ->join("ref_kelurahan", "ref_kelurahan.kd_kecamatan", "=", "ref_kecamatan.kd_kecamatan")
            ->select(db::raw("ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan,nm_kecamatan,nm_kelurahan"))
            ->orderByRaw("ref_kelurahan.kd_kecamatan,ref_kelurahan.kd_kelurahan")->get()->toArray();

        return collect($data)->groupBy(['nm_kecamatan']);
    }

    public function create()
    {
        $data = [
            'action' => route('master-potongan.store'),
            'method' => 'post',
            'kelurahan' => $this->KelurahanCollection()
        ];
        return view('ms_potongan.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $tahun = $request->tahun;
        $kd_buku = $request->kd_buku;
        $jenis = $request->jenis;
        $persentase = $request->persentase;
        $cek = MsPotongan::whereraw(db::raw("tahun='$tahun' and kd_buku='$kd_buku'"))->pluck('kd_buku', 'tahun')->toArray();
        // dd($cek);
        $msg = "";
        if (count($cek) == 0) {
            // $msg .= "Oke";
            db::beginTransaction();
            try {
                //code...
                MsPotongan::create(
                    [
                        'tahun' => $tahun,
                        'kd_buku' => $kd_buku,
                        'jenis' => $jenis,
                        'persentase' => $jenis == 1 ? $persentase : null,
                        'tgl_mulai' => new Carbon($request->tgl_mulai),
                        'tgl_selesai' => new Carbon($request->tgl_selesai)
                    ]
                );

                // kec_kel
                if (count($request->kec_kel) > 0) {
                    foreach ($request->kec_kel as $kec_kel) {
                        $exp = explode('_', $kec_kel);
                        $kd_kecamatan = $exp[0];
                        $kd_kelurahan = $exp[1];
                        MsPotonganScope::create([
                            'tahun' => $tahun,
                            'kd_buku' => $kd_buku,
                            'kd_propinsi' => '35',
                            'kd_dati2' => '07',
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                        ]);
                    }
                }
                db::commit();
                $flash = [
                    'success' => 'Data potongan berhasil di tambah'
                ];
            } catch (\Throwable $th) {
                //throw $th;
                db::rollBack();
                $flash = [
                    'error' => $th->getMessage()
                ];

                // dd($th);
            }
        } else {
            // $msg .= "Data sudah ada";
            $flash = [
                'Warning' => 'Potongan untuk buku ' . $kd_buku . ' pada tahun ' . $tahun . 'Sudah ada'
            ];
        }
        // return $msg;
        return redirect(route('master-potongan.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exp = explode('-', $id);
        $tahun = $exp[0];
        $buku = $exp[1];

        $potongan = MsPotongan::whereraw("tahun='$tahun' and kd_buku='$buku'")
            ->select(DB::raw("tahun,kd_buku,jenis, case when jenis='1' then 'Potongan Persentase' else 'Potongan Selisih tahun berjalan dengan tahun sebelumnya' end  keterangan,persentase"))
            ->first();
        $scope = MsPotonganScope::join('ref_kecamatan', function ($join) {
            $join->on('ref_kecamatan.kd_propinsi', '=', 'ms_potongan_kebijakan_scope.kd_propinsi')
                ->on('ref_kecamatan.kd_dati2', '=', 'ms_potongan_kebijakan_scope.kd_dati2')
                ->on('ref_kecamatan.kd_kecamatan', '=', 'ms_potongan_kebijakan_scope.kd_kecamatan');
        })
            ->join('ref_kelurahan', function ($join) {
                $join->on('ref_kelurahan.kd_propinsi', '=', 'ms_potongan_kebijakan_scope.kd_propinsi')
                    ->on('ref_kelurahan.kd_dati2', '=', 'ms_potongan_kebijakan_scope.kd_dati2')
                    ->on('ref_kelurahan.kd_kecamatan', '=', 'ms_potongan_kebijakan_scope.kd_kecamatan')
                    ->on('ref_kelurahan.kd_kelurahan', '=', 'ms_potongan_kebijakan_scope.kd_kelurahan');
            })->select(db::raw("ms_potongan_kebijakan_scope.*,nm_kelurahan,nm_kecamatan"))
            ->whereraw("tahun='$tahun' and kd_buku='$buku'")->orderbyraw('ms_potongan_kebijakan_scope.kd_kecamatan,ms_potongan_kebijakan_scope.kd_kelurahan')->get()->toarray();
        $scopePotongan = collect($scope)->groupBy(['nm_kecamatan']);
        // dd($groupedScope);
        return view('ms_potongan.show', compact('potongan', 'scopePotongan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $exp = explode('-', $id);
        $tahun = $exp[0];
        $buku = $exp[1];

        $potongan = MsPotongan::whereraw("tahun='$tahun' and kd_buku='$buku'")
            ->select(DB::raw("tahun,kd_buku,jenis, case when jenis='1' then 'Potongan Persentase' else 'Potongan Selisih tahun berjalan dengan tahun sebelumnya' end  keterangan,persentase,to_char(tgl_mulai,'dd mon yyyy') tgl_mulai,to_char(tgl_selesai,'dd mon yyyy') tgl_selesai"))
            ->first();
        $scope = MsPotonganScope::whereraw("tahun='$tahun' and kd_buku='$buku'")->selectraw("kd_kecamatan||'_'||kd_kelurahan kec_kel")->pluck('kec_kel')->toarray();
        // $scopePotongan = collect($scope)->groupBy(['nm_kecamatan']);
        $data = [
            'action' => route('master-potongan.update', $id),
            'method' => 'patch',
            'kelurahan' => $this->KelurahanCollection(),
            'potongan' => $potongan,
            'scope' => $scope
        ];

        // dd($data['scope']);
        // dd($potongan);

        return view('ms_potongan.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tahun = $request->tahun;
        $kd_buku = $request->kd_buku;
        $jenis = $request->jenis;
        $persentase = $request->persentase;
        db::beginTransaction();
        try {
            //code...
            MsPotongan::whereraw("tahun='$tahun' and kd_buku='$kd_buku'")->update(
                [
                    'tahun' => $tahun,
                    'kd_buku' => $kd_buku,
                    'jenis' => $jenis,
                    'persentase' => $jenis == 1 ? $persentase : null,
                    'tgl_mulai' => new Carbon($request->tgl_mulai),
                    'tgl_selesai' => new Carbon($request->tgl_selesai)
                ]
            );

            MsPotonganScope::whereraw("tahun='$tahun' and kd_buku='$kd_buku'")->delete();
            // kec_kel
            if (count($request->kec_kel) > 0) {
                foreach ($request->kec_kel as $kec_kel) {
                    $exp = explode('_', $kec_kel);
                    $kd_kecamatan = $exp[0];
                    $kd_kelurahan = $exp[1];
                    MsPotonganScope::create([
                        'tahun' => $tahun,
                        'kd_buku' => $kd_buku,
                        'kd_propinsi' => '35',
                        'kd_dati2' => '07',
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                    ]);
                }
            }
            db::commit();
            $flash = [
                'success' => 'Data potongan berhasil di edit'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            $flash = [
                'error' => $th->getMessage()
            ];
        }

        return redirect(route('master-potongan.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exp = explode('-', $id);
        $tahun = $exp[0];
        $buku = $exp[1];

        // $potongan = MsPotongan::whereraw("tahun='$tahun' and kd_buku='$buku'")
        db::beginTransaction();
        try {
            //code...
            MsPotongan::whereraw("tahun='$tahun' and kd_buku='$buku'")->delete();
            MsPotonganScope::whereraw("tahun='$tahun' and kd_buku='$buku'")->delete();
            DB::commit();
            $flash = ['success' => 'Berhasil hapus data'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            $flash = ['error' => $th->getMessage()];
        }
        return redirect(route('master-potongan.index'))->with($flash);
    }

    public function individu(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::connection('oracle_satutujuh')->table("ms_potongan_individu")
                ->join('sppt', function ($join) {
                    $join->on('ms_potongan_individu.kd_propinsi', '=', 'sppt.kd_propinsi')
                        ->on('ms_potongan_individu.kd_dati2', '=', 'sppt.kd_dati2')
                        ->on('ms_potongan_individu.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                        ->on('ms_potongan_individu.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                        ->on('ms_potongan_individu.kd_blok', '=', 'sppt.kd_blok')
                        ->on('ms_potongan_individu.no_urut', '=', 'sppt.no_urut')
                        ->on('ms_potongan_individu.kd_jns_op', '=', 'sppt.kd_jns_op')
                        ->on('ms_potongan_individu.thn_pajak_sppt', '=', 'sppt.thn_pajak_sppt');
                })
                ->select(db::raw("ms_potongan_individu.kd_propinsi, ms_potongan_individu.kd_dati2, ms_potongan_individu.kd_kecamatan, ms_potongan_individu.kd_kelurahan, ms_potongan_individu.kd_blok, ms_potongan_individu.no_urut, ms_potongan_individu.kd_jns_op, ms_potongan_individu.thn_pajak_sppt, nm_wp_sppt, nilai_potongan,to_char(tgl_mulai,'yyyy-mm-dd') tgl_mulai,to_char(tgl_selesai,'yyyy-mm-dd') tgl_selesai "));
            if ($request->has('tahun')) {
                $thn_pajak_sppt = $request->tahun;
                $data = $data->whereraw("ms_potongan_individu.thn_pajak_sppt='$thn_pajak_sppt'");
            }


            $cari = $request->search;
            if ($cari <> '') {
                $q = strtolower($cari);

                $nop = onlyNumber($q);
                if ($nop <> '') {
                    $data = $data->whereraw("(ms_potongan_individu.kd_propinsi||ms_potongan_individu.kd_dati2||ms_potongan_individu.kd_kecamatan||ms_potongan_individu.kd_kelurahan||ms_potongan_individu.kd_blok||ms_potongan_individu.no_urut||ms_potongan_individu.kd_jns_op like '%" . $nop . "%')");
                } else {
                    $data = $data->whereraw("(
                         lower(nm_wp_sppt) like '%" . $q . "%'
                    )");
                }
            }


            return  DataTables::of($data)
                /*   ->filter(function ($query) use ($request) {
                    if ($request->has('tahun')) {
                        $thn_pajak_sppt = $request->tahun;
                        $query->whereraw("ms_potongan_individu.thn_pajak_sppt='$thn_pajak_sppt'");
                    }


                    $cari = $request->search;
                    if ($cari <> '') {
                        $q = strtolower($cari);

                        $nop = onlyNumber($q);
                        if ($nop <> '') {
                            $query->whereraw("(ms_potongan_individu.kd_propinsi||ms_potongan_individu.kd_dati2||ms_potongan_individu.kd_kecamatan||ms_potongan_individu.kd_kelurahan||ms_potongan_individu.kd_blok||ms_potongan_individu.no_urut||ms_potongan_individu.kd_jns_op like '%" . $nop . "%')");
                        } else {
                            $query->whereraw("(
                             lower(nm_wp_sppt) like '%" . $q . "%'
                        )");
                        }
                    }
                }) */
                ->addColumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->editColumn('nilai_potongan', function ($row) {
                    return angka($row->nilai_potongan);
                })
                ->addColumn('masa_berlaku', function ($row) {
                    return tglindo($row->tgl_mulai) . ' s/d ' . tglIndo($row->tgl_selesai);
                })
                ->addColumn('aksi', function ($row) {
                    $nop = $row->kd_propinsi  . $row->kd_dati2  . $row->kd_kecamatan  . $row->kd_kelurahan  . $row->kd_blok . $row->no_urut  . $row->kd_jns_op . $row->thn_pajak_sppt;
                    $btn = "<a href='" . url('master-potongan-individu', $nop) . "' class='text-info'><i class='fas fa-edit'></i></a>";
                    $btn .= " <a href='#' data-href='" . url('master-potongan-individu-hapus', $nop) . "' class='hapuspotongan text-danger'><i class='fas fa-trash'></i></a>";
                    return $btn;
                })
                ->rawColumns(['nop', 'nilai_potongan', 'masa_berlaku', 'aksi'])->make(true);
        }

        $data = [
            'action' => url('master-potongan-individu-nop'),
            'method' => 'post',
            'potongan' => []
        ];

        return view('ms_potongan/form_individu', compact('data'));
    }

    public function Storeindividu(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'max:2048|required',
        ]);

        if ($validator->fails()) {
            $msg = "";
            foreach ($validator->errors()->all() as $rk) {
                $msg .= $rk . ', ';
            }
            $msg = \substr($msg, '0', '-2');
            return redirect(url('master-potongan-individu'))->with(['error' => $msg]);
        }


        $file = $request->file('file');
        $path = $file->getRealPath();
        $ext = $file->extension();
        // return $ext;
        if (!in_array($ext, ["xlsx", "csv", "xls"])) {
            // return $default;
            return redirect(url('master-potongan-individu'))->with(['error' => 'Format tidak sesuai']);
        }

        $array = Excel::toArray(new MsPotonganIndividuImport(), $file);
        $tgl_mulai = $request->tgl_mulai;
        if ($tgl_mulai == '') {
            $tgl_mulai = date('Y-m-d');
        }
        $tgl_selesai = $request->tgl_selesai;
        if ($tgl_selesai == '') {
            $tgl_selesai = '9999-' . date('m-d');
        }
        DB::beginTransaction();
        try {
            //code...

            foreach ($array[0] as $row) {

                $nop = $row['nop'];
                $res = onlyNumber($nop);
                $row['kd_propinsi'] = substr($res, 0, 2);
                $row['kd_dati2'] = substr($res, 2, 2);
                $row['kd_kecamatan'] = substr($res, 4, 3);
                $row['kd_kelurahan'] = substr($res, 7, 3);
                $row['kd_blok'] = substr($res, 10, 3);
                $row['no_urut'] = substr($res, 13, 4);
                $row['kd_jns_op'] = substr($res, 17, 1);


                $cek = MsPotonganIndividu::where([
                    'kd_propinsi' => $row['kd_propinsi'],
                    'kd_dati2' => $row['kd_dati2'],
                    'kd_kecamatan' => $row['kd_kecamatan'],
                    'kd_kelurahan' => $row['kd_kelurahan'],
                    'kd_blok' => $row['kd_blok'],
                    'no_urut' => $row['no_urut'],
                    'kd_jns_op' => $row['kd_jns_op'],
                    'thn_pajak_sppt' => $row['thn_pajak_sppt']
                ])->count();


                if ($cek == 0) {
                    MsPotonganIndividu::create([
                        'kd_propinsi' => $row['kd_propinsi'],
                        'kd_dati2' => padding($row['kd_dati2'], '0', 2),
                        'kd_kecamatan' => padding($row['kd_kecamatan'], '0', 3),
                        'kd_kelurahan' => padding($row['kd_kelurahan'], '0', 3),
                        'kd_blok' => padding($row['kd_blok'], '0', 3),
                        'no_urut' => padding($row['no_urut'], '0', 4),
                        'kd_jns_op' => $row['kd_jns_op'],
                        'thn_pajak_sppt' => $row['thn_pajak_sppt'],
                        'nilai_potongan' => $row['nilai_potongan'],
                        'created_at' => db::raw("sysdate"),
                        'created_by' => Auth()->user()->id,
                        'tgl_mulai' => new Carbon($tgl_mulai),
                        'tgl_selesai' => new Carbon($tgl_selesai)
                    ]);
                } else {
                    MsPotonganIndividu::where([
                        'kd_propinsi' => $row['kd_propinsi'],
                        'kd_dati2' => padding($row['kd_dati2'], '0', 2),
                        'kd_kecamatan' => padding($row['kd_kecamatan'], '0', 3),
                        'kd_kelurahan' => padding($row['kd_kelurahan'], '0', 3),
                        'kd_blok' => padding($row['kd_blok'], '0', 3),
                        'no_urut' => padding($row['no_urut'], '0', 4),
                        'kd_jns_op' => $row['kd_jns_op'],
                        'thn_pajak_sppt' => $row['thn_pajak_sppt']
                    ])->update([
                        'kd_propinsi' => $row['kd_propinsi'],
                        'kd_dati2' => padding($row['kd_dati2'], '0', 2),
                        'kd_kecamatan' => padding($row['kd_kecamatan'], '0', 3),
                        'kd_kelurahan' => padding($row['kd_kelurahan'], '0', 3),
                        'kd_blok' => padding($row['kd_blok'], '0', 3),
                        'no_urut' => padding($row['no_urut'], '0', 4),
                        'kd_jns_op' => $row['kd_jns_op'],
                        'thn_pajak_sppt' => $row['thn_pajak_sppt'],
                        'nilai_potongan' => $row['nilai_potongan'],
                        'created_at' => db::raw("sysdate"),
                        'created_by' => Auth()->user()->id,
                        'tgl_mulai' => new Carbon($tgl_mulai),
                        'tgl_selesai' => new Carbon($tgl_selesai)
                    ]);
                }

                DB::connection("oracle_dua")->statement(db::raw("begin hitung_potongan('" . $row['kd_kecamatan'] . "', '" . $row['kd_kelurahan'] . "', '" . $row['kd_blok'] . "', '" . $row['no_urut'] . "', '" . $row['kd_jns_op'] . "', '" . $row['thn_pajak_sppt'] . "' ); commit; end;"));
            }
            db::commit();
            return redirect(url('master-potongan-individu'))->with(['success' => 'berhasil import']);
        } catch (\Throwable $th) {
            //throw $th;
            db::rollBack();
            Log::error($th);
            return redirect(url('master-potongan-individu'))->with(['error' => $th->getMessage()]);
        }
    }


    public function StoreindividuNop(Request $request)
    {
        $nop = $request->nop;
        $res = onlyNumber($nop);
        $thn_pajak_sppt = $request->thn_pajak_sppt;
        $nilai_potongan = $request->nilai_potongan;
        $row['kd_propinsi'] = substr($res, 0, 2);
        $row['kd_dati2'] = substr($res, 2, 2);
        $row['kd_kecamatan'] = substr($res, 4, 3);
        $row['kd_kelurahan'] = substr($res, 7, 3);
        $row['kd_blok'] = substr($res, 10, 3);
        $row['no_urut'] = substr($res, 13, 4);
        $row['kd_jns_op'] = substr($res, 17, 1);
        $tgl_mulai = $request->tgl_mulai;
        if ($tgl_mulai == '') {
            $tgl_mulai = date('Y-m-d');
        }
        $tgl_selesai = $request->tgl_selesai;
        if ($tgl_selesai == '') {
            $tgl_selesai = '9999-' . date('m-d');
        }

        DB::beginTransaction();
        try {

            $cek = MsPotonganIndividu::where([
                'kd_propinsi' => $row['kd_propinsi'],
                'kd_dati2' => $row['kd_dati2'],
                'kd_kecamatan' => $row['kd_kecamatan'],
                'kd_kelurahan' => $row['kd_kelurahan'],
                'kd_blok' => $row['kd_blok'],
                'no_urut' => $row['no_urut'],
                'kd_jns_op' => $row['kd_jns_op'],
                'thn_pajak_sppt' => $thn_pajak_sppt
            ])->count();

            if ($cek == 0) {
                MsPotonganIndividu::create([
                    'kd_propinsi' => $row['kd_propinsi'],
                    'kd_dati2' => $row['kd_dati2'],
                    'kd_kecamatan' => $row['kd_kecamatan'],
                    'kd_kelurahan' => $row['kd_kelurahan'],
                    'kd_blok' => $row['kd_blok'],
                    'no_urut' => $row['no_urut'],
                    'kd_jns_op' => $row['kd_jns_op'],
                    'thn_pajak_sppt' => $thn_pajak_sppt,
                    'nilai_potongan' => $nilai_potongan,
                    'created_at' => db::raw("sysdate"),
                    'created_by' => Auth()->user()->id,
                    'tgl_mulai' => new Carbon($tgl_mulai),
                    'tgl_selesai' => new Carbon($tgl_selesai)
                ]);
            } else {
                MsPotonganIndividu::where([
                    'kd_propinsi' => $row['kd_propinsi'],
                    'kd_dati2' => $row['kd_dati2'],
                    'kd_kecamatan' => $row['kd_kecamatan'],
                    'kd_kelurahan' => $row['kd_kelurahan'],
                    'kd_blok' => $row['kd_blok'],
                    'no_urut' => $row['no_urut'],
                    'kd_jns_op' => $row['kd_jns_op'],
                    'thn_pajak_sppt' => $thn_pajak_sppt
                ])->update([
                    'kd_propinsi' => $row['kd_propinsi'],
                    'kd_dati2' => $row['kd_dati2'],
                    'kd_kecamatan' => $row['kd_kecamatan'],
                    'kd_kelurahan' => $row['kd_kelurahan'],
                    'kd_blok' => $row['kd_blok'],
                    'no_urut' => $row['no_urut'],
                    'kd_jns_op' => $row['kd_jns_op'],
                    'thn_pajak_sppt' => $thn_pajak_sppt,
                    'nilai_potongan' => $nilai_potongan,
                    'created_at' => db::raw("sysdate"),
                    'created_by' => Auth()->user()->id,
                    'tgl_mulai' => new Carbon($tgl_mulai),
                    'tgl_selesai' => new Carbon($tgl_selesai)
                ]);
            }

            DB::commit();

            DB::connection("oracle_dua")->statement(db::raw("begin hitung_potongan('" . $row['kd_kecamatan'] . "', '" . $row['kd_kelurahan'] . "', '" . $row['kd_blok'] . "', '" . $row['no_urut'] . "', '" . $row['kd_jns_op'] . "', '" . $thn_pajak_sppt . "' ); commit; end;"));
            $msg = [
                'success' => 'Berhasil di proses'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            $msg = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('master-potongan') . '?tab=individu')->with($msg);
    }

    public function HapusIndividu($res)
    {
        $row['kd_propinsi'] = substr($res, 0, 2);
        $row['kd_dati2'] = substr($res, 2, 2);
        $row['kd_kecamatan'] = substr($res, 4, 3);
        $row['kd_kelurahan'] = substr($res, 7, 3);
        $row['kd_blok'] = substr($res, 10, 3);
        $row['no_urut'] = substr($res, 13, 4);
        $row['kd_jns_op'] = substr($res, 17, 1);
        $row['thn_pajak_sppt'] = substr($res, 18, 4);
        // dd($row);
        DB::beginTransaction();
        try {
            MsPotonganIndividu::where($row)->delete();
            DB::commit();
            DB::connection("oracle_dua")->statement(db::raw("begin hitung_potongan('" . $row['kd_kecamatan'] . "', '" . $row['kd_kelurahan'] . "', '" . $row['kd_blok'] . "', '" . $row['no_urut'] . "', '" . $row['kd_jns_op'] . "', '" . $row['thn_pajak_sppt'] . "' ); commit; end;"));
            $msg = [
                'success' => 'Berhasil di hapus'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollBack();
            $msg = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('master-potongan') . '?tab=individu')->with($msg);
    }

    public function EditIndividu($res)
    {
        $row['kd_propinsi'] = substr($res, 0, 2);
        $row['kd_dati2'] = substr($res, 2, 2);
        $row['kd_kecamatan'] = substr($res, 4, 3);
        $row['kd_kelurahan'] = substr($res, 7, 3);
        $row['kd_blok'] = substr($res, 10, 3);
        $row['no_urut'] = substr($res, 13, 4);
        $row['kd_jns_op'] = substr($res, 17, 1);
        $row['thn_pajak_sppt'] = substr($res, 18, 4);
        $pot = MsPotonganIndividu::where($row)->first();

        $data = [
            'action' => url('master-potongan-individu', $res),
            'method' => 'post',
            'potongan' => $pot
        ];
        return view('ms_potongan/form_individu', compact('data'));
    }

    public function UpdateIndividu(Request $request, $nop)
    {
        $nop = $request->nop;
        $res = onlyNumber($nop);
        $thn_pajak_sppt = $request->thn_pajak_sppt;
        $nilai_potongan = $request->nilai_potongan;
        $row['kd_propinsi'] = substr($res, 0, 2);
        $row['kd_dati2'] = substr($res, 2, 2);
        $row['kd_kecamatan'] = substr($res, 4, 3);
        $row['kd_kelurahan'] = substr($res, 7, 3);
        $row['kd_blok'] = substr($res, 10, 3);
        $row['no_urut'] = substr($res, 13, 4);
        $row['kd_jns_op'] = substr($res, 17, 1);

        $tgl_mulai = $request->tgl_mulai;
        if ($tgl_mulai == '') {
            $tgl_mulai = date('Y-m-d');
        }
        $tgl_selesai = $request->tgl_selesai;
        if ($tgl_selesai == '') {
            $tgl_selesai = '9999-' . date('m-d');
        }

        DB::beginTransaction();
        try {
            MsPotonganIndividu::where($row)->where('thn_pajak_sppt', $thn_pajak_sppt)->update([
                'kd_propinsi' => $row['kd_propinsi'],
                'kd_dati2' => $row['kd_dati2'],
                'kd_kecamatan' => $row['kd_kecamatan'],
                'kd_kelurahan' => $row['kd_kelurahan'],
                'kd_blok' => $row['kd_blok'],
                'no_urut' => $row['no_urut'],
                'kd_jns_op' => $row['kd_jns_op'],
                'thn_pajak_sppt' => $thn_pajak_sppt,
                'nilai_potongan' => $nilai_potongan,
                'created_at' => db::raw("sysdate"),
                'created_by' => Auth()->user()->id,
                'tgl_mulai' => new Carbon($tgl_mulai),
                'tgl_selesai' => new Carbon($tgl_selesai)
            ]);
            DB::commit();
            DB::connection("oracle_dua")->statement(db::raw("begin hitung_potongan('" . $row['kd_kecamatan'] . "', '" . $row['kd_kelurahan'] . "', '" . $row['kd_blok'] . "', '" . $row['no_urut'] . "', '" . $row['kd_jns_op'] . "', '" . $thn_pajak_sppt . "' ); commit; end;"));
            $msg = [
                'success' => 'Berhasil di update'
            ];
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            $msg = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('master-potongan') . '?tab=individu')->with($msg);
    }

    public function listPotonganSql($tahun, $jenis)
    {
        $tahun = $tahun ?? date('Y');
        $jenis = $jenis ?? '';

        $sql = DB::connection("oracle_satutujuh")
            ->table(db::raw("sppt_potongan_detail a"))
            ->join(db::raw("sppt c"), function ($join) {
                $join->on('a.kd_propinsi', '=', 'c.kd_propinsi')
                    ->on('a.kd_dati2', '=', 'c.kd_dati2')
                    ->on('a.kd_kecamatan', '=', 'c.kd_kecamatan')
                    ->on('a.kd_kelurahan', '=', 'c.kd_kelurahan')
                    ->on('a.kd_blok', '=', 'c.kd_blok')
                    ->on('a.no_urut', '=', 'c.no_urut')
                    ->on('a.kd_jns_op', '=', 'c.kd_jns_op')
                    ->on('a.thn_pajak_sppt', '=', 'c.thn_pajak_sppt');
            })
            ->join(db::raw("dat_objek_pajak f"), function ($join) {
                $join->on('c.kd_propinsi', '=', 'f.kd_propinsi')
                    ->on('c.kd_dati2', '=', 'f.kd_dati2')
                    ->on('c.kd_kecamatan', '=', 'f.kd_kecamatan')
                    ->on('c.kd_kelurahan', '=', 'f.kd_kelurahan')
                    ->on('c.kd_blok', '=', 'f.kd_blok')
                    ->on('c.no_urut', '=', 'f.no_urut')
                    ->on('c.kd_jns_op', '=', 'f.kd_jns_op');
            })
            ->join(db::raw("ref_kecamatan g"), function ($join) {
                $join->on('f.kd_Kecamatan', '=', 'g.kd_kecamatan');
            })
            ->join(db::raw("ref_kelurahan h"), function ($join) {
                $join->on('h.kd_kecamatan', '=', 'f.kd_kecamatan')
                    ->on('h.kd_kelurahan', '=', 'f.kd_kelurahan');
            })
            ->leftjoin(db::raw("ref_buku d"), function ($join) {
                $join->on('d.thn_awal', '<=', 'c.thn_pajak_sppt')
                    ->on('d.thn_akhir', '>=', 'c.thn_pajak_sppt')
                    ->on('d.nilai_min_buku', '<=', 'c.pbb_yg_harus_dibayar_sppt')
                    ->on('d.nilai_max_buku', '>=', 'c.pbb_yg_harus_dibayar_sppt');
            })
            ->leftjoin(db::raw('ms_potongan_kebijakan e'), function ($join) {
                $join->on('e.tahun', '=', 'a.thn_pajak_sppt')
                    ->on('e.kd_buku', '=', 'd.kd_buku');
            })
            ->leftjoin(db::raw("ms_potongan_individu b"), function ($join) {
                $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                    ->on('a.kd_dati2', '=', 'b.kd_dati2')
                    ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                    ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                    ->on('a.kd_blok', '=', 'b.kd_blok')
                    ->on('a.no_urut', '=', 'b.no_urut')
                    ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                    ->on('a.thn_pajak_sppt', '=', 'b.thn_pajak_sppt');
            })
            ->select(db::raw("a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, f.jalan_op, blok_kav_no_op, nm_kelurahan, nm_kecamatan, a.thn_pajak_sppt, nm_wp_sppt, c.pbb_yg_harus_dibayar_sppt pbb, d.kd_buku, a.nilai_potongan, case when b.nilai_potongan is not null then '3' else e.jenis end jenis,
            case when b.nilai_potongan is not null then 'individu' else e.persentase||'%' end persentase
            "))
            ->whereraw("a.thn_pajak_sppt = '$tahun' AND jns_potongan = '1' AND a.nilai_potongan > 0");

        if ($jenis <> '') {
            $sql = $sql->whereraw("case when b.nilai_potongan is not null then '3' else e.jenis end = '$jenis'");
        }

        return $sql;
    }

    public function listPotongan(Request $request)
    {
        if ($request->ajax()) {
            $tahun_pajak = $request->tahun_pajak;
            $jenis = $request->jenis;


            $data = $this->listPotonganSql($tahun_pajak, $jenis);
            return  DataTables::of($data)
                ->addColumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->addColumn('alamat', function ($row) {
                    $alamat = "";
                    if ($row->jalan_op <> '') {
                        $alamat .= $row->jalan_op;
                    }

                    if ($row->blok_kav_no_op <> '') {
                        $alamat .= " " . $row->blok_kav_no_op;
                    }
                    return $alamat;
                })
                ->rawColumns(['nop', 'alamat'])->make(true);
        }
        return  view('ms_potongan.list_data');
    }


    public function sqlRekapPotongan($tahun)
    {
        $table = "(SELECT a.kd_propinsi,
        a.kd_dati2,
        a.kd_kecamatan,
        a.kd_kelurahan,
        a.kd_blok,
        a.no_urut,
        a.kd_jns_op,
        a.thn_pajak_sppt,
        c.pbb_yg_harus_dibayar_sppt pbb,
        d.kd_buku,
        a.nilai_potongan,
        CASE
           WHEN b.nilai_potongan IS NOT NULL THEN '3'
           ELSE e.jenis
        END
           jenis,
        CASE
           WHEN b.nilai_potongan IS NOT NULL THEN 'Individu'
           ELSE e.persentase || '%'
        END
           persentase
   FROM sppt_potongan_detail a
        INNER JOIN sppt c
           ON     A.KD_PROPINSI = C.KD_PROPINSI
              AND A.KD_DATI2 = C.KD_DATI2
              AND A.KD_KECAMATAN = C.KD_KECAMATAN
              AND A.KD_KELURAHAN = C.KD_KELURAHAN
              AND A.KD_BLOK = C.KD_BLOK
              AND A.NO_URUT = C.NO_URUT
              AND A.KD_JNS_OP = C.KD_JNS_OP
              AND A.THN_PAJAK_SPPT = C.THN_PAJAK_SPPT
        LEFT JOIN ref_buku d
           ON     D.THN_AWAL <= C.THN_PAJAK_SPPT
              AND D.THN_AKHIR >= C.THN_PAJAK_SPPT
              AND D.NILAI_MIN_BUKU <=
                     C.PBB_YG_HARUS_DIBAYAR_SPPT
              AND D.NILAI_MAX_BUKU >=
                     C.PBB_YG_HARUS_DIBAYAR_SPPT
        LEFT JOIN ms_potongan_kebijakan e
           ON     E.TAHUN = A.THN_PAJAK_SPPT
              AND E.KD_BUKU = D.KD_BUKU
        LEFT JOIN ms_potongan_individu b
           ON     A.KD_PROPINSI = B.KD_PROPINSI
              AND A.KD_DATI2 = B.KD_DATI2
              AND A.KD_KECAMATAN = B.KD_KECAMATAN
              AND A.KD_KELURAHAN = B.KD_KELURAHAN
              AND A.KD_BLOK = B.KD_BLOK
              AND A.NO_URUT = B.NO_URUT
              AND A.KD_JNS_OP = B.KD_JNS_OP
              AND A.THN_PAJAK_SPPT = B.THN_PAJAK_SPPT
  WHERE     a.thn_pajak_sppt = '$tahun'
        AND jns_potongan = '1'
        AND a.nilai_potongan > 0) rkp";
        return DB::connection("oracle_satutujuh")->table(db::raw($table))->groupBy("persentase")->select(db::raw("persentase jenis, SUM (nilai_potongan) nilai"))->orderby('persentase', 'asc');
    }

    public function RekapPotongan(Request $request)
    {
        $tahun = $request->tahun ?? date('Y');

        $data = $this->sqlRekapPotongan($tahun)->get();
        return  view('ms_potongan.rekap_data', compact('data', 'tahun'));
    }

    public function listPotonganExcel(Request $request)
    {
        $tahun_pajak = $request->tahun_pajak;
        $jenis = $request->jenis;
        $data = $this->listPotonganSql($tahun_pajak, $jenis)->get();

        if ($jenis == '1') {
            $ket = 'Persentase';
        } else if ($jenis == '2') {
            $ket = 'Selisih';
        } else {
            $ket = 'Individu';
        }

        $param = [
            'data' => $data
        ];
        return Excel::download(new PotonganObjekExcel($param), 'Data potongan ' . $tahun_pajak . ' jenis ' . $ket . '.xlsx');
    }
}
