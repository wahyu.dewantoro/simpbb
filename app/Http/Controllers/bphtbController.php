<?php

namespace App\Http\Controllers;

use App\Exports\bphtbExport;
use App\Helpers\Bphtb;
use App\Kecamatan;
use App\Services\simBphtbService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class bphtbController extends Controller
{
    //

    protected $simBphtbService;

    public function __construct(simBphtbService $simBphtbService)
    {
        $this->simBphtbService = $simBphtbService;
    }

    public function index(Request $request)
    {
        $tahun = $request->tahun ?? date('Y');
        $data = Bphtb::rekapKecamatan($tahun)->simplePaginate(10);
        return view('bphtb.index', compact('data', 'tahun'));
    }

    public function show($kd_kecamatan, $tahun)
    {
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->firstorfail();
        $data = Bphtb::ajuanKecamatan($tahun, $kd_kecamatan)->orderByRaw(DB::raw("rpt_sspd_final.nop"))->simplePaginate(10);
        return view('bphtb.index_detail', compact('data', 'kecamatan', 'tahun', 'kd_kecamatan'));
    }


    public function getExcel($kd_kecamatan, $tahun)
    {
        /*    dd(
            [$kd_kecamatan, $tahun]
        ); */
        return Excel::download(new bphtbExport($tahun, $kd_kecamatan), 'Ajuan BPHTB kecamatan ' . $kd_kecamatan . '.xlsx');
    }

    public function detailNop(Request $request)
    {
        // dd($request->all());
        $nop = $request->nop;
        $sspd = Bphtb::ajuanNop($nop)->get()->toArray();
        // dd($sspd);
        return view('bphtb._detail', compact('sspd'));
    }


    public function apiDataSspd()
    {
        // Data yang akan dikirimkan
        $data = [
            'start_date' => '2024-01-01',
            'end_date' => '2024-01-20',
        ];

        // Basic Auth credentials
        $username = 'simpbb_sandbox@sipanji.id'; // Ganti dengan username autentikasi
        $password = 's4nb0xpbb'; // Ganti dengan password autentikasi

        // URL API
        $url = 'https://bphtb-bapenda.malangkab.go.id/sandbox/api/sspd-bphtb'; // Ganti dengan URL API tujuan

        // Cache key (berbasis parameter)
        $cacheKey = 'sspd_bphtb_response_' . md5($url . json_encode($data));

        // Cek cache, jika ada maka ambil dari cache
        return Cache::remember($cacheKey, now()->addMinutes(30), function () use ($url, $username, $password, $data) {
            try {
                // Inisialisasi Guzzle Client
                $client = new Client();

                // Kirim request POST
                $response = $client->post($url, [
                    'auth' => [$username, $password], // Basic Authentication
                    'form_params' => $data, // Parameter POST
                ]);

                // Ambil isi response
                $responseBody = json_decode($response->getBody(), true);

                return response()->json($responseBody);
            } catch (\Exception $e) {
                // Tangani error
                return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage(),
                ], 500);
            }
        });
    }

    public function sspdIndex(Request $request)
    {

        if ($request->ajax()) {
            $startDate = $request->start_date;
            $endDate = $request->end_date;
            $data = $this->simBphtbService->sspdFetchData($startDate, $endDate);
            return response()->json($data);
        }
        return view('bphtb.sspd.index');
    }

    public function sspdShow($noreg)
    {
        $response = $this->simBphtbService->sspdShowData($noreg);
        $data = $response['data'] ?? [];
        return view('bphtb.sspd.show', compact('data'));
    }
}
