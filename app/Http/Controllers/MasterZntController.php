<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\LokasiObjek;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Validator;
use Illuminate\Validation\Rule;

class MasterZntController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lokasi = LokasiObjek::pluck('nama_lokasi', 'id')->toarray();
        if ($request->ajax()) {
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $tahun = date('Y');
            $data = DB::connection('oracle_satutujuh')->table(db::raw("dat_znt"))
                ->leftjoin(db::raw("sim_pbb.users users"), 'users.id', '=', 'dat_znt.created_by')
                ->leftjoin(db::raw("sim_pbb.users us"), 'us.id', '=', 'dat_znt.updated_by')
                ->select(db::raw(" dat_znt.kd_propinsi,
                dat_znt.kd_dati2,
                dat_znt.kd_kecamatan,
                dat_znt.kd_kelurahan,
                dat_znt.kd_znt,
                dat_znt.lokasi_objek_id,
                    users.nama created_by,
                    us.nama updated_by,
                (SELECT LISTAGG (dat_peta_znt.kd_blok ||' [ '||  case when status_peta_blok='0' then 'SISTEP' else 'SISMIOP' end  ||' ]', ', ') WITHIN GROUP (ORDER BY dat_peta_znt.kd_blok) asd 
                   FROM dat_peta_znt
                   join dat_peta_blok on dat_peta_blok.kd_kecamatan=dat_peta_znt.kd_kecamatan
                   and dat_peta_blok.kd_kelurahan=dat_peta_znt.kd_kelurahan and 
                   dat_peta_blok.kd_blok=dat_peta_znt.kd_blok
                  WHERE     dat_peta_znt.kd_kecamatan = dat_znt.kd_kecamatan
                        AND dat_peta_znt.kd_kelurahan = dat_znt.kd_kelurahan
                        AND kd_znt = dat_znt.kd_znt)
                   blok,
                (SELECT LISTAGG ( (nir * 1000), ', ')
                           WITHIN GROUP (ORDER BY thn_nir_znt)
                   FROM dat_nir
                  WHERE     kd_kecamatan = dat_znt.kd_kecamatan
                        AND kd_kelurahan = dat_znt.kd_kelurahan
                        AND kd_znt = dat_znt.kd_znt
                        AND thn_nir_znt = '" . $tahun . "') nir,
                        REGEXP_REPLACE (DAT_ZNT.kd_znt, '[^0-9]', '') cs,
                        (select case when count(1) >0 then 1 else 0 end  from dat_op_bumi where kd_znt=dat_znt.kd_znt and kd_kecamatan=dat_znt.kd_kecamatan and kd_kelurahan=dat_znt.kd_kelurahan  ) kepakai,
                        ( select nama_lokasi from sim_pbb.lokasi_objek where id=dat_znt.lokasi_objek_id) nama_lokasi
                        "))
                // ->leftjoin('dat_op_bumi', 'DAT_ZNT.KD_ZNT', '=', 'DAT_OP_BUMI.KD_ZNT')
                ->whereraw("DAT_ZNT.kd_kecamatan='" . $kd_kecamatan . "' and dat_znt.kd_kelurahan='" . $kd_kelurahan . "'")
                ->orderbyraw("regexp_replace(DAT_ZNT.kd_znt, '[^0-9]', '') nulls first,DAT_ZNT.kd_znt asc")
                ->get();
            return view('masterznt/index_data', compact('data', 'lokasi'));
        }

        $kecamatan = Kecamatan::login()->orderby('kd_kecamatan')->get();


        return view('masterznt/index', compact('kecamatan', 'lokasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function storeBlok(Request $request)
    {
        // return $request->all();

        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_znt = $request->kd_znt;
        $messages = [
            'required' => ':attribute harus disertakan',
            'unique' => ':attribute sudah terdaftar dalam database',
            'numeric' => ':attribute harus angka',
            'digits' => ':attribute kudu :digits digits.',
            'date_format' => ':attribute tidak sesuai format, pastikan format :attribute adalah :format.',
        ];


        $validator = FacadesValidator::make(array_map('strtoupper', $request->all()), [
            'kd_blok' => 'required',
            'kd_blok' => Rule::unique('oracle_satutujuh.dat_peta_znt')->where(function ($query) use ($kd_kecamatan, $kd_kelurahan, $kd_znt) {
                return $query->whereraw("(kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_znt='$kd_znt' ) ");
            }),

        ], $messages);

        if ($validator->fails()) {
            // jika error
            $msg = "";
            foreach ($validator->errors()->all() as $rk) {
                $msg .= $rk . ', ';
            }
            $msg = \substr($msg, '0', '-2');
            $status = 0;
        } else {
            DB::beginTransaction();
            try {
                //code...
                DB::connection('oracle_satutujuh')->table('dat_peta_znt')->insert([
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'kd_kecamatan' => $request->kd_kecamatan,
                    'kd_kelurahan' => $request->kd_kelurahan,
                    'kd_znt' => $request->kd_znt,
                    'kd_blok' => sprintf("%03s", $request->kd_blok),
                ]);
                DB::commit();
                $status = 1;
                $msg = "Berhasil di tambahkan";
            } catch (\Throwable $th) {
                db::rollBack();
                Log::emergency($th);
                $status = 0;
                $msg = $th->getMessage();
            }
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];

        // return $request->all();
    }

    public function storeNir(Request $request)
    {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_znt = $request->kd_znt;
        $thn_nir_znt = $request->thn_nir_znt;
        $nir = $request->nir;
        $nilai_hkpd = $request->nilai_hkpd ?? 100;
        $is_edit = $request->is_edit;
        $messages = [
            'required' => ':attribute harus disertakan',
            'unique' => ':attribute sudah terdaftar dalam database',
            'numeric' => ':attribute harus angka',
            'digits' => ':attribute kudu :digits digits.',
            'date_format' => ':attribute tidak sesuai format, pastikan format :attribute adalah :format.',
        ];

        $validator = FacadesValidator::make(array_map('strtoupper', $request->all()), [
            'thn_nir_znt' => 'required',
            'nir' => 'required',

        ], $messages);


        if ($validator->fails()) {
            // jika error
            $msg = "";
            foreach ($validator->errors()->all() as $rk) {
                $msg .= $rk . ', ';
            }
            $msg = \substr($msg, '0', '-2');
            // dd($msg);
            $status = 0;
        } else {
            DB::beginTransaction();
            try {
                //code...
                $nir = $request->nir / 1000;
                // $connection = DB::connection('oracle_satutujuh');
                /* $exist = $connection->table('dat_nir')->whereraw("(kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_znt='$kd_znt' and thn_nir_znt='$thn_nir_znt' ) ")->first();
                if ($exist && $is_edit == 0) {
                    $status = 0;
                    $msg = "Tahun " . $thn_nir_znt . " Telah memiliki NIR " . angka(($exist->nir * 1000));
                } else {
                    $cekdua = $connection->table('dat_nir')->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'  and nir='$nir'  and thn_nir_znt='$thn_nir_znt'")->first();
                    if ($cekdua && $is_edit == 0) {
                        $status = 0;
                        $msg = "Pada tahun " . $thn_nir_znt . " NIR dengan " . $request->nir . " telah terekam pada kode ZNT :" . $cekdua->kd_znt;
                    } else { */

                if ($is_edit == 0) {

                    DB::connection('oracle_satutujuh')->table('dat_nir')->insert([
                        'kd_propinsi' => '35',
                        'kd_dati2' => '07',
                        'kd_kecamatan' => $request->kd_kecamatan,
                        'kd_kelurahan' => $request->kd_kelurahan,
                        'kd_znt' => strtoupper($request->kd_znt),
                        'thn_nir_znt' => $request->thn_nir_znt,
                        'nir' => $nir,
                        'nilai_hkpd' => $nilai_hkpd,
                        'kd_kanwil' => '01',
                        'kd_kantor' => '01',
                        'jns_dokumen' => '1',
                        'no_dokumen' => db::raw("to_char(sysdate,'yyyymmddhh24i')"),
                        'created_by' => Auth()->user()->id,
                        'created_at' => Carbon::now()
                    ]);

                    DB::commit();
                    $status = 1;
                    $msg = "Berhasil di tambahkan";
                } else {
                    DB::connection('oracle_satutujuh')->table('dat_nir')
                        ->where([
                            'kd_propinsi' => '35',
                            'kd_dati2' => '07',
                            'kd_kecamatan' => $request->kd_kecamatan,
                            'kd_kelurahan' => $request->kd_kelurahan,
                            'kd_znt' => strtoupper($request->kd_znt),
                            'thn_nir_znt' => $request->thn_nir_znt
                        ])
                        ->update([
                            'kd_propinsi' => '35',
                            'kd_dati2' => '07',
                            'kd_kecamatan' => $request->kd_kecamatan,
                            'kd_kelurahan' => $request->kd_kelurahan,
                            'kd_znt' => strtoupper($request->kd_znt),
                            'thn_nir_znt' => $request->thn_nir_znt,
                            'nir' => $nir,
                            'nilai_hkpd' => $nilai_hkpd,
                            'kd_kanwil' => '01',
                            'kd_kantor' => '01',
                            'jns_dokumen' => '1',
                            'no_dokumen' => db::raw("to_char(sysdate,'yyyymmddhh24i')"),
                            'updated_by' => Auth()->user()->id,
                            'updated_at' => Carbon::now()
                        ]);
                    DB::commit();
                    $status = 1;
                    $msg = "Berhasil di edit";
                }
                /* }
                } */
            } catch (\Throwable $th) {
                db::rollBack();
                Log::emergency($th);
                $status = 0;
                $msg = $th->getMessage();
            }
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];

        return $request->all();
    }


    public function store(Request $request)
    {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_znt = $request->kd_znt;
        $messages = [
            'required' => ':attribute harus disertakan',
            'unique' => ':attribute sudah terdaftar dalam database',
            'numeric' => ':attribute harus angka',
            'digits' => ':attribute kudu :digits digits.',
            'date_format' => ':attribute tidak sesuai format, pastikan format :attribute adalah :format.',
        ];



        $validator = FacadesValidator::make(array_map('strtoupper', $request->all()), [
            'kd_znt' => 'required'
        ], $messages);
        if ($validator->fails()) {
            $msg = "";
            foreach ($validator->errors()->all() as $rk) {
                $msg .= $rk . ', ';
            }
            $msg = \substr($msg, '0', '-2');
            // dd($msg);
            $status = 0;
        } else {
            DB::beginTransaction();
            try {
                //code...
                $table = DB::connection('oracle_satutujuh')->table('dat_znt');
                // cek duplikasi
                $cek = $table->whereraw("kd_kecamatan='$kd_kecamatan'
                            and kd_kelurahan='$kd_kelurahan' and lower(kd_znt)=lower('$kd_znt')")
                    ->select(db::raw("count(1) res"))
                    ->first();
                if ($cek->res == 0) {
                    DB::connection('oracle_satutujuh')->table('dat_znt')->insert([
                        'kd_propinsi' => '35',
                        'kd_dati2' => '07',
                        'kd_kecamatan' => $request->kd_kecamatan,
                        'kd_kelurahan' => $request->kd_kelurahan,
                        'kd_znt' => strtoupper($request->kd_znt),
                        'lokasi_objek_id' => $request->lokasi_objek_id,
                        'created_by' => Auth()->user()->id,
                        'created_at' => Carbon::now()
                    ]);
                    DB::commit();
                    $status = 1;
                    $msg = "Berhasil di tambahkan";
                } else {
                    $status = 0;
                    $msg = $kd_znt . ' Telah tersedia di dalam database';
                }
            } catch (\Throwable $th) {

                db::rollBack();
                Log::emergency($th);
                $status = 0;
                $msg = $th->getMessage();
            }
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id;
        $cek = explode('_', $id);
        if (count($cek) == 3) {
            $kd_kecamatan = $cek[0];
            $kd_kelurahan = $cek[1];
            $kd_znt = $cek[2];
            $data = DB::connection('oracle_satutujuh')->table('dat_znt')
                ->leftjoin(db::raw("sim_pbb.users users"), 'users.id', '=', 'dat_znt.created_by')
                ->leftjoin(db::raw("sim_pbb.users us"), 'us.id', '=', 'dat_znt.updated_by')
                ->select(db::raw("dat_znt.kd_propinsi,
                dat_znt.kd_dati2,
                dat_znt.kd_kecamatan,
                dat_znt.kd_kelurahan,
                dat_znt.kd_znt,
                dat_znt.lokasi_objek_id,
                    users.nama created_by,
                    us.nama updated_by,
                    dat_znt.created_at,
                    dat_znt.updated_at,
                (select nm_kecamatan from ref_kecamatan where kd_kecamatan=dat_znt.kd_kecamatan)
                kecamatan,
                (select nm_kelurahan from ref_kelurahan where kd_kecamatan=dat_znt.kd_kecamatan and kd_kelurahan=dat_znt.kd_kelurahan)
                kelurahan,
                (
                select listagg(kd_blok, ', ') within group (order by kd_blok)
                from dat_peta_znt 
                where kd_kecamatan=dat_znt.kd_kecamatan
                and kd_kelurahan=dat_znt.kd_kelurahan
                and kd_znt=dat_znt.kd_znt
              ) blok"))
                ->where('dat_znt.kd_kecamatan', $kd_kecamatan)->where('dat_znt.kd_kelurahan', $kd_kelurahan)
                ->where('dat_znt.kd_znt', $kd_znt)
                ->first();
            $blok_arr = DB::connection('oracle_satutujuh')->table('dat_peta_blok')
                ->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->pluck('kd_blok')->toArray();


            $nir = DB::connection("oracle_satutujuh")->select(db::raw("SELECT distinct thn_nir_znt,
            dat_nir.kd_propinsi,
            dat_nir.kd_dati2,
            dat_nir.kd_kecamatan,
            dat_nir.kd_kelurahan,
            dat_nir.kd_znt,
            users.nama created_by,
            dat_nir.created_at,
            us.nama updated_by,
            dat_nir.updated_at,
            nir * 1000 nir,
            dat_nir.nilai_hkpd,
            kd_kls_tanah,
            nilai_per_m2_tanah * 1000 nilai_per_m2_tanah,
            case when thn_pajak_sppt is not null then '1' else '0' end pakai
       FROM dat_nir
       left join sim_pbb.users on users.id=dat_nir.created_by
    left join sim_pbb.users us on us.id=dat_nir.updated_by
            LEFT JOIN (select * from kelas_tanah where  upper(kelas_tanah.kd_kls_tanah) not like '%X%')  kelas_tanah
               ON     KELAS_TANAH.THN_awal_kls_tanah <= dat_nir.thn_nir_znt
                  AND KELAS_TANAH.THN_akhir_kls_tanah >= dat_nir.thn_nir_znt
                  AND KELAS_TANAH.NILAI_MIN_TANAH <= nir
                  AND KELAS_TANAH.NILAI_Max_TANAH > nir 
     left join (select distinct dat_op_bumi.kd_propinsi,
     dat_op_bumi.kd_dati2,
     dat_op_bumi.kd_kecamatan,
     dat_op_bumi.kd_kelurahan,
     dat_op_bumi.kd_znt,
     thn_pajak_sppt 
      from dat_op_bumi
      join sppt on dat_op_bumi.kd_propinsi=sppt.kd_propinsi and
     dat_op_bumi.kd_dati2=sppt.kd_dati2 and
     dat_op_bumi.kd_kecamatan=sppt.kd_kecamatan and
     dat_op_bumi.kd_kelurahan=sppt.kd_kelurahan and
     dat_op_bumi.kd_blok=sppt.kd_blok and
     dat_op_bumi.no_urut=sppt.no_urut and
     dat_op_bumi.kd_jns_op=sppt.kd_jns_op) ck on dat_nir.thn_nir_znt=ck.thn_pajak_sppt and
     dat_nir.kd_propinsi=ck.kd_propinsi and
     dat_nir.kd_dati2=ck.kd_dati2 and
     dat_nir.kd_kecamatan=ck.kd_kecamatan and
     dat_nir.kd_kelurahan=ck.kd_kelurahan and
     dat_nir.kd_znt=dat_nir.kd_znt
      WHERE dat_nir.kd_kecamatan = '$kd_kecamatan' AND dat_nir.kd_kelurahan = '$kd_kelurahan' AND dat_nir.kd_znt = '$kd_znt'
     order by thn_nir_znt desc"));

            $blokpeta = DB::connection("oracle_satutujuh")->select(db::raw("select a.*,(select count(1) from dat_op_bumi
            where kd_kecamatan=a.kd_kecamatan
            and kd_kelurahan=a.kd_kelurahan
            and kd_blok=a.kd_blok
            and kd_znt=a.kd_znt
           ) op
            from DAT_PETA_ZNT a
            where kd_kecamatan='$kd_kecamatan'
            and kd_kelurahan='$kd_kelurahan'
            and kd_znt='$kd_znt'"));

            return view('masterznt/show', compact('data', 'blok_arr', 'nir', 'blokpeta'));
        }

        // return count($cek); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_znt = $request->kd_znt;
        $messages = [
            'required' => ':attribute harus disertakan',
            'unique' => ':attribute sudah terdaftar dalam database',
            'numeric' => ':attribute harus angka',
            'digits' => ':attribute kudu :digits digits.',
            'date_format' => ':attribute tidak sesuai format, pastikan format :attribute adalah :format.',
        ];



        $validator = FacadesValidator::make(array_map('strtoupper', $request->all()), [
            'kd_znt' => 'required',
        ], $messages);
        if ($validator->fails()) {
            $msg = "";
            foreach ($validator->errors()->all() as $rk) {
                $msg .= $rk . ', ';
            }
            $msg = \substr($msg, '0', '-2');
            // dd($msg);
            $status = 0;
        } else {
            DB::beginTransaction();
            try {
                //code...
                /* $table = DB::connection('oracle_satutujuh')->table('dat_znt');
                // cek duplikasi
                $cek = $table->whereraw("kd_kecamatan='$kd_kecamatan'
                            and kd_kelurahan='$kd_kelurahan' and lower(kd_znt)=lower('$kd_znt')")
                    ->select(db::raw("count(1) res"))
                    ->first();
                if ($cek->res == 0) { */
                DB::connection('oracle_satutujuh')->table('dat_znt')
                    ->where([
                        'kd_propinsi' => '35',
                        'kd_dati2' => '07',
                        'kd_kecamatan' => $request->kd_kecamatan,
                        'kd_kelurahan' => $request->kd_kelurahan,
                        'kd_znt' => strtoupper($request->kd_znt)
                    ])
                    ->update([
                        'kd_propinsi' => '35',
                        'kd_dati2' => '07',
                        'kd_kecamatan' => $request->kd_kecamatan,
                        'kd_kelurahan' => $request->kd_kelurahan,
                        'kd_znt' => strtoupper($request->kd_znt),
                        'lokasi_objek_id' => $request->lokasi_objek_id,
                        'updated_by' => Auth()->user()->id,
                        'updated_at' => Carbon::now()
                    ]);
                DB::commit();
                $status = 1;
                $msg = "Berhasil di edit";
                /* } else {
                    $status = 0;
                    $msg = $kd_znt . ' Telah tersedia di dalam database';
                } */
            } catch (\Throwable $th) {

                db::rollBack();
                Log::emergency($th);
                $status = 0;
                $msg = $th->getMessage();
            }
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBlok($id)
    {

        // return $id;

        $cek = explode('_', $id);
        if (count($cek) == 4) {
            $kd_kecamatan = $cek[0];
            $kd_kelurahan = $cek[1];
            $kd_znt = $cek[2];
            $kd_blok = $cek[3];
            db::beginTransaction();
            try {
                $connection = DB::connection('oracle_satutujuh');
                $connection->table('DAT_PETA_ZNT')->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_znt', $kd_znt)
                    ->where('kd_blok', $kd_blok)
                    ->delete();
                db::commit();
                $flash = [
                    'success' => 'Berhasil menghapus BLOK'
                ];
            } catch (\Throwable $th) {
                //throw $th;
                db::rollBack();
                $flash = [
                    'error' => $th->getMessage()
                ];
            }

            return redirect(url('refrensi/znt') . '/' . $kd_kecamatan . '_' . $kd_kelurahan . '_' . $kd_znt)->with($flash);
        }
    }

    public function destroyNir($id)
    {
        $cek = explode('_', $id);
        if (count($cek) == 4) {
            $kd_kecamatan = $cek[0];
            $kd_kelurahan = $cek[1];
            $kd_znt = $cek[2];
            $thn_nir_znt = $cek[3];
            db::beginTransaction();
            try {
                $connection = DB::connection('oracle_satutujuh');
                $connection->table('DAT_NIR')->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('thn_nir_znt', $thn_nir_znt)
                    ->where('kd_znt', $kd_znt)->delete();
                db::commit();
                $flash = [
                    'success' => 'Berhasil menghapus NIR'
                ];
            } catch (\Throwable $th) {
                //throw $th;
                db::rollBack();
                $flash = [
                    'error' => $th->getMessage()
                ];
            }

            return redirect(url('refrensi/znt') . '/' . $kd_kecamatan . '_' . $kd_kelurahan . '_' . $kd_znt)->with($flash);
        }
    }

    public function destroy($id)
    {
        //
        $cek = explode('_', $id);
        if (count($cek) == 3) {
            $kd_kecamatan = $cek[0];
            $kd_kelurahan = $cek[1];
            $kd_znt = $cek[2];

            // hapus dat nir
            db::beginTransaction();
            try {
                $connection = DB::connection('oracle_satutujuh');

                $connection->table('DAT_NIR')->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_znt', $kd_znt)->delete();

                $connection->table('dat_peta_znt')->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_znt', $kd_znt)->delete();

                $connection->table('dat_znt')->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_znt', $kd_znt)->delete();

                db::commit();
                $flash = [
                    'success' => 'Berhasil menghapus ZNT'
                ];
            } catch (\Throwable $th) {
                //throw $th;
                db::rollBack();

                $flash = [
                    'error' => $th->getMessage()
                ];
            }

            return redirect(url('refrensi/znt') . '?kd_kecamatan=' . $kd_kecamatan . '&kd_kelurahan=' . $kd_kelurahan)->with($flash);
        }
    }
}
