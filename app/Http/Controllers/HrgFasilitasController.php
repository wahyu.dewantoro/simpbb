<?php

namespace App\Http\Controllers;

use App\Models\FasNonDep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use SnappyPdf as PDF;

class HrgFasilitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function coreSql($tahun)
    {
        return  DB::connection('oracle_satutujuh')->select(DB::raw("select fasilitas.*,(select nilai_non_dep*1000 
        from fas_non_dep
        where thn_non_dep='$tahun' and kd_fasilitas=fasilitas.kd_fasilitas) nilai 
        from fasilitas"));
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $fasilitas = $this->coreSql($tahun);
            $read = 1;
            return view('dbkb/hrg_fas/_form', compact('fasilitas', 'read'));
        }
        return view('dbkb/hrg_fas/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $fasilitas = $this->coreSql($tahun);
            $read = 0;
            return view('dbkb/hrg_fas/_form', compact('fasilitas', 'read'));
        }

        return view('dbkb/hrg_fas/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            //code...

            $arfas = $request->except(['thn_non_dep', '_method', '_token']);
            // $angka = [];
            foreach ($arfas as $i => $item) {
                FasNonDep::where([
                    'thn_non_dep' => $request->thn_non_dep,
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'kd_fasilitas' => str_replace('nilai_', '', $i)
                ])->delete();

                $angka = (int)$item / 1000;
                FasNonDep::create([
                    'thn_non_dep' => $request->thn_non_dep,
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'kd_fasilitas' => str_replace('nilai_', '', $i),
                    'nilai_non_dep' => $angka
                ]);
            }
            // dd($angka);

            $flash = ['success' => 'Berhasil memproses harga fasilitas pada tahun ' . $request->thn_non_dep];
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.harga-fasilitas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportPdf(Request $request)
    {
        // ?return $request->all();
        $tahun = $request->tahun;
        $fasilitas = $this->coreSql($tahun);
        $read = 1;
        $pages = view('dbkb/hrg_fas/exportPdf', compact('fasilitas', 'read','tahun'));
        $pdf = pdf::loadHTML($pages)
            ->setOption('enable-local-file-access', true)
            ->setPaper('A4');

        return $pdf->download();
    }
}
