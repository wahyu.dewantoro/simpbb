<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Billing_kolektif;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Helpers\gallade;

class DaftarNopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan','asc')->get();
        $kelurahan = [];
        if($kecamatan->count()=='1'){
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                    ->login()->orderBy('nm_kelurahan','asc')
                    ->select('nm_kelurahan', 'kd_kelurahan')
                    ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start=$tahun-5;
        for($year= $tahun;$year>=$start;$year--){
            $tahun_pajak[$year]=$year;
        }
        $result = ['data','data'];
        return view('dafnom.daftar_nop', compact('result','kecamatan','tahun_pajak','kelurahan'));
    }
    public function search(Request $request){
        $return=response()->json(['status'=>false,'msg'=>"Data tidak ditemukan.","data"=>[]]);
        $request=$request->only(['kecamatan','kelurahan','tahun_pajak','kode_billing','status']);
        $where['billing_kolektif.kd_kecamatan']=$request['kecamatan'];
        $where['billing_kolektif.tahun_pajak']=$request['tahun_pajak'];
        if($request['kode_billing']&&$request['kode_billing']!="-"){
            $where['data_billing.kobil']=$request['kode_billing'];
        }
        if($request['kelurahan']&&$request['kelurahan']!="-"){
            $where['billing_kolektif.kd_kelurahan']=$request['kelurahan'];
        }
        if($request['status']!==null&&$request['status']!="-"){
            $where['data_billing.kd_status']=$request['status'];
        }
        // $limit=15;
        // $page=0;
        $select=['billing_kolektif.*',
            'data_billing.nama_kecamatan',
            'data_billing.nama_kelurahan',
            'data_billing.created_at',
            'data_billing.kd_status',
            'data_billing.tgl_bayar',
            db::raw("data_billing.KD_PROPINSI||'.'||data_billing.KD_DATI2||'.'||data_billing.KD_KECAMATAN||'.'||data_billing.KD_KELURAHAN||'.'||data_billing.KD_BLOK||'-'||data_billing.NO_URUT||'.'||data_billing.KD_JNS_OP kobil")
            ];
        $getData=billing_kolektif::where($where)
                ->select($select)
                ->orderBy(db::raw('data_billing.created_at,billing_kolektif.no_urut'),'desc')
                ->join('data_billing', 'data_billing.data_billing_id', '=', 'billing_kolektif.data_billing_id')
                //->skip($page)->take($limit)
                ->get();
        $setData=[];
        if($getData->count()){
            $list=['no','kobil','nop','nama_wp','pbb','nama_kecamatan','nama_kelurahan','created_at','tgl_bayar','kd_status','aksi'];
            $no=1;
            $result=[];
            foreach($getData->toArray() as $item){
               //$btnExcel=gallade::anchorInfo($item['billing_kolektif_id'],'<i class="fas fa-file-excel"></i>','title="Cetak Excel" data-excel="true"');
                $disabled=($item['kd_status'])?"":'disabled'; 
                $btnPdf=gallade::anchorInfo('data_dafnom/pdf/'.$item['data_billing_id']."/".$item['billing_kolektif_id'],'<i class="fas fa-file-pdf"></i>','title="Cetak Pdf" data-pdf="true" ',$disabled);
                $result=[
                    'no'=> $no,
                    'kobil'=>$item['kobil'],
                    'nop'=>$item['kd_propinsi'].".".
                                $item['kd_dati2'].".".
                                $item['kd_kecamatan'].".".
                                $item['kd_kelurahan'].".".
                                $item['kd_blok']."-".
                                $item['no_urut'].".".
                                $item['kd_jns_op'],
                    'nama_wp'=>$item['nm_wp'],
                    'pbb'=>gallade::parseQuantity($item['pbb']),
                    'nama_kecamatan'=>$item['nama_kecamatan'],
                    'nama_kelurahan'=>$item['nama_kelurahan'],
                    'created_at'=>$item['created_at'],
                    'tgl_bayar'=>$item['tgl_bayar'], // v_dafnom:205
                    'kd_status'=>($item['kd_status'])?"<span class='btn btn-success'>Lunas</span>":"<span class='btn btn-warning'>Belum Lunas</span>",
                    'aksi'=>'<div class="btn-group">'.$btnPdf.'
                      </div>'
                ];
                $no++;
                $setData[]=Arr::only($result, $list);
            }
            //$setData=gallade::generateinTbody($setData,"Data Masih Kosong");
            $return=response()->json(["msg"=>"Pencarian data berhasil.","status"=>true,'data'=>$setData]);
        }
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
