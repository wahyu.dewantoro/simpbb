<?php

namespace App\Http\Controllers;

use App\Exports\Blok;
use App\Exports\DhkExport;
use App\Exports\Dhr;
use App\Exports\DhrMin;
use App\Exports\ListPendataan;
use App\Exports\NjopTurunExcel;
use App\Exports\Perbandingan_njop;
use App\Exports\NopNonAktif;
use App\Exports\Pci;
use App\Exports\PembayaranBlokir;
use App\Exports\PerbandinganDhkpExcell;
use App\Exports\RekapDhkpExcell;
use App\Exports\SimulasiKenaikanHkpdExcell;
use App\Helpers\Baku;
use App\Helpers\Dhkp;
use App\Helpers\gallade;
use App\Helpers\Layanan_conf;
use App\Helpers\Pajak;
use App\Helpers\PembatalanObjek;
use App\Helpers\PenetapanSppt;
use App\Imports\MsPotonganIndividuImport;
use App\Jobs\disposisiPenelitian;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Billing_kolektif;
use App\Models\DAT_OBJEK_PAJAK;
use App\Models\DAT_SUBJEK_PAJAK;
use App\Models\Data_billing;
use App\Models\Jenis_layanan;
use App\Models\JenisPendataan;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use App\Models\Notaperhitungan;
use App\Models\PendataanBatch;
use App\Models\PendataanObjek;
use App\Models\pengaktifanObjek;
use App\pegawaiStruktural;
use App\PembayaranMutasi;
use App\PembayaranMutasiData;
use App\PembayaranSppt;
use App\Sppt;
use App\SpptOltp;
use App\Traits\SpptKoreksiData;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use DataTables;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\View;
use Lcobucci\JWT\Token\DataSet;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\ErrorHandler\Collecting;
// use SnappyPdf as PDF;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
// use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yajra\DataTables\Facades\DataTables;

class LaporanAnalisaController extends Controller
{
    use SpptKoreksiData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function laporan_pendataan()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        if (!$kecamatan->count()) {
            $kecamatan = Kecamatan::orderBy('nm_kecamatan', 'asc')->get();
        }
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $pembuat = PendataanBatch::join('users', 'pendataan_batch.created_by', '=', 'users.id')->selectraw("distinct created_by,nama")->get();
        $jp = JenisPendataan::get();
        return view('analisa.laporan_pendataan', compact('result', 'kecamatan', 'kelurahan', 'jp', 'pembuat'));
    }
    private function PendataanBatch($request)
    {
        $result = PendataanBatch::ListPendataan()
            ->where(function ($query) use ($request) {
                if (in_array('status', array_keys($request))) {
                    if ($request['status'] != '-' && $request['status'] != null) {
                        if ($request['status'] == 'x') {
                            $query->whereraw("verifikasi_kode is null");
                        } else {
                            $query->whereraw("verifikasi_kode='" . $request['status'] . "'");
                        }
                    }
                }
                if (in_array('jenis_pendataan_id', array_keys($request))) {
                    if ($request['jenis_pendataan_id'] && $request['jenis_pendataan_id'] != '-' && $request['jenis_pendataan_id'] != null) {
                        $query->whereraw("jenis_pendataan_id='" . $request['jenis_pendataan_id'] . "'");
                    }
                }
                if (in_array('created_by', array_keys($request))) {
                    if ($request['created_by'] && $request['created_by'] != '-' && $request['created_by'] != null) {
                        $query->whereraw("pendataan_batch.created_by='" . $request['created_by'] . "'");
                    }
                }
                if (in_array('tanggal', array_keys($request))) {
                    if ($request['tanggal'] && $request['tanggal'] != '-' && $request['tanggal'] != null) {
                        $query->whereraw("trunc(pendataan_batch.created_at)= trunc(to_date('" . $request['tanggal'] . "','dd-mm-yyyy'))");
                    }
                }
            })->get();
        return $result;
    }
    public function laporan_pendataan_search(Request $request)
    {
        $search = $request->only('kecamatan', 'kelurahan', 'jenis_pendataan_id', 'status', 'created_by', 'tanggal');
        $dataSet = $this->PendataanBatch($search);
        $datatables = DataTables::of($dataSet);
        return $datatables->addIndexColumn()
            ->addColumn('tanggal_indo', function ($row) {
                return tglIndo($row->tanggal);
            })
            ->addColumn('raw_tag', function ($row) {
                $list = [
                    '1' => "<i class='far fa-check-circle text-success'></i> Disetujui",
                    '2' => "<i class='far fa-check-circle text-success'></i> Verifikasi PETA",
                    '0' => "<i class='fas fa-exclamation-circle text-danger'></i> Tidak Disetujui",
                    "" => "<i class='fas fa-user-clock text-warning'></i> Belum di verifikasi"
                ];
                $status = "<i class='fas fa-user-clock text-warning'></i> Belum di verifikasi";
                if (in_array($row->verifikasi_kode, array_keys($list))) {
                    $status = $list[$row->verifikasi_kode];
                }
                return $status;
            })->make(true);
    }
    public function laporan_pendataan_cetak(Request $request)
    {

        try {
            $search = $request->only('kecamatan', 'kelurahan', 'jenis_pendataan_id', 'status', 'created_by', 'tanggal');
            $data = $this->PendataanBatch($search);
            // dd($data->toArray());
            $search = [
                'jenis' => 'Semua',
                'status' => 'Semua',
                'pegawai' => 'Seluruh Pegawai',
                'tanggal' => 'Semua',
            ];
            if ($request->has('status')) {
                if ($request->status && $request->status != '-') {
                    $list = [
                        "x" => "Belum di verifikasi",
                        "2" => "Verifikasi Peta",
                        "1" => "Disetujui",
                        "0" => "Tidak Disetujui"
                    ];
                    if (in_array($request->status, array_keys($list))) {
                        $search['status'] = $list[$request->status];
                    }
                }
            }
            if ($request->has('jenis_pendataan_id')) {
                if ($request->jenis_pendataan_id && $request->jenis_pendataan_id != '-') {
                    $list = [
                        "1" => "SISMIOP",
                        "2" => "Mutasi Pecah",
                        "3" => "Penilaian Individu",
                        "4" => "Pendataan Baru",
                        "5" => "Pembatalan",
                        "6" => "Pembetulan"
                    ];
                    if (in_array($request->jenis_pendataan_id, array_keys($list))) {
                        $search['jenis'] = $list[$request->jenis_pendataan_id];
                    }
                }
            }
            if ($request->has('created_by')) {
                if ($request->created_by && $request->created_by != '-') {
                    $getPegawai = User::select(['nama'])->where(['id' => $request->created_by])->get();
                    if ($getPegawai->count()) {
                        $search['pegawai'] = $getPegawai->first()->nama;
                    }
                }
            }
            if ($request->has('tanggal')) {
                if ($request->tanggal && $request->tanggal != '-') {
                    $search['tanggal'] = $request->tanggal;
                }
            }
            $export = new ListPendataan($data->toArray(), $search);
            return Excel::download($export, 'daftar_nop_pendataan.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    public function pci()
    {
        $result = [];
        return view('analisa.pci', compact('result'));
    }
    public function pci_search(Request $request, $pageNumber = 1)
    {
        $nik = $request->only('nik');
        $dataSet = [];
        if ($nik) {
            if ($nik['nik'] != '') {
                $cn = onlyNumber($nik['nik']);
                $cw = preg_replace("/[^a-zA-Z ]+/", "", strtolower($nik['nik']));
                $wh = "";
                if ($cn != '') {
                    $wh .= " subjek_pajak_id like '%" . $cn . "%' or";
                }
                if ($cw != "") {
                    $wh .= " lower(trim(nm_wp)) like '%" . $cw . "%' or";
                }
                $wh = substr($wh, 0, -2);
                $where = "(" . $wh . ")";
                $select = [
                    'subjek_pajak_id',
                    'nm_wp',
                    'jalan_wp',
                    'rt_wp',
                    'rw_wp',
                    'kelurahan_wp',
                    'kota_wp'
                ];
                $objek_pajak_select = 'subjek_pajak_id,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,njop_bumi,njop_bng,jalan_op';
                $dataSet = DAT_SUBJEK_PAJAK::whereRaw($where)
                    ->select($select)
                    ->with(['objek_pajak:' . $objek_pajak_select])
                    ->orderby('subjek_pajak_id', 'asc')
                    ->get();
                $listResult = [];
                $dataSet->transform(function ($set) use ($listResult) {
                    if ($set->objek_pajak) {
                        $mergeData = [];
                        foreach ($set->objek_pajak->toArray() as $item) {
                            $getPembayaran = $this->getPembayaran([
                                $item['kd_propinsi'],
                                $item['kd_dati2'],
                                $item['kd_kecamatan'],
                                $item['kd_kelurahan'],
                                $item['kd_blok'],
                                $item['no_urut'],
                                $item['kd_jns_op'],
                            ]);
                            $mergeData[] = array_merge($item, Collect($set)->except('objek_pajak')->toArray(), (array)$getPembayaran);
                        }
                        return $mergeData;
                    }
                });
                $dataSet = $dataSet->filter()->flatten(1);
            }
        }
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->addColumn('raw_tag', function ($data) {
                return '<small>' . $data['nm_wp'] . '<br>' . trim($data['subjek_pajak_id']) . '<br> <span class="text-bold">' .
                    $data['jalan_wp'] . '  RT ' . $data['rt_wp'] . ' RW ' . $data['rw_wp'] . ' ,'  . $data['kelurahan_wp'] . ',' . $data['kota_wp'] . '</span></small>';
                // return '<p class="p-0 m-0">NIK : ' . $data['subjek_pajak_id'] . '</p>' .
                //     '<p class="p-0 m-0">NAMA : ' . $data['nm_wp'] . '</p>';
            })
            ->addIndexColumn()
            ->make(true);
    }
    private function getPembayaran($nop)
    {
        $sppt = DB::connection('oracle_dua')->table('sppt')
            ->selectraw("thn_pajak_sppt,nm_wp_sppt, luas_bumi_sppt, njop_bumi_sppt, luas_bng_sppt, njop_bng_sppt, pbb_yg_harus_dibayar_sppt pbb ")
            ->whereraw("kd_propinsi ='" . $nop[0] . "'")
            ->whereraw("kd_dati2 ='" . $nop[1] . "'")
            ->whereraw("kd_kecamatan ='" . $nop[2] . "'")
            ->whereraw("kd_kelurahan ='" . $nop[3] . "'")
            ->whereraw("kd_blok ='" . $nop[4] . "'")
            ->whereraw("no_urut ='" . $nop[5] . "'")
            ->whereraw("kd_jns_op ='" . $nop[6] . "' and  thn_pajak_sppt>=2014")
            ->orderby('THN_PAJAK_SPPT')->get();
        if (!$sppt->count()) {
            return [];
        }
        $result = $sppt->last();
        $yearLenght = Carbon::now()->year;
        $yearSppt = $result->thn_pajak_sppt + 1;
        $yearList = [];
        while ($yearSppt <= $yearLenght) {
            $yearList[] = $yearSppt;
            $yearSppt++;
        }
        $status = 'Lancar';
        if (count($yearList) >= 1) {
            $status = 'Kurang Lancar';
            if (count($yearList) >= 3) {
                $status = 'Tidak Lancar';
            }
        }
        $result->keterangan = join(',', $yearList);
        $result->status = $status;
        $getkelkecOP = DB::connection('oracle_dua')->table('ref_kelurahan')
            ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
            })
            ->where(['ref_kelurahan.kd_kecamatan' => $nop[2]])
            ->where(['ref_kelurahan.kd_kelurahan' => $nop[3]])
            ->get()->first();
        $result->nm_kelurahan = $getkelkecOP->nm_kelurahan;
        $result->nm_kecamatan = $getkelkecOP->nm_kecamatan;
        return $result;
    }
    public function pci_cetak(Request $request)
    {
        $nik = $request->only('nik');
        $dataSet = [];
        if ($nik) {


            $cn = onlyNumber($nik['nik']);
            $cw = preg_replace("/[^a-zA-Z ]+/", "", strtolower($nik['nik']));
            $wh = "";
            if ($cn != '') {
                $wh .= " subjek_pajak_id like '%" . $cn . "%' or";
            }
            if ($cw != "") {
                $wh .= " lower(nm_wp) like '%" . $cw . "%' or";
            }
            $wh = substr($wh, 0, -2);
            $where = "(" . $wh . ")";
            $select = [
                'subjek_pajak_id',
                'nm_wp',
                'jalan_wp',
                'kelurahan_wp',
                'kota_wp',
                'rt_wp',
                'rw_wp'
            ];
            $objek_pajak_select = 'subjek_pajak_id,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,njop_bumi,njop_bng,jalan_op';
            $dataSet = DAT_SUBJEK_PAJAK::whereRaw($where)
                ->select($select)
                ->with(['objek_pajak:' . $objek_pajak_select])
                ->orderbyraw("trim(nm_wp) asc")
                ->get();

            //  dd($dataSet)   ;
            $listResult = [];
            $dataSet->transform(function ($set) use ($listResult) {
                if ($set->objek_pajak) {
                    $mergeData = [];
                    foreach ($set->objek_pajak->toArray() as $key => $item) {
                        $getPembayaran = $this->getPembayaran([
                            $item['kd_propinsi'],
                            $item['kd_dati2'],
                            $item['kd_kecamatan'],
                            $item['kd_kelurahan'],
                            $item['kd_blok'],
                            $item['no_urut'],
                            $item['kd_jns_op'],
                        ]);
                        if ($getPembayaran) {
                            $resultMerge = array_merge($item, Collect($set)->except('objek_pajak')->toArray(), (array)$getPembayaran);
                            $resultMerge['nop'] = $item['kd_propinsi'] . "." .
                                $item['kd_dati2'] . "." .
                                $item['kd_kecamatan'] . "." .
                                $item['kd_kelurahan'] . "." .
                                $item['kd_blok'] . "-" .
                                $item['no_urut'] . "." .
                                $item['kd_jns_op'];
                            $mergeData[] = $resultMerge;
                        }
                    }
                    return $mergeData;
                }
            });
            $dataSet = $dataSet->filter()->flatten(1);
        }
        // dd($dataSet->toArray());
        $export = new Pci($dataSet->toArray());
        return Excel::download($export, 'laporan_pci.xlsx');
    }
    public function pembayaran_terblokir(Request $request)
    {
        if ($request->ajax()) {
            $jenis = $request->jenis;
            $kd_kategori = $request->kd_kategori;

            if ($kd_kategori == '00') {
                $jenis = 'KP';
                $kd_kategori = '';
            }


            $data = $this->listKoreksiPiutang($jenis);
            // ->whereraw("sppt_koreksi.no_transaksi like 'KP%'");
            $search = Strtolower($request->search);
            $tahun_sppt = $request->tahun ?? '';
            $tahun_proses = $request->tahun_proses ?? '';


            if ($kd_kategori != '') {
                $data = $data->whereraw("kd_kategori='$kd_kategori'");
            }

            if ($tahun_sppt != '') {
                $data = $data->whereraw("sppt_koreksi.thn_pajak_sppt ='$tahun_sppt' ");
            }

            if ($tahun_proses != '') {
                $data = $data->whereraw("to_char(sppt_koreksi.tgl_surat,'yyyy') ='$tahun_proses' ");
            }

            if ($search != '') {
                $angka = onlyNumber($search);
                $wh = "lower(no_surat) like '%$search%' or lower(no_transaksi) like '%$search%' or lower(nm_kategori) like '%$search%' or lower(keterangan) like '%$search%' ";
                if ($angka) {
                    $wh .= "or sppt_koreksi.kd_propinsi||sppt_koreksi.kd_dati2||sppt_koreksi.kd_kecamatan||sppt_koreksi.kd_kelurahan||sppt_koreksi.kd_blok||sppt_koreksi.no_urut||sppt_koreksi.kd_jns_op like '%$angka%'";
                }
                $data = $data->whereraw('(' . $wh . ')');
            }
            $data = $data->orderByRaw("sppt_koreksi.created_at desc");
            return  DataTables::of($data)
                ->addColumn('aksi', function ($row) use ($jenis) {

                    if ($jenis == '2') {
                        $buka = "buka_empat";
                    } else {
                        $buka = "buka";
                    }

                    $btn = '<button data-kd_propinsi="' . $row->kd_propinsi . '" data-kd_dati2="' . $row->kd_dati2 . '" data-kd_kecamatan="' . $row->kd_kecamatan . '" data-kd_kelurahan="' . $row->kd_kelurahan . '" data-kd_blok="' . $row->kd_blok . '" data-no_urut="' . $row->no_urut . '" data-kd_jns_op="' . $row->kd_jns_op . '" data-thn_pajak_sppt="' . $row->thn_pajak_sppt . '" class="' . $buka . ' btn btn-sm btn-warning"><i class="far fa-trash-alt"></i></button>';

                    return $btn;
                })
                ->editcolumn('jns_koreksi', function ($row) {
                    // return 'Blokir Pembayaran';
                    /* switch ($row->jns_koreksi) {
                        case '3':
                            # code...
                            $var = "Koreksi Piu";
                            break;
                        case '3':
                                # code...
                                $var = "Koreksi Piutang";
                                break;
    
                        default:
                            # code...
                            $var = "Blokir Pembayaran";
                            break;
                    }
                    return $var; */
                })
                ->editcolumn('verifikasi_kode', function ($row) {

                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $res = "Telah di setujui";

                            break;
                        case '0':
                            # code...
                            $res = "Tidak di setujui";
                            break;
                        default:
                            # code...
                            $res = "Belum di verifikasi";
                            break;
                    }

                    return $res;
                })
                ->addColumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '.' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->editColumn('keterangan', function ($row) {
                    if ($row->keterangan == '') {
                        $ket = '-';
                    } else {
                        $ket = $row->keterangan;
                    }
                    return $ket;
                })

                ->editColumn('nm_kategori', function ($row) {

                    if ($row->nm_kategori == '') {
                        $ket = $row->keterangan;
                    } else {
                        $ket = $row->nm_kategori;
                    }
                    return $ket;
                })

                ->addColumn('surat', function ($row) {

                    return $row->no_surat . '<br><small>' . tglIndo($row->tgl_surat) . '</smal>';
                })
                ->rawColumns(['aksi', 'jns_koreksi', 'verifikasi_kode', 'keterangan', 'surat', 'nop'])->make(true);
        }

        return view('analisa.pembayaran_terblokir');
    }


    public function pembayaranblokirverifikasi(Request $request)
    {
        if ($request->ajax()) {
            $data = db::connection("oracle_satutujuh")->table('sppt_Koreksi')
                ->select(db::raw("no_transaksi,no_surat,tgl_surat,keterangan,verifikasi_kode,trunc(created_at) tanggal,verifikasi_keterangan,jns_koreksi,count(1) jum_op "))
                ->whereraw("jns_koreksi='3'")
                ->whereraw("verifikasi_at is null")
                ->groupByraw("no_transaksi,no_surat,tgl_surat,keterangan,verifikasi_kode,trunc(created_at),verifikasi_keterangan,jns_koreksi")
                ->orderByRaw("trunc(created_at) desc,no_transaksi desc");

            $search = Strtolower($request->search);
            if ($search != '') {
                $wh = "lower(no_surat) like '%$search%' or lower(no_transaksi) like '%$search%' ";
                $data = $data->whereraw('(' . $wh . ')');
            }

            return  DataTables::of($data)
                ->addColumn('aksi', function ($row) {
                    // return '<button data-kd_propinsi="' . $row->kd_propinsi . '" data-kd_dati2="' . $row->kd_dati2 . '" data-kd_kecamatan="' . $row->kd_kecamatan . '" data-kd_kelurahan="' . $row->kd_kelurahan . '" data-kd_blok="' . $row->kd_blok . '" data-no_urut="' . $row->no_urut . '" data-kd_jns_op="' . $row->kd_jns_op . '" data-thn_pajak_sppt="' . $row->thn_pajak_sppt . '" class="buka btn btn-sm btn-warning"><i class="fas fa-book-reader"></i></button>';
                    return '<a href="' . route('pembayaran.blokir.verifikasi-form', $row->no_transaksi) . '"><i class="fas fa-edit text-success"></i></a>';
                })
                ->editcolumn('tanggal', function ($row) {
                    return tglIndo($row->tanggal);
                })
                ->editcolumn('tgl_surat', function ($row) {
                    return tglIndo($row->tgl_surat);
                })
                ->editcolumn('verifikasi_kode', function ($row) {

                    switch ($row->verifikasi_kode) {
                        case '1':
                            # code...
                            $res = "Telah di setujui";

                            break;
                        case '0':
                            # code...
                            $res = "Tidak di setujui";
                            break;
                        default:
                            # code...
                            $res = "Belum di verifikasi";
                            break;
                    }

                    return $res;
                })
                ->rawColumns(['tanggal', 'aksi', 'tgl_surat', 'verifikasi_kode'])->make(true);
        }

        return view('analisa.pembayaran_terblokir_verifikasi');
    }


    public function pembayaranTerblokirDetail(Request $request, $trx)
    {
        if ($request->ajax()) {
            $data = $this->getPembayaranTerblokir()->whereraw("no_transaksi='$trx'");
            $search = Strtolower($request->search);
            if ($search != '') {
                $angka = onlyNumber($search);
                $wh = "lower(no_surat) like '%$search%' or lower(nm_wp_sppt) like '%$search%'";
                if ($angka) {
                    $wh .= "or sppt.kd_propinsi||sppt.kd_dati2||sppt.kd_kecamatan||sppt.kd_kelurahan||sppt.kd_blok||sppt.no_urut||sppt.kd_jns_op like '%$angka%'";
                }
                $data = $data->whereraw('(' . $wh . ')');
            }
            return  DataTables::of($data)
                ->addColumn('aksi', function ($row) {
                    return '<button data-kd_propinsi="' . $row->kd_propinsi . '" data-kd_dati2="' . $row->kd_dati2 . '" data-kd_kecamatan="' . $row->kd_kecamatan . '" data-kd_kelurahan="' . $row->kd_kelurahan . '" data-kd_blok="' . $row->kd_blok . '" data-no_urut="' . $row->no_urut . '" data-kd_jns_op="' . $row->kd_jns_op . '" data-thn_pajak_sppt="' . $row->thn_pajak_sppt . '" class="buka btn btn-sm btn-warning"><i class="fas fa-book-reader"></i></button>';
                })
                ->addcolumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->addcolumn('wajib_pajak', function ($row) {
                    return $row->nm_wp_sppt;
                })
                ->editcolumn('pbb_akhir', function ($row) {
                    return angka($row->pbb_akhir);
                })
                ->rawColumns(['wajib_pajak', 'nop', 'pbb_akhir', 'aksi'])->make(true);
        }

        $data = $this->listKoreksiPiutang()->whereraw("no_transaksi='$trx'")->first();
        return view('analisa.pembayaran_terblokir_show', compact('data', 'trx'));
    }

    public function pembayaranblokirnop(Request $request)
    {
        if ($request->ajax()) {

            $data = $this->getPembayaranTerblokir();
            $nop = $request->nop ?? '';
            if ($nop != '') {
                $res = onlyNumber($nop);
                $kd_propinsi = substr($res, 0, 2);
                $kd_dati2 = substr($res, 2, 2);
                $kd_kecamatan = substr($res, 4, 3);
                $kd_kelurahan = substr($res, 7, 3);
                $kd_blok = substr($res, 10, 3);
                $no_urut = substr($res, 13, 4);
                $kd_jns_op = substr($res, 17, 1);
                $data = $data->whereraw("sppt.kd_propinsi='" . $kd_propinsi . "' and 
                sppt.kd_dati2='" . $kd_dati2 . "' and 
                sppt.kd_kecamatan='" . $kd_kecamatan . "' and 
                sppt.kd_kelurahan='" . $kd_kelurahan . "' and 
                sppt.kd_blok='" . $kd_blok . "' and 
                sppt.no_urut='" . $no_urut . "' and 
                sppt.kd_jns_op='" . $kd_jns_op . "'");
            }

            return  DataTables::of($data)
                ->addColumn('aksi', function ($row) {
                    return '<button data-kd_propinsi="' . $row->kd_propinsi . '" data-kd_dati2="' . $row->kd_dati2 . '" data-kd_kecamatan="' . $row->kd_kecamatan . '" data-kd_kelurahan="' . $row->kd_kelurahan . '" data-kd_blok="' . $row->kd_blok . '" data-no_urut="' . $row->no_urut . '" data-kd_jns_op="' . $row->kd_jns_op . '" data-thn_pajak_sppt="' . $row->thn_pajak_sppt . '" class="buka btn btn-sm btn-warning"><i class="fas fa-book-reader"></i></button>';
                })
                ->addcolumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->addcolumn('wajib_pajak', function ($row) {
                    return $row->nm_wp_sppt;
                })
                ->addcolumn('data_sk', function ($row) {
                    return $row->no_surat . '<br> <span class="text-success">' . tglIndo($row->tgl_surat) . '</span>';
                })
                ->editcolumn('pbb_akhir', function ($row) {
                    return angka($row->pbb_akhir);
                })
                ->rawColumns(['wajib_pajak', 'nop', 'pbb_akhir', 'data_sk', 'aksi'])->make(true);
        }
        return view('analisa.pembayaran_terblokir_nop');
    }


    public function pembayaranblokirverifikasiForm(Request $request, $trx)
    {

        $data = $this->listKoreksiPiutang()->whereraw("no_transaksi='$trx'")->first();
        $rekap = $this->RekapBlokirFormulir($trx);
        return view('analisa.pembayaran_terblokir_show_form', compact('data', 'trx', 'rekap'));
    }

    private function Notransaksi($blokir = 0)
    {

        switch ($blokir) {
            case '1':
                # code...
                $kode = "IB";
                break;
            case '2':
                # code...
                $kode = "IV";
                break;

            default:
                # code...
                $kode = "KP";
                break;
        }

        return  DB::connection("oracle_satutujuh")->table("sppt_koreksi")
            ->select(db::raw("'" . $kode . "'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'" . $kode . "'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
            ->whereraw("no_transaksi like '" . $kode . "'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi;
    }

    public function  pembayaran_terblokir_store(Request $request)
    {
        // return $request->all();
        $res = onlyNumber($request->nop);
        $data = $request->only(
            [
                'thn_pajak_sppt',
                'no_surat',
                'tgl_surat',
                'jns_koreksi',
                'keterangan'
            ]
        );
        $tgl = new Carbon($request->tgl_surat);

        $data['tahun'] = $tgl->format('Y');
        $data['kd_propinsi'] = substr($res, 0, 2);
        $data['kd_dati2'] = substr($res, 2, 2);
        $data['kd_kecamatan'] = substr($res, 4, 3);
        $data['kd_kelurahan'] = substr($res, 7, 3);
        $data['kd_blok'] = substr($res, 10, 3);
        $data['no_urut'] = substr($res, 13, 4);
        $data['kd_jns_op'] = substr($res, 17, 1);

        $arrayBatal = ['01', '02', '03'];
        if (in_array($request->jns_koreksi, $arrayBatal)) {
            $res = PembatalanObjek::iventarisasi($data);
        } elseif ($request->jns_koreksi == '07') {
            $res = PembatalanObjek::penyesuaian($data);
        } else {
            $res = PembatalanObjek::cancel($data);
        }

        // return $data;
        return redirect(url('pendataan/penangguhan'))->with(['success' => "Telah diproses"]);
    }

    public function bukaBlokir(Request $request)
    {
        $where = $request->except(['_token']);
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...
            $upd['status_pembayaran_sppt'] = '0';
            $cs = DB::connection("oracle_satutujuh")->table("sppt")
                ->select('tgl_terbit_sppt')->where($where)->first();

            if ($cs->tgl_terbit_sppt == '') {
                $upd['tgl_terbit_sppt'] = Carbon::now();
            }

            DB::connection("oracle_satutujuh")->table('sppt_koreksi')->where($where)->delete();
            DB::connection("oracle_satutujuh")->table('sppt')->where($where)->update($upd);
            DB::connection("oracle_satutujuh")->commit();
            $res['msg'] = "Berhasil di buka";
        } catch (\Throwable $th) {
            //throw $th;
            db::connection("oracle_satutujuh")->rollBack();
            $res['msg'] = $th->getMessage();
        }
        return $res;
    }

    public function bukaBlokirMasal(Request $request)
    {
        return $this->bukaBlokir($request);
    }

    public function blokirUpload(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'max:2048|required',
        ]);

        $blokir = $request->blokir ?? 0;

        $blokir = $request->blokir ?? 0;
        switch ($blokir) {
            case '2':
                # code...
                $jns_koreksi = '2';
                break;
            default:
                # code...
                $jns_koreksi = '3';
                break;
        }

        $status = 1;
        $msg = "Data tidak di proses";
        if ($validator->fails()) {
            $msg = "";
            foreach ($validator->errors()->all() as $rk) {
                $msg .= $rk . ', ';
            }
            $msg = \substr($msg, '0', '-2');
            $status = 0;
        }

        $file = $request->file('file');
        $path = $file->getRealPath();
        $ext = $file->extension();
        // return $ext;
        if (!in_array($ext, ["xlsx", "xls"])) {
            $msg = "Format tidak di perbolehkan";
            $status = 0;
        }

        if ($status == '1') {
            $array = Excel::toArray(new MsPotonganIndividuImport(), $file);
            $keterangan = $request->keterangan;
            $trx = $this->Notransaksi($blokir);
            $data = [];
            $sqlsppt = "";
            $sql_cek = "";

            foreach ($array[0] as $item) {
                $res = onlyNumber($item['nop']);
                if ($res != '') {
                    $thn_pajak_sppt = onlyNumber($item['tahun']);
                    $var['kd_propinsi'] = substr($res, 0, 2);
                    $var['kd_dati2'] = substr($res, 2, 2);
                    $var['kd_kecamatan'] = substr($res, 4, 3);
                    $var['kd_kelurahan'] = substr($res, 7, 3);
                    $var['kd_blok'] = substr($res, 10, 3);
                    $var['no_urut'] = substr($res, 13, 4);
                    $var['kd_jns_op'] = substr($res, 17, 1);
                    $var['thn_pajak_sppt'] = $thn_pajak_sppt;

                    $oltp = SpptOltp::where($var)->selectraw("pbb_yg_harus_dibayar_sppt pbb")->first();
                    $kd_propinsi = $var['kd_propinsi'];
                    $kd_dati2 = $var['kd_dati2'];
                    $kd_kecamatan = $var['kd_kecamatan'];
                    $kd_kelurahan = $var['kd_kelurahan'];
                    $kd_blok = $var['kd_blok'];
                    $no_urut = $var['no_urut'];
                    $kd_jns_op = $var['kd_jns_op'];
                    $sql_cek .= " select '$kd_propinsi' kd_propinsi,'$kd_dati2' kd_dati2,'$kd_kecamatan' kd_kecamatan,'$kd_kelurahan' kd_kelurahan,'$kd_blok' kd_blok,'$no_urut' no_urut,'$kd_jns_op' kd_jns_op,'$thn_pajak_sppt' thn_pajak_sppt ,'" . $oltp->pbb . "' nilai_koreksi from dual union";

                    $sqlsppt .= "update sppt set status_pembayaran_sppt='3' where thn_pajak_sppt='" . $thn_pajak_sppt . "'  and kd_kecamatan='" . $var['kd_kecamatan'] . "' and kd_kelurahan='" . $var['kd_kelurahan'] . "'  and kd_blok='" . $var['kd_blok'] . "' and no_urut='" . $var['no_urut'] . "' and kd_jns_op='" . $var['kd_jns_op'] . "'; ";

                    $ins = $var;
                    $ins['tgl_surat'] = new Carbon($request->tgl_surat);
                    $ins['no_surat'] = $request->no_surat;
                    $ins['jns_koreksi'] = $jns_koreksi;
                    $ins['keterangan'] = $keterangan;
                    $ins['created_at'] = db::raw("sysdate");
                    $ins['created_by'] = Auth()->user()->id;
                    $ins['no_transaksi'] = $trx;
                    $ins['verifikasi_kode'] = 1;
                    $ins['verifikasi_keterangan'] = $keterangan;
                    $ins['verifikasi_by'] = auth()->user()->id;
                    $ins['verifikasi_at'] = db::raw("sysdate");
                    $data[] = $ins;
                }
            }

            DB::connection("oracle_satutujuh")->beginTransaction();
            try {

                if ($sql_cek <> '') {
                    $sql_cek = substr($sql_cek, 0, -5);
                    DB::connection("oracle_satutujuh")
                        ->statement(db::raw(" begin 
                    insert into sppt_koreksi
                        select a.kd_propinsi,
                        a.kd_dati2,
                        a.kd_kecamatan,
                        a.kd_kelurahan,
                        a.kd_blok,
                        a.no_urut,
                        a.kd_jns_op,
                        a.thn_pajak_sppt,
                        '" . $keterangan . "' keterangan,
                            sysdate created_at,
                        '" . Auth()->user()->id . "' created_by,
                        '" . $request->no_surat . "' no_surat,
                        '" . new Carbon($request->tgl_surat) . "' tgl_surat,
                        sysdate verifikasi_at,
                        '" . Auth()->user()->id . "' verifikasi_by,
                        '1' verifikasi_kode,
                        '" . $keterangan . "'verifikasi_keterangan,
                        '" . $trx . "' no_transaksi,
                        '$jns_koreksi' jns_koreksi,
                        a.nilai_koreksi
                        from (" . $sql_cek . ") a
                        left join sppt_koreksi b on 
                        a.kd_propinsi=b.kd_propinsi and 
                        a.kd_dati2=b.kd_dati2 and 
                        a.kd_kecamatan=b.kd_kecamatan and 
                        a.kd_kelurahan=b.kd_kelurahan and 
                        a.kd_blok=b.kd_blok and 
                        a.no_urut=b.no_urut and 
                        a.kd_jns_op=b.kd_jns_op and 
                        a.thn_pajak_sppt=b.thn_pajak_sppt
                        where b.thn_pajak_sppt is null; end;"));
                }



                if ($sqlsppt <> '') {
                    DB::connection("oracle_satutujuh")->statement(db::raw("begin " . $sqlsppt . " end;"));
                }

                DB::connection("oracle_satutujuh")->commit();
                $status = 1;
                $msg = "Data berhasil di upload";
            } catch (\Throwable $th) {
                DB::connection("oracle_satutujuh")->rollBack();
                $status = 0;
                $msg = 'Gagal di upload, ada beberapa nop yang sudah di koreksi';
                Log::error($th);
                // dd($th);
            }
        }

        return ['status' => $status, 'msg' => $msg];
    }

    public function pembayaranblokirverifikasiFormStore(Request $request, $trx)
    {
        // return $request->all();

        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            //code...
            DB::connection("oracle_satutujuh")->table('sppt_koreksi')->where('no_transaksi', $trx)
                ->update([
                    'verifikasi_kode' => $request->verifikasi_kode,
                    'verifikasi_keterangan' => $request->verifikasi_keterangan,
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_at' => db::raw("sysdate")
                ]);

            DB::connection('oracle_satutujuh')->commit();
            $flash['success'] = 'Berhasil di veririkasi';
        } catch (\Throwable $th) {

            DB::connection('oracle_satutujuh')->rollBack();
            $flash['error'] = $th->getMessage();
        }

        return redirect(route('pembayaran.blokir.verifikasi'))->with($flash);
    }

    private function getPembayaranTerblokir()
    {
        return DB::connection("oracle_satutujuh")
            ->table("sppt")
            ->join("sppt_koreksi", function ($join) {
                $join->on("sppt.kd_propinsi", "=", "sppt_koreksi.kd_propinsi")
                    ->on("sppt.kd_dati2", "=", "sppt_koreksi.kd_dati2")
                    ->on("sppt.kd_kecamatan", "=", "sppt_koreksi.kd_kecamatan")
                    ->on("sppt.kd_kelurahan", "=", "sppt_koreksi.kd_kelurahan")
                    ->on("sppt.kd_blok", "=", "sppt_koreksi.kd_blok")
                    ->on("sppt.no_urut", "=", "sppt_koreksi.no_urut")
                    ->on("sppt.kd_jns_op", "=", "sppt_koreksi.kd_jns_op")
                    ->on("sppt.thn_pajak_sppt", "=", "sppt_koreksi.thn_pajak_sppt");
            })
            ->leftjoin("sppt_potongan", function ($join) {
                $join->on("sppt.kd_propinsi", "=", "sppt_potongan.kd_propinsi")
                    ->on("sppt.kd_dati2", "=", "sppt_potongan.kd_dati2")
                    ->on("sppt.kd_kecamatan", "=", "sppt_potongan.kd_kecamatan")
                    ->on("sppt.kd_kelurahan", "=", "sppt_potongan.kd_kelurahan")
                    ->on("sppt.kd_blok", "=", "sppt_potongan.kd_blok")
                    ->on("sppt.no_urut", "=", "sppt_potongan.no_urut")
                    ->on("sppt.kd_jns_op", "=", "sppt_potongan.kd_jns_op")
                    ->on("sppt.thn_pajak_sppt", "=", "sppt_potongan.thn_pajak_sppt");
            })

            ->select(db::raw("sppt.kd_propinsi,
                            sppt.kd_dati2,
                            sppt.kd_kecamatan,
                            sppt.kd_kelurahan,
                            sppt.kd_blok,
                            sppt.no_urut,
                            sppt.kd_jns_op,
                            nm_wp_sppt,
                            sppt.thn_pajak_sppt,
                            pbb_yg_harus_dibayar_sppt pbb,
                            nvl(nilai_potongan,0) potongan, 
                            pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0) pbb_akhir,
                            sppt_koreksi.tgl_surat,
                            sppt_koreksi.no_surat,
                            sppt_koreksi.keterangan"));
    }

    private function RekapBlokirFormulir($trx)
    {
        return  DB::connection("oracle_satutujuh")
            ->table("sppt")
            ->leftjoin("sppt_potongan", function ($join) {
                $join->on("sppt.kd_propinsi", "=", "sppt_potongan.kd_propinsi")
                    ->on("sppt.kd_dati2", "=", "sppt_potongan.kd_dati2")
                    ->on("sppt.kd_kecamatan", "=", "sppt_potongan.kd_kecamatan")
                    ->on("sppt.kd_kelurahan", "=", "sppt_potongan.kd_kelurahan")
                    ->on("sppt.kd_blok", "=", "sppt_potongan.kd_blok")
                    ->on("sppt.no_urut", "=", "sppt_potongan.no_urut")
                    ->on("sppt.kd_jns_op", "=", "sppt_potongan.kd_jns_op")
                    ->on("sppt.thn_pajak_sppt", "=", "sppt_potongan.thn_pajak_sppt");
            })
            ->leftjoin("sppt_koreksi", function ($join) {
                $join->on("sppt.kd_propinsi", "=", "sppt_koreksi.kd_propinsi")
                    ->on("sppt.kd_dati2", "=", "sppt_koreksi.kd_dati2")
                    ->on("sppt.kd_kecamatan", "=", "sppt_koreksi.kd_kecamatan")
                    ->on("sppt.kd_kelurahan", "=", "sppt_koreksi.kd_kelurahan")
                    ->on("sppt.kd_blok", "=", "sppt_koreksi.kd_blok")
                    ->on("sppt.no_urut", "=", "sppt_koreksi.no_urut")
                    ->on("sppt.kd_jns_op", "=", "sppt_koreksi.kd_jns_op")
                    ->on("sppt.thn_pajak_sppt", "=", "sppt_koreksi.thn_pajak_sppt");
            })
            ->select(db::raw("sppt.thn_pajak_sppt, sum(pbb_yg_harus_dibayar_sppt - NVL(nilai_potongan, 0)) pbb_akhir"))
            ->groupby("sppt.thn_pajak_sppt")
            ->whereraw("no_transaksi='$trx'")->get();
    }
    /*  private function listKoreksiPiutang($jenis = 'KP')
    {
        if ($jenis == '2') {
            $wh = "(jns_koreksi in ('2') or sppt_koreksi.no_transaksi not like '%KP%'  )";
        } else {
            $wh = "( jns_koreksi='3' and sppt_koreksi.no_transaksi like 'KP%')";
        }

        return db::connection("oracle_satutujuh")->table('sppt_Koreksi')
            ->leftjoin('sim_pbb.users', 'users.id', '=', 'sppt_Koreksi.created_by')
            ->leftjoin(db::raw("sim_pbb.users c"), 'c.id', '=', 'sppt_koreksi.verifikasi_by')
            ->leftjoin('sppt', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            })
            ->whereRaw($wh)
            ->selectRaw("no_transaksi,sppt_koreksi.kd_propinsi,sppt_koreksi.kd_dati2,sppt_koreksi.kd_kecamatan,sppt_koreksi.kd_kelurahan,sppt_koreksi.kd_blok,sppt_koreksi.no_urut,sppt_koreksi.kd_jns_op,sppt_koreksi.thn_pajak_sppt,keterangan,sppt_koreksi.created_at,users.nama,verifikasi_keterangan,c.nama verifikator,jns_koreksi,verifikasi_kode,no_surat,tgl_surat,kd_kategori,nm_kategori,pbb_yg_harus_dibayar_sppt pbb")
            ->orderBy("created_at", "desc");
    } */

    public function pembayaranTerblokirPdf($trx)
    {
        $data = $this->listKoreksiPiutang()->whereraw("no_transaksi='$trx'")->first();
        $nop = $this->getPembayaranTerblokir()->whereraw("no_transaksi='$trx'")->get();
        // dd($nop);

        $pages = View::make('analisa.pembayaran_terblokir_pdf', compact('data', 'nop'));
        $pdf = pdf::loadHTML($pages)->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }

    public function pembayaran_terblokir_cetak(Request $request)
    {
        $search = $request->only('kecamatan', 'kelurahan');
        $dataSet = $this->getPembayaranTerblokir($search);
        $keckelget = DB::connection('oracle_dua')->table('ref_kelurahan')
            ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
            ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
            })
            /* ->where(function ($q) use ($search) {
                if ($search['kecamatan'] && $search['kecamatan'] != '-') {
                    $q->where(['ref_kelurahan.kd_kecamatan' => $search['kecamatan']]);
                }
                if ($search['kelurahan'] && $search['kelurahan'] != '-') {
                    $q->where(['ref_kelurahan.kd_kelurahan' => $search['kelurahan']]);
                }
            }) */
            ->get()->first();
        $keckel = [
            'nm_kecamatan' => 'Semua',
            'nm_kelurahan' => 'Semua'
        ];
        if ($search['kecamatan'] && $search['kecamatan'] != '-') {
            $keckel['nm_kecamatan'] = $keckelget->nm_kecamatan;
        }
        if ($search['kelurahan'] && $search['kelurahan'] != '-') {
            $keckel['nm_kelurahan'] = $keckelget->nm_kelurahan;
        }
        // dd($keckel);
        $export = new PembayaranBlokir($dataSet->toArray(), $keckel);
        return Excel::download($export, 'laporan_pembayaran_blokir.xlsx');
    }
    public function blok_list_group(Request $request)
    {
        $group = $request->only(['kd_kecamatan', 'kd_kelurahan']);
        $Blok = DAT_OBJEK_PAJAK::select(['kd_blok', db::raw('count(kd_blok) t')])
            ->where($group)
            ->groupBy('kd_blok')
            ->orderBy('kd_blok')
            ->get()
            ->pluck('kd_blok', 'kd_blok')->toArray();
        return response()->json($Blok);
    }
    public function blok()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        if (!$kecamatan->count()) {
            $kecamatan = Kecamatan::orderBy('nm_kecamatan', 'asc')->get();
        }
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $blok = [];
        return view('analisa.blok', compact('result', 'kecamatan', 'kelurahan', 'blok'));
    }
    public function blok_search(Request $request)
    {
        $search = $request->only('kecamatan', 'kelurahan', 'blok');
        $dataSet = $this->blok_list($search);
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            return $data->kd_propinsi . "." .
                $data->kd_dati2 . "." .
                $data->kd_kecamatan . "." .
                $data->kd_kelurahan . "." .
                $data->kd_blok . "-" .
                $data->no_urut . "." .
                $data->kd_jns_op;
        })
            ->editColumn('pbb_terhutang_sppt', function ($data) {
                return gallade::parseQuantity($data->pbb_terhutang_sppt);
            })
            ->editColumn('nir', function ($data) {
                return gallade::parseQuantity($data->nir);
            })
            ->addColumn('raw_tag', function ($data) {
                return $data->status_op;
            })
            ->addColumn('thn_pajak_sppt', function ($row) {
                return date('Y');
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function blok_cetak(Request $request)
    {
        $search = $request->only('kecamatan', 'kelurahan', 'blok');
        try {
            $dataSet = $this->blok_list($search)->get()->toArray();
            $keckel = DB::connection('oracle_dua')->table('ref_kelurahan')
                ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                    $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
                })
                ->where(['ref_kelurahan.kd_kecamatan' => $search['kecamatan']])
                ->where(['ref_kelurahan.kd_kelurahan' => $search['kelurahan']])
                ->get()->first();
            $export = new Blok($dataSet, (array)$keckel);
            return Excel::download($export, 'blok_cetak.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    private function blok_list($search)
    {

        $kd_kecamatan = $search['kecamatan'] ?? '';
        $kd_kelurahan = $search['kelurahan'] ?? '';
        $kd_blok = $search['blok'] ?? '';

        $where = " dat_objek_pajak.kd_propinsi='35' and dat_objek_pajak.kd_dati2='07' ";

        if ($kd_kecamatan != '-') {
            $where .= " and dat_objek_pajak.kd_kecamatan='$kd_kecamatan' ";
        }
        if ($kd_kelurahan != '-') {
            $where .= " and dat_objek_pajak.kd_kelurahan='$kd_kelurahan' ";
        }
        if ($kd_blok != '-') {
            $where .= " and dat_objek_pajak.kd_blok='$kd_blok' ";
        }

        $result = DB::connection("oracle_satutujuh")->table(DB::raw("dat_objek_pajak"))
            ->join('dat_subjek_pajak', 'dat_objek_pajak.subjek_pajak_id', '=', 'dat_subjek_pajak.subjek_pajak_id')
            ->join('dat_op_bumi', function ($join) {
                $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
            ->join('kelas_tanah', function ($join) {
                $join->whereraw("to_char (sysdate, 'yyyy') between kelas_tanah.thn_awal_kls_tanah
                                                    and kelas_tanah.thn_akhir_kls_tanah
                                    and ((case when luas_bumi >0 then nilai_sistem_bumi / luas_bumi else 0 end)) between kelas_tanah.nilai_min_tanah
                                                        and kelas_tanah.nilai_max_tanah
                                    and kelas_tanah.kd_kls_tanah not like '%X%'");
            })
            ->leftjoin('sppt', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'sppt.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'sppt.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'sppt.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'sppt.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'sppt.kd_jns_op')
                    ->whereraw("sppt.thn_pajak_sppt= TO_CHAR (SYSDATE, 'yyyy')");
            })
            ->whereraw($where)
            ->selectraw(" dat_objek_pajak.kd_propinsi,
            dat_objek_pajak.kd_dati2,
            dat_objek_pajak.kd_kecamatan,
            dat_objek_pajak.kd_kelurahan,
            dat_objek_pajak.kd_blok,
            dat_objek_pajak.no_urut,
            dat_objek_pajak.kd_jns_op,
            dat_objek_pajak.subjek_pajak_id,
            nm_wp,
            jalan_wp,
            blok_kav_no_wp,
            rt_wp,
            rw_wp,
            kelurahan_wp,
            kota_wp,
            telp_wp,
            jalan_op,
            blok_kav_no_op,
            rt_op,
            rw_op,
            case
               when jns_bumi in ('1', '2', '3') then 'Aktif'
               else 'Non Aktif'
            end
               status_op,
            kd_znt,
            ((case when luas_bumi >0 then nilai_sistem_bumi / luas_bumi else 0 end)) * 1000 nir,
            kelas_tanah.kd_kls_tanah,
            total_luas_bumi,
            case
               when total_luas_bumi > 0 then njop_bumi / total_luas_bumi
               else 0
            end
               njop_bumi_per_meter,
            njop_bumi,
            total_luas_bng,
            case when total_luas_bng > 0 then njop_bng / total_luas_bng end
               njop_bng_per_meter,
            njop_bng,
            pbb_terhutang_sppt");

        return $result;

        /* $result = [];
        $where = [
            'dat_objek_pajak.kd_propinsi' => '35',
            'dat_objek_pajak.kd_dati2' => '07'
        ];
        if ($search && in_array('kecamatan', array_keys($search)) && in_array('kelurahan', array_keys($search))) {
            if (in_array('kecamatan', array_keys($search))) {
                $where['dat_objek_pajak.kd_kecamatan'] = $search['kecamatan'];
            }
            if (in_array('kelurahan', array_keys($search))) {
                $where['dat_objek_pajak.kd_kelurahan'] = $search['kelurahan'];
            }
            if (in_array('blok', array_keys($search))) {
                if ($search['blok'] != '-') {
                    $where['dat_objek_pajak.kd_blok'] = $search['blok'];
                }
            }
            $select = [
                DB::raw("distinct dat_objek_pajak.kd_propinsi"),
                "dat_objek_pajak.kd_dati2",
                "dat_objek_pajak.kd_kecamatan",
                "dat_objek_pajak.kd_kelurahan",
                "dat_objek_pajak.kd_blok",
                "dat_objek_pajak.no_urut",
                "dat_objek_pajak.kd_jns_op",
                "dat_objek_pajak.jns_transaksi_op",
                "SPPTLAST.thn_pajak_sppt",
                "SPPTLAST.blok_kav_no_wp_sppt",
                "SPPTLAST.nm_wp_sppt",
                "SPPTLAST.kd_kls_tanah",
                "SPPTLAST.thn_awal_kls_tanah",
                "SPPTLAST.luas_bumi_sppt",
                "SPPTLAST.luas_bng_sppt",
                "SPPTLAST.njop_bumi_sppt",
                "SPPTLAST.njop_bng_sppt",
                "pbb_terhutang_sppt",
                DB::raw("kelas_tanah.nilai_min_tanah*1000 nilai_min_tanah"),
                DB::raw("kelas_tanah.nilai_max_tanah*1000 nilai_max_tanah"),
                DB::raw("kelas_tanah.nilai_per_m2_tanah*1000 nilai_per_m2_tanah"),
                "dat_op_bumi.kd_znt",
                "dat_op_bumi.jns_bumi",
                DB::raw("dat_nir.nir*1000 nir")
            ];
            $joinSppt = 'SELECT "SPPT".*
            FROM "SPPT"
            inner JOIN (
                SELECT "SPPT"."KD_PROPINSI",
                "SPPT"."KD_DATI2",
                "SPPT"."KD_KECAMATAN",
                "SPPT"."KD_KELURAHAN",
                "SPPT"."KD_BLOK",
                "SPPT"."NO_URUT",
                "SPPT"."KD_JNS_OP",
                max("SPPT"."THN_PAJAK_SPPT") THN_PAJAK_SPPT
                FROM "SPPT"
                where  LOWER (SPPT.kd_kls_tanah) NOT LIKE ' . "'%x%'" . '
                GROUP BY "SPPT"."KD_PROPINSI",
                "SPPT"."KD_DATI2",
                "SPPT"."KD_KECAMATAN",
                "SPPT"."KD_KELURAHAN",
                "SPPT"."KD_BLOK",
                "SPPT"."NO_URUT",
                "SPPT"."KD_JNS_OP"
            ) "SPPTLAST"
                ON "SPPTLAST"."KD_PROPINSI" = "SPPT"."KD_PROPINSI"
                AND "SPPTLAST"."KD_DATI2" = "SPPT"."KD_DATI2"
                AND "SPPTLAST"."KD_KECAMATAN" = "SPPT"."KD_KECAMATAN"
                AND "SPPTLAST"."KD_KELURAHAN" = "SPPT"."KD_KELURAHAN"
                AND "SPPTLAST"."KD_BLOK" = "SPPT"."KD_BLOK"
                AND "SPPTLAST"."NO_URUT" = "SPPT"."NO_URUT"
                AND "SPPTLAST"."KD_JNS_OP" = "SPPT"."KD_JNS_OP"
                AND "SPPTLAST"."THN_PAJAK_SPPT" = "SPPT"."THN_PAJAK_SPPT"';
            $ordeBy = [
                "dat_objek_pajak.kd_propinsi",
                "dat_objek_pajak.kd_dati2",
                "dat_objek_pajak.kd_kecamatan",
                "dat_objek_pajak.kd_kelurahan",
                "dat_objek_pajak.kd_blok",
                "dat_objek_pajak.no_urut",
                "dat_objek_pajak.kd_jns_op"
            ];
            $result = DAT_OBJEK_PAJAK::where($where)
                ->select($select)
                ->orderBy(DB::raw(join(',', $ordeBy)))
                ->join("dat_op_bumi", function ($join) {
                    $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                        ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                        ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                        ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                        ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                        ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                        ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
                })
                ->join(DB::raw("(" . $joinSppt . ") spptLast"), function ($join) {
                    $join->on('spptLast.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                        ->on('spptLast.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                        ->on('spptLast.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                        ->on('spptLast.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                        ->on('spptLast.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                        ->on('spptLast.no_urut', '=', 'dat_objek_pajak.no_urut')
                        ->on('spptLast.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
                })
                ->join("kelas_tanah", function ($join) {
                    $join->on('kelas_tanah.kd_Kls_tanah', '=', 'spptLast.kd_kls_tanah')
                        ->on('kelas_tanah.thn_awal_kls_tanah', '=', 'spptLast.thn_awal_kls_tanah');
                })
                ->join("dat_nir", function ($join) {
                    $join->on('dat_nir.kd_propinsi', '=', 'spptLast.kd_propinsi')
                        ->on('dat_nir.kd_dati2', '=', 'spptLast.kd_dati2')
                        ->on('dat_nir.kd_kecamatan', '=', 'spptLast.kd_kecamatan')
                        ->on('dat_nir.kd_kelurahan', '=', 'spptLast.kd_kelurahan')
                        ->on('dat_nir.thn_nir_znt', '=', 'spptLast.thn_pajak_sppt')
                        ->on('dat_nir.nir', '>=', 'kelas_tanah.nilai_min_tanah')
                        ->on('dat_nir.nir', '<=', 'kelas_tanah.nilai_max_tanah');
                })
                ->get();
           
        }
        return $result; */
    }
    public function dhr()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        if (!$kecamatan->count()) {
            $kecamatan = Kecamatan::orderBy('nm_kecamatan', 'asc')->get();
        }
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('analisa.dhr', compact('result', 'kecamatan', 'kelurahan'));
    }
    public function dhk()
    {
        $result = [];
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        if (!$kecamatan->count()) {
            $kecamatan = Kecamatan::orderBy('nm_kecamatan', 'asc')->get();
        }
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $year = Carbon::now()->year;
        $tahunList = [];
        for ($i = $year; $i >= 2014; $i--) {
            $tahunList[] = $i;
        }
        return view('analisa.dhk', compact('result', 'kecamatan', 'kelurahan', 'tahunList'));
    }



    function dhk_list()
    {

        $dataset = DB::connection("oracle_satutujuh")->table("sppt")
            ->join("dat_op_bumi", function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('sppt.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
            })
            ->join('ref_kecamatan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'ref_kecamatan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'ref_kecamatan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
            })
            ->join('ref_kelurahan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'ref_kelurahan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'ref_kelurahan.kd_dati2')
                    ->on('sppt.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan')
                    ->on('sppt.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
            })
            ->whereraw("luas_bumi_sppt>0")
            ->groupbyraw("sppt.kd_kecamatan|| sppt.kd_kelurahan,nm_kecamatan,nm_kelurahan,
               sppt.kd_kecamatan,
               sppt.kd_kelurahan,
               sppt.kd_blok,
             kd_znt,hkpd,(sppt.njop_bumi_sppt / sppt.luas_bumi_sppt)
             order by sppt.kd_blok,kd_znt")
            ->select(db::raw("sppt.kd_kecamatan|| sppt.kd_kelurahan collect,nm_kecamatan,nm_kelurahan, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, kd_znt,hkpd,(sppt.njop_bumi_sppt / sppt.luas_bumi_sppt) nir, sum(luas_bumi_sppt) luas_bumi_sppt, count(1) totalnop"));


        return $dataset;
    }
    public function dhk_search(Request $request)
    {
        // $search = $request->only('kecamatan', 'kelurahan', 'nop', 'tahun');
        $kecamatan = $request->kecamatan;
        $kelurahan = $request->kelurahan;
        $tahun = $request->tahun;
        $dataSet = $this->dhk_list();
        if ($kecamatan <> '-') {
            $dataSet = $dataSet->where('sppt.kd_kecamatan', $kecamatan);
        }

        if ($kelurahan <> '-') {
            $dataSet = $dataSet->where('sppt.kd_kelurahan', $kelurahan);
        }
        // if ($tahun <> '') {
        $dataSet = $dataSet->where('sppt.thn_pajak_sppt', $tahun);
        // }

        $datatables = DataTables::of($dataSet);
        return $datatables
            ->editColumn('totalnop', function ($data) {
                return gallade::parseQuantity($data->totalnop);
            })->editColumn('luas_bumi_sppt', function ($data) {
                return gallade::parseQuantity($data->luas_bumi_sppt);
            })
            ->editColumn('nir', function ($data) {
                return gallade::parseQuantity($data->nir);
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function dhr_search(Request $request)
    {
        $search = $request->only('kecamatan', 'kelurahan', 'nop');
        $dataSet = $this->dhr_list($search);
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            return $data->kd_propinsi . "." .
                $data->kd_dati2 . "." .
                $data->kd_kecamatan . "." .
                $data->kd_kelurahan . "." .
                $data->kd_blok . "-" .
                $data->no_urut . "." .
                $data->kd_jns_op;
        })
            ->editColumn('jns_bumi', function ($data) {
                $listJenis = [
                    '1' => 'TANAH + BANGUNAN',
                    '2' => 'KAVLING',
                    '3' => 'TANAH KOSONG',
                    '4' => 'FASILITAS UMUM',
                    '5' => 'NON AKTIF'
                ];
                $result = '-';
                if (in_array($data->jns_bumi, array_keys($listJenis))) {
                    $result = $listJenis[$data->jns_bumi];
                }
                return $result;
            })
            ->editColumn('nir', function ($data) {
                return gallade::parseQuantity($data->nir);
            })
            ->editColumn('luas_bumi_sppt', function ($data) {
                return gallade::parseQuantity($data->luas_bumi_sppt);
            })
            ->editColumn('njop_bumi_sppt', function ($data) {
                return gallade::parseQuantity($data->njop_bumi_sppt);
            })
            ->editColumn('luas_bng_sppt', function ($data) {
                return gallade::parseQuantity($data->luas_bng_sppt);
            })
            ->editColumn('njop_bng_sppt', function ($data) {
                return gallade::parseQuantity($data->njop_bng_sppt);
            })
            ->editColumn('pbb_tanah', function ($data) {
                return gallade::parseQuantity($data->pbb_tanah);
            })
            ->editColumn('pbb_bangunan', function ($data) {
                return gallade::parseQuantity($data->pbb_bangunan);
            })
            ->addColumn('raw_tag', function ($data) {
                if ($data->jns_transaksi_op == '3' || in_array($data->jns_bumi, ['5', '4'])) {
                    return "<span class='badge badge-warning '>non Aktif</span>";
                }
                return "<span class='badge badge-success '>Aktif</span>";
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function dhr_cetak(Request $request)
    {
        $search = $request->only('kecamatan', 'kelurahan', 'nop');
        try {
            $dataSet = $this->dhr_list($search);
            $keckel = DB::connection('oracle_dua')->table('ref_kelurahan')
                ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                    $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
                })
                ->where(function ($sql) use ($search) {
                    if ($search['kecamatan'] && $search['kecamatan'] != "-") {
                        $sql->where(['ref_kelurahan.kd_kecamatan' => $search['kecamatan']]);
                    }
                    if ($search['kelurahan'] && $search['kelurahan'] != "-") {
                        $sql->where(['ref_kelurahan.kd_kelurahan' => $search['kelurahan']]);
                    }
                })->get()->first();
            if (!$keckel) return response()->json(["msg" => 'kecamatan/kelurahan tidak di temukan', "status" => false]);
            $kecamatanKelurhaan = (array)$keckel;
            if ($search['kecamatan'] && $search['kecamatan'] == "-") {
                $kecamatanKelurhaan['nm_kecamatan'] = "Semua";
            }
            if ($search['kelurahan'] && $search['kelurahan'] == "-") {
                $kecamatanKelurhaan['nm_kelurahan'] = "Semua";
            }
            $kecamatanKelurhaan = (array)$keckel;
            $export = new Dhr($dataSet->toArray(), $kecamatanKelurhaan);
            return Excel::download($export, 'dhr_cetak.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }

    public function dhr_cetak_excell(Request $request)
    {
        $kecamatan = $request->kecamatan ?? '-';
        $kelurahan = $request->kelurahan ?? '-';
        $tahun = $request->tahun ?? date('Y');
        $data = $this->dhk_list();
        if ($kecamatan <> '-') {
            $data = $data->where('sppt.kd_kecamatan', $kecamatan);
        }

        if ($kelurahan <> '-') {
            $data = $data->where('sppt.kd_kelurahan', $kelurahan);
        }
        $data = $data->where('sppt.thn_pajak_sppt', $tahun)->get();
        // dd($data);
        return Excel::download(new DhkExport($data), 'data_dhk.xlsx');
    }

    public function dhr_cetak_min(Request $request)
    {

        if ($request->qr == 'excell') {
            return $this->dhr_cetak_excell($request);
        }

        $kecamatan = $request->kecamatan ?? '-';
        $kelurahan = $request->kelurahan ?? '-';
        $tahun = $request->tahun ?? date('Y');
        $data = $this->dhk_list();
        if ($kecamatan <> '-') {
            $data = $data->where('sppt.kd_kecamatan', $kecamatan);
        }

        if ($kelurahan <> '-') {
            $data = $data->where('sppt.kd_kelurahan', $kelurahan);
        }
        $data = $data->where('sppt.thn_pajak_sppt', $tahun)->get()->toarray();
        // collect
        // return view('home');
        $datadesa = collect($data)->groupBy('collect');
        $pages = [];
        $kaban = pegawaiStruktural::whereraw("kode='01'")->first();

        foreach ($datadesa as $data) {
            $nm_kecamatan = $data[0]->nm_kecamatan;
            $nm_kelurahan = $data[0]->nm_kelurahan;

            if ($request->qr == 'yes') {
                $url =   config('app.url') . 'analisa/dhk_cetak?qr=yes&tahun=' . $tahun . '&kecamatan=' . $kecamatan . '&kelurahan=' . $kelurahan;
                $qrcode = QrCode::style('square')->size(70)->errorCorrection('L')->generate($url);
            } else {
                $qrcode = '';
            }

            $pages[] = View::make('analisa.cetak.dhr_min', compact('data', 'tahun', 'nm_kecamatan', 'nm_kelurahan', 'kaban', 'qrcode'));
        }

        $pdf = pdf::loadHTML($pages)->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream();
    }
    private function dhr_list($search, $extra = false, $select = false)
    {
        $result = [];
        $where = [
            'dat_objek_pajak.kd_propinsi' => '35',
            'dat_objek_pajak.kd_dati2' => '07'
        ];
        if ($search && in_array('kecamatan', array_keys($search)) && in_array('kelurahan', array_keys($search))) {
            if (in_array('kecamatan', array_keys($search))) {
                if ($search['kecamatan'] != "-") {
                    $where['dat_objek_pajak.kd_kecamatan'] = $search['kecamatan'];
                }
            }
            if (in_array('kelurahan', array_keys($search))) {
                if ($search['kelurahan'] != "-") {
                    $where['dat_objek_pajak.kd_kelurahan'] = $search['kelurahan'];
                }
            }
            if (in_array('nop', array_keys($search))) {
                if (strlen($search['nop']) == '24') {
                    $nop = explode('.', $search['nop']);
                    $blok_urut = explode('-', $nop['4']);
                    $where['dat_objek_pajak.kd_blok'] = $blok_urut[0];
                    $where['dat_objek_pajak.no_urut'] = $blok_urut[1];
                    $where['dat_objek_pajak.kd_jns_op'] = $nop[5];
                }
            }
            if (!$select) {
                $select = [
                    DB::raw("distinct dat_objek_pajak.kd_propinsi"),
                    "dat_objek_pajak.kd_dati2",
                    "dat_objek_pajak.kd_kecamatan",
                    "dat_objek_pajak.kd_kelurahan",
                    "dat_objek_pajak.kd_blok",
                    "dat_objek_pajak.no_urut",
                    "dat_objek_pajak.kd_jns_op",
                    "dat_objek_pajak.jns_transaksi_op",
                    "SPPTLAST.thn_pajak_sppt",
                    "SPPTLAST.blok_kav_no_wp_sppt",
                    "SPPTLAST.nm_wp_sppt",
                    "SPPTLAST.kd_kls_tanah",
                    "SPPTLAST.thn_awal_kls_tanah",
                    "SPPTLAST.luas_bumi_sppt",
                    "SPPTLAST.njop_bumi_sppt",
                    "SPPTLAST.luas_bng_sppt",
                    "SPPTLAST.njop_bng_sppt",
                    DB::raw("SPPTLAST.njop_bumi_sppt/1000 pbb_tanah"),
                    DB::raw("SPPTLAST.njop_bng_sppt/1000 pbb_bangunan"),
                    // DB::raw("kelas_tanah.nilai_min_tanah*1000 nilai_min_tanah"),
                    // DB::raw("kelas_tanah.nilai_max_tanah*1000 nilai_max_tanah"),
                    // DB::raw("kelas_tanah.nilai_per_m2_tanah*1000 nilai_per_m2_tanah"),
                    "dat_op_bumi.kd_znt",
                    "dat_op_bumi.jns_bumi",
                    DB::raw("dat_nir.nir*1000 nir"),
                    // DB::raw("rekom_znt.kd_znt rekom_znt")
                ];
            }
            $joinSppt = 'SELECT "SPPT".*
            FROM "SPPT"
            inner JOIN (
                SELECT "SPPT"."KD_PROPINSI",
                "SPPT"."KD_DATI2",
                "SPPT"."KD_KECAMATAN",
                "SPPT"."KD_KELURAHAN",
                "SPPT"."KD_BLOK",
                "SPPT"."NO_URUT",
                "SPPT"."KD_JNS_OP",
                max("SPPT"."THN_PAJAK_SPPT") THN_PAJAK_SPPT
                FROM "SPPT"
                where  LOWER (SPPT.kd_kls_tanah) NOT LIKE ' . "'%x%'" . '
                GROUP BY "SPPT"."KD_PROPINSI",
                "SPPT"."KD_DATI2",
                "SPPT"."KD_KECAMATAN",
                "SPPT"."KD_KELURAHAN",
                "SPPT"."KD_BLOK",
                "SPPT"."NO_URUT",
                "SPPT"."KD_JNS_OP"
            ) "SPPTLAST"
                ON "SPPTLAST"."KD_PROPINSI" = "SPPT"."KD_PROPINSI"
                AND "SPPTLAST"."KD_DATI2" = "SPPT"."KD_DATI2"
                AND "SPPTLAST"."KD_KECAMATAN" = "SPPT"."KD_KECAMATAN"
                AND "SPPTLAST"."KD_KELURAHAN" = "SPPT"."KD_KELURAHAN"
                AND "SPPTLAST"."KD_BLOK" = "SPPT"."KD_BLOK"
                AND "SPPTLAST"."NO_URUT" = "SPPT"."NO_URUT"
                AND "SPPTLAST"."KD_JNS_OP" = "SPPT"."KD_JNS_OP"
                AND "SPPTLAST"."THN_PAJAK_SPPT" = "SPPT"."THN_PAJAK_SPPT"';
            $join_rekom = "SELECT
                    dn.KD_PROPINSI,
                    dn.KD_DATI2,
                    dn.KD_KECAMATAN,
                    dn.KD_KELURAHAN,
                    dn.THN_NIR_ZNT,
                    max(dn.KD_ZNT) KD_ZNT
                FROM
                    DAT_NIR dn
                GROUP BY dn.KD_PROPINSI,
                    dn.KD_DATI2,
                    dn.KD_KECAMATAN,
                    dn.KD_KELURAHAN,
                    dn.THN_NIR_ZNT 
                ORDER BY dn.THN_NIR_ZNT  desc";
            $ordeBy = [
                "dat_objek_pajak.kd_propinsi",
                "dat_objek_pajak.kd_dati2",
                "dat_objek_pajak.kd_kecamatan",
                "dat_objek_pajak.kd_kelurahan",
                "dat_objek_pajak.kd_blok",
                "dat_objek_pajak.no_urut",
                "dat_objek_pajak.kd_jns_op"
            ];
            $result = DAT_OBJEK_PAJAK::where($where)->select($select);
            if (!$extra) {
                $result = $result->orderBy(DB::raw(join(',', $ordeBy)));
            }
            $result = $result->join("dat_op_bumi", function ($join) {
                $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                    ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                    ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                    ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                    ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                    ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                    ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
            })
                ->leftjoin(DB::raw("(" . $joinSppt . ") spptLast"), function ($join) {
                    $join->on('spptLast.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                        ->on('spptLast.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                        ->on('spptLast.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                        ->on('spptLast.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                        ->on('spptLast.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                        ->on('spptLast.no_urut', '=', 'dat_objek_pajak.no_urut')
                        ->on('spptLast.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
                })
                ->leftjoin("kelas_tanah", function ($join) {
                    $join->on('kelas_tanah.kd_Kls_tanah', '=', 'spptLast.kd_kls_tanah')
                        ->on('kelas_tanah.thn_awal_kls_tanah', '=', 'spptLast.thn_awal_kls_tanah');
                })
                ->leftjoin("dat_nir", function ($join) {
                    $join->on('dat_nir.kd_propinsi', '=', 'spptLast.kd_propinsi')
                        ->on('dat_nir.kd_dati2', '=', 'spptLast.kd_dati2')
                        ->on('dat_nir.kd_kecamatan', '=', 'spptLast.kd_kecamatan')
                        ->on('dat_nir.kd_kelurahan', '=', 'spptLast.kd_kelurahan')
                        ->on('dat_nir.thn_nir_znt', '=', 'spptLast.thn_pajak_sppt')
                        ->on('dat_nir.nir', '>=', 'kelas_tanah.nilai_min_tanah')
                        ->on('dat_nir.nir', '<=', 'kelas_tanah.nilai_max_tanah');
                })
                ->leftjoin(DB::raw("(" . $join_rekom . ") rekom_znt"), function ($join) {
                    $join->on('rekom_znt.kd_propinsi', '=', 'spptLast.kd_propinsi')
                        ->on('rekom_znt.kd_dati2', '=', 'spptLast.kd_dati2')
                        ->on('rekom_znt.kd_kecamatan', '=', 'spptLast.kd_kecamatan')
                        ->on('rekom_znt.kd_kelurahan', '=', 'spptLast.kd_kelurahan')
                        ->on('rekom_znt.thn_nir_znt', '=', 'spptLast.thn_pajak_sppt');
                });
            if (!$extra) {
                $result = $result->get();
            }
            // dd($dataSet->toArray());    
        }
        return $result;
    }
    public function nop_non_aktif(Request $request)
    {

        if ($request->ajax()) {
            $data = DB::connection("oracle_satutujuh")->table("dat_op_bumi")
                ->join('dat_objek_pajak', function ($join) {
                    $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                        ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                        ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                        ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                        ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                        ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                        ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
                })
                ->join('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
                ->join('ref_kecamatan', function ($join) {
                    $join->on('dat_op_bumi.kd_propinsi', '=', 'ref_kecamatan.kd_propinsi')
                        ->on('dat_op_bumi.kd_dati2', '=', 'ref_kecamatan.kd_dati2')
                        ->on('dat_op_bumi.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
                })
                ->join('ref_kelurahan', function ($join) {
                    $join->on('dat_op_bumi.kd_propinsi', '=', 'ref_kelurahan.kd_propinsi')
                        ->on('dat_op_bumi.kd_dati2', '=', 'ref_kelurahan.kd_dati2')
                        ->on('dat_op_bumi.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                        ->on('dat_op_bumi.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
                })
                ->leftjoin(db::raw("(select distinct b.kd_propinsi,b.kd_dati2,b.kd_kecamatan,b.kd_kelurahan,b.kd_blok,b.no_urut,b.kd_jns_op 
                            from sim_pbb.data_billing a
                            join sim_pbb.billing_kolektif b on a.data_billing_id=b.data_billing_id 
                            where a.kd_jns_op='4' and a.kd_status='0' and a.expired_at >sysdate and a.deleted_at is null) ab"), function ($join) {
                    $join->on('dat_op_bumi.kd_propinsi', '=', 'ab.kd_propinsi')
                        ->on('dat_op_bumi.kd_dati2', '=', 'ab.kd_dati2')
                        ->on('dat_op_bumi.kd_kecamatan', '=', 'ab.kd_kecamatan')
                        ->on('dat_op_bumi.kd_kelurahan', '=', 'ab.kd_kelurahan')
                        ->on('dat_op_bumi.kd_blok', '=', 'ab.kd_blok')
                        ->on('dat_op_bumi.no_urut', '=', 'ab.no_urut')
                        ->on('dat_op_bumi.kd_jns_op', '=', 'ab.kd_jns_op');
                })
                ->leftjoin("pengaktifan_objek", function ($join) {
                    $join->on('pengaktifan_objek.kd_propinsi_asal', '=', 'dat_op_bumi.kd_propinsi')
                        ->on('pengaktifan_objek.kd_dati2_asal', '=', 'dat_op_bumi.kd_dati2')
                        ->on('pengaktifan_objek.kd_kecamatan_asal', '=', 'dat_op_bumi.kd_kecamatan')
                        ->on('pengaktifan_objek.kd_kelurahan_asal', '=', 'dat_op_bumi.kd_kelurahan')
                        ->on('pengaktifan_objek.kd_blok_asal', '=', 'dat_op_bumi.kd_blok')
                        ->on('pengaktifan_objek.no_urut_asal', '=', 'dat_op_bumi.no_urut')
                        ->on('pengaktifan_objek.kd_jns_op_asal', '=', 'dat_op_bumi.kd_jns_op');
                })
                ->orderbyraw("dat_op_bumi.jns_bumi desc")
                ->selectraw("dat_op_bumi.kd_propinsi,
                                dat_op_bumi.kd_dati2,
                                dat_op_bumi.kd_kecamatan,
                                dat_op_bumi.kd_kelurahan,
                                dat_op_bumi.kd_blok,
                                dat_op_bumi.no_urut,
                                dat_op_bumi.kd_jns_op,
                                jns_bumi,
                                nm_wp,
                                jalan_op,
                                blok_kav_no_op,
                                rt_op,
                                rw_op,
                                nm_kelurahan,
                                nm_kecamatan,
                                total_luas_bumi,
                                total_luas_bng")->whereraw("luas_bumi>0 and  dat_op_bumi.kd_jns_op!='7' and jns_bumi in ('4','5') and ab.kd_propinsi is null and pengaktifan_objek.kd_propinsi_asal is null");

            $cr = $request->pencarian ?? '';
            if ($cr != '') {
                $cr = strtolower($cr);
                $ak = onlyNumber($cr);
                if ($ak == '') {
                    $data = $data->whereraw("lower(nm_wp) like '%$cr%'");
                } else {
                    $data = $data->whereraw("
                    dat_op_bumi.kd_propinsi||
                                dat_op_bumi.kd_dati2||
                                dat_op_bumi.kd_kecamatan||
                                dat_op_bumi.kd_kelurahan||
                                dat_op_bumi.kd_blok||
                                dat_op_bumi.no_urut||
                                dat_op_bumi.kd_jns_op
                    like '%$ak%'");
                }
            }

            $datatables = datatables::of($data)
                ->addColumn('nop', function ($row) {
                    $nop = '';
                    $nop .= $row->kd_propinsi . '.';
                    $nop .= $row->kd_dati2 . '.';
                    $nop .= $row->kd_kecamatan . '.';
                    $nop .= $row->kd_kelurahan . '.';
                    $nop .= $row->kd_blok . '-';
                    $nop .= $row->no_urut . '.';
                    $nop .= $row->kd_jns_op;
                    $nop .= "<br><small class='text-info'>" . jenisBumi($row->jns_bumi) . "<small>";
                    return $nop;
                })
                ->addColumn('alamat', function ($row) {
                    $alamat = "";
                    $alamat .= $row->jalan_op;
                    $alamat .= " " . $row->blok_kav_no_op;
                    if ($row->rt_op <> '') {
                        $alamat .= " RT " . $row->rt_op;
                    }
                    if ($row->rw_op <> '') {
                        $alamat .= " RW " . $row->rw_op;
                    }
                    $alamat .= "<br><small class='text-info'> " . $row->nm_kelurahan;
                    $alamat .= ", " . $row->nm_kecamatan . "</small>";

                    return $alamat;
                })
                ->addColumn('luas_bumi', function ($row) {
                    return angka($row->total_luas_bumi);
                })
                ->addColumn('luas_bng', function ($row) {
                    // return 'luas_bng';
                    return angka($row->total_luas_bng);
                })
                ->addColumn('action', function ($row) {
                    $btn = "<button  
                    data-kd_propinsi='" . $row->kd_propinsi . "'
                    data-kd_dati2='" . $row->kd_dati2 . "'
                    data-kd_kecamatan='" . $row->kd_kecamatan . "'
                    data-kd_kelurahan='" . $row->kd_kelurahan . "'
                    data-kd_blok='" . $row->kd_blok . "'
                    data-no_urut='" . $row->no_urut . "'
                    data-kd_jns_op='" . $row->kd_jns_op . "'
                    class='reactive btn btn-xs btn-flat btn-info'><i class='fas fa-edit'></i></button>";
                    return $btn;
                })
                ->rawcolumns([
                    'nop',
                    'alamat',
                    'luas_bumi',
                    'luas_bng',
                    'action'
                ])
                ->make(true);
            return $datatables;
        }
        return view('analisa.index_nop_non_aktif');
    }

    public  function NopReaktif(Request $request)
    {
        $data = pengaktifanObjek::whereraw('pendataan_objek_id is not null');
        $datatables = datatables::of($data)
            ->addColumn('nop', function ($row) {
                $nop = '';
                $nop .= $row->kd_propinsi . '.';
                $nop .= $row->kd_dati2 . '.';
                $nop .= $row->kd_kecamatan . '.';
                $nop .= $row->kd_kelurahan . '.';
                $nop .= $row->kd_blok . '-';
                $nop .= $row->no_urut . '.';
                $nop .= $row->kd_jns_op;
                return $nop;
            })
            ->addColumn('nop_asal', function ($row) {
                $nop = '';
                $nop .= $row->kd_propinsi_asal . '.';
                $nop .= $row->kd_dati2_asal . '.';
                $nop .= $row->kd_kecamatan_asal . '.';
                $nop .= $row->kd_kelurahan_asal . '.';
                $nop .= $row->kd_blok_asal . '-';
                $nop .= $row->no_urut_asal . '.';
                $nop .= $row->kd_jns_op_asal;
                return $nop;
            })

            ->addColumn('tanggal', function ($row) {
                return $row->created_at;
            })
            ->addColumn('action', function ($row) {
                return '-';
            })
            ->addColumn('petugas', function ($row) {
                // return $row->users->nama ?? '';
                $user = User::find($row->created_by);


                return $user->nama ?? '';
            })
            ->rawcolumns([
                'nop',
                'nop_asal',
                'action',
                'tanggal',
                'petugas'
            ])
            ->make(true);
        return $datatables;
    }


    public function pengaktifanStore(Request $request)
    {

        $kd_propinsi = $request->kd_propinsi;
        $kd_dati2 = $request->kd_dati2;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_blok = $request->kd_blok;
        $no_urut = $request->no_urut;
        $kd_jns_op = $request->kd_jns_op;

        $cektahun = Sppt::where([
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op
        ])->select(DB::raw("max(thn_pajak_sppt) thn_min_sppt,max(thn_pajak_sppt) thn_max_sppt"))->first();


        $thn_min_sppt = $cektahun->thn_min_sppt;
        $thn_max_sppt = $cektahun->thn_max_sppt;

        $ceksppt = DB::table(DB::raw("(SELECT " . $thn_min_sppt . " + LEVEL - 1 tahun
              FROM DUAL
        CONNECT BY " . $thn_min_sppt . " + LEVEL - 1 <= " . date('Y') . ") ref_tahun"))
            ->select(DB::raw("'$kd_propinsi' kd_propinsi,
       '$kd_dati2' kd_dati2,
       '$kd_kecamatan' kd_kecamatan,
       '$kd_kelurahan' kd_kelurahan,
       '$kd_blok' kd_blok,
       '$no_urut' no_urut,
       '$kd_jns_op' kd_jns_op,
       tahun thn_pajak_sppt,
       (SELECT CASE
                  WHEN pbb > pokok_bayar THEN NULL
                  ELSE TO_CHAR (tanggal, 'yyyy-mm-dd')
               END
                  tanggal
          FROM (  SELECT a.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                            pbb,
                         SUM (jml_sppt_yg_dibayar - denda_sppt) pokok_bayar,
                         MAX (tgl_pembayaran_sppt) tanggal
                    FROM pbb.sppt a
                         LEFT JOIN pbb.sppt_potongan c
                            ON     a.kd_propinsi = c.kd_propinsi
                               AND a.kd_dati2 = c.kd_dati2
                               AND a.kd_kecamatan = c.kd_kecamatan
                               AND a.kd_kelurahan = c.kd_kelurahan
                               AND a.kd_blok = c.kd_blok
                               AND a.no_urut = c.no_urut
                               AND a.kd_jns_op = c.kd_jns_op
                               AND a.thn_pajak_sppt = c.thn_pajak_sppt
                         LEFT JOIN pbb.pembayaran_sppt b
                            ON     a.kd_propinsi = b.kd_propinsi
                               AND a.kd_dati2 = b.kd_dati2
                               AND a.kd_kecamatan = b.kd_kecamatan
                               AND a.kd_kelurahan = b.kd_kelurahan
                               AND a.kd_blok = b.kd_blok
                               AND a.no_urut = b.no_urut
                               AND a.kd_jns_op = b.kd_jns_op
                               AND a.thn_pajak_sppt = b.thn_pajak_sppt
                   WHERE     a.KD_PROPINSI = '$kd_propinsi'
                         AND a.KD_DATI2 = '$kd_dati2'
                         AND a.KD_KECAMATAN = '$kd_kecamatan'
                         AND a.KD_KELURAHAN = '$kd_kelurahan'
                         AND a.KD_BLOK = '$kd_blok'
                         AND a.NO_URUT = '$no_urut'
                         AND a.KD_JNS_OP = '$kd_jns_op'
                         AND a.THN_PAJAK_SPPT = ref_tahun.tahun
                GROUP BY   a.pbb_yg_harus_dibayar_sppt
                         - NVL (nilai_potongan, 0)))
          tgl_pembayaran_sppt,
       (SELECT no_transaksi
          FROM pbb.pengurang_piutang
         WHERE   KD_PROPINSI = '$kd_propinsi'
               AND KD_DATI2 = '$kd_dati2'
               AND KD_KECAMATAN = '$kd_kecamatan'
               AND KD_KELURAHAN = '$kd_kelurahan'
               AND KD_BLOK = '$kd_blok'
               AND NO_URUT = '$no_urut'
               AND KD_JNS_OP = '$kd_jns_op'
               AND THN_PAJAK_SPPT = ref_tahun.tahun
               AND ROWNUM = 1)
          koreksi,
            (SELECT kd_kategori
          FROM pbb.pengurang_piutang
         WHERE   pengurang_piutang.kd_kategori in ('04','05','06')
               AND KD_PROPINSI = '$kd_propinsi'
               AND KD_DATI2 = '$kd_dati2'
               AND KD_KECAMATAN = '$kd_kecamatan'
               AND KD_KELURAHAN = '$kd_kelurahan'
               AND KD_BLOK = '$kd_blok'
               AND NO_URUT = '$no_urut'
               AND KD_JNS_OP = '$kd_jns_op'
               AND THN_PAJAK_SPPT = ref_tahun.tahun
               AND ROWNUM = 1) kd_kategori,
               (SELECT count(1)
          FROM pbb.sppt
         WHERE    KD_PROPINSI = '$kd_propinsi'
               AND KD_DATI2 = '$kd_dati2'
               AND KD_KECAMATAN = '$kd_kecamatan'
               AND KD_KELURAHAN = '$kd_kelurahan'
               AND KD_BLOK = '$kd_blok'
               AND NO_URUT = '$no_urut'
               AND KD_JNS_OP = '$kd_jns_op'
               AND THN_PAJAK_SPPT = ref_tahun.tahun
               ) is_sppt"))
            ->get();

        $tahunPenetapan = [];

        foreach ($ceksppt as $row) {
            if ($row->is_sppt == '0' || ($row->kd_kategori != '' && $row->tgl_pembayaran_sppt == '')) {
                $tahunPenetapan[] = $row->thn_pajak_sppt;
            }
        }

        // return $ceksppt;
        // return $tahunPenetapan;

        // get nop baru

        $nop_asal = "";
        $nop_asal .= $kd_propinsi;
        $nop_asal .= $kd_dati2;
        $nop_asal .= $kd_kecamatan;
        $nop_asal .= $kd_kelurahan;
        $nop_asal .= $kd_blok;
        $nop_asal .= $no_urut;
        $nop_asal .= $kd_jns_op;
        $nop_proses =  Pajak::generateNOP($kd_kecamatan, $kd_kelurahan, $kd_blok, auth()->user()->id);


        // di proses dengan nop yang baru
        $kd_propinsi_proses = substr($nop_proses, 0, 2);
        $kd_dati2_proses = substr($nop_proses, 2, 2);
        $kd_kecamatan_proses = substr($nop_proses, 4, 3);
        $kd_kelurahan_proses = substr($nop_proses, 7, 3);
        $kd_blok_proses = substr($nop_proses, 10, 3);
        $no_urut_proses = substr($nop_proses, 13, 4);
        $kd_jns_op_proses = substr($nop_proses, 17, 1);

        // return $request->all();
        /* 
            --yang di pindah ke nop baru penetapan di nop baru
1. kategori is not null & tgl_pembayaran is null || is_sppt='1'
2. sisanya di migrasi pembayaran

        */




        // identifikasi sppt
        $kode_billing = "";

        DB::beginTransaction();
        try {


            $co = DB::connection("oracle_satutujuh")->table("DAT_OBJEK_PAJAK")
                ->selectraw("total_luas_bumi,total_luas_bng")
                ->whereraw("kd_propinsi = '$kd_propinsi'
                        and kd_dati2 = '$kd_dati2'
                        and kd_kecamatan = '$kd_kecamatan'
                        and kd_kelurahan = '$kd_kelurahan'
                        and kd_blok = '$kd_blok'
                        and no_urut = '$no_urut'
                        and kd_jns_op = '$kd_jns_op'")->first();

            if ($co->total_luas_bng > 0) {
                $jns_bumi = 1;
            } else {
                $jns_bumi = 3;
            }
            $userid = Auth()->user()->id;
            $now = Carbon::now();
            $jns_pendataan = '7';
            $batchData = [
                'jenis_pendataan_id' => $jns_pendataan,
                'tipe' => 'PD',
                'verifikasi_at' => $now,
                'verifikasi_by' => $userid,
                'verifikasi_deskripsi' => 'Pengaktifan objek'
            ];
            $batch = PendataanBatch::create($batchData);
            $batal = DB::connection("oracle_dua")->select(DB::raw("SELECT '2' JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,substr(A.SUBJEK_PAJAK_ID,1,16) SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,'" . $jns_bumi . "' JNS_BUMI FROM DAT_OBJEK_PAJAK A
            left JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
            left JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
            WHERE a.kd_kecamatan='$kd_kecamatan'
            AND a.kd_kelurahan='$kd_kelurahan'
            AND a.kd_blok='$kd_blok'
            AND a.no_urut='$no_urut'
            AND a.kd_jns_op='$kd_jns_op' "))[0];
            // $nop_asal = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
            $dat_obj = [
                'pendataan_batch_id' => $batch->id,
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' =>  $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'nop_asal' => onlyNumber($nop_asal),
                'subjek_pajak_id' => $batal->subjek_pajak_id ?? '',
                'nm_wp' => $batal->nm_wp ?? '',
                'jalan_wp' => $batal->jalan_wp ?? '',
                'blok_kav_no_wp' => $batal->blok_kav_no_wp ?? '',
                'rw_wp' => padding(($batal->rw_wp ?? ''), 0, 2),
                'rt_wp' => padding(($batal->rt_wp ?? ''), 0, 3),
                'kelurahan_wp' => ($batal->kelurahan_wp ?? ''),
                'kecamatan_wp' => '-',
                'propinsi_wp' => '-',
                'kota_wp' => $batal->kota_wp ?? '',
                'kd_pos_wp' => ($batal->kd_pos_wp ?? ''),
                'telp_wp' => ($batal->telp_wp ?? ''),
                'npwp' => null,
                'status_pekerjaan_wp' => ($batal->status_pekerjaan_wp ?? ''),
                'no_persil' => ($batal->no_persil ?? ''),
                'jalan_op' => ($batal->jalan_op ?? ''),
                'blok_kav_no_op' => ($batal->blok_kav_no_op ?? ''),
                'rw_op' => padding(($batal->rw_op ?? ''), 0, 2),
                'rt_op' => padding(($batal->rt_op ?? ''), 0, 3),
                'kd_status_cabang' =>  null,
                'kd_status_wp' =>  $request->alasan == '6' ? '5' : $batal->kd_status_wp,
                'kd_znt' =>  $batal->kd_znt,
                'luas_bumi' => $batal->luas_bumi,
                'jns_bumi' => $batal->jns_bumi,
                'created_at' => $now,
                'created_by' => $userid,
                'updated_at' => $now,
                'updated_by' => $userid
            ];

            $objek = PendataanObjek::create($dat_obj);
            // $spop = [];
            $nop_asal = onlyNumber($objek->nop_asal);
            $jtr = 2;
            // $nop_proses = $nop_asal;

            $data = [
                'jenis_pendataan_id' => $objek->batch->jenis_pendataan_id,
                'pendataan_objek_id' => $objek->id,
                'nop_proses' => $nop_proses,
                'nop_asal' => $nop_asal,
                'jns_transaksi' => $jtr,
                'jalan_op' => $objek->jalan_op,
                'blok_kav_no_op' => $objek->blok_kav_no_op,
                'rt_op' => $objek->rt_op,
                'rw_op' => $objek->rw_op,
                'no_persil' => $objek->no_persil,
                'luas_bumi' => $objek->luas_bumi,
                'kd_znt' => $objek->kd_znt,
                'jns_bumi' => $objek->jns_bumi,
                'jml_bng' => $objek->jml_bng,
                'subjek_pajak_id' => $objek->subjek_pajak_id != '' ? $objek->subjek_pajak_id : substr($nop_asal, 2, 16),
                'nm_wp' => $objek->nm_wp <> '' ? $objek->nm_wp : 'WAJIB PAJAK',
                'jalan_wp' => $objek->jalan_wp <> '' ? $objek->jalan_wp : '-',
                'blok_kav_no_wp' => $objek->blok_kav_no_wp,
                'rt_wp' => $objek->rt_wp,
                'rw_wp' => $objek->rw_wp,
                'kelurahan_wp' => $objek->kelurahan_wp,
                'kecamatan_wp' => $objek->kecamatan_wp,
                'kota_wp' => $objek->kota_wp,
                'propinsi_wp' => $objek->propinsi_wp,
                'kd_pos_wp' => $objek->kd_pos_wp,
                'status_pekerjaan_wp' => $objek->status_pekerjaan_wp <> '' ? $objek->status_pekerjaan_wp : '5',
                'kd_status_wp' => $objek->kd_status_wp,
                'npwp' => $objek->npwp,
                'telp_wp' => $objek->telp_wp,
            ];


            $data['created_by'] = $objek->created_by;
            $data['created_at'] = now();
            $data['keterangan'] = "";

            $req = new \Illuminate\Http\Request();
            $req->merge($data);
            Pajak::prosesPendataan($req);

            // insert ke pengaktifan objek
            $dataAktifkan = [
                'kd_propinsi' => $kd_propinsi_proses,
                'kd_dati2' => $kd_dati2_proses,
                'kd_kecamatan' => $kd_kecamatan_proses,
                'kd_kelurahan' => $kd_kelurahan_proses,
                'kd_blok' => $kd_blok_proses,
                'no_urut' => $no_urut_proses,
                'kd_jns_op' => $kd_jns_op_proses,
                'kd_propinsi_asal' => $kd_propinsi,
                'kd_dati2_asal' => $kd_dati2,
                'kd_kecamatan_asal' => $kd_kecamatan,
                'kd_kelurahan_asal' => $kd_kelurahan,
                'kd_blok_asal' => $kd_blok,
                'no_urut_asal' => $no_urut,
                'kd_jns_op_asal' => $kd_jns_op,
                'created_by' => auth()->user()->id,
                'created_at' => now(),
                'pendataan_objek_id' => $objek->id
            ];

            DB::connection("oracle_satutujuh")->table('pengaktifan_objek')->insert($dataAktifkan);

            // migrasikan pembayaran
            // PEMBAYARAN_SPPT_MUTASI_BATCH
            $bm = [
                'deskripsi' => "Dari " . formatnop($nop_asal) . " ke " . formatnop($nop_proses),
                'nop_asal' => $nop_asal,
                'nop_tujuan' => $nop_proses,
                'created_at' => now(),
                'created_by' => auth()->user()->id
            ];

            // get data pembayaran nop asal
            $ps = PembayaranSppt::where([
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
            ])
                ->whereNotIn('thn_pajak_sppt', $tahunPenetapan)
                ->select(DB::raw('distinct kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt'))
                ->get();

            $pm = PembayaranMutasi::create($bm);
            // PEMBAYARAN_SPPT_MUTASI_DATA
            foreach ($ps as $item) {
                $bmo = [
                    'kd_propinsi' => $kd_propinsi_proses,
                    'kd_dati2' => $kd_dati2_proses,
                    'kd_kecamatan' => $kd_kecamatan_proses,
                    'kd_kelurahan' => $kd_kelurahan_proses,
                    'kd_blok' => $kd_blok_proses,
                    'no_urut' => $no_urut_proses,
                    'kd_jns_op' => $kd_jns_op_proses,
                    'kd_propinsi_asal' => $kd_propinsi,
                    'kd_dati2_asal' => $kd_dati2,
                    'kd_kecamatan_asal' => $kd_kecamatan,
                    'kd_kelurahan_asal' => $kd_kelurahan,
                    'kd_blok_asal' => $kd_blok,
                    'no_urut_asal' => $no_urut,
                    'kd_jns_op_asal' => $kd_jns_op,
                    'created_at' => now(),
                    'created_by' => auth()->user()->id,
                    'psmb_id' => $pm->id,
                    'thn_pajak_sppt' => $item->thn_pajak_sppt
                ];

                PembayaranMutasiData::create($bmo);
            }

            $set_null = [];
            if (count($tahunPenetapan) > 0) {
                $sqlpenetapan = "";
                $hariIni = Carbon::now();
                // Tentukan tanggal 1 Januari dan 31 Agustus
                $tanggalAwal = Carbon::parse(date('Y') . '-01-01');
                $tanggalAkhir = Carbon::parse(date('Y') . '-08-31');

                // Tentukan variabel berdasarkan pengkondisian
                if ($hariIni->between($tanggalAwal, $tanggalAkhir)) {
                    $tanggalTujuan = Carbon::parse(date('Y') . '-08-31');
                } else {
                    $tanggalTujuan = Carbon::parse(date('Y') . '-12-30');
                }

                $jatuh_tempo = $tanggalTujuan->format('Ymd');

                foreach ($tahunPenetapan as $tahun) {

                    // identifikasi HKPD nop asal
                    $hkpd = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                        ->whereraw(" thn_hkpd='$tahun' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                          and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")->count();


                    if ($hkpd == 0) {
                        $lh = DB::connection("oracle_satutujuh")->table("sppt")->select("hkpd")
                            ->whereraw("thn_pajak_sppt='$tahun'and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                          and kd_blok='$kd_blok' and no_urut='$no_urut' and kd_jns_op='$kd_jns_op'")
                            ->first();

                        if ($lh) {
                            $nilai = $lh->hkpd;
                            $inshkpd = [
                                'kd_propinsi' => $kd_propinsi_proses,
                                'kd_dati2' => $kd_dati2_proses,
                                'kd_kecamatan' => $kd_kecamatan_proses,
                                'kd_kelurahan' => $kd_kelurahan_proses,
                                'kd_blok' => $kd_blok_proses,
                                'no_urut' => $no_urut_proses,
                                'kd_jns_op' => $kd_jns_op_proses,
                                'hkpd' => $nilai,
                                'thn_hkpd' => $tahun,
                                'created_at' => Carbon::now(),
                                'created_by' => auth()->user()->id
                            ];

                            DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")->insert($inshkpd);
                            DB::connection("oracle_satutujuh")->commit();
                        }
                    }


                    // menetapkan
                    $tgl_terbit = date('Ymd');
                    $ls = PenetapanSppt::proses($kd_propinsi_proses, $kd_dati2_proses, $kd_kecamatan_proses, $kd_kelurahan_proses, $kd_blok_proses, $no_urut_proses, $kd_jns_op_proses, $tahun, $tgl_terbit, $jatuh_tempo);

                    $sb = $ls['PBB_YG_HARUS_DIBAYAR_SPPT'] ?? 0;
                    $sql_penetapan = "";
                    $sql_penetapan .= "
                      delete from pbb.MS_POTONGAN_INDIVIDU where   thn_pajak_sppt='$tahun' and kd_kecamatan='$kd_kecamatan_proses' and kd_kelurahan='$kd_kelurahan_proses'
                      and kd_blok='$kd_blok_proses' and no_urut='$no_urut_proses' and kd_jns_op='$kd_jns_op_proses';";


                    $sql_penetapan .= " hitung_potongan('$kd_kecamatan_proses','$kd_kelurahan_proses','$kd_blok_proses','$no_urut_proses','$kd_jns_op_proses','$tahun'); ";

                    if ($sqlpenetapan <> '') {
                        DB::connection("oracle_satutujuh")->statement(DB::raw("BEGIN
                                            " . $sqlpenetapan . "
                                        END;"));
                    }
                }
            }

            // membentuk nota perhitungan
            $tagihanDua = DB::connection("oracle_spo")->table(DB::raw("(
                                            select sppt_oltp.*
                                            from sppt_oltp
                                            left join pbb.sppt_koreksi on sppt_oltp.kd_propinsi=sppt_koreksi.kd_propinsi and
                                            sppt_oltp.kd_dati2=sppt_koreksi.kd_dati2 and
                                            sppt_oltp.kd_Kecamatan=sppt_koreksi.kd_Kecamatan and
                                            sppt_oltp.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                                            sppt_oltp.kd_blok=sppt_koreksi.kd_blok and
                                            sppt_oltp.no_urut=sppt_koreksi.no_urut and
                                            sppt_oltp.kd_jns_op=sppt_koreksi.kd_jns_op and
                                            sppt_oltp.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt and
                                            sppt_koreksi.jns_koreksi='3'
                                            where jns_koreksi is null
                                        )  sppt_oltp"))
                ->select(db::raw("sppt_oltp.kd_propinsi,
                                        sppt_oltp.kd_dati2,
                                        sppt_oltp.kd_Kecamatan,
                                        sppt_oltp.kd_kelurahan,
                                        sppt_oltp.kd_blok,
                                        sppt_oltp.no_urut,
                                        sppt_oltp.kd_jns_op,
                                        sppt_oltp.thn_pajak_sppt  tahun_pajak,
                    PBB_YG_HARUS_DIBAYAR_SPPT  pbb,nm_wp_sppt nm_wp,PBB_YG_HARUS_DIBAYAR_SPPT pokok   , denda denda,PBB_YG_HARUS_DIBAYAR_SPPT+denda total"))
                ->whereraw("status_pembayaran_sppt!='1' and kd_propinsi='$kd_propinsi_proses' and kd_dati2='$kd_dati2_proses' 
                        and kd_kecamatan='$kd_kecamatan_proses'  and kd_kelurahan='$kd_kelurahan_proses' and kd_blok='$kd_blok_proses' 
                        and no_urut='$no_urut_proses' and kd_jns_op='$kd_jns_op_proses'")
                ->get();
            $ArTagihan = [];
            $cek_terbit = "";
            foreach ($tagihanDua as $tagihan) {
                $cek_terbit .= "(kd_propinsi='" . $tagihan->kd_propinsi . "' and 
                    kd_dati2='" . $tagihan->kd_dati2 . "' and 
                    kd_kecamatan='" . $tagihan->kd_kecamatan . "' and 
                    kd_kelurahan='" . $tagihan->kd_kelurahan . "' and 
                    kd_blok='" . $tagihan->kd_blok . "' and 
                    no_urut='" . $tagihan->no_urut . "' and 
                    kd_jns_op='" . $tagihan->kd_jns_op . "' and 
                    tahun_pajak='" . $tagihan->tahun_pajak . "' and tgl_terbit_sppt is nul ) or ";

                $ArTagihan[] = [
                    'kd_propinsi' => $tagihan->kd_propinsi,
                    'kd_dati2' => $tagihan->kd_dati2,
                    'kd_kecamatan' => $tagihan->kd_kecamatan,
                    'kd_kelurahan' => $tagihan->kd_kelurahan,
                    'kd_blok' => $tagihan->kd_blok,
                    'no_urut' => $tagihan->no_urut,
                    'kd_jns_op' => $tagihan->kd_jns_op,
                    'tahun_pajak' => $tagihan->tahun_pajak,
                    'pbb' => $tagihan->pbb,
                    'nm_wp' => $tagihan->nm_wp,
                    'pokok' => $tagihan->pokok,
                    'denda' => $tagihan->denda,
                    'total' => $tagihan->total
                ];
            }


            $url_cetak = "";
            // membentuk kode billing
            if (count($ArTagihan) > 0) {


                $year = date('Y');
                $urut = DB::table('data_billing')
                    ->select(db::raw("nvl(max( CAST(no_urut AS NUMBER)),0) nomer"))->where('tahun_pajak', $year)
                    ->where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)
                    ->where('kd_jns_op', 4)
                    ->first();
                $kode = $urut->nomer;

                $no_urut = sprintf("%04s", $kode + 1);
                $kobil = $kd_propinsi_proses . $kd_dati2_proses . $kd_kecamatan_proses . $kd_kelurahan_proses . '999' . $no_urut . '4';
                $lokasi = DB::connection('oracle_dua')->table('ref_kelurahan')
                    ->join('ref_kecamatan', 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                    ->selectraw("nm_kecamatan,nm_kelurahan")
                    ->whereraw("ref_kecamatan.KD_KECAMATAN ='$kd_kecamatan_proses' and KD_KELURAHAN = '$kd_kelurahan_proses'")->first();
                $kecamatan = $lokasi->nm_kecamatan;
                $kelurahan = $lokasi->nm_kelurahan;
                $setDatabiling = array(
                    "KD_PROPINSI" => $kd_propinsi_proses,
                    "KD_DATI2" => $kd_dati2_proses,
                    "KD_KECAMATAN" => $kd_kecamatan_proses,
                    "KD_KELURAHAN" => $kd_kelurahan_proses,
                    "KD_BLOK" => '999',
                    "NO_URUT" => $no_urut,
                    "KD_JNS_OP" => '4',
                    "TAHUN_PAJAK" => $year,
                    "KD_STATUS" => "0",
                    "NAMA_WP" => $ArTagihan[count($ArTagihan) - 1]['nm_wp'],
                    "NAMA_KELURAHAN" => $kelurahan,
                    "NAMA_KECAMATAN" => $kecamatan,
                    "KOBIL" => $kobil
                );
                $Data_billing = Data_billing::create($setDatabiling);
                // $Data_billing="";

                $getId = $Data_billing->data_billing_id;
                $url_cetak = url('data_dafnom/pdf_cetak') . '/' . $getId;
                $kode_billing = $kobil;
                $billing_kolektif = [];
                $tglNow = Carbon::now();
                $montNow = $tglNow->month;
                $next = $tglNow->addDays(1);
                if ($montNow != $next->month) {
                    $next = Carbon::now()->endOfMonth();
                }

                // dd($ArTagihan);

                foreach ($ArTagihan as $tag) {
                    $billing_kolektif[] = [
                        'data_billing_id' => $getId,
                        'kd_propinsi' => $tag['kd_propinsi'],
                        'kd_dati2' => $tag['kd_dati2'],
                        'kd_kecamatan' => $tag['kd_kecamatan'],
                        'kd_kelurahan' => $tag['kd_kelurahan'],
                        'kd_blok' => $tag['kd_blok'],
                        'no_urut' => $tag['no_urut'],
                        'kd_jns_op' => $tag['kd_jns_op'],
                        'tahun_pajak' => $tag['tahun_pajak'],
                        'pbb' => $tag['pbb'],
                        'nm_wp' => $tag['nm_wp'],
                        'pokok' => $tag['pokok'],
                        'denda' => $tag['denda'],
                        'total' => $tag['total'],
                        'expired_at' => new carbon($Data_billing->expired_at ?? '')
                    ];
                }
                Billing_kolektif::insert($billing_kolektif);
            }




            // nota
            $peg = pegawaiStruktural::where('kode', '02')->first();
            $nota = Notaperhitungan::create([
                'tanggal' => now(),
                'kota' => 'Kepanjen',
                'pegawai' => $peg->nama,
                'nip' => $peg->nip,
                'jabatan' => $peg->jabatan,
                'pangkat' => '-',
                'kobil' => $kobil,
                'data_billing_id' => $getId,
                'pendataan_objek_id' => $objek->id
            ]);
            // UPDATE EXPIRED
            $ta = date('Y') . '1130';
            if (strtotime(date('Ymd'))   >= strtotime(date('Y') . '1130')) {
                $ta = date('Y') . '1230';
            }

            $exp = new Carbon($ta);
            Data_billing::where('kobil', $kobil)->update(['expired_at' => $exp]);

            DB::commit();
            $pesan = "Berhasil di aktifkan";
            $status = '1';
        } catch (\Throwable $th) {

            DB::rollback();
            Log::error($th);
            $pesan = $th->getMessage();
            $status = '0';
            $url_cetak = "";
        }


        return [
            'kode_billing' => $kode_billing,
            'pesan' => $pesan,
            'status' => $status,
            'url' => $url_cetak
        ];
    }

    public function loadTunggakan(Request $request)
    {
        $kd_propinsi = $request->kd_propinsi;
        $kd_dati2 = $request->kd_dati2;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_blok = $request->kd_blok;
        $no_urut = $request->no_urut;
        $kd_jns_op = $request->kd_jns_op;

        /* 
        $start = date('Y');
        if ($kd_blok != '000') {
            // cek tahun minimal
            $minth = DB::connection("oracle_satutujuh")->table('sppt')
                ->whereraw("kd_propinsi = '$kd_propinsi'
            and kd_dati2 = '$kd_dati2'
            and kd_kecamatan = '$kd_kecamatan'
            and kd_kelurahan = '$kd_kelurahan'
            and kd_blok = '$kd_blok'
            and no_urut = '$no_urut'
            and kd_jns_op = '$kd_jns_op'")
                ->select(db::raw("min(cast(thn_pajak_sppt as number) ) tahun"))
                ->first();

            if ($minth->tahun != '') {
                $start = $minth->tahun;
            } else {
                $cs = DB::connection("oracle_satutujuh")->table("ref_kelurahan")->select("thn_sismiop")
                    ->whereraw("kd_propinsi = '$kd_propinsi'
                            and kd_dati2 = '$kd_dati2'
                            and kd_kecamatan = '$kd_kecamatan'
                                and kd_kelurahan = '$kd_kelurahan'")->first();
                $start = $cs->thn_sismiop <> '' ? $cs->thn_sismiop : date('Y');
            }
        }
        $tagihan = DB::connection("oracle_satutujuh")->select(DB::raw("select a.tahun,
                                pbb,
                                potongan,
                                terbayar,
                                koreksi,
                                sisa
                        from (    select " . $start . " + level - 1 tahun
                                    from dual
                                connect by " . $start . " + level - 1 <= to_char (sysdate, 'yyyy')) a
                                left join
                                (select sppt.thn_pajak_sppt masa_pajak,
                                        to_char (tgl_terbit_sppt, 'yyyy') tahun_piutang,
                                        pbb_yg_harus_dibayar_sppt pbb,
                                        nvl (nilai_potongan, 0) potongan,
                                        case
                                        when nvl (pokok, 0) >
                                                (  pbb_yg_harus_dibayar_sppt
                                                    - nvl (nilai_potongan, 0))
                                        then
                                            (pbb_yg_harus_dibayar_sppt - nvl (nilai_potongan, 0))
                                        else
                                            nvl (pokok, 0)
                                        end
                                        terbayar,
                                        case when jns_koreksi='3' then 
                                        pbb_yg_harus_dibayar_sppt - 
                                        nvl (nilai_potongan, 0) else 0 end koreksi,
                                        (  pbb_yg_harus_dibayar_sppt
                                        - nvl (nilai_potongan, 0)
                                        - (case
                                            when nvl (pokok, 0) >
                                                    (  pbb_yg_harus_dibayar_sppt
                                                        - nvl (nilai_potongan, 0))
                                            then
                                                (pbb_yg_harus_dibayar_sppt - nvl (nilai_potongan, 0))
                                            else
                                                nvl (pokok, 0)
                                            end) - (case when jns_koreksi='3' then 
                                        pbb_yg_harus_dibayar_sppt - 
                                        nvl (nilai_potongan, 0) else 0 end))
                                        sisa,
                                        no_transaksi,
                                        jns_koreksi
                                from sppt
                                        left join sppt_potongan
                                        on     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                                            and sppt.kd_dati2 = sppt_potongan.kd_dati2
                                            and sppt.kd_kecamatan = sppt_potongan.kd_kecamatan
                                            and sppt.kd_kelurahan = sppt_potongan.kd_kelurahan
                                            and sppt.kd_blok = sppt_potongan.kd_blok
                                            and sppt.no_urut = sppt_potongan.no_urut
                                            and sppt.kd_jns_op = sppt_potongan.kd_jns_op
                                            and sppt.thn_pajak_sppt = sppt_potongan.thn_pajak_sppt
                                        left join sppt_koreksi
                                        on     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                            and sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                            and sppt.kd_kecamatan = sppt_koreksi.kd_kecamatan
                                            and sppt.kd_kelurahan = sppt_koreksi.kd_kelurahan
                                            and sppt.kd_blok = sppt_koreksi.kd_blok
                                            and sppt.no_urut = sppt_koreksi.no_urut
                                            and sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                            and sppt.thn_pajak_sppt = sppt_koreksi.thn_pajak_sppt
                                        left join
                                        (  select kd_propinsi,
                                                kd_dati2,
                                                kd_kecamatan,
                                                kd_kelurahan,
                                                kd_blok,
                                                no_urut,
                                                kd_jns_op,
                                                thn_pajak_sppt,
                                                sum (
                                                    nvl (jml_sppt_yg_dibayar, 0) - nvl (denda_sppt, 0))
                                                    pokok
                                            from pembayaran_sppt
                                        group by kd_propinsi,
                                                kd_dati2,
                                                kd_kecamatan,
                                                kd_kelurahan,
                                                kd_blok,
                                                no_urut,
                                                kd_jns_op,
                                                thn_pajak_sppt) pembayaran_sppt
                                        on     sppt.kd_propinsi = pembayaran_sppt.kd_propinsi
                                            and sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                            and sppt.kd_kecamatan = pembayaran_sppt.kd_kecamatan
                                            and sppt.kd_kelurahan = pembayaran_sppt.kd_kelurahan
                                            and sppt.kd_blok = pembayaran_sppt.kd_blok
                                            and sppt.no_urut = pembayaran_sppt.no_urut
                                            and sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                            and sppt.thn_pajak_sppt = pembayaran_sppt.thn_pajak_sppt
                                where     sppt.kd_propinsi = '$kd_propinsi'
                                        and sppt.kd_dati2 = '$kd_dati2'
                                        and sppt.kd_kecamatan = '$kd_kecamatan'
                                        and sppt.kd_kelurahan = '$kd_kelurahan'
                                        and sppt.kd_blok = '$kd_blok'
                                        and sppt.no_urut = '$no_urut'
                                        and sppt.kd_jns_op = '$kd_jns_op') b
                                on b.masa_pajak = a.tahun
                        order by tahun asc"));
 */

        $tagihan = [];

        $wp = DAT_OBJEK_PAJAK::with(['Kecamatan',  'DatSubjekPajak'])->where(
            [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op
            ]
        )->first();

        return view('analisa.modal_nop_non_aktif', compact('tagihan', 'wp'));
    }

    public function nop_non_aktif_search(Request $request)
    {
        $search = $request->only('nop');
        $where = "";
        if ($search && in_array('nop', array_keys($search))) {
            if ($search['nop'] != '') {
                $nop = explode('.', $search['nop']);
                $blok_urut = explode('-', $nop['4']);
                $where = "
                AND mdop.kd_propinsi='" . $nop[0] . "'
                AND mdop.kd_dati2='" . $nop[1] . "'
                AND mdop.kd_kecamatan='" . $nop[2] . "'
                AND mdop.kd_kelurahan='" . $nop[3] . "'
                AND mdop.kd_blok='" . $blok_urut[0] . "'
                AND mdop.no_urut='" . $blok_urut[1] . "'
                AND mdop.kd_jns_op='" . $nop[5] . "'
                ";
            }
        }
        $year = Carbon::now()->year;
        $sql = "SELECT distinct mdop.kd_propinsi,
                    mdop.kd_dati2,
                    mdop.kd_kecamatan,
                    mdop.kd_kelurahan,
                    mdop.kd_blok,
                    mdop.no_urut,
                    mdop.kd_jns_op,
                    mdop.jalan_op,
                    mdop.total_luas_bumi,
                    mdop.total_luas_bng,
                    mdsp.nm_wp,
                    mdsp.jalan_wp,
                    mdsp.telp_wp,
                    mdop.subjek_pajak_id,
                    uh.unflag_time,
                    uh.thn_pajak_sppt,
                    dob.jns_bumi,
                    case when uh.unflag_desc is null then '-' else uh.unflag_desc end keterangan
                FROM
                    DAT_OBJEK_PAJAK mdop 
                LEFT JOIN DAT_SUBJEK_PAJAK mdsp 
                    ON mdsp.SUBJEK_PAJAK_ID =mdop.SUBJEK_PAJAK_ID 
                LEFT JOIN DAT_OP_BUMI dob 
                	ON dob.kd_propinsi=mdop.kd_propinsi
                    AND dob.kd_dati2=mdop.kd_dati2
                    AND dob.kd_kecamatan=mdop.kd_kecamatan
                    AND dob.kd_kelurahan=mdop.kd_kelurahan
                    AND dob.kd_blok=mdop.kd_blok
                    AND dob.no_urut=mdop.no_urut
                    AND dob.kd_jns_op=mdop.kd_jns_op
                    left join (
   select kd_propinsi,kd_dati2,kd_Kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,max(unflag_id) u_id 
    from unflag_history
    group by kd_propinsi,kd_dati2,kd_Kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op) ui on ui.kd_propinsi = mdop.kd_propinsi
AND ui.kd_dati2 = mdop.kd_dati2
AND ui.kd_kecamatan = mdop.kd_kecamatan
AND ui.kd_kelurahan = mdop.kd_kelurahan
AND ui.kd_blok = mdop.kd_blok
AND ui.no_urut = mdop.no_urut
AND ui.kd_jns_op = mdop.kd_jns_op 
LEFT JOIN pbb.UNFLAG_HISTORY uh 
                    ON uh.unflag_id=ui.u_id
                WHERE (mdop.JNS_TRANSAKSI_OP='3' OR dob.JNS_BUMI='5' OR dob.JNS_BUMI='4')
                " . $where;

        $dataSet = DB::connection('oracle_dua')->select(db::raw($sql));
        // dd($dataSet);
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('nop', function ($data) {
            return $data->kd_propinsi . "." .
                $data->kd_dati2 . "." .
                $data->kd_kecamatan . "." .
                $data->kd_kelurahan . "." .
                $data->kd_blok . "-" .
                $data->no_urut . "." .
                $data->kd_jns_op;
        })
            ->addColumn('nik', function ($data) {
                return $data->subjek_pajak_id;
            })
            ->addColumn('alasan', function ($data) {
                if (in_array($data->jns_bumi, ['5', '4'])) {
                    return 'Objek - [' . jenisBumi($data->jns_bumi) . ']';
                }
                return 'Subjek - Tidak Aktif';
            })
            ->addColumn('action', function ($data) {
                $nop = $data->kd_propinsi . "." .
                    $data->kd_dati2 . "." .
                    $data->kd_kecamatan . "." .
                    $data->kd_kelurahan . "." .
                    $data->kd_blok . "-" .
                    $data->no_urut . "." .
                    $data->kd_jns_op;
                return gallade::buttonInfo('<i class="fas fa-chevron-circle-right"></i> Pengaktifan NOP', 'title="Pengaktifan NOP" data-pengaktifan="' . $nop . '"');
            })
            ->editColumn('unflag_time', function ($data) {
                return tglIndo($data->unflag_time);
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function nop_non_aktif_cetak(Request $request)
    {
        $search = $request->only('nop');
        $where = "";
        try {
            if ($search && in_array('nop', array_keys($search))) {
                if ($search['nop'] != '') {
                    $nop = explode('.', $search['nop']);
                    $blok_urut = explode('-', $nop['4']);
                    $where = "
                    AND mdop.kd_propinsi='" . $nop[0] . "'
                    AND mdop.kd_dati2='" . $nop[1] . "'
                    AND mdop.kd_kecamatan='" . $nop[2] . "'
                    AND mdop.kd_kelurahan='" . $nop[3] . "'
                    AND mdop.kd_blok='" . $blok_urut[0] . "'
                    AND mdop.no_urut='" . $blok_urut[1] . "'
                    AND mdop.kd_jns_op='" . $nop[5] . "'
                    ";
                }
            }
            $year = Carbon::now()->year;
            $sql = "SELECT distinct mdop.kd_propinsi,
                        mdop.kd_dati2,
                        mdop.kd_kecamatan,
                        mdop.kd_kelurahan,
                        mdop.kd_blok,
                        mdop.no_urut,
                        mdop.kd_jns_op,
                        mdop.jalan_op,
                        mdop.total_luas_bumi,
                        mdop.total_luas_bng,
                        mdsp.nm_wp,
                        mdsp.jalan_wp,
                        mdsp.telp_wp,
                        mdop.subjek_pajak_id,
                        uh.unflag_time,
                        case when uh.unflag_desc is null then '-' else uh.unflag_desc end keterangan
                    FROM
                        DAT_OBJEK_PAJAK mdop 
                    LEFT JOIN DAT_SUBJEK_PAJAK mdsp 
                        ON mdsp.SUBJEK_PAJAK_ID =mdop.SUBJEK_PAJAK_ID 
                    LEFT JOIN pbb.UNFLAG_HISTORY uh 
                        ON uh.kd_propinsi=mdop.kd_propinsi
                        AND uh.kd_dati2=mdop.kd_dati2
                        AND uh.kd_kecamatan=mdop.kd_kecamatan
                        AND uh.kd_kelurahan=mdop.kd_kelurahan
                        AND uh.kd_blok=mdop.kd_blok
                        AND uh.no_urut=mdop.no_urut
                        AND uh.kd_jns_op=mdop.kd_jns_op
                    WHERE mdop.JNS_TRANSAKSI_OP='3'
                    " . $where . "
                    order by 
                        mdop.kd_propinsi,
                        mdop.kd_dati2,
                        mdop.kd_kecamatan,
                        mdop.kd_kelurahan,
                        mdop.kd_blok,
                        mdop.no_urut,
                        mdop.kd_jns_op
                    ";
            $dataSet = DB::connection('oracle_dua')->select(db::raw($sql));
            $result = [];
            $no = 1;
            foreach ($dataSet as $item) {
                $item = (array)$item;
                $result[] = [
                    $no,
                    $item['subjek_pajak_id'],
                    $item['nm_wp'],
                    $item['kd_propinsi'] . '.' .
                        $item['kd_dati2'] . '.' .
                        $item['kd_kecamatan'] . '.' .
                        $item['kd_kelurahan'] . '.' .
                        $item['kd_blok'] . '-' .
                        $item['no_urut'] . '.' .
                        $item['kd_jns_op'],
                    $item['jalan_op'],
                    $item['total_luas_bumi'],
                    $item['total_luas_bng'],
                    $item['unflag_time'],
                    $item['keterangan']
                ];
                $no++;
            }
            $export = new NopNonAktif($result);
            return Excel::download($export, 'nop_non_aktif.xlsx');
            // // return view('analisa.cetak.nop_non_aktif',compact('result'));
            // $pages = View::make('analisa.cetak.nop_non_aktif',compact('result'));
            // $pdf = pdf::loadHTML($pages)
            //     ->setPaper('A4');
            // $pdf->setOption('enable-local-file-access', true);
            // return $pdf->stream();
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    public function pengaktifan_nop(Request $request)
    {
        $request = $request->only('data');
        $getNOP = Http::get(url('api/ceknop'), [
            'nop' => $request['data'],
            'noTag' => true
        ]);
        $return = ["msg" => "Proses Pengaktifan NOP gagal.", "status" => false];
        if ($getNOP) {
            $getData = (array)$getNOP->object();
            if (count($getData)) {
                if (!isset($getData['status'])) {
                    return response()->json(["msg" => $getData['msg'], "status" => false]);
                }
                $get = (array)$getData['data'];
                $result = true;
                try {
                    $nomor_layanan = Layanan_conf::pengaktifan_nop();
                    if ($nomor_layanan) {
                        $key = '10';
                        $saveLayanan['nomor_layanan'] = $nomor_layanan;
                        $saveLayanan['jenis_layanan_id'] = $key;
                        $saveLayanan['jenis_layanan_nama'] = Jenis_layanan::find($key)->nama_layanan;
                        $saveLayanan['nik'] = '0000000000000000';
                        $saveLayanan['nama'] = 'Pengaktifan NOP  [1 Permohonan ]';
                        $saveLayanan['alamat'] = '-';
                        $saveLayanan['kelurahan'] = '-';
                        $saveLayanan['kecamatan'] = '-';
                        $saveLayanan['dati2'] = 'KABUPATEN MALANG';
                        $saveLayanan['propinsi'] = 'JAWA TIMUR';
                        $saveLayanan['nomor_telepon'] = '0000000';
                        $saveLayanan['keterangan'] = 'Pengaktifan NOP';
                        $saveLayanan['jenis_objek'] = '4';
                        $saveLayanan['kd_status'] = '0';
                        $Layanan = new Layanan($saveLayanan);
                        $Layanan->save();
                        $nop = explode('.', $request['data']);
                        $blok_urut = explode('-', $nop['4']);
                        $saveLayananObjek = [
                            'nik_wp' => $get['nik_wp'],
                            'nama_wp' => $get['nama_wp'],
                            'alamat_wp' => $get['alamat_wp'] ?? '-',
                            'kelurahan_wp' => $get['kelurahan_wp'] ?? '-',
                            'kecamatan_wp' => $get['kecamatan_wp'] ?? '-',
                            'dati2_wp' => $get['dati2_wp'],
                            'propinsi_wp' => $get['propinsi_wp'],
                            'rt_wp' => $get['rt_wp'],
                            'rw_wp' => $get['rw_wp'],
                            'telp_wp' => $get['notelp_wp'],
                            'alamat_op' => $get['alamat_op'],
                            'rt_op' => $get['nop_rt'],
                            'rw_op' => $get['nop_rw'],
                            'kd_propinsi' => $nop[0],
                            'kd_dati2' => $nop[1],
                            'kd_kecamatan' => $nop[2],
                            'kd_kelurahan' => $nop[3],
                            'kd_blok' => $blok_urut[0],
                            'no_urut' => $blok_urut[1],
                            'kd_jns_op' => $nop[5],
                            'nomor_layanan' => $nomor_layanan,
                            'luas_bumi' => $get['luas_bumi'],
                            'luas_bng' => $get['luas_bng'],
                            'njop_bumi' => $get['njop_bumi'],
                            'njop_bng' => $get['njop_bng']
                        ];
                        $savelayanan_objek = new Layanan_objek($saveLayananObjek);
                        $Layanan->layanan_objek()->save($savelayanan_objek);
                    }
                } catch (\Exception $e) {
                    $return = $e->getMessage();
                    $result = false;
                }
                // dd($return);
                if ($result) {
                    dispatch(new disposisiPenelitian($nomor_layanan))->onQueue('layanan');
                    $return = [
                        "msg" => "<p>Persiapan Pengaktifan NOP bisa dilanjutkan dengan proses Pengaktifan.</p>
                                <div class='btn-group col-12'>
                                <a href='" . url("penelitian/kantor?nomor_layanan=" . $nomor_layanan) . "'  target='_BLANK' class='btn btn-default'><i class='fas fa-file-alt'></i> Proses Pengaktifan</a></div>
                                ",
                        "status" => true,
                    ];
                }
            }
        }
        return response()->json($return);
    }
    public function perbandingan_njop()
    {
        $result = [];
        $User = Auth()->user();
        $user = $User->Unitkerja()->pluck('UNIT_KERJA.kd_unit')->first();
        $listUser = User::whereRaw(DB::raw("user_unit_kerja.kd_unit like '" . $user . "%'"))
            ->leftJoin('user_unit_kerja', function ($join) {
                $join->On('user_unit_kerja.user_id', '=', 'users.id');
            })
            ->orderBy('users.nama')
            ->get();
        $Jenis_layanan = Jenis_layanan::all();
        return view('laporan_penelitian.perbandingan_njop', compact('result', 'listUser', 'Jenis_layanan'));
    }
    public function perbandingan_njop_search(Request $request)
    {
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        $where = false;
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $where .= " WHERE a.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and a.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')";
            } else {
                $where .= " WHERE to_char(a.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'";
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $where .= " WHERE to_char(a.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'";
            }
        }
        $and = ($where) ? ' and ' : ' WHERE ';
        $listUser = $request->only('list_user');
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $where .= $and . " a.pemutakhiran_by='" . $listUser['list_user'] . "'";
            }
        }
        $setLayanan = $request->only(['layanan']);
        $and = ($where) ? ' and ' : ' WHERE ';
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $where .= $and . " l.jenis_layanan_id='" . $setLayanan['layanan'] . "'";
            }
        }
        $dataSet = $this->perbandinganJob($where);
        $datatables = datatables::of($dataSet);
        return $datatables->addIndexColumn()->make(true);
    }
    public function perbandingan_njop_cetak(Request $request)
    {
        ini_set('memory_limit', '-1');
        $setTgl = $request->only(['tgl', 'all']);
        $tanggal = "";
        $where = false;
        if (in_array('tgl', array_keys($setTgl)) && !in_array('all', array_keys($setTgl))) {
            $tgl = explode('-', str_replace(' ', '', $setTgl['tgl']));
            $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
            $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
            if ($after != $before) {
                $where .= " WHERE a.pemutakhiran_at>=TO_DATE ('" . $after . "','dd/mm/yyyy') and a.pemutakhiran_at<=TO_DATE ('" . $before . "','dd/mm/yyyy')";
            } else {
                $where .= " WHERE to_char(a.pemutakhiran_at,'dd/mm/yyyy')='" . $after . "'";
            }
        } else {
            if (!in_array('all', array_keys($setTgl))) {
                $now = Carbon::now()->format('d/m/Y');
                $where .= " WHERE to_char(a.pemutakhiran_at,'dd/mm/yyyy')='" . $now . "'";
            }
        }
        $and = ($where) ? ' and ' : ' WHERE ';
        $listUser = $request->only('list_user');
        if ($listUser) {
            if ($listUser['list_user'] != "-") {
                $where .= $and . " a.pemutakhiran_by='" . $listUser['list_user'] . "'";
            }
        }
        $setLayanan = $request->only(['layanan']);
        $and = ($where) ? ' and ' : ' WHERE ';
        if (in_array('layanan', array_keys($setLayanan))) {
            if ($setLayanan['layanan'] != "-") {
                $where .= $and . " l.jenis_layanan_id='" . $setLayanan['layanan'] . "'";
            }
        }
        try {
            $dataSet = $this->perbandinganJob($where);
            $result = [[
                'NO',
                'NOMOR PELAYANAN',
                'NOP',
                'JENIS AJUAN',
                'NJOP AWAL (T)',
                'NJOP AWAL (B)',
                'NJOP AKHIR (T)',
                'NJOP AKHIR (B)',
                'PROSENTASE (T)',
                'PROSENTASE (B)',
                'PENELITI'
            ]];
            $no = 1;
            foreach ($dataSet as $item) {
                $item = (array)$item;
                $result[] = [
                    $no,
                    $item['nomor_layanan'],
                    $item['nop'],
                    $item['jenis_layanan_nama'],
                    $item['njop_bumi_meter_last'],
                    $item['njop_bng_meter_last'],
                    $item['njop_bumi_meter'],
                    $item['njop_bng_meter'],
                    $item['persen_bumi'],
                    $item['persen_bng'],
                    $item['pemutakhiran_nama']
                ];
                $no++;
            }
            $export = new Perbandingan_njop($result);
            return Excel::download($export, 'perbandingan_njop.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    private function perbandinganJob($where = false)
    {
        $raw = "SELECT
                NOMOR_LAYANAN,
                KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'-'||NO_URUT||'.'||KD_JNS_OP NOP,
                JENIS_LAYANAN_NAMA,
                LUAS_BUMI,
                LUAS_BNG,
                NJOP_BUMI_METER,
                NJOP_BNG_METER,
                NJOP_BUMI_METER_LAST,
                NJOP_BNG_METER_LAST,
                CASE
                    WHEN NJOP_BUMI_METER_LAST >NJOP_BUMI_METER THEN round(( NJOP_BUMI_METER-NJOP_BUMI_METER_LAST) / NJOP_BUMI_METER_LAST * 100, 0)
                    ELSE 0
                END persen_bumi,
                CASE
                    WHEN NJOP_BNG_METER_LAST >NJOP_BNG_METER THEN round(( NJOP_BNG_METER-NJOP_BNG_METER_LAST) / NJOP_BNG_METER_LAST * 100, 0)
                    ELSE 0
                END persen_bng,
                pemutakhiran_at,
                pemutakhiran_by,
                pemutakhiran_nama
            FROM
                (
                SELECT
                    DISTINCT a.nomor_layanan,
                    b.kd_propinsi,
                    b.kd_dati2,
                    b.kd_kecamatan,
                    b.kd_kelurahan,
                    b.kd_blok,
                    b.no_urut,
                    b.kd_jns_op,
                    jl.nama_layanan jenis_layanan_nama,
                    total_luas_bumi luas_bumi,
                    total_luas_bng luas_bng,
                    CASE
                        WHEN total_luas_bumi>0 THEN round(b.njop_bumi / total_luas_bumi, 0)
                        ELSE 0
                    END njop_bumi_meter,
                    CASE
                        WHEN c.njop_bumi_sppt IS NULL THEN CASE
                            WHEN d.luas_bumi_sppt IS NOT NULL THEN d.njop_bumi_sppt / d.luas_bumi_sppt
                            ELSE 0
                        END
                        ELSE CASE
                            WHEN c.luas_bumi_sppt IS NOT NULL THEN c.njop_bumi_sppt / c.luas_bumi_sppt
                            ELSE 0
                        END
                    END njop_bumi_meter_last,
                    CASE
                        WHEN total_luas_bng>0 THEN round(b.njop_bng / total_luas_bng, 0)
                        ELSE 0
                    END njop_bng_meter,
                    CASE
                        WHEN c.njop_bng_sppt IS NULL THEN (CASE
                            WHEN d.njop_bng_sppt >0 THEN d.njop_bng_sppt / d.luas_bng_sppt
                            ELSE 0
                        END)
                        ELSE (CASE
                            WHEN c.luas_bng_sppt>0 THEN c.njop_bng_sppt / c.luas_bng_sppt
                            ELSE 0
                        END)
                    END NJOP_BNG_METER_LAST,
                    a.pemutakhiran_at,
                    a.pemutakhiran_by,
                    e.nama pemutakhiran_nama
                FROM
                    layanan_objek a
                join layanan l
                    on l.nomor_layanan=a.nomor_layanan
                join jenis_layanan jl
                    on jl.id=l.jenis_layanan_id
                JOIN pbb.dat_objek_pajak b ON
                    a.nomor_formulir = b.no_formulir_spop
                LEFT JOIN sppt_bkp c ON
                    c.layanan_objek_id = a.id
                LEFT JOIN sppt_bkp d ON
                    d.layanan_objek_id = a.nop_gabung
                LEFT JOIN users e ON
                    e.id = a.pemutakhiran_by
                " . $where . "
            ) ";
        return DB::select($raw);
    }

    public function NjopTahunQuery($now, $last)
    {
        return  DB::connection("oracle_satutujuh")
            ->table(db::raw("(SELECT kd_propinsi,
                                kd_dati2,
                                kd_kecamatan,
                                kd_kelurahan,
                                kd_blok,
                                no_urut,
                                kd_jns_op,
                                nm_wp_sppt,
                                luas_bumi_sppt luas_bumi,
                                CASE
                                WHEN luas_bumi_sppt > 0
                                THEN
                                    ROUND (njop_bumi_sppt / luas_bumi_sppt)
                                ELSE
                                    0
                                END
                                njop_bumi,
                                kd_kls_tanah,
                                luas_bng_sppt luas_bng,
                                CASE
                                WHEN luas_bng_sppt > 0
                                THEN
                                    ROUND (njop_bng_sppt / luas_bng_sppt)
                                ELSE
                                    0
                                END
                                njop_bng,
                                kd_kls_bng,
                                GET_TARIFPBB(njop_bng_sppt+njop_bumi_sppt,njoptkp_sppt,thn_pajak_sppt) tarif,
                                pbb_yg_harus_dibayar_sppt pbb
                        FROM sppt
                        WHERE thn_pajak_sppt = '" . $now . "') a"))
            ->join(db::raw(" (SELECT kd_propinsi,
                        kd_dati2,
                        kd_kecamatan,
                        kd_kelurahan,
                        kd_blok,
                        no_urut,
                        kd_jns_op,
                        nm_wp_sppt,
                        luas_bumi_sppt luas_bumi,
                        CASE
                            WHEN luas_bumi_sppt > 0
                            THEN
                                ROUND (njop_bumi_sppt / luas_bumi_sppt)
                            ELSE
                                0
                        END
                            njop_bumi,
                            kd_kls_tanah,
                        luas_bng_sppt luas_bng,
                        CASE
                            WHEN luas_bng_sppt > 0
                            THEN
                                ROUND (njop_bng_sppt / luas_bng_sppt)
                            ELSE
                                0
                        END
                            njop_bng,
                            kd_kls_bng,
                            GET_TARIFPBB(njop_bng_sppt+njop_bumi_sppt,njoptkp_sppt,thn_pajak_sppt) tarif,
                        pbb_yg_harus_dibayar_sppt pbb
                    FROM sppt
                WHERE thn_pajak_sppt = '$last') 
        b"), function ($join) {
                $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                    ->on('a.kd_dati2', '=', 'b.kd_dati2')
                    ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                    ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                    ->on('a.kd_blok', '=', 'b.kd_blok')
                    ->on('a.no_urut', '=', 'b.no_urut')
                    ->on('a.kd_jns_op', '=', 'b.kd_jns_op');
            })
            ->selectraw(" a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.nm_wp_sppt, b.luas_bumi luas_bumi_last, a.luas_bumi luas_bumi_now, b.njop_bumi njop_bumi_last, a.njop_bumi njop_bumi_now, b.luas_bng luas_bng_last, a.luas_bng luas_bng_now, b.njop_bng njop_bng_last, a.njop_bng njop_bng_now, b.pbb pbb_last, a.pbb pbb_now,a.kd_kls_tanah kd_kls_tanah_now,b.kd_kls_tanah kd_kls_tanah_last, a.kd_kls_bng kd_kls_bng_now,b.kd_kls_bng kd_kls_bng_last,
            rtrim(to_char(a.tarif, 'FM90D99'), to_char(0, 'D'))  tarif_now,
            rtrim(to_char(b.tarif, 'FM90D99'), to_char(0, 'D'))  tarif_last")
            ->whereraw("a.luas_bumi > 0");
    }

    public function NjopTahun(Request $request)
    {

        if ($request->ajax()) {
            $now = $request->now;
            $last = $request->last;
            $data = $this->NjopTahunQuery($now, $last);
            return  DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    $jenis = '';
                    if ($request->has('jenis')) {
                        $jenis = $request->jenis;
                    }

                    $status = '';
                    if ($request->has('status')) {
                        $status = $request->status;
                    }

                    if ($jenis == '1') {
                        // bumi
                        if ($status == '1') {
                            // naik
                            $query->whereraw("(a.njop_bumi>b.njop_bumi)");
                        } else if ($status == '2') {
                            // turun
                            $query->whereraw("(a.njop_bumi<b.njop_bumi)");
                        } else {
                            $query->whereraw("((a.njop_bumi<b.njop_bumi ) or (a.njop_bumi>b.njop_bumi))");
                        }
                    } else if ($jenis == '2') {
                        // bng
                        // $query->whereraw("(a.njop_bng<b.njop_bng and a.luas_bng>0)");
                        if ($status == '1') {
                            // naik
                            $query->whereraw("(a.njop_bng>b.njop_bng)");
                        } else if ($status == '2') {
                            // turun
                            $query->whereraw("((a.njop_bng<b.njop_bng and a.luas_bng>0))");
                        } else {
                            $query->whereraw("(((a.njop_bng<b.njop_bng and a.luas_bng>0) ) or (a.njop_bng>b.njop_bng))");
                        }
                    } else {
                        // bng dan bumi
                        if ($status == '1') {
                            // naik
                            $query->whereraw("(a.njop_bng>b.njop_bng or  a.njop_bumi>b.njop_bumi ) ");
                        } else if ($status == '2') {
                            // turun
                            $query->whereraw("((a.njop_bng<b.njop_bng and a.luas_bng>0) or  a.njop_bumi<b.njop_bumi )");
                        } else {
                            $query->whereraw("(((a.njop_bng<b.njop_bng and a.luas_bng>0) or  a.njop_bumi<b.njop_bumi  ) or (a.njop_bng>b.njop_bng or  a.njop_bumi>b.njop_bumi))");
                        }
                    }
                })
                ->addColumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->addColumn('keterangan', function ($row) {
                    $deskripsi = "";
                    if ($row->njop_bng_last > $row->njop_bng_now) {
                        $deskripsi .= "NJOP bangunan turun ,";
                    }
                    if ($row->njop_bng_last < $row->njop_bng_now) {
                        $deskripsi .= "NJOP bangunan naik ,";
                    }

                    if ($row->njop_bumi_last > $row->njop_bumi_now) {
                        $deskripsi .= "NJOP bumi turun ,";
                    }

                    if ($row->njop_bumi_last < $row->njop_bumi_now) {
                        $deskripsi .= "NJOP bumi naik ,";
                    }

                    if ($deskripsi <> '') {
                        $deskripsi = substr($deskripsi, 0, -1);
                    }

                    return $deskripsi;
                })
                ->rawColumns(['nop', 'keterangan'])->make(true);
        }

        $now = date('Y');
        $last = $now - 1;
        return  view('analisa.njop_tahun', compact('now', 'last'));
    }

    public function NjopTahunExcel(Request $request)
    {
        $now = $request->now;
        $last = $request->last;
        $jenis = $request->jenis;
        $query = $this->NjopTahunQuery($now, $last);
        // $jenis = $request->jenis;

        $jenis = '';
        if ($request->has('jenis')) {
            $jenis = $request->jenis;
        }

        $status = '';
        if ($request->has('status')) {
            $status = $request->status;
        }

        if ($jenis == '1') {
            // bumi
            if ($status == '1') {
                // naik
                $query->whereraw("(a.njop_bumi>b.njop_bumi)");
            } else if ($status == '2') {
                // turun
                $query->whereraw("(a.njop_bumi<b.njop_bumi)");
            } else {
                $query->whereraw("((a.njop_bumi<b.njop_bumi ) or (a.njop_bumi>b.njop_bumi))");
            }
        } else if ($jenis == '2') {
            // bng
            // $query->whereraw("(a.njop_bng<b.njop_bng and a.luas_bng>0)");
            if ($status == '1') {
                // naik
                $query->whereraw("(a.njop_bng>b.njop_bng)");
            } else if ($status == '2') {
                // turun
                $query->whereraw("((a.njop_bng<b.njop_bng and a.luas_bng>0))");
            } else {
                $query->whereraw("(((a.njop_bng<b.njop_bng and a.luas_bng>0) ) or (a.njop_bng>b.njop_bng))");
            }
        } else {
            // bng dan bumi
            if ($status == '1') {
                // naik
                $query->whereraw("(a.njop_bng>b.njop_bng or  a.njop_bumi>b.njop_bumi ) ");
            } else if ($status == '2') {
                // turun
                $query->whereraw("((a.njop_bng<b.njop_bng and a.luas_bng>0) or  a.njop_bumi<b.njop_bumi )");
            } else {
                $query->whereraw("(((a.njop_bng<b.njop_bng and a.luas_bng>0) or  a.njop_bumi<b.njop_bumi  ) or (a.njop_bng>b.njop_bng or  a.njop_bumi>b.njop_bumi))");
            }
        }

        $data = $query->get();
        $param = [
            'data' => $data,
            'now' => $now,
            'last' => $last
        ];
        return Excel::download(new NjopTurunExcel($param), 'NJOP turun di tahun ' . $now . ' .xlsx');
    }

    public function rekapDhkpAll(Request $request)
    {

        if ($request->ajax()) {
            /*            $tahun = $request->tahun;
            $data = Dhkp::RekapAll($tahun);
            if ($request->kd_kecamatan <> '') {
                $kd_kecamatan = $request->kd_kecamatan;
                $data = $data->where("ref_kelurahan.kd_kecamatan", $kd_kecamatan);
            }

            $data = $data->get();
            return view('analisa.dhkp.rekap_all_table', compact('data'));
            */

            $tahun = $request->tahun;
            $kd_kecamatan = $request->kd_kecamatan ?? null;
            $buku = [1, 2, 3, 4, 5];

            if ($tahun == date('Y')) {
                $cut = date('Ymd');
            } else {
                $cut = $tahun . '1231';
            }

            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];

            if ($kd_kecamatan != '') {
                $param['kd_kecamatan'] = $kd_kecamatan;
            }


            $cn = implode("", $buku) . $kd_kecamatan . $cut . "dhkp-rekap-all";
            $seconds = 5000;
            $data = Cache::remember($cn, $seconds, function () use ($param) {
                return  Baku::RekapDesa($param)->get();
            });

            return view('dhkp.rekap_desa_table_dua', compact('data'));
            // $rekap = Baku::RekapDesa($param)->get();

            // dd($rekap);
        }

        $i = date('Y') + 1;
        $tahun = [];
        for ($thn = $i; $thn >= 2014; $thn--) {
            $tahun[] = $thn;
        }

        $kecamatan = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
            ->select(db::raw("kd_kecamatan,nm_kecamatan"))->orderby('kd_kecamatan')
            ->pluck('nm_kecamatan', 'kd_kecamatan')->toArray();

        $ts = date('Y');
        // $filter = $request->getQueryString();
        return view('analisa.dhkp.rekap_all', compact('tahun', 'kecamatan', 'ts'));
    }

    public function rekapDhkpAllExcell(Request $request)
    {
        return Excel::download(new RekapDhkpExcell($request), 'rekap dhkp ' . $request->tahun . '.xlsx');
    }

    public function rekapDhkPerbandingan(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $tahun_last = $request->tahun_last;
            $data = Dhkp::RekapPerbandingan($tahun, $tahun_last);

            if ($request->kd_kecamatan <> '') {
                $kd_kecamatan = $request->kd_kecamatan;
                $data = $data->where("ref_kelurahan.kd_kecamatan", $kd_kecamatan);
            }

            $data = $data->get();
            return view('analisa.dhkp.rekap_perbandingan_table', compact('data', 'tahun', 'tahun_last'));
        }

        $i = date('Y') + 1;
        $tahun = [];
        for ($thn = $i; $thn >= 2014; $thn--) {
            $tahun[] = $thn;
        }

        $kecamatan = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
            ->select(db::raw("kd_kecamatan,nm_kecamatan"))->orderby('kd_kecamatan')
            ->pluck('nm_kecamatan', 'kd_kecamatan')->toArray();

        $ts = date('Y');
        return view('analisa.dhkp.rekap_perbandingan', compact('tahun', 'kecamatan', 'ts'));
    }

    public function rekapDhkPerbandinganExcell(Request $request)
    {
        return Excel::download(new PerbandinganDhkpExcell($request), 'Perbandingan DHKP ' . $request->tahun . '.xlsx');
    }


    public function sqlAnalisaKenaikan($kd_kecamatan, $kd_kelurahan, $tahun, $hkpd, $kd_znt, $naik_kelas = '')
    {
        $tahun_semula = date('Y');

        $wz = $kd_znt <> '' ? " and dat_op_bumi.kd_znt='$kd_znt' " : " ";

        $nk = $naik_kelas <> '' ? $naik_kelas : ' (kelas_tanah - kelas_tanah_baru) ';
        $ktb = $naik_kelas <> '' ? " lpad((kelas_tanah - '" . $naik_kelas . "'),3,'0')" : " MIN (
            getKelasTanah (
               $tahun_semula,
               hit_naik_kelas (tarif_pbb_lama / 100,
                  pbb_lama,
                  (" . $hkpd . " / 100),
                  luas_bumi)))
         OVER (PARTITION BY kd_znt, nir) ";
        return DB::connection("oracle_satutujuh")->select(db::raw("select a.*,    
        (select case when luas_bumi_sppt>0 then  njop_bumi_sppt /luas_bumi_sppt else 0 end njop  
from sppt
where thn_pajak_sppt='$tahun_semula'
and kd_kecamatan=a.kd_kecamatan and kd_kelurahan=a.kd_kelurahan
and kd_blok=a.kd_blok and no_urut=a.no_urut and kd_jns_op=a.kd_jns_op) njop_sppt,  
        nvl(nilai_potongan,0) potongan_individu,    
        (  (SELECT persentase
                  FROM ms_potongan_kebijakan
                 WHERE     tahun = '$tahun_semula'
                       AND kd_buku = get_buku (a.pbb_all_lama))
             / 100)
     persentase_potongan
  from ( SELECT tmp_c.*,
  (luas_bumi * njop_m2_tanah_baru) njop_tanah_baru,
  (luas_bumi * njop_m2_tanah_baru) * (hkpd_baru / 100)
     nilai_njkp_baru,
    ( (luas_bumi * njop_m2_tanah_baru) 
   + (CASE
         WHEN njop_bng < njoptkp THEN 0
         ELSE (njop_bng - njoptkp)
      END)) * (hkpd_baru / 100)
     nilai_njkp_all_baru,
  TO_CHAR (
     tarifpbb (
        (luas_bumi * njop_m2_tanah_baru) * (hkpd_baru / 100),
        '$tahun'))
     tarif_pbb_baru,
  TO_CHAR (
     tarifpbb (
        ( (luas_bumi * njop_m2_tanah_baru) 
   + (CASE
         WHEN njop_bng < njoptkp THEN 0
         ELSE (njop_bng - njoptkp)
      END)) * (hkpd_baru / 100),
        '$tahun'))
     tarif_pbb_all_baru,
  ROUND (
       (  tarifpbb (
               (luas_bumi * njop_m2_tanah_baru)
             * (hkpd_baru / 100),
             '$tahun')
        / 100)
     * (luas_bumi * njop_m2_tanah_baru)
     * (hkpd_baru / 100))
     pbb_baru,
  ROUND (
       tarifpbb (
        ( (luas_bumi * njop_m2_tanah_baru) 
   + (CASE
         WHEN njop_bng < njoptkp THEN 0
         ELSE (njop_bng - njoptkp)
      END)) * (hkpd_baru / 100),
        '$tahun')
     * ( 
     ( (luas_bumi * njop_m2_tanah_baru) 
   + (CASE
         WHEN njop_bng < njoptkp THEN 0
         ELSE (njop_bng - njoptkp)
      END)) * (hkpd_baru / 100)
     )
     / 100)
     pbb_all_baru
FROM (SELECT tmp_b.*,
        (kelas_tanah - kelas_tanah_baru) kenaikan_kelas,
        njop_m2 (kelas_tanah_baru, '$tahun') njop_m2_tanah_baru
   FROM (SELECT nop,
   kd_propinsi,
                       kd_dati2,
                       kd_kecamatan,
                       kd_kelurahan,
                       kd_blok,
                       no_urut,
                       kd_jns_op,
                kd_znt,
                kategori_lokasi,
                    alamat_objek,
                nir,
                kelas_tanah,
                luas_bumi,
                njop_m2_tanah,
                njop_tanah,
                luas_bng,
                njop_m2_bng,
                njop_bng,
                njoptkp,
                hkpd_lama,
                nilai_njkp_lama,
                nilai_njkp_all_lama,
                TO_CHAR (tarif_pbb_lama) tarif_pbb_lama,
                TO_CHAR (tarif_pbb_all_lama) tarif_pbb_all_lama,
                pbb_lama,
                pbb_all_lama,
                (" . $hkpd . ") hkpd_baru,
                " . $ktb . "
                   AS kelas_tanah_baru
           FROM (
           SELECT    dat_objek_pajak.kd_propinsi
                        || '.'
                        || dat_objek_pajak.kd_dati2
                        || '.'
                        || dat_objek_pajak.kd_kecamatan
                        || '.'
                        || dat_objek_pajak.kd_kelurahan
                        || '.'
                        || dat_objek_pajak.kd_blok
                        || '-'
                        || dat_objek_pajak.no_urut
                        || '.'
                        || dat_objek_pajak.kd_jns_op
                           nop,
                           dat_objek_pajak.kd_propinsi,
                           dat_objek_pajak.kd_dati2,
                           dat_objek_pajak.kd_kecamatan,
                           dat_objek_pajak.kd_kelurahan,
                           dat_objek_pajak.kd_blok,
                           dat_objek_pajak.no_urut,
                           dat_objek_pajak.kd_jns_op,
                        dat_op_bumi.kd_znt,
                        jalan_op||' '||blok_kav_no_op alamat_objek,
                        nama_lokasi kategori_lokasi,
                        nir * 1000 nir,
                        kd_kls_tanah kelas_tanah,
                        total_luas_bumi luas_bumi,
                        nilai_per_m2_tanah * 1000 njop_m2_tanah,
                        (total_luas_bumi * nilai_per_m2_tanah * 1000)
                           njop_tanah,
                        total_luas_bng luas_bng,
                        CASE
                           WHEN total_luas_bng > 0
                           THEN
                              njop_bng / total_luas_bng
                           ELSE
                              0
                        END
                           njop_m2_bng,
                        njop_bng,
                        (NVL (nilai_njoptkp, 0) * 1000) njoptkp,
                        nilai_hkpd / 100 hkpd_lama,
                          (nilai_hkpd / 100)
                        * (total_luas_bumi * nilai_per_m2_tanah * 1000)
                           nilai_njkp_lama,
                          (((nilai_hkpd / 100)
                        * (total_luas_bumi * nilai_per_m2_tanah * 1000) ) +                                
                        (case when  njop_bng < (NVL (nilai_njoptkp, 0) * 1000) then 0 else ( njop_bng - (NVL (nilai_njoptkp, 0) * 1000)) end))   
                           nilai_njkp_all_lama,
                        (tarifpbb (
                              (nilai_hkpd / 100)
                            * (  total_luas_bumi
                               * nilai_per_m2_tanah
                               * 1000),
                            '$tahun_semula'))
                           tarif_pbb_lama,
                        tarifpbb (
                             (((nilai_hkpd / 100)
                        * (total_luas_bumi * nilai_per_m2_tanah * 1000) ) +                                
                        (case when  njop_bng < (NVL (nilai_njoptkp, 0) * 1000) then 0 else ( njop_bng - (NVL (nilai_njoptkp, 0) * 1000)) end)),
                           '$tahun_semula')
                           tarif_pbb_all_lama,
                        ROUND (
                             (nilai_hkpd / 100)
                           * (  total_luas_bumi
                              * nilai_per_m2_tanah
                              * 1000)
                           * (  tarifpbb (
                                     (nilai_hkpd / 100)
                                   * (  total_luas_bumi
                                      * nilai_per_m2_tanah
                                      * 1000),
                                   '$tahun_semula')
                              / 100))
                           pbb_lama,
                        round(tarifpbb (
                             (((nilai_hkpd / 100)
                        * (total_luas_bumi * nilai_per_m2_tanah * 1000) ) +                                
                        (case when  njop_bng < (NVL (nilai_njoptkp, 0) * 1000) then 0 else ( njop_bng - (NVL (nilai_njoptkp, 0) * 1000)) end)),
                           '$tahun_semula') *     (((nilai_hkpd / 100)
                        * (total_luas_bumi * nilai_per_m2_tanah * 1000) ) +                                
                        (case when  njop_bng < (NVL (nilai_njoptkp, 0) * 1000) then 0 else ( njop_bng - (NVL (nilai_njoptkp, 0) * 1000)) end)) /100)
                           pbb_all_lama
                   FROM dat_objek_pajak
                        JOIN dat_op_bumi
                           ON     dat_objek_pajak.kd_propinsi =
                                     dat_op_bumi.kd_propinsi
                              AND dat_objek_pajak.kd_dati2 =
                                     dat_op_bumi.kd_dati2
                              AND dat_objek_pajak.kd_kecamatan =
                                     dat_op_bumi.kd_kecamatan
                              AND dat_objek_pajak.kd_kelurahan =
                                     dat_op_bumi.kd_kelurahan
                              AND dat_objek_pajak.kd_blok =
                                     dat_op_bumi.kd_blok
                              AND dat_objek_pajak.no_urut =
                                     dat_op_bumi.no_urut
                              AND dat_objek_pajak.kd_jns_op =
                                     dat_op_bumi.kd_jns_op
                        JOIN dat_nir
                           ON     dat_objek_pajak.kd_propinsi =
                                     dat_nir.kd_propinsi
                              AND dat_objek_pajak.kd_dati2 =
                                     dat_nir.kd_dati2
                              AND dat_objek_pajak.kd_kecamatan =
                                     dat_nir.kd_kecamatan
                              AND dat_objek_pajak.kd_kelurahan =
                                     dat_nir.kd_kelurahan
                              AND dat_op_bumi.kd_znt = dat_nir.kd_znt
                              AND dat_nir.thn_nir_znt = '$tahun_semula'
                        JOIN kelas_tanah
                           ON     kelas_tanah.thn_awal_kls_tanah <=
                                     $tahun_semula
                              AND kelas_tanah.thn_akhir_kls_tanah >=
                                     $tahun_semula
                              AND kelas_tanah.nilai_min_tanah <
                                     DAT_NIR.NIR
                              AND kelas_tanah.nilai_max_tanah >=
                                     dat_nir.nir
                              AND kelas_tanah.kd_kls_tanah != 'XXX'
                        join dat_znt on dat_op_bumi.kd_propinsi=dat_znt.kd_propinsi and
                                        dat_op_bumi.kd_dati2=dat_znt.kd_dati2 and
                                        dat_op_bumi.kd_kecamatan=dat_znt.kd_kecamatan and
                                        dat_op_bumi.kd_kelurahan=dat_znt.kd_kelurahan and
                                        dat_op_bumi.kd_znt=dat_znt.kd_znt 
                        left join sim_pbb.lokasi_objek on lokasi_objek.id=dat_znt.lokasi_objek_id
                        LEFT JOIN dat_subjek_pajak_njoptkp
                           ON     DAT_SUBJEK_PAJAK_NJOPTKP.kd_propinsi =
                                     dat_objek_pajak.kd_propinsi
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.kd_dati2 =
                                     dat_objek_pajak.kd_dati2
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.kd_kecamatan =
                                     dat_objek_pajak.kd_kecamatan
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.kd_kelurahan =
                                     dat_objek_pajak.kd_kelurahan
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.kd_blok =
                                     dat_objek_pajak.kd_blok
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.no_urut =
                                     dat_objek_pajak.no_urut
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.kd_jns_op =
                                     dat_objek_pajak.kd_jns_op
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.subjek_pajak_id =
                                     dat_objek_pajak.subjek_pajak_id
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.thn_njoptkp =
                                     '$tahun_semula'
                        LEFT JOIN njoptkp
                           ON     njoptkp.thn_awal <=
                                     DAT_SUBJEK_PAJAK_NJOPTKP.thn_njoptkp
                              AND njoptkp.thn_akhir >=
                                     DAT_SUBJEK_PAJAK_NJOPTKP.thn_njoptkp
                              AND DAT_SUBJEK_PAJAK_NJOPTKP.thn_njoptkp =
                                     '$tahun_semula'
                  WHERE     dat_objek_pajak.kd_Kecamatan = '$kd_kecamatan'
                        AND dat_objek_pajak.kd_kelurahan = '$kd_kelurahan'
                        " . $wz . "
                        AND dat_op_bumi.luas_bumi > 0
                        and jns_bumi in ('1','2','3')
                        and jns_transaksi_op in ('1','2')
                        ) tmp_a) tmp_b)
                        tmp_c) a
                        left join ms_potongan_individu b on    a.kd_propinsi=b.kd_propinsi and
                        a.kd_dati2=b.kd_dati2 and
                        a.kd_kecamatan=b.kd_kecamatan and
                        a.kd_kelurahan=b.kd_kelurahan and
                        a.kd_blok=b.kd_blok and
                        a.no_urut=b.no_urut and
                        a.kd_jns_op=b.kd_jns_op and b.thn_pajak_sppt='$tahun_semula'"));
    }


    public function menaikanKelasExcel(Request $request)
    {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $tahun = $request->tahun;
        $hkpd = $request->hkpd;
        $kd_znt = $request->kd_znt ?? '';
        $naik_kelas = $request->naik_kelas ?? '';
        $data = $this->sqlAnalisaKenaikan($kd_kecamatan, $kd_kelurahan, $tahun, $hkpd, $kd_znt, $naik_kelas);
        $filename = $kd_kecamatan . $kd_kelurahan . ' simulasi kenaikan.xlsx';
        return Excel::download(new SimulasiKenaikanHkpdExcell($data), $filename);
    }

    public function menaikanKelas(Request $request)
    {

        if ($request->ajax()) {

            $tipe = $request->jenis ?? '';
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            if ($tipe == 'kawasan') {

                $ck = DB::connection("oracle_satutujuh")->table("dat_znt")
                    ->whereraw("dat_znt.kd_kecamatan='$kd_kecamatan' and dat_znt.kd_kelurahan='$kd_kelurahan'")
                    ->select(db::raw("count(distinct case when REGEXP_REPLACE (dat_znt.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end) sm  "))
                    ->first();

                $and = "";
                if ($ck->sm == 2) {
                    // sismiop
                    $and = " and REGEXP_REPLACE (DAT_ZNT.kd_znt, '[^0-9]', '') is null";
                }
                $znt = DB::connection("oracle_satutujuh")->table("dat_znt")
                    ->leftjoin(db::raw("sim_pbb.lokasi_objek lo "), 'lo.id', '=', 'dat_znt.lokasi_objek_id')
                    ->whereraw("dat_znt.kd_kecamatan='$kd_kecamatan' and dat_znt.kd_kelurahan='$kd_kelurahan' " . $and)
                    ->select(db::raw("dat_znt.kd_znt ,nama_lokasi"))
                    ->orderbyraw("cast(REGEXP_REPLACE (DAT_ZNT.kd_znt, '[^0-9]', '') as number) asc nulls first,dat_znt.kd_znt asc")
                    ->pluck('nama_lokasi', 'kd_znt')->toArray();
                return $znt;

                // pluck('nm_kelurahan', 'kd_kelurahan')->toArray();
            } else {

                $tahun = $request->tahun;
                $hkpd = $request->hkpd;
                $kd_znt = $request->kd_znt ?? '';
                $naik_kelas = $request->naik_kelas ?? '';
                $data = $this->sqlAnalisaKenaikan($kd_kecamatan, $kd_kelurahan, $tahun, $hkpd, $kd_znt, $naik_kelas);

                return view('analisa.menaikan-kelas_data', compact('data'));
            }
        }

        $kecamatan = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
            ->select(db::raw("kd_kecamatan,nm_kecamatan"))->orderby('kd_kecamatan')
            ->pluck('nm_kecamatan', 'kd_kecamatan')->toArray();
        $ts = date('Y');
        $tahun = [];
        for ($i = $ts + 1; $i >= $ts; $i--) {
            $tahun[] = $i;
        }

        return view('analisa.menaikan-kelas', compact('ts', 'kecamatan', 'tahun'));
    }
}
