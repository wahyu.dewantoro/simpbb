<?php

namespace App\Http\Controllers;

use App\Helpers\gallade;
use App\Models\Penagihan;
use App\Models\PenagihanDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class PenagihanDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result=[];
        return view('penagihan.daftar', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function PenagihanDetail (Request $request, $id){
        $result=[];
        return view('penagihan.detail', compact('result','id'));   
    }
    public function PenagihanDetailSearch(Request $request, $id){
        $select=[
            'penagihan_detail.penagihan_id',
            'penagihan_detail.kd_propinsi',
            'penagihan_detail.kd_dati2',
            'penagihan_detail.kd_kecamatan',
            'penagihan_detail.kd_kelurahan',
            'penagihan_detail.kd_blok',
            'penagihan_detail.no_urut',
            'penagihan_detail.kd_jns_op',
            'penagihan_detail.tahun',
            'penagihan_detail.alamat_objek',
            'penagihan_detail.wp',
            'penagihan_detail.alamat_wp',
            'penagihan_detail.pbb',
            'penagihan_detail.buku',
            'penagihan_detail.tagihan',
            'ref_kecamatan.nm_kecamatan',
            'ref_kelurahan.nm_kelurahan',
            'pembayaran_sppt.tgl_pembayaran_sppt'
        ];
        $join="penagihan_detail.kd_propinsi
            and ref_kecamatan.kd_propinsi=penagihan_detail.kd_propinsi
            and ref_kecamatan.kd_dati2=penagihan_detail.kd_dati2
            and ref_kecamatan.kd_kecamatan=penagihan_detail.kd_kecamatan
        ";
        $joinR="penagihan_detail.kd_propinsi
            and ref_kelurahan.kd_propinsi=penagihan_detail.kd_propinsi
            and ref_kelurahan.kd_dati2=penagihan_detail.kd_dati2
            and ref_kelurahan.kd_kecamatan=penagihan_detail.kd_kecamatan
            and ref_kelurahan.kd_kelurahan=penagihan_detail.kd_kelurahan
        ";
        $joinPS="penagihan_detail.kd_propinsi
            and pembayaran_sppt.kd_propinsi=penagihan_detail.kd_propinsi
            and pembayaran_sppt.kd_dati2=penagihan_detail.kd_dati2
            and pembayaran_sppt.kd_kecamatan=penagihan_detail.kd_kecamatan
            and pembayaran_sppt.kd_kelurahan=penagihan_detail.kd_kelurahan
            and pembayaran_sppt.kd_blok=penagihan_detail.kd_blok
            and pembayaran_sppt.no_urut=penagihan_detail.no_urut
            and pembayaran_sppt.kd_jns_op=penagihan_detail.kd_jns_op
            and pembayaran_sppt.thn_pajak_sppt=penagihan_detail.tahun
        ";
        $dataSet=PenagihanDetail::select($select)->where(['penagihan_id'=>$id])
                ->join(db::raw('pbb.ref_kecamatan ref_kecamatan'),'ref_kecamatan.kd_propinsi','=',db::raw($join))
                ->join(db::raw('pbb.ref_kelurahan ref_kelurahan'),'ref_kelurahan.kd_propinsi','=',db::raw($joinR))
                ->leftJoin(db::raw('pbb.pembayaran_sppt pembayaran_sppt'),'pembayaran_sppt.kd_propinsi','=',db::raw($joinPS))
                ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
                        return $data->tgl_pembayaran_sppt?'Sudah Lunas ('.$data->tgl_pembayaran_sppt.')':'Belum Lunas';
                        //return gallade::anchorInfo(url('penagihan_daftar/detail',[$data->penagihan_id]),"<i class='fas fa-search'></i>",'title="Detail Data"');
                    })
                    ->addColumn('nop', function ($data) {
                        return $data->kd_propinsi.".".
                        $data->kd_dati2.".".
                        $data->kd_kecamatan.".".
                        $data->kd_kelurahan.".".
                        $data->kd_blok."-".
                        $data->no_urut.".".
                        $data->kd_jns_op;
                    })
                    ->editColumn('pbb', function($data){
                        return gallade::parseQuantity($data->pbb);
                    })  
                    ->editColumn('tagihan', function($data){
                        return gallade::parseQuantity($data->tagihan);
                    })  
                    ->addIndexColumn()   
                    ->removeColumn(['penagihan_id',
                    'kd_propinsi',
                    'kd_dati2',
                    'kd_kecamatan',
                    'kd_kelurahan',
                    'kd_blok',
                    'no_urut',
                    'kd_jns_op','tgl_pembayaran_sppt'])
                ->make(true);
    }
    public function PenagihanSearch(Request $request){
        $select=['penagihan.penagihan_id',
                'penagihan.nomor_penagihan',
                'penagihan.created_at',
                'penagihan.kd_status',
                
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2021' THEN 1 ELSE 0 END) tagihan2021"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2021' AND pembayaran_sppt.tgl_pembayaran_sppt IS not NULL THEN 1 ELSE 0 END) lunas2021"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2020' THEN 1 ELSE 0 END) tagihan2020"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2020' AND pembayaran_sppt.tgl_pembayaran_sppt IS not NULL THEN 1 ELSE 0 END) lunas2020"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2019' THEN 1 ELSE 0 END) tagihan2019"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2019' AND pembayaran_sppt.tgl_pembayaran_sppt IS not NULL THEN 1 ELSE 0 END) lunas2019"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2018' THEN 1 ELSE 0 END) tagihan2018"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2018' AND pembayaran_sppt.tgl_pembayaran_sppt IS not NULL THEN 1 ELSE 0 END) lunas2018"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2017' THEN 1 ELSE 0 END) tagihan2017"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2017' AND pembayaran_sppt.tgl_pembayaran_sppt IS not NULL THEN 1 ELSE 0 END) lunas2017"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2016' THEN 1 ELSE 0 END) tagihan2016"),
                DB::raw("sum(CASE WHEN PENAGIHAN_DETAIL.TAHUN='2016' AND pembayaran_sppt.tgl_pembayaran_sppt IS not NULL THEN 1 ELSE 0 END) lunas2016"),

                DB::raw("count(penagihan_detail.penagihan_detail_id) total_tagihan"),
                DB::raw("sum(CASE WHEN pembayaran_sppt.tgl_pembayaran_sppt IS NOT NULL THEN 1 ELSE 0 END) total_lunas"),
            ];
        $groupBy=[
            'penagihan.penagihan_id',
            'penagihan.nomor_penagihan',
            'penagihan.created_at',
            'penagihan.kd_status'
        ];
        $joinPS="penagihan_detail.kd_propinsi
            and pembayaran_sppt.kd_propinsi=penagihan_detail.kd_propinsi
            and pembayaran_sppt.kd_dati2=penagihan_detail.kd_dati2
            and pembayaran_sppt.kd_kecamatan=penagihan_detail.kd_kecamatan
            and pembayaran_sppt.kd_kelurahan=penagihan_detail.kd_kelurahan
            and pembayaran_sppt.kd_blok=penagihan_detail.kd_blok
            and pembayaran_sppt.no_urut=penagihan_detail.no_urut
            and pembayaran_sppt.kd_jns_op=penagihan_detail.kd_jns_op
            and pembayaran_sppt.thn_pajak_sppt=penagihan_detail.tahun
        ";
        $dataSet=Penagihan::select($select)
                ->leftJoin('penagihan_detail', 'penagihan_detail.penagihan_id', '=', 'penagihan.penagihan_id')
                ->orderBy('penagihan.created_at','DESC')
                ->where(['penagihan.penagihan_id'=>'085b3316-943a-11ec-adb1-3c7c3f59d7ed'])
                ->leftJoin(db::raw('pbb.pembayaran_sppt pembayaran_sppt'),'pembayaran_sppt.kd_propinsi','=',db::raw($joinPS))
                ->groupBy($groupBy)
                ->get();
        $datatables = Datatables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
                        return gallade::anchorInfo(url('penagihan_daftar/detail',[$data->penagihan_id]),"<i class='fas fa-search'></i>",'title="Detail Data"');
                    })
                    ->editColumn('lunas2016', function($data){
                        return gallade::parseQuantity($data->lunas2016);
                    })   
                    ->editColumn('tagihan2016', function($data){
                        return gallade::parseQuantity($data->tagihan2016);
                    })   
                    ->editColumn('lunas2017', function($data){
                        return gallade::parseQuantity($data->lunas2017);
                    })   
                    ->editColumn('tagihan2017', function($data){
                        return gallade::parseQuantity($data->tagihan2017);
                    })   
                    ->editColumn('lunas2018', function($data){
                        return gallade::parseQuantity($data->lunas2018);
                    })   
                    ->editColumn('tagihan2018', function($data){
                        return gallade::parseQuantity($data->tagihan2018);
                    })   
                    ->editColumn('lunas2019', function($data){
                        return gallade::parseQuantity($data->lunas2019);
                    })   
                    ->editColumn('tagihan2019', function($data){
                        return gallade::parseQuantity($data->tagihan2019);
                    })   
                    ->editColumn('lunas2020', function($data){
                        return gallade::parseQuantity($data->lunas2020);
                    })   
                    ->editColumn('tagihan2020', function($data){
                        return gallade::parseQuantity($data->tagihan2020);
                    })   
                    ->editColumn('lunas2021', function($data){
                        return gallade::parseQuantity($data->lunas2021);
                    })   
                    ->editColumn('tagihan2021', function($data){
                        return gallade::parseQuantity($data->tagihan2021);
                    })   
                    ->editColumn('total_tagihan', function($data){
                        return gallade::parseQuantity($data->total_tagihan);
                    })   
                    ->editColumn('jumlah_tagihan', function($data){
                        return gallade::parseQuantity($data->jumlah_tagihan);
                    })   
                    ->addIndexColumn()   
                    ->removeColumn(['penagihan_id'])
                ->make(true);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
