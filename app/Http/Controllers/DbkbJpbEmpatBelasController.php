<?php

namespace App\Http\Controllers;


use App\Models\DbkbJpbEmpatBelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;

class DbkbJpbEmpatBelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DbkbJpbEmpatBelas::orderby('thn_dbkb_jpb14', 'desc')->get();
        return view('dbkb.jepebeempatbelas.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'action' => route('dbkb.jepebe-empat-belas.store'),
            'method' => 'post',
        ];
        return view('dbkb.jepebeempatbelas.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data=$request->only(['thn_dbkb_jpb14','nilai'])
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            $data = [
                'kd_propinsi' => '35',
                'kd_dati2' => '07',
                'thn_dbkb_jpb14' => $request->thn_dbkb_jpb14,
                'nilai_dbkb_jpb14' => $request->nilai_dbkb_jpb14 / 1000,
            ];
            DbkbJpbEmpatBelas::create($data);
            DB::connection('oracle_satutujuh')->commit();
            $flash = ['success' => 'Berhasil di proses'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
        }

        return redirect(route('dbkb.jepebe-empat-belas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'action' => route('dbkb.jepebe-empat-belas.update', $id),
            'method' => 'patch',
            'jpb' => DbkbJpbEmpatBelas::where('thn_dbkb_jpb14', $id)->first()
        ];
        return view('dbkb.jepebeempatbelas.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $data=$request->only(['thn_dbkb_jpb14','nilai'])
        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            $data = [
                'kd_propinsi' => '35',
                'kd_dati2' => '07',
                'thn_dbkb_jpb14' => $request->thn_dbkb_jpb14,
                'nilai_dbkb_jpb14' => $request->nilai_dbkb_jpb14 / 1000,
            ];
            DbkbJpbEmpatBelas::where('thn_dbkb_jpb14', $id)->update($data);
            DB::connection('oracle_satutujuh')->commit();
            $flash = ['success' => 'Berhasil di proses'];
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
        }

        return redirect(route('dbkb.jepebe-empat-belas.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
