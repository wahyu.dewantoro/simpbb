<?php

namespace App\Http\Controllers;

use App\Exports\ExcelRekapBayarHarian;
use App\Exports\PembayaranHarian;
use App\Exports\RealisasiBaku;
use App\Exports\RealisasiBulanan;
use App\Exports\RealisasiDetailKecamatan;
use App\Exports\RealisasiHarian;
use App\Exports\RealisasiPembayaranKobil;
use App\Exports\RealisasiRangkingKecamatan;
use App\Exports\RealisasiRangkingKelurahan;
use App\Exports\RealisasiTargetApbd;
use App\Exports\realisasiTunggakanBukuExport;
use App\Exports\realisasiTunggakanExport;
use App\Helpers\Baku;
use App\Helpers\Realisasi;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Billing_kolektif;
// use App\User;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Maatwebsite\Excel\Facades\Excel;
use DOMPDF as PDF;
use Illuminate\Support\Facades\Cache;
// use Dompdf\Dompdf as DompdfDompdf;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\View;
// use PhpOffice\PhpWord\Writer\PDF\DomPDF;
// use Svg\Tag\Rect;
// use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class RealisasiController extends Controller
{
    //


    public function opsiBuku()
    {
        return ['0' => 'Semua Buku', '1' => 'Buku 1 dan 2', '2' => 'Buku 3, 4 dan 5'];
    }

    public function targetApbd(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        $realisasi = Realisasi::targetApbd($tanggal);
        return view('realisasi/target_apbd', compact('tanggal', 'realisasi', 'tahun'));
    }

    public function ExceltargetApbd(Request $request)
    {
        $param['tanggal'] = $request->tanggal ?? date('d M Y');
        return Excel::download(new RealisasiTargetApbd($param), 'Realisasi target APBD cut off ' . $param['tanggal'] . '.xlsx');
    }

    public function PdftargetApbd(Request $request)
    {
        // $tahun = $request->tahun ?? date('Y');
        $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        $realisasi = Realisasi::targetApbd($tanggal);
        $pdf = PDF::loadView('realisasi/target_apbd_pdf', compact('tahun', 'tanggal', 'realisasi'))->setPaper('a4', 'landscape');
        $filename = 'Realisasi target APBD cut off ' . $tanggal . '.pdf';
        return $pdf->download($filename);
    }

    public function realHarian(Request $request)
    {
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $kd_kecamatan = "";
        if (in_array('Kecamatan', $role)) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();

            foreach ($kec as $kec) {
                $kd_kecamatan = $kec;
            }
        }
        $tanggal = $request->tanggal ?? date('d M Y');
        $kd_kecamatan = $request->kd_kecamatan ?? $kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan ?? '';
        $tg = Carbon::create($tanggal)->format('Ymd');
        $data = Realisasi::Harian($tg, $kd_kecamatan, $kd_kelurahan);
        $kecamatan = Kecamatan::login()->get();
        $tanggal = Carbon::create($tanggal)->format('d M Y');
        $param = "tanggal=" . $tanggal . "&kd_kecamatan=" . $kd_kecamatan . "&kd_kelurahan=" . $kd_kelurahan;
        return view('realisasi/harian', compact('kecamatan', 'tanggal', 'data', 'param'));
    }

    public function excelRealHarian(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $param['kd_kecamatan'] = $request->kd_kecamatan ?? '';
        $param['kd_kelurahan'] = $request->kd_kelurahan ?? '';
        $param['tanggal'] = Carbon::create($tanggal)->format('Ymd');
        $param['kecamatan'] = Kecamatan::where('kd_kecamatan', $param['kd_kecamatan'])->first()->nm_kecamatan ?? '';
        $param['kelurahan'] = Kelurahan::where('kd_kecamatan', $param['kd_kecamatan'])->where('kd_kelurahan', $param['kd_kelurahan'])->first()->nm_kelurahan ?? '';
        return Excel::download(new RealisasiHarian($param), 'Realisasi Harian.xlsx');
    }

    public function pdfRealHarian(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $kd_kecamatan = $request->kd_kecamatan ?? '';
        $kd_kelurahan = $request->kd_kelurahan ?? '';
        $tg = Carbon::create($tanggal)->format('Ymd');
        $data = Realisasi::Harian($tg, $kd_kecamatan, $kd_kelurahan);

        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan ?? '';
        $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan ?? '';

        $pdf = PDF::loadView('realisasi/harian_pdf', compact('data', 'tanggal', 'kecamatan', 'kelurahan'))->setPaper('a4', 'landscape');
        $filename = 'Realisasi Harian ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }


    public function realBulanan(Request $request)
    {
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $kd_kecamatan = "";
        if (in_array('Kecamatan', $role)) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();

            foreach ($kec as $kec) {
                $kd_kecamatan = $kec;
            }
        }
        $kecamatan = Kecamatan::login()->get();
        $bulan = $request->bulan ?? date('m-Y');
        $kd_kecamatan = $request->kd_kecamatan ?? $kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan ?? '';
        $data = Realisasi::Bulanan($bulan, $kd_kecamatan, $kd_kelurahan);
        $param = "bulan=" . $bulan . "&kd_kecamatan=" . $kd_kecamatan . "&kd_kelurahan=" . $kd_kelurahan;
        return view('realisasi/bulanan', compact('kecamatan', 'bulan', 'data', 'param'));
    }

    public function excelRealBulanan(Request $request)
    {
        $param['kd_kecamatan'] = $request->kd_kecamatan ?? '';
        $param['kd_kelurahan'] = $request->kd_kelurahan ?? '';
        $param['bulan'] = $request->bulan ?? date('m-Y');

        $param['kecamatan'] = Kecamatan::where('kd_kecamatan', $param['kd_kecamatan'])->first()->nm_kecamatan ?? '';
        $param['kelurahan'] = Kelurahan::where('kd_kecamatan', $param['kd_kecamatan'])->where('kd_kelurahan', $param['kd_kelurahan'])->first()->nm_kelurahan ?? '';
        return Excel::download(new RealisasiBulanan($param), 'Realisasi Bulanan.xlsx');
    }

    public function pdfRealBulanan(Request $request)
    {
        $bulan = $request->bulan ?? date('m-Y');
        $kd_kecamatan = $request->kd_kecamatan ?? '';
        $kd_kelurahan = $request->kd_kelurahan ?? '';
        $data = Realisasi::Bulanan($bulan, $kd_kecamatan, $kd_kelurahan);
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan ?? '';
        $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan ?? '';

        $pdf = PDF::loadView('realisasi/bulanan_pdf', compact('kecamatan', 'kelurahan', 'bulan', 'data'))->setPaper('a4', 'landscape');
        $filename = 'realisasi bulanan ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }


    public function realDetailKecamatan(Request $request)
    {
        $kecamatan = Kecamatan::login()->get();
        $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->buku ?? ['1', '2'];
        $basicQuery = Realisasi::DetailKecamatan($tanggal, $buku);
        $data = new Paginator($basicQuery, 10);
        $param = $request->getQueryString();
        return view('realisasi/detail_kecamatan', compact('kecamatan', 'param', 'tanggal', 'data'));
    }

    public function ExcelRealDetailKecamatan(Request $request)
    {
        // Log
        $param['tanggal'] = $request->tanggal ?? date('d M Y');
        $param['buku'] = $request->buku ?? ['1', '2'];
        return Excel::download(new RealisasiDetailKecamatan($param), 'Realisasi detail kecamatan.xlsx');
    }

    public function pdfRealDetailKecamatan(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->buku ?? ['1', '2'];
        $data = Realisasi::DetailKecamatan($tanggal, $buku);
        $pdf = PDF::loadView('realisasi/detail_kecamatan_pdf', compact('tanggal', 'data'))->setPaper('a4', 'landscape');
        $filename = 'detail kecamatan ' . time() . auth()->user()->id . '.pdf';
        return $pdf->download($filename);
    }

    public function apiRealRangkingKecamatan(Request $request)
    {
        // $tanggal = $request->tanggal;

        $tanggal = $request->tanggal ?? date('d M Y');
        $tgl = new Carbon($tanggal);
        $cut = $tgl->format('Ymd');
        if ($tanggal != '') {

            $buku = [1, 2];
            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];


            $cn = implode("", $buku) . $cut . "rankkecamatan";
            $seconds = 5000;
            $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
                return Baku::RangkingKecamatan($param)->get();
            });

            // dd($realisasi);


            $res = [];
            foreach ($realisasi as $row) {
                $res[] = (object)[
                    "kecamatan" => $row->nm_kecamatan,
                    "jumlah_objek" => (int)$row->jumlah_op,
                    "baku" => (int)($row->baku),
                    "stimulus" => $row->potongan,
                    "koreksi" => $row->koreksi,
                    "pokok" => (int)$row->bayar,
                    "denda" => null,
                    "realisasi" => (int)$row->bayar,
                    "persentase_realisasi" => (float)$row->persen,
                ];
            }
            return  [
                'isFalse' => false,
                'data' => $res
            ];
        } else {
            return  [
                'isFalse' => true,
                'data' => null
            ];
        }
    }

    public function realRangkingKecamatan(Request $request)
    {

        if ($request->ajax()) {

            $tanggal = $request->tanggal ?? date('d M Y');
            $tgl = new Carbon($tanggal);
            $cut = $tgl->format('Ymd');
            if ($request->kd_buku != '') {
                if ($request->kd_buku == '1') {
                    $buku = [1, 2];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }
            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];


            $cn = implode("", $buku) . $cut . "rankkecamatan";
            $seconds = 5000;
            $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
                return Baku::RangkingKecamatan($param)->get();
            });


            $excel = '0';
            return view('realisasi.rangking_kecamatan_table_dua', compact('realisasi', 'excel'));
        }
        $lb = $this->opsiBuku();

        return view('realisasi/rangking_kecamatan', compact('lb'));
    }

    public function realRangkingKekelurahan(Request $request)
    {
        if ($request->ajax()) {

            // dd($request->all());

            $tanggal = $request->tanggal ?? date('d M Y');
            $tgl = new Carbon($tanggal);
            $cut = $tgl->format('Ymd');
            if ($request->kd_buku != '') {
                if ($request->kd_buku == '1') {
                    $buku = [1, 2];
                } else if ($request->kd_buku == '0') {
                    $buku = [1, 2, 3, 4, 5];
                } else {
                    $buku = [3, 4, 5];
                }
            } else {
                $buku = [1, 2, 3, 4, 5];
            }
            $param = [
                'cut' => $cut,
                'buku' => $buku
            ];

            // dd($buku);


            $cn = implode("", $buku) . $cut . "rankdesa";

            // dd($cn);
            $seconds = 500;
            $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
                // return user()->roles()->pluck('name')->toarray();
                return Baku::RangkingDesa($param)->get();
            });


            $excel = '0';


            return view('realisasi.rangking_kelurahan_table_dua', compact('realisasi', 'excel'));
        }
        $lb = $this->opsiBuku();
        return view('realisasi/rangking_kelurahan', compact('lb'));
    }


    public function ExcelrealRangkingKecamatan(Request $request)
    {

        /*  $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->kd_buku;
        $realisasi = Realisasi::RangkingKecamatan($tanggal, $buku);
        $excel = '0';
        $ket = $this->opsiBuku()[$buku]; */
        $tanggal = $request->tanggal ?? date('d M Y');
        $tgl = new Carbon($tanggal);
        $cut = $tgl->format('Ymd');
        if ($request->kd_buku != '') {
            if ($request->kd_buku == '1') {
                $buku = [1, 2];
            } else if ($request->kd_buku == '0') {
                $buku = [1, 2, 3, 4, 5];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }
        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];


        $cn = implode("", $buku) . $cut . "rankkecamatan";
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return Baku::RangkingKecamatan($param)->get();
        });


        $filename = 'Rangking Kecamatan cut ' . tglIndo($cut)  . '.xlsx';
        return Excel::download(new RealisasiRangkingKecamatan($realisasi), $filename);
    }

    public function ExcelrealRangkingKelurahan(Request $request)
    {

        // $tanggal = $request->tanggal ?? date('d M Y');
        // $buku = $request->kd_buku;
        $tanggal = $request->tanggal ?? date('d M Y');

        $tgl = new Carbon($tanggal);
        $cut = $tgl->format('Ymd');


        if ($request->kd_buku != '') {
            if ($request->kd_buku == '1') {
                $buku = [1, 2];
            } else if ($request->kd_buku == '0') {
                $buku = [1, 2, 3, 4, 5];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }
        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];

        // $realisasi = Baku::RangkingDesa($param)->get();
        $cn = implode("", $buku) . $cut . "rankdesa";
        $seconds = 500;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            // return user()->roles()->pluck('name')->toarray();
            return Baku::RangkingDesa($param)->get();
        });


        // $ket = $this->opsiBuku()[$buku];
        $filename = 'Rangking desa cut ' . tglIndo($tanggal) . '.xlsx';
        // $realisasi = Realisasi::RangkingKelurahan($tanggal, $buku);
        return Excel::download(new RealisasiRangkingKelurahan($realisasi), $filename);
    }



    public function PdfrealRangkingKecamatan(Request $request)
    {
        /* $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        $buku = $request->buku ?? ['1', '2'];
        $realisasi = Realisasi::RangkingKecamatan($tanggal, $buku); */

        /* $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->kd_buku;
        $realisasi = Realisasi::RangkingKecamatan($tanggal, $buku);
        $excel = '0';
        $ket = $this->opsiBuku()[$buku]; */

        $tanggal = $request->tanggal ?? date('d M Y');
        $tgl = new Carbon($tanggal);
        $cut = $tgl->format('Ymd');
        if ($request->kd_buku != '') {
            if ($request->kd_buku == '1') {
                $buku = [1, 2];
            } else if ($request->kd_buku == '0') {
                $buku = [1, 2, 3, 4, 5];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }
        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];


        $cn = implode("", $buku) . $cut . "rankkecamatan";
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return Baku::RangkingKecamatan($param)->get();
        });

        $tanggal = $cut;
        $filename = 'Rangking Kecamatan cut ' . tglIndo($cut) . '.pdf';

        $pages = View::make('realisasi/rangking_kecamatan_pdf', compact('tanggal', 'realisasi'));
        $pdf = PDF::loadHTML($pages)->setPaper('A4', 'landscape');

        return $pdf->download($filename);
    }

    public function PdfrealRangkingKelurahan(Request $request)
    {
        ini_set('memory_limit', -1);


        /* $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->kd_buku;
        $ket = $this->opsiBuku()[$buku];
        $filename = 'Rangking kelurahan cut ' . tglIndo($tanggal) . ' buku ' . $ket . '.pdf';
        $realisasi = Realisasi::RangkingKelurahan($tanggal, $buku);
 */


        $tanggal = $request->tanggal ?? date('d M Y');

        $tgl = new Carbon($tanggal);
        $cut = $tgl->format('Ymd');

        $filename = 'Rangking desa cut ' . tglIndo($cut) . '.pdf';
        if ($request->kd_buku != '') {
            if ($request->kd_buku == '1') {
                $buku = [1, 2];
            } else if ($request->kd_buku == '0') {
                $buku = [1, 2, 3, 4, 5];
            } else {
                $buku = [3, 4, 5];
            }
        } else {
            $buku = [1, 2, 3, 4, 5];
        }
        $param = [
            'cut' => $cut,
            'buku' => $buku
        ];

        // $realisasi = Baku::RangkingDesa($param)->get();
        $cn = implode("", $buku) . $cut . "rankdesa";
        $seconds = 500;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            // return user()->roles()->pluck('name')->toarray();
            return Baku::RangkingDesa($param)->get();
        });

        $ket = "";

        $pages = View::make('realisasi/rangking_kelurahan_pdf', compact('tanggal', 'ket', 'realisasi'));
        $pdf = PDF::loadHTML($pages)
            ->setPaper('A4', 'landscape');
        return $pdf->download($filename);
    }

    public function realBaku(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $param = [
            'is_tunggakan' => false,
            'cut' => $tanggal
        ];

        $cn = "sql_real_baku_" . str_replace(' ', '_', $tanggal);
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::RekapRealiasasi($param)->orderby('buku')->get();
        });

        $filter = $request->getQueryString();
        return view('realisasi.baku_dua', compact('tanggal', 'realisasi', 'param', 'filter'));


        /* $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        // dd($tahun);
        $realisasi = Realisasi::Baku($tanggal);

        // dd($realisasi);
        $param = $request->getQueryString();

        return view('realisasi/baku', compact('tahun', 'tanggal', 'realisasi', 'param')); */
    }

    public function excelRealBaku(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $param = [
            'is_tunggakan' => false,
            'cut' => $tanggal
        ];

        $cn = "sql_real_baku_" . str_replace(' ', '_', $tanggal);
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::RekapRealiasasi($param)->orderby('buku')->get();
        });
        $data = [];
        $pokok = 0;
        $denda = 0;
        $jumlah = 0;
        foreach ($realisasi as $row) {
            $pokok += $row->pokok;
            $denda += $row->denda;
            $jumlah += $row->jumlah;

            $data[] = [
                'Buku ' . $row->buku,
                $row->pokok,
                $row->denda,
                $row->jumlah
            ];
        }
        $data[] = [
            'Total',
            $pokok,
            $denda,
            $jumlah
        ];

        // dd($data);
        $filename = "Realisasi baku " . str_replace(' ', '_', $tanggal) . ".xlsx";
        return Excel::download(new realisasiTunggakanExport($data), $filename);


        /*         $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        $param['tahun'] = $tahun;
        $param['tanggal'] = $tanggal;
        return Excel::download(new RealisasiBaku($param), 'Realisasi Baku cut off '  . tglIndo($tanggal) . '.xlsx'); */
    }
    public function pdfRealBaku(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        $realisasi = Realisasi::Baku($tanggal);
        $pdf = PDF::loadView('realisasi/baku_pdf', compact('tahun', 'tanggal', 'realisasi'))->setPaper('a4', 'landscape');
        $filename = 'realisasi baku cut off '  . tglIndo($tanggal) . '.pdf';
        return $pdf->download($filename);
    }

    public function pembayaranHarian(Request $request)
    {

        if ($request->ajax()) {
            $tgl_start = $request->tgl_start;
            $tgl_end = $request->tgl_end;
            $data = Realisasi::pembayaranHarian($tgl_start, $tgl_end);

            return  DataTables::of($data)
                ->editColumn('nop', function ($row) {
                    return formatnop($row->nop);
                })
                ->editColumn('tgl_pembayaran_sppt', function ($row) {
                    return tglIndo($row->tgl_pembayaran_sppt);
                })

                ->addColumn('keterangan', function ($row) {
                    $ket = "NTPD :" . $row->pengesahan;
                    $ket .= "<br>via " . $row->nama_bank;
                    if ($row->kobil_tahun) {
                        $ket .= "Kobil : " . $row->kobil_tahun;
                    }

                    return "<small>" . $ket . "</small>";
                })
                ->editColumn('pokok', function ($row) {
                    return angka($row->pokok);
                })
                ->editColumn('denda', function ($row) {
                    return angka($row->denda);
                })
                ->editColumn('total', function ($row) {
                    return angka($row->total);
                })
                ->rawColumns([
                    'keterangan',
                    'nop',
                    'tgl_pembayaran_sppt',
                    'pokok',
                    'denda',
                    'total'
                ])
                ->make(true);
        }
        return view('realisasi/pembayaran_bank_harian');
    }

    public function pdfPembayaranHarian(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $data = Realisasi::pembayaranHarian($tanggal);
        $pdf = PDF::loadView('realisasi/pembayaran_bank_harian_pdf', compact('tanggal', 'data'))->setPaper('a4', 'landscape');
        $filename = 'Pembayaran tanggal ' . $tanggal . '.pdf';
        return $pdf->download($filename);
    }

    public function excelPembayaranHarian(Request $request)
    {
        // $param['tanggal'] = $request->tanggal ?? date('d M Y');
        $param['tgl_start'] = $request->start;
        $param['tgl_end'] = $request->end;

        return Excel::download(new PembayaranHarian($param), 'pembayaran nop.xlsx');
    }

    public function pembayaranKobil(Request $request)
    {

        if ($request->ajax()) {
            $tanggal = $request->tanggal ?? date('d M Y');
            $tanggal = date('d/m/Y', strtotime($tanggal));
            $data = Billing_kolektif::whereraw("tgl_bayar >= to_date('$tanggal','dd/mm/yyyy') and tgl_bayar < to_date('$tanggal','dd/mm/yyyy')+1")
                ->select(DB::raw("kobil, billing_kolektif.kd_propinsi, billing_kolektif.kd_dati2, billing_kolektif.kd_kecamatan, billing_kolektif.kd_kelurahan, billing_kolektif.kd_blok, billing_kolektif.no_urut, billing_kolektif.kd_jns_op, billing_kolektif.tahun_pajak,pokok,denda,total,tgl_bayar,nm_wp"))
                ->join('data_billing', 'data_billing.data_billing_id', '=', 'billing_kolektif.data_billing_id')
                ->get();
            return DataTables::of($data)
                ->addColumn('nop', function ($row) {
                    return $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                })
                ->addColumn('tanggal_bayar', function ($row) {
                    return tglIndo($row->tgl_bayar);
                })
                ->rawColumns(['nop', 'tanggal_bayar'])
                ->make(true);
        }

        $tanggal = $request->tanggal ?? date('d M Y');
        return view('realisasi/pembayaran_kobil', compact('tanggal'));
    }

    public function pembayaranKobilExcel(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $param = ['tanggal' => date('d/m/Y', strtotime($tanggal))];
        return Excel::download(new RealisasiPembayaranKobil($param), 'pembayaran via kobil ' . $tanggal . '.xlsx');
    }

    public function realTunggakan(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $param = [
            'is_tunggakan' => true,
            'cut' => $tanggal
        ];

        $cn = "sql_real_tunggakan_" . str_replace(' ', '_', $tanggal);
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::RekapRealiasasi($param)->orderby('buku')->get();
        });

        $filter = $request->getQueryString();

        // return $filter;
        // $realisasi = Baku::RekapRealiasasi($param)->orderby('buku')->get();
        return view('realisasi.tunggakan_dua', compact('tanggal', 'realisasi', 'param', 'filter'));

        /*  $tahun = date('Y', strtotime($tanggal));
        $akhir = $tahun - 1;
        $awal = $akhir - 4;

        $uk = Auth()->user()->unitKerja()->first();

        $scope = ['kd_kecamatan' => $uk->kd_kecamatan, 'kd_kelurahan' => $uk->kd_kelurahan];
        $realisasi = Realisasi::RealisasiTunggakan($tanggal, $scope);
        return view('realisasi/tunggakan', compact('tanggal', 'realisasi', 'tahun', 'awal', 'akhir')); */
    }

    public function realBakuBuku(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->buku ?? 1;
        $param = [
            'is_tunggakan' => false,
            'cut' => $tanggal,
            'buku' => $buku
        ];
        // $realisasi = Baku::Realisasi($param)->toSql();

        $cn = "sql_real_baku_detail_" . $buku . "_" . str_replace(' ', '_', $tanggal);
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::Realisasi($param)->get();
        });

        $data = [];
        foreach ($realisasi as $row) {
            $data[] = [
                $row->kd_propinsi . '.' .
                    $row->kd_dati2 . '.' .
                    $row->kd_kecamatan . '.' .
                    $row->kd_kelurahan . '.' .
                    $row->kd_blok . '.' .
                    $row->no_urut . '.' .
                    $row->kd_jns_op,
                $row->thn_pajak_sppt,
                $row->tahun_terbit,
                tglIndo($row->tgl_terbit_sppt),
                $row->buku,
                $row->pokok,
                $row->denda,
                $row->jumlah,
                tglIndo($row->tanggal_bayar),
            ];
        }

        $filename = "Relaisasi baku buku" . $buku . ".xlsx";
        return Excel::download(new realisasiTunggakanBukuExport($data), $filename);
    }

    public function realTunggakanBukuExcel(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $buku = $request->buku ?? 1;
        $param = [
            'is_tunggakan' => true,
            'cut' => $tanggal,
            'buku' => $buku
        ];
        // $realisasi = Baku::Realisasi($param)->toSql();

        $cn = "sql_real_tunggakan_detail_" . $buku . "_" . str_replace(' ', '_', $tanggal);
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::Realisasi($param)->get();
        });

        $data = [];
        foreach ($realisasi as $row) {
            $data[] = [
                $row->kd_propinsi . '.' .
                    $row->kd_dati2 . '.' .
                    $row->kd_kecamatan . '.' .
                    $row->kd_kelurahan . '.' .
                    $row->kd_blok . '.' .
                    $row->no_urut . '.' .
                    $row->kd_jns_op,
                $row->thn_pajak_sppt,
                $row->tahun_terbit,
                tglIndo($row->tgl_terbit_sppt),
                $row->buku,
                $row->pokok,
                $row->denda,
                $row->jumlah,
                tglIndo($row->tanggal_bayar),
            ];
        }

        $filename = "Relaisasi tunggakan buku" . $buku . ".xlsx";
        return Excel::download(new realisasiTunggakanBukuExport($data), $filename);
    }

    public function realTunggakanPdf(Request $request)
    {
        // dd($request->all());
        $tanggal = $request->tanggal ?? date('d M Y');
        $tahun = date('Y', strtotime($tanggal));
        $akhir = $tahun - 1;
        $awal = $akhir - 4;

        $uk = Auth()->user()->unitKerja()->first();
        $scope = ['kd_kecamatan' => $uk->kd_kecamatan, 'kd_kelurahan' => $uk->kd_kelurahan];
        $realisasi = Realisasi::RealisasiTunggakan($tanggal, $scope);

        $pages = View::make('realisasi/tunggakan_pdf', compact('tanggal', 'realisasi', 'tahun', 'awal', 'akhir'));
        $pdf = SnappyPdf::loadHTML($pages)
            ->setOrientation('landscape')
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->download('Realisasi tunggakan.pdf');
    }

    public function realTunggakanExcel(Request $request)
    {
        $tanggal = $request->tanggal ?? date('d M Y');
        $param = [
            'is_tunggakan' => true,
            'cut' => $tanggal
        ];

        $cn = "sql_real_tunggakan_" . str_replace(' ', '_', $tanggal);
        $seconds = 5000;
        $realisasi = Cache::remember($cn, $seconds, function () use ($param) {
            return  Baku::RekapRealiasasi($param)->orderby('buku')->get();
        });
        $data = [];
        $pokok = 0;
        $denda = 0;
        $jumlah = 0;
        foreach ($realisasi as $row) {
            $pokok += $row->pokok;
            $denda += $row->denda;
            $jumlah += $row->jumlah;

            $data[] = [
                'Buku ' . $row->buku,
                $row->pokok,
                $row->denda,
                $row->jumlah
            ];
        }
        $data[] = [
            'Total',
            $pokok,
            $denda,
            $jumlah
        ];

        // dd($data);
        $filename = "Realisasi tunggakan " . str_replace(' ', '_', $tanggal) . ".xlsx";
        return Excel::download(new realisasiTunggakanExport($data), $filename);


        // $param['tanggal'] = $request->tanggal ?? date('d M Y');



        /*   $uk = Auth()->user()->unitKerja()->first();
        // $scope=['kd_kecamatan'=>$uk->kd_kecamatan,'kd_kelurahan'=>$uk->kd_kelurahan];
        $param['kd_kecamatan'] = $uk->kd_kecamatan;
        $param['kd_kelurahan'] = $uk->kd_kelurahan;

        // $realisasi = Realisasi::RealisasiTunggakan($tanggal,$scope);
        return Excel::download(new realisasiTunggakanExport($param), 'Realisasi Tunggakan.xlsx'); */
    }

    public function rekapHarianPembayaran(Request $request)
    {
        if ($request->ajax()) {
            $tanggala = $request->start ?? '01 ' . date('M Y');
            $tanggalb = $request->end ?? date('d M Y');
            $ta = date('Ymd', strtotime($tanggala));
            $tb = date('Ymd', strtotime($tanggalb));

            $data = Realisasi::rekapHarian($ta, $tb)->get();
            return view('realisasi/rekapHarian_', compact('data', 'tanggala', 'tanggalb'));
        }

        return view('realisasi/rekapHarian');
    }

    public function rekapHarianPembayaranExcel(Request $request)
    {
        // realisasi/rekap-harian-pembayaran-excel
        return (new ExcelRekapBayarHarian($request))->download('rekap pembayaran harian.xlsx');
    }
}
