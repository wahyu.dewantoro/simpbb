<?php

namespace App\Http\Controllers;

use App\Exports\KompensasiLebihBayar as ExportsKompensasiLebihBayar;
use App\Helpers\gallade;
use App\Helpers\InformasiObjek;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\kompensasiLebihBayar;
use App\Models\SPPT_Potongan;
use App\Models\SPPT_Potongan_det;
use App\pegawaiStruktural;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use SnappyPdf as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\View;

class KompensasiBayarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->login()->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        return view('kompensasi_bayar.list_kompensasi_baru', compact('kecamatan', 'kelurahan'));
    }
    public function search(Request $request)
    {
        $select = ['*'];
        $request = $request->only('kecamatan', 'kelurahan', 'nop');
        $dataSet = kompensasiLebihBayar::select($select)
            ->where(function ($q) use ($request) {
                if (in_array('nop', array_keys($request))) {
                    if ($request["nop"] != '') {
                        $nop = explode(".", $request["nop"]);
                        $blok_urut = explode("-", $nop[4]);
                        $q->where([
                            'kd_propinsi' => $nop[0],
                            'kd_dati2' => $nop[1],
                            'kd_kecamatan' => $nop[2],
                            'kd_kelurahan' => $nop[3],
                            'kd_blok' => $blok_urut[0],
                            'no_urut' => $blok_urut[1],
                            'kd_jns_op' => $nop[5],
                        ]);
                    }
                }
                if (in_array('kecamatan', array_keys($request))) {
                    if ($request['kecamatan'] != "-") {
                        $q->where(['kd_kecamatan' => $request['kecamatan']]);
                    }
                }
                if (in_array('kelurahan', array_keys($request))) {
                    if ($request['kelurahan'] != "-") {
                        $q->where(['kd_kelurahan' => $request['kelurahan']]);
                    }
                }
            })
            ->orderBy('id', 'desc')
            ->get();
        $datatables = datatables()::of($dataSet);
        $roles = Auth()->user()->roles->pluck('name');
        $userADmin = true;
        if ((!in_array('Super User', $roles->toArray()) && !in_array('Staff Pelayanan', $roles->toArray())) && $roles->count() == 1) {
            $userADmin = false;
        }
        return $datatables->addColumn('nop', function ($data) {
            return $data['kd_propinsi'] . "." .
                $data['kd_dati2'] . "." .
                $data['kd_kecamatan'] . "." .
                $data['kd_kelurahan'] . "." .
                $data['kd_blok'] . "-" .
                $data['no_urut'] . "." .
                $data['kd_jns_op'];
        })
            ->editColumn('pbb_asli', function ($data) {
                return gallade::parseQuantity($data['pbb_asli']);
            })
            ->editColumn('pbb_bayar', function ($data) {
                return gallade::parseQuantity($data['pbb_bayar']);
            })
            ->editColumn('selisih', function ($data) {
                return gallade::parseQuantity($data['selisih']);
            })
            ->addColumn('raw_tag', function ($data) use ($userADmin) {
                $nomorKlb = $data['kd_propinsi'] . "." .
                    $data['kd_dati2'] . "." .
                    $data['kd_kecamatan'] . "." .
                    $data['kd_kelurahan'] . "." .
                    $data['kd_blok'] . "-" .
                    $data['no_urut'] . "." .
                    $data['kd_jns_op'] . "." .
                    $data['thn_pajak_sppt'];
                $btn = "";
                if (($data['kd_status'] == '0' || $data['kd_status'] == null) && $userADmin) {
                    //
                    $btn = gallade::buttonInfo('<i class="fas fa-chevron-circle-right"></i> Verifikasi', 'title="Verifikasi" data-verifikasi="' . $nomorKlb . '"', '', 'primary');
                }
                if ($data['kd_status'] == '1') {
                    $btn = gallade::anchorInfo(
                        'sk?nomor_klb=' . $nomorKlb,
                        '<i class="fas fa-file-pdf"></i> SK-' . $data['kd_status'],
                        'title="Cetak SK-2" data-pdf="true"',
                        '',
                        'warning'
                    );
                }
                return '<div class="btn-group">' . $btn . "</div>";
            })
            ->addIndexColumn()
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kompensasi_bayar.kompensasi_baru');
    }
    public function ceknop(Request $request)
    {
        $setdata = $request->only('nop');
        $return['status'] = false;
        $return['msg'] = 'NOP tidak ditemukan.';
        try {
            if ($setdata) {
                $nop = explode(".", $setdata['nop']);
                $blok_urut = explode("-", $nop[4]);
                $tglNow = Carbon::now();
                $tahun = $tglNow->year;
                $sql = "select  dat_subjek_pajak.subjek_pajak_id nik_wp,
                            total_luas_bumi luas_bumi,
                            total_luas_bng luas_bng,
                            njop_bumi,
                            njop_bng ,
                            (select kd_kls_tanah
                        from kelas_tanah
                        where thn_awal_kls_tanah <=" . $tahun . " and thn_akhir_kls_tanah>=" . $tahun . "
                        and  nilai_min_tanah<= ( njop_bumi/NULLIF(total_luas_bumi,0) /1000) and nilai_max_tanah>=( njop_bumi/NULLIF(total_luas_bumi,0) /1000)
                        and lower(kd_kls_tanah) not like 'x%')
                        kd_kls_tanah,
                            case when total_luas_bng=0  then null else                   
                            (select kd_kls_bng
                        from kelas_bangunan
                        where thn_awal_kls_bng <=" . $tahun . " and thn_akhir_kls_bng>=" . $tahun . "
                        and  nilai_min_bng<= ( njop_bng/NULLIF(total_luas_bng,0) /1000) and nilai_max_bng>=( njop_bng/NULLIF(total_luas_bng,0) /1000)
                        and lower(kd_kls_bng) not like 'x%') end 
                            kd_kls_bng,
                            dat_subjek_pajak.nm_wp nama_wp,
                            dat_subjek_pajak.jalan_wp alamat_wp,
                            dat_subjek_pajak.rt_wp,
                            dat_subjek_pajak.rw_wp,
                            dat_subjek_pajak.kelurahan_wp,
                            '' kecamatan_wp,
                            dat_subjek_pajak.telp_wp,
                            dat_subjek_pajak.kota_wp dati2_wp,
                            '' propinsi_wp,
                            dat_objek_pajak.jalan_op nop_alamat,
                            dat_objek_pajak.rt_op nop_rt,
                            dat_objek_pajak.rw_op nop_rw,
                            ref_kelurahan.nm_kelurahan nop_kelurahan,
                            ref_kecamatan.nm_kecamatan nop_kecamatan,
                            'KABUPATEN MALANG' nop_dati2,
                            'JAWA TIMUR' nop_propinsi,
                            dat_objek_pajak.jns_transaksi_op,
                            dob.jns_bumi
                from pbb.dat_objek_pajak dat_objek_pajak
                join pbb.dat_subjek_pajak dat_subjek_pajak
                            on dat_subjek_pajak.subjek_pajak_id=dat_objek_pajak.subjek_pajak_id
                LEFT JOIN dat_op_bumi dob 
                    ON dob.kd_propinsi=dat_objek_pajak.kd_propinsi
                    AND dob.kd_dati2=dat_objek_pajak.kd_dati2
                    AND dob.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                    AND dob.kd_kelurahan=dat_objek_pajak.kd_kelurahan
                    AND dob.kd_blok=dat_objek_pajak.kd_blok
                    AND dob.no_urut=dat_objek_pajak.no_urut
                    AND dob.kd_jns_op=dat_objek_pajak.kd_jns_op
                join pbb.ref_kecamatan ref_kecamatan
                            on ref_kecamatan.kd_propinsi=dat_objek_pajak.kd_propinsi
                            and ref_kecamatan.kd_dati2=dat_objek_pajak.kd_dati2
                            and ref_kecamatan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                        join pbb.ref_kelurahan ref_kelurahan
                            on ref_kelurahan.kd_propinsi=dat_objek_pajak.kd_propinsi
                            and ref_kelurahan.kd_dati2=dat_objek_pajak.kd_dati2
                            and ref_kelurahan.kd_kecamatan=dat_objek_pajak.kd_kecamatan
                            and ref_kelurahan.kd_kelurahan=dat_objek_pajak.kd_kelurahan
                            WHERE dat_objek_pajak.kd_propinsi='" . $nop[0] . "'
                        AND dat_objek_pajak.kd_dati2='" . $nop[1] . "'
                        AND dat_objek_pajak.kd_kecamatan='" . $nop[2] . "'
                        AND dat_objek_pajak.kd_kelurahan='" . $nop[3] . "'
                        AND dat_objek_pajak.kd_blok='" . $blok_urut[0] . "'
                        AND dat_objek_pajak.no_urut='" . $blok_urut[1] . "'
                        AND dat_objek_pajak.kd_jns_op='" . $nop[5] . "'";
                $result = DB::connection('oracle_satutujuh')->select(db::raw($sql));
                $where = [
                    'kd_propinsi' => $nop[0],
                    'kd_dati2' => $nop[1],
                    'kd_kecamatan' => $nop[2],
                    'kd_kelurahan' => $nop[3],
                    'kd_blok' => $blok_urut[0],
                    'no_urut' => $blok_urut[1],
                    'kd_jns_op' => $nop[5],
                    'status_pembayaran_sppt' => '1',
                    // 'thn_pajak_sppt'=>'2022'
                ];
                $dataBayar = Collect(InformasiObjek::riwayatPembayaranOther($where));
                $hiddenList = ['total_bayar', 'tanggal_bayar'];
                $dataBayar->transform(function ($res) use ($hiddenList) {
                    $result = null;
                    $res->total_bayar = gallade::floatValParse($res->total_bayar);
                    $res->pbb = gallade::floatValParse($res->pbb);
                    if ($res->total_bayar > $res->pbb) {
                        $res->selisih = $res->total_bayar - $res->pbb;
                    } else {
                        $res->selisih = $res->pbb - $res->total_bayar;
                    }
                    $cek = kompensasiLebihBayar::where([
                        'kd_propinsi' => $res->kd_propinsi,
                        'kd_dati2' => $res->kd_dati2,
                        'kd_kecamatan' => $res->kd_kecamatan,
                        'kd_kelurahan' => $res->kd_kelurahan,
                        'kd_blok' => $res->kd_blok,
                        'no_urut' => $res->no_urut,
                        'kd_jns_op' => $res->kd_jns_op,
                        'thn_pajak_sppt' => $res->thn_pajak_sppt,
                    ])->get();
                    // dd($res);
                    $res->tanggal_bayar_real = explode(',', $res->tanggal_bayar_real);
                    $res->tanggal_bayar = current($res->tanggal_bayar_real);
                    if ($res->tanggal_bayar && !$cek->count()) {
                        $isCheck = "";
                        if (in_array($res->thn_pajak_sppt, ['2022'])) {
                            $isCheck = "checked='checked'";
                        }
                        $result = [
                            // 'nop'=>$res->kd_propinsi.".".
                            //         $res->kd_dati2.".".
                            //         $res->kd_kecamatan.".".
                            //         $res->kd_kelurahan.".".
                            //         $res->kd_blok."-".
                            //         $res->no_urut.".".
                            //         $res->kd_jns_op,
                            // 'nm_wp_sppt'=>$res->nm_wp_sppt,
                            'raw_tag' => "<input type='radio' class='form-check-input check-header check-filled' name='data[" . $res->thn_pajak_sppt . "][tahun]' value='" . $res->thn_pajak_sppt . "' " . $isCheck . ">" .
                                gallade::parseInputFormHidden(Arr::only((array)$res, $hiddenList), $hiddenList, $res->thn_pajak_sppt),
                            'thn_pajak_sppt' => $res->thn_pajak_sppt,
                            'total_bayar' => gallade::parseQuantity($res->total_bayar),
                            'pbb' => "<input type='text' class='form-controll-sm numeric numericMask' name='data[" . $res->thn_pajak_sppt . "][pbb]' value='" . $res->pbb . "' style='width:100px'>",
                            'selisih' => "<input type='text' class='form-controll numeric numericMask'  name='data[" . $res->thn_pajak_sppt . "][selisih]' value='" . $res->selisih . "' style='width:100px'>",
                            'tgl_pembayaran_sppt' => $res->tanggal_bayar,
                            'keterangan' => ''
                        ];
                    } else {
                        $list = [
                            '1' => 'Masuk Proses Kompensasi Bayar tahun berikutnya',
                            '0' => 'Kompensasi Bayar'
                        ];
                        $keterangan = "-";
                        /* if(in_array($res->status_pembayaran_sppt,array_keys($list))){
                            $keterangan=$list[$res->status_pembayaran_sppt];
                        } */
                        $result = [
                            // 'nop'=>$res->kd_propinsi.".".
                            //         $res->kd_dati2.".".
                            //         $res->kd_kecamatan.".".
                            //         $res->kd_kelurahan.".".
                            //         $res->kd_blok."-".
                            //         $res->no_urut.".".
                            //         $res->kd_jns_op,
                            // 'nm_wp_sppt'=>$res->nm_wp_sppt,
                            'raw_tag' => "-",
                            'thn_pajak_sppt' => $res->thn_pajak_sppt,
                            'total_bayar' => gallade::parseQuantity($res->total_bayar),
                            'pbb' => gallade::parseQuantity($res->pbb),
                            'selisih' => gallade::parseQuantity($res->selisih),
                            'tgl_pembayaran_sppt' => $res->tanggal_bayar,
                            'keterangan' => $keterangan
                        ];
                    }
                    return $result;
                });
                $dataBayar = $dataBayar->filter();
                if ($result) {
                    $return['data'] = (array)current($result);
                    $return['data']['dati2_wp'] = str_replace('-', '.', $return['data']['dati2_wp']);
                    $dati2 = explode('.', $return['data']['dati2_wp']);
                    if (count($dati2) >= 2) {
                        $return['data']['propinsi_wp'] = $dati2[count($dati2) - 1];
                        $return['data']['dati2_wp'] = $dati2[0];
                    }
                    $return['data']['kelurahan_wp'] = str_replace('-', '.', $return['data']['kelurahan_wp']);
                    $kelurahan_wp = explode('.', $return['data']['kelurahan_wp']);
                    if (count($kelurahan_wp) >= 2) {
                        $return['data']['kecamatan_wp'] = $kelurahan_wp[count($kelurahan_wp) - 1];
                        $return['data']['kelurahan_wp'] = $kelurahan_wp[0];
                    }
                    $rt_wp = sprintf("%03s", str_replace(" ", "", $return['data']['rt_wp']));
                    $rw_wp = sprintf("%02s", str_replace(" ", "", $return['data']['rw_wp']));
                    $nop_rt = sprintf("%03s", str_replace(" ", "", $return['data']['nop_rt']));
                    $nop_rw = sprintf("%02s", str_replace(" ", "", $return['data']['nop_rw']));
                    $return['data']['rt_rw_wp'] = $rt_wp . '/' . $rw_wp;
                    $return['data']['notelp_wp'] = $return['data']['telp_wp'];
                    $return['data']['rt_rw_op'] = $nop_rt . '/' . $nop_rw;
                    $return['data']['alamat_op'] = $return['data']['nop_alamat'];
                    $return['data']['nik_wp'] = substr($return['data']['nik_wp'], 0, 16);
                    $return['tag'] = [
                        'riwayat_pembayaran' => gallade::generateinTbody($dataBayar->toArray(), "Data Masih Kosong", 9)
                    ];
                    $return['status'] = true;
                    $return['msg'] = 'NOP berhasil ditemukan.';
                }
            }
        } catch (\Exception $e) {
            $return = $e->getMessage();
            Log::info($return);
            $result = false;
        }
        return response()->json($return);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $return = [];
        $list = [
            "nop" => "required",
            "nik_wp" => "required",
            "nama_wp" => "required",
            "alamat_wp" => "required",
            "nop_alamat" => "required",
            "nop_rt" => "required",
            "nop_rw" => "required",
            "nop_kelurahan" => "required",
            "nop_kecamatan" => "required",
            "luas_bumi" => "required",
            "luas_bng" => "required",
            "data" => "required"
        ];
        $validation = validator($request->all(), $list, [
            'required' => 'Kolom :attribute harus di isi.',
            'data.required' => 'Daftar tahun harus di pilih.',
        ]);
        if ($validation->fails()) {
            return response()->json([
                'status' => false,
                'msg' => '<ol><li>' . join('</li><li>', $validation->errors()->all()) . '</li></ol>'
            ]);
        }
        DB::beginTransaction();
        try {
            $requestAll = $request->only(array_keys($list));
            $collectData = Collect($requestAll);
            $listA = $collectData->except('data')->toArray();
            $getB = $collectData->only('data')->toArray();
            $listB = collect($getB['data'])->filter(function ($res) {
                if (in_array('tahun', array_keys($res))) {
                    return $res;
                }
            })->toArray();
            $listCombine = Collect([$listA])->crossJoin($listB);
            $listNop = Collect();
            $lastNumber = kompensasiLebihBayar::select([DB::raw('max(id)+1 id')])->first();
            $listCombine = $listCombine->transform(function ($res) use ($listNop, $lastNumber) {
                $result = array_merge($res['0'], $res['1']);
                $listNop->push($result["nop"] . '.' . $result['tahun']);
                $nop = explode(".", $result["nop"]);
                $blok_urut = explode("-", $nop[4]);
                $listSave = [
                    'id' => $lastNumber->id,
                    'kd_propinsi' => $nop[0],
                    'kd_dati2' => $nop[1],
                    'kd_kecamatan' => $nop[2],
                    'kd_kelurahan' => $nop[3],
                    'kd_blok' => $blok_urut[0],
                    'no_urut' => $blok_urut[1],
                    'kd_jns_op' => $nop[5],
                    'thn_pajak_sppt' => $result['tahun'],
                    'nama_wp' => $result['nama_wp'],
                    'alamat_wp' => $result['alamat_wp'],
                    'alamat_op' => $result['nop_alamat'],
                    'pbb_asli' => gallade::floatValParse($result['pbb']),
                    'pbb_bayar' => gallade::floatValParse($result['total_bayar']),
                    'selisih' => gallade::floatValParse($result['selisih']),
                    'tgl_bayar' => $result['tanggal_bayar'],
                    'kd_status' => '0',
                    'created_by' => Auth()->user()->id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ];
                $lastNumber->id = $lastNumber->id + 1;
                return $listSave;
            });
            $listSave = $listCombine->toArray();
            kompensasiLebihBayar::insert($listSave);
            $nomor_klb = join('|', $listNop->toArray());
            $return = [
                "msg" => "<p>Kompensasi Lebih Bayar berhasil di buat.</p> 
                            <p>Download SK.</p>
                            <a href='" . url("kompensasi_lebih_bayar/sk?nomor_klb=" . $nomor_klb) . "'
                            target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i> Downlaod SK</a>",
                "status" => true,
                'curl' => $nomor_klb
            ];
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $msg = $e->getMessage(); //'Proses Kompensasi Bayar belum berhasil.';//
            return response()->json(["msg" => $msg, "status" => false]);
        }
        return response()->json($return);
    }
    private function _cekTahunBayarReady($data, $tahun = false)
    {
        $nop = explode(".", $data);
        $blok_urut = explode("-", $nop[4]);
        $yearAvailable = ($tahun) ? $tahun : $nop[6];
        $where = [
            'kd_propinsi' => $nop[0],
            'kd_dati2' => $nop[1],
            'kd_kecamatan' => $nop[2],
            'kd_kelurahan' => $nop[3],
            'kd_blok' => $blok_urut[0],
            'no_urut' => $blok_urut[1],
            'kd_jns_op' => $nop[5],
            'thn_pajak_sppt' => $yearAvailable
        ];
        $result = InformasiObjek::riwayatPembayaran($where)->orderby('thn_pajak_sppt')
            ->where(['status_pembayaran_sppt' => '1'])->get();
        if (!$result->count()) {
            return $yearAvailable;
        }
        $tahun = $yearAvailable + 1;
        return $this->_cekTahunBayarReady($data, $tahun);
    }
    public function verifikasi(Request $request)
    {
        $return = ["msg" => "Proses Verifikasi Kompnsasi Bayar tidak berhasil.", "status" => false];
        $request = $request->only('data');
        if (!$request) {
            return response()->json($return);
        }
        if (in_array('nomor_klb', array_keys($request['data']))) {
            $request = $request['data'];
            DB::beginTransaction();
            try {
                $nop = explode(".", $request["nomor_klb"]);
                $blok_urut = explode("-", $nop[4]);
                $where = [
                    'kd_propinsi' => $nop[0],
                    'kd_dati2' => $nop[1],
                    'kd_kecamatan' => $nop[2],
                    'kd_kelurahan' => $nop[3],
                    'kd_blok' => $blok_urut[0],
                    'no_urut' => $blok_urut[1],
                    'kd_jns_op' => $nop[5],
                    'thn_pajak_sppt' => $nop[6],
                ];
                kompensasiLebihBayar::where($where)->update(['kd_status' => '1']);
                $getData = kompensasiLebihBayar::where($where)->select(['selisih'])->first()->toArray();
                // sppt_potongan 
                $tahun = $this->_cekTahunBayarReady($request["nomor_klb"]);
                $updatePotonganSppt = [
                    'kd_propinsi' => $nop[0],
                    'kd_dati2' => $nop[1],
                    'kd_kecamatan' => $nop[2],
                    'kd_kelurahan' => $nop[3],
                    'kd_blok' => $blok_urut[0],
                    'no_urut' => $blok_urut[1],
                    'kd_jns_op' => $nop[5],
                    'thn_pajak_sppt' => $tahun,
                    'jns_potongan' => '3',
                    'nilai_potongan' => $getData['selisih'],
                    'keterangan' => 'Kompensasi Lebih Bayar dari pembayaran tahun:' . $nop[6],
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'created_by' => Auth()->user()->id
                ];
                // SPPT_Potongan::insert($updatePotonganSppt);
                SPPT_Potongan_det::insert($updatePotonganSppt);

                DB::connection("oracle_satutujuh")
                    ->statement(DB::raw("  begin hitung_potongan('" . $nop[2] . "','" . $nop[3] . "','" . $blok_urut[0] . "','" . $blok_urut[1] . "','" . $nop[5] . "','" . $tahun . "'); commit;   end;"));
                $return = ["msg" => "Proses Verifikasi Kompnsasi Bayar berhasil.", "status" => true];
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                // $msg=$e->getMessage();//'Proses Kompensasi Bayar belum berhasil.';//
                $msg = "Silahkan cek objek melalui data dan informasi! Objek terindikasi non aktif atau ketetapan pada " . $tahun . " belum ada";
                return response()->json(["msg" => $msg, "status" => false]);
            }
        }
        return response()->json($return);
    }
    public function sk(Request $request)
    {
        $request = $request->only('nomor_klb');
        if ($request) {
            if (strlen($request['nomor_klb']) < '29') {
                return redirect('kompensasi_lebih_bayar/daftar');
            }
            $nomor_klb = explode('.', $request['nomor_klb']);
            $blok = explode('-', $nomor_klb[4]);
            $getData = kompensasiLebihBayar::where([
                'kd_propinsi' => $nomor_klb[0],
                'kd_dati2' => $nomor_klb[1],
                'kd_kecamatan' => $nomor_klb[2],
                'kd_kelurahan' => $nomor_klb[3],
                'kd_blok' => $blok[0],
                'no_urut' => $blok[1],
                'kd_jns_op' => $nomor_klb[5],
                'thn_pajak_sppt' => $nomor_klb[6],
            ])->get()->first()->toArray();
            $data = array_merge([
                'nop' => $getData['kd_propinsi'] . '.' .
                    $getData['kd_dati2'] . '.' .
                    $getData['kd_kecamatan'] . '.' .
                    $getData['kd_kelurahan'] . '.' .
                    $getData['kd_blok'] . '-' .
                    $getData['no_urut'] . '.' .
                    $getData['kd_jns_op'],
                'no_sk' => sprintf("%05s", 1),
                'tahunpajak' => '2022',
                'nop_kelurahan' => $nomor_klb[0] . "." . $nomor_klb[1] . "." . $nomor_klb[2],
                'tanggalsk' => '23 Juni  2022',
            ], $getData);
            $data['pbb_bayar'] = gallade::parseQuantity($data['pbb_bayar']);
            $data['pbb_asli'] = gallade::parseQuantity($data['pbb_asli']);
            $data['selisih'] = gallade::parseQuantity($data['selisih']);
            $url =   config('app.url') . 'kompensasi_lebih_bayar/sk?nomor_klb=' . $request['nomor_klb'];
            // return view('kompensasi_bayar.cetak_sk', compact('data','url'));
            $kaban = pegawaiStruktural::where('kode', '01')->first();

            $pages = VieW::make('kompensasi_bayar.cetak_sk', compact('data', 'url', 'kaban'));
            $pdf = PDF::loadHTML($pages)
                ->setPaper('A4');
            $pdf->setOption('enable-local-file-access', true);
            return $pdf->stream();
        }
    }
    public function total(Request $request)
    {
        $request = $request->only('kecamatan', 'kelurahan', 'nop');
        try {
            $select = [
                DB::raw('sum(pbb_asli) pbb_asli'),
                DB::raw('sum(pbb_bayar) pbb_bayar'),
                DB::raw('sum(selisih) selisih'),
            ];
            $dataSet = kompensasiLebihBayar::select($select)
                ->where(function ($q) use ($request) {
                    if (in_array('nop', array_keys($request))) {
                        if ($request["nop"] != '') {
                            $nop = explode(".", $request["nop"]);
                            $blok_urut = explode("-", $nop[4]);
                            $q->where([
                                'kd_propinsi' => $nop[0],
                                'kd_dati2' => $nop[1],
                                'kd_kecamatan' => $nop[2],
                                'kd_kelurahan' => $nop[3],
                                'kd_blok' => $blok_urut[0],
                                'no_urut' => $blok_urut[1],
                                'kd_jns_op' => $nop[5],
                            ]);
                        }
                    }
                    if (in_array('kecamatan', array_keys($request))) {
                        if ($request['kecamatan'] != "-") {
                            $q->where(['kd_kecamatan' => $request['kecamatan']]);
                        }
                    }
                    if (in_array('kelurahan', array_keys($request))) {
                        if ($request['kelurahan'] != "-") {
                            $q->where(['kd_kelurahan' => $request['kelurahan']]);
                        }
                    }
                })
                ->orderBy('id', 'desc')
                ->get()->first();
            $resultSet = Collect($dataSet->toArray())->transform(function ($res) {
                return "Rp. " . gallade::parseQuantity($res);
            });
            $result = gallade::generateinTbody([$resultSet->toArray()], "Data Masih Kosong", 9);
            return response()->json(["data" => $result, "status" => true]);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    public function cetak(Request $request)
    {
        $request = $request->only('kecamatan', 'kelurahan', 'nop');

        // dd(in_array('kelurahan', array_keys($request)));

        try {
            $select = ['*'];
            $dataSet = kompensasiLebihBayar::select($select)
                ->where(function ($q) use ($request) {
                    if (in_array('nop', array_keys($request))) {
                        if ($request["nop"] != '') {
                            $nop = explode(".", $request["nop"]);
                            $blok_urut = explode("-", $nop[4]);
                            $q->where([
                                'kd_propinsi' => $nop[0],
                                'kd_dati2' => $nop[1],
                                'kd_kecamatan' => $nop[2],
                                'kd_kelurahan' => $nop[3],
                                'kd_blok' => $blok_urut[0],
                                'no_urut' => $blok_urut[1],
                                'kd_jns_op' => $nop[5],
                            ]);
                        }
                        if (in_array('kecamatan', array_keys($request))) {
                            if ($request['kecamatan'] != "-") {
                                $q->where(['kd_kecamatan' => $request['kecamatan']]);
                            }
                        }
                        if (in_array('kelurahan', array_keys($request))) {
                            if ($request['kelurahan'] != "-") {
                                $q->where(['kd_kelurahan' => $request['kelurahan']]);
                            }
                        }
                    }
                })
                ->orderBy('id', 'desc')
                ->get();
            $kota_wp = "Semua Kecamatan";
            $kelurahan_wp = "Semua Kelurahan";
            if (in_array('kecamatan', array_keys($request)) || in_array('kelurahan', array_keys($request))) {
                $getkelkecOP = DB::connection('oracle_dua')->table('ref_kecamatan')
                    ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                    ->leftJoin(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) use ($request) {
                        $join->On('ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                            ->where(['ref_kelurahan.kd_kelurahan' => $request['kelurahan']??'']);
                    })
                    ->where(function ($q) use ($request) {
                        $q->where(['ref_kecamatan.kd_kecamatan' => $request['kecamatan']]);
                    })
                    ->get()->first();
                $kelurahan_wp = $getkelkecOP->nm_kelurahan ?? "Semua Kelurahan";
                $kota_wp = $getkelkecOP->nm_kecamatan ?? "Semua Kecamatan";
            }
            $export = new ExportsKompensasiLebihBayar($dataSet->toArray(), [
                'kota_wp' => $kota_wp,
                'kelurahan_wp' => $kelurahan_wp,
            ]);
            return Excel::download($export, 'kompensasi_lebih_bayar.xlsx');
        } catch (\Exception $e) {
            // dd($e);

            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
