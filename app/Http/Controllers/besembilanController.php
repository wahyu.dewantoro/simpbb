<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class besembilanController extends Controller
{
    //

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $start = date('Ymd', strtotime($request->start));
            $end = date('Ymd', strtotime($request->end));

            $url = 'https://majapahit.bankjatim.co.id/digital/rest/nontrx/v';
            $keys = DB::table('besembilan_key')
                ->select(db::raw("source,keyid"))
                ->get();

            $bulk = [];
            foreach ($keys as $rk) {
                $response = Http::post($url, [
                    'key' => $rk->keyid,
                    'tglawal' => $start,
                    'tglakhir' => $end
                ]);
                $res = $response->json();
                $data = $res['history'] ?? [];

                $status = $response->successful();
                if ($status == true) {
                    foreach ($data as $row) {
                        $bulk[] = [
                            'dateTime' => tglindo($row['dateTime']) . ' ' . date('H:i:s', strtotime($row['dateTime'])),
                            'description' => $row['description'],
                            'transactionCode' => $row['transactionCode'],
                            'amount' => $row['amount'],
                            'flag' => $row['flag'],
                            'ccy' => $row['ccy'],
                            'reffno' => $row['reffno'],
                            'source' => $rk->source
                        ];
                    }
                }
            }

            $data = $bulk;
            return view('besembilan._data', compact('data'));
        }
        return view('besembilan.index');
    }

    public function indexPelimpahan(Request $request)
    {
        if ($request->ajax()) {
            $start = date('Ymd', strtotime($request->start));
            $end = date('Ymd', strtotime($request->end));

            $data = DB::table("besembilan_mutasi")->where('flag', 'D')
                ->whereraw("trunc(datetime) between to_date('$start','yyyymmdd') and to_date('$end','yyyymmdd')")
                ->orderBy("datetime")
                ->get();
            return view('besembilan._dataPelimpahan', compact('data'));
        }

        return view('besembilan.index_pelimpahan');
    }
}
