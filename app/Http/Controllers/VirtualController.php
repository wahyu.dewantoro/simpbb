<?php

namespace App\Http\Controllers;

use App\Exports\dataVaExport;
use App\Models\Data_billing;
use App\VirtualAccount;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class VirtualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = VirtualAccount::orderby('tanggal_exp', 'desc');
        $a = Carbon::now()->subDay(6)->format('m/d/Y');
        $b = Carbon::now()->format('m/d/Y');


        if ($request->has('daterange')) {
            $daterange = $request->daterange;
        } else {
            $daterange = $a . ' - ' . $b;
        }

        $search = strtolower(($request->search ?? ''));
        $status = $request->status ?? '99';
        $appends = [];
        if ($search != '' or $status != '' or $daterange != '') {
            if ($daterange != '') {
                $exp = explode(' - ', $daterange);
                $start = new Carbon($exp[0]);
                $start = $start->format('Ymd');
                $end = new Carbon($exp[1]);
                $end = $end->format('Ymd');
                $data = $data->whereraw(" (trunc(tanggal_bayar) between to_date('$start','yyyymmdd') and to_date('$end','yyyymmdd'))");
                $appends['daterange'] = $daterange;
            }

            if ($search != '') {
                $nop = onlyNumber($search);

                $ff = "lower(nama_wp) like '%$search%' or";

                if ($nop != '') {
                    $ff .= " kobil like '%$nop%' or nop like '%$nop%' or nomor_va like '%$nop%' or";
                }

                $ff = substr($ff, 0, -2);
                $data = $data->whereraw($ff);
                $appends['search'] = $search;
            }

            switch ($status) {
                case '1':
                    # code...
                    $wh = "amount is not null";
                    break;
                case '2':
                    $wh = "amount is null and tanggal_exp>sysdate";
                    break;
                case '3':
                    $wh = "amount is null and tanggal_exp<sysdate";
                    break;
                default:
                    # code...
                    $wh = "";
                    break;
            }
            if ($wh != '') {
                $data = $data->whereraw($wh);
                $appends['status'] = $status;
            }
        }

        $data = $data->paginate(10)->appends($appends);

        return view('virtual_account/index', compact('data', 'daterange'));
    }


    public function cetak(Request $request)
    {
        // return $request->all();
        $data = VirtualAccount::orderby('tanggal_exp', 'desc');
        $a = Carbon::now()->subDay(6)->format('m/d/Y');
        $b = Carbon::now()->format('m/d/Y');


        if ($request->has('daterange')) {
            $daterange = $request->daterange;
        } else {
            $daterange = $a . ' - ' . $b;
        }

        $search = strtolower(($request->search ?? ''));
        $status = $request->status ?? '99';
        $appends = [];
        if ($search != '' or $status != '' or $daterange != '') {
            if ($daterange != '') {
                $exp = explode(' - ', $daterange);
                $start = new Carbon($exp[0]);
                $start = $start->format('Ymd');
                $end = new Carbon($exp[1]);
                $end = $end->format('Ymd');
                $data = $data->whereraw(" (trunc(tanggal_bayar) between to_date('$start','yyyymmdd') and to_date('$end','yyyymmdd'))");
                $appends['daterange'] = $daterange;
            }

            if ($search != '') {
                $nop = onlyNumber($search);

                $ff = "lower(nama_wp) like '%$search%' or";

                if ($nop != '') {
                    $ff .= " kobil like '%$nop%' or nop like '%$nop%' or nomor_va like '%$nop%' or";
                }

                $ff = substr($ff, 0, -2);
                $data = $data->whereraw($ff);
                $appends['search'] = $search;
            }

            switch ($status) {
                case '1':
                    # code...
                    $wh = "amount is not null";
                    break;
                case '2':
                    $wh = "amount is null and tanggal_exp>sysdate";
                    break;
                case '3':
                    $wh = "amount is null and tanggal_exp<sysdate";
                    break;
                default:
                    # code...
                    $wh = "";
                    break;
            }
            if ($wh != '') {
                $data = $data->whereraw($wh);
                $appends['status'] = $status;
            }
        }
        $data = $data->get();
        return Excel::download(new dataVaExport($data), 'pembayaran va.xlsx');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id = decrypt($id);
        // return $id;
        $data = Data_billing::with('VirtualAccount')->findOrFail($id);

        // return $data;
        return view('virtual_account.flayer', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
