<?php

namespace App\Http\Controllers;

use App\Helpers\Layanan_conf;
use App\Jobs\disposisiPenelitian;
use App\Models\DAT_OBJEK_PAJAK;
use App\Models\Jenis_layanan;
use App\Models\Layanan;
use App\Models\Layanan_dokumen;
use App\Models\Layanan_objek;
use App\PermohonanBphtb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class layananBpthbCotroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = PermohonanBphtb::with(['Layanan', 'Objek', 'Objek.Notaperhitungan','Objek.Notaperhitungan.DataBilling', 'VmonitorLayanan'])->orderby('nomor_layanan_bphtb', 'desc')->paginate(10);
        if ($request->ajax()) {
            return view('layanan-bphtb.index_data', compact('data'))->render();
        }

        /* 
        foreach($data as $i){
            dd($i->Objek);
        } */
        // return $data;
        return view('layanan-bphtb.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PermohonanBphtb::findorfail($id);
        if ($data->status != '') {
            // abort(404);
            return redirect(url('layanan-bphtb'))->with(['error' => 'Berkas sudah di mapping']);
        }

        $jp = Jenis_layanan::wherein('id', [
            6,
            3,
            9
        ])->orderby("ORDER")
            ->pluck('nama_layanan', 'id')->toarray();
        $pbb = DAT_OBJEK_PAJAK::where([
            'kd_propinsi' => $data->kd_propinsi,
            'kd_dati2' => $data->kd_dati2,
            'kd_kecamatan' => $data->kd_kecamatan,
            'kd_kelurahan' => $data->kd_kelurahan,
            'kd_blok' => $data->kd_blok,
            'no_urut' => $data->no_urut,
            'kd_jns_op' => $data->kd_jns_op
        ])->first();

        // return $pbb;
        $dokumen = json_decode($data->dokumen_lampiran);
        $penjual = json_decode($data->penjual, true);

        // return $penjual;
        return view('layanan-bphtb.form', compact('data', 'jp', 'pbb', 'dokumen', 'penjual'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        DB::beginTransaction();
        try {


            $bphtb = PermohonanBphtb::findorfail($id);
            $jenis_layanan_id = $request->jenis_layanan_id;
            $jl = Jenis_layanan::find($jenis_layanan_id);
            $nomor_layanan = Layanan_conf::nomor_layanan();
            $layanan = [
                'nomor_layanan' => $nomor_layanan,
                'tanggal_layanan' => now(),
                'jenis_layanan_id' => $jl->id,
                'jenis_layanan_nama' => $jl->nama_layanan,
                'nik' => str_pad(random_int(0, 9999999999999999), 16, '0', STR_PAD_LEFT),
                'nama' => $bphtb->nama_pemohon,
                'alamat' => 'Jl. Agus Salim No.7',
                'kelurahan' => 'Kiduldalem',
                'kecamatan' => 'Klojen',
                'dati2' => 'Kota Malang',
                'propinsi' => 'Jawa Timur',
                'nomor_telepon' => $bphtb->nomor_pemohon,
                'keterangan' => $bphtb->keterangan,
                'jenis_objek' => 5
            ];



            $pbb = DAT_OBJEK_PAJAK::where(
                [
                    'kd_propinsi' => $bphtb->kd_propinsi,
                    'kd_dati2' => $bphtb->kd_dati2,
                    'kd_kecamatan' => $bphtb->kd_kecamatan,
                    'kd_kelurahan' => $bphtb->kd_kelurahan,
                    'kd_blok' => $bphtb->kd_blok,
                    'no_urut' => $bphtb->no_urut,
                    'kd_jns_op' => $bphtb->kd_jns_op,

                ]
            )->first();


            $subjek_pajak = $pbb->DatSubjekPajak;
            $layanan_objek = [];
            if ($jenis_layanan_id == '3') {
                // pembetulan luas
                $layanan_objek = [
                    'nomor_layanan' => $nomor_layanan,
                    'nik_wp' => formatTo16Digit($pbb->subjek_pajak_id),
                    'nama_wp' => $subjek_pajak->nm_wp,
                    'alamat_wp' => $subjek_pajak->jalan_wp,
                    'rt_wp' => $subjek_pajak->rt_wp,
                    'rw_wp' => $subjek_pajak->rw_wp,
                    'kelurahan_wp' => $subjek_pajak->kelurahan_wp,
                    'dati2_wp' => $subjek_pajak->kota_wp,
                    'propinsi_wp' => null,
                    'kd_propinsi' => $bphtb->kd_propinsi,
                    'kd_dati2' => $bphtb->kd_dati2,
                    'kd_kecamatan' => $bphtb->kd_kecamatan,
                    'kd_kelurahan' => $bphtb->kd_kelurahan,
                    'kd_blok' => $bphtb->kd_blok,
                    'no_urut' => $bphtb->no_urut,
                    'kd_jns_op' => $bphtb->kd_jns_op,
                    'luas_bumi' => onlyNumber($request->luas_bumi_pembetulan),
                    'luas_bng' => onlyNumber($request->luas_bng_pembetulan),
                    'telp_wp' => '0000000000',
                    'alamat_op' => $pbb->jalan_op . ' ' . $pbb->blok_kav_op,
                    'rt_op' => $pbb->rt_op,
                    'rw_op' => $pbb->rw_op,
                ];
            }


            if ($jenis_layanan_id == 9) {
                // mutasi penuh

                $layanan_objek = [
                    'nomor_layanan' => $nomor_layanan,
                    'nik_wp' => $request->nik_wp_penuh,
                    'nama_wp' => $request->nm_wp_penuh,
                    'alamat_wp' => $request->alamat_wp_penuh,
                    'rt_wp' => $request->rt_wp_penuh,
                    'rw_wp' => $request->rw_wp_penuh,
                    'kelurahan_wp' => $request->kelurahan_wp_penuh,
                    'dati2_wp' => $request->dati2_wp_penuh,
                    'propinsi_wp' => $request->propinsi_wp_penuh,
                    'kd_propinsi' => $bphtb->kd_propinsi,
                    'kd_dati2' => $bphtb->kd_dati2,
                    'kd_kecamatan' => $bphtb->kd_kecamatan,
                    'kd_kelurahan' => $bphtb->kd_kelurahan,
                    'kd_blok' => $bphtb->kd_blok,
                    'no_urut' => $bphtb->no_urut,
                    'kd_jns_op' => $bphtb->kd_jns_op,
                    'luas_bumi' => $pbb->total_luas_bumi,
                    'luas_bng' => $pbb->total_luas_bng,
                    'telp_wp' => '0000000000',
                    'alamat_op' => $pbb->jalan_op . ' ' . $pbb->blok_kav_op,
                    'rt_op' => $pbb->rt_op,
                    'rw_op' => $pbb->rw_op,
                ];
            }


            if ($jenis_layanan_id == 6) {
                // mutasi pecah

                // induknya
                // pembetulan luas
                $luas_induk = onlyNumber($pbb->total_luas_bumi) -  onlyNumber($request->luas_bumi_pecah);
                $luas_pecah = onlyNumber($request->luas_bumi_pecah);
                $layanan_objek[] = [
                    'nomor_layanan' => $nomor_layanan,
                    'nik_wp' => formatTo16Digit($pbb->subjek_pajak_id),
                    'nama_wp' => $subjek_pajak->nm_wp,
                    'alamat_wp' => $subjek_pajak->jalan_wp,
                    'rt_wp' => $subjek_pajak->rt_wp,
                    'rw_wp' => $subjek_pajak->rw_wp,
                    'kelurahan_wp' => $subjek_pajak->kelurahan_wp,
                    'dati2_wp' => $subjek_pajak->kota_wp,
                    'propinsi_wp' => null,
                    'kd_propinsi' => $bphtb->kd_propinsi,
                    'kd_dati2' => $bphtb->kd_dati2,
                    'kd_kecamatan' => $bphtb->kd_kecamatan,
                    'kd_kelurahan' => $bphtb->kd_kelurahan,
                    'kd_blok' => $bphtb->kd_blok,
                    'no_urut' => $bphtb->no_urut,
                    'kd_jns_op' => $bphtb->kd_jns_op,
                    'luas_bumi' => $luas_induk,
                    'luas_bng' => onlyNumber($pbb->total_luas_bng),
                    'telp_wp' => '0000000000',
                    'alamat_op' => $pbb->jalan_op . ' ' . $pbb->blok_kav_op,
                    'rt_op' => $pbb->rt_op,
                    'rw_op' => $pbb->rw_op,
                    'sisa_pecah_total_gabung' => $luas_induk - $luas_pecah,
                    'hasil_pecah_hasil_gabung' => $luas_pecah
                ];


                // obejk pecahan nya
                $layanan_objek[] = [
                    'nomor_layanan' => $nomor_layanan,
                    'nik_wp' => $request->nik_wp_pecah,
                    'nama_wp' => $request->nm_wp_pecah,
                    'alamat_wp' => $request->alamat_wp_pecah,
                    'rt_wp' => $request->rt_wp_pecah,
                    'rw_wp' => $request->rw_wp_pecah,
                    'kelurahan_wp' => $request->kelurahan_wp_pecah,
                    'dati2_wp' => $request->dati2_wp_pecah,
                    'propinsi_wp' => $request->propinsi_wp_pecah,
                    'kd_propinsi' => $bphtb->kd_propinsi,
                    'kd_dati2' => $bphtb->kd_dati2,
                    'kd_kecamatan' => $bphtb->kd_kecamatan,
                    'kd_kelurahan' => $bphtb->kd_kelurahan,
                    'kd_blok' => $bphtb->kd_blok,
                    'no_urut' => '0000',
                    'kd_jns_op' => '0',
                    'luas_bumi' => $luas_pecah,
                    'luas_bng' => onlyNumber($request->luas_bng_pecah),
                    'telp_wp' => '0000000000',
                    'alamat_op' => $pbb->jalan_op . ' ' . $pbb->blok_kav_op,
                    'rt_op' => $pbb->rt_op,
                    'rw_op' => $pbb->rw_op,
                    'sisa_pecah_total_gabung' => $luas_induk - $luas_pecah,
                    'hasil_pecah_hasil_gabung' => $luas_pecah
                ];
            }


            // dd(count($layanan_objek));
            Layanan::create($layanan);

            // dokumen nya

            $doks = json_decode($bphtb->dokumen_lampiran);
            foreach ($doks as $key => $value) {
                # code...
                $dokumen = [
                    'nomor_layanan' => $nomor_layanan,
                    'tablename' => 'LAYANAN',
                    'filename' => $value->filename,
                    'nama_dokumen' => $value->filename,
                    'url' => $value->url,
                    'keyid' => '-',
                    'disk' => '-'
                ];
                Layanan_dokumen::create($dokumen);
            }

            // cek pecah atau tidak
            if ($jenis_layanan_id == 6) {

                // jika pecah
                $lo = Layanan_objek::create($layanan_objek[0]);

                $pecahan = $layanan_objek[1];
                $pecahan['nop_gabung'] = $lo->id;
                Layanan_objek::create($pecahan);
            } else {
                Layanan_objek::create($layanan_objek);
            }

            PermohonanBphtb::find($bphtb->id)
                ->update([
                    'status' => 1,
                    'nomor_layanan_pbb' => $nomor_layanan
                ]);

            // disposisiPenelitian($nomor_layanan)-
            disposisiPenelitian::dispatch($nomor_layanan)->onQueue('layanan');
            DB::commit();

            $flash = [
                'success' => 'Berhasil proses.'
            ];
            // return ['layanan' => $layanan, 'layanan_objek' => $layanan_objek];
            //code...
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            DB::rollBack();
            // return $th;
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('layanan-bphtb'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
