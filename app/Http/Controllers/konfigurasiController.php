<?php

namespace App\Http\Controllers;

use App\HisJatuhTempo;
use App\Imports\NopRekonImport;
use App\Jobs\updateJatuhTempo;
use App\Sppt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class konfigurasiController extends Controller
{
    //

    public function jatuhTempo(Request $request)
    {

        $tahun = date('Y');
        return view('konfigurasi.jatuhtempo', compact('tahun'));
    }

    public function previewUpload(Request $request)
    {
        $default = response()->json(['status' => false, 'msg' => "Format tidak sesuai"]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->getRealPath();
            $ext = $file->extension();
            // return $ext;
            if (!in_array($ext, ["xlsx", "csv", "xls"])) {
                return $default;
            }

            $array = Excel::toArray(new NopRekonImport(), $file);

            // dd($array[0]);

            $data = [];
            // $tgl_jat = date('Y-m-d', strtotime($request->tanggal));
            $tgl = date('Y-m-d', strtotime($request->tanggal));
            foreach ($array[0] as $row) {
                $res = trim(onlyNumber($row['nop']));

                if ($res <> '' && $row['tahun'] <> '') {
                    $datanop['kd_propinsi'] = substr($res, 0, 2) ?? null;
                    $datanop['kd_dati2'] = substr($res, 2, 2) ?? null;
                    $datanop['kd_kecamatan'] = substr($res, 4, 3) ?? null;
                    $datanop['kd_kelurahan'] = substr($res, 7, 3) ?? null;
                    $datanop['kd_blok'] = substr($res, 10, 3) ?? null;
                    $datanop['no_urut'] = substr($res, 13, 4) ?? null;
                    $datanop['kd_jns_op'] = substr($res, 17, 1) ?? null;

                    $datanop['thn_pajak_sppt'] = $row['tahun'];

                    $datanop['tgl_jatuh_tempo_sppt'] = $tgl;
                    if (strlen($res) <> 18) {
                        $datanop['status'] = 0;
                        if (strlen($res) <> 18) {
                            $datanop['keterangan'] = 'Digit NOP harus 18';
                        } else {
                            $datanop['keterangan'] = '';
                        }
                    } else {
                        $datanop['status'] = 1;
                        $datanop['keterangan'] = 'Bisa di proses';
                    }
                    $data[] = $datanop;
                }
            }
            // $kode_tp = $request->kode_tp;
            return view('konfigurasi/jatuhtempo_', compact('data'));
        }


        return response()->json($default);
    }

    public function UploadMasal(Request $request)
    {
        DB::beginTransaction();
        try {
            $batchnop = [];
            $kd_propinsi =  explode('|', $request->kd_propinsi);
            $kd_dati2 =  explode('|', $request->kd_dati2);
            $kd_kecamatan =  explode('|', $request->kd_kecamatan);
            $kd_kelurahan =  explode('|', $request->kd_kelurahan);
            $kd_blok =  explode('|', $request->kd_blok);
            $no_urut =  explode('|', $request->no_urut);
            $kd_jns_op =  explode('|', $request->kd_jns_op);
            $thn_pajak_sppt =  explode('|', $request->thn_pajak_sppt);
            $tgl_jatuh_tempo_sppt =  explode('|', $request->tgl_jatuh_tempo_sppt);




            // $tanggal = isset($tgl_jatuh_tempo_sppt[count($tgl_jatuh_tempo_sppt) - 1]) ? date('Ymd', strtotime($tgl_jatuh_tempo_sppt[count($tgl_jatuh_tempo_sppt) - 1])) : null;



            foreach ($kd_propinsi as $i => $row) {

                $wh['kd_propinsi'] = $kd_propinsi[$i] ?? null;
                $wh['kd_dati2'] = $kd_dati2[$i] ?? null;
                $wh['kd_kecamatan'] = $kd_kecamatan[$i] ?? null;
                $wh['kd_kelurahan'] = $kd_kelurahan[$i] ?? null;
                $wh['kd_blok'] = $kd_blok[$i] ?? null;
                $wh['no_urut'] = $no_urut[$i] ?? null;
                $wh['kd_jns_op'] = $kd_jns_op[$i] ?? null;
                $wh['thn_pajak_sppt'] = $thn_pajak_sppt[$i] ?? null;

                DB::connection("oracle_satutujuh")->table('sppt')->where($wh)
                    ->update(['tgl_jatuh_tempo_sppt' => new Carbon($tgl_jatuh_tempo_sppt[$i])]);
            }

            // RekonNop::insert($batchnop);
            DB::commit();

            $response = [
                'status' => true,
                'msg' => 'berhasil update tanggaljatuh tempo'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $response = [
                'status' => false,
                'msg' => $th->getMessage()
            ];
        }

        return response()->json($response);
    }

    public function jatuhTempoProses(Request $request)
    {
        // dd($request->thn_pajak_sppt);
        $nop = splitnop(onlyNumber($request->nop));
        // dd($nop);

        try {
            DB::beginTransaction();
            $hasil = DB::connection('oracle_satutujuh')->table('sppt')
                ->wherein('thn_pajak_sppt', $request->thn_pajak_sppt)
                ->where($nop)
                ->update([
                    'tgl_jatuh_tempo_sppt' => new Carbon($request->tg_jatuh_tempo_sppt)
                ]);

            DB::commit();
            $pesan = ['success' => "Tanggal jatuh tempo berhasil d proses"];
        } catch (\Exception $e) {

            DB::rollback();
            $pesan = ['error' => $e->getMessage()];
        }
        return redirect(url('refrensi/jatuhtempo'))->with($pesan);
    }
}
