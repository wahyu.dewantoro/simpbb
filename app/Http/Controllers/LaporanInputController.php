<?php

namespace App\Http\Controllers;

use App\Exports\LaporanInputPermohonan;
use Illuminate\Http\Request;
use App\Models\Jenis_layanan;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use App\Models\Layanan_dokumen;
use App\Helpers\gallade;
use App\Helpers\Layanan_conf;
use App\Helpers\LayananUpdate;
use App\Models\JenisPengurangan;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use SnappyPdf as PDF;
use App\pegawaiStruktural;
use App\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class LaporanInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $tahun_pajak = [];
        $tahun =  date('Y');
        $start = $tahun - 5;
        for ($year = $tahun; $year >= $start; $year--) {
            $tahun_pajak[$year] = $year;
        }
        $Jenis_layanan = Jenis_layanan::all();
        $Jenis_layanan->push((object)['id' => '8online', 'nama_layanan' => 'Pembetulan Online']);
        $jenisDokumen = [
            'Seluruh Bekas' => 'Seluruh Bekas',
            'No. IMB' => 'No. IMB',
            'No. Sertifikat' => 'No. Sertifikat',
            'Akta Jual Beli' => 'Akta Jual Beli',
            'Gambar' => 'Gambar',
            'Perjanjian Sewa Menyewa' => 'Perjanjian Sewa Menyewa',
            'SPPT' => 'SPPT',
            'No. Akta' => 'No. Akta',
            'No. SBU' => 'No. SBU',
            'No. Kemenhun' => 'No. Kemenhun',
            'Surat Keterangan Lurah' => 'Surat Keterangan Lurah',
            'Lain-lain' => 'Lain-lain'
        ];
        return view('layanan.laporan-input', compact('kecamatan', 'Jenis_layanan', 'kelurahan', 'tahun_pajak', 'jenisDokumen'));
    }

    public function search(Request $request)
    {
        $search = $request->only(['status', 'layanan', 'jenis_objek', 'tgl', 'all', 'nik_nama_wp', 'nop']);
        $group = [
            'layanan.nomor_layanan',
            'layanan.jenis_layanan_id',
            'layanan.jenis_layanan_nama',
            'layanan.nama',
            'layanan.keterangan',
            'layanan.created_at',
            'layanan.kd_status',
            'layanan.jenis_objek',
            'jenis_pengurangan.jenis',
            'jenis_pengurangan.nama_pengurangan'
        ];
        $roles = Auth()->user()->roles->pluck('name');
        $select = array_merge($group, [db::raw("sum(CASE WHEN layanan_objek.pemutakhiran_at IS NULL THEN 0 ELSE 1 END) pemutakhiran_count,
        sum(CASE WHEN layanan_objek.is_tolak = '1' THEN 1 ELSE 0 END) tolak_count,
        sum(CASE WHEN layanan_objek.id IS NULL THEN 0 ELSE 1 END) objek_count,
        sum(CASE WHEN layanan_objek.id IS NOT NULL AND layanan.jenis_layanan_id ='6' THEN 1 ELSE 0 END) pecah_count,
   	    sum(CASE WHEN layanan_objek.id IS NOT NULL AND layanan.jenis_layanan_id ='7' and layanan_objek.nop_gabung is null  THEN 1 ELSE 0 END) gabung_count")]);

        if ((!in_array('Super User', $roles->toArray())   && !in_array('UPT PELAYANAN PAJAK DAERAH (UPT)', $roles->toArray())  && !in_array('Staft Penelitian', $roles->toArray()) && !in_array('Staff Pelayanan', $roles->toArray()) && !in_array('Kasi Pelayanan', $roles->toArray()) && !in_array('UNIT PELAKSANA TEKNIS (UPT) PPD', $roles->toArray())) && $roles->count() == 1) {
            $select = array_merge($select, [db::raw(" '1' userdesa")]);
        }
        $layanan = Layanan::select($select);

        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {

            $layanan->where([
                'created_by' => Auth()->user()->id,
            ]);
        }
        if ($search) {
            if ($search['status'] != '-') {

                if (in_array($search['status'], ['x'])) {
                    $layanan->whereRaw("layanan_objek.is_tolak = '1' and (layanan_objek.pemutakhiran_at is not  null and kd_status!='1')");
                } elseif (in_array($search['status'], ['1'])) {
                    $layanan->whereRaw("(kd_status = '1'
                    OR (layanan_objek.pemutakhiran_at IS NOT NULL
                        AND ((layanan.jenis_layanan_id ='7' 
                            and layanan_objek.nop_gabung is null
                            and layanan_objek.is_tolak is null)
                            or 
                            (layanan.jenis_layanan_id ='6' 
                            and layanan_objek.is_tolak is null
                            AND layanan.NOMOR_LAYANAN NOT IN (
                                SELECT DISTINCT layanan_objek.NOMOR_LAYANAN 
                                FROM layanan_objek
                                WHERE layanan_objek.PEMUTAKHIRAN_AT IS NULL OR layanan_objek.IS_TOLAK IS NOT null)
                            )
                            or (layanan.jenis_layanan_id not in ('6','7') 
                                AND layanan_objek.is_tolak IS NULL
                                AND layanan.NOMOR_LAYANAN NOT IN (
                                    SELECT
                                        DISTINCT layanan_objek.NOMOR_LAYANAN
                                    FROM
                                        layanan_objek
                                    WHERE
                                        layanan_objek.PEMUTAKHIRAN_AT IS NULL
                                        OR layanan_objek.IS_TOLAK IS NOT NULL))
                            ) ))");
                } else {
                    $layanan->whereRaw("kd_status='" . $search['status'] . "'");
                }
                if (in_array($search['status'], ['0', '2'])) {
                    $layanan->whereRaw("layanan_objek.pemutakhiran_at is null");
                    if (in_array($search['status'], ['0'])) {
                        $layanan->whereRaw(" (
                            (layanan.jenis_layanan_id ='7' 
                            and layanan_objek.nop_gabung is null
                            and layanan_objek.is_tolak is null)
                            or 
                            (layanan.jenis_layanan_id ='6' 
                            and layanan_objek.is_tolak is null)
                            or layanan.jenis_layanan_id not in ('6','7')
                        ) ");
                    }
                }
            }
            if ($search['jenis_objek'] != '-') {
                $layanan->where('jenis_objek', $search['jenis_objek']);
            }
            if ($search['tgl'] != '-' && !in_array('all', array_keys($search))) {
                $tgl = explode('-', str_replace(' ', '', $search['tgl']));
                $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
                $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
                if ($after != $before) {
                    $start = Carbon::createFromFormat('m/d/Y', $tgl[0])->startOfDay()->format('YmdHi');
                    $end = Carbon::createFromFormat('m/d/Y', $tgl[1])->endOfDay()->format('YmdHi');
                    $layanan->whereraw("Layanan.created_at between to_date('" . $start . "','yyyymmddhh24mi')  and to_date('" . $end . "','yyyymmddhh24mi')");
                } else {
                    $layanan->whereraw("to_char(Layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
                }
            }
            if ($search['layanan'] != '-') {
                if ($search['layanan'] == '8online') {
                    $layanan->where('jenis_layanan_id', '8')
                        ->whereRaw("jenis_layanan_nama like '%(online)%'");
                } else {
                    $layanan->where('jenis_layanan_id', $search['layanan']);
                }
                if ($search['layanan'] == '8') {
                    $layanan->whereRaw("jenis_layanan_nama not like '%(online)%'");
                }
            }
            if ($search['nik_nama_wp'] && $search['nik_nama_wp'] != '-') {
                $layanan->whereRaw("(layanan_objek.nama_wp like '%" . $search['nik_nama_wp'] . "%' or layanan_objek.nik_wp like '%" . $search['nik_nama_wp'] . "%') ");
            }
            if ($search['nop'] && $search['nop'] != '-') {
                $nopS = explode(".", $search['nop']);
                $blok_urutS = explode("-", $nopS[4]);
                $str = "(layanan_objek.kd_propinsi='" . $nopS[0] . "'
                AND layanan_objek.kd_dati2='" . $nopS[1] . "'
                AND layanan_objek.kd_kecamatan='" . $nopS[2] . "'
                AND layanan_objek.kd_kelurahan='" . $nopS[3] . "'
                AND layanan_objek.kd_blok='" . $blok_urutS[0] . "'
                AND layanan_objek.no_urut='" . $blok_urutS[1] . "'
                AND layanan_objek.kd_jns_op='" . $nopS[5] . "')";
                $layanan->whereRaw($str);
            }
        } else {
            $now = Carbon::now()->format('d/m/Y');
            $layanan->whereraw("to_char(Layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
        }
        $dataSet = $layanan->leftJoin('layanan_objek', function ($join) {
            $join->On('layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan');
            // ->whereRaw('layanan_objek.pemutakhiran_at is not null');
        })
            ->join('jenis_pengurangan', 'jenis_pengurangan.id', "=", "layanan_objek.jenis_pengurangan", 'left outer')
            ->whereRaw("Layanan.jenis_objek!='4'")
            ->orderBy('Layanan.created_at', 'desc')
            ->groupBy($group);

        $datatables = Datatables::of($dataSet);
        $roles = $roles->toArray();
        return $datatables->addColumn('option', function ($data) use ($roles) {

            $btnDelete = "";
            $btnUpdate = "";
            if (($data->pemutakhiran_count == "0" || $data->pemutakhiran_count == '') && !isset($data->userdesa) && $data->kd_status != '1') {
                $btnDelete = gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Delete" data-delete="' . $data->nomor_layanan . '"', 'danger');
            }
            if (in_array('Super User', $roles)) {
                $btnDelete = gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Delete" data-delete="' . $data->nomor_layanan . '"', 'danger');
            }
            if (in_array($data->jenis_objek, ['3', '5']) && $data->kd_status == '2' && !isset($data->userdesa)) {
                $btnUpdate = gallade::buttonInfo('<i class="fas fa-file-signature"></i>', 'title="Update Berkas" data-update="' . $data->nomor_layanan . '"', 'primary') .
                    gallade::buttonInfo('<i class="fas fa-times"></i>', 'title="Tolak Berkas" data-tolak="' . $data->nomor_layanan . '"', 'danger');
            }
            $btnPdf = "";
            //'.$data->pemutakhiran_at.'-'.$data->kd_status.'
            $btnTandaterima = gallade::anchorInfo(
                'laporan_input/tanda_terima?nomor_layanan=' . $data->nomor_layanan,
                '<i class="fas fa-file-pdf"></i>',
                'title="Cetak Tanda Terima" data-pdf="true"',
                '',
                'primary'
            );
            if ($data->jenis_objek == '3' && $data->kd_status == '2') {
                $btnTandaterima = "";
            }
            $btnCBK = "";
            if ($data->jenis_objek == '3') {
                $btnCBK = gallade::anchorInfo(
                    'laporan_input/cetak_berkas_kolektif?nomor_layanan=' . $data->nomor_layanan,
                    '<i class="fas fa-file-pdf"></i>',
                    'title="Cetak Berkas Kolektif" data-pdf="true"',
                    '',
                    'warning'
                );
            }
            if ($data->jenis_layanan_id == '8' && $data->kd_status == '1') {
                $btnPdf = gallade::anchorInfo(
                    'laporan_input/cetak_pdf?nomor_layanan=' . $data->nomor_layanan,
                    '<i class="fas fa-file-pdf"></i>',
                    'title="Cetak Pdf" data-pdf="true"'
                );
            }
            if ($data->jenis_layanan_id == '5') {
                if ($data->jenis == '0' && $data->kd_status == '1') {
                    $btnPdf = gallade::anchorInfo(
                        'penelitian/sk?nomor_layanan=' . $data->nomor_layanan,
                        '<i class="fas fa-file-pdf"></i> SK',
                        'title="Cetak SK-1" data-pdf="true"',
                        '',
                        'warning'
                    );
                }
                if ($data->jenis == '1' && $data->pemutakhiran_count > 0) {
                    $btnPdf = gallade::anchorInfo(
                        'penelitian/sk?nomor_layanan=' . $data->nomor_layanan,
                        '<i class="fas fa-file-pdf"></i> SK',
                        'title="Cetak SK-2" data-pdf="true"',
                        '',
                        'warning'
                    );
                }
            }
            return '<div class="btn-group">' . $btnDelete . $btnUpdate . $btnPdf . $btnTandaterima . $btnCBK . "</div>";
        })
            ->editColumn('jenis_layanan_nama', function ($data) {
                $nama = $data->jenis_layanan_nama;
                if ($data->jenis_layanan_id == '5') {
                    $nama = $nama . " [" . $data->nama_pengurangan . "]";
                }
                return $nama;
            })
            ->editColumn('nomor_layanan', function ($data) {
                $listObjek = ['1' => 'Pribadi', '2' => 'Badan', '3' => 'Kolektif', '4' => 'Pendataaan', '5' => 'Permohonan BPHTB'];
                $jenisObjek = '--';
                if (in_array($data->jenis_objek, array_keys($listObjek))) {
                    $jenisObjek = $listObjek[$data->jenis_objek];
                }
                return '<p class="p-0 m-0">' . $data->nomor_layanan . '</p><i class="text-info">' . $jenisObjek . '</i>';
            })
            ->editColumn('created_at', function ($data) {
                $his = date('H:i:s', strtotime($data->created_at));
                return '<p class="p-0 m-0">' . tglIndo($data->created_at) . '</p><i>' . $his . '</i>';
            })
            ->editColumn('kd_status', function ($data) {
                $countData = $data->objek_count;
                if ($data->jenis_layanan_id == '6') {
                    $countData = $data->pecah_count;
                }
                if ($data->jenis_layanan_id == '7') {
                    $countData = $data->gabung_count;
                }
                if ($data->pemutakhiran_count >= $countData && $data->kd_status != '1' && $data->jenis_layanan_id != '8') {
                    $data->kd_status = '1';
                }
                $status = [
                    // '0'=>"<span class='badge badge-warning '>Verifikai</span>",
                    '0' => "<span class='badge badge-warning '>Penelitian</span>",
                    '1' => "<span class='badge badge-success '>Selesai</span>",
                    '2' => "<span class='badge badge-warning '>Berkas belum Masuk</span>",
                    '3' => "<span class='badge badge-danger '>Berkas ditolak </span>",
                    // '3'=>"<span class='badge badge-primary '>Penelitian</span>",
                    // '3'=>"<span class='badge badge-primary '>Selesai</span>",
                ];
                $setstatus = "<span class='badge badge-warning '>--</span>";
                if (in_array($data->kd_status, array_keys($status))) {
                    $setstatus = $status[$data->kd_status];
                }
                if ($data->tolak_count > 0) {
                    if ($data->jenis_objek == '3') {
                        $setstatus .= "<span class='badge badge-danger '>[" . $data->tolak_count . "] Ajuan di tolak</span>";
                    } else {
                        $setstatus = "<span class='badge badge-danger '> Ajuan di tolak</span>";
                    }
                }
                return $setstatus;
            })
            ->addIndexColumn()
            ->removeColumn(['penagihan_id'])
            ->make(true);
    }

    public function export(Request $request)
    {
        try {
            $search = $request->only(['status', 'layanan', 'jenis_objek', 'tgl', 'all', 'nik_nama_wp', 'nop']);
            $group = [
                'layanan.nomor_layanan',
                'layanan.jenis_layanan_id',
                'layanan.jenis_layanan_nama',
                'layanan.nama',
                'layanan.keterangan',
                'layanan.created_at',
                'layanan.kd_status',
                'layanan.jenis_objek',
                'jenis_pengurangan.jenis',
                'jenis_pengurangan.nama_pengurangan'
            ];
            $roles = Auth()->user()->roles->pluck('name');
            $select = array_merge($group, [db::raw("sum(CASE WHEN layanan_objek.pemutakhiran_at IS NULL THEN 0 ELSE 1 END) pemutakhiran_count,
            sum(CASE WHEN layanan_objek.is_tolak = '1' THEN 1 ELSE 0 END) tolak_count,
            sum(CASE WHEN layanan_objek.id IS NULL THEN 0 ELSE 1 END) objek_count,
            sum(CASE WHEN layanan_objek.id IS NOT NULL AND layanan.jenis_layanan_id ='6' THEN 1 ELSE 0 END) pecah_count,
   	        sum(CASE WHEN layanan_objek.id IS NOT NULL AND layanan.jenis_layanan_id ='7' and layanan_objek.nop_gabung is null  THEN 1 ELSE 0 END) gabung_count")]);
            // if(!in_array('Staff Pelayanan',$roles->toArray())&&$roles->count()==1){
            //     $select=array_merge($select,[db::raw(" '1' userdesa")]);
            // }

            if ((!in_array('Super User', $roles->toArray()) && !in_array('PTSL SELOREJ0', $roles->toArray()) && !in_array('PELAYANAN DAN PENELITIAN  BPHTB', $roles->toArray()) && !in_array('Staff Pelayanan', $roles->toArray())) && $roles->count() == 1) {
                $select = array_merge($select, [db::raw(" '1' userdesa")]);
            }
            $layanan = Layanan::select($select);

            $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
            $kelurahan = [];
            if ($kecamatan->count() == '1') {
                // $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan)
                //         ->login()
                //         ->orderBy('nm_kelurahan','asc')
                //         ->select('nm_kelurahan', 'kd_kelurahan')
                //         ->get();
                $layanan->where([
                    'created_by' => Auth()->user()->id,
                ]);
            }
            if ($search) {
                if ($search['status'] != '-') {
                    if (in_array($search['status'], ['x'])) {
                        $layanan->whereRaw("layanan_objek.is_tolak = '1' and (layanan_objek.pemutakhiran_at is not  null and kd_status!='1')");
                    }
                    if (in_array($search['status'], ['1'])) {
                        $layanan->whereRaw("(kd_status = '1'
                        OR (layanan_objek.pemutakhiran_at IS NOT NULL
                            AND ((layanan.jenis_layanan_id ='7' 
                                and layanan_objek.nop_gabung is null
                                and layanan_objek.is_tolak is null)
                                or 
                                (layanan.jenis_layanan_id ='6' 
                                and layanan_objek.is_tolak is null
                                AND layanan.NOMOR_LAYANAN NOT IN (
                                    SELECT DISTINCT layanan_objek.NOMOR_LAYANAN 
                                    FROM layanan_objek
                                    WHERE layanan_objek.PEMUTAKHIRAN_AT IS NULL OR layanan_objek.IS_TOLAK IS NOT null)
                                )
                                or (layanan.jenis_layanan_id not in ('6','7') 
                                    AND layanan_objek.is_tolak IS NULL
                                    AND layanan.NOMOR_LAYANAN NOT IN (
                                        SELECT
                                            DISTINCT layanan_objek.NOMOR_LAYANAN
                                        FROM
                                            layanan_objek
                                        WHERE
                                            layanan_objek.PEMUTAKHIRAN_AT IS NULL
                                            OR layanan_objek.IS_TOLAK IS NOT NULL))
                                ) ))");
                    } else {
                        $layanan->whereRaw("kd_status='" . $search['status'] . "'");
                    }
                    if (in_array($search['status'], ['0', '2'])) {
                        $layanan->whereRaw("layanan_objek.pemutakhiran_at is null");
                        if (in_array($search['status'], ['0'])) {
                            $layanan->whereRaw(" (
                                (layanan.jenis_layanan_id ='7' 
                                and layanan_objek.nop_gabung is null
                                and layanan_objek.is_tolak is null)
                                or 
                                (layanan.jenis_layanan_id ='6' 
                                and layanan_objek.is_tolak is null)
                                or layanan.jenis_layanan_id not in ('6','7')
                            ) ");
                            // and layanan_objek.nop_gabung is not null
                        }
                    }
                }
                if ($search['jenis_objek'] != '-') {
                    $layanan->where('jenis_objek', $search['jenis_objek']);
                }
                if ($search['tgl'] != '-' && !in_array('all', array_keys($search))) {
                    $tgl = explode('-', str_replace(' ', '', $search['tgl']));
                    $after = Carbon::createFromFormat('m/d/Y', $tgl[0])->format('d/m/Y');
                    $before = Carbon::createFromFormat('m/d/Y', $tgl[1])->format('d/m/Y');
                    if ($after != $before) {
                        $start = Carbon::createFromFormat('m/d/Y', $tgl[0])->startOfDay()->format('YmdHi');
                        $end = Carbon::createFromFormat('m/d/Y', $tgl[1])->endOfDay()->format('YmdHi');
                        $layanan->whereraw("Layanan.created_at between to_date('" . $start . "','yyyymmddhh24mi')  and to_date('" . $end . "','yyyymmddhh24mi')");
                    } else {
                        $layanan->whereraw("to_char(Layanan.created_at,'dd/mm/yyyy')='" . $after . "'");
                    }
                }
                if ($search['layanan'] != '-') {
                    if ($search['layanan'] == '8online') {
                        $layanan->where('jenis_layanan_id', '8')
                            ->whereRaw("jenis_layanan_nama like '%(online)%'");
                    } else {
                        $layanan->where('jenis_layanan_id', $search['layanan']);
                    }
                    if ($search['layanan'] == '8') {
                        $layanan->whereRaw("jenis_layanan_nama not like '%(online)%'");
                    }
                }
                if ($search['nik_nama_wp'] && $search['nik_nama_wp'] != '-') {
                    $layanan->whereRaw("(layanan_objek.nama_wp like '%" . $search['nik_nama_wp'] . "%' or layanan_objek.nik_wp like '%" . $search['nik_nama_wp'] . "%') ");
                }
                if ($search['nop'] && $search['nop'] != '-') {
                    $nopS = explode(".", $search['nop']);
                    $blok_urutS = explode("-", $nopS[4]);
                    $str = "(layanan_objek.kd_propinsi='" . $nopS[0] . "'
                    AND layanan_objek.kd_dati2='" . $nopS[1] . "'
                    AND layanan_objek.kd_kecamatan='" . $nopS[2] . "'
                    AND layanan_objek.kd_kelurahan='" . $nopS[3] . "'
                    AND layanan_objek.kd_blok='" . $blok_urutS[0] . "'
                    AND layanan_objek.no_urut='" . $blok_urutS[1] . "'
                    AND layanan_objek.kd_jns_op='" . $nopS[5] . "')";
                    $layanan->whereRaw($str);
                }
            } else {
                $now = Carbon::now()->format('d/m/Y');
                $layanan->whereraw("to_char(Layanan.created_at,'dd/mm/yyyy')='" . $now . "'");
            }
            $dataSet = $layanan->leftJoin('layanan_objek', function ($join) {
                $join->On('layanan_objek.nomor_layanan', '=', 'layanan.nomor_layanan');
                // ->whereRaw('layanan_objek.pemutakhiran_at is not null');
            })
                ->join('jenis_pengurangan', 'jenis_pengurangan.id', "=", "layanan_objek.jenis_pengurangan", 'left outer')
                ->whereRaw("Layanan.jenis_objek!='4'")
                ->orderBy('Layanan.created_at', 'desc')
                ->groupBy($group)
                ->get();
            $export = new LaporanInputPermohonan($dataSet->toArray());
            return Excel::download($export, 'daftar_Permohonan.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    public function cetak_list_kolektif($findNop = false, $data = [], $result = false)
    {
        $pages = [];
        if (count($data) > 0) {
            $getLayanan = $data;
            $getObjek = collect($getLayanan['objek'])->sortBy('id')->toArray();
            $firstObjek = current($getObjek);
            $jenisLayanan = Jenis_layanan::where(['id' => $getLayanan['jenis_layanan_id']])->first()->toArray();
            $page = gallade::clean($jenisLayanan['nama_layanan']);
            $getkelkecOP = DB::table(db::raw("spo.ref_kelurahan ref_kelurahan"))
                ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                    $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
                })
                ->where(['ref_kelurahan.kd_kecamatan' => $firstObjek['kd_kecamatan']])
                ->where(['ref_kelurahan.kd_kelurahan' => $firstObjek['kd_kelurahan']])
                ->get()->first();
            $listObjek = [];
            // ['1', '2','3','6','7','9']
            // mutasi gabung
            // dd($getObjek);
            if (in_array($getLayanan['jenis_layanan_id'], ['7'])) {
                $dataHasilGabung = ['nop' => '', 'nama' => '', 'luas_bumi' => '0', 'luas_bng' => '0'];
                $no = 1;
                $totalGabungTanah = 0;
                $totalGabungBng = 0;
                foreach ($getObjek as $item) {
                    if ($item['nop_gabung'] == "" || $item['nop_gabung'] == null) {
                        $dataHasilGabung = [
                            'nop' => $item['kd_propinsi'] . "." .
                                $item['kd_dati2'] . "." .
                                $item['kd_kecamatan'] . "." .
                                $item['kd_kelurahan'] . "." .
                                $item['kd_blok'] . "-" .
                                $item['no_urut'] . "." .
                                $item['kd_jns_op'],
                            'nama' => $item['nama_wp'],
                            'luas_bumi' => $item['luas_bumi'],
                            'luas_bng' => $item['luas_bng'],
                            'nik_wp' => $item['nik_wp'],
                            'alamat_wp' => $item['alamat_wp'],
                            'rt_wp' => $item['rt_wp'],
                            'rw_wp' => $item['rw_wp'],
                            'kelurahan_wp' => $item['kelurahan_wp'],
                            'kecamatan_wp' => $item['kecamatan_wp'],
                            'dati2_wp' => $item['dati2_wp'],
                            'propinsi_wp' => $item['propinsi_wp'],
                            'telp_wp' => $item['telp_wp'],
                            'alamat_op' => $item['alamat_op'],
                            'rt_op' => $item['rt_op'],
                            'rw_op' => $item['rw_op'],
                        ];
                        $totalGabungTanah = 0;
                    } else {
                        $kelurahan_wp = explode('-', $item['kelurahan_wp']);
                        if (count($kelurahan_wp) == '2') {
                            $item['kecamatan_wp'] = $kelurahan_wp[1];
                            $item['kelurahan_wp'] = $kelurahan_wp[0];
                        }
                        $dati2_wp = explode('-', $item['dati2_wp']);
                        if (count($dati2_wp) == '2') {
                            $item['propinsi_wp'] = $dati2_wp[1];
                            $item['dati2_wp'] = $dati2_wp[0];
                        }
                        $item['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_op']));
                        $item['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_op']));
                        $item['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                        $item['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));
                        // dd($item);
                        $totalGabungTanah = $totalGabungTanah + $item['luas_bumi'];
                        $totalGabungBng = $totalGabungBng + $item['luas_bng'];
                        $listObjek[] = [
                            'no' => $no,
                            'nop_sebelum' => $item['kd_propinsi'] . "." .
                                $item['kd_dati2'] . "." .
                                $item['kd_kecamatan'] . "." .
                                $item['kd_kelurahan'] . "." .
                                $item['kd_blok'] . "-" .
                                $item['no_urut'] . "." .
                                $item['kd_jns_op'],
                            "nop" => $dataHasilGabung['nop'],
                            'nama_sebelum' => $item['nama_wp'],
                            'nama_wp' => $dataHasilGabung['nama'],
                            'nik_wp' => $dataHasilGabung['nik_wp'],
                            'alamat_wp' => $dataHasilGabung['alamat_wp'],
                            'rt_rw_wp' => $dataHasilGabung['rt_wp'] . '/' . $dataHasilGabung['rw_wp'],
                            'kelurahan_wp' => $dataHasilGabung['kelurahan_wp'],
                            'kecamatan_wp' => $dataHasilGabung['kecamatan_wp'],
                            'dati2_wp' => $dataHasilGabung['dati2_wp'],
                            'propinsi_wp' => $dataHasilGabung['propinsi_wp'],
                            'notelp_wp' => $dataHasilGabung['telp_wp'],
                            'alamat_op' => $dataHasilGabung['alamat_op'],
                            'rt_rw_op' => $dataHasilGabung['rt_op'] . '/' . $dataHasilGabung['rw_op'],
                            'kelurahan_op' => $getkelkecOP->nm_kelurahan,
                            'kecamatan_op' => $getkelkecOP->nm_kecamatan,
                            'luas_bumi_sebelum' => $item['luas_bumi'],
                            'luas_bng_sebelum' => $item['luas_bng'],
                            'luas_bumi_setelah' => $totalGabungTanah,
                            'luas_bng_setelah' => $totalGabungBng,
                            'luas_bumi_ob' => $item['luas_bumi'],
                            'luas_bng_ob' => $item['luas_bng'],
                        ];
                        //$dataHasilGabung['nop']='';
                        //$dataHasilGabung['nama']='';
                        $no++;
                    }
                }
            }
            if (in_array($getLayanan['jenis_layanan_id'], ['6'])) {
                $dataHasilGabung = ['nop' => '', 'nama' => '', 'luas_bumi' => '0', 'luas_bng' => '0'];
                $no = 1;
                foreach ($getObjek as $item) {
                    if ($item['nop_gabung'] == "" || $item['nop_gabung'] == null) {
                        $dataHasilGabung = [
                            'nop' => $item['kd_propinsi'] . "." .
                                $item['kd_dati2'] . "." .
                                $item['kd_kecamatan'] . "." .
                                $item['kd_kelurahan'] . "." .
                                $item['kd_blok'] . "-" .
                                $item['no_urut'] . "." .
                                $item['kd_jns_op'],
                            'nama' => $item['nama_wp'],
                            'luas_bumi' => $item['luas_bumi'],
                            'luas_bng' => $item['luas_bng']
                        ];
                        $kelurahan_wp = explode('-', $item['kelurahan_wp']);
                        if (count($kelurahan_wp) == '2') {
                            $item['kecamatan_wp'] = $kelurahan_wp[1];
                            $item['kelurahan_wp'] = $kelurahan_wp[0];
                        }
                        $dati2_wp = explode('-', $item['dati2_wp']);
                        if (count($dati2_wp) == '2') {
                            $item['propinsi_wp'] = $dati2_wp[1];
                            $item['dati2_wp'] = $dati2_wp[0];
                        }
                        $item['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_op']));
                        $item['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_op']));
                        $item['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                        $item['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));
                        $listObjek[] = [
                            'no' => $no,
                            'nop_sebelum' => $dataHasilGabung['nop'],
                            'nama_sebelum' => $dataHasilGabung['nama'],
                            'nama_wp' => '',
                            'nik_wp' => '',
                            'alamat_wp' => '',
                            'rt_rw_wp' => '',
                            'kelurahan_wp' => '',
                            'kecamatan_wp' => '',
                            'dati2_wp' => '',
                            'propinsi_wp' => '',
                            'notelp_wp' => '',
                            'alamat_op' => '',
                            'rt_rw_op' => '',
                            'kelurahan_op' => '',
                            'kecamatan_op' => '',
                            'luas_bumi_sebelum' => $item['luas_bumi'],
                            'luas_bng_sebelum' => $item['luas_bng'],
                            'luas_bumi_setelah' => $item['sisa_pecah_total_gabung'],
                            'luas_bng_setelah' => $item['luas_bng'],
                            'luas_bumi_ob' => $item['hasil_pecah_hasil_gabung'],
                            'luas_bng_ob' => $item['luas_bng'],
                        ];
                    } else {
                        $kelurahan_wp = explode('-', $item['kelurahan_wp']);
                        if (count($kelurahan_wp) == '2') {
                            $item['kecamatan_wp'] = $kelurahan_wp[1];
                            $item['kelurahan_wp'] = $kelurahan_wp[0];
                        }
                        $dati2_wp = explode('-', $item['dati2_wp']);
                        if (count($dati2_wp) == '2') {
                            $item['propinsi_wp'] = $dati2_wp[1];
                            $item['dati2_wp'] = $dati2_wp[0];
                        }
                        $item['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_op']));
                        $item['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_op']));
                        $item['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                        $item['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));
                        $listObjek[] = [
                            'no' => $no,
                            'nop_sebelum' => $dataHasilGabung['nop'],
                            'nama_sebelum' => $dataHasilGabung['nama'],
                            'nama_wp' => $item['nama_wp'],
                            'nik_wp' => $item['nik_wp'],
                            'alamat_wp' => $item['alamat_wp'],
                            'rt_rw_wp' => $item['rt_wp'] . '/' . $item['rw_wp'],
                            'kelurahan_wp' => $item['kelurahan_wp'],
                            'kecamatan_wp' => $item['kecamatan_wp'],
                            'dati2_wp' => $item['dati2_wp'],
                            'propinsi_wp' => $item['propinsi_wp'],
                            'notelp_wp' => $item['telp_wp'],
                            'alamat_op' => $item['alamat_op'],
                            'rt_rw_op' => $item['rt_op'] . '/' . $item['rw_op'],
                            'kelurahan_op' => $getkelkecOP->nm_kelurahan,
                            'kecamatan_op' => $getkelkecOP->nm_kecamatan,
                            'luas_bumi_sebelum' => $dataHasilGabung['luas_bumi'],
                            'luas_bng_sebelum' => $dataHasilGabung['luas_bng'],
                            'luas_bumi_setelah' => $dataHasilGabung['luas_bumi'] + $item['luas_bumi'],
                            'luas_bng_setelah' => $dataHasilGabung['luas_bng'] + $item['luas_bng'],
                            'luas_bumi_ob' => $item['luas_bumi'],
                            'luas_bng_ob' => $item['luas_bng'],
                        ];
                        $dataHasilGabung['luas_bumi'] = $dataHasilGabung['luas_bumi'] - $item['luas_bumi'];
                        $dataHasilGabung['luas_bng'] = $dataHasilGabung['luas_bng'] - $item['luas_bng'];
                        //$dataHasilGabung['nop']='';
                        //$dataHasilGabung['nama']='';
                    }
                    $no++;
                }
            }
            if (in_array($getLayanan['jenis_layanan_id'], ['1', '3', '9'])) {
                $no = 1;
                foreach ($getObjek as $item) {
                    $nop = "";
                    if ($getLayanan['jenis_layanan_id'] != '1') {
                        $nop = $item['kd_propinsi'] . "." .
                            $item['kd_dati2'] . "." .
                            $item['kd_kecamatan'] . "." .
                            $item['kd_kelurahan'] . "." .
                            $item['kd_blok'] . "-" .
                            $item['no_urut'] . "." .
                            $item['kd_jns_op'];
                    }
                    $kelurahan_wp = explode('-', $item['kelurahan_wp']);
                    if (count($kelurahan_wp) == '2') {
                        $item['kecamatan_wp'] = $kelurahan_wp[1];
                        $item['kelurahan_wp'] = $kelurahan_wp[0];
                    }
                    $dati2_wp = explode('-', $item['dati2_wp']);
                    if (count($dati2_wp) == '2') {
                        $item['propinsi_wp'] = $dati2_wp[1];
                        $item['dati2_wp'] = $dati2_wp[0];
                    }
                    $item['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_op']));
                    $item['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_op']));
                    $item['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                    $item['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));
                    $listObjek[] = [
                        'no' => $no,
                        'nop' => $nop,
                        'nik_wp' => $item['nik_wp'],
                        'nama_wp' => $item['nama_wp'],
                        'alamat_wp' => $item['alamat_wp'],
                        'rt_rw_wp' => $item['rt_wp'] . '/' . $item['rw_wp'],
                        'kelurahan_wp' => $item['kelurahan_wp'],
                        'kecamatan_wp' => $item['kecamatan_wp'],
                        'dati2_wp' => $item['dati2_wp'],
                        'propinsi_wp' => $item['propinsi_wp'],
                        'notelp_wp' => $item['telp_wp'],
                        'alamat_op' => $item['alamat_op'],
                        'rt_rw_op' => $item['rt_op'] . '/' . $item['rw_op'],
                        'kelurahan_op' => $getkelkecOP->nm_kelurahan,
                        'kecamatan_op' => $getkelkecOP->nm_kecamatan,
                        'luas_bumi' => $item['luas_bumi'],
                        'luas_bng' => $item['luas_bng'],
                    ];
                    $no++;
                }
            }
            if ($getLayanan['jenis_layanan_id'] == '2') {
                $no = 1;
                foreach ($getObjek as $item) {
                    $kelurahan_wp = explode('-', $item['kelurahan_wp']);
                    if (count($kelurahan_wp) == '2') {
                        $item['kecamatan_wp'] = $kelurahan_wp[1];
                        $item['kelurahan_wp'] = $kelurahan_wp[0];
                    }
                    $dati2_wp = explode('-', $item['dati2_wp']);
                    if (count($dati2_wp) == '2') {
                        $item['propinsi_wp'] = $dati2_wp[1];
                        $item['dati2_wp'] = $dati2_wp[0];
                    }
                    $item['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_op']));
                    $item['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_op']));
                    $item['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                    $item['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));
                    $nop = $item['kd_propinsi'] . "." .
                        $item['kd_dati2'] . "." .
                        $item['kd_kecamatan'] . "." .
                        $item['kd_kelurahan'] . "." .
                        $item['kd_blok'] . "-" .
                        $item['no_urut'] . "." .
                        $item['kd_jns_op'];
                    $listObjek[] = [
                        'no' => $no,
                        'nop' => $nop,
                        'nik_wp' => $item['nik_wp'],
                        'nama_wp' => $item['nama_wp'],
                        'alamat_wp' => $item['alamat_wp'],
                        'rt_rw_wp' => $item['rt_wp'] . '/' . $item['rw_wp'],
                        'kelurahan_wp' => $item['kelurahan_wp'],
                        'kecamatan_wp' => $item['kecamatan_wp'],
                        'dati2_wp' => $item['dati2_wp'],
                        'propinsi_wp' => $item['propinsi_wp'],
                        'notelp_wp' => $item['telp_wp'],
                        'alamat_op' => $item['alamat_op'],
                        'rt_rw_op' => $item['rt_op'] . '/' . $item['rw_op'],
                        'kelurahan_op' => $getkelkecOP->nm_kelurahan,
                        'kecamatan_op' => $getkelkecOP->nm_kecamatan,
                        'luas_bumi' => $item['luas_bumi'],
                        'luas_bng' => $item['luas_bng'],
                    ];
                    $no++;
                }
            }
            // dd($getObjek);
            $permohonanKolektif = [
                'kelurahan' => $getkelkecOP->nm_kelurahan,
                'kecamatan' => $getkelkecOP->nm_kecamatan,
                'data' => $listObjek,
                'tgl' => tglIndo($getLayanan['tanggal_layanan']),
                'nama_layanan' => $jenisLayanan['nama_layanan'],
                'nomor_layanan' => $getLayanan['nomor_layanan']
            ];
            //  return view('layanan.cetak.kolektif.'.$page, compact('permohonanKolektif'));
            $pages = View::make('layanan.cetak.kolektif.' . $page, compact('permohonanKolektif'));
            if ($result) {
                return $pages;
            }
        }
        if ($findNop && is_array($findNop)) {
            foreach ($findNop as $setNop) {
                $layanan = Layanan::where(['nomor_layanan' => $setNop])->get();
                if ($layanan->count() > 0) {
                    $getLayanan = $layanan->first()->toArray();
                    $getLayanan['objek'] = Layanan_objek::where(['nomor_layanan' => $setNop])->get()->toArray();
                    $getLayanan['layanan_dokumen'] = Layanan_dokumen::where(['nomor_layanan' => $setNop])->get()->toArray();
                    if ($getLayanan['jenis_objek'] == '3') {
                        $pages[] = $this->cetak_list_kolektif(false, $getLayanan, true);
                    }
                }
            }
        }
        if (!$result) {
            $pdf = pdf::loadHTML($pages)
                ->setPaper('A4')
                ->setOrientation('landscape');
            $pdf->setOption('enable-local-file-access', true);
            return $pdf->stream();
        }
    }
    public function cetak_berkas_kolektif(Request $request)
    {
        $request = $request->only('nomor_layanan');
        if (!$request['nomor_layanan']) {
            redirect('laporan_input');
        }
        $nomorLayanan = explode(',', $request['nomor_layanan']);
        if (count($nomorLayanan) == '1') {
            $nomorLayanan = $nomorLayanan[0];
            $result = [];
            $layanan = Layanan::where('layanan.nomor_layanan', $nomorLayanan)->get();
            // dd($layanan->toArray());
            if ($layanan->count() > 0) {
                $getLayanan = $layanan->first()->toArray();
                $getLayanan['objek'] = Layanan_objek::where(['nomor_layanan' => $nomorLayanan])->get()->toArray();
                $getLayanan['layanan_dokumen'] = Layanan_dokumen::where(['nomor_layanan' => $nomorLayanan])->get()->toArray();
                return $this->cetak_list_kolektif(false, $getLayanan);
            }
        } else {
            return $this->cetak_list_kolektif($nomorLayanan);
        }
    }
    public function cetak_tanda_terima_kolektif($getLayanan = [])
    {
        $pages = [];
        if (count($getLayanan) > 0) {
            $getObjek = $getLayanan['objek'];
            $firstObjek = current($getObjek);
            $nop = $firstObjek['kd_propinsi'] . "." .
                $firstObjek['kd_dati2'] . "." .
                $firstObjek['kd_kecamatan'] . "." .
                $firstObjek['kd_kelurahan'] . "." .
                $firstObjek['kd_blok'] . "-" .
                $firstObjek['no_urut'] . "." .
                $firstObjek['kd_jns_op'];
            $getAlamatOP = $firstObjek['alamat_op'];
            $getkelkecOP = DB::table(db::raw("spo.ref_kelurahan ref_kelurahan"))
                ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                    $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
                })
                ->where(['ref_kelurahan.kd_kecamatan' => $firstObjek['kd_kecamatan']])
                ->where(['ref_kelurahan.kd_kelurahan' => $firstObjek['kd_kelurahan']])
                ->get()->first();
            $user = User::select(['nama'])->where(['id' => $getLayanan['updated_by']])->get()->first();
            $userDesa = User::where(['id' => $getLayanan['created_by']])->get()->first();
            $tglPerkiraan = tglIndo($getLayanan['updated_at']);
            if ($getLayanan['jenis_layanan_id'] != '2') {
                $default = '14';
                $tglPerkiraan = tglIndo(Carbon::createFromFormat('Y-m-d H:i:s', $getLayanan['updated_at'])->addDay($default));
            }
            $tableDokumen = [];
            $no = 1;
            if ($getLayanan['jenis_objek'] == '3') {
                foreach ($getLayanan['layanan_dokumen'] as $itemDokumen) {
                    $tableDokumen[] = [
                        'no' => $no,
                        'nama_dokumen' => $itemDokumen['nama_dokumen'],
                        'keterangan' => $itemDokumen['keterangan']
                    ];
                    $no++;
                }
            }
            if ($getLayanan['jenis_layanan_id'] == '7') {
                $nop = '-';
                foreach ($getObjek as $index => $item) {
                    if ($item['nop_gabung'] == null || $item['nop_gabung'] == '0' || $item['nop_gabung'] == '') {
                        unset($getObjek[$index]);
                    }
                }
            }
            if ($getLayanan['jenis_objek'] == '3') {
                $getLayanan['nik'] = "";
            }
            $permohonan = [
                'nomor_layanan' => $getLayanan['nomor_layanan'],
                'tanggal_permohonan' => tglIndo($getLayanan['updated_at']),
                'tanggal_selesai' => $tglPerkiraan,
                'tgl' => $getLayanan['updated_at'],
                'keterangan' => ucfirst($getLayanan['keterangan']),
                'jenis_layanan' => 'Pengajuan ' . $getLayanan['jenis_layanan_nama'],
                'jenis_layanan_id' => $getLayanan['jenis_layanan_id'],
                'nama_pemohon' => $userDesa['nama'],
                'nik_pemohon' => $userDesa['nik'],
                'alamat_pemohon' => "",
                'kelurahan_pemohon' => $getkelkecOP->nm_kelurahan,
                'kecamatan_pemohon' => $getkelkecOP->nm_kecamatan,
                'dati2_pemohon' => 'Kabupaten Malang',
                'propinsi_pemohon' => 'Propinsi Jawa Timur',
                'nomor_hp' => $getLayanan['nomor_telepon'],
                'nop' => $nop,
                'jenis_objek' => $getLayanan['jenis_objek'],
                'alamat_op' => $getAlamatOP,
                'kelurahan_op' => $getkelkecOP->nm_kelurahan ?? '-',
                'kecamatan_op' => $getkelkecOP->nm_kecamatan ?? '-',
                'rtrw_wp' => $firstObjek['rt_wp'] . '/' . $firstObjek['rw_wp'],
                'rtrw_op' => $firstObjek['rt_op'] . '/' . $firstObjek['rw_op'],
                'petugas_penerima_berkas' => $user->nama, //$getLayanan['created_by']
                'luas_bumi' => gallade::parseQuantity($firstObjek['luas_bumi']),
                'luas_bng' => gallade::parseQuantity($firstObjek['luas_bng']),
                'njop_bumi' => gallade::parseQuantity($firstObjek['njop_bumi']),
                'njop_bng' => gallade::parseQuantity($firstObjek['njop_bng']),
                'lokasi_objek_nama' => $firstObjek['lokasi_objek_nama'],
                'kelompok_objek_nama' => $firstObjek['kelompok_objek_nama'],
                'nama_wp' => $getLayanan['nama'],
                'alamat_wp' => $getLayanan['alamat'],
                'tanggal_sk' => $firstObjek['created_at'],
                'nomor_telepon' => $getLayanan['nomor_telepon'],
                'table_dokumen' => $tableDokumen
            ];
            // dd($getLayanan);
            // return view('layanan.cetak.kolektif.tandaterima', compact('permohonan','getObjek'));
            $pages = View::make('layanan.cetak.kolektif.tandaterima', compact('permohonan', 'getObjek'));
        }
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true)
            ->setOption('enable-external-links', true);
        return $pdf->stream();
    }
    public function cetak_tanda_terima_bphtb($getLayanan = [])
    {
        $pages = [];
        if (count($getLayanan) > 0) {
            $getObjek = $getLayanan['objek'];
            $firstObjek = current($getObjek);
            $nop = $firstObjek['kd_propinsi'] . "." .
                $firstObjek['kd_dati2'] . "." .
                $firstObjek['kd_kecamatan'] . "." .
                $firstObjek['kd_kelurahan'] . "." .
                $firstObjek['kd_blok'] . "-" .
                $firstObjek['no_urut'] . "." .
                $firstObjek['kd_jns_op'];
            $getAlamatOP = $firstObjek['alamat_op'];
            $getkelkecOP = DB::table(db::raw("spo.ref_kelurahan ref_kelurahan"))
                ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                    $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
                })
                ->where(['ref_kelurahan.kd_kecamatan' => $firstObjek['kd_kecamatan']])
                ->where(['ref_kelurahan.kd_kelurahan' => $firstObjek['kd_kelurahan']])
                ->get()->first();
            $user = User::select(['nama'])->where(['id' => $getLayanan['updated_by']])->get()->first();
            $userDesa = User::where(['id' => $getLayanan['created_by']])->get()->first();
            $tglPerkiraan = tglIndo($getLayanan['updated_at']);
            if ($getLayanan['jenis_layanan_id'] != '2') {
                $default = '14';
                $tglPerkiraan = tglIndo(Carbon::createFromFormat('Y-m-d H:i:s', $getLayanan['updated_at'])->addDay($default));
            }
            $tableDokumen = [];
            $no = 1;
            $ext = "";
            if ($getLayanan['jenis_objek'] == '5') {
                foreach ($getLayanan['layanan_dokumen'] as $itemDokumen) {
                    $tableDokumen[] = [
                        'no' => $no,
                        'nama_dokumen' => $itemDokumen['nama_dokumen'],
                        'keterangan' => env('URL_SIMBPHTB', 'http://192.168.1.215/bphtb') . $itemDokumen['filename']
                    ];
                    $no++;
                }
                $ext = " [SIM-BPHTB]";
                $userDesa['nama'] = $getLayanan['nama'];
                $userDesa['nik'] = $getLayanan['nik'];
            }
            if ($getLayanan['jenis_layanan_id'] == '7') {
                $nop = '-';
                foreach ($getObjek as $index => $item) {
                    if ($item['nop_gabung'] == null || $item['nop_gabung'] == '0' || $item['nop_gabung'] == '') {
                        unset($getObjek[$index]);
                    }
                }
            }
            if ($getLayanan['jenis_objek'] == '3') {
                $getLayanan['nik'] = "";
            }
            $permohonan = [
                'nomor_layanan' => $getLayanan['nomor_layanan'],
                'tanggal_permohonan' => tglIndo($getLayanan['updated_at']),
                'tanggal_selesai' => $tglPerkiraan,
                'tgl' => $getLayanan['updated_at'],
                'keterangan' => ucfirst($getLayanan['keterangan']),
                'jenis_layanan' => 'Pengajuan ' . $getLayanan['jenis_layanan_nama'] . $ext,
                'jenis_layanan_id' => $getLayanan['jenis_layanan_id'],
                'pengurus' => $getLayanan['nama'],
                'nik_pengurus' => $getLayanan['nik'],
                'nama_pemohon' => $firstObjek['nama_wp'],
                'nik_pemohon' => $firstObjek['nik_wp'],
                'alamat_pemohon' => "",
                'kelurahan_pemohon' => $getkelkecOP->nm_kelurahan,
                'kecamatan_pemohon' => $getkelkecOP->nm_kecamatan,
                'dati2_pemohon' => 'Kabupaten Malang',
                'propinsi_pemohon' => 'Propinsi Jawa Timur',
                'nomor_hp' => $getLayanan['nomor_telepon'],
                'nop' => $nop,
                'jenis_objek' => $getLayanan['jenis_objek'],
                'alamat_op' => $getAlamatOP,
                'kelurahan_op' => $getkelkecOP->nm_kelurahan ?? '-',
                'kecamatan_op' => $getkelkecOP->nm_kecamatan ?? '-',
                'rtrw_wp' => $firstObjek['rt_wp'] . '/' . $firstObjek['rw_wp'],
                'rtrw_op' => $firstObjek['rt_op'] . '/' . $firstObjek['rw_op'],
                'petugas_penerima_berkas' => $user->nama, //$getLayanan['created_by']
                'luas_bumi' => gallade::parseQuantity($firstObjek['luas_bumi']),
                'luas_bng' => gallade::parseQuantity($firstObjek['luas_bng']),
                'njop_bumi' => gallade::parseQuantity($firstObjek['njop_bumi']),
                'njop_bng' => gallade::parseQuantity($firstObjek['njop_bng']),
                'lokasi_objek_nama' => $firstObjek['lokasi_objek_nama'],
                'kelompok_objek_nama' => $firstObjek['kelompok_objek_nama'],
                'nama_wp' => $getLayanan['nama'],
                'alamat_wp' => $getLayanan['alamat'],
                'tanggal_sk' => $firstObjek['created_at'],
                'nomor_telepon' => $getLayanan['nomor_telepon'],
                'table_dokumen' => $tableDokumen
            ];
            // dd($getLayanan);
            // return view('layanan.cetak.kolektif.tandaterima', compact('permohonan','getObjek'));
            $pages = View::make('layanan.cetak.tandaterima_bphtb', compact('permohonan', 'getObjek'));
        }
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $pdf->setOption('enable-local-file-access', true)
            ->setOption('enable-external-links', true);
        return $pdf->stream();
    }
    public function tanda_terima(Request $request)
    {
        $request = $request->only('nomor_layanan');
        if (!$request['nomor_layanan']) {
            redirect('laporan_input');
        }
        $nomorLayanan = explode(',', $request['nomor_layanan']);
        if (count($nomorLayanan) == '1') {
            $nomorLayanan = $nomorLayanan[0];
            $result = [];
            $layanan = Layanan::where(['nomor_layanan' => $nomorLayanan])->get();
            if ($layanan->count() > 0) {
                // with(['objek','layanan_dokumen'])
                $getLayanan = $layanan->first()->toArray();
                $getLayanan['objek'] = Layanan_objek::where(['nomor_layanan' => $nomorLayanan])->get()->toArray();
                $getLayanan['layanan_dokumen'] = Layanan_dokumen::where(['nomor_layanan' => $nomorLayanan])->get()->toArray();

                if (in_array($getLayanan['jenis_objek'], ['3'])) {
                    return $this->cetak_tanda_terima_kolektif($getLayanan);
                }
                if (in_array($getLayanan['jenis_objek'], ['5'])) {
                    return $this->cetak_tanda_terima_bphtb($getLayanan);
                }
                $getObjek = $getLayanan['objek'];
                $firstObjek = current($getObjek);
                // dd($getLayanan['layanan_dokumen']);
                $nop = $firstObjek['kd_propinsi'] . "." .
                    $firstObjek['kd_dati2'] . "." .
                    $firstObjek['kd_kecamatan'] . "." .
                    $firstObjek['kd_kelurahan'] . "." .
                    $firstObjek['kd_blok'] . "-" .
                    $firstObjek['no_urut'] . "." .
                    $firstObjek['kd_jns_op'];
                $getAlamatOP = $firstObjek['alamat_op']; //Layanan_conf::getAlamatNOP($nop);
                // dd($firstObjek);
                $getkelkecOP = DB::table(db::raw("spo.ref_kelurahan ref_kelurahan"))
                    ->select(['ref_kelurahan.nm_kelurahan', 'ref_kecamatan.nm_kecamatan'])
                    ->join(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                        $join->On('ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan');
                    })
                    ->where(['ref_kelurahan.kd_kecamatan' => $firstObjek['kd_kecamatan']])
                    ->where(['ref_kelurahan.kd_kelurahan' => $firstObjek['kd_kelurahan']])
                    ->get()->first();
                // $join->On('layanan_objek.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan');
                //     })
                //     ->join(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                //         $join->On('layanan_objek.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                //             ->On('layanan_objek.kd_kelurahan', '=', 'ref_kelurahan.kd_kelurahan');
                //     });
                $user = User::select(['nama'])->where(['id' => $getLayanan['created_by']])->get()->first();
                $tglPerkiraan = tglIndo($getLayanan['tanggal_layanan']);
                if ($getLayanan['jenis_layanan_id'] != '2') {
                    $default = ($getLayanan['jenis_objek'] == '2') ? '14' : '7';
                    if ($getLayanan['jenis_layanan_id'] == '4') {
                        $default = '21';
                    }
                    $tglPerkiraan = tglIndo(Carbon::createFromFormat('Y-m-d H:i:s', $getLayanan['tanggal_layanan'])->addDay($default));
                }
                if ($getLayanan['jenis_layanan_id'] == '6' && $getLayanan['jenis_objek'] != '3') {
                    $berkasInduk = [];
                    foreach ($getLayanan['layanan_dokumen'] as $itemDokumen) {
                        $berkasInduk[$itemDokumen['keyid']][] = [
                            'nama_dokumen' => $itemDokumen['nama_dokumen'],
                            'keterangan' => $itemDokumen['keterangan']
                        ];
                    }
                    $listPecah = [];
                    foreach ($getObjek as $itemPecah) {
                        if ($itemPecah['nop_gabung'] != null && $itemPecah['nop_gabung'] != '0' && $itemPecah['nop_gabung'] != '') {
                            $listPecah[$itemPecah['id']] = [
                                'berkas' => (in_array($itemPecah['id'], array_keys($berkasInduk))) ? $berkasInduk[$itemPecah['id']] : false,
                                'nik_wp' => $itemPecah['nik_wp'],
                                'nama_wp' => $itemPecah['nama_wp'],
                                'alamat_wp' => $itemPecah['alamat_wp'],
                                'lokasi_objek_nama' => $itemPecah['lokasi_objek_nama'],
                                'luas_bumi' => $itemPecah['luas_bumi'],
                                'luas_bng' => $itemPecah['luas_bng'],
                                'telp_wp' => $itemPecah['telp_wp'],

                            ];
                        }
                    }
                    $permohonan = [
                        'id' => $firstObjek['id'],
                        'jenis_layanan_id' => $getLayanan['jenis_layanan_id'],
                        'nomor_layanan' => $getLayanan['nomor_layanan'],
                        'tanggal_permohonan' => tglIndo($getLayanan['tanggal_layanan']),
                        'tanggal_selesai' => $tglPerkiraan,
                        'tgl' => $getLayanan['tanggal_layanan'],
                        'keterangan' => ucfirst($getLayanan['keterangan']),
                        'jenis_layanan' => 'Pengajuan ' . $getLayanan['jenis_layanan_nama'],
                        'nop' => $nop,

                        'nik_wp_pemohon' => $getLayanan['nik'],
                        'nama_pemohon' => $getLayanan['nama'],
                        'nomor_telepon_pemohon' => $getLayanan['nomor_telepon'],

                        'luas_bumi' => $firstObjek['luas_bumi'],
                        'luas_bng' => $firstObjek['luas_bng'],
                        'berkas' => in_array($firstObjek['id'], array_keys($berkasInduk)) ? $berkasInduk[$firstObjek['id']] : false,
                        'petugas_penerima_berkas' => $user->nama, //$getLayanan['created_by']
                        'pecah' => $listPecah
                    ];
                    // return view('layanan.cetak.tandaterima_pecah', compact('permohonan'));
                    $pages = View::make('layanan.cetak.tandaterima_pecah', compact('permohonan'));
                } else {
                    $tableDokumen = [];
                    $no = 1;
                    foreach ($getLayanan['layanan_dokumen'] as $itemDokumen) {
                        $tableDokumen[] = [
                            'no' => $no,
                            'nama_dokumen' => $itemDokumen['nama_dokumen'],
                            'keterangan' => $itemDokumen['keterangan']
                        ];
                        $no++;
                    }
                    if ($getLayanan['jenis_layanan_id'] == '7') {
                        $nop = '-';
                        foreach ($getObjek as $index => $item) {
                            if ($item['nop_gabung'] == null || $item['nop_gabung'] == '0' || $item['nop_gabung'] == '') {
                                unset($getObjek[$index]);
                            }
                        }
                    }
                    if ($getLayanan['jenis_objek'] == '3') {
                        $getLayanan['nik'] = "";
                    }
                    $permohonan = [
                        'nomor_layanan' => $getLayanan['nomor_layanan'],
                        'tanggal_permohonan' => tglIndo($getLayanan['tanggal_layanan']),
                        'tanggal_selesai' => $tglPerkiraan,
                        'tgl' => $getLayanan['tanggal_layanan'],
                        'keterangan' => ucfirst($getLayanan['keterangan']),
                        'jenis_layanan' => 'Pengajuan ' . $getLayanan['jenis_layanan_nama'],
                        'jenis_layanan_id' => $getLayanan['jenis_layanan_id'],
                        'nama_pemohon' => $getLayanan['nama'],
                        'nik_pemohon' => $getLayanan['nik'],
                        'alamat_pemohon' => $getLayanan['alamat'],
                        'kelurahan_pemohon' => $getLayanan['kelurahan'],
                        'kecamatan_pemohon' => $getLayanan['kecamatan'],
                        'dati2_pemohon' => $getLayanan['dati2'],
                        'propinsi_pemohon' => $getLayanan['propinsi'],
                        'nomor_hp' => $getLayanan['nomor_telepon'],
                        'nop' => $nop,
                        'jenis_objek' => $getLayanan['jenis_objek'],
                        'alamat_op' => $getAlamatOP,
                        'kelurahan_op' => $getkelkecOP->nm_kelurahan ?? '-',
                        'kecamatan_op' => $getkelkecOP->nm_kecamatan ?? '-',
                        'rtrw_wp' => $firstObjek['rt_wp'] . '/' . $firstObjek['rw_wp'],
                        'rtrw_op' => $firstObjek['rt_op'] . '/' . $firstObjek['rw_op'],
                        'petugas_penerima_berkas' => $user->nama, //$getLayanan['created_by']
                        'luas_bumi' => gallade::parseQuantity($firstObjek['luas_bumi']),
                        'luas_bng' => gallade::parseQuantity($firstObjek['luas_bng']),
                        'njop_bumi' => gallade::parseQuantity($firstObjek['njop_bumi']),
                        'njop_bng' => gallade::parseQuantity($firstObjek['njop_bng']),
                        'lokasi_objek_nama' => $firstObjek['lokasi_objek_nama'],
                        'kelompok_objek_nama' => $firstObjek['kelompok_objek_nama'],
                        'nama_wp' => $getLayanan['nama'],
                        'alamat_wp' => $getLayanan['alamat'],
                        'tanggal_sk' => $firstObjek['created_at'],
                        'nomor_telepon' => $getLayanan['nomor_telepon'],
                        'table_dokumen' => $tableDokumen
                    ];
                    // dd($getLayanan);
                    // return view('layanan.cetak.tandaterima', compact('permohonan','getObjek'));
                    $pages = View::make('layanan.cetak.tandaterima', compact('permohonan', 'getObjek'));
                }
                $pdf = pdf::loadHTML($pages)
                    ->setPaper('A4');
                $pdf->setOption('enable-local-file-access', true);
                return $pdf->stream();
            }
        } else {
            // return $this->cetak_kolektif($nomorLayanan);
        }
    }
    public function cetak_pdf(Request $request)
    {
        $request = $request->only('nomor_layanan');
        if ($request['nomor_layanan']) {
            $result = [];
            $layanan = Layanan::where(['nomor_layanan' => $request['nomor_layanan']])
                ->get();
            if ($layanan->count() > 0) {
                $getLayanan = $layanan->first()->toArray();
                $getObjek = Layanan_objek::where(['nomor_layanan' => $request['nomor_layanan']])->get()->toArray();
                $getObjek = current($getObjek);
                $nop = $getObjek['kd_propinsi'] . "." .
                    $getObjek['kd_dati2'] . "." .
                    $getObjek['kd_kecamatan'] . "." .
                    $getObjek['kd_kelurahan'] . "." .
                    $getObjek['kd_blok'] . "-" .
                    $getObjek['no_urut'] . "." .
                    $getObjek['kd_jns_op'];
                $getAlamatOP = $getObjek['alamat_op']; //Layanan_conf::getAlamatNOP($nop);
                $getOPlog = LayananUpdate::getOPlog([
                    $getObjek['kd_propinsi'],
                    $getObjek['kd_dati2'],
                    $getObjek['kd_kecamatan'],
                    $getObjek['kd_kelurahan'],
                    $getObjek['kd_blok'],
                    $getObjek['no_urut'],
                    $getObjek['kd_jns_op']
                ]);
                $permohonan = [
                    'nomor_layanan' => $getLayanan['nomor_layanan'],
                    'tanggal_permohonan' => tglIndo($getLayanan['tanggal_layanan']),
                    'keterangan' => $getLayanan['keterangan'],
                    'jenis_layanan' => 'Pengajuan ' . $getLayanan['jenis_layanan_nama'],
                    'nama_pemohon' => $getLayanan['nama'],
                    'nik_pemohon' => $getLayanan['nik'],
                    'alamat_pemohon' => $getLayanan['alamat'],
                    'kelurahan_pemohon' => $getLayanan['kelurahan'],
                    'kecamatan_pemohon' => $getLayanan['kecamatan'],
                    'dati2_pemohon' => $getLayanan['dati2'],
                    'propinsi_pemohon' => $getLayanan['propinsi'],
                    'nomor_hp' => $getLayanan['nomor_telepon'],
                    'nop' => $nop,
                    'alamat_op' => $getAlamatOP . ', RT/RW ' . $getObjek['rt_op'] . '/' . $getObjek['rw_op'],
                    'petugas_penerima_berkas' => $getLayanan['created_by'],
                    'luas_bumi' => gallade::parseQuantity($getObjek['luas_bumi']),
                    'luas_bng' => gallade::parseQuantity($getObjek['luas_bng']),
                    'njop_bumi' => gallade::parseQuantity($getObjek['njop_bumi']),
                    'njop_bng' => gallade::parseQuantity($getObjek['njop_bng']),

                    'nama_wp' => $getLayanan['nama'],
                    'alamat_wp' => $getLayanan['alamat'],
                    'tanggal_sk' => $getObjek['created_at'],
                    'nomor_telepon' => $getLayanan['nomor_telepon'],

                    'nama_wp_before' => $getOPlog['nama_wp'],
                    'alamat_wp_before' => $getOPlog['alamat_wp'],
                    'alamat_op_before' => $getOPlog['alamat_op'],
                    'nomor_telepon_before' => $getOPlog['nomor_telepon'],
                ];
                $url = config('app.url') . 'laporan_input/cetak_pdf?nomor_layanan' . $request['nomor_layanan'];
                if (in_array($getLayanan['jenis_layanan_id'], ['5'])) {
                    $jp = JenisPengurangan::where(['id' => $getObjek['jenis_pengurangan'], 'jenis' => '0'])->get();
                    if ($jp->count() > 0) {
                        $peg = pegawaiStruktural::where('kode', '01')->first()->toArray();
                        $get = $jp->first();
                        $yearNow = Carbon::now()->year;
                        $getPbb = LayananUpdate::getPBB($nop, $yearNow);
                        $pbb = 0;
                        if (count($getPbb)) {
                            if (isset(current($getPbb)->pbb)) {
                                $pbb = current($getPbb)->pbb;
                            }
                        }
                        $pbb = current($getPbb)->pbb;
                        $permohonan['pbb'] = gallade::parseQuantity($pbb);
                        $permohonan['pengurangan'] = gallade::parseQuantity($get->pengurangan);
                        $np = $get->pengurangan * $pbb / 100;
                        $permohonan['nilai_pengurangan'] = gallade::parseQuantity($np);
                        $permohonan['hasil_pbb'] = gallade::parseQuantity($pbb - $np);
                        // return view('layanan.cetak.pengurangan_selesai_depan', compact('permohonan','peg','url',));
                        $pages = View::make('layanan.cetak.pengurangan_selesai_depan', compact('permohonan', 'peg', 'url'));
                    } else {
                        $peg = pegawaiStruktural::where('kode', '02')->first()->toArray();
                        //  return view('layanan.cetak.pembetulan_data_pribadi', compact('permohonan','peg','url'));
                        $pages = View::make('layanan.cetak.pembetulan_data_pribadi', compact('permohonan', 'peg', 'url'));
                    }
                } else {
                    $peg = pegawaiStruktural::where('kode', '02')->first()->toArray();
                    // return view('layanan.cetak.pembetulan_data_pribadi', compact('permohonan','peg','url'));
                    $pages = View::make('layanan.cetak.pembetulan_data_pribadi', compact('permohonan', 'peg', 'url'));
                }
                $pdf = pdf::loadHTML($pages)->setPaper('A4');
                $pdf->setOption('enable-local-file-access', true);
                return $pdf->stream();
            }
        }
    }
    public function detail(Request $request)
    {
        $request = $request->only('list');
        $return = response()->json(["msg" => "Detail data, bermasalah.", "status" => false]);
        if ($request['list']) {
            $result = [];
            $nomor_layanan = $request['list']['nomor_layanan'];
            $layanan = Layanan::with('layanan_objek')
                ->with('layanan_dokumen')
                ->where(['nomor_layanan' => $nomor_layanan])->get();
            if ($layanan->count()) {
                $getLayanan = current($layanan->toArray());
                $c = ['data' => ':', 'class' => 'w-5 text-center'];
                $setData = [
                    [['data' => 'Nomor Layanan', 'class' => 'w-15'], $c, $getLayanan['nomor_layanan']],
                    ['Layanan', $c, $getLayanan['jenis_layanan_nama']],
                    ['NIK', $c, $getLayanan['nik']],
                    ['Nama', $c, $getLayanan['nama']],
                    ['Alamat', $c, $getLayanan['alamat']],
                    ['Kelurahan', $c, $getLayanan['kelurahan']],
                    ['Kecamatan', $c, $getLayanan['kecamatan']],
                    ['Kota/Kabupaten', $c, $getLayanan['dati2']],
                    ['Propinsi', $c, $getLayanan['propinsi']],
                    ['No Telepon', $c, $getLayanan['nomor_telepon']],
                    ['Keterangan', $c, $getLayanan['keterangan']]
                ];
                $setObjek = [];
                $setSubjek = [];
                if (count((array)$getLayanan['layanan_objek']) > 0) {
                    $getObjek = $getLayanan['layanan_objek'];
                    $setSubjek = [
                        [['data' => 'NIK', 'class' => 'w-10'], $c, $getObjek['nik_wp']],
                        ['Nama', $c, $getObjek['nama_wp']],
                        ['Alamat', $c, $getObjek['alamat_wp']],
                        ['RT/RW', $c, $getObjek['rt_wp'] . '/' . $getObjek['rw_wp']],
                        ['Kelurahan', $c, $getObjek['kelurahan_wp']],
                        ['Kecamatan', $c, $getObjek['kecamatan_wp']],
                        ['Kota/Kabupaten', $c, $getObjek['dati2_wp']],
                        ['Propinsi', $c, $getObjek['propinsi_wp']],
                    ];
                    $nop = $getObjek['kd_propinsi'] . "." .
                        $getObjek['kd_dati2'] . "." .
                        $getObjek['kd_kecamatan'] . "." .
                        $getObjek['kd_kelurahan'] . "." .
                        $getObjek['kd_blok'] . "-" .
                        $getObjek['no_urut'] . "." .
                        $getObjek['kd_jns_op'];
                    $kecamatan = Kecamatan::where(['kd_kecamatan' => $getObjek['kd_kecamatan']])->select('nm_kecamatan')->get();
                    $kelurahan = Kelurahan::where([
                        'kd_kecamatan' => $getObjek['kd_kecamatan'],
                        'kd_kelurahan' => $getObjek['kd_kelurahan']
                    ])
                        ->select(['nm_kelurahan'])->get();
                    $setObjek = [
                        [['data' => 'NOP', 'class' => 'w-20'], $c, $nop],
                        ['Alamat', $c, $getObjek['alamat_op']],
                        ['RT/RW', $c, $getObjek['rt_op'] . '/' . $getObjek['rw_op']],
                        ['Kelurahan', $c, current($kelurahan->toArray())['nm_kelurahan']],
                        ['Kecamatan', $c, current($kecamatan->toArray())['nm_kecamatan']],
                        ['Kota/Kabupaten', $c, 'KABUPATEN MALANG'],
                        ['Propinsi', $c, 'JAWA TIMUR'],
                        ['Luas/Njop Bumi', $c, gallade::parseQuantity($getObjek['luas_bumi']) . '/' . gallade::parseQuantity($getObjek['njop_bumi'])],
                        ['Luas/Njop Bangunan', $c, gallade::parseQuantity($getObjek['luas_bng']) . '/' . gallade::parseQuantity($getObjek['njop_bng'])],
                        ['Kelompok/Lokasi Objek', $c, $getObjek['kelompok_objek_nama'] . '/' . $getObjek['lokasi_objek_nama']]
                    ];
                }
                $setDokumen = [];
                if (count($getLayanan['layanan_dokumen']) > 0) {
                    $getDokumen = $getLayanan['layanan_dokumen'];
                    $listDokumen = ['no', 'filename', 'nama_dokumen', 'keterangan'];
                    foreach ($getDokumen as $item) {
                        $btn = '<a href="laporan_input/berkas?nomor_layanan=' . $item['nomor_layanan'] . '&id=' . $item['id'] . '" target="_BLANK" class="btn btn-info btn-block">Preview</a>';
                        $setDokumen[] = [
                            '',
                            $item['filename'],
                            $item['nama_dokumen'],
                            $item['keterangan'],
                            $btn
                        ];
                    }
                }
                $jl = ['1' => 'Data Pribadi', '2' => 'Data Badan', '3' => 'Data Kolektif'];
                $result['data-layanan'] = gallade::generateinTbody($setData, "Data Masih Kosong");
                $result['jenis_layanan'] = ($jl[$getLayanan['jenis_objek']]) ? $jl[$getLayanan['jenis_objek']] : '---';
                $result['data-subjek'] = gallade::generateinTbody($setSubjek, "Data Masih Kosong");
                $result['data-objek'] = gallade::generateinTbody($setObjek, "Data Masih Kosong");
                $result['data-dokumen'] = gallade::generateinTbody($setDokumen, "Data Masih Kosong", 5);
                $result['nomor_layanan'] = $$nomor_layanan;
            }
            $return = response()->json(["msg" => "Detail data Layanan input ditemukan.", "status" => true, 'data' => $result]);
        }
        return $return;
    }
    public function berkas(Request $request)
    {
        $request = $request->only('nomor_layanan', 'id');
        if ($request['nomor_layanan'] && $request['id']) {
            $layanan_dokumen = layanan_dokumen::where(['nomor_layanan' => $request['nomor_layanan'], 'id' => $request['id']])->get()->first()->toArray();
            $pathToFile = 'storage/app/' . $layanan_dokumen['disk'] . '/' . $layanan_dokumen['filename'];
            return response()->file($pathToFile);
        }
    }
    public function cetak_sk(Request $request)
    {
        $data = $request->only('id');
        if (!$data) return;
        $layanan = Layanan::where(['nomor_layanan' => $data['id']])->first();
        $data = [
            'nm_wp' => $layanan->nama,
            'nopel' => $data['id'],
            'tahunpajak' => '2021',
            'tanggalsk' => tglIndo($layanan->created_at)
        ];
        $pdf = PDF::loadView('layanan.cetak.sk_pembatalan', compact('data'))->setPaper('a4', 'portrait');
        $filename = time() . '.pdf';
        return $pdf->stream($filename);
    }
    public function destroy(Request $request)
    {
        $get = $request->only('list');
        $return = ["msg" => "Proses Hapus Ajuan tidak berhasil.", "status" => false];
        if (!$get) {
            return response()->json($return);
        }
        $get = $get['list'];
        DB::beginTransaction();
        try {
            // Layanan_dokumen::where(['nomor_layanan'=> $nop['id']])->delete();
            // Layanan_objek::where(['nomor_layanan'=> $nop['id']])->delete();
            $cekJenisObjekExternal = Layanan::where(['nomor_layanan' => $get['nomor_layanan']])
                ->select('jenis_objek')
                ->first();
            Layanan::where(['nomor_layanan' => $get['nomor_layanan']])->update(['deleted_by' => Auth()->user()->id, 'keterangan' => $get['keterangan']]);
            Layanan::where(['nomor_layanan' => $get['nomor_layanan']])->delete();
            $disk = 'dokumen_pendukung/' . $get['nomor_layanan'];
            $isExists = Storage::disk('public')->exists($disk);
            if ($isExists) {
                $isExists = Storage::disk('public')->deleteDirectory($disk);
            }
            if (in_array($cekJenisObjekExternal->jenis_objek, ['5'])) {
                $time = Carbon::now()->format('Y-m-d.H:i:s');
                $API = env('BPHTB_API_SECRET');
                $send = Http::post(env('BPHTB_API') . '/permohonan_pbb/update', [
                    'headerLayananPbbBphtbReq' => [
                        'signature' => sha1($API . $time),
                        'time' => $time
                    ],
                    'body' => [
                        'nomor_layanan' => $get['nomor_layanan'],
                        'status' => '5'
                    ]
                ]);
                // send BPHTB
            }
            $return = ["msg" => "Proses Hapus Ajuan berhasil.", "status" => true];
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $return = ["msg" => $e->getMessage(), "status" => false];
        }
        return response()->json($return);
    }
}
