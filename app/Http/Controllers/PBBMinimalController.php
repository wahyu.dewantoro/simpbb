<?php

namespace App\Http\Controllers;

use App\Helpers\gallade;
use App\Models\JenisPengurangan;
use App\Models\PBBMinimal;
use App\Models\Tarif;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use DataTables;

class PBBMinimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Tarif';
        $data = [];
        $nowYear = Carbon::now()->year;
        $rangeZ = 5;
        $lasYear = $nowYear + $rangeZ;
        $listYear = [];
        for ($i = $nowYear; $i <= $lasYear; $i++) {
            $listYear[$i] = $i;
        }
        return view('pbb_minimal.index', compact('title', 'data', 'listYear'));
    }

    public function tarif_search()
    {
        $select = [
            'kd_propinsi',
            'kd_dati2',
            'thn_pbb_minimal',
            'no_sk_pbb_minimal',
            'tgl_sk_pbb_minimal',
            'nilai_pbb_minimal',
            'tgl_rekam_pbb_minimal'
        ];
        $dataSet = PBBMinimal::select($select)->orderBy('tgl_rekam_pbb_minimal', 'desc');
        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('raw_tag', function ($q) {
            $btnDelete = gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Delete" data-delete=""', 'danger');
            $btn = '<div class="btn-group">' . $btnDelete . "</div>";
            return $btn;
        })->editColumn('nilai_tarif', function ($q) {
            return number_format($q->nilai_tarif, strlen($q->nilai_tarif) - 1);
        })->editColumn('njop_min', function ($q) {
            return gallade::parseQuantity($q->njop_min);
        })->editColumn('njop_max', function ($q) {
            return gallade::parseQuantity($q->njop_max);
        })
            ->addIndexColumn()
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tarif_store(Request $request)
    {
        $return = ["msg" => "Proses gagal.", "status" => false];
        DB::beginTransaction();
        try {
            $list = [
                'kd_propinsi' => 'required',
                'kd_dati2' => 'required',
                'thn_pbb_minimal' => 'required',
                'no_sk_pbb_minimal' => 'required',
                'tgl_sk_pbb_minimal' => 'required|date',
                'nilai_pbb_minimal' => 'required',
            ];
            $validator = Validator::make($request->all(), $list);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'msg' => $validator->errors()->all()
                ]);
            }
            $saveData = $request->only(array_keys($list));
            $saveData['tgl_rekam_pbb_minimal'] = Carbon::now()->format('Y-m-d H:i:s');
            $saveData['nip_perekam_pbb_minimal'] = "060000000000000000";
            PBBMinimal::updateOrCreate($saveData);
            $return = ["msg" => "Proses Berhasil.", "status" => true];
            DB::commit();
        } catch (\Exception $th) {
            $return = ["msg" => "Proses Berhasil.", "status" => $th->getMessage()];
            DB::rollback();
        }
        return response()->json($return);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $return = ["msg" => "Proses gagal.", "status" => false];
        $list = $request->only('id');
        if ($list) {
            DB::beginTransaction();
            try {
                $objek = PBBMinimal::findorfail($list['id']);
                $objek->delete();
                $return = ["msg" => "Proses Berhasil.", "status" => true];
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $msg = $e->getMessage(); //"Proses tidak berhasil";//
                return response()->json(["msg" => $msg, "status" => false]);
            }
        }
        $return = ["msg" => "Proses berhasil.", "status" => true];
        return response()->json($return);
    }
}
