<?php

namespace App\Http\Controllers;

// use App\Models\DbkbJpbEmpat;

use App\Models\DbkbJpbEmpat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbJpbEmpatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function coreSql($tahun)
    {
        return "select lantai_min_jpb4,lantai_max_jpb4,sum(satu) satu,
        sum(dua) dua,
        sum(tiga) tiga,
        sum(empat) empat
        from (
        select  lantai_min_jpb4,lantai_max_jpb4,
        case when kls_dbkb_jpb4='1' then nilai_dbkb_jpb4*1000 else null end satu, 
        case when kls_dbkb_jpb4='2' then nilai_dbkb_jpb4*1000 else null end dua,
        case when kls_dbkb_jpb4='3' then nilai_dbkb_jpb4*1000 else null end tiga,
        case when kls_dbkb_jpb4='4' then nilai_dbkb_jpb4*1000 else null end empat
        from dbkb_jpb4
        where thn_dbkb_jpb4='$tahun'
        group by lantai_min_jpb4,lantai_max_jpb4,kls_dbkb_jpb4,nilai_dbkb_jpb4) fix
        group by lantai_min_jpb4,lantai_max_jpb4
        order by lantai_min_jpb4,lantai_max_jpb4";
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DB::connection('oracle_satutujuh')->select(DB::raw($this->coreSql($tahun)));
            $read = 1;
            return view('dbkb.jepebeempat._index', compact('data', 'read'));
        }
        return view('dbkb.jepebeempat.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DB::connection('oracle_satutujuh')->select(DB::raw($this->coreSql($tahun)));
            if (count($data) == 0) {
                $data = DB::connection('oracle_satutujuh')->select(DB::raw($this->coreSql($tahun - 1)));
            }
            $read = 0;
            return view('dbkb.jepebeempat._index', compact('data', 'read'));
        }
        return view('dbkb/jepebeempat/form');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();
        $tahun = $request->thn_dbkb_jpb4;
        $data = [];
        for ($i = 0; $i <= 2; $i++) {
            $variable = $req['nilai_' . $i];

            switch ($i) {

                case '1':
                    $lmin = '3';
                    $lmax = '5';
                    break;
                case '2':
                    $lmin = '6';
                    $lmax = '99';
                    break;
                default:
                    $lmin = '1';
                    $lmax = '2';
                    break;
            }


            foreach ($variable as $idx => $item) {
                $data[] = [
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'thn_dbkb_jpb4' => $tahun,
                    'lantai_min_jpb4' => $lmin,
                    'lantai_max_jpb4' => $lmax,
                    'kls_dbkb_jpb4' => $idx + 1,
                    'nilai_dbkb_jpb4' => is_null($item) == '1' ? 0 : $item / 1000
                ];
            }
        }

        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            DbkbJpbEmpat::where(['thn_dbkb_jpb4' => $request->thn_dbkb_jpb4])->delete();
            foreach ($data as $ins) {
                DbkbJpbEmpat::create($ins);
            }

            $flash = ['success' => 'Berhasil memproses DBKB JPB4 pada tahun ' . $request->thn_dbkb_jpb4];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.jepebe-empat.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
