<?php

namespace App\Http\Controllers;

use App\Helpers\Dokumen;
use Illuminate\Http\Request;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use App\Models\Layanan_dokumen;
use App\KelompokObjek;
use App\LokasiObjek;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Helpers\Layanan_conf;
use App\Imports\layanan_kolektif;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use App\Helpers\gallade;
use App\Helpers\InformasiObjek;
use App\Helpers\LayananUpdate;
use App\Imports\layanan_pembatalan;
use App\Jobs\PendataanPenilaianJob;
use App\Jobs\disposisiPenelitian;
use App\Jobs\LayananKolektif;
use App\Models\JenisPengurangan;
use App\Models\Layanan_objek_temp;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
// use DataTables;
// use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

use function Symfony\Component\String\b;

class LayananRegistrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        $layanan = Jenis_layanan::class;
        $layanan_all = $layanan::orderBy('id')->get();
        $Jenis_layanan = $layanan::orderBy('order')
            ->whereNotIn('id', ['3', '8', '7', '6'])
            ->orderBy('id')
            ->get();
        $KelompokObjek = KelompokObjek::all();
        $LokasiObjek = LokasiObjek::all();
        $jenisDokumen = Dokumen::list();
        $jenisAgama = [
            '1' => 'Islam',
            '2' => 'Protestan',
            '3' => 'Katolik',
            '4' => 'Hindu',
            '5' => 'Budha',
            '6' => 'Konghucu',
            '7' => 'Lain-lain'
        ];
        $SSubjekPajak = [
            '1' => 'Pemilik',
            '2' => 'Penyewa',
            '3' => 'Pengelola',
            '4' => 'Pemakai/Pemanfaat',
            '5' => 'Sengketa'
        ];
        $jenisPengurangan = JenisPengurangan::all();
        return view('layanan.form-baru', compact('kecamatan', 'Jenis_layanan', 'layanan_all', 'KelompokObjek', 'LokasiObjek', 'jenisDokumen', 'jenisAgama', 'SSubjekPajak', 'jenisPengurangan'));
    }
    public function pembetulan()
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        $Jenis_layanan = Jenis_layanan::where('nama_layanan', 'LIKE', '%Pembetulan%')->orderBy('order')->get();
        $KelompokObjek = KelompokObjek::all();
        $LokasiObjek = LokasiObjek::all();
        $jenisDokumen = Dokumen::list();
        $jenisAgama = [
            '1' => 'Islam',
            '2' => 'Protestan',
            '3' => 'Katolik',
            '4' => 'Hindu',
            '5' => 'Budha',
            '6' => 'Konghucu',
            '7' => 'Lain-lain'
        ];
        return view('layanan.form-baru-pembetulan', compact('kecamatan', 'Jenis_layanan', 'KelompokObjek', 'LokasiObjek', 'jenisDokumen', 'jenisAgama'));
    }
    public function pembetulanBadan()
    {
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        $Jenis_layanan = Jenis_layanan::where('nama_layanan', 'LIKE', '%Pembetulan%')->orderBy('order')->get();
        $KelompokObjek = KelompokObjek::all();
        $LokasiObjek = LokasiObjek::all();
        $jenisDokumen = Dokumen::list();
        $jenisAgama = [
            '1' => 'Islam',
            '2' => 'Protestan',
            '3' => 'Katolik',
            '4' => 'Hindu',
            '5' => 'Budha',
            '6' => 'Konghucu',
            '7' => 'Lain-lain'
        ];
        return view('layanan.form-baru-pembetulan', compact('kecamatan', 'Jenis_layanan', 'KelompokObjek', 'LokasiObjek', 'jenisDokumen', 'jenisAgama'));
    }
    public function mutasigabung()
    {
        $Jenis_layanan = Jenis_layanan::where(['id' => 7])->get()->first();
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        //dd($Jenis_layanan->toArray());
        $KelompokObjek = KelompokObjek::all(); //7
        $LokasiObjek = LokasiObjek::all();
        $jenisDokumen = Dokumen::list();
        $jenisAgama = [
            '1' => 'Islam',
            '2' => 'Protestan',
            '3' => 'Katolik',
            '4' => 'Hindu',
            '5' => 'Budha',
            '6' => 'Konghucu',
            '7' => 'Lain-lain'
        ];
        return view('layanan.form-baru-mutasi_gabung', compact('Jenis_layanan', 'kecamatan', 'jenisAgama', 'KelompokObjek', 'LokasiObjek', 'jenisDokumen'));
    }
    public function kolektif_online()
    {
        $layanan = Jenis_layanan::whereIn('id', ['1', '2', '3', '6', '7', '9'])
            ->orderBy('nama_layanan')
            ->get();
        $layanan_all = [];
        foreach ($layanan as $item) {
            $item->page = gallade::clean($item->nama_layanan);
            if ($item->id == '3') {
                $item->nama_layanan = 'Pembetulan';
            }
            $layanan_all[] = $item;
        }
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        if ($kecamatan->count() == '1') {
            //->login()
            $user = Auth()->user();
            $role = $user->roles()->pluck('name')->toarray();
            $kelurahan = Kelurahan::where('kd_kecamatan', $kecamatan->first()->kd_kecamatan);
            if(in_array('Desa',$role)){
                $kelurahan=$kelurahan->login();
            }
            $kelurahan=$kelurahan->orderBy('nm_kelurahan', 'asc')
                ->select('nm_kelurahan', 'kd_kelurahan')
                ->get();
        }
        $KelompokObjek = KelompokObjek::all(); //7
        $LokasiObjek = LokasiObjek::all();
        return view('layanan.kolektif_online.index', compact('layanan_all', 'kecamatan', 'kelurahan', 'KelompokObjek', 'LokasiObjek'));
    }
    public function update_kolektif_temp(Request $request)
    {
        $set = [
            "jenis_layanan_id",
            "nik_wp",
            "nama_wp",
            "alamat_wp",
            "rt_rw_wp",
            "kelurahan_wp",
            "kecamatan_wp",
            "dati2_wp",
            "propinsi_wp",
            "alamat_op",
            "rt_rw_op",
            "luas_bumi",
            "luas_bng",
            "kelompok_objek_id",
            "lokasi_objek_id",
            "kd_kecamatan",
            "kd_kelurahan",
            "nop",
            "list_nop",
            "list_pecah",
            "telp_wp",
            "id"
        ];
        $getData = $request->only($set);
        $rtrwwp = explode('/', $getData['rt_rw_wp']);
        $rtrwop = explode('/', $getData['rt_rw_op']);
        $namaKelompokObjek = null;
        $namaLokasiObjek = null;
        if (in_array('kelompok_objek_id', array_keys($getData))) {
            $getKo = KelompokObjek::find($getData['kelompok_objek_id']);
            if ($getKo) {
                $namaKelompokObjek = $getKo->nama_kelompok;
            }
        }
        if (in_array('lokasi_objek_id', array_keys($getData))) {
            $namaLo = LokasiObjek::find($getData['lokasi_objek_id']);
            if ($namaLo) {
                $namaLokasiObjek = $namaLo->nama_lokasi;
            }
        }
        if (in_array('nop', array_keys($getData)) && $getData['jenis_layanan_id'] != '1') {
            $nop = explode(".", $getData['nop']);
            $blok_urut = explode("-", $nop[4]);
            $nopdefault = [$nop[0], $nop[1], $nop[2], $nop[3], $blok_urut[0], $blok_urut[1], $nop[5]];
        } else {
            $nopdefault = ['35', '07', $getData['kd_kecamatan'] ?? '000', $getData['kd_kelurahan'] ?? '000', '000', '0000', '0'];
        }
        if (!in_array($getData['jenis_layanan_id'], ["6"])) {
            $tahun = Carbon::now()->year;
            $cekLayanan = LayananUpdate::cekNOPLayanan([
                $nopdefault[0],
                $nopdefault[1],
                $nopdefault[2],
                $nopdefault[3],
                '',
                $nopdefault[6],
            ], [
                $nopdefault[4],
                $nopdefault[5]
            ], $tahun);
            if ($cekLayanan) {
                return response()->json(["msg" => 'NOP sudah di Ajukan Pada tahun ' . $tahun, "status" => false]);
            }
        }
        // dd($nopdefault);
        $saveData = [
            'kd_propinsi' => $nopdefault[0],
            'kd_dati2' => $nopdefault[1],
            'kd_kecamatan' => $nopdefault[2],
            'kd_kelurahan' => $nopdefault[3],
            'rt_op' => sprintf("%03s", preg_replace('/[ _]/', "", $rtrwop[0])),
            'rw_op' => sprintf("%02s", preg_replace('/[ _]/', "", $rtrwop[1])),
            'rt_wp' => sprintf("%03s", preg_replace('/[ _]/', "", $rtrwwp[0])),
            'rw_wp' => sprintf("%02s", preg_replace('/[ _]/', "", $rtrwwp[1])),
            'jenis_layanan_id' => $getData['jenis_layanan_id'],
            'nik_wp' => $getData['nik_wp'],
            'nama_wp' => $getData['nama_wp'],
            'alamat_wp' => $getData['alamat_wp'],
            'kelurahan_wp' => $getData['kelurahan_wp'],
            'kecamatan_wp' => $getData['kecamatan_wp'],
            'dati2_wp' => $getData['dati2_wp'],
            'propinsi_wp' => $getData['propinsi_wp'],
            'alamat_op' => $getData['alamat_op'],
            'luas_bumi' => $getData['luas_bumi'],
            'luas_bng' => $getData['luas_bng'],
            'kelompok_objek_id' => $getData['kelompok_objek_id'] ?? null,
            'kelompok_objek_nama' => $namaKelompokObjek,
            'lokasi_objek_id' => $getData['lokasi_objek_id'] ?? null,
            'lokasi_objek_nama' => $namaLokasiObjek,
            'kd_blok' => $nopdefault[4],
            'no_urut' => $nopdefault[5],
            'kd_jns_op' => $nopdefault[6],
            'created_by' => Auth()->user()->id,
            'telp_wp' => $getData['telp_wp'] ?? null
        ];
        if ($getData['jenis_layanan_id'] == '6') {
            $saveData['hasil_pecah_hasil_gabung'] = $request->hasil_pecah_hasil_gabung;
            $saveData['sisa_pecah_total_gabung'] = $request->sisa_pecah_total_gabung;
        }
        $return = ["msg" => "Proses Layanan tidak berhasil.", "status" => false];
        DB::beginTransaction();
        try {
            if ($getData['jenis_layanan_id'] == '2') {
                $saveData['alasan'] = $request->alasan;
            }
            $Layanan = Layanan_objek_temp::updateOrCreate(['id' => $getData['id']], $saveData);
            Layanan_objek_temp::where(['nop_gabung' => $getData['id']])->delete();
            if ($getData['jenis_layanan_id'] == '6') {
                if (!in_array('list_pecah', array_keys($getData))) {
                    return response()->json(["msg" => "Daftar Data Pecah harus di isi.", "status" => false]);
                }
                $luasSisa = $saveData['luas_bumi'];
                foreach ($getData['list_pecah'] as $item) {
                    $rtrwop = explode('/', $item['nop_rt_rw_pecah']);
                    $rtrwop[0] = sprintf("%03s", preg_replace('/[ _]/', "", $rtrwop[0]));
                    $rtrwop[1] = sprintf("%02s", preg_replace('/[ _]/', "", $rtrwop[1]));
                    $rtrwwp = explode('/', $item['rt_rw_wp_pecah']);
                    $rtrwwp[0] = sprintf("%03s", preg_replace('/[ _]/', "", $rtrwwp[0]));
                    $rtrwwp[1] = sprintf("%02s", preg_replace('/[ _]/', "", $rtrwwp[1]));
                    $saveData['nik_wp'] = $item['nik_wp_pecah'];
                    $saveData['nama_wp'] = $item['nama_wp_pecah'];
                    $saveData['telp_wp'] = $item['notelp_wp_pecah'];
                    $saveData['rt_wp'] = $rtrwwp[0];
                    $saveData['rw_wp'] = $rtrwwp[1];
                    $saveData['rt_op'] = $rtrwop[0];
                    $saveData['rw_op'] = $rtrwop[1];
                    $saveData['alamat_wp'] = $item['alamat_wp_pecah'];
                    $saveData['kelurahan_wp'] = $item['kelurahan_wp_pecah'];
                    $saveData['kecamatan_wp'] = $item['kecamatan_wp_pecah'];
                    $saveData['dati2_wp'] = $item['dati2_wp_pecah'];
                    $saveData['propinsi_wp'] = $item['propinsi_wp_pecah'];
                    $saveData['kd_blok'] = '000';
                    $saveData['no_urut'] = '0000';
                    $saveData['kd_jns_op'] = '0';
                    $saveData['luas_bumi'] = $item['luas_bumi_pecah'];
                    $saveData['luas_bng'] = $item['luas_bng_pecah'];
                    $saveData['alamat_op'] = $item['alamat_pecah'];
                    $saveData['hasil_pecah_hasil_gabung'] = null;
                    $saveData['sisa_pecah_total_gabung'] = null;
                    $namaKelompokObjek = null;
                    $namaLokasiObjek = null;
                    if (in_array('kelompok_objek_id_pecah', array_keys($item))) {
                        $getKo = KelompokObjek::find($item['kelompok_objek_id_pecah']);
                        if ($getKo) {
                            $namaKelompokObjek = $getKo->nama_kelompok;
                        }
                    }
                    if (in_array('lokasi_objek_idpecah', array_keys($item))) {
                        $namaLo = LokasiObjek::find($item['lokasi_objek_idpecah']);
                        if ($namaLo) {
                            $namaLokasiObjek = $namaLo->nama_lokasi;
                        }
                    }
                    $saveData['kelompok_objek_id'] = $item['kelompok_objek_id_pecah'];
                    $saveData['lokasi_objek_id'] = $item['lokasi_objek_idpecah'];
                    $saveData['kelompok_objek_nama'] = $namaKelompokObjek;
                    $saveData['lokasi_objek_nama'] = $namaLokasiObjek;
                    // $saveData['nop_gabung']=join($nopdefault);
                    $saveData['nop_gabung'] = $Layanan->id;
                    $luasSisa = $luasSisa - $saveData['luas_bumi'];
                    Layanan_objek_temp::updateOrCreate($saveData);
                }
                // Layanan_objek_temp::where(['id'=>$Layanan->id])->update(['hasil_pecah_hasil_gabung'=>$luasSisa]);
            }
            if ($getData['jenis_layanan_id'] == '7') {
                if (!in_array('list_nop', array_keys($getData))) {
                    return response()->json(["msg" => "Daftar Data Gabung harus di isi.", "status" => false]);
                }
                foreach ($getData['list_nop'] as $item) {
                    $nop = explode(".", $item['nop']);
                    $blok_urut = explode("-", $nop[4]);
                    $saveData['luas_bumi'] = $item['luas_bumi'];
                    $saveData['luas_bng'] = $item['luas_bng'];
                    $saveData['nik_wp'] = $item['nik_wp'];
                    $saveData['nama_wp'] = $item['nama_wp'];
                    $saveData['alamat_op'] = $item['nop_alamat'];
                    $saveData['rt_op'] = $item['nop_rt'];
                    $saveData['rw_op'] = $item['nop_rw'];
                    $saveData['kd_propinsi'] = $nop[0];
                    $saveData['kd_dati2'] = $nop[1];
                    $saveData['kd_kecamatan'] = $nop[2];
                    $saveData['kd_kelurahan'] = $nop[3];
                    $saveData['kd_blok'] = $blok_urut[0];
                    $saveData['no_urut'] = $blok_urut[1];
                    $saveData['kd_jns_op'] = $nop[5];
                    $saveData['alamat_wp'] = $item['alamat_wp'];
                    $saveData['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                    $saveData['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));

                    $saveData['kelurahan_wp'] = $item['kelurahan_wp'];
                    $saveData['kecamatan_wp'] = $item['kecamatan_wp'];
                    $saveData['dati2_wp'] = $item['dati2_wp'];
                    $saveData['propinsi_wp'] = $item['propinsi_wp'];
                    // $saveData['nop_gabung']=join($nopdefault);
                    $saveData['nop_gabung'] = $Layanan->id;
                    Layanan_objek_temp::updateOrCreate($saveData);
                }
            }
            $return = ["msg" => "Proses Layanan berhasil.", "status" => true];
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $msg = $e->getMessage(); //"Proses tidak berhasil";//
            return response()->json(["msg" => $msg, "status" => false]);
        }
        return response()->json($return);
    }
    public function store_kolektif_temp(Request $request)
    {
        $set = [
            "jenis_layanan_id",
            "nik_wp",
            "nama_wp",
            "alamat_wp",
            "rt_rw_wp",
            "kelurahan_wp",
            "kecamatan_wp",
            "dati2_wp",
            "propinsi_wp",
            "alamat_op",
            "rt_rw_op",
            "luas_bumi",
            "luas_bng",
            "kelompok_objek_id",
            "lokasi_objek_id",
            "kecamatan",
            "kelurahan",
            "nop",
            "list_nop",
            "list_pecah",
            "telp_wp"
        ];
        $getData = $request->only($set);
        $rtrwwp = ['000', '00'];
        if (isset($getData['rt_rw_wp'])) {
            $rtrwwp = explode('/', $getData['rt_rw_wp']);
        }
        $rtrwop = explode('/', $getData['rt_rw_op']);
        $namaKelompokObjek = null;
        $namaLokasiObjek = null;
        if (in_array('kelompok_objek_id', array_keys($getData))) {
            $getKo = KelompokObjek::find($getData['kelompok_objek_id']);
            if ($getKo) {
                $namaKelompokObjek = $getKo->nama_kelompok;
            }
        }
        if (in_array('lokasi_objek_id', array_keys($getData))) {
            $namaLo = LokasiObjek::find($getData['lokasi_objek_id']);
            if ($namaLo) {
                $namaLokasiObjek = $namaLo->nama_lokasi;
            }
        }
        if (in_array('nop', array_keys($getData)) && $getData['jenis_layanan_id'] != '1') {
            $nop = explode(".", $getData['nop']);
            $blok_urut = explode("-", $nop[4]);
            $nopdefault = [$nop[0], $nop[1], $nop[2], $nop[3], $blok_urut[0], $blok_urut[1], $nop[5]];
        } else {
            $nopdefault = ['35', '07', $getData['kecamatan'] ?? '000', $getData['kelurahan'] ?? '000', '000', '0000', '0'];
        }
        if (!in_array($getData['jenis_layanan_id'], ["6", "1", "7"])) {
            $tahun = Carbon::now()->year;
            $cekLayanan = LayananUpdate::cekNOPLayanan([
                $nopdefault[0],
                $nopdefault[1],
                $nopdefault[2],
                $nopdefault[3],
                '',
                $nopdefault[6],
            ], [
                $nopdefault[4],
                $nopdefault[5]
            ], $tahun);
            if ($cekLayanan) {
                return response()->json(["msg" => 'NOP sudah di Ajukan Pada tahun ' . $tahun, "status" => false]);
            }
        }
        $rwOP = '00';
        $rwWP = '00';
        if (isset($rtrwop[1])) {
            $rwOP = sprintf("%02s", preg_replace('/[ _]/', "", $rtrwop[1]));
        }
        if (isset($rtrwwp[1])) {
            $rwWP = sprintf("%02s", preg_replace('/[ _]/', "", $rtrwwp[1]));
        }
        $saveData = [
            'kd_propinsi' => $nopdefault[0],
            'kd_dati2' => $nopdefault[1],
            'kd_kecamatan' => $nopdefault[2],
            'kd_kelurahan' => $nopdefault[3],
            'rt_op' => sprintf("%03s", preg_replace('/[ _]/', "", $rtrwop[0])),
            'rw_op' => $rwOP,
            'rt_wp' => sprintf("%03s", preg_replace('/[ _]/', "", $rtrwwp[0])),
            'rw_wp' => $rwWP,
            'jenis_layanan_id' => $getData['jenis_layanan_id'],
            'nik_wp' => $getData['nik_wp'],
            'nama_wp' => $getData['nama_wp'],
            'alamat_wp' => $getData['alamat_wp'],
            'kelurahan_wp' => $getData['kelurahan_wp'],
            'kecamatan_wp' => $getData['kecamatan_wp'],
            'dati2_wp' => $getData['dati2_wp'],
            'propinsi_wp' => $getData['propinsi_wp'],
            'alamat_op' => $getData['alamat_op'],
            'luas_bumi' => $getData['luas_bumi'],
            'luas_bng' => $getData['luas_bng'],
            'kelompok_objek_id' => $getData['kelompok_objek_id'] ?? null,
            'kelompok_objek_nama' => $namaKelompokObjek,
            'lokasi_objek_id' => $getData['lokasi_objek_id'] ?? null,
            'lokasi_objek_nama' => $namaLokasiObjek,
            'kd_blok' => $nopdefault[4],
            'no_urut' => $nopdefault[5],
            'kd_jns_op' => $nopdefault[6],
            'created_by' => Auth()->user()->id,
            'telp_wp' => $getData['telp_wp'] ?? null
        ];
        if ($getData['jenis_layanan_id'] == '6') {
            $saveData['hasil_pecah_hasil_gabung'] = $request->hasil_pecah_hasil_gabung;
            $saveData['sisa_pecah_total_gabung'] = $request->sisa_pecah_total_gabung;
        }
        $return = ["msg" => "Proses Layanan tidak berhasil.", "status" => false];
        DB::beginTransaction();
        try {
            // alasan
            if ($getData['jenis_layanan_id'] == '2') {
                $saveData['alasan'] = $request->alasan;
            }
            $Layanan = Layanan_objek_temp::updateOrCreate($saveData);
            if ($getData['jenis_layanan_id'] == '6') {
                if (!in_array('list_pecah', array_keys($getData))) {
                    return response()->json(["msg" => "Daftar Data Pecah harus di isi.", "status" => false]);
                }
                $luasSisa = $saveData['luas_bumi'];
                foreach ($getData['list_pecah'] as $item) {
                    $rtrwop = explode('/', $item['nop_rt_rw_pecah']);
                    $rtrwop[0] = sprintf("%03s", preg_replace('/[ _]/', "", $rtrwop[0]));
                    $rtrwop[1] = sprintf("%02s", preg_replace('/[ _]/', "", $rtrwop[1]));
                    $rtrwwp = explode('/', $item['rt_rw_wp_pecah']);
                    $rtrwwp[0] = sprintf("%03s", preg_replace('/[ _]/', "", $rtrwwp[0]));
                    $rtrwwp[1] = sprintf("%02s", preg_replace('/[ _]/', "", $rtrwwp[1]));
                    $saveData['nik_wp'] = $item['nik_wp_pecah'];
                    $saveData['nama_wp'] = $item['nama_wp_pecah'];
                    $saveData['telp_wp'] = $item['notelp_wp_pecah'];
                    $saveData['rt_wp'] = $rtrwwp[0];
                    $saveData['rw_wp'] = $rtrwwp[1];
                    $saveData['rt_op'] = $rtrwop[0];
                    $saveData['rw_op'] = $rtrwop[1];
                    $saveData['alamat_wp'] = $item['alamat_wp_pecah'];
                    $saveData['kelurahan_wp'] = $item['kelurahan_wp_pecah'];
                    $saveData['kecamatan_wp'] = $item['kecamatan_wp_pecah'];
                    $saveData['dati2_wp'] = $item['dati2_wp_pecah'];
                    $saveData['propinsi_wp'] = $item['propinsi_wp_pecah'];
                    $saveData['kd_blok'] = '000';
                    $saveData['no_urut'] = '0000';
                    $saveData['kd_jns_op'] = '0';
                    $saveData['luas_bumi'] = $item['luas_bumi_pecah'];
                    $saveData['luas_bng'] = $item['luas_bng_pecah'];
                    $saveData['alamat_op'] = $item['alamat_pecah'];
                    $saveData['hasil_pecah_hasil_gabung'] = null;
                    $saveData['sisa_pecah_total_gabung'] = null;
                    $namaKelompokObjek = null;
                    $namaLokasiObjek = null;
                    if (in_array('kelompok_objek_id_pecah', array_keys($item))) {
                        $getKo = KelompokObjek::find($item['kelompok_objek_id_pecah']);
                        if ($getKo) {
                            $namaKelompokObjek = $getKo->nama_kelompok;
                        }
                    }
                    if (in_array('lokasi_objek_idpecah', array_keys($item))) {
                        $namaLo = LokasiObjek::find($item['lokasi_objek_idpecah']);
                        if ($namaLo) {
                            $namaLokasiObjek = $namaLo->nama_lokasi;
                        }
                    }
                    $saveData['kelompok_objek_id'] = $item['kelompok_objek_id_pecah'];
                    $saveData['lokasi_objek_id'] = $item['lokasi_objek_idpecah'];
                    $saveData['kelompok_objek_nama'] = $namaKelompokObjek;
                    $saveData['lokasi_objek_nama'] = $namaLokasiObjek;
                    // $saveData['nop_gabung']=join($nopdefault);
                    $saveData['nop_gabung'] = $Layanan->id;
                    $luasSisa = $luasSisa - $saveData['luas_bumi'];
                    Layanan_objek_temp::updateOrCreate($saveData);
                }
                // Layanan_objek_temp::where(['id'=>$Layanan->id])->update(['hasil_pecah_hasil_gabung'=>$luasSisa]);
            }
            if ($getData['jenis_layanan_id'] == '7') {
                if (!in_array('list_nop', array_keys($getData))) {
                    return response()->json(["msg" => "Daftar Data Gabung harus di isi.", "status" => false]);
                }
                foreach ($getData['list_nop'] as $item) {
                    $nop = explode(".", $item['nop']);
                    $blok_urut = explode("-", $nop[4]);
                    $saveData['luas_bumi'] = $item['luas_bumi'];
                    $saveData['luas_bng'] = $item['luas_bng'];
                    $saveData['nik_wp'] = $item['nik_wp'];
                    $saveData['nama_wp'] = $item['nama_wp'];
                    $saveData['alamat_op'] = $item['nop_alamat'];
                    $saveData['rt_op'] = $item['nop_rt'];
                    $saveData['rw_op'] = $item['nop_rw'];
                    $saveData['kd_propinsi'] = $nop[0];
                    $saveData['kd_dati2'] = $nop[1];
                    $saveData['kd_kecamatan'] = $nop[2];
                    $saveData['kd_kelurahan'] = $nop[3];
                    $saveData['kd_blok'] = $blok_urut[0];
                    $saveData['no_urut'] = $blok_urut[1];
                    $saveData['kd_jns_op'] = $nop[5];
                    $saveData['alamat_wp'] = $item['alamat_wp'];
                    $saveData['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $item['rt_wp']));
                    $saveData['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $item['rw_wp']));

                    $saveData['kelurahan_wp'] = $item['kelurahan_wp'];
                    $saveData['kecamatan_wp'] = $item['kecamatan_wp'];
                    $saveData['dati2_wp'] = $item['dati2_wp'];
                    $saveData['propinsi_wp'] = $item['propinsi_wp'];
                    // $saveData['nop_gabung']=join($nopdefault);
                    $saveData['nop_gabung'] = $Layanan->id;
                    Layanan_objek_temp::updateOrCreate($saveData);
                }
            }
            $return = ["msg" => "Proses Layanan berhasil.", "status" => true];
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $msg = "Proses tidak berhasil"; //$e->getMessage();//
            log::emergency($e->getMessage());
            return response()->json(["msg" => $msg, "status" => false]);
        }
        return response()->json($return);
    }
    public function search_kolektif_temp(Request $request)
    {
        $select = [
            'layanan_objek_temp.rt_op',
            'layanan_objek_temp.rw_op',
            'layanan_objek_temp.rt_wp',
            'layanan_objek_temp.rw_wp',
            'layanan_objek_temp.jenis_layanan_id',
            'layanan_objek_temp.nik_wp',
            'layanan_objek_temp.nama_wp',
            'layanan_objek_temp.alamat_wp',
            'layanan_objek_temp.kelurahan_wp',
            'layanan_objek_temp.kecamatan_wp',
            'layanan_objek_temp.dati2_wp',
            'layanan_objek_temp.propinsi_wp',
            'layanan_objek_temp.alamat_op',
            'layanan_objek_temp.luas_bumi',
            'layanan_objek_temp.luas_bng',
            'layanan_objek_temp.kelompok_objek_id',
            'layanan_objek_temp.kelompok_objek_nama',
            'layanan_objek_temp.lokasi_objek_id',
            'layanan_objek_temp.lokasi_objek_nama',
            'layanan_objek_temp.id',
            'layanan_objek_temp.kd_propinsi',
            'layanan_objek_temp.kd_dati2',
            'layanan_objek_temp.kd_kecamatan',
            'layanan_objek_temp.kd_kelurahan',
            'layanan_objek_temp.kd_blok',
            'layanan_objek_temp.no_urut',
            'layanan_objek_temp.kd_jns_op',
            "layanan_objek_temp.nop_gabung",
            "layanan_objek_temp.telp_wp",
            "layanan_objek_temp.alasan",
            "jenis_layanan.nama_layanan"
        ];
        $dataSet = Layanan_objek_temp::select($select)
            ->where(['layanan_objek_temp.jenis_layanan_id' => $request->jenis, 'layanan_objek_temp.created_by' => Auth()->user()->id])
            ->join('jenis_layanan', 'jenis_layanan.id', "=", "layanan_objek_temp.jenis_layanan_id")
            ->orderBy('layanan_objek_temp.id', 'asc')
            ->get();

        $this->page = Jenis_layanan::where(['id' => $request->jenis])->get()->first()->nama_layanan;

        $datatables = DataTables::of($dataSet);
        return $datatables->addColumn('option', function ($data) {
            $page = gallade::clean($data->nama_layanan);
            $btnDelete = gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Delete" data-delete="' . $data->id . '" data-reload="' . $data->jenis_layanan_id . '"', 'danger');
            $btnEdit = gallade::buttonInfo('<i class="fas fa-file-signature"></i>', 'title="Edit" data-target="#edit-' . $page . '" data-edit="' . $data->id . '" data-reload="' . $data->jenis_layanan_id . '"', 'warning');
            $input = "<input type='hidden' name='data[" . $data->jenis_layanan_id . "][]' value='" . $data->id . "'>";
            if ($data->nop_gabung != "" || $data->nop_gabung != null) {
                $btnDelete = "";
                $btnEdit = "";
            }
            return $input . '<div class="btn-group">' . $btnDelete . $btnEdit . "</div>";
        })
            ->addColumn('rt_rw_op', function ($data) {
                return $data->rt_op . '/' . $data->rw_op;
            })
            ->addColumn('rt_rw_wp', function ($data) {
                return $data->rt_wp . '/' . $data->rw_wp;
            })
            ->addColumn('nop', function ($data) {
                if (($data->nop_gabung != "" || $data->nop_gabung != null) && $data->jenis_layanan_id == '6') {
                    return '-- Pecah --';
                }
                if (($data->nop_gabung == "" || $data->nop_gabung == null) && $data->jenis_layanan_id == '7') {
                    return '-- Hasil Gabung --';
                }
                return $data->kd_propinsi . '.' .
                    $data->kd_dati2 . "." .
                    $data->kd_kecamatan . "." .
                    $data->kd_kelurahan . "." .
                    $data->kd_blok . "-" .
                    $data->no_urut . "." .
                    $data->kd_jns_op;
            })
            ->addIndexColumn()
            ->removeColumn(['id'])
            ->make(true);
    }
    public function kolektif_refresh()
    {
        Layanan_objek_temp::where(['created_by' => Auth()->user()->id])->delete();
        return redirect(route('kolektif_online.index'));
    }
    public function kolektif_edit(Request $request)
    {
        $list = $request->only('id');
        $return = ["msg" => "Proses tidak berhasil.", "status" => false];
        if ($list) {
            try {
                $where = "layanan_objek_temp.nop_gabung='" . $list['id'] . "' or  layanan_objek_temp.id='" . $list['id'] . "'";
                $temp = Layanan_objek_temp::whereRaw($where)
                    ->leftJoin(db::raw("spo.ref_kelurahan ref_kelurahan"), function ($join) {
                        $join->On('ref_kelurahan.kd_kecamatan', '=', 'layanan_objek_temp.kd_kecamatan')
                            ->On('ref_kelurahan.kd_kelurahan', '=', 'layanan_objek_temp.kd_kelurahan');
                    })
                    ->leftJoin(db::raw("spo.ref_kecamatan ref_kecamatan"), function ($join) {
                        $join->On('ref_kecamatan.kd_kecamatan', '=', 'layanan_objek_temp.kd_kecamatan');
                    })
                    ->get();
                $temp = collect($temp)->sortBy('id')->toArray();
                $data = [];
                $listTable = [];
                $listTablePecah = [];
                foreach ($temp as $item) {
                    if ($item['nop_gabung'] == 'null' || $item['nop_gabung'] == '0' || $item['nop_gabung'] == '') {
                        $data = $item;
                        $data['rt_rw_op'] = $data['rt_op'] . "/" . $data['rw_op'];
                        $data['rt_rw_wp'] = $data['rt_wp'] . "/" . $data['rw_wp'];
                        $data['nop'] = $item['kd_propinsi'] . "." .
                            $item['kd_dati2'] . "." .
                            $item['kd_kecamatan'] . "." .
                            $item['kd_kelurahan'] . "." .
                            $item['kd_blok'] . "-" .
                            $item['no_urut'] . "." .
                            $item['kd_jns_op'];
                        if ($item['jenis_layanan_id'] == '3') {
                            $get = Http::get('localhost/simpbb/api/ceknop', [
                                'nop' => $data['nop']
                            ]);
                            $get = (array)$get->object()->data;
                            foreach ($get as $key => $value) {
                                if ($value != null) {
                                    $data[$key . '_label'] = $value;
                                }
                            }
                        }
                    } else {
                        if ($item['jenis_layanan_id'] == '6') {
                            $idTab = Str::random(5);
                            // dd($item);
                            $ob = [
                                '<b>Alamat</b> : <i>' . $item['alamat_op'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][alamat_pecah]" value="' . $item['alamat_op'] . '">',
                                '<b>RT/RW</b> : <i>' . $item['rt_op'] . "/" . $item['rw_op'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][nop_rt_rw_pecah]" value="' . $item['rt_op'] . "/" . $item['rw_op'] . '">',
                                '<b>Luas Bumi</b> : <i>' . $item['luas_bumi'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][luas_bumi_pecah]" data-count="pecah" value="' . $item['luas_bumi'] . '">',
                                '<b>Luas Bangungan</b> : <i>' . $item['luas_bng'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][luas_bng_pecah]" value="' . $item['luas_bng'] . '">',
                                '<b>Jenis Penggunaan Bangunan </b> : <i>' . $item['kelompok_objek_nama'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][kelompok_objek_id_pecah]" value="' . $item['kelompok_objek_id'] . '">',
                                '<b>Lokasi Objek</b> : <i>' . $item['lokasi_objek_nama'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][lokasi_objek_idpecah]" value="' . $item['lokasi_objek_id'] . '">',
                            ];
                            $wp = [
                                '<b>NIK</b> : <i>' . $item['nik_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][nik_wp_pecah]" value="' . $item['nik_wp'] . '">',
                                '<b>Nama</b> : <i>' . $item['nama_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][nama_wp_pecah]" value="' . $item['nama_wp'] . '">',
                                '<b>No. Telp</b> : <i>' . $item['telp_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][notelp_wp_pecah]" value="' . $item['telp_wp'] . '">',
                                '<b>RT/RW</b> : <i>' . $item['rt_wp'] . "/" . $item['rw_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][rt_rw_wp_pecah]" value="' . $item['rt_wp'] . "/" . $item['rw_wp'] . '">',
                            ];
                            $dw = [
                                '<b>Alamat</b> : <i>' . $item['alamat_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][alamat_wp_pecah]" value="' . $item['alamat_wp'] . '">',
                                '<b>Kelurahan </b> : <i>' . $item['kelurahan_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][kelurahan_wp_pecah]" value="' . $item['kelurahan_wp'] . '">',
                                '<b>Kecamatan </b> : <i>' . $item['kecamatan_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][kecamatan_wp_pecah]" value="' . $item['kecamatan_wp'] . '">',
                                '<b>Kota / Kabupaten</b> : <i>' . $item['dati2_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][dati2_wp_pecah]" value="' . $item['dati2_wp'] . '">',
                                '<b>Propinsi </b> : <i>' . $item['propinsi_wp'] . '</i><input type="hidden" name="list_pecah[' . $idTab . '][propinsi_wp_pecah]" value="' . $item['propinsi_wp'] . '">',
                            ];
                            $del = '<span class="btn btn-danger btn-flat data-delete un" data-list-id="' . $idTab . '" title="Hapus Data "><i class="fas fa-trash"></i></span>';
                            $edit = '<span class="btn btn-warning btn-flat un" data-list-id="' . $idTab . '" title="Edit Data " ><i class="fas fa-file-signature"></i></span>';
                            $button = '<div clas="btn-group">' . $del . $edit . '</div>';
                            $listTablePecah[] = [
                                'no' => '',
                                'ob' => "<ul class='m-0'><li>" . join('</li><li>', $ob) . "</li></ul>",
                                'wp' => "<ul class='m-0'><li>" . join('</li><li>', $wp) . "</li></ul>",
                                'dw' => "<ul class='m-0'><li>" . join('</li><li>', $dw) . "</li></ul>",
                                'option' => $button
                            ];
                        }
                        if ($item['jenis_layanan_id'] == '7') {
                            // dd($item);
                            $nopStr = $item['kd_propinsi'] . "." .
                                $item['kd_dati2'] . "." .
                                $item['kd_kecamatan'] . "." .
                                $item['kd_kelurahan'] . "." .
                                $item['kd_blok'] . "-" .
                                $item['no_urut'] . "." .
                                $item['kd_jns_op'];
                            $idTab = Str::random(5);
                            $listTable[] = [
                                'no' => "<input type='hidden' name='list_nop[" . $nopStr . "][luas_bumi]' value='" . $item['luas_bumi'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][luas_bng]' value='" . $item['luas_bng'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nik_wp]' value='" . $item['nik_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nama_wp]' value='" . $item['nama_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_alamat]' value='" . $item['alamat_op'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_rw]' value='" . $item['rw_op'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_rt]' value='" . $item['rt_op'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][rw_wp]' value='" . $item['rw_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][rt_wp]' value='" . $item['rt_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_kelurahan]' value='" . $item['kd_kelurahan'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_kecamatan]' value='" . $item['kd_kecamatan'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][alamat_wp]' value='" . $item['alamat_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][kelurahan_wp]' value='" . $item['kelurahan_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][kecamatan_wp]' value='" . $item['kecamatan_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][dati2_wp]' value='" . $item['dati2_wp'] . "'>
                                    <input type='hidden' name='list_nop[" . $nopStr . "][propinsi_wp]' value='" . $item['propinsi_wp'] . "'>
                                ",
                                "nik" => $item['nik_wp'],
                                "nama" => $item['nama_wp'],
                                'nop' => $nopStr . "<input type='hidden' name='list_nop[" . $nopStr . "][nop]' value='" . $nopStr . "'>",
                                "alamat" => $item['alamat_op'],
                                "rt_rw" => $item['rt_op'] . '/' . $item['rw_op'],
                                "kelurahan" => $item['nm_kelurahan'],
                                "kecamatan" => $item['nm_kecamatan'],
                                'luas_bumi' => $item['luas_bumi'],
                                'luas_bangunan' => $item['luas_bng'],
                                'option' => gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Hapus NOP" data-list-id="' . $nopStr . '" data-tag-tab="' . $idTab . '"'),
                            ];
                        }
                    }
                }
                $tag['list_nop'] = gallade::generateinTbody($listTable, "Data Masih Kosong", count($listTable));
                $tag['list_pecah'] = gallade::generateinTbody($listTablePecah, "Data Masih Kosong", count($listTablePecah));
                $return = ["msg" => "Proses berhasil.", "status" => true, 'data' => $data, 'tag' => $tag];
            } catch (\Exception $e) {
                $msg = $e->getMessage(); //"Proses tidak berhasil";//
                return response()->json(["msg" => $msg, "status" => false]);
            }
        }
        return response()->json($return);
    }
    public function kolektif_delete(Request $request)
    {
        $list = $request->only('id');
        if ($list) {
            DB::beginTransaction();
            try {
                $temp = Layanan_objek_temp::where(['id' => $list['id']]);
                $getTemp = $temp->get();
                if ($getTemp->count() > 0) {
                    $getTemp = $getTemp->first()->toArray();

                    $where = [
                        'jenis_layanan_id' => $getTemp['jenis_layanan_id'],
                        'nop_gabung' => $getTemp['id']
                    ];
                    Layanan_objek_temp::where($where)->delete();
                }
                $temp->delete();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $msg = $e->getMessage(); //"Proses tidak berhasil";//
                return response()->json(["msg" => $msg, "status" => false]);
            }
        }
        $return = ["msg" => "Proses berhasil.", "status" => true];
        return response()->json($return);
    }
    public function mutasipecah()
    {
        $Jenis_layanan = Jenis_layanan::where(['id' => 6])->get()->first();
        $kecamatan = Kecamatan::login()->orderBy('nm_kecamatan', 'asc')->get();
        $kelurahan = [];
        // dd($Jenis_layanan->toArray());
        $KelompokObjek = KelompokObjek::all(); //7
        $LokasiObjek = LokasiObjek::all();
        $jenisDokumen = Dokumen::list();
        $jenisAgama = [
            '1' => 'Islam',
            '2' => 'Protestan',
            '3' => 'Katolik',
            '4' => 'Hindu',
            '5' => 'Budha',
            '6' => 'Konghucu',
            '7' => 'Lain-lain'
        ];
        return view('layanan.form-baru-mutasi_pecah', compact('Jenis_layanan', 'kecamatan', 'jenisAgama', 'KelompokObjek', 'LokasiObjek', 'jenisDokumen'));
    }
    public function ceknoptambah(Request $request)
    {
        $nop = $request->only('nop');
        $default = ['status' => false, 'msg' => "NOP tidak ditemukan."];
        if (!$nop) return response()->json($default);
        // $get = Layanan_conf::cek_nop($nop['nop']);
        $nopFormat = explode(".", $nop['nop']);
        $blok_urut = explode("-", $nopFormat[4]);
        $tahun = Carbon::now()->year;
        $cekLayanan = LayananUpdate::cekNOPLayanan($nopFormat, $blok_urut, $tahun);
        if ($cekLayanan) {
            $jenis = $request->only('jenis');
            if (in_array('jenis', array_keys($jenis))) {
                if (!in_array($jenis['jenis'], ['7'])) {
                    return response()->json(["msg" => 'NOP sudah di Ajukan Pada tahun ' . $tahun, "status" => false]);
                }
            } else {
                return response()->json(["msg" => 'NOP sudah di Ajukan Pada tahun ' . $tahun, "status" => false]);
            }
        }
        $send = ['nop' => $nop['nop']];
        if ($request->only('jenis')) {
            $send = ['nop' => $nop['nop'], 'jenis' => $request->only('jenis')];
        }
        $get = Http::get('localhost/simpbb/api/ceknop', $send);
        if ($get) {
            $getData = (array)$get->object();
            if (!$getData['status']) {
                return response()->json(["msg" => $getData['msg'], "status" => false]);
            }
            // dd($getData);
            // $get = (array)$get;
            $get = (array)$getData['data'];
            $nopStr = $nop['nop'];
            $idTab = Str::random(5);
            $tagTr[] = [
                'no' => "
                    <input type='hidden' name='list_nop[" . $nopStr . "][luas_bumi]' value='" . $get['luas_bumi'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][luas_bng]' value='" . $get['luas_bng'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nik_wp]' value='" . $get['nik_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nama_wp]' value='" . $get['nama_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_alamat]' value='" . $get['alamat_op'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_rw]' value='" . $get['nop_rw'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_rt]' value='" . $get['nop_rt'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][rw_wp]' value='" . $get['rw_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][rt_wp]' value='" . $get['rt_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_kelurahan]' value='" . $get['nop_kelurahan'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][nop_kecamatan]' value='" . $get['nop_kecamatan'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][alamat_wp]' value='" . $get['alamat_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][kelurahan_wp]' value='" . $get['kelurahan_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][kecamatan_wp]' value='" . $get['kecamatan_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][dati2_wp]' value='" . $get['dati2_wp'] . "'>
                    <input type='hidden' name='list_nop[" . $nopStr . "][propinsi_wp]' value='" . $get['propinsi_wp'] . "'>
                ",
                "nik" => $get['nik_wp'],
                "nama" => $get['nama_wp'],
                'nop' => $nopStr . "<input type='hidden' name='list_nop[" . $nopStr . "][nop]' value='" . $nopStr . "'>",
                "alamat" => $get['alamat_op'],
                "rt_rw" => $get['nop_rt'] . '/' . $get['nop_rw'],
                "kelurahan" => $get['nop_kelurahan'],
                "kecamatan" => $get['nop_kecamatan'],
                'luas_bumi' => $get['luas_bumi'],
                'luas_bangunan' => $get['luas_bng'],
                'option' => gallade::buttonInfo('<i class="fas fa-trash"></i>', 'title="Hapus NOP" data-list-id="' . $nopStr . '" data-tag-tab="' . $idTab . '"', 'warning')
            ];
            $riwayat = LayananUpdate::riwayatPembayaran(['nop' => $nop['nop']]);
            $default = [
                'status' => true,
                'msg' => "NOP ditemukan.",
                'data' => gallade::generateinTbody($tagTr, "Data Masih Kosong", '11'),
                'data_list_id' => $nopStr,
                'tab' => '<li class="nav-item mb-1" id="link-' . $idTab . '"><a class="nav-link tabs-h" href="#tab-' . $idTab . '" data-toggle="tab"> <i class="fas fa-book"></i> ' . $nop['nop'] . ' </a></li>',
                'tag' => '<div class="tab-pane fade show" id="tab-' . $idTab . '">
                        <div class="card">
                            <div class="col-sm-12  body-scroll">
                                <table class="table table-bordered table-striped table-counter table-sm">
                                    <thead class="text-center">
                                        <tr>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">NOP</th>
                                            <th rowspan="2">Tahun</th>
                                            <th rowspan="2">Nama</th>
                                            <th rowspan="2">PBB</th>
                                            <th colspan="3">Pembayaran</th>
                                            <th rowspan="2">Keterangan</th>
                                        </tr>
                                        <tr>
                                            <th>Pokok</th>
                                            <th>Denda</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody data-name="' . $nop['nop'] . '" class="tag-html">' . $riwayat . "</tbody>
                                </table>
                            </div>
                        </div>
                        </div>"
            ];
        }
        return response()->json($default);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preview_(Request $request)
    {
        $default = response()->json(['status' => false, 'msg' => "Format tidak sesuai"]);
        if ($request->hasFile('import_excel')) {
            $file = $request->file('import_excel');
            $path = $file->getRealPath();
            $ext = $file->extension();
            if (!in_array($ext, ["xlsx", "csv", "xls"])) {
                return $default;
            }
            ini_set('memory_limit', '-1');
            $IlayananKolektif = new layanan_kolektif();
            $array = Excel::toArray($IlayananKolektif, $file);
            //$IlayananKolektif->getUnknownSheet()
            $jenisLayanan = [
                "form_pembetulan" => '3',
                "form_mutasi_penuh" => '9',
                "form_mutasi_gabung" => '7',
                "form_mutasi_pecah" => '6',
                "form_pembatalan" => '2',
                "form_data_baru" => '1'
            ];
            $list = [];
            foreach ($array as $key => $value) {
                $key = strtolower(str_replace(' ', '_', $key));
                if (in_array($key, array_keys($jenisLayanan))) {
                    $list[$jenisLayanan[$key]] = $this->validasiData($value, $jenisLayanan[$key]);
                }
            }
            $checkDupliacte = collect();
        }
    }
    private function validasiData($row, $rule = '-')
    {
        $rule_set = [
            "nop" => "required|min:24|max:24|regex:/^(([0-9]){2}\.){2}(([0-9]){3}\.){2}([0-9]){3}\-([0-9]){4}\.([0-9])$/u",
        ];
        if ($rule == '3') {
            $rule_set['nama_wp'] = 'required';
            $rule_set['alamat_wp'] = 'required';
            $rule_set['alamat_op'] = 'required';
            $rule_set['luas_tanah_m2'] = 'required';
            $rule_set['luas_bangunan_m2'] = 'required';
            $rule_set['nik_wp'] = 'required';
        }
        $messages = [
            'required' => 'Kolom :attribute Harus di isi.',
            'min' => 'Minimal panjang :attribute :min digit',
            'max' => 'Maksimal panjang :attribute :max digit',
            'regex' => 'Format :attribute tidak sesuai.'
        ];
        $result = [];
        foreach ($row as $item) {
            $validation = Validator::make($item, $rule_set, $messages);
            if (!$validation->fails()) {
                $result[] = $item;
            }
        }
        return $result;
    }
    // preview excel close
    public function preview(Request $request)
    {
        $default = response()->json(['status' => false, 'msg' => "Format tidak sesuai"]);
        if ($request->hasFile('import_excel')) {
            $file = $request->file('import_excel');
            $path = $file->getRealPath();
            $ext = $file->extension();
            if (!in_array($ext, ["xlsx", "csv", "xls"])) {
                return $default;
            }
            ini_set('memory_limit', '-1');
            $array = Excel::toArray(new layanan_kolektif(), $file);
            $checkDupliacte = collect();
            $list_data = [];
            $data_false = [];
            $no_false = 1;
            $rule = [
                "no" => "required",
                "layanan" => "required",
                "nik" => "required|min:16|max:16",
                "nama" => "required",
                "no_telp" => "required",
                "alamat" => "required",
                "rtrw" => "required|regex:/^([0-9])*\/([0-9])*$/u",
                "kelurahan" => "required",
                "kecamatan" => "required",
                "kabupatenkota" => "required",
                "propinsi" => "required",
                "keterangan" => "",
                "nop" => "required|min:24|max:24|regex:/^(([0-9]){2}\.){2}(([0-9]){3}\.){2}([0-9]){3}\-([0-9]){4}\.([0-9])$/u",
                "nop_induk" => "",
                "alamat_objek" => "",
                "rtrw_objek" => "",
                "kelurahan_objek" => "",
                "kecamatan_objek" => "",
                "luas_bumi" => "",
                "hasil_pecah" => "",
                "sisa_pecah" => "",
                "hasil_gabung" => "",
                "total_gabung" => "",
                "luas_bangunan" => "",
                "kelompok_objek" => "",
                "lokasi_objek" => ""
            ];
            $messages = [
                'required' => 'Kolom :attribute Harus di isi.',
                'min' => 'Minimal panjang :attribute :min digit',
                'max' => 'Maksimal panjang :attribute :max digit',
                'regex' => 'Format :attribute tidak sesuai.'
            ];
            // validasi input
            foreach ($array[0] as $row) {
                $kd_layanan = explode('.', $row['layanan']);
                $rule_set = $rule;
                if ($kd_layanan[0] == '1') {
                    $rule_set['nop'] = '';
                    $rule_set['kelurahan_objek'] = 'required';
                    $rule_set['kecamatan_objek'] = 'required';
                    $rule_set['alamat_objek'] = 'required';
                    $rule_set['rtrw_objek'] = 'required|regex:/^([0-9])*\/([0-9])*$/u';
                }
                if ($kd_layanan[0] == '3') {
                    $rule_set['luas_bumi'] = 'required';
                    $rule_set['luas_bangunan'] = 'required';
                }
                if ($kd_layanan[0] == '8') {
                    $rule_set['alamat_objek'] = 'required';
                    $rule_set['rtrw_objek'] = 'required';
                }
                if ($kd_layanan[0] == '6') {
                    $rule_set['hasil_pecah'] = 'required';
                    // $rule_set['sisa_pecah']='required';
                }
                if ($kd_layanan[0] == '7') {
                    $rule_set['hasil_gabung'] = 'required';
                    $rule_set['nop_induk'] = 'required';
                    // $rule_set['total_gabung']='required';
                }
                $validation = Validator::make($row, $rule_set, $messages);
                if (!$validation->fails()) {
                    $row['unique'] = $row['nik'] . '.' . $kd_layanan[0] . '.' . $row['nop'];
                    $list_data[$row['no']] = Arr::only($row, array_keys($rule_set));
                    $checkDupliacte[$row['no']] = (object) $row;
                } else {
                    $error[] = $validation->errors()->count();
                    if ($validation->errors()->count() < 11) {
                        $data_false[$no_false] = Arr::only($row, array_keys($rule_set));
                        //$data_false[$no_false]=Arr::only($row,array_keys($rule_set));
                        //'Format tidak sesuai. <span class="hidden">'..'</span>';
                        $data_false[$no_false]['msg'] = 'Format tidak sesuai.';
                        $data_false[$no_false]['no'] = '';
                        $layanan = explode('.', $row['layanan']);
                        if (count($layanan) == 2) {
                            $data_false[$no_false]['sisa_pecah'] = $row['luas_bumi'] - $row['hasil_pecah'];
                            $data_false[$no_false]['total_gabung'] = $row['luas_bumi'] + $row['hasil_gabung'];
                            $data_false[$no_false]['layanan'] = $layanan[1];
                            if ($row['lokasi_objek']) {
                                $data_false[$no_false]['lokasi_objek'] = explode('.', $row['lokasi_objek'])[1];
                            }
                            if ($row['kelompok_objek']) {
                                $data_false[$no_false]['kelompok_objek'] = explode('.', $row['kelompok_objek'])[1];
                            }
                        }
                        foreach ($validation->errors()->toArray() as $key => $setV) {
                            $data_false[$no_false][$key] = '<span class="badge badge-warning ">' . join('|', $setV) . '</span>';
                        }
                        $no_false++;
                    }
                }
            }
            // remoove data double
            $removeDuplicate = $checkDupliacte->duplicates(['unique']);
            if (count($removeDuplicate) > 0) {
                foreach ($removeDuplicate->toArray() as $key => $value) {
                    $set = $list_data[$key];
                    $data_false[$key] = $set;
                    $data_false[$key]['no'] = '';
                    $data_false[$key]['msg'] = 'Data lebih dari satu.';
                    $layanan = explode('.', $set['layanan']);
                    if ($layanan[0] == '6') {
                        $data_false[$key]['sisa_pecah'] = $set['luas_bumi'] - $set['hasil_pecah'];
                    }
                    if ($layanan[0] == '7') {
                        $data_false[$key]['total_gabung'] = $set['luas_bumi'] + $set['hasil_gabung'];
                    }
                    $data_false[$key]['layanan'] = $layanan[1];
                    if ($row['lokasi_objek']) {
                        $data_false[$no_false]['lokasi_objek'] = explode('.', $row['lokasi_objek'])[1];
                    }
                    if ($row['kelompok_objek']) {
                        $data_false[$no_false]['kelompok_objek'] = explode('.', $row['kelompok_objek'])[1];
                    }
                    $no_false++;
                    unset($list_data[$key]);
                }
            }
            $result_data = [];
            $no = 1;
            // save temp
            DB::beginTransaction();
            try {
                $tempSave = [];
                Layanan_objek_temp::where(['created_by' => Auth()->user()->id])->delete();
                foreach ($list_data as $item) {
                    $layanan = explode('.', $item['layanan']);
                    $rtrw = explode('/', $item['rtrw']);
                    $rtrw_objek = explode('/', $item['rtrw_objek']);
                    if ($layanan[0] == '1') {
                        $item['nop'] = '35.07.999.999.999-9999.0';
                    }
                    $item['sisa_pecah_total_gabung'] = $item['hasil_pecah_hasil_gabung'] = 0;
                    if ($layanan[0] == '6') {
                        $item['sisa_pecah_total_gabung'] = $item['hasil_pecah'];
                        $item['hasil_pecah_hasil_gabung'] = $item['luas_bumi'] - $item['hasil_pecah'];
                    }
                    if ($layanan[0] == '7') {
                        $item['sisa_pecah_total_gabung'] = $item['hasil_gabung'];
                        $item['hasil_pecah_hasil_gabung'] = $item['luas_bumi'] + $item['hasil_gabung'];
                    }
                    $lokasi_objek = ['', ''];
                    $kelompok_objek = ['', ''];
                    if ($item['lokasi_objek']) {
                        $lokasi_objek = explode('.', $item['lokasi_objek']);
                    }
                    if ($item['kelompok_objek']) {
                        $kelompok_objek = explode('.', $item['kelompok_objek']);
                    }
                    $nop = explode(".", $item['nop']);
                    $blok_urut = explode("-", $nop[4]);
                    $tempSave[] = [
                        'nik_wp' => $item['nik'],
                        'nama_wp' => $item['nama'],
                        'alamat_wp' => $item['alamat'],
                        'rt_wp' => $rtrw[0],
                        'rw_wp' => $rtrw[1],
                        'kelurahan_wp' => $item['kelurahan'],
                        'kecamatan_wp' => $item['kecamatan'],
                        'dati2_wp' => $item['kabupatenkota'],
                        'propinsi_wp' => $item['propinsi'],
                        'kd_propinsi' => $nop[0],
                        'kd_dati2' => $nop[1],
                        'kd_kecamatan' => $nop[2],
                        'kd_kelurahan' => $nop[3],
                        'kd_blok' => $blok_urut[0],
                        'no_urut' => $blok_urut[1],
                        'kd_jns_op' => $nop[5],
                        'luas_bumi' => $item['luas_bumi'],
                        'luas_bng' => $item['luas_bangunan'],
                        'kelompok_objek_id' => $kelompok_objek[0],
                        'kelompok_objek_nama' => $kelompok_objek[1],
                        'lokasi_objek_id' => $lokasi_objek[0],
                        'lokasi_objek_nama' => $lokasi_objek[1],
                        'telp_wp' => $item['no_telp'],
                        'alamat_op' => $item['alamat_objek'],
                        'rt_op' => $rtrw_objek[0],
                        'rw_op' => $rtrw_objek[1],
                        'sisa_pecah_total_gabung' => $item['sisa_pecah_total_gabung'],
                        'hasil_pecah_hasil_gabung' => $item['hasil_pecah_hasil_gabung'],
                        'created_by' => Auth()->user()->id,
                        'jenis_layanan_id' => $layanan[0],
                        'nop_gabung' => $item['nop_induk']
                    ];
                    // if ($layanan[0] == '7') {
                    //     $listGabung = explode(',', $item['nop_gabung']);
                    //     foreach ($listGabung as $itemz) {
                    //         $nopz = explode(".", $itemz);
                    //         $blok_urutz = explode("-", $nopz[4]);
                    //         $tempSave[] = [
                    //             'nik_wp' => $item['nik'],
                    //             'nama_wp' => $item['nama'],
                    //             'alamat_wp' => $item['alamat'],
                    //             'rt_wp' => $rtrw[0],
                    //             'rw_wp' => $rtrw[1],
                    //             'kelurahan_wp' => $item['kelurahan'],
                    //             'kecamatan_wp' => $item['kecamatan'],
                    //             'dati2_wp' => $item['kabupatenkota'],
                    //             'propinsi_wp' => $item['propinsi'],
                    //             'kd_propinsi' => $nopz[0],
                    //             'kd_dati2' => $nopz[1],
                    //             'kd_kecamatan' => $nopz[2],
                    //             'kd_kelurahan' => $nopz[3],
                    //             'kd_blok' => $blok_urutz[0],
                    //             'no_urut' => $blok_urutz[1],
                    //             'kd_jns_op' => $nopz[5],
                    //             'luas_bumi' => 0,
                    //             'luas_bng' => 0,
                    //             'kelompok_objek_id' => '',
                    //             'kelompok_objek_nama' => '',
                    //             'lokasi_objek_id' => '',
                    //             'lokasi_objek_nama' => '',
                    //             'telp_wp' => '',
                    //             'alamat_op' => '',
                    //             'rt_op' => '',
                    //             'rw_op' => '',
                    //             'sisa_pecah_total_gabung' => 0,
                    //             'hasil_pecah_hasil_gabung' => 0,
                    //             'created_by' => Auth()->user()->id,
                    //             'jenis_layanan_id' => '7',
                    //             'nop_gabung' => $nop[0] . $nop[1] . $nop[2] . $nop[3] . $blok_urut[0] . $blok_urut[1] . $nop[5]
                    //         ];
                    //     }
                    // }
                }
                // save batch
                Layanan_objek_temp::insert($tempSave);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                //"Terjadi Kesalahan, Harap Hubungi Admin";//$e->getMessage();
                $msg = $e->getMessage();
                return response()->json(["msg" => $msg, "status" => false]);
            }
            $selectPreview = [
                'id',
                'nik_wp',
                'nama_wp',
                'kelurahan_wp',
                'kecamatan_wp',
                'dati2_wp',
                'propinsi_wp',
                'kd_propinsi',
                'kd_dati2',
                'kd_kecamatan',
                'kd_kelurahan',
                'kd_blok',
                'no_urut',
                'kd_jns_op',
                'jenis_layanan_id',
                'nop_gabung'
            ];
            $getLayananTemp = Layanan_objek_temp::where(['created_by' => Auth()->user()->id])
                ->select($selectPreview)
                ->orderbyraw('nop_gabung,jenis_layanan_id,id asc')
                ->get();
            $total = 0;
            $resultSplitData = [];
            $result_preview = [];
            $inputHidden = "";
            if ($getLayananTemp->count()) {
                $listHiden = ['id'];
                foreach ($getLayananTemp->toArray() as $item) {;
                    $item['nop'] = '';
                    if ($item['jenis_layanan_id'] != '1') {
                        $item['nop'] = $item['kd_propinsi'] . '.' . $item['kd_dati2'] . '.' . $item['kd_kecamatan'] . '.' . $item['kd_kelurahan'] . '.' . $item['kd_blok'] . '-' . $item['no_urut'] . '.' . $item['kd_jns_op'];
                    }
                    if ($item['jenis_layanan_id'] == '7' && $item['nop_gabung'] != '') {
                        $inputHidden .= '<input type="hidden" name="data[' . $item['jenis_layanan_id'] . '][]" value="' . $item['id'] . '" class="form-style-none" >';
                    }
                    if ($item['nop_gabung'] == '') {
                        $setItem = [
                            'no' => '<input type="hidden" name="data[' . $item['jenis_layanan_id'] . '][]" value="' . $item['id'] . '" class="form-style-none" >',
                            'nik_wp' => $item['nik_wp'],
                            'nama_wp' => $item['nama_wp'],
                            // 'kelurahan_wp'=>$item['kelurahan_wp'],
                            // 'kecamatan_wp'=>$item['kecamatan_wp'],
                            'dati2_wp' => $item['dati2_wp'],
                            'propinsi_wp' => $item['propinsi_wp'],
                            'nop' => $item['nop'],
                            'option' => gallade::buttonInfo('<i class="fas fa-search"></i>', 'title="Detail Layanan" data-layanan="' . $item['id'] . '"')
                        ];
                        $resultSplitData[$item['jenis_layanan_id']][] = $setItem;
                        $total++;
                    }
                }
            }
            //$listJenis=['1','2','3','4','5','6','7','8','9'];
            $listJenis = Jenis_layanan::orderBy('id')->get();
            foreach ($listJenis->toArray() as $jenis) {
                $setData = [];
                if (in_array($jenis['id'], array_keys($resultSplitData))) {
                    $setData = $resultSplitData[$jenis['id']];
                }
                if ($jenis['id'] == '7' && count($setData) > 1) {
                    $no = $setData[count($setData) - 1]['no'];
                    $setData[count($setData) - 1]['no'] = $no . $inputHidden;
                }
                $result_preview[$jenis['id']] = [
                    'table' => gallade::generateinTbody($setData, "Data " . $jenis['nama_layanan'] . " Masih Kosong", '21'),
                    'count' => count($setData)
                ];
            }
            $return['preview_kolektif'] = $result_preview;
            $return['preview_kolektif_invalid'] = gallade::generateinTbody($data_false, "Data Masih Kosong", '21');
            $return['jumlah_nop_invalid'] = count($data_false);
            $return['jumlah_nop'] = $total;
            $return['status'] = true;
            $default = $return;
        }
        return response()->json($default);
    }
    public function detail(Request $request)
    {
        $return = response()->json(['status' => false, 'msg' => "Data tidak ditemukan."]);
        $requestdata = $request->only('id');
        if ($requestdata) {
            $select = [
                'nama_badan',
                'nomor_akta_badan',
                'nomor_sbu_badan',
                'nomor_kemenhum',
                'nik_wp',
                'nama_wp',
                'tanggal_lahir_wp',
                'alamat_wp',
                DB::raw("rt_wp||'/'||rw_wp rtrw_wp"),
                'kelurahan_wp',
                'kecamatan_wp',
                'dati2_wp',
                'propinsi_wp',
                'agama_wp',
                'kd_propinsi',
                'kd_dati2',
                'kd_kecamatan',
                'kd_kelurahan',
                'kd_blok',
                'no_urut',
                'kd_jns_op',
                'luas_bumi',
                'luas_bng',
                'njop_bumi',
                'njop_bng',
                'kelompok_objek_id',
                'kelompok_objek_nama',
                'lokasi_objek_id',
                'lokasi_objek_nama',
                'created_at',
                'updated_at',
                'telp_wp',
                'alamat_op',
                DB::raw("rt_op||'/'||rw_op rtrw_op"),
                'sisa_pecah_total_gabung',
                'hasil_pecah_hasil_gabung',
                'zona'
            ];
            $getTemp = Layanan_objek_temp::select($select)
                ->where(['id' => $requestdata['id']])->get();
            if ($getTemp->count() > 0) {
                $data = $getTemp->first()->toArray();
                $data['nop'] = $data['kd_propinsi'] . "." .
                    $data['kd_dati2'] . "." .
                    $data['kd_kecamatan'] . "." .
                    $data['kd_kelurahan'] . "." .
                    $data['kd_blok'] . "-" .
                    $data['no_urut'] . "." .
                    $data['kd_jns_op'];
                $return = [
                    'data' => $data,
                    'status' => true,
                    'msg' => "Data ditemukan."
                ];
            }
        }
        return response()->json($return);
    }
    public function storeKolektif_external(Request $request)
    {
        $return = ["msg" => "Proses Layanan gagal.", "status" => false];
        if ($request->only('send_data')) {
            DB::beginTransaction();
            ini_set('memory_limit', '-1');
            try {
                $nomor_layanan = Layanan_conf::nomor_layanan();
                $now = Carbon::now()->format('Y-m-d H:i:s');
                $getData = $request->only('send_data');
                $saveList = Collect($getData['send_data'])->except(['list_pecah', 'list_berkas'])->toArray();
                $tahun = Carbon::now()->year;
                $cekExistNop = Layanan_objek::where([
                    'kd_propinsi' => $saveList['kd_propinsi'],
                    'kd_dati2' => $saveList['kd_dati2'],
                    'kd_kecamatan' => $saveList['kd_kecamatan'],
                    'kd_kelurahan' => $saveList['kd_kelurahan'],
                    'kd_blok' => $saveList['kd_blok'],
                    'no_urut' => $saveList['no_urut'],
                    'kd_jns_op' => $saveList['kd_jns_op']
                ])
                    ->join('layanan', 'layanan.nomor_layanan', '=', 'layanan_objek.nomor_layanan')
                    ->whereRaw("to_char(layanan_objek.created_at,'YYYY')='" . $tahun . "' and layanan.deleted_at is null and layanan.kd_status!='3'")->count();
                if ($cekExistNop) {
                    return response()->json(["msg" => 'NOP sudah di ajukan pada tahun [ ' . $tahun . ' ]', "status" => false]);
                }
                // dd($getData['send_data']);
                $saveListInduk = $saveList;
                $saveListInduk['jenis_layanan_nama'] = Jenis_layanan::find($saveListInduk['jenis_layanan_id'])->nama_layanan;
                $saveListInduk['nomor_layanan'] = $nomor_layanan;
                $saveListInduk['created_at'] = $now;
                $saveListInduk['updated_at'] = $now;
                $saveListInduk['nik'] = $saveListInduk['nik_notaris'] ?? $saveListInduk['nik_wp']; // nik PPATS
                $saveListInduk['nama'] = $saveListInduk['notaris'] ?? $saveListInduk['nama_wp']; // Nama PPATS
                $saveListInduk['alamat'] = $saveListInduk['alamat_wp'];
                $saveListInduk['kelurahan'] = $saveListInduk['kelurahan_wp'];
                $saveListInduk['kecamatan'] = $saveListInduk['kecamatan_wp'];
                $saveListInduk['dati2'] = 'KABUPATEN MALANG';
                $saveListInduk['propinsi'] = 'JAWA TIMUR';
                $saveListInduk['nomor_telepon'] = $saveListInduk['telp_wp'];
                $saveListInduk['jenis_objek'] = '5';
                $saveListInduk['created_by'] = Auth()->user()->id;
                $saveListInduk['kd_status'] = '2';
                unset($saveListInduk['id']);
                $Layanan = new Layanan($saveListInduk);
                $Layanan->save();
                $saveList = Collect($saveList)->except(['id', 'created_by', 'jenis_layanan_id', 'status', 'notaris', 'nik_notaris'])->toArray();
                $saveList['created_at'] = $now;
                $saveList['updated_at'] = $now;
                $saveList['nomor_layanan'] = $nomor_layanan;
                $saveList['nop_gabung'] = $Layanan->id;
                $getId = new Layanan_objek($saveList);
                $getId->save();

                if (isset($getData['send_data']['list_pecah'])) {
                    foreach ($getData['send_data']['list_pecah'] as $itemSet) {
                        $itemSet = Collect($itemSet)->except(['id', 'created_by', 'jenis_layanan_id', 'status'])->toArray();
                        $itemSet['created_at'] = $now;
                        $itemSet['updated_at'] = $now;
                        $itemSet['nop_gabung'] = $Layanan->id;
                        $itemSet['nomor_layanan'] = $nomor_layanan;
                        $getId = new Layanan_objek($itemSet);
                        $getId->save();
                    }
                }
                if (isset($getData['send_data']['list_berkas'])) {
                    $berkasList = $getData['send_data']['list_berkas'];
                    $savedokumen = [];
                    foreach ($berkasList as $key => $setdokumen) {
                        $saveList = [
                            'tablename' => "berkas_bphtb",
                            'keyid' => '-',
                            'disk' => '-',
                            'filename' => $setdokumen['filename'],
                            'nama_dokumen' => $setdokumen['nama_dokumen'],
                            'keterangan' => $setdokumen['keterangan'] ?? 'Ajuan Eksternal Sistem PBB',
                            'nomor_layanan' => $nomor_layanan
                        ];
                        $savedokumen[] = new Layanan_dokumen($saveList);
                    }
                    if ($savedokumen) {
                        $Layanan->layanan_dokumen()->saveMany($savedokumen);
                    }
                }
                DB::commit();
                $return = ["msg" => "Proses Layanan Berhasil.", "status" => true, 'nomor_layanan' => $nomor_layanan];
            } catch (\Exception $e) {
                DB::rollback();
                $msg = $e->getMessage();
                return response()->json(["msg" => $msg, "status" => false]);
            }
        }
        return response()->json($return);
    }
    public function storeKolektif(Request $request)
    {
        $return = ["msg" => "Proses Layanan gagal.", "status" => false];
        $requestdata = $request->only('data');
        if (!$requestdata || count($requestdata) == '0') {
            return response()->json(["msg" => "Daftar Layanan Kolektif masih kosong. Harap di isi terlebih dahulu.", "status" => false]);
        }
        if (count($requestdata) > 0) {
            $listNomorLayanan = [];
            DB::beginTransaction();
            try {
                ini_set('memory_limit', '-1');
                foreach ($requestdata['data'] as $key => $item) {
                    $nomor_layanan = Layanan_conf::nomor_layanan();
                    $now = Carbon::now();
                    $select = [
                        'nama_badan',
                        'nomor_akta_badan',
                        'nomor_sbu_badan',
                        'nomor_kemenhum',
                        'nik_wp',
                        'nama_wp',
                        'tanggal_lahir_wp',
                        'alamat_wp',
                        'rt_wp',
                        'rw_wp',
                        'kelurahan_wp',
                        'kecamatan_wp',
                        'dati2_wp',
                        'propinsi_wp',
                        'agama_wp',
                        'kd_propinsi',
                        'kd_dati2',
                        'kd_kecamatan',
                        'kd_kelurahan',
                        'kd_blok',
                        'no_urut',
                        'kd_jns_op',
                        'luas_bumi',
                        'luas_bng',
                        'njop_bumi',
                        'njop_bng',
                        'kelompok_objek_id',
                        'kelompok_objek_nama',
                        'lokasi_objek_id',
                        'lokasi_objek_nama',
                        'created_at',
                        'updated_at',
                        'telp_wp',
                        'alamat_op',
                        'rt_op',
                        'rw_op',
                        'sisa_pecah_total_gabung',
                        'hasil_pecah_hasil_gabung',
                        'zona',
                        'nop_gabung',
                        DB::raw("'" . $nomor_layanan . "' nomor_layanan"),
                        DB::raw("'" . $now . "' created_at"),
                    ];
                    //'nop_gabung',
                    $moveTemp = Layanan_objek_temp::select($select)
                        ->whereIn('id', $item)
                        ->orderBy('id')
                        ->get();
                    $saveLayanan['nomor_layanan'] = $nomor_layanan;
                    $saveLayanan['jenis_layanan_id'] = $key;
                    $saveLayanan['jenis_layanan_nama'] = Jenis_layanan::find($key)->nama_layanan;
                    $saveLayanan['nik'] = '0000000000000000';
                    $saveLayanan['nama'] = 'Kolektif - ' . $saveLayanan['jenis_layanan_nama'] . ' [ ' . $moveTemp->count() . ' Permohonan ]';
                    $saveLayanan['alamat'] = '-';
                    $saveLayanan['kelurahan'] = '-';
                    $saveLayanan['kecamatan'] = '-';
                    $saveLayanan['dati2'] = 'KABUPATEN MALANG';
                    $saveLayanan['propinsi'] = 'JAWA TIMUR';
                    $saveLayanan['nomor_telepon'] = '0000000';
                    $saveLayanan['keterangan'] = '';
                    $saveLayanan['jenis_objek'] = '3';
                    $saveLayanan['kd_status'] = '2';
                    $Layanan = new Layanan($saveLayanan);
                    $Layanan->save();

                    if ($moveTemp->count() > 0) {
                        //  ID objek bukan id objek temp
                        $layanan_objek = $moveTemp->toArray();
                        $id = "";
                        foreach ($layanan_objek as $itemSet) {
                            if ($itemSet['nop_gabung'] != "" && $itemSet['nop_gabung'] != null) {
                                $itemSet['nop_gabung'] = $id;
                            }
                            $getId = new Layanan_objek($itemSet);
                            $getId->save();
                            if ($itemSet['nop_gabung'] == "" || $itemSet['nop_gabung'] == null) {
                                $id = $getId->id;
                            }
                        }
                        // Layanan_objek::insert($layanan_objek);
                        Layanan_objek_temp::whereIn('id', $item)->delete();
                    }
                    $listNomorLayanan[$key] = $nomor_layanan;
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                //"Terjadi Kesalahan, Harap Hubungi Admin";//$e->getMessage();
                $msg = $e->getMessage();
                $listNomorLayanan = [];
                return response()->json(["msg" => $msg, "status" => false]);
            }
            $return = [
                "msg" => "<p>Layanan Kolektif Online berhasil di buat.</p> 
                            <p>Download Surat Pengantar.</p>
                            <a href='" . url("laporan_input/cetak_berkas_kolektif?nomor_layanan=" . join(',', $listNomorLayanan)) . "'
                            target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i> Surat Pengantar Kolektif</a>",
                "status" => true,
                'curl' => join(',', $listNomorLayanan)
            ];
            // Call Job 
            // if ($listNomorLayanan) {
            //     foreach ($listNomorLayanan as $key => $item) {
            //         if ($item) {
            //             //ejaan
            //             if ($key == '8') {
            //                 dispatch(new LayananKolektif($item))->onQueue('layanan');
            //             } else {
            //                 dispatch(new disposisiPenelitian($item))->onQueue('layanan');
            //             }
            //         }
            //     }
            // }
        }
        return response()->json($return);
    }
    public function updateBerkasKolektif(Request $request)
    {
        $return = ["msg" => "Proses update berkas gagal.", "status" => false];
        $getData = $request->only('list');
        if (!$getData) {
            return response()->json($return);
        }
        $getData = $getData['list'];
        $JobRun = true;
        DB::beginTransaction();
        try {
            $nomor_layanan = $getData['nomor_layanan'];
            $update = [
                'kd_status' => '0',
                'updated_by' => Auth()->user()->id,
                'updated_at' => Carbon::now()
            ];
            Layanan::where(['nomor_layanan' => $nomor_layanan])
                ->update($update);
            if (in_array('berkas', array_keys($getData))) {
                $saveList = [
                    'tablename' => "layanan",
                    'keyid' => '-',
                    'disk' => '-',
                    'filename' => 'Berkas Kolektif',
                    'nama_dokumen' => 'Berkas Kolektif',
                    'keterangan' => $getData['berkas'],
                    'nomor_layanan' => $nomor_layanan
                ];
                $savedokumen = new Layanan_dokumen($saveList);
                $savedokumen->save();
            }
            DB::commit();
            $return = ["msg" => "Proses update berkas berhasil.", "status" => true];
            $cekJenisObjekExternal = Layanan::where(['nomor_layanan' => $nomor_layanan])
                ->select('jenis_objek')
                ->first();
            if (in_array($cekJenisObjekExternal->jenis_objek, ['5'])) {
                $time = Carbon::now()->format('Y-m-d.H:i:s');
                $API = env('BPHTB_API_SECRET');
                $send = Http::post(env('BPHTB_API') . '/permohonan_pbb/update', [
                    'headerLayananPbbBphtbReq' => [
                        'signature' => sha1($API . $time),
                        'time' => $time
                    ],
                    'body' => [
                        'nomor_layanan' => $nomor_layanan,
                        'status' => '2'
                    ]
                ]);
                // send BPHTB
            }
        } catch (\Exception $e) {
            DB::rollback();
            $msg = $e->getMessage();
            $JobRun = false;
            return response()->json(["msg" => $msg, "status" => false]);
        }
        if ($JobRun) {
            dispatch(new disposisiPenelitian($nomor_layanan))->onQueue('layanan');
        }
        return response()->json($return);
    }
    public function tolakBerkasKolektif(Request $request)
    {
        $return = ["msg" => "Proses Tolak Permohonan gagal.", "status" => false];
        $getData = $request->only('list');
        if (!$getData) {
            return response()->json($return);
        }
        $getData = $getData['list'];
        DB::beginTransaction();
        try {
            $nomor_layanan = $getData['nomor_layanan'];
            $keterangan = $getData['keterangan'];
            $update = [
                'kd_status' => '3',
                'keterangan' => $keterangan
            ];
            Layanan::where(['nomor_layanan' => $nomor_layanan])->update($update);
            $return = ["msg" => "Proses Tolak berkas berhasil.", "status" => true];
            DB::commit();
            $cekJenisObjekExternal = Layanan::where(['nomor_layanan' => $nomor_layanan])
                ->select('jenis_objek')
                ->first();
            if (in_array($cekJenisObjekExternal->jenis_objek, ['5'])) {
                $time = Carbon::now()->format('Y-m-d.H:i:s');
                $API = env('BPHTB_API_SECRET');
                $send = Http::post(env('BPHTB_API') . '/permohonan_pbb/update', [
                    'headerLayananPbbBphtbReq' => [
                        'signature' => sha1($API . $time),
                        'time' => $time
                    ],
                    'body' => [
                        'nomor_layanan' => $nomor_layanan,
                        'status' => '4'
                    ]
                ]);
                // send BPHTB
            }
        } catch (\Exception $e) {
            DB::rollback();
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
        return response()->json($return);
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajuan_run_job(Request $request)
    {

        $search = $request->only(['hari', 'jenis', 'nomor_layanan']);
        $select = [
            db::raw('layanan.nomor_layanan,count(layanan_objek.nomor_layanan) tba')
        ];
        $where = "(layanan_objek.nomor_layanan,layanan_objek.id) not in (select nomor_layanan,layanan_objek_id from penelitian_task ) 
        AND layanan.JENIS_OBJEK ='1'
        AND layanan_objek.pemutakhiran_at IS null";
        if (in_array('nomor_layanan', array_keys($search))) {
            $where .= " AND layanan_objek.nomor_layanan='" . $search['nomor_layanan'] . "'";
        }
        $getData = Layanan::select($select)
            ->join('layanan_objek', 'layanan_objek.nomor_layanan', "=", "layanan.nomor_layanan")
            ->whereRaw($where)
            ->groupBy(['layanan.nomor_layanan'])
            ->get();
        if ($getData->count() > 0) {
            foreach ($getData->toArray() as $item) {
                echo "Run JOB LAYANAN : " . $item['nomor_layanan'] . "</br>";
                dispatch(new disposisiPenelitian($item['nomor_layanan']))->onQueue('layanan');
            }
        }
    }
    public function store(Request $request)
    {
        $return = ["msg" => "Proses Layanan gagal.", "status" => false];
        $listLayanan = [
            'jenis_layanan_id',
            'nik',
            'nama',
            'alamat',
            'kelurahan',
            'kecamatan',
            'dati2',
            'propinsi',
            'nomor_telepon',
            'keterangan',
            'email',
            'pengurus'
        ];
        $nomor_layanan = Layanan_conf::nomor_layanan();
        if ($nomor_layanan) {
            $cekNOP = $request->only(['nop', 'jenis_layanan_id', 'pre_kecamatan', 'pre_kelurahan']);
            $jenisObjek = $request->only('jenisObjek');
            if ($cekNOP['nop'] && $cekNOP['jenis_layanan_id'] != '1') {
                $nop = explode(".", $cekNOP['nop']);
                $blok_urut = explode("-", $nop[4]);
                $nopdefault = [$nop[0], $nop[1], $nop[2], $nop[3], $blok_urut[0], $blok_urut[1], $nop[5]];

                $user = Auth()->user();
                $userE = ['Super User', 'Staff Pelayanan', 'Kasi Pelayanan', 'Kepala Bidang'];
                $role = $user->roles()->pluck('name')->filter(function ($value) use ($userE) {
                    return in_array($value, $userE);
                });
                if (!in_array($cekNOP['jenis_layanan_id'], ["6", '7']) && !$role->count()) {
                    $tahun = Carbon::now()->year;
                    $cekLayanan = LayananUpdate::cekNOPLayanan($nop, $blok_urut, $tahun);
                    if ($cekLayanan) {
                        return response()->json(["msg" => 'NOP sudah di Ajukan Pada tahun ' . $tahun, "status" => false]);
                    }
                }
            } else {
                $nopdefault = ['35', '07', $cekNOP['pre_kecamatan'] ?? '000', $cekNOP['pre_kelurahan'] ?? '000', '000', '0000', '0'];
            }
            if (in_array($cekNOP['jenis_layanan_id'], ['5'])) {
                $cekType = JenisPengurangan::where(['id' => $request->jenis_pengurangan, 'jenis' => '0'])->get();
                $listValid = [
                    'FC KTP/SIM dan KK' => 'FC KTP/SIM dan KK',
                    'Sket Lurah' => 'Sket Lurah',
                    'FC SK Pensiun / Veteran' => 'FC SK Pensiun / Veteran'
                ];
                if ($request->jenis_pengurangan == '1') {
                    $listValid = [
                        'FC KTP/SIM dan KK' => 'FC KTP/SIM dan KK',
                        'Sket Lurah' => 'Sket Lurah',
                        'FC SK Pensiun / Veteran' => 'FC SK Pensiun / Veteran',
                        'Asli SPPT' => 'Asli SPPT'
                    ];
                }
                if ($cekType->count() > 0) {
                    $CekDokumen = $request->only('dokumen');
                    if (in_array('dokumen', array_keys($CekDokumen))) {
                        foreach ($CekDokumen['dokumen'] as $item) {
                            if (in_array($item['nama_dokumen'], $listValid)) {
                                unset($listValid[$item['nama_dokumen']]);
                            }
                        }
                    }
                    if (count($listValid) > 0) {
                        $joinBerkas = join(',', $listValid);
                        return response()->json(["msg" => 'Berkas harap di lengkapi: ' . $joinBerkas, "status" => false]);
                    }
                }
            }
            //
            if ($cekNOP['nop'] && $cekNOP['jenis_layanan_id'] != '10') {
                $NOPAPI = env('API_CEKNOP', 'localhost/simpbb/api/ceknop');
                $get = Http::get($NOPAPI, ['nop' => $cekNOP['nop'], 'jenis' => $cekNOP['jenis_layanan_id']]);
                if ($get) {
                    $getData = (array)$get->object();
                    if (in_array('status', $getData)) {
                        if (!$getData['status']) {
                            return response()->json(["msg" => $getData['msg'], "status" => false]);
                        }
                    } else {
                        return response()->json(["msg" => $getData, "status" => false]);
                    }
                }
                // dd($get);
            }
            if (in_array($cekNOP['jenis_layanan_id'], ['7'])) {
                $listNop = $request->only('list_nop');
                if (count($listNop['list_nop']) < 2) {
                    return response()->json(["msg" => 'Minimal 2 NOP untuk mengajukan mutasi gabung', "status" => false]);
                }
            }
            //
            $status = true;
            $statusLayananBerkas = true;
            DB::beginTransaction();
            ini_set('memory_limit', '-1');
            try {
                $saveLayanan = $request->only($listLayanan);
                $jenis_layanan_nama = Jenis_layanan::find($saveLayanan['jenis_layanan_id'])->nama_layanan;
                // if ($jenisObjek['jenisObjek'] == '1') {
                $listLayananObjek = [
                    'nik_wp', 'telp_wp', 'nama_wp', 'tanggal_lahir_wp', 'alamat_wp', 'kelurahan_wp', 'kecamatan_wp', 'dati2_wp',
                    'propinsi_wp', 'rt_wp', 'rw_wp', 'agama_wp', 'alamat_op', 'luas_bumi', 'sisa_pecah_total_gabung', 'hasil_pecah_hasil_gabung', 'njop_bumi',
                    'luas_bng', 'njop_bng', 'kelompok_objek_id', 'lokasi_objek_id', 'zona'
                ];
                $saveLayanan['jenis_layanan_nama'] = Jenis_layanan::find($saveLayanan['jenis_layanan_id'])->nama_layanan;
                $saveLayanan['jenis_objek'] = $jenisObjek['jenisObjek'];
                $saveLayanan['nomor_layanan'] = $nomor_layanan;
                // dd($saveLayanan);
                // // layanan_objek pribadi pre_kelurahan
                $layanan_objek = $request->only($listLayananObjek);
                $layanan_objek['nik_wp'] = substr($layanan_objek['nik_wp'], 0, 16);
                // dd($layanan_objek);
                $saveLayanan['nik'] = substr($saveLayanan['nik'], 0, 16);
                if ($cekNOP['jenis_layanan_id'] == '1') {
                    $layanan_objek['nik_wp'] = $saveLayanan['nik'];
                    $layanan_objek['nama_wp'] = $saveLayanan['nama'];
                    $layanan_objek['alamat_wp'] = $saveLayanan['alamat'] . ' - ' . $request->blok_kav_no_wp;
                    $layanan_objek['kelurahan_wp'] = $saveLayanan['kelurahan'];
                    $layanan_objek['kecamatan_wp'] = $saveLayanan['kecamatan'];
                    $layanan_objek['dati2_wp'] = $saveLayanan['dati2'];
                    $layanan_objek['propinsi_wp'] = $saveLayanan['propinsi'];
                    $layanan_objek['telp_wp'] = $saveLayanan['nomor_telepon'];
                    $layanan_objek['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $request->rt_op));
                    $layanan_objek['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $request->rw_op));
                    $layanan_objek['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $request->rt_wp_baru));
                    $layanan_objek['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $request->rw_wp_baru));
                    $layanan_objek['status_subjek_pajak'] = $request->status_subjek_pajak;
                    $layanan_objek['alamat_op'] = $request->alamat_op . ' - ' . $request->blok_kav_no;
                }
                if (in_array($cekNOP['jenis_layanan_id'], ['2', '4', '5', '9', '6'])) {
                    $layanan_objek['alamat_op'] = $request->nop_alamat;
                    if ($cekNOP['jenis_layanan_id'] == '6') {
                        $layanan_objek['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $request->nop_rt));
                        $layanan_objek['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $request->nop_rw));
                    }
                }
                if (in_array($cekNOP['jenis_layanan_id'], ['6', '7'])) {
                    $layanan_objek['alamat_wp'] = $saveLayanan['alamat'] . ' - ' . $request->blok_kav_no_wp;
                }
                if (in_array($cekNOP['jenis_layanan_id'], ['7'])) {
                    $layanan_objek['nik_wp'] = $saveLayanan['nik'];
                    $layanan_objek['nama_wp'] = $saveLayanan['nama'];
                    $layanan_objek['alamat_wp'] = $saveLayanan['alamat'] . ' - ' . $request->blok_kav_no;
                    $layanan_objek['kelurahan_wp'] = $saveLayanan['kelurahan'];
                    $layanan_objek['kecamatan_wp'] = $saveLayanan['kecamatan'];
                    $layanan_objek['dati2_wp'] = $saveLayanan['dati2'];
                    $layanan_objek['propinsi_wp'] = $saveLayanan['propinsi'];
                    $layanan_objek['telp_wp'] = $saveLayanan['nomor_telepon'];
                    $layanan_objek['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $request->nop_rt));
                    $layanan_objek['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $request->nop_rw));
                    $layanan_objek['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $request->rt_wp_baru));
                    $layanan_objek['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $request->rw_wp_baru));
                    $layanan_objek['alamat_op'] = $request->nop_alamat . ' - ' . $request->blok_kav_no_op;
                    $nopdefault[2] = $request->nop_kecamatan;
                    $nopdefault[3] = $request->nop_kelurahan;
                }
                if (in_array($cekNOP['jenis_layanan_id'], ['9'])) {
                    $layanan_objek['nik_wp'] = $request->nik;
                    $layanan_objek['telp_wp'] = $request->nomor_telepon;
                    $layanan_objek['nama_wp'] = $request->nama;
                    $layanan_objek['alamat_wp'] = $request->alamat;
                    $layanan_objek['kelurahan_wp'] = $request->kelurahan;
                    $layanan_objek['kecamatan_wp'] = $request->kecamatan;
                    $layanan_objek['dati2_wp'] = $request->dati2;
                    $layanan_objek['propinsi_wp'] = $request->propinsi;
                    $layanan_objek['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $request->rt_wp_baru));
                    $layanan_objek['rw_wp'] = sprintf("%02s", preg_replace('/[ _]/', "", $request->rw_wp_baru));
                    // dd($layanan_objek);
                }
                if (in_array($cekNOP['jenis_layanan_id'], ['5'])) {
                    $layanan_objek['jenis_pengurangan'] = $request->jenis_pengurangan;

                    // dd($layanan_objek);
                }
                $kelompokObjekNama = ($layanan_objek['kelompok_objek_id']) ? KelompokObjek::find($layanan_objek['kelompok_objek_id'])->nama_kelompok : '';
                $lokasiObjekNama = ($layanan_objek['lokasi_objek_id']) ? LokasiObjek::find($layanan_objek['lokasi_objek_id'])->nama_lokasi : '';
                // $layanan_objek['sisa_pecah_total_gabung'] = '';
                // $layanan_objek['hasil_pecah_hasil_gabung'] = '';
                $layanan_objek['tanggal_lahir_wp'] = ''; //Carbon::createFromFormat('d-m-Y',str_replace(' ','',$layanan_objek['tanggal_lahir_wp']))->format('Y-m-d');
                //nop
                $layanan_objek['kd_propinsi'] = $nopdefault[0];
                $layanan_objek['kd_dati2'] = $nopdefault[1];
                $layanan_objek['kd_kecamatan'] = $nopdefault[2];
                $layanan_objek['kd_kelurahan'] = $nopdefault[3];
                $layanan_objek['kd_blok'] = $nopdefault[4];
                $layanan_objek['no_urut'] = $nopdefault[5];
                $layanan_objek['kd_jns_op'] = $nopdefault[6];

                $layanan_objek['kelompok_objek_nama'] = $kelompokObjekNama;
                $layanan_objek['lokasi_objek_nama'] = $lokasiObjekNama;
                $layanan_objek['nomor_layanan'] = $nomor_layanan;
                // dd($layanan_objek);
                $Layanan = new Layanan($saveLayanan);
                $Layanan->save();
                if ($jenisObjek['jenisObjek'] == '2') {
                    $layanan_objek['nomor_akta_badan'] = $saveLayanan['nik'];
                    $layanan_objek['nama_badan'] = $saveLayanan['nama'];
                    $databadan = $request->only(['nomor_sbu_badan', 'nomor_kemenhum']);
                    $layanan_objek['nomor_sbu_badan'] = $databadan['nomor_sbu_badan'];
                    $savelayanlayanan_objekan_objek['nomor_kemenhum'] = $databadan['nomor_kemenhum'];
                }
                // dd($layanan_objek);
                $savelayanan_objek = new Layanan_objek($layanan_objek);
                $getIdObjek = $Layanan->layanan_objek()->save($savelayanan_objek);
                // mutasi gabung
                $keyIdList = ['0' => $getIdObjek->id];
                if ($saveLayanan['jenis_layanan_id'] == '7') {
                    $nop_gabung = $request->only('list_nop');
                    // $nop_gabung=$layanan_objekGabung;
                    if ($nop_gabung) {
                        foreach ($nop_gabung['list_nop'] as $nopGabung) {
                            // dd($nopGabung);
                            $nopz = explode(".", $nopGabung['nop']);
                            $blok_urutz = explode("-", $nopz[4]);
                            $layanan_objek['kd_propinsi'] = $nopz[0];
                            $layanan_objek['kd_dati2'] = $nopz[1];
                            $layanan_objek['kd_kecamatan'] = $nopz[2];
                            $layanan_objek['kd_kelurahan'] = $nopz[3];
                            $layanan_objek['kd_blok'] = $blok_urutz[0];
                            $layanan_objek['no_urut'] = $blok_urutz[1];
                            $layanan_objek['kd_jns_op'] = $nopz[5];
                            $layanan_objek['luas_bumi'] = $nopGabung['luas_bumi'];

                            $layanan_objek['luas_bng'] = $nopGabung['luas_bng'];

                            $layanan_objek['alamat_op'] = $nopGabung['nop_alamat'];
                            $layanan_objek['rt_op'] = sprintf("%03s", preg_replace('/[ _]/', "", $nopGabung['nop_rt']));
                            $layanan_objek['rw_op'] = sprintf("%02s", preg_replace('/[ _]/', "", $nopGabung['nop_rw']));
                            // $layanan_objek['nop_gabung']=join($nopdefault);
                            $layanan_objek['nop_gabung'] = $getIdObjek->id;
                            // dd($layanan_objek);
                            $savelayanan_objek = new Layanan_objek($layanan_objek);
                            $id = $Layanan->layanan_objek()->save($savelayanan_objek);
                            $keyIdList[$nopGabung['nop']] = $id->id;
                        }
                    }
                }
                if ($saveLayanan['jenis_layanan_id'] == '6') {
                    $nop_pecah = $request->only('list_pecah');
                    // dd($nop_pecah);
                    if ($nop_pecah) {
                        foreach ($nop_pecah['list_pecah'] as $key => $nopPecah) {
                            $layanan_objek['nik_wp'] = $nopPecah['nik_wp_pecah'];
                            $layanan_objek['nama_wp'] = $nopPecah['nama_wp_pecah'];
                            $layanan_objek['alamat_wp'] = $nopPecah['alamat_wp_pecah'] . '-' . $nopPecah['blok_kav_no_wp_pecah'];
                            $layanan_objek['kelurahan_wp'] = $nopPecah['kelurahan_wp_pecah'];
                            $layanan_objek['kecamatan_wp'] = $nopPecah['kecamatan_wp_pecah'];
                            $layanan_objek['dati2_wp'] = $nopPecah['dati2_wp_pecah'];
                            $layanan_objek['propinsi_wp'] = $nopPecah['propinsi_wp_pecah'];
                            // $layanan_objek['agama_wp'] = $nopPecah['agama_wp_pecah'];

                            $layanan_objek['kelompok_objek_id'] = $nopPecah['kelompok_objek_id_pecah'];
                            $layanan_objek['kelompok_objek_nama'] = '';
                            $KelompokObjek = KelompokObjek::where(['id' => $nopPecah['kelompok_objek_id_pecah']])->get();
                            if ($KelompokObjek->count() > 0) {
                                $layanan_objek['kelompok_objek_nama'] = $KelompokObjek->first()->nama_kelompok;
                            }
                            $layanan_objek['lokasi_objek_id'] = $nopPecah['lokasi_objek_idpecah'];
                            $layanan_objek['lokasi_objek_nama'] = '';
                            $LokasiObjek = LokasiObjek::where(['id' => $nopPecah['lokasi_objek_idpecah']])->get();
                            if ($LokasiObjek->count() > 0) {
                                $layanan_objek['lokasi_objek_nama'] = $LokasiObjek->first()->nama_lokasi;
                            }
                            $layanan_objek['telp_wp'] = $nopPecah['notelp_wp_pecah'];
                            $layanan_objek['rt_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $nopPecah['rt_wp_pecah']));
                            $layanan_objek['rw_wp'] = sprintf("%03s", preg_replace('/[ _]/', "", $nopPecah['rw_wp_pecah']));

                            $layanan_objek['kd_blok'] = '000';
                            $layanan_objek['no_urut'] = '0000';
                            $layanan_objek['kd_jns_op'] = '0';
                            $layanan_objek['alamat_op'] = $nopPecah['alamat_pecah'] . '-' . $nopPecah['blok_kav_no_pecah'];

                            $layanan_objek['luas_bumi'] = $nopPecah['luas_bumi_pecah'];
                            $layanan_objek['luas_bng'] = $nopPecah['luas_bng_pecah'];
                            // $layanan_objek['nop_gabung'] = join($nopdefault);
                            $layanan_objek['nop_gabung'] = $getIdObjek->id;
                            $savelayanan_objek = new Layanan_objek($layanan_objek);
                            $id = $Layanan->layanan_objek()->save($savelayanan_objek);
                            $keyIdList[$nopPecah['nik_wp_pecah']] = $id->id;
                        }
                    }
                }
                // dd($layanan_objek);

                // } else {
                // $saveLayanan=$request->only($listLayanan);
                // $saveLayanan['jenis_layanan_nama']=Jenis_layanan::find($saveLayanan['jenis_layanan_id'])->nama_layanan;
                // $saveLayanan['nik']='-';
                // $saveLayanan['nama']='-';
                // $saveLayanan['alamat']='-';
                // $saveLayanan['jenis_objek']=$jenisObjek['jenisObjek'];
                // $saveLayanan['nomor_layanan']=$nomor_layanan;
                // $Layanan=new Layanan($saveLayanan);
                // $Layanan->save();
                // // layanan_objek badan
                // $listLayananObjek=['nik_wp','nama_wp','tanggal_lahir_wp','alamat_wp','kelurahan_wp','kecamatan_wp','dati2_wp','propinsi_wp','rt_wp','rw_wp','luas_bumi',
                // 'sisa_pecah_total_gabung','hasil_pecah_hasil_gabung','njop_bumi','luas_bng','njop_bng','kelompok_objek_id','lokasi_objek_id','zona','nama_badan',
                // 'nomor_akta_badan','nomor_sbu_badan','nomor_kemenhum'];
                // $layanan_objek=$request->only($listLayananObjek);
                // $layanan_objek['tanggal_lahir_wp']='2020-01-01';//Carbon::createFromFormat('d-m-Y','01-01-2000')->format('Y-m-d');
                // //nop
                // $layanan_objek['kd_propinsi']=$nopdefault[0];
                // $layanan_objek['kd_dati2']=$nopdefault[1];
                // $layanan_objek['kd_kecamatan']=$nopdefault[2];
                // $layanan_objek['kd_kelurahan']=$nopdefault[3];
                // $layanan_objek['kd_blok']=$nopdefault[4];
                // $layanan_objek['no_urut']=$nopdefault[5];
                // $layanan_objek['kd_jns_op']=$nopdefault[6];

                // $layanan_objek['kelompok_objek_nama']=KelompokObjek::find($layanan_objek['kelompok_objek_id'])->nama_kelompok;
                // $layanan_objek['lokasi_objek_nama']=LokasiObjek::find($layanan_objek['lokasi_objek_id'])->nama_lokasi;
                // $layanan_objek['nomor_layanan']=$nomor_layanan;
                // //print_r($layanan_objek);
                // $savelayanan_objek=new Layanan_objek($layanan_objek);
                // $Layanan->layanan_objek()->save($savelayanan_objek);
                // }
                // // dokumen_pendukung
                $savedokumen = [];
                $dokumenData = $request->only('dokumen');
                if ($dokumenData) {
                    // dd($dokumenData);
                    // dd($dokumenData);
                    foreach ($dokumenData['dokumen'] as $key => $setdokumen) {
                        // $ofilename=$nomor_layanan.str_replace(' ','',strtolower($setdokumen['nama_dokumen'])).'.'.$setdokumen['file']->getClientOriginalExtension();
                        // $disk='public/dokumen_pendukung/'.$nomor_layanan;
                        $saveList = [
                            'tablename' => "layanan",
                            // 'keyid'=>encrypt($nomor_layanan),
                            // 'disk'=>$disk,
                            // 'filename'=> $ofilename,
                            'keyid' => '-',
                            'disk' => '-',
                            'filename' => '-',
                            'nama_dokumen' => $setdokumen['nama_dokumen'],
                            'keterangan' => $setdokumen['keterangan'],
                            'nomor_layanan' => $nomor_layanan
                        ];
                        if ($saveLayanan['jenis_layanan_id'] == '6' || $saveLayanan['jenis_layanan_id'] == '7') {
                            if (in_array($setdokumen['jenis'], array_keys($keyIdList))) {
                                // $saveList['keyid']=preg_replace('/[.-]/', '', $setdokumen['jenis']); 
                                $saveList['keyid'] = $keyIdList[$setdokumen['jenis']];
                            }
                        }
                        $savedokumen[] = new Layanan_dokumen($saveList);
                        // $path = $setdokumen['file']->storeAs($disk,$ofilename); //upload
                    }
                    $Layanan->layanan_dokumen()->saveMany($savedokumen);
                } else {
                    // Layanan::where(['nomor_layanan'=>$nomor_layanan])->update(['kd_status'=>'2']);
                    // $statusLayananBerkas=false;
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                // delete file upload $nomor_layanan
                $disk = 'dokumen_pendukung/' . $nomor_layanan;
                $isExists = Storage::disk('public')->exists($disk);
                if ($isExists) {
                    $isExists = Storage::disk('public')->deleteDirectory($disk);
                }
                //"Terjadi Kesalahan, Harap Hubungi Admin";//
                $msg = "Proses tidak berhasil, coba ulangi lagi"; //$e->getMessage();//
                log::emergency($e->getMessage());
                $status = false;
                return response()->json(["msg" => $msg, "status" => false]);
            }
            if (!$statusLayananBerkas) {
                return response()->json([
                    "msg" => "Layanan " . $jenis_layanan_nama . " berhasil di buat.\n Harap Melakukan update Berkas ",
                    "status" => false
                ]);
            }
            $return = [
                "msg" => "<p>Layanan " . $jenis_layanan_nama . " berhasil di buat.</p> 
                            <p>Download Tanda terima.</p>
                            <a href='" . url("laporan_input/tanda_terima?nomor_layanan=" . $nomor_layanan) . "'
                            target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i> Tanda Terima</a>",
                "status" => true,
                'curl' => $nomor_layanan
            ];
            if ($status) {
                dispatch(new disposisiPenelitian($nomor_layanan))->onQueue('layanan');
                if (in_array($cekNOP['jenis_layanan_id'], ['5'])) {
                    if ($cekType->count() > 0) {
                        Layanan::where(['nomor_layanan' => $nomor_layanan])->update(['kd_status' => '1']);
                        #exec Job Pengurangan
                        // <a href='" . url("laporan_input/cetak_pdf?nomor_layanan=" . $nomor_layanan) . "'
                        //             target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i> SK </a>
                        $nopResult = $nopdefault[0] . "." . $nopdefault[1] . "." . $nopdefault[2] . "." . $nopdefault[3] . "." . $nopdefault[4] . "-" . $nopdefault[5] . "." . $nopdefault[6];
                        $return = [
                            "msg" => "<p>Layanan " . $jenis_layanan_nama . " berhasil di buat.</p>
                                    <p>SK dapat di cetak di menu  'Penelitian -> Daftar SK' dengan pecarian menggunakan Nomor Layanan atau NOP</p>
                                    <p>SPPT dapat di cetak di menu  'Data dan Informasi -> Riwayat Pembayarn' dengan pecarian menggunakan NOP</p>
                                    <b>
                                        <p>Nomor Layanan : " . $nomor_layanan . "</p>
                                        <p>NOP : " . $nopResult . "</p>
                                    </b>
                                    <div class='btn-group col-12'>
                                    <a href='" . url("penelitian/sk?nomor_layanan=" . $nomor_layanan) . "'
                                                target='_BLANK' class='btn btn-default'><i class='fas fa-file-alt'></i> SK </a>
                                    <a href='" . url("informasi/riwayat?nop=" . $nopResult) . "'
                                                target='_BLANK' class='btn btn-default'><i class='fas fa-file-alt'></i> SPPT </a></div>
                                    ",
                            "status" => true,
                            'curl' => $nomor_layanan
                        ];
                    }
                }
            }
        }
        return response()->json($return);
    }
    public function storepembetulan(Request $request)
    {
        $return = ["msg" => "Proses Layanan gagal.", "status" => false];
        $nomor_layanan = Layanan_conf::nomor_layanan();
        if ($nomor_layanan) {
            $cekNOP = $request->only(['nop', 'jenis_layanan_id', 'pre_kecamatan', 'pre_kelurahan']);
            //dd($cekNOP['jenis_layanan_id']);
            $jenisObjek = $request->only('jenisObjek');
            if ($cekNOP['nop'] && $cekNOP['jenis_layanan_id'] != '1') {
                $nop = explode(".", $cekNOP['nop']);
                $blok_urut = explode("-", $nop[4]);
                $nopdefault = [$nop[0], $nop[1], $nop[2], $nop[3], $blok_urut[0], $blok_urut[1], $nop[5]];
                // if($cekNOP['jenis_layanan_id']=='8'){
                //     $cekBayar=LayananUpdate::cekpembayaran($nop,$blok_urut);
                //     if(!$cekBayar){
                //         $tahun=Carbon::now()->year;
                //         return response()->json(["msg"=>'Harap di lakukan pelunasan PBB sampai tahun '.$tahun,"status"=>false]);
                //     }
                // }
                // if (!in_array($cekNOP['jenis_layanan_id'], ["6"])) {
                //     $tahun = Carbon::now()->year;
                //     $cekLayanan = LayananUpdate::cekNOPLayanan($nop, $blok_urut, $tahun);
                //     if ($cekLayanan) {
                //         return response()->json(["msg" => 'NOP sudah di Ajukan Pada tahun ' . $tahun, "status" => false]);
                //     }
                // }
            } else {
                $nopdefault = ['35', '07', $cekNOP['pre_kecamatan'] ?? '000', $cekNOP['pre_kelurahan'] ?? '000', '000', '0000', '0'];
            }
            $status = true;
            $statusLayananBerkas = true;
            DB::beginTransaction();
            try {
                ini_set('memory_limit', '-1');
                $cekBadan = $request->only(['jenisObjek']);
                if ($cekBadan['jenisObjek'] == '2') {
                    $listLayanan = [
                        'jenis_layanan_id',
                        'nik_badan_pembetulan',
                        'nama_badan_pembetulan',
                        'nomor_sbu_badan_pembetulan',
                        'nomor_kemenhum_pembetulan',
                        'alamat_wp_pembetulan',
                        'kelurahan_wp_pembetulan',
                        'kecamatan_wp_pembetulan',
                        'dati2_wp_pembetulan',
                        'propinsi_wp_pembetulan',
                        'telp_wp_pembetulan',
                        'keterangan',
                    ];
                    $rawLayanan = $request->only($listLayanan);
                    $namaLayanan = Jenis_layanan::find($rawLayanan['jenis_layanan_id'])->nama_layanan;
                    $saveLayanan = [
                        'jenis_layanan_id' => $rawLayanan['jenis_layanan_id'],
                        'nik' => $rawLayanan['nik_badan_pembetulan'],
                        'nama' => $rawLayanan['nama_badan_pembetulan'],
                        'alamat' => $rawLayanan['alamat_wp_pembetulan'],
                        'kelurahan' => $rawLayanan['kelurahan_wp_pembetulan'],
                        'kecamatan' => $rawLayanan['kecamatan_wp_pembetulan'],
                        'dati2' => $rawLayanan['dati2_wp_pembetulan'],
                        'propinsi' => $rawLayanan['propinsi_wp_pembetulan'],
                        'nomor_telepon' => $rawLayanan['telp_wp_pembetulan'],
                        'keterangan' => $rawLayanan['keterangan'],
                        'jenis_layanan_nama' => $namaLayanan,
                        'jenis_objek' => $jenisObjek['jenisObjek'],
                        'nomor_layanan' => $nomor_layanan,
                        'pengurus' => '1',
                        'kd_status' => ($cekNOP['jenis_layanan_id'] == '8') ? '1' : '0'
                    ];
                    $Layanan = new Layanan($saveLayanan);
                    $Layanan->save();
                    $rawLayananObjek = [
                        'nik_badan_pembetulan',
                        'nama_badan_pembetulan',
                        'nomor_sbu_badan_pembetulan',
                        'nomor_kemenhum_pembetulan',
                        'alamat_wp_pembetulan',
                        'kelurahan_wp_pembetulan',
                        'kecamatan_wp_pembetulan',
                        'dati2_wp_pembetulan',
                        'propinsi_wp_pembetulan',
                        'telp_wp_pembetulan', //
                        'rt_wp_pembetulan',
                        'rw_wp_pembetulan',
                        'nop_alamat_pembetulan', //
                        'nop_rt_pembetulan', //
                        'nop_rw_pembetulan', //
                        'luas_bumi_pembetulan',
                        'luas_bng_pembetulan',
                        'njop_bumi_pembetulan',
                        'njop_bng_pembetulan'
                    ];
                    $rawLayananObjek = $request->only($rawLayananObjek);
                    //dd($saveLayanan);
                    $saveLayananObjek = [
                        'nik_wp' => $rawLayananObjek['nik_badan_pembetulan'],
                        'nama_wp' => $rawLayananObjek['nama_badan_pembetulan'],
                        'nomor_akta_badan' => $rawLayananObjek['nik_badan_pembetulan'],
                        'nama_badan' => $rawLayananObjek['nama_badan_pembetulan'],
                        'nomor_sbu_badan' => $rawLayananObjek['nomor_sbu_badan_pembetulan'],
                        'nomor_kemenhum' => $rawLayananObjek['nomor_kemenhum_pembetulan'],
                        'alamat_wp' => $rawLayananObjek['alamat_wp_pembetulan'],
                        'kelurahan_wp' => $rawLayananObjek['kelurahan_wp_pembetulan'],
                        'kecamatan_wp' => $rawLayananObjek['kecamatan_wp_pembetulan'],
                        'dati2_wp' => $rawLayananObjek['dati2_wp_pembetulan'],
                        'propinsi_wp' => $rawLayananObjek['propinsi_wp_pembetulan'],
                        'rt_wp' => $rawLayananObjek['rt_wp_pembetulan'],
                        'rw_wp' => $rawLayananObjek['rw_wp_pembetulan'],
                        'telp_wp' => $rawLayananObjek['telp_wp_pembetulan'],
                        'alamat_op' => $rawLayananObjek['nop_alamat_pembetulan'],
                        'rt_op' => $rawLayananObjek['nop_rt_pembetulan'],
                        'rw_op' => $rawLayananObjek['nop_rw_pembetulan'],
                        'kd_propinsi' => $nopdefault[0],
                        'kd_dati2' => $nopdefault[1],
                        'kd_kecamatan' => $nopdefault[2],
                        'kd_kelurahan' => $nopdefault[3],
                        'kd_blok' => $nopdefault[4],
                        'no_urut' => $nopdefault[5],
                        'kd_jns_op' => $nopdefault[6],
                        'nomor_layanan' => $nomor_layanan,
                        'luas_bumi' => $rawLayananObjek['luas_bumi_pembetulan'],
                        'luas_bng' => $rawLayananObjek['luas_bng_pembetulan'],
                        'njop_bumi' => $rawLayananObjek['njop_bumi_pembetulan'],
                        'njop_bng' => $rawLayananObjek['njop_bng_pembetulan']
                    ];
                    // dd($saveLayananObjek);
                    // dd($cekBadan);
                } else {
                    // dd($request->all());
                    $listLayanan = [
                        'jenis_layanan_id',
                        'nik_wp_pembetulan',
                        'nama_wp_pembetulan',
                        'alamat_wp_pembetulan',
                        'kelurahan_wp_pembetulan',
                        'kecamatan_wp_pembetulan',
                        'dati2_wp_pembetulan',
                        'propinsi_wp_pembetulan',
                        'telp_wp_pembetulan',
                        'keterangan',
                    ];
                    $rawLayanan = $request->only($listLayanan);
                    $namaLayanan = Jenis_layanan::find($rawLayanan['jenis_layanan_id'])->nama_layanan;
                    $saveLayanan = [
                        'jenis_layanan_id' => $rawLayanan['jenis_layanan_id'],
                        'nik' => $rawLayanan['nik_wp_pembetulan'],
                        'nama' => $rawLayanan['nama_wp_pembetulan'],
                        'alamat' => $rawLayanan['alamat_wp_pembetulan'],
                        'kelurahan' => $rawLayanan['kelurahan_wp_pembetulan'],
                        'kecamatan' => $rawLayanan['kecamatan_wp_pembetulan'],
                        'dati2' => $rawLayanan['dati2_wp_pembetulan'],
                        'propinsi' => $rawLayanan['propinsi_wp_pembetulan'],
                        'nomor_telepon' => $rawLayanan['telp_wp_pembetulan'],
                        'keterangan' => $rawLayanan['keterangan'],
                        'jenis_layanan_nama' => $namaLayanan,
                        'jenis_objek' => $jenisObjek['jenisObjek'],
                        'nomor_layanan' => $nomor_layanan,
                        'pengurus' => '1',
                        'kd_status' => ($cekNOP['jenis_layanan_id'] == '8') ? '1' : '0'
                    ];
                    $Layanan = new Layanan($saveLayanan);
                    $Layanan->save();
                    $rawLayananObjek = [
                        'nik_wp_pembetulan',
                        'nama_wp_pembetulan',
                        'alamat_wp_pembetulan',
                        'kelurahan_wp_pembetulan',
                        'kecamatan_wp_pembetulan',
                        'dati2_wp_pembetulan',
                        'propinsi_wp_pembetulan',
                        'telp_wp_pembetulan', //
                        'rt_wp_pembetulan',
                        'rw_wp_pembetulan',
                        'nop_alamat_pembetulan', //
                        'nop_rt_pembetulan', //
                        'nop_rw_pembetulan', //
                        'luas_bumi_pembetulan',
                        'luas_bng_pembetulan',
                        'njop_bumi_pembetulan',
                        'njop_bng_pembetulan'
                    ];
                    $rawLayananObjek = $request->only($rawLayananObjek);
                    //dd($saveLayanan);
                    $saveLayananObjek = [
                        'nik_wp' => $rawLayananObjek['nik_wp_pembetulan'],
                        'nama_wp' => $rawLayananObjek['nama_wp_pembetulan'],
                        'alamat_wp' => $rawLayananObjek['alamat_wp_pembetulan'],
                        'kelurahan_wp' => $rawLayananObjek['kelurahan_wp_pembetulan'],
                        'kecamatan_wp' => $rawLayananObjek['kecamatan_wp_pembetulan'],
                        'dati2_wp' => $rawLayananObjek['dati2_wp_pembetulan'],
                        'propinsi_wp' => $rawLayananObjek['propinsi_wp_pembetulan'],
                        'rt_wp' => $rawLayananObjek['rt_wp_pembetulan'],
                        'rw_wp' => $rawLayananObjek['rw_wp_pembetulan'],
                        'telp_wp' => $rawLayananObjek['telp_wp_pembetulan'],
                        'alamat_op' => $rawLayananObjek['nop_alamat_pembetulan'],
                        'rt_op' => $rawLayananObjek['nop_rt_pembetulan'],
                        'rw_op' => $rawLayananObjek['nop_rw_pembetulan'],
                        'kd_propinsi' => $nopdefault[0],
                        'kd_dati2' => $nopdefault[1],
                        'kd_kecamatan' => $nopdefault[2],
                        'kd_kelurahan' => $nopdefault[3],
                        'kd_blok' => $nopdefault[4],
                        'no_urut' => $nopdefault[5],
                        'kd_jns_op' => $nopdefault[6],
                        'nomor_layanan' => $nomor_layanan,
                        'luas_bumi' => $rawLayananObjek['luas_bumi_pembetulan'],
                        'luas_bng' => $rawLayananObjek['luas_bng_pembetulan'],
                        'njop_bumi' => $rawLayananObjek['njop_bumi_pembetulan'],
                        'njop_bng' => $rawLayananObjek['njop_bng_pembetulan']
                    ];
                }
                // dd($saveLayananObjek);
                $savelayanan_objek = new Layanan_objek($saveLayananObjek);
                $Layanan->layanan_objek()->save($savelayanan_objek);

                // // // dokumen_pendukung
                $savedokumen = [];
                $dokumenData = $request->only('dokumen');
                if ($dokumenData) {
                    foreach ($dokumenData['dokumen'] as $key => $setdokumen) {
                        // $ofilename=$nomor_layanan.str_replace(' ','',strtolower($setdokumen['nama_dokumen'])).'.'.$setdokumen['file']->getClientOriginalExtension();
                        $disk = 'public/dokumen_pendukung/' . $nomor_layanan;
                        $saveList = [
                            'tablename' => "layanan",
                            // 'keyid'=>encrypt($nomor_layanan),
                            // 'disk'=>$disk,
                            // 'filename'=> $ofilename,
                            'keyid' => '-',
                            'disk' => '-',
                            'filename' => '-',
                            'nama_dokumen' => $setdokumen['nama_dokumen'],
                            'keterangan' => $setdokumen['keterangan'],
                            'nomor_layanan' => $nomor_layanan
                        ];
                        $savedokumen[] = new Layanan_dokumen($saveList);
                        //$path = $setdokumen['file']->storeAs($disk,$ofilename); //upload
                    }
                    $Layanan->layanan_dokumen()->saveMany($savedokumen);
                } else {
                    // Layanan::where(['nomor_layanan'=>$nomor_layanan])->update(['kd_status'=>'2']);
                    // $statusLayananBerkas=false;
                }
                if ($cekNOP['jenis_layanan_id'] == '8' && $statusLayananBerkas) {
                    $setDataUpdae = $request->only(['nik', 'nama', 'alamat', 'kelurahan', 'kecamatan', 'dati2', 'propinsi', 'nomor_telepon']);
                    //update data diri 1.7
                    LayananUpdate::updateDataDiri_sppt($setDataUpdae, $nopdefault);
                    LayananUpdate::updateDataDiri_dat_subjek_pajak($nopdefault, ['alamat_op' => $request->nop_alamat], $rawLayananObjek['nik_wp_pembetulan']);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                // delete file upload $nomor_layanan
                $disk = 'dokumen_pendukung/' . $nomor_layanan;
                $isExists = Storage::disk('public')->exists($disk);
                if ($isExists) {
                    $isExists = Storage::disk('public')->deleteDirectory($disk);
                    $isExists = Storage::disk('public')->deleteDirectory($disk);
                }
                //"Terjadi Kesalahan, Harap Hubungi Admin";//
                $msg = "Terjadi Kesalahan,coba ulangi lagi"; //$e->getMessage();
                $status = false;
                return response()->json(["msg" => $msg, "status" => false]);
            }
            if (!$statusLayananBerkas) {
                return response()->json([
                    "msg" => "Layanan pembetulan berhasil di buat.\n Harap Melakukan update Berkas ",
                    "status" => false
                ]);
            }
            $return = [
                "msg" => "<p>Layanan pembetulan berhasil di buat.</p> 
                        <p>Download Tanda terima.</p>
                        <a href='" . url("laporan_input/tanda_terima?nomor_layanan=" . $nomor_layanan) . "'
                        target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i></a>",
                "status" => true,
                'curl' => $nomor_layanan
            ];
            if ($cekNOP['jenis_layanan_id'] == '8') {
                $return = [
                    "msg" => "<p>Layanan pembetulan berhasil di buat.</p> 
                            <p>Download Tanda terima.</p>
                            <a href='" . url("laporan_input/tanda_terima?nomor_layanan=" . $nomor_layanan) . "'
                            target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i> Tanda Terima</a>
                            <a href='" . url("laporan_input/cetak_pdf?nomor_layanan=" . $nomor_layanan) . "'
                            target='_BLANK' class='btn btn-primary btn-block'><i class='fas fa-download'></i> SK </a>",
                    "status" => true,
                    'curl' => $nomor_layanan
                ];
            }
            //<p>SK dapat di keluarkan setelah proses verifikasi selesai.</p>
            if ($status) {
                $cekBadan = $request->only(['jenisObjek']);
                if ($cekNOP['jenis_layanan_id'] == '8' && $cekBadan['jenisObjek'] != '2') {
                    dispatch(new PendataanPenilaianJob(['nomor_layanan' => $nomor_layanan, 'pemutakhiran_by' => auth()->user()->id]))->onQueue('layanan');
                } else {
                    dispatch(new disposisiPenelitian($nomor_layanan))->onQueue('layanan');
                }
            }
            //,'curl'=>$nomor_layanan
            // if($cekNOP['jenis_layanan_id']=='8'){
            //     $return['curl']=$nomor_layanan;
            // }
        }
        return response()->json($return);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function storePembetulanOnline(Request $request)
    {

        // dd($request->all());
        DB::beginTransaction();
        try {
            //code...
            $nomor_layanan = Layanan_conf::nomor_layanan();
            $layanan['nomor_layanan'] = $nomor_layanan;
            $layanan['jenis_layanan_id'] = '8';
            $layanan['jenis_layanan_nama'] = 'Pembetulan Data WP (online)';
            $layanan['nik'] = $request->nik;
            $layanan['nama'] = $request->nama;
            $layanan['alamat'] = $request->alamat;
            $layanan['kelurahan'] = $request->kelurahan;
            $layanan['kecamatan'] = $request->kecamatan;
            $layanan['dati2'] = $request->dati2;
            $layanan['propinsi'] = $request->propinsi;
            $layanan['nomor_telepon'] = $request->nomor_telepon;
            $layanan['keterangan'] = 'Pembetulan subjek/wajib pajak secara mandiri(Online)';
            $layanan['jenis_objek'] = '1';

            $hasil = Layanan::create($layanan);
            $layanan_objek['nomor_layanan'] = $layanan['nomor_layanan'];
            $layanan_objek['kd_propinsi'] = $request->kd_propinsi;
            $layanan_objek['kd_dati2'] = $request->kd_dati2;
            $layanan_objek['kd_kecamatan'] = $request->kd_kecamatan;
            $layanan_objek['kd_kelurahan'] = $request->kd_kelurahan;
            $layanan_objek['kd_blok'] = $request->kd_blok;
            $layanan_objek['no_urut'] = $request->no_urut;
            $layanan_objek['kd_jns_op'] = $request->kd_jns_op;

            $layanan_objek['nik_wp'] = $layanan['nik'];
            $layanan_objek['nama_wp'] = $layanan['nama'];
            $layanan_objek['alamat_wp'] = $layanan['alamat'] . ' - ' . $request->blok_kav_no_wp;
            $layanan_objek['kelurahan_wp'] = $layanan['kelurahan'];
            $layanan_objek['kecamatan_wp'] = $layanan['kecamatan'];
            $layanan_objek['dati2_wp'] = $layanan['dati2'];
            $layanan_objek['propinsi_wp'] = $layanan['propinsi'];
            $layanan_objek['telp_wp'] = $layanan['nomor_telepon'];
            $layanan_objek['luas_bumi'] = $request->luas_bumi;
            $layanan_objek['luas_bng'] = $request->luas_bng;
            Layanan_objek::create($layanan_objek);

            $disk = 'pendukung';
            // scan ktp
            $filektp = $request->file('scan_ktp');
            $ofilenamektp = time() . '_ktp.' . $filektp->getClientOriginalExtension();

            $saveKtp = [
                'tablename' => "layanan",
                'keyid' => $nomor_layanan,
                'disk' => $disk,
                'filename' => $ofilenamektp,
                'nama_dokumen' => 'Scan KTP',
                'keterangan' => '-',
                'nomor_layanan' => $nomor_layanan
            ];

            if (Storage::disk($disk)->put($ofilenamektp, File::get($filektp))) {
                Layanan_dokumen::create($saveKtp);
            }

            // bukti kepemilikan
            $fileBukti = $request->file('bukti_kepemilikan');
            $orifileBukti = time() . '_kepemilikan.' . $fileBukti->getClientOriginalExtension();
            $saveBukti = [
                'tablename' => "layanan",
                'keyid' => $nomor_layanan,
                'disk' => $disk,
                'filename' => $orifileBukti,
                'nama_dokumen' => 'Bukti Kepemilikan',
                'keterangan' => '-',
                'nomor_layanan' => $nomor_layanan
            ];

            if (Storage::disk($disk)->put($orifileBukti, File::get($fileBukti))) {
                Layanan_dokumen::create($saveBukti);
            }


            $fileSelfie = $request->file('selfie_ktp');
            $ofiSelfie = time() . '_selfie.' . $fileSelfie->getClientOriginalExtension();
            $saveSelfie = [
                'tablename' => "layanan",
                'keyid' => $nomor_layanan,
                'disk' => $disk,
                'filename' => $ofiSelfie,
                'nama_dokumen' => 'Selfie bersama KTP Bukti Kepemilikan',
                'keterangan' => '-',
                'nomor_layanan' => $nomor_layanan
            ];
            // $fileSelfie->storeAs($disk, $ofiSelfie);
            if (Storage::disk($disk)->put($ofiSelfie, File::get($fileSelfie))) {
                Layanan_dokumen::create($saveSelfie);
            }
            DB::commit();
            $status = true;

            // kirim pesan 
            /* Http::post(config('app.whatsapp_send_url'), [
                'number' => $request->nomor_telepon,
                'message' => "Permohonan dari Bapak/Ibu " . $layanan['nama'] . "  telah di terima dengan nomor pelayanan \n *" . $hasil->nomor_layanan . "* ! Estimasi penelitian kurang lebih 3 hari kerja. \n\n\n SIPANJI *#NOREPLY*",
            ]); */
        } catch (\Throwable $th) {
            //throw $th;
            log::emergency($th);
            DB::rollBack();
            $status = false;
            $hasil = [];
        }

        return ['status' => $status, 'hasil' => $hasil];
    }
}
