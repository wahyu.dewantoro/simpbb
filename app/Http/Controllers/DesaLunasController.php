<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\Kelurahan;
use App\DesaLunas;
use App\Exports\DesaLunasExport;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
// use DOMPDF as PDF;
// use Datatables;
use Yajra\Datatables\Datatables;
// use SnappyPdf;
use SnappyPdf as PDF;


class DesaLunasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('permission:desa lunas')->only(['index']);
        // $this->middleware('permission:add_desa_lunas')->only(['create', 'store', 'edit', 'update', 'destroy']);
        // $this->middleware('permission:edit_users')->only(['edit', 'update']);
        // $this->middleware('permission:delete_users')->only(['destroy']);
    }


    public function coreSql($user, $thn)
    {

        $kk = "";
        // $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $kd_kecamatan = "";
        $kk = "";
        if (in_array('Kecamatan', $role)) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
            foreach ($kec as $kec) {
                $kd_kecamatan .= " sppt.kd_kecamatan ='" . $kec . "' or";
                $kk .= " A.kd_kecamatan ='" . $kec . "' or";
            }

            $kd_kecamatan = " and ( " . substr($kd_kecamatan, 0, -2) . " )";
            $kk = " and ( " . substr($kk, 0, -2) . " )";
        }
        $basicQuery =  DB::select("SELECT A.*, B.NM_KECAMATAN,(
            select sum(pbb_yg_harus_dibayar_sppt-potongan)
            FROM pbb.core_sppt sppt
            LEFT JOIN
            (SELECT a.kd_propinsi,
            a.kd_dati2,
            a.kd_kecamatan,
            a.kd_kelurahan,
            a.kd_blok,
            a.no_urut,
            a.kd_jns_op,
            a.thn_pajak_sppt,
            kd_status
            FROM sim_pbb.sppt_penelitian a
            LEFT JOIN SIM_PBB.DATA_BILLING b
            ON a.data_billing_id = b.data_billing_id
            WHERE kd_status = '0') aj
            ON     aj.kd_propinsi = sppt.kd_propinsi
            AND aj.kd_dati2 = sppt.kd_dati2
            AND aj.kd_kecamatan = sppt.kd_kecamatan
            AND aj.kd_kelurahan = sppt.kd_kelurahan
            AND aj.kd_blok = sppt.kd_blok
            AND aj.no_urut = sppt.no_urut
            AND aj.kd_jns_op = sppt.kd_jns_op
            AND aj.thn_pajak_sppt = sppt.thn_pajak_sppt
            WHERE     sppt.thn_pajak_sppt = a.tahun_pajak
            AND aj.thn_pajak_sppt IS NULL
            AND sppt.kd_kecamatan = a.kd_kecamatan
            AND status_pembayaran_sppt IN ('0', '1')
            and sppt.kd_kelurahan=a.kd_kelurahan   AND get_buku (pbb_yg_harus_dibayar_sppt) IN (1,2)
            ) nilai_baku,C.NM_KELURAHAN from DESA_LUNAS A
                                        LEFT JOIN pbb.REF_KECAMATAN B ON A.KD_KECAMATAN = B.KD_KECAMATAN
                                        LEFT JOIN pbb.REF_KELURAHAN C ON A.KD_KECAMATAN = C.KD_KECAMATAN AND A.KD_KELURAHAN = C.KD_KELURAHAN 
                                        where TAHUN_PAJAK = '$thn' 
                                        " . $kk . "
                                        order by tanggal_lunas asc,created_at asc");
        return $basicQuery;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $thn = $request->thn_pajak ?? date('Y');
            $basicQuery = $this->coreSql(Auth()->user(), $thn);
            return Datatables::of($basicQuery)
                ->addColumn('aksi', function ($row) {
                    $aksi = '<a href="' . route('realisasi.desalunas.edit', [$row->kd_kecamatan . '_' . $row->kd_kelurahan . '_' . $row->tahun_pajak]) . '"><i class="fas fa-edit text-info"></i></a>';

                    $aksi .= ' <a onclick="return confirm(\" Apakah anda yakin ? \")" href="' . url('realisasi/desalunas-delete', [$row->kd_kecamatan . '_' . $row->kd_kelurahan . '_' . $row->tahun_pajak]) . '"><i class="fas fa-trash text-danger"></i></a>';
                    return $aksi;
                })
                ->addColumn('baku', function ($row) {
                    return number_format(bakuDesa($row->kd_kecamatan, $row->kd_kelurahan), 0, '', '.');
                })
                ->addColumn('lunas', function ($row) {
                    return tglIndo($row->tanggal_lunas);
                })
                ->rawColumns(['aksi', 'baku', 'lunas'])
                ->make(true);
        }

        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $input = 'kecamatan';
        $reskec = array_filter($role, function ($item) use ($input) {
            if (stripos($item, $input) !== false) {
                return true;
            }
            return false;
        });

        $inputdesa = 'desa';
        $resdes = array_filter($role, function ($item) use ($inputdesa) {
            if (stripos($item, $inputdesa) !== false) {
                return true;
            }
            return false;
        });

        $is_kecamatan = count($reskec) > 0 ? true : false;
        $is_kelurahan = count($resdes) > 0 ? true : false;

        return view('desa_lunas/index', compact('is_kecamatan', 'is_kelurahan'));
    }

    public function pdf(Request $request)
    {
        $tahun = $request->thn_pajak ?? date('Y');
        $data = $this->coreSql(Auth()->user(), $tahun);
        $page = view('desa_lunas/cetak_pdf', compact('data', 'tahun'));
        $pdf = pdf::loadHTML($page)
            ->setPaper('A4');
        $filename = time() . auth()->user()->id . '.pdf';
        $pdf->setOption('enable-local-file-access', true);
        return $pdf->stream($filename);
    }

    public function excel(Request $request)
    {
        $tahun = $request->thn_pajak ?? date('Y');
        return Excel::download(new DesaLunasExport($tahun), 'desaLunas.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Tambah Desa Lunas',
            'method' => 'post',
            'action' => route('realisasi.desalunas.store')
        ];
        $kecamatan = Kecamatan::get();
        $kelurahan = '-';
        return view('desa_lunas.form', compact('data', 'kecamatan', 'kelurahan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = [
            'kd_kecamatan' => $request->kd_kecamatan,
            'kd_kelurahan' => $request->kd_kelurahan,
            'tahun_pajak' => $request->tahun_pajak,
            'tanggal_lunas' => new carbon($request->tanggal_lunas)
        ];
        // dd($data);
        DB::beginTransaction();
        try {
            //code...
            DesaLunas::create($data);
            $flash = [
                'success' => 'Berhasil menambahkan desa lunas.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            Log::error($e);
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        // dd($flash);
        return redirect(route('realisasi.desalunas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $exp=explode('_',$id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $exp = explode('_', $id);
        $lunas = DesaLunas::where('kd_kecamatan', $exp[0])->where('kd_kelurahan', $exp[1])->where('tahun_pajak', $exp[2])->firstorfail();

        // dd($lunas);
        $data = [
            'title' => 'Edit Desa Lunas',
            'method' => 'patch',
            'action' => route('realisasi.desalunas.update', $id),
            'lunas' => $lunas
        ];
        $kecamatan = Kecamatan::get();
        $kelurahan = Kelurahan::where('kd_kecamatan',  $exp[0])->get();
        // dd($kelurahan);
        return view('desa_lunas.form', compact('data', 'kecamatan', 'kelurahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();
        try {
            //code...
            $exp = explode('_', $id);
            $lunas = DesaLunas::where('kd_kecamatan', $exp[0])->where('kd_kelurahan', $exp[1])->where('tahun_pajak', $exp[2]);
            $data = [
                'kd_kecamatan' => $request->kd_kecamatan,
                'kd_kelurahan' => $request->kd_kelurahan,
                'tahun_pajak' => $request->tahun_pajak,
                'tanggal_lunas' => new carbon($request->tanggal_lunas)
            ];

            $lunas->update($data);
            $flash = [
                'success' => 'berhasil di update.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        // dd($flash);
        return redirect(route('realisasi.desalunas.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $exp = explode('_', $id);
            $lunas = DesaLunas::where('kd_kecamatan', $exp[0])->where('kd_kelurahan', $exp[1])->where('tahun_pajak', $exp[2]);
            $lunas->delete();
            $flash = [
                'success' => 'berhasil di hapus data.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        return redirect(route('realisasi.desalunas.index'))->with($flash);
    }
}
