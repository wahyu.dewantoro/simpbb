<?php

namespace App\Http\Controllers;

use App\Exports\NopAktif;
use App\Exports\penangguhanIventarisasiExcel;
use App\Exports\templateImportIventarisasi;
use App\Helpers\gallade;
use App\Helpers\Layanan_conf;
use App\Helpers\Pajak;
use App\Helpers\PembatalanObjek;
use App\Imports\NopRekonImport;
use App\Jobs\disposisiPenelitian;
use App\Kecamatan;
use App\Kelurahan;
use App\Models\Jenis_layanan;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use App\Models\PendataanBatch;
use App\Models\PendataanObjek;
use App\penelitianTask;
use App\SpptKoreksi;
use App\SpptOltp;
use App\Traits\SpptKoreksiData;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use SnappyPdf as PDF;
use DataTables;
use Illuminate\Support\Facades\Log;

class PenangguhanNopController extends Controller
{
    use SpptKoreksiData;

    public function JenisPenangguhan()
    {
        $jns = KategoriIventarisasi();
        return $jns;
    }

    public function index(Request $request)
    {
        $tab = $request->tab ?? 'Koreksi Piutang';
        return view('penangguhan.indexNew', compact('tab'));
    }

    public function formIventarisasi()
    {
        return view('penangguhan/form_iventarisasi');
    }

    public function formIventarisasiUpload()
    {
        return view('penangguhan/form_iventarisasi_upload');
    }

    public function TemplateIventarisasiUpload()
    {
        $array = [];


        for ($i = 1; $i <= 10; $i++) {
            $array[] = [
                'no' => $i,
                'nop' => null,
            ];
        }
        $filename = "template iventarisasi " . time() . ".xlsx";
        return Excel::download(new templateImportIventarisasi($array), $filename);
    }

    public function previewIventarisasiUpload(Request $request)
    {

        // return $request->all();
        $file = $request->file;
        $array = Excel::toArray(new NopRekonImport(), $file);

        $array = $array[0];

        $where = "";
        $objek = null;
        foreach ($array as $row) {
            if ($row['nop'] != '') {
                $nop_proses = onlyNumber($row['nop']);
                $where .= "(
                    kd_propinsi='" . substr($nop_proses, 0, 2) . "' and
                    kd_dati2='" . substr($nop_proses, 2, 2) . "' and
                    kd_kecamatan='" . substr($nop_proses, 4, 3) . "' and
                    kd_kelurahan='" . substr($nop_proses, 7, 3) . "' and
                    kd_blok='" . substr($nop_proses, 10, 3) . "' and
                    no_urut='" . substr($nop_proses, 13, 4) . "' and
                    kd_jns_op='" . substr($nop_proses, 17, 1) . "'
                ) or";
            }
        }

        if ($where != '') {
            $where = substr($where, 0, -2);

            $objek = DB::connection("oracle_satutujuh")->table("dat_objek_pajak")
                ->join('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', '=', 'dat_objek_pajak.subjek_pajak_id')
                ->selectRaw("kd_propinsi,kd_dati2,kd_Kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,nm_wp")
                ->whereraw($where)->get();
        }
        $jns_koreksi = $request->jns_koreksi;
        $tgl_surat = $request->tgl_surat;
        $no_surat = $request->no_surat;
        return view('penangguhan/form_iventarisasi_upload_preview', compact('jns_koreksi', 'tgl_surat', 'no_surat', 'objek'));
    }

    public function storeIventarisasiUpload(Request $request)
    {
        // return $request->all();
        $cc = count($request->kd_propinsi);
        $datas = [];

        $ct = new Carbon($request->tgl_surat);
        $tanggal = $ct->format('Y-m-d');
        $tahun = $ct->format('Y');
        for ($i = 0; $i < $cc; $i++) {
            $datas[] = [
                'thn_pajak_sppt' => $tahun,
                'no_surat' => $request->no_surat,
                'tgl_surat' => $tanggal,
                'jns_koreksi' => $request->jns_koreksi,
                'tahun' => $tahun,
                'kd_propinsi' => $request->kd_propinsi[$i],
                'kd_dati2' => $request->kd_dati2[$i],
                'kd_kecamatan' => $request->kd_kecamatan[$i],
                'kd_kelurahan' => $request->kd_kelurahan[$i],
                'kd_blok' => $request->kd_blok[$i],
                'no_urut' => $request->no_urut[$i],
                'kd_jns_op' => $request->kd_jns_op[$i]
            ];
        }
        // DB::beginTransaction();
        try {
            foreach ($datas as $data) {
                $arrayBatal = ['01', '02', '03'];
                if (in_array($data['jns_koreksi'], $arrayBatal)) {
                    $res = PembatalanObjek::iventarisasi($data);
                } elseif ($request->jns_koreksi == '07') {
                    $res = PembatalanObjek::penyesuaian($data);
                } else {
                    $res = PembatalanObjek::cancel($data);
                }
            }
            // DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            // DB::rollBack();
        }
        return redirect(url('pendataan/penangguhan'))->with(['success' => "Telah diproses"]);
    }

    public function formUploadKoreksi()
    {
        return view('penangguhan/form_upload_koreksi');
    }

    public function previewUploadkoreksi(Request $request)
    {
        // return $request->all();

        $file = $request->file;
        $array = Excel::toArray(new NopRekonImport(), $file);
        $array = $array[0];

        // $this->info(json_encode())

        $daftar_nop = [];
        foreach ($array as $row) {

            if ($row['nop'] != '') {
                $nop_proses = onlyNumber($row['nop']);
                $noptahun = [
                    'kd_propinsi' => substr($nop_proses, 0, 2),
                    'kd_dati2' => substr($nop_proses, 2, 2),
                    'kd_kecamatan' => substr($nop_proses, 4, 3),
                    'kd_kelurahan' => substr($nop_proses, 7, 3),
                    'kd_blok' => substr($nop_proses, 10, 3),
                    'no_urut' => substr($nop_proses, 13, 4),
                    'kd_jns_op' => substr($nop_proses, 17, 1),
                    'tahun' => $row['tahun_pajak']
                ];

                $daftar_nop[] = $noptahun;
            }
        }




        // dd($daftar_nop);

        // dd($array);
    }

    public function formKoreksi()
    {
        return view('penangguhan/form_koreksi');
    }

    public function storeKoreksi(Request $request)
    {
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            $jenis = $request->jenis ?? 0;
            switch ($jenis) {
                case '1':
                    # code...
                    $jns_koreksi = '3';
                    $kd = 'IV';
                    break;
                default:
                    # code...
                    $jns_koreksi = '3';
                    $kd = 'KP';
                    break;
            }


            $nop = $request->nop;
            $thn_pajak_sppt = $request->thn_pajak_sppt;
            $keterangan = $request->keterangan;
            // $trx = $this->Notransaksi($blokir);
            $trx = PembatalanObjek::NoTransaksi($kd, '07');
            $res = onlyNumber($nop);
            $var['kd_propinsi'] = substr($res, 0, 2);
            $var['kd_dati2'] = substr($res, 2, 2);
            $var['kd_kecamatan'] = substr($res, 4, 3);
            $var['kd_kelurahan'] = substr($res, 7, 3);
            $var['kd_blok'] = substr($res, 10, 3);
            $var['no_urut'] = substr($res, 13, 4);
            $var['kd_jns_op'] = substr($res, 17, 1);
            $var['thn_pajak_sppt'] = $thn_pajak_sppt;
            $res = $var;

            $ins = $var;
            $ins['tgl_surat'] = new Carbon($request->tgl_surat);
            $ins['no_surat'] = $request->no_surat;
            $ins['jns_koreksi'] = $jns_koreksi;
            $ins['nilai_koreksi'] = onlyNumber($request->nilai_koreksi);
            $ins['keterangan'] = $keterangan;
            $ins['no_transaksi'] = $trx;
            $ins['created_at'] = db::raw("sysdate");
            $ins['created_by'] = Auth()->user()->id;

            $ins['verifikasi_kode'] = 1;
            $ins['verifikasi_keterangan'] = $keterangan;
            $ins['verifikasi_by'] = auth()->user()->id;
            $ins['verifikasi_at'] = db::raw("sysdate");

            DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);
            DB::connection("oracle_satutujuh")->table('sppt')->where($var)->update(['status_pembayaran_sppt' => '3']);

            DB::connection("oracle_satutujuh")->commit();
            $res['msg'] = "berhasil di proses ";
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection("oracle_satutujuh")->rollBack();
            $res['msg'] = $th->getMessage();
        }
        return redirect(url('pendataan/penangguhan'));
    }

    public function penangguhanNop(Request $request)
    {
        if ($request->ajax()) {
            $var = $request->nop ?? '';
            $var = onlyNumber($var);
            if ($var != '') {
                $kd_propinsi = substr($var, 0, 2);
                $kd_dati2 = substr($var, 2, 2);
                $kd_kecamatan = substr($var, 4, 3);
                $kd_kelurahan = substr($var, 7, 3);
                $kd_blok = substr($var, 10, 3);
                $no_urut = substr($var, 13, 4);
                $kd_jns_op = substr($var, 17, 1);


                $objek = DB::connection('oracle_dua')->table(db::raw("dat_objek_pajak dop"))
                    ->leftjoin(db::raw("dat_op_bumi dob"), function ($join) {
                        $join->on('dop.kd_propinsi', '=', 'dob.kd_propinsi')
                            ->on('dop.kd_dati2', '=', 'dob.kd_dati2')
                            ->on('dop.kd_kecamatan', '=', 'dob.kd_kecamatan')
                            ->on('dop.kd_kelurahan', '=', 'dob.kd_kelurahan')
                            ->on('dop.kd_blok', '=', 'dob.kd_blok')
                            ->on('dop.no_urut', '=', 'dob.no_urut')
                            ->on('dop.kd_jns_op', '=', 'dob.kd_jns_op');
                    })
                    ->leftjoin(db::raw("dat_subjek_pajak dsp"), 'dsp.subjek_pajak_id', '=', 'dop.subjek_pajak_id')
                    ->selectRaw(db::raw("dop.*,jns_bumi,kd_znt,nm_wp,jalan_wp,blok_kav_no_wp,rw_wp,rt_wp,kelurahan_wp,kota_wp,kd_pos_wp,telp_wp,npwp,status_pekerjaan_wp,
                                        (select count(1) from dat_op_bangunan 
                                        where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
                                        and kd_blok=dop.kd_blok
                                        and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
                                        ) jml_bng
        "))->whereraw("dop.kd_propinsi='35' and dop.kd_dati2='07' and dop.kd_kecamatan='$kd_kecamatan' and dop.kd_kelurahan='$kd_kelurahan' and dop.kd_blok='$kd_blok' and dop.no_urut='$no_urut' and dop.kd_jns_op='$kd_jns_op' ")->first();
                $where['sppt.kd_propinsi'] = $kd_propinsi;
                $where['sppt.kd_dati2'] = $kd_dati2;
                $where['sppt.kd_kecamatan'] = $kd_kecamatan;
                $where['sppt.kd_kelurahan'] = $kd_kelurahan;
                $where['sppt.kd_blok'] = $kd_blok;
                $where['sppt.no_urut'] = $no_urut;
                $where['sppt.kd_jns_op'] = $kd_jns_op;
                $pembayaran = DB::connection("oracle_satutujuh")->table("sppt")
                    ->leftjoin('sppt_potongan', function ($join) {
                        $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                            ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                            ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                            ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                            ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                            ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                            ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                            ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
                    })
                    ->leftjoin(db::raw("( select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,tgl_surat,no_surat,keterangan,no_transaksi
                                        from sppt_koreksi where verifikasi_kode='1' ) sppt_koreksi"), function ($join) {
                        $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                            ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                            ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                            ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                            ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                            ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                            ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                            ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt')
                            ->on('sppt.status_pembayaran_sppt', '=', 3);
                    })
                    ->leftjoin(
                        db::raw("( SELECT a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.tahun_pajak thn_pajak_sppt, LISTAGG ( kobil, ', ') WITHIN GROUP (ORDER BY kobil) kobil
                                    FROM sim_pbb.billing_kolektif a
                                    JOIN sim_pbb.data_billing b ON a.data_billing_id=b.data_billing_id
                                    WHERE kd_status = '0' AND b.expired_at > SYSDATE and b.deleted_at is null
                                GROUP BY a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.tahun_pajak) sppt_penelitian"),
                        function ($join) {
                            $join->on('sppt.kd_propinsi', '=', 'sppt_penelitian.kd_propinsi')
                                ->on('sppt.kd_dati2', '=', 'sppt_penelitian.kd_dati2')
                                ->on('sppt.kd_kecamatan', '=', 'sppt_penelitian.kd_kecamatan')
                                ->on('sppt.kd_kelurahan', '=', 'sppt_penelitian.kd_kelurahan')
                                ->on('sppt.kd_blok', '=', 'sppt_penelitian.kd_blok')
                                ->on('sppt.no_urut', '=', 'sppt_penelitian.no_urut')
                                ->on('sppt.kd_jns_op', '=', 'sppt_penelitian.kd_jns_op')
                                ->on('sppt.thn_pajak_sppt', '=', 'sppt_penelitian.thn_pajak_sppt');
                        }
                    )
                    ->leftjoin('pembayaran_sppt', function ($join) {
                        $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
                            ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
                            ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
                            ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
                            ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
                            ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
                            ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
                            ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
                    })
                    ->where($where)
                    ->whereraw("sppt.thn_pajak_sppt>=2003")
                    ->groupbyraw("sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.thn_pajak_sppt, nm_wp_sppt, sppt.pbb_yg_harus_dibayar_sppt, NVL (nilai_potongan, 0), sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt_koreksi.keterangan, sppt_koreksi.tgl_surat, SPPT_KOREKSI.NO_SURAT, CASE WHEN status_pembayaran_sppt!='1' THEN spo.get_denda ( pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt.tgl_jatuh_tempo_sppt, SYSDATE) ELSE NULL END, status_pembayaran_sppt, kobil,sppt_koreksi.no_transaksi")
                    ->select(db::raw(" sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.thn_pajak_sppt, nm_wp_sppt, status_pembayaran_sppt, sppt.pbb_yg_harus_dibayar_sppt pbb, NVL (nilai_potongan, 0) potongan, sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb_akhir, CASE WHEN status_pembayaran_sppt!='1' THEN spo.get_denda ( pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt.tgl_jatuh_tempo_sppt, SYSDATE) ELSE NULL END denda, sppt_koreksi.keterangan, sppt_koreksi.tgl_surat, SPPT_KOREKSI.NO_SURAT, SUM (denda_sppt) denda_bayar, SUM (jml_sppt_yg_dibayar) jumlah_bayar, LISTAGG ( TO_CHAR (tgl_pembayaran_sppt, 'yyyy-mm-dd'), ',') WITHIN GROUP (ORDER BY tgl_pembayaran_sppt) AS tgl_bayar, kobil,sppt_koreksi.no_transaksi no_koreksi"))
                    ->orderby("sppt.thn_pajak_sppt", 'asc')->get();

                $jenis = $this->JenisPenangguhan();
                $kec = DB::connection("oracle_satutujuh")->table('ref_kecamatan')->where('kd_kecamatan', $kd_kecamatan)
                    ->select('nm_kecamatan')->first();
                $kecamatan = $kec->nm_kecamatan;
                $kel = DB::connection("oracle_satutujuh")->table("ref_kelurahan")->where('kd_kecamatan', $kd_kecamatan)
                    ->where('kd_kelurahan', $kd_kelurahan)
                    ->select('nm_kelurahan')->first();
                $kelurahan = $kel->nm_kelurahan;
                return view('penangguhan/_index', compact('objek', 'pembayaran', 'kecamatan', 'kelurahan', 'jenis'));
            } else {
                return '';
            }
        }

        abort('404');
    }


    public function cekNopKp(Request $request)
    {
        $nop = $request->nop;
        $kd_propinsi = substr($nop, 0, 2);
        $kd_dati2 = substr($nop, 2, 2);
        $kd_kecamatan = substr($nop, 4, 3);
        $kd_kelurahan = substr($nop, 7, 3);
        $kd_blok = substr($nop, 10, 3);
        $no_urut = substr($nop, 13, 4);
        $kd_jns_op = substr($nop, 17, 1);
        $thn_pajak_sppt = $request->thn_pajak_sppt;

        // cek di sppt koreksi
        $koreksi = SpptKoreksi::where(
            [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'thn_pajak_sppt' => $thn_pajak_sppt,
            ]
        )->first();

        if ($koreksi == null) {
            $oltp = SpptOltp::where(
                [
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_pajak_sppt' => $thn_pajak_sppt
                ]
            )->whereraw("pbb_yg_harus_dibayar_sppt>0")->first();
            if ($oltp != null) {
                $pbb = $oltp->pbb_yg_harus_dibayar_sppt ?? 0;
                $no_transaksi = "";
                $keterangan = "";
                $status = '1';
                if ($oltp->data_billing_id != '') {
                    $status = '0';
                    $keterangan .= "NOP masuk kedalam Kode Billing / Nota Perhitungan";
                }

                if ($pbb == 0) {
                    $status = '0';
                    $keterangan .= "NOP tidak memiliki tagihan";
                }
            } else {
                $status = '0';
                $pbb = "";
                $no_transaksi = "";
                $keterangan = "NOP tidak di temukan";
            }
        } else {
            $status = '0';
            $pbb = "";
            $no_transaksi = $koreksi->no_transaksi;
            $keterangan = "Nop telah di ";

            switch ($koreksi->jns_koreksi) {
                case '1':
                    # code...
                    $keterangan .= " blokir karena Mutasi Pecah / Gabung";
                    break;
                case '2':
                    # code...
                    $keterangan .= " blokir karena penangguhan";
                    break;
                default:
                    # code...
                    $keterangan .= " koreksi piutang";
                    break;
            }
        }


        return [
            'status' => $status,
            'pbb' => $pbb,
            'no_transaksi' => $no_transaksi,
            'keterangan' => $keterangan
        ];
    }

    public function getDataAll(Request $request) {}


    public function penonaktifan(Request $request)
    {
        // return $request->all();

        $kd_propinsi = $request->kd_propinsi;
        $kd_dati2 = $request->kd_dati2;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_blok = $request->kd_blok;
        $no_urut = $request->no_urut;
        $kd_jns_op = $request->kd_jns_op;
        $alasan = $request->alasan;
        $als  = $this->JenisPenangguhan()[$request->alasan];
        if ($request->keterangan <> '') {
            $als .= ', ' . $request->keterangan;
        }

        Db::beginTransaction();
        try {

            // proses insert data pendataan
            $userid = user()->id;
            $now = Carbon::now();

            $jns_pendataan = '5';
            $batchData = [
                'jenis_pendataan_id' => $jns_pendataan,
                'tipe' => 'PD',
                'verifikasi_at' => $now,
                'verifikasi_by' => $userid,
                'verifikasi_deskripsi' => $als
            ];
            $batch = PendataanBatch::create($batchData);
            $jns = '5';

            if ($request->alasan == '4') {
                $jns = '4';
            }


            $batal = DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,substr(A.SUBJEK_PAJAK_ID,1,16) SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,5 JNS_BUMI FROM DAT_OBJEK_PAJAK A
            left JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
            left JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
            WHERE a.kd_kecamatan='$kd_kecamatan'
            AND a.kd_kelurahan='$kd_kelurahan'
            AND a.kd_blok='$kd_blok'
            AND a.no_urut='$no_urut'
            AND a.kd_jns_op='$kd_jns_op' "))[0];

            $nop_asal = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
            $dat_obj = [
                'pendataan_batch_id' => $batch->id,
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' =>  $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'nop_asal' => onlyNumber($nop_asal),
                'subjek_pajak_id' => $batal->subjek_pajak_id ?? '',
                'nm_wp' => $batal->nm_wp ?? '',
                'jalan_wp' => $batal->jalan_wp ?? '',
                'blok_kav_no_wp' => $batal->blok_kav_no_wp ?? '',
                'rw_wp' => padding(($batal->rw_wp ?? ''), 0, 2),
                'rt_wp' => padding(($batal->rt_wp ?? ''), 0, 3),
                'kelurahan_wp' => ($batal->kelurahan_wp ?? ''),
                'kecamatan_wp' => '-',
                'propinsi_wp' => '-',
                'kota_wp' => $batal->kota_wp ?? '',
                'kd_pos_wp' => ($batal->kd_pos_wp ?? ''),
                'telp_wp' => ($batal->telp_wp ?? ''),
                'npwp' => null,
                'status_pekerjaan_wp' => ($batal->status_pekerjaan_wp ?? ''),
                'no_persil' => ($batal->no_persil ?? ''),
                'jalan_op' => ($batal->jalan_op ?? ''),
                'blok_kav_no_op' => ($batal->blok_kav_no_op ?? ''),
                'rw_op' => padding(($batal->rw_op ?? ''), 0, 2),
                'rt_op' => padding(($batal->rt_op ?? ''), 0, 3),
                'kd_status_cabang' =>  null,
                'kd_status_wp' =>  $request->alasan == '6' ? '5' : $batal->kd_status_wp,
                'kd_znt' =>  $batal->kd_znt,
                'luas_bumi' => $batal->luas_bumi,
                'jns_bumi' => $batal->jns_bumi,
                'created_at' => $now,
                'created_by' => $userid,
                'updated_at' => $now,
                'updated_by' => $userid
            ];

            $objek = PendataanObjek::create($dat_obj);

            $spop = [];
            $nop_asal = onlyNumber($objek->nop_asal);
            $jtr = 2;
            $nop_proses = $nop_asal;

            $data = [
                'jenis_pendataan_id' => $objek->batch->jenis_pendataan_id,
                'pendataan_objek_id' => $objek->id,
                'nop_proses' => $nop_proses,
                'nop_asal' => $nop_asal,
                'jns_transaksi' => $jtr,
                'jalan_op' => $objek->jalan_op,
                'blok_kav_no_op' => $objek->blok_kav_no_op,
                'rt_op' => $objek->rt_op,
                'rw_op' => $objek->rw_op,
                'no_persil' => $objek->no_persil,
                'luas_bumi' => $objek->luas_bumi,
                'kd_znt' => $objek->kd_znt,
                'jns_bumi' => $objek->jns_bumi,
                'jml_bng' => $objek->jml_bng,
                'subjek_pajak_id' => $objek->subjek_pajak_id != '' ? $objek->subjek_pajak_id : substr($nop_asal, 2, 16),
                'nm_wp' => $objek->nm_wp <> '' ? $objek->nm_wp : 'WAJIB PAJAK',
                'jalan_wp' => $objek->jalan_wp <> '' ? $objek->jalan_wp : '-',
                'blok_kav_no_wp' => $objek->blok_kav_no_wp,
                'rt_wp' => $objek->rt_wp,
                'rw_wp' => $objek->rw_wp,
                'kelurahan_wp' => $objek->kelurahan_wp,
                'kecamatan_wp' => $objek->kecamatan_wp,
                'kota_wp' => $objek->kota_wp,
                'propinsi_wp' => $objek->propinsi_wp,
                'kd_pos_wp' => $objek->kd_pos_wp,
                'status_pekerjaan_wp' => $objek->status_pekerjaan_wp <> '' ? $objek->status_pekerjaan_wp : '5',
                'kd_status_wp' => $objek->kd_status_wp,
                'npwp' => $objek->npwp,
                'telp_wp' => $objek->telp_wp,
            ];


            $data['created_by'] = $objek->created_by;
            $data['created_at'] = db::raw('sysdate');
            $data['keterangan'] = "";

            $req = new \Illuminate\Http\Request();
            $req->merge($data);
            Pajak::prosesPendataan($req);

            $spop[] = $data['nop_proses'];
            // blokir semua tagihan untuk nop tersebut
            $ck = DB::connection("oracle_satutujuh")->select(db::raw("select sppt.kd_propinsi,
                sppt.kd_dati2,
                sppt.kd_kecamatan,
                sppt.kd_kelurahan,
                sppt.kd_blok,
                sppt.no_urut,
                sppt.kd_jns_op,
                sppt.no_urut,
                sppt.thn_pajak_sppt,jns_koreksi,no_surat
                        from sppt  
                        left join sppt_koreksi on sppt.kd_propinsi=sppt_koreksi.kd_propinsi and
                sppt.kd_dati2=sppt_koreksi.kd_dati2 and
                sppt.kd_kecamatan=sppt_koreksi.kd_kecamatan and
                sppt.kd_kelurahan=sppt_koreksi.kd_kelurahan and
                sppt.kd_blok=sppt_koreksi.kd_blok and
                sppt.no_urut=sppt_koreksi.no_urut and
                sppt.kd_jns_op=sppt_koreksi.kd_jns_op and
                sppt.thn_pajak_sppt=sppt_koreksi.thn_pajak_sppt
                where sppt.kd_propinsi='$kd_propinsi' and 
                sppt.kd_dati2='$kd_dati2' and 
                sppt.kd_kecamatan='$kd_kecamatan' and 
                sppt.kd_kelurahan='$kd_kelurahan' and 
                sppt.kd_blok='$kd_blok' and 
                sppt.no_urut='$no_urut' and 
                sppt.kd_jns_op='$kd_jns_op' and 
                status_pembayaran_sppt!='1' 
                and jns_koreksi is null"));

            // ambil nomoer transaksi untuk sppt_koreksi
            $ntr = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                ->select(db::raw("'IV'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'IV'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
                ->whereraw("no_transaksi like 'IV'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi;
            $koreksi = [];
            $wblokir = [];
            foreach ($ck as $tg) {
                if ($tg->thn_pajak_sppt == date('Y')) {
                    $update = ['status_pembayaran_sppt' => 3, 'tgl_terbit_sppt' => null];
                } else {
                    $update = ['status_pembayaran_sppt' => 3];
                }
                DB::connection('oracle_satutujuh')->table('sppt')
                    ->where([
                        'kd_propinsi' => $tg->kd_propinsi,
                        'kd_dati2' => $tg->kd_dati2,
                        'kd_kecamatan' => $tg->kd_kecamatan,
                        'kd_kelurahan' => $tg->kd_kelurahan,
                        'kd_blok' => $tg->kd_blok,
                        'no_urut' => $tg->no_urut,
                        'kd_jns_op' => $tg->kd_jns_op,
                        'thn_pajak_sppt' => $tg->thn_pajak_sppt
                    ])
                    ->update($update);

                $koreksi[] = [
                    'kd_propinsi' => $tg->kd_propinsi,
                    'kd_dati2' => $tg->kd_dati2,
                    'kd_kecamatan' => $tg->kd_kecamatan,
                    'kd_kelurahan' => $tg->kd_kelurahan,
                    'kd_blok' => $tg->kd_blok,
                    'no_urut' => $tg->no_urut,
                    'kd_jns_op' => $tg->kd_jns_op,
                    'thn_pajak_sppt' => $tg->thn_pajak_sppt,
                    'keterangan' => $alasan,
                    'verifikasi_kode' => '1',
                    'no_surat' => $this->JenisPenangguhan()[$request->alasan],
                    'tgl_surat' => db::raw("sysdate"),
                    'verifikasi_by' => auth()->user()->id,
                    'verifikasi_keterangan' => $als,
                    'no_transaksi' => $ntr,
                    'created_at' => db::raw("sysdate"),
                    'created_by' => auth()->user()->id,
                    'jns_koreksi' => '2'
                ];
            }

            if (!empty($koreksi)) {
                DB::connection('oracle_satutujuh')
                    ->table('sppt_koreksi')
                    ->insert($koreksi);
            }

            DB::commit();
            $rs = [
                'status' => '1',
                'pesan' => 'Objek Telah di non aktifkan '
            ];
        } catch (\Throwable $th) {
            log::error($th);
            DB::rollBack();
            $rs = [
                'status' => '0',
                'pesan' => $th->getMessage()
            ];
        }
        return $rs;
    }


    public function penangguhanNop_search(Request $request)
    {
        $search = $request->only('nop');
        $where = "";
        $result = [];
        if ($search && in_array('nop', array_keys($search))) {
            if ($search['nop'] != '') {
                $getNOP = Http::get(url('api/ceknop'), [
                    'nop' => $search['nop'],
                    'noTag' => true
                ]);
                if ($getNOP) {
                    $getData = (array)$getNOP->object();
                    if (count($getData)) {
                        if (isset($getData['status'])) {
                            $result = [(array)$getData['data']];
                        }
                    }
                }
            }
        }
        $datatables = DataTables::of($result);
        return $datatables->addColumn('nop', function ($data) use ($search) {
            if ($search && in_array('nop', array_keys($search))) {
                return $search['nop'];
            }
        })
            ->addColumn('action', function ($data) use ($search) {
                if ($search && in_array('nop', array_keys($search)) && $data['jns_transaksi_op'] != '3') {
                    if (!in_array($data['jns_bumi'], ['5', '4'])) {
                        $nop = $search['nop'];
                        return gallade::buttonInfo('<i class="fas fa-chevron-circle-right"></i> Penagguhan NOP', 'title="Pengaktifan NOP" data-penangguhan="' . $nop . '"');
                    }
                }
                return "<span class='badge badge-warning '>Nop non aktif</span>";
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function penangguhanNop_cetak(Request $request)
    {
        $search = $request->only('nop');
        $where = "";
        try {
            if ($search && in_array('nop', array_keys($search))) {
                if ($search['nop'] != '') {
                    $nop = explode('.', $search['nop']);
                    $blok_urut = explode('-', $nop['4']);
                    $where = "
                    AND lo.kd_propinsi='" . $nop[0] . "'
                    AND lo.kd_dati2='" . $nop[1] . "'
                    AND lo.kd_kecamatan='" . $nop[2] . "'
                    AND lo.kd_kelurahan='" . $nop[3] . "'
                    AND lo.kd_blok='" . $blok_urut[0] . "'
                    AND lo.no_urut='" . $blok_urut[1] . "'
                    AND lo.kd_jns_op='" . $nop[5] . "'
                    ";
                }
            }
            $year = Carbon::now()->year;
            $sql = "SELECT distinct lo.kd_propinsi,
                    lo.kd_dati2,
                    lo.kd_kecamatan,
                    lo.kd_kelurahan,
                    lo.kd_blok,
                    lo.no_urut,
                    lo.kd_jns_op,
                    lo.nik_wp ,
                    lo.NAMA_WP ,
                    lo.alamat_op ,
                    lo.luas_bumi ,
                    lo.luas_bng ,
                    lo.created_at,
                    dop.jns_transaksi_op 
                from sim_pbb.layanan_objek lo
                join sim_pbb.layanan 
                    on layanan.nomor_layanan=lo.nomor_layanan
                JOIN dat_objek_pajak dop 
                    ON dop.kd_propinsi=lo.kd_propinsi
                    AND dop.kd_dati2=lo.kd_dati2
                    AND dop.kd_kecamatan=lo.kd_kecamatan
                    AND dop.kd_kelurahan=lo.kd_kelurahan
                    AND dop.kd_blok=lo.kd_blok
                    AND dop.no_urut=lo.no_urut
                    AND dop.kd_jns_op=lo.kd_jns_op
                where layanan.jenis_layanan_id='10'
                and layanan.deleted_at is NULL
                AND dop.jns_transaksi_op!='3'
                " . $where . "
                order by 
                    lo.kd_propinsi,
                    lo.kd_dati2,
                    lo.kd_kecamatan,
                    lo.kd_kelurahan,
                    lo.kd_blok,
                    lo.no_urut,
                    lo.kd_jns_op";
            $dataSet = DB::connection('oracle_dua')->select(db::raw($sql));
            $result = [];
            $no = 1;
            foreach ($dataSet as $item) {
                $item = (array)$item;
                $result[] = [
                    $no,
                    $item['nik_wp'],
                    $item['nama_wp'],
                    $item['kd_propinsi'] . '.' .
                        $item['kd_dati2'] . '.' .
                        $item['kd_kecamatan'] . '.' .
                        $item['kd_kelurahan'] . '.' .
                        $item['kd_blok'] . '-' .
                        $item['no_urut'] . '.' .
                        $item['kd_jns_op'],
                    $item['alamat_op'],
                    $item['luas_bumi'],
                    $item['luas_bng'],
                    $item['created_at']
                ];
                $no++;
            }
            $export = new NopAktif($result);
            return Excel::download($export, 'nop_aktif.xlsx');
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            return response()->json(["msg" => $msg, "status" => false]);
        }
    }
    public function penangguhan_nop_proses(Request $request)
    {
        $requestGet = $request->only('data');
        $getNOP = Http::get(url('api/ceknop'), [
            'nop' => $requestGet['data']['nop'],
            'noTag' => true
        ]);
        $return = ["msg" => "Proses Penagguhan NOP gagal.", "status" => false];
        if ($getNOP) {
            $getData = (array)$getNOP->object();
            if (count($getData)) {
                if (!isset($getData['status'])) {
                    return response()->json(["msg" => $getData['msg'], "status" => false]);
                }
                $get = (array)$getData['data'];
                $result = true;
                DB::beginTransaction();
                $key = '2';
                $jenis_layanan_nama = Jenis_layanan::find($key)->nama_layanan;
                try {
                    $nomor_layanan = Layanan_conf::pengaktifan_nop();
                    if ($nomor_layanan) {
                        $saveLayanan['nomor_layanan'] = $nomor_layanan;
                        $saveLayanan['jenis_layanan_id'] = $key;
                        $saveLayanan['jenis_layanan_nama'] = $jenis_layanan_nama;
                        $saveLayanan['nik'] = '0000000000000000';
                        $saveLayanan['nama'] = 'Penagguhan NOP  [1 Permohonan ]';
                        $saveLayanan['alamat'] = '-';
                        $saveLayanan['kelurahan'] = '-';
                        $saveLayanan['kecamatan'] = '-';
                        $saveLayanan['dati2'] = 'KABUPATEN MALANG';
                        $saveLayanan['propinsi'] = 'JAWA TIMUR';
                        $saveLayanan['nomor_telepon'] = '0000000';
                        $saveLayanan['keterangan'] = 'Penagguhan NOP';
                        $saveLayanan['jenis_objek'] = '4';
                        $saveLayanan['kd_status'] = '0';
                        $Layanan = new Layanan($saveLayanan);
                        $Layanan->save();
                        $nop = explode('.', $request['data']['nop']);
                        $blok_urut = explode('-', $nop['4']);
                        $keteranganAlasan = $request['data']['keterangan'];
                        if (is_array($request['data']['keterangan'])) {
                            $keteranganAlasan = join(' . ', $request['data']['keterangan']);
                        }
                        $saveLayananObjek = [
                            'nik_wp' => $get['nik_wp'],
                            'nama_wp' => $get['nama_wp'],
                            'alamat_wp' => $get['alamat_wp'] ?? '-',
                            'kelurahan_wp' => $get['kelurahan_wp'] ?? '-',
                            'kecamatan_wp' => $get['kecamatan_wp'] ?? '-',
                            'dati2_wp' => $get['dati2_wp'],
                            'propinsi_wp' => $get['propinsi_wp'],
                            'rt_wp' => $get['rt_wp'],
                            'rw_wp' => $get['rw_wp'],
                            'telp_wp' => $get['notelp_wp'],
                            'alamat_op' => $get['alamat_op'],
                            'rt_op' => $get['nop_rt'],
                            'rw_op' => $get['nop_rw'],
                            'kd_propinsi' => $nop[0],
                            'kd_dati2' => $nop[1],
                            'kd_kecamatan' => $nop[2],
                            'kd_kelurahan' => $nop[3],
                            'kd_blok' => $blok_urut[0],
                            'no_urut' => $blok_urut[1],
                            'kd_jns_op' => $nop[5],
                            'nomor_layanan' => $nomor_layanan,
                            'luas_bumi' => $get['luas_bumi'],
                            'luas_bng' => $get['luas_bng'],
                            'njop_bumi' => $get['njop_bumi'],
                            'njop_bng' => $get['njop_bng'],
                            'keterangan_pembatalan' => $keteranganAlasan
                        ];
                        $savelayanan_objek = new Layanan_objek($saveLayananObjek);
                        $Layanan->layanan_objek()->save($savelayanan_objek);

                        $penelitian = '1';
                        if ($get['luas_bumi'] > 10000) {
                            $penelitian = '2';
                        }
                        $getId = Layanan_objek::select(['id'])->where(['nomor_layanan' => $nomor_layanan])->get()->first();
                        $where = [
                            'nomor_layanan' => $nomor_layanan,
                            'layanan_objek_id' => $getId->id
                        ];
                        $task = [
                            'nomor_layanan' => $nomor_layanan,
                            'layanan_objek_id' => $getId->id,
                            'jenis_layanan_nama' => $saveLayanan['jenis_layanan_nama'],
                            'penelitian' => $penelitian
                        ];
                        penelitianTask::firstOrCreate($where, $task);
                        $penelitian = Layanan_objek::with('layanan')->where('id', $getId->id)->wherenull('nomor_formulir')->first();
                        $penelitian = Pajak::loadPenelitian($penelitian);
                        $dataspop = DB::connection("oracle_dua")->select(DB::raw("SELECT 3 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,NULL NOP_ASAL,A.SUBJEK_PAJAK_ID,NM_WP,JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,KD_POS_WP,TELP_WP,NPWP,STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI FROM DAT_OBJEK_PAJAK A
                                    JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                                    JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                                    WHERE a.kd_kecamatan='" . $nop['2'] . "'
                                    AND a.kd_kelurahan='" . $nop['3'] . "'
                                    AND a.kd_blok='" . $blok_urut['0'] . "'
                                    AND a.no_urut='" . $blok_urut['1'] . "'
                                    AND a.kd_jns_op='" . $nop['5'] . "'"));
                        $dataspop = (array)$dataspop[0];
                        $kelurahan_wp = explode('-', $dataspop['kelurahan_wp']);
                        $dataspop['kecamatan_wp'] = '';
                        $dataspop['status_pekerjaan_wp'] = substr($dataspop['status_pekerjaan_wp'], 0, 14);
                        if (count($kelurahan_wp) > 1) {
                            $dataspop['kelurahan_wp'] = $kelurahan_wp['1'];
                            $dataspop['kecamatan_wp'] = $kelurahan_wp['0'];
                        }
                        $kota_wp = explode('-', $dataspop['kota_wp']);
                        $dataspop['propinsi_wp'] = '';
                        if (count($kota_wp) > 1) {
                            $dataspop['kota_wp'] = $kota_wp['1'];
                            $dataspop['propinsi_wp'] = $kota_wp['0'];
                        }
                        $dataRequest = [
                            'jenis_layanan' => '13_' . $key,
                            'npwp' => '',
                            'lhp_id' => $penelitian['id'],
                            'pendataan_objek_id' => '',
                            'created_by' => Auth()->user()->id,

                        ];
                        $dataspop = array_merge($dataspop, $dataRequest);
                        $request->merge($dataspop);
                        $spop = Pajak::pendataanSpop($request);
                        Pajak::pendataanObjekPajak($request, $spop);
                        $wherenop = [
                            ['kd_propinsi', '=', $nop['0']],
                            ['kd_dati2', '=', $nop['1']],
                            ['kd_kecamatan', '=', $nop['2']],
                            ['kd_kelurahan', '=', $nop['3']],
                            ['kd_blok', '=', $blok_urut['0']],
                            ['no_urut', '=', $blok_urut['1']],
                            ['kd_jns_op', '=', $nop['5']]
                        ];


                        // inventaris tunggakan
                        $inv = DB::connection("oracle_satutujuh")->table("SPPT")
                            ->leftjoin("sppt_koreksi", function ($join) {
                                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
                            })->whereraw("jns_koreksi is null and status_pembayaran_sppt='0'")
                            ->where([
                                ['sppt.kd_propinsi', '=', $nop['0']],
                                ['sppt.kd_dati2', '=', $nop['1']],
                                ['sppt.kd_kecamatan', '=', $nop['2']],
                                ['sppt.kd_kelurahan', '=', $nop['3']],
                                ['sppt.kd_blok', '=', $blok_urut['0']],
                                ['sppt.no_urut', '=', $blok_urut['1']],
                                ['sppt.kd_jns_op', '=', $nop['5']]
                            ])
                            ->select(db::raw("sppt.kd_propinsi,
                            sppt.kd_dati2,
                            sppt.kd_kecamatan,
                            sppt.kd_kelurahan,
                            sppt.kd_blok,
                            sppt.no_urut,
                            sppt.kd_jns_op,
                            sppt.thn_pajak_sppt"))
                            ->get();
                        $koreksi = [];
                        $ntr = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                            ->select(db::raw("'IV'||to_char(sysdate,'yyddmm')||lpad(nvl(max(replace(no_transaksi,'IV'||to_char(sysdate,'yyddmm'),'')),0)+1,3,0) no_transaksi"))
                            ->whereraw("no_transaksi like 'IV'||to_char(sysdate,'yyddmm')||'%'")->first()->no_transaksi;
                        $wblokir = "";
                        foreach ($inv as $rc) {
                            $wblokir .= "(kd_propinsi = '" . $rc->kd_propinsi . "' and
                            kd_dati2 = '" . $rc->kd_dati2 . "' and
                            kd_kecamatan = '" . $rc->kd_kecamatan . "' and
                            kd_kelurahan = '" . $rc->kd_kelurahan . "' and
                            kd_blok = '" . $rc->kd_blok . "' and
                            no_urut = '" . $rc->no_urut . "' and
                            kd_jns_op = '" . $rc->kd_jns_op . "' and
                            thn_pajak_sppt = '" . $rc->thn_pajak_sppt . "' ) or";
                            $koreksi[] = [
                                'kd_propinsi' => $rc->kd_propinsi,
                                'kd_dati2' => $rc->kd_dati2,
                                'kd_kecamatan' => $rc->kd_kecamatan,
                                'kd_kelurahan' => $rc->kd_kelurahan,
                                'kd_blok' => $rc->kd_blok,
                                'no_urut' => $rc->no_urut,
                                'kd_jns_op' => $rc->kd_jns_op,
                                'thn_pajak_sppt' => $rc->thn_pajak_sppt,
                                'keterangan' => 'Iventarisir objek',
                                'verifikasi_kode' => '1',
                                'no_surat' => $ntr,
                                'tgl_surat' => db::raw("sysdate"),
                                'verifikasi_by' => auth()->user()->id,
                                'verifikasi_keterangan' => $keteranganAlasan <> '' ? $keteranganAlasan : 'Iventarisasi objek',
                                'no_transaksi' => $ntr,
                                'created_at' => db::raw("sysdate"),
                                'created_by' => auth()->user()->id,
                                'jns_koreksi' => '1'
                            ];
                        }

                        if ($wblokir != '') {
                            $wblokir = substr($wblokir, 0, -2);
                            DB::connection('oracle_satutujuh')->table('sppt')
                                ->whereraw($wblokir)
                                ->update(['status_pembayaran_sppt' => 3]);
                        }

                        if (!empty($koreksi)) {
                            DB::connection('oracle_satutujuh')
                                ->table('sppt_koreksi')
                                ->insert($koreksi);
                        }


                        $updatePenelitian = Layanan_objek::where(['id' => $penelitian['id']]);
                        $updatePenelitian->update([
                            'nomor_formulir' => $spop['no_formulir'],
                            'pemutakhiran_at' => Carbon::now(),
                            'pemutakhiran_by' => auth()->user()->id,
                            'keterangan_pembatalan' => $keteranganAlasan <> '' ? $keteranganAlasan : 'Pembatalan objek',
                        ]);
                        DB::commit();
                    }
                } catch (\Exception $e) {
                    DB::rollBack();
                    $return = $e->getMessage();
                    $result = false;
                }
                if ($result) {
                    $return = [
                        "msg" => "<p>Penangguhan NOP berhasil.</p>
                                <p>SK dapat di cetak di menu  'Penelitian -> Daftar SK' dengan pecarian menggunakan Nomor Layanan atau NOP</p>
                                <p>Hasil Penelitian dapat di cetak di menu  'Penelitian -> Hasil peneltian SK' dengan pecarian menggunakan NOP</p>
                                <b>
                                    <p>Nomor Layanan : " . $nomor_layanan . "</p>
                                    <p>NOP : " . $request['data']['nop'] . "</p>
                                </b>
                                <div class='btn-group col-12'>
                                <a href='" . url("penelitian/sk?nomor_layanan=" . $nomor_layanan) . "'
                                            target='_BLANK' class='btn btn-default'><i class='fas fa-file-alt'></i> SK </a>
                                <a href='" . url("penelitian/hasil-penelitiant?nomor_layanan=" . $nomor_layanan) . "'
                                            target='_BLANK' class='btn btn-default'><i class='fas fa-file-alt'></i> Hasil Penelitian </a></div>
                                ",
                        "status" => true,
                        'curl' => $nomor_layanan
                    ];
                }
            }
        }
        return response()->json($return);
    }

    public function getTunggakan(Request $request)
    {
        $var = $request->nop ?? '';
        $var = onlyNumber($var);
        $data = [];
        if ($var != '') {
            $kd_propinsi = substr($var, 0, 2);
            $kd_dati2 = substr($var, 2, 2);
            $kd_kecamatan = substr($var, 4, 3);
            $kd_kelurahan = substr($var, 7, 3);
            $kd_blok = substr($var, 10, 3);
            $no_urut = substr($var, 13, 4);
            $kd_jns_op = substr($var, 17, 1);

            $data = DB::connection("oracle_satutujuh")->select(DB::raw("select to_char(tgl_terbit_sppt,'yyyy') tahun_piutang ,thn_pajak_sppt,pbb_yg_harus_dibayar_sppt -potongan pbb 
                from core_sppt
                where   kd_propinsi='$kd_propinsi' and kd_dati2='$kd_dati2' and 
                     kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'
                and kd_blok='$kd_blok' and no_urut='$no_urut'
                and kd_jns_op='$kd_jns_op'
                and status_pembayaran_sppt='0' 
                and tgl_terbit_sppt is not null
                order by to_char(tgl_terbit_sppt,'yyyy')  asc"));
        }
        return view('penangguhan/_form_iventarisasi_tunggkan', compact('data'));
    }

    public function indexDataIventarisasi(Request $request)
    {
        return view('penangguhan/index_iventarisasi');
    }

    public function indexDataIventarisasiExport(Request $request)
    {
        $jenis = $request->jenis ?? 'excel';

        // return $request->all();


        $kd_kategori = $request->kd_kategori;

        if ($kd_kategori == '00') {
            $jkp = 'KP';
            $kd_kategori = '';
        } else {
            $jkp = '2';
        }


        // return $jkp;

        $data = $this->listKoreksiPiutang($jkp);
        // ->whereraw("sppt_koreksi.no_transaksi like 'KP%'");
        $search = Strtolower($request->search);
        $tahun_sppt = $request->tahun ?? '';
        $tahun_proses = $request->tahun_proses ?? '';


        if ($kd_kategori != '') {
            $data = $data->whereraw("kd_kategori='$kd_kategori'");
        }

        if ($tahun_sppt != '') {
            $data = $data->whereraw("sppt_koreksi.thn_pajak_sppt ='$tahun_sppt' ");
        }

        if ($tahun_proses != '') {
            $data = $data->whereraw("to_char(sppt_koreksi.tgl_surat,'yyyy') ='$tahun_proses' ");
        }

        if ($search != '') {
            $angka = onlyNumber($search);
            $wh = "lower(no_surat) like '%$search%' or lower(no_transaksi) like '%$search%' or lower(nm_kategori) like '%$search%' or lower(keterangan) like '%$search%' ";
            if ($angka) {
                $wh .= "or sppt_koreksi.kd_propinsi||sppt_koreksi.kd_dati2||sppt_koreksi.kd_kecamatan||sppt_koreksi.kd_kelurahan||sppt_koreksi.kd_blok||sppt_koreksi.no_urut||sppt_koreksi.kd_jns_op like '%$angka%'";
            }
            $data = $data->whereraw('(' . $wh . ')');
        }
        $data = $data->orderByRaw("sppt_koreksi.created_at desc")->get();



        $dataexcell = [];
        foreach ($data as $row) {

            $dataexcell[] = [
                $row->no_transaksi,
                ($row->nm_kategori == '' ? 'Koreksi Piutang' : $row->nm_kategori),
                $row->kd_propinsi . '.' .
                    $row->kd_dati2 . '.' .
                    $row->kd_kecamatan . '.' .
                    $row->kd_kelurahan . '.' .
                    $row->kd_blok . '.' .
                    $row->no_urut . '.' .
                    $row->kd_jns_op,
                $row->thn_pajak_sppt,
                $row->no_surat,
                tglIndo($row->tgl_surat),
                $row->pbb

            ];
        }
        // return $dataexcell;
        $filename = "data iventarisasi or koreksi  " . $kd_kategori;
        if ($jenis == 'csv') {
            $filename = $filename . '.csv';
            $ext = \Maatwebsite\Excel\Excel::CSV;
        } else {
            $filename = $filename . '.xlsx';
            $ext = \Maatwebsite\Excel\Excel::XLSX;
        }
        return Excel::download(new penangguhanIventarisasiExcel($dataexcell), $filename, $ext);
    }
}
