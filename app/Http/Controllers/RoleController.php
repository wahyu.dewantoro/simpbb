<?php

namespace App\Http\Controllers;

// use App\Authorizable;

use App\Menu;
use App\Permission;
use App\Role;
use App\RoleMenu;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    // use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('permission:view_roles')->only(['index']);
        // $this->middleware('permission:add_roles')->only(['create', 'store']);
        // $this->middleware('permission:edit_roles')->only(['edit', 'update']);
        // $this->middleware('permission:delete_roles')->only(['destroy']);
    }

    public function index(Request $request)
    {


        $roles = Role::orderby("id", "asc");
        if ($request->cari) {
            $q = strtolower($request->cari);
            $roles = $roles->whereraw("lower(name) like '%$q%'");
        }
        $roles = $roles->paginate();
        return view('role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $permissions = Permission::all();
        $menus = Menu::with(['childs', 'childs.childs', 'childs.childs.childs'])->where('parent_id', 0)->get();
        $form = (object)[
            'method' => 'post',
            'action' => route('roles.store'),
            'title' => 'Create Role'
        ];
        return view('role.form', compact('form', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'role' => 'bail|required|min:2',
            'menu' => 'required|min:1'
        ]);

        DB::beginTransaction();
        try {
            //code...
            $role = Role::create(['name' => $request->role]);
            $menus = $request->get('menu', []);
            $datamenu = [];
            foreach ($menus as $menu) {
                $datamenu[] = [
                    'menu_id' => $menu,
                    'role_id' => $role->id
                ];
            }

            if (!empty($datamenu)) {
                RoleMenu::insert($datamenu);
            }
            // $msg = $role;
            DB::commit();

            $gm = DB::table("permissions")->wherein('menu_id', $request->menu)->pluck('name')->toarray();

            $role->syncPermissions($gm);

            Cache::flush();
            $flash = [
                'success' => 'Role ' . $role->name . ' telah di tambahkan'
            ];
        } catch (\Exception $th) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $th->getMessage()
            ];
        }


        return redirect(route('roles.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $menus = Menu::with(['childs', 'childs.childs', 'childs.childs.childs'])->where('parent_id', 0)->get();
        $active = $role->menu()->pluck('id')->toarray();
        $form = (object)[
            'method' => 'patch',
            'action' => route('roles.update', $role),
            'title' => 'Create Role',
            'role' => $role,
            'active' => $active
        ];
        return view('role.form', compact('form', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $gm = DB::table("permissions")->wherein('menu_id', $request->menu)->pluck('name')->toarray();
            $role = Role::findOrFail($id);
            $role->update(['name' => $request->role]);
            $menus = $request->get('menu', []);
            // hapus role menu id
            RoleMenu::where('role_id', $id)->delete();

            $datamenu = [];
            foreach ($menus as $menu) {
                $datamenu[] = [
                    'menu_id' => $menu,
                    'role_id' => $role->id
                ];
            }

            if (!empty($datamenu)) {
                RoleMenu::insert($datamenu);
            }

            $role->syncPermissions($gm);
            $flash = [
                'success' => ' Role berhasil di update'
            ];
            DB::commit();
            Cache::flush();
        } catch (\Throwable $th) {
            DB::rollBack();
            $flash = [
                'errorr' => $th->getMessage()
            ];
        }

        return redirect()->route('roles.index')->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
        try {
            //code...
            $role->revokePermissionTo(Permission::all());
            $role->delete();
            $flash = ['success' => 'Role ' . $role->name . ' telah di hapus'];
        } catch (\Exception $th) {
            //throw $th;
            $flash = ['error' => $th->getMessage()];
        }

        return redirect(route('roles.index'))->with($flash);
    }
}
