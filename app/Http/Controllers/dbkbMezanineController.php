<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class dbkbMezanineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = DB::connection("oracle_satutujuh")->table("DBKB_MEZANIN")->orderby("THN_DBKB_MEZANIN", "desc")->get();
        return view('dbkb/mezanin/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = [
            'action' => url('dbkb/mezanine'),
            'method' => 'post'
        ];
        return view('dbkb/mezanin/form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "thn_dbkb_mezanin" => 'required',
            "nilai_dbkb_mezanin" => 'required',
        ]);
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...
            $data = [
                "kd_propinsi" => '35',
                'kd_dati2' => '07',
                "thn_dbkb_mezanin" => $request->thn_dbkb_mezanin,
                "nilai_dbkb_mezanin" => (onlyNumber($request->nilai_dbkb_mezanin) / 1000),
            ];
            // return $data;
            DB::connection("oracle_satutujuh")->table('DBKB_MEZANIN')->insert($data);
            DB::connection("oracle_satutujuh")->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection("oracle_satutujuh")->rollBack();
            Log::alert($th);
        }

        return Redirect(url('dbkb/mezanine'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = [
            'action' => url('dbkb/mezanine', $id),
            'method' => 'patch',
            'dbkb' => DB::connection("oracle_satutujuh")->table('DBKB_MEZANIN')->where('thn_dbkb_mezanin', $id)->first()
        ];

        return view('dbkb/mezanin/form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "thn_dbkb_mezanin" => 'required',
            "nilai_dbkb_mezanin" => 'required',
        ]);
        DB::connection("oracle_satutujuh")->beginTransaction();
        try {
            //code...
            $data = [
                "kd_propinsi" => '35',
                'kd_dati2' => '07',
                "thn_dbkb_mezanin" => $request->thn_dbkb_mezanin,
                "nilai_dbkb_mezanin" => (onlyNumber($request->nilai_dbkb_mezanin) / 1000),
            ];
            // return $data;
            DB::connection("oracle_satutujuh")->table('DBKB_MEZANIN')->where('thn_dbkb_mezanin', $id)->update($data);
            DB::connection("oracle_satutujuh")->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection("oracle_satutujuh")->rollBack();
            Log::alert($th);
        }

        return Redirect(url('dbkb/mezanine'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
