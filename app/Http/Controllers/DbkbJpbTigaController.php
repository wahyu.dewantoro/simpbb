<?php

namespace App\Http\Controllers;

use App\Models\DbkbJpb3;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbJpbTigaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function coreSql($tahun)
    {
        return "select lbr_bent_min_dbkb_jpb3,lbr_bent_max_dbkb_jpb3,
        sum(kolom_0_4) kolom_0_4,sum(kolom_5_7) kolom_5_7,
        sum(kolom_8_10) kolom_8_10,sum(kolom_11_99) kolom_11_99
        from (
        select lbr_bent_min_dbkb_jpb3,lbr_bent_max_dbkb_jpb3,
        case when ting_kolom_min_dbkb_jpb3=0 and ting_kolom_max_dbkb_jpb3=4 then nilai_dbkb_jpb3*1000 else null end  kolom_0_4 ,
        case when ting_kolom_min_dbkb_jpb3=5 and ting_kolom_max_dbkb_jpb3=7 then nilai_dbkb_jpb3*1000 else null end  kolom_5_7,
        case when ting_kolom_min_dbkb_jpb3=8 and ting_kolom_max_dbkb_jpb3=10 then nilai_dbkb_jpb3*1000 else null end kolom_8_10,
        case when ting_kolom_min_dbkb_jpb3=11 and ting_kolom_max_dbkb_jpb3=99 then nilai_dbkb_jpb3*1000 else null end kolom_11_99
        from dbkb_jpb3
        where thn_dbkb_jpb3='$tahun' 
        group by lbr_bent_min_dbkb_jpb3,lbr_bent_max_dbkb_jpb3,ting_kolom_min_dbkb_jpb3,ting_kolom_max_dbkb_jpb3,nilai_dbkb_jpb3
        ) a
        group by lbr_bent_min_dbkb_jpb3,lbr_bent_max_dbkb_jpb3
        order by lbr_bent_min_dbkb_jpb3,lbr_bent_max_dbkb_jpb3";
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            // $data = DbkbJpb3::where('THN_DBKB_JPB3', $tahun)->orderbyraw('LBR_BENT_MIN_DBKB_JPB3, LBR_BENT_MAX_DBKB_JPB3, TING_KOLOM_MIN_DBKB_JPB3, TING_KOLOM_MAX_DBKB_JPB3')->get();
            $data = DB::connection("oracle_satutujuh")->select(DB::raw($this->coreSql($tahun)));
            $read = 1;
            return view('dbkb.jepebetiga._index', compact('data', 'read'));
        }
        return view('dbkb.jepebetiga.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            // $data = DbkbJpb3::where('THN_DBKB_JPB3', $tahun)->orderbyraw('LBR_BENT_MIN_DBKB_JPB3, LBR_BENT_MAX_DBKB_JPB3, TING_KOLOM_MIN_DBKB_JPB3, TING_KOLOM_MAX_DBKB_JPB3')->get();
            $data = DB::connection("oracle_satutujuh")->select(DB::raw($this->coreSql($tahun)));
            if (count($data) == 0) {
                $data = DB::connection("oracle_satutujuh")->select(DB::raw($this->coreSql($tahun - 1)));
            }
            $read = 0;
            return view('dbkb.jepebetiga._index', compact('data', 'read'));
        }
        return view('dbkb/jepebetiga/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $tahun = $request->thn_dbkb_jpb3;
        $req = $request->all();

        for ($idx = 0; $idx <= 8; $idx++) {
            $var = $req['nilai_' . $idx];
            switch ($idx) {
                case '1':
                    $lb_min = '10';
                    $lb_max = '13';
                    break;
                case '2':
                    $lb_min = '14';
                    $lb_max = '17';
                    break;
                case '3':
                    $lb_min = '18';
                    $lb_max = '21';
                    break;
                case '4':
                    $lb_min = '22';
                    $lb_max = '25';
                    break;
                case '5':
                    $lb_min = '26';
                    $lb_max = '29';
                    break;
                case '6':
                    $lb_min = '30';
                    $lb_max = '33';
                    break;
                case '7':
                    $lb_min = '34';
                    $lb_max = '37';
                    break;
                case '8':
                    $lb_min = '38';
                    $lb_max = '99';
                    break;
                default:
                    # code...
                    $lb_min = '0';
                    $lb_max = '9';
                    break;
            }

            foreach ($var as  $i => $item) {

                switch ($i) {
                    case '1':
                        $kolmin = 5;
                        $kolmax = 7;
                        break;
                    case '2':
                        $kolmin = 8;
                        $kolmax = 10;
                        break;
                    case '3':
                        $kolmin = 11;
                        $kolmax = 99;
                        break;
                    default:
                        $kolmin = 0;
                        $kolmax = 4;
                        break;
                }


                $data[] = [
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'thn_dbkb_jpb3' => $tahun,
                    'lbr_bent_min_dbkb_jpb3' => $lb_min,
                    'lbr_bent_max_dbkb_jpb3' => $lb_max,
                    'ting_kolom_min_dbkb_jpb3' => $kolmin,
                    'ting_kolom_max_dbkb_jpb3' => $kolmax,
                    'nilai_dbkb_jpb3' =>  is_null($item) == 1 ? 0 : $item / 1000
                ];
            }
        }


        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            DbkbJpb3::where(['thn_dbkb_jpb3' => $request->thn_dbkb_jpb3])->delete();
            foreach ($data as $ins) {
                DbkbJpb3::create($ins);
            }

            $flash = ['success' => 'Berhasil memproses DBKB JPB3 pada tahun ' . $request->thn_dbkb_jpb3];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.jepebe-tiga.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
