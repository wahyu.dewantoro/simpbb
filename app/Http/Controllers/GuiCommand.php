<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\GenerateDafnomlist;
use App\Jobs\GenerateDafnom;
use Illuminate\Support\Facades\Storage;
use App\Models\Data_billing;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use App\Helpers\Dafnom;
use App\Helpers\LayananUpdate;
use App\Jobs\disposisiPenelitian;
use App\Jobs\PendataanPenilaianJob;
use App\Models\Layanan;
use App\Models\Layanan_objek;

class GuiCommand extends Controller
{
    
    // 
    public function layananAllEmptyfilePembetulan(){
        if(isset($_GET['nomor_layanan'])){
            $nomor_layanan=$_GET['nomor_layanan'];
            echo "Job layanan Pembetulan Eja=>".$nomor_layanan." </br>";
            dispatch(new PendataanPenilaianJob(['nomor_layanan' => $nomor_layanan]))->onQueue('layanan');
        }
    }
    public function layananReexecute(){
        if(isset($_GET['nomor_layanan'])){
            $nomor_layanan=$_GET['nomor_layanan'];
            echo "Layanan Reexecute=>".$nomor_layanan." </br>";
            $cek=Layanan_objek::where(['nomor_layanan'=>$nomor_layanan])->get();
            if($cek->count()){
                $getData=current($cek->toArray());
                $nopdefault = [
                    $getData['kd_propinsi'], 
                    $getData['kd_dati2'], 
                    $getData['kd_kecamatan'], 
                    $getData['kd_kelurahan'],
                    $getData['kd_blok'],
                    $getData['no_urut'],
                    $getData['kd_jns_op']
                ];
                $setDataUpdae=[
                    'nama'=>$getData['nama_wp'],
                    'alamat'=>$getData['alamat_wp'],
                    'kelurahan'=>$getData['kelurahan_wp'],
                    'dati2'=>$getData['dati2_wp'],
                ];
                LayananUpdate::updateDataDiri_dat_subjek_pajak($nopdefault, ['alamat_op' => $getData['alamat_op']],$getData['nik_wp']);
                LayananUpdate::updateDataDiri_sppt($setDataUpdae,$nopdefault);
                echo "Job layanan =>".$nomor_layanan." </br>";
                dispatch(new PendataanPenilaianJob(['nomor_layanan' => $nomor_layanan]))->onQueue('layanan');
            }
        }
    }
    public function layananAllEmptyfile(){
        if(isset($_GET['nomor_layanan'])){
            $nomor_layanan=$_GET['nomor_layanan'];
            echo "Job layanan =>".$nomor_layanan." </br>";
            dispatch(new disposisiPenelitian($nomor_layanan))->onQueue('layanan');
        }
        if(isset($_GET['tgl'])){
            $after=Carbon::now()->format('d/m/Y');
            $layanan=Layanan::whereraw("to_char(layanan.created_at,'dd/mm/yyyy')='".$after."' and layanan.jenis_objek='1'")->get();
            foreach($layanan->toArray() as $item){
                echo "Job layanan =>".$item["nomor_layanan"]." </br>";
                dispatch(new disposisiPenelitian($item["nomor_layanan"]))->onQueue('layanan');
            }
            //
        }

    }
    public function generateAllEmptyfile(){
        if(isset($_GET['id'])){
            $item["data_billing_id"]=$_GET['id'];
            $dafnom=Storage::disk('dafnom_list_pdf')->exists('dafnom_list/'.$item["data_billing_id"].'.pdf');
            if(!$dafnom){
                echo "cetak_list =>".$item["kobil"]." - ".$item["nama_wp"]." </br>";
                dispatch(new GenerateDafnomlist(['id_billing'=>$item["data_billing_id"]]))->onQueue('dafnom');
            }
            $pembayaran=Storage::disk('dafnom_list_pdf')->exists('pembayaran/'.$item["data_billing_id"].'.pdf');
            if(!$pembayaran){
                echo "cetak_pembyaran =>".$item["data_billing_id"]." </br>";
                dispatch(new GenerateDafnom(['id_billing'=>$item["data_billing_id"]]))->onQueue('dafnom');
            }
        }else{
            if(isset($_GET['before'])){
                $before=$_GET['before'];
                $getDataBilling=Data_billing::where('created_at','>=',Carbon::now()->subDays($before))->get();
            }else{
                $getDataBilling=Data_billing::get();
            }
            if(isset($_GET['check'])){
                $countDafnom=$countdafnombayar=$lunas=0;
                foreach($getDataBilling->toArray() as $item){
                    $dafnom=Storage::disk('dafnom_list_pdf')->exists('dafnom_list/'.$item["data_billing_id"].'.pdf');
                    if(!$dafnom){
                        $countDafnom++;
                    }
                    
                    $pembayaran=Storage::disk('dafnom_list_pdf')->exists('pembayaran/'.$item["data_billing_id"].'.pdf');
                    if(!$pembayaran){
                        $countdafnombayar++;
                        if($item['kd_status']=='1'){
                            $lunas++;
                        }
                    }
                }
                echo "total_list =>".$countDafnom." dafnom </br>";
                echo "total_pembyaran =>".$countdafnombayar." (".$lunas. " Lunas) </br>";
            }else{
                foreach($getDataBilling->toArray() as $item){
                    $dafnom=Storage::disk('dafnom_list_pdf')->exists('dafnom_list/'.$item["data_billing_id"].'.pdf');
                    if(!$dafnom){
                        echo "cetak_list =>".$item["kobil"]." - ".$item["nama_wp"]." </br>";
                        dispatch(new GenerateDafnomlist(['id_billing'=>$item["data_billing_id"]]))->onQueue('dafnom');
                    }
                    
                    $pembayaran=Storage::disk('dafnom_list_pdf')->exists('pembayaran/'.$item["data_billing_id"].'.pdf');
                    if(!$pembayaran&&$item['kd_status']=='1'){
                        echo "cetak_pembyaran =>".$item["kobil"]." - ".$item["nama_wp"]." </br>";
                        dispatch(new GenerateDafnom(['id_billing'=>$item["data_billing_id"]]))->onQueue('dafnom');
                    }
                }
            }
        }
    }
    public function runjob(){
        //Artisan::call('config:cache');
        //Artisan::call('queue:restart');
        //Artisan::call('queue:flush');
        Artisan::call('queue:listen --queue='.Dafnom::defaultQueuename().' --timeout=0 --sleep=1 ');
        //return dd(Artisan::output());
        return 'OK';
    }
    public function terminal($command, $option){
        $artisan = Artisan::call($command.':'.$option);
        return Artisan::output();
    }
}
