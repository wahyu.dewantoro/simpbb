<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\KecamatanLunas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SnappyPdf as PDF;

class kecamatanLunasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tahun = $request->thn_pajak ?? date('Y');
        $data = KecamatanLunas::with('kecamatan')
            ->where('tahun_pajak', $tahun)
            ->orderBy('tanggal_lunas', 'asc')->paginate();
        return view('kecamatan_lunas/index', compact('data','tahun'));
    }

    public function pdf(Request $request)
    {
        $tahun = $request->thn_pajak ?? date('Y');
        $data = KecamatanLunas::with('kecamatan')
            ->where('tahun_pajak', $tahun)
            ->orderBy('tanggal_lunas')
            ->get();

        // $pdf = PDF::loadView('kecamatan_lunas/cetak_pdf', compact('data','tahun'))->setPaper('a4');

        $pdf = PDF::loadView('kecamatan_lunas/cetak_pdf', ['data' => $data, 'tahun' => $tahun])->setPaper('A4')
            ->setOption('margin-left', '1')
            ->setOption('margin-right', '1')
            ->setOption('margin-top', '5')
            ->setOption('margin-bottom', '5')
            ->setOption('enable-local-file-access', true);

        $filename = time() . auth()->user()->id . '.pdf';
        return $pdf->stream($filename);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Tambah Kecamatan Lunas',
            'method' => 'post',
            'action' => route('realisasi.kecamatanlunas.store')
        ];
        $kecamatan = Kecamatan::get();
        return view('kecamatan_lunas.form', compact('data', 'kecamatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = [
            'kd_kecamatan' => $request->kd_kecamatan,
            'tahun_pajak' => $request->tahun_pajak,
            'tanggal_lunas' => new carbon($request->tanggal_lunas)
        ];
        // dd($data);
        DB::beginTransaction();
        try {
            //code...
            KecamatanLunas::create($data);
            $flash = [
                'success' => 'Berhasil menambahkan kecamatan lunas.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        // dd($flash);
        return redirect(route('realisasi.kecamatanlunas.index'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $exp=explode('_',$id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $exp = explode('_', $id);
        $lunas = KecamatanLunas::where('kd_kecamatan', $exp[0])->where('tahun_pajak', $exp[1])->firstorfail();

        // dd($lunas);
        $data = [
            'title' => 'Edit Kecamatan Lunas',
            'method' => 'patch',
            'action' => route('realisasi.kecamatanlunas.update', $id),
            'lunas' => $lunas
        ];
        $kecamatan = Kecamatan::get();
        return view('kecamatan_lunas.form', compact('data', 'kecamatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        DB::beginTransaction();
        try {
            //code...
            $exp = explode('_', $id);
            $lunas = KecamatanLunas::where('kd_kecamatan', $exp[0])->where('tahun_pajak', $exp[1]);
            $data = [
                'kd_kecamatan' => $request->kd_kecamatan,
                'tahun_pajak' => $request->tahun_pajak,
                'tanggal_lunas' => new carbon($request->tanggal_lunas)
            ];

            $lunas->update($data);
            $flash = [
                'success' => 'berhasil di update.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        // dd($flash);
        return redirect(route('realisasi.kecamatanlunas.index'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            //code...
            $exp = explode('_', $id);
            $lunas = KecamatanLunas::where('kd_kecamatan', $exp[0])->where('tahun_pajak', $exp[1]);
            $lunas->delete();
            $flash = [
                'success' => 'berhasil di hapus data.'
            ];
            DB::commit();
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            $flash = [
                'error' => $e->getMessage()
            ];
        }
        return redirect(route('realisasi.kecamatanlunas.index'))->with($flash);
    }
}
