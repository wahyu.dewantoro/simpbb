<?php

namespace App\Http\Controllers;

use App\Models\DbkbJpb7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbkbJpbTujuhController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function Coresql($tahun)
    {
        return "SELECT jns_dbkb_jpb7,case when jns_dbkb_jpb7='1' then 'Hotel Non Resort' else 'Hotel Resort' end Jenis,
        lantai_min_jpb7,lantai_max_jpb7,sum(bintang_1) bintang_1 
        ,sum(bintang_2) bintang_2
        ,sum(bintang_3) bintang_3
        ,sum(bintang_4) bintang_4
        ,sum(bintang_5) bintang_5
        from (
        select jns_dbkb_jpb7,lantai_min_jpb7,lantai_max_jpb7,
         case when bintang_dbkb_jpb7='1' then nilai_dbkb_jpb7 else null end bintang_1,
           case when bintang_dbkb_jpb7='2' then nilai_dbkb_jpb7 else null end bintang_2, 
        case when bintang_dbkb_jpb7='3' then nilai_dbkb_jpb7 else null end bintang_3,
        case when bintang_dbkb_jpb7='4' then nilai_dbkb_jpb7 else null end bintang_4,
        case when bintang_dbkb_jpb7='5' then nilai_dbkb_jpb7 else null end bintang_5
        from dbkb_jpb7
        where thn_dbkb_jpb7='$tahun'
        group by jns_dbkb_jpb7,lantai_min_jpb7,lantai_max_jpb7,bintang_dbkb_jpb7,nilai_dbkb_jpb7
        ) a 
        group by jns_dbkb_jpb7,lantai_min_jpb7,lantai_max_jpb7
        order by jns_dbkb_jpb7,lantai_min_jpb7,lantai_max_jpb7";
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DB::connection("oracle_satutujuh")->select(db::raw($this->Coresql($tahun)));
            $read = 1;
            return view('dbkb.jepebetujuh._index', compact('data', 'read'));
        }
        return view('dbkb.jepebetujuh.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $tahun = $request->tahun;
            $data = DB::connection("oracle_satutujuh")->select(db::raw($this->Coresql($tahun)));
            if (count($data) == 0) {
                $data = DB::connection("oracle_satutujuh")->select(db::raw($this->Coresql($tahun - 1)));
            }
            $read = 0;
            return view('dbkb.jepebetujuh._index', compact('data', 'read'));
        }
        return view('dbkb/jepebetujuh/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $tahun = $request->thn_dbkb_jpb7;
        $req = $request->all();

        for ($idx = 0; $idx <= 8; $idx++) {
            switch ($idx) {
                case '1':
                    $min = 3;
                    $max = 5;
                    $jenis = 1;
                    break;
                case '2':
                    $min = 6;
                    $max = 12;
                    $jenis = 1;
                    break;

                case '3':
                    $min = 13;
                    $max = 20;
                    $jenis = 1;
                    break;
                case '4':
                    $min = 21;
                    $max = 24;
                    $jenis = 1;
                    break;
                case '5':
                    $min = 25;
                    $max = 99;
                    $jenis = 1;
                    break;
                case '6':
                    $min = 1;
                    $max = 2;
                    $jenis = 2;
                    break;
                case '7':
                    $min = 3;
                    $max = 5;
                    $jenis = 2;
                    break;
                case '8':
                    $min = 6;
                    $max = 12;
                    $jenis = 2;
                    break;
                case '1':
                    $min = 3;
                    $max = 5;
                    $jenis = 1;
                    break;
                default:
                    # code...
                    $min = 1;
                    $max = 2;
                    $jenis = 1;
                    break;
            }


            foreach ($req['nilai_' . $idx] as $i => $itm) {

                // $data[]=$key++;

                // $data[]=$;

                $data[] = [
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'jns_dbkb_jpb7' => $jenis,
                    'thn_dbkb_jpb7' => $tahun,
                    'lantai_min_jpb7' => $min,
                    'lantai_max_jpb7' => $max,
                    'bintang_dbkb_jpb7' => $i + 1,
                    'nilai_dbkb_jpb7' => is_null($itm) == 1 ? 0 : $itm
                ];
            }
        }


        // dd($data);
        // return $data;
        // die();

        DB::connection('oracle_satutujuh')->beginTransaction();
        try {
            DbkbJpb7::where(['thn_dbkb_jpb7' => $request->thn_dbkb_jpb7])->delete();
            foreach ($data as $ins) {
                DbkbJpb7::create($ins);
            }

            $flash = ['success' => 'Berhasil memproses DBKB JPB7 pada tahun ' . $request->thn_dbkb_jpb7];
            DB::connection('oracle_satutujuh')->commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::connection('oracle_satutujuh')->rollBack();
            $flash = ['error' => $th->getMessage()];
            Log::error($th);
        }
        return redirect(route('dbkb.jepebe-tujuh.index'))->with($flash);

        // dd(['ori' => $request->all(), 'olah' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
