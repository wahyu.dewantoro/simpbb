<?php

namespace App\Http\Controllers;

use App\TempatBayar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;

class TempatBayarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TempatBayar::paginate(10);
        return view('tempatbayar.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'url' => url('refrensi/tempat-bayar'),
            'method' => 'post'
        ];
        return view('tempatbayar.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->only('nama_tempat');
        DB::beginTransaction();
        try {
            TempatBayar::create($data);
            DB::commit();

            $flash = [
                'success' => 'Berhasil menambahkan'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('refrensi/tempat-bayar'))->with($flash);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tempat=TempatBayar::findorfail($id);
        $data = [
            'url' => url('refrensi/tempat-bayar',$id),
            'method' => 'patch',
            'tempat'=>$tempat
        ];
        return view('tempatbayar.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data = $request->only('nama_tempat');
        $tempat=TempatBayar::findorfail($id);

        DB::beginTransaction();
        try {
            $tempat->update($data);
            DB::commit();
            $flash = [
                'success' => 'Berhasil update data'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('refrensi/tempat-bayar'))->with($flash);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tempat=TempatBayar::findorfail($id);

        DB::beginTransaction();
        try {
            $tempat->delete();
            DB::commit();
            $flash = [
                'success' => 'Berhasil hapus data'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th);
            $flash = [
                'error' => $th->getMessage()
            ];
        }
        return redirect(url('refrensi/tempat-bayar'))->with($flash);
    }
}
