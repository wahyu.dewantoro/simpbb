<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class espptHistory extends Model
{
    //
    protected $guarded = [];
    protected $table = 'esppt_history';
}
