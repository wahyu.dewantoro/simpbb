<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class HisJatuhTempo extends Model
{
    //
    protected $guarded = [];
    protected $table = 'history_jatuh_tempo';
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->created_at = Carbon::now();
            $item->created_by = Auth::user()->id;
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });


        static::updating(function ($item) {
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });
    }
}
