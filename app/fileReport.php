<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class fileReport extends Model
{
    //

    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'file_report';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->id = (string) Str::uuid();
            $item->created_at = Carbon::now();
            $item->updated_at = Carbon::now();
        });
    }

    public function scopeDhkp($sql)
    {
        $sql->whereraw("keterangan like 'DHKP%'")
            ->whereraw("trunc(created_at)=trunc(sysdate)");
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        if (in_array('Kecamatan', $role)) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
            $sql->whereIn('file_report.kd_kecamatan', $kec);
        }
    }
}
