<?php

namespace App\Imports;

// use Illuminate\Support\Collection;

use App\RekonNop;
// use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class NopRekonImport implements WithHeadingRow, FromCollection
{


    public function collection()
    {
        return RekonNop::all();
    }

    /* public function headingRow(): int
    {
        return 2;
    } */
}
