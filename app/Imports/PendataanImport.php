<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PendataanImport implements WithHeadingRow, FromCollection
{
    /**
     * @param Collection $collection
     */
    public function collection()
    {
        //
    }
}
