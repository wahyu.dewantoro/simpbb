<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class layanan_kolektif implements WithHeadingRow,SkipsEmptyRows,WithValidation,SkipsOnFailure,WithCalculatedFormulas
{
    /**
    * @param Collection $collection
    */
    use SkipsFailures;
    public function rules(): array
    {
        return [
            'no' => "required",
            'layanan' => "required",
            'nik' => "required",
        ];
    }
    public function headingRow(): int
    {
        return 2;
    }
}
