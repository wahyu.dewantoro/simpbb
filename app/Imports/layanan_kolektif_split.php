<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class layanan_kolektif_split implements WithMultipleSheets ,SkipsUnknownSheets
{
    /**
    * @param Collection $collection
    */
    public $unknownSheet;

    public function __construct(){
        $this->unknownSheet = false;
    }
    public function sheets(): array
    {
        return [
            'FORM PEMBETULAN' => new layanan_pembetulan(),
            'FORM MUTASI PENUH' => new layanan_mutasi_penuh(),
            'FORM MUTASI GABUNG' => new layanan_mutasi_gabung(),
            'FORM MUTASI PECAH' => new layanan_mutasi_pecah(),
            'FORM PEMBATALAN' => new layanan_pembatalan(),
            'FORM DATA BARU' => new layanan_data_baru(),
        ];
    }
    public function onUnknownSheet($sheetName)
    {
        $this->unknownSheet[]=$sheetName;
    }
    public function getUnknownSheet(){
        return $this->unknownSheet;
    }
}
