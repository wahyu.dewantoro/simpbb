<?php

namespace App\Imports;

use App\MsPotonganIndividu;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpsertColumns;
use Maatwebsite\Excel\Concerns\WithUpserts;

class MsPotonganIndividuImport implements ToModel, WithUpserts, WithUpsertColumns, WithHeadingRow
{
    /**
     * @param Collection $collection
     */

    public function model(array $row)
    {
        return new MsPotonganIndividu([
            'kd_propinsi' => $row['KD_PROPINSI'],
            'kd_dati2' => $row['KD_DATI2'],
            'kd_kecamatan' => $row['KD_KECAMATAN'],
            'kd_kelurahan' => $row['KD_KELURAHAN'],
            'kd_blok' => $row['KD_BLOK'],
            'no_urut' => $row['NO_URUT'],
            'kd_jns_op' => $row['KD_JNS_OP'],
            'thn_pajak_sppt' => $row['THN_PAJAK_SPPT'],
            'nilai_potongan' => $row['NILAI_POTONGAN'],

        ]);
    }

    public function upsertColumns()
    {
        return [
            'kd_propinsi',
            'kd_dati2',
            'kd_kecamatan',
            'kd_kelurahan',
            'kd_blok',
            'no_urut',
            'kd_jns_op',
            'thn_pajak_sppt'
        ];
    }

    public function uniqueBy()
    {
        return [
            'kd_propinsi',
            'kd_dati2',
            'kd_kecamatan',
            'kd_kelurahan',
            'kd_blok',
            'no_urut',
            'kd_jns_op',
            'thn_pajak_sppt'
        ];
    }
}
