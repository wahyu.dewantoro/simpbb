<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class layanan_pembetulan implements WithHeadingRow,SkipsEmptyRows,WithValidation,SkipsOnFailure
{
    /**
    * @param Collection $collection
    */
    use SkipsFailures;
    public function rules(): array
    {
        return [
            // 'nop' => "required",
        ];
    }
    public function headingRow(): int
    {
        return 7;
    }
}