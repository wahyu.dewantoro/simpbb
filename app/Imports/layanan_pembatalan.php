<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;

class layanan_pembatalan implements WithHeadingRow,SkipsEmptyRows,WithValidation,SkipsOnFailure
{
    /**
    * @param Collection $collection
    */
    use SkipsFailures;
    public function rules(): array
    {
        return [
            'nop' => ['required','string'],
        ];
    }
    public function headingRow(): int
    {
        return 7;
    }
    public function onFailure(Failure ...$failures)
    {
        
    }
    
}