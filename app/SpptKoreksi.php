<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpptKoreksi extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    protected $table = 'sppt_koreksi';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = ['thn_pajak_sppt', 'kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op', 'thn_pajak_sppt'];
}


/* 
jns_koreksi
1 -> untuk mutasi gabung dan pecah
2 -> iventarisasi
3 -> KP
4 -> objek pembatalan
*/