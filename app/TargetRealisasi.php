<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TargetRealisasi extends Model
{
    //

    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'target_realisasi';
    public $timestamps = false;

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::creating(function ($item) {
    //         $item->created_at = Carbon::now();
    //         $item->created_by = AUth()->user()->id;
    //         $item->updated_by = AUth()->user()->id;
    //         $item->updated_at = Carbon::now();
    //     });


    //     static::updating(function ($item) {
    //         $item->updated_by = AUth()->user()->id;
    //         $item->updated_at = Carbon::now();
    //     });
    // }

}
