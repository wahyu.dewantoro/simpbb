<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LokasiObjek extends Model
{
    //
    protected $guarded = [];
    protected $table = 'lokasi_objek';
    public $timestamps = false;

    public function layanan_objek()
    {
        return $this->hasMany('App\Models\Layanan_objek', 'id','lokasi_objek_id');
    }
}
