<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekonHistory extends Model
{
    //
    protected $guarded = [];
    protected $table = 'rekon_history';
}
