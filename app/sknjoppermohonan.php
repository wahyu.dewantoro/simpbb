<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class sknjoppermohonan extends Model
{
    //
    protected $guarded = [];
    protected $table = 'sknjop_permohonan';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->id = Uuid::uuid1()->toString();
            $item->tanggal_permohonan = Carbon::now();
            $item->created_at = Carbon::now();
            $item->created_by = Auth::user()->id;
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
            $item->KD_STATUS = '1';
        });


        static::updating(function ($item) {
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });
    }

    public function scopePencarian($sql)
    {
        $q = strtolower(request('cari'));
        $sql = $sql->whereraw("( nomor_layanan like '%$q%' or (lower(NAMA_PEMOHON) like '%$q%' or lower(ALAMAT_PEMOHON) like '%$q%' or lower(KELURAHAN_PEMOHON) like '%$q%' or lower(KECAMATAN_PEMOHON) like '%$q%' or lower(DATI2_PEMOHON) like '%$q%' or lower(PROPINSI_PEMOHON) like '%$q%'))");
        return $sql;
    }

    public function scopeWepe($sql){
        $res=Auth()->user()->hasRole('Wajib Pajak');
        if($res==true){
            return $sql->where('created_by',Auth()->user()->id);
        }
    }

    public function nop()
    {
        return $this->hasMany('App\SkNJop', 'sknjop_permohonan_id', 'id');
    }

    public function penerima()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
