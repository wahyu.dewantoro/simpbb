<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;


class dataNirExport implements FromArray, WithHeadings, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data;
    function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return   $this->data;
    }

    public function headings(): array
    {
        return [
            "NO",
            "Kecamatan",
            "Kelurahan",
            "ZNT",
            "Lokasi",
            "Objek",
            "Tahun",
            "NIR",
            "HKPD",

        ];
    }
}
