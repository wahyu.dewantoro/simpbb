<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ListPendataan implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $ListPendataan;
    protected $search;

    public function __construct(array $ListPendataan,array $search)
    {
        $this->ListPendataan = $ListPendataan;
        $this->search = $search;
    }
    
    public function title(): string
    {
        return 'Laporan Daftar Pendataan ';
    }
    public function view(): View
    {
        return view('analisa/cetak/listpendataan', [
            'result' => $this->ListPendataan,
            'search'=>$this->search
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
