<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
use App\Helpers\Piutang;
use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithProperties;

class detailPiutangExport implements FromView, ShouldAutoSize, WithProperties
{
   /**
    * @return \Illuminate\Support\Collection
    */

   protected $parameter;

   function __construct($parameter)
   {
      $this->parameter = $parameter;
   }

   public function view(): View
   {

      $kd_kecamatan = $this->parameter['kecamatan'] ?? null;
      $kd_kelurahan = $this->parameter['kelurahan'] ?? null;
      $turut = $this->turut ?? 5;
      $buku = $this->parameter['buku'];

      $piutang = Piutang::detailKecamatan($kd_kecamatan, $turut, $buku)
         ->selectraw("
        CASE WHEN a IS NOT NULL  then get_buku(a) else null end buku_a,
        CASE WHEN b IS NOT NULL  then get_buku(b) else null end buku_b,
        CASE WHEN c IS NOT NULL  then get_buku(c) else null end buku_c,
        CASE WHEN d IS NOT NULL  then get_buku(d) else null end buku_d,
        CASE WHEN e IS NOT NULL  then get_buku(e) else null end buku_e,
        CASE
           WHEN a IS NOT NULL
           THEN
              (
              SELECT jln_wp_sppt||' '||blok_kav_no_wp_sppt||' '||kelurahan_wp_sppt||' '||kota_wp_sppt alamat
                 FROM spo.sppt a                   
                WHERE     thn_pajak_sppt = TO_CHAR (SYSDATE, 'yyyy')
                      AND a.kd_kecamatan = RPT_PIUTANG.kd_kecamatan
                      AND a.kd_kelurahan = RPT_PIUTANG.kd_kelurahan
                      AND a.kd_blok = RPT_PIUTANG.kd_blok
                      AND a.no_urut = RPT_PIUTANG.no_urut
                      AND a.kd_jns_op = RPT_PIUTANG.kd_jns_op)
           ELSE
              NULL
        END alamat_a,
        CASE
           WHEN b IS NOT NULL
           THEN
              (
              SELECT jln_wp_sppt||' '||blok_kav_no_wp_sppt||' '||kelurahan_wp_sppt||' '||kota_wp_sppt alamat
                 FROM spo.sppt a                   
                WHERE     thn_pajak_sppt = TO_CHAR (SYSDATE, 'yyyy') - 1
                      AND a.kd_kecamatan = RPT_PIUTANG.kd_kecamatan
                      AND a.kd_kelurahan = RPT_PIUTANG.kd_kelurahan
                      AND a.kd_blok = RPT_PIUTANG.kd_blok
                      AND a.no_urut = RPT_PIUTANG.no_urut
                      AND a.kd_jns_op = RPT_PIUTANG.kd_jns_op)
           ELSE
              NULL
        END alamat_b,
        CASE
           WHEN c IS NOT NULL
           THEN
              (
              SELECT jln_wp_sppt||' '||blok_kav_no_wp_sppt||' '||kelurahan_wp_sppt||' '||kota_wp_sppt alamat
                 FROM spo.sppt a                   
                WHERE     thn_pajak_sppt = TO_CHAR (SYSDATE, 'yyyy') -2
                      AND a.kd_kecamatan = RPT_PIUTANG.kd_kecamatan
                      AND a.kd_kelurahan = RPT_PIUTANG.kd_kelurahan
                      AND a.kd_blok = RPT_PIUTANG.kd_blok
                      AND a.no_urut = RPT_PIUTANG.no_urut
                      AND a.kd_jns_op = RPT_PIUTANG.kd_jns_op)
           ELSE
              NULL
        END alamat_c,
        CASE
           WHEN d IS NOT NULL
           THEN
              (
              SELECT jln_wp_sppt||' '||blok_kav_no_wp_sppt||' '||kelurahan_wp_sppt||' '||kota_wp_sppt alamat
                 FROM spo.sppt a                   
                WHERE     thn_pajak_sppt = TO_CHAR (SYSDATE, 'yyyy') - 3
                      AND a.kd_kecamatan = RPT_PIUTANG.kd_kecamatan
                      AND a.kd_kelurahan = RPT_PIUTANG.kd_kelurahan
                      AND a.kd_blok = RPT_PIUTANG.kd_blok
                      AND a.no_urut = RPT_PIUTANG.no_urut
                      AND a.kd_jns_op = RPT_PIUTANG.kd_jns_op)
           ELSE
              NULL
        END alamat_d,
        CASE
           WHEN e IS NOT NULL
           THEN
              (
              SELECT jln_wp_sppt||' '||blok_kav_no_wp_sppt||' '||kelurahan_wp_sppt||' '||kota_wp_sppt alamat
                 FROM spo.sppt a                   
                WHERE     thn_pajak_sppt = TO_CHAR (SYSDATE, 'yyyy') - 4
                      AND a.kd_kecamatan = RPT_PIUTANG.kd_kecamatan
                      AND a.kd_kelurahan = RPT_PIUTANG.kd_kelurahan
                      AND a.kd_blok = RPT_PIUTANG.kd_blok
                      AND a.no_urut = RPT_PIUTANG.no_urut
                      AND a.kd_jns_op = RPT_PIUTANG.kd_jns_op)
           ELSE
              NULL
        END alamat_e, 
        (
            SELECT  jalan_op||' '||blok_kav_no_op alamat
            from spo.dat_objek_pajak a
            join spo.ref_kecamatan kec on a.kd_kecamatan=kec.kd_kecamatan
            join spo.ref_kelurahan kel on a.kd_kecamatan=kel.kd_kecamatan and a.kd_kelurahan=kel.kd_kelurahan
              WHERE   a.kd_kecamatan = RPT_PIUTANG.kd_kecamatan
                    AND a.kd_kelurahan = RPT_PIUTANG.kd_kelurahan
                    AND a.kd_blok = RPT_PIUTANG.kd_blok
                    AND a.no_urut = RPT_PIUTANG.no_urut
                    AND a.kd_jns_op = RPT_PIUTANG.kd_jns_op) alamat_op,nm_kelurahan,nm_kecamatan")
         ->join(DB::raw('spo.ref_kecamatan kec'), function ($join) {
            $join->on(DB::raw("RPT_PIUTANG.kd_kecamatan"), '=', DB::raw("kec.kd_kecamatan"));
         })
         ->join(DB::raw('spo.ref_kelurahan kel'), function ($join) {
            $join->on(DB::raw("RPT_PIUTANG.kd_kecamatan"), '=', DB::raw("kel.kd_kecamatan"))
            ->on(DB::raw("RPT_PIUTANG.kd_kelurahan"), '=', DB::raw("kel.kd_kelurahan"));
         })
         ->get();
      $kelurahan = $kd_kelurahan <> '' && $kd_kecamatan <> '' ? Kelurahan::where('kd_kelurahan', $kd_kelurahan)->where('kd_kecamatan', $kd_kecamatan)->first() : null;
      $kecamatan = $kd_kecamatan <> '' ? Kecamatan::whereraw("kd_kecamatan='$kd_kecamatan'")->first() : null;
      $tahun = date('Y');
      return view('piutang.showExcel', compact('piutang', 'tahun', 'kelurahan', 'kecamatan'));
   }
   public function properties(): array
   {
      return [
         'creator'        => 'SIMPBB  part of SIPANJI',
         'title'          => 'DATA PIUTANG',
         'description'    => 'Piutang 5 tahun Terakhir',
         'subject'        => auth()->user()->nama,
         'keywords'       => 'laporan,piutang,realisasi,simpbb,sipanji,bapenda, kab malang',
         'category'       => 'PIUTANG',
         'manager'        => 'Kepala Bidang PBB P2',
         'company'        => 'BAPENDA KABUPATEN MALANG',
      ];
   }
}
