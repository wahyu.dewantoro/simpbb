<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DhkpWajibPajakExcel implements FromArray, WithHeadings, ShouldAutoSize, WithStyles, WithProperties
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $dhkp;

    public function __construct(array $dhkp)
    {
        $this->dhkp = $dhkp;
    }

    public function array(): array
    {
        return $this->dhkp;
    }

    public function headings(): array
    {
        return [
            'No',
            'NOP',
            'Alamat Objek',
            'Wajib Pajak',
            'Alamat WP',
            'Luas Bumi',
            'Luas Bng',
            'Buku',
            'PBB Awal',
            'Perubahan',
            'PBB Akhir',
            'Stimulus',
            'Harus Bayar',
            'Estimasi Denda',
            'koreksi',
            'Tgl Koreksi',
            'Realisasi',
            'Tgl Realisasi',
            'Keterangan'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => ['bold' => true, 'size' => 11],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => false
                ],
            ],

        ];
    }

    public function properties(): array
    {
        return [
            'creator'        => 'PBB P2',
            'title'          => 'DHKP Wajib Pajak',
            'description'    => 'Di unduh pada ' . now(),

            'category'       => 'DHKP SPPT PBB P2',
            'manager'        => 'PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }
}
