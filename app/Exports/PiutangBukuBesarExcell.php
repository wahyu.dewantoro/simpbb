<?php

namespace App\Exports;

use App\Helpers\Piutang;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PiutangBukuBesarExcell implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $param;

    function __construct($param)
    {
        $this->param = $param;
    }
    public function view(): View
    {
        $param = $this->param;
        $data = $param['data'];
        $thn_awal = $param['thn_awal'];
        $thn_akhir = $param['thn_akhir'];
        return view('piutang.buku_besar_excel', compact(
            'data',
            'thn_awal',
            'thn_akhir'
        ));
    }
}
