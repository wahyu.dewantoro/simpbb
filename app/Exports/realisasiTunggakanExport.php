<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;


class realisasiTunggakanExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles, WithProperties
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $realisasi;

    function __construct($realisasi)
    {
        $this->realisasi = $realisasi;
    }

    public function array(): array
    {
        return $this->realisasi;
    }

    public function headings(): array
    {
        return [
            'Buku',
            'Pokok',
            'Denda',
            'Jumlah'
        ];
    }


    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => ['bold' => true, 'size' => 11],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => false
                ],
            ],

        ];
    }
    public function properties(): array
    {
        return [
            'creator'        => 'PBB P2',
            'title'          => 'Realisasi tunggakan',
            'description'    => 'Di unduh pada ' . now(),

            'category'       => 'Realisasi',
            'manager'        => 'PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }

    /* public function view(): View
    {
        $tanggal = $this->parameter['tanggal'];
        $tahun = date('Y', strtotime($tanggal));
        $akhir = $tahun - 1;
        $awal = $akhir - 4;

        $scope = ['kd_kecamatan' => $this->parameter['kd_kecamatan'], 'kd_kelurahan' =>$this->parameter['kd_kelurahan']];
        $realisasi = Realisasi::RealisasiTunggakan($tanggal, $scope);

        return View('realisasi/tunggakan_excel', compact('tanggal', 'realisasi', 'tahun', 'awal', 'akhir'));
    }

    public function properties(): array
    {
        return [
            'creator'        => 'SIMPBB  part of SIPANJI',
            'title'          => 'Laporan realisasi tunggakan pbb',
            'description'    => 'Data realisasi pbb p2 selama 5 tahun terakhir dan tahun berjalan',
            'subject'        => auth()->user()->nama,
            'keywords'       => 'laporan,piutang,realisasi,simpbb,sipanji,bapenda, kab malang',
            'category'       => 'Realisasi',
            'manager'        => 'Kepala Bidang PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    } */
}
