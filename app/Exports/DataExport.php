<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DataExport implements FromView,ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data;
    function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        $data = $this->data;
        return view('tmp.data_piutang', compact('data'));
    }
}
