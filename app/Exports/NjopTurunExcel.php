<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class NjopTurunExcel implements FromView,ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $param;

    public function __construct($param)
    {
        $this->param = $param;
    }


    public function view(): View
    {

        $data = $this->param['data'];
        $now = $this->param['now'];
        $last = $this->param['last'];
        return view('analisa.njop_tahun_excel', compact('data', 'now', 'last'));
    }
}
