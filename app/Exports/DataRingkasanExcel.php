<?php

namespace App\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DataRingkasanExcel implements ShouldAutoSize, FromArray, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $data;
    function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return   $this->data;
    }

    public function headings(): array
    {
        return [
            'NOP',
            'ALAMAT OP',
            'NIK',
            'WAJIB PAJAK',
            'ALAMAT WP',
            'ZNT',
            'NIR',
            'LOKASI ZNT',
            'L. BUMI',
            'NJOP BUMI',
            'L. BNG',
            'NJOP BNG',
            'TOTAL NJOP',
            'NJOPTKP',
            'TARIF',
            'PBB',
            'KETERANGAN',

        ];
    }
}
