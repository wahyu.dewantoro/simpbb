<?php

namespace App\Exports;

use App\Helpers\Realisasi;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExcelRekapBayarHarian implements FromQuery, WithHeadings, ShouldAutoSize
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $request;

    function __construct($request)
    {
        $this->request = $request;
    }


    public function query()
    {
        $request = $this->request;

        $tanggala = $request->tanggala ?? '01 ' . date('M Y');
        $tanggalb = $request->tanggalb ?? date('d M Y');
        $ta = date('Ymd', strtotime($tanggala));
        $tb = date('Ymd', strtotime($tanggalb));

        return  Realisasi::rekapHarian($ta, $tb);
    }

    public function headings(): array
    {
        return [
            'Tanggal',
            'BANK JATIM',
            'POS',
            'TOTAL'
        ];
    }
}
