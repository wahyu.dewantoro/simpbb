<?php

namespace App\Exports;

use App\Helpers\Piutang;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithProperties;

class rekapPiutangExport implements FromView, ShouldAutoSize, WithProperties
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $parameter;


    function __construct($parameter)
    {
        $this->parameter = $parameter;
     
    }

    public function view(): View
    {
        $turut=$this->parameter['turut'];
        $buku=$this->parameter['buku'];
        $piutang = Piutang::rekapKecamatan($turut,$buku)->orderby('nm_kecamatan')->get();
        $tahun=date('Y');
        return view('piutang.indexExcel', compact('piutang', 'tahun'));
    }
    public function properties(): array
    {
        return [
            'creator'        => 'SIMPBB  part of SIPANJI',
            'title'          => 'DATA PIUTANG',
            'description'    => 'Piutang 5 tahun Terakhir',
            'subject'        => auth()->user()->nama,
            'keywords'       => 'laporan,piutang,realisasi,simpbb,sipanji,bapenda, kab malang',
            'category'       => 'PIUTANG',
            'manager'        => 'Kepala Bidang PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }
}
