<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class NjopPbbExport implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    protected $kd_kecamatan;

    public function __construct($kd_kecamatan)
    {
        $this->kd_kecamatan = $kd_kecamatan;
    }

    /* public function startCell(): string
    {
        return 'A2';
    }
 */

    public function view(): View
    {

        $data = DB::connection('oracle_satutujuh')->table('sppt')->select(db::raw("kd_propinsi||'.' || kd_dati2||'.' ||kd_kecamatan||'.' ||kd_kelurahan||'.' ||kd_blok||'.' ||no_urut||'.' ||kd_jns_op nop,nm_wp_sppt,luas_bumi_sppt,luas_bng_sppt,case when luas_bumi_sppt>0 then   njop_bumi_sppt/luas_bumi_sppt else 0 end  njop_bumi_meter,case when luas_bng_sppt>0 then  njop_bng_sppt/luas_bng_sppt else 0 end njop_bng_meter,njop_bumi_sppt + njop_bng_sppt  total_njop"))
            ->whereraw("thn_pajak_sppt='2022'     and status_pembayaran_sppt<>'3' and kd_kecamatan='" . $this->kd_kecamatan . "'")->get();
        return view('njop_pbb', compact('data'));
    }
}
