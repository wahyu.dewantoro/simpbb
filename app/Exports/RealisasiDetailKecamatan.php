<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RealisasiDetailKecamatan implements FromView, ShouldAutoSize
{
  
    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $param=$this->parameter;
        
        $tanggal = $param['tanggal'] ?? date('d M Y');
        $buku = $param['buku'] ?? ['1', '2'];
        $data = Realisasi::DetailKecamatan($tanggal, $buku);
        return View('realisasi/detail_kecamatan_excel', compact('tanggal', 'data'));
        
    }
}
