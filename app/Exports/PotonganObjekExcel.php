<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PotonganObjekExcel implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $param;
    public function __construct($param)
    {
        $this->param = $param;
    }


    public function view(): View
    {
        $data = $this->param['data'];
        return view('ms_potongan.list_data_excel', compact('data'));
    }
}
