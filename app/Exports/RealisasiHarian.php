<?php

namespace App\Exports;

use App\Helpers\Realisasi;
// use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\FromCollection;

class RealisasiHarian implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $parameter;

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }


    public function view(): View
    {
        $param=$this->parameter;
        $tanggal = $param['tanggal'];
        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $data = Realisasi::Harian($tanggal, $kd_kecamatan, $kd_kelurahan);
        $kecamatan=$param['kecamatan'];
        $kelurahan=$param['kelurahan'];
      return  View('realisasi/harian_excel', compact('data', 'tanggal', 'kecamatan', 'kelurahan'));
    }
}
