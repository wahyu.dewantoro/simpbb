<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class Pci implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $Pci;

    public function __construct(array $Pci)
    {
        $this->Pci = $Pci;
    }
    
    public function title(): string
    {
        return 'Laporan PCI ';
    }
    public function view(): View
    {
        return view('analisa/cetak/pci', [
            'result' => $this->Pci
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
