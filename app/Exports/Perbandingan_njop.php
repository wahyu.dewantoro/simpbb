<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class Perbandingan_njop implements FromArray
{
    protected $perbandingannjop;

    public function __construct(array $perbandingannjop)
    {
        $this->perbandingannjop = $perbandingannjop;
    }

    public function array(): array
    {
        return $this->perbandingannjop;
    }
}
