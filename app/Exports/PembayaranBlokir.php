<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class PembayaranBlokir implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $PembayaranBlokir;
    protected $keckel;

    public function __construct(array $PembayaranBlokir, array $keckel)
    {
        $this->PembayaranBlokir = $PembayaranBlokir;
        $this->keckel = $keckel;
    }
    
    public function title(): string
    {
        return 'Laporan Pembayaran ter Blokir ';
    }
    public function view(): View
    {
        return view('analisa/cetak/pembayaranBlokir', [
            'result' => $this->PembayaranBlokir,
            'keckel'=>$this->keckel
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
