<?php

namespace App\Exports;

use App\Helpers\Lhp;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class ExcelNotaPerhitungan implements FromView, ShouldAutoSize

{
    protected $request;

    function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $request = $this->request;
        $res = Lhp::notaListExport($request);
        $data = $res['data'];
        $excel=1;
        return View('penelitian.nota-perhitungan-print-konten', compact('data','excel'));
    }
}
