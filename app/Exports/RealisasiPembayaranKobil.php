<?php

namespace App\Exports;

use App\Models\Billing_kolektif;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class RealisasiPembayaranKobil implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $parameter;

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $tanggal = $this->parameter['tanggal'];
        $data = Billing_kolektif::whereraw("tgl_bayar >= to_date('$tanggal','dd/mm/yyyy') and tgl_bayar < to_date('$tanggal','dd/mm/yyyy')+1")
        ->select(DB::raw("kobil, billing_kolektif.kd_propinsi, billing_kolektif.kd_dati2, billing_kolektif.kd_kecamatan, billing_kolektif.kd_kelurahan, billing_kolektif.kd_blok, billing_kolektif.no_urut, billing_kolektif.kd_jns_op, billing_kolektif.tahun_pajak,pokok,denda,total,tgl_bayar,nm_wp"))
        ->join('data_billing', 'data_billing.data_billing_id', '=', 'billing_kolektif.data_billing_id')
        ->get();

        return View('realisasi/pembayaran_kobil_excel', compact('data', 'tanggal'));
    }
}
