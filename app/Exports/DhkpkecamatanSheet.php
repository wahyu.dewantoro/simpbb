<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use App\Sppt;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;



// class DhkpkecamatanSheet implements FromCollection
class DhkpkecamatanSheet implements FromView, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $param;
    function __construct($param)
    {
        $this->param = $param;
    }

    public function title(): string
    {
        return $this->param['nm_kecamatan'];
    }

    public function view(): View
    {
        $param = $this->param;

        $tahun = date('Y');
        $kd_kecamatan = $param['kd_kecamatan'];
        $buku=$param['buku'];

        $sppt = Sppt::select(DB::raw("sppt.kd_propinsi||'.'||sppt.kd_dati2||'.'||sppt.kd_kecamatan||'.'||sppt.kd_kelurahan||'.'||sppt.kd_blok||'-'||sppt.no_urut||'.'||sppt.kd_jns_op nop,nm_wp_sppt,  sppt.jln_wp_sppt,sppt.blok_kav_no_wp_sppt,sppt.rt_wp_sppt,sppt.rw_wp_sppt ,
                                        sppt.kelurahan_wp_sppt,sppt.kota_wp_sppt ,sppt.luas_bumi_sppt,sppt.luas_bng_sppt,sppt.pbb_yg_harus_dibayar_sppt,sppt.tgl_jatuh_tempo_sppt ,sppt.status_pembayaran_sppt,get_buku(sppt.pbb_yg_harus_dibayar_sppt) buku,jalan_op,blok_kav_no_op,rt_op,rw_op,
                                        (select tgl_pembayaran_sppt
                                        from pembayaran_sppt b
                                        where b.kd_propinsi=sppt.kd_propinsi
                                        and b.kd_dati2=sppt.kd_dati2
                                        and b.kd_kecamatan=sppt.kd_kecamatan
                                        and b.kd_kelurahan=sppt.kd_kelurahan
                                        and b.kd_blok=sppt.kd_blok
                                        and b.no_urut=sppt.no_urut
                                        and b.kd_jns_op=sppt.kd_jns_op
                                        and b.thn_pajak_sppt=sppt.thn_pajak_sppt
                                        and rownum=1)   tgl_bayar
                                        "))
            ->join('dat_objek_pajak', function ($join) {
                $join->on(db::raw('sppt.kd_propinsi'), '=', db::raw('dat_objek_pajak.kd_propinsi'))
                    ->on(db::raw('sppt.kd_dati2'), '=', db::raw('dat_objek_pajak.kd_dati2'))
                    ->on(db::raw('sppt.kd_kecamatan'), '=', db::raw('dat_objek_pajak.kd_kecamatan'))
                    ->on(db::raw('sppt.kd_kelurahan'), '=', db::raw('dat_objek_pajak.kd_kelurahan'))
                    ->on(db::raw('sppt.kd_blok'), '=', db::raw('dat_objek_pajak.kd_blok'))
                    ->on(db::raw('sppt.no_urut'), '=', db::raw('dat_objek_pajak.no_urut'))
                    ->on(db::raw('sppt.kd_jns_op'), '=', db::raw('dat_objek_pajak.kd_jns_op'));
            })->whereraw("thn_pajak_sppt='$tahun'
                                    and sppt.kd_kecamatan='$kd_kecamatan' and 
                                    get_buku(sppt.pbb_yg_harus_dibayar_sppt) in ($buku)
                                    and status_pembayaran_sppt<>'2'")->orderbyraw('sppt.kd_kelurahan asc, sppt.nm_wp_sppt asc')->get();
        return view('dhkp/_cetak', [
            'dhkp' => $sppt,
            'kecamatan' => $param['nm_kecamatan'],
            'kelurahan' => '',
            'tahun' => $tahun,
            'buku' => $buku,
        ]);
    }
}
