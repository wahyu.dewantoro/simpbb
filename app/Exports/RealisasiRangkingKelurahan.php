<?php

namespace App\Exports;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;


class RealisasiRangkingKelurahan implements FromView,  ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $realisasi;

    function __construct($realisasi)
    {
        $this->realisasi = $realisasi;
    }

    public function view(): View
    {

        $realisasi = $this->realisasi;
        $excel = '1';
        return view('realisasi.rangking_kelurahan_table_dua', compact('realisasi', 'excel'));

    }
}
