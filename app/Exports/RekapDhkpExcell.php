<?php

namespace App\Exports;

use App\Helpers\Dhkp;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RekapDhkpExcell implements FromView,ShouldAutoSize

{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $request;

    function __construct($request)
    {
        $this->request = $request;
    }
    public function view(): View
    {
        $request = $this->request;
        $tahun = $request->tahun;
        $data = Dhkp::RekapAll($tahun);
        if ($request->kd_kecamatan <> '') {
            $kd_kecamatan = $request->kd_kecamatan;
            $data = $data->where("ref_kelurahan.kd_kecamatan", $kd_kecamatan);
        }

        $data = $data->get();
        $excel='1';
        return view('analisa.dhkp.rekap_all_table', compact('data','excel'));

        /* return view('exports.invoices', [
            'invoices' => Invoice::all()
        ]); */
    }
}
