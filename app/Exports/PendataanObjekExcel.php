<?php

namespace App\Exports;

use App\Helpers\Pendataan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PendataanObjekExcel implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $parameter;
    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }
    public function view(): View
    {
        $param = $this->parameter;

        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $created_by = $param['created_by'];
        $tanggal = $param['tanggal'];

        $query = Pendataan::ReportObjek()->orderby('dat_objek_pajak.tgl_perekaman_op', 'desc');
        if (!empty($kd_kecamatan)) {
            $query->whereraw("dat_objek_pajak.kd_kecamatan='$kd_kecamatan'");
        }

        if (!empty($kd_kelurahan)) {
            $query->whereraw("dat_objek_pajak.kd_kelurahan='$kd_kelurahan'");
        }

        if (!empty($created_by)) {
            $query->whereraw("created_by='$created_by'");
        }
        if (!empty($tanggal)) {
            $query->whereraw("trunc(created_at)= trunc(to_date('$tanggal','dd-mm-yyyy')) ");
        }
        $data = $query->get();
        return View('pendataan/report-objek-excel', compact('data', 'param'));
    }
}
