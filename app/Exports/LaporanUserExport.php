<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LaporanUserExport implements FromArray, WithHeadings, ShouldAutoSize
{
    protected $data;

    // Konstruktor untuk menerima data array
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    // Mengembalikan array yang akan diexport
    public function array(): array
    {
        return $this->data;
    }

    // Menambahkan header kolom
    public function headings(): array
    {
        return [
            'No',
            'Nomor Layanan',
            'NOP',
            'Nama',
            'Alamat',
            'Jenis Layanan',
            'Luas Bumi',
            'Luas Bangunan',
            'Status',
            'Petugas',
        ];
    }
}
