<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class RealisasiRangkingKecamatan implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $realisasi;

    function __construct($realisasi)
    {
        $this->realisasi = $realisasi;
    }


    public function view(): View
    {

        $realisasi = $this->realisasi;
        $excel = '1';
        return view('realisasi.rangking_kecamatan_table_dua', compact('realisasi', 'excel'));

        /* $tahun = $this->parameter['tahun'];
        $tanggal = $this->parameter['tanggal'];
        $buku = $this->parameter['buku'];
        $realisasi = Realisasi::RangkingKecamatan($tanggal, $buku);
        return View('realisasi/rangking_kecamatan_excel', compact('tahun','tanggal', 'buku','realisasi')); */
    }

    public function columnWidths(): array
    {
        return [];
       /*  return [
            'A' => 4,
            'B' => 24,
            'C' => 11,
            'D' => 17,
            'E' => 17,
            'F' => 17,
            'G' => 17,
            'H' => 17,
            'I' => 10
        ]; */
    }
}
