<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
// use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromArray;
// use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use Maatwebsite\Excel\Concerns\WithHeadings;

class excel_laporan_tagihan implements FromArray, ShouldAutoSize, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;
    protected $data;

    public function __construct(array  $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data['data'];
    }

    public function headings(): array
    {

        if ($this->data['jenis'] == '2') {
            return [
                'No',
                'NOP',
                'Nama',
                'Alamat Subjek',
                'Alamat Objek',
                'Buku',
                'Masa Pajak',
                // 'Terbit',
                'Ketetapan',
                'Stimulus',
                'Denda',
                'keterangan',
                'status objek'
            ];
        } else {
            return [
                'No',
                'NOP',
                'Nama',
                'Alamat Subjek',
                'Alamat Objek',
                'Buku',
                'Masa Pajak',
                'Terbit',
                'Ketetapan',
                'Stimulus',
                'Denda',
                'keterangan'
            ];
        }
    }

    /*  public function view(): View
    {
        $param = $this->data;
    return view('laporan_tagihan/index_excel', compact('param'));
    } */
}
