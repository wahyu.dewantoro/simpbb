<?php

namespace App\Exports;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class RealisasiBaku implements FromView, WithColumnWidths
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $parameter;

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $tahun = $this->parameter['tahun'];
        $tanggal=$this->parameter['tanggal'];
        $realisasi = Realisasi::Baku($tanggal);
        return View('realisasi/baku_excel', compact('tahun','tanggal', 'realisasi'));
    }

    public function columnWidths(): array
    {
        return [
            'A' => 22,
            'B' => 11,            
            'C'=>17,
            'D'=>17,
            'E'=>17,
            'F'=>17,
            'G'=>9,
            'H'=>17,
        ];
    }
}
