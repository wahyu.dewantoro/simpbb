<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class DaftarSK implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $DaftarSK;
    protected $Subjek;

    public function __construct(array $DaftarSK,array $Subjek)
    {
        $this->DaftarSK = $DaftarSK;
        $this->Subjek = $Subjek;
    }
    
    public function title(): string
    {
        return 'Laporan Daftar SK ';
    }
    public function view(): View
    {
        return view('suratkeputusan/cetak', [
            'result' => $this->DaftarSK,
            'subjek' => $this->Subjek,
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
