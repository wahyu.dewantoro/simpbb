<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class Dafnom_export implements FromArray
{
    protected $dafnom_export;

    public function __construct(array $dafnom_export)
    {
        $this->dafnom_export = $dafnom_export;
    }

    public function array(): array
    {
        return $this->dafnom_export;
    }
}
