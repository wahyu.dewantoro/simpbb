<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

// use App\Kecamatan;
// use App\Kelurahan;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DhkpExport implements WithMultipleSheets
{
    /**
     * @return \Illuminate\Support\Collection
     */


    protected $tahun;
    protected $kd_kecamatan;
    protected $kd_kelurahan;
    protected $buku;

    function __construct($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $this->tahun = $tahun;
        $this->kd_kecamatan = $kd_kecamatan;
        $this->kd_kelurahan = $kd_kelurahan;
        $this->buku = $buku;
    }


    public function sheets(): array
    {
        $sheets = [];
        $buku = 1;
        $loop = 5;

        if (!in_array(1, $this->buku) || !in_array(345, $this->buku)) {
            if (in_array(1, $this->buku)) {
                $sheets[] = new DhkpExportBukuSheet($this->tahun, $this->kd_kecamatan, $this->kd_kelurahan, 1);
            }

            if (in_array(2, $this->buku)) {
                $sheets[] = new DhkpExportBukuSheet($this->tahun, $this->kd_kecamatan, $this->kd_kelurahan, 2);
            }

            if (in_array(3, $this->buku)) {
                $sheets[] = new DhkpExportBukuSheet($this->tahun, $this->kd_kecamatan, $this->kd_kelurahan, 3);
            }

            if (in_array(4, $this->buku)) {
                $sheets[] = new DhkpExportBukuSheet($this->tahun, $this->kd_kecamatan, $this->kd_kelurahan, 4);
            }
            if (in_array(5, $this->buku)) {
                $sheets[] = new DhkpExportBukuSheet($this->tahun, $this->kd_kecamatan, $this->kd_kelurahan, 5);
            }
        } else {
            for ($buku; $buku <= $loop; $buku++) {
                $sheets[] = new DhkpExportBukuSheet($this->tahun, $this->kd_kecamatan, $this->kd_kelurahan, $buku);
            }
        }



        return $sheets;
    }
}
