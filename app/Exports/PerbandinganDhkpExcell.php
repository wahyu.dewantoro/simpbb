<?php

namespace App\Exports;

use App\Helpers\Dhkp;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PerbandinganDhkpExcell implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $request;

    function __construct($request)
    {
        $this->request = $request;
    }
    public function view(): View
    {
        $request = $this->request;
        $tahun = $request->tahun;
        $tahun_last = $request->tahun_last;
        $data = Dhkp::RekapPerbandingan($tahun, $tahun_last);
        if ($request->kd_kecamatan <> '') {
            $kd_kecamatan = $request->kd_kecamatan;
            $data = $data->where("ref_kelurahan.kd_kecamatan", $kd_kecamatan);
        }

        $data = $data->get();
        $excel = '1';
        return view('analisa.dhkp.rekap_perbandingan_table', compact('data', 'excel', 'tahun', 'tahun_last'));
    }
}
