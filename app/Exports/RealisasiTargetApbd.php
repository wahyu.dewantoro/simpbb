<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithProperties;

class RealisasiTargetApbd implements FromView, WithColumnWidths, WithProperties
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $parameter;

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 22,
            'B' => 17,
            'C' => 17,
            'D' => 17,
            'E' => 17,
            'F' => 17,
            'G' => 17,
            'H' => 17,
            'I' => 17,
            'J' => 9
        ];
    }

    public function view(): View
    {
        $tanggal = $this->parameter['tanggal'];
        $tahun = date('Y', strtotime($tanggal));
        $tanggal = date('Ymd', strtotime($tanggal));

        $realisasi = Realisasi::targetApbd($tanggal);
        return view('realisasi.target_apbd_excel', ['cutoff' => $tanggal, 'tahun' => $tahun, 'realisasi' => $realisasi]);
    }
    public function properties(): array
    {
        $tanggal = $this->parameter['tanggal'];
        $tahun = date('Y', strtotime($tanggal));
        $tanggal = date('Ymd', strtotime($tanggal));
        return [
            'creator'        => 'SIMPBB  part of SIPANJI',
            // 'lastModifiedBy' => 'Patrick Brouwers',
            'title'          => 'LAPORAN TARGET APBD DAN REALISASI PAJAK BUMI DAN BANGUNAN TAHUN PAJAK ' .$tahun.' cut off '.tglIndo($tanggal),
            'description'    => 'Laporan realisasi',
            'subject'        => auth()->user()->nama,
            'keywords'       => 'laporan,realisasi,simpbb,sipanji,bapenda, kab malang',
            'category'       => 'Realisasi',
            'manager'        => 'Kepala Bidang PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }
}
