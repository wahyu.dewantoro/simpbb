<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RealisasiBulanan implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $parameter;

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $param=$this->parameter;
        $bulan = $param['bulan'];
        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $data = Realisasi::Bulanan($bulan, $kd_kecamatan, $kd_kelurahan);
        $kecamatan=$param['kecamatan'];
        $kelurahan=$param['kelurahan'];
      return  View('realisasi/bulanan_excel', compact('data', 'bulan', 'kecamatan', 'kelurahan'));
    }
}

