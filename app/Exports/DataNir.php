<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class DataNir implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $DataNir;
    protected $keckel;

    public function __construct(array $DataNir,array $keckel)
    {
        $this->DataNir = $DataNir;
        $this->keckel = $keckel;
    }
    
    public function title(): string
    {
        return 'REKAP DATA NIR ';
    }
    public function view(): View
    {
        return view('analisa/cetak/data_nir', [
            'result' => $this->DataNir,
            'result' => $this->keckel,
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
