<?php

namespace App\Exports;

use App\Sppt;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class spptSheetKelurahan implements FromCollection, WithTitle, WithCustomStartCell, WithEvents,ShouldAutoSize
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $kd_kecamatan;
    protected $kd_kelurahan;
    protected $title;

    public function __construct($kd_kecamatan, $kd_kelurahan, $title)
    {
        $this->kd_kecamatan = $kd_kecamatan;
        $this->kd_kelurahan  = $kd_kelurahan;
        $this->title = $title;
    }

    public function startCell(): string
    {
        return 'A2';
    }


    public function collection()
    {
        return Sppt::join('dat_objek_pajak', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                ->on('sppt.no_urut', '=', 'dat_objek_pajak.no_urut')
                ->on('sppt.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
        })->join('dat_op_bumi', function ($join) {
            $join->on('dat_op_bumi.kd_propinsi', '=', 'dat_objek_pajak.kd_propinsi')
                ->on('dat_op_bumi.kd_dati2', '=', 'dat_objek_pajak.kd_dati2')
                ->on('dat_op_bumi.kd_kecamatan', '=', 'dat_objek_pajak.kd_kecamatan')
                ->on('dat_op_bumi.kd_kelurahan', '=', 'dat_objek_pajak.kd_kelurahan')
                ->on('dat_op_bumi.kd_blok', '=', 'dat_objek_pajak.kd_blok')
                ->on('dat_op_bumi.no_urut', '=', 'dat_objek_pajak.no_urut')
                ->on('dat_op_bumi.kd_jns_op', '=', 'dat_objek_pajak.kd_jns_op');
        })
            ->selectraw(DB::raw("sppt.kd_propinsi,
         sppt.kd_dati2,
         sppt.kd_kecamatan,
         sppt.kd_kelurahan,
         sppt.kd_blok,
         sppt.no_urut,
         sppt.kd_jns_op,
         thn_Pajak_sppt,
         nm_wp_sppt,
         luas_bumi_sppt,
         luas_bng_sppt,
         njop_bumi_sppt,
         njop_bng_sppt,
         pbb_yg_harus_dibayar_sppt,
         case when luas_bumi_sppt >0 then njop_bumi_sppt/luas_bumi_sppt else 0 end njop_bumi_m,
         case when luas_bng_sppt >0 then    njop_bng_sppt/luas_bng_sppt else 0 end njop_bng_m,
         kd_znt,jalan_op"))
            ->where('sppt.thn_pajak_sppt', date('Y'))->where('sppt.kd_kecamatan', $this->kd_kecamatan)->where('sppt.kd_kelurahan', $this->kd_kelurahan)->get();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return  $this->title;
        // return 'asdasf';
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class => function (AfterSheet $event) {
                /** @var Sheet $sheet */
                $sheet = $event->sheet;

                $sheet->mergeCells('A1:G1');
                $sheet->setCellValue('A1', "NOP");

                $sheet->setCellValue('H1', "TAHUN");
                $sheet->setCellValue('I1', "WAJIB PAJAK");
                $sheet->setCellValue('J1', "LUAS BUMI");
                $sheet->setCellValue('K1', "LUAS BNG");
                $sheet->setCellValue('L1', "NJOP BUMI");
                $sheet->setCellValue('M1', "NJOP BNG");
                $sheet->setCellValue('N1', "PBB");
                $sheet->setCellValue('O1', "NJOP BUM/M");
                $sheet->setCellValue('P1', "NJOP BNG/M");
                $sheet->setCellValue('Q1', "ZNT");
                $sheet->setCellValue('R1', "JALAN OBJEK");



                $styleArray = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];

                $cellRange = 'A1:R1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
            },
        ];
    }


  /*   public function headings(): array
    {
        return [
            'NOP KD_PROPINSI',
            'NOP KD_DATI2',
            'NOP KD_KECAMATAN',
            'NOP KD_KELURAHAN',
            'NOP KD_BLOK',
            'NOP NO_URUT',
            'NOP KD_JNS_OP',
            "TAHUN_PAJAK thn_Pajak_sppt",
            "WAJIB_PAJAK nm_wp_sppt",
            "LUAS_BUMI luas_bumi_sppt",
            "LUAS_BNG luas_bng_sppt",
            "NJOP_BUMI njop_bumi_sppt",
            "NJOP_BNG njop_bng_sppt",
            "PBB pbb_yg_harus_dibayar_sppt",
            "NJOP_BUMI/M njop_bumi_m",
            "NJOP_BNGI/M njop_bng_m",
            "ZNT kd_znt",
            "JALAN_OP jalan_op",

 
        ];
    } */
}
