<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class LunasLimaTahun implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $parameter;

    function __construct($parameter)
    {
        $this->kd_kecamatan = $parameter;
    }


    public function view(): View
    {

        $data = DB::connection('oracle_dua')->select(DB::raw("
    SELECT    y.kd_propinsi
           ||'.'|| y.kd_dati2
           ||'.'|| y.kd_kecamatan
           ||'.'|| y.kd_kelurahan
           ||'.'|| y.kd_blok
           ||'-'|| y.no_urut
           ||'.'|| y.kd_jns_op
              nop,
           z.nm_kecamatan,
           c.nm_kelurahan,
           (SELECT nm_wp_sppt
              FROM sppt
             WHERE     kd_propinsi = y.kd_propinsi
                   AND kd_dati2 = y.kd_dati2
                   AND kd_kecamatan = y.kd_kecamatan
                   AND kd_kelurahan = y.kd_kelurahan
                   AND kd_blok = y.kd_blok
                   AND no_urut = y.no_urut
                   AND kd_jns_op = y.kd_jns_op
                   AND thn_pajak_sppt = '2016')
              nm_wp,
           (SELECT    q.jalan_op
                   || ' '
                   || q.blok_kav_no_op
                   || 'RW : '
                   || q.rw_op
                   || ', RT : '
                   || q.rt_op
                   || ', Desa / Kel : '
                   || (SELECT nm_kelurahan
                         FROM ref_kelurahan
                        WHERE     kd_propinsi = q.kd_propinsi
                              AND kd_dati2 = q.kd_dati2
                              AND kd_kecamatan = q.kd_kecamatan
                              AND kd_kelurahan = q.kd_kelurahan)
                   || ', Kec : '
                   || (SELECT nm_kecamatan
                         FROM ref_kecamatan
                        WHERE     kd_propinsi = q.kd_propinsi
                              AND kd_dati2 = q.kd_dati2
                              AND kd_kecamatan = q.kd_kecamatan)
                   || ', '
                   || (SELECT nm_dati2
                         FROM ref_dati2
                        WHERE     kd_propinsi = q.kd_propinsi
                              AND kd_dati2 = q.kd_dati2)
              FROM dat_objek_pajak q
             WHERE     q.kd_propinsi = y.kd_propinsi
                   AND q.kd_dati2 = y.kd_dati2
                   AND q.kd_kecamatan = y.kd_kecamatan
                   AND q.kd_kelurahan = y.kd_kelurahan
                   AND q.kd_blok = y.kd_blok
                   AND q.no_urut = y.no_urut
                   AND q.kd_jns_op = y.kd_jns_op)
              alamat_wp,
           y.pbb2017,
           y.pbb2018,
           y.pbb2019,
           y.pbb2020,
           y.pbb2016
      FROM (SELECT x.*,
                   (SELECT pbb_yg_harus_dibayar_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2017')
                      pbb2017,
                   (SELECT pbb_yg_harus_dibayar_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2018')
                      pbb2018,
                   (SELECT pbb_yg_harus_dibayar_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2019')
                      pbb2019,
                   (SELECT pbb_yg_harus_dibayar_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2020')
                      pbb2020,
                   (SELECT pbb_yg_harus_dibayar_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2016')
                      pbb2016,
                   (SELECT status_pembayaran_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2017')
                      status2017,
                   (SELECT status_pembayaran_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2018')
                      status2018,
                   (SELECT status_pembayaran_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2019')
                      status2019,
                   (SELECT status_pembayaran_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2020')
                      status2020,
                   (SELECT status_pembayaran_sppt
                      FROM sppt
                     WHERE     kd_propinsi = x.kd_propinsi
                           AND kd_dati2 = x.kd_dati2
                           AND kd_kecamatan = x.kd_kecamatan
                           AND kd_kelurahan = x.kd_kelurahan
                           AND kd_blok = x.kd_blok
                           AND no_urut = x.no_urut
                           AND kd_jns_op = x.kd_jns_op
                           AND thn_pajak_sppt = '2016')
                      status2016
              FROM (SELECT DISTINCT kd_propinsi,
                                    kd_dati2,
                                    kd_kecamatan,
                                    kd_kelurahan,
                                    kd_blok,
                                    no_urut,
                                    kd_jns_OP
                      FROM sppt
                     WHERE thn_pajak_sppt = '2016') x) y,
           ref_kecamatan z,
           ref_kelurahan c
     WHERE     z.kd_propinsi = y.kd_propinsi
           AND z.kd_dati2 = y.kd_dati2
           AND z.kd_kecamatan = y.kd_kecamatan
           AND c.kd_propinsi = y.kd_propinsi
           AND c.kd_dati2 = y.kd_dati2
           AND c.kd_kecamatan = y.kd_kecamatan
           AND c.kd_kelurahan = y.kd_kelurahan
           AND y.status2017 = '0'
           AND y.status2018 = '0'
           AND y.status2019 = '0'
           AND y.status2020 = '0'
           AND y.status2016 = '0'
           and y.kd_kecamatan='" . $this->kd_kecamatan . "'"));
        return view('realisasi.lunaslimatahun', compact('data'));
    }
}
