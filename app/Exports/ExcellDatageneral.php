<?php

namespace App\Exports;

use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExcellDatageneral implements FromView, ShouldAutoSize
{

    /**
     * @return \Illuminate\Support\Collection
     */
    protected $param;
    function __construct($param)
    {
        $this->param = $param;
    }
    public function view(): View
    {
        $array = $this->param;
        // $data = $array['data'];

        $sql = $array['sql'];
        $connection        = $array['connection'];
        $data = DB::connection($connection)->select(DB::raw($sql));
        $view = $array['view'];
        return view($view, compact('data'));
    }
}