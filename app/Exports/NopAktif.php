<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class NopAktif implements FromArray,WithHeadings,ShouldAutoSize,WithEvents
{
    protected $NopAktif;

    public function __construct(array $NopAktif)
    {
        $this->NopAktif = $NopAktif;
    }
    public function headings(): array
    {
        return [
            'NO',
            'NIK',
            'NAMA WP',
            'NOP',
            'ALAMAT',
            'LUAS BUMI',
            'LUAS BANGUNAN',
            'TANGGAL'
        ];
    }
    public function registerEvents(): array
    {
        return [
            // AfterSheet::class    => function(AfterSheet $event) {
            //     $cellRange = 'A1:I1'; // All headers
            //     $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            // },
        ];
    }
    public function array(): array
    {
        return $this->NopAktif;
    }
}
