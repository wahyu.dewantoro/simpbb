<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class cetak_dhkp_excel implements FromArray, WithHeadings,ShouldAutoSize,WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data;

    function __construct($data)
    {
        $this->data=$data;
    }

    public function array(): array
    {
        //
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'NO',
            'NOP',
            'ALAMAT OBJEK',
            'WAJIB PAJAK',
            'BUMI',
            'BNG',
            'PBB',
            'STIMULUS',
            'HARUS DIBAYAR'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
