<?php

namespace App\Exports;

use App\Helpers\Dhkp;
use App\Sppt;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithTitle;

// use App\Kecamatan;
// use Maatwebsite\Excel\Concerns\WithMultipleSheets;
// use Maatwebsite\Excel\Concerns\WithTitle;

class DhkpKabupaten implements FromView, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $tahun;

    function __construct($tahun)
    {
        $this->tahun = $tahun;
    }


    public function title(): string
    {
        return 'DHKP 3,4,5';
    }

    public function view(): View
    {


        $tahun = $this->tahun;
        $buku = '3,4,5';
        $param = [
            'tahun' => date('Y'),
            'buku' => $buku,
            // 'kd_kecamatan' => $this->kd_kecamatan
        ];
        $sppt = Dhkp::exportExcelWepe($param);
        return view('dhkp/_cetak', [
            'dhkp' => $sppt,
            'tahun' => $tahun,
            'buku' => $buku,
        ]);
    }
}
