<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class detailBayarExport implements FromView,ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        $data = $this->data;
        $objek = $data['objek'];
        $nop = $data['nop'];
        $bayar = $data['bayar'];
        return view('objek.riwayatBayar_konten_excel',  compact('objek', 'nop', 'bayar'));
    }
}
