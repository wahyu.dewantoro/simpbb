<?php

namespace App\Exports;

// use Illuminate\Contracts\View\View;

use App\TmpSaldo;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
// use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\FromView;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class dataSaldoexport implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    protected $tahun;

    function __construct($tahun)
    {
        $this->tahun = $tahun;
    }

    public function collection()
    {

        return $this->tahun;
    }

    public function headings(): array
    {
        return [
            'NOP',
            'TAHUN_PAJAK',
            'TAHUN_TERBIT',
            'NM_WP_SPPT',
            'ALAMAT_OP',
            'ALAMAT_WP',
            'PBB',
            'Stimulus',
            'KOREKSI',
            'BAYAR',
            'SALDO_SISA',

        ];
    }
}
