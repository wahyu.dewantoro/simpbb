<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use App\Helpers\Dhkp;
use App\Kecamatan;
use App\Kelurahan;
use App\Sppt;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;


use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;


class DhkpExportBukuSheet implements WithColumnFormatting, FromView, ShouldAutoSize, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $tahun;
    protected $kd_kecamatan;
    protected $kd_kelurahan;
    protected $buku;


    function __construct($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $this->tahun = $tahun;
        $this->kd_kecamatan = $kd_kecamatan;
        $this->kd_kelurahan = $kd_kelurahan;
        $this->buku = $buku;
    }

    public function title(): string
    {
        return 'Buku ' . $this->buku;
    }

    public function view(): View
    {
        $tahun = $this->tahun;
        $kd_kecamatan = $this->kd_kecamatan;
        $kd_kelurahan = $this->kd_kelurahan;
        $buku = $this->buku;
 

        $param = [
            'tahun' => $tahun,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'buku' =>$buku
        ];
        $sppt = Dhkp::exportExcelWepe($param);

        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan;
        $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan??'';
        return view('dhkp/_cetak', [
            'dhkp' => $sppt,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'tahun' => $tahun,
            'buku' => $buku,
        ]);
    }

    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    /*  public function collection()
    {
        //
    } */
}
