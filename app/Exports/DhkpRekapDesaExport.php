<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DhkpRekapDesaExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles, WithProperties
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $dhkp;

    public function __construct(array $dhkp)
    {
        $this->dhkp = $dhkp;
    }

    public function array(): array
    {
        return $this->dhkp;
    }

    public function headings(): array
    {
        return [
            'Kecamatan',
            'Kel/Desa',
            'Objek',
            'Ketetapan PBB',
            'Stimulus',
            'Bayar',
            'Koreksi',
            'Sisa',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => ['bold' => true, 'size' => 11],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => false
                ],
            ],

        ];
    }

    public function properties(): array
    {
        return [
            'creator'        => 'PBB P2',
            'title'          => 'DHKP Wajib Pajak',
            'description'    => 'Di unduh pada ' . now(),

            'category'       => 'DHKP SPPT PBB P2',
            'manager'        => 'PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }

    /*  protected $tahun;
    protected $kd_kecamatan;
    protected $buku;
    protected $kd_kelurahan;

    function __construct($tahun, $kd_kecamatan, $buku, $kd_kelurahan)
    {
        $this->tahun = $tahun;
        $this->kd_kecamatan = $kd_kecamatan;
        $this->buku = $buku;
        $this->kd_kelurahan = $kd_kelurahan;
    }


    public function view(): View
    {
        $tahun = $this->tahun;
        $kd_kecamatan = $this->kd_kecamatan;
        $buku = $this->buku;
        $kd_kelurahan = $this->kd_kelurahan ?? null;


        $preview = Dhkp::RekapDesa($tahun, $kd_kecamatan, $buku, $kd_kelurahan);
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan;

        if (!empty($kd_kelurahan)) {
            $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan;
        }


        switch ($buku) {
            case '1':
                # code...
                $jns_buku='Buku 1 & 2';
                break;
                case '2':
                    # code...
                    $jns_buku='Buku 3, 4 dan 5';
                    break;
                
            default:
                # code...
                $jns_buku="";
                break;
        }

        return view('dhkp/_cetak_rekap_desa', [
            'data' => $preview,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan ?? null,
            'tahun' => $tahun,
            'buku' => $jns_buku
        ]);
    } */
}
