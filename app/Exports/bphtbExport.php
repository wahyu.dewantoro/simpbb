<?php

namespace App\Exports;

use App\Helpers\Bphtb;
use App\Kecamatan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

// use Maatwebsite\Excel\Concerns\FromCollection;

class bphtbExport implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $tahun;
    protected $kd_kecamatan;

    function __construct($tahun, $kd_kecamatan)
    {
        $this->tahun = $tahun;
        $this->kd_kecamatan = $kd_kecamatan;
    }


    public function view(): View
    {
        $tahun = $this->tahun;
        $kd_kecamatan = $this->kd_kecamatan;
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->firstorfail();
        $data = Bphtb::ajuanKecamatan($tahun, $kd_kecamatan)->orderByRaw(DB::raw("rpt_sspd_final.nop"))->get();
        return view('bphtb.excel', compact('data', 'kecamatan', 'tahun', 'kd_kecamatan'));


        /* $preview = Dhkp::RekapDesa($tahun, $kd_kecamatan, $buku);
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan;
        return view('dhkp/_cetak_rekap_desa', [
            'data' => $preview,
            'kecamatan' => $kecamatan,
            'tahun' => $tahun,
            'buku' => implode(',', $buku)
        ]); */
    }
}
