<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
use App\Helpers\Dhkp;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DhkpRekapPerbandinganDesaExport implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $tahun;
    protected $kd_kecamatan;

    function __construct($nm_kecamatan,$tahun, $kd_kecamatan, $buku)
    {
        $this->tahun = $tahun;
        $this->kd_kecamatan = $kd_kecamatan;
        $this->buku = $buku;
        $this->nm_kecamatan=$nm_kecamatan;
    }
    public function view(): View
    {
        $tahun = $this->tahun;
        $kd_kecamatan = $this->kd_kecamatan;
        $buku = $this->buku;
        $kecamatan = $this->nm_kecamatan;
        $preview = Dhkp::RekapPerbandinganDesa($tahun, $kd_kecamatan, $buku);
        return view('dhkp/_cetak_perbadingan_rekap_desa', [
            'data' => $preview,
            'tahun' => $tahun,
            'buku' => $buku,
            'kecamatan' => $kecamatan
        ]);
    }
}
