<?php

namespace App\Exports;

use App\Helpers\Piutang;
use App\Kecamatan;
use App\Kelurahan;
use App\Sppt;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithProperties;


class piutangTahunExcell implements FromView, ShouldAutoSize, WithProperties
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $parameter;

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $tahun = $this->parameter ?? date('Y');
        $data = DB::connection("oracle_dua")->table(DB::raw('sppt a'))->select(DB::raw("a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op,jalan_op,blok_kav_no_op, nm_kelurahan,nm_kecamatan ,thn_pajak_sppt,nm_wp_sppt ,jln_wp_sppt,blok_kav_no_wp_sppt, kelurahan_wp_sppt,kota_wp_sppt,pbb_yg_harus_dibayar_sppt pbb,get_buku(pbb_yg_harus_dibayar_sppt) buku"))
            ->join(DB::raw('dat_objek_pajak b'), function ($join) {
                $join->on(DB::raw('a.kd_propinsi'), '=', DB::raw('b.kd_propinsi'))
                    ->on(DB::raw('a.kd_dati2'), '=', DB::raw('b.kd_dati2'))
                    ->on(DB::raw('a.kd_kecamatan'), '=', DB::raw('b.kd_kecamatan'))
                    ->on(DB::raw('a.kd_kelurahan'), '=', DB::raw('b.kd_kelurahan'))
                    ->on(DB::raw('a.kd_blok'), '=', DB::raw('b.kd_blok'))
                    ->on(DB::raw('a.no_urut'), '=', DB::raw('b.no_urut'))
                    ->on(DB::raw('a.kd_jns_op'), '=', DB::raw('b.kd_jns_op'));
            })
            ->join(DB::raw('ref_kecamatan c'), function ($join) {
                $join->on(DB::raw('a.kd_kecamatan'), '=', DB::raw('c.kd_kecamatan'));
            })
            ->join(DB::raw('ref_kelurahan d'), function ($join) {
                $join->on(DB::raw('a.kd_kecamatan'), '=', DB::raw('d.kd_kecamatan'))
                    ->on(DB::raw('a.kd_kelurahan'), '=', DB::raw('d.kd_kelurahan'));
            })
            ->whereraw("thn_pajak_sppt='$tahun' and status_pembayaran_sppt='0'")->get();
        return view('piutang/tahunanExcel', compact('data', 'tahun'));
    }

    public function properties(): array
    {
        return [
            'creator'        => 'SIMPBB  part of SIPANJI',
            'title'          => 'DATA PIUTANG',
            'keywords'       => 'laporan,piutang,realisasi,simpbb,sipanji,bapenda, kab malang',
            'category'       => 'PIUTANG',
            'manager'        => 'Kepala Bidang PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }
}
