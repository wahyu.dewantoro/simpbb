<?php

namespace App\Exports;

use App\Helpers\Dhkp;
use App\Kecamatan;
use App\Kelurahan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class DhkpPerbandinganExport implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $tahun;
    protected $kd_kecamatan;
    protected $kd_kelurahan;

    function __construct($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $this->tahun = $tahun;
        $this->kd_kecamatan = $kd_kecamatan;
        $this->kd_kelurahan = $kd_kelurahan;
        $this->buku = $buku;
    }
    public function view(): View
    {
        $tahun = $this->tahun;
        $kd_kecamatan = $this->kd_kecamatan;
        $kd_kelurahan = $this->kd_kelurahan;
        $buku = $this->buku;
        $kecamatan = Kecamatan::where('kd_kecamatan', $kd_kecamatan)->first()->nm_kecamatan;
        $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->where('kd_kelurahan', $kd_kelurahan)->first()->nm_kelurahan;

        $preview = $sppt = Dhkp::PerbandinganWepe($tahun, $kd_kecamatan, $kd_kelurahan, $buku)->get();
        return view('dhkp/_cetak_perbadingan_desa', [
            'data' => $preview,
            'tahun' => $tahun,
            'buku' => $buku,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan
        ]);
    }
}
