<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class Blok implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $blok;
    protected $keckel;

    public function __construct(array $blok,array $keckel)
    {
        $this->blok = $blok;
        $this->keckel = $keckel;
    }
    
    public function title(): string
    {
        return 'Laporan Blok ';
    }
    public function view(): View
    {
        return view('analisa/cetak/blok', [
            'result' => $this->blok,
            'keckel'=>$this->keckel
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
