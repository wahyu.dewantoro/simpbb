<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;use App\Helpers\Pendataan;

use App\Helpers\Pendataan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;


class PendataanSubjekExcel implements FromView, ShouldAutoSize, WithColumnFormatting
{
    protected $parameter;
    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $param = $this->parameter;

        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $created_by = $param['created_by'];
        $tanggal = $param['tanggal'];

        $query = Pendataan::ReportSubjek()->orderby('dat_objek_pajak.tgl_perekaman_op', 'desc');
        if (!empty($kd_kecamatan)) {
            $query->whereraw("dat_objek_pajak.kd_kecamatan='$kd_kecamatan'");
        }

        if (!empty($kd_kelurahan)) {
            $query->whereraw("dat_objek_pajak.kd_kelurahan='$kd_kelurahan'");
        }

        if (!empty($created_by)) {
            $query->whereraw("created_by='$created_by'");
        }
        if (!empty($tanggal)) {
            $query->whereraw("trunc(created_at)= trunc(to_date('$tanggal','dd-mm-yyyy')) ");
        }
        $data = $query->get();
        return View('pendataan/report-subjek-excel', compact('data', 'param'));
    }

    public function columnFormats(): array
    {
        return [
            // 'A' => NumberFormat::FORMAT_TEXT,
            // 'B' => NumberFormat::FORMAT_NUMBER,
            // 'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            // 'E' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
