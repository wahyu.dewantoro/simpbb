<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class penangguhanIventarisasiExcel implements  WithHeadings, ShouldAutoSize, WithStyles,FromArray
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        //
        return $this->data;
    }


    public function styles(Worksheet $sheet)
    {
        return [
            1    => [
                'font' => ['bold' => true],
                /* 'fill' => [
                    'fillType'   => Fill::FILL_SOLID,
                    'startColor' => ['argb' => Color::RED],
                ], */
            ],
        ];
    }

    public function headings(): array
    {
        return [
            'No Transaksi',
            'Jenis',
            'NOP',
            'Tahun Pajak',
            'No Surat',
            'Tanggal Surat',
            'PBB'
        ];
    }
}
