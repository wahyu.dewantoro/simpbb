<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class NilaiKoreksiExcel implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */


    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }


    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'kd_propinsi',
            'kd_dati2',
            'kd_kecamatan',
            'kd_kelurahan',
            'kd_blok',
            'no_urut',
            'kd_jns_op',
            'thn_pajak_sppt',
            'nilai_koreksi',
        ];
    }
}
