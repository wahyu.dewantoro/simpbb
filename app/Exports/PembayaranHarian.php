<?php

namespace App\Exports;

use App\Helpers\Realisasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class PembayaranHarian implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $param = $this->parameter;
        $tgl_start = $param['tgl_start'] ?? date('d M Y');
        $tgl_end = $param['tgl_end'] ?? date('d M Y');
        $data = Realisasi::pembayaranHarian($tgl_start, $tgl_end)->get();

        return View('realisasi/pembayaran_bank_harian_excel', compact('tgl_start','tgl_end', 'data'));
    }
}
