<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DesaLunasExport implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $tahun;
    function __construct($tahun)
    {
        $this->tahun = $tahun;
    }

    public function view(): View
    {
        $kk = "";
        $user = Auth()->user();
        $role = $user->roles()->pluck('name')->toarray();
        $kd_kecamatan = "";
        $kk = "";
        if (in_array('Kecamatan', $role)) {
            $kec = $user->unitkerja()->pluck('kd_kecamatan')->toArray();
            foreach ($kec as $kec) {
                $kd_kecamatan .= " sppt.kd_kecamatan ='" . $kec . "' or";
                $kk .= " A.kd_kecamatan ='" . $kec . "' or";
            }

            $kd_kecamatan = " and ( " . substr($kd_kecamatan, 0, -2) . " )";
            $kk = " and ( " . substr($kk, 0, -2) . " )";
        }
        $thn = $this->tahun;
        $data =  DB::select("SELECT A.*, B.NM_KECAMATAN,null nilai_baku,C.NM_KELURAHAN from DESA_LUNAS A
                                        LEFT JOIN SPO.REF_KECAMATAN B ON A.KD_KECAMATAN = B.KD_KECAMATAN
                                        LEFT JOIN spo.REF_KELURAHAN C ON A.KD_KECAMATAN = C.KD_KECAMATAN AND A.KD_KELURAHAN = C.KD_KELURAHAN 
                                        where TAHUN_PAJAK = '$thn' 
                                        " . $kk . "
                                        order by tanggal_lunas asc,created_at asc");
        return view('desa_lunas/cetak_excel', ['data' => $data]);
    }
}
