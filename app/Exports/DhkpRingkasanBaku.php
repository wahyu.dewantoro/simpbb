<?php

namespace App\Exports;

use App\Helpers\Dhkp;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DhkpRingkasanBaku implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $parameter;
    function __construct($parameter)
    {
        $this->parameter = $parameter;
    }

    public function view(): View
    {
        $param = $this->parameter;
        $dhkp = Dhkp::ringkasanBaku($param);
        $tahun = $param['tahun'];
        return View('dhkp/ringkasan_baku_excel', compact('dhkp', 'tahun'));
    }
}
