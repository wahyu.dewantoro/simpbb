<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class KompensasiLebihBayar implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $KompensasiLebihBayar;
    protected $Subjek;

    public function __construct(array $KompensasiLebihBayar,array $Subjek)
    {
        $this->KompensasiLebihBayar = $KompensasiLebihBayar;
        $this->Subjek = $Subjek;
    }
    
    public function title(): string
    {
        return 'Laporan Kompensasi Lebih Bayar ';
    }
    public function view(): View
    {
        return view('kompensasi_bayar/cetak', [
            'result' => $this->KompensasiLebihBayar,
            'subjek' => $this->Subjek,
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
