<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class reportTarikdataSpptexcel implements FromArray, WithHeadings,ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $array;
    // protected $tahun;

    function __construct($array)
    {
        $this->array = $array;
    }

    public function array(): array
    {
        //
        return $this->array['data'];
    }

    public function headings(): array
    {
        $tahun = $this->array['tahun'];
        $tahun_last = $this->array['tahun_last'];
        return [
            'NOP',
            'PBB ' . $tahun,
            'Insentif ' . $tahun,
            'PBB Akhir ' . $tahun,
            'PBB ' . $tahun_last,
            'Insentif ' . $tahun_last,
            'PBB Akhir ' . $tahun_last,
        ];
    }
}
