<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class LaporanInputPermohonan implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $LaporanInputPermohonan;

    public function __construct(array $LaporanInputPermohonan)
    {
        $this->LaporanInputPermohonan = $LaporanInputPermohonan;
    }
    
    public function title(): string
    {
        return 'Daftar Permohonan';
    }
    public function view(): View
    {
        return view('layanan/cetak/cetak', [
            'result' => $this->LaporanInputPermohonan
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
