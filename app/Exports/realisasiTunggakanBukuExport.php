<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class realisasiTunggakanBukuExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles, WithProperties
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $realisasi;

    function __construct($realisasi)
    {
        $this->realisasi = $realisasi;
    }

    public function array(): array
    {
        return $this->realisasi;
    }

    public function headings(): array
    {

        /* nop
thn_pajak_sppt
tahun_terbit
tgl_terbit_sppt
buku
pokok
denda
jumlah
tanggal_bayar */
        return [
            'NOP',
            'Tahun Pajak',
            'Tahun Terbit',
            'Tanggal Terbit',
            'Buku',
            'Pokok',
            'Denda',
            'Jumlah',
            'Tanggal Bayar'
        ];
    }


    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => ['bold' => true, 'size' => 11],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER,
                    'wrapText' => false
                ],
            ],

        ];
    }
    public function properties(): array
    {
        return [
            'creator'        => 'PBB P2',
            'title'          => 'Realisasi tunggakan',
            'description'    => 'Di unduh pada ' . now(),

            'category'       => 'Realisasi',
            'manager'        => 'PBB P2',
            'company'        => 'BAPENDA KABUPATEN MALANG',
        ];
    }
}
