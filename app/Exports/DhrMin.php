<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class DhrMin implements WithColumnFormatting, FromView, ShouldAutoSize,WithTitle
{
    protected $dhr;
    protected $keckel;

    public function __construct(array $dhr,array $keckel)
    {
        $this->dhr = $dhr;
        $this->keckel = $keckel;
    }
    
    public function title(): string
    {
        return 'Laporan DHR ';
    }
    public function view(): View
    {
        return view('analisa/cetak/dhr_min', [
            'result' => $this->dhr,
            'keckel'=>$this->keckel
        ]);
    }
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'H' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
