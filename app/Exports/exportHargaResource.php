<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class exportHargaResource implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $grouped;

    function __construct($grouped)
    {
        $this->grouped = $grouped;
    }
    public function view(): View
    {
        $grouped = $this->grouped;
        return view('ssh.resource.export_excel', compact('grouped'));
    }
}
