<?php

namespace App\Exports;

use App\Kelurahan;
use App\Models\Sppt;
use App\Sppt as AppSppt;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SpptExport implements WithMultipleSheets
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $kd_kecamatan;

    public function __construct($kd_kecamatan)
    {
        $this->kd_kecamatan = $kd_kecamatan;
    }

    public function sheets(): array
    {
        $sheets = [];
        $kel = Kelurahan::where('kd_kecamatan', $this->kd_kecamatan)->get();
        foreach ($kel as $rk) {
            $sheets[] = new spptSheetKelurahan($rk->kd_kecamatan, $rk->kd_kelurahan,$rk->kd_kelurahan.' - '.$rk->nm_kelurahan);
        }
        return $sheets;
    }

    /* public function collection()
    {
        return AppSppt::where('thn_pajak_sppt','2022')->where('kd_kecamatan','010')->where('kd_kelurahan','001')->get();
    } */
}
