<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class template_upload_nir_excel implements FromArray, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $znt;

    function __construct($znt)
    {
        $this->znt = $znt;
    }

    public function array(): array
    {
        //
        return $this->znt;
    }

    public function headings(): array
    {
        return [
            'KD_KECAMATAN',
            'KD_KELURAHAN',
            'KD_ZNT',
            'OBJEK',
            'LOKASI',
            'HKPD',
            'NIR'
        ];
    }
}
