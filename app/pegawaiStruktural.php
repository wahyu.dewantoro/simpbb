<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pegawaiStruktural extends Model
{
    //
    protected $guarded = [];
    protected $table = 'pegawai_struktural';
    /* protected $rules = [
        'kode' => 'sometimes|required|unique:pegawai_struktural',
    ]; */
}
