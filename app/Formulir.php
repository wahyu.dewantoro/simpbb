<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formulir extends Model
{
    protected $guarded = [];
    protected $table = 'ref_formulir';

    public function layanan()
    {
        return $this->belongsToMany('App\Models\Jenis_layanan', 'ref_formulir_layanan', 'ref_formulir_id', 'jenis_layanan_id');
        
    }
}
