<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormulirLayanan extends Model
{
    protected $guarded = [];
    protected $table = 'ref_formulir_layanan';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
