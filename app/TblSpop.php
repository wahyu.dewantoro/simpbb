<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblSpop extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'tbl_spop';
    public $timestamps = false;
    protected $primaryKey = ['no_formulir'];
}
