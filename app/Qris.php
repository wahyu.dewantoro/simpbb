<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qris extends Model
{
    //
    protected $guarded = [];
    protected $table = 'v_qris';
    public $timestamps = false;
    public $incrementing = false;

/* 
    public function DataBilling()
    {
        return $this->hasOne('App\Models\Data_billing', 'data_billing_id', 'data_billing_id');
    } */
}
