<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Data_billing extends Model
{
    use SoftDeletes;
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'data_billing';
    public $timestamps = false;
    protected $primaryKey = 'data_billing_id';
    protected $dates = ['deleted_at', 'expired_at'];
    protected $appends = ['IsExpired', 'QrisValue', 'JenisBilling', 'PokokNominal', 'DendaNominal', 'TotalNominal', 'PaymentStatus'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $tglNow = Carbon::now();
            $montNow = $tglNow->month;
            $next = $tglNow->addDays(1);
            if ($montNow != $next->month) {
                $next = Carbon::now()->endOfMonth();
            }
            $item->created_at = Carbon::now();
            $item->created_by = Auth()->user()->id ?? 1901;
            $item->expired_at = $next;
        });
    }

    public function Qris()
    {
        return $this->hasOne('App\Models\Qris', 'data_billing_id', 'data_billing_id');
    }

    public function VirtualAccount()
    {
        return $this->hasOne('App\Models\VirtualAccount', 'data_billing_id', 'data_billing_id');
    }
	

    public function billing_kolektif()
    {
        return $this->hasMany('App\Models\Billing_kolektif', 'data_billing_id', 'data_billing_id');
    }

    public function getQrisValueAttribute()
    {
        return $this->qris->qr_value ?? '';
    }

    public function getPaymentStatusAttribute()
    {
        // Periksa apakah transaksi sudah expired
        if ($this->expired_at && Carbon::now()->greaterThan($this->expired_at)) {
            return 'Expired'; // Transaksi kedaluwarsa
        }

        // Periksa nilai dari kd_status
        if ($this->kd_status == 1) {
            return 'Lunas'; // Jika kd_status = 1
        } else {
            return 'Belum Lunas'; // Jika kd_status = 0
        }
    }

    public function getPokokNominalAttribute()
    {
        return $this->billing_kolektif()->sum('pokok');
    }

    public function getDendaNominalAttribute()
    {
        return $this->billing_kolektif()->sum('denda');
    }

    public function getTotalNominalAttribute()
    {
        return $this->billing_kolektif()->sum('total');
    }

    public function getIsExpiredAttribute()
    {
        return $this->expired_at && $this->expired_at->isPast(); // Jika 'expired_at' sudah lewat
    }

    public function getJenisBillingAttribute()
    {
        $jns = $this->kd_jns_op;
        switch ($jns) {
            case '1':
                # code...
                $jenis = 'Desa';
                break;
            case '2':
                # code...
                $jenis = 'Pendataan';
                break;
            case '3':
                # code...
                $jenis = 'Penelitian';
                break;
            case '4':
                # code...
                $jenis = 'Pengaktifan';
                break;
            case '5':
                # code...
                $jenis = 'STPD';
                break;
            case '6':
                # code...
                $jenis = 'Virtual Account';
                break;
            default:
                # code...
                $jenis = 'Qris';
                break;
        }

        return $jenis;
        // return $this->expired_at && $this->expired_at->isPast(); // Jika 'expired_at' sudah lewat
    }
}

/* 
    kd_jns_Op
    1 => desa
    2 => pendataan
    3 => Penelitian
    4 => pengaktifan
    5 => STPD
	6 => Virtual Account
	7 => Qris
*/