<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpbLimasBelas extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb15';
    public $timestamps = false;
}
