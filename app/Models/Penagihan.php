<?php

namespace App\Models;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Penagihan extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'penagihan';
    public $timestamps = false;
    protected $primaryKey = 'penagihan_id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $now=Carbon::now();
            $userId=Auth::user()->id;
            $item->penagihan_id = Uuid::uuid1()->toString();
            $item->created_at = $now;
            // $item->created_by = $userId;
            // $item->updated_by = $userId;
            $item->updated_at = $now;
        });

    }
}