<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penagihan_piutang extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'penagihan_piutang';
    public $timestamps = false;
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            
        });

    }
}
