<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Layanan extends Model
{
    use SoftDeletes;
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'layanan';
    public $timestamps = false;
    protected $primaryKey = 'nomor_layanan';
    protected $dates = ['deleted_at'];
    public $incrementing = false;
    protected $casts = [
        'nomor_layanan' => 'string',
    ];

    protected $fillable = [
        'nomor_layanan',
        'jenis_layanan_id',
        'nik',
        'nama',
        'alamat',
        'kelurahan',
        'kecamatan',
        'dati2',
        'propinsi',
        'nomor_telepon',
        'keterangan',
        'jenis_objek',
        'jenis_layanan_nama',
        'tanggal_layanan',
        'created_at',
        'kd_status',
        'verifikasi_by',
        'updated_by',
        'created_by',
        'updated_at',
        'is_online'
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            if (Auth()->user()) {
                $userid = Auth()->user()->id;
            } else {
                $user = User::whereraw("lower(nama) like '%admin%'")->first();
                $userid = $user->id;
            }

            $dateNow = Carbon::now();
            $item->tanggal_layanan = $dateNow;
            $item->created_at = $dateNow;
            if ($item->kd_status == '') {
                $item->kd_status = '0';
            }
            $item->verifikasi_by = $userid;
            $item->created_by = $userid;
            $item->updated_by = $userid;
            $item->updated_at = $dateNow;
        });
    }


    public function layanan_objek()
    {
        // 'App\Models\Layanan_objek'
        return $this->hasOne(layanan_objek::class, 'nomor_layanan', 'nomor_layanan');
    }


    public function layanan_dokumen()
    {
        return $this->hasMany('App\Models\Layanan_dokumen', 'nomor_layanan', 'nomor_layanan');
    }

    public function objek()
    {
        return $this->hasMany(layanan_objek::class, 'nomor_layanan', 'nomor_layanan');
    }
}
