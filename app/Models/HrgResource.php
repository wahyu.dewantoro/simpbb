<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HrgResource extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'hrg_resource';
    public $timestamps = false;
}
