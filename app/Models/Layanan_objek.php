<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Layanan_objek extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'layanan_objek';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $casts = [
        'nomor_layanan' => 'string',
    ];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->created_at = Carbon::now();
        });
    }



    public function layanan()
    {
        return $this->hasOne('App\Models\Layanan', 'nomor_layanan', 'nomor_layanan');
    }

    public function jpb()
    {
        // return $this->hasMany('App\KelompokObjek',  'kelompok_objek_id','id');
        return $this->BelongsTo('App\KelompokObjek',  'kelompok_objek_id', 'id');
    }

    public function lokasi()
    {
        // return $this->hasOne('App\LokasiObjek', 'id', 'lokasi_objek_id');
        // return $this->hasMany('App\LokasiObjek', 'lokasi_objek_id', 'id');
        return $this->belongsTo('App\LokasiObjek', 'lokasi_objek_id', 'id');
    }

    public function pendukung()
    {
        return $this->hasMany('App\Models\Layanan_dokumen', 'keyid', 'id')
            ->where('tablename', 'LAYANAN_OBJEK');
    }


    public function Notaperhitungan()
    {
        return $this->hasOne(Notaperhitungan::class, 'layanan_objek_id', 'id')->orderby('data_billing_id', 'desc');
    }
}
