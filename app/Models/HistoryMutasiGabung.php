<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class HistoryMutasiGabung extends Model
{
    //
    protected $guarded = [];
    protected $table = 'history_mutasi_gabung';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->id = Uuid::uuid1()->toString();
        });
    }
}
