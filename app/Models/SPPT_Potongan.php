<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SPPT_Potongan extends Model
{
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    protected $table = 'sppt_potongan';
    public $timestamps = false;

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'thn_pajak_sppt',
        'jns_potongan',
        'nilai_potongan',
        'keterangan',
        'created_at',
        'created_by'
    ];
}
