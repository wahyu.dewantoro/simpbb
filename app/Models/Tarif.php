<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tarif extends Model
{
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    protected $table = 'tarif';
    public $timestamps = false;

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'thn_awal',
        'thn_akhir',
        'njop_min',
        'njop_max',
        'nilai_tarif'
    ];
}
