<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratTagihanNop extends Model
{
    //
    protected $guarded = [];
    protected $table = 'surat_tagihan_nop';
    public $timestamps = false;
}
