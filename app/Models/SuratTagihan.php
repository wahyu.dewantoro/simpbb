<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SuratTagihan extends Model
{
    //
    protected $guarded = [];
    protected $table = 'surat_tagihan';
    public $timestamps = false;


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $now = Carbon::now();
            $item->created_at = $now;
            $item->created_by = Auth()->user()->id;
        });
    }
}
