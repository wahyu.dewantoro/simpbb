<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MsPotonganScope extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    protected $table = 'ms_potongan_kebijakan_scope';
    public $timestamps = false;
    public $incrementing = false;
}
