<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spop extends Model
{

    // table spop di sim_pbb
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'tbl_spop';
    public $timestamps = false;
    protected $primaryKey = ['no_formulir'];

    public function jenisLayanan()
    {
        return $this->hasOne('App\Models\Jenis_layanan', 'id', 'jenis_layanan_id');
    }

    public function layananObjek()
    {
        return $this->hasOne('App\Models\Layanan_objek', 'nomor_formulir', 'no_formulir');
        // return $this->hasOne('App\Models\Layanan_objek', 'nomor_formulir', 'no_formulir');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function pendataan_objek()
    {
        return $this->hasOne('App\Models\PendataanObjek', 'layanan_objek_id', 'id');
    }
}
