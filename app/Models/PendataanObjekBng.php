<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PendataanObjekBng extends Model
{
    //
    protected $guarded = [];
    protected $table = 'pendataan_objek_bng';
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            // $item->created_at = Carbon::now();
            $now = Carbon::now();
            $user_id = Auth()->user()->id;

            $item->created_at = $now;
            $item->created_by = $user_id;
            $item->updated_at = $now;
            $item->updated_by = $user_id;
        });
    }
}
