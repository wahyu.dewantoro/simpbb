<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class kompensasiLebihBayar extends Model
{
    use SoftDeletes;
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'kompensasi_lebih_bayar';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->created_at =Carbon::now()->format('Y-m-d H:i:s');
            $item->created_by = Auth()->user()->id;
        });
    }
}
