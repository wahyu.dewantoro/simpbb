<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpbEmpat extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb4';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati4', 'thn_dbkb_jpb4', 'kls_dbkb_jp4', 'lantai_min_jpb4', 'lantai_max_jpb4'];
}
