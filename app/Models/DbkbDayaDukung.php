<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbDayaDukung extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_daya_dukung';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'thn_dbkb_daya_dukung', 'type_konstruksi'];
}
