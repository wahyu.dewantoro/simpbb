<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class StpPegawai extends Model
{
    //
    protected $guarded = [];
    protected $table = 'srt_tgs_pendataan_pegawai';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->id = Uuid::uuid1()->toString();
        });
    }
}
