<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jenis_layanan extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'jenis_layanan';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
