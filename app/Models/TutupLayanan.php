<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TutupLayanan extends Model
{
    protected $guarded = [];
    protected $table = 'tutup_layanan';
    public $timestamps = false;
}
