<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpb7 extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb7';
    public $timestamps = false;
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati6',
        'thn_dbkb_jpb6',
        'jns_dpkb_jpb7',
        'bintang_dbkb_jpb7',
        'lantai_min_jpb7',
        'lantai_max_jpb7'
    ];
}
