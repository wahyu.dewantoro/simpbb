<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpbEnamBelas extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb16';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'thn_dbkb_jpb16', 'kls_dbkb_jpb16', 'lantai_min_jpb16', 'lantai_max_jpb16'];
}
