<?php

namespace App\Models;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SuratTugasObjek extends Model
{
    //
    protected $guarded = [];
    protected $table = 'surat_tugas_objek';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->id = Uuid::uuid1()->toString();
            $item->created_at = Carbon::now();
            $item->created_by = Auth::user()->id;
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });


        static::updating(function ($item) {
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });
    }
}
