<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Layanan_objek_temp extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'layanan_objek_temp';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
