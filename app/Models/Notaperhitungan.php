<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class Notaperhitungan extends Model
{
    //
    use AutoNumberTrait;
    protected $guarded = [];
    protected $table = 'nota_perhitungan';

    public function getAutoNumberOptions()
    {
        return [
            // 001/nota-03/V/2022
            'nomor' => [
                'format' =>  '?' . '/nota-01/' . romawi((int)date('m')) . '/' . date('Y'), // Format kode yang akan digunakan.
                'length' => 5 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }

    public function billing()
    {
        return $this->hasOne('App\Models\Data_billing', 'kobil', 'kobil');
    }

    public function DataBilling()
    {
        return $this->hasOne(Data_billing::class, 'data_billing_id', 'data_billing_id');
    }
}
