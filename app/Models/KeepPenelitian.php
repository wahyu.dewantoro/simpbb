<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeepPenelitian extends Model
{
    //
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'keep_penelitian';
    public $timestamps = false;
    public $incrementing = false;
    // protected $primaryKey = 'id';


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'peneliti_by');
    }
}
