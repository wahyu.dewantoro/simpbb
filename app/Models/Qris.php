<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Qris extends Model
{
    //
    protected $guarded = [];
    protected $table = 'qris';
    protected $primaryKey = 'bill_number';
    public $incrementing = false;
    protected $keyType = 'string';
}
