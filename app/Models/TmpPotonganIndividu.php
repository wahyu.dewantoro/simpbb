<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmpPotonganIndividu extends Model
{

    protected $guarded = [];
    protected $table = 'potongan_individu_tmp';
    public $timestamps = false;
}
