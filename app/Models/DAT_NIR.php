<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class DAT_NIR extends Model
{
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    protected $table = 'dat_nir';
    public $timestamps = false;

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_znt',
        'thn_nir_znt',
        'kd_kanwil',
        'kd_kantor',
        'jns_dokumen',
        'no_dokumen',
        'nir',
    ];
}
