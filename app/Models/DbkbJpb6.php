<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpb6 extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb6';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati6', 'thn_dbkb_jpb6', 'kls_dbkb_jp6'];
}
