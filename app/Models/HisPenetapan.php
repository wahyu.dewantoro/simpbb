<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HisPenetapan extends Model
{
    protected $table = 'his_penetapan';
    protected $guarded = [];
}
