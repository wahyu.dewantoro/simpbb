<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpb3 extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb3';
    public $timestamps = false;
    protected $primaryKey = [
        'KD_PROPINSI',
        'KD_DATI2',
        'THN_DBKB_JPB3',
        'LBR_BENT_MIN_DBKB_JPB3',
        'LBR_BENT_MAX_DBKB_JPB3',
        'TING_KOLOM_MIN_DBKB_JPB3',
        'TING_KOLOM_MAX_DBKB_JPB3'
    ];
}
