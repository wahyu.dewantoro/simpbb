<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatPetaBlok extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dat_peta_blok';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok'];
}
