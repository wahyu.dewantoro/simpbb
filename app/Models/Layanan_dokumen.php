<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Layanan_dokumen extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'layanan_dokumen';
    public $timestamps = false;
    protected $primaryKey = 'id';
    
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $item->created_at = Carbon::now();
        });
    }
}
