<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PBBMinimal extends Model
{
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    protected $table = 'pbb_minimal';
    public $timestamps = false;

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        'kd_propinsi',
        'kd_dati2',
        'thn_pbb_minimal',
        'no_sk_pbb_minimal',
        'tgl_sk_pbb_minimal',
        'nilai_pbb_minimal',
        'tgl_rekam_pbb_minimal',
        'nip_perekam_pbb_minimal'
    ];
}
