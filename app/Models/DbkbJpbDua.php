<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpbDua extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb2';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'thn_dbkb_jpb2', 'kls_dbkb_jp2', 'lantai_min_jpb2', 'lantai_max_jpb2'];
}
