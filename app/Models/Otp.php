<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    // table spop di sim_pbb
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'otp';
    public $timestamps = false;
    protected $primaryKey = ['kode'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            // $item->id = Uuid::uuid1()->toString();
            $item->kode = randomString(4);
            $item->expired_at = Carbon::now()->addMinutes(2);
        });
    }
}
