<?php

namespace App\Models;

use Alfa6661\AutoNumber\AutoNumberTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PendataanBatch extends Model
{
    use AutoNumberTrait;

    //
    protected $guarded = [];
    protected $table = 'pendataan_batch';
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {

            $now = Carbon::now();
            $user_id = Auth()->user()->id ?? '1700';

            $item->created_at = $now;
            $item->created_by = $user_id;
            $item->updated_at = $now;
            $item->updated_by = $user_id;
        });
    }

    public function getAutoNumberOptions()
    {
        return [
            'nomor_batch' => [
                // 'format' => 'IDV' . '?',
                'format' => function () {
                    // return 'SO/' . date('Y.m.d') . '/' . $this->branch . '/?'; 
                    // return $this->tipe.date('ym').'?';
                    return date('ym') . '?';
                },
                'length' => 6 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]

            // return 'SO/' . date('Y.m.d') . '/' . $this->branch . '/?'; 
        ];
    }

    public function scopeListPendataan($sql, $id = null)
    {
        $sql = $sql->selectraw("pendataan_batch.id,nama_pendataan,nomor_batch,to_char(pendataan_batch.created_at,'yyyy-mm-dd') tanggal, verifikator.nama verifikasi_pegawai,verifikasi_at,verifikasi_deskripsi,verifikasi_kode,users.nama pegawai_pendata,count(1) objek,jenis_pendataan_id")
            ->join('jenis_pendataan', 'jenis_pendataan.id', '=', 'pendataan_batch.jenis_pendataan_id')
            ->join('pendataan_objek', 'pendataan_objek.pendataan_batch_id', '=', 'pendataan_batch.id')
            ->leftjoin('users', 'users.id', '=', 'pendataan_batch.created_by')
            ->leftjoin(db::raw('users verifikator'), 'verifikator.id', '=', 'pendataan_batch.verifikasi_by');
        if ($id <> '') {
            $sql = $sql->whereraw("pendataan_batch.id='$id'");
        }
        $sql = $sql->groupby(DB::raw("pendataan_batch.id,nama_pendataan,nomor_batch,to_char(pendataan_batch.created_at,'yyyy-mm-dd') , verifikasi_kode,verifikasi_at,verifikator.nama,users.nama,verifikasi_by,verifikasi_at,verifikasi_deskripsi,jenis_pendataan_id"));
    }
}
