<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SpptBkp extends Model
{
    protected $guarded = [];
    protected $table = 'sppt_bkp';
    
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $now = Carbon::now();
            $item->created_at = $now;
            $item->updated_at = $now;
        });
    }
}
