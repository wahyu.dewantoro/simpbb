<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Billing_kolektif extends Model
{
    use SoftDeletes;
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'billing_kolektif';
    public $timestamps = false;
    protected $primaryKey = 'billing_kolektif_id';
    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            $tglNow=Carbon::now();
            $montNow=$tglNow->month;
            $next=$tglNow->addDays(2);
            if($montNow!=$next->month){
                $next=Carbon::now()->endOfMonth();
            }
            $item->expired_at =$next;
        });
    }

    public function data_billing()
    {
        return $this->belongsTo('App\Models\Data_billing', 'data_billing_id', 'data_billing_id');
    }
}
