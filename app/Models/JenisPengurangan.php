<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class JenisPengurangan extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'jenis_pengurangan';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_pengurangan',
        'jenis',
        'pengurangan'
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            
        });
    }
}
