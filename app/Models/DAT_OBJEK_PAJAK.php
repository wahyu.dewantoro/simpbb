<?php

namespace App\Models;

use App\Kelurahan;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DAT_OBJEK_PAJAK extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $table = 'dat_Objek_pajak';
    // protected $primaryKey = 'port_id';
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op'
    ];

    public $incrementing = false;
    protected $fillable = [];
    protected $append = [
        'NmWp',
        'NmKecamatan',
        'NmKelurahan'
    ];

    // Kecamatan

    public function Kecamatan()
    {
        return $this->hasOne('App\Kecamatan', 'kd_kecamatan', 'kd_kecamatan');
    }

    public function Kelurahan()
    {

        // return $this->kd_kelurahan;
        return $this->hasOne('App\Kelurahan', 'kd_kecamatan', 'kd_kecamatan')
            ->where('ref_kelurahan.kd_kelurahan', $this->kd_kelurahan);
    }



    public function getNmKecamatanAttribute()
    {
        return $this->Kecamatan->nm_kecamatan ?? '';
    }

    public function getNmKelurahanAttribute()
    {
        // return $this->Kelurahan->nm_kelurahan ?? '';
        return $this->Kelurahan->nm_kelurahan ?? '';
    }

    public function DatSubjekPajak()
    {
        return $this->hasOne('App\Models\DAT_SUBJEK_PAJAK', 'subjek_pajak_id', 'subjek_pajak_id');
    }


    public function getNmWpAttribute()
    {
        return $this->DatSubjekPajak->nm_wp ?? "";
    }

    public function DatOpBumi()
    {
        // App\Models\DatOpBumi
        return $this->HasOne(DatOpBumi::class, 'kd_propinsi', 'kd_propinsi')
            ->where("dat_op_bumi.kd_dati2", $this->kd_dati2)
            ->where("dat_op_bumi.kd_kecamatan", $this->kd_kecamatan)
            ->where("dat_op_bumi.kd_kelurahan", $this->kd_kelurahan)
            ->where("dat_op_bumi.kd_blok", $this->kd_blok)
            ->where("dat_op_bumi.no_urut", $this->no_urut)
            ->where("dat_op_bumi.kd_jns_op", $this->kd_jns_op);
    }
}
