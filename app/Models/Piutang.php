<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Piutang extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'piutang';
    public $timestamps = false;
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            
        });

    }
}
