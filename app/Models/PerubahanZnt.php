<?php

namespace App\Models;

use Alfa6661\AutoNumber\AutoNumberTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PerubahanZnt extends Model
{
    //
    use AutoNumberTrait;

    protected $table = 'perubahan_znt';
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {

            $now = Carbon::now();
            $user_id = Auth()->user()->id??'1678';

            $item->created_at = $now;
            $item->created_by = $user_id;
        });
    }

    public function getAutoNumberOptions()
    {
        return [
            'nomor_batch' => [
                'format' => 'ZNT' . '?',
                'length' =>  5 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }

    public function objek()
    {
        return $this->hasMany('App\Models\PerubahanZntObjek', 'perubahan_znt_id', 'id');
    }
}
