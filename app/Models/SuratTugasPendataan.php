<?php

namespace App\Models;

use Alfa6661\AutoNumber\AutoNumberTrait;
use App\pegawaiStruktural;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class SuratTugasPendataan extends Model
{
    protected $guarded = [];
    protected $table = 'srt_tgs_pendataan';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    use AutoNumberTrait;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $peg = pegawaiStruktural::where('kode', '03')->first();

            $now = Carbon::now();
            $userid = Auth()->user()->id;
            $item->id = Uuid::uuid1()->toString();
            $item->surat_kota = 'KEPANJEN';
            $item->surat_pegawai = $peg->nama;
            $item->surat_nip = $peg->nip;
            $item->surat_jabatan = $peg->jabatan;
            $item->surat_pangkat = 'PEMBINA';
            $item->surat_tanggal = $now;

            $item->created_at = $now;
            $item->created_by = $userid;
            $item->updated_by = $userid;
            $item->updated_at = $now;
        });


        static::updating(function ($item) {
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });
    }

    public function getAutoNumberOptions()
    {
        return [
            'surat_nomor' => [
                'format' => '094/?/35.07.205/' . date('Y'),
                'length' => 5 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }

    public function objek()
    {
        return $this->hasMany('App\Models\StpObjek', 'srt_tgs_pendataan_id', 'id');
    }

    public function pegawai()
    {
        return $this->hasMany('App\Models\StpPegawai', 'srt_tgs_pendataan_id', 'id');
    }
}
