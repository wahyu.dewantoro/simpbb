<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendataanObjekPenetapan extends Model
{
    //
    protected $table = 'pendataan_objek_penetapan';
    public $timestamps = false;
    protected $guarded = [];
}
