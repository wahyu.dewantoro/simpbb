<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DAT_SUBJEK_PAJAK extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $table = 'dat_subjek_pajak';
    protected $primaryKey = 'port_id';
    public $incrementing = false;
    protected $fillable = [];

    public function objek_pajak()
    {
        return $this->hasMany(DAT_OBJEK_PAJAK::class, 'subjek_pajak_id', 'subjek_pajak_id');
    }
}
