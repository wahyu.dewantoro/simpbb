<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerubahanZntObjek extends Model
{
    //
    protected $guarded = [];
    protected $table = 'perubahan_znt_objek';
    public $timestamps = false;
}
