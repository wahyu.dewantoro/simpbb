<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpb5 extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb5';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati5', 'thn_dbkb_jpb5', 'kls_dbkb_jp5', 'lantai_min_jpb5', 'lantai_max_jpb5'];
}
