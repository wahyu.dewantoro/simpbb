<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HrgSatuanKegiatan extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'hrg_satuan';
    public $timestamps = false;
}
