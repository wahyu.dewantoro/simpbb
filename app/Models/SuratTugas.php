<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Alfa6661\AutoNumber\AutoNumberTrait;

class SuratTugas extends Model
{
    //
    protected $guarded = [];
    protected $table = 'surat_tugas';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
    use AutoNumberTrait;


    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->id = Uuid::uuid1()->toString();
            $item->created_at = Carbon::now();
            $item->created_by = Auth::user()->id;
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });


        static::updating(function ($item) {
            $item->updated_by = Auth::user()->id;
            $item->updated_at = Carbon::now();
        });
    }

    public function getAutoNumberOptions()
    {
        return [
            'batch' => [
                'format' => date('Ymd').'?', // Format kode yang akan digunakan.
                'length' => 3 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }
    public function objek()
    {
        return $this->hasMany('App\Models\SuratTugasObjek', 'surat_tugas_id', 'id')->whereraw('surat_tugas_objek.deleted_at is null');
    }
    
    public function pegawai()
    {
        return $this->hasMany('App\Models\SuratTugasPegawai', 'surat_tugas_id', 'id')->whereraw('surat_tugas_pegawai.deleted_at is null');
    }
}
