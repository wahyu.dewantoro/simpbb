<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class pengaktifanObjek extends Model
{
    //

    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    protected $table = 'pengaktifan_objek';
    public $timestamps = false;

    protected $primaryKey = null;
    public $incrementing = false;

    // public $append = ['NamaPetugas'];

    // public function Users()
    // {
    //     return $this->hasOne('App\user', 'id', 'created_by');
    // }


    // public function getNamaPetugasAttribute()
    // {
    //     // return $this->Users->nama ?? '-';
    //     return '';
    // }
}
