<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenagihanDetail extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'penagihan_detail';
    public $timestamps = false;
    protected $primaryKey = 'penagihan_detail_id';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            
        });

    }
}
