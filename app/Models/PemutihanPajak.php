<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemutihanPajak extends Model
{
    //
    protected $connection = 'oracle_spo';
    protected $guarded = [];
    protected $table = 'pemutihan_pajak';
    public $timestamps = false;
    protected $primaryKey = 'id';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($item) {
            $item->status = 1;
        });
    }
}
