<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PendataanObjek extends Model
{
    //

    protected $table = 'pendataan_objek';
    public $timestamps = false;
    protected $guarded = [];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            // $item->created_at = Carbon::now();
            $now = Carbon::now();
            $user_id = Auth()->user()->id??'1700';

            $item->created_at = $now;
            $item->created_by = $user_id;
            $item->updated_at = $now;
            $item->updated_by = $user_id;
        });
    }

    public function bng()
    {
        return $this->hasMany('App\Models\PendataanObjekBng', 'pendataan_objek_id', 'id');
    }

    public function spop()
    {
        return $this->hasOne('App\Models\Spop', 'pendataan_objek_id', 'id');
    }

    public function batch()
    {
        return $this->hasOne('App\Models\PendataanBatch', 'id', 'pendataan_batch_id');
    }

    public function penetapan()
    {
        return $this->hasMany('App\Models\PendataanObjekPenetapan', 'pendataan_objek_id', 'id');
    }
}
