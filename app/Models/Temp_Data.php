<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temp_Data extends Model
{
    protected $connection = 'oracle';
    protected $guarded = [];
    protected $table = 'temp_data';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
