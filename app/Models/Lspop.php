<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lspop extends Model
{
    //
    // table spop di sim_pbb
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'tbl_lspop';
    public $timestamps = false;
    protected $primaryKey = ['no_formulir'];



    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
