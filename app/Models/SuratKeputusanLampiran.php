<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratKeputusanLampiran extends Model
{
    //
    protected $guarded = [];
    protected $table = 'surat_keputusan_lampiran';
    public $timestamps = false;
}

