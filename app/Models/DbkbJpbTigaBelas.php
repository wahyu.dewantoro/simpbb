<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpbTigaBelas extends Model
{
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb13';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'thn_dbkb_jpb12', 'kls_dbkb_jpb12', 'lantai_min_jpb12', 'lantai_max_jpb12'];
}
