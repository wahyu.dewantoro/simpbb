<?php

namespace App\Models;

use Alfa6661\AutoNumber\AutoNumberTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class SuratKeputusan extends Model
{
    use AutoNumberTrait;
    protected $guarded = [];
    protected $table = 'surat_keputusan';
    public $timestamps = false;


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($item) {
            /* $now = Carbon::now();
            $item->tanggal = $now; */
        });
    }

    public function getAutoNumberOptions()
    {
        $urut = '?';

        // Nomor : 900.1.13.1/            /Pengurangan.PBB/35.07.404/2023
        // 'format' => '973/' . $urut . '/' . $this->jenis . '/35.07.205/' . date('Y'),
        $df = '900.1.13.1';
        if ($this->jenis == 'Keberatan.PBB' || $this->jenis == 'Pembetulan.PBB' || $this->jenis == 'Pendaftaran Obyek Baru.PBB') {
            $df = '973';
        }

        return [
            'nomor' => [
                'format' => $df . '/' . $urut . '/' . $this->jenis . '/35.07.404/' . date('Y'),
                'length' => 5 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }


    public function Hasil()
    {
        return $this->hasOne('App\Models\SuratKeputusanLampiran', 'surat_keputusan_id', 'id')->wherenull('is_last');
    }

    public function last()
    {
        return $this->hasMany('App\Models\SuratKeputusanLampiran', 'surat_keputusan_id', 'id')->wherenotnull('is_last');
    }

    public function TblSpop()
    {
        return $this->hasOne('App\Models\Spop', 'layanan_objek_id', 'layanan_objek_id')->orderbyraw('tbl_spop.jenis_layanan_id ,tbl_spop.created_at desc');
    }

    public function LayananObjek()
    {
        return $this->hasOne('App\Models\Layanan_objek', 'id', 'layanan_objek_id');
    }
}
