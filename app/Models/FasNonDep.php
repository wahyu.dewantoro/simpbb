<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FasNonDep extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'FAS_NON_DEP';
    public $timestamps = false;
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'thn_nop_dep', 'kd_fasilitas'];
}
