<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbkbJpbEmpatBelas extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dbkb_jpb14';
    public $timestamps = false;
}
