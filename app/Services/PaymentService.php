<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class PaymentService
{

    protected $username;
    protected $password;
    protected $endpoint;

    public function __construct()
    {

        $this->username = 'usertest';
        $this->password = 'asdasf';
        $this->endpoint = 'http://192.168.1.215/pembayaran-pbb/api/inquiry';
    }

    public function Inquiry($data)
    {
        // Mengirim POST request dengan Basic Auth
        $response = Http::withBasicAuth($this->username, $this->password)
            ->post($this->endpoint, $data);

        // Mengecek apakah responsnya sukses
        if ($response->successful()) {
            // Mengembalikan data JSON dari API
            $data = $response->json();
            $is_error = false;
        } else {
            // $msg = 'Gagal mengirim data ke API.';
            $is_error = true;
            $data = [];
        }

        // Jika terjadi kesalahan, kembalikan error atau log
        return [
            'status_code' => $response->status(),
            'is_error' => $is_error,
            'data' => $data

        ];
    }
}
