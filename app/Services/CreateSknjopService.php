<?php

namespace App\Services;

use App\Helpers\Layanan_conf;
use App\Jobs\BatchSknjop;
use App\pegawaiStruktural;
use App\SkNJop;
use App\sknjoppermohonan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CreateSknjopService
{
    public function handle($param)
    {
        // $nomor_layanan = Layanan_conf::nomor_layanan();
        // $peg = pegawaiStruktural::where('kode', '02')->first();
        // return $param;

        $permohonan = $param['permohonan'];
        $data = $param['data'];
        DB::beginTransaction();
        try {
            $core = sknjoppermohonan::create($permohonan);
            $data['sknjop_permohonan_id'] = $core->id;
            $sk = SkNJop::create($data);
            DB::commit();
            dispatch(new BatchSknjop($core->id))->onQueue('sknjop');
            $pesan = [
                'type' => 'success',
                'desc' => 'Permohonan SK NJOP telah di submit',
                'url' => url('sknjop') . '/' . $core->id,
                'file' => url('sknjop-pdf') . '/' . $sk->id
            ];
        } catch (\Exception $th) {
            DB::rollback();
            Log::alert($th);
            // $pesan = ['error' => $th->getMessage()];
            $pesan = [
                'type' => 'error',
                'desc' =>  $th->getMessage(),
                'url' => url('sknjop')
            ];
        }

        return $pesan;
    }
}
