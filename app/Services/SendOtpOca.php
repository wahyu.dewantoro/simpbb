<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class SendOtpOca
{
    protected $apiKey;
    protected $apiUrl;
    protected $template_id;

    public function __construct()
    {
        $this->apiKey = config('oca.oca_api_key'); // Simpan API key di file .env
        $this->apiUrl = config('oca.oca_url'); // URL endpoint dari OCA
        $this->template_id = config('oca.template_otp');
    }

    public function sendMessage($phoneNumber, $otp)
    {
        $phoneNumber = NomorWa($phoneNumber);
        $postOtp = [
            "phone_number" => $phoneNumber,
            "message" => [
                "type" => "template",
                "template" => [
                    "template_code_id" => $this->template_id,
                    "payload" => [
                        [
                            "position" => "body",
                            "parameters" => [
                                [
                                    "type" => "text",
                                    "text" => $otp
                                ]
                            ]
                        ],
                        [
                            "position" => "button",
                            "parameters" => [
                                [
                                    "sub_type" => "url",
                                    "index" => "0",
                                    "parameters" => [
                                        [
                                            "type" => "text",
                                            "text" => $otp
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];



        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $this->apiKey,
            'Content-Type' => 'application/json',
        ])->withOptions([
                    'verify' => false  // Nonaktifkan verifikasi SSL sementara
                ])
            ->post($this->apiUrl, $postOtp);

        return $response->json();
    }
}
