<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class RiwayatPembayaranService
{
    public function getRiwayatPembayaran($params)
    {

        $data = DB::connection('oracle_satutujuh')
            ->table('sppt')
            ->selectRaw('
        sppt.thn_pajak_sppt,
        TRUNC(sppt.tgl_terbit_sppt) AS tgl_terbit_sppt,
        sppt.pbb_yg_harus_dibayar_sppt - NVL(sppt_potongan.nilai_potongan, 0) AS pbb,
        jml_sppt_yg_dibayar - denda_sppt AS pokok,
        denda_sppt AS denda,
        jml_sppt_yg_dibayar AS jumlah,
        TRUNC(tgl_pembayaran_sppt) AS tgl_bayar,
        pembayaran_sppt.pengesahan,
        ROW_NUMBER() OVER (PARTITION BY sppt.thn_pajak_sppt ORDER BY tgl_pembayaran_sppt) AS pembayaran_ke,
        sppt_koreksi.no_transaksi AS no_koreksi, 
        sppt_koreksi.keterangan AS keterangan_koreksi,
        pch.nop_proses AS nop_pecahan
    ')
            ->leftJoin('pembayaran_sppt', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
                    ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
            })
            ->leftJoin('sppt_potongan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->leftJoin('sppt_koreksi', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            })
            ->leftJoin(DB::raw('(SELECT 
            a.pengesahan, 
            a.nop_proses, 
            a.nop_asal
        FROM sim_pbb.data_billing a
        JOIN sim_pbb.nota_perhitungan b ON b.data_billing_id = a.data_billing_id
        JOIN sim_pbb.layanan_objek c ON c.id = b.LAYANAN_OBJEK_ID
        JOIN sim_pbb.tbl_spop d ON d.no_formulir = c.nomor_formulir
        ) PCH'), function ($join) {
                $join->on('pch.pengesahan', '=', 'pembayaran_sppt.pengesahan')
                    ->on('pch.nop_asal', '=', DB::raw("sppt.kd_propinsi || sppt.kd_dati2 || sppt.kd_kecamatan || 
                    sppt.kd_kelurahan || sppt.kd_blok || sppt.no_urut || sppt.kd_jns_op"));
            })
            ->where('sppt.kd_propinsi', $params['kd_propinsi'])
            ->where('sppt.kd_dati2', $params['kd_dati2'])
            ->where('sppt.kd_kecamatan', $params['kd_kecamatan'])
            ->where('sppt.kd_kelurahan', $params['kd_kelurahan'])
            ->where('sppt.kd_blok', $params['kd_blok'])
            ->where('sppt.no_urut', $params['no_urut'])
            ->where('sppt.kd_jns_op', $params['kd_jns_op'])
            ->orderBy('sppt.thn_pajak_sppt')
            ->orderBy('tgl_pembayaran_sppt')
            ->orderBy('tgl_rekam_byr_sppt')
            ->get();

        // dd($data); // Debug hasil query

        return $data;
        /*  return DB::connection('oracle_satutujuh')
            ->table('sppt')
            ->selectRaw('
                sppt.thn_pajak_sppt,
                TRUNC(sppt.tgl_terbit_sppt) AS tgl_terbit_sppt,
                sppt.pbb_yg_harus_dibayar_sppt - NVL(sppt_potongan.nilai_potongan, 0) AS pbb,
                jml_sppt_yg_dibayar - denda_sppt AS pokok,
                denda_sppt AS denda,
                jml_sppt_yg_dibayar AS jumlah,
                TRUNC(tgl_pembayaran_sppt) AS tgl_bayar,
                pembayaran_sppt.pengesahan,
                ROW_NUMBER() OVER (PARTITION BY sppt.thn_pajak_sppt ORDER BY tgl_pembayaran_sppt) AS pembayaran_ke,
                sppt_koreksi.no_transaksi AS no_koreksi, 
                sppt_koreksi.keterangan AS keterangan_koreksi,
                pch.nop_proses AS nop_pecahan
            ')
            ->leftJoin('pembayaran_sppt', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
                    ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
            })
            ->leftJoin('sppt_potongan', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
            })
            ->leftJoin('sppt_koreksi', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            })
            ->leftJoin(DB::raw('(SELECT 
                    a.pengesahan, 
                    a.nop_proses, 
                    a.nop_asal
                FROM sim_pbb.data_billing a
                JOIN sim_pbb.nota_perhitungan b ON b.data_billing_id = a.data_billing_id
                JOIN sim_pbb.layanan_objek c ON c.id = b.LAYANAN_OBJEK_ID
                JOIN sim_pbb.tbl_spop d ON d.no_formulir = c.nomor_formulir
                ) pch'), function ($join) {
                $join->on('pch.pengesahan', '=', 'pembayaran_sppt.pengesahan')
                    ->on('pch.nop_asal', '=', DB::raw("sppt.kd_propinsi || sppt.kd_dati2 || sppt.kd_kecamatan || 
                            sppt.kd_kelurahan || sppt.kd_blok || sppt.no_urut || sppt.kd_jns_op"));
            })
            ->where('sppt.kd_propinsi', $params['kd_propinsi'])
            ->where('sppt.kd_dati2', $params['kd_dati2'])
            ->where('sppt.kd_kecamatan', $params['kd_kecamatan'])
            ->where('sppt.kd_kelurahan', $params['kd_kelurahan'])
            ->where('sppt.kd_blok', $params['kd_blok'])
            ->where('sppt.no_urut', $params['no_urut'])
            ->where('sppt.kd_jns_op', $params['kd_jns_op'])
            ->orderBy('sppt.thn_pajak_sppt')
            ->orderBy('tgl_pembayaran_sppt')
            ->orderBy('tgl_rekam_byr_sppt')
            ->get(); */
    }
}
