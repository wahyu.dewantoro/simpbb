<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class simBphtbService
{
    protected $baseUri;
    protected $username;
    protected $password;

    public function __construct()
    {
        $this->baseUri = config('simBphtb.base_url');
        $this->username = config('simBphtb.username');
        $this->password = config('simBphtb.password');
    }

    public function sspdFetchData(string $startDate, string $endDate)
    {
        $url = $this->baseUri . '/api/sspd-bphtb';
        $param = [
            'start_date' => $startDate,
            'end_date' => $endDate,
        ];
        $cacheKey = 'sspd_bphtb_response_' . md5($url . json_encode($param));

        $username = $this->username;
        $password = $this->password;
        try {
            $response = Http::withBasicAuth($username, $password)
                ->post($url, $param);

            return $response->json();
        } catch (\Exception $e) {
            // Tangani error
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function sspdShowData(string $noreg)
    {
        $url = $this->baseUri . '/api/sspd-bphtb/' . $noreg;
        // return $url;
        $cacheKey = 'sspd_bphtb_show_response_' . md5($url . $noreg);

        $username = $this->username;
        $password = $this->password;
        try {
            $response = Http::withBasicAuth($username, $password)
                ->get($url);
            return $response->json();
        } catch (\Exception $e) {
            // Tangani error
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
