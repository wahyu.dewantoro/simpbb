<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait SpptKoreksiData
{
    public function listKoreksiPiutang($jenis = 'KP')
    {
        if ($jenis == '2') {
            $wh = "(jns_koreksi in ('2') or sppt_koreksi.no_transaksi not like '%KP%'  )";
        } else {
            $wh = "( jns_koreksi='3' and sppt_koreksi.no_transaksi like 'KP%')";
        }

        return DB::connection("oracle_satutujuh")->table('sppt_Koreksi')
            ->leftjoin('sim_pbb.users', 'users.id', '=', 'sppt_Koreksi.created_by')
            ->leftjoin(db::raw("sim_pbb.users c"), 'c.id', '=', 'sppt_koreksi.verifikasi_by')
            ->leftjoin('sppt', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            })
            ->whereRaw($wh)
            ->selectRaw("no_transaksi,sppt_koreksi.kd_propinsi,sppt_koreksi.kd_dati2,sppt_koreksi.kd_kecamatan,sppt_koreksi.kd_kelurahan,sppt_koreksi.kd_blok,sppt_koreksi.no_urut,sppt_koreksi.kd_jns_op,sppt_koreksi.thn_pajak_sppt,keterangan,sppt_koreksi.created_at,users.nama,verifikasi_keterangan,c.nama verifikator,jns_koreksi,verifikasi_kode,no_surat,tgl_surat,kd_kategori,nm_kategori,pbb_yg_harus_dibayar_sppt pbb")
            ->orderBy("created_at", "desc");
    }
}
