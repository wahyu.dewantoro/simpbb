<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

trait OcaWablas
{


    public function send($param)
    {


        $parameter = [];
        foreach ($param['param'] as $row) {
            $parameter[] =  [
                "type" => "text",
                "text" => $row
            ];
        }

        $msg = [
            "type" => "template",
            "template" => [
                "template_code_id" => $param['template_id'],
                "payload" => [
                    [
                        "position" => "body",
                        "parameters" => $parameter
                    ]
                ]
            ]
        ];
        // return 
        $response = Http::withToken('eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiYXBwbGljYXRpb24iOiI2NjM4NTNjYjBjOTFhODAwMTNjMWIzYTIiLCJpYXQiOjE1MTYyMzkwMjJ9.AC8yPE-MU6am0SeIKs9zC3wGTsLSKJt1DWByd96yNQHcC6XUv89gD6nNi6ok6U8u8EIaJ2beYKxF7mcHW73H2gGaPAYrQJtt2xDAyRsKcRVDIjctYZEZw096AIzfPpCRuPN2tC_Jm1pgBccEll9QwKaVjJzgddsCtF7Kzk7xJdUpR4Exm01dsv7G3c0xFrf-MbwPTwR6E3DMrvhirVQLheLp2Ay4EMj0q1jhAJgGitNc5QSgH25zhmw11omz_q2ybu0XSvlNqqv_gJAefdsRl0LcDn-DJsHBb_dB5T4Euld0w8V_BUa3vorNYp7Jd8xHhNFpXzdKF0wkAenYMcAWoQ')->post('https://wa01.ocatelkom.co.id/api/v2/push/message', [
            'phone_number' => $param['nomor'],
            'message' => $msg
        ]);

        return $response->body();
    }

    public function NotifAcc($nomor, $param)
    {
        $template_id = "d1e8cded_1722_41d6_acf6_019fbaa188c5:pbb_notifikasi_diterima";

        $send = [
            'template_id' => $template_id,
            'nomor' => NomorWa($nomor),
            'param' => $param
        ];
        $res = $this->send($send);
        return $res;
    }

    public function NotifReject($nomor, $param)
    {
        $template_id = "d1e8cded_1722_41d6_acf6_019fbaa188c5:pbb_notifikasi_ditolak";
        $send = [
            'template_id' => $template_id,
            'nomor' => NomorWa($nomor),
            'param' => $param
        ];
        $res = $this->send($send);
        // Log::info($res);
        return $res;
    }
}
