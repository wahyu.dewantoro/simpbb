<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    //
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'role_has_menus';
    public $timestamps = false;

    // public function Role()
    // {
    //     return $this->belongsToMany('App\Role', 'user_unit_kerja', 'role_id', 'menu_id');
    // }
}
