<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class sendWaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/SendWa.php';
    }

    /**
     * Bootstrap services.
     *send
     * @return void
     */
    public function boot()
    {
        //
    }
}
