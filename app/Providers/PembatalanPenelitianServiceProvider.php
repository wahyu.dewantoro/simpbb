<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PembatalanPenelitianServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/PembatalanPenelitian.php';
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
