<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        // https://esppt.id/simpbb
        $actual_link = URL::current();
        /*         if (str_contains($actual_link, 'esppt.id/simpbb')) {
            \URL::forceScheme('https');
        }
 */
        if (Str::contains($actual_link, 'esppt.id/')) {
            \URL::forceScheme('https');
        }
    }
}
