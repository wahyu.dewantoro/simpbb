<?php

namespace App\Jobs;

use App\Helpers\Pajak;
use App\Models\JenisPengurangan;
use App\Models\Layanan;
// use App\ObjekPajak;
// use App\opBangunan;
use App\penelitianTask;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
// use JenisPengurangan;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class disposisiPenelitian implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $param;
    public function __construct($param)
    {
        //
        $this->param = $param;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /* 
        Penelitian : 1-> kantor 
                    2->lapangan 
        
        Data Baru -> 1
        Pembetulan -> 3
        Mutasi Gabung -> 7
        Pembatalan -> 2
        Mutasi Penuh -> 9
        Mutasi Pecah -> 6
        */




        $nomor_layanan = $this->param;
        $penelitian = 1;
        $verlap = [1, 3, 7, 2, 9, 6];
        $nonStandart = [12, 13, 15, 16, 6, 7, 5, 3, 8, 2, 9, 4];

        // $layanan = Layanan::find($nomor_layanan);
        // list objek
        $layanan = Layanan::select(db::raw("layanan_objek.*,layanan.jenis_layanan_nama,layanan.jenis_layanan_id"))
            ->join('layanan_objek', 'layanan_objek.nomor_layanan', "=", 'layanan.nomor_layanan')
            ->whereraw("layanan.nomor_layanan='" . $nomor_layanan . "'")
            ->get();

        /* ->whereraw("(layanan_objek.nop_gabung is null   or (layanan_objek.nop_gabung is not  null and layanan.jenis_layanan_id=6) )
                    and 
                    layanan.nomor_layanan='".$nomor_layanan."'
                    ") */


        /* $param['layanan_objek_id'] = $request->lhp_id;
                $param['nop_proses'] = $request->nop_proses;
                $param['jns_pengurangan'] = $request->jns_pengurangan;
                $param['nilai_pengurangan'] = $request->nilai_pengurangan;
                $flash['msg'] = Pajak::prosesPotongan($param); */





        if ($layanan->count() > 1) {
            $kolektf = 1;
            // $penelitian = '1';
        } else {
            $kolektf = 0;
            // $penelitian = '1';
        }

        foreach ($layanan as $obj) {
            if ($obj->jenis_layanan_id == '5') {
                // cek selesai depan atau tidak
                $jp = JenisPengurangan::where('id', $obj->jenis_pengurangan)->first();
                if ($jp->jenis == '0') {
                    // proses langsung 
                    $nop_proses = $obj->kd_propinsi . $obj->kd_dati2 . $obj->kd_kecamatan . $obj->kd_kelurahan . $obj->kd_blok . $obj->no_urut . $obj->kd_jns_op;
                    $param['jenis_layanan'] = $obj->jenis_layanan_id.'_'.$obj->jenis_layanan_id;
                    $param['layanan_objek_id'] = $obj->id;
                    $param['nop_proses'] = $nop_proses;
                    $param['jns_pengurangan'] = $jp->id;
                    // $param['nilai_pengurangan'] = $jp->nilai_pengurangan;
                    $param['keterangan'] = '';
                    $flash['msg'] = Pajak::prosesPotongan($param);
                }
            } else {
                // limpahkan ke penelitian

                // cek luas tanah
                if ($kolektf == 1) {
                    $penelitian = '1';
                } else {
                    // cek luas tanah mutasi gabung
                    $luas_bumi = $obj->luas_bumi;
                    // get gabung
                    // $opinduk = $obj->kd_propinsi . $obj->kd_dati2 . $obj->kd_kecamatan . $obj->kd_kelurahan . $obj->kd_blok . $obj->no_urut . $obj->kd_jns_op;
                    $opinduk = $obj->id;
                    $gb = DB::select(db::raw("select luas_bumi 
                from layanan_objek
                where nomor_layanan='$nomor_layanan' 
                and nop_gabung='$opinduk'"));
                    foreach ($gb as $rgb) {
                        $luas_bumi += $rgb->luas_bumi;
                    }
                    if ($luas_bumi > 10000 || $obj->kelompok_objek_id == '10') {
                        switch ($obj->kelompok_objek_id) {
                            case '1':
                                # code...
                                $penelitian = '1';
                                break;
                            default:
                                # code...
                                $penelitian = '2';
                                break;
                        }
                    } else {
                        $penelitian = '1';
                    }
                }
                $where = [
                    'nomor_layanan' => $nomor_layanan,
                    'layanan_objek_id' => $obj->id
                ];
                $task = [
                    'nomor_layanan' => $nomor_layanan,
                    'layanan_objek_id' => $obj->id,
                    'jenis_layanan_nama' => $obj->jenis_layanan_nama,
                    'penelitian' => $penelitian
                ];
                DB::beginTransaction();
                try {
                    //code...
                    penelitianTask::firstOrCreate($where, $task);
                    DB::commit();
                } catch (\Throwable $th) {
                    //throw $th;
                    DB::rollBack();
                    log::emergency($th);
                }
            }
        }
    }
}
