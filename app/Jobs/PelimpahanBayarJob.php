<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class PelimpahanBayarJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $param;
    public function __construct($param)
    {
        //
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $jenis = $this->param['jenis'] ?? '';
        $nop = $this->param['nop'] ?? '';
        $tahun = $this->param['tahun'] ?? '';

        if ($jenis <> '' && $nop <> '' && $tahun <> '') {



            $kobil = onlyNumber($nop);
            $kd_propinsi = substr($kobil, 0, 2);
            $kd_dati2 = substr($kobil, 2, 2);
            $kd_kecamatan = substr($kobil, 4, 3);
            $kd_kelurahan = substr($kobil, 7, 3);
            $kd_blok = substr($kobil, 10, 3);
            $no_urut = substr($kobil, 13, 4);
            $kd_jns_op = substr($kobil, 17, 1);
            // if (config('app.env') != 'local') {
            if ($jenis == 1) {
                // flag

                DB::connection('oracle_spo')->statement(DB::raw("
                begin
                UPDATE sppt@to17
                SET status_pembayaran_sppt = 1
              WHERE ROWID IN (SELECT a.ROWID
                                FROM sppt@to17 a
                                     JOIN
                                     (SELECT a.KD_PROPINSI,
                                             a.KD_DATI2,
                                             a.KD_KECAMATAN,
                                             a.KD_KELURAHAN,
                                             a.KD_BLOK,
                                             a.NO_URUT,
                                             a.KD_JNS_OP,
                                             a.THN_PAJAK_SPPT
                                        FROM (SELECT KD_PROPINSI,
                                                     KD_DATI2,
                                                     KD_KECAMATAN,
                                                     KD_KELURAHAN,
                                                     KD_BLOK,
                                                     NO_URUT,
                                                     KD_JNS_OP,
                                                     THN_PAJAK_SPPT,
                                                     PEMBAYARAN_SPPT_KE,
                                                     '01' KD_KANWIL,
                                                     '01' KD_KANTOR,
                                                     KD_TP,
                                                     DENDA_SPPT,
                                                     JML_SPPT_YG_DIBAYAR,
                                                     TGL_PEMBAYARAN_SPPT,
                                                     TGL_REKAM_BYR_SPPT,
                                                     '060000000000000000'
                                                        NIP_REKAM_BYR_SPPT,
                                                     pengesahan,
                                                     TRIM (kode_bank_bayar)
                                                FROM pembayaran_sppt
                                                     JOIN ws_pembayaran_tahun
                                                        ON pembayaran_sppt.pengesahan =
                                                              ws_pembayaran_tahun.kodepengesahan
                                               WHERE     nop = '" . $nop . "'
                                                     AND tahun_pajak = '" . $tahun . "') a
                                             LEFT JOIN
                                             (SELECT * FROM pembayaran_sppt@to17) b
                                                ON     a.KD_PROPINSI = b.KD_PROPINSI
                                                   AND a.KD_DATI2 = b.KD_DATI2
                                                   AND a.KD_KECAMATAN = b.KD_KECAMATAN
                                                   AND a.KD_KELURAHAN = b.KD_KELURAHAN
                                                   AND a.KD_BLOK = b.KD_BLOK
                                                   AND a.NO_URUT = b.NO_URUT
                                                   AND a.KD_JNS_OP = b.KD_JNS_OP
                                                   AND a.THN_PAJAK_SPPT = b.THN_PAJAK_SPPT
                                       WHERE b.thn_pajak_sppt IS NULL) b
                                        ON     a.KD_PROPINSI = b.KD_PROPINSI
                                           AND a.KD_DATI2 = b.KD_DATI2
                                           AND a.KD_KECAMATAN = b.KD_KECAMATAN
                                           AND a.KD_KELURAHAN = b.KD_KELURAHAN
                                           AND a.KD_BLOK = b.KD_BLOK
                                           AND a.NO_URUT = b.NO_URUT
                                           AND a.KD_JNS_OP = b.KD_JNS_OP
                                           AND a.THN_PAJAK_SPPT = b.THN_PAJAK_SPPT);
                INSERT INTO pembayaran_sppt@to17 (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE, KD_KANWIL, KD_KANTOR, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT, pengesahan, kode_bank)
                SELECT a.*
                        FROM (SELECT KD_PROPINSI,
                                    KD_DATI2,
                                    KD_KECAMATAN,
                                    KD_KELURAHAN,
                                    KD_BLOK,
                                    NO_URUT,
                                    KD_JNS_OP,
                                    THN_PAJAK_SPPT,
                                    PEMBAYARAN_SPPT_KE,
                                    '01' KD_KANWIL,
                                    '01' KD_KANTOR,
                                    KD_TP,
                                    DENDA_SPPT,
                                    JML_SPPT_YG_DIBAYAR,
                                    TGL_PEMBAYARAN_SPPT,
                                    TGL_REKAM_BYR_SPPT,
                                    '060000000000000000' NIP_REKAM_BYR_SPPT,
                                    pengesahan,
                                    TRIM (kode_bank_bayar)
                                FROM pembayaran_sppt
                                    JOIN ws_pembayaran_tahun
                                        ON pembayaran_sppt.pengesahan =
                                                ws_pembayaran_tahun.kodepengesahan
                                WHERE nop = '" . $nop . "' AND tahun_pajak = '" . $tahun . "') a
                            LEFT JOIN (SELECT * FROM pembayaran_sppt@to17) b
                                ON     a.KD_PROPINSI = b.KD_PROPINSI
                                    AND a.KD_DATI2 = b.KD_DATI2
                                    AND a.KD_KECAMATAN = b.KD_KECAMATAN
                                    AND a.KD_KELURAHAN = b.KD_KELURAHAN
                                    AND a.KD_BLOK = b.KD_BLOK
                                    AND a.NO_URUT = b.NO_URUT
                                    AND a.KD_JNS_OP = b.KD_JNS_OP
                                    AND a.THN_PAJAK_SPPT = b.THN_PAJAK_SPPT
                        WHERE b.thn_pajak_sppt IS NULL;
            commit;
                end;
            "));
            }

            if ($jenis == 2) {
                // unflag
                $sql = "";

                if ($kd_blok == '999') {
                    // kobil

                    $row = DB::table(DB::raw("data_billing a"))->join(db::raw("billing_kolektif b"), 'a.data_billing_id', '=', 'b.data_billing_id')
                        ->selectraw(" b.kd_kecamatan,b.kd_kelurahan,b.kd_blok,b.no_urut,b.kd_jns_op,b.tahun_pajak")
                        ->whereraw("kobil='" . $kobil . "'
                    and a.tahun_pajak='" . $tahun . "'")->get();

                    foreach ($row as $row) {
                        $kd_kecamatan = $row->kd_kecamatan;
                        $kd_kelurahan = $row->kd_kelurahan;
                        $kd_blok = $row->kd_blok;
                        $no_urut = $row->no_urut;
                        $kd_jns_op = $row->kd_jns_op;
                        $tahun_pajak = $row->tahun_pajak;

                        $sql .= "
                            UPDATE SPPT set status_pembayaran_sppt='0'
                            where thn_pajak_sppt='" . $tahun_pajak . "'
                            and kd_Kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'
                            and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "';

                            DELETE FROM pembayaran_sppt where thn_pajak_sppt='" . $tahun_pajak . "'
                            and kd_Kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'
                            and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "';";
                    }
                } else {
                    $sql .= "
                UPDATE SPPT set status_pembayaran_sppt='0'
                where thn_pajak_sppt='" . $tahun . "'
                and kd_Kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'
                and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "';

                DELETE FROM pembayaran_sppt where thn_pajak_sppt='" . $tahun . "'
                and kd_Kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'
                and kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "';";
                }

                if ($sql != '') {
                    DB::connection('oracle_satutujuh')->statement(DB::raw("begin  " . $sql . " end;"));
                }
            }
        }
    }
}
