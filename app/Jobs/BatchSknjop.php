<?php

namespace App\Jobs;

use App\SkNJop;
use App\skNjopDokumen;
// use App\sknjoppermohonan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use romanzipp\QueueMonitor\Traits\IsMonitored;
use SnappyPdf as PDF;

class BatchSknjop implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $id;
    public function __construct($id)
    {
        //
        $this->id=$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $nop = SkNJop::whereraw("sknjop_permohonan_id='" . $this->id . "'")->get();
        // Log::info($nop);
        $disk = Storage::disk('sknjop');
        $mp = array();
        foreach ($nop as $sknjop) {
            $url = config('app.url') . 'sknjop-pdf/' . $sknjop->id;
            $pages = View::make('sknjop/_pdf', compact('sknjop', 'url'));
            $mp[] = $pages;
            $pdf = pdf::loadHTML($pages)
                ->setPaper('A4');
            $path = 'SKNJOP ' . time() . '.pdf';
            $pdf->setOption('enable-local-file-access', true);
            if ($disk->put($path, $pdf->output())) {
                $file = [
                    'sknjop_id' => $sknjop->id,
                    'disk' => 'sknjop',
                    'path' => $path,
                    'filename' => str_replace('/', ' ', $sknjop->nomer_sk)
                ];
                skNjopDokumen::create($file);
                Log::info("SK NJOP " . $sknjop->nomer_sk . " telah di buat");
            }else{
                log::warning("SK NJOP " . $sknjop->nomer_sk . " gagal di buat");
            }
        }


        if (!empty($mp)) {
            $pdfmulti = pdf::loadHTML($mp);
            $disk->put($this->id . '.pdf', $pdfmulti->output());
        }
    }
}
