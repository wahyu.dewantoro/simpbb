<?php

namespace App\Jobs;

use App\Exports\piutangTahunExcell;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class generatePiutangTahunJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $disk = 'reports';
        $tahun = $this->data;
        $filename = 'Piutang ' . $tahun . '.xlsx';

        if (Storage::disk('reports')->exists($filename)) {
            Storage::disk($disk)->delete($filename);
        }
        Excel::store(new piutangTahunExcell($tahun), $filename, $disk);
    }
}
