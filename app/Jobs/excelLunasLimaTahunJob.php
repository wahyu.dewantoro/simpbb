<?php

namespace App\Jobs;

use App\Exports\LunasLimaTahun;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class excelLunasLimaTahunJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $kd_kecamatan = $this->data;
        $disk = 'reports';
        $filename = 'Belum lunas 2016-2020 ' . $kd_kecamatan . '.xlsx';
        Excel::store(new LunasLimaTahun($kd_kecamatan), $filename, $disk);
    }
}
