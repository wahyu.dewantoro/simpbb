<?php

namespace App\Jobs;

use App\SkNJop;
use App\skNjopDokumen;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\View;
use romanzipp\QueueMonitor\Traits\IsMonitored;
use SnappyPdf as PDF;

class genareteSkNJOP implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $id;
    public function __construct($id)
    {
        //
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $id = $this->id;
        $sknjop = SkNJop::findorfail($id);
        $url = url('sknjop-pdf', $id);
        $pages = View::make('sknjop/_pdf', compact('sknjop', 'url'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $path = 'sk' . time() . '.pdf';

        $pdf->setOption('enable-local-file-access', true);
       
        $disk = Storage::disk('sknjop');
        if ($disk->put($path, $pdf->output())) {
            $file = [
                'sknjop_id' => $id,
                'disk' => 'sknjop',
                'path' => $path,
                'filename' => str_replace('/', ' ', $sknjop->nomer_sk)
            ];
            $res = skNjopDokumen::create($file);
            Log::info("SK NJOP " . $sknjop->nomer_sk . " telah di buat");

            return $res;
        } else {
            log::warning("SK NJOP " . $sknjop->nomer_sk . " gagal di buat");
            return null;
        }
    }
}
