<?php

namespace App\Jobs;

use App\Helpers\Pajak;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class pendataanObjek implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $spop;
    public function __construct($spop)
    {
        //
        $this->spop = $spop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $request = new \Illuminate\Http\Request();
        $request->merge($this->spop);
        $flash = Pajak::prosesPendataan($request);
        // Log::info(['request_pendataan' => $request]);
    }
}
