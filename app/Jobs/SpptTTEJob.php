<?php

namespace App\Jobs;

use App\Helpers\Esppt;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SpptTTEJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $request;
    public function __construct($request)
    {
        $this->request = $request;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if (config('app.debug') == true) {
            $thn_pajak_sppt = $this->request['thn_pajak_sppt'];
            $kd_propinsi = $this->request['kd_propinsi'];
            $kd_dati2 = $this->request['kd_dati2'];
            $kd_kecamatan = $this->request['kd_kecamatan'];
            $kd_kelurahan = $this->request['kd_kelurahan'];
            $kd_blok = $this->request['kd_blok'];
            $no_urut = $this->request['no_urut'];
            $kd_jns_op = $this->request['kd_jns_op'];
            $nop = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op . $thn_pajak_sppt;
            $file = Esppt::generate($nop, 'save', '0', '');

            log::info('https://esppt.id/api/sppt-upload');
            $http = Http::attach(
                'file',
                file_get_contents($file),
                time() . '.pdf'
            )
                ->post('https://esppt.id/api/sppt-upload', $this->request);
            log::info($http);
            // config('app.tnde_url')
            // https://esppt.id/api/sppt-upload
        }
    }
}
