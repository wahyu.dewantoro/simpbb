<?php

namespace App\Jobs;

use App\Helpers\Esppt;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

class SendSpptTte implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = DB::connection("oracle_satutujuh")
            ->table("sppt")->selectraw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt")
            ->whereraw("tte_upload_at is null and rownum<=20")
            ->orderByraw("thn_pajak_sppt desc,kd_propinsi desc,kd_dati2 desc,kd_kecamatan desc,kd_kelurahan desc,kd_blok desc,no_urut desc,kd_jns_op desc")
            ->get();
        // render file sppt dulu


        $arrayFile = [
            [
                'name' => 'kd_bidang',
                'contents' => '02'
            ],
            [
                'name' => 'jenis',
                'contents' => '1'
            ],
            [
                'name' => 'keterangan',
                'contents' => 'TTE dokumen sppt PBB P2'
            ],
            [
                'name' => 'table_name',
                'contents' => 'sppt'
            ],
            [
                'name' => 'table_id',
                'contents' => '-'
            ]
        ];

        $filedel = [];


        foreach ($data as $sppt) {
            $whereup = "";
            $whereup .= " (";
            $whereup .= " kd_propinsi ='" . $sppt->kd_propinsi . "' and ";
            $whereup .= " kd_dati2 ='" . $sppt->kd_dati2 . "' and ";
            $whereup .= " kd_kecamatan ='" . $sppt->kd_kecamatan . "' and ";
            $whereup .= " kd_kelurahan ='" . $sppt->kd_kelurahan . "' and ";
            $whereup .= " kd_blok ='" . $sppt->kd_blok . "' and ";
            $whereup .= " no_urut ='" . $sppt->no_urut . "' and ";
            $whereup .= " kd_jns_op ='" . $sppt->kd_jns_op . "' and ";
            $whereup .= " thn_pajak_sppt='" . $sppt->thn_pajak_sppt . "'";
            $whereup .= " ) ";

            // $whereup = substr($whereup, 0, -2);
            DB::connection("oracle_satutujuh")->table("sppt")->whereraw($whereup)->update(['tte_upload_at' => Carbon::now()]);


            $nop = $sppt->kd_propinsi . $sppt->kd_dati2 . $sppt->kd_kecamatan . $sppt->kd_kelurahan . $sppt->kd_blok . $sppt->no_urut . $sppt->kd_jns_op . $sppt->thn_pajak_sppt;
            $filename = $nop . '.pdf';
            $filedel[] = $filename;
            if (Storage::disk('sppt')->exists($filename)) {
                Storage::disk('sppt')->delete($filename);
            }
            Esppt::generate($nop, 'save', '1', '');

            $disk = Storage::disk('sppt');
            $file_path = $disk->path($filename);
            $file_mime = $disk->mimeType($filename);
            $arrayFile[] = [
                'name' => 'table_name_file[]',
                'contents' => 'sppt'
            ];
            $arrayFile[] = [
                'name' => 'table_id_file[]',
                'contents' => $nop
            ];
            $arrayFile[] = [
                'name' => 'file[]',
                'filename' => $filename,
                'Mime-Type' => $file_mime,
                'contents' => fopen($file_path, 'r')
            ];
        }

        $client = new Client();
        $response = $client->request("POST", config('app.site_tte'), [
            'multipart' => $arrayFile
        ]);

        $code = $response->getStatusCode();
        /* if ($code == 200) {
            $whereup = substr($whereup, 0, -2);
            DB::connection("oracle_satutujuh")->table("sppt")->whereraw($whereup)->update(['tte_upload_at' => Carbon::now()]);
        } */

        foreach ($filedel as $del) {
            if (Storage::disk('sppt')->exists($del)) {
                Storage::disk('sppt')->delete($del);
            }
        }
    }
}
