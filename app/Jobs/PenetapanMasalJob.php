<?php

namespace App\Jobs;

use App\Helpers\Pajak;
use App\Models\Notifikasi;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PenetapanMasalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */


    public function handle()
    {
        $request = $this->request;
        // Log::info($request);
        $tgl_terbit = $request['tgl_terbit'];
        $tgl_jatuh_tempo = $request['tgl_jatuh_tempo'];
        $val_tgl_terbit = date_format(date_create($tgl_terbit), "dmY");
        $val_tgl_jatuh_tempo = date_format(date_create($tgl_jatuh_tempo), "dmY");

        $kd_propinsi = '35';
        $kd_dati2 = '07';
        $kd_kecamatan = is_null($request['kd_kecamatan']) ? '' : $request['kd_kecamatan'];
        $kd_kelurahan = is_null($request['kd_kelurahan']) ? '' : $request['kd_kelurahan'];
        $kd_blok = is_null($request['kd_blok']) ? '' : $request['kd_blok'];
        $no_urut = is_null($request['no_urut']) ? '' : $request['no_urut'];
        // $kd_jns_op = $request['kd_jns_op'];
        $tahun = $request['tahun_pajak'];
        $nip = '000000000000001003';

        /* 
        log::info([
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'tahun' => $tahun
        ]); */


        if (
            $kd_kecamatan != '' &&
            $kd_kelurahan != '' &&
            $kd_blok != '' &&
            $no_urut != ''
        ) {
            // by nop
            $nop = Pajak::ListNop()->whereraw("dat_objek_pajak.kd_kecamatan ='" . $kd_kecamatan . "' and dat_objek_pajak.kd_kelurahan='" . $kd_kelurahan . "' and dat_objek_pajak.kd_blok='" . $kd_blok . "' and no_urut='" . $no_urut . "' ")->get();
            foreach ($nop as $row) {
                $deskripsi_nop = " Pada NOP " . $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                log::info($deskripsi_nop);
                DB::connection('oracle_satutujuh')->statement(DB::raw("call PBB.PENENTUAN_NJOP ( '" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $tahun . "', 1 )"));
                DB::connection('oracle_satutujuh')->statement(DB::raw("call pbb.PENETAPAN_NOP_20('" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $tahun . "', to_date('" . $val_tgl_terbit . "','ddmmyyyy') ,  to_date('" . $val_tgl_jatuh_tempo . "','ddmmyyyy') , '" . $nip . "')"));
            }
        } else if (
            $kd_kecamatan != '' &&
            $kd_kelurahan != '' &&
            $kd_blok != '' &&
            $no_urut == ''
        ) {
            // per blok

            // $deskripsi_nop=" Pada NOP ". $row->kd_propinsi.'.'. $row->kd_dati2.'.'. $row->kd_kecamatan.'.'. $row->kd_kelurahan.'.'. $row->kd_blok.'-'. $row->no_urut.'.'. $row->kd_jns_op;

            $wilayah = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
                ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                ->select(db::raw("nm_kecamatan,nm_kelurahan"))
                ->whereraw(db::raw("ref_kecamatan.kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'"))
                ->first();

            $deskripsi_nop = " Pada  Kecamatan " . $wilayah->nm_kecamatan . "  kelurahan/desa " . $wilayah->nm_kelurahan . " blok " . $kd_blok;

            $nop = Pajak::ListNop()->whereraw("dat_objek_pajak.kd_kecamatan ='" . $kd_kecamatan . "' and dat_objek_pajak.kd_kelurahan='" . $kd_kelurahan . "' and dat_objek_pajak.kd_blok='" . $kd_blok . "'")->get();
            foreach ($nop as $row) {
                DB::connection('oracle_satutujuh')->statement(DB::raw("call PBB.PENENTUAN_NJOP ( '" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $tahun . "', 1 )"));
                DB::connection('oracle_satutujuh')->statement(DB::raw("call pbb.PENETAPAN_NOP_20('" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $tahun . "', to_date('" . $val_tgl_terbit . "','ddmmyyyy') ,  to_date('" . $val_tgl_jatuh_tempo . "','ddmmyyyy') , '" . $nip . "')"));
            }
        } else if (
            $kd_kecamatan != '' &&
            $kd_kelurahan != '' &&
            $kd_blok == '' &&
            $no_urut == ''
        ) {
            // per kelurahan
            $wilayah = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
                ->join('ref_kelurahan', 'ref_kelurahan.kd_kecamatan', '=', 'ref_kecamatan.kd_kecamatan')
                ->select(db::raw("nm_kecamatan,nm_kelurahan"))
                ->whereraw(db::raw("ref_kecamatan.kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "'"))
                ->first();

            $deskripsi_nop = " Pada  Kecamatan " . $wilayah->nm_kecamatan . "  kelurahan/desa " . $wilayah->nm_kelurahan;
            // log::info($deskripsi_nop);
            DB::connection("oracle_satutujuh")->statement(DB::raw("call PENETAPAN_KELURAHAN('" . $tahun . "','" . $kd_kecamatan . "', '" . $kd_kelurahan . "', to_date('" . $val_tgl_terbit . "','ddmmyyyy') ,  to_date('" . $val_tgl_jatuh_tempo . "','ddmmyyyy') , '" . $nip . "')"));
        } else if (
            $kd_kecamatan != '' &&
            $kd_kelurahan == '' &&
            $kd_blok == '' &&
            $no_urut == ''
        ) {
            // per kecamatan
            $wilayah = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
                ->select(db::raw("nm_kecamatan"))
                ->whereraw(db::raw("ref_kecamatan.kd_kecamatan='" . $kd_kecamatan . "'"))
                ->first();

            $deskripsi_nop = " Pada  Kecamatan " . $wilayah->nm_kecamatan;
            // log::info($deskripsi_nop);
            DB::connection("oracle_satutujuh")->statement(DB::raw("call penetapan_kecamatan('" . $tahun . "','" . $kd_kecamatan . "', to_date('" . $val_tgl_terbit . "','ddmmyyyy') ,  to_date('" . $val_tgl_jatuh_tempo . "','ddmmyyyy') , '" . $nip . "')"));
        } else {
            // se kabupaten
            $deskripsi_nop = " Pada  Kabupaten Malang";
            // log::info($deskripsi_nop);
            $kec = DB::connection("oracle_satutujuh")->table("ref_kecamatan")->get();
            foreach ($kec as $row) {
                DB::connection("oracle_satutujuh")->statement(DB::raw("call penetapan_kecamatan('" . $tahun . "','" . $row->kd_kecamatan . "', to_date('" . $val_tgl_terbit . "','ddmmyyyy') ,  to_date('" . $val_tgl_jatuh_tempo . "','ddmmyyyy') , '" . $nip . "')"));
            }
        }

        $notif = [
            'tipe' => 'info',
            'judul' => 'Penetepan Masal',
            'deskripsi' => "Proses penetapan " . $deskripsi_nop . " telah selesai ",
            'user_id' => $request['user_id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        Notifikasi::create($notif);

        // selesai ke table notifikasi
    }
}


