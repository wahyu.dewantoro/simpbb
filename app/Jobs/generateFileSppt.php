<?php

namespace App\Jobs;

use App\Helpers\Esppt;
use App\Sppt;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class generateFileSppt implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use IsMonitored; // <---
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $param = $this->data;
        // $jenis = $param['jenis'];
        $tahun = $param['tahun'] ?? date('Y');
        $kd_kecamatan = $param['kd_kecamatan'] ?? '';
        $kd_kelurahan = $param['kd_kelurahan'] ?? '';
        $kd_blok = $param['kd_blok'] ?? '';
        $no_urut = $param['no_urut'] ?? '';
        $kd_jns_op = $param['kd_jns_op'] ?? '';



        $sppt = Sppt::where('thn_pajak_sppt', $tahun);
        if ($kd_kecamatan <> '') {
            $sppt = $sppt->whereraw("kd_kecamatan='$kd_kecamatan'");
        }
        if ($kd_kelurahan <> '') {
            $sppt = $sppt->whereraw("kd_kelurahan='$kd_kelurahan'");
        }
        if ($kd_blok <> '') {
            $sppt = $sppt->whereraw("kd_blok='$kd_blok'");
        }
        if ($no_urut <> '') {
            $sppt = $sppt->whereraw("no_urut='$no_urut'");
        }

        if ($kd_jns_op <> '') {
            $sppt = $sppt->whereraw("kd_jns_op='$kd_jns_op'");
        }

        $sppt = $sppt->get();
        foreach ($sppt as $row) {
            $nop = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op . $row->thn_pajak_sppt;
            // $res = Esppt::generate($nop, 'save');
            $res = Esppt::generate($nop, 'save', '0');
            Log::info($res);
        }
    }
    // }
}
