<?php

namespace App\Jobs;

use App\HisJatuhTempo;
use App\Sppt;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class updateJatuhTempo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	//, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */


    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $var = $this->data;
        $tahun = $var['tahun'];
        $tanggal = $var['tanggal'];
        try {
            //code...
            DB::beginTransaction();
            Sppt::whereraw("thn_pajak_sppt='" . $tahun . "' and status_pembayaran_sppt=0")->update(['tgl_jatuh_tempo_sppt' => $tanggal]);

            DB::commit();
            Log::info("Tanggal jatuh tempo SPPT tahun pajak " . $tahun . " telah di rubah menjadi " . tglIndo($tanggal));
        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();
            Log::error($e->getMessage());
        }
    }
}
