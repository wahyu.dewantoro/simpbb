<?php

namespace App\Jobs;

use App\Exports\DhkpKabupaten;
use App\fileReport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class DhkpFileKecamatan implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use IsMonitored; // <---
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $kd_kecamatan;
    public function __construct($kd_kecamatan)
    {
        //
        $this->kd_kecamatan = $kd_kecamatan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Log::info($this->param);

        ini_set('memory_limit', '-1');
        $kd_kec = $this->kd_kecamatan;
        $disk = 'reports';
        $filename = 'DHKP ' . $kd_kec . '.xlsx';
        Excel::store(new DhkpKabupaten($kd_kec), $filename, $disk);
        $rekam = [
            'keterangan' => 'DHKP file kecamatan ' . $kd_kec,
            'disk' => $disk,
            'filename' => $filename
        ];
        fileReport::create($rekam);
        // Log::info('DHKP 3,4,5 telah di generate');

    }
}
