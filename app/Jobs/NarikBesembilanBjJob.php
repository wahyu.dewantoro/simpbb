<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;

class NarikBesembilanBjJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $tanggal;
    public function __construct($tanggal)
    {
        //
        $this->tanggal = $tanggal;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Artisan::call('besembilan:pbb', ['--tanggal' => $this->tanggal]);
    }
}
