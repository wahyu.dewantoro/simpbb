<?php

namespace App\Jobs;

use App\Helpers\Pajak;
use App\Models\Data_billing;
use App\RekonHistory;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class RekonPembayaranJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $param;
    public function __construct($param)
    {
        //
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */


    public function handle()
    {
        $param = $this->param;

        $rek = DB::table('rekon_batch')
            ->selectraw("id,to_char(tanggal_pembayaran,'yyyymmdd') tanggal_bayar,kode_bank")
            ->where("id", $param)->first();

        $nop_kobil = DB::table('rekon_nop')->select(db::raw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,tanggal_pembayaran"))
            ->whereraw(" rekon_batch_id='" . $param . "' and kd_blok='999'")->get();



        DB::beginTransaction();
        try {
            //code...
            // KodePengesahan()
            $kode_bank = trim($rek->kode_bank);
            $tanggal = $rek->tanggal_bayar;

            DB::statement("delete from pbb.pembayaran_sppt where trim(kode_bank)='" . $kode_bank . "' and trunc(tgl_pembayaran_sppt)=to_date('" . $tanggal . "','yyyymmdd')");

            
            foreach ($nop_kobil as $detail) {
                $kobil = $detail->kd_propinsi . $detail->kd_dati2 . $detail->kd_kecamatan . $detail->kd_kelurahan . $detail->kd_blok . $detail->no_urut . $detail->kd_jns_op;
                $tahun_pajak = $detail->th_pajak_sppt;
                $tanggal = $rek->tanggal_bayar;
                DB::statement("update data_billing set kd_status='0' ,tgl_bayar=null where kobil='" . $kobil . "' and kd_status='1' and trunc(tgl_bayar)=to_date('" . $tanggal . "') and tahun_pajak='".$tahun_pajak."'");
            }

            // list nop
            $ln = DB::table("rekon_nop")->select(db::raw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,denda,jumlah,to_char(tanggal_pembayaran,'yyyymmdd') tanggal_pembayaran"))
                ->where('rekon_batch_id', $rek->id)->get();
            foreach ($ln as $row) {
                $ntpd = KodePengesahan();
                if ($row->kd_blok == '999') {
                    // kobil
                    $kobil = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op;
                    DB::statement(db::raw("begin SPO.PROC_PEMBAYARAN_KOBIL ( '" . $kobil . "', '" . $row->thn_pajak_sppt . "', to_date('" . $tanggal . "','yyyymmdd'), '" . $ntpd . "', '" . $kode_bank . "' ); commit; end;"));
                } else {
                    // nop biasa
                    DB::statement(db::raw("begin 
                            SPO.PROC_PEMBAYARAN ( '" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $row->thn_pajak_sppt . "', to_date('" . $tanggal . "','yyyymmdd'), '" . $row->denda . "','" . $row->jumlah . "', '" . $ntpd . "', '" . $kode_bank . "');
                            commit;
                            end; "));
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            log::error($th);
        }
    }
}
