<?php

namespace App\Jobs;

use App\Helpers\Lhp;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class GenerateSk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $layanan_objek_id;
    public function __construct($layanan_objek_id)
    {
        //
        $this->layanan_objek_id = $layanan_objek_id;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Lhp::getSk($this->layanan_objek_id);
    }
}
