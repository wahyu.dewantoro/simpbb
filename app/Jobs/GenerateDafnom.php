<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DOMPDF as PDF;
use App\Models\Data_billing;
use Illuminate\Support\Facades\DB;
use App\Models\Billing_kolektif;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Helpers\Dafnom;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class GenerateDafnom implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    //use IsMonitored; // <---
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $setData = [];
    public $timeout = 300;
    public function __construct($setData)
    {
        $this->setData = $setData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->setData) {
            $id_billing = $this->setData['id_billing'];
            return Dafnom::makedaftarBillingKolektifPembayaran($id_billing);
        }
    }
}
