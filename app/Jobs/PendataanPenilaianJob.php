<?php

namespace App\Jobs;

use App\Helpers\Pajak;
use App\Helpers\PenetapanSppt;
use App\Models\Layanan;
use App\Models\Layanan_objek;
// use App\ObjekPajak;
// use App\OpBumi;
// use App\SubjekPajak;
// use App\TblSpop;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class PendataanPenilaianJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $param;
    public function __construct($param)
    {
        //
        $this->param = $param;
    }

    public function handle()
    {
        // ada dua jenis 1 -> dari ajuan pelayanan , 2 -> dari pendataan kantor/ tanpa ajuan layanan

        // 1 -> dari ajuan layanan
        $userId = $this->param['pemutakhiran_by'] ??  Pajak::userid();
        $nomor_layanan = $this->param['nomor_layanan'];
        $tahun = substr($nomor_layanan, 0, 4);
        $objeklayanan = Layanan_objek::with('layanan')->whereraw("nomor_layanan='$nomor_layanan'")->get();
        foreach ($objeklayanan as $objek) {
            $jenis_ajuan_layanan = $objek->layanan->jenis_layanan_id;
            // jenis ajuan pembetulan subjek pajak
            if ($jenis_ajuan_layanan == '8') {
                // hanya pendataan saja, tanpa penilaian dan penetapan
                $kd_propinsi = $objek->kd_propinsi;
                $kd_dati2 = $objek->kd_dati2;
                $kd_kecamatan = $objek->kd_kecamatan;
                $kd_kelurahan = $objek->kd_kelurahan;
                $kd_blok = $objek->kd_blok;
                $no_urut = $objek->no_urut;
                $kd_jns_op = $objek->kd_jns_op;



                DB::beginTransaction();
                try {
                    //generete data spop/pembetulan
                    /* $dataspop = (array) DB::connection("oracle_satutujuh")->select(DB::raw("SELECT 2 JNS_TRANSAKSI, A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_PROSES,NULL NOP_BERSAMA,A.KD_PROPINSI|| A.KD_DATI2|| A.KD_KECAMATAN|| A.KD_KELURAHAN|| A.KD_BLOK|| A.NO_URUT|| A.KD_JNS_OP NOP_ASAL, 
                    '" . $objek->nik_wp . "' SUBJEK_PAJAK_ID,'" . $objek->nama_wp . "' NM_WP,'" . $objek->alamat_wp . "' JALAN_WP,null BLOK_KAV_NO_WP,
                        '" . $objek->rw_wp . "' RW_WP,'" . $objek->rt_wp . "' RT_WP,'" . strtoupper($objek->kelurahan_wp) . " - " . strtoupper($objek->kecamatan_wp) . "' KELURAHAN_WP,
                       '" . $objek->dati2_wp . " - " . $objek->propinsi_wp . "' KOTA_WP,null KD_POS_WP,null TELP_WP,null NPWP, 5 STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,KD_ZNT,LUAS_BUMI,JNS_BUMI 
                       FROM DAT_OBJEK_PAJAK A
                        JOIN dat_subjek_pajak b ON a.subjek_pajak_id=b.subjek_pajak_id
                        JOIN dat_op_bumi c ON a.kd_propinsi=c.kd_propinsi AND a.kd_dati2=c.kd_dati2 AND a.kd_kecamatan=c.kd_kecamatan AND a.kd_kelurahan=c.kd_kelurahan AND a.kd_blok=c.kd_blok AND a.no_urut=c.no_urut AND a.kd_jns_op=c.kd_jns_op
                        WHERE a.kd_kecamatan='$kd_kecamatan'
                        AND a.kd_kelurahan='$kd_kelurahan'
                        AND a.kd_blok='$kd_blok'
                        AND a.no_urut='$no_urut'
                        AND a.kd_jns_op='$kd_jns_op' "))[0];
                         */



                    $dataspop = DB::connection("oracle_satutujuh")
                        ->table('dat_objek_pajak as a')
                        ->join('dat_subjek_pajak as b', 'a.subjek_pajak_id', '=', 'b.subjek_pajak_id')
                        ->join('dat_op_bumi as c', function ($join) {
                            $join->on('a.kd_propinsi', '=', 'c.kd_propinsi')
                                ->on('a.kd_dati2', '=', 'c.kd_dati2')
                                ->on('a.kd_kecamatan', '=', 'c.kd_kecamatan')
                                ->on('a.kd_kelurahan', '=', 'c.kd_kelurahan')
                                ->on('a.kd_blok', '=', 'c.kd_blok')
                                ->on('a.no_urut', '=', 'c.no_urut')
                                ->on('a.kd_jns_op', '=', 'c.kd_jns_op');
                        })
                        ->selectRaw("
                                2 AS JNS_TRANSAKSI, 
                                A.KD_PROPINSI || A.KD_DATI2 || A.KD_KECAMATAN || A.KD_KELURAHAN || A.KD_BLOK || A.NO_URUT || A.KD_JNS_OP AS NOP_PROSES,
                                NULL AS NOP_BERSAMA, 
                                A.KD_PROPINSI || A.KD_DATI2 || A.KD_KECAMATAN || A.KD_KELURAHAN || A.KD_BLOK || A.NO_URUT || A.KD_JNS_OP AS NOP_ASAL,
                                ? AS SUBJEK_PAJAK_ID, 
                                ? AS NM_WP, 
                                ? AS JALAN_WP, 
                                NULL AS BLOK_KAV_NO_WP, 
                                ? AS RW_WP, 
                                ? AS RT_WP, 
                                ? AS KELURAHAN_WP, 
                                ? AS KOTA_WP, 
                                NULL AS KD_POS_WP, 
                                NULL AS TELP_WP, 
                                NULL AS NPWP, 
                                5 AS STATUS_PEKERJAAN_WP, 
                                NO_PERSIL, 
                                JALAN_OP, 
                                BLOK_KAV_NO_OP, 
                                RW_OP, 
                                RT_OP, 
                                KD_STATUS_CABANG, 
                                KD_STATUS_WP, 
                                KD_ZNT, 
                                LUAS_BUMI, 
                                JNS_BUMI
                            ", [
                            $objek->nik_wp,
                            $objek->nama_wp,
                            $objek->alamat_wp,
                            $objek->rw_wp,
                            $objek->rt_wp,
                            strtoupper($objek->kelurahan_wp) . ' - ' . strtoupper($objek->kecamatan_wp),
                            $objek->dati2_wp . ' - ' . $objek->propinsi_wp
                        ])
                        ->where('a.kd_kecamatan', $kd_kecamatan)
                        ->where('a.kd_kelurahan', $kd_kelurahan)
                        ->where('a.kd_blok', $kd_blok)
                        ->where('a.no_urut', $no_urut)
                        ->where('a.kd_jns_op', $kd_jns_op)
                        ->first();

                    $request = new \Illuminate\Http\Request();
                    $dataspop = (array) $dataspop; // Konversi ke array
                    $dataspop['lhp_id'] = $objek->id;
                    $dataspop['jenis_layanan'] = 'x_8';

                    $request->merge($dataspop);

                    Pajak::pendatanSubjek($request);
                    $spop = Pajak::pendataanSpop($request);
                    Pajak::pendataanObjekPajak($request, $spop);

                    Layanan_objek::where('id', $objek->id)
                        ->update([
                            'nomor_formulir' => $spop['no_formulir'],
                            'pemutakhiran_at' => Carbon::now(),
                            'pemutakhiran_by' => $userId
                        ]);



                    $cek = DB::connection('oracle_satutujuh')
                        ->table('sppt')
                        ->select('status_pembayaran_sppt')
                        ->where([
                            ['thn_pajak_sppt', Carbon::now()->year],
                            ['kd_kecamatan', $kd_kecamatan],
                            ['kd_kelurahan', $kd_kelurahan],
                            ['kd_blok', $kd_blok],
                            ['no_urut', $no_urut],
                            ['kd_jns_op', $kd_jns_op],
                        ])
                        ->first();

                    // Data untuk proses penetapan
                    $obj = [
                        'kd_propinsi'  => $kd_propinsi,
                        'kd_dati2'     => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok'      => $kd_blok,
                        'no_urut'      => $no_urut,
                        'kd_jns_op'    => $kd_jns_op,
                        'nip'          => Pajak::nip(),
                        'terbit'       => Carbon::now()->format('Ymd'),
                        'tempo'        => Carbon::createFromFormat('dmY', '3009' . Carbon::now()->year)->format('Ymd'),
                        'tahun'        => Carbon::now()->year
                    ];

                    // Jika status pembayaran masih 0 atau tidak ada data, proses penetapan SPPT
                    if (!optional($cek)->status_pembayaran_sppt || optional($cek)->status_pembayaran_sppt == '0') {
                        PenetapanSppt::proses(
                            $obj['kd_propinsi'],
                            $obj['kd_dati2'],
                            $obj['kd_kecamatan'],
                            $obj['kd_kelurahan'],
                            $obj['kd_blok'],
                            $obj['no_urut'],
                            $obj['kd_jns_op'],
                            $obj['tahun'],
                            $obj['terbit'],
                            $obj['tempo']
                        );
                    }



                    /*   $cek = DB::connection('oracle_satutujuh')->table(DB::raw("sppt"))->select('status_pembayaran_sppt')
                        ->whereraw("thn_pajak_sppt='" . date('Y') . "'
                        and kd_kecamatan='" . $kd_kecamatan . "'
                        and kd_kelurahan='" . $kd_kelurahan . "'
                        and kd_blok='" . $kd_blok . "'
                        and no_urut='" . $no_urut . "'
                        and kd_jns_op='" . $kd_jns_op . "'
                    ")->first();

                    $obj = [
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'nip' => Pajak::nip(),
                        'terbit' => date('Ymd'),
                        'tempo' => date('dmY', strtotime('3009' . date('Y'))),
                        'tahun' => date('Y')
                    ];
                    if ($cek) {
                        if ($cek->status_pembayaran_sppt == '0') {
                            // Pajak::PenentuanNjop($obj);
                            // Pajak::prosesPenetapan($obj);
                            PenetapanSppt::proses('35', '07', $obj['kd_kecamatan'], $obj['kd_kelurahan'], $obj['kd_blok'], $obj['no_urut'],  $obj['kd_jns_op'], $obj['tahun'], $obj['terbit'], $obj['tempo']);
                        }
                    } else {
                        // Pajak::PenentuanNjop($obj);
                        // Pajak::prosesPenetapan($obj);
                        PenetapanSppt::proses('35', '07', $obj['kd_kecamatan'], $obj['kd_kelurahan'], $obj['kd_blok'], $obj['no_urut'],  $obj['kd_jns_op'], $obj['tahun'], $obj['terbit'], $obj['tempo']);
                    }

 */

                    DB::commit();

                    Log::info("Berhasil menetapkan");
                    $msg = "Proses pembetulan subjek pajak berhasil";
                } catch (\Throwable $th) {
                    //throw $th;
                    $msg = $th->getMessage();
                    DB::rollBack();
                    log::emergency($th);
                }
                // log::info($msg);
            }
        }
    }
}
