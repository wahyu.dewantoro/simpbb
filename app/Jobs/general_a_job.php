<?php

namespace App\Jobs;

use App\Exports\ExcellDatageneral;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class general_a_job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $param;
    function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $array = $this->param;
        // $data = $array['data'];
        // $tahun = $array['tahun'];
        $sql = $array['sql'];
        $connection        = $array['connection'];
        $param = [
            'view' => 'general/tarikan_a',
            'sql' => $sql,
            'connection' => $connection
        ];
        $filename = $array['filename'];
        Excel::store(new ExcellDatageneral($param), $filename, 'public');
    }
}
