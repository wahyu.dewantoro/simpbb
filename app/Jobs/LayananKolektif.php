<?php

namespace App\Jobs;

use App\Helpers\LayananUpdate;
use App\Models\Layanan_objek;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use romanzipp\QueueMonitor\Traits\IsMonitored;
use Illuminate\Support\Facades\DB;
use App\Jobs\PendataanPenilaianJob;

class LayananKolektif implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    //use IsMonitored;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $setData = [];
    public $timeout = 300;
    public function __construct($setData)
    {
        $this->setData = $setData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->setData) {
            $select=[
                'kd_propinsi',
                'kd_dati2',
                'kd_kecamatan',
                'kd_kelurahan',
                'kd_blok',
                'no_urut',
                'kd_jns_op',
                'alamat_op',
                'nik_wp'
            ];
            $getData=Layanan_objek::select($select)
                    ->where(['nomor_layanan'=>$this->setData])
                    ->get();
            foreach($getData->toArray() as $item){
                $nop=[
                    $item['kd_propinsi'],
                    $item['kd_dati2'],
                    $item['kd_kecamatan'],
                    $item['kd_kelurahan'],
                    $item['kd_blok'],
                    $item['no_urut'],
                    $item['kd_jns_op'],
                ];
                LayananUpdate::updateDataDiri_dat_subjek_pajak($nop,['alamat_op'=> $item['alamat_op']],$item['nik_wp']);
            }
            dispatch(new PendataanPenilaianJob(['nomor_layanan'=>$this->setData]))->onQueue('layanan');
        }
    }
}
