<?php

namespace App\Jobs;

use App\Exports\DhkpExport;
// use App\FileGenerator;
use App\fileReport;
// use App\Helpers\Dhkp;
// use App\Kecamatan;
// use App\Kelurahan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
// use DOMPDF as PDF;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class DhkpFileGenerator implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use IsMonitored; // <---
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $param = $this->data;
        $tahun = $param['tahun'];
        $kd_kecamatan = $param['kd_kecamatan'];
        $kd_kelurahan = $param['kd_kelurahan'];
        $nm_kelurahan = $param['nm_kelurahan'];
        $nm_kecamatan = $param['nm_kecamatan'];
        $buku = [1, 2, 3, 4, 5];
        $disk = 'reports';
        $filename = 'DHKP/' . tglIndo(date('Ymd')) . '/' . $nm_kecamatan . '/' . $nm_kelurahan.' ' . time() . '.xlsx';
        Excel::store(new DhkpExport($tahun, $kd_kecamatan, $kd_kelurahan, $buku), $filename, $disk);

        $rekam = [
            'keterangan' => 'DHKP',
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'disk' => $disk,
            'filename' => $filename
        ];
        fileReport::create($rekam);
        // Log::info($filename . ' telah di buat');
    }
}
