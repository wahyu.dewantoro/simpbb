<?php

namespace App\Jobs;

use App\Helpers\Esppt;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class generateMultiNopSppt implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	//, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $variable;
    public function __construct($variable)
    {
        $this->variable = $variable;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $param = $this->variable;
        $vvp['tahun'] = $param['tahun'] ?? date('Y');
        $vvp['kd_kecamatan'] = $param['kd_kecamatan'] ?? null;
        $vvp['kd_kelurahan'] = $param['kd_kelurahan'] ?? null;
        $vvp['kd_blok'] = $param['kd_blok'] ?? null;
        $res = Esppt::multiPage($vvp, 'save');
    }
}
