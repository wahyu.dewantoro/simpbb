<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use romanzipp\QueueMonitor\Traits\IsMonitored;

class NormalisasiKelas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, IsMonitored;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // call procedure e 
        try {
            //code...
            $kec = $this->data['kecamatan'];
            $tahun = $this->data['tahun'];
            $kelas = $this->data['naik'];
            DB::connection('oracle_dua')->statement("call normalisasi_kelas_kecamatan('$tahun','$kec',$kelas)");
            Log::info("Normalisasi naik " . $kelas . " Kelas untuk " . $kec . " Selesai.");
        } catch (\Throwable $th) {
            Log::error("Normalisasi naik " . $kelas . " Kelas untuk " . $kec . " Gagal.");
            Log::error($th->getMessage());
        }
    }
}
