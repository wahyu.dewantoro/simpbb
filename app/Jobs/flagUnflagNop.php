<?php

namespace App\Jobs;

use App\Models\Data_billing;
use App\RekonHistory;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class flagUnflagNop implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $param;
    public function __construct($param)
    {
        //
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $jenis = $this->param['jenis'] ?? '';



        $kd_propinsi = $this->param['kd_propinsi'];
        $kd_dati2 = $this->param['kd_dati2'];
        $kd_kecamatan = $this->param['kd_kecamatan'];
        $kd_kelurahan = $this->param['kd_kelurahan'];
        $kd_blok = $this->param['kd_blok'];
        $no_urut = $this->param['no_urut'];
        $kd_jns_op = $this->param['kd_jns_op'];
        $kode_bank = $this->param['kode_bank'];
        $tanggal = $this->param['tanggal'];
        $batch_id = $this->param['batch_id'];
        $thn_pajak_sppt = $this->param['thn_pajak_sppt'] ?? '';
        $pokok = $this->param['pokok'] ?? null;
        $denda = $this->param['denda'] ?? null;
        $jumlah = $this->param['jumlah'] ?? null;



        if (
            $jenis <> ''  && $thn_pajak_sppt <> '' &&
            $kd_propinsi <> '' &&
            $kd_dati2 <> '' &&
            $kd_kecamatan <> '' &&
            $kd_kelurahan <> '' &&
            $kd_blok <> '' &&
            $no_urut <> '' &&
            $kd_jns_op <> ''
        ) {



            if ($jenis == '1') {
                // flagg
                $flagnop = "";
                if ($kd_blok == '999') {
                    // kobil/nota
                    $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
                    $lkobil = DB::select(db::raw("select a.*          
                    FROM billing_kolektif a
                    JOIN data_billing b
                    ON a.data_billing_id = b.data_billing_id
                    where b.kobil='$kobil'
                    and b.tahun_pajak='" . $thn_pajak_sppt . "'
                    and b.deleted_at is null
                    and a.deleted_at is null"));

                    $pengesahan=KodePengesahan();

                    foreach ($lkobil as $rk) {
                        $flagnop .= " 
                            DELETE FROM PEMBAYARAN_SPPT where kd_propinsi='" . $rk->kd_propinsi . "' and kd_dati2='" . $rk->kd_dati2 . "' 
                            and kd_kecamatan='" . $rk->kd_kecamatan . "' and kd_kelurahan='" . $rk->kd_kelurahan . "' and kd_blok='" . $rk->kd_blok . "' 
                            and no_urut='" . $rk->no_urut . "' and kd_jns_op='" . $rk->kd_jns_op . "' and thn_pajak_sppt='" . $rk->tahun_pajak . "';

                        INSERT INTO PEMBAYARAN_SPPT (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE, KD_KANWIL, KD_KANTOR, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT,kode_bank,pengesahan)
                        VALUES ('" . $rk->kd_propinsi . "', '" . $rk->kd_dati2 . "', '" . $rk->kd_kecamatan . "', '" . $rk->kd_kelurahan . "', '" . $rk->kd_blok . "', '" . $rk->no_urut . "', '" . $rk->kd_jns_op . "', '" . $rk->tahun_pajak . "', '1', '01', '01', '04', '" . $rk->denda . "', '" . $rk->total . "', TO_DATE ('" . $tanggal . "', 'yyyymmdd'), SYSDATE, '060000000000000000','" . $kode_bank . "','".$pengesahan."');  ";

                        $flagnop .= " UPDATE SPPT set status_pembayaran_sppt=1 where kd_propinsi='" . $rk->kd_propinsi . "' and kd_dati2='" . $rk->kd_dati2 . "' 
                            and kd_kecamatan='" . $rk->kd_kecamatan . "' and kd_kelurahan='" . $rk->kd_kelurahan . "' and kd_blok='" . $rk->kd_blok . "' 
                            and no_urut='" . $rk->no_urut . "' and kd_jns_op='" . $rk->kd_jns_op . "' and thn_pajak_sppt='" . $rk->tahun_pajak . "'; ";
                    }


                    DB::connection('oracle_satutujuh')->statement("BEGIN  
                        ".$flagnop."
                    END;");

                    Data_billing::where('kobil',$kobil)->where('tahun_pajak',$thn_pajak_sppt)->update([
                        'kd_status'=>1,
                        'tgl_bayar'=>db::raw("TO_DATE ('" . $tanggal . "', 'yyyymmdd')"),
                        'pengesahan'=>$pengesahan
                    ]);
                } else {
                    // nop
                    $pengesahan=KodePengesahan();
                    DB::connection('oracle_satutujuh')->statement("INSERT INTO PEMBAYARAN_SPPT (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE, KD_KANWIL, KD_KANTOR, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT,kode_bank,'pengesahan')
                    VALUES ('" . $kd_propinsi . "', '" . $kd_dati2 . "', '" . $kd_kecamatan . "', '" . $kd_kelurahan . "', '" . $kd_blok . "', '" . $no_urut . "', '" . $kd_jns_op . "', '" . $thn_pajak_sppt . "', '1', '01', '01', '04', '" . $denda . "', '" . $jumlah . "', TO_DATE ('" . $tanggal . "', 'yyyymmdd'), SYSDATE, '060000000000000000','" . $kode_bank . "','".$pengesahan."')");

                    DB::connection('oracle_satutujuh')->statement("UPDATE SPPT set status_pembayaran_sppt=1 where kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' 
                        and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' 
                        and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "' and thn_pajak_sppt='" . $thn_pajak_sppt . "'");
                }

              
            } else {
                // unflag
                $unflagnop = "";
                if ($kd_blok == '999') {
                    $kobil = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
                    $lkobil = db::select(db::raw("select a.*          
                    FROM billing_kolektif a
                    JOIN data_billing b
                    ON a.data_billing_id = b.data_billing_id
                    where b.kobil='$kobil'
                    and b.tahun_pajak='" . $thn_pajak_sppt . "'
                    and b.deleted_at is null
                    and a.deleted_at is null"));

                    foreach ($lkobil as $rk) {
                        DB::connection('oracle_satutujuh')->statement("DELETE FROM PEMBAYARAN_SPPT  where kd_propinsi='" . $rk->kd_propinsi . "' and kd_dati2='" . $rk->kd_dati2 . "' 
                                                                and kd_kecamatan='" . $rk->kd_kecamatan . "' and kd_kelurahan='" . $rk->kd_kelurahan . "' and kd_blok='" . $rk->kd_blok . "' 
                                                                and no_urut='" . $rk->no_urut . "' and kd_jns_op='" . $rk->kd_jns_op . "' and thn_pajak_sppt='" . $rk->tahun_pajak . "'");

                        DB::connection('oracle_satutujuh')->statement("UPDATE SPPT set status_pembayaran_sppt=0 where kd_propinsi='" . $rk->kd_propinsi . "' and kd_dati2='" . $rk->kd_dati2 . "' 
                                                                                and kd_kecamatan='" . $rk->kd_kecamatan . "' and kd_kelurahan='" . $rk->kd_kelurahan . "' and kd_blok='" . $rk->kd_blok . "' 
                                                                                and no_urut='" . $rk->no_urut . "' and kd_jns_op='" . $rk->kd_jns_op . "' and thn_pajak_sppt='" . $rk->tahun_pajak . " ' ");
                    }
                    Data_billing::where('kobil',$kobil)->where('tahun_pajak',$thn_pajak_sppt)->update([
                        'kd_status'=>0,
                        'tgl_bayar'=>null,
                        'pengesahan'=>null
                    ]);
                } else {
                    DB::connection('oracle_satutujuh')->statement("DELETE FROM PEMBAYARAN_SPPT  where kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' 
                    and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' 
                    and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "' and thn_pajak_sppt='" . $thn_pajak_sppt . "'");
                    DB::connection('oracle_satutujuh')->statement("UPDATE SPPT set status_pembayaran_sppt=0 where kd_propinsi='" . $kd_propinsi . "' and kd_dati2='" . $kd_dati2 . "' 
                            and kd_kecamatan='" . $kd_kecamatan . "' and kd_kelurahan='" . $kd_kelurahan . "' and kd_blok='" . $kd_blok . "' 
                            and no_urut='" . $no_urut . "' and kd_jns_op='" . $kd_jns_op . "' and thn_pajak_sppt='" . $thn_pajak_sppt . " ' ");
                }

                
            
            }

            $datahistory = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'thn_pajak_sppt' => $thn_pajak_sppt,
                'pokok' => $pokok,
                'denda' => $denda,
                'jumlah' => $jumlah,
                'tanggal_pembayaran' => new Carbon($tanggal),
                'rekon_batch_id' => $batch_id,
                'flag' => $jenis
            ];
            RekonHistory::insert($datahistory);
        }
    }
}
