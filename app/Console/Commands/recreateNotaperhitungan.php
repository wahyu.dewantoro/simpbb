<?php

namespace App\Console\Commands;

use App\Models\Layanan_objek;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class recreateNotaperhitungan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notaperhitungan:recreate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = DB::table(DB::raw("data_billing a"))
            ->join(
                DB::raw('(SELECT layanan_objek_id, MAX(data_billing_id) as data_billing_id FROM nota_perhitungan GROUP BY layanan_objek_id)  b'),
                'a.data_billing_id',
                '=',
                'b.data_billing_id'
            )
            ->join('layanan_objek as c', 'c.id', '=', 'b.layanan_objek_id')
            ->join('layanan as d', 'd.nomor_layanan', '=', 'c.nomor_layanan')
            ->whereNull('a.tgl_bayar')
            ->where('a.expired_at', '<', DB::raw('SYSDATE')) // Jika bukan Oracle, ganti dengan NOW() atau CURRENT_TIMESTAMP
            ->whereRaw("TO_CHAR(a.created_at, 'yyyy') >= '2024' and jenis_layanan_id='7'") // Jika bukan Oracle, sesuaikan format tanggalnya
            ->orderByRaw('d.nomor_layanan asc ,b.layanan_objek_id asc')
            ->select([
                'b.layanan_objek_id',
                'd.jenis_layanan_id',
                'd.jenis_layanan_nama',
                DB::raw("nvl(c.nop_gabung,c.id) layanan_objek_parent"),
                DB::raw("to_char (a.created_at, 'yyyy') tahun_penelitian")
            ])
            ->limit(5)
            ->get();

        foreach ($data as $row) {
            // nop proses 
            $layanan_objek_id = $row->layanan_objek_id;
            $tahun = $row->tahun_penelitian;
            $na = DB::table('tbl_spop')->where('layanan_objek_id', $layanan_objek_id)->first();
            $np = preg_replace('/[^0-9]/', '', $na->nop_proses);
            $nop_proses = [
                'kd_propinsi' => substr($np, 0, 2),
                'kd_dati2' => substr($np, 2, 2),
                'kd_kecamatan' => substr($np, 4, 3),
                'kd_kelurahan' => substr($np, 7, 3),
                'kd_blok' => substr($np, 10, 3),
                'no_urut' => substr($np, 13, 4),
                'kd_jns_op' => substr($np, 17, 1)
            ];

            $jenis_layanan_id = $row->jenis_layanan_id;

            // Ambil data layanan objek berdasarkan kondisi jenis_layanan_id
            $layananObjek = ($jenis_layanan_id != 7)
                ? Layanan_objek::where('id', $row->layanan_objek_parent)->get()
                : Layanan_objek::where('nop_gabung', $row->layanan_objek_parent)->get();

            // Format data menggunakan map untuk lebih ringkas
            $nop_asal = $layananObjek->map(function ($lo) {
                return [
                    'sppt_oltp.kd_propinsi' => $lo->kd_propinsi,
                    'sppt_oltp.kd_dati2' => $lo->kd_dati2,
                    'sppt_oltp.kd_kecamatan' => $lo->kd_kecamatan,
                    'sppt_oltp.kd_kelurahan' => $lo->kd_kelurahan,
                    'sppt_oltp.kd_blok' => $lo->kd_blok,
                    'sppt_oltp.no_urut' => $lo->no_urut,
                    'sppt_oltp.kd_jns_op' => $lo->kd_jns_op,
                ];
            });


            // mencari komposisi beban
            $persen = 100;
            if ($row->jenis_layanan_id == '6') {

                // mencari total luas induk, dari data pecahan
                $subQuery1 = DB::table('layanan_objek')
                    ->selectRaw('NVL(nop_gabung, id)')
                    ->where('id', $layanan_objek_id);

                $subQuery2 = DB::table('layanan_objek')
                    ->selectRaw('NVL(nop_gabung, id)')
                    ->where('id', $layanan_objek_id);

                $firstQuery = DB::table('layanan_objek as lo')
                    ->select([
                        'lo.id',
                        DB::raw("CASE 
                                        WHEN lo.nop_gabung IS NULL 
                                        THEN CASE 
                                                WHEN l.jenis_layanan_id = 6 THEN lo.sisa_pecah_total_gabung 
                                                ELSE lo.luas_bumi 
                                            END 
                                        ELSE lo.luas_bumi 
                                    END AS luas_bumi")
                    ])
                    ->join('layanan as l', 'l.nomor_layanan', '=', 'lo.nomor_layanan')
                    ->whereIn('lo.nop_gabung', $subQuery1)
                    ->orWhereIn('lo.id', $subQuery2);

                $nestedSubQuery = DB::table('layanan_objek')
                    ->selectRaw('CAST(nop_gabung AS NUMBER) AS nop_gabung')
                    ->where('id', $layanan_objek_id)
                    ->unionAll(
                        DB::table('layanan_objek')
                            ->selectRaw('CAST(id AS NUMBER)')
                            ->where('id', $layanan_objek_id)
                            ->whereNull('nop_gabung')
                    );

                $nestedJoin = DB::table('layanan_objek as a')
                    ->select([
                        'a.id',
                        'a.kd_propinsi',
                        'a.kd_dati2',
                        'a.kd_kecamatan',
                        'a.kd_kelurahan',
                        'a.kd_blok',
                        'a.no_urut',
                        'a.kd_jns_op'
                    ])
                    ->joinSub($nestedSubQuery, 'b', function ($join) {
                        $join->on('a.id', '=', 'b.nop_gabung');
                    });

                $mainJoin = DB::table('layanan_objek as a')
                    ->select([
                        'a.id',
                        'a.kd_propinsi',
                        'a.kd_dati2',
                        'a.kd_kecamatan',
                        'a.kd_kelurahan',
                        'a.kd_blok',
                        'a.no_urut',
                        'a.kd_jns_op'
                    ])
                    ->joinSub($nestedJoin, 'b', function ($join) {
                        $join->on('a.kd_propinsi', '=', 'b.kd_propinsi')
                            ->on('a.kd_dati2', '=', 'b.kd_dati2')
                            ->on('a.kd_kecamatan', '=', 'b.kd_kecamatan')
                            ->on('a.kd_kelurahan', '=', 'b.kd_kelurahan')
                            ->on('a.kd_blok', '=', 'b.kd_blok')
                            ->on('a.no_urut', '=', 'b.no_urut')
                            ->on('a.kd_jns_op', '=', 'b.kd_jns_op')
                            ->whereRaw('a.id != b.id')
                            ->whereNotNull('a.nomor_formulir');
                    });

                $secondQuery = DB::table('layanan_objek as dd')
                    ->select(['dd.id', 'dd.luas_bumi'])
                    ->joinSub($mainJoin, 'ee', 'ee.id', '=', 'dd.nop_gabung');

                $luas_pecah = $firstQuery->unionAll($secondQuery)->get();

                $total_luas_bumi = 0;
                foreach ($luas_pecah as $row) {
                    $total_luas_bumi += $row->luas_bumi;
                }
                $persentase_pecah = [];
                $arra = [];
                foreach ($luas_pecah as $row) {
                    $arra[$row->id] = ['luas' => $row->luas_bumi, 'total' => $total_luas_bumi];
                    $persentase_pecah[$row->id] = round(($row->luas_bumi / $total_luas_bumi) * 100, 2);
                }
                //mencari tagihan dari induk untuk pecahan
                $persen = $persentase_pecah[$layanan_objek_id] ?? 100;
            }

            // mencari tagihan asal
            $kolom = $jenis_layanan_id == '6' ? 'sppt_oltp.pbb_terhutang_sppt' : 'sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT';
            $tagihan = DB::connection("oracle_spo")
                ->table('sppt_oltp')
                ->selectRaw("
                    sppt_oltp.kd_propinsi, sppt_oltp.kd_dati2, sppt_oltp.kd_kecamatan, 
                    sppt_oltp.kd_kelurahan, sppt_oltp.kd_blok, sppt_oltp.no_urut, sppt_oltp.kd_jns_op, 
                    sppt_oltp.thn_pajak_sppt AS tahun_pajak,
                    ROUND(" . $kolom . " * (" . $persen . " / 100)) AS pbb, 
                    sppt_oltp.nm_wp_sppt AS nm_wp,
                    ROUND(" . $kolom . " * (" . $persen . " / 100)) AS pokok,  
                    ROUND(sppt_oltp.denda * (" . $persen . " / 100)) AS denda,
                    ROUND(" . $kolom . " * (" . $persen . " / 100)) + ROUND(sppt_oltp.denda * (" . $persen . " / 100)) AS total
                ")

                ->whereRaw("sppt_oltp.thn_pajak_sppt <'" . $tahun . "'
                        AND  sppt_oltp.PBB_YG_HARUS_DIBAYAR_SPPT>0 
                        and sppt_oltp.status_pembayaran_sppt!='3'");
            $tagihan = $tagihan->where(function ($sql) use ($nop_asal) {
                foreach ($nop_asal as $value) {
                    $sql->orWhere(function ($dep) use ($value) {
                        foreach ($value as $i => $a) {
                            # code...
                            $dep->where($i, $a);
                        }
                    });
                }
            });


            $tagihanfix = $tagihan->get();
            $ArTagihan = [];
            foreach ($tagihanfix as $tagihan) {
                if ($tagihan->pokok > 0) {
                    $ArTagihan[] = [
                        'kd_propinsi' => $tagihan->kd_propinsi,
                        'kd_dati2' => $tagihan->kd_dati2,
                        'kd_kecamatan' => $tagihan->kd_kecamatan,
                        'kd_kelurahan' => $tagihan->kd_kelurahan,
                        'kd_blok' => $tagihan->kd_blok,
                        'no_urut' => $tagihan->no_urut,
                        'kd_jns_op' => $tagihan->kd_jns_op,
                        'tahun_pajak' => $tagihan->tahun_pajak,
                        'pbb' => $tagihan->pbb,
                        'nm_wp' => $tagihan->nm_wp,
                        'pokok' => $tagihan->pokok,
                        'denda' => $tagihan->denda,
                        'total' => $tagihan->total
                    ];
                }
            }


            /*      // tagihan nop proses
            $tagihanDua = DB::connection("oracle_spo")->table("sppt_oltp")
                ->select(db::raw("sppt_oltp.kd_propinsi,
            sppt_oltp.kd_dati2,
            sppt_oltp.kd_Kecamatan,
            sppt_oltp.kd_kelurahan,
            sppt_oltp.kd_blok,
            sppt_oltp.no_urut,
            sppt_oltp.kd_jns_op,
            sppt_oltp.thn_pajak_sppt  tahun_pajak,
                    PBB_YG_HARUS_DIBAYAR_SPPT  pbb,nm_wp_sppt nm_wp,PBB_YG_HARUS_DIBAYAR_SPPT pokok   , denda denda,PBB_YG_HARUS_DIBAYAR_SPPT+denda total"))
                ->where($nop_proses)
                ->whereRaw("status_pembayaran_sppt='0' and thn_pajak_sppt>='" . $tahun . "'")
                ->get();

            foreach ($tagihanDua as $tagihan) {
                if ($tagihan->pokok > 0) {
                    $ArTagihan[] = [
                        'kd_propinsi' => $tagihan->kd_propinsi,
                        'kd_dati2' => $tagihan->kd_dati2,
                        'kd_kecamatan' => $tagihan->kd_kecamatan,
                        'kd_kelurahan' => $tagihan->kd_kelurahan,
                        'kd_blok' => $tagihan->kd_blok,
                        'no_urut' => $tagihan->no_urut,
                        'kd_jns_op' => $tagihan->kd_jns_op,
                        'tahun_pajak' => $tagihan->tahun_pajak,
                        'pbb' => $tagihan->pbb,
                        'nm_wp' => $tagihan->nm_wp,
                        'pokok' => $tagihan->pokok,
                        'denda' => $tagihan->denda,
                        'total' => $tagihan->total
                    ];
                }
            } */

            // Output hasil dalam format JSON yang lebih rapi
            $this->info(json_encode([
                'nop_proses' => $nop_proses,
                'persen' => $persen,
                'nop_asal' => $nop_asal,
                'tagihan' => $ArTagihan
            ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        }
    }
}
