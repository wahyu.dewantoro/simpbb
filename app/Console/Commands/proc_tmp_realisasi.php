<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class proc_tmp_realisasi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:tmp_realisasi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate data tmp_realisasi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->info('Sedang memproses....');
        DB::beginTransaction();
        try {
            // db::connection('oracle_satutujuh')->table('tmp_realisasi')->delete();
            /*  $data = DB::connection('oracle_satutujuh')->table('core_sppt')
            ->whereraw("thn_pajak_sppt IN (TO_CHAR (SYSDATE, 'yyyy'),
            TO_CHAR (SYSDATE, 'yyyy') - 1)")
                ->select(db::raw("kd_propinsi,
                kd_dati2,
                kd_kecamatan,
                kd_kelurahan,
                kd_blok,
                no_urut,
                kd_jns_op,
                thn_pajak_sppt,
                status_pembayaran_sppt,
                buku,
                pbb_yg_harus_dibayar_sppt-potongan pbb,
                TRUNC (tgl_bayar) tgl_bayar,
                pokok_bayar (1,
                             kd_propinsi,
                             kd_dati2,
                             kd_kecamatan,
                             kd_kelurahan,
                             kd_blok,
                             no_urut,
                             kd_jns_op,
                             thn_pajak_sppt)
                   pokok,
                pokok_bayar (2,
                             kd_propinsi,
                             kd_dati2,
                             kd_kecamatan,
                             kd_kelurahan,
                             kd_blok,
                             no_urut,
                             kd_jns_op,
                             thn_pajak_sppt)
                   denda,
                pokok_bayar (3,
                             kd_propinsi,
                             kd_dati2,
                             kd_kecamatan,
                             kd_kelurahan,
                             kd_blok,
                             no_urut,
                             kd_jns_op,
                             thn_pajak_sppt)
                   jumlah,
                SYSDATE created_at"))->get()->toArray();
            $i = 1;
            foreach (array_chunk($data, 10000) as $row) {
                $this->info('Memproses 10000 data ke ' . $i);
                $ins = [
                    'kd_propinsi' => $row->kd_propinsi,
                    'kd_dati2' => $row->kd_dati2,
                    'kd_kecamatan' => $row->kd_kecamatan,
                    'kd_kelurahan' => $row->kd_kelurahan,
                    'kd_blok' => $row->kd_blok,
                    'no_urut' => $row->no_urut,
                    'kd_jns_op' => $row->kd_jns_op,
                    'thn_pajak_sppt' => $row->thn_pajak_sppt,
                    'status_pembayaran' => $row->status_pembayaran,
                    'buku' => $row->buku,
                    'pbb' => $row->pbb,
                    'tgl_bayar' => $row->tgl_bayar,
                    'pokok' => $row->pokok,
                    'denda' => $row->denda,
                    'jumlah' => $row->jumlah,
                    'created_at' => db::raw('sysdate')
                ];
                db::connection('oracle_satutujuh')->table('tmp_realisasi')->insert($ins);


                $this->info('Berhasil insert 1000 data ke ' . $i);
                $i++;
            } */

            $max = 100000;
            $total = DB::connection('oracle_satutujuh')->table('core_sppt')
            ->whereraw("thn_pajak_sppt IN (TO_CHAR (SYSDATE, 'yyyy'),TO_CHAR (SYSDATE, 'yyyy') - 1)")
            ->selectraw("count(1) jumlah")->first();
            $pages = ceil($total->jumlah / $max);
            for ($i = 1; $i < ($pages + 1); $i++) {
                $start = microtime(true);
                $offset = (($i - 1)  * $max);
                $start = ($offset == 0 ? 0 : ($offset + 1));
                $legacy = DB::connection('oracle_satutujuh')->table('core_sppt')
                    ->whereraw("thn_pajak_sppt IN (TO_CHAR (SYSDATE, 'yyyy'),
            TO_CHAR (SYSDATE, 'yyyy') - 1)")
                    ->select(db::raw("kd_propinsi,
                kd_dati2,
                kd_kecamatan,
                kd_kelurahan,
                kd_blok,
                no_urut,
                kd_jns_op,
                thn_pajak_sppt,
                status_pembayaran_sppt,
                buku,
                pbb_yg_harus_dibayar_sppt-potongan pbb,
                TRUNC (tgl_bayar) tgl_bayar,
                pokok_bayar (1,
                             kd_propinsi,
                             kd_dati2,
                             kd_kecamatan,
                             kd_kelurahan,
                             kd_blok,
                             no_urut,
                             kd_jns_op,
                             thn_pajak_sppt)
                   pokok,
                pokok_bayar (2,
                             kd_propinsi,
                             kd_dati2,
                             kd_kecamatan,
                             kd_kelurahan,
                             kd_blok,
                             no_urut,
                             kd_jns_op,
                             thn_pajak_sppt)
                   denda,
                pokok_bayar (3,
                             kd_propinsi,
                             kd_dati2,
                             kd_kecamatan,
                             kd_kelurahan,
                             kd_blok,
                             no_urut,
                             kd_jns_op,
                             thn_pajak_sppt)
                   jumlah,
                SYSDATE created_at"))
                    ->skip($start)->take($max)->get();

               /*  foreach ($legacy as $row) {
                    $ins = [
                        'kd_propinsi' => $row->kd_propinsi,
                        'kd_dati2' => $row->kd_dati2,
                        'kd_kecamatan' => $row->kd_kecamatan,
                        'kd_kelurahan' => $row->kd_kelurahan,
                        'kd_blok' => $row->kd_blok,
                        'no_urut' => $row->no_urut,
                        'kd_jns_op' => $row->kd_jns_op,
                        'thn_pajak_sppt' => $row->thn_pajak_sppt,
                        'status_pembayaran' => $row->status_pembayaran,
                        'buku' => $row->buku,
                        'pbb' => $row->pbb,
                        'tgl_bayar' => $row->tgl_bayar,
                        'pokok' => $row->pokok,
                        'denda' => $row->denda,
                        'jumlah' => $row->jumlah,
                        'created_at' => db::raw('sysdate')
                    ];
                    db::connection('oracle_satutujuh')->table('tmp_realisasi')->insert($ins);
                } */
                sleep(0);
                $end = (microtime(true) - $start);
                $this->info('Page ' . $i . '/' . $pages . ', ' . round($end, 2) . ' Seconds');
            }
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            $this->info($th->getMessage());
        }
    }
}


