<?php

namespace App\Console\Commands;

use App\Helpers\PenetapanSppt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MenetapkanSppt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'menetapkan:sppt {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Menetapkan SPPT berdasarkan tahun dan kode kecamatan/kelurahan';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tahun = $this->option('tahun') ?? date('Y');
        $kdKecamatan = $this->option('kd_kecamatan');
        $kdKelurahan = $this->option('kd_kelurahan');

        $this->info("Memulai proses penetapan SPPT untuk tahun {$tahun}");

        $query = DB::connection('oracle_satutujuh')
            ->table('dat_objek_pajak')
            ->join('dat_op_bumi', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
            })
            ->leftjoin('dat_nir',function($join) use ($tahun) {
             $join->on('dat_nir.kd_propinsi','=','dat_op_bumi.kd_propinsi')
                    ->on('dat_nir.kd_dati2','=','dat_op_bumi.kd_dati2')
                    ->on('dat_nir.kd_kecamatan','=','dat_op_bumi.kd_kecamatan')
                    ->on('dat_nir.kd_kelurahan','=','dat_op_bumi.kd_kelurahan')
                    ->on('dat_nir.kd_znt','=','dat_op_bumi.kd_znt')
                    ->where('dat_nir.thn_nir_znt',$tahun);
            })
            ->select(
                'dat_objek_pajak.kd_propinsi',
                'dat_objek_pajak.kd_dati2',
                'dat_objek_pajak.kd_kecamatan',
                'dat_objek_pajak.kd_kelurahan',
                'dat_objek_pajak.kd_blok',
                'dat_objek_pajak.no_urut',
                'dat_objek_pajak.kd_jns_op'
            )
            ->where('dat_objek_pajak.total_luas_bumi', '>', 0)
            ->where('dat_objek_pajak.jns_transaksi_op', '!=', '3')
            ->whereIn('dat_op_bumi.jns_bumi', ['1', '2', '3'])
            ->whereraw("dat_nir.kd_znt is not null");

        // Filter berdasarkan input
        if ($kdKecamatan) {
            $query->where('dat_objek_pajak.kd_kecamatan', $kdKecamatan);
        }

        if ($kdKelurahan) {
            $query->where('dat_objek_pajak.kd_kelurahan', $kdKelurahan);
        }

        // $query->where('dat_objek_pajak.kd_blok','017');
        // Ambil total data
        $data = $query->get();
        $jumlah = $data->count();

        if ($jumlah === 0) {
            $this->info('Tidak ada data yang ditemukan.');
            return Command::SUCCESS;
        }

        $this->info("Total data ditemukan: {$jumlah}");

        $no = 1;

        $total = count($data); // Total data yang akan diproses
        $current = 0;          // Mulai dari 0

        foreach ($data as $rk) {
            $current++;

            $tglTerbit = $tahun . '0102';
            $tglJatuhTempo = $tahun . '0831';
            $nop = implode('.', [
                $rk->kd_propinsi,
                $rk->kd_dati2,
                $rk->kd_kecamatan,
                $rk->kd_kelurahan,
                $rk->kd_blok,
                $rk->no_urut,
                $rk->kd_jns_op
            ]);

            // $this->info("Proses {$no}/{$jumlah} : {$nop} [" . now() . "]");

            // Proses Penetapan
            $res = PenetapanSppt::proses(
                $rk->kd_propinsi,
                $rk->kd_dati2,
                $rk->kd_kecamatan,
                $rk->kd_kelurahan,
                $rk->kd_blok,
                $rk->no_urut,
                $rk->kd_jns_op,
                $tahun,
                $tglTerbit,
                $tglJatuhTempo
            );

            // Log hasil
            // $this->info(json_encode($res));
            Log::info(json_encode($res));

            $no++;

            $percentage = intval(($current / $total) * 100);
            echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
            usleep(100000); // Delay untuk efek progress
        } 
        echo "\n";
        $this->info('Proses selesai.');
        return Command::SUCCESS;
    }
}
