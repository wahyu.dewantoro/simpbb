<?php

namespace App\Console\Commands;

use App\Helpers\Pajak;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PajakCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'pajak:penetapan {tahun?} {sismiop?} {kecamatan?} {kelurahan?} {blok?} {urut?} {jns_op?}';
    // {--jenis=} {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=} { --kd_blok=} { --no_urut=} { --kd_jns_op=}
    // {--sismiop=}
    // 35.07.250.003.019.0416.0
    protected $signature = 'pajak:penetapan {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=} { --kd_blok=} { --no_urut=} { --kd_jns_op=}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proses penetapan PBB , urutan tahun pajak,sistep(0)/sismiop(1), kecamatan,kelurahan [opsional],kd_blok [opsional],no_urut [opsional],kd_jns_op [opsional] ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $tahun = $this->option('tahun') ?? date('Y');
        // $sismiop = $this->option('sismiop') ?? 1;
        $kecamatan = $this->option('kd_kecamatan') ?? null;
        $kelurahan = $this->option('kd_kelurahan') ?? null;
        $blok = $this->option('kd_blok') ?? null;
        $urut = $this->option('no_urut') ?? null;
        $jns_op = $this->option('kd_jns_op') ?? null;

        /* $tahun = $this->argument('tahun') ?? date('Y');
        $sismiop = $this->argument('sismiop') ?? null;
        $kecamatan = $this->argument('kecamatan') ?? null;
        $kelurahan = $this->argument('kelurahan') ?? null;
        $blok = $this->argument('blok') ?? null;
        $urut = $this->argument('urut') ?? null;
        $jns_op = $this->argument('jns_op') ?? null; */


        // || $sismiop == null
        if ($kecamatan == null) {
            $this->alert('sismiop/sistep dan kode kecamatan harus di sertakan');
        } else {

            // $where = "  jns_transaksi_op<>'3' and jns_bumi <>'4' and a.kd_kecamatan='$kecamatan' ";
            $where = " total_luas_bumi>0 and  jns_transaksi_op != '3'
            AND jns_bumi IN ('1', '2', '3') ";

            /* if ($sismiop == 1) {
                $where .= " and a.kd_blok<>'000' ";
            } else {
                $where .= " and a.kd_blok='000' ";
            } */

            if ($kecamatan != null) {
                $where .= " and a.kd_kecamatan='$kecamatan' ";
            }

            if ($kelurahan != null) {
                $where .= " and a.kd_kelurahan='$kelurahan' ";
            }

            if ($blok != null) {
                $where .= " and a.kd_blok='$blok' ";
            }

            if ($urut != null) {
                $where .= " and a.no_urut='$urut' ";
            }

            if ($jns_op != null) {
                $where .= " and a.kd_jns_op='$jns_op' ";
            }


            $op = DB::connection('oracle_satutujuh')->table(DB::raw('dat_objek_pajak a'))
                ->join(db::raw('dat_op_bumi b'), function ($join) {
                    $join->on(db::raw('a.kd_propinsi'), '=', db::raw('b.kd_propinsi'))
                        ->on(db::raw('a.kd_dati2'), '=', db::raw('b.kd_dati2'))
                        ->on(db::raw('a.kd_kecamatan'), '=', db::raw('b.kd_kecamatan'))
                        ->on(db::raw('a.kd_kelurahan'), '=', db::raw('b.kd_kelurahan'))
                        ->on(db::raw('a.kd_blok'), '=', db::raw('b.kd_blok'))
                        ->on(db::raw('a.no_urut'), '=', db::raw('b.no_urut'))
                        ->on(db::raw('a.kd_jns_op'), '=', db::raw('b.kd_jns_op'));
                })
                ->select(db::raw('a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op,b.luas_bumi,kd_znt,no_bumi'))
                ->whereraw($where)
                ->orderbyraw("a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut")
                ->get();
            $totalop = $op->count();
            $this->info(angka($totalop) . ' objek pajak akan di proses');
            $i = 1;

            // $total = $op->count();
            // $nomor = 1;

            $this->info("Sedang mempersiapkan data....");
            foreach ($op as  $row) {
                $this->info("Objek ke " . angka($i) . " dari " . angka($totalop) . " objek ");
                $vPenilaian = [
                    'kd_propinsi' => $row->kd_propinsi,
                    'kd_dati2' => $row->kd_dati2,
                    'kd_kecamatan' => $row->kd_kecamatan,
                    'kd_kelurahan' => $row->kd_kelurahan,
                    'kd_blok' => $row->kd_blok,
                    'no_urut' => $row->no_urut,
                    'kd_jns_op' => $row->kd_jns_op,
                    'no_bumi' => $row->no_bumi,
                    'kd_znt' => $row->kd_znt,
                    'luas_bumi' => $row->luas_bumi,
                    'tahun' => $tahun,
                ];
                // proses penilaian / Penentuan NJOP
                Pajak::PenentuanNjop($vPenilaian);
                $sppt = DB::connection('oracle_satutujuh')->table('sppt')
                    ->select('status_pembayaran_sppt')
                    ->where('thn_pajak_sppt', $tahun)
                    ->whereraw("kd_kecamatan='" . $row->kd_kecamatan . "' and kd_kelurahan='" . $row->kd_kelurahan . "' and kd_blok='" . $row->kd_blok . "' and no_urut='" . $row->no_urut . "' and kd_jns_op='" . $row->kd_jns_op . "' ")->first();

                $penetapan = 1;
                if ($sppt) {
                    if ($sppt->status_pembayaran_sppt <> '0') {
                        $penetapan = 0;
                    }
                }

                $nop = $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                if ($penetapan == 1) {
                    $penetapan = [
                        'kd_propinsi' => $row->kd_propinsi,
                        'kd_dati2' => $row->kd_dati2,
                        'kd_kecamatan' => $row->kd_kecamatan,
                        'kd_kelurahan' => $row->kd_kelurahan,
                        'kd_blok' => $row->kd_blok,
                        'no_urut' => $row->no_urut,
                        'kd_jns_op' => $row->kd_jns_op,
                        'tahun' => $tahun,
                        'terbit' => $tahun . '0102',
                        'tempo' => $tahun . '0831',
                        'nip' => '000000000202203086'
                    ];
                    Pajak::prosesPenetapan($penetapan);
                    $sql_potongan = " hitung_potongan('" . $row->kd_kecamatan . "','" . $row->kd_kelurahan . "','" . $row->kd_blok . "','" . $row->no_urut . "','" . $row->kd_jns_op . "','" . $tahun . "'); ";
                    DB::connection("oracle_satutujuh")->statement(DB::raw("Begin " . $sql_potongan . " commit; end;"));


                    $this->info('[' . angka($i) . '/' . angka($totalop) . '] ' .  $nop . ' : Telah di lakukan penilaian dan penetapan PBB tahun pajak ' . $tahun);
                } else {
                    $this->info('[' . angka($i) . '/' . angka($totalop) . '] ' .  $nop . ' : Hanya dilakukan penilaian NJOP , karena sudah lunas');
                }
                $i++;

                // $nomor++;
            }

            $this->info("SELESAI");
        }
    }
}
