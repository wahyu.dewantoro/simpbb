<?php

namespace App\Console\Commands;

use App\Helpers\Pajak;
use App\Helpers\RekonQris;
use App\Jobs\BatchSknjop;
use App\Jobs\disposisiPenelitian;
use App\Jobs\excelLunasLimaTahunJob;
use App\Jobs\generatePiutangTahunJob;
use App\Jobs\NormalisasiKelas;
use App\Jobs\normalisasiTarifSppt;
use App\Jobs\PendataanPenilaianJob;
use App\Jobs\RekonPembayaranJob;
use App\Kecamatan;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use App\Unitkerja;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class xxgeneralCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testrekon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**d
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $data = DB::select("select to_char(created_at,'ddmmyyyy') tanggal,kobil,data_billing_id
        from v_qris
        where   trunc(created_at) between to_date('01012024','ddmmyyyy')
        and to_date('21022024','ddmmyyyy')
        and amount_pay is null");

        // Log::info($data)

        foreach ($data as $row) {
            # code...
            $this->info(json_encode($row));
            $pay = [
                'data_billing_id' => trim($row->data_billing_id),
                'kobil' => trim($row->kobil),
                'tanggal' => $row->tanggal
            ];
            RekonQris::proses($pay);
        }

        /* 
        $begin = new DateTime('2024-01-01');
        $end = new DateTime('2024-02-22');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {
            $tgl = $dt->format("Ymd");
            $this->info("Sedang narik data :" . $tgl);
            Artisan::call('besembilan:pbb', ['--tanggal' => $tgl]);
        }
 */


        // 	$id = $this->option('id') ?? '';

        // //$nopel = '20230221074 ';
        // $this->info('Sistem sedang berjalan, mohon ditunggu!');
        // $res = DB::table('layanan_objek')->select('id')->wherenotnull('pemutakhiran_at');
        //     /*if($nopel<>''){
        // 	$res=$res->whereraw("nomor_layanan='$nopel'");
        // 	}			*/
        // 	//if($id<>''){
        // 	$res=$res->whereraw("id='$id'");
        // 	//}
        // 	$res=$res->get();


        // foreach ($res as $row) {
        //     $this->info($row->id);
        //     Pajak::coreCancel($row->id);
        //     Layanan_objek::where('id', $row->id)->update(['pemutakhiran_at' => null, 'pemutakhiran_by' => null, 'nomor_formulir' => null]);
        // }

        // $this->info('Selesai');
        // $this->info(date('Y').' -> '.substr(date('Y'),2,2)  );

        // dispatch(new PendataanPenilaianJob(['nomor_layanan' =>'20220616006']))->onQueue('testing');

        /* 
        $objek = [
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '200', 'kd_kelurahan' => '012', 'kd_blok' => '012', 'no_urut' => '0244', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '020', 'kd_kelurahan' => '007', 'kd_blok' => '000', 'no_urut' => '8139', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '020', 'kd_kelurahan' => '007', 'kd_blok' => '000', 'no_urut' => '8140', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '240', 'kd_kelurahan' => '012', 'kd_blok' => '012', 'no_urut' => '0126', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '014', 'kd_blok' => '004', 'no_urut' => '0022', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '009', 'kd_blok' => '004', 'no_urut' => '0135', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '002', 'kd_blok' => '020', 'no_urut' => '0050', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '130', 'kd_kelurahan' => '005', 'kd_blok' => '002', 'no_urut' => '0063', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '130', 'kd_kelurahan' => '013', 'kd_blok' => '021', 'no_urut' => '0094', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '014', 'kd_blok' => '023', 'no_urut' => '0338', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '015', 'kd_blok' => '013', 'no_urut' => '0314', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '018', 'kd_blok' => '019', 'no_urut' => '0065', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '060', 'kd_kelurahan' => '004', 'kd_blok' => '000', 'no_urut' => '5669', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '110', 'kd_kelurahan' => '004', 'kd_blok' => '019', 'no_urut' => '0026', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '110', 'kd_kelurahan' => '013', 'kd_blok' => '001', 'no_urut' => '0308', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '190', 'kd_kelurahan' => '007', 'kd_blok' => '008', 'no_urut' => '0218', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '180', 'kd_kelurahan' => '004', 'kd_blok' => '005', 'no_urut' => '0176', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '270', 'kd_kelurahan' => '002', 'kd_blok' => '007', 'no_urut' => '0081', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '260', 'kd_kelurahan' => '008', 'kd_blok' => '042', 'no_urut' => '0138', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '200', 'kd_kelurahan' => '002', 'kd_blok' => '018', 'no_urut' => '0162', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '270', 'kd_kelurahan' => '008', 'kd_blok' => '001', 'no_urut' => '0264', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '040', 'kd_kelurahan' => '005', 'kd_blok' => '000', 'no_urut' => '7625', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '014', 'kd_blok' => '005', 'no_urut' => '0289', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '260', 'kd_kelurahan' => '009', 'kd_blok' => '044', 'no_urut' => '0142', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '250', 'kd_kelurahan' => '015', 'kd_blok' => '009', 'no_urut' => '0453', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '200', 'kd_kelurahan' => '010', 'kd_blok' => '015', 'no_urut' => '0089', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '004', 'kd_blok' => '003', 'no_urut' => '0152', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '100', 'kd_kelurahan' => '011', 'kd_blok' => '006', 'no_urut' => '0035', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '170', 'kd_kelurahan' => '005', 'kd_blok' => '021', 'no_urut' => '0131', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '270', 'kd_kelurahan' => '008', 'kd_blok' => '004', 'no_urut' => '0258', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '070', 'kd_kelurahan' => '009', 'kd_blok' => '020', 'no_urut' => '0076', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '140', 'kd_kelurahan' => '023', 'kd_blok' => '006', 'no_urut' => '0234', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '161', 'kd_kelurahan' => '001', 'kd_blok' => '011', 'no_urut' => '0290', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '190', 'kd_kelurahan' => '007', 'kd_blok' => '004', 'no_urut' => '0324', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '190', 'kd_kelurahan' => '012', 'kd_blok' => '025', 'no_urut' => '0058', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '160', 'kd_kelurahan' => '007', 'kd_blok' => '004', 'no_urut' => '0072', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '100', 'kd_kelurahan' => '005', 'kd_blok' => '032', 'no_urut' => '0048', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '130', 'kd_kelurahan' => '005', 'kd_blok' => '013', 'no_urut' => '0009', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '140', 'kd_kelurahan' => '016', 'kd_blok' => '024', 'no_urut' => '0149', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '130', 'kd_kelurahan' => '012', 'kd_blok' => '014', 'no_urut' => '0236', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '002', 'kd_blok' => '005', 'no_urut' => '0174', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '240', 'kd_kelurahan' => '004', 'kd_blok' => '006', 'no_urut' => '0177', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '140', 'kd_kelurahan' => '018', 'kd_blok' => '058', 'no_urut' => '0144', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '260', 'kd_kelurahan' => '004', 'kd_blok' => '014', 'no_urut' => '0197', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '171', 'kd_kelurahan' => '004', 'kd_blok' => '041', 'no_urut' => '0170', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '018', 'kd_blok' => '010', 'no_urut' => '0160', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '100', 'kd_kelurahan' => '015', 'kd_blok' => '013', 'no_urut' => '0280', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '120', 'kd_kelurahan' => '015', 'kd_blok' => '025', 'no_urut' => '0091', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '008', 'kd_blok' => '008', 'no_urut' => '0220', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '210', 'kd_kelurahan' => '011', 'kd_blok' => '014', 'no_urut' => '0158', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '040', 'kd_kelurahan' => '004', 'kd_blok' => '000', 'no_urut' => '1261', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '040', 'kd_kelurahan' => '008', 'kd_blok' => '000', 'no_urut' => '3278', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '130', 'kd_kelurahan' => '011', 'kd_blok' => '011', 'no_urut' => '0163', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '070', 'kd_kelurahan' => '007', 'kd_blok' => '019', 'no_urut' => '0499', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '010', 'kd_kelurahan' => '003', 'kd_blok' => '001', 'no_urut' => '1774', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '140', 'kd_kelurahan' => '006', 'kd_blok' => '001', 'no_urut' => '0127', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '140', 'kd_kelurahan' => '021', 'kd_blok' => '016', 'no_urut' => '0231', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '002', 'kd_blok' => '012', 'no_urut' => '0140', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '009', 'kd_blok' => '004', 'no_urut' => '0206', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '150', 'kd_kelurahan' => '015', 'kd_blok' => '016', 'no_urut' => '0222', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '161', 'kd_kelurahan' => '001', 'kd_blok' => '011', 'no_urut' => '0289', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '030', 'kd_kelurahan' => '005', 'kd_blok' => '006', 'no_urut' => '0173', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '141', 'kd_kelurahan' => '005', 'kd_blok' => '008', 'no_urut' => '0221', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '141', 'kd_kelurahan' => '008', 'kd_blok' => '004', 'no_urut' => '0374', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '190', 'kd_kelurahan' => '010', 'kd_blok' => '014', 'no_urut' => '0086', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '190', 'kd_kelurahan' => '012', 'kd_blok' => '021', 'no_urut' => '0129', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '060', 'kd_kelurahan' => '001', 'kd_blok' => '000', 'no_urut' => '6457', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '060', 'kd_kelurahan' => '003', 'kd_blok' => '000', 'no_urut' => '4689', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '060', 'kd_kelurahan' => '008', 'kd_blok' => '000', 'no_urut' => '4087', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '060', 'kd_kelurahan' => '012', 'kd_blok' => '000', 'no_urut' => '2913', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '060', 'kd_kelurahan' => '012', 'kd_blok' => '000', 'no_urut' => '2991', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '160', 'kd_kelurahan' => '001', 'kd_blok' => '008', 'no_urut' => '0321', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '120', 'kd_kelurahan' => '011', 'kd_blok' => '029', 'no_urut' => '0085', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '080', 'kd_kelurahan' => '008', 'kd_blok' => '000', 'no_urut' => '1213', 'kd_jns_op' => '7'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '180', 'kd_kelurahan' => '004', 'kd_blok' => '005', 'no_urut' => '0181', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '180', 'kd_kelurahan' => '010', 'kd_blok' => '006', 'no_urut' => '0258', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '110', 'kd_kelurahan' => '013', 'kd_blok' => '001', 'no_urut' => '0309', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '110', 'kd_kelurahan' => '013', 'kd_blok' => '001', 'no_urut' => '0310', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '171', 'kd_kelurahan' => '004', 'kd_blok' => '040', 'no_urut' => '0129', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '171', 'kd_kelurahan' => '007', 'kd_blok' => '007', 'no_urut' => '0062', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '260', 'kd_kelurahan' => '002', 'kd_blok' => '005', 'no_urut' => '0161', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '260', 'kd_kelurahan' => '010', 'kd_blok' => '028', 'no_urut' => '0157', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '260', 'kd_kelurahan' => '002', 'kd_blok' => '004', 'no_urut' => '0107', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '250', 'kd_kelurahan' => '002', 'kd_blok' => '030', 'no_urut' => '0212', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '012', 'kd_blok' => '033', 'no_urut' => '0121', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '250', 'kd_kelurahan' => '016', 'kd_blok' => '005', 'no_urut' => '0119', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '270', 'kd_kelurahan' => '008', 'kd_blok' => '015', 'no_urut' => '0460', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '140', 'kd_kelurahan' => '020', 'kd_blok' => '016', 'no_urut' => '0161', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '210', 'kd_kelurahan' => '002', 'kd_blok' => '007', 'no_urut' => '0336', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '230', 'kd_kelurahan' => '006', 'kd_blok' => '011', 'no_urut' => '0130', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '230', 'kd_kelurahan' => '007', 'kd_blok' => '008', 'no_urut' => '0093', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '220', 'kd_kelurahan' => '008', 'kd_blok' => '012', 'no_urut' => '0247', 'kd_jns_op' => '0'],
            ['kd_propinsi' => '35', 'kd_dati2' => '07', 'kd_kecamatan' => '100', 'kd_kelurahan' => '005', 'kd_blok' => '024', 'no_urut' => '0205', 'kd_jns_op' => '0']
        ];

        $proses = [];
        foreach ($objek as $row) {
            
             $sppt = DB::connection('oracle_satutujuh')->table('sppt')
                // ->select('status_pembayaran_sppt')
                ->where('thn_pajak_sppt', '2022')
                ->whereraw("kd_kecamatan='" . $row['kd_kecamatan'] . "' and kd_kelurahan='" . $row['kd_kelurahan'] . "' and kd_blok='" . $row['kd_blok'] . "' and no_urut='" . $row['no_urut'] . "' and kd_jns_op='" . $row['kd_jns_op'] . "' ")->first();
            if ($sppt) {
                $proses[] = 'pajak:penetapan 2022 1 '.$row['kd_kecamatan'].' '.$row['kd_kelurahan'].' '.$row['kd_blok'].' '.$row['no_urut'].' '.$row['kd_jns_op'];
            } 
        }
        log::info($proses); */

        /*   foreach($objek as $obj){
            $tahun = '2022';
            $sismiop = '1';
            $kecamatan =$obj['kd_kecamatan'];
            $kelurahan = $obj['kd_kelurahan'];
            $blok =$obj['kd_blok'];
            $urut = $obj['no_urut'];
            $jns_op = $obj['kd_jns_op'];
    
            if ($kecamatan == null || $sismiop == null) {
                $this->alert('sismiop/sistep dan kode kecamatan harus di sertakan');
            } else {
    
                $where = "  jns_transaksi_op<>'3' and jns_bumi <>'4' and a.kd_kecamatan='$kecamatan' ";
    
                if ($sismiop == 1) {
                    $where .= " and a.kd_blok<>'000' ";
                } else {
                    $where .= " and a.kd_blok='000' ";
                }
    
                if ($kelurahan != null) {
                    $where .= " and a.kd_kelurahan='$kelurahan' ";
                }
    
                if ($blok != null) {
                    $where .= " and a.kd_blok='$blok' ";
                }
    
                if ($urut != null) {
                    $where .= " and a.no_urut='$urut' ";
                }
    
                if ($jns_op != null) {
                    $where .= " and a.kd_jns_op='$jns_op' ";
                }
    
    
                $op = DB::connection('oracle_satutujuh')->table(DB::raw('dat_objek_pajak a'))
                    ->join(db::raw('dat_op_bumi b'), function ($join) {
                        $join->on(db::raw('a.kd_propinsi'), '=', db::raw('b.kd_propinsi'))
                            ->on(db::raw('a.kd_dati2'), '=', db::raw('b.kd_dati2'))
                            ->on(db::raw('a.kd_kecamatan'), '=', db::raw('b.kd_kecamatan'))
                            ->on(db::raw('a.kd_kelurahan'), '=', db::raw('b.kd_kelurahan'))
                            ->on(db::raw('a.kd_blok'), '=', db::raw('b.kd_blok'))
                            ->on(db::raw('a.no_urut'), '=', db::raw('b.no_urut'))
                            ->on(db::raw('a.kd_jns_op'), '=', db::raw('b.kd_jns_op'));
                    })
                    ->select(db::raw('a.kd_propinsi,a.kd_dati2,a.kd_kecamatan,a.kd_kelurahan,a.kd_blok,a.no_urut,a.kd_jns_op,b.luas_bumi,kd_znt,no_bumi'))
                    ->whereraw($where)->get();
                $totalop = $op->count();
                $this->info(angka($totalop) . ' objek pajak akan di proses');
                $i = 1;
                foreach ($op as  $row) {
                    $vPenilaian = [
                        'kd_propinsi' => $row->kd_propinsi,
                        'kd_dati2' => $row->kd_dati2,
                        'kd_kecamatan' => $row->kd_kecamatan,
                        'kd_kelurahan' => $row->kd_kelurahan,
                        'kd_blok' => $row->kd_blok,
                        'no_urut' => $row->no_urut,
                        'kd_jns_op' => $row->kd_jns_op,
                        'no_bumi' => $row->no_bumi,
                        'kd_znt' => $row->kd_znt,
                        'luas_bumi' => $row->luas_bumi,
                        'tahun' => $tahun,
                    ];
                    // proses penilaian / Penentuan NJOP
                    Pajak::PenentuanNjop($vPenilaian);
                    $sppt = DB::connection('oracle_satutujuh')->table('sppt')
                        ->select('status_pembayaran_sppt')
                        ->where('thn_pajak_sppt', $tahun)
                        ->whereraw("kd_kecamatan='" . $row->kd_kecamatan . "' and kd_kelurahan='" . $row->kd_kelurahan . "' and kd_blok='" . $row->kd_blok . "' and no_urut='" . $row->no_urut . "' and kd_jns_op='" . $row->kd_jns_op . "' ")->first();
    
                    $penetapan = 1;
                    if ($sppt) {
                        if ($sppt->status_pembayaran_sppt <> '0') {
                            $penetapan = 0;
                        }
                    }
    
                    $nop = $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op;
                    if ($penetapan == 1) {
                        $penetapan = [
                            'kd_propinsi' => $row->kd_propinsi,
                            'kd_dati2' => $row->kd_dati2,
                            'kd_kecamatan' => $row->kd_kecamatan,
                            'kd_kelurahan' => $row->kd_kelurahan,
                            'kd_blok' => $row->kd_blok,
                            'no_urut' => $row->no_urut,
                            'kd_jns_op' => $row->kd_jns_op,
                            'tahun' => $tahun,
                            'terbit' => $tahun . date('md'),
                            'tempo' => $tahun . '0930',
                            'nip' => '000000000202203086'
                        ];
                        Pajak::prosesPenetapan($penetapan);
                        $this->info('[' . $i . '/' . $totalop . ']' .  $nop . ' : Telah di lakukan penilaian dan penetapan PBB tahun pajak ' . $tahun);
                    } else {
                        $this->info('[' . $i . '/' . $totalop . ']' .  $nop . ' : Hanya dilakukan penilaian NJOP , karena sudah lunas');
                    }
                    $i++;
                }
            }
        }
 */
        //    Log::info('Running job at '.time()) ;


        /*   $np=Layanan::get();
        foreach($np as $rp){
            disposisiPenelitian::dispatch($rp->nomor_layanan)->onQueue('rekon');
            
            // dispatch((new disposisiPenelitian($rp->nomor_layanan))->onQueue('rekon'));

    
        }
         */
        // dispatch(new disposisiPenelitian('20220301008'));

        // proses pendataan ejaan subjek pajak
        // PendataanPenilaianJob::dispatch(['nomor_layanan' => '20220224003']);
        // dispatch(new PendataanPenilaianJob(['nomor_layanan' => '20220224003']));


        // rekon
        /* $param['kode_bank']='114';
        $param['tanggal']='20220124';
        dispatch(new RekonPembayaranJob($param))->onQueue('rekon'); */

        // BatchSknjop
        // BatchSknjop::dispatch('98866a7a-7287-11ec-a723-3c970ed77c8f');
        // dispatch(new BatchSknjop('98866a7a-7287-11ec-a723-3c970ed77c8f'));



        /*  // normalisasi kelas 
        $kecamatan = Unitkerja::Kecamatan()->select(DB::raw("kd_kecamatan,lpad(trim(kd_upt),3,'0') kode_upt"))
        ->whereraw("kd_kecamatan in ('220', '150', '180', '190', '240', '250', '260', '270')")->get();
        $naik = 1;
            
        $tahun = '2022';

    
        foreach ($kecamatan as $index=> $kec) {
            $data = ['tahun' => $tahun, 'kecamatan' => $kec->kd_kecamatan, 'naik' => $naik];
            dispatch(new NormalisasiKelas($data))->onQueue("fast".$index);
        }

 */
        // normalisasi sppt
        // loop kecamatan
        /* $kecamatan = Unitkerja::Kecamatan()->select(DB::raw("kd_kecamatan,lpad(trim(kd_upt),3,'0') kode_upt"))->get();
        $tahun = '2022';
        foreach ($kecamatan as $kec) {
            $data=['tahun'=>$tahun,'kecamatan'=>$kec->kd_kecamatan];
            dispatch(new normalisasiTarifSppt($data))->onQueue($kec->kode_upt);
        } */


        /*         $kecamatan = Kecamatan::get();
        foreach ($kecamatan as $rk) {
            dispatch(new excelLunasLimaTahunJob($rk->kd_kecamatan));
            
            // ->onQueue($rk->kd_kecamatan);
        }
 */

        /* $begin = new DateTime("2021-12-01");
        $end   = new DateTime("2021-11-01");

        for ($i = $begin; $i >= $end; $i->modify('-1 day')) {
            $tanggal=$i->format("Ymd");
            $res="php artisan report:rankingkecamatan ".$tanggal;
            $this->info($res);
        } */
        /* for($tahun=2014; $tahun<=2021;$tahun++){
            dispatch(new generatePiutangTahunJob($tahun));
        } */
    }
}
