<?php

namespace App\Console\Commands;

use App\Helpers\PembatalanObjek;
use App\Helpers\PenetapanSppt;
use App\Jobs\NarikBesembilanBjJob;
use App\Jobs\RekonPembayaranJob;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DemoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trial:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // $data = DB::connection("oracle_satutujuh")->select("select thn_awal,thn_akhir,njop_min,njop_max,nilai_tarif 
        // from tarif where 2024 between thn_awal and thn_akhir");

        $seconds = 6000;
        $data = Cache::remember(md5('cache_tarif'), $seconds, function () {
            $data = DB::connection("oracle_satutujuh")->table('tarif')->selectRaw('thn_awal,thn_akhir,njop_min,njop_max,nilai_tarif')->get()->toArray();
            $tmp = [];
            foreach ($data as $row) {
                $tmp[] = [
                    'thn_awal' => (int)$row->thn_awal,
                    'thn_akhir' => (int)$row->thn_akhir,
                    'njop_min' => (int)$row->njop_min,
                    'njop_max' => (int)$row->njop_max,
                    'nilai_tarif' => (float)$row->nilai_tarif,
                ];
            }
            return $tmp;
        });


        // get max tarif di tahun 
        $tahun = '2024';
        $tm = 0;
        foreach ($data as $row) {
            if ($row['thn_awal'] <= $tahun && $row['thn_akhir'] >= $tahun) {
                if ($tm < $row['nilai_tarif']) {
                    $tm = $row['nilai_tarif'];
                }
            }
        }
        $tarif = $tm;
        $nilai = "9900000";

        foreach ($data as $row) {
            if ($row['thn_awal'] <= $tahun && $row['thn_akhir'] >= $tahun &&    $row['njop_min'] <= $nilai && $row['njop_max'] >= $nilai) {
                $tarif = $row['nilai_tarif'];
                break;
            }
        }
        $this->info($tarif);

        /* Log::info($tmp);
        $this->info(json_encode($tmp)); */
        // Log::info($dat

        /* $kel = DB::connection("oracle_satutujuh")->select("select kd_kecamatan,kd_kelurahan
        from ref_kelurahan 
        order by kd_kecamatan,kd_kelurahan
        ");

        foreach ($kel as $row) {
            $this->info("Proses kecamatan ".$row->kd_kecamatan." kelurahan ".$row->kd_kelurahan);
            Artisan::call('report:sppt', ['--tahun' => 2024, '--kd_kecamatan' => $row->kd_kecamatan, '--kd_kelurahan' => $row->kd_kelurahan]);
        }
        $this->info("selesai"); */
    }
}
