<?php

namespace App\Console\Commands;

use App\Models\Data_billing;
use App\PembayaranSppt;
use App\RekonBatch;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Stmt\TryCatch;

class RekonBayarDua extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rekon-bayar  {--tp=}   {--tanggal=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'penyamaan pembayaran';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $tp = $this->option('tp') ?? '';
        $tanggal = $this->option('tanggal') ?? '';
        if ($tp <> '' && $tanggal <> '') {
            // ambil data rekon batch
            $this->info('Mencari data rekon');
            $rb = RekonBatch::whereraw("trunc(tanggal_pembayaran)=to_date('" . $tanggal . "','yyyy-mm-dd') and kode_bank='" . $tp . "'")
                ->select(db::raw("max(id) rekon_id"))
                ->first();
            // dd($rb->rekon_id);

            if ($rb->rekon_id != null) {
                // Identifikasi nop NOP Kobil
                $this->info('Identifikasi nop non kode billing');
                $rekon_batch_id = $rb->rekon_id;

                // mencari perbedaan jumlah bayar atau bahkan kosong
                /*  $rekon_to_bayar_nop = DB::select(db::raw("SELECT rekon.*, bayar.denda denda_bayar, bayar.jumlah jumlah_bayar
                                                                FROM (SELECT kd_propinsi,
                                                                            kd_dati2,
                                                                            kd_kecamatan,
                                                                            kd_kelurahan,
                                                                            kd_blok,
                                                                            no_urut,
                                                                            kd_jns_op,
                                                                            thn_pajak_sppt,
                                                                            denda,
                                                                            jumlah,
                                                                            TRUNC (tanggal_pembayaran) tanggal
                                                                        FROM rekon_nop
                                                                    WHERE rekon_batch_id = '" . $rekon_batch_id . "' AND kd_blok != '999') rekon
                                                                    LEFT JOIN
                                                                    (SELECT bayar.*,
                                                                            kobil.kd_propinsi_kobil,
                                                                            kobil.kd_dati2_kobil,
                                                                            kobil.kd_kecamatan_kobil,
                                                                            kobil.kd_kelurahan_kobil,
                                                                            kobil.kd_blok_kobil,
                                                                            kobil.no_urut_kobil,
                                                                            kobil.kd_jns_op_kobil,
                                                                            kobil.thn_pajak_sppt_kobil
                                                                        FROM (SELECT kd_propinsi,
                                                                                    kd_dati2,
                                                                                    kd_kecamatan,
                                                                                    kd_kelurahan,
                                                                                    kd_blok,
                                                                                    no_urut,
                                                                                    kd_jns_op,
                                                                                    thn_pajak_sppt,
                                                                                    denda_sppt denda,
                                                                                    jml_sppt_yg_dibayar jumlah,
                                                                                    TRUNC (tgl_pembayaran_sppt) tanggal,
                                                                                    kode_bank
                                                                                FROM pbb.pembayaran_sppt
                                                                            WHERE     kode_bank = '" . $tp . "'
                                                                                    AND TRUNC (tgl_pembayaran_sppt) =
                                                                                            TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd')) bayar
                                                                            LEFT JOIN
                                                                            (SELECT billing_Kolektif.kd_propinsi,
                                                                                    billing_Kolektif.kd_dati2,
                                                                                    billing_Kolektif.kd_kecamatan,
                                                                                    billing_Kolektif.kd_kelurahan,
                                                                                    billing_Kolektif.kd_blok,
                                                                                    billing_Kolektif.no_urut,
                                                                                    billing_Kolektif.kd_jns_op,
                                                                                    billing_Kolektif.tahun_pajak thn_pajak_sppt,
                                                                                    denda,
                                                                                    total jumlah,
                                                                                    TRUNC (tgl_bayar) tanggal,
                                                                                    data_billing.kd_propinsi kd_propinsi_kobil,
                                                                                    data_billing.kd_dati2 kd_dati2_kobil,
                                                                                    data_billing.kd_kecamatan kd_kecamatan_kobil,
                                                                                    data_billing.kd_kelurahan kd_kelurahan_kobil,
                                                                                    data_billing.kd_blok kd_blok_kobil,
                                                                                    data_billing.no_urut no_urut_kobil,
                                                                                    data_billing.kd_jns_op kd_jns_op_kobil,
                                                                                    data_billing.tahun_pajak thn_pajak_sppt_kobil
                                                                                FROM data_billing
                                                                                    JOIN billing_kolektif
                                                                                        ON data_billing.data_billing_id =
                                                                                            BILLING_KOLEKTIF.data_billing_id
                                                                            WHERE kd_status = '1') kobil
                                                                                ON     bayar.kd_propinsi = kobil.kd_propinsi
                                                                                AND bayar.kd_dati2 = kobil.kd_dati2
                                                                                AND bayar.kd_kecamatan = kobil.kd_kecamatan
                                                                                AND bayar.kd_kelurahan = kobil.kd_kelurahan
                                                                                AND bayar.kd_blok = kobil.kd_blok
                                                                                AND bayar.no_urut = kobil.no_urut
                                                                                AND bayar.kd_jns_op = kobil.kd_jns_op
                                                                                AND bayar.thn_pajak_sppt = kobil.thn_pajak_sppt
                                                                                AND bayar.tanggal = kobil.tanggal
                                                                    WHERE kobil.kd_propinsi_kobil IS NULL) bayar
                                                                        ON     rekon.kd_propinsi = bayar.kd_propinsi
                                                                        AND rekon.kd_dati2 = bayar.kd_dati2
                                                                        AND rekon.kd_kecamatan = bayar.kd_kecamatan
                                                                        AND rekon.kd_kelurahan = bayar.kd_kelurahan
                                                                        AND rekon.kd_blok = bayar.kd_blok
                                                                        AND rekon.no_urut = bayar.no_urut
                                                                        AND rekon.kd_jns_op = bayar.kd_jns_op
                                                                        AND rekon.thn_pajak_sppt = bayar.thn_pajak_sppt
                                                                        AND rekon.tanggal = bayar.tanggal
                                                            where rekon.jumlah<>bayar.jumlah or bayar.jumlah is null")); */

                $rekon_to_bayar_nop = DB::select(db::raw("select * from (
                                                                SELECT kode_bank,
                                                                kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,
                                                                                   a.tanggal_pembayaran,
                                                                                   a.pokok pokok,
                                                                                   nvl(a.denda,0) denda,
                                                                                   a.jumlah,
                                                                                   ( select sum(jml_sppt_yg_dibayar)
                                                                                   from pbb.pembayaran_sppt aa
                                                                                   left join SIM_PBB.DATA_BILLING bb on aa.pengesahan=bb.pengesahan 
                                                                                   where trunc(tgl_pembayaran_sppt)=trunc(b.tanggal_pembayaran)
                                                                                   and aa.kode_bank=b.kode_bank
                                                                                   and bb.pengesahan is  null
                                                                                   and aa.kd_kecamatan=a.kd_kecamatan
                                                                                   and aa.kd_kelurahan=a.kd_kelurahan
                                                                                   and aa.kd_blok=a.kd_blok
                                                                                   and aa.no_urut=a.no_urut
                                                                                   and aa.kd_jns_op=a.kd_jns_op
                                                                                   and aa.thn_pajak_sppt=a.thn_pajak_sppt
                                                                                   ) jml_bayar
                                                                              FROM sim_pbb.rekon_nop a
                                                                                   JOIN
                                                                                   (  SELECT tanggal_pembayaran, kode_bank, MAX (id) rekon_batch_id
                                                                                        FROM sim_pbb.rekon_batch a
                                                                                       WHERE     a.kode_bank = '$tp'
                                                                                             AND a.tanggal_pembayaran =TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd')
                                                                                    GROUP BY kode_bank, tanggal_pembayaran) b
                                                                                      ON a.rekon_batch_id = b.rekon_batch_id
                                                                                      and a.kd_blok!='999')
                                                                                      where jumlah<>jml_bayar
                                                                                      or jml_bayar is null"));
                try {
                    $fer = 1;
                    foreach ($rekon_to_bayar_nop as $row) {
                        $ntpdnop = date('YmdHis') + $fer;

                        if ($row->jml_bayar != $row->jumlah && $row->jml_bayar != '') {
                            // lakukan update pembayaran

                            $npp=$row->kd_propinsi.".". $row->kd_dati2.".". $row->kd_kecamatan.".". $row->kd_kelurahan.".". $row->kd_blok.".". $row->no_urut.".". $row->kd_jns_op." : tahun pajak: ".$row->thn_pajak_sppt;

                            $this->info("penyamaan nominal pembayaran nop " . $npp . " , denda:" . $row->denda . " jml:" . ($row->jumlah));

                            $cek = DB::table(db::raw("pbb.pembayaran_sppt"))
                                ->select(db::raw("to_char(tgl_pembayaran_sppt,'yyyy-mm-dd') tanggal,pengesahan"))
                                ->where('kd_propinsi', $row->kd_propinsi)
                                ->where('kd_dati2', $row->kd_dati2)
                                ->where('kd_kecamatan', $row->kd_kecamatan)
                                ->where('kd_kelurahan', $row->kd_kelurahan)
                                ->where('kd_blok', $row->kd_blok)
                                ->where('no_urut', $row->no_urut)
                                ->where('kd_jns_op', $row->kd_jns_op)
                                ->where('thn_pajak_sppt', $row->thn_pajak_sppt)
                                ->where('kode_bank', $row->kode_bank)->get();
                            foreach ($cek as $item) {
                                DB::statement(db::raw("begin  spo.proc_hapus_bayar('" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $row->thn_pajak_sppt . "',to_date('" . $tanggal . "','yyyy-mm-dd'),'" . $row->kode_bank . "','" . $item->pengesahan . "'); commit; end;"));
                            }

                            //  proses flaging
                            $dnd=$row->denda<>''?$row->denda:0;

                            DB::statement(DB::raw("begin SPO.proc_pembayaran ('" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $row->thn_pajak_sppt . "',
                            TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'),
                            " . $dnd . ",
                            " . $row->jumlah . ",
                            '" . $ntpdnop . "',
                            '" . $tp . "'); commit; end;"));
                        } elseif ($row->jml_bayar != $row->jumlah && $row->jml_bayar == '') {
                            $dnd=$row->denda<>''?$row->denda:0;
                            $this->info("flag pembayaran nop");
                            DB::statement(DB::raw("begin SPO.proc_pembayaran ('" . $row->kd_propinsi . "', '" . $row->kd_dati2 . "', '" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "', '" . $row->kd_blok . "', '" . $row->no_urut . "', '" . $row->kd_jns_op . "', '" . $row->thn_pajak_sppt . "',
                            TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'),
                            " . $dnd . ",
                            " . $row->jumlah . ",
                            '" . $ntpdnop . "',
                            '" . $tp . "'); commit; end;"));
                        } else {
                            // by
                        }
                        $fer++;
                    }
                } catch (\Throwable $th) {
                    $this->error($th->getMessage());
                }

                // mencari pembayaran nop yang tidak ada di rekon
                $bayar_to_rekon_nop = DB::select(db::raw("SELECT bayar.kd_propinsi,
                                                                        bayar.kd_dati2,
                                                                        bayar.kd_kecamatan,
                                                                        bayar.kd_kelurahan,
                                                                        bayar.kd_blok,
                                                                        bayar.no_urut,
                                                                        bayar.kd_jns_op,
                                                                        bayar.thn_pajak_sppt,
                                                                        rekon.jumlah
                                                                    FROM (SELECT bayar.*,
                                                                                kobil.kd_propinsi_kobil,
                                                                                kobil.kd_dati2_kobil,
                                                                                kobil.kd_kecamatan_kobil,
                                                                                kobil.kd_kelurahan_kobil,
                                                                                kobil.kd_blok_kobil,
                                                                                kobil.no_urut_kobil,
                                                                                kobil.kd_jns_op_kobil,
                                                                                kobil.thn_pajak_sppt_kobil
                                                                        FROM (SELECT kd_propinsi,
                                                                                        kd_dati2,
                                                                                        kd_kecamatan,
                                                                                        kd_kelurahan,
                                                                                        kd_blok,
                                                                                        no_urut,
                                                                                        kd_jns_op,
                                                                                        thn_pajak_sppt,
                                                                                        denda_sppt denda,
                                                                                        jml_sppt_yg_dibayar jumlah,
                                                                                        TRUNC (tgl_pembayaran_sppt) tanggal,
                                                                                        kode_bank
                                                                                FROM pbb.pembayaran_sppt
                                                                                WHERE     kode_bank = '" . $tp . "'
                                                                                        AND TRUNC (tgl_pembayaran_sppt) =
                                                                                            TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd')) bayar
                                                                                LEFT JOIN
                                                                                (SELECT billing_Kolektif.kd_propinsi,
                                                                                        billing_Kolektif.kd_dati2,
                                                                                        billing_Kolektif.kd_kecamatan,
                                                                                        billing_Kolektif.kd_kelurahan,
                                                                                        billing_Kolektif.kd_blok,
                                                                                        billing_Kolektif.no_urut,
                                                                                        billing_Kolektif.kd_jns_op,
                                                                                        billing_Kolektif.tahun_pajak thn_pajak_sppt,
                                                                                        denda,
                                                                                        total jumlah,
                                                                                        TRUNC (tgl_bayar) tanggal,
                                                                                        data_billing.kd_propinsi kd_propinsi_kobil,
                                                                                        data_billing.kd_dati2 kd_dati2_kobil,
                                                                                        data_billing.kd_kecamatan kd_kecamatan_kobil,
                                                                                        data_billing.kd_kelurahan kd_kelurahan_kobil,
                                                                                        data_billing.kd_blok kd_blok_kobil,
                                                                                        data_billing.no_urut no_urut_kobil,
                                                                                        data_billing.kd_jns_op kd_jns_op_kobil,
                                                                                        data_billing.tahun_pajak thn_pajak_sppt_kobil
                                                                                FROM data_billing
                                                                                        JOIN billing_kolektif
                                                                                        ON data_billing.data_billing_id =
                                                                                                BILLING_KOLEKTIF.data_billing_id
                                                                                WHERE kd_status = '1') kobil
                                                                                ON     bayar.kd_propinsi = kobil.kd_propinsi
                                                                                    AND bayar.kd_dati2 = kobil.kd_dati2
                                                                                    AND bayar.kd_kecamatan = kobil.kd_kecamatan
                                                                                    AND bayar.kd_kelurahan = kobil.kd_kelurahan
                                                                                    AND bayar.kd_blok = kobil.kd_blok
                                                                                    AND bayar.no_urut = kobil.no_urut
                                                                                    AND bayar.kd_jns_op = kobil.kd_jns_op
                                                                                    AND bayar.thn_pajak_sppt = kobil.thn_pajak_sppt
                                                                                    AND bayar.tanggal = kobil.tanggal
                                                                        WHERE kobil.kd_propinsi_kobil IS NULL) bayar
                                                                        LEFT JOIN (SELECT kd_propinsi,
                                                                                        kd_dati2,
                                                                                        kd_kecamatan,
                                                                                        kd_kelurahan,
                                                                                        kd_blok,
                                                                                        no_urut,
                                                                                        kd_jns_op,
                                                                                        thn_pajak_sppt,
                                                                                        denda,
                                                                                        jumlah,
                                                                                        TRUNC (tanggal_pembayaran) tanggal
                                                                                    FROM rekon_nop
                                                                                    WHERE rekon_batch_id = '" . $rekon_batch_id . "' AND kd_blok != '999') rekon
                                                                        ON     rekon.kd_propinsi = bayar.kd_propinsi
                                                                            AND rekon.kd_dati2 = bayar.kd_dati2
                                                                            AND rekon.kd_kecamatan = bayar.kd_kecamatan
                                                                            AND rekon.kd_kelurahan = bayar.kd_kelurahan
                                                                            AND rekon.kd_blok = bayar.kd_blok
                                                                            AND rekon.no_urut = bayar.no_urut
                                                                            AND rekon.kd_jns_op = bayar.kd_jns_op
                                                                            AND rekon.thn_pajak_sppt = bayar.thn_pajak_sppt
                                                                            AND rekon.tanggal = bayar.tanggal
                                                                    WHERE rekon.jumlah IS NULL"));


                foreach ($bayar_to_rekon_nop as $row) {
                    $nop = $row->kd_propinsi . '.' .
                        $row->kd_dati2 . '.' .
                        $row->kd_kecamatan . '.' .
                        $row->kd_kelurahan . '.' .
                        $row->kd_blok . '.' .
                        $row->no_urut . '.' .
                        $row->kd_jns_op . ' -> ' .
                        $row->thn_pajak_sppt;

                    $this->info('Proses unflag nop non kobil : ' . $nop);
                    $s_flag = "delete from  pbb.pembayaran_sppt  where kd_propinsi='" . $row->kd_propinsi . "' and kd_dati2='" . $row->kd_dati2 . "' and kd_kecamatan='" . $row->kd_kecamatan . "' and kd_kelurahan='" . $row->kd_kelurahan . "' and kd_blok='" . $row->kd_blok . "' and no_urut='" . $row->no_urut . "' and kd_jns_op='" . $row->kd_jns_op . "' and thn_pajak_sppt='" . $row->thn_pajak_sppt . "' and trunc(tgl_pembayaran_sppt)=to_date('" . $tanggal . "','yyyy-mm-dd') and trim(kode_bank)='" . $tp . "' ";
                    DB::connection("oracle_satutujuh")->statement(db::raw("begin " . $s_flag . "; commit; end;"));
                }


                //  data kobil  ke pembayaran
                $this->info('Identifikasi kode billing');
                $kobil_ke_bayar = DB::select(db::raw("SELECT rekon.kd_propinsi,
                                                                rekon.kd_dati2,
                                                                rekon.kd_kecamatan,
                                                                rekon.kd_kelurahan,
                                                                rekon.kd_blok,
                                                                rekon.no_urut,
                                                                rekon.kd_jns_op,
                                                                rekon.thn_pajak_sppt,
                                                                nvl(rekon.denda,0) denda ,
                                                                rekon.jumlah,
                                                                bayar.denda denda_bayar,
                                                                bayar.jumlah jumlah_bayar,
                                                                bayar.kode_bank,
                                                                bayar.pengesahan
                                                        FROM (SELECT kd_propinsi,
                                                                        kd_dati2,
                                                                        kd_kecamatan,
                                                                        kd_kelurahan,
                                                                        kd_blok,
                                                                        no_urut,
                                                                        kd_jns_op,
                                                                        thn_pajak_sppt,
                                                                        denda,
                                                                        jumlah,
                                                                        TRUNC (tanggal_pembayaran) tanggal
                                                                FROM rekon_nop
                                                                WHERE rekon_batch_id = '" . $rekon_batch_id . "' AND kd_blok = '999') rekon
                                                                LEFT JOIN
                                                                (  SELECT kd_propinsi_kobil kd_propinsi,
                                                                        kd_dati2_kobil kd_dati2,
                                                                        kd_kecamatan_kobil kd_kecamatan,
                                                                        kd_kelurahan_kobil kd_kelurahan,
                                                                        kd_blok_kobil kd_blok,
                                                                        no_urut_kobil no_urut,
                                                                        kd_jns_op_kobil kd_jns_op,
                                                                        thn_pajak_sppt_kobil thn_pajak_sppt,
                                                                        tanggal,
                                                                        kode_bank,
                                                                        pengesahan,
                                                                        SUM (denda) denda,
                                                                        SUM (jumlah) jumlah
                                                                    FROM (SELECT bayar.*,
                                                                                kobil.kd_propinsi_kobil,
                                                                                kobil.kd_dati2_kobil,
                                                                                kobil.kd_kecamatan_kobil,
                                                                                kobil.kd_kelurahan_kobil,
                                                                                kobil.kd_blok_kobil,
                                                                                kobil.no_urut_kobil,
                                                                                kobil.kd_jns_op_kobil,
                                                                                kobil.thn_pajak_sppt_kobil
                                                                            FROM (SELECT kd_propinsi,
                                                                                        kd_dati2,
                                                                                        kd_kecamatan,
                                                                                        kd_kelurahan,
                                                                                        kd_blok,
                                                                                        no_urut,
                                                                                        kd_jns_op,
                                                                                        thn_pajak_sppt,
                                                                                        denda_sppt denda,
                                                                                        jml_sppt_yg_dibayar jumlah,
                                                                                        TRUNC (tgl_pembayaran_sppt) tanggal,
                                                                                        kode_bank,
                                                                                        pengesahan
                                                                                    FROM pbb.pembayaran_sppt
                                                                                    WHERE     kode_bank = '" . $tp . "'
                                                                                        AND TRUNC (tgl_pembayaran_sppt) =
                                                                                                TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'))
                                                                                bayar
                                                                                LEFT JOIN
                                                                                (SELECT billing_Kolektif.kd_propinsi,
                                                                                        billing_Kolektif.kd_dati2,
                                                                                        billing_Kolektif.kd_kecamatan,
                                                                                        billing_Kolektif.kd_kelurahan,
                                                                                        billing_Kolektif.kd_blok,
                                                                                        billing_Kolektif.no_urut,
                                                                                        billing_Kolektif.kd_jns_op,
                                                                                        billing_Kolektif.tahun_pajak thn_pajak_sppt,
                                                                                        denda,
                                                                                        total jumlah,
                                                                                        TRUNC (tgl_bayar) tanggal,
                                                                                        data_billing.kd_propinsi kd_propinsi_kobil,
                                                                                        data_billing.kd_dati2 kd_dati2_kobil,
                                                                                        data_billing.kd_kecamatan kd_kecamatan_kobil,
                                                                                        data_billing.kd_kelurahan kd_kelurahan_kobil,
                                                                                        data_billing.kd_blok kd_blok_kobil,
                                                                                        data_billing.no_urut no_urut_kobil,
                                                                                        data_billing.kd_jns_op kd_jns_op_kobil,
                                                                                        data_billing.tahun_pajak thn_pajak_sppt_kobil,
                                                                                        data_billing.pengesahan,
                                                                                        data_billing.kode_bank
                                                                                    FROM data_billing
                                                                                        JOIN billing_kolektif
                                                                                            ON data_billing.data_billing_id =
                                                                                                BILLING_KOLEKTIF.data_billing_id
                                                                                    WHERE kd_status = '1') kobil
                                                                                    ON     bayar.kd_propinsi = kobil.kd_propinsi
                                                                                        AND bayar.kd_dati2 = kobil.kd_dati2
                                                                                        AND bayar.kd_kecamatan = kobil.kd_kecamatan
                                                                                        AND bayar.kd_kelurahan = kobil.kd_kelurahan
                                                                                        AND bayar.kd_blok = kobil.kd_blok
                                                                                        AND bayar.no_urut = kobil.no_urut
                                                                                        AND bayar.kd_jns_op = kobil.kd_jns_op
                                                                                        AND bayar.thn_pajak_sppt = kobil.thn_pajak_sppt
                                                                                        AND bayar.tanggal = kobil.tanggal
                                                                                        and bayar.pengesahan=kobil.pengesahan
                                                                                        and bayar.kode_bank=kobil.kode_bank
                                                                            WHERE kobil.kd_propinsi_kobil IS NOT NULL) t_bayar
                                                                GROUP BY kd_propinsi_kobil,
                                                                        kd_dati2_kobil,
                                                                        kd_kecamatan_kobil,
                                                                        kd_kelurahan_kobil,
                                                                        kd_blok_kobil,
                                                                        no_urut_kobil,
                                                                        kd_jns_op_kobil,
                                                                        thn_pajak_sppt_kobil,
                                                                        kode_bank,pengesahan,
                                                                        tanggal) bayar
                                                                ON     rekon.kd_propinsi = bayar.kd_propinsi
                                                                    AND rekon.kd_dati2 = bayar.kd_dati2
                                                                    AND rekon.kd_kecamatan = bayar.kd_kecamatan
                                                                    AND rekon.kd_kelurahan = bayar.kd_kelurahan
                                                                    AND rekon.kd_blok = bayar.kd_blok
                                                                    AND rekon.no_urut = bayar.no_urut
                                                                    AND rekon.kd_jns_op = bayar.kd_jns_op
                                                                    AND rekon.thn_pajak_sppt = bayar.thn_pajak_sppt
                                                                    AND rekon.tanggal = bayar.tanggal
                                                        WHERE rekon.jumlah <> bayar.jumlah OR bayar.jumlah IS NULL"));
                foreach ($kobil_ke_bayar as $row) {

                    $del = 0;
                    $ins = 0;
                    if ($row->jumlah != $row->jumlah_bayar && $row->jumlah_bayar != '') {
                        // jika nominal tidak sama
                        $del = 1;
                        $ins = 1;
                    } else if ($row->jumlah != $row->jumlah_bayar && $row->jumlah_bayar == '') {
                        $del = 0;
                        $ins = 1;
                    } else {
                        // by
                    }

                    $kobil = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op;
                    $tahun = $row->thn_pajak_sppt;

                    $this->info("Proses rekon kobil " . $kobil . " ->" . $tahun . " , del=" . $del . " | ins=" . $ins);
                    if ($del == 1) {

                        $unflag_kkobil = " DELETE FROM pbb.pembayaran_sppt
                                        WHERE ROWID IN (select c.rowid
                                                        from data_billing a
                                                        join billing_kolektif b on a.data_billing_id=b.data_billing_id
                                                        join pbb.pembayaran_sppt c on b.kd_propinsi=c.kd_propinsi and
                                                        b.kd_dati2=c.kd_dati2 and
                                                        b.kd_kecamatan=c.kd_kecamatan and
                                                        b.kd_kelurahan=c.kd_kelurahan and
                                                        b.kd_blok=c.kd_blok and
                                                        b.no_urut=c.no_urut and
                                                        b.kd_jns_op=c.kd_jns_op and
                                                        b.tahun_pajak=c.thn_pajak_sppt and
                                                        a.pengesahan=c.pengesahan
                                                        where a.kobil='$kobil' and a.tahun_pajak='$tahun'
                                                        and kd_status='1'  and  trunc(tgl_bayar)=TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'));
                                                                                        
                                                                                        UPDATE sim_pbb.data_billing
                                                                                        SET kd_status = 0, tgl_bayar = NULL, pengesahan = NULL
                                                                                        WHERE     kobil ='" . $kobil . "'
                                                                                        AND tahun_pajak ='" . $tahun . "'
                                                                                        and kd_status = '1'
                                                                                        and trunc(tgl_bayar)=TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd');";
                        DB::statement("begin " . $unflag_kkobil . " commit; end;");
                    }

                    if ($ins == 1) {

                        $ntpd = date('YmdHis');
                        $kobil = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op;


                        DB::statement(DB::raw("BEGIN 
                        DELETE FROM pbb.pembayaran_sppt
                        WHERE ROWID IN (select c.rowid
                        from data_billing a
                        join billing_kolektif b on a.data_billing_id=b.data_billing_id
                        join pbb.pembayaran_sppt c on b.kd_propinsi=c.kd_propinsi and
                        b.kd_dati2=c.kd_dati2 and
                        b.kd_kecamatan=c.kd_kecamatan and
                        b.kd_kelurahan=c.kd_kelurahan and
                        b.kd_blok=c.kd_blok and
                        b.no_urut=c.no_urut and
                        b.kd_jns_op=c.kd_jns_op and
                        b.tahun_pajak=c.thn_pajak_sppt and
                        a.pengesahan=c.pengesahan
                        where a.kobil='$kobil' and a.tahun_pajak='" . $row->thn_pajak_sppt . "'
                        and kd_status='1'  and  trunc(tgl_bayar)=TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'));

                            DELETE FROM spo.pembayaran_sppt
                                    WHERE ROWID IN (select c.rowid
                                    from data_billing a
                                    join billing_kolektif b on a.data_billing_id=b.data_billing_id
                                    join spo.pembayaran_sppt c on b.kd_propinsi=c.kd_propinsi and
                                    b.kd_dati2=c.kd_dati2 and
                                    b.kd_kecamatan=c.kd_kecamatan and
                                    b.kd_kelurahan=c.kd_kelurahan and
                                    b.kd_blok=c.kd_blok and
                                    b.no_urut=c.no_urut and
                                    b.kd_jns_op=c.kd_jns_op and
                                    b.tahun_pajak=c.thn_pajak_sppt and
                                    a.pengesahan=c.pengesahan
                                    where a.kobil='$kobil' and a.tahun_pajak='" . $row->thn_pajak_sppt . "'
                                    and kd_status='1'  and  trunc(tgl_bayar)=TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'));

                    UPDATE pbb.sppt
                    SET status_pembayaran_sppt = '0'
                  WHERE ROWID IN (SELECT b.ROWID
                                    FROM sim_pbb.billing_kolektif a
                                         JOIN pbb.sppt b
                                            ON     a.tahun_pajak = b.thn_pajak_sppt
                                               AND a.kd_propinsi = b.kd_propinsi
                                               AND a.kd_dati2 = b.kd_dati2
                                               AND a.kd_kecamatan = b.kd_kecamatan
                                               AND a.kd_kelurahan = b.kd_kelurahan
                                               AND a.kd_blok = b.kd_blok
                                               AND a.no_urut = b.no_urut
                                               AND a.kd_jns_op = b.kd_jns_op
                                   WHERE data_billing_id IN (SELECT data_billing_id
                                                         FROM sim_pbb.data_billing
                                                        WHERE  kobil ='" . $kobil . "'
                                                              AND tahun_pajak  in (" . $row->thn_pajak_sppt . ") and deleted_at is null ));

                                                              UPDATE sim_pbb.data_billing
                                                                        SET kd_status = 0, tgl_bayar = NULL, pengesahan = NULL
                                                                        WHERE  kobil ='" . $kobil . "'
                                                                        AND tahun_pajak  in (" . $row->thn_pajak_sppt . ") and deleted_at is null;
                                                              commit;
                                                              END;
                                                          "));

                        DB::statement(db::raw("call spo.proc_pembayaran_kobil('" . $kobil . "','" . $row->thn_pajak_sppt . "',to_date('" . $tanggal . "','yyyy-mm-dd'),'" . $ntpd . "','" . $tp . "')"));
                    }
                }

                // bayar ke rekon nop kobil
                $bayar_ke_rekon_kobil = DB::select(db::raw("select bayar.kd_propinsi,
                bayar.kd_dati2,
                bayar.kd_kecamatan,
                bayar.kd_kelurahan,
                bayar.kd_blok,
                bayar.no_urut,
                bayar.kd_jns_op,
                bayar.thn_pajak_sppt,bayar.tanggal,bayar.jumlah,rekon.jumlah jumlah_rekon
                 from (  SELECT kd_propinsi_kobil kd_propinsi,
                                 kd_dati2_kobil kd_dati2,
                                 kd_kecamatan_kobil kd_kecamatan,
                                 kd_kelurahan_kobil kd_kelurahan,
                                 kd_blok_kobil kd_blok,
                                 no_urut_kobil no_urut,
                                 kd_jns_op_kobil kd_jns_op,
                                 thn_pajak_sppt_kobil thn_pajak_sppt,
                                 tanggal,
                                 SUM (denda) denda,
                                 SUM (jumlah) jumlah
                            FROM (SELECT bayar.*,
                                         kobil.kd_propinsi_kobil,
                                         kobil.kd_dati2_kobil,
                                         kobil.kd_kecamatan_kobil,
                                         kobil.kd_kelurahan_kobil,
                                         kobil.kd_blok_kobil,
                                         kobil.no_urut_kobil,
                                         kobil.kd_jns_op_kobil,
                                         kobil.thn_pajak_sppt_kobil
                                    FROM (SELECT kd_propinsi,
                                                 kd_dati2,
                                                 kd_kecamatan,
                                                 kd_kelurahan,
                                                 kd_blok,
                                                 no_urut,
                                                 kd_jns_op,
                                                 thn_pajak_sppt,
                                                 denda_sppt denda,
                                                 jml_sppt_yg_dibayar jumlah,
                                                 TRUNC (tgl_pembayaran_sppt) tanggal,
                                                 kode_bank,
                                                 pengesahan
                                            FROM pbb.pembayaran_sppt
                                           WHERE     kode_bank = '" . $tp . "'
                                                 AND TRUNC (tgl_pembayaran_sppt) =
                                                        TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'))
                                         bayar
                                         LEFT JOIN
                                         (SELECT billing_Kolektif.kd_propinsi,
                                                 billing_Kolektif.kd_dati2,
                                                 billing_Kolektif.kd_kecamatan,
                                                 billing_Kolektif.kd_kelurahan,
                                                 billing_Kolektif.kd_blok,
                                                 billing_Kolektif.no_urut,
                                                 billing_Kolektif.kd_jns_op,
                                                 billing_Kolektif.tahun_pajak thn_pajak_sppt,
                                                 denda,
                                                 total jumlah,
                                                 TRUNC (tgl_bayar) tanggal,
                                                 data_billing.kd_propinsi kd_propinsi_kobil,
                                                 data_billing.kd_dati2 kd_dati2_kobil,
                                                 data_billing.kd_kecamatan kd_kecamatan_kobil,
                                                 data_billing.kd_kelurahan kd_kelurahan_kobil,
                                                 data_billing.kd_blok kd_blok_kobil,
                                                 data_billing.no_urut no_urut_kobil,
                                                 data_billing.kd_jns_op kd_jns_op_kobil,
                                                 data_billing.tahun_pajak thn_pajak_sppt_kobil,
                                                 data_billing.pengesahan
                                            FROM data_billing
                                                 JOIN billing_kolektif
                                                    ON data_billing.data_billing_id =
                                                          BILLING_KOLEKTIF.data_billing_id
                                           WHERE kd_status = '1') kobil
                                            ON     bayar.kd_propinsi = kobil.kd_propinsi
                                               AND bayar.kd_dati2 = kobil.kd_dati2
                                               AND bayar.kd_kecamatan = kobil.kd_kecamatan
                                               AND bayar.kd_kelurahan = kobil.kd_kelurahan
                                               AND bayar.kd_blok = kobil.kd_blok
                                               AND bayar.no_urut = kobil.no_urut
                                               AND bayar.kd_jns_op = kobil.kd_jns_op
                                               AND bayar.thn_pajak_sppt = kobil.thn_pajak_sppt
                                               AND bayar.tanggal = kobil.tanggal
                                               AND bayar.pengesahan = kobil.pengesahan
                                   WHERE kobil.kd_propinsi_kobil IS NOT NULL) t_bayar
                        GROUP BY kd_propinsi_kobil,
                                 kd_dati2_kobil,
                                 kd_kecamatan_kobil,
                                 kd_kelurahan_kobil,
                                 kd_blok_kobil,
                                 no_urut_kobil,
                                 kd_jns_op_kobil,
                                 thn_pajak_sppt_kobil,
                                 tanggal) bayar
                 left join (SELECT kd_propinsi,
                               kd_dati2,
                               kd_kecamatan,
                               kd_kelurahan,
                               kd_blok,
                               no_urut,
                               kd_jns_op,
                               thn_pajak_sppt,
                               denda,
                               jumlah,
                               TRUNC (tanggal_pembayaran) tanggal
                          FROM rekon_nop
                         WHERE rekon_batch_id = '" . $rekon_batch_id . "' AND kd_blok = '999') rekon on rekon.kd_propinsi = bayar.kd_propinsi
                             AND rekon.kd_dati2 = bayar.kd_dati2
                             AND rekon.kd_kecamatan = bayar.kd_kecamatan
                             AND rekon.kd_kelurahan = bayar.kd_kelurahan
                             AND rekon.kd_blok = bayar.kd_blok
                             AND rekon.no_urut = bayar.no_urut
                             AND rekon.kd_jns_op = bayar.kd_jns_op
                             AND rekon.thn_pajak_sppt = bayar.thn_pajak_sppt
                             AND rekon.tanggal = bayar.tanggal         
                 where rekon.jumlah is null"));

                foreach ($bayar_ke_rekon_kobil as $row) {
                    $kobil = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op;
                    $tahun = $row->thn_pajak_sppt;
                    $this->info("unflag kobil " . $kobil . ' -> ' . $tahun);

                    $unflag_kkobil = " DELETE FROM pbb.pembayaran_sppt
                                        WHERE ROWID IN (select c.rowid
                        from data_billing a
                        join billing_kolektif b on a.data_billing_id=b.data_billing_id
                        join pbb.pembayaran_sppt c on b.kd_propinsi=c.kd_propinsi and
                        b.kd_dati2=c.kd_dati2 and
                        b.kd_kecamatan=c.kd_kecamatan and
                        b.kd_kelurahan=c.kd_kelurahan and
                        b.kd_blok=c.kd_blok and
                        b.no_urut=c.no_urut and
                        b.kd_jns_op=c.kd_jns_op and
                        b.tahun_pajak=c.thn_pajak_sppt and
                        a.pengesahan=c.pengesahan
                        where a.kobil='$kobil' and a.tahun_pajak='" . $tahun . "'
                        and kd_status='1'  and  trunc(tgl_bayar)=TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'));";
                    DB::statement("begin " . $unflag_kkobil . " commit; end;");
                }
            }

            DB::connection("oracle_satutujuh")->statement(db::raw("call proc_pelunasan_sppt(TO_DATE ('" . $tanggal . "', 'yyyy-mm-dd'))"));
        } else {
            $this->error('masukan parameter --tp= dan --tanggal=yyyy-mm-dd');
        }
        $this->info('Selesai.');
    }
}
