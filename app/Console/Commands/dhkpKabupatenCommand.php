<?php

namespace App\Console\Commands;

use App\Exports\DhkpKabupaten;
use App\fileReport;
use App\Jobs\DhkpFileGenerator;
use App\Jobs\DhkpFileKecamatan;
use App\Kecamatan;
use App\Sppt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class dhkpKabupatenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dhkp:kabupaten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate file dhkpper kecamatan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;

        $disk = 'reports';
        $old = fileReport::whereraw("trunc(sysdate) - trunc(created_at) >0 and keterangan like 'DHKP Kecamatan 3,4,5%'");
        if ($old->get()) {
            foreach ($old->get() as $file) {
                Storage::disk($file->disk)->delete($file->filename);
            }
            $old->delete();
        }


        for ($tahun = 2022; $tahun <= 2023; $tahun++) {
            // $tahun = date('Y');
            $filename = 'DHKP Kecamatan 3,4,5 ' . $tahun . ' - ' . date('Ymd') . '.xlsx';

            Excel::store(new DhkpKabupaten($tahun), $filename, $disk);
            $rekam = [
                'keterangan' => $filename,
                'disk' => $disk,
                'filename' => $filename
            ];
            fileReport::create($rekam);
            Log::info('Generate ' . $filename . ' successfully');
        }

        // $this->info('Generate '.$filename.' successfully');
    }
}
