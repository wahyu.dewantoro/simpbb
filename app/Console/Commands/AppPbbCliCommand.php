<?php

namespace App\Console\Commands;

use App\Helpers\PembatalanObjek;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AppPbbCliCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pbb:cli  
        {--option= : Specify the option to execute}
        {--kd_kecamatan= : Kode Kecamatan}
        {--kd_kelurahan= : Kode Kelurahan}
        {--tahun_sppt= :tahun sppt}
        {--tahun_nir= :tahun nir}
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proses PBB CLI untuk berbagai keperluan seperti pembatalan sistep';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $option = $this->option('option');

        if (!$option) {
            $this->info('No --option was provided.');
            return Command::INVALID;
        }

        switch ($option) {
            case 'sistep':
                $this->prosesPembatalanSistep();
                return Command::SUCCESS;
            case 'copy-hkpd':
                $this->prosesRataHkpd();
                return Command::SUCCESS;
            default:
                $this->alert("Option '$option' is not available.");
                return Command::FAILURE;
        }
    }


    private function prosesRataHkpd()
    {
        $tahun_nir = $this->option('tahun_nir');
        $tahun_sppt = $this->option('tahun_sppt');

        if (!$tahun_nir) {
            $this->info('tahun_nir harus di sertakan');
            return 0;
        }


        if (!$tahun_sppt) {
            $this->info('tahun_sppt harus di sertakan');
            return 0;
        }

        $this->info("identifikasi data");
        $getrata = DB::connection('oracle_satutujuh')->select(DB::raw("select dat_nir.thn_nir_znt,dat_nir.kd_kecamatan,dat_nir.kd_kelurahan, case when  regexp_replace(dat_nir.kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end objek,dat_nir.kd_znt,dat_nir.nilai_hkpd,nvl(hkpd_rata_rata,100) hkpd_rata
                                                                        from dat_nir 
                                                                        left join (select kd_kecamatan,kd_kelurahan,kd_znt,count(hkpd) jumlah,floor(sum(hkpd) /count(hkpd)) hkpd_rata_rata 
                                                                        from (
                                                                        select  dat_op_bumi.kd_kecamatan,dat_op_bumi.kd_kelurahan,kd_znt,nvl(hkpd,100) hkpd
                                                                        from dat_op_bumi
                                                                        join sppt on dat_op_bumi.kd_propinsi=sppt.kd_propinsi and 
                                                                        dat_op_bumi.kd_dati2=sppt.kd_dati2 and 
                                                                        dat_op_bumi.kd_kecamatan=sppt.kd_kecamatan and 
                                                                        dat_op_bumi.kd_kelurahan=sppt.kd_kelurahan and 
                                                                        dat_op_bumi.kd_blok=sppt.kd_blok and 
                                                                        dat_op_bumi.no_urut=sppt.no_urut and 
                                                                        dat_op_bumi.kd_jns_op=sppt.kd_jns_op and sppt.thn_pajak_sppt='$tahun_sppt'
                                                                        ) 
                                                                        group by  kd_kecamatan,kd_kelurahan,kd_znt) rata on rata.kd_kecamatan=dat_nir.kd_kecamatan and rata.kd_kelurahan=dat_nir.kd_kelurahan
                                                                        and rata.kd_znt=dat_nir.kd_znt
                                                                        where thn_nir_znt='$tahun_nir'"));
        $total = count($getrata); // Total data yang akan diproses
        $current = 0;          // Mulai dari 0
        $this->info("Proses proses update hkpd pada znt nir");
        foreach ($getrata as $row) {
            $current++;

            $kd_kecamatan = $row->kd_kecamatan;
            $kd_kelurahan = $row->kd_kelurahan;

            $hkpd_asal = $row->hkpd_rata;

            if ($kd_kecamatan == '010' && ($kd_kelurahan == '010' || $kd_kelurahan == '006')) {
                $pengurang = floor(0.2 * $hkpd_asal);
                $hkpd_jadi = $hkpd_asal - $pengurang;
            } else {
                $hkpd_jadi = $hkpd_asal;
            }


            DB::connection("oracle_satutujuh")->table('dat_nir')
                ->where([
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan
                ])
                ->update(['nilai_hkpd' => $hkpd_jadi]);



            $percentage = intval(($current / $total) * 100);
            echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
            usleep(100000); // Delay untuk efek progress
        }
        echo "\n";
        $this->info("selesai.");
    }

    private function prosesPembatalanSistep(): void
    {
        $kd_kecamatan = $this->option('kd_kecamatan');
        $kd_kelurahan = $this->option('kd_kelurahan');

        if (!$kd_kecamatan) {
            $this->info('Tidak ada kode kecamatan');
            return;
        }
        if (!$kd_kelurahan) {
            $this->info('Tidak ada kode kelurahan');
            return;
        }

        $this->info('identifikasi objek');
        $bj = DB::connection("oracle_satutujuh")->table("dat_op_bumi")
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_jns_op='7' and jns_bumi not in ('4','5')")->get();

        $daftar_objek = [];


        foreach ($bj as $value) {
            $r = [
                'kd_propinsi' => $value->kd_propinsi,
                'kd_dati2' => $value->kd_dati2,
                'kd_kecamatan' => $value->kd_kecamatan,
                'kd_kelurahan' => $value->kd_kelurahan,
                'kd_blok' => $value->kd_blok,
                'no_urut' => $value->no_urut,
                'kd_jns_op' => $value->kd_jns_op
            ];
            $daftar_objek[] = $r;
        }


        $this->info('Idendtifikasi tunggakan');
        $tunggakan = DB::connection("oracle_satutujuh")->select(DB::raw("select sppt.kd_propinsi,
                                                                    sppt.kd_dati2,
                                                                    sppt.kd_kecamatan,
                                                                    sppt.kd_kelurahan,
                                                                    sppt.kd_blok,
                                                                    sppt.no_urut,
                                                                    sppt.kd_jns_op,
                                                                    sppt.thn_pajak_sppt,
                                                                    jns_koreksi,
                                                                    sppt.status_pembayaran_sppt,
                                                                    oltp.pbb_yg_harus_dibayar_sppt
                                                            from sppt
                                                                    left join sppt_koreksi
                                                                    on     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                                                        and sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                                                        and sppt.kd_kecamatan = sppt_koreksi.kd_kecamatan
                                                                        and sppt.kd_kelurahan = sppt_koreksi.kd_kelurahan
                                                                        and sppt.kd_blok = sppt_koreksi.kd_blok
                                                                        and sppt.no_urut = sppt_koreksi.no_urut
                                                                        and sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                                                        and sppt.thn_pajak_sppt = sppt_koreksi.thn_pajak_sppt
                                                                    left join sppt_pembatalan
                                                                    on     sppt.kd_propinsi = sppt_pembatalan.kd_propinsi
                                                                        and sppt.kd_dati2 = sppt_pembatalan.kd_dati2
                                                                        and sppt.kd_kecamatan = sppt_pembatalan.kd_kecamatan
                                                                        and sppt.kd_kelurahan = sppt_pembatalan.kd_kelurahan
                                                                        and sppt.kd_blok = sppt_pembatalan.kd_blok
                                                                        and sppt.no_urut = sppt_pembatalan.no_urut
                                                                        and sppt.kd_jns_op = sppt_pembatalan.kd_jns_op
                                                                        and sppt.thn_pajak_sppt = sppt_pembatalan.thn_pajak_sppt
                                                                        and tgl_mulai <= sysdate
                                                                        and tgl_selesai >= sysdate
                                                                    left join spo.sppt_oltp oltp
                                                                    on     sppt.kd_propinsi = oltp.kd_propinsi
                                                                        and sppt.kd_dati2 = oltp.kd_dati2
                                                                        and sppt.kd_kecamatan = oltp.kd_kecamatan
                                                                        and sppt.kd_kelurahan = oltp.kd_kelurahan
                                                                        and sppt.kd_blok = oltp.kd_blok
                                                                        and sppt.no_urut = oltp.no_urut
                                                                        and sppt.kd_jns_op = oltp.kd_jns_op
                                                                        and sppt.thn_pajak_sppt = oltp.thn_pajak_sppt
                                                            where     sppt.kd_propinsi = '35'
                                                                    and sppt.kd_dati2 = '07'
                                                                    and sppt.kd_kecamatan = '" . $kd_kecamatan . "'
                                                                    and sppt.kd_kelurahan = '" . $kd_kelurahan . "'
                                                                    and sppt.kd_jns_op = '7'
                                                                    and sppt.status_pembayaran_sppt != '1'
                                                                    and sppt_pembatalan.thn_pajak_sppt is null
                                                                    and sppt_koreksi.thn_pajak_sppt is null"));

        $no_surat = '900.1.13.1/3866/35.07.404/2024';
        $tgl_surat = '2024-09-03';
        $keterangan = "Pembatalan karena objek di migrasi ke sismiop";
        $now = Carbon::now();
        $userid = 1678;






        $dd = [];
        $notr = PembatalanObjek::NoTransaksi('SM', '06');

        $this->info("Persiapan data koreksi");
        foreach ($tunggakan as $key => $value) {
            if (!in_array($value->jns_koreksi, ['1', '3'])) {
                $thn_pajak_sppt = $value->thn_pajak_sppt;
                // $ins = $var;
                $ins['kd_propinsi'] = $value->kd_propinsi;
                $ins['kd_dati2'] = $value->kd_dati2;
                $ins['kd_kecamatan'] = $value->kd_kecamatan;
                $ins['kd_kelurahan'] = $value->kd_kelurahan;
                $ins['kd_blok'] = $value->kd_blok;
                $ins['no_urut'] = $value->no_urut;
                $ins['kd_jns_op'] = $value->kd_jns_op;
                $ins['thn_pajak_sppt'] = $thn_pajak_sppt;

                // $now = Carbon::now();
                $ins['tgl_surat'] = new Carbon($tgl_surat);
                $ins['no_surat'] = $no_surat;
                $ins['jns_koreksi'] = '3';
                $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                $ins['keterangan'] = $keterangan;
                $ins['no_transaksi'] = $notr;
                $ins['created_at'] = $now;
                $ins['created_by'] = $userid;
                $ins['nm_kategori'] = $keterangan;
                $ins['kd_kategori'] = '06';

                $ins['verifikasi_kode'] = 1;
                $ins['verifikasi_keterangan'] = $keterangan;
                $ins['verifikasi_by'] = $userid;
                $ins['verifikasi_at'] = $now;
                $dd[] = $ins;
            }
        }


        // insert ke sppt koreksi
        $total = count($dd); // Total data yang akan diproses
        $current = 0;          // Mulai dari 0
        $this->info("Proses input ke sppt koreksi");
        foreach ($dd as $key => $koreksi) {
            $current++;
            $kd_propinsi = $koreksi['kd_propinsi'];
            $kd_dati2 = $koreksi['kd_dati2'];
            $kd_kecamatan = $koreksi['kd_kecamatan'];
            $kd_kelurahan = $koreksi['kd_kelurahan'];
            $kd_blok = $koreksi['kd_blok'];
            $no_urut = $koreksi['no_urut'];
            $kd_jns_op = $koreksi['kd_jns_op'];
            $thn_pajak_sppt = $koreksi['thn_pajak_sppt'];


            $cek = DB::connection("oracle_satutujuh")->table('sppt_koreksi')
                ->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_pajak_sppt' => $thn_pajak_sppt
                ])->exists();
            // Log::info($cek);

            if ($cek) {
                // update
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')
                    ->where([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $thn_pajak_sppt
                    ])
                    ->update($koreksi);
                Log::info("Update :" . json_encode($koreksi));
            } else {
                // insert
                DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($koreksi);
                Log::info("Insert :" . json_encode($koreksi));
            }

            // update status di sppt
            DB::connection("oracle_satutujuh")->table('sppt')->where([
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'thn_pajak_sppt' => $thn_pajak_sppt
            ])->update(['status_pembayaran_sppt' => '3']);


            $percentage = intval(($current / $total) * 100);
            echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
            usleep(100000); // Delay untuk efek progress
        }
        echo "\n";
        $this->info("Proses pembatalan objek");

        $total = count($daftar_objek); // Total data yang akan diproses
        $current = 0;
        foreach ($daftar_objek as $key => $obj) {
            $current++;
            # code...
            $kd_propinsi = $obj['kd_propinsi'];
            $kd_dati2 = $obj['kd_dati2'];
            $kd_kecamatan = $obj['kd_kecamatan'];
            $kd_kelurahan = $obj['kd_kelurahan'];
            $kd_blok = $obj['kd_blok'];
            $no_urut = $obj['no_urut'];
            $kd_jns_op = $obj['kd_jns_op'];

            DB::connection("oracle_satutujuh")->table('dat_op_bumi')
                ->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op
                ])->update([
                    'jns_bumi' => '5'
                ]);

            $percentage = intval(($current / $total) * 100);
            echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
            usleep(100000); // Delay untuk efek progress

        }




        /* 
             $current++;
            // proses source nya disini

            

            $percentage = intval(($current / $total) * 100);
            echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
            usleep(100000); // Delay untuk efek progress
           
           */

        echo "\n"; // Pindahkan ke baris baru setelah progress selesai
        // Tambahkan logika proses di sini
        $this->info("Proses pembatalan sistep berhasil untuk kecamatan $kd_kecamatan dan kelurahan $kd_kelurahan.");
    }
}
