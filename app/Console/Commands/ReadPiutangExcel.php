<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Piutang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReadPiutangExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'piutang:read {file : Path to the Excel file} {--sheet= : Name of the sheet to process}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read and process piutang data from an Excel file, with an optional sheet parameter';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filePath = $this->argument('file');
        $sheetOption = $this->option('sheet'); // Ambil nama sheet dari opsi

        // Periksa apakah file ada
        if (!file_exists($filePath)) {
            $this->error("File {$filePath} tidak ditemukan.");
            return Command::FAILURE;
        }

        try {
            // Baca semua sheet dari file Excel
            $sheets = Excel::toCollection(null, $filePath);

            if ($sheetOption) {
                // Periksa apakah sheet yang diminta ada
                if (!$sheets->has($sheetOption)) {
                    $this->error("Sheet '{$sheetOption}' tidak ditemukan dalam file.");
                    return Command::FAILURE;
                }

                // Proses sheet tertentu
                $this->processSheet($sheets->get($sheetOption), $sheetOption);
            } else {
                // Jika opsi sheet tidak diberikan, proses semua sheet
                foreach ($sheets as $sheetName => $sheetData) {
                    $this->processSheet($sheetData, $sheetName);
                }
            }
        } catch (\Exception $e) {
            $this->error('Terjadi kesalahan saat membaca file Excel: ' . $e->getMessage());
            return Command::FAILURE;
        }

        $this->info('File Excel berhasil diproses.');
        return Command::SUCCESS;
    }

    /**
     * Proses data dari sheet.
     *
     * @param  \Illuminate\Support\Collection $sheetData
     * @param  string $sheetName
     * @return void
     */
    protected function processSheet($sheetData, $sheetName)
    {
        $this->info("Processing sheet: {$sheetName}");

        foreach ($sheetData as $rowIndex => $row) {
            if ($rowIndex === 0) {
                // Skip header row
                continue;
            }

            DB::connection('oracle_satutujuh')->beginTransaction();
            try {
                //code...

                $nop = $row[0];
                $objp = preg_replace('/[^0-9]/', '', $nop);
                $rowinsert = [
                    'nop' => $objp,
                    'kd_propinsi' => substr($objp, 0, 2),
                    'kd_dati2' => substr($objp, 2, 2),
                    'kd_kecamatan' => substr($objp, 4, 3),
                    'kd_kelurahan' => substr($objp, 7, 3),
                    'kd_blok' => substr($objp, 10, 3),
                    'no_urut' => substr($objp, 13, 4),
                    'kd_jns_op' => substr($objp, 17, 1),
                    'tahun_terbit' => $row[1],
                    'tahun_pajak' => $row[2],
                    'nm_wp' => $row[3],
                    'ketetapan' => $row[6],
                    'sheet' => $sheetName,
                ];

                DB::connection("oracle_satutujuh")
                    ->table('PIUTANG_KERTAS_KERJA_23')
                    ->insert($rowinsert);

                $this->line("Row {$rowIndex} saved: " . implode(', ', $rowinsert));

                DB::connection("oracle_satutujuh")->commit();
            } catch (\Throwable $th) {
                DB::connection("oracle_satutujuh")->rollBack();
                //throw $th;
                $this->line("Row {$rowIndex} gagal simpan -> " . $th->getMessage());
                Log::error($th);
            }
        }
    }
}
