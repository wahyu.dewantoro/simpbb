<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class copyNirSistep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pendataan:nirsistep  {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Penentuan nir sistep';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $tahun = $this->option('tahun') ?? '';
        $kd_kecamatan = $this->option('kd_kecamatan') ?? '';
        $kd_kelurahan = $this->option('kd_kelurahan') ?? '';

        if ($tahun == '') {
            $this->info("harus menyertakan tahun");
        } else {

            $tahun_last = $tahun - 1;
            $wh = "";
            if ($kd_kecamatan != "") {
                $wh .= " and a.kd_kecamatan='$kd_kecamatan' ";
            }
            if ($kd_kelurahan != "") {
                $wh .= " and a.kd_kelurahan='$kd_kelurahan' ";
            }

            $objek = DB::connection("oracle_satutujuh")->select("select  a.* 
            from (
            select distinct kd_kecamatan,kd_kelurahan,   case when  regexp_replace(kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end ket
            from dat_znt
            ) a
            join (select  kd_kecamatan,kd_kelurahan,count(1) jm
            from (
            select distinct kd_kecamatan,kd_kelurahan,   case when  regexp_replace(kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end ket
            from dat_znt
          
            ) tmp
            group by kd_kecamatan,kd_kelurahan 
            having count(1) =1) b on a.kd_kecamatan=b.kd_kecamatan and a.kd_kelurahan=b.kd_kelurahan
            where ket='sistep' " . $wh . "
            order by a.kd_kecamatan , a.kd_kelurahan asc
            ");

            foreach ($objek as $row) {
                $this->info('Sedang memproses kelurahan : ' . $row->kd_kelurahan . ', kecamatan :' . $row->kd_kecamatan . ' ');

                DB::connection('oracle_satutujuh')->statement(db::raw("insert into dat_nir  (kd_propinsi,
                kd_dati2,
                kd_kecamatan,
                kd_kelurahan,
                kd_znt,
                thn_nir_znt,
                kd_kanwil,
                kd_kantor,
                jns_dokumen,
                no_dokumen,
                nir,
                created_by,
                created_at,nilai_hkpd)
                select a.* 
                from (
                select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_znt,'" . $tahun . "' thn_nir_znt,kd_kanwil,kd_kantor,jns_dokumen,to_char(sysdate,'yyyymmddhh24i')  no_dokumen,nir,'" . (Auth()->user()->id ?? 1678) . "' created_by,sysdate created_at,nilai_hkpd
                from dat_nir
                where thn_nir_znt='" . $tahun_last . "'
                    and kd_kecamatan='" . $row->kd_kecamatan . "' and kd_kelurahan='" . $row->kd_kelurahan . "'
                ) a 
                left join (
                select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_znt
                from dat_nir 
                where thn_nir_znt='" . $tahun . "'
                and kd_kecamatan='" . $row->kd_kecamatan . "' and kd_kelurahan='" . $row->kd_kelurahan . "'
                ) b on a.kd_propinsi=b.kd_propinsi and
                a.kd_dati2=b.kd_dati2 and
                a.kd_kecamatan=b.kd_kecamatan and
                a.kd_kelurahan=b.kd_kelurahan and
                a.kd_znt=b.kd_znt 
                where b.kd_propinsi is  null"));



                $this->info('Proses kelurahan : ' . $row->kd_kelurahan . ', kecamatan :' . $row->kd_kecamatan . ' selesai');
            }
            $this->info("selesai");

            // $jsn = json_encode($objek);
            // $this->info($jsn);
        }
    }
}
