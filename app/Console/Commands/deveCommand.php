<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class deveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:dev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->info('0');

        $data = DB::connection("oracle_satutujuh")->select("select * from (
            select  a.* ,(select count(1) from dat_peta_znt
            where kd_Kecamatan=a.kd_kecamatan and kd_kelurahan=a.kd_kelurahan ) blok
           from (
           select distinct kd_kecamatan,kd_kelurahan,   case when  regexp_replace(kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end ket
           from dat_znt
           ) a
           join (select  kd_kecamatan,kd_kelurahan,count(1) jm
           from (
           select distinct kd_kecamatan,kd_kelurahan,   case when  regexp_replace(kd_znt, '[^0-9]', '') is null then 'sismiop' else 'sistep' end ket
           from dat_znt
           ) tmp
           group by kd_kecamatan,kd_kelurahan 
           having count(1) =1) b on a.kd_kecamatan=b.kd_kecamatan and a.kd_kelurahan=b.kd_kelurahan
           where ket='sistep'
           order by a.kd_kecamatan , a.kd_kelurahan asc
           ) where blok<50");

           $no=1;
        foreach ($data as $row) {
            $znt = DB::connection("oracle_satutujuh")->select("select
                kd_znt
                 from dat_znt
                 where kd_kecamatan='" . $row->kd_kecamatan . "' and kd_kelurahan='" . $row->kd_kelurahan . "'");
            foreach ($znt as $dd) {
                $dd = [
                    'kd_propinsi' => '35',
                    'kd_dati2' => '07',
                    'kd_kecamatan' => $row->kd_kecamatan,
                    'kd_kelurahan' => $row->kd_kelurahan,
                    'kd_blok' => '000',
                    'kd_znt' => $dd->kd_znt
                ];

                $ck = DB::connection("oracle_satutujuh")->table("dat_peta_znt")->where($dd)->selectraw("count(1) jm")->first();
                if ($ck->jm == 0) {
                    DB::connection("oracle_satutujuh")->table("dat_peta_znt")->insert($dd);
                    $this->info($no." insert");
                }else{
                    $this->info($no." skip");
                }
                $no++;
            }
        }
        $this->info("selesai");
    }
}
