<?php

namespace App\Console\Commands;

use App\Kecamatan;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class rangkingKecamatanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:rankingkecamatan {tanggal?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'menggenerate ranking realisasi kecamatan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $tanggal = $this->argument('tanggal') ?? date('Ymd');
        $tahun = date('Y', strtotime($tanggal));

        DB::beginTransaction();
        try {
            DB::table('rpt_ranking_kecamatan')->whereraw("trunc(cut_off) = to_date('$tanggal','yyyymmdd')")->delete();
            $this->info("Data ranking kecamatan cut off pada " . tglindo($tanggal) . " telah di kosongkan!");

            $kecamatan = Kecamatan::get();
            foreach ($kecamatan as $kk) {
                $kec = $kk->kd_kecamatan;
                $data = DB::connection('oracle_dua')->select(DB::raw("SELECT a.kd_kecamatan,nm_kecamatan,buku,objek,pbb,bayar-denda pokok,denda,bayar
                from (
                select sppt.kd_kecamatan,get_buku(pbb_yg_harus_dibayar_sppt) buku,count(1) objek,
                                 sum(pbb_yg_harus_dibayar_sppt) pbb,
                                 sum(nvl (
                                    (select b.denda_sppt
                                       from pembayaran_sppt b
                                      where     b.kd_propinsi = sppt.kd_propinsi
                                            and b.kd_dati2 = sppt.kd_dati2
                                            and b.kd_kecamatan = sppt.kd_kecamatan
                                            and b.kd_kelurahan = sppt.kd_kelurahan
                                            and b.kd_blok = sppt.kd_blok
                                            and b.no_urut = sppt.no_urut
                                            and b.kd_jns_op = sppt.kd_jns_op
                                            and b.thn_pajak_sppt = sppt.thn_pajak_sppt
                                            and rownum = 1
                                            and trunc(tgl_pembayaran_sppt)<=to_date('$tanggal','yyyymmdd')
                                            ),
                                    0))
                                    denda,
                                 sum(nvl (
                                    (select  jml_sppt_yg_dibayar
                                       from pembayaran_sppt b
                                      where     b.kd_propinsi = sppt.kd_propinsi
                                            and b.kd_dati2 = sppt.kd_dati2
                                            and b.kd_kecamatan = sppt.kd_kecamatan
                                            and b.kd_kelurahan = sppt.kd_kelurahan
                                            and b.kd_blok = sppt.kd_blok
                                            and b.no_urut = sppt.no_urut
                                            and b.kd_jns_op = sppt.kd_jns_op
                                            and b.thn_pajak_sppt = sppt.thn_pajak_sppt
                                            and rownum = 1
                                            and trunc(tgl_pembayaran_sppt)<=to_date('$tanggal','yyyymmdd')
                                            ),
                                    0))
                                    bayar
                            from sppt
                           where     thn_pajak_sppt ='$tahun'
                                 and status_pembayaran_sppt in (0,1)
                                 and sppt.kd_kecamatan='$kec'
                                 group by kd_kecamatan,get_buku(pbb_yg_harus_dibayar_sppt)
                                 ) a 
                                 join ref_kecamatan b on a.kd_kecamatan=b.kd_kecamatan"));

                foreach ($data as $row) {
                    DB::table('rpt_ranking_kecamatan')->insert(
                        [
                            'kd_kecamatan' => $row->kd_kecamatan,
                            'nm_kecamatan' => $row->nm_kecamatan,
                            'buku' => $row->buku,
                            'objek' => $row->objek,
                            'baku' => $row->pbb,
                            'pokok' => $row->pokok,
                            'denda' => $row->denda,
                            'total' => $row->bayar,
                            'created_at' => Carbon::now(),
                            'cut_off' =>new Carbon($tanggal)
                        ]
                    );

                    $this->info($row->nm_kecamatan . ' buku ' . $row->buku . ' telah di insert.');
                }
            }

            DB::commit();
            $this->info("Data ranking kecamatan cut off pada " . tglindo($tanggal) . " selesai di generate!");
        } catch (\Exception $e) {
            DB::rollback();
            $this->error($e->getMessage());
        }
    }
}
