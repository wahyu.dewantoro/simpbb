<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateNjopBangunan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pendataan:rekelas-bng';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $dataIndividu = DB::connection("oracle_satutujuh")->table("rekelas_bng_2024")
            ->get();
        // ->whereRaw("rownum<=10")

        $now = Carbon::now();

        $jumlah = $dataIndividu->count();

        $no = 1;
        foreach ($dataIndividu as $row) {

            $this->info("Memproses data ke " . $no . " / " . $jumlah);
            $wh = [
                'kd_propinsi' => $row->kd_propinsi,
                'kd_dati2' => $row->kd_dati2,
                'kd_kecamatan' => $row->kd_kecamatan,
                'kd_kelurahan' => $row->kd_kelurahan,
                'kd_blok' => $row->kd_blok,
                'no_urut' => $row->no_urut,
                'kd_jns_op' => $row->kd_jns_op,
                'no_bng' => $row->no_bng,
                'no_formulir_individu' => $row->no_formulir_individu
            ];
            $update = [
                'kd_propinsi' => $row->kd_propinsi,
                'kd_dati2' => $row->kd_dati2,
                'kd_kecamatan' => $row->kd_kecamatan,
                'kd_kelurahan' => $row->kd_kelurahan,
                'kd_blok' => $row->kd_blok,
                'no_urut' => $row->no_urut,
                'kd_jns_op' => $row->kd_jns_op,
                'no_bng' => $row->no_bng,
                'no_formulir_individu' => $row->no_formulir_individu,
                'nilai_individu' => $row->nilai_individu,
                'tgl_penilaian_individu' => $now,
                'nip_penilai_individu' => $row->nip_penilai_individu,
                'tgl_pemeriksaan_individu' => $now,
                'nip_pemeriksa_individu' => $row->nip_pemeriksa_individu,
                'tgl_rekam_nilai_individu' => $now,
                'nip_perekam_individu' => $row->nip_perekam_individu
            ];

            $cek = DB::connection("oracle_satutujuh")->table("dat_nilai_individu")->where($wh)->select(db::raw("count(1) jum"))->first();
            if ($cek->jum > 0) {
                // update
                DB::connection("oracle_satutujuh")->table("dat_nilai_individu")->where($wh)->update($update);
                $this->info("update  ke " . $no . " / " . $jumlah . " selesai.");
            } else {
                // insert
                DB::connection("oracle_satutujuh")->table("dat_nilai_individu")->insert($update);
                $this->info("insert  ke " . $no . " / " . $jumlah . " selesai.");
            }
            $no++;
        }

        $this->info("Selesai");
    }
}
