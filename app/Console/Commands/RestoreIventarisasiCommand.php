<?php

namespace App\Console\Commands;

use App\Helpers\Pendataan;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RestoreIventarisasiCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iventarisasi:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        $nops = config('daftarnop.nop');
        $start = 1;
        $total = count($nops);
        $this->info($total);

        foreach ($nops as $row) {
            $this->info($start . "/" . $total . " Sedang memproses nop " . json_encode($row));
            $nop = onlyNumber($row);
            $kd_propinsi = substr($nop, 0, 2);
            $kd_dati2 = substr($nop, 2, 2);
            $kd_kecamatan = substr($nop, 4, 3);
            $kd_kelurahan = substr($nop, 7, 3);
            $kd_blok = substr($nop, 10, 3);
            $no_urut = substr($nop, 13, 4);
            $kd_jns_op = substr($nop, 17, 1);

            $wh = [
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op
            ];

            $krs = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                ->selectraw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,jns_koreksi")
                ->where($wh)->whereraw("to_char(created_at,'yyyy')='2024' and no_transaksi not like '%KP%'")->get();
            foreach ($krs as $kr) {
                try {
                    //code...

                    // cek tanggal terbit null apa ndak

                    // get tanggal terbit
                    /*   $ct =    DB::connection("oracle_satutujuh")->table("log_sppt")->selectraw("ROWIDTOCHAR(max(rowid)) ri")
                        ->whereraw("
                        kd_kecamatan='" . $kr->kd_kecamatan . "' and 
                        kd_kelurahan='" . $kr->kd_kelurahan . "' and 
                        kd_blok='" . $kr->kd_blok . "' and 
                        no_urut='" . $kr->no_urut . "' and 
                        kd_jns_op='" . $kr->kd_jns_op . "' and 
                        thn_pajak_sppt  ='" . $kr->thn_pajak_sppt  . "' and tgl_terbit_sppt is not null
                    ")->first();

                    if ($ct) { */
                    // $re = DB::connection("oracle_satutujuh")->table("log_sppt")->select("tgl_terbit_sppt")
                    //     ->whereRaw("ROWIDTOCHAR(rowid)='" . $ct->ri . "'")->first();
                    DB::connection("oracle_satutujuh")->table("sppt")
                        ->whereraw("  kd_kecamatan='" . $kr->kd_kecamatan . "' and 
                    kd_kelurahan='" . $kr->kd_kelurahan . "' and 
                    kd_blok='" . $kr->kd_blok . "' and 
                    no_urut='" . $kr->no_urut . "' and 
                    kd_jns_op='" . $kr->kd_jns_op . "' and 
                    thn_pajak_sppt  ='" . $kr->thn_pajak_sppt  . "'")
                        ->update(['status_pembayaran_sppt' => '0']);
                    // 'tgl_terbit_sppt' => new Carbon($re->tgl_terbit_sppt),
                    // }

                    DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                        ->whereraw("kd_kecamatan='" . $kr->kd_kecamatan . "' and 
                                    kd_kelurahan='" . $kr->kd_kelurahan . "' and 
                                    kd_blok='" . $kr->kd_blok . "' and 
                                    no_urut='" . $kr->no_urut . "' and 
                                    kd_jns_op='" . $kr->kd_jns_op . "' and 
                                    thn_pajak_sppt  ='" . $kr->thn_pajak_sppt  . "'")
                        ->delete();
                } catch (\Throwable $th) {
                    //throw $th;
                    Log::info($th);
                }
            }

            $start++;
            // $this->info(json_encode($row));
        }
        /*   foreach ($nops as $row) {
            $this->info($start . "/" . $total . " Sedang memproses nop " . json_encode($row));
            Pendataan::CloningSpopLspop($row);
            try {
                //code...

                // list nop data sppt koreksi
                $krs = DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                    ->selectraw("kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,jns_koreksi")
                    ->where($row)->whereraw("to_char(created_at,'yyyy')='2024' and no_transaksi not like '%KP%'")->get();
               
                foreach ($krs as $kr) {

                    // get tanggal terbit
                    $ct =    DB::connection("oracle_satutujuh")->table("log_sppt")->selectraw("ROWIDTOCHAR(max(rowid)) ri")
                        ->whereraw("
                        kd_kecamatan='" . $kr->kd_kecamatan . "' and 
                        kd_kelurahan='" . $kr->kd_kelurahan . "' and 
                        kd_blok='" . $kr->kd_blok . "' and 
                        no_urut='" . $kr->no_urut . "' and 
                        kd_jns_op='" . $kr->kd_jns_op . "' and 
                        thn_pajak_sppt  ='" . $kr->thn_pajak_sppt  . "' and tgl_terbit_sppt is not null
                    ")->first();

                    if ($ct) {
                        $re = DB::connection("oracle_satutujuh")->table("log_sppt")->select("tgl_terbit_sppt")
                            ->whereRaw("ROWIDTOCHAR(rowid)='" . $ct->ri . "'")->first();
                        DB::connection("oracle_satutujuh")->table("sppt")
                            ->whereraw("  kd_kecamatan='" . $kr->kd_kecamatan . "' and 
                    kd_kelurahan='" . $kr->kd_kelurahan . "' and 
                    kd_blok='" . $kr->kd_blok . "' and 
                    no_urut='" . $kr->no_urut . "' and 
                    kd_jns_op='" . $kr->kd_jns_op . "' and 
                    thn_pajak_sppt  ='" . $kr->thn_pajak_sppt  . "'")
                            ->update(['tgl_terbit_sppt' => new Carbon($re->tgl_terbit_sppt), 'status_pembayaran_sppt' => '0']);
                    }

                    DB::connection("oracle_satutujuh")->table("sppt_koreksi")
                        ->whereraw("kd_kecamatan='" . $kr->kd_kecamatan . "' and 
                                    kd_kelurahan='" . $kr->kd_kelurahan . "' and 
                                    kd_blok='" . $kr->kd_blok . "' and 
                                    no_urut='" . $kr->no_urut . "' and 
                                    kd_jns_op='" . $kr->kd_jns_op . "' and 
                                    thn_pajak_sppt  ='" . $kr->thn_pajak_sppt  . "'")
                        ->delete();
                }
            } catch (\Throwable $th) {
                //throw $th;
                Log::info($th);
            }

            $start++;
        } */
        $this->info("selesai");
    }
}
