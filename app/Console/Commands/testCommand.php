<?php

namespace App\Console\Commands;

use App\Helpers\PembatalanObjek;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class testCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Log::info('Test log ' . date('Y-md H:i:s'));

        $tunggakan = DB::connection("oracle_satutujuh")->select(DB::raw("
        select  id layanan_objek_id,
        pemutakhiran_by,pemutakhiran_at,keterangan_pembatalan,
        kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,'2023' thn_pajak_Sppt, (select pbb_yg_harus_dibayar_sppt from spo.sppt_oltp
        where kd_propinsi=sppt.kd_propinsi and 
        kd_dati2=sppt.kd_dati2 and 
        kd_kecamatan=sppt.kd_kecamatan and 
        kd_kelurahan=sppt.kd_kelurahan and 
        kd_blok=sppt.kd_blok and 
        no_urut=sppt.no_urut and 
        kd_jns_op=sppt.kd_jns_op and 
        thn_pajak_sppt='2023') pbb_yg_harus_dibayar_sppt
         from sim_pbb.layanan_objek sppt
         where id in (
          select b.layanan_objek_id
          from sppt_pembatalan b
          left join sppt_koreksi c on b.kd_propinsi=c.kd_propinsi and
        b.kd_dati2=c.kd_dati2 and
        b.kd_Kecamatan=c.kd_Kecamatan and
        b.kd_kelurahan=c.kd_kelurahan and
        b.kd_blok=c.kd_blok and
        b.no_urut=c.no_urut and
        b.kd_jns_op=c.kd_jns_op and b.thn_pajak_sppt=c.thn_pajak_sppt 
           where c.thn_pajak_sppt is  null
           and to_char(tgl_mulai,'yyyy')='2023'
                      and b.layanan_objek_id is not null and b.layanan_objek_id!='96458')"));
        // $this->info(count($tunggakan));
        $pembatalan = [];
        $dd = [];
        $wh = "";
        // $this->info(json_encode($tunggakan));
        foreach ($tunggakan as $key => $value) {
            # code...
            // $this->info()
            $keterangan = $value->keterangan_pembatalan;
            // if (!in_array('3', ['1', '3'])) {
            $thn_pajak_sppt = $value->thn_pajak_sppt;
            $var['kd_propinsi'] = $value->kd_propinsi;
            $var['kd_dati2'] = $value->kd_dati2;
            $var['kd_kecamatan'] = $value->kd_kecamatan;
            $var['kd_kelurahan'] = $value->kd_kelurahan;
            $var['kd_blok'] = $value->kd_blok;
            $var['no_urut'] = $value->no_urut;
            $var['kd_jns_op'] = $value->kd_jns_op;
            $var['thn_pajak_sppt'] = $value->thn_pajak_sppt;

            $wh .= " ( sppt.kd_propinsi='" . $value->kd_propinsi . "' and 
                                    sppt.kd_dati2='" . $value->kd_dati2 . "' and 
                                    sppt.kd_kecamatan='" . $value->kd_kecamatan . "' and 
                                    sppt.kd_kelurahan='" . $value->kd_kelurahan . "' and 
                                    sppt.kd_blok='" . $value->kd_blok . "' and 
                                    sppt.no_urut='" . $value->no_urut . "' and 
                                    sppt.kd_jns_op='" . $value->kd_jns_op . "' and 
                                    sppt.thn_pajak_sppt='" . $value->thn_pajak_sppt . "' ) or";

            $ins = $var;
            $notr = PembatalanObjek::NoTransaksi('IV', '03');
            $now = new Carbon($value->pemutakhiran_at);
            $ins['tgl_surat'] = $now;
            $ins['no_surat'] = $notr;
            $ins['jns_koreksi'] = '3';
            $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
            $ins['keterangan'] = $keterangan;
            $ins['no_transaksi'] = $notr;
            $ins['created_at'] = $now;
            $ins['created_by'] = $value->pemutakhiran_by;
            $ins['nm_kategori'] = $keterangan;
            $ins['kd_kategori'] = '06';

            $ins['verifikasi_kode'] = 1;
            $ins['verifikasi_keterangan'] = 'O';
            $ins['verifikasi_by'] = $value->pemutakhiran_by;
            $ins['verifikasi_at'] = $now;
            $ins['layanan_objek_id'] = $value->layanan_objek_id ?? '';
            // $this->info($ins);


            $dd[] = $ins;

            /*    // if (in_array($postData['jns_koreksi'], ['03', '04', '05'])) {
            $pembatalan[] = [
                'kd_propinsi' => $value->kd_propinsi,
                'kd_dati2' => $value->kd_dati2,
                'kd_kecamatan' => $value->kd_kecamatan,
                'kd_kelurahan' => $value->kd_kelurahan,
                'kd_blok' => $value->kd_blok,
                'no_urut' => $value->no_urut,
                'kd_jns_op' => $value->kd_jns_op,
                'thn_pajak_sppt' => $value->thn_pajak_sppt,
                'layanan_objek_id' =>  $value->layanan_objek_id ?? '',
                'tgl_mulai' => $now
            ]; */
            // }
            // }
        }
        // $this->info(json_encode($wh));
        if (count($dd) > 0) {
            $wh = substr($wh, 0, -2);
            $ws = str_replace('sppt.', '', $wh);
            if (count($pembatalan) > 0) {
                DB::connection("oracle_satutujuh")->table('sppt_pembatalan')
                    ->whereraw($ws)
                    ->whereraw("tgl_selesai=to_date('31129999','ddmmyyyy')")
                    ->update(['tgl_selesai' => db::raw("sysdate-1")]);

                DB::connection("oracle_satutujuh")->table('sppt_pembatalan')->insert($pembatalan);
            }

            DB::connection("oracle_satutujuh")->table('sppt_koreksi')->whereraw($ws)->delete();
            DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);

            DB::connection("oracle_satutujuh")->table('sppt')->whereraw($wh)->update(['status_pembayaran_sppt' => '3']);
        }
    }
}
