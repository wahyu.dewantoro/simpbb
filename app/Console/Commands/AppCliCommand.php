<?php

namespace App\Console\Commands;

use App\Exports\ArrayExport;
use App\Helpers\PembatalanObjek;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class AppCliCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pbb:data {--data=} {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Validasi opsi wajib
        $jenis = $this->option('data');
        $tahun = $this->option('tahun');
        $kd_kecamatan = $this->option('kd_kecamatan');

        if (!$jenis) {
            $this->error('Opsi --data wajib diberikan.');
            return 1; // Kode error untuk proses gagal
        }


        if (!$kd_kecamatan && $jenis != 'list_wp') {
            $this->error('Opsi --kd_kecamatan wajib diberikan.');
            return 1; // Kode error untuk proses gagal
        }

        // Ambil opsi opsional
        $kd_kelurahan = $this->option('kd_kelurahan');


        switch ($jenis) {
            case 'sistep':
                $kdkecamatan = $kd_kecamatan;
                $kdkelurahan = $kd_kelurahan;
                $no_surat = '900.1.13.1/3866/35.07.404/2024';
                $tgl_surat = '2024-09-03';


                DB::connection('oracle_satutujuh')->beginTransaction();
                try {

                    $keterangan = "Pembatalan karena objek di migrasi ke sismiop";
                    $tunggakan = DB::connection("oracle_satutujuh")->select(DB::raw("select sppt.kd_propinsi,
                                                                    sppt.kd_dati2,
                                                                    sppt.kd_kecamatan,
                                                                    sppt.kd_kelurahan,
                                                                    sppt.kd_blok,
                                                                    sppt.no_urut,
                                                                    sppt.kd_jns_op,
                                                                    sppt.thn_pajak_sppt,
                                                                    jns_koreksi,
                                                                    sppt.status_pembayaran_sppt,
                                                                    oltp.pbb_yg_harus_dibayar_sppt
                                                            from sppt
                                                                    left join sppt_koreksi
                                                                    on     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                                                        and sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                                                        and sppt.kd_kecamatan = sppt_koreksi.kd_kecamatan
                                                                        and sppt.kd_kelurahan = sppt_koreksi.kd_kelurahan
                                                                        and sppt.kd_blok = sppt_koreksi.kd_blok
                                                                        and sppt.no_urut = sppt_koreksi.no_urut
                                                                        and sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                                                        and sppt.thn_pajak_sppt = sppt_koreksi.thn_pajak_sppt
                                                                    left join sppt_pembatalan
                                                                    on     sppt.kd_propinsi = sppt_pembatalan.kd_propinsi
                                                                        and sppt.kd_dati2 = sppt_pembatalan.kd_dati2
                                                                        and sppt.kd_kecamatan = sppt_pembatalan.kd_kecamatan
                                                                        and sppt.kd_kelurahan = sppt_pembatalan.kd_kelurahan
                                                                        and sppt.kd_blok = sppt_pembatalan.kd_blok
                                                                        and sppt.no_urut = sppt_pembatalan.no_urut
                                                                        and sppt.kd_jns_op = sppt_pembatalan.kd_jns_op
                                                                        and sppt.thn_pajak_sppt = sppt_pembatalan.thn_pajak_sppt
                                                                        and tgl_mulai <= sysdate
                                                                        and tgl_selesai >= sysdate
                                                                    left join spo.sppt_oltp oltp
                                                                    on     sppt.kd_propinsi = oltp.kd_propinsi
                                                                        and sppt.kd_dati2 = oltp.kd_dati2
                                                                        and sppt.kd_kecamatan = oltp.kd_kecamatan
                                                                        and sppt.kd_kelurahan = oltp.kd_kelurahan
                                                                        and sppt.kd_blok = oltp.kd_blok
                                                                        and sppt.no_urut = oltp.no_urut
                                                                        and sppt.kd_jns_op = oltp.kd_jns_op
                                                                        and sppt.thn_pajak_sppt = oltp.thn_pajak_sppt
                                                            where     sppt.kd_propinsi = '35'
                                                                    and sppt.kd_dati2 = '07'
                                                                    and sppt.kd_kecamatan = '$kdkecamatan'
                                                                    and sppt.kd_kelurahan = '$kdkelurahan'
                                                                    and sppt.kd_jns_op='7'
                                                                    and sppt.status_pembayaran_sppt != '1'
                                                                    and sppt_pembatalan.thn_pajak_sppt is null
                                                                    and sppt_koreksi.thn_pajak_sppt is null"));
                    $now = Carbon::now();
                    $userid = 1678;
                    $dd = [];
                    $wh = "";
                    // return $tunggakan;
                    $this->info("tunggakan :" . count($tunggakan));
                    foreach ($tunggakan as $key => $value) {
                        # code...
                        if (!in_array($value->jns_koreksi, ['1', '3'])) {
                            $thn_pajak_sppt = $value->thn_pajak_sppt;
                            // $ins = $var;
                            $ins['kd_propinsi'] = $value->kd_propinsi;
                            $ins['kd_dati2'] = $value->kd_dati2;
                            $ins['kd_kecamatan'] = $value->kd_kecamatan;
                            $ins['kd_kelurahan'] = $value->kd_kelurahan;
                            $ins['kd_blok'] = $value->kd_blok;
                            $ins['no_urut'] = $value->no_urut;
                            $ins['kd_jns_op'] = $value->kd_jns_op;
                            $ins['thn_pajak_sppt'] = $thn_pajak_sppt;
                            $notr = PembatalanObjek::NoTransaksi('SM', '06');
                            // $now = Carbon::now();
                            $ins['tgl_surat'] = new Carbon($tgl_surat);
                            $ins['no_surat'] = $no_surat;
                            $ins['jns_koreksi'] = '3';
                            $ins['nilai_koreksi'] = onlyNumber($value->pbb_yg_harus_dibayar_sppt);
                            $ins['keterangan'] = $keterangan;
                            $ins['no_transaksi'] = $notr;
                            $ins['created_at'] = $now;
                            $ins['created_by'] = $userid;
                            $ins['nm_kategori'] = $keterangan;
                            $ins['kd_kategori'] = '06';

                            $ins['verifikasi_kode'] = 1;
                            $ins['verifikasi_keterangan'] = $keterangan;
                            $ins['verifikasi_by'] = $userid;
                            $ins['verifikasi_at'] = $now;
                            $dd[] = $ins;
                        }
                    }


                    $bj = DB::connection("oracle_satutujuh")->table("dat_op_bumi")
                        ->whereraw("kd_kecamatan='$kdkecamatan' and kd_kelurahan='$kdkelurahan' 
                        and kd_jns_op='7' 
                        and jns_bumi not in ('4','5')")->get();
                    $wha = "";
                    foreach ($bj as $value) {
                        $wha .= " ( kd_propinsi='" . $value->kd_propinsi . "' and 
                        kd_dati2='" . $value->kd_dati2 . "' and 
                        kd_kecamatan='" . $value->kd_kecamatan . "' and 
                        kd_kelurahan='" . $value->kd_kelurahan . "' and 
                        kd_blok='" . $value->kd_blok . "' and 
                        no_urut='" . $value->no_urut . "' and 
                        kd_jns_op='" . $value->kd_jns_op . "') or";
                    }

                    // return $dd;
                    if (count($dd) > 0) {
                        DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($dd);
                    }
                    // dd($dd);

                    // dd($postData);
                    if ($wh != "") {
                        DB::connection("oracle_satutujuh")->statement("update sppt set status_pembayaran_sppt='3' where   (" . substr($wh, 0, -2) . ")");
                    }

                    if ($wha != "") {
                        DB::connection("oracle_satutujuh")->statement("update dat_op_bumi set jns_bumi='5' where  jns_bumi not in ('4','5') and  (" . substr($wha, 0, -2) . ")");
                    }
                    DB::connection("oracle_satutujuh")->commit();
                    // $with = ['success' => 'Telah berhasil di batalkan'];

                    $this->info('Telah berhasil di batalkan');
                } catch (\Throwable $th) {
                    //throw $th;
                    Log::error($th);

                    DB::connection('oracle_satutujuh')->commit();
                    // $with = ['error' => $th->getMessage()];
                    $this->error($th->getMessage());
                }

                break;
            case 'rekap-sppt':
                # code...
                if (!$tahun) {
                    $this->error('Opsi --tahun wajib diberikan.');
                    return 1; // Kode error untuk proses gagal
                }


                $this->info('Memulai proses export data : ' . $kd_kecamatan . ' - ' . $kd_kelurahan);
                $sppt = DB::connection("oracle_satutujuh")->table('sppt')
                    ->selectRaw("
                    sppt.kd_propinsi || '.' || 
                    sppt.kd_dati2 || '.' || 
                    sppt.kd_kecamatan || '.' || 
                    sppt.kd_kelurahan || '.' || 
                    sppt.kd_blok || '.' || 
                    sppt.no_urut || '.' ||  
                    sppt.kd_jns_op AS nop,
                    sppt.thn_pajak_sppt,
                    luas_bumi_sppt,
                    CASE 
                        WHEN luas_bumi_sppt > 0 THEN njop_bumi_sppt / luas_bumi_sppt 
                        ELSE 0 
                    END AS njop_bumi,
                    luas_bng_sppt,
                    CASE 
                        WHEN luas_bng_sppt > 0 THEN njop_bng_sppt / luas_bng_sppt 
                        ELSE 0 
                    END AS njop_bng,
                    njoptkp_sppt,
                    hkpd,
                    tarif,
                    sppt.pbb_yg_harus_dibayar_sppt AS pbb,
                    NVL(nilai_potongan, 0) AS insentif,
                    sppt.pbb_yg_harus_dibayar_sppt - NVL(nilai_potongan, 0) AS harus_bayar,
                    dat_op_bumi.kd_znt,
                    nir,
                    get_buku(sppt.pbb_yg_harus_dibayar_sppt) AS buku")
                    ->leftJoin('dat_op_bumi', function ($join) {
                        $join->on('sppt.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                            ->on('sppt.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                            ->on('sppt.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                            ->on('sppt.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                            ->on('sppt.kd_blok', '=', 'dat_op_bumi.kd_blok')
                            ->on('sppt.no_urut', '=', 'dat_op_bumi.no_urut')
                            ->on('sppt.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
                    })
                    ->leftJoin('dat_nir', function ($join) use ($tahun) {
                        $join->on('sppt.kd_propinsi', '=', 'dat_nir.kd_propinsi')
                            ->on('sppt.kd_dati2', '=', 'dat_nir.kd_dati2')
                            ->on('sppt.kd_kecamatan', '=', 'dat_nir.kd_kecamatan')
                            ->on('sppt.kd_kelurahan', '=', 'dat_nir.kd_kelurahan')
                            ->on('dat_nir.kd_znt', '=', 'dat_op_bumi.kd_znt')
                            ->where('dat_nir.thn_nir_znt', '=', $tahun);
                    })
                    ->leftJoin('sppt_potongan_detail', function ($join) {
                        $join->on('sppt.kd_propinsi', '=', 'sppt_potongan_detail.kd_propinsi')
                            ->on('sppt.kd_dati2', '=', 'sppt_potongan_detail.kd_dati2')
                            ->on('sppt.kd_kecamatan', '=', 'sppt_potongan_detail.kd_kecamatan')
                            ->on('sppt.kd_kelurahan', '=', 'sppt_potongan_detail.kd_kelurahan')
                            ->on('sppt.kd_blok', '=', 'sppt_potongan_detail.kd_blok')
                            ->on('sppt.no_urut', '=', 'sppt_potongan_detail.no_urut')
                            ->on('sppt.kd_jns_op', '=', 'sppt_potongan_detail.kd_jns_op')
                            ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan_detail.thn_pajak_sppt');
                    })
                    ->where('sppt.thn_pajak_sppt', '=', $tahun)
                    ->where('sppt.kd_kecamatan', '=', $kd_kecamatan)
                    ->where('sppt.kd_kelurahan', '=', $kd_kelurahan)
                    // ->limit(3)
                    ->get();




                // Gabungkan header dan data
                $arrayVar = [];
                $arrayVar[] = [
                    'No',
                    'NOP',
                    'Tahun Pajak',
                    'Luas Bumi',
                    'NJOP Bumi',
                    'Luas Bng',
                    'NJOP Bng',
                    'NJOPTKP',
                    'HKPD',
                    'Tarif',
                    'PBB',
                    'Insentif',
                    'Harus Bayar',
                    'ZNT',
                    'NIR',
                    'BUKU',
                ];


                $total = count($sppt); // Total data yang akan diproses
                $current = 0;          // Mulai dari 0
                foreach ($sppt as $key => $item) {
                    $current++;
                    $arrayVar[] = [
                        $key + 1, // Nomor
                        $item->nop,
                        $item->thn_pajak_sppt,
                        $item->luas_bumi_sppt,
                        $item->njop_bumi,
                        $item->luas_bng_sppt,
                        $item->njop_bng,
                        $item->njoptkp_sppt,
                        $item->hkpd,
                        $item->tarif,
                        $item->pbb,
                        $item->insentif,
                        $item->harus_bayar,
                        $item->kd_znt,
                        $item->nir,
                        $item->buku,
                    ];

                    // Tampilkan progress bar
                    $percentage = intval(($current / $total) * 100);
                    echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
                    usleep(100000); // Delay untuk efek progress
                }

                $fileName = 'sppt_data ' . $tahun . ' - ' . $kd_kecamatan . ' - ' . $kd_kelurahan . '.xlsx';
                $disk = 'tarikan'; // Disk yang digunakan (misalnya, "tarikan")


                // Cek apakah file sudah ada
                if (Storage::disk($disk)->exists($fileName)) {
                    // Hapus file jika sudah ada
                    Storage::disk($disk)->delete($fileName);
                }

                // Simpan file ke storage disk "tarikan"
                Excel::store(new ArrayExport($arrayVar), $fileName, $disk);



                // $this->info('Berhasil export data');
                echo "\n"; // Pindahkan ke baris baru setelah progress selesai
                $this->info('Proses export data selesai.');
                Command::SUCCESS;

                break;

            case 'list_wp':
                # code...
                // $this->info("list+sppt");
                for ($thn = 2003; $thn <= 2024; $thn++) {
                    $this->info("memproses tahun :" . $thn);
                    $sppt = DB::connection("oracle_satutujuh")->select("SELECT
                                            a.kd_propinsi || '.' || a.kd_dati2 || '.' || a.kd_kecamatan || '.' || a.kd_kelurahan || '.' || a.kd_blok || '.' || a.no_urut || '.' || a.kd_jns_op nop,
                                            jalan_op || ' ' || nm_kelurahan || ' ' || nm_kecamatan alamat_op,
                                            a.thn_pajak_sppt,
                                            to_char( b.tgl_terbit_sppt, 'yyyy' ) thn_terbit_sppt,
                                            nm_wp_sppt nama_wp,
                                            jln_wp_sppt || ' ' || blok_kav_no_wp_sppt || ' ' || kelurahan_wp_sppt || ' ' || kota_wp_sppt alamat_wp 
                                        FROM
                                            tmp_v_cut_saldo_24_tiga a
                                            JOIN dat_objek_pajak c ON a.kd_propinsi = c.kd_propinsi 
                                            AND a.kd_dati2 = c.kd_dati2 
                                            AND a.kd_kecamatan = c.kd_kecamatan 
                                            AND a.kd_kelurahan = c.kd_kelurahan 
                                            AND a.kd_blok = c.kd_blok 
                                            AND a.no_urut = c.no_urut 
                                            AND a.kd_jns_op = c.kd_jns_op
                                            JOIN ref_kecamatan d ON d.kd_kecamatan = a.kd_kecamatan
                                            JOIN ref_kelurahan e ON e.kd_kecamatan = a.kd_kecamatan 
                                            AND e.kd_Kelurahan = a.kd_kelurahan
                                            LEFT JOIN sppt b ON a.kd_propinsi = b.kd_propinsi 
                                            AND a.kd_dati2 = b.kd_dati2 
                                            AND a.kd_kecamatan = b.kd_kecamatan 
                                            AND a.kd_kelurahan = b.kd_kelurahan 
                                            AND a.kd_blok = b.kd_blok 
                                            AND a.no_urut = b.no_urut 
                                            AND a.kd_jns_op = b.kd_jns_op 
                                            AND a.thn_pajak_sppt = b.thn_pajak_sppt 
                                        where TAHUN_TERBIT='" . $thn . "'");

                    // Gabungkan header dan data
                    $arrayVar = [];
                    $arrayVar[] = [
                        'No',
                        'NOP',
                        'ALAMAT_OP',
                        'THN_PAJAK_SPPT',
                        'THN_TERBIT_SPPT',
                        'NAMA_WP',
                        'ALAMAT_WP',
                    ];


                    $total = count($sppt); // Total data yang akan diproses
                    $current = 0;          // Mulai dari 0
                    foreach ($sppt as $key => $item) {
                        $current++;
                        $arrayVar[] = [
                            $key + 1, // Nomor
                            $item->nop,
                            $item->alamat_op,
                            $item->thn_pajak_sppt,
                            $item->thn_terbit_sppt,
                            $item->nama_wp,
                            $item->alamat_wp,
                        ];

                        // Tampilkan progress bar
                        $percentage = intval(($current / $total) * 100);
                        echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
                        usleep(100000); // Delay untuk efek progress
                    }

                    $fileName = 'daftar nama tahun ' . $thn  . '.xlsx';
                    $disk = 'tarikan'; // Disk yang digunakan (misalnya, "tarikan")


                    // Cek apakah file sudah ada
                    if (Storage::disk($disk)->exists($fileName)) {
                        // Hapus file jika sudah ada
                        Storage::disk($disk)->delete($fileName);
                    }

                    // Simpan file ke storage disk "tarikan"
                    Excel::store(new ArrayExport($arrayVar), $fileName, $disk);



                    // $this->info('Berhasil export data');
                    echo "\n"; // Pindahkan ke baris baru setelah progress selesai
                    $this->info('Proses export data selesai.');
                }
                Command::SUCCESS;
                break;
            default:
                # code...
                $this->info('nothing');
                break;
        }

        return 0; // Kode sukses
    }
}
