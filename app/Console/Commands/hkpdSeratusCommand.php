<?php

namespace App\Console\Commands;

use App\Helpers\PenetapanSppt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class hkpdSeratusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hkpd:seratus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $objek = config('hkpdseratus.nop');


        $no = 1;
        foreach ($objek as $nop_asal) {

            $nop_asal = onlyNumber($nop_asal);

            $kd_propinsi = substr($nop_asal, 0, 2);
            $kd_dati2 = substr($nop_asal, 2, 2);
            $kd_kecamatan = substr($nop_asal, 4, 3);
            $kd_kelurahan = substr($nop_asal, 7, 3);
            $kd_blok = substr($nop_asal, 10, 3);
            $no_urut = substr($nop_asal, 13, 4);
            $kd_jns_op = substr($nop_asal, 17, 1);
            $tahun = '2024';
            $hkpd = [
                'thn_hkpd' => $tahun,
                'kd_propinsi' => $kd_propinsi,
                'kd_dati2' => $kd_dati2,
                'kd_kecamatan' => $kd_kecamatan,
                'kd_kelurahan' => $kd_kelurahan,
                'kd_blok' => $kd_blok,
                'no_urut' => $no_urut,
                'kd_jns_op' => $kd_jns_op,
                'hkpd' => 100
            ];

            $this->info($no . " proses hkpd " . $nop_asal);
            DB::connection("oracle_satutujuh")->beginTransaction();
            try {
                //code...
                $ch = DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                    ->where([
                        'thn_hkpd' => $tahun,
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op
                    ])
                    ->count();
                if ($ch == 0) {
                    DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")->insert($hkpd);
                } else {
                    DB::connection("oracle_satutujuh")->table("HKPD_OBJEK_INDIVIDU")
                        ->where([
                            'thn_hkpd' => $tahun,
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op
                        ])
                        ->update($hkpd);
                }

                DB::connection("oracle_satutujuh")->commit();
            } catch (\Throwable $th) {
                //throw $th;
                DB::connection("oracle_satutujuh")->rollBack();
                Log::alert($th);
            }

            $tgl_terbit = $tahun . '0102';
            $tgl_jatuh_tempo = $tahun . '0831';
            $this->info($no . " proses penetapan ulang  " . $nop_asal);
            PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun, $tgl_terbit, $tgl_jatuh_tempo);

            $no++;
        }

        $this->info("selesai");
    }
}
