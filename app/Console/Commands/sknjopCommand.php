<?php

namespace App\Console\Commands;

use App\SkNJop;
use App\skNjopDokumen;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use SnappyPdf as PDF;

class sknjopCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sknjop {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate dokumen sk njop';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    //  protected $id
     
    public function __construct()
    {
        parent::__construct();
        // $this->id=$id;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id=$this->argument('id');
        $sknjop = SkNJop::findorfail($id);
        $url = url('sknjop-pdf', $id);
        $pages = View::make('sknjop/_pdf', compact('sknjop', 'url'));
        $pdf = pdf::loadHTML($pages)
            ->setPaper('A4');
        $path = 'sk' . time() . '.pdf';

        $pdf->setOption('enable-local-file-access', true);
       
        $disk = Storage::disk('sknjop');
        if ($disk->put($path, $pdf->output())) {
            $file = [
                'sknjop_id' => $id,
                'disk' => 'sknjop',
                'path' => $path,
                'filename' => str_replace('/', ' ', $sknjop->nomer_sk).'.pdf'
            ];
            $res=skNjopDokumen::create($file);
            Log::info("SK NJOP " . $sknjop->nomer_sk . " telah di buat");
            $this->info("SK NJOP " . $sknjop->nomer_sk . " telah di buat");
            return $res;
        } else {
            log::warning("SK NJOP " . $sknjop->nomer_sk . " gagal di buat");
            $this->error("SK NJOP " . $sknjop->nomer_sk . " gagal di buat");
        }
    }
}
