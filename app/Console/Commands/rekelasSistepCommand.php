<?php

namespace App\Console\Commands;

use App\Models\PerubahanZnt;
use App\Models\PerubahanZntObjek;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class rekelasSistepCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pendataan:rekelas {--kd_kecamatan=}   {--kd_kelurahan=}  {--tahun=} {--naik=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'rekelas objek sistep';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $kd_kecamatan = $this->option('kd_kecamatan') ?? '';
        $kd_kelurahan = $this->option('kd_kelurahan') ?? '';
        $naik = $this->option('naik') ?? '';
        $tahun = $this->option('tahun') ?? date('Y');

        $now = Carbon::now();
        if (
            $kd_kecamatan == '' ||
            $kd_kelurahan == '' ||
            $naik == ''
        ) {
            $this->error("Harus memasukan kecamatan kelurahan");
        } else {

            // get objek;
            $this->info('Identifikasi data');
            DB::beginTransaction();
            try {
                //code...

                $batch = DB::connection("oracle_satutujuh")->select("select * from (select distinct kd_propinsi,kd_dati2, kd_kecamatan,kd_kelurahan,kd_blok,kd_znt 
            from dat_op_bumi
            where (kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan')  
            and regexp_replace(kd_znt, '[^0-9]', '') is not null )");
           
                foreach ($batch as $bb) {

                    $znt_baru = (int)$bb->kd_znt - (int)$naik;

                    if ($znt_baru < 0) {
                        $znt_baru = 1;
                    }

                    $znt_baru = str_pad($znt_baru, 2, '0');
                    // $pb['nomor_batch'] = '';
                    $pb['kd_propinsi'] = $bb->kd_propinsi;
                    $pb['kd_dati2'] = $bb->kd_dati2;
                    $pb['kd_kecamatan'] = $bb->kd_kecamatan;
                    $pb['kd_kelurahan'] = $bb->kd_kelurahan;
                    $pb['kd_blok'] = $bb->kd_blok;
                    $pb['kd_znt_lama'] = $bb->kd_znt;
                    $pb['kd_znt_baru'] = $znt_baru;
                    $pb['keterangan'] = 'Rekelas ' . $tahun;
                    $pb['verifikasi_kode'] = '1';
                    $pb['verifikasi_by'] = '1678';
                    $pb['verifikasi_at'] = $now;
                    $pb['created_by'] = '1678';
                    $pb['created_at'] = '1678';

                    $znt = PerubahanZnt::create($pb);
                    $nobatch = $znt->nomor_batch;
                    $zntid = $znt->id;
                    $objek_ke = 1;
                    $kd_znt_baru = $znt_baru;

                    $this->info('Memproses data batch ' . $znt->nomor_batch);

                    $objeks = DB::connection("oracle_satutujuh")->select("select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op ,luas_bumi,no_bumi
                                         from dat_op_bumi
                                        where (kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' and kd_znt='" . $pb['kd_znt_lama'] . "')");

                    foreach ($objeks as $objek) {

                        $objek = (array)$objek;
                        $this->info("Proses objek ke" . $objek_ke . ' pada batch ' . $nobatch);
                        $objek['perubahan_znt_id'] = $zntid;
                        $abc = $objek;
                        unset($abc['luas_bumi']);
                        unset($abc['no_bumi']);
                        PerubahanZntObjek::insert($abc);

                        // dat_op_znt
                        $dat_op_znt = [
                            'kd_propinsi' => $objek['kd_propinsi'],
                            'kd_dati2' => $objek['kd_dati2'],
                            'kd_kecamatan' => $objek['kd_kecamatan'],
                            'kd_kelurahan' => $objek['kd_kelurahan'],
                            'kd_blok' => $objek['kd_blok'],
                            'no_urut' => $objek['no_urut'],
                            'kd_jns_op' => $objek['kd_jns_op'],
                            'thn_znt' => $tahun,
                            'kd_znt' => $kd_znt_baru,
                            'nomor_batch' => $nobatch
                        ];
                        $where = [
                            'kd_propinsi' => $dat_op_znt['kd_propinsi'],
                            'kd_dati2' => $dat_op_znt['kd_dati2'],
                            'kd_kecamatan' => $dat_op_znt['kd_kecamatan'],
                            'kd_kelurahan' => $dat_op_znt['kd_kelurahan'],
                            'kd_blok' => $dat_op_znt['kd_blok'],
                            'no_urut' => $dat_op_znt['no_urut'],
                            'kd_jns_op' => $dat_op_znt['kd_jns_op'],
                            'thn_znt' => $dat_op_znt['thn_znt']
                        ];

                        // $this->info('Merekam  dat op znt dari batch ' . $znt->nomor_batch . ' =>' . json_encode($where));

                        $ck = DB::connection("oracle_satutujuh")
                            ->table("DAT_OP_ZNT")
                            ->where($where)
                            ->delete();

                        $dat_op_znt['created_at'] = $now;
                        $dat_op_znt['created_by'] = 1678;

                        DB::connection("oracle_satutujuh")->table("DAT_OP_ZNT")->insert($dat_op_znt);

                   

                        $objek_ke++;
                    }
                 
                }

                $this->info('Identifikasi data selesai');
                DB::commit();
            } catch (\Throwable $th) {
                //throw $th;
                DB::rollBack();
                Log::error($th);
                $this->error($th->getMessage());
            }

            $this->info('selesai');
        }
    }
}
