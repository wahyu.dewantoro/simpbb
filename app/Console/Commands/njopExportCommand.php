<?php

namespace App\Console\Commands;

use App\Exports\NjopPbbExport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class njopExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:njop_pbb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $kec = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
            ->whereraw("to_number(kd_kecamatan)>=250")
            ->orderby('kd_kecamatan')->get();

        foreach ($kec as $row) {
            $this->info('Sedang memproses ' . $row->nm_kecamatan);
            $filename = 'NJOP PBB  kecamatan ' .  $row->nm_kecamatan . ' - ' .  time() . '.xlsx';
            Excel::store(new NjopPbbExport($row->kd_kecamatan), $filename, 'reports');
            $this->info($filename . ' created');
        }
        $this->info('Selesai...');
    }
}
