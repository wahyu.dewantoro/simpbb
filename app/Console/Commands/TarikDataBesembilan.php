<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TarikDataBesembilan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'besembilan:pbb {--tanggal=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Penarikan data mutasi besembilan dari bank jatim';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set("memory_limit", -1);
        $tanggal = $this->option('tanggal') ?? date('Ymd');
        $this->info($tanggal);
        $url = 'https://majapahit.bankjatim.co.id/digital/rest/nontrx/v';
        // get key
        $keys = DB::table('besembilan_key')
            ->select(db::raw("source,keyid"))
            ->get();

        // Log::info($keys);

        $pesanwa = "*MUTASI BESEMBILAN* <br>pembayaran PBB Kab Malang<br><br>Tanggal :*" . tglindo($tanggal) . '*<br><br>';

        foreach ($keys as $rk) {
            $this->info('Sedang menarik data ' . $rk->source);
            $response = Http::post($url, [
                'key' => $rk->keyid,
                'tglawal' => $tanggal,
                'tglakhir' => $tanggal
            ]);

            $res = $response->json();

            // Log::info($res);


            $data = $res['history'] ?? [];

            $status = $response->successful();
            if ($status == true) {

                // hapus data dulu
                DB::statement(DB::raw("begin delete from besembilan_mutasi where source='" . $rk->source . "' and trunc(datetime)= to_date('" . $tanggal . "','yyyymmdd'); commit; end;"));

                // generate data bulk insert
                $bulk = [];
                foreach ($data as $row) {
                    $bulk[] = [
                        'dateTime' => new Carbon($row['dateTime']),
                        'description' => $row['description'],
                        'transactionCode' => $row['transactionCode'],
                        'amount' => $row['amount'],
                        'flag' => $row['flag'],
                        'ccy' => $row['ccy'],
                        'reffno' => $row['reffno'],
                        'source' => $rk->source,
                        'created_at' => Carbon::now()
                    ];
                }
                // $bulk = array_unique($bulk);
                DB::table('besembilan_mutasi')->insert($bulk);

                $pesanwa .= 'source :*' . $rk->source . '*<br>responseDesc :' . $res['responseDesc'] . '<br>responseCode :' . $res['responseCode'] . '<br> Jumlah :' . count($data) . ' transaksi <br><br>-------------<br>';
            } else {
                $pesanwa .= 'source :*' . $rk->source . '*<br>GAGAL Menarik data<br><br>-------------<br>';
            }
        }

        // send wa
        /* $wa = Http::withToken('8urAVeIotGVD32lzPC42GeI6ReJ15lAZxP8zN4X0SRtOYDqUHz')->post('192.168.1.205/ncm/api/send_message', [
            'list_phone' => '082330319913',
            'message' => $pesanwa
        ]); */
    }
}
