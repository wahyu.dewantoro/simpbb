<?php

namespace App\Console\Commands;

use App\Jobs\RekonPembayaranJob;
use Illuminate\Console\Command;

class rekonPembayaranCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     * 
     * 
     */

     /* 
./artisan rekon:batch --id=1128 && ./artisan rekon:batch --id=1127 && ./artisan rekon:batch --id=1125 && ./artisan rekon:batch --id=1123 
 */
    protected $signature = 'rekon:batch  {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rekon Pembayaran by batch uppload';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->option('id');
        dispatch(new RekonPembayaranJob($id))->onQueue('rekon_lokal');
    }
}
