<?php

namespace App\Console\Commands;

use App\Helpers\PembatalanObjek;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ReadExcelCommand extends Command
{
    protected $signature = 'excel:read {--option= : Specify the option to execute} {file}';
    protected $description = 'Read an Excel file and process its data';

    public function handle()
    {
        $option = $this->option('option');

        if (!$option) {
            $this->info('No --option was provided.');
            return Command::INVALID;
        }

        switch ($option) {
            case 'koreksi':
                $this->Koreksi();
                return Command::SUCCESS;
            case 'iventarisasi':
                $this->Iventarisasi();
                return Command::SUCCESS;
            default:
                $this->alert("Option '$option' is not available.");
                return Command::FAILURE;
        }
    }


    private function Iventarisasi()
    {
        $filePath = $this->argument('file');

        if (!file_exists($filePath)) {
            $this->error("File not found: $filePath");
            return 1;
        }

        // Membaca file Excel
        $rows = Excel::toArray([], $filePath);

        $total = count($rows[0]) - 1; // Total data yang akan diproses
        $current = 0;
        foreach ($rows[0] as $i => $row) {
            // Proses setiap baris data
            if ($i > 0) {
                $current++;
                $kd_propinsi = $row[1];
                $kd_dati2 = $row[2];
                $kd_kecamatan = $row[3];
                $kd_kelurahan = $row[4];
                $kd_blok = $row[5];
                $no_urut = $row[6];
                $kd_jns_op = $row[7];
                $thn_pajak_sppt = $row[8];
                $nomor = $row[9];
                $keterangan = $row[10];
                $jns_koreksi = '01';

                // cek lunas ke spo nya 

                $spo = DB::connection("oracle_spo")->table("sppt_oltp")->where([
                    'kd_propinsi' => $kd_propinsi,
                    'kd_dati2' => $kd_dati2,
                    'kd_kecamatan' => $kd_kecamatan,
                    'kd_kelurahan' => $kd_kelurahan,
                    'kd_blok' => $kd_blok,
                    'no_urut' => $no_urut,
                    'kd_jns_op' => $kd_jns_op,
                    'thn_pajak_sppt' => $thn_pajak_sppt
                ])->first();

                // sppt.pbb_terhutang_sppt=sppt.pbb_yg_harus_dibayar_sppt

                if ($spo) {
                    $status = $spo->status_pembayaran_sppt ?? '0';
                    $a =( $spo->pbb_terhutang_sppt -  $spo->faktor_pengurang_sppt);
                    $b = $spo->pbb_yg_harus_dibayar_sppt;

                    if ($status == '0'  &&  $a == $b) {
                        // proses IV
                        $tgl =  Carbon::parse('2024-12-31');


                        $data['thn_pajak_sppt'] = $thn_pajak_sppt;
                        $data['no_surat'] = $nomor;
                        $data['tgl_surat'] = $tgl;
                        $data['jns_koreksi'] = $jns_koreksi;
                        $data['keterangan'] = $keterangan;


                        $data['tahun'] = '2024';
                        $data['kd_propinsi'] = $kd_propinsi;
                        $data['kd_dati2'] = $kd_dati2;
                        $data['kd_kecamatan'] = $kd_kecamatan;
                        $data['kd_kelurahan'] = $kd_kelurahan;
                        $data['kd_blok'] = $kd_blok;
                        $data['no_urut'] = $no_urut;
                        $data['kd_jns_op'] = $kd_jns_op;

                        // $arrayBatal = ['01', '02', '03'];
                        // if (in_array($request->jns_koreksi, $arrayBatal)) {
                        $res = PembatalanObjek::iventarisasi($data);
                        log::info(json_encode($res));
                        /* } elseif ($request->jns_koreksi == '07') {
                            $res = PembatalanObjek::penyesuaian($data);
                        } else {
                            $res = PembatalanObjek::cancel($data);
                        } */
                    } else {
                        log::info("tidak di proses");
                    }
                } else {
                    log::info("data tidak ada");
                }
                // Tampilkan progress bar
                $percentage = intval(round(($current / $total) * 100, 2));
                echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
                usleep(100000); // Delay untuk efek progress
            }
        }

        $this->info("File processed successfully.");
        return 0;
    }

    private function Koreksi()
    {

        $filePath = $this->argument('file');

        if (!file_exists($filePath)) {
            $this->error("File not found: $filePath");
            return 1;
        }

        // Membaca file Excel
        $rows = Excel::toArray([], $filePath);

        $total = count($rows[0]) - 1; // Total data yang akan diproses
        $current = 0;
        foreach ($rows[0] as $i => $row) {
            // Proses setiap baris data
            if ($i > 0) {
                $current++;
                $kd_propinsi = $row[1];
                $kd_dati2 = $row[2];
                $kd_kecamatan = $row[3];
                $kd_kelurahan = $row[4];
                $kd_blok = $row[5];
                $no_urut = $row[6];
                $kd_jns_op = $row[7];
                $thn_pajak_sppt = $row[8];
                $nomor = $row[9];
                $keterangan = $row[10];

                $cek = DB::connection("oracle_satutujuh")->table('sppt_koreksi')
                    ->where([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $thn_pajak_sppt
                    ])->exists();

                $jenis = '3';
                switch ($jenis) {
                    case '1':
                        # code...
                        $jns_koreksi = '3';
                        $kd = 'IV';
                        break;
                    default:
                        # code...
                        $jns_koreksi = '3';
                        $kd = 'KP';
                        break;
                }

                // nilai kp

                $oltp = DB::connection("oracle_spo")->table('sppt_oltp')
                    ->where([
                        'kd_propinsi' => $kd_propinsi,
                        'kd_dati2' => $kd_dati2,
                        'kd_kecamatan' => $kd_kecamatan,
                        'kd_kelurahan' => $kd_kelurahan,
                        'kd_blok' => $kd_blok,
                        'no_urut' => $no_urut,
                        'kd_jns_op' => $kd_jns_op,
                        'thn_pajak_sppt' => $thn_pajak_sppt
                    ])->first();
                $nilai_koreksi = $oltp->pbb_yg_harus_dibayar_sppt ?? 0;

                $trx = PembatalanObjek::NoTransaksi($kd, '07');

                $var['kd_propinsi'] = $kd_propinsi;
                $var['kd_dati2'] = $kd_dati2;
                $var['kd_kecamatan'] = $kd_kecamatan;
                $var['kd_kelurahan'] = $kd_kelurahan;
                $var['kd_blok'] = $kd_blok;
                $var['no_urut'] = $no_urut;
                $var['kd_jns_op'] = $kd_jns_op;
                $var['thn_pajak_sppt'] = $thn_pajak_sppt;
                $res = $var;

                $ins = $var;
                $ins['tgl_surat'] = Carbon::parse('2024-12-31');
                $ins['no_surat'] = $nomor;
                $ins['jns_koreksi'] = $jns_koreksi;
                $ins['nilai_koreksi'] = onlyNumber($nilai_koreksi);
                $ins['keterangan'] = $keterangan;
                $ins['no_transaksi'] = $trx;
                $ins['created_at'] = db::raw("sysdate");
                $ins['created_by'] = 1678;

                $ins['verifikasi_kode'] = 1;
                $ins['verifikasi_keterangan'] = $keterangan;
                $ins['verifikasi_by'] = 1678;
                $ins['verifikasi_at'] = db::raw("sysdate");


                if ($cek) {
                    // update
                    DB::connection("oracle_satutujuh")->table('sppt_koreksi')
                        ->where([
                            'kd_propinsi' => $kd_propinsi,
                            'kd_dati2' => $kd_dati2,
                            'kd_kecamatan' => $kd_kecamatan,
                            'kd_kelurahan' => $kd_kelurahan,
                            'kd_blok' => $kd_blok,
                            'no_urut' => $no_urut,
                            'kd_jns_op' => $kd_jns_op,
                            'thn_pajak_sppt' => $thn_pajak_sppt
                        ])
                        ->update($ins);
                } else {
                    // insert
                    DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);
                }

                DB::connection("oracle_satutujuh")->table('sppt')->where($var)->update(['status_pembayaran_sppt' => '3']);

                Log::info(json_encode($row));
                // $this->info($i . ' = >' . json_encode($row));

                // Tampilkan progress bar
                $percentage = intval(round(($current / $total) * 100, 2));
                echo "\r[" . str_repeat('#', $percentage / 2) . str_repeat(' ', 50 - ($percentage / 2)) . "] $percentage%";
                usleep(100000); // Delay untuk efek progress
            }
        }

        $this->info("File processed successfully.");
        return 0;
    }
}
