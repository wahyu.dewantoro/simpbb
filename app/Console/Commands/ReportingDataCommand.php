<?php

namespace App\Console\Commands;

use App\Exports\reportTarikdataSpptexcel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
// use Maatwebsite\Excel\Facades\Excel;

class ReportingDataCommand extends Command
{
       /**
        * The name and signature of the console command.
        *
        * @var string
        */
       protected $signature = 'report:sppt {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=}';

       /**
        * The console command description.
        *
        * @var string
        */
       protected $description = 'Narik data sppt dengan perbandingan tahun sebelum nya';

       /**
        * Create a new command instance.
        *
        * @return void
        */
       public function __construct()
       {
              parent::__construct();
       }

       /**
        * Execute the console command.
        *
        * @return int
        */
       public function handle()
       {

              $tahun = $this->option('tahun') ?? '';
              $kd_kecamatan = $this->option('kd_kecamatan') ?? '';
              $kd_kelurahan = $this->option('kd_kelurahan') ?? '';

              $this->info($tahun);
              $this->info($kd_kecamatan);
              $this->info($kd_kelurahan);

              if (
                     $tahun <> '' &&
                     $kd_kecamatan <> '' &&
                     $kd_kelurahan <> ''
              ) {
                     $this->info('sedang memproses data ' . $kd_kecamatan . '-' . $kd_kelurahan);
                     $tahun_last = $tahun - 1;
                     $sql = "
            SELECT    kd_propinsi
                   || '.'
                   || kd_dati2
                   || '.'
                   || kd_kecamatan
                   || '.'
                   || kd_kelurahan
                   || '.'
                   || kd_blok
                   || '.'
                   || no_urut
                   || '.'
                   || kd_jns_op
                      nop,
                   pbb_now,
                   insentif_now,
                   pbb_now - insentif_now pbb_now_akhir,
                   pbb_last,
                   insentif_last,
                   pbb_akhir_last
              FROM (SELECT kd_propinsi,
                           kd_dati2,
                           kd_kecamatan,
                           kd_kelurahan,
                           kd_blok,
                           no_urut,
                           kd_jns_op,
                           pbb_now,
                           NVL (pbb_last, pbb_last_asal) pbb_last,
                           NVL (insentif_last, insentif_last_asal) insentif_last,
                             NVL (pbb_last, pbb_last_asal)
                           - NVL (insentif_last, insentif_last_asal)
                              pbb_akhir_last,
                             pbb_now
                           - (  NVL (pbb_last, pbb_last_asal)
                              - NVL (insentif_last, insentif_last_asal))
                              insentif_now
                      FROM (SELECT aaaaa.*,
                                   ROUND ( (NVL (persentase, 0) / 100) * pbb_last_asal)
                                      insentif_last_asal
                              FROM (SELECT aaaa.*,
                                           (CASE
                                               WHEN ROUND (
                                                       (tarif_last / 100) * njkp_last) <
                                                       nilai_pbb_minimal
                                               THEN
                                                  nilai_pbb_minimal
                                               ELSE
                                                  ROUND ( (tarif_last / 100) * njkp_last)
                                            END)
                                              pbb_last_asal,
                                           get_buku (
                                              CASE
                                                 WHEN ROUND (
                                                         (tarif_last / 100) * njkp_last) <
                                                         nilai_pbb_minimal
                                                 THEN
                                                    ROUND (
                                                       (tarif_last / 100) * njkp_last)
                                                 ELSE
                                                    nilai_pbb_minimal
                                              END)
                                              buku
                                      FROM (SELECT aaa.*,
                                                     (aaa.njop_last - njoptkp)
                                                   * (nilai_hkpd_last / 100)
                                                      njkp_last,
                                                   get_tarifpbb (njop_last,
                                                                 njoptkp,
                                                                 (aaa.tahun - 1))
                                                      tarif_last
                                              FROM (SELECT aa.*,
                                                           njop_tanah * lt_now
                                                              njop_bumi_last,
                                                           njop_bng_now njop_bng_last,
                                                             (njop_tanah * lt_now)
                                                           + njop_bng_now
                                                              njop_last,
                                                           nilai_hkpd nilai_hkpd_last,
                                                           NVL (nilai_njoptkp, 0) * 1000
                                                              njoptkp
                                                      FROM (SELECT a.kd_propinsi,
                                                                   a.kd_dati2,
                                                                   a.kd_kecamatan,
                                                                   a.kd_kelurahan,
                                                                   a.kd_blok,
                                                                   a.no_urut,
                                                                   a.kd_jns_op,
                                                                   c.subjek_pajak_id,
                                                                   d.nop_asal,
                                                                   a.thn_pajak_sppt tahun,
                                                                   a.luas_bumi_sppt
                                                                      lt_now,
                                                                   a.luas_bng_sppt lb_now,
                                                                   a.njop_bng_sppt
                                                                      njop_bng_now,
                                                                   a.pbb_yg_harus_dibayar_sppt
                                                                      pbb_now,
                                                                   b.pbb_yg_harus_dibayar_sppt
                                                                      pbb_last,
                                                                   e.nilai_potongan
                                                                      insentif_last
                                                              FROM sppt a
                                                                   LEFT JOIN sppt b
                                                                      ON     a.kd_propinsi =
                                                                                b.kd_propinsi
                                                                         AND a.kd_dati2 =
                                                                                b.kd_dati2
                                                                         AND a.kd_kecamatan =
                                                                                b.kd_kecamatan
                                                                         AND a.kd_kelurahan =
                                                                                b.kd_kelurahan
                                                                         AND a.kd_blok =
                                                                                b.kd_blok
                                                                         AND a.no_urut =
                                                                                b.no_urut
                                                                         AND a.kd_jns_op =
                                                                                b.kd_jns_op
                                                                         AND b.thn_pajak_sppt =
                                                                                (  a.thn_pajak_sppt
                                                                                 - 1)
                                                                   LEFT JOIN
                                                                   dat_objek_pajak c
                                                                      ON     a.kd_propinsi =
                                                                                c.kd_propinsi
                                                                         AND a.kd_dati2 =
                                                                                c.kd_dati2
                                                                         AND a.kd_kecamatan =
                                                                                c.kd_kecamatan
                                                                         AND a.kd_kelurahan =
                                                                                c.kd_kelurahan
                                                                         AND a.kd_blok =
                                                                                c.kd_blok
                                                                         AND a.no_urut =
                                                                                c.no_urut
                                                                         AND a.kd_jns_op =
                                                                                c.kd_jns_op
                                                                   LEFT JOIN tbl_spop d
                                                                      ON d.no_formulir =
                                                                            c.no_formulir_spop
                                                                   LEFT JOIN
                                                                   sppt_potongan_detail e
                                                                      ON     e.jns_potongan =
                                                                                '1'
                                                                         AND e.thn_pajak_sppt =
                                                                                (  a.thn_pajak_sppt
                                                                                 - 1)
                                                                         AND a.kd_propinsi =
                                                                                e.kd_propinsi
                                                                         AND a.kd_dati2 =
                                                                                e.kd_dati2
                                                                         AND a.kd_kecamatan =
                                                                                e.kd_kecamatan
                                                                         AND a.kd_kelurahan =
                                                                                e.kd_kelurahan
                                                                         AND a.kd_blok =
                                                                                e.kd_blok
                                                                         AND a.no_urut =
                                                                                e.no_urut
                                                                         AND a.kd_jns_op =
                                                                                e.kd_jns_op
                                                             WHERE     a.thn_pajak_sppt =
                                                                          '" . $tahun . "'
                                                                   AND a.kd_kecamatan =
                                                                          '" . $kd_kecamatan . "'
                                                                   AND a.kd_kelurahan =
                                                                          '" . $kd_kelurahan . "') aa
                                                           LEFT JOIN
                                                           (SELECT    a.kd_propinsi
                                                                   || a.kd_dati2
                                                                   || a.kd_kecamatan
                                                                   || a.kd_kelurahan
                                                                   || a.kd_blok
                                                                   || a.no_urut
                                                                   || a.kd_jns_op
                                                                      nop_asal,
                                                                   d.nilai_hkpd,
                                                                   (  e.nilai_per_m2_tanah
                                                                    * 1000)
                                                                      njop_tanah
                                                              FROM dat_objek_pajak a
                                                                   JOIN tbl_spop b
                                                                      ON a.no_formulir_spop =
                                                                            b.no_formulir
                                                                   JOIN dat_op_bumi c
                                                                      ON     a.kd_propinsi =
                                                                                c.kd_propinsi
                                                                         AND a.kd_dati2 =
                                                                                c.kd_dati2
                                                                         AND a.kd_kecamatan =
                                                                                c.kd_kecamatan
                                                                         AND a.kd_kelurahan =
                                                                                c.kd_kelurahan
                                                                         AND a.kd_blok =
                                                                                c.kd_blok
                                                                         AND a.no_urut =
                                                                                c.no_urut
                                                                         AND a.kd_jns_op =
                                                                                c.kd_jns_op
                                                                   JOIN dat_nir d
                                                                      ON     d.thn_nir_znt =
                                                                                '" . $tahun_last . "'
                                                                         AND d.kd_kecamatan =
                                                                                c.kd_kecamatan
                                                                         AND d.kd_kelurahan =
                                                                                c.kd_kelurahan
                                                                         AND c.kd_znt =
                                                                                d.kd_znt
                                                                   JOIN kelas_tanah e
                                                                      ON     d.nir BETWEEN e.nilai_min_tanah
                                                                                       AND e.nilai_max_tanah
                                                                         AND e.kd_kls_tanah NOT LIKE
                                                                                '%X%'
                                                                         AND '" . $tahun_last . "' BETWEEN e.thn_awal_kls_tanah
                                                                                        AND e.thn_akhir_kls_tanah
                                                             WHERE     nop_asal
                                                                          IS NOT NULL
                                                                   AND a.kd_kecamatan =
                                                                          '" . $kd_kecamatan . "'
                                                                   AND a.kd_kelurahan =
                                                                          '" . $kd_kelurahan . "') bb
                                                              ON bb.nop_asal =
                                                                    aa.nop_asal
                                                           LEFT JOIN
                                                           dat_subjek_pajak_njoptkp cc
                                                              ON     cc.subjek_pajak_id =
                                                                        aa.subjek_pajak_id
                                                                 AND thn_njoptkp = '" . $tahun_last . "'
                                                           LEFT JOIN njoptkp dd
                                                              ON cc.thn_njoptkp BETWEEN dd.thn_awal
                                                                                    AND dd.thn_akhir)
                                                   aaa) aaaa
                                           JOIN pbb_minimal bbbb
                                              ON bbbb.thn_pbb_minimal = (aaaa.tahun - 1))
                                   aaaaa
                                   LEFT JOIN ms_potongan_kebijakan cd
                                      ON     CD.TAHUN = (aaaaa.tahun - 1)
                                         AND jenis = '1'
                                         AND cd.kd_buku = aaaaa.buku) tmp) final
          ORDER BY kd_Propinsi,
                   kd_dati2,
                   kd_kecamatan,
                   kd_kelurahan,
                   kd_blok,
                   no_urut";
                     Log::info($sql);

                     
                     /* 
            $datas = DB::connection("oracle_satutujuh")->select(db::raw($sql));


            $data = [];
            foreach ($datas as $row) {
                $data[] = [
                    'nop' => $row->nop,
                    'pbb_now' => $row->pbb_now,
                    'insentif_now' => $row->insentif_now,
                    'pbb_now_akhir' => $row->pbb_now_akhir,
                    'pbb_last' => $row->pbb_last,
                    'insentif_last' => $row->insentif_last,
                    'pbb_akhir_last ' => $row->pbb_akhir_last,
                ];
            }

            // return Excel::download($export, 'invoices.xlsx');
            $arr = [
                'data' => $data,
                'tahun' => $tahun,
                'tahun_last' => $tahun_last
            ];
            Excel::store(new reportTarikdataSpptexcel($arr), 'data ' . $kd_kecamatan . '-' . $kd_kelurahan . '.xlsx', 'local'); */
                     $this->info("selesai");
              } else {
                     $this->error("harus menyertakan tahun, kecamatan, dan desa");
              }
       }
}
