<?php

namespace App\Console\Commands;

use App\Helpers\PenetapanSppt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class hitungnopcommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hitung-sppt-lama';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $data = DB::connection("oracle_satutujuh")->table('tmp_mtd_gabung')->get();

        foreach ($data as $row) {
            $kd_propinsi = '35';
            $kd_dati2 = '07';
            $kd_kecamatan = $row->kd_kecamatan;
            $kd_kelurahan = $row->kd_kelurahan;
            $kd_blok = $row->kd_blok;
            $no_urut = $row->no_urut;
            $kd_jns_op = $row->kd_jns_op;
            $tahun = '2023';
            $tgl_terbit = '20230-01-01';
            $tgl_jatuh_tempo = '2023-12-30';

            PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $tahun, $tgl_terbit, $tgl_jatuh_tempo);

            $this->info(json_encode($row));
        }
        $this->info('Selesai');
    }
}
