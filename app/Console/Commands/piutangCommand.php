<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class piutangCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:piutang';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Membuat laporan piutang objek';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        DB::beginTransaction();
        try {
            //code...
            DB::table('rpt_piutang')->delete();
            $this->info('LAPORAN PIUTANG  : Data di bersihkan');

            DB::statement(DB::raw("insert into rpt_piutang 
            select * 
            from (
            select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,sum(a) a,sum(b) b,sum(c) c,sum(d) d ,sum(e) e
            from (
            select kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,
            case when thn_pajak_sppt=to_char(sysdate,'yyyy') and status_pembayaran_sppt=0 then pbb_yg_harus_dibayar_sppt else null end a,
            case when thn_pajak_sppt=to_char(sysdate,'yyyy')-1 and status_pembayaran_sppt=0 then pbb_yg_harus_dibayar_sppt else null end b,
            case when thn_pajak_sppt=to_char(sysdate,'yyyy')-2 and status_pembayaran_sppt=0 then pbb_yg_harus_dibayar_sppt else null end c,
            case when thn_pajak_sppt=to_char(sysdate,'yyyy')-3 and status_pembayaran_sppt=0 then pbb_yg_harus_dibayar_sppt else null end d,
            case when thn_pajak_sppt=to_char(sysdate,'yyyy')-4 and status_pembayaran_sppt=0 then pbb_yg_harus_dibayar_sppt else null end e
            from spo.sppt
            where thn_pajak_sppt <=to_char(sysdate,'yyyy') and thn_pajak_sppt >=to_char(sysdate,'yyyy')-4 and status_pembayaran_sppt=0 
             ) 
            group by kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op
            ) 
            where a is not null or  b is not null or c is not null or d is not null or e is not null"));
            DB::commit();
            $this->info('Data piutang telah di perbarui');
            Log::info("LAPORAN PIUTANG : Telah di perbarui");
        } catch (Exception $e) {
            //throw $th;
            DB::rollback();
            Log::error("LAPORAN PIUTANG  : " . $e->getMessage());
            $this->error($e->getMessage());
        }
    }
}
