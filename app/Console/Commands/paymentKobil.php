<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;


class paymentKobil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:kobil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arr = DB::select(DB::raw("select b.kobil,b.tanggal, (select sum(total) from billing_kolektif where data_billing_id=a.data_billing_id) total,tahun
        from data_billing a
        join tmp_unflag_flag   b on a.kobil=b.kobil
        where deleted_at is null and kd_status='0'"));
        foreach ($arr as $row) {
            $response = Http::withBody(
                '{
          "Nop": "' . $row->kobil . '",
          "Merchant": "6010",
          "DateTime": "' . $row->tanggal . '",
          "Reference": "907402",
          "TotalBayar": ' . $row->total . ',
          "KodeInstitusi": "001011",
          "NoHp": "08123456789",
          "Email": "a@a.com",
          "Tagihan": [
            {
              "Tahun": "'.$row->tahun.'"
            }
          ]
        }',
                'json'
            )
                ->withHeaders([
                    'Accept' => '*/*',
                    'User-Agent' => 'Thunder Client (https://www.thunderclient.com)',
                    'Authorization' => 'Basic YmFua2phdGltOjA3MTkwNGRhY2RlMzk2YjExMDliYjkyMTgwOTQ3NTM2',
                    'Content-Type' => 'application/json',
                ])
                ->post('192.168.1.205/service/marketplace/api/payment');

            $this->info($row->kobil.' =>'.$response->body());
        }
        $this->info('Seleesai....');
    }
}
