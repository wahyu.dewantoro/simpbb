<?php

namespace App\Console\Commands;

use App\Helpers\Pajak;
use App\Jobs\pendataanObjek as JobsPendataanObjek;
use App\Models\PendataanObjek;
// use App\Models\PendataanObjek
use App\Models\PendataanObjekBng;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class pendataaanObjekCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pendataan:kancrit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $objek = PendataanObjek::leftjoin('tbl_spop','pendataan_objek.id','=','tbl_spop.pendataan_objek_id')
        ->whereraw('tbl_spop.nop_proses is null')
        ->selectraw("pendataan_objek.*")
        ->orderby('id', 'asc')->get();
        
        // where('pendataan_batch_id', $request->id)->orderby('id', 'asc')->get();
        $i = 0;
        foreach ($objek as $row) {
            // Log::info(['data'=>$row]);
            $kd_propinsi = $row->kd_propinsi;
            $kd_dati2 = $row->kd_dati2;
            $kd_kecamatan = $row->kd_kecamatan;
            $kd_kelurahan = $row->kd_kelurahan;
            $kd_blok = $row->kd_blok;
            $no_urut = trim($row->no_urut);
            $kd_jns_op = $row->kd_jns_op;
            $nop_asal = onlynumber($row->nop_asal);
            // $this->info('NOP asal:'.$nop_asal);
            if ($no_urut == '') {
                $nop_proses = Pajak::generateNOP($kd_kecamatan, $kd_kelurahan, $kd_blok);
                $jtr = 1;

                $i++;
            } else {
                $nop_proses = $nop_asal;
                $jtr = 2;
            }
            $data = [
                'pendataan_objek_id' => $row->id,
                'nop_proses' => $nop_proses,
                'nop_asal' => $nop_asal,
                'jns_transaksi' => $jtr,
                'jalan_op' => $row->jalan_op,
                'blok_kav_no_op' => $row->blok_kav_no_op,
                'rt_op' => $row->rt_op,
                'rw_op' => $row->rw_op,
                'no_persil' => $row->no_persil,
                'luas_bumi' => $row->luas_bumi,
                'kd_znt' => $row->kd_znt,
                'jns_bumi' => $row->jns_bumi,
                'jml_bng' => $row->jml_bng,
                'subjek_pajak_id' => $row->subjek_pajak_id,
                'nm_wp' => $row->nm_wp,
                'jalan_wp' => $row->jalan_wp,
                'blok_kav_no_wp' => $row->blok_kav_no_wp,
                'rt_wp' => $row->rt_wp,
                'rw_wp' => $row->rw_wp,
                'kelurahan_wp' => $row->kelurahan_wp,
                'kecamatan_wp' => $row->kecamatan_wp,
                'kota_wp' => $row->kota_wp,
                'propinsi_wp' => $row->propinsi_wp,
                'kd_pos_wp' => $row->kd_pos_wp,
                'status_pekerjaan_wp' => $row->status_pekerjaan_wp,
                'kd_status_wp' => $row->kd_status_wp,
                'npwp' => $row->npwp,
                'telp_wp' => $row->telp_wp,
            ];

            if ($row->jns_bumi == 1) {
                $bng = PendataanObjekBng::where('pendataan_objek_id', $row->id)->get();
                $kd_jpb = [];
                $thn_dibangun_bng = [];
                $thn_renovasi_bng = [];
                $luas_bng = [];
                $jml_lantai_bng = [];
                $kondisi_bng = [];
                $jns_konstruksi_bng = [];
                $jns_atap_bng = [];
                $kd_dinding = [];
                $kd_lantai = [];
                $kd_langit_langit = [];
                $daya_listrik = [];
                $created_at = [];
                foreach ($bng as $bng) {
                    $kd_jpb[] = $bng->kd_jpb;
                    $thn_dibangun_bng[] = $bng->thn_dibangun_bng;
                    $thn_renovasi_bng[] = $bng->thn_renovasi_bng;
                    $luas_bng[] = $bng->luas_bng;
                    $jml_lantai_bng[] = $bng->jml_lantai_bng;
                    $kondisi_bng[] = $bng->kondisi_bng;
                    $jns_konstruksi_bng[] = $bng->jns_konstruksi_bng;
                    $jns_atap_bng[] = $bng->jns_atap_bng;
                    $kd_dinding[] = $bng->kd_dinding;
                    $kd_lantai[] = $bng->kd_lantai;
                    $kd_langit_langit[] = $bng->kd_langit_langit;
                    $daya_listrik[] = $bng->daya_listrik;
                    $created_at[] = $bng->created_at;
                }

                $data['kd_jpb'] = $kd_jpb;
                $data['thn_dibangun_bng'] = $thn_dibangun_bng;
                $data['thn_renovasi_bng'] = $thn_renovasi_bng;
                $data['luas_bng'] = $luas_bng;
                $data['jml_lantai_bng'] = $jml_lantai_bng;
                $data['kondisi_bng'] = $kondisi_bng;
                $data['jns_konstruksi_bng'] = $jns_konstruksi_bng;
                $data['jns_atap_bng'] = $jns_atap_bng;
                $data['kd_dinding'] = $kd_dinding;
                $data['kd_lantai'] = $kd_lantai;
                $data['kd_langit_langit'] = $kd_langit_langit;
                $data['daya_listrik'] = $daya_listrik;
            }
            $data['created_by'] = $row->created_by;
            $data['created_at'] = DB::raw('sysdate');
            JobsPendataanObjek::dispatch($data)->onQueue('pendataan');
            
            $this->info('NOP proses:'.$data['nop_proses']);
        }
    }
}
