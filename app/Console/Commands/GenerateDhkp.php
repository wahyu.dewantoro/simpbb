<?php

namespace App\Console\Commands;

// use App\Exports\DhkpExport;
// use App\Exports\DhkpKabupaten;
use App\fileReport;
use App\Jobs\DhkpFileGenerator;
use App\Kecamatan;
use App\Kelurahan;
use App\Sppt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
// use Maatwebsite\Excel\Facades\Excel;

class GenerateDhkp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dhkp:kecamatan {kd_kecamatan?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'genereate dokumen DHKP kecamatan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            //code...
            // Log::info('running generate DHKP file');


            // delete old file
            $old = fileReport::whereraw("trunc(sysdate) - trunc(created_at) >0 and keterangan like 'DHKP%'");
            if ($old->get()) {
                foreach ($old->get() as $file) {
                    Storage::disk($file->disk)->delete($file->filename);
                }
                $old->delete();
            }


            $kd_kecamatan = $this->argument('kd_kecamatan') ?? '';

            // Log::info('untuk kecamatan ' . $kd_kecamatan);

            // cek sppt
            $cs = Sppt::whereraw("thn_pajak_sppt ='" . date('Y') . "'");
            if ($kd_kecamatan <> '') {
                $cs = $cs->where('kd_kecamatan', $kd_kecamatan);
            }
            $cs = $cs->get()->count();
            // Log::info('Log jumlah sppt ' . $cs);

            // Log::info('Jumlah sppt : '.$cs);
            if ($cs > 0) {
                $kecamatan = Kecamatan::query();
                if ($kd_kecamatan <> '') {
                    $kecamatan = $kecamatan->where('kd_kecamatan', $kd_kecamatan);
                }
                
                $kecamatan = $kecamatan->get();
                foreach ($kecamatan as $kec) {
                    $kelurahahan = Kelurahan::where('kd_kecamatan', $kec->kd_kecamatan)->get();
                    foreach ($kelurahahan as $kel) {
                        $param['kd_kecamatan'] = $kel->kd_kecamatan;
                        $param['kd_kelurahan'] = $kel->kd_kelurahan;
                        $param['nm_kelurahan'] = $kel->nm_kelurahan;
                        $param['nm_kecamatan'] = $kec->nm_kecamatan;
                        $param['tahun'] = date('Y');

                        $queue = str_replace(' ', '_', $kec->nm_kecamatan);
                        dispatch(new DhkpFileGenerator($param));

                        log::info("generate DHKP desa " . $kel->nm_kelurahan . " kec " . $kec->nm_kecamatan . " telah di antrikan untuk di proses");
                    }
                }
            } else {
                Log::info("DHKP : Belum ada penetapan di tahun pajak " . date('Y'));
            }
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
        }
    }
}
