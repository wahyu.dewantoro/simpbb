<?php

namespace App\Console\Commands;

use App\Models\Data_billing;
// use App\PembayaranSppt;
// use App\RekonBatch;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

// use Illuminate\Support\Facades\Log;
// use PhpParser\Node\Stmt\TryCatch;

class RekonBayarTiga extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rekon:pembayaran  {--tp=}   {--tanggal=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'penyamaan pembayaran';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tp = $this->option('tp') ?? '';
        $tanggal = $this->option('tanggal') ?? '';


        if ($tp <> '' && $tanggal <> '') {
            // ambil data rekon batch
            $this->info('identifikasi rekon. . . . .');

            $rekon = DB::table('rekon_batch')
                ->selectraw("max(id) id")
                ->whereraw("kode_bank='$tp' and tanggal_pembayaran=to_date('" . $tanggal . "','yyyy-mm-dd')")
                ->first();

            if ($rekon) {
                $this->info('Rekon sedang berlangsung .......');

                // cek kobil dobel
                $cd = DB::connection("oracle_satutujuh")->select(DB::raw("select aa.* 
                from (
                select a.data_billing_id,a.kobil,a.tahun_pajak,sum(total)  jumlah 
                from sim_pbb.data_billing a
                join sim_pbb.billing_kolektif b on a.data_billing_id=b.data_billing_id
                group by a.data_billing_id,a.kobil,a.tahun_pajak
                ) aa
                join (
                SELECT    kd_propinsi
                               || kd_dati2
                               || kd_kecamatan
                               || kd_kelurahan
                               || kd_blok
                               || no_urut
                               || kd_jns_op
                                  kobil,
                               thn_pajak_sppt tahun_pajak,jumlah
                          FROM sim_pbb.rekon_nop
                         WHERE rekon_batch_id = '" . $rekon->id . "' AND kd_blok = '999'
                ) bb on bb.kobil=aa.kobil and aa.tahun_pajak=bb.tahun_pajak
                and aa.jumlah=bb.jumlah"));


                foreach ($cd as $cd) {
                    $idb = $cd->data_billing_id;
                    DB::table("data_billing")->whereraw("data_billing_id='$idb'")->update(['deleted_at' => null]);
                }


                $dbel = DB::select(db::raw("SELECT KOBIL,TAHUN_PAJAK,COUNT(1),MAX(DATA_BILLING_ID) DATA_BILLING_ID 
                FROM DATA_BILLING
                WHERE TGL_BAYAR=TO_DATE('" . $tanggal . "','yyyy-mm-dd')
                GROUP BY KOBIL,TAHUN_PAJAK
                HAVING COUNT(1) >1"));
                foreach ($dbel as $rr) {
                    $ib = $rr->data_billing_id;
                    $kbl = $rr->kobil;
                    $thn = $rr->tahun_pajak;

                    DB::statement(db::raw("begin 
                    update data_billing set deleted_at=sysdate where kobil='" . $kbl . "' and tahun_pajak='" . $thn . "' and data_billing_id!='" . $ib . "';
                    commit;
                    end;"));
                }

                // get data unflag
                $unflag = DB::select(db::raw("select distinct case when bb.pengesahan is null then  aa.kd_propinsi else bb.kd_propinsi end kd_propinsi,
                                                case when bb.pengesahan is null then  aa.kd_dati2 else bb.kd_dati2 end kd_dati2,
                                                case when bb.pengesahan is null then  aa.kd_kecamatan else bb.kd_kecamatan end kd_kecamatan,
                                                case when bb.pengesahan is null then  aa.kd_kelurahan else bb.kd_kelurahan end kd_kelurahan,
                                                case when bb.pengesahan is null then  aa.kd_blok else bb.kd_blok end kd_blok,
                                                case when bb.pengesahan is null then  aa.no_urut else bb.no_urut end no_urut,
                                                case when bb.pengesahan is null then  aa.kd_jns_op else bb.kd_jns_op end kd_jns_op,
                                                case when bb.pengesahan is null then  aa.thn_pajak_sppt else bb.tahun_pajak end thn_pajak_sppt,
                                                case when bb.pengesahan is not null then '1' else null end is_kobil,
                                                aa.kode_bank,aa.pengesahan
                                   from pbb.pembayaran_sppt aa
                                   left join SIM_PBB.DATA_BILLING bb on aa.pengesahan=bb.pengesahan 
                                   where trunc(tgl_pembayaran_sppt)=to_date('" . $tanggal . "','yyyy-mm-dd')
                                   and aa.kode_bank='" . $tp . "'"));

                foreach ($unflag as $ru) {
                    $kobil = $ru->kd_propinsi . $ru->kd_dati2 . $ru->kd_kecamatan . $ru->kd_kelurahan . $ru->kd_blok . $ru->no_urut . $ru->kd_jns_op;
                    $this->info('Proses unflag nop ' . formatnop($kobil) . ' tahun pajak ' . $ru->thn_pajak_sppt . '  ......');
                    DB::statement(db::raw("begin  spo.proc_hapus_bayar('" . $ru->kd_propinsi . "', '" . $ru->kd_dati2 . "', '" . $ru->kd_kecamatan . "', '" . $ru->kd_kelurahan . "', '" . $ru->kd_blok . "', '" . $ru->no_urut . "', '" . $ru->kd_jns_op . "', '" . $ru->thn_pajak_sppt . "',to_date('" . $tanggal . "','yyyy-mm-dd'),'" . $tp . "','" . $ru->pengesahan . "'); commit; end;"));
                }

                // sapu bersih pembayaram
                DB::connection("oracle_satutujuh")->table("pembayaran_sppt")->whereraw("kode_bank='$tp' and trunc(tgl_pembayaran_sppt)=to_date('" . $tanggal . "','yyyy-mm-dd')")->delete();
                DB::connection("oracle_satutujuh")->commit();
                // mulai looping data pembayaran untuk di flag

                $nop = DB::select(db::raw("select kode_bank,a.tanggal_pembayaran,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op,thn_pajak_sppt,pokok,denda,jumlah
                from rekon_batch a
                join rekon_nop b on a.id=b.rekon_batch_id
                join (select max(id) id 
                          from rekon_batch
                          where kode_bank='$tp' and tanggal_pembayaran=to_date('" . $tanggal . "','yyyy-mm-dd') ) rc on rc.id=a.id"));
                $asd = 0;
                foreach ($nop as $row) {
                    // proses rekon 
                    $pengesahan = date('YmdHis') + $asd;
                    $kobil = $row->kd_propinsi . $row->kd_dati2 . $row->kd_kecamatan . $row->kd_kelurahan . $row->kd_blok . $row->no_urut . $row->kd_jns_op;

                    $this->info('Sedang memproses flag nop ' . formatnop($kobil) . ' tahun pajak ' . $row->thn_pajak_sppt . '  ......');
                    //code...
                    if ($row->kd_blok == '999' || $row->kd_jns_op == '-' || $row->kd_propinsi == '15') {
                        $kobil = onlyNumber($kobil);
                        $cb = Data_billing::whereraw("trim(kobil)='" . $kobil . "' and tahun_pajak='" . $row->thn_pajak_sppt . "'");
                        $data_billing_id = $cb->first()->data_billing_id ?? '';
                        // log::info("trim(kobil)='" . $kobil . "' and tahun_pajak='" . $row->thn_pajak_sppt . "'");
                        if ($cb->count() == 0) {
                            $ac = DB::table('v_virtual_account')->whereraw("nomor_va='$kobil'")->selectraw('kobil,data_billing_id')
                                ->first();
                            if ($ac) {
                                $kobil = $ac->kobil;
                                $data_billing_id = $ac->data_billing_id;
                            }
                        }

                        // kobil
                        Data_billing::whereraw("kobil='" . $kobil . "' and tahun_pajak='" . $row->thn_pajak_sppt . "'")
                            ->update(['expired_at' => db::raw("sysdate+1"), 'deleted_at' => null]);

                        db::connection("oracle_spo")->statement(db::raw("begin 
                                spo.proc_pembayaran_kobil ( '" . $kobil . "', '" . $row->thn_pajak_sppt . "', to_date('" . $tanggal . "','yyyy-mm-dd'), '" . $pengesahan . "', '" . $tp . "' );
                                commit;
                                end;"));
                                
                        $ifqris = DB::table('v_qris')->where("data_billing_id", $data_billing_id)->wherenull('pjsp')->count();
                        if ($ifqris > 0) {
                            // update qris
                            DB::table('qris')->where('data_billing_id', $data_billing_id)
                                ->update([
                                    'purposetrx' => 'Pembayaran PBB',
                                    'storelabel' => 'PBB MALANG KEPANJEN',
                                    'amount_pay' =>        $row->jumlah,
                                    'pjsp' => 'Bank Jatim',
                                    'transactiondate' => new carbon($tanggal)
                                ]);
                        }


                        $ifva = Db::table("v_virtual_account")->where("data_billing_id", $data_billing_id)->wherenull('tanggal_bayar')->count();
                        if ($ifva > 0) {
                            DB::table('virtual_account')->where('data_billing_id', $data_billing_id)
                                ->update([
                                    'tanggal_bayar' => new carbon($tanggal),
                                    'amount' =>   $row->jumlah
                                ]);
                        }
                    } else {
                        // nop
                        db::connection("oracle_spo")->statement(db::raw(" 
                                begin 
                                proc_pembayaran ( '" . $row->kd_propinsi . "','" . $row->kd_dati2 . "','" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "','" . $row->kd_blok . "','" . $row->no_urut . "', '" . $row->kd_jns_op . "','" . $row->thn_pajak_sppt . "', to_date('" . $tanggal . "','yyyy-mm-dd'), '" . $row->denda . "', '" . $row->jumlah . "', '" . $pengesahan . "', '" . $tp . "' );
                                proc_pelunasan ('" . $row->kd_propinsi . "','" . $row->kd_dati2 . "','" . $row->kd_kecamatan . "', '" . $row->kd_kelurahan . "','" . $row->kd_blok . "','" . $row->no_urut . "', '" . $row->kd_jns_op . "','" . $row->thn_pajak_sppt . "');
                                commit; 
                                end; 
                                "));
                    }
                    $this->info('Berhasil memproses nop ' . formatnop($kobil) . ' tahun pajak ' . $row->thn_pajak_sppt . '  ......');
                    $asd++;
                }
            } else {
                $this->info("Data rekon tidak di temukan ... ");
            }
        } else {
            $this->error('masukan parameter --tp= dan --tanggal=yyyy-mm-dd');
        }
        $this->info('Selesai.');
    }
}
