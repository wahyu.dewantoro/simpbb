<?php

namespace App\Console\Commands;

use App\Exports\ExcellDatageneral;
use App\Helpers\Pajak;
use App\Jobs\BatchSknjop;
use App\Jobs\disposisiPenelitian;
use App\Jobs\excelLunasLimaTahunJob;
use App\Jobs\general_a_job;
use App\Jobs\generatePiutangTahunJob;
use App\Jobs\NormalisasiKelas;
use App\Jobs\normalisasiTarifSppt;
use App\Jobs\PendataanPenilaianJob;
use App\Jobs\RekonPembayaranJob;
use App\Kecamatan;
use App\Models\Layanan;
use App\Models\Layanan_objek;
use App\Unitkerja;
use DateInterval;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class generalCommand extends Command
{
       /**
        * The name and signature of the console command.
        *
        * @var string
        */
       protected $signature = 'tarikdata';

       /**
        * The console command description.
        *
        * @var string
        */
       protected $description = 'Command description';

       /**d
        * Create a new command instance.
        *
        * @return void
        */
       public function __construct()
       {
              parent::__construct();
       }

       /**
        * Execute the console command.
        *
        * @return int
        */
       public function handle()
       {

              $layanan = DB::select("select a.id
              from (
                SELECT LAYANAN_OBJEK.ID,
                       LAYANAN_OBJEK.KD_PROPINSI,
                       LAYANAN_OBJEK.KD_DATI2,
                       LAYANAN_OBJEK.KD_KECAMATAN,
                       LAYANAN_OBJEK.KD_KELURAHAN,
                       LAYANAN_OBJEK.KD_BLOK,
                       LAYANAN_OBJEK.NO_URUT,
                       LAYANAN_OBJEK.KD_JNS_OP,
                       LAYANAN_OBJEK.NOMOR_LAYANAN,
                          'Kec. '
                       || ref_kecamatan.nm_kecamatan
                       || ' Ds. '
                       || ref_kelurahan.nm_kelurahan
                       || ' '
                       || layanan_objek.alamat_op
                          AS alamat_op,
                       LAYANAN.JENIS_LAYANAN_NAMA,
                       LAYANAN_OBJEK.LUAS_BUMI,
                       LAYANAN_OBJEK.LUAS_BNG,
                       LAYANAN_OBJEK.NAMA_WP,
                       LAYANAN_OBJEK.NOP_GABUNG,
                       LAYANAN.JENIS_LAYANAN_ID,
                       LAYANAN_OBJEK.KETERANGAN_TOLAK,
                       users.nama usernameinput,
                       petugas.nama petugas,
                       peneliti.nama petugaspeneliti
                  FROM LAYANAN_OBJEK
                       LEFT JOIN LAYANAN
                          ON LAYANAN.NOMOR_LAYANAN = LAYANAN_OBJEK.NOMOR_LAYANAN
                       LEFT JOIN USERS ON USERS.ID = LAYANAN.CREATED_BY
                       LEFT JOIN users petugas ON PETUGAS.ID = LAYANAN.UPDATED_BY
                       LEFT JOIN users peneliti
                          ON PENELITI.ID = LAYANAN_OBJEK.PEMUTAKHIRAN_BY
                       INNER JOIN spo.ref_kecamatan ref_kecamatan
                          ON REF_KECAMATAN.KD_KECAMATAN = LAYANAN_OBJEK.KD_KECAMATAN
                       INNER JOIN spo.ref_kelurahan ref_kelurahan
                          ON     REF_KELURAHAN.KD_KECAMATAN =
                                    LAYANAN_OBJEK.KD_KECAMATAN
                             AND REF_KELURAHAN.KD_KELURAHAN =
                                    LAYANAN_OBJEK.KD_KELURAHAN
                 WHERE     layanan.deleted_at IS NULL
                       AND layanan_objek.is_tolak IS NOT NULL
                       AND layanan.jenis_layanan_id = '4'
                       ) a
                       left join surat_keputusan b on a.id=b.layanan_objek_id
                       where nomor is null");
              foreach ($layanan as $row) {
                     $url = url('sk') . '/' . acak($row->id);
                     $this->info($url);
                     Http::get($url);
              }
              /*   $start = 2003;
        $end = 2012;
        for ($i = $start; $i <= $end; $i++) {
            $this->info("Memproses tahun " . $i);
            $sql = "
            SELECT NOP,
            is_aktif,
                   TAHUN_PAJAK,
                   PBB
              FROM (SELECT    kd_propinsi
                           || '.'
                           || kd_dati2
                           || '.'
                           || kd_kecamatan
                           || '.'
                           || kd_kelurahan
                           || '.'
                           || kd_blok
                           || '.'
                           || no_urut
                           || '.'
                           || kd_jns_op
                              NOP,
                              is_aktif,
                           kd_propinsi,
                           kd_dati2,
                           kd_kecamatan,
                           kd_kelurahan,
                           kd_blok,
                           no_urut,
                           kd_jns_op,
                           nm_kecamatan,
                           nm_kelurahan,
                           alamat_op,
                           nm_wp_sppt,
                           alamat_wp,
                           status_pembayaran_sppt,
                           thn_pajak_sppt tahun_pajak,
                           pbb,
                           bayar,
                           pbb - bayar kurang_bayar
                      FROM (  SELECT sppt.kd_propinsi,
                                     sppt.kd_dati2,
                                     sppt.kd_kecamatan,
                                     sppt.kd_kelurahan,
                                     sppt.kd_blok,
                                     sppt.no_urut,
                                     sppt.kd_jns_op,
                                     sppt.thn_pajak_sppt,
                                     nm_kecamatan,
                                     nm_kelurahan,
                                     sppt.nm_wp_sppt,
                                        jln_wp_sppt
                                     || ' '
                                     || blok_kav_no_wp_sppt
                                     || ' RT'
                                     || rt_wp_sppt
                                     || ' RW'
                                     || rw_wp_sppt
                                     || ' '
                                     || kelurahan_wp_sppt
                                     || ' '
                                     || kota_wp_sppt
                                        alamat_wp,
                                        jalan_op
                                     || ' '
                                     || blok_kav_no_op
                                     || ' RT '
                                     || rt_op
                                     || ' RW '
                                     || rw_op
                                        alamat_op,
                                     status_pembayaran_sppt,
                                     pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                                        pbb,
                                     SUM (
                                        NVL (jml_sppt_yg_dibayar, 0) - NVL (denda_sppt, 0))
                                        bayar,
                                           case when jns_bumi in ('1','2','3') then '1' else '0' end is_aktif
                                FROM sppt
                                     JOIN dat_objek_pajak
                                        ON     dat_objek_pajak.kd_propinsi =
                                                  sppt.kd_propinsi
                                           AND dat_objek_pajak.kd_dati2 = sppt.kd_dati2
                                           AND dat_objek_pajak.kd_kecamatan =
                                                  sppt.kd_kecamatan
                                           AND dat_objek_pajak.kd_kelurahan =
                                                  sppt.kd_kelurahan
                                           AND dat_objek_pajak.kd_blok = sppt.kd_blok
                                           AND dat_objek_pajak.no_urut = sppt.no_urut
                                           AND dat_objek_pajak.kd_jns_op = sppt.kd_jns_op
                                           JOIN dat_op_bumi
                                        ON     dat_objek_pajak.kd_propinsi =
                                                  dat_op_bumi.kd_propinsi
                                           AND dat_objek_pajak.kd_dati2 = dat_op_bumi.kd_dati2
                                           AND dat_objek_pajak.kd_kecamatan =
                                                  dat_op_bumi.kd_kecamatan
                                           AND dat_objek_pajak.kd_kelurahan =
                                                  dat_op_bumi.kd_kelurahan
                                           AND dat_objek_pajak.kd_blok = dat_op_bumi.kd_blok
                                           AND dat_objek_pajak.no_urut = dat_op_bumi.no_urut
                                           AND dat_objek_pajak.kd_jns_op = dat_op_bumi.kd_jns_op
                                     JOIN ref_kecamatan
                                        ON ref_kecamatan.kd_kecamatan =
                                              dat_objek_pajak.kd_kecamatan
                                     JOIN ref_kelurahan
                                        ON     ref_kelurahan.kd_kecamatan =
                                                  dat_objek_pajak.kd_kecamatan
                                           AND ref_kelurahan.kd_kelurahan =
                                                  dat_objek_pajak.kd_kelurahan
                                     LEFT JOIN sppt_potongan
                                        ON     sppt.kd_propinsi = sppt_potongan.kd_propinsi
                                           AND sppt.kd_dati2 = sppt_potongan.kd_dati2
                                           AND sppt.kd_kecamatan =
                                                  sppt_potongan.kd_kecamatan
                                           AND sppt.kd_kelurahan =
                                                  sppt_potongan.kd_kelurahan
                                           AND sppt.kd_blok = sppt_potongan.kd_blok
                                           AND sppt.no_urut = sppt_potongan.no_urut
                                           AND sppt.kd_jns_op = sppt_potongan.kd_jns_op
                                           AND sppt.thn_pajak_sppt =
                                                  sppt_potongan.thn_pajak_sppt
                                     LEFT JOIN sppt_koreksi
                                        ON     sppt.kd_propinsi = sppt_koreksi.kd_propinsi
                                           AND sppt.kd_dati2 = sppt_koreksi.kd_dati2
                                           AND sppt.kd_kecamatan =
                                                  sppt_koreksi.kd_kecamatan
                                           AND sppt.kd_kelurahan =
                                                  sppt_koreksi.kd_kelurahan
                                           AND sppt.kd_blok = sppt_koreksi.kd_blok
                                           AND sppt.no_urut = sppt_koreksi.no_urut
                                           AND sppt.kd_jns_op = sppt_koreksi.kd_jns_op
                                           AND sppt.thn_pajak_sppt =
                                                  sppt_koreksi.thn_pajak_sppt
                                     LEFT JOIN pembayaran_sppt
                                        ON     sppt.kd_propinsi =
                                                  pembayaran_sppt.kd_propinsi
                                           AND sppt.kd_dati2 = pembayaran_sppt.kd_dati2
                                           AND sppt.kd_kecamatan =
                                                  pembayaran_sppt.kd_kecamatan
                                           AND sppt.kd_kelurahan =
                                                  pembayaran_sppt.kd_kelurahan
                                           AND sppt.kd_blok = pembayaran_sppt.kd_blok
                                           AND sppt.no_urut = pembayaran_sppt.no_urut
                                           AND sppt.kd_jns_op = pembayaran_sppt.kd_jns_op
                                           AND sppt.thn_pajak_sppt =
                                                  pembayaran_sppt.thn_pajak_sppt
                               WHERE     sppt_koreksi.kd_propinsi IS NULL
                                     AND sppt.thn_pajak_sppt = '$i'
                            GROUP BY case when jns_bumi in ('1','2','3') then '1' else '0' end, 
                            sppt.kd_propinsi,
                                     sppt.kd_dati2,
                                     sppt.kd_kecamatan,
                                     sppt.kd_kelurahan,
                                     sppt.kd_blok,
                                     sppt.no_urut,
                                     sppt.kd_jns_op,
                                     sppt.thn_pajak_sppt,
                                     sppt.nm_wp_sppt,
                                     sppt.jln_wp_sppt,
                                     sppt.blok_kav_no_wp_sppt,
                                     sppt.rt_wp_sppt,
                                     sppt.rw_wp_sppt,
                                     sppt.kelurahan_wp_sppt,
                                     sppt.kota_wp_sppt,
                                     jalan_op,
                                     blok_kav_no_op,
                                     rt_op,
                                     rw_op,
                                     nm_kecamatan,
                                     nm_kelurahan,
                                     status_pembayaran_sppt,
                                     pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0)
                                     order by pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) desc
                                     )
                           tmp
                     WHERE bayar = 0
                     ) tmp";

            $param = [
                'view' => 'general/tarikan_a',
                'sql' => $sql,
                'connection' => 'oracle_satutujuh',
                'filename' => "data tahun " . $i . ".xlsx"
            ];

            // Excel::store(new ExcellDatageneral($param), $filename, 'public');

            general_a_job::dispatch($param)->onQueue("narik");

            $this->info("selesai tahun " . $i);
        }
        $this->info("DONE"); */
       }
}
