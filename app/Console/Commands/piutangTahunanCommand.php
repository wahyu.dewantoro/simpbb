<?php

namespace App\Console\Commands;

use App\Jobs\generatePiutangTahunJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class piutangTahunanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:piutangtahun';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate file piutang per tahun';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        $now=date('Y');
        for ($tahun =$now; $tahun >= 2003; $tahun--) {
            generatePiutangTahunJob::dispatch($tahun);
            // ->onQueue('piutang');
        }
    }
}
