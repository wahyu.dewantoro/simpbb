<?php

namespace App\Console\Commands;

use App\Exports\tanggalBayarKertasKerjaExcell;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TmpExcellTanggal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kertas-kerja-tanggal {--tahun=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // for ($tahun = 2003; $tahun <= 2022; $tahun++) {
            $tahun=$this->option('tahun') ??date('Y');

            $filename = "Tanggal bayar th " . $tahun . ".xlsx";
            $this->info("memproses ".$filename);
            $data = DB::connection("oracle_satutujuh")->select(db::raw("select a.nop,a.tahun,a.pbb,to_char(max(tgl_pembayaran_sppt),'yyyy-mm-dd') tanggal 
            from tmp_kertas_kerja a
            left join pembayaran_sppt b on a.kd_propinsi=b.kd_propinsi and 
            a.kd_dati2=b.kd_dati2 and 
            a.kd_kecamatan=b.kd_kecamatan and 
            a.kd_kelurahan=b.kd_kelurahan and 
            a.kd_blok=b.kd_blok and 
            a.no_urut=b.no_urut and 
            a.kd_jns_op=b.kd_jns_op and a.tahun=b.thn_pajak_sppt
            where a.tahun='$tahun'
            group by a.nop,a.tahun,a.pbb
            order by nop asc"));
            
            Excel::store(new tanggalBayarKertasKerjaExcell($data), $filename, 'reports');
        // }
        $this->info("selesai.");
    }
}
