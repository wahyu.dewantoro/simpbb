<?php

namespace App\Console\Commands;

use App\Helpers\PembatalanObjek;
use App\Imports\NopRekonImport;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel as FacadesExcel;
use Illuminate\Support\Facades\File;

class importKoreksiKapeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:koreksi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        // $array = Excel::toArray(new UsersImport, 'users.xlsx');
        // $this->error("erro");
        // die();


        // $directory =  storage_path('kp');
        // $storage = Storage::allFiles($directory);
        $storage = File::allFiles(storage_path('kp'));

        foreach ($storage as $file) {
            // $this->info($file->getFilename());

            $nm_file = 'kp/' . $file->getFilename();
            $array = FacadesExcel::toArray(new NopRekonImport(), storage_path($nm_file));

            $data = $array[0];
            $jumlah = count($data);
            $i = 1;

            $no_surat = $data[0]['nomor_surat'];
            $tgl = $data[0]['tanggal_surat'];

            if ($no_surat != '' && $tgl != '') {
                $tgl = new Carbon($tgl);
                $jns_koreksi = '3';
                $kd = 'KP';
                $trx = PembatalanObjek::NoTransaksi($kd, '07');
                $user_id = '1700';
                $keterangan = 'Koreksi rekon desa';
                foreach ($data as $row) {
                    if ($row['nop'] != '') {

                        $nop = onlyNumber($row['nop']);
                        $tahun = $row['tahun_pajak'];

                        $this->info($nm_file . " => " . $i . " / " . $jumlah . ' : ' . $nop . " - " . $tahun . " Sedang  di proses");
                        DB::connection("oracle_satutujuh")->beginTransaction();
                        try {
                            //code...

                            $var['kd_propinsi'] = substr($nop, 0, 2);
                            $var['kd_dati2'] = substr($nop, 2, 2);
                            $var['kd_kecamatan'] = substr($nop, 4, 3);
                            $var['kd_kelurahan'] = substr($nop, 7, 3);
                            $var['kd_blok'] = substr($nop, 10, 3);
                            $var['no_urut'] = substr($nop, 13, 4);
                            $var['kd_jns_op'] = substr($nop, 17, 1);
                            $var['thn_pajak_sppt'] = $tahun;
                            $res = $var;

                            $ins = $var;
                            $ins['tgl_surat'] = $tgl;
                            $ins['no_surat'] = $no_surat;
                            $ins['jns_koreksi'] = $jns_koreksi;
                            // $ins['nilai_koreksi'] = onlyNumber($request->nilai_koreksi);
                            $ins['keterangan'] = $keterangan;
                            $ins['no_transaksi'] = $trx;
                            $ins['created_at'] = Carbon::now();
                            $ins['created_by'] = $user_id;

                            $ins['verifikasi_kode'] = 1;
                            $ins['verifikasi_keterangan'] = $keterangan;
                            $ins['verifikasi_by'] = $user_id;
                            $ins['verifikasi_at'] = Carbon::now();

                            // cek koreksi
                            $ck = DB::connection("oracle_satutujuh")->table('pengurang_piutang')
                                ->where($var)

                                ->count();

                            if ($ck == 0) {
                                // cek iv
                                $cka = DB::connection("oracle_satutujuh")->table('ref_iventarisasi')
                                    ->where($var)
                                    ->count();
                                if ($cka == 1) {
                                    $ins['kd_kategori'] = '';
                                    $ins['nm_kategori'] = '';
                                    DB::connection("oracle_satutujuh")->table('sppt_koreksi')->where($var)->update($ins);
                                } else {
                                    DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);
                                }
                            }


                            DB::connection("oracle_satutujuh")->table('sppt')->where($var)->update(['status_pembayaran_sppt' => '3']);
                            DB::connection("oracle_satutujuh")->commit();

                            $this->info($nop . " - " . $tahun . " berhasil di proses");
                        } catch (\Throwable $th) {
                            //throw $th;
                            DB::connection("oracle_satutujuh")->rollBack();
                            $this->error($nop . " - " . $tahun . " gagal di proses " . $th->getMessage());
                        }
                        $i++;
                    }
                }
            }

            /* 
            $data = $array[0];
            $jumlah = count($data);
            $this->info($nm_file . ' => ' . $jumlah); */
        }
        // $this->info(json_encode($storage));
        die();
        // storage\kp\UPLOAD KP  Banjararum SINGOSARI.xlsx


        $nm_file = 'kp/a.xlsx';
        $array = FacadesExcel::toArray(new NopRekonImport(), storage_path($nm_file));


        $data = $array[0];
        $jumlah = count($data);
        $i = 1;

        $no_surat = $data[0]['nomor_surat'];
        $tgl = $data[0]['tanggal_surat'];

        if ($no_surat != '' && $tgl != '') {




            $tgl = new Carbon($tgl);

            $jns_koreksi = '3';
            $kd = 'KP';
            $trx = PembatalanObjek::NoTransaksi($kd, '07');
            $user_id = '1700';
            $keterangan = 'Koreksi rekon desa';


            foreach ($data as $row) {
                if ($row['nop'] != '') {

                    $nop = onlyNumber($row['nop']);
                    $tahun = $row['tahun_pajak'];

                    $this->info($i . " / " . $jumlah . ' : ' . $nop . " - " . $tahun . " berhasil di proses");
                    DB::connection("oracle_satutujuh")->beginTransaction();
                    try {
                        //code...
                        $var['kd_propinsi'] = substr($nop, 0, 2);
                        $var['kd_dati2'] = substr($nop, 2, 2);
                        $var['kd_kecamatan'] = substr($nop, 4, 3);
                        $var['kd_kelurahan'] = substr($nop, 7, 3);
                        $var['kd_blok'] = substr($nop, 10, 3);
                        $var['no_urut'] = substr($nop, 13, 4);
                        $var['kd_jns_op'] = substr($nop, 17, 1);
                        $var['thn_pajak_sppt'] = $tahun;
                        $res = $var;

                        $ins = $var;
                        $ins['tgl_surat'] = $tgl;
                        $ins['no_surat'] = $no_surat;
                        $ins['jns_koreksi'] = $jns_koreksi;
                        // $ins['nilai_koreksi'] = onlyNumber($request->nilai_koreksi);
                        $ins['keterangan'] = $keterangan;
                        $ins['no_transaksi'] = $trx;
                        $ins['created_at'] = Carbon::now();
                        $ins['created_by'] = $user_id;

                        $ins['verifikasi_kode'] = 1;
                        $ins['verifikasi_keterangan'] = $keterangan;
                        $ins['verifikasi_by'] = $user_id;
                        $ins['verifikasi_at'] = Carbon::now();

                        DB::connection("oracle_satutujuh")->table('sppt_koreksi')->insert($ins);
                        DB::connection("oracle_satutujuh")->table('sppt')->where($var)->update(['status_pembayaran_sppt' => '3']);

                        DB::connection("oracle_satutujuh")->commit();

                        $this->info($nop . " - " . $tahun . " berhasil di proses");
                    } catch (\Throwable $th) {
                        //throw $th;
                        DB::connection("oracle_satutujuh")->rollBack();
                        $this->error($nop . " - " . $tahun . " gagal di proses " . $th->getMessage());
                    }
                    $i++;
                }
            }
        }
    }
}
