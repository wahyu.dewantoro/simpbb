<?php

namespace App\Console\Commands;

use App\Helpers\Lhp;
use App\Jobs\GenerateSk;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class generateSkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sk:penelitian';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate SK hasil penelitian';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        // GenerateSk::dispatch(10101)->onQueue('generatesk');

         $data = Lhp::listSk()->whereraw("case when surat_keputusan.id is not null then 1 else 0 end=0 and rownum <= 100")->get();
        if ($data) {
            $this->info('Sedang memproses sk penelitian');
            foreach ($data as $i => $row) {
                $this->info('data ke '. $i);
                GenerateSk::dispatch($row->layanan_objek_id)->onQueue('generatesk');
            }

            $this->info('Proses selesai');
        } else {
            $this->info('Tidak ada data yang di proses');
        }
    }
}
