<?php

namespace App\Console\Commands;

use App\Exports\SpptExport;
use App\Kecamatan;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class spptExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sppt:export {--kd_kecamatan=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $kd_kecamatan = $this->option('kd_kecamatan') ?? '';
        $kec = Kecamatan::query();
        if ($kd_kecamatan <> '') {
            $kec = $kec->where('kd_kecamatan', $kd_kecamatan);
        }

        $kec = $kec->get();

        foreach ($kec as $rk) {
            $this->info('Sedang memproses data  '. $rk->nm_kecamatan);
            $filename = 'DHR   ' . $rk->kd_kecamatan . ' - ' . $rk->nm_kecamatan . ' - '.time().'.xlsx';
            Excel::store(new SpptExport($rk->kd_kecamatan), $filename, 'reports');
            $this->info($filename . ' created');
        }
    }
}
