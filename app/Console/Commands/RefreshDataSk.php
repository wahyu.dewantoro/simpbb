<?php

namespace App\Console\Commands;

use App\Helpers\Lhp;
use App\Models\SuratKeputusan;
use App\Models\SuratKeputusanLampiran;
use Illuminate\Console\Command;

class RefreshDataSk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sk:penelitian-refresh {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perbaikan data SK';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    // protected $id

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //    $id=$this->argument('id');
        $id = $this->option('id') ?? null;

        // $row = [7172];
        // foreach ($row as $id) {
        if ($id <> '') {
            $sk = SuratKeputusan::where('layanan_objek_id', $id)->first();
            if ($sk) {
                SuratKeputusanLampiran::where('surat_keputusan_id', $sk->id)->delete();
                $penelitian = Lhp::getHasilNjop($id);
                $spop = $penelitian['core'];
                $hasil = $penelitian['hasil'];
                $last = $penelitian['last'];
                $alamat_op = "-";
                $alamat_wp = "-";
                if ($hasil) {
                    $alamat_op = $hasil->jalan_op . ' ' . $hasil->blok_kav_no_op . ' RT ' . ($hasil->rt_op ?? '') . ' RW ' . ($hasil->rw_op ?? '');
                    $alamat_wp = $hasil->jalan_wp . ' ' . $hasil->blok_kav_no_wp ?? '' . ' ' . $hasil->rw_wp . ' ' . $hasil->rt_wp . ' <br>' . $hasil->kelurahan_wp . ' ' . $hasil->kota_wp;
                }
                SuratKeputusanLampiran::create(['surat_keputusan_id' => $sk->id, 'is_last' => null, 'kd_propinsi' => $hasil->kd_propinsi, 'kd_dati2' => $hasil->kd_dati2, 'kd_kecamatan' => $hasil->kd_kecamatan, 'kd_kelurahan' => $hasil->kd_kelurahan, 'kd_blok' => $hasil->kd_blok, 'no_urut' => $hasil->no_urut, 'kd_jns_op' => $hasil->kd_jns_op, 'alamat_op' => $alamat_op, 'nm_wp' => $hasil->nm_wp ?? $spop->nm_wp, 'alamat_wp' => $alamat_wp ?? '-', 'luas_bumi' => $hasil->total_luas_bumi ?? 0, 'njop_bumi' => $hasil->njop_bumi ?? 0, 'luas_bng' => $hasil->total_luas_bng ?? 0, 'njop_bng' => $hasil->njop_bng ?? 0,]);
                foreach ($last as $last) {
                    $alamat_wp_last = $last->jalan_wp . ' ' . $last->blok_kav_no_wp ?? '' . ' ' . $last->rw_wp . ' ' . $last->rt_wp . ' <br>' . $last->kelurahan_wp . ' ' . $last->kota_wp;
                    SuratKeputusanLampiran::create(['surat_keputusan_id' => $sk->id, 'is_last' => '1', 'kd_propinsi' => $last->kd_propinsi, 'kd_dati2' => $last->kd_dati2, 'kd_kecamatan' => $last->kd_kecamatan, 'kd_kelurahan' => $last->kd_kelurahan, 'kd_blok' => $last->kd_blok, 'no_urut' => $last->no_urut, 'kd_jns_op' => $last->kd_jns_op, 'alamat_op' => $alamat_op, 'nm_wp' => $last->nm_wp ?? '-', 'alamat_wp' => $alamat_wp_last, 'luas_bumi' => $last->total_luas_bumi ?? 0, 'njop_bumi' => $last->njop_bumi ?? 0, 'luas_bng' => $last->total_luas_bng ?? 0, 'njop_bng' => $last->njop_bng ?? 0,]);
                }
            } else {
                Lhp::getSk($id);
            }
            $this->info($id . ' Telah di proses');
        }else{
            $this->error('Id harus disertakan');
        }
        
    }
}
