<?php

namespace App\Console\Commands;

use App\Jobs\generateFileSppt;
use App\Jobs\generateMultiNopSppt;
use App\Sppt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class spptCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sppt {--jenis=} {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=} { --kd_blok=} { --no_urut=} { --kd_jns_op=}';
    //  {kd_kelurahan?} {kd_blok?} {no_urut?} {kd_jns_op?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate file E- SPPT => jenis : single/multi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    //  35.07.220.001.004.0283.0 

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $jenis = $this->option('jenis') ?? 'multi';
        $tahun = $this->option('tahun') ?? date('Y');
        $kd_kecamatan = $this->option('kd_kecamatan') ?? null;
        $kd_kelurahan = $this->option('kd_kelurahan') ?? null;
        $kd_blok = $this->option('kd_blok') ?? null;
        $no_urut = $this->option('no_urut') ?? null;
        $kd_jns_op = $this->option('kd_jns_op') ?? null;

        Log::info($jenis);
        if ($jenis == 'single') {
            // single file
            $data = Sppt::select(DB::raw("thn_pajak_sppt,kd_propinsi,kd_dati2,kd_kecamatan,kd_kelurahan,kd_blok,no_urut,kd_jns_op"))
                ->whereraw("thn_pajak_sppt='$tahun' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'");
            if ($kd_blok <> '') {
                $data = $data->whereraw("kd_blok='$kd_blok'");
            }
            if ($no_urut <> '') {
                $data = $data->whereraw("no_urut='$no_urut'");
            }
            if ($kd_jns_op <> '') {
                $data = $data->whereraw("kd_jns_op='$kd_jns_op'");
            }
            $data = $data->get();
            foreach ($data as $row) {
                $param['tahun'] = $row->thn_pajak_sppt;
                $param['kd_kecamatan'] = $row->kd_kecamatan;
                $param['kd_kelurahan'] = $row->kd_kelurahan;
                $param['kd_blok'] = $row->kd_blok;
                $param['no_urut'] = $row->no_urut;
                $param['kd_jns_op'] = $row->kd_jns_op;
                dispatch(new generateFileSppt($param))->onQueue('sppt');
                // Log::info($param);
            }
        } else {
            // multi nop in one file
            // log::info("thn_pajak_sppt='$tahun' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'");
            $data = Sppt::select(DB::raw("distinct thn_pajak_sppt,kd_kecamatan,kd_kelurahan,kd_blok"))
                ->whereraw("thn_pajak_sppt='$tahun' and kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'");
            if ($kd_blok <> '') {
                $data = $data->whereraw("kd_blok='$kd_blok'");
            }


            $data = $data->get();
            foreach ($data as $row) {
                $param['tahun'] = $row->thn_pajak_sppt;
                $param['kd_kecamatan'] = $row->kd_kecamatan;
                $param['kd_kelurahan'] = $row->kd_kelurahan;
                $param['kd_blok'] = $row->kd_blok;
                dispatch(new generateMultiNopSppt($param))->onQueue('sppt');
                // Log::info($param);
            }
        }

     
    }
}
