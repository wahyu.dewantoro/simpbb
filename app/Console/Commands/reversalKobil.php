<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class reversalKobil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kobil:reflag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arr = DB::select(DB::raw("select * from tmp_unflag_flag "));

        $no = 1;
        foreach ($arr as $i => $row) {

            $this->info("Proses ke :" . $no);
            $no++;
            $reversal = Http::withBody(
                '{
          "Nop": "' . $row->kobil . '",
          "Reference": "3504836196797971",
          "DateTime": "' . $row->tanggal . '",
          "Tagihan": [
            {
              "Tahun": "' . $row->tahun . '"
            }
          ]
        }',
                'json'
            )
                ->withHeaders([
                    'Accept' => '*/*',
                    'User-Agent' => 'Thunder Client (https://www.thunderclient.com)',
                    'Authorization' => 'Basic YmFua2phdGltOjA3MTkwNGRhY2RlMzk2YjExMDliYjkyMTgwOTQ3NTM2',
                    'Content-Type' => 'application/json',
                ])
                ->post('192.168.1.205/service/marketplace/api/reversal');

            $this->info('Reversal : ' . $reversal->body());

            //  payment
            $patment =  Http::withBody(
                '{
              "Nop": "' . $row->kobil . '",
              "Merchant": "6010",
              "DateTime": "' . $row->tanggal . '",
              "Reference": "907402",
              "TotalBayar": ' . $row->total . ',
              "KodeInstitusi": "001011",
              "NoHp": "08123456789",
              "Email": "a@a.com",
              "Tagihan": [
                {
                  "Tahun": "' . $row->tahun . '"
                }
              ]
            }',
                'json'
            )
                ->withHeaders([
                    'Accept' => '*/*',
                    'User-Agent' => 'Thunder Client (https://www.thunderclient.com)',
                    'Authorization' => 'Basic YmFua2phdGltOjA3MTkwNGRhY2RlMzk2YjExMDliYjkyMTgwOTQ3NTM2',
                    'Content-Type' => 'application/json',
                ])
                ->post('192.168.1.205/service/marketplace/api/payment');

            $this->info('payment :' . $row->kobil . ' =>' . $patment->body());
        }
        $this->info('Selesai .....');
    }
}
