<?php

namespace App\Console\Commands;

use App\Exports\DataExport;
use App\Helpers\Piutang;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class exportDataExcell extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'piutang-excell {--tahun=} {--kd_kecamatan=} {--kd_kelurahan=}';

    /**
     * The console command description.
     *
     * @var string`
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_init', -1);
        $tahun = $this->option('tahun') ?? '';
        $kd_kecamatan = $this->option('kd_kecamatan') ?? '';
        $kd_kelurahan = $this->option('kd_kelurahan') ?? '';

        /*  $tahun = $this->option('tahun') ?? '';
        $kd_kecamatan = $this->option('kd_kecamatan') ?? '';
        $kd_kelurahan = $this->option('kd_kelurahan') ?? '';
        $filename = "piutang th " . $tahun . '.xlsx';
        $this->info("proses mengambil data th " . $tahun);
        $data = Piutang::PerNop($kd_kecamatan, $kd_kelurahan, $tahun)->get();
        try {
            //code...
            Excel::store(new DataExport($data), $filename, 'reports');
        } catch (\Throwable $th) {
            log::info($th);
        }
         */

        // $this->info("selesai");

        if ($kd_kecamatan != '') {
            $wh = "ref_kecamatan.kd_kecamatan='$kd_kecamatan'";
            if ($kd_kelurahan <> '') {
                $wh .= " and kd_kelurahan='$kd_kelurahan'";
            }
            $kawasan = DB::connection("oracle_satutujuh")->table("ref_kecamatan")
                ->join('ref_kelurahan', 'ref_kecamatan.kd_kecamatan', '=', 'ref_kelurahan.kd_kecamatan')
                ->select(db::raw("ref_kecamatan.kd_kecamatan,nm_kecamatan,kd_kelurahan,nm_kelurahan"))
                ->whereRaw($wh)
                ->orderByRaw("ref_kecamatan.kd_kecamatan,kd_kelurahan ")
                ->get();

            foreach ($kawasan as $rk) {
                for ($tahun = 2003; $tahun < date('Y'); $tahun++) {
                    $filename = $rk->kd_kecamatan . '' . $rk->kd_kelurahan . ' - ' . $tahun . ' ds ' . $rk->nm_kelurahan . ' - kec ' . $rk->nm_kecamatan . '.xlsx';
                    $this->info('Sedang memproses :' . $filename);
                    $data = Piutang::PerNop($rk->kd_kecamatan, $rk->kd_kelurahan, $tahun)->get();

                    Excel::store(new DataExport($data), $filename, 'reports');
                }
            }
            $this->info('Selesai!');
        } else {
            // $this->info('kecamatan kosong !');
            if ($tahun != '') {


                $kawasan = DB::connection("oracle_satutujuh")->table('ref_kecamatan')->select(db::raw("kd_kecamatan, null kd_kelurahan,nm_kecamatan"))
                    ->orderby("kd_kecamatan")->get();



                foreach ($kawasan as $rk) {
                    $filename = $rk->kd_kecamatan . ' - ' . $tahun . '  kec ' . $rk->nm_kecamatan . '.xlsx';
                    $this->info('Sedang memproses :' . $filename);
                    $data = Piutang::PerNop($rk->kd_kecamatan, $rk->kd_kelurahan, $tahun)->get();

                    if (Storage::disk('reports')->exists($filename)) {
                        Storage::disk('reports')->delete($filename);
                    }

                    Excel::store(new DataExport($data), $filename, 'reports');
                }
            } else {
                $this->info("kosooognnnggg");
            }
        }
    }
}
