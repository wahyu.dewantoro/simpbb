<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Output\BufferedOutput;

class runningQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'running:queue {nama_queue?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // cek queue u
        $queue = $this->argument('nama_queue') ?? 'default';
        $cd = DB::select(db::raw("SELECT nvl(sum(jalan),0) jalan,nvl(sum(belum_jalan),0) belum 
                                from (
                                select  case when attempts=1 then 1 else 0 end  jalan,case when attempts=0 then 1 else 0 end  belum_jalan 
                                from jobs
                                where queue='" . $queue . "')"));

        if ($cd[0]->jalan == 0 and $cd[0]->belum > 0) {
            Log::info("queue " . $queue . " dijalankan");
            // $log=storage_path('logs/queue_' . date('Y-m-d') . '.log');
            $output = new BufferedOutput();
            Artisan::call('queue:work', ['--queue' => $queue, '--timeout' => 0], $output);
            Log::info($output);
        } else {
            Log::info("queue " . $queue . " tidak dijalankan");
        }
    }
}
