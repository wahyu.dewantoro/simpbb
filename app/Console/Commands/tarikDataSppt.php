<?php

namespace App\Console\Commands;

use App\Exports\dataSaldoexport;
use App\Exports\NilaiKoreksiExcel;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\ErrorHandler\Collecting;

// use Maatwebsite\Excel\Facades\Excel;

class tarikDataSppt extends Command
{
       /**
        * The name and signature of the console command.
        *
        * @var string
        */
       protected $signature = 'tarik:sppt';

       /**
        * The console command description.
        *
        * @var string
        */
       protected $description = 'Command description';

       /**
        * Create a new command instance.
        *
        * @return void
        */
       public function __construct()
       {
              parent::__construct();
       }

       /**
        * Execute the console command.
        *
        * @return int
        */
       public function handle()
       {
              for ($tahun = 2003; $tahun <= 2023; $tahun++) {

                     $this->info('narik data ' . $tahun);

                     /*                      $koreksi = DB::connection('oracle_satutujuh')->table(DB::raw("(
                            select a.kd_propinsi,
                            a.kd_dati2,
                            a.kd_kecamatan,
                            a.kd_kelurahan,
                            a.kd_blok,
                            a.no_urut,
                            a.kd_jns_op,
                            a.thn_pajak_sppt,b.nilai_koreksi
                            from ref_koreksi a
                            left join (
                            select a.kd_propinsi,
                            a.kd_dati2,
                            a.kd_kecamatan,
                            a.kd_kelurahan,
                            a.kd_blok,
                            a.no_urut,
                            a.kd_jns_op,
                            a.thn_pajak_sppt,pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0) nilai_koreksi
                            from sppt a
                            left join sppt_potongan b on a.kd_propinsi=b.kd_propinsi and
                            a.kd_dati2=b.kd_dati2 and
                            a.kd_kecamatan=b.kd_kecamatan and
                            a.kd_kelurahan=b.kd_kelurahan and
                            a.kd_blok=b.kd_blok and
                            a.no_urut=b.no_urut and
                            a.kd_jns_op=b.kd_jns_op and
                            a.thn_pajak_sppt=b.thn_pajak_sppt
                            ) b on a.kd_propinsi=b.kd_propinsi and
                            a.kd_dati2=b.kd_dati2 and
                            a.kd_kecamatan=b.kd_kecamatan and
                            a.kd_kelurahan=b.kd_kelurahan and
                            a.kd_blok=b.kd_blok and
                            a.no_urut=b.no_urut and
                            a.kd_jns_op=b.kd_jns_op and
                            a.thn_pajak_sppt=b.thn_pajak_sppt)"))
                            ->where('thn_pajak_sppt', $tahun)
                            ->get();
                     Excel::store(new NilaiKoreksiExcel($koreksi), 'koreksi saldo ' . $tahun . '.xlsx'); */
                     /*                      $data = DB::connection('oracle_satutujuh')->table('tmp_saldo_cut_22')
                            ->where('tahun_terbit', $tahun)
                            ->where('saldo_sisa', '>', 0)
                            ->get();
                     Excel::store(new dataSaldoexport($data), 'rincian saldo ' . $tahun . '.xlsx');
 */

                     $data = DB::connection('oracle_satutujuh')->table(DB::raw("(select a.kd_propinsi||'.'||
                     a.kd_dati2||'.'||
                     a.kd_kecamatan||'.'||
                     a.kd_kelurahan||'.'||
                     a.kd_blok||'.'||
                     a.no_urut||'.'||
                     a.kd_jns_op nop,tahun_pajak,tahun_terbit,a.nm_wp_sppt, nm_kelurahan||' -'|| nm_kecamatan alamat_op,
                                 jln_wp_sppt||' '||kelurahan_wp_sppt||' '||kota_wp_sppt alamat_wp,
                                 pbb,potongan,koreksi,bayar,saldo_sisa
                             from tmp_saldo_cut_23 a
                            left join ref_kecamatan c on c.kd_kecamatan=a.kd_kecamatan
                             left join ref_kelurahan d on d.kd_kecamatan=a.kd_kecamatan and a.kd_kelurahan=d.kd_kelurahan 
                             left join sppt b on b.kd_propinsi=a.kd_propinsi and
                     b.kd_dati2=a.kd_dati2 and
                     b.kd_kecamatan=a.kd_kecamatan and
                     b.kd_kelurahan=a.kd_kelurahan and
                     b.kd_blok=a.kd_blok and
                     b.no_urut=a.no_urut and
                     b.kd_jns_op=a.kd_jns_op and
                     b.thn_pajak_sppt=a.tahun_pajak
                     where saldo_sisa >0
                     )"))
                            ->where('tahun_terbit', $tahun)
                            ->where('saldo_sisa', '>', 0)
                            ->get();
                     Excel::store(new dataSaldoexport($data), 'saldo 81 - ' . $tahun . '.xlsx', 'local');
                     $this->info($tahun . ' selesai');
              }
       }
}
