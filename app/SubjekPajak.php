<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjekPajak extends Model
{
    //
    protected $connection = 'oracle_satutujuh';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'dat_subjek_pajak';
    public $timestamps = false;
    protected $primaryKey = ['subjek_pajak_id'];
}
