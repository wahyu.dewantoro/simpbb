<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranSppt extends Model
{
    
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'pembayaran_sppt';
    public $timestamps = false;
    // protected $primaryKey = ['thn_pajak_sppt','kd_propinsi','kd_dati2','kd_kecamatan','kd_kelurahan','kd_blok','no_urut','kd_jns_op'];

}
