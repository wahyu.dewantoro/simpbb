<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RptPiutang extends Model
{
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'rpt_piutang';
    public $timestamps = false;

    
    public function scopeBerturut($query, $value = 5)
    {
        $sql = "";
        if ($value == 3) {
            $sql = $query->whereraw("(a is not null and b is not null and c is not null) or ( b is not null and c is not null and d is not null) or (c is not null and d is not null and e is not null)");
        } else {
            $sql = $query->whereraw("(a is not null and b is not null and c is not null and d is not null and e is not null)");
        }

        return $sql;
    }

    public function scopeBuku($query, $buku)
    {
        $sql = "";
        for ($i = 'a'; $i <= 'e'; $i++) {
            $sql .= " get_buku(" . $i . ") in (" . $buku . ")  or";
        }

        $sql = substr($sql, 0, -2);
        return $query->whereraw(" rpt_piutang.rowid in ( select rowid from rpt_piutang where (".$sql.") )" );
    }
}
