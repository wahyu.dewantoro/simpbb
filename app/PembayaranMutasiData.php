<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranMutasiData extends Model
{
    use \Awobaz\Compoships\Compoships;
    //
    protected $connection = 'oracle_dua';
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'pembayaran_sppt_mutasi_data';
    public $timestamps = false;
}
