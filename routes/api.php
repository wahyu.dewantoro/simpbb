<?php

use App\Formulir;
use App\Helpers\Dhkp;
use App\Helpers\InformasiObjek;
use App\Helpers\Pajak;
use App\Helpers\RekonQris;
use App\Jobs\NarikBesembilanBjJob;
use App\Jobs\SendSpptTte;
use App\Kelurahan;
// use App\models\Otp;
// use App\Models\PendataanObjekBng;
use Barryvdh\Debugbar\Facades\Debugbar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::get('getnop', 'Api\pendudukController@nop');
    Route::post('store_kolektif_external', 'LayananRegistrasiController@storeKolektif_external');
});

Route::post('login', 'Api\AuthController@login');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Api/SknjopApiController

Route::post('post-sknjop', 'Api\SknjopApiController@post')->middleware('auth:api');


Route::get('ceknik', 'Api\pendudukController@index');
Route::get('ceknop', 'Api\pendudukController@nop');
Route::get('ceknop_badan', 'Api\pendudukController@nopBadan');
Route::get('objek', 'objekController@show');
Route::get('objek-cekbayar', 'objekController@cekPembayaran');

Route::post('send-otp', function (Request $request) {

    $otp = randomString(4);
    Http::post(config('app.whatsapp_send_url'), [
        'number' => $request->nomor,
        'message' => "Kode OTP untuk permohonan salinan E-SPPT  \n *" . $otp . "* \n\n\n SIPANJI *#NOREPLY*",
    ]);
    return $otp;
});

Route::get('generate_empty', 'GuiCommand@generateAllEmptyfile');
Route::get('layanan_empty', 'GuiCommand@layananAllEmptyfile');
Route::get('layanan_pembetulan_empty', 'GuiCommand@layananAllEmptyfilePembetulan');
Route::get('layanan_execute', 'GuiCommand@layananReexecute');
Route::get('runjob', 'GuiCommand@runjob');
Route::get('schedule_run', function () {

    Artisan::call('queue:restart');
    Artisan::call('queue:flush');
    Artisan::call('schedule:run');
    return response()->json(
        'the schedule has been run'
    );
});

Route::get('run_generate_dhkp', function () {
    Artisan::call('config:clear');
    Artisan::call('queue:restart');
    Artisan::call('queue:flush');
    Artisan::call('make:dhkp');
    // Artisan::call('schedule:run');

    return "work";
});

route::get('znt', function (Request $request) {
    $kecamatan = $request->kecamatan;
    $kelurahan = $request->kelurahan;
    $blok = padding($request->blok, 0, 3);
    $tahun = date('Y');

    $sql = "select * from v_dat_nir where  kd_propinsi = '35'
    AND kd_dati2 = '07'
    AND kd_kecamatan = '$kecamatan'
    AND kd_kelurahan = '$kelurahan'
    AND kd_blok = '$blok'
    and thn_nir_znt='$tahun' 
    order by regexp_replace(kd_znt, '[^0-9]', '') nulls first,kd_znt asc";
    return DB::connection('oracle_dua')->select(DB::raw($sql));
});

route::get('form-lspop', function (Request $request) {
    $count = $request->index;
    $luas_bng = onlynumber(angka((int)$request->luas_bng / (int)$count)) ?? '';
    $jml_lantai = 1;
    $jpb = $request->jpb;
    $jenis_ajuan = $request->jenis_ajuan;
    $lhp_id = $request->lhp_id;
    $bangunan = [];
    if (isset($lhp_id)) {
        $bangunan = DB::table('tbl_lspop')->where('layanan_objek_id', $lhp_id)->get()->toArray();
    }

    $objp = preg_replace('/[^0-9]/', '', $request->nop ?? '');
    if ($objp != '') {
        $kd_propinsi = substr($objp, 0, 2);
        $kd_dati2 = substr($objp, 2, 2);
        $kd_kecamatan = substr($objp, 4, 3);
        $kd_kelurahan = substr($objp, 7, 3);
        $kd_blok = substr($objp, 10, 3);
        $no_urut = substr($objp, 13, 4);
        $kd_jns_op = substr($objp, 17, 1);
        $bangunan = DB::connection('oracle_satutujuh')->table(db::raw("dat_op_bangunan b"))
            ->select(db::raw("b.*,nilai_individu"))
            ->leftjoin(db::raw('tbl_lspop a'), 'a.no_formulir', '=', 'b.no_formulir_lspop')
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan' 
                    and kd_blok='$kd_blok' and no_urut='$no_urut'")->get()->toArray();
    }

    $id = $request->id;
    $pendataan = '0';
    if (isset($id) && $id <> '') {
        $pendataan = '1';
        $bangunan = DB::table('pendataan_objek_bng')->selectraw('nilai_individu,pendataan_objek_bng.*')->where('pendataan_objek_id', $id)->get()->toArray();
    }

    return view('lspop._form', compact('count', 'jpb', 'luas_bng', 'jml_lantai', 'jenis_ajuan', 'bangunan', 'pendataan'));
});

route::get('jenis-ajuan', function (Request $request) {
    $penelitian = $request->penelitian;
    $tahun = date('Y');
    $data = Formulir::where('tahun', $tahun)->where('penelitian', $penelitian)->pluck('uraian', 'id')->toArray();
    return $data;
});

// auth:api
// Route::group(['middleware' => ['auth:api']], function () {
route::get('cekznt', function (Request $request) {
    $kecamatan = $request->kd_kecamatan ?? null;
    $kd_kelurahan = $request->kd_kelurahan ?? null;
    $tahun = $request->tahun;
    $kd_znt = $request->kd_znt;

    $nir = DB::connection('oracle_dua')
        ->table("v_dat_nir")
        ->selectraw('count(1) res')
        ->whereraw("kd_kecamatan='$kecamatan' and kd_kelurahan='$kd_kelurahan' and thn_nir_znt='$tahun' and kd_znt='$kd_znt'")->first();
    return $nir->res ?? 0;
});
route::get('nop-last-number', function (Request $request) {
    $kd_kecamatan = $request->kd_kecamatan;
    $kd_kelurahan = $request->kd_kelurahan;
    $kd_blok = $request->kd_blok;
    $userid =  $request->user_id;

    return Pajak::generateNOP($kd_kecamatan, $kd_kelurahan, $kd_blok, $userid);
});

route::get('cek-unique-nop', function (Request $request) {
    $kd_kecamatan = $request->kd_kecamatan;
    $kd_kelurahan = $request->kd_kelurahan;
    $kd_blok = $request->kd_blok;
    $no_urut = $request->no_urut;
    $kd_jns_op = $request->kd_jns_op;

    // dat_objek_pajak
    $checking = DB::connection('oracle_satutujuh')->table('dat_objek_pajak')
        ->select(db::raw("kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop"))
        ->whereraw(DB::raw("kd_propinsi='35' and kd_dati2='07' 
                and kd_kecamatan='$kd_kecamatan' 
                and kd_kelurahan='$kd_kelurahan' 
                and kd_blok='$kd_blok' 
                and no_urut='$no_urut' 
                and kd_jns_op='$kd_jns_op'"))->get()->toArray();


    // cek tmp spop
    $ni = '3507' . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
    $ca = DB::table("tbl_spop_tmp")->whereraw(" trim(nop_proses)='$ni' ")->select('nop_proses')->get()->toArray();

    // return $checking;

    $gabung = array_merge($checking, $ca);
    // log::info($gabung);

    // return false;
    if (count($gabung) > 0) {
        return 'false';
    } else {
        return 'true';
    }
});
// });

route::post('data-objek-simple', function (Request $request) {
    // return $request->all();
    $kd_propinsi = $request->kd_propinsi ?? '35';
    $kd_dati2 = $request->kd_dati2 ?? '07';
    $kd_kecamatan = $request->kd_kecamatan;
    $kd_kelurahan = $request->kd_kelurahan;
    $kd_blok = $request->kd_blok;
    $no_urut = $request->no_urut;
    $kd_jns_op = $request->kd_jns_op;

    /*    $tt = DB::connection("oracle_satutujuh")->table("SPPT")
        ->select(db::raw("max(thn_pajak_sppt) thn_pajak_sppt"))
        ->whereraw("kd_propinsi='$kd_propinsi' and
        kd_dati2='$kd_dati2' and
        kd_kecamatan='$kd_kecamatan' and
        kd_kelurahan='$kd_kelurahan' and
        kd_blok='$kd_blok' and
        no_urut='$no_urut' and
        kd_jns_op='$kd_jns_op' ")
        ->first(); */
    $tt = DB::connection('oracle_satutujuh')
        ->table('SPPT')
        ->selectRaw('MAX(thn_pajak_sppt) AS thn_pajak_sppt')
        ->where('kd_propinsi', $kd_propinsi)
        ->where('kd_dati2', $kd_dati2)
        ->where('kd_kecamatan', $kd_kecamatan)
        ->where('kd_kelurahan', $kd_kelurahan)
        ->where('kd_blok', $kd_blok)
        ->where('no_urut', $no_urut)
        ->where('kd_jns_op', $kd_jns_op)
        ->first();

    $tahun = $tt->thn_pajak_sppt ?? date('Y');

    try {
        //code...
        $status = true;
        $data = DB::connection('oracle_dua')->table(db::raw("dat_objek_pajak dop"))
            ->leftjoin(db::raw("dat_op_bumi dob"), function ($join) {
                $join->on('dop.kd_propinsi', '=', 'dob.kd_propinsi')
                    ->on('dop.kd_dati2', '=', 'dob.kd_dati2')
                    ->on('dop.kd_kecamatan', '=', 'dob.kd_kecamatan')
                    ->on('dop.kd_kelurahan', '=', 'dob.kd_kelurahan')
                    ->on('dop.kd_blok', '=', 'dob.kd_blok')
                    ->on('dop.no_urut', '=', 'dob.no_urut')
                    ->on('dop.kd_jns_op', '=', 'dob.kd_jns_op');
            })
            ->selectraw("dop.kd_propinsi,
            dop.kd_dati2,
            dop.kd_kecamatan,
            dop.kd_kelurahan,
            dop.kd_blok,
            dop.no_urut,
            dop.kd_jns_op,
            trim(dop.subjek_pajak_id) subjek_pajak_id,
            (SELECT nm_wp
               FROM pbb.dat_subjek_pajak
              WHERE subjek_pajak_id = dop.subjek_pajak_id)
               nm_wp,
            jalan_op,
            CASE
               WHEN TRIM (blok_kav_no_op) != '-' THEN blok_kav_no_op
               ELSE NULL
            END
               blok_kav_no_op,
            CASE
               WHEN rw_op IS NOT NULL AND LPAD (TRIM (rw_op), 2, 0) != '00'
               THEN
                  'RW ' || rw_op
               ELSE
                  NULL
            END
               rw_op,
            CASE
               WHEN rt_op IS NOT NULL AND LPAD (TRIM (rt_op), 3, 0) != '000'
               THEN
                  'RT ' || rt_op
               ELSE
                  NULL
            END
               rt_op,
            nm_kelurahan,
            nm_kecamatan,
         case when total_luas_bumi > 0 then total_luas_bumi else 
            (select luas_bumi_sppt from sppt 
           where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
        and kd_blok=dop.kd_blok
        and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
        and thn_pajak_sppt='$tahun' )
         end    total_luas_bumi,
            nvl((select case when luas_bumi_sppt >0 then njop_bumi_sppt/luas_bumi_sppt else null end from sppt 
        where  kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
        and kd_blok=dop.kd_blok
        and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
        and thn_pajak_sppt='$tahun'
    ),          (select nir*1000 
        from dat_nir
        where kd_znt=dob.kd_znt
        and kd_kecamatan=dob.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan
        and thn_nir_znt='" . date('Y') . "')) njop_bumi
            ,
            case when total_luas_bng > 0 then total_luas_bng else 
            (select luas_bng_sppt from sppt 
           where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
        and kd_blok=dop.kd_blok
        and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
        and thn_pajak_sppt='$tahun' )
         end   
            total_luas_bng,
            case when njop_bng>0 then  njop_bng/total_luas_bng else 
            case when total_luas_bng > 0 then total_luas_bng else 
            (select case when luas_bng_sppt >0 then  njop_bng_sppt/ luas_bng_sppt else 0 end from sppt 
           where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
        and kd_blok=dop.kd_blok
        and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
        and thn_pajak_sppt='$tahun' )
         end 
            end njop_bng  ,
            (SELECT kd_znt_peta
               FROM pbb.v_alamat_objek
              WHERE     kd_kecamatan = dop.kd_kecamatan
                    AND kd_kelurahan = dop.kd_kelurahan
                    AND kd_blok = dop.kd_blok
                    AND no_urut = dop.no_urut
                    AND kd_jns_op = dop.kd_jns_op
                    AND thn_nir_znt = TO_CHAR (SYSDATE, 'yyyy')
                    AND ROWNUM = 1)
               kd_znt")
            ->join(db::raw('ref_kecamatan rk'), 'rk.kd_kecamatan', '=', 'dop.kd_kecamatan')
            ->join(db::raw('ref_kelurahan ra'), function ($join) {
                $join->on('ra.kd_kecamatan', '=', 'dop.kd_kecamatan')
                    ->on('ra.kd_kelurahan', '=', 'dop.kd_kelurahan');
            })
            ->whereraw("dop.kd_propinsi='$kd_propinsi' and
                        dop.kd_dati2='$kd_dati2' and
                        dop.kd_kecamatan='$kd_kecamatan' and
                        dop.kd_kelurahan='$kd_kelurahan' and
                        dop.kd_blok='$kd_blok' and
                        dop.no_urut='$no_urut' and
                        dop.kd_jns_op='$kd_jns_op' ")->first();
        // return $data;

    } catch (\Throwable $th) {
        //throw $th;
        $status = false;
        $data = null;
    }
    return [
        'status' => $status,
        'data' => $data
    ];
});

route::post('data-objek', function (Request $request) {
    // return response()->json($request->all());
    $kd_propinsi = $request->kd_propinsi ?? '35';
    $kd_dati2 = $request->kd_dati2 ?? '07';
    $kd_kecamatan = $request->kd_kecamatan;
    $kd_kelurahan = $request->kd_kelurahan;
    $kd_blok = $request->kd_blok;
    $no_urut = $request->no_urut;
    $kd_jns_op = $request->kd_jns_op;


    $nopgabung = $kd_propinsi . $kd_dati2 . $kd_kecamatan . $kd_kelurahan . $kd_blok . $no_urut . $kd_jns_op;
    $variable['kd_propinsi'] = $kd_propinsi;
    $variable['kd_dati2'] = $kd_dati2;
    $variable['kd_kecamatan'] = $kd_kecamatan;
    $variable['kd_kelurahan'] = $kd_kelurahan;
    $variable['kd_blok'] = $kd_blok;
    $variable['no_urut'] = $no_urut;
    $variable['kd_jns_op'] = $kd_jns_op;

    $tt = DB::connection("oracle_satutujuh")->table("SPPT")
        ->select(db::raw("max(thn_pajak_sppt) thn_pajak_sppt"))
        ->whereraw("kd_propinsi='$kd_propinsi' and
    kd_dati2='$kd_dati2' and
    kd_kecamatan='$kd_kecamatan' and
    kd_kelurahan='$kd_kelurahan' and
    kd_blok='$kd_blok' and
    no_urut='$no_urut' and
    kd_jns_op='$kd_jns_op' ")
        ->first();
    $tahun = $tt->thn_pajak_sppt ?? date('Y');

    $objek = DB::connection('oracle_dua')->table(db::raw("dat_objek_pajak dop"))
        ->leftjoin(db::raw("dat_op_bumi dob"), function ($join) {
            $join->on('dop.kd_propinsi', '=', 'dob.kd_propinsi')
                ->on('dop.kd_dati2', '=', 'dob.kd_dati2')
                ->on('dop.kd_kecamatan', '=', 'dob.kd_kecamatan')
                ->on('dop.kd_kelurahan', '=', 'dob.kd_kelurahan')
                ->on('dop.kd_blok', '=', 'dob.kd_blok')
                ->on('dop.no_urut', '=', 'dob.no_urut')
                ->on('dop.kd_jns_op', '=', 'dob.kd_jns_op');
        })
        ->leftjoin(db::raw("dat_subjek_pajak dsp"), 'dsp.subjek_pajak_id', '=', 'dop.subjek_pajak_id')
        ->selectRaw(db::raw("dop.kd_propinsi,
		dop.kd_dati2,
		dop.kd_kecamatan,
		dop.kd_kelurahan,
		dop.kd_blok,
		dop.no_urut,
		dop.kd_jns_op,
		dop.subjek_pajak_id,
		dop.no_formulir_spop,
		dop.no_persil,
		dop.jalan_op,
		dop.blok_kav_no_op,
		dop.rw_op,
		dop.rt_op,
		dop.kd_status_cabang,
		dop.kd_status_wp,
        case when total_luas_bumi > 0 then total_luas_bumi else 
        (select luas_bumi_sppt from sppt 
       where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
    and kd_blok=dop.kd_blok
    and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
    and thn_pajak_sppt='$tahun' )
     end  total_luas_bumi,
        case when total_luas_bng > 0 then total_luas_bng else 
        (select luas_bng_sppt from sppt 
       where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
    and kd_blok=dop.kd_blok
    and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
    and thn_pajak_sppt='$tahun' )
     end 
        total_luas_bng,
		nvl((select case when luas_bumi_sppt >0 then njop_bumi_sppt/luas_bumi_sppt else null end from sppt 
        where  kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
        and kd_blok=dop.kd_blok
        and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
        and thn_pajak_sppt='" . date('Y') . "'
    ),          (select nir*1000 
        from dat_nir
        where kd_znt=dob.kd_znt
        and kd_kecamatan=dob.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan
        and thn_nir_znt='" . date('Y') . "')) njop_bumi,
        case when njop_bng>0 then  njop_bng/total_luas_bng else 
        case when total_luas_bng > 0 then total_luas_bng else 
        (select case when luas_bng_sppt >0 then  njop_bng_sppt/ luas_bng_sppt else 0 end from sppt 
       where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
    and kd_blok=dop.kd_blok
    and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
    and thn_pajak_sppt='$tahun' )
     end 
        end 
        njop_bng,
		dop.status_peta_op,
		dop.jns_transaksi_op,
		dop.tgl_pendataan_op,
		dop.nip_pendata,
		dop.tgl_pemeriksaan_op,
		dop.nip_pemeriksa_op,
		dop.tgl_perekaman_op,
		dop.nip_perekam_op,
        jns_bumi,kd_znt,nm_wp,jalan_wp,blok_kav_no_wp,rw_wp,rt_wp,kelurahan_wp,kota_wp,kd_pos_wp,telp_wp,npwp,status_pekerjaan_wp,
            (select count(1) from dat_op_bangunan 
            where kd_kecamatan=dop.kd_kecamatan and kd_kelurahan=dop.kd_kelurahan 
            and kd_blok=dop.kd_blok
            and no_urut=dop.no_urut and kd_jns_op=dop.kd_jns_op
            ) jml_bng
        "))
        ->whereraw("dop.kd_propinsi='35' and dop.kd_dati2='07' and dop.kd_kecamatan='$kd_kecamatan' and dop.kd_kelurahan='$kd_kelurahan' and dop.kd_blok='$kd_blok' and dop.no_urut='$no_urut' and dop.kd_jns_op='$kd_jns_op' ")->first();

    $where['sppt.kd_propinsi'] = $kd_propinsi;
    $where['sppt.kd_dati2'] = $kd_dati2;
    $where['sppt.kd_kecamatan'] = $kd_kecamatan;
    $where['sppt.kd_kelurahan'] = $kd_kelurahan;
    $where['sppt.kd_blok'] = $kd_blok;
    $where['sppt.no_urut'] = $no_urut;
    $where['sppt.kd_jns_op'] = $kd_jns_op;
    $riwayatpembayaran = DB::connection("oracle_satutujuh")->table("sppt")
        ->leftjoin('sppt_potongan', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
        })
        ->leftjoin(db::raw("pengurang_piutang sppt_koreksi"), function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_koreksi.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'sppt_koreksi.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'sppt_koreksi.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'sppt_koreksi.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'sppt_koreksi.kd_blok')
                ->on('sppt.no_urut', '=', 'sppt_koreksi.no_urut')
                ->on('sppt.kd_jns_op', '=', 'sppt_koreksi.kd_jns_op')
                ->on('sppt.thn_pajak_sppt', '=', 'sppt_koreksi.thn_pajak_sppt');
            // ->on('sppt.status_pembayaran_sppt', '=', 3);
        })
        ->leftjoin(
            db::raw("( SELECT a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.tahun_pajak thn_pajak_sppt, LISTAGG ( kobil, ', ') WITHIN GROUP (ORDER BY kobil) kobil
                                    FROM sim_pbb.billing_kolektif a
                                    JOIN sim_pbb.data_billing b ON a.data_billing_id=b.data_billing_id
                                    WHERE kd_status = '0' AND b.expired_at > SYSDATE and b.deleted_at is null
                                GROUP BY a.kd_propinsi, a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op, a.tahun_pajak) sppt_penelitian"),
            function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'sppt_penelitian.kd_propinsi')
                    ->on('sppt.kd_dati2', '=', 'sppt_penelitian.kd_dati2')
                    ->on('sppt.kd_kecamatan', '=', 'sppt_penelitian.kd_kecamatan')
                    ->on('sppt.kd_kelurahan', '=', 'sppt_penelitian.kd_kelurahan')
                    ->on('sppt.kd_blok', '=', 'sppt_penelitian.kd_blok')
                    ->on('sppt.no_urut', '=', 'sppt_penelitian.no_urut')
                    ->on('sppt.kd_jns_op', '=', 'sppt_penelitian.kd_jns_op')
                    ->on('sppt.thn_pajak_sppt', '=', 'sppt_penelitian.thn_pajak_sppt');
            }
        )
        ->leftjoin('pembayaran_sppt', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok')
                ->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut')
                ->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op')
                ->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
        })
        ->where($where)
        ->whereraw("sppt.thn_pajak_sppt>=2003")
        ->groupbyraw("sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.thn_pajak_sppt, nm_wp_sppt, sppt.pbb_yg_harus_dibayar_sppt, NVL (nilai_potongan, 0), sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt_koreksi.keterangan, sppt_koreksi.tgl_surat, SPPT_KOREKSI.NO_SURAT, CASE WHEN status_pembayaran_sppt!='1' THEN spo.get_denda ( pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt.tgl_jatuh_tempo_sppt, SYSDATE) ELSE NULL END, status_pembayaran_sppt, kobil,sppt_koreksi.no_transaksi,
            luas_bumi_sppt ,
 case when luas_bumi_sppt>0 then  njop_bumi_sppt / luas_bumi_sppt else 0 end,
luas_bng_sppt,
case when luas_bng_sppt>0 then  njop_bng_sppt / luas_bng_sppt else 0 end
        ")
        ->select(db::raw("sppt.kd_propinsi, sppt.kd_dati2, sppt.kd_kecamatan, sppt.kd_kelurahan, sppt.kd_blok, sppt.no_urut, sppt.kd_jns_op, sppt.thn_pajak_sppt, nm_wp_sppt, case when SPPT_KOREKSI.NO_TRANSAKSI is not null then '3' else
        status_pembayaran_sppt
        end status_pembayaran_sppt,
        luas_bumi_sppt luas_bumi,
 case when luas_bumi_sppt>0 then  njop_bumi_sppt / luas_bumi_sppt else 0 end njop_bumi,
luas_bng_sppt luas_bng,
case when luas_bng_sppt>0 then  njop_bng_sppt / luas_bng_sppt else 0 end njop_bng
        , sppt.pbb_yg_harus_dibayar_sppt pbb, NVL (nilai_potongan, 0) potongan, sppt.pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0) pbb_akhir, CASE WHEN status_pembayaran_sppt!='1' THEN spo.get_denda ( pbb_yg_harus_dibayar_sppt - NVL (nilai_potongan, 0), sppt.tgl_jatuh_tempo_sppt, SYSDATE) ELSE NULL END denda, sppt_koreksi.keterangan, sppt_koreksi.tgl_surat, SPPT_KOREKSI.NO_SURAT, SUM (denda_sppt) denda_bayar, SUM (jml_sppt_yg_dibayar) jumlah_bayar, LISTAGG ( TO_CHAR (tgl_pembayaran_sppt, 'yyyy-mm-dd'), ',') WITHIN GROUP (ORDER BY tgl_pembayaran_sppt) AS tgl_bayar, kobil,sppt_koreksi.no_transaksi no_koreksi"))
        ->orderby("sppt.thn_pajak_sppt", 'asc')->get();


    $sk = DB::table(db::raw("(select a.layanan_objek_id
        from surat_keputusan a
        join (
        select distinct layanan_objek_id
        from tbl_spop
        where nop_proses='$nopgabung') b on a.layanan_objek_id=b.layanan_objek_id
        where to_char(tanggal,'yyyy')=to_char(sysdate,'yyyy') ) res"))->first();
    $url_sk = '';
    if ($sk) {
        $url_sk = config('app.url') . '/sk/' . acak($sk->layanan_objek_id);
    }

    return [
        'objek' => $objek,
        'pembayaran' => $riwayatpembayaran,
        'sk' => $url_sk
    ];
});

route::post('riwayat-pembayaran', function (Request $request) {
    $kd_propinsi = $request->kd_propinsi ?? '35';
    $kd_dati2 = $request->kd_dati2 ?? '07';
    $kd_kecamatan = $request->kd_kecamatan;
    $kd_kelurahan = $request->kd_kelurahan;
    $kd_blok = $request->kd_blok;
    $no_urut = $request->no_urut;
    $kd_jns_op = $request->kd_jns_op;

    $where['sppt.kd_propinsi'] = $kd_propinsi;
    $where['sppt.kd_dati2'] = $kd_dati2;
    $where['sppt.kd_kecamatan'] = $kd_kecamatan;
    $where['sppt.kd_kelurahan'] = $kd_kelurahan;
    $where['sppt.kd_blok'] = $kd_blok;
    $where['sppt.no_urut'] = $no_urut;
    $where['sppt.kd_jns_op'] = $kd_jns_op;

    return DB::connection("oracle_satutujuh")->table("sppt")->select(db::raw(" sppt.thn_pajak_sppt tahun, (select to_char(tgl_pembayaran_sppt,'yyyy-mm-dd') from pembayaran_sppt where kd_propinsi=sppt.kd_propinsi and
                                                kd_dati2=sppt.kd_dati2 and
                                                kd_kecamatan=sppt.kd_kecamatan and
                                                kd_kelurahan=sppt.kd_kelurahan and
                                                kd_blok=sppt.kd_blok and
                                                no_urut=sppt.no_urut and
                                                kd_jns_op=sppt.kd_jns_op and
                                                thn_pajak_sppt=sppt.thn_pajak_sppt and
                                                rownum=1) tanggal, pbb_yg_harus_dibayar_sppt - nvl(nilai_potongan,0) pbb"))
        ->leftjoin('sppt_potongan', function ($join) {
            $join->on('sppt.kd_propinsi', '=', 'sppt_potongan.kd_propinsi')
                ->on('sppt.kd_dati2', '=', 'sppt_potongan.kd_dati2')
                ->on('sppt.kd_kecamatan', '=', 'sppt_potongan.kd_kecamatan')
                ->on('sppt.kd_kelurahan', '=', 'sppt_potongan.kd_kelurahan')
                ->on('sppt.kd_blok', '=', 'sppt_potongan.kd_blok')
                ->on('sppt.no_urut', '=', 'sppt_potongan.no_urut')
                ->on('sppt.kd_jns_op', '=', 'sppt_potongan.kd_jns_op')
                ->on('sppt.thn_pajak_sppt', '=', 'sppt_potongan.thn_pajak_sppt');
        })
        ->where($where)->whereraw("rownum<=10")
        ->orderby('sppt.thn_pajak_sppt', 'desc')
        ->get();
});

route::post('subjek-pajak', function (Request $request) {
    $nik = $request->nik ?? '';
    $res = null;
    try {
        //code...
        $data = DB::connection('oracle_satutujuh')->table('dat_subjek_pajak')
            ->select(db::raw("trim(SUBJEK_PAJAK_ID) SUBJEK_PAJAK_ID, trim(NM_WP) NM_WP, trim(JALAN_WP) JALAN_WP, trim(BLOK_KAV_NO_WP) BLOK_KAV_NO_WP, trim(RW_WP) RW_WP, trim(RT_WP) RT_WP, trim(KELURAHAN_WP) KELURAHAN_WP, trim(KOTA_WP) KOTA_WP, trim(KD_POS_WP) KD_POS_WP, trim(TELP_WP) TELP_WP, trim(NPWP) NPWP, trim(STATUS_PEKERJAAN_WP) STATUS_PEKERJAAN_WP"))
            ->whereraw("trim(subjek_pajak_id)='" . $nik . "'")
            ->get();
        foreach ($data as $row) {
            $res = (array)$row;
        }
    } catch (\Throwable $th) {
        //throw $th;
        $res = null;
    }
    return ['data' => $res];
});

route::post('feedback-tte', function (Request $request) {
    DB::beginTransaction();
    try {


        $id = $request->id;
        $url = $request->url;
        $table = $request->table;
        $res = [];

        // retrun $table;

        if ($table == 'sppt') {
            $nop = $id;
            $kd_propinsi = substr($nop, 0, 2);
            $kd_dati2 = substr($nop, 2, 2);
            $kd_kecamatan = substr($nop, 4, 3);
            $kd_kelurahan = substr($nop, 7, 3);
            $kd_blok = substr($nop, 10, 3);
            $no_urut = substr($nop, 13, 4);
            $kd_jns_op = substr($nop, 17, 1);
            $thn_pajak_sppt = substr($nop, 18, 4);

            $res = DB::connection("oracle_satutujuh")->table(DB::raw($table))
                ->where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->where('thn_pajak_sppt', $thn_pajak_sppt)

                ->update([
                    'tte_file_url' => $url,
                    'tte_upload_at' => Carbon::now()
                ]);
            DB::commit();
        }
    } catch (\Throwable $error) {
        DB::rollBack();
        $res = $error->getMessage();
    }
    return $res;
})->name('api.feedback.tte');

route::get('send-sppt-tte', function () {
    Debugbar::disable();
    SendSpptTte::dispatch()->onQueue('sppt');
    Log::info("Mengirim file sppt untuk di TTE");
});

route::get('call-besembilan', function (Request $request) {
    $kemarin = date('Ymd', strtotime('-1 days'));
    NarikBesembilanBjJob::dispatch($kemarin)->onQueue("besembilan");
});


route::get('rekon-qris', function () {
    $tanggal = date('dmY', strtotime('-1 days'));

    $data = DB::select("select to_char(created_at,'ddmmyyyy') tanggal,kobil,data_billing_id
    from v_qris
    where   trunc(created_at) between trunc(sysdate-1)
    and trunc(sysdate)
    and amount_pay is null");
    foreach ($data as $row) {

        $pay = [
            'data_billing_id' => trim($row->data_billing_id),
            'kobil' => trim($row->kobil),
            'tanggal' => $row->tanggal
        ];
        RekonQris::proses($pay);
    }
});


route::post('pembayaran-sppt', function (Request $request) {
    $nop = $request->nop;
    $res = onlyNumber($nop);
    $variable['kd_propinsi'] = substr($res, 0, 2);
    $variable['kd_dati2'] = substr($res, 2, 2);
    $variable['kd_kecamatan'] = substr($res, 4, 3);
    $variable['kd_kelurahan'] = substr($res, 7, 3);
    $variable['kd_blok'] = substr($res, 10, 3);
    $variable['no_urut'] = substr($res, 13, 4);
    $variable['kd_jns_op'] = substr($res, 17, 1);

    $data = InformasiObjek::pembayaranSppt($variable)->get();
    return response()->json($data);
});


route::post('data-dan-informasi', function (Request $request) {
    $nop = onlyNumber($request->nop);
    $getData = InformasiObjek::dataDanInformasi($nop);
    return response()->json($getData);
});


route::post('data-dhkp', function (Request $request) {
    $tahun = date('Y');
    $kd_kecamatan = $request->kecamatan ?? null;
    $kd_kelurahan = null;
    // $kd_buku = $request->buku ?? null;
    if ($tahun != '' && $kd_kecamatan != '') {

        $buku = [1, 2];
        $param = [
            'tahun' => $tahun,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'buku' => $buku
        ];
        $preview = Dhkp::RingkasanDhkpWepe($param);

        $data = [];
        foreach ($preview as $row) {
            $data[] = (object)[
                // "nm_sektor" => $row->nm_sektor,
                "kecamatan" => $row->kecamatan,
                "kelurahan" => $row->kelurahan,
                "objek_lunas" => (int)$row->op_lunas,
                "baku_lunas" => (int) $row->jumlah_lunas,
                "objek_belum_lunas" => (int)$row->op_belum,
                "baku_belum_lunas" => (int) $row->jumlah_belum,
                "objek_total" => (int) $row->op_total,
                "baku_total" => (int) $row->jumlah_total,
            ];
        }

        return [
            'isFalse' => false,
            'data' => $data
        ];
    } else {
        return  [
            'isFalse' => true,
            'data' => null
        ];
    }
});

route::get('desa', function (Request $request) {
    $kd_kecamatan = $request->kd_kecamatan;
    $desa = Kelurahan::orderby('kd_Kecamatan')->orderby('kd_kelurahan')->where("kd_kecamatan", $kd_kecamatan)
        ->pluck('nm_kelurahan', 'kd_kelurahan')->toarray();
    return $desa;
});
route::post('data-realisasi', 'RealisasiController@apiRealRangkingKecamatan');

route::post('data-jenis-layanan', 'Api\DaftarNOpelController@JenisLayanan');
route::post('data-nopel', 'Api\DaftarNOpelController@DaftarNopel');
Route::middleware('auth.basic')->group(function () {
    route::get('e-sppt/{nop}', 'SpptController@show');
});

Route::middleware('basic.auth')->group(function () {
    route::get('e-sppt/{nop}', 'SpptController@show');
    route::post('objek-informasi', function (Request $request) {
        $nop = onlyNumber($request->nop);
        try {
            //code...
            $rc = 200;
            $getData = InformasiObjek::dataDanInformasi($nop);
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            $getData = [];
            $rc = 500;
        }

        return response()->json($getData, $rc);
    });
    route::post('objek-informasi-simple', function (Request $request) {
        // return $request->all();
        $kd_propinsi = $request->kd_propinsi ?? '35';
        $kd_dati2 = $request->kd_dati2 ?? '07';
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_blok = $request->kd_blok;
        $no_urut = $request->no_urut;
        $kd_jns_op = $request->kd_jns_op;

        /*   $tt = DB::connection("oracle_satutujuh")->table("SPPT")
            ->select(db::raw("max(thn_pajak_sppt) thn_pajak_sppt"))
            ->whereraw("kd_propinsi='$kd_propinsi' and
            kd_dati2='$kd_dati2' and
            kd_kecamatan='$kd_kecamatan' and
            kd_kelurahan='$kd_kelurahan' and
            kd_blok='$kd_blok' and
            no_urut='$no_urut' and
            kd_jns_op='$kd_jns_op' ")
            ->first(); */
        $tt = DB::connection("oracle_satutujuh")
            ->table("SPPT")
            ->where("kd_propinsi", $kd_propinsi)
            ->where("kd_dati2", $kd_dati2)
            ->where("kd_kecamatan", $kd_kecamatan)
            ->where("kd_kelurahan", $kd_kelurahan)
            ->where("kd_blok", $kd_blok)
            ->where("no_urut", $no_urut)
            ->where("kd_jns_op", $kd_jns_op)
            ->max("thn_pajak_sppt");

        $tahun = $tt->thn_pajak_sppt ?? date('Y');

        try {
            //code...
            $status = true;

            $data = DB::connection('oracle_dua')->table(DB::raw("dat_objek_pajak dop"))
                ->leftJoin(DB::raw("dat_op_bumi dob"), function ($join) {
                    $join->on('dop.kd_propinsi', '=', 'dob.kd_propinsi')
                        ->on('dop.kd_dati2', '=', 'dob.kd_dati2')
                        ->on('dop.kd_kecamatan', '=', 'dob.kd_kecamatan')
                        ->on('dop.kd_kelurahan', '=', 'dob.kd_kelurahan')
                        ->on('dop.kd_blok', '=', 'dob.kd_blok')
                        ->on('dop.no_urut', '=', 'dob.no_urut')
                        ->on('dop.kd_jns_op', '=', 'dob.kd_jns_op');
                })
                ->selectRaw("
                dop.kd_propinsi,
                dop.kd_dati2,
                dop.kd_kecamatan,
                dop.kd_kelurahan,
                dop.kd_blok,
                dop.no_urut,
                dop.kd_jns_op,
                TRIM(dop.subjek_pajak_id) AS subjek_pajak_id,
                (SELECT nm_wp FROM pbb.dat_subjek_pajak WHERE subjek_pajak_id = dop.subjek_pajak_id) AS nm_wp,
                jalan_op,
                CASE 
                    WHEN TRIM(blok_kav_no_op) != '-' THEN blok_kav_no_op 
                    ELSE NULL 
                END AS blok_kav_no_op,
                CASE 
                    WHEN rw_op IS NOT NULL AND LPAD(TRIM(rw_op), 2, '0') != '00' THEN 'RW ' || rw_op 
                    ELSE NULL 
                END AS rw_op,
                CASE 
                    WHEN rt_op IS NOT NULL AND LPAD(TRIM(rt_op), 3, '0') != '000' THEN 'RT ' || rt_op 
                    ELSE NULL 
                END AS rt_op,
                nm_kelurahan,
                nm_kecamatan,
                CASE 
                    WHEN total_luas_bumi > 0 THEN total_luas_bumi 
                    ELSE (
                        SELECT luas_bumi_sppt FROM sppt 
                        WHERE kd_kecamatan = dop.kd_kecamatan 
                        AND kd_kelurahan = dop.kd_kelurahan 
                        AND kd_blok = dop.kd_blok 
                        AND no_urut = dop.no_urut 
                        AND kd_jns_op = dop.kd_jns_op 
                        AND thn_pajak_sppt = '$tahun'
                    ) 
                END AS total_luas_bumi,
                NVL((
                    SELECT CASE 
                        WHEN luas_bumi_sppt > 0 THEN njop_bumi_sppt / luas_bumi_sppt 
                        ELSE NULL 
                    END FROM sppt 
                    WHERE kd_kecamatan = dop.kd_kecamatan 
                    AND kd_kelurahan = dop.kd_kelurahan 
                    AND kd_blok = dop.kd_blok 
                    AND no_urut = dop.no_urut 
                    AND kd_jns_op = dop.kd_jns_op 
                    AND thn_pajak_sppt = '$tahun'
                ), (
                    SELECT nir * 1000 FROM dat_nir 
                    WHERE kd_znt = dob.kd_znt 
                    AND kd_kecamatan = dob.kd_kecamatan 
                    AND kd_kelurahan = dop.kd_kelurahan 
                    AND thn_nir_znt = '" . date('Y') . "'
                )) AS njop_bumi,
                CASE 
                    WHEN total_luas_bng > 0 THEN total_luas_bng 
                    ELSE (
                        SELECT luas_bng_sppt FROM sppt 
                        WHERE kd_kecamatan = dop.kd_kecamatan 
                        AND kd_kelurahan = dop.kd_kelurahan 
                        AND kd_blok = dop.kd_blok 
                        AND no_urut = dop.no_urut 
                        AND kd_jns_op = dop.kd_jns_op 
                        AND thn_pajak_sppt = '$tahun'
                    ) 
                END AS total_luas_bng,
                CASE 
                    WHEN njop_bng > 0 THEN njop_bng / total_luas_bng 
                    ELSE (
                        SELECT CASE 
                            WHEN luas_bng_sppt > 0 THEN njop_bng_sppt / luas_bng_sppt 
                            ELSE 0 
                        END FROM sppt 
                        WHERE kd_kecamatan = dop.kd_kecamatan 
                        AND kd_kelurahan = dop.kd_kelurahan 
                        AND kd_blok = dop.kd_blok 
                        AND no_urut = dop.no_urut 
                        AND kd_jns_op = dop.kd_jns_op 
                        AND thn_pajak_sppt = '$tahun'
                    ) 
                END AS njop_bng,
                (SELECT kd_znt_peta FROM pbb.v_alamat_objek 
                 WHERE kd_kecamatan = dop.kd_kecamatan 
                 AND kd_kelurahan = dop.kd_kelurahan 
                 AND kd_blok = dop.kd_blok 
                 AND no_urut = dop.no_urut 
                 AND kd_jns_op = dop.kd_jns_op 
                 AND thn_nir_znt = TO_CHAR(SYSDATE, 'yyyy') 
                 AND ROWNUM = 1
                ) AS kd_znt ")
                ->join(DB::raw('ref_kecamatan rk'), 'rk.kd_kecamatan', '=', 'dop.kd_kecamatan')
                ->join(DB::raw('ref_kelurahan ra'), function ($join) {
                    $join->on('ra.kd_kecamatan', '=', 'dop.kd_kecamatan')
                        ->on('ra.kd_kelurahan', '=', 'dop.kd_kelurahan');
                })
                ->where('dop.kd_propinsi', $kd_propinsi)
                ->where('dop.kd_dati2', $kd_dati2)
                ->where('dop.kd_kecamatan', $kd_kecamatan)
                ->where('dop.kd_kelurahan', $kd_kelurahan)
                ->where('dop.kd_blok', $kd_blok)
                ->where('dop.no_urut', $no_urut)
                ->where('dop.kd_jns_op', $kd_jns_op)
                ->first();
        } catch (\Throwable $th) {
            //throw $th;
            $status = false;
            $data = null;
        }
        return [
            'status' => $status,
            'data' => $data
        ];
    });
    route::post('riwayat-pembayaran', 'TrackingPembayaranController@apiRiwayatPembayaran');
    route::resource('permohonan-bphtb', 'Api\permohonanBphtbController');

    // list data bphtb selesai layanan



});


// Route::middleware('basic.auth')->get('/user', function (Request $request) {});
