<?php

use App\Exports\DataExport;
use App\Helpers\Baku;
use App\Helpers\Esppt;
use App\Helpers\PenetapanSppt;
use App\Helpers\Piutang;
use App\Helpers\SendWa;
use App\Kelurahan;
use App\Models\Notifikasi;
use App\User;
use App\UserOTP;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

// use SnappyPdf as PDF;
// use Barryvdh\DomPDF\Facade\Pdf;\
use DOMPDF as pdf;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    // 'verify' => true
]);

route::get('/', function () {
    // dd(Auth()->user());
    return redirect(url('home'));
    // return 'Landing Page';
    // return view('landing.index');
});


route::get('resend-otp/{id}', function (Request $request, $user_id) {
    $user_id = decrypt($user_id);
    $user = User::find($user_id);
    if ($user) {

        $userOtp = UserOTP::create([
            'user_id' => $user->id,
            'otp_code' => rand(100000, 999999),
            'expired_at' => Carbon::now()->addMinutes(4)
        ]);

        $sendWa = [
            'phone' => $user->telepon,
            'message' => $userOtp->otp_code . ' ini adalah kode OTP anda , jangan bagikan kepada siapapun.'
        ];
        SendWa::simple($sendWa);
        $status = 'true';
    } else {
        $status = 'false';
    }
    return ['st' => $status];
});

route::post('register-verifikasi', function (Request $request) {
    // return $request->all();

    $user_id = decrypt($request->id);
    $kode_otp = $request->otp;

    $user = User::with('activeOTP')->find($user_id);
    $otp_user = $user->activeOTP()->pluck('otp_code')->toarray();
    // dd($otp_user);

    if (count($otp_user) > 0) {
        // dd($otp_user);
        $otp = $request->kode_otp;
        if (in_array($otp, $otp_user)) {
            $status = 1;
            $user = User::find($user_id);
            $user->update(['email_verified_at' => now()]);
        } else {
            $status = 0;
        }
    } else {
        $status = 0;
    }
    if ($status == 1) {
        // $this->guard()->login($user);
        return redirect(url('home'));
    } else {

        return redirect(route('otp-verification', encrypt($user->id)))->with('pesan', '<span class="text-danger"><i class="fas fa-exclamation-triangle"></i> Kode OTP yang dimasukan tidak sesuai</span>');
    }
});

// route::post('register-verifikasi', 'Auth\RegisterController@verifOtp');

Route::get('/{user_id}/otp-verification', function ($user_id) {
    $user_id = decrypt($user_id);
    $user = User::with('activeOTP')->find($user_id);
    if ($user) {
        if ($user->email_verified_at == '') {
            return view('registerOtp', compact('user'));
        } else {
            return redirect(url('home'));
        }
    } else {
        // abort(404);
        return redirect(url('register'))->with('pesan', '<div class="alert alert-warning" role="alert">
        Silahkan melakukan pendaftaran akun!
      </div>');
    }

    // return view('otp-verification', compact('user'));
})->middleware([])->name('otp-verification');



// Route::get('/', 'HomeController@index')->name('home');
/* route::any('page-sppt', function () {
    return redirect(url('login'));
}); */

// Route::any('page-sppt', 'SpptController@pencarianSppt');
// Route::post('cari-sppt', 'SpptController@prosePencarian');


Route::get('/mal_sppt', function () {
    $pdf = pdf::loadview('mal_esppt');
    return $pdf->stream();
});


Route::get('informasi/riwayat-cetak', 'TrackingPembayaranController@cetak');
Route::get('/logout', 'Auth\LoginController@logout');


/* Route::post('/login', function (Request $request) {
    $credentials = request()->only('username', 'password');
    if (auth()->attempt($credentials)) {
        Auth::logoutOtherDevices($credentials['password']);
        return redirect()->intended();
    }
    return back()->withStatus('User sudah login.');
}); */

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('e-sppt-ntpd', 'DataDafnomController@espptntpd');
Route::get('sknjop-pdf/{id}', 'sknjopController@generatePdf');

route::get('downloadfile/{id}', function ($filename) {
    if (!Storage::disk('reports')->exists($filename)) {
        abort(404, 'Dokumen tidak ditemukan');
    }
    $file = Storage::disk('reports')->get($filename);
    $type = Storage::disk('reports')->mimeType($filename);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

route::get('download/{disk}/{filename}', function ($disk, $filename) {
    $filename = rapikanacak($filename);
    if (!Storage::disk($disk)->exists($filename)) {
        abort(404, 'Dokumen tidak ditemukan');
    }

    $type = Storage::disk($disk)->mimeType($filename);
    $exp = explode('/', $filename);
    $file_name = $exp[count($exp) - 1];
    $headers = [
        'Content-Type'        => $type,
        'Content-Disposition' => 'attachment; filename="' . $file_name . '"',
    ];

    return \Response::make(Storage::disk($disk)->get($filename), 200, $headers);
});

route::get('preview-dok/{disk}/{filename}', function ($disk, $filename) {
    $filename = rapikanacak($filename);
    if (!Storage::disk($disk)->exists($filename)) {
        abort(404, 'Dokumen tidak ditemukan');
    }

    $file = Storage::disk($disk)->get($filename);
    $type = Storage::disk($disk)->mimeType($filename);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::group(['middleware' => ['auth', 'isOn']], function () {
    Route::resource('users', 'UserController');
    Route::get('users-upgrade', 'UserController@upgradeUser')->name('upgrade.user.form')->middleware('wajibPajakVerification');
    Route::post('users-upgrade', 'UserController@upgradeUserStore')->name('upgrade.user.store')->middleware('wajibPajakVerification');

    Route::prefix('jobs')->group(function () {
        Route::queueMonitor();
    });
});
Route::get('informasi/sppt/cetak/{nop}', 'SpptController@show');
Route::get('informasi/sspd/cetak/{nop}', 'SpptController@sspd');
Route::get('informasi/sppt/cetak_tanda_terima/{nop}', 'SpptController@show_tanda_terima');
// cekUpdateProfile
Route::group(['middleware' => ['auth', 'isOn']], function () {
    route::get(
        'whatsapp',
        function () {
            return view('whatsapp');
        }
    );

    Route::get('/home', 'HomeController@index')->name('home');
    // ->middleware(['permission:home.menu']);;
    Route::get('impersonate', 'UserController@Impersonate')->name('impersonate');
    Route::get('impersonate/{user}', 'UserController@ProsesImpersonate')->name('impersonate.proses');
    Route::get('impersonate-leave', 'UserController@LeaveImpersonate')->name('impersonate.leave');
    Route::get('reset_password/{user}', 'UserController@reset_password')->name('users.reset_password');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::group(['prefix' => 'dafnom', 'as' => 'dafnom.'], function () {
        Route::get('/', 'DafnomController@index')->middleware(['permission:dafnom.menu']);
        Route::resource('riwayat', 'DafnomRiwayatController')->middleware(['permission:dafnom.riwayat.menu']);
        Route::post('preview', 'DafnomController@preview');
        Route::post('cek_pembuatan_dafnom', 'DafnomController@cek_pembuatan_dafnom');
        Route::post('store', 'DafnomController@store');
    });

    Route::group(['prefix' => 'data_dafnom', 'as' => 'data_dafnom.'], function () {
        Route::get('/', 'DataDafnomController@index')->middleware(['permission:data_dafnom.menu']);
        Route::get('search', 'DataDafnomController@search');
        Route::get('detail', 'DataDafnomController@detail');
        Route::post('delete', 'DataDafnomController@delete');
        Route::get('pdf_cetak/{data_billing}', 'DataDafnomController@pdf_cetak');
        Route::get('pdf/{data_billing}', 'DataDafnomController@pdf');
        Route::get('pdf/{data_billing}/{billing_kolektif}', 'DataDafnomController@pdf');
    });
    Route::group(['prefix' => 'daftar_nop', 'as' => 'daftar_nop.'], function () {
        Route::get('/', 'DaftarNopController@index');

        Route::get('/search', 'DaftarNopController@search');
    });
    Route::get('dafnom_realisasi', 'DafnomRealisasiController@index');
    route::get('kode-blok', function (Request $request) {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;

        $res = DB::connection('oracle_satutujuh')->table('dat_peta_znt')->select(db::raw('distinct kd_blok'))
            ->whereraw("kd_kecamatan='$kd_kecamatan' and kd_kelurahan='$kd_kelurahan'")
            ->orderby('kd_blok')
            ->pluck('kd_blok')->toArray();
        return $res;
    });

    route::get('desa', function (Request $request) {
        $user = Auth()->user()->unitKerja()->first();
        $kd_kecamatan = $request->kd_kecamatan ?? $user->kd_kecamatan;
        $kelurahan = Kelurahan::where('kd_kecamatan', $kd_kecamatan)->orderby('kd_kelurahan', 'asc');
        $role = Auth()->user()->roles()->pluck('name')->toarray();
        $inputdesa = 'desa';
        $resdes = array_filter($role, function ($item) use ($inputdesa) {
            if (stripos($item, $inputdesa) !== false) {
                return true;
            }
            return false;
        });
        if (count($resdes) > 0) {
            if ($user->kd_kelurahan) {
                $kelurahan = $kelurahan->where('kd_kelurahan', $user->kd_kelurahan);
            }
        }
        $kelurahan = $kelurahan->selectraw("nm_kelurahan nm_kelurahan, kd_kelurahan")->pluck('nm_kelurahan', 'kd_kelurahan')->toArray();
        return $kelurahan;
    });


    route::get('desa-blok-sppt', function (Request $request) {

        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $blok = DB::connection('oracle_satutujuh')->table('DAT_OBJEK_PAJAK')
            ->join('dat_op_bumi', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
            })->select(db::raw(" distinct           dat_objek_pajak.kd_blok"))
            ->whereraw("total_luas_bumi>0 and  jns_transaksi_op != '3'
            AND jns_bumi IN ('1', '2', '3') and dat_objek_pajak.kd_kecamatan ='" . $kd_kecamatan . "' and dat_objek_pajak.kd_kelurahan='" . $kd_kelurahan . "'")
            ->orderby("dat_objek_pajak.kd_blok")
            ->get();
        // return $blok;
        $arr = [];
        foreach ($blok as $blok) {
            $arr[$blok->kd_blok] = $blok->kd_blok;
        }
        return $arr;
    });

    route::get('desa-blok', function (Request $request) {
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $blok = DB::connection('oracle_satutujuh')->table('DAT_OBJEK_PAJAK')
            ->join('dat_op_bumi', function ($join) {
                $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                    ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                    ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                    ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                    ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
                    ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
                    ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
            })->select(db::raw(" distinct           dat_objek_pajak.kd_blok"))
            ->whereraw("total_luas_bumi>0 and  jns_transaksi_op != '3'
            AND jns_bumi IN ('1', '2', '3') and dat_objek_pajak.kd_kecamatan ='" . $kd_kecamatan . "' 
            and dat_objek_pajak.kd_kelurahan='" . $kd_kelurahan . "'")
            ->orderby("dat_objek_pajak.kd_blok")
            ->get();
        // return $blok;
        $arr = [];
        foreach ($blok as $blok) {
            $arr[$blok->kd_blok] = $blok->kd_blok;
        }
        return $arr;
    });

    route::get('cekfile', function (Request $request) {
        $filename = $request->file;
        if (!Storage::disk('reports')->exists($filename)) {
            return 0;
        } else {
            return 1;
        }
    });

    route::get('downloadfile/{id}', function ($filename) {
        if (!Storage::disk('reports')->exists($filename)) {
            abort(404, 'Dokumen tidak ditemukan');
        }
        $file = Storage::disk('reports')->get($filename);
        $type = Storage::disk('reports')->mimeType($filename);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    });

    Route::group(['prefix' => 'dhkp', 'as' => 'dhkp.'], function () {
        Route::get('ringkasan', 'DhkpController@ringkasanBaku')->middleware(['permission:dhkp.ringkasan.menu']);
        Route::get('ringkasan-excel', 'DhkpController@ringkasanBakuExcel');
        Route::get('ringkasan-pdf', 'DhkpController@ringkasanBakuPdf');
        Route::get('dokumen', 'DhkpController@dokumenDhkp')->middleware(['permission:dhkp.dokumen.menu']);
        Route::get('dokumen-download', 'DhkpController@dokumenDhkpDownload');
        Route::get('wepe', 'DhkpController@index')->middleware(['permission:dhkp.wepe.menu']);
        Route::get('wepe-excel', 'DhkpController@cetakExcel');
        Route::get('wepe-pdf', 'DhkpController@cetakPdf');
        Route::get('desa', 'DhkpController@rekapDesa')->middleware(['permission:dhkp.desa.menu']);
        Route::get('desa/cetak', 'DhkpController@rekapDesaCetak');
        Route::get('desa/cetak-pdf', 'DhkpController@rekapDesaCetakPdf');
        route::get('perbandingan', 'DhkpController@DesaPerbandingan')->middleware(['permission:dhkp.perbandingan.menu']);
        route::get('perbandingan-excel', 'DhkpController@DesaPerbandinganExcel');
        route::get('perbandingan-pdf', 'DhkpController@DesaPerbandinganPdf');
        Route::get('rekapbandingdesa', 'DhkpController@rekapDesaPerbandingan');
        Route::get('perbandingandesa-pdf', 'DhkpController@rekapDesaPerbanCetakPdf');
        Route::get('perbandingandesa/cetak', 'DhkpController@rekapDesaPerbanCetak');
        // rekapperbandingan
        route::get('rekapperbandingan', 'LaporanAnalisaController@rekapDhkPerbandingan')->name('rekap-perbandingan')->middleware(['permission:dhkp.rekapperbandingan.menu']);
        route::get('rekapperbandingan-excell', 'LaporanAnalisaController@rekapDhkPerbandinganExcell')->name('rekap-perbandingan-excel');
        route::get('cetak-dokumen', 'DhkpController@FormCetak')->name('cetak');
        route::post('cetak-dokumen', 'DhkpController@FormCetakRender')->name('cetak.proses');
        route::get('cetak-dokumen-excel', 'DhkpController@prosesExcel')->name('cetak.excel');
        Route::get('tagihan', 'TagihanController@indexDhkp')->name('tagihan.index');
        Route::get('tagihan-excel', 'TagihanController@indexDhkpExcel')->name('tagihan.excel');
        route::get('tunggakan-excel', 'TagihanController@excelTunggakan');
    });

    Route::group(['prefix' => 'tagihan-sppt', 'as' => 'tagihan-sppt.'], function () {
        route::get('/', 'TagihanSpptController@index')->name('index');
        route::post('register-pembayaran', 'TagihanSpptController@registerPembayaran')->name('register.pembayaran');
        route::get('history', 'TagihanSpptController@historyTagihan')->name('history');
    });
    Route::group(['prefix' => 'realisasi', 'as' => 'realisasi.'], function () {

        // kobil ->view_pembayaran_kobil
        Route::get('kobil', 'RealisasiController@pembayaranKobil')->middleware(['permission:realisasi.kobil.menu']);
        Route::get('kobil-excel', 'RealisasiController@pembayaranKobilExcel');
        Route::get('pembayaran', 'RealisasiController@pembayaranHarian')->middleware(['permission:realisasi.pembayaran.menu']);
        Route::get('pembayaran-excel', 'RealisasiController@excelPembayaranHarian');
        Route::get('pembayaran-pdf', 'RealisasiController@pdfPembayaranHarian');
        Route::get('targetapbd', 'RealisasiController@targetApbd')->middleware(['permission:realisasi.targetapbd.menu']);
        Route::get('targetapbd-excel', 'RealisasiController@ExceltargetApbd');
        Route::get('targetapbd-pdf', 'RealisasiController@PdftargetApbd');
        Route::get('baku', 'RealisasiController@realBaku')->middleware(['permission:realisasi.baku.menu']);
        Route::get('baku-buku', 'RealisasiController@realBakuBuku')->middleware(['permission:realisasi.baku.menu']);
        Route::get('baku-excel', 'RealisasiController@excelRealBaku');
        Route::get('baku-pdf', 'RealisasiController@pdfRealBaku');
        Route::get('harian', 'RealisasiController@realHarian')->middleware(['permission:realisasi.harian.menu']);
        Route::get('harian-excel', 'RealisasiController@excelRealHarian');
        Route::get('harian-pdf', 'RealisasiController@pdfRealHarian');
        Route::get('bulanan', 'RealisasiController@realBulanan')->middleware(['permission:realisasi.bulanan.menu']);
        Route::get('bulanan-excel', 'RealisasiController@excelRealBulanan');
        Route::get('bulanan-pdf', 'RealisasiController@pdfRealBulanan');
        Route::get('detailkecamatan', 'RealisasiController@realDetailKecamatan')->middleware(['permission:realisasi.detailkecamatan.menu']);
        Route::get('detailkecamatan-excel', 'RealisasiController@ExcelRealDetailKecamatan');
        Route::get('detailkecamatan-pdf', 'RealisasiController@pdfRealDetailKecamatan');
        Route::get('rangkingkecamatan', 'RealisasiController@realRangkingKecamatan')->middleware(['permission:realisasi.rangkingkecamatan.menu']);
        Route::get('rangkingkelurahan', 'RealisasiController@realRangkingKekelurahan');
        Route::get('rangkingkecamatan-excel', 'RealisasiController@ExcelrealRangkingKecamatan');
        Route::get('rangkingkelurahan-excel', 'RealisasiController@ExcelrealRangkingKelurahan');
        Route::get('rangkingkecamatan-pdf', 'RealisasiController@PdfrealRangkingKecamatan');
        Route::get('rangkingkelurahan-pdf', 'RealisasiController@PdfrealRangkingKelurahan');
        Route::get('kecamatanlunas-pdf', 'kecamatanLunasController@pdf');
        Route::get('kecamatanlunas-excel', 'kecamatanLunasController@excel');
        Route::resource('kecamatanlunas', 'kecamatanLunasController')->middleware(['permission:realisasi.kecamatanlunas.menu']);
        Route::get('desalunas-pdf', 'DesaLunasController@pdf');
        Route::get('desalunas-excel', 'DesaLunasController@excel');
        Route::get('desalunas-delete/{id}', 'DesaLunasController@destroy');
        Route::resource('desalunas', 'DesaLunasController')->except(['destroy'])->middleware(['permission:realisasi.desalunas.menu']);
        Route::resource('targetrealisasi', 'TargetRealisasiController');
        Route::post('rekon-upload', 'RekonsiliasiController@previewUpload');
        Route::resource('rekon', 'RekonsiliasiController');
        Route::resource('unflag', 'UnflagController')->only(['index', 'store'])->middleware(['permission:realisasi.unflag.menu']);
        Route::get('tunggakan', 'RealisasiController@realTunggakan')->middleware(['permission:realisasi.tunggakan.menu']);
        Route::get('tunggakan-buku', 'RealisasiController@realTunggakanBukuExcel')->middleware(['permission:realisasi.tunggakan.menu']);
        Route::get('tunggakan-excel', 'RealisasiController@realTunggakanExcel');
        // Route::get('tunggakan-pdf', 'RealisasiController@realTunggakanPdf');
        Route::get('rekap-harian-pembayaran', 'RealisasiController@rekapHarianPembayaran');
        Route::get('rekap-harian-pembayaran-excel', 'RealisasiController@rekapHarianPembayaranExcel');
    });

    Route::group(['prefix' => 'kompensasi_lebih_bayar', 'as' => 'kompensasi_lebih_bayar.'], function () {
        Route::get('/', 'KompensasiBayarController@create');
        Route::get('/daftar', 'KompensasiBayarController@index');
        Route::get('/search', 'KompensasiBayarController@search');
        Route::post('/store', 'KompensasiBayarController@store');
        Route::get('/verifikasi', 'KompensasiBayarController@verifikasi');
        Route::get('/ceknop', 'KompensasiBayarController@ceknop');
        Route::get('/sk', 'KompensasiBayarController@sk');
        Route::get('/cetak', 'KompensasiBayarController@cetak');
        Route::get('/total', 'KompensasiBayarController@total');
    });
    Route::group(['prefix' => 'informasi', 'as' => 'informasi.'], function () {
        Route::get('objek-pajak', 'objekPajakController@index')->middleware(['wajibPajakVerification']);
        Route::get('riwayat-pembayaran', 'objekPajakController@riwayatPembayaran');
        Route::get('riwayat-pembayaran-cetak', 'objekPajakController@riwayatPembayaranCetak');
        Route::get('objek-pajak-show', 'objekPajakController@show');
        Route::get('objek-pajak-detail', 'objekPajakController@detailObjek');
        Route::get('objek-pajak-list', 'objekPajakController@showList');
        Route::get('objek-pajak-list-data', 'objekPajakController@showListDatatables');
        // mutasi pecah
        Route::get('mutasi-pecah', 'objekPajakController@mutasiPecah')->name('mutasi-pecah.index');
        Route::get('mutasi-pecah-pdf', 'objekPajakController@mutasiPecahPdf')->name('mutasi-pecah.pdf');
        Route::get('objek-pajak-njop-cetak', 'objekPajakController@cetakNjop');
        Route::get('ringkasan', 'objekPajakController@ringkasanObjek');
        Route::get('sppt', 'SpptController@index');
        Route::resource('riwayat', 'TrackingPembayaranController')->middleware(['permission:informasi.riwayat.menu', 'wajibPajakVerification']);
        // bphtb
        Route::get('bphtb', 'bphtbController@index');
        Route::get('bphtb-detail', 'bphtbController@detailNop');
        Route::get('bphtb/{kd_kecamatan}/{tahun}', 'bphtbController@show')->name('bphtb.show');
        Route::get('bphtb-excel/{kd_kecamatan}/{tahun}', 'bphtbController@getExcel')->name('bphtb.excel');
        route::get('piutang', 'piutangController@index')->middleware(['permission:informasi.piutang.menu']);
        route::get('rekap-piutang', 'piutangController@rekap');
        route::get('piutang-excel', function (Request $request) {
            $kd_kecamatan = $request->kd_kecamatan;
            $kd_kelurahan = $request->kd_kelurahan;
            $thn_pajak_sppt = $request->thn_pajak_sppt;
            $status_objek = $request->status_objek;
            $data = Piutang::PerNop($kd_kecamatan, $kd_kelurahan, $thn_pajak_sppt, $status_objek)->get();
            $filename = "piutang " . $thn_pajak_sppt . " - ";
            if ($kd_kecamatan <> '') {
                $filename .= $kd_kecamatan;
            }
            if ($kd_kelurahan <> '') {
                $filename .= $kd_kelurahan;
            }
            return Excel::download(new DataExport($data), $filename . '.xlsx');
        });
    });

    Route::get('change_password/{id}', 'UserController@edit_password')->name('users.change_password');
    Route::post('update_password/{id}', 'UserController@update_password')->name('users.update_password');
    Route::get('monitor-queue', function () {
        // return view('monitor-queue');
        return redirect(url('jobs'));
    });
    route::group(['prefix' => 'penagihan_form', 'as' => 'penagihan_form.'], function () {
        route::get('/', 'PenagihanController@index');
    });
    route::group(['prefix' => 'penagihan_daftar', 'as' => 'penagihan_daftar.'], function () {
        route::get('/', 'PenagihanDataController@index');
        route::post('penagihansearch', 'PenagihanDataController@PenagihanSearch')->name('penagihansearch');
        route::post('penagihandetailsearch/{id}', 'PenagihanDataController@PenagihanDetailSearch')->name('penagihandetailsearch');
        route::get('detail/{id}', 'PenagihanDataController@PenagihanDetail')->name('detail');
    });
    route::group(['prefix' => 'realisasi_penagihan_piutang', 'as' => 'realisasi_penagihan_piutang.'], function () {
        route::get('/', 'RealisasiPenagihanController@index');
        route::get('search', 'RealisasiPenagihanController@search')->name('search');
        route::get('detail/{id}', 'RealisasiPenagihanController@detail')->name('detail');
        route::get('detail_kelurahan/{id}', 'RealisasiPenagihanController@detailkelurahan')->name('detailkelurahan');
        route::get('searchdetail/{id}', 'RealisasiPenagihanController@searchdetail')->name('searchdetail');
        route::get('searchdetail_kelurahan/{id}', 'RealisasiPenagihanController@searchdetailkelurahan')->name('searchdetailkelurahan');
    });
    Route::group(['prefix' => 'verifikasi_layanan', 'as' => 'verifikasi_layanan.'], function () {
        Route::get('/', 'VerifikasiLayananController@index');
        Route::get('search', 'VerifikasiLayananController@search')->name('search');
    });
    Route::group(['prefix' => 'pembetulan', 'as' => 'pembetulan.'], function () {
        Route::get('/', 'LayananRegistrasiController@pembetulan')->middleware(['OperasionalLayanan', 'permission:pembetulan.menu']);
        Route::get('/badan', 'LayananRegistrasiController@pembetulanBadan')->middleware(['OperasionalLayanan', 'permission:pembetulan.menu']);
        Route::post('store_pembetulan', 'LayananRegistrasiController@storepembetulan')->middleware(['OperasionalLayanan']);
    });
    Route::group(['prefix' => 'permohonan', 'as' => 'permohonan.'], function () {
        Route::get('entry_user', 'LaporanPermohonan@user')->middleware(['permission:permohonan.entry_user.menu']);
        Route::get('entry_kelurahan', 'LaporanPermohonan@kelurahan')->middleware(['permission:permohonan.entry_kelurahan.menu']);
        Route::get('entry_bulan', 'LaporanPermohonan@bulan')->middleware(['permission:permohonan.entry_bulan.menu']);
        Route::get('entry_user_search', 'LaporanPermohonan@user_search');
        Route::get('entry_kelurahan_search', 'LaporanPermohonan@kelurahan_search');
        Route::get('entry_bulan_search', 'LaporanPermohonan@bulan_search');
        Route::get('user_cetak', 'LaporanPermohonan@user_cetak');
        Route::get('kelurahan_cetak', 'LaporanPermohonan@kelurahan_cetak');
        Route::get('bulan_cetak', 'LaporanPermohonan@bulan_cetak');
        Route::get('hapus_permohonan', 'LaporanPermohonan@hapus_permohonan');
        Route::get('hapus_permohonan_search', 'LaporanPermohonan@hapus_permohonan_search');
        Route::get('hapus_permohonan_cetak', 'LaporanPermohonan@hapus_permohonan_cetak');
        Route::get('induk_pecah_minus', 'LaporanPermohonan@induk_pecah_minus')->middleware(['permission:permohonan.induk_pecah_minus.menu']);
        Route::get('induk_pecah_minus_search', 'LaporanPermohonan@induk_pecah_minus_search');
        Route::get('induk_pecah_minus_cetak', 'LaporanPermohonan@induk_pecah_minus_cetak');
        Route::get('daftar_pembetulan', 'LaporanPermohonan@daftar_pembetulan')->middleware(['permission:permohonan.daftar_pembetulan.menu']);
        Route::get('daftar_pembetulan_search', 'LaporanPermohonan@daftar_pembetulan_search');
        Route::get('daftar_pembetulan_cetak', 'LaporanPermohonan@daftar_pembetulan_cetak');
    });
    Route::group(['prefix' => 'laporan_pendataan', 'as' => 'laporan_pendataan.'], function () {
        route::get('/', 'LaporanAnalisaController@laporan_pendataan')->name('laporan_pendataan');
        route::get('/search', 'LaporanAnalisaController@laporan_pendataan_search')->name('laporan_pendataan_search');
        route::get('/cetak', 'LaporanAnalisaController@laporan_pendataan_cetak')->name('laporan_pendataan_cetak');
    });
    Route::group(['prefix' => 'analisa', 'as' => 'analisa.'], function () {
        Route::get('nop_non_aktif', 'LaporanAnalisaController@nop_non_aktif');
        Route::get('nop-reaktif', 'LaporanAnalisaController@NopReaktif');
        Route::post('nop_non_aktif', 'LaporanAnalisaController@pengaktifanStore');
        Route::post('nop_non_aktif/load-tunggakan', 'LaporanAnalisaController@loadTunggakan');
        Route::get('nop_non_aktif_search', 'LaporanAnalisaController@nop_non_aktif_search');
        Route::get('nop_non_aktif_cetak', 'LaporanAnalisaController@nop_non_aktif_cetak');
        Route::get('pengaktifan_nop', 'LaporanAnalisaController@pengaktifan_nop');
        Route::get('perbandingan_njop', 'LaporanAnalisaController@perbandingan_njop');
        Route::get('perbandingan_njop_search', 'LaporanAnalisaController@perbandingan_njop_search');
        Route::get('perbandingan_njop_cetak', 'LaporanAnalisaController@perbandingan_njop_cetak');
        Route::get('njop-tahun', 'LaporanAnalisaController@NjopTahun');
        Route::get('njop-tahun-excel', 'LaporanAnalisaController@NjopTahunExcel');
        Route::get('blok', 'LaporanAnalisaController@blok');
        Route::get('blok_search', 'LaporanAnalisaController@blok_search');
        Route::get('blok_cetak', 'LaporanAnalisaController@blok_cetak');
        Route::get('blok_list_group', 'LaporanAnalisaController@blok_list_group');
        Route::get('dhr', 'LaporanAnalisaController@dhr');
        Route::get('dhr_search', 'LaporanAnalisaController@dhr_search');
        Route::get('dhr_cetak', 'LaporanAnalisaController@dhr_cetak');
        Route::get('dhk', 'LaporanAnalisaController@dhk');
        Route::get('dhk_search', 'LaporanAnalisaController@dhk_search');
        Route::get('dhk_cetak', 'LaporanAnalisaController@dhr_cetak_min');
        Route::get('dhk_cetak_excell', 'LaporanAnalisaController@dhr_cetak_excell');
        Route::get('pci', 'LaporanAnalisaController@pci');
        Route::get('pci_search', 'LaporanAnalisaController@pci_search');
        Route::get('pci_cetak', 'LaporanAnalisaController@pci_cetak');
        Route::get('menaikan-kelas', 'LaporanAnalisaController@menaikanKelas');
        Route::get('menaikan-kelas-excell', 'LaporanAnalisaController@menaikanKelasExcel');

        Route::group(['prefix' => 'dhkp', 'as' => 'analisa.'], function () {
            route::get('rekap-all', 'LaporanAnalisaController@rekapDhkpAll')->name('rekap-all');
            route::get('rekap-all-excell', 'LaporanAnalisaController@rekapDhkpAllExcell')->name('rekap-all-excel');
        });
    });
    Route::group(['prefix' => 'laporan_penelitian', 'as' => 'laporan_penelitian.'], function () {
        Route::get('user', 'LaporanPenelitian@user')->middleware(['permission:laporan_penelitian.user.menu']);
        Route::get('kelurahan', 'LaporanPenelitian@kelurahan')->middleware(['permission:laporan_penelitian.kelurahan.menu']);
        Route::get('bulan', 'LaporanPenelitian@bulan')->middleware(['permission:laporan_penelitian.bulan.menu']);
        Route::get('user_search', 'LaporanPenelitian@user_search');
        Route::get('kelurahan_search', 'LaporanPenelitian@kelurahan_search');
        Route::get('bulan_search', 'LaporanPenelitian@bulan_search');
        Route::get('user_cetak', 'LaporanPenelitian@user_cetak');
        Route::get('kelurahan_cetak', 'LaporanPenelitian@kelurahan_cetak');
        Route::get('bulan_cetak', 'LaporanPenelitian@bulan_cetak');
        Route::get('bulan_total', 'LaporanPenelitian@bulan_total');
        Route::get('nota_perhitungan', 'LaporanPenelitian@nota_perhitungan');
        Route::get('belum_bayar', 'LaporanPenelitian@belum_bayar')->middleware(['permission:laporan_penelitian.belum_bayar.menu']);
        Route::get('sudah_bayar', 'LaporanPenelitian@sudah_bayar')->middleware(['permission:laporan_penelitian.sudah_bayar.menu']);
        Route::get('nota_perhitungan_search', 'LaporanPenelitian@nota_perhitungan_search');
        Route::get('nota_perhitungan_cetak', 'LaporanPenelitian@nota_perhitungan_cetak');
        Route::get('penolakan', 'LaporanPenelitian@penolakan')->middleware(['permission:laporan_penelitian.penolakan.menu']);
        Route::get('penolakan_search', 'LaporanPenelitian@penolakan_search');
        Route::get('penolakan_cetak', 'LaporanPenelitian@penolakan_cetak');
        Route::get('permohonan_tanpa_nota', 'LaporanPenelitian@permohonan_tanpa_nota')->middleware(['permission:laporan_penelitian.permohonan_tanpa_nota.menu']);
        Route::get('permohonan_tanpa_nota_search', 'LaporanPenelitian@permohonan_tanpa_nota_search');
        Route::get('permohonan_tanpa_nota_cetak', 'LaporanPenelitian@permohonan_tanpa_nota_cetak');
    });
    Route::group(['prefix' => 'kolektif_online', 'as' => 'kolektif_online.'], function () {
        Route::group(['middleware' => ['OperasionalLayanan']], function () {
            Route::get('/', 'LayananRegistrasiController@kolektif_online')->middleware(['permission:kolektif_online.menu']);
            Route::post('/', 'LayananRegistrasiController@storeKolektif');
            Route::get('delete', 'LayananRegistrasiController@kolektif_delete');
            Route::get('edit', 'LayananRegistrasiController@kolektif_edit');
            Route::get('refresh', 'LayananRegistrasiController@kolektif_refresh');
            Route::post('store', 'LayananRegistrasiController@store_kolektif_temp');
            Route::post('update', 'LayananRegistrasiController@update_kolektif_temp');
            Route::get('search', 'LayananRegistrasiController@search_kolektif_temp');
            Route::get('ceknop_tambah', 'LayananRegistrasiController@ceknoptambah');
        });
        Route::get('updateberkas', 'LayananRegistrasiController@updateBerkasKolektif');
        Route::get('tolakberkas', 'LayananRegistrasiController@tolakBerkasKolektif');
    });
    Route::group(['prefix' => 'mutasi_gabung', 'as' => 'mutasi_gabung.'], function () {
        Route::get('/', 'LayananRegistrasiController@mutasigabung')->middleware(['OperasionalLayanan', 'permission:mutasi_gabung.menu']);
        Route::get('ceknop_tambah', 'LayananRegistrasiController@ceknoptambah')->middleware(['OperasionalLayanan']);
        Route::post('store_mutasi_gabung', 'LayananRegistrasiController@storemutasigabung')->middleware(['OperasionalLayanan']);
    });
    Route::group(['prefix' => 'mutasi_pecah', 'as' => 'mutasi_pecah.'], function () {
        Route::get('/', 'LayananRegistrasiController@mutasipecah')->middleware(['OperasionalLayanan', 'permission:mutasi_pecah.menu']);
        Route::get('ceknop_tambah', 'LayananRegistrasiController@ceknoptambah')->middleware(['OperasionalLayanan']);
        Route::post('store_mutasi_pecah', 'LayananRegistrasiController@storemutasipecah')->middleware(['OperasionalLayanan']);
    });
    Route::group(['prefix' => 'layanan', 'as' => 'layanan.'], function () {
        Route::get('/', 'LayananRegistrasiController@index')
            ->middleware(['OperasionalLayanan', 'permission:layanan.menu']);
        Route::get('detail', 'LayananRegistrasiController@detail');
        Route::post('store', 'LayananRegistrasiController@store');
        Route::post('store_kolektif', 'LayananRegistrasiController@storeKolektif');
        Route::post('preview', 'LayananRegistrasiController@preview');

        // PermohonanController
        route::get('pembetulan-online', 'PermohonanController@ajuanOnline')->middleware(['OperasionalLayanan', 'permission:layanan.pembetulan-online.menu']);
        route::get('pembetulan-online/{id}', 'PermohonanController@ajuanOnlineVerifikasi');
        route::patch('pembetulan-online/{id}', 'PermohonanController@storeajuanOnlineVerifikasi');
        route::get('pembetulan-online/{id}/reject', 'PermohonanController@ajuanOnlineVerifikasiReject');

        // store dokumen pendukung
        route::post('simpan-lampiran', 'PermohonanController@saveLampiran')->name('simpan-lampiran.post');
        route::get('hapus-lampiran', 'PermohonanController@hapusLampiran')->name('hapus-lampiran.get');
    });
    Route::group(['prefix' => 'laporan_input', 'as' => 'laporan.'], function () {
        Route::get('/', 'LaporanInputController@index')->middleware(['permission:laporan_input.menu']);
        Route::get('search', 'LaporanInputController@search');
        Route::get('export', 'LaporanInputController@export');
        Route::get('detail', 'LaporanInputController@detail');
        Route::get('berkas', 'LaporanInputController@berkas');
        Route::get('tanda_terima', 'LaporanInputController@tanda_terima');
        Route::get('cetak_berkas_kolektif', 'LaporanInputController@cetak_berkas_kolektif');
        Route::get('cetak_pdf', 'LaporanInputController@cetak_pdf');
        Route::get('cetak_sk', 'LaporanInputController@cetak_sk');
        Route::get('delete', 'LaporanInputController@destroy');
    });
    route::group(['prefix' => 'rekap_jenis_layanan', 'as' => 'rekap_jenis_layanan.'], function () {
        route::get('/', 'RekapJenisLayanan@index');
        Route::get('search', 'RekapJenisLayanan@search');
        Route::get('detail', 'RekapJenisLayanan@detail');
    });

    Route::group(['prefix' => 'refrensi', 'as' => 'refrensi.'], function () {
        Route::resource('tempat-bayar', 'TempatBayarController');
        Route::resource('unitkerja', 'unitKerjaController');
        Route::resource('struktural', 'StrukturalController');
        Route::resource('kelompokobjek', 'refKelompokObjekController');
        Route::resource('lokasiobjek', 'reflokasiObjekController');
        Route::get('jatuhtempo', 'konfigurasiController@jatuhTempo')->name('jatuh.tempo');
        Route::post('jatuhtempo', 'konfigurasiController@jatuhTempoProses')->name('jatuh.tempo-proses');
        Route::post('jatuhtempo-preview', 'konfigurasiController@previewUpload')->name('jatuh.tempo-preview');
        Route::post('jatuhtempo-masal', 'konfigurasiController@UploadMasal')->name('jatuh.tempo-masal');
        // Formulir
        // nomorFormulirController
        Route::resource('formulir', 'nomorFormulirController')->middleware(["can:nomor formulir"]);
        Route::resource('jenispengurangan', 'JenisPenguranganController');
        Route::get('data_nir_search', 'DataNirController@search');
        route::get('data_nir/template-import', 'DataNirController@templateExcel')->name('data_nir.upload-template');
        Route::post('data_nir/preview', 'DataNirController@uploadPreview')->name('data_nir.upload-preview');
        Route::get('data_nir/upload', 'DataNirController@upload')->name('data_nir.upload');
        Route::get('data_nir_search', 'DataNirController@search');
        route::resource('data_nir', 'DataNirController');
        route::get('data_nir_cetak', 'DataNirController@cetak');
        Route::get('tarif', 'TarifController@index');
        Route::get('tarif_search', 'TarifController@tarif_search');
        Route::post('tarif_create', 'TarifController@tarif_store');
        Route::get('tarif_delete', 'TarifController@destroy');
        Route::get('pbb_minimal', 'PBBMinimalController@index');
        Route::get('pbb_minimal_search', 'PBBMinimalController@tarif_search');
        Route::post('pbb_minimal_create', 'PBBMinimalController@tarif_store');
        Route::get('pbb_minimal_delete', 'PBBMinimalController@destroy');
        Route::resource('pemutihan', 'PemutihanPajakController');
        Route::resource('znt', 'MasterZntController')->only(['index', 'show']);
        route::post('znt-post', 'MasterZntController@store')->name('znt.simpan');
        route::post('znt-update', 'MasterZntController@update')->name('znt.update');
        route::post('znt-post-nir', 'MasterZntController@storeNir')->name('znt.simpan-nir');
        route::post('znt-post-blok', 'MasterZntController@storeBlok')->name('znt.simpan-blok');
        route::get('znt-delete/{id}', 'MasterZntController@destroy')->name('znt.hapus');
        route::get('znt-nir-delete/{id}', 'MasterZntController@destroyNir')->name('znt.hapus-nir');
        route::get('znt-blok-delete/{id}', 'MasterZntController@destroyBlok')->name('znt.hapus-blok');
        Route::resource('blok-peta', 'petaBLokController')->except(['create', 'destroy', 'show']);
        route::get('blok-blok-delete/{id}', 'petaBLokController@destroy')->name('blok-peta.hapus');
        Route::resource('tutup-layanan', 'TutupLayananController');
        route::resource('camat', 'RefCamatController')->except(['destroy', 'create', 'edit', 'show', 'update']);
        route::get('camat-hapus/{id}', 'RefCamatController@destroy')->name('camat.destroy');
        route::post('camat-update', 'RefCamatController@update')->name('camat.update');
        route::resource('lurah', 'RefLurahController')->except(['destroy', 'create', 'edit', 'show', 'update']);
        route::get('lurah-hapus/{id}', 'RefLurahController@destroy')->name('lurah.destroy');
        route::post('lurah-update', 'RefLurahController@update')->name('lurah.update');
    });


    Route::post('sknjop-kolektif', 'sknjopController@storeKolektif');
    Route::get('sknjop-cetak/{id}', 'sknjopController@previewCetak');
    Route::get('sknjop-tanda-terima/{id}', 'sknjopController@tandaterima');
    Route::get('sknjop-tanda-cetak/{id}', 'sknjopController@tandaterimaPreview');
    Route::get('sknjop-render-batch/{id}', 'sknjopController@batchRender');
    Route::get('sknjop-cetak-batch/{id}', 'sknjopController@batchPreview');
    Route::resource('sknjop', 'sknjopController')->except(['edit', 'update'])->middleware(['permission:sknjop.menu']);
    Route::resource('sknjop-verifikasi', 'SknjopVerifikasiController')->except(['create', 'store', 'destroy']);
    // PenetapanObjekController
    route::resource('penetapan', 'PenetapanObjekController')->only(['index', 'create', 'store']);
    route::get('cetak-sppt', 'SpptController@FormCetak')->name('cetak.index');
    route::post('cetak-sppt', 'SpptController@generateSppt')->name('cetak.generate');
    // penelitian
    Route::group(['prefix' => 'penelitian', 'as' => 'penelitian.'], function () {
        route::get('penolakan', 'penelitianKantorController@ditolak');
        route::get('sk-penolakan/{id}', 'penelitianKantorController@ditolakSk');
        route::get('hasil-penelitian', 'LhpController@index')->middleware(['permission:penelitian.hasil-penelitian.menu']);
        route::get('hasil-penelitian-cancel/{id}', 'LhpController@cancelPenelitian')->name('hasil.cancel');
        route::get('kantor-hasil-penelitian/{id}', 'penelitianKantorController@hasilPenelitian');
        route::get('lapangan-hasil-penelitian/{id}', 'penelitianLapanganController@hasilPenelitian');
        route::get('nota-perhitungan', 'LhpController@notaPerhitungan')->middleware(['permission:penelitian.nota-perhitungan.menu']);
        route::get('nota-perhitungan-print', 'LhpController@notaPerhitunganprint');
        route::get('nota-perhitungan-excel', 'LhpController@notaPerhitunganExcel');
        route::get('nota-perhitungan-hapus/{id}', 'LhpController@notaPerhitunganhapus');
        route::get('kantor-switch/{id}', 'penelitianKantorController@switchPenelitian');
        route::get('lapangan-switch/{id}', 'penelitianLapanganController@switchPenelitian');
        route::post('kantor-pecah', 'penelitianKantorController@savePecah');
        route::get('kantor-pecah-proses', 'penelitianKantorController@savePecahFinal');
        route::get('kantor-pecah-list', 'penelitianKantorController@dataPecah');
        route::post('kantor-tolak/{id}', 'penelitianKantorController@SimpanTolak')->name('kantor.tolak');
        route::resource('kantor', 'penelitianKantorController')->middleware(['permission:penelitian.kantor.menu']);
        route::resource('lapangan', 'penelitianLapanganController')->middleware(['permission:penelitian.lapangan.menu']);
        route::get('objek-penelitian', 'penelitianLapanganController@listObjekPenelitian');
        route::get('cetak-spop/{formulir}', 'spopController@cetakSpop');
        route::get('sk', 'SuratKeputusanController@index')->middleware(['permission:penelitian.sk.menu']);
        route::get('sk_cetak', 'SuratKeputusanController@cetak');
    });
    route::get('besembilan', 'besembilanController@index')->middleware(['permission:besembilan.menu']);
    route::get('pelimpahan', 'besembilanController@indexPelimpahan');
    Route::group(['prefix' => 'dbkb', 'as' => 'dbkb.'], function () {
        Route::resource('harga-fasilitas', 'HrgFasilitasController')->only(['index', 'create', 'store']);
        Route::get('harga-fasilitas-export', 'HrgFasilitasController@exportPdf')->name('harga-fasilitas.export');
        Route::resource('daya-dukung', 'DbkbDayaDukungController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-dua', 'DbkbJpbDuaController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-tiga', 'DbkbJpbTigaController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-empat', 'DbkbJpbEmpatController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-lima', 'DbkbJpbLimaController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-enam', 'DbkbJpbEnamController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-tujuh', 'DbkbJpbTujuhController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-delapan', 'DbkbJpbDelapanController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-sembilan', 'DbkbJpbSembilanController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-dua-belas', 'DbkbJpbDuaBelasController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-tiga-belas', 'DbkbJpbTigaBelasController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-empat-belas', 'DbkbJpbEmpatBelasController')->only(['index', 'create', 'store', 'edit', 'update']);
        Route::resource('jepebe-lima-belas', 'DbkbJpbLimaBelasController')->only(['index', 'create', 'store']);
        Route::resource('jepebe-enam-belas', 'DbkbJpbEnamBelasController')->only(['index', 'create', 'store']);
        route::resource('mezanine', 'dbkbMezanineController');
    });

    Route::group(['prefix' => 'ssh', 'as' => 'ssh.'], function () {
        Route::get('harga-resource-cetak/{tahun}/{tipe}', 'HrgResourceController@cetak')->name('harga-resource.cetak');
        Route::resource('harga-resource', 'HrgResourceController')->only(['index', 'create', 'store']);
        Route::resource('harga-kegiatan', 'HrgKegiatanController')->only(['index', 'create', 'store']);
        Route::get('harga-kegiatan.export', 'HrgKegiatanController@exportPdf')->name('harga-kegiatan.export');
    });
    Route::group(['prefix' => 'potongan', 'as' => 'potongan.'], function () {
        route::get('/', 'MsPotonganController@listPotongan')->name('index');
        route::get('/excel', 'MsPotonganController@listPotonganExcel')->name('excel');
        route::get('/rekap', 'MsPotonganController@RekapPotongan')->name('rekap');
    });

    route::resource('kelola-nop', 'KelolaNopController')->middleware('wajibPajakVerification');
    // route::get('kelola-nop-verifikasi/{id}', 'KelolaNopController@verifikasi');
    // route::post('kelola-nop-verifikasi/{id}', 'KelolaNopController@verifikasiStore');
    route::post('kelola-nop-preview', 'KelolaNopController@previewNop');
    route::resource('kelola-nop-verifikasi', 'KelolaNopVerifikasiController')->only(['index', 'edit', 'update']);
    route::resource('kelola-objek-verifikasi', 'KelolaObjekVerifikasi');


    route::post('permohonan-pengelola-objek/{id}', 'KelolaNopController@storePermohonanPengelola');

    route::resource('kelola-developer', 'kelolaDeveloperController');
    // route::resource('kelola-developer-verifikasi', 'KelolaDeveloperVerifikasiController')->only(['index', 'edit', 'update']);
    route::resource('kelola-rayon', 'KelolaRayonController');
});


route::get('tes', function (Request $request) {
    $url = 'http://localhost/simpbb/esppt?nop=3507260001017053902023';
    return view("iframe", compact('url'));
});

Route::get('esppt', function (Request $request) {
    $nop = $request->nop;
    return Esppt::generate($nop);
});

Route::get('/qrsppt', function (Request $request) {
    $nop = $request->nop;
    $url = url('esppt') . '?nop=' . $nop;
    return  \QrCode::size(200)
        ->style('square')
        ->generate($url);
});



route::get('penelitian/nota-perhitungan-pdf/{id}', 'LhpController@notaPerhitunganPdf');
Route::get('menus', 'MenuController@index');
Route::get('menus-show', 'MenuController@show');
Route::post('menus', 'MenuController@store')->name('menus.store');
Route::patch('menus-update/{id}', 'MenuController@updateFunction')->name('menus.update');
Route::get('menu-hapus/{id}', 'MenuController@hapus');
Route::get('menus/create', 'MenuController@create')->name('menus.create');
Route::get('menus/{id}', 'MenuController@edit')->name('menus.edit');
route::get('sk/{ide}', 'SuratKeputusanController@cetakSK');
route::get('sk-tte', 'SuratKeputusanController@ContohSkTte');


Route::group(['middleware' => ['auth', 'isOn']], function () {

    // pembayaran-qris
    route::resource('pembayaran-qris', 'QrisController')->only(['index', 'show']);
    route::get('pembayaran-qris-cetak', 'QrisController@cetak');
    // pembayaran-va
    route::resource('pembayaran-va', 'VirtualController')->only(['index', 'show']);
    route::get('pembayaran-va-cetak', 'VirtualController@cetak');
    // pembayaran-va.show
    // route::get('pembayaran-va/')


    // pembayaran-va.show

    Route::get('pembayaran-blokir-nop', 'LaporanAnalisaController@pembayaranblokirnop')->name('pembayaran.blokir.nop');
    Route::group(['prefix' => 'pembayaran-blokir', 'as' => 'pembayaran.blokir.'], function () {
        Route::get('verifikasi', 'LaporanAnalisaController@pembayaranblokirverifikasi')->name('verifikasi');
        Route::get('verifikasi/{trx}', 'LaporanAnalisaController@pembayaranblokirverifikasiForm')->name('verifikasi-form');
        Route::post('verifikasi/{trx}', 'LaporanAnalisaController@pembayaranblokirverifikasiFormStore')->name('verifikasi-form.store');
        Route::post('upload', 'LaporanAnalisaController@blokirUpload')->name('upload');
        Route::post('buka-blokir', 'LaporanAnalisaController@bukaBlokir')->name('buka');
        Route::post('buka-blokir-trx', 'LaporanAnalisaController@bukaBlokirMasal')->name('bukamasal');
        Route::get('pdf/{trx}', 'LaporanAnalisaController@pembayaranTerblokirPdf')->name('pdf');
        Route::get('/', 'LaporanAnalisaController@pembayaran_terblokir')->name('index');
        // pendataan/penangguhan?tab=Koreksi%20Piutang
        Route::get('/{trx}', 'LaporanAnalisaController@pembayaranTerblokirDetail')->name('show');
        Route::post('/', 'LaporanAnalisaController@pembayaran_terblokir_store')->name('store');
    });

    Route::group(['prefix' => 'pendataan', 'as' => 'pendataan.'], function () {
        route::get('/', 'PendataanController@index')->name('index');
        route::get('/cetak', 'PendataanController@cetak_listpendataan')->name('cetak_listpendataan');
        route::get('/{id}/edit', 'PendataanController@edit')->name('edit');
        route::post('/edit/{id}', 'PendataanController@update')->name('update');
        route::get('/hapus-pendataan/{id}', 'PendataanController@destroy')->name('delete');
        route::get('/show/{id}', 'PendataanController@show')->name('show');
        route::get('/show-text/{id}', 'PendataanController@showModal')->name('show-modal');
        route::get('/create', 'PendataanController@create')->name('create');
        route::post('/', 'PendataanController@store')->name('store');
        route::get('/import', 'PendataanController@formImport')->name('import');
        route::post('/import-preview', 'PendataanController@previewImport')->name('import.preview');
        route::post('/import-store', 'PendataanController@storeImport')->name('import.store');
        route::get('/cetak-draft-pdf/{id}', 'PendataanController@draftPdf')->name('draft.pdf');
        route::get('/verifikasi', 'PendataanController@indexVerifikasi')->name('verifikasi');
        route::get('/verifikasi-form/{id}', 'PendataanController@showVerifikasi')->name('verifikasi.form');
        route::post('/verifikasi-simpan', 'PendataanController@storeVerifikasi')->name('verifikasi.store');
        route::get('/verifikasi-peta', 'PendataanController@indexVerifikasiPeta')->name('verifikasi.peta');
        route::get('/verifikasi-peta-form/{id}', 'PendataanController@showVerifikasiPeta')->name('verifikasi.peta.form');
        route::post('/verifikasi-peta-simpan', 'PendataanController@storeVerifikasiPeta')->name('verifikasi.peta.store');
        route::get('pembatalan-sistep', 'PendataanController@formPembatalanSistep');
        route::post('pembatalan-sistep', 'PendataanController@storePembatalanSistep');
        Route::get('penangguhan', 'PenangguhanNopController@index');
        Route::get('penangguhan-form-iventarisasi', 'PenangguhanNopController@formIventarisasi');
        Route::get('penangguhan-upload-iventarisasi', 'PenangguhanNopController@formIventarisasiUpload');
        Route::get('penangguhan-upload-template', 'PenangguhanNopController@TemplateIventarisasiUpload');
        Route::post('penangguhan-upload-iventarisasi', 'PenangguhanNopController@previewIventarisasiUpload');
        route::post('penangguhan-upload-iventarisasi-store', 'PenangguhanNopController@storeIventarisasiUpload');
        // 
        Route::get('penangguhan-form-koreksi', 'PenangguhanNopController@formKoreksi');
        Route::post('penangguhan-preview-koreksi', 'PenangguhanNopController@previewUploadkoreksi');
        Route::get('penangguhan-upload-koreksi', 'PenangguhanNopController@formUploadKoreksi');
        Route::post('penangguhan-koreksi', 'PenangguhanNopController@storeKoreksi');
        Route::get('penangguhan-list-tunggakan', 'PenangguhanNopController@getTunggakan');
        Route::get('penangguhan-iventarisasi', 'PenangguhanNopController@indexDataIventarisasi');
        Route::get('penangguhaniventarisasiexport', 'PenangguhanNopController@indexDataIventarisasiExport');
        Route::post('penangguhan/data', 'PenangguhanNopController@getDataAll');
        Route::get('penangguhan_nop', 'PenangguhanNopController@penangguhanNop');
        Route::post('penangguhan_nop', 'PenangguhanNopController@penonaktifan');
        Route::post('penangguhan-cek-nop-kp', 'PenangguhanNopController@cekNopKp');
        // PerubahanZntController
        Route::resource('merubah-znt', 'PerubahanZntController')->except(['destroy']);
        Route::get('merubah-znt-destroy/{id}', 'PerubahanZntController@destroy')->name('merubah-znt.destroy');
        Route::get('merubah-znt-verifikasi', 'PerubahanZntController@verifikasiIndex')->name('merubah-znt.verifikasi.index');
        Route::get('merubah-znt-verifikasi/{id}', 'PerubahanZntController@verifikasi')->name('merubah-znt.verifikasi');
        Route::post('merubah-znt-verifikasi-store/{id}', 'PerubahanZntController@verifikasiStore')->name('merubah-znt.verifikasi-proses');
    });

    route::group(['middleware' => 'Surapermission:penelitian.surat-tugas.create.menu'], function () {
        route::get('surat-tugas-hapus/{id}', 'SuratTugasPendataanController@destroy')->name('surat-tugas.delete');
        route::get('surat-tugas-cetak/{id}', 'SuratTugasPendataanController@cetak')->name('surat-tugas.cetak');
        route::get('surat-tugas-lampiran/{id}', 'SuratTugasPendataanController@cetakLampiran')->name('surat-tugas.lampiran');
        route::resource('surat-tugas', 'SuratTugasPendataanController')->except(['destroy']);
    });

    Route::group(['prefix' => 'laporan/pendataan', 'as' => 'laporan.pendataan.'], function () {
        route::get('/objek', 'PendataanController@reportObjek')->name('objek');
        route::get('/objek-excel', 'PendataanController@reportObjekExcel')->name('objek.excel');
        route::get('/subjek', 'PendataanController@reportSubjek')->name('subjek');
        route::get('/subjek-excel', 'PendataanController@reportSubjekExcel')->name('subjek.excel');
        route::get('/item-objek', 'PendataanController@reportSubjek')->name('item-objek');
    });

    route::resource("pembayaran-mutasi", "PembayaranMutasiController");

    Route::resource('kurang-bayar', 'kurangBayarController');
    route::get('notifikasi-show', function (Request $request) {
        if ($request->user_id) {
            $data = Notifikasi::selectraw("tipe,judul,deskripsi")
                ->where('user_id', $request->user_id)
                ->wherenull('status')->get();

            Notifikasi::where('user_id', $request->user_id)->update(['status' => 1]);
            if ($data == null) {
                return [];
            }
            return $data;
        } else {
            return [];
        }
    });

    Route::group(['middleware' => ['permission:master-potongan.menu']], function () {
        Route::get('master-potongan-individu', 'MsPotonganController@individu');
        Route::get('master-potongan-individu/{nop}', 'MsPotonganController@EditIndividu');
        Route::post('master-potongan-individu/{nop}', 'MsPotonganController@UpdateIndividu');
        Route::get('master-potongan-individu-hapus/{nop}', 'MsPotonganController@HapusIndividu');
        Route::post('master-potongan-individu', 'MsPotonganController@Storeindividu');
        Route::post('master-potongan-individu-nop', 'MsPotonganController@StoreindividuNop');
        Route::resource('master-potongan', 'MsPotonganController');
    });

    Route::get('surat-tagihan-hapus/{id}', 'SuratTagihanController@destroy')->name('surat-tagihan.hapus');
    Route::resource('surat-tagihan', 'SuratTagihanController')->except(['destroy', 'edit', 'update']);
    route::get('surat-tagihan-kobil/{id}', 'suratTagihanController@formKobil')->name('surat-tagihan.kobil.create');
    route::post('surat-tagihan-kobil/{id}', 'suratTagihanController@formKobilStore')->name('surat-tagihan.kobil.store');
    Route::resource('koreksi-sismiop', 'KoreksiSismiopController');

    Route::get('laporan-tagihan', 'TagihanController@index')->name('laporan.tagihan.index');
    Route::get('laporan-tagihan-excel', 'TagihanController@indexExcel')->name('laporan.tagihan.excel');

    Route::group(['prefix' => 'mandiri', 'as' => 'mandiri.'], function () {
        // Route::post('pembetulan-subjek', 'LayananRegistrasiController@storePembetulanOnline');
        Route::resource('layanan', 'LayananMandiriController');
    });

    route::get('daftar-objek-wp', function (Request $request) {

        $data = getNopWp();
        $response = [];
        foreach ($data as $row) {
            $row = (object)$row;
            $response[] = [
                'id' => $row->nop,
                'text' => $row->nama
            ];
        }
        $arrayVar = $response;
        $search = (string) $request->get('q', '');
        if ($search === '') {
            return response()->json($arrayVar);
        }

        // Filter data berdasarkan id atau text
        $filteredData = array_filter($arrayVar, function ($item) use ($search) {
            return stripos($item['id'], $search) !== false || stripos($item['text'], $search) !== false;
        });

        // Reset indeks array dan kirim data sebagai JSON
        $response = array_values($filteredData);
        return response()->json($response);
    });
    route::get('sspd-bphtb', 'bphtbController@sspdIndex')->name('bphtb.sspd.index');
    route::get('sspd-bphtb/{registrasi}', 'bphtbController@sspdShow')->name('bphtb.sspd.show');

    route::resource('layanan-bphtb', 'layananBpthbCotroller');
});

Route::get('surat-tagihan-cetak/{id}', 'SuratTagihanController@cetakPdf')->name('surat-tagihan.cetak');
route::get('preview-esppt', function (Request $request) {
    $nf = $request->nama_file;
    $pathToFile = Storage::disk('sppt')->path($nf);
    return response()->file($pathToFile);
});

route::get('count-objek', function (Request $request) {
    $kd_kecamatan = $request->kd_kecamatan ?? '';
    $kd_kelurahan = $request->kd_kelurahan ?? '';
    $kd_blok = $request->kd_blok ?? '';
    $no_urut = padding(($request->no_urut ?? ''), 0, 4);
    $tahun = $request->tahun;

    $objek = DB::connection('oracle_satutujuh')->table('DAT_OBJEK_PAJAK')
        ->join('dat_op_bumi', function ($join) {
            $join->on('dat_objek_pajak.kd_propinsi', '=', 'dat_op_bumi.kd_propinsi')
                ->on('dat_objek_pajak.kd_dati2', '=', 'dat_op_bumi.kd_dati2')
                ->on('dat_objek_pajak.kd_kecamatan', '=', 'dat_op_bumi.kd_kecamatan')
                ->on('dat_objek_pajak.kd_kelurahan', '=', 'dat_op_bumi.kd_kelurahan')
                ->on('dat_objek_pajak.kd_blok', '=', 'dat_op_bumi.kd_blok')
                ->on('dat_objek_pajak.no_urut', '=', 'dat_op_bumi.no_urut')
                ->on('dat_objek_pajak.kd_jns_op', '=', 'dat_op_bumi.kd_jns_op');
        })
        ->leftjoin(DB::raw("(SELECT kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, pbb_yg_harus_dibayar_sppt pbb
                                FROM sppt
                                WHERE thn_pajak_sppt = '$tahun') sppt"), function ($join) {
            $join->on('dat_objek_pajak.kd_propinsi', '=', 'sppt.kd_propinsi')
                ->on('dat_objek_pajak.kd_dati2', '=', 'sppt.kd_dati2')
                ->on('dat_objek_pajak.kd_kecamatan', '=', 'sppt.kd_kecamatan')
                ->on('dat_objek_pajak.kd_kelurahan', '=', 'sppt.kd_kelurahan')
                ->on('dat_objek_pajak.kd_blok', '=', 'sppt.kd_blok')
                ->on('dat_objek_pajak.no_urut', '=', 'sppt.no_urut')
                ->on('dat_objek_pajak.kd_jns_op', '=', 'sppt.kd_jns_op');
        })
        ->select(db::raw("count(1) jumlah,sum(case when pbb is not null then 1 else 0 end) sudah,sum(case when pbb is  null then 1 else 0 end) belum"))
        ->whereraw(" total_luas_bumi>0 and  jns_transaksi_op != '3'
        AND jns_bumi IN ('1', '2', '3')");


    if ($kd_kecamatan != '') {
        $objek = $objek->where('dat_objek_pajak.kd_kecamatan', $kd_kecamatan);
    }
    if ($kd_kelurahan != '') {
        $objek = $objek->where('dat_objek_pajak.kd_kelurahan', $kd_kelurahan);
    }
    if ($kd_blok != '') {
        $objek = $objek->where('dat_objek_pajak.kd_blok', $kd_blok);
    }
    if ($no_urut != '0000') {
        $objek = $objek->where('dat_objek_pajak.no_urut', $no_urut);
    }
    $objek = $objek->first();
    return ['jumlah' => angka($objek->jumlah), 'sudah' => angka($objek->sudah), 'belum' => angka($objek->belum)];
});


route::post('detail-nir', function (Request $request) {
    $kd_kecamatan = $request->kd_kecamatan;
    $kd_kelurahan = $request->kd_kelurahan;
    $znt = $request->kd_znt;
    $data = DB::connection("oracle_satutujuh")->select(db::raw("select thn_nir_znt,
    nir * 1000 nir,
    kd_kls_tanah,
    nilai_per_m2_tanah * 1000 nilai_per_m2_tanah
    from dat_nir
    join kelas_tanah ON     KELAS_TANAH.THN_awal_kls_tanah <= dat_nir.thn_nir_znt
    AND KELAS_TANAH.THN_akhir_kls_tanah >= dat_nir.thn_nir_znt
    AND KELAS_TANAH.NILAI_MIN_TANAH <= nir
    AND KELAS_TANAH.NILAI_Max_TANAH > nir
    where dat_nir.kd_Kecamatan='$kd_kecamatan'
    and dat_nir.kd_Kelurahan='$kd_kelurahan'
    and kd_znt='$znt' and 
    upper(kelas_tanah.kd_kls_tanah) not like '%X%'
    and thn_nir_znt>=2014 order by thn_nir_znt asc"));
    return view('detail-nir', compact('data'));
});



route::get('sppt-lunas-delete', function () {
    $data = DB::connection("oracle_satutujuh")->select("select * 
from (
select kd_propinsi,
kd_dati2,
kd_kecamatan,
kd_kelurahan,
kd_blok,
no_urut,
kd_jns_op,
thn_pajak_sppt,(select count(1) from sppt 
where kd_propinsi=pembayaran_sppt.kd_propinsi and
kd_dati2=pembayaran_sppt.kd_dati2 and
kd_kecamatan=pembayaran_sppt.kd_kecamatan and
kd_kelurahan=pembayaran_sppt.kd_kelurahan and
kd_blok=pembayaran_sppt.kd_blok and
no_urut=pembayaran_sppt.no_urut and
kd_jns_op=pembayaran_sppt.kd_jns_op and
thn_pajak_sppt=pembayaran_sppt.thn_pajak_sppt  ) is_sppt 
from pembayaran_sppt
where trunc(tgl_pembayaran_sppt) between to_date('01012024','ddmmyyyy') and trunc(sysdate)
) where is_sppt<>1
order by thn_pajak_sppt");

    foreach ($data as $row) {
        $cek = DB::connection("oracle_satutujuh")->table('log_sppt')
            ->select(db::raw("to_char(tgl_terbit_sppt,'yyyy-mm-dd') tgl_terbit_sppt,to_char(tgl_terbit_sppt,'yyyy-mm-dd') tgl_jatuh_tempo_sppt"))
            ->where(
                [
                    'kd_propinsi' => $row->kd_propinsi,
                    'kd_dati2' => $row->kd_dati2,
                    'kd_kecamatan' => $row->kd_kecamatan,
                    'kd_kelurahan' => $row->kd_kelurahan,
                    'kd_blok' => $row->kd_blok,
                    'no_urut' => $row->no_urut,
                    'kd_jns_op' => $row->kd_jns_op,
                    'thn_pajak_sppt' => $row->thn_pajak_sppt,
                ]
            )
            ->first();
        if ($cek) {
            $kd_propinsi = $row->kd_propinsi;
            $kd_dati2 = $row->kd_dati2;
            $kd_kecamatan = $row->kd_kecamatan;
            $kd_kelurahan = $row->kd_kelurahan;
            $kd_blok = $row->kd_blok;
            $no_urut = $row->no_urut;
            $kd_jns_op = $row->kd_jns_op;
            $thn_pajak_sppt = $row->thn_pajak_sppt;
            $tgl_terbit = $cek->tgl_terbit_sppt;
            $tgl_jatuh_tempo_sppt = $cek->tgl_jatuh_tempo_sppt;

            PenetapanSppt::proses($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $thn_pajak_sppt, $tgl_terbit, $tgl_jatuh_tempo_sppt);
        }
    }
});

route::get('test-baku', function () {


    $param = [
        'is_tunggakan' => true,
        'cut' => date('Ymd'),

    ];
    // return Baku::get($param)->get();
    return Baku::RekapRealiasasi($param)->orderby('buku')->get();
});


/* route::get('pembayaran-online', function () {
    // return 'pembayaran-online';
    return view('layouts.front.app');
}); */


Route::get('otp-verify/{id}', 'Auth\OTPVerificationController@showVerificationForm')->name('otp.verify.form');
Route::post('otp-verify', 'Auth\OTPVerificationController@verifyOtp')->name('otp.verify.store');
route::get('permohonan-bphtb/return-hasil', 'Api\permohonanBphtbController@returnHasil');

// route::get('test-oca', 'WhatsAppController@sendMessage');


// Route::get('otp-verify/{username}', [OTPValidationController::class, 'showVerificationForm'])->name('otp.verify');
// Route::post('otp-verify', [OTPValidationController::class, 'verifyOtp']);
