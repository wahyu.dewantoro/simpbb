<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th class="text-center" rowspan="2" width="10px" class="text-center">No</th>
            <th class="text-center" width="200px" rowspan="2">JENIS</th>
            <th class="text-center" width="200px" rowspan="2">Lantai</th>
            <th class="text-center" colspan="5">Bintang</th>
        <tr>
            <th class="text-center">1</th>
            <th class="text-center">2</th>
            <th class="text-center">3</th>
            <th class="text-center">4</th>
            <th class="text-center">5</th>

        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i=>$rk)
        <tr>
            <td class="text-center">{{ $i+1 }} </td>
            <td>{{ $rk->jenis }}</td>
            <td class="text-center">{{ $rk->lantai_min_jpb7 }} s.d. {{ $rk->lantai_max_jpb7 }}</td>
            <td class="text-right"> @if($read=='0')  <input type="text" name="nilai_{{ $i }}[]"  class="form-control angka"  value="{{ $rk->bintang_1 }}">  @else {{ $rk->bintang_1 }} @endif</td>
            <td class="text-right"> @if($read=='0')  <input type="text" name="nilai_{{ $i }}[]"  class="form-control angka"  value="{{ $rk->bintang_2 }}">  @else {{ $rk->bintang_2 }} @endif</td>
            <td class="text-right"> @if($read=='0')  <input type="text" name="nilai_{{ $i }}[]"  class="form-control angka"  value="{{ $rk->bintang_3 }}">  @else {{ $rk->bintang_3 }} @endif</td>
            <td class="text-right"> @if($read=='0')  <input type="text" name="nilai_{{ $i }}[]"  class="form-control angka"  value="{{ $rk->bintang_4 }}">  @else {{ $rk->bintang_4 }} @endif</td>
            <td class="text-right"> @if($read=='0')  <input type="text" name="nilai_{{ $i }}[]"  class="form-control angka"  value="{{ $rk->bintang_5 }}">  @else {{ $rk->bintang_5 }} @endif</td>
            {{-- <td @if($read=='1' ) class="text-right" @endif>
                @if($read=='0')
                <input class="form-control angka" required type="text" value="{{ $rk->nilai_dbkb_jpb6!=''?$rk->nilai_dbkb_jpb6*1000:0 }}" name="nilai_{{ $rk->kls_dbkb_jpb7 }}" id="nilai_{{ $rk->kls_dbkb_jpb7 }}">
            @else
            {{ $rk->nilai_dbkb_jpb6!=''?angka($rk->nilai_dbkb_jpb6*1000):0 }}
            @endif

            </td> --}}
        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.jepebe-enam.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
