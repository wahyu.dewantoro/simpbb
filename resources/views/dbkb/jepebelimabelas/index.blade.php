@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>DBKB JPB 15 ( TANGKI MINYAK )</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    <a href="{{ route('dbkb.jepebe-lima-belas.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-file"></i> Konfigurasi
                    </a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-2">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group row">
                                    <label for="thn_dbkb_jpb15" class="col-sm-6 col-form-label">Tahun</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control angka" name="thn_dbkb_jpb15" id="thn_dbkb_jpb15" value="{{ date('Y') }}" placeholder="Tahun" maxlength="4" autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div id="konten"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {
        // $('#thn_dbkb_jpb15').trigger('change')

        loaddata($('#thn_dbkb_jpb15').val())

        function loaddata(thn) {
            if (thn.length == 4) {
                openloading()
                $.ajax({
                    url: "{{ route('dbkb.jepebe-lima-belas.index') }}"
                    , data: {
                        'tahun': thn
                    }
                    , success: function(res) {
                        closeloading()
                        $('#konten').html(res)
                    }
                })
            } else {
                $('#konten').html('')
            }
        }


        $('#thn_dbkb_jpb15').on('keyup change', function(e) {
            thn = $('#thn_dbkb_jpb15').val()
            loaddata(thn)
        })
    });

</script>
@endsection
