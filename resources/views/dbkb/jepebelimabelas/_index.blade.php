<table class="table table-sm table-bordered">
    <thead>
        <tr>
            <th width="10px" class="text-center">No</th>
            <th class="text-center">Jenis</th>
            <th class="text-center">Kapasitas</th>
            <th class="text-center" width="200px">Nilai </th>
        </tr>
    </thead>
    <tbody>
        @php
        $no=1;
        @endphp
        @foreach ($data as $i => $rk)
        <tr>
            <td class="text-center"> {{ $no}} </td>
            <td>{{ $rk->jns_tangki_dbkb_jpb15=='2'?'Bawah Tanah':'Di Atas Tanah' }} </td>
            <td>{{ angka($rk->kapasitas_min_dbkb_jpb15).' s.d. '.angka($rk->kapasitas_max_dbkb_jpb15) }} </td>

            <td @if($read=='1' ) class="text-right" @endif>
                @if($read=='0')

                <input type="hidden" name="jns_tangki_dbkb_jpb15[]" value="{{ $rk->jns_tangki_dbkb_jpb15 }}">
                <input type="hidden" name="kapasitas_min_dbkb_jpb15[]" value="{{ $rk->kapasitas_min_dbkb_jpb15 }}">
                <input type="hidden" name="kapasitas_max_dbkb_jpb15[]" value="{{ $rk->kapasitas_max_dbkb_jpb15 }}">


                <input class="form-control angka" required type="text" value="{{ $rk->nilai_dbkb_jpb15!=''?$rk->nilai_dbkb_jpb15*1000:0 }}" name="nilai_dbkb_jpb15[]">
                @else
                {{ $rk->nilai_dbkb_jpb15!=''?angka($rk->nilai_dbkb_jpb15*1000):0 }}
                @endif
            </td>
        </tr>
        @php
        $no++;
        @endphp
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.daya-dukung.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
