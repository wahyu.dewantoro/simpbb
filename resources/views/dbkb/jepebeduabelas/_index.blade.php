<table class="table table-sm table-bordered">
    <thead>
        <tr>
            <th width="10px" class="text-center">KODE</th>
            <th class="text-center">Type</th>
            <th class="text-center" width="200px">Nilai </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $rk)
        <tr>
            <td class="text-center">{{ $rk->type_dbkb_jpb12 }} </td>
            <td>{{ strtoupper(typeparkiran()[$rk->type_dbkb_jpb12]) }} </td>
            <td @if($read=='1' ) class="text-right" @endif>
                @if($read=='0')
                <input class="form-control angka" required type="text" value="{{ $rk->nilai_dbkb_jpb12!=''?$rk->nilai_dbkb_jpb12*1000:0 }}" name="nilai_{{ $rk->type_dbkb_jpb12 }}" id="nilai_{{ $rk->type_dbkb_jpb12 }}">
                @else
                {{ $rk->nilai_dbkb_jpb12!=''?angka($rk->nilai_dbkb_jpb12*1000):0 }}
                @endif

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.jepebe-dua-belas.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
