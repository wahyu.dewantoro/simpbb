@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Form DBKB JPB 8 (BENGKEL/GUDANG/PERTANIAN)</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body p-2">
                        <form action="{{ route('dbkb.jepebe-delapan.store') }}" method="post">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group row">
                                        <label for="thn_dbkb_jpb8" class="col-sm-6 col-form-label">Tahun</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control angka" name="thn_dbkb_jpb8" id="thn_dbkb_jpb8" placeholder="Tahun" maxlength="4" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div id="konten"></div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {
        $('#thn_dbkb_jpb8').on('keyup', function(e) {
            thn = $('#thn_dbkb_jpb8').val()
            // console.log(thn.length)

            if (thn.length == 4) {
                openloading()
                $.ajax({
                    url: "{{ route('dbkb.jepebe-delapan.create') }}"
                    , data: {
                        'tahun': thn
                    }
                    , success: function(res) {
                        closeloading()
                        $('#konten').html(res)
                    }
                })
            } else {
                $('#konten').html('')
            }
        })
    });

</script>
@endsection
