<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th rowspan="2" width="10px" class="text-center">No</th>
            <th colspan="2" class="text-center">Lebar Bentang</th>
            <th colspan="2" class="text-center">Tinggi Kolom</th>
            <th rowspan="2" class="text-center" width="200px">Nilai </th>
        </tr>
        <tr>
            <th class="text-center">Min</th>
            <th class="text-center">Max</th>
            <th class="text-center">Min</th>
            <th class="text-center">Max</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i=>$rk)
        <tr>
            <td class="text-center">{{ $i+1 }} </td>
            <td class="text-center">{{ $rk->lbr_bent_min_dbkb_jpb3  }}</td>
            <td class="text-center">{{ $rk->lbr_bent_max_dbkb_jpb3  }}</td>
            <td class="text-center">{{ $rk->ting_kolom_min_dbkb_jpb3  }}</td>
            <td class="text-center">{{ $rk->ting_kolom_max_dbkb_jpb3  }}</td>



            <td @if($read=='1' ) class="text-right" @endif>
                @if($read=='0')
                <input class="form-control angka" required type="text" value="{{ $rk->nilai_dbkb_jpb3!=''?$rk->nilai_dbkb_jpb3*1000:0 }}" name="nilai_{{$rk->lbr_bent_min_dbkb_jpb3.'_'.$rk->lbr_bent_max_dbkb_jpb3.'_'.                   $rk->ting_kolom_min_dbkb_jpb3.'_'.$rk->ting_kolom_max_dbkb_jpb3                }}" id="nilai_{{ $rk->lbr_bent_min_dbkb_jpb3.'_'.$rk->lbr_bent_max_dbkb_jpb3.'_'.$rk->ting_kolom_min_dbkb_jpb3.'_'.$rk->ting_kolom_max_dbkb_jpb3 }}">
                @else
                {{ $rk->nilai_dbkb_jpb3!=''?angka($rk->nilai_dbkb_jpb3*1000):0 }}
                @endif

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.jepebe-dua.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
