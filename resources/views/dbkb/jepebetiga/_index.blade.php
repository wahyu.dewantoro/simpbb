<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th rowspan="2" width="10px" class="text-center">No</th>
            <th width="200px" rowspan="2" class="text-center">Lebar Bentang</th>
            <th colspan="4" class="text-center">Tinggi Kolom</th>
        </tr>
        <tr>
            <th class="text-center">
                < 5M</th>
            <th class="text-center"> 5M s.d. 7M </th>
            <th class="text-center"> 8M s.d. 10M </th>
            <th class="text-center"> > 10M </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i=>$rk)
        <tr>
            <td class="text-center">{{ $i+1 }} </td>
            <td class="text-center">{{ angka($rk->lbr_bent_min_dbkb_jpb3) }} M s.d. {{ angka($rk->lbr_bent_max_dbkb_jpb3) }} M</td>
            <td class="text-right"> @if($read=='1') {{ angka($rk->kolom_0_4) }} @else <input type="text" name="nilai_{{$i}}[]" class="form-control angka" value="{{ $rk->kolom_0_4 }}"> @endif </td>
            <td class="text-right"> @if($read=='1') {{ angka($rk->kolom_5_7) }} @else <input type="text" name="nilai_{{$i}}[]" class="form-control angka" value="{{ $rk->kolom_5_7 }}"> @endif </td>
            <td class="text-right"> @if($read=='1') {{ angka($rk->kolom_8_10) }} @else <input type="text" name="nilai_{{$i}}[]" class="form-control angka" value="{{ $rk->kolom_8_10 }}"> @endif </td>
            <td class="text-right"> @if($read=='1') {{ angka($rk->kolom_11_99) }} @else <input type="text" name="nilai_{{$i}}[]" class="form-control angka" value="{{ $rk->kolom_11_99 }}"> @endif </td>

        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.jepebe-tiga.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
