<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th width="10px" class="text-center">No</th>
            <th class="text-center">Kelas</th>
            <th class="text-center" width="200px">Nilai </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i=>$rk)
        <tr>
            <td class="text-center">{{ $i+1 }} </td>
            <td> Kelas {{ $rk->kls_dbkb_jpb6 }} </td>
            <td @if($read=='1' ) class="text-right" @endif>
                @if($read=='0')
                <input class="form-control angka" required type="text" value="{{ $rk->nilai_dbkb_jpb6!=''?$rk->nilai_dbkb_jpb6*1000:0 }}" name="nilai_{{ $rk->kls_dbkb_jpb6 }}" id="nilai_{{ $rk->kls_dbkb_jpb6 }}">
                @else
                {{ $rk->nilai_dbkb_jpb6!=''?angka($rk->nilai_dbkb_jpb6*1000):0 }}
                @endif

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.jepebe-enam.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
