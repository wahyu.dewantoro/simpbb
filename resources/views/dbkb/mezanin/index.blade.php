@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>DBKB Mezanin</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a href="{{ url('dbkb/mezanine/create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-file"></i> Tambah
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body p-2">
                            <table class="table table-sm table-bordered text-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="5px">No</th>
                                        <th class="text-center" width="100px">Tahun</th>
                                        <th class="text-center">Nilai</th>
                                        <th class="text-center" width="100px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td class="text-center">{{ $item->thn_dbkb_mezanin }}</td>
                                            <td class="text-right">{{ angka($item->nilai_dbkb_mezanin * 1000) }}</td>
                                            <th class="text-center">
                                                <a href="{{ url('dbkb/mezanine').'/'. $item->thn_dbkb_mezanin.'/edit' }}">Edit</a>
                                            </th>
                                        </tr>

                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
