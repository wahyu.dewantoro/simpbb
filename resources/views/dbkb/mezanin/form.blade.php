@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>DBKB Mezanin</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a href="{{ url('dbkb/mezanine/create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-file"></i> Tambah
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body p-2">
                            <form action="{{ $data['action'] }}" method="post">
                                @method($data['method'])
                                @csrf
                                <div class="form-group row">
                                    <label for="thn_dbkb_mezanin" class="col-form-control col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <input type="text" name="thn_dbkb_mezanin" id="thn_dbkb_mezanin"
                                            value="{{ old('thn_dbkb_mezanin') ?? ($data['dbkb']->thn_dbkb_mezanin ?? '') }}"
                                            class="form-control angka form-control-sm  @error('thn_dbkb_mezanin') is-invalid  @enderror  ">
                                        @error('thn_dbkb_mezanin')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nilai_dbkb_mezanin" class="col-form-control col-md-4">Nilai</label>
                                    <div class="col-md-8">
                                        @php
                                            $n = $data['dbkb']->nilai_dbkb_mezanin ?? '';
                                            if ($n != '') {
                                                $n = $n * 1000;
                                            }

                                            // dd($n);
                                        @endphp

                                        <input type="text" name="nilai_dbkb_mezanin" id="nilai_dbkb_mezanin"
                                            value="{{ old('nilai_dbkb_mezanin') ?? ($n) }}"
                                            class="form-control  ribuan form-control-sm  @error('nilai_dbkb_mezanin') is-invalid  @enderror  ">
                                        @error('nilai_dbkb_mezanin')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i>
                                        Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script>
        $(document).on('keyup', '.angka', function(event) {
            var isi = $(this).val()
            var a = isi.toString();
            var b = a.replace(/[^\d]/g, "");
            $(this).val(b)

        });

        $(document).on('keyup', '.ribuan', function(event) {
            var isi = $(this).val()
            var b = formatRupiah(isi)

            $(this).val(b)

        });

        $('#nilai_dbkb_mezanin').trigger('keyup')
    </script>
@endsection
