@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Harga Fasilitas</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-2">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group row">
                                    <label for="thn_non_dep" class="col-sm-6 col-form-label">Tahun</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control angka" name="thn_non_dep" id="thn_non_dep" value="{{ date('Y') }}" placeholder="Tahun" maxlength="4" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="float-sm-right">
                                    <a href="{{ route('dbkb.harga-fasilitas.create') }}" class="btn btn-primary btn-sm">
                                        <i class="fas fa-file"></i> Konfigurasi
                                    </a>
                                    <button class="btn btn-flat btn-sm btn-warning" type="button" id="cetak"> <i class="fas fa-print"></i> Cetak</button>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div id="konten"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {
        // $('#thn_non_dep').trigger('change')

        loaddata($('#thn_non_dep').val())

        function loaddata(thn) {
            if (thn.length == 4) {
                openloading()
                $.ajax({
                    url: "{{ route('dbkb.harga-fasilitas.index') }}"
                    , data: {
                        'tahun': thn
                    }
                    , success: function(res) {
                        closeloading()
                        $('#konten').html(res)
                    }
                })
            } else {
                $('#konten').html('')
            }
        }


        $('#thn_non_dep').on('keyup change', function(e) {
            thn = $('#thn_non_dep').val()
            loaddata(thn)
        })

        $('#cetak').on('click', function(e) {
            e.preventDefault()
            url = "{{ route('dbkb.harga-fasilitas.export') }}?tahun=" + $('#thn_non_dep').val()
            window.open(url, '_blank')
        })

    });

</script>
@endsection
