@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Form Harga Fasilitas</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body p-2">
                        <form action="{{ route('dbkb.harga-fasilitas.store') }}" method="post">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group row">
                                        <label for="thn_non_dep" class="col-sm-6 col-form-label">Tahun</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control angka" name="thn_non_dep" id="thn_non_dep" placeholder="Tahun" maxlength="4" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div id="konten"></div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {
        $('#thn_non_dep').on('keyup', function(e) {
            thn = $('#thn_non_dep').val()
            console.log(thn.length)

            if (thn.length == 4) {
                openloading()
                $.ajax({
                    url: "{{ route('dbkb.harga-fasilitas.create') }}"
                    , data: {
                        'tahun': thn
                    }
                    , success: function(res) {
                        closeloading()
                        $('#konten').html(res)
                    }
                })
            } else {
                $('#konten').html('')
            }
        })
    });

</script>
@endsection
