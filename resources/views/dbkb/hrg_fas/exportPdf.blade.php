<html>
<head>
    @include('layouts.style_pdf')
</head>
<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah"><b>HARGA FASILITAS {{ $tahun }}</b></p>
    @include('dbkb/hrg_fas/_form',compact('fasilitas','read'))
</body>
</html>
