<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th class="text-center" width="30px">Kode</th>
            <th class="text-center" width="600px">Fasilitas</th>
            <th class="text-center" width="100px">Satuan</th>
            <th class="text-center" width="10px">Status</th>
            <th class="text-center" width="10px">Ketergantungan</th>
            <th class="text-center">Nilai</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($fasilitas as $rk)
        <tr>
            <td class="text-center">{{ $rk->kd_fasilitas }} </td>
            <td>{{ $rk->nm_fasilitas }} </td>
            <td class="text-center"><?=  $rk->satuan_fasilitas=='M2'? 'M<sup>2</sup>':$rk->satuan_fasilitas ?></td>
            <td class="text-center">{{ $rk->status_fasilitas }} </td>
            <td class="text-center">{{ $rk->ketergantungan }} </td>
            <td @if($read=='1' ) class="text-right" @endif>
                @if($read=='0')
                <input class="form-control form-control-sm angka" required type="text" value="{{ $rk->nilai!=''?$rk->nilai:0 }}" name="nilai_{{ $rk->kd_fasilitas }}" id="nilai_{{ $rk->kd_fasilitas }}">
                @else
                {{ $rk->nilai!=''?angka($rk->nilai):0 }}
                @endif

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">

    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.harga-fasilitas.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
