@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>DBKB JPB 14 ( POMPA BENSIN )</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    <a href="{{ route('dbkb.jepebe-empat-belas.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-file"></i> Tambah
                    </a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-2">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-sm table-bordered table-hover">
                                    <thead class="bg-info">
                                        <tr>
                                            <th width="20px" class="text-center">No</th>
                                            <th width="100px" class="text-center">Tahun</th>
                                            <th class="text-center">Nilai</th>
                                            <th width="100px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $i=>$item)
                                        <tr>
                                            <td class="text-center">{{ $i+1 }}</td>
                                            <td class="text-center">{{ $item->thn_dbkb_jpb14 }}</td>
                                            <td class="text-right">{{ angka($item->nilai_dbkb_jpb14*1000) }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('dbkb.jepebe-empat-belas.edit', $item->thn_dbkb_jpb14 ) }}"> <i class="fas fa-edit text-success"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>


</script>
@endsection
