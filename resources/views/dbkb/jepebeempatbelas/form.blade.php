@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>FORM DBKB JPB 14 ( POMPA BENSIN )</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-2">
                        <div class="row">
                            <div class="col-md-4">
                                <form action="{{ $data['action'] }}" method="post">
                                    @csrf
                                    @method($data['method'])
                                    <div class="form-group">
                                        <label for="">Tahun</label>
                                        <input type="text" class="form-control angka" name="thn_dbkb_jpb14" id="thn_dbkb_jpb14" value="{{ $data['jpb']->thn_dbkb_jpb14??'' }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nilai</label>
                                        <input type="text" class="form-control angka" name="nilai_dbkb_jpb14" id="nilai_dbkb_jpb14" value="{{ isset($data['jpb']->nilai_dbkb_jpb14)?$data['jpb']->nilai_dbkb_jpb14*1000:'' }}">
                                    </div>
                                    <div class="float-right">
                                        <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
                                        <a href="{{ route('dbkb.jepebe-empat-belas.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>


</script>
@endsection
