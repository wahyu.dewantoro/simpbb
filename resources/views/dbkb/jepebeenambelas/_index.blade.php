<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th rowspan="2" width="15px" class="text-center">No</th>
            <th width="200px" rowspan="2" class="text-center">Lantai</th>
            <th colspan="4" class="text-center">Kelas</th>
        </tr>
        <tr>
            <th class="text-center">1</th>
            <th class="text-center">2</th>
            <th class="text-center">3</th>
            <th class="text-center">4</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i=>$rk)
        <tr>
            <td class="text-center">{{ $i+1 }} </td>
            <td class="text-center">{{ $rk->lantai_min_jpb16 }} s.d. {{ $rk->lantai_max_jpb16 }}</td>
            <td class="text-right"> @if($read=='0') <input type="text" class="form-control angka" name="nilai_{{$i}}[]" value="{{ $rk->satu }}"> @else {{ angka($rk->satu) }} @endif</td>
            <td class="text-right"> @if($read=='0') <input type="text" class="form-control angka" name="nilai_{{$i}}[]" value="{{ $rk->dua }}"> @else {{ angka($rk->dua) }} @endif</td>
            <td class="text-right"> @if($read=='0') <input type="text" class="form-control angka" name="nilai_{{$i}}[]" value="{{ $rk->tiga }}"> @else {{ angka($rk->tiga) }} @endif</td>
            <td class="text-right"> @if($read=='0') <input type="text" class="form-control angka" name="nilai_{{$i}}[]" value="{{ $rk->empat }}"> @else {{ angka($rk->empat) }} @endif</td>
        </tr>
        @endforeach
    </tbody>
</table>
@if($read=='0')
<div class="float-right">
    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('dbkb.jepebe-enam-belas.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
@endif
