<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>SIPANJI - PBB P2</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('favicon.ico') }}" rel="icon">
    <link href="{{ asset('favicon.ico') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Dosis:300,400,500,,600,700,700i|Lato:300,300i,400,400i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('Butterfly') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('Butterfly') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{ asset('Butterfly') }}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{ asset('Butterfly') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="{{ asset('Butterfly') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('Butterfly') }}/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.6.0/css/all.min.css"
        integrity="sha512-Kc323vGBEqzTmouAECnVceyQqyqdsSiqLQISBL29aUW4U/M7pSPA/gEUZQqv1cwx4OnYxTxve5UMg5GT6L4JJg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- =======================================================
  * Template Name: Butterfly
  * Template URL: https://bootstrapmade.com/butterfly-free-bootstrap-theme/
  * Updated: Mar 17 2024 with Bootstrap v5.3.3
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center justify-content-between">

            <a href="/" class="logo"><img src="{{ asset('sipanji.png') }}" alt="" class="img-fluid"></a>
            <!-- Uncomment below if you prefer to use text as a logo -->
            <!-- <h1 class="logo"><a href="index.html">Butterfly</a></h1> -->

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#about">Fitur</a></li>
                    <li><a class="nav-link scrollto" href="#services">Jenis Pelayanan</a></li>
                    {{-- <li><a class="nav-link scrollto " href="#portfolio">Alur Pelayanan</a></li> --}}
                    {{-- <li><a class="nav-link scrollto" href="#team">Team</a></li> --}}
                    {{-- <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i
                                        class="bi bi-chevron-right"></i></a>
                                <ul>
                                    <li><a href="#">Deep Drop Down 1</a></li>
                                    <li><a href="#">Deep Drop Down 2</a></li>
                                    <li><a href="#">Deep Drop Down 3</a></li>
                                    <li><a href="#">Deep Drop Down 4</a></li>
                                    <li><a href="#">Deep Drop Down 5</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Drop Down 2</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                        </ul>
                    </li> --}}
                    {{-- <li><a class="nav-link scrollto" href="#contact">Contact</a></li> --}}
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">

        <div class="container">
            <div class="row">
                <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <h1>SIPANJI - PBB P2<br> BAPENDA KABUPATEN MALANG</h1>
                    <h2>Hadir untuk mempermudah masyarakat guna mengajukan pelayanan data Pajak Daerah PBB-P2 di
                        lingkungan Pemerintah Daerah Kabupaten Malang.</h2>
                    <div><a href="{{ url('login') }}" class="btn-get-started scrollto">Mulai / Login</a></div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img">
                    <img src="{{ asset('Butterfly') }}/assets/img/hero-img.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 d-flex justify-content-center  align-items-stretch position-relative">
                        <img src="{{ url('tax_dua-removebg-preview.png') }}" style="max-width: 100%" alt="">
                    </div>
                    <div
                        class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                        <h3>Dashboard layanan PBB P2 Bapenda Kabupaten Malang</h3>
                        <p>Menu atau fitur untuk mengecek data Pelayanan PBB-P2 di lingkungan Pemerintah Daerah
                            Kabupaten Malang.</p>


                        <div class="icon-box">
                            <div class="icon"><i class="fa-solid fa-receipt"></i></div>
                            <h4 class="title"><a href="#cek-sppt">Cek SPPT</a></h4>
                            <p class="description">layanan yang memungkinkan wajib pajak untuk memeriksa dan
                                memverifikasi informasi terkait tagihan pajak terutang </p>
                        </div>

                        <div class="icon-box">
                            <div class="icon"><i class="fa-solid fa-money-check-dollar"></i></div>
                            <h4 class="title"><a href="https://esppt.id/pembayaran-pbb">Pembayaran</a></h4>
                            <p class="description">Memungkinkan wajib pajak untuk melakukan pembayaran SPPT secara aman,
                                mudah, dan efisien</p>
                        </div>


                    </div>
                </div>

            </div>
        </section><!-- End About Section -->

        <section class="about" id="cek-sppt">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 d-flex justify-content-center position-relative">
                        <img src="{{ url('tax-bg.png') }}" alt="" style="max-width: 100%">
                    </div>
                    <div
                        class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                        <h3>Cek SPPT & Riwayat Pembayaran</h3>
                        <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group my-3">
                                        <input type="text" name="nop" id="nop" class="form-control"
                                            placeholder="NOP (Nomor Objek Pajak)">
                                    </div>
                                    <div class="form-group my-3">
                                        <input type="text" name="nomor_wa" id="nomor_wa" class="form-control"
                                            placeholder="Nomor Whatsapp">
                                    </div>
                                    <div class="form-group my-3">
                                        <div class="float-end">
                                            <button id="cek" class="btn btn-primary">Cek</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>
        <!-- ======= Services Section ======= -->
        <section id="services" class="services section-bg">
            <div class="container">
                <div class="section-title">
                    <h2>JENIS PELAYANAN</h2>
                    <p>Badan Pendapatan Daerah (Bapenda) Kabupaten Malang merupakan lembaga pemerintahan yang berperan
                        penting dalam mengelola dan meningkatkan pendapatan daerah. Bapenda Kabupaten Malang memberikan
                        berbagai layanan kepada masyarakat, termasuk pengelolaan pajak daerah, retribusi, dan
                        sumber-sumber pendapatan lainnya.</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box">
                            <div class="icon"><i class="fa-solid fa-retweet text-danger"></i></div>
                            <h4 class="title"><a href="">Mutasi</a></h4>
                            <p class="description">Layanan mutasi pajak merupakan salah satu pelayanan yang disediakan
                                oleh Badan Pendapatan Daerah (Bapenda) untuk membantu masyarakat dalam proses
                                perpindahan atau perubahan data wajib pajak. Layanan ini biasanya terkait dengan
                                perpindahan kepemilikan aset, seperti kendaraan bermotor atau properti, dari satu
                                pemilik ke pemilik lain, atau perubahan lokasi alamat wajib pajak. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box">
                            <div class="icon"><i class="fa-solid fa-file-pen text-success"></i></div>
                            <h4 class="title"><a href="">Pembetulan</a></h4>
                            <p class="description">Layanan pajak pembetulan adalah fasilitas yang disediakan oleh Badan
                                Pendapatan Daerah (Bapenda) untuk membantu wajib pajak melakukan koreksi atau pembetulan
                                atas data atau informasi pajak yang telah dilaporkan sebelumnya. Layanan ini bertujuan
                                untuk memastikan bahwa data perpajakan yang tercatat akurat dan sesuai dengan kondisi
                                sebenarnya, serta menghindari potensi sanksi akibat kesalahan pelaporan. </p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6" data-wow-delay="0.1s">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-chat-text" style="color: #3fcdc7;"></i></div>
                            <h4 class="title"><a href="">Keberatan & Pengurangan</a></h4>
                            <p class="description">Layanan pajak keberatan dan pengurangan adalah fasilitas yang
                                disediakan oleh Badan Pendapatan Daerah (Bapenda) untuk membantu wajib pajak yang merasa
                                tidak puas atau keberatan terhadap penetapan pajak yang dikenakan kepada mereka. Layanan
                                ini memungkinkan wajib pajak untuk mengajukan permohonan keberatan atau pengurangan
                                pajak dengan harapan mendapatkan peninjauan kembali dan penyesuaian yang lebih adil.</p>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Services Section -->


    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">

        {{-- <div class="footer-newsletter">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <h4>Join Our Newsletter</h4>
                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                        <form action="" method="post">
                            <input type="email" name="email"><input type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h3>BAPENDA KAB MALANG</h3>
                        <p>
                            Jl. Raden Panji no: 158<br> Kepanjen Jawa Timur<br> Indonesia 65163
                            <br>
                            <strong>Phone:</strong>
                            03413904898
                            <br>
                            <strong>Email:</strong> bapenda@malangkab.go.id<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>PELAYANAN PAJAK</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <ul>
                                    <li><i class="bx bx-chevron-right"></i> <a href="https://sipanji.id/">SIPANJI</a>
                                    </li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="https://esppt.id">PBB - P2</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul>
                                    <li><i class="bx bx-chevron-right"></i> <a
                                            href="https://bphtb-bapenda.malangkab.go.id">BPHTB</a></li>
                                    <li><i class="bx bx-chevron-right"></i> <a href="https://sipanji.id/pdrd">PDRD</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">

                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Our Social Networks</h4>
                        {{-- <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p> --}}
                        <div class="social-links mt-3">
                            <a href="https://x.com/bapendamalang" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="https://www.facebook.com/bapenda.kabmalang/about" class="facebook"><i
                                    class="bx bxl-facebook"></i></a>
                            <a href="https://www.instagram.com/bapenda_malangkab/" class="instagram"><i
                                    class="bx bxl-instagram"></i></a>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container py-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Butterfly</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/butterfly-free-bootstrap-theme/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('Butterfly') }}/assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="{{ asset('Butterfly') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('Butterfly') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="{{ asset('Butterfly') }}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="{{ asset('Butterfly') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="{{ asset('Butterfly') }}/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('Butterfly') }}/assets/js/main.js"></script>

</body>

</html>
