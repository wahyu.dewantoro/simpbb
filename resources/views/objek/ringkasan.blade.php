@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Ringkasan Objek Pajak</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-left">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card card-primary card-outline card-tabs">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan" required id="kd_kecamatan">
                            @if (count($kecamatan) > 1)
                            <option value="">-- kecamatan --</option>
                            @endif
                            @foreach ($kecamatan as $rowkec)
                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                {{ $rowkec->kd_kecamatan.' - '. $rowkec->nm_kecamatan }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan" id="kd_kelurahan">
                            <option value="">-- Kelurahan / Desa -- </option>
                        </select>
                    </div>

                    <div class="col-md-4">

                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" id="subjek_pajak" name="subjek_pajak" placeholder="Subjek Pajak">
                            <span class="input-group-append">
                                <button id="cari" class="btn btn-sm btn-info btn-flat"> <i class="fas fa-search"></i> cari</button>
                                <button id="excell" class="btn btn-sm btn-success btn-flat"><i class="far fa-file-alt"></i> Excel</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="hasil_kecamatan"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')

<script>
    $(document).ready(function() {

        // $('#excell').hide()

        $('#hasil_kecamatan').html('');


        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            // console.log(kk)
            getKelurahan(kk);
        })

        function getKelurahan(kk) {
            var html = '<option value="">-- pilih --</option>';
            if (kk != '') {
                Swal.fire({
                    // icon: 'error',
                    title: '<i class="fas fa-sync fa-spin"></i>'
                    , text: "Sedang membuka daftar kelurahan/desa"
                    , allowOutsideClick: false
                    , allowEscapeKey: false
                    , showConfirmButton: false
                })

                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        $('#excell').show();
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v + '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan').val(k);
                            }
                        });
                        $('#kd_kelurahan').html(html);
                        if (count != 1) {
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        }
                        swal.close();
                    }
                    , error: function(res) {
                        $('#excell').hide();
                        $('#kd_kelurahan').html(html);
                        swal.close();
                    }
                });
            } else {
                $('#excell').hide();
                $('#kd_kelurahan').html(html);
            }
        }


        $('#cari').on('click', function() {
            kd_kecamatan = $('#kd_kecamatan').val();
            kd_kelurahan = $('#kd_kelurahan').val() ?? ''
            subjek_pajak = $('#subjek_pajak').val() ?? ''
            showData(kd_kecamatan, kd_kelurahan,subjek_pajak)
        });


        $('#excell').on('click', function() {

            url_ = "{{ url('informasi/objek-pajak-list') }}?excel=true&kd_kecamatan=" + $('#kd_kecamatan').val() + "&kd_kelurahan=" + $('#kd_kelurahan').val() + "&subjek_pajak=" + $('#subjek_pajak').val()
            window.location.href = url_;
        })

        function showData(kd_kecamatan, kd_kelurahan,subjek_pajak) {
            $('#hasil_kecamatan').html('')
            // && kd_kelurahan != ''
            if (kd_kecamatan != '') {
                openloading()
                $.ajax({
                    url: "{{ url('informasi/objek-pajak-list') }}"
                    , data: {
                        kd_kecamatan: kd_kecamatan
                        , kd_kelurahan: kd_kelurahan,
                        subjek_pajak:subjek_pajak
                    }
                    , success: function(res) {
                        $('#hasil_kecamatan').html(res)
                        closeloading();
                    }
                    , error: function(e) {
                        Swal.fire({
                            icon: 'error'
                            , title: 'Peringatan'
                            , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                            , allowOutsideClick: false
                            , allowEscapeKey: false
                        , })
                        $('#hasil_kecamatan').html('')
                        closeloading();
                    }
                });
            }
        }


        /* $('#cek_kecamatan').on('click', function(e) {
            $('#hasil_kecamatan').html('')
            e.preventDefault();
            openloading()
            $.ajax({
                url: "{{ url('informasi/objek-pajak-list') }}"
                , data: {
                    kd_kecamatan: $('#kd_kecamatan').val()
                }
                , success: function(res) {
                    $('#hasil_kecamatan').html(res)
                    closeloading();
                }
                , error: function(e) {
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('#hasil_kecamatan').html('')
                    closeloading();
                }
            });
        }); */
    });

</script>
@endsection
