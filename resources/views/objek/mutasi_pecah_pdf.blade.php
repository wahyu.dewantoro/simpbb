<html>
<head>
    @include('layouts.style_pdf')
</head>
<body>
    @include('layouts.kop_pdf')
    <h3 class="text-tengah">MUTASI PECAH<br>
        {{ $lokasi->nm_kelurahan }} - {{ $lokasi->nm_kecamatan}}
    </h3>
    @include('objek.mutasi_pecah_index',compact('data','cetak'))
</body>
</html>
