<style>
    .table-sm td,
    .table-sm th {
        padding: 1px;
        font-size: 11px;
    }
</style>
<div class="row">
    <div class="col-md-4">
        <div class="card card-outline card-success">

            <div class="card-body p-1">
                <dl>
                    <dt><i class="fas fa-map-marker-alt mr-1"></i> OBJEK</dt>
                    <dd class="text-muted">
                        {{ $objek->kd_propinsi . '.' . $objek->kd_dati2 . '.' . $objek->kd_kecamatan . '.' . $objek->kd_kelurahan . '.' . $objek->kd_blok . '-' . $objek->no_urut . '.' . $objek->kd_jns_op }}<br>
                        {{ $objek->jalan_op . ' ' . $objek->no_persil . ' ' . $objek->blok_kav_no_op }}
                        {{ $objek->rt_op != '' ? 'RT ' . $objek->rt_op : '' }}
                        {{ $objek->rw_op != '' ? 'RW ' . $objek->rw_op : '' }}<br>
                        {{ $objek->nm_kelurahan }}, {{ $objek->nm_kecamatan }}<br>

                        @if ($objek->nop_asal != '' && Str::substr($objek->nop_asal, -5, 5) != 0000 && $objek->nop_asal != onlynumber($nop))
                            <b>NOP ASAL :</b><br> {{ formatnop($objek->nop_asal) }}
                        @endif

                        @if ($gabungke != null)
                            <b>MUTASI GABUNG KE :</b><br> {{ formatnop($gabungke->nop_proses) }}
                        @endif

                    </dd>
                    <hr>
                    <dt><i class="fas fa-book mr-1"></i> SUBJEK</dt>
                    <dd class="text-muted">
                        {{ $objek->subjek_pajak_id }}<br>
                        {{ $objek->nm_wp }}<br>
                        {{ $objek->jalan_wp . ' ' . $objek->blok_kav_no_wp }}
                        {{ $objek->rt_wp != '' ? 'RT ' . $objek->rt_wp : '' }}
                        {{ $objek->rw_wp != '' ? 'RW ' . $objek->rw_wp : '' }}<br>
                        {{ $objek->kelurahan_wp }}<br>
                        {{ $objek->kota_wp }}<br>
                        TELP: {{ $objek->telp_wp }}
                    </dd>
                </dl>
            </div>
        </div>
        @if (count($pemutakhiran) > 0)
            <div class="card card-outline card-danger">
                <div class="card-header p-1">
                    <div class="card-title">PERMOHONAN / PENDATAAN</div>
                </div>
                <div class="card-body p-1">
                    <table class="table table-sm text-sm" id="rw">
                        <thead>
                            <th>JENIS</th>
                            <th>PETUGAS</th>
                            <th width="10px"></th>
                        </thead>
                        <tbody>
                            @foreach ($pemutakhiran as $item)
                                <tr>
                                    <td> {{ $item->nama_layanan }}<br>
                                        <span class="text-info">{{ $item->jenis }} </span>
                                    </td>
                                    <td>{{ $item->created_by }}<br>
                                        <span class="text-danger">{{ tglIndo($item->created_at) }}</span>
                                    </td>
                                    <td class="text-center">
                                        @if ($item->nama_layanan != 'Perubahan ZNT')
                                            <a data-toggle="tooltip" data-placement="top" title="Detail Penelitian"
                                                class=" detail-objek" role="button" data-jenis="{{ $item->jenis }}"
                                                data-id="{{ $item->id }}" tabindex="0"><i
                                                    class="fas fa-2x fa-info-circle text-info"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        @if (count($pecah) > 0)
            <div class="card card-outline card-danger">
                <div class="card-header p-1">
                    <div class="card-title">Pecah Objek</div>
                </div>
                <div class="card-body p-1">
                    <table class="table table-sm text-sm" id="example2">
                        <thead>
                            <th>No</th>
                            <th>NOP</th>
                            <th>Tahun</th>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($pecah as $pc)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ formatnop($pc->nop_proses) }}
                                    </td>
                                    <td>{{ $pc->tahun }}
                                    </td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

        @if (count($gabung) > 0)
            <div class="card card-outline card-danger">
                <div class="card-header p-1">
                    <div class="card-title">Mutasi Gabung Dari</div>
                </div>
                <div class="card-body p-1">
                    <table class="table table-sm text-sm" id="example2">
                        <thead>
                            <th>No</th>
                            <th>NOP</th>
                            <th>Tahun</th>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($gabung as $gb)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ formatnop($gb->nop_asal) }}
                                    </td>
                                    <td>{{ $gb->tahun }}
                                    </td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
    <div class="col-md-8">
        <div class="row">
            @if (auth()->user()->is_wp() == false)
                <div class="col-md-6">
                    <table class="table table-borderless text-sm table-sm">
                        <tbody>
                            <tr style="vertical-align: top">
                                <td width="20%">STATUS</td>
                                <td width="1px">:</td>
                                <td>
                                    @if ($objek->jns_transaksi_op == '3')
                                        NON AKTIF
                                    @else
                                        {{ jenisBumi($objek->jns_bumi) }}
                                    @endif
                                </td>
                            </tr>
                            <tr style="vertical-align: top">
                                <td>ZNT</td>
                                <td>:</td>
                                <td>{{ $objek->kd_znt }}
                                    <a data-kd_znt='{{ $objek->kd_znt }}'
                                        data-kd_kecamatan="{{ $objek->kd_kecamatan }}"
                                        data-kd_kelurahan="{{ $objek->kd_kelurahan }}" data-toggle="tooltip"
                                        data-placement="top" title="Detail ZNT" id="znt_nir" role="button"
                                        tabindex="0"><i class="fas  fa-info-circle text-info"></i></a>
                                </td>
                            </tr>

                            <tr style="vertical-align: top">
                                <td>KEPEMILIKAN</td>
                                <td>:</td>
                                <td>{{ statusWP($objek->kd_status_wp) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @endif
            <div class="col-md-6">
                <table class="table table-bordered table-sm">
                    <thead class="bg-success">
                        <tr style="vertical-align: top">
                            <th class="text-center">Objek</th>
                            <th class="text-center">Luas</th>
                            <th class="text-center">NJOP/M<sup>2</sup></th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="vertical-align: top">
                            <td>Bumi</td>
                            <td class="text-center">{{ angka($objek->total_luas_bumi) }}</td>
                            <td class="text-right">
                                {{ angka($objek->total_luas_bumi > 0 ? round($objek->njop_bumi / $objek->total_luas_bumi) : 0) }}
                            </td>
                            <td class="text-right">{{ angka($objek->njop_bumi) }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td>Bangunan</td>
                            <td class="text-center">{{ angka($objek->total_luas_bng) }}</td>
                            <td class="text-right">
                                {{ angka($objek->total_luas_bng > 0 ? round($objek->njop_bng / $objek->total_luas_bng) : 0) }}
                            </td>
                            <td class="text-right">{{ angka($objek->njop_bng) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                @include('objek._bangunan', ['bangunan' => $bangunan])

                @if (count($sppt) > 0)
                    {{-- collapsed-card card-outline --}}
                    <div class="card card-outline  card-info">
                        <div class="card-header">
                            <h3 class="card-title">SPPT <br>
                                @if (auth()->user()->is_wp() == false)
                                    <small><a href="#" data-toggle="tooltip" data-placement="top"
                                            data-nop='{{ $nop }}' title="History oembayaran"
                                            id="data_pembayaran" role="button" tabindex="0">[History
                                            Pembayaran]</a></small>
                                @endif

                            </h3>
                            <div class="card-tools">
                                {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                            </button> --}}
                            </div>
                        </div>
                        <div class="card-body p-0">
                            @include('tracking_pembayaran._riwayatnjop', [
                                'data' => $sppt,
                                'nop' => $nop,
                                'tahun_nota' => $tahun_nota,
                                'show_sppt' => $show_sppt,
                            ])
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl" style="max-width: 98%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">DETAIL PENELITIAN / PENDATAAN</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-znt">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Informasi NIR</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-riwayat">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Riwayat Pembayaran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "responsive": true,
            "pagingType": "simple"
        });


        $('#data_pembayaran').on('click', function(e) {
            e.preventDefault()
            let nop = $(this).data('nop')
            $('.modal-body').html('')
            Swal.fire({
                // icon: 'error',
                title: '<i class="fas fa-sync fa-spin"></i>',
                text: "Sedang memuat data.........",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false
            })
            $.ajax({
                url: "{{ url('informasi/riwayat') }}",
                data: {
                    nop
                },
                success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-riwayat").modal('show');
                },
                error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error',
                        title: 'Peringatan',
                        text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    })
                    $('.modal-body').html('')
                }
            });
        });

        $('#znt_nir').on('click', function(e) {
            e.preventDefault()
            let kd_kecamatan = $(this).data('kd_kecamatan')
            let kd_kelurahan = $(this).data('kd_kelurahan')
            let kd_znt = $(this).data('kd_znt')
            $('.modal-body').html('')
            Swal.fire({
                // icon: 'error',
                title: '<i class="fas fa-sync fa-spin"></i>',
                text: "Sedang memuat data.........",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false
            })
            $.ajax({
                url: "{{ url('detail-nir') }}",
                method: 'post',
                data: {
                    kd_kecamatan,
                    kd_kelurahan,
                    kd_znt
                },
                success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-znt").modal('show');
                },
                error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error',
                        title: 'Peringatan',
                        text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    })
                    $('.modal-body').html('')
                }
            });
        });


        $('#rw').on('click', '.detail-objek', function(e) {
            e.preventDefault()
            let id = $(this).data('id')
            let jenis = $(this).data('jenis')
            let url_detail
            if (jenis == 'PERMOHONAN') {
                url_detail = "{{ url('penelitian/kantor') }}/" + id
            } else {
                url_detail = "{{ url('pendataan/show-text') }}/" + id
            }

            $('.modal-body').html('')
            Swal.fire({
                // icon: 'error',
                title: '<i class="fas fa-sync fa-spin"></i>',
                text: "Sedang memuat data.........",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false
            })
            $.ajax({
                url: url_detail,
                success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-xl").modal('show');
                },
                error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error',
                        title: 'Peringatan',
                        text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    })
                    $('.modal-body').html('')
                }
            });
        })
    })
</script>
