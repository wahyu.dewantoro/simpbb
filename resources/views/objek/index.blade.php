@extends('layouts.app')
@section('style')
    <style>

    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data dan Informasi Objek Pajak</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-left">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline card-tabs">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group input-group-sm">
                                @if ($is_wp == false)
                                    <input autofocus="true" required type="text" name="nop" id="nop"
                                        autofocus="true" value="{{ request()->get('nop') ?? '' }}"
                                        class="form-control form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}"
                                        placeholder="Masukan nomor objek pajak (NOP)">
                                @else
                                    <select name="nop" id="nop"
                                        class="form-control form-control-sm select-nop {{ $errors->has('nop') ? 'is-invalid' : '' }}">
                                        <option value=""></option>
                                    </select>
                                @endif
                                <span class="input-group-append">
                                    <button id="cek" type="button" class="btn btn-sm btn-success"> <i
                                            class="fas fa-search"></i> </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if ($is_wp == true && count($nop) == 0 && Auth()->user()->is_pengelola == '1')
                                <br><br>
                                <div class="callout callout-danger">
                                    <h5><i class="fas fa-bullhorn text-danger"></i> Pemberitahuan</h5>
                                    <p>Belum ada NOP yang terdaftar atau dikelola. Silakan melakukan registrasi untuk NOP
                                        yang Anda kelola. </p>
                                    <a href="{{ url('kelola-nop') }}" style="text-decoration:none;"
                                        class="btn btn-sm btn-info "><i class="fas fa-box-open"></i>
                                        Kelola NOP</a>
                                </div>
                            @endif
                            <div id="hasil">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $('.select-nop').select2({
                placeholder: 'Cari nop . . .',
                allowClear: true,
                width: 'resolve',
                language: {
                    searching: function() {
                        return "Sedang mencari..."; // Ubah teks pencarian di sini
                    }
                },
                ajax: {
                    url: '{{ url('daftar-objek-wp') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term // parameter query untuk pencarian
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                templateResult: function(data) {
                    if (!data.id) {
                        return data.text;
                    }
                    // Tampilkan ID di depan nama di dropdown
                    return $('<span>' + formatnop(data.id) + ' - ' + data.text + '</span>');
                },
                templateSelection: function(data) {
                    if (!data.id) {
                        return data.text;
                    }
                    // Tampilkan ID di depan nama saat item dipilih
                    return formatnop(data.id) + ' - ' + data.text;
                }
            });

            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    $('#cek').trigger('click');
                }
            });

            $('#nop').trigger('keyup');

            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });
            $('#hasil_kecamatan').html('');
            $('#hasil').html('')

            $('#cek').on('click', function(e) {
                $('#hasil_kecamatan').html('');
                e.preventDefault();
                openloading();
                $.ajax({
                    url: "{{ url('informasi/objek-pajak-detail') }}",
                    data: {
                        nop: $('#nop').val()
                    },
                    success: function(res) {
                        $('#hasil').html(res)
                        closeloading()
                    },
                    error: function(e) {
                        closeloading()
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan',
                            text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                        })
                        $('#hasil').html('')
                    }
                });
            });


        });
    </script>
@endsection
