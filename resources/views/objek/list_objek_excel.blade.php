<table class="table-sm table table-bordered" id="table-data">
    <thead>
        <tr>
            <th class="text-center">NOP</th>
            <th>Alamat OP</th>
            <th class="text-center">Wajib Pajak</th>
            <th>Alamat WP</th>
            <th>ZNT</th>
            <th>NIR</th>
            <th>Lokasi ZNT</th>
            <th class="text-center">L. Bumi</th>
            <th class="text-center">NJOP Bumi</th>
            <th class="text-center">L. BNG</th>
            <th class="text-center">NJOP BNG</th>
            <th class="text-center">TOTAL NJOP</th>
            <th class="text-center">NJOPTKP</th>
            <th class="text-center">Tarif</th>
            <th class="text-center">PBB </th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{{ $row->nop_format }}</td>
                <td>{{ $row->alamat_op . '  ' . $row->rt_op . '  ' . $row->rw_op . '  ' . $row->kelurahan_op . '  ' . $row->kecamatan_op }}
                </td>
                <td>{{ $row->nm_wp }}</td>
                <td>{{ $row->alamat_wp }} RT {{ $row->rt_wp }} RW {{ $row->rw_wp }} </td>
                <td>{{ $row->kd_znt }}</td>
                <td>{{ $row->nir }}</td>
                <td>{{ $row->nama_lokasi }}</td>
                <td>{{ $row->total_luas_bumi }}</td>
                <td>{{ $row->njop_bumi }}</td>
                <td>{{ $row->total_luas_bng }}</td>
                <td>{{ $row->njop_bng }}</td>
                <td>{{ $row->njop_pbb }}</td>
                <td>{{ $row->njoptkp_pbb }}</td>
                <td>{{ $row->nilai_tarif }}</td>
                <td>{{ $row->pbb_tahun_berjalan }}</td>
                <td>{{ $row->transaksi . ' , ' . $row->status_peta }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
