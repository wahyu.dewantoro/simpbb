@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Detail Riwayat Pembayaran</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-left">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card card-primary card-outline card-tabs">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="input-group input-group-sm">
                            <input autofocus="true" required type="text" name="nop" id="nop" autofocus="true" value="{{ request()->get('nop') ?? '' }}" class="form-control form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}" placeholder="Masukan nomor objek pajak (NOP)">
                            <span class="input-group-append">
                                <button id="cek" type="button" class="btn btn-sm btn-success"> <i class="fas fa-search"></i> </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="hasil"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>
@endsection
@section('script')
<script>
    $(document).ready(function() {


        $(document).on('keypress', function(e) {
            if (e.which == 13) {
                // alert('You pressed enter!');
                $('#cek').trigger('click');
            }
        });

        $('#nop').trigger('keyup');

        $('#nop').on('keyup', function() {
            var nop = $(this).val();
            var convert = formatnop(nop);
            $(this).val(convert);
        });
        $('#hasil_kecamatan').html('');
        $('#hasil').html('')

        $('#cek').on('click', function(e) {
            $('#hasil_kecamatan').html('');
            e.preventDefault();
            openloading();
            $.ajax({
                url: "{{ url('informasi/riwayat-pembayaran') }}"
                , data: {
                    nop: $('#nop').val()
                }
                , success: function(res) {
                    $('#hasil').html(res)
                    closeloading()
                }
                , error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('#hasil').html('')
                }
            });
        });

 
    });

</script>
@endsection
