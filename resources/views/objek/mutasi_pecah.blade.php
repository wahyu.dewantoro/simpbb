@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Mutasi Pecah</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-left">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline card-tabs">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan" required
                                id="kd_kecamatan">
                                @if (count($kecamatan) > 1)
                                    <option value="">-- kecamatan --</option>
                                @endif
                                @foreach ($kecamatan as $rowkec)
                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                        value="{{ $rowkec->kd_kecamatan }}">
                                        {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan" id="kd_kelurahan">
                                <option value="">-- Kelurahan / Desa -- </option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button id='cetak_pdf' disabled class="btn btn-sm btn-flat btn-warning"><i
                                    class="far fa-file-pdf"></i> PDF</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="hasil_kecamatan"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header p-1">
                    <h4 class="modal-title">Detail Objek</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0 m-0">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $('#hasil_kecamatan').html('');

            getKelurahan($('#kd_kecamatan').val());

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            function getKelurahan(kk) {
                var html = '<option value="">-- pilih --</option>';
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }

                            $('#kd_kelurahan').trigger('change');

                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }
            }


            $('#kd_kecamatan,#kd_kelurahan').on('change', function() {
                kd_kecamatan = $('#kd_kecamatan').val();
                kd_kelurahan = $('#kd_kelurahan').val();
                showData(kd_kecamatan, kd_kelurahan)
            });


            $('#cetak_pdf').on('click', function(e) {
                e.preventDefault();
                kd_kecamatan = $('#kd_kecamatan').val();
                kd_kelurahan = $('#kd_kelurahan').val();

                window.open("{{ route('informasi.mutasi-pecah.pdf') }}?kd_kecamatan=" + kd_kecamatan +
                    "&kd_kelurahan=" + kd_kelurahan, "_blanl")
            })

            function showData(kd_kecamatan, kd_kelurahan) {
                $('#hasil_kecamatan').html('')
                $('#cetak_pdf').prop('disabled', true);
                if (kd_kecamatan != '' && kd_kelurahan != '') {
                    openloading()
                    $.ajax({
                        url: "{{ url('/informasi/mutasi-pecah') }}",
                        data: {
                            kd_kecamatan: kd_kecamatan,
                            kd_kelurahan: kd_kelurahan,
                            show: "{{ $show }}"
                        },
                        success: function(res) {
                            $('#cetak_pdf').prop('disabled', false);
                            $('#hasil_kecamatan').html(res)
                            closeloading();
                        },
                        error: function(e) {
                            $('#cetak_pdf').prop('disabled', true);
                            Swal.fire({
                                icon: 'error',
                                title: 'Peringatan',
                                text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                            })
                            $('#hasil_kecamatan').html('')
                            closeloading();
                        }
                    });
                }
            }


            /* $('#cek_kecamatan').on('click', function(e) {
                $('#hasil_kecamatan').html('')
                e.preventDefault();
                openloading()
                $.ajax({
                    url: "{{ url('informasi/objek-pajak-list') }}"
                    , data: {
                        kd_kecamatan: $('#kd_kecamatan').val()
                    }
                    , success: function(res) {
                        $('#hasil_kecamatan').html(res)
                        closeloading();
                    }
                    , error: function(e) {
                        Swal.fire({
                            icon: 'error'
                            , title: 'Peringatan'
                            , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                            , allowOutsideClick: false
                            , allowEscapeKey: false
                        , })
                        $('#hasil_kecamatan').html('')
                        closeloading();
                    }
                });
            }); */
        });
    </script>
@endsection
