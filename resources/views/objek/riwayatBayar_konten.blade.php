<style>
    .table-sm td,
    .table-sm th {
        padding: 1px;
        font-size: 11px;
    }
</style>
<div class="row">
    @if ($objek != null)
        <div class="col-md-3">
            <div class="card card-outline card-success">

                <div class="card-body p-1">
                    <dl>
                        <dt><i class="fas fa-map-marker-alt mr-1"></i> OBJEK</dt>
                        <dd class="text-muted">
                            {{ $objek->kd_propinsi . '.' . $objek->kd_dati2 . '.' . $objek->kd_kecamatan . '.' . $objek->kd_kelurahan . '.' . $objek->kd_blok . '-' . $objek->no_urut . '.' . $objek->kd_jns_op }}<br>
                            {{ $objek->jalan_op . ' ' . $objek->no_persil . ' ' . $objek->blok_kav_no_op }}
                            {{ $objek->rt_op != '' ? 'RT ' . $objek->rt_op : '' }}
                            {{ $objek->rw_op != '' ? 'RW ' . $objek->rw_op : '' }}<br>
                            {{ $objek->nm_kelurahan }}, {{ $objek->nm_kecamatan }}<br>

                            @if ($objek->nop_asal != '' && Str::substr($objek->nop_asal, -5, 5) != 0000 && $objek->nop_asal != onlynumber($nop))
                                <b>NOP ASAL :</b><br> {{ formatnop($objek->nop_asal) }}
                            @endif
                        </dd>
                        <hr>
                        <dt><i class="fas fa-book mr-1"></i> SUBJEK</dt>
                        <dd class="text-muted">
                            {{ $objek->subjek_pajak_id }}<br>
                            {{ $objek->nm_wp }}<br>
                            {{ $objek->jalan_wp . ' ' . $objek->blok_kav_no_wp }}
                            {{ $objek->rt_wp != '' ? 'RT ' . $objek->rt_wp : '' }}
                            {{ $objek->rw_wp != '' ? 'RW ' . $objek->rw_wp : '' }}<br>
                            {{ $objek->kelurahan_wp }}<br>
                            {{ $objek->kota_wp }}<br>
                            TELP: {{ $objek->telp_wp }}
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-9">
        <div class="float-right">
            {{-- <button></button> --}}
            <a href="{{ url('informasi/riwayat-pembayaran-cetak') }}?nop={{ $objek->kd_propinsi . '.' . $objek->kd_dati2 . '.' . $objek->kd_kecamatan . '.' . $objek->kd_kelurahan . '.' . $objek->kd_blok . '-' . $objek->no_urut . '.' . $objek->kd_jns_op }}"
                class="btn btn-sm btn-primary">Cetak</a>
        </div>
        <table class="table table-sm table-borderless" style="border:1px solid grey">
            <thead>
                <th class="text-center" width="10%">Tahun Pajak</th>
                <th class="text-center" width="10%">PBB</th>
                <th class="text-left">Tanggal</th>
                <th class="text-center">Pokok</th>
                <th class="text-center">Denda</th>
                <th class="text-center">Jumlah</th>
                <th class="text-center">Keterangan</th>
            </thead>
            <tbody>
                @php
                    $tmpthn = '';
                    $tmppokok = 0;
                @endphp
                @foreach ($bayar as $row)
                    @php

                        if ($tmpthn != $row->thn_pajak_sppt) {
                            $tmppokok = 0;
                        }
                        $tmppokok += $row->pokok;
                    @endphp
                    <tr @if ($tmpthn != $row->thn_pajak_sppt) style="border-top:1px solid grey" @endif>
                        @php
                            $tahun_pj = $tmpthn != $row->thn_pajak_sppt ? $row->thn_pajak_sppt : '';
                        @endphp
                        <td class="text-center">{{ $tahun_pj }}
                            @if ($tahun_pj != '' && $row->tgl_terbit_sppt != '')
                                <br><span class="text-info">{{ tglIndo($row->tgl_terbit_sppt) }}</span>
                            @endif
                        </td>

                        <td class="text-center">{{ $tmpthn != $row->thn_pajak_sppt ? angka($row->pbb) : '' }} </td>
                        <td class="text-left "> {{ $row->pembayaran_ke . '. ' . tglIndo($row->tgl_bayar) }} </td>
                        <td class="text-right">{{ $row->tgl_bayar != '' ? angka($row->pokok) : '' }}</td>
                        <td class="text-right">{{ $row->tgl_bayar != '' ? angka($row->denda) : '' }}</td>
                        <td class="text-right">{{ $row->tgl_bayar != '' ? angka($row->jumlah) : '' }}</td>
                        <td class="text-right">
                            @php
                                $ket = '';
                                if ($row->no_koreksi != '') {
                                    $ket .= $row->no_koreksi . ' <br>' . $row->keterangan_koreksi . '<br>';
                                }

                                if ($tmppokok > $row->pbb) {
                                    $ket .= 'Lebih Bayar<br>';
                                }

                                if ($tmppokok == $row->pbb) {
                                    $ket .= 'Lunas<br>';
                                }

                                if ($row->nop_pecahan != '') {
                                    $ket .= 'Dari pembayaran ' . formatnop($row->nop_pecahan) . '<br>';
                                }

                                if ($ket != '') {
                                    $ket = substr($ket, 0, -4);
                                }
                            @endphp



                            <?= $ket ?>

                        </td>
                    </tr>
                    @php
                        $tmpthn = $row->thn_pajak_sppt;
                    @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>



<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    })
</script>
