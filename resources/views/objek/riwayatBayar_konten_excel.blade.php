<table class="table table-sm table-borderless">
    <thead>
        <tr>
            <th>Tahun Pajak</th>
            <th>PBB</th>
            <th>Tanggal</th>
            <th class="text-center">Pokok</th>
            <th class="text-center">Denda</th>
            <th class="text-center">Jumlah</th>
            <th class="text-center">Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @php
            $tmpthn = '';
            $tmppokok = 0;
        @endphp
        @foreach ($bayar as $row)
            @php

                if ($tmpthn != $row->thn_pajak_sppt) {
                    $tmppokok = 0;
                }
                $tmppokok += $row->pokok;
            @endphp
            <tr @if ($tmpthn != $row->thn_pajak_sppt) ale="border-top:1px solid grey" @endif>
                <td class="text-center">{{ $tmpthn != $row->thn_pajak_sppt ? $row->thn_pajak_sppt : '' }}</td>

                <td class="text-center">{{ $tmpthn != $row->thn_pajak_sppt ? $row->pbb : '' }} </td>
                <td class="text-left "> {{ $row->pembayaran_ke . '. ' . tglIndo($row->tgl_bayar) }} </td>
                <td class="text-right">{{ $row->tgl_bayar != '' ? $row->pokok : '' }}</td>
                <td class="text-right">{{ $row->tgl_bayar != '' ? $row->denda : '' }}</td>
                <td class="text-right">{{ $row->tgl_bayar != '' ? $row->jumlah : '' }}</td>
                <td class="text-right">
                    @php
                        $ket = '';
                     /*    if ($row->no_koreksi != '') {
                            $ket .= $row->no_koreksi;
                            // ' <br>' . $row->keterangan_koreksi . '<br>';
                        } */

                        if ($tmppokok > $row->pbb) {
                            $ket .= 'Lebih Bayar<br>';
                        }

                        if ($tmppokok == $row->pbb) {
                            $ket .= 'Lunas<br>';
                        }

                        if ($row->nop_pecahan != '') {
                            $ket .= 'Dari pembayaran ' . formatnop($row->nop_pecahan) . '<br>';
                        }

                        if ($ket != '') {
                            $ket = substr($ket, 0, -4);
                        }
                    @endphp



                    <?= $ket ?>

                </td>
            </tr>
            @php
                $tmpthn = $row->thn_pajak_sppt;
            @endphp
        @endforeach
    </tbody>
</table>
