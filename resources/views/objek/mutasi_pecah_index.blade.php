<table class="table table-sm table-bordered text-sm" id="example2">
    <thead class="bg-info">
        <tr>
            <th>NOP</th>
            <th>Alamat OP</th>
            <th>Wajib Pajak</th>
            <th>Bumi</th>
            <th>BNG</th>
            <th>Status</th>
            <th>Tgl Penelitian</th>
            <th>Keterangan</th>
            {{-- @if ($cetak == '0' && $show == '1')
            <th>Detail</th>
            @endif --}}
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr @if ($row->nop_asal == '') class="table-info" @endif>
                <td>{{ $row->nop_format }}</td>
                <td>{{ $row->alamat_op }}</td>
                <td>{{ $row->nm_wp }}</td>
                <td>{{ $row->total_luas_bumi }}</td>
                <td>{{ $row->total_luas_bng }}</td>
                <td>{{ $row->status_peta }}</td>
                <td>{{ tglIndo($row->tgl_perekaman_op) }}</td>
                <td>{{ $row->keterangan }}
                    @if ($row->nop_asal != '')
                        <br> Dari {{ formatnop($row->nop_asal) }}
                    @endif
                </td>
                {{-- @if ($cetak == '0' && $show == '1')
            <td class="text-center">
                <a href="#" data-nop="{{ $row->nop_asli }}" class="detail"><i class="fas fa-binoculars text-success"></i></a>
            </td>
            @endif --}}
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function() {

        $('.detail').on('click', function(e) {
            e.preventDefault()
            nop = $(this).data('nop')
            nopDetail(nop)
        })

        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });

        function nopDetail(nop) {
            // console.log(nop)
            $('.modal-body').html('')
            openloading();
            $.ajax({
                url: "{{ url('informasi/objek-pajak-show') }}",
                data: {
                    nop: nop
                },
                success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-xl").modal('show');
                },
                error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error',
                        title: 'Peringatan',
                        text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                    })
                    $('.modal-body').html('')
                }
            });
        }
    })
</script>
