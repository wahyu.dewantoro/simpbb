@if (count($bangunan) > 0)
<div class="card  collapsed-card card-outline card-info">
    <div class="card-header">
        <h3 class="card-title">Data Bangunan</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        @foreach ($bangunan as $idg => $bng)

        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-sm">
                    <tbody>
                        <tr class="bg-success">
                            <td class="text-center"><strong>Bangunan Ke {{ $idg + 1 }}</strong></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-borderless table-sm">
                                            <tbody>
                                                <tr>
                                                    <td width="35%">Jenis Bng</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ penggunaanBangunan($bng->kd_jpb) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Luas Bng</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ angka($bng->luas_bng) }} M<sup>2</sup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Lantai</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $bng->jml_lantai_bng }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Thn Dibangun</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $bng->thn_dibangun_bng }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Thn Renovasi</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $bng->thn_renovasi_bng }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Kondisi Bng</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ kondisi($bng->kondisi_bng) }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td width="35%">Kontruksi</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ kontruksi($bng->jns_konstruksi_bng) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Atap</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ atap($bng->jns_atap_bng) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Dinding</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ dinding($bng->kd_dinding) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Lantai</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ lantai($bng->kd_lantai) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Langit langit</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ langit($bng->kd_langit_langit) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Daya Listrik</td>
                                                    <td>:</td>
                                                    <td>{{ angka($bng->daya_listrik) }} Watt</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        {{-- <tr class="bg-success">
                    <td class="text-center">Fasilitas</td>
                </tr> --}}
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table table-sm table-borderless">
                                                    <thead>
                                                        <tr class="bg-info">
                                                            <th colspan="3">Kolam Renang</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td width="35%">Luas (M<sup>2</sup>)</td>
                                                            <td>:</td>
                                                            <td>{{ angka($bng->luas_kolam) }} </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="35%">Finishing</td>
                                                            <td>:</td>
                                                            <td>
                                                                @if(angka($bng->luas_kolam)!=0)
                                                                {{ $bng->finishing_kolam=='1'?'Di Plester':'Dengan pelapis' }}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table table-sm table-borderless">
                                                    <thead>
                                                        <tr class="bg-info">
                                                            <th style="text-align: center" colspan="3">AC</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td width="50%">Split</td>
                                                            <td width='1px'>:</td>
                                                            <td>
                                                                {{ $bng->acsplit }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Window</td>
                                                            <td width='1px'>:</td>
                                                            <td>
                                                                {{ $bng->acwindow }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>AC Sentral</td>
                                                            <td width='1px'>:</td>
                                                            <td>
                                                                {{ $bng->acsentral=='1'?'Ada':'Tidak Ada' }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                <tr class="bg-info">
                                                    <th colspan="7">Luas perkerasan halaman (M<sup>2</sup>)</th>
                                                </tr>
                                                <tr>
                                                    <td width="90px">Ringan</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ angka($bng->luas_perkerasan_ringan) }}
                                                    </td>
                                                    <td wudth="10px"></td>
                                                    <td width="90px">Berat</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ angka($bng->luas_perkerasan_berat) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Sedang</td>
                                                    <td>:</td>
                                                    <td>
                                                        {{ angka($bng->luas_perkerasan_sedang) }}
                                                    </td>
                                                    <td></td>
                                                    <td>Dg Penutup</td>
                                                    <td>:</td>
                                                    <td>
                                                        {{ angka($bng->luas_perkerasan_dg_tutup) }}
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                        <table class="table table-sm table-bordered">
                                            <thead>
                                                <tr class="bg-info">
                                                    <th>Langan Tenis</th>
                                                    <th>Dg Lampu</th>
                                                    <th>Tanpa Lampu</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Beton</td>
                                                    <td style="text-align: center">{{ $bng->lap_tenis_lampu_beton }}
                                                    </td>
                                                    <td style="text-align: center">{{ $bng->lap_tenis_beton }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Aspal</td>
                                                    <td style="text-align: center">{{ $bng->lap_tenis_lampu_aspal }}
                                                    </td>
                                                    <td style="text-align: center">{{ $bng->lap_tenis_aspal }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanah Liat/Rumput</td>
                                                    <td style="text-align: center">{{ $bng->lap_tenis_lampu_rumput }}
                                                    </td>
                                                    <td style="text-align: center">{{ $bng->lap_tenis_rumput }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                <tr class="bg-info">
                                                    <th colspan="3">LIFT</th>
                                                </tr>
                                                <tr>
                                                    <td width="30px">Penumpang</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $bng->lift_penumpang }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="">Barang</td>
                                                    <td width="">:</td>
                                                    <td>{{ $bng->lift_barang }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="">Kapsul</td>
                                                    <td width="">:</td>
                                                    <td>{{ $bng->lift_kapsul }}</td>
                                                </tr>

                                            </tbody>
                                        </table>

                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                <tr class="bg-info">
                                                    <th colspan="2">Tangga berjalan dengan lebar</th>
                                                </tr>
                                                <tr>
                                                    <td width="50%">
                                                        <=0.80 M : {{ $bng->tgg_berjalan_a }}</td>
                                                    <td> >0.80 M : {{ $bng->tgg_berjalan_b }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Panjang Pagar</td>
                                                    <td>: {{ $bng->pjg_pagar }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Bahan Pagar</td>
                                                    <td>: {{ $bng->bhn_pagar }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                <tr class="bg-info">
                                                    <td colspan="3">Pemadam Kebakaran</td>
                                                </tr>
                                                <tr>
                                                    <td width="200px">Hydrant :
                                                        {{ $bng->hydrant == '1' ? 'V' : 'X' }}</td>
                                                    <td width="200px">Sprinkler :
                                                        {{ $bng->sprinkler == '1' ? 'V' : 'X' }}
                                                    </td>
                                                    <td width="200px">Fire Alarm :
                                                        {{ $bng->fire_alarm == '1' ? 'V' : 'X' }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah PABX</td>
                                                    <td colspan="3">: {{ $bng->jml_pabx }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Sumur artesis</td>
                                                    <td colspan="3">: {{ $bng->sumur_artesis }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    @if ($bng->kd_jpb == '03' || $bng->kd_jpb == '08')
                                    <div class="col-md-12">
                                        <div class="card card-outline card-success">
                                            <div class="card-header p-1">
                                                <p class="card-title">Data tambahan untuk bangunan
                                                    {{ penggunaanBangunan(3) }} Atau
                                                    {{ penggunaanBangunan(8) }}</p>
                                            </div>
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <table class="table table-borderless table-sm text-sm">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Tinggi Kolom</td>
                                                                    <td>: {{ $bng->jpb3_8_tinggi_kolom }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Daya Dukung Lantai</td>
                                                                    <td>: {{ $bng->jpb3_8_dd_lantai }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <table class="table table-borderless table-sm text-sm">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Lebar Bentang</td>
                                                                    <td>: {{ $bng->jpb3_8_lebar_bentang }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Keliling Dinding</td>
                                                                    <td>: {{ $bng->jpb3_8_kel_dinding }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <table class="table table-borderless table-sm text-sm">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Luas MEZZANINE M<sup>2</sup></td>
                                                                    <td>: {{ $bng->jpb3_8_mezzanine }}</td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if (in_array($bng->kd_jpb, ['02', '09', '05', '06', '07', '12', '13', '15', '16']))
                                    <div class="col-md-12">
                                        <div class="card card-outline card-warning">
                                            <div class="card-header p-1">
                                                <p class="card-title">Data tambahan untuk bangunan NON Standart
                                                </p>
                                            </div>
                                            <div class="card-body p-1">
                                                @if (!in_array($bng->kd_jpb, ['15', '07']))
                                                <p>Kelas Bangunan : Kelas @if (in_array($bng->kd_jpb, ['02', '09', '04', '06']))
                                                    {{ $bng->jpb_lain_kls_bng }}
                                                    @else
                                                    @if ($bng->kd_jpb == '05')
                                                    {{ $bng->jpb5_kls_bng }}
                                                    @endif

                                                    @if ($bng->kd_jpb == '13')
                                                    {{ $bng->jpb13_kls_bng }}
                                                    @endif
                                                    @endif
                                                </p>
                                                @endif

                                                @if ($bng->kd_jpb == '05')
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p>LUAS KMR DGN AC SENTRAL (M<sup>2</sup>) :
                                                            {{ $bng->jpb5_luas_kamar }}</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p>LUAS RUANG LAIN DGN AC SENTRAL (M<sup>2</sup>) :
                                                            {{ $bng->jpb5_luas_rng_lain }}</p>
                                                    </div>
                                                </div>
                                                @endif

                                                @if ($bng->kd_jpb == '07')
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>

                                                                <tr>
                                                                    <td>Jenis Hotel</td>
                                                                    <td>:
                                                                        {{ jenisHotel($bng->jpb7_jns_hotel) }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jumlah Bintang</td>
                                                                    <td>:
                                                                        {{ bintangHotel($bng->jpb7_bintang) }}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jumlah Kamar</td>
                                                                    <td>: {{ $bng->jpb7_jml_kamar }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Luas Kamar dengan AC Sentral
                                                                        M<sup>2</sup></td>
                                                                    <td>: {{ $bng->jpb7_luas_kamar }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Luas Ruang lain dengan AC Sentral
                                                                        M<sup>2</sup></td>
                                                                    <td>: {{ $bng->jpb7_luas_rng_lain }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                @endif
                                                @if ($bng->kd_jpb == '13')
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jumlah Apartemen</td>
                                                                    <td>: {{ $bng->jpb13_jml }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Luas Kamar dengan AC Sentral
                                                                        M<sup>2</sup></td>
                                                                    <td>: {{ $bng->jpb13_luas_kamar }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Luas Ruang lain dengan AC Sentral
                                                                        M<sup>2</sup></td>
                                                                    <td>: {{ $bng->jpb13_luas_rng_lain }}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                @endif

                                                @if ($bng->kd_jpb == '15')
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Kapasitas Tangki M<sup>3</sup></td>
                                                                    <td>: {{ $bng->jpb15_kapasitas_tangki }}
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <table class="table-sm table-borderless table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Letak Tangki</td>
                                                                    <td>: {{ $bng->jpb15_letak_tangki }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        {{-- @if(!empty($bng->nip_pemeriksa_bng))
                <tr class="bg-success">
                    <td class="text-center">Informasi perekaman</td>
                </tr>
                <tr>
                    <td>
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td>Jenis Transaksi</td>
                                    <td>: {{ jenisTransaksiBangunan($bng->jns_transaksi) }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Pendataan</td>
                            <td>: {{ tglIndo($bng->tgl_pendataan_bng) }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Penelitian</td>
                            <td>: {{ tglIndo($bng->tgl_pemeriksaan_bng) }}</td>
                        </tr>
                        <tr>
                            <td>Nomor Formulir</td>
                            <td>: {{ $bng->no_formulir }}</td>
                        </tr>
                        <tr>
                            <td>NIP Pendata</td>
                            <td>: {{ $bng->nip_pendata_bng }}</td>
                        </tr>
                        <tr>
                            <td>NIP Peneliti</td>
                            <td>: {{ $bng->nip_pemeriksa_bng }}</td>
                        </tr>
                    </tbody>
                </table>
                </td>
                </tr>
                @endif --}}
                </tbody>
                </table>
            </div>




        </div>

        @endforeach
    </div>
</div>
@endif
