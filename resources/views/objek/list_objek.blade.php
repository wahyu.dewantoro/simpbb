<div class="row">
    <div class="col-12">
        <table class="table-sm table table-bordered" id="table-data">
            <thead>
                <tr>
                    <th style="width:100px" class="text-center">NOP</th>
                    <th style="width:150px" class="text-center">Wajib Pajak</th>
                    <th>Alamat OP</th>
                    <th style="width:50px" class="text-center">L. Bumi</th>
                    <th style="width:50px" class="text-center">L. BNG</th>
                    <th style="width:90px">Keterangan</th>
                    <th style="width:50px" class="text-center">PBB </th>
                    <th style="width:30px">Detail</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header p-1">
                <h4 class="modal-title">Detail Objek</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        // console.log("ready!");
        function formatRupiah(angka) {
            // console.log(angka)
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return rupiah;

        }


        $('#table-data').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            ajax: "{!! url('informasi/objek-pajak-list-data') .
                '?kd_kecamatan=' .
                $kd_kecamatan .
                '&kd_kelurahan=' .
                $kd_kelurahan .
                '&subjek_pajak=' .
                $subjek_pajak !!}",
            columns: [{
                data: 'nop_format',
                orderable: false,
                name: 'nop_format',

            }, {
                data: 'nm_wp',
                orderable: false,
                name: 'nm_wp',
                render: function(data, type, row) {
                    return '<small>' + data + '<br>'+row.nik+'</small>';
                }
            }, {
                data: 'alamat_op',
                orderable: false,
                name: 'alamat_op'
            }, {
                data: 'total_luas_bumi',
                orderable: false,
                name: 'total_luas_bumi',
                class: 'text-right',
                render: function(data, type, row) {
                    if (data == '' || data == null) {
                        data = '0';
                    }
                    return formatRupiah(data);
                }
            }, {
                data: 'total_luas_bng',
                orderable: false,
                name: 'total_luas_bng',
                class: 'text-right',
                render: function(data, type, row) {
                    if (data == '' || data == null) {
                        data = '0';
                    }
                    return formatRupiah(data);
                }
            }, {
                data: 'transaksi',
                orderable: false,
                name: 'transaksi',
                render: function(data, type, row) {
                    return '<small>' + data + '<br><span class="text-danger">' + row
                        .status_peta + '</span></small>';
                }
            }, {
                data: 'pbb_tahun_berjalan',
                orderable: false,
                name: 'pbb_tahun_berjalan',
                class: 'text-right',
                render: function(data, type, row) {
                    if (data == '' || data == null) {
                        data = '0';
                    }
                    return formatRupiah(data);
                }
            }, {
                data: 'detail',
                class: 'text-right',
                orderable: false,


            }],
            "order": [],
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            bAutoWidth: false
        });




    });

    function nopDetail(nop) {
        // console.log(nop)
        $('.modal-body').html('')
        openloading();
        $.ajax({
            url: "{{ url('informasi/objek-pajak-detail') }}",
            data: {
                nop: nop
            },
            success: function(res) {
                $('.modal-body').html(res)
                closeloading()
                $("#modal-xl").modal('show');
            },
            error: function(e) {
                closeloading()
                Swal.fire({
                    icon: 'error',
                    title: 'Peringatan',
                    text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                })
                $('.modal-body').html('')
            }
        });
    }
</script>
