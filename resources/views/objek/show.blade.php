<style>
    .table-sm td,
    .table-sm th {
        padding: .1em;
    }

</style>
<div class="card card-primary card-tabs">
    {{-- <div class="card-header p-0 pt-1"> --}}
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Objek Pajak</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Subjek Pajak</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="custom-content-above-messages-tab" data-toggle="pill" href="#custom-content-above-messages" role="tab" aria-controls="custom-content-above-messages" aria-selected="false">Data Bangunan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="custom-content-above-njop-tab" data-toggle="pill" href="#custom-content-above-njop" role="tab" aria-controls="custom-content-above-njop" aria-selected="false">Riwayat NJOP</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" id="custom-content-above-settings-tab" data-toggle="pill" href="#custom-content-above-settings" role="tab" aria-controls="custom-content-above-settings" aria-selected="false">SPPT</a>
            </li> --}}
        </ul>
    </div>
    <div class="card-body p-0">
        <div class="tab-content p-0" id="custom-content-above-tabContent">
            <div class="tab-pane fade show active " id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table-bordered">
                            <tbody>
                                <tr>
                                    <th class="bg-success text-center">OBJEK PAJAK </th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <table class="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td width="120px">NOP</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $objek->kd_propinsi .'.' .$objek->kd_dati2 .'.' .$objek->kd_kecamatan .'.' .$objek->kd_kelurahan .'.' .$objek->kd_blok .'-' .$objek->no_urut .'.' .$objek->kd_jns_op }}
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td>Alamat</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }}
                                                                <br>
                                                                RW : {{ $objek->rw_op }} / RT :{{ $objek->rt_op }}
                                                                <br>
                                                                {{ $objek->nm_kelurahan }} -
                                                                {{ $objek->nm_kecamatan }}
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td>Cabang</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{ $objek->kd_status_cabang == '0' ? 'Tidak' : 'Ya' }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <table class="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td width="120px">Luas Bumi</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{ angka($objek->total_luas_bumi) }} M<sup>2</sup>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Luas Bng</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{ angka($objek->total_luas_bng) }} M<sup>2</sup>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>NJOP Bumi</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                Rp. {{ angka($objek->njop_bumi) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>NJOP Bng</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                Rp. {{ angka($objek->njop_bng) }}
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">
                                                <table class="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td class="p-0" width="100px">Status WP</td>
                                                            <td width="1px">:</td>
                                                            <td class="p-0">
                                                                {{ statusWP($objek->kd_status_wp) }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="p-0">ZNT</td>
                                                            <td width="1px">:</td>
                                                            <td class="p-0">
                                                                {{ $objek->kd_znt }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="p-0">Jenis Tanah</td>
                                                            <td width="1px">:</td>
                                                            <td class="p-0">
                                                                {{ jenisBumi($objek->jns_bumi) }}
                                                            </td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-success text-center">DATA PEREKAMAN </th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td width="40%">
                                                               @if(!empty($perekam->pendataan_objek_id)) Jenis Pendataan @else Jenis Perekaman @endif </td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ jenisTransaksiTanah($objek->jns_transaksi_op) }}
                                                            </td>
                                                        </tr>
                                                        @if(empty($perekam))
                                                        <tr>
                                                            <td width="40%">Nomor Formulir</td>
                                                            <td>:</td>
                                                            <td>
                                                                {{ substr($objek->no_formulir_spop,0,4) }}.{{ substr($objek->no_formulir_spop,4,4) }}.{{ substr($objek->no_formulir_spop,8,3) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>NIP Pendata</td>
                                                            <td>:</td>
                                                            <td> {{ $objek->nip_pendata }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tgl Pendataan</td>
                                                            <td>:</td>
                                                            <td> {{ tglIndo($objek->tgl_pendataan_op) }}</td>
                                                        </tr>
                                                        @else

                                                        @if($perekam->nomor_layanan<>'')


                                                            <tr>
                                                                <td width="30%">Nomor Layanan</td>
                                                                <td>:</td>
                                                                <td>
                                                                    {{ formatnomor($perekam->nomor_layanan)}}
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Jenis Permohonan</td>
                                                                <td width="1px">:</td>
                                                                <td>{{ $perekam->nama_layanan }}</td>
                                                            </tr>
                                                            @else
                                                            <tr>
                                                                <td width="40%">  @if($perekam->nomor_batch<>'')  Batch Pendataan @else Nomor Formulir @endif</td>
                                                                <td>:</td>
                                                                <td>
                                                                    @if($perekam->nomor_batch<>'')  {{ $perekam->nomor_batch }} @else   {{ formatnomor($objek->no_formulir_spop)}} @endif
                                                                </td>
                                                            </tr>
                                                            @endif

                                                            <tr>
                                                                <td>Tanggal Perekaman</td>
                                                                <td width="1px">:</td>
                                                                <td>{{ tglIndo($perekam->created_at) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>@if(!empty($perekam->pendataan_objek_id)) Pendata Oleh @else Diteliti Oleh @endif </td>
                                                                <td width="1px">:</td>
                                                                <td>{{ $perekam->nama }}</td>
                                                            </tr>
                                                            @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">

                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="bg-success text-center">DAFTAR PERMOHONAN </th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-sm table-border">
                                                    <thead>
                                                        <tr>
                                                            <th>NOP</th>
                                                            <th>NOMOR</th>
                                                            <th>Nama</th>
                                                            <th>Luas</th>
                                                            <th>Permohonan</th>
                                                            <th>Keterangan</th>
                                                            <th>Tanggal</th>
                                                            <th>User</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($layananHistory as $itemL)
                                                        <tr>
                                                            <td>
                                                                @if($itemL['nop_proses'])
                                                                    {{formatnop($itemL['nop_proses'])}}
                                                                @else
                                                                    {{$itemL['kd_propinsi']}}.
                                                                    {{$itemL['kd_dati2']}}.
                                                                    {{$itemL['kd_kecamatan']}}.
                                                                    {{$itemL['kd_kelurahan']}}.
                                                                    {{$itemL['kd_blok']}}-
                                                                    {{$itemL['no_urut']}}.
                                                                    {{$itemL['kd_jns_op']}}
                                                                @endif
                                                            </td>
                                                            <td>{{$itemL['nomor_layanan']}}</td>
                                                            <td>{{$itemL['nama_wp']}}</td>
                                                            <td>{{$itemL['luas_bumi']}}</td>
                                                            <td>{{$itemL['jenis_layanan_nama']}}</td>
                                                            <td>
                                                                @if(($itemL['nop_gabung']==null||$itemL['nop_gabung']=='')&&$itemL['jenis_layanan_id']=='6')
                                                                    Induk Pecah
                                                                @elseif(($itemL['nop_gabung']!=null||$itemL['nop_gabung']!='')&&$itemL['jenis_layanan_id']=='6')
                                                                    Pecah
                                                                @endif
                                                            </td>
                                                            <td>{{$itemL['pemutakhiran_at']}}</td>
                                                            <td>{{$itemL['nama_penelitian']}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="bg-success text-center">DAFTAR PENDATAAN OBJEK</th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-sm table-border">
                                                    <thead>
                                                        <tr>
                                                            <th>NOP</th>
                                                            <th>Nama </th>
                                                            <th>Luas </th>
                                                            <th>Jenis</th>
                                                            <th>Tanggal</th>
                                                            <th>Pegawai</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($pendataanHistory as $itemP)
                                                        <tr>
                                                            <td>
                                                                {{formatnop($itemP['nop_proses'])}}
                                                            </td>
                                                            <td>{{$itemP['nm_wp']}}</td>
                                                            <td>{{$itemP['luas_bumi']}}</td>
                                                            <td>{{$itemP['nama_pendataan']}}</td>

                                                            <td>{{$itemP['verifikasi_at']}}</td>
                                                            <td>{{$itemP['pegawai']}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4">

                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-sm table-bordered">
                            <tbody>
                                <tr class="bg-success text-center">
                                    <td>INFORMASI SUBJEK PAJAK</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table table-sm  table-borderless">
                                                    <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td width="30%">Nama</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $subjek->nm_wp }}</td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td width="30%">NIK</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $subjek->subjek_pajak_id }}</td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td>Alamat</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{ $subjek->jalan_wp }} {{ $subjek->blok_kav_no_wp }}<br>
                                                                RW : {{ $subjek->rw_wp }} RT : {{ $subjek->rt_wp }}<br>
                                                                {{ $subjek->kelurahan_wp }} <br>
                                                                {{ $subjek->kota_wp }}
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td width="30%">Kode Pos</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $subjek->kd_pos_wp }}</td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td>No Telp</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{ $subjek->telp_wp }}
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td>NPWP</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $subjek->npwp }}</td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td>Pekerjaan</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ jenisPekerjaan(trim($subjek->status_pekerjaan_wp)) }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{$add_desc['status']}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Keterangan</td>
                                                            <td width="1px">:</td>
                                                            <td>
                                                                {{$add_desc['keterangan']}}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
            <div class="tab-pane fade" id="custom-content-above-messages" role="tabpanel" aria-labelledby="custom-content-above-messages-tab">
                @include('objek._bangunan', ['bangunan' => $bangunan])
            </div>

            <div class="tab-pane fade" id="custom-content-above-njop" role="tabpanel" aria-labelledby="custom-content-above-njop-tab">
                <a onclick="window.open('{{ url('informasi/objek-pajak-njop-cetak') }}?nop={{ str_replace('.', '', $nop) }}','newwindow','width=700,height=1000'); return false;" class="float-right text-success" href="{{ url('informasi/objek-pajak-njop-cetak') }}?nop={{ str_replace('.', '', $nop) }}">[<i class="fas fa-print"></i> Cetak ]</a>
                @include('tracking_pembayaran._riwayatnjop', [
                'data' => $sppt,
                'nop' => $nop,
                'tahun_nota'=>$tahun_nota,
                'show_sppt'=>$show_sppt
                ])
            </div>
        </div>
    </div>
</div>
