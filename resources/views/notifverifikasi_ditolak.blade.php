@extends('layouts.app')
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8 m-5">
                <div class="card card-info">
                    {{-- <div class="card-header">
                        <h3 class="card-title">Akun dalam Proses Verifikasi</h3>
                    </div> --}}
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="alert alert-info alert-dismissible">
                            <h5><i class="icon fas fa-exclamation-triangle"></i> Verifikasi Akun</h5>
                            Akun Anda telah berhasil diverifikasi. Namun, setelah pemeriksaan dokumen yang Anda unggah, kami
                            menemukan bahwa dokumen tersebut belum memenuhi persyaratan yang diperlukan.
                            <br><br>
                            Untuk memperbarui dokumen Anda, silakan kunjungi link berikut ini: <a href="#">Update
                                dokumen</a>
                            <br><br>
                            Jika ada pertanyaan lebih lanjut atau membutuhkan bantuan, jangan ragu untuk menghubungi tim
                            kami.

                            Terima kasih atas perhatian dan kerjasamanya.
                            <br><br>


                        </div>
                        <div class="text-center mt-4">
                            {{-- <a href="{{ url('/') }}" class="btn btn-primary">
                                <i class="fas fa-home"></i> Kembali ke Beranda
                            </a> --}}
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
@endsection
