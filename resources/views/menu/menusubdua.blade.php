@foreach($childs as $child)
@php
$hitung=$child->childs;
@endphp
<li class="nav-item">
    <a class="nav-link" <?= $hitung>0 ? 'role="button" tabindex="0"' :'href="'.url($child->url).'"' ?>>
        <i class="nav-icon  {{ $child->icon<>''?$child->icon:'far fa-circle fas-sm'}} "></i>
        <p>{{ $child->title }} <?= $hitung>0 ? '<i class="right fas fa-angle-left"></i>' :'' ?> </p>
    </a>
    @if($hitung)
    <ul class="nav nav-treeview">
        @include('menu.menusubdua',['childs' => showMenu($child->id)])
    </ul>
    @endif
</li>


@endforeach
