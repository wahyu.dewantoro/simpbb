<ul>
    @foreach($childs as $child)
    <li>
        {{-- {{ $child->title }} --}}
        <div class="form-check">
            <input @if( in_array($child->id,$active??[])) checked @endif type="checkbox" name="menu[]" class="form-check-input" id="{{ $child->id }}" value="{{ $child->id }}" >
            <label for="{{ $child->id }}">
                {{ $child->title }}
            </label>
        </div>
        @if(count($child->childs))
        @include('menu.manageChild',['childs' => $child->childs,'active'=>$active??[]])
        @endif
    </li>
    @endforeach
</ul>
