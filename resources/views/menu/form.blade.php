@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Form Menu</h5>
                    <div class="card-tools">
                        <a href="{{ url('menus')}}" class="btn btn-flat btn-info"> <i class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ $data['action']}}" method="post">
                        @csrf
                        @method($data['method'])
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>Urutan</label>
                                            <input type="number" name="urut" value="{{ $menu->urut??''}}" class="form-control form-control">
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" name="title" value="{{ $menu->title??''}}" class="form-control form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>URL</label>
                                    <input type="text" name="url" value="{{ $menu->url??''}}" class="form-control form-control">
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Icon fontawesome</label>
                                    <input type="text" name="icon" value="{{ $menu->icon??''}}" class="form-control form-control">
                                </div>

                                <div class="form-group">
                                    <label>Parent</label>
                                    <select style="width: 100%" class="form-control select" name="parent_id" data-placeholder="Parent menu">
                                        <option value=""> </option>
                                        @php
                                        $selected=$menu->parent_id??'';
                                        @endphp
                                        @foreach($allMenus as $key => $value)
                                        <option @if($selected==$key) selected @endif value="{{ $key }}">{{ $value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <button class="btn btn-flat btn-success"><i class="fas fa-save"></i> Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{url('js/treeview.js')}}"></script>
<script>
    $(document).ready(function() {

        $(".select").select2({
            placeholder: "Parent Menu"
            , allowClear: true
        });





    });

</script>


@endsection
