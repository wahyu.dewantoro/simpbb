 @extends('layouts.app')
 @section('content')

 <div class="container">
     <div class="row justify-content-center">
         <div class="col-md-10">

             <div class="card">
                 <div class="card-header">
                     <h5 class="card-title">Daftar Menu</h5>
                     <div class="card-tools">
                         <a href="{{ url('menus/create')}}" class="btn btn-flat btn-info"> <i class="far fa-plus-square"></i> Tambah</a>
                         {{-- <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                             Tambah
                         </button> --}}
                     </div>
                 </div>

                 <div class="card-body">
                     @if ($message = Session::get('success'))
                     <div class="alert alert-success  alert-dismissible">
                         <button type="button" class="close" data-dismiss="alert">×</button>
                         <strong>{{ $message }}</strong>
                     </div>
                     @endif
                     @if(count($errors) > 0)
                     <div class="alert alert-danger  alert-dismissible">
                         <button type="button" class="close" data-dismiss="alert">×</button>
                         @foreach($errors->all() as $error)
                         {{ $error }}<br>
                         @endforeach
                     </div>
                     @endif
                     <div class="row">
                         <div class="col-md-12">

                             <ul id="tree1">
                                 @foreach($menus as $menu)
                                 <li>
                                     @php
                                     $count=count($menu->childs);
                                     @endphp
                                     <label><i class="fas fa-edit text-success edit" data-urut="{{ $menu->urut }}" data-title="{{ $menu->title }}" data-url="{{ $menu->url }}" data-parent_id="{{ $menu->parent_id}}" data-icon="{{ $menu->icon }}" data-id="{{ $menu->id }}"></i> {{ $menu->title }} @if($count==0) <a href="{{ url('menu-hapus/'.$menu->id)}}"><i class="fas fa-trash-alt text-danger"></i></a> @endif</label>
                                     @if($count>0)
                                     @include('menu.manageChildDua',['childs' => $menu->childs])
                                     @endif
                                 </li>
                                 @endforeach
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 @endsection
 @section('script')
 <script src="{{url('js/treeview.js')}}"></script>
 <script>
     $(document).ready(function() {

         $(".select").select2({
             placeholder: "parent menu"
             , allowClear: true
         });


         $('.edit').on('click', function(e) {
             e.preventDefault()
             window.location.href = "{{ url('menus')}}/" + $(this).data('id')

             /* $('#title').val($(this).data('title'))
             $('#url').val($(this).data('url'))
             $('#icon').val($(this).data('icon'))
             $('#parent_id').val($(this).data('parent_id'))

             $('#parent_id').trigger('change'); // Notify any JS components that the value changed

             $('#urut').val($(this).data('urut'))

             //  $('#parent_id').select2().select2('val', $(this).data('parent_id'))
             $('#modal-edit').modal('show'); */

         })


     });

 </script>


 @endsection
 @section('css')
 <link href="{{ url('css/treeview.css') }}" rel="stylesheet">


 @endsection
