<ul>
    @foreach($childs as $child)
    @php
    $count=count($child->childs);
    @endphp
    <li>
        <div class="form-check">
            <label for="{{ $child->id }}">
                <i class="fas fa-edit text-success edit" data-parent_id="{{ $child->parent_id}}" data-urut="{{ $child->urut }}" data-title="{{ $child->title }}" data-url="{{ $child->url }}" data-icon="{{ $child->icon }}" data-id="{{ $child->id }}"></i> {{ $child->title }}
                @if($count==0)
                <a href="{{ url('menu-hapus',$child->id)}}"><i class="fas fa-trash-alt text-danger"></i></a>
                @endif
            </label>
        </div>
        @if($count>0)
        @include('menu.manageChildDua',['childs' => $child->childs,'active'=>$active??[]])
        @endif
    </li>
    @endforeach
</ul>
