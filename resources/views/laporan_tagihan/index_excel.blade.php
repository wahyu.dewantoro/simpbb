<html>

<body>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>NOP</th>
                <th>Nama</th>
                <th>Alamat Subjek</th>
                <th>Alamat Objek</th>
                <th>buku</th>
                <th>masa</th>
                <th>ketetapan</th>
                <th>Stimulus</th>
                <th>denda</th>
                <th>keterangan</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($param['data'] as $item)
                <tr>
                    <td>{{ $no }}</td>
                    <td>
                        {{ $item->kd_propinsi .
                            '.' .
                            $item->kd_dati2 .
                            '.' .
                            $item->kd_kecamatan .
                            '.' .
                            $item->kd_kelurahan .
                            '.' .
                            $item->kd_blok .
                            '-' .
                            $item->no_urut .
                            '.' .
                            $item->kd_jns_op }}
                    </td>
                    <td>{{ $item->nm_wp_sppt }}</td>
                    <td>{{ $item->jln_wp_sppt }}
                        {{ $item->blok_kav_no_wp_sppt }}{{ $item->rt_wp_sppt != '' ? 'RT ' . $item->rt_wp_sppt : '' }}
                        {{ $item->rw_wp_sppt != '' ? 'RW ' . $item->rw_wp_sppt : '' }} {{ $item->kelurahan_wp_sppt }}
                        {{ $item->kota_wp_sppt }}
                    </td>
                    <td>{{ $item->nm_kelurahan . ' ' . $item->nm_kecamatan }}</td>
                    <td>{{ romawi($item->kd_buku) }}</td>
                    <td>{{ $item->thn_pajak_sppt }}</td>
                    <td>{{ $item->pbb }}</td>
                    <td>{{ $item->potongan }}</td>
                    <td>{{ $item->es_denda }}</td>
                    <td>
                        @if ($param['data'] == '1')
                            {{ $item->no_transaksi != '' ? $item->no_transaksi . ', ' . $item->keterangan : '' }}
                            {{ $item->no_stpd != '' ? $item->no_stpd : '' }}
                        @else
                            {{ $item->no_transaksi!='IV223112015'?$item->no_transaksi:'' }}
                        @endif

                        @if($item->kobil<>'')
                            {{ $item->kobil }}
                        @endif

                    </td>
                </tr>
                @php
                    $no++;
                @endphp
            @endforeach

        </tbody>
    </table>
</body>

</html>
