@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tunggakan DHKP</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-left">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline card-tabs">
                <div class="card-body">
                    <div class="form-inline" id="filter-form">
                        
                        <select class="form-control pencarian form-control-sm mb-2 mr-sm-2" name="tahun_terbit" required style="display: none"
                            id="tahun_terbit">
                            <option value="">Tahun Terbit</option>
                            @foreach ($arrTahun as $item)
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endforeach
                        </select>
                        <select class="form-control pencarian form-control-sm mb-2 mr-sm-2" name="tahun_pajak" required
                            id="tahun_pajak">
                            <option value="">Tahun Pajak</option>
                            @foreach ($arrTahun as $item)
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endforeach
                        </select>

                        <select class="form-control pencarian form-control-sm mb-2 mr-sm-2" name="kd_kecamatan" required
                            id="kd_kecamatan">
                            @if (count($kecamatan) > 1)
                                <option value="">-- Kecamatan --</option>
                            @endif
                            @foreach ($kecamatan as $rowkec)
                                <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                    value="{{ $rowkec->kd_kecamatan }}">
                                    {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                </option>
                            @endforeach
                        </select>

                        <select class="form-control pencarian form-control-sm mb-2 mr-sm-2" name="kd_kelurahan"
                            id="kd_kelurahan">
                            <option value="">-- Kelurahan / Desa -- </option>
                        </select>
                        <select class="form-control pencarian form-control-sm mb-2 mr-sm-2" name="buku" id="buku">
                            <option value="">Semua Buku</option>
                            @php
                                $bsel ='1';
                                $arb = [1 => 'Buku 1 dan 2', 'Buku 3,4, dan 5'];
                            @endphp
                            @for ($b = 1; $b <= 2; $b++)
                                <option @if ($bsel == $b) selected @endif value="{{ $b }}">
                                    {{ $arb[$b] }}</option>
                            @endfor
                        </select>
                        <button type="button" id="cetak" class="btn btn-sm btn-success mb-2 mr-sm-2"><i
                                class="far fa-file-alt"></i>
                            Excel</button>
                    </div>
                    <div class="table-responsive">
                        <table
                            class="table table-sm t
                        
                        able-bordered text-sm"
                            id="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NOP</th>
                                    <th>Nama</th>
                                    <th>Masa</th>
                                    <th>Ketetapan</th>
                                    <th>Stimulus</th>
                                    <th>Denda</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <style>
        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let tableHasil
            tableHasil = $('#table').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ url()->current() }}",
                    data: function(d) {
                        d.tahun_pajak = $('#tahun_pajak').val()
                        d.tahun_terbit = $('#tahun_terbit').val()
                        d.kd_kecamatan = $('#kd_kecamatan').val()
                        d.kd_kelurahan = $('#kd_kelurahan').val()
                        d.buku = $('#buku').val()
                    }
                },
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        searchable: false
                    }, {
                        data: 'nop',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            // alamat_wp
                            return data + "<br><small class='text-info'>" + row.alamat_objek +
                                "</small>"
                        }

                    }, {
                        data: 'nm_wp_sppt',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            // alamat_wp
                            return data + "<br><small class='text-info'>" + row.alamat_wp +
                                "</small>"
                        }
                    },
                    {
                        data: 'thn_pajak_sppt',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'pbb',
                        orderable: false,
                        searchable: false,
                        class: 'text-right',
                        render: function(data, type, row) {
                            // alamat_wp
                            return data + "<br><small class='text-info'>" + row.buku +
                                "</small>"
                        }
                    }, {
                        data: 'potongan',
                        orderable: false,
                        searchable: false,
                        class: 'text-right'
                    }, {
                        data: 'denda',
                        orderable: false,
                        searchable: false,
                        class: 'text-right'
                    }, {
                        data: 'keterangan',
                        orderable: false,
                        searchable: false
                    }
                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            var kecamatan = "{{ request()->get('kd_kecamatan') }}";
            if (kecamatan == '') {
                var kecamatan = $('#kd_kecamatan').val()
            }
            getKelurahan(kecamatan);

            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                $('#kd_kelurahan').html(html);
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }

            $('#tahun_pajak,#kd_kecamatan,#kd_kelurahan,#buku,#tahun_terbit').on('change', function(e) {
                pencarian()
            })

            $('#buku').trigger("change")

            function pencarian() {
                tableHasil.draw();
            }

            // $(document).on('#cetak', 'click', function(e) {
            $('#cetak').on('click', function(e) {

                e.preventDefault()
                let tahun_pajak = $('#tahun_pajak').val()
                let tahun_terbit = $('#tahun_terbit').val()
                let kd_kecamatan = $('#kd_kecamatan').val()
                let kd_kelurahan = $('#kd_kelurahan').val()
                let buku = $('#buku').val()
                let param = "?kd_kecamatan=" + kd_kecamatan + "&kd_kelurahan=" +
                    kd_kelurahan + "&buku=" + buku +
                    "&tahun_pajak=" + tahun_pajak +
                    "&tahun_terbit=" + tahun_terbit
                let url = "{{ route('dhkp.tagihan.excel') }}" + param
                window.open(url)
            })



        });
    </script>
@endsection
