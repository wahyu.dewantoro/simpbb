<div class="card card-outline card-info">
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-hover table-sm table-bordered text-sm">
                <thead>
                    <tr style="vertical-align: middle">
                        <th style="text-align: center">Kecamatan</th>
                        <th style="text-align: center">Kel/Desa</th>
                        <th style="text-align: center">Objek</th>
                        <th style="text-align: center">Ketetapan PBB</th>
                        <th style="text-align: center">Stimulus</th>
                        <th style="text-align: center">Bayar</th>
                        <th style="text-align: center">Koreksi</th>
                        <th style="text-align: center">Sisa</th>
                        <th style="text-align: center">Download</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach ($rekap as $row)
                        <tr>
                            <td>{{ $row->kd_kecamatan . ' - ' . $row->nm_kecamatan }}</td>
                            <td>{{ $row->kd_kelurahan . ' - ' . $row->nm_kelurahan }}</td>
                            <td class="text-center">{{ angka($row->jumlah_op) }}</td>
                            <td class="text-center">{{ angka($row->baku) }}</td>
                            <td class="text-center">{{ angka($row->potongan) }}</td>
                            <td class="text-center">{{ angka($row->bayar) }}</td>
                            <td class="text-center">{{ angka($row->koreksi) }}</td>
                            <td class="text-center">{{ angka($row->sisa) }}</td>

                            <td class="text-center">
                                <a href="{{ url('dhkp/tagihan-excel') }}?{{ $var_cetak }}" target="_blank"><i
                                        class="fas fa-file-download text-success"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
