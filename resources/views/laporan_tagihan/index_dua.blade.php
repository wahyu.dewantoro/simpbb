    @extends('layouts.app')

    @section('content')
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1>Penagihan</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="{{ url()->current() }}" class="" id="filter-form">
                            <div class="card card-primary card-outline">
                                <div class="card-header p-1">
                                    <h3 class="card-title text-bold">Filter Data</h3>
                                </div>
                                <div class="card-body p-1">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <label for="kd_kecamatan" class="col-form-label col-sm-4">Kecamatan <sup
                                                        class="text-danger">*</sup></label>
                                                <div class="col-sm-6">
                                                    <select class="form-control form-control-sm  select2"
                                                        name="kd_kecamatan" required id="kd_kecamatan"
                                                        data-placeholder="Kecamatan">
                                                        @if (count($kecamatan) > 1)
                                                            <option value=""></option>
                                                        @endif
                                                        @foreach ($kecamatan as $rowkec)
                                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                                value="{{ $rowkec->kd_kecamatan }}">
                                                                {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kd_kelurahan" class="col-form-label col-sm-4">Desa /
                                                    Kelurahan <sup class="text-danger"></sup></label>
                                                <div class="col-sm-6">
                                                    <select class="form-control form-control-sm select2" name="kd_kelurahan"
                                                        id="kd_kelurahan" data-placeholder="Desa / Kelurahan">
                                                        @if (count($kecamatan) > 1)
                                                            <option value=""></option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="buku" class="col-form-label col-sm-4">Buku <sup
                                                        class="text-danger">*</sup></label>
                                                <div class="col-sm-6">
                                                    <select class="form-control form-control-sm " name="buku" required
                                                        id="buku">
                                                        <option value="">Pilih</option>
                                                        @php
                                                            $bsel = request()->get('buku') ?? '1';
                                                            $arb = [1 => 'Buku 1 dan 2', 'Buku 3,4, dan 5'];
                                                        @endphp
                                                        @for ($b = 1; $b <= 2; $b++)
                                                            <option @if ($bsel == $b) selected @endif
                                                                value="{{ $b }}">{{ $arb[$b] }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <label for="tahun" class="col-form-label col-sm-4">Tahun Terbit</label>
                                                <div class="col-sm-6">
                                                    <select name="tahun" id="tahun"
                                                        class="form-control form-control-sm">
                                                        <option value="">Semua Tahun</option>
                                                        @php
                                                            $sel = request()->get('tahun') ?? '';
                                                        @endphp
                                                        @for ($t = date('Y'); $t >= 2003; $t--)
                                                            <option @if ($sel == $t) selected @endif
                                                                value="{{ $t }}">{{ $t }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tahun_pajak" class="col-form-label col-sm-4">Tahun/Masa
                                                    Pajak</label>
                                                <div class="col-sm-6">
                                                    <select name="tahun_pajak" id="tahun_pajak"
                                                        class="form-control form-control-sm">
                                                        <option value="">Semua Tahun</option>
                                                        @php
                                                            $sel = request()->get('tahun_pajak') ?? '';
                                                        @endphp
                                                        @for ($t = date('Y'); $t >= 2003; $t--)
                                                            <option @if ($sel == $t) selected @endif
                                                                value="{{ $t }}">{{ $t }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="float-right">
                                        <button class="btn btn-sm btn-info mb-2 mr-sm-2"><i class="fa fa-search"></i>
                                            Tampilkan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <div id="preview-data"></div>

                    </div>

                </div>
            </div>
        </section>
    @endsection
    @section('script')
        <script>
            $(document).ready(function() {

                $('#kd_kecamatan').on('change', function() {
                    var kk = $('#kd_kecamatan').val();
                    getKelurahan(kk);
                })

                $('.select2').select2({
                    'allowClear': true
                })

                function getKelurahan(kk) {
                    let jk = "{{ count($kecamatan) }}"
                    // console.log(jk)
                    if (jk > 1) {
                        $('#kd_kelurahan').html('<option value=""></option>');
                    } else {
                        $('#kd_kelurahan').html('');
                    }
                    if (kk != '') {
                        $.ajax({
                            url: "{{ url('desa') }}",
                            data: {
                                'kd_kecamatan': kk
                            },
                            success: function(data) {
                                $.each(data, function(key, value) {
                                    var newOption = new Option(key + '-' + value, key, true, true);
                                    $('#kd_kelurahan').append(newOption)
                                });
                                if (jk > 1) {
                                    $('#kd_kelurahan').val(null).trigger('change');
                                }
                            },
                            error: function(res) {

                            }
                        });
                    }
                }

                $('#kd_kecamatan').trigger('change')

                $("#filter-form").on("submit", function(event) {
                    openloading();
                    $('#preview-data').html('')
                    event.preventDefault();
                    openloading()
                    $.ajax({
                        url: "{{ url()->current() }}",
                        data: $(this).serialize(),
                        success: function(res) {
                            closeloading();
                            $('#preview-data').html(res)

                        },
                        error: function(res) {
                            closeloading();
                            toastr.error('Error', 'Gagal menampilkan data');
                            $('#preview-data').html('')
                        }
                    })

                });
            });
        </script>
    @endsection
