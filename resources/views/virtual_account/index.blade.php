@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Virtual Account</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <form action="{{ url()->current() }}" id="form-filter">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <input type="text" id="daterange" name="daterange"
                                        class="form-control form-control-sm" value="{{ $daterange }}">
                                </div>
                                <div class="input-group-prepend">
                                    <select name="status" id="status" class="form-control form-control-sm">
                                        @php
                                            $ar = [
                                                '0' => 'Semua',
                                                '1' => 'Lunas',
                                                '2' => 'Belum Lunas',
                                                '3' => 'Expired',
                                            ];
                                        @endphp
                                        @foreach ($ar as $ai => $item)
                                            <option @if ((request()->get('status') ?? '99') == $ai) selected @endif
                                                value="{{ $ai }}">{{ $item }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <input class="form-control form-control-sm py-2 border-right-0 border" type="search"
                                    value="{{ request()->get('search') }}" id="search" name='search'
                                    placeholder="Pencarian">
                                <span class="input-group-append">
                                    <div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div>
                                    {{-- <div class="btn-group-text bg-success"><i class="fa fa-file"></i></div> --}}
                                    <button type="button" id="cetak" class="btn btn-sm btn-info btn-flat"><i
                                            class="fa fa-file"></i></button>
                                </span>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="card-body p-0 table-responsive">
                    <table class="table table-sm text-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Billing / NOP</th>
                                <th>Nomor VA</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $item)
                                <tr>
                                    <td>{{ $index + $data->firstItem() }}</td>
                                    <td>{{ $item->kobil ?? '' }}
                                        <br>
                                        <small class="text-info">{{ formatnop($item->nop) }} /
                                            {{ $item->nama_wp ?? '' }}</small>
                                        <br>
                                        <small class="text-primary">{{ $item->keterangan }}</small>

                                    </td>
                                    <td>
                                        {{ $item->nomor_va }}
                                    </td>
                                    <td>
                                        @php
                                            $pokok = $item->pokok;
                                            $denda = $item->denda;
                                            $total = $item->total;
                                        @endphp
                                        Pokok : {{ angka($pokok) }}
                                        <br>
                                        <span class="text-info">denda
                                            {{ angka($denda) }}</span>
                                        <br>
                                        <span class="text-primary">denda
                                            {{ angka($total) }}</span>
                                    </td>
                                    <td>
                                        @if ($item->amount != '')
                                            <span class="text-success"><i class="fas fa-check"></i> Lunas</span><br>
                                            <span class="text-info">{{ $item->reference }}<br>
                                                {{ $item->tanggal_bayar }}
                                            </span>
                                        @else
                                            @php
                                                $a = strtotime(date('Ymdhis'));
                                                $b = strtotime($item->tanggal_exp);
                                            @endphp
                                            @if ($b > $a)
                                                <span class="text-info"><i class="fas fa-exclamation-triangle"></i> Belum
                                                    lunas</span>
                                            @else
                                                <span class="text-danger"><i class="far fa-window-close"></i> Expired</span>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer p-1 ">
                    <div class="row">
                        <div class="col-6">
                            Total : {{ $data->total() }}
                        </div>
                        <div class="col-6">
                            <div class="float-right">
                                {!! $data->links() !!}
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#status').on('change', function() {

                $('#form-filter').submit()
            })


            $('#daterange').daterangepicker({
                // var dr=$(this).val()
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
            }, function(start, end, label) {

                console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format(
                    'YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            });

            $('#daterange').on('change', function() {
                $('#form-filter').submit()
            })

            $('#cetak').on("click", function(e) {
                e.preventDefault()
                var url_ = "{{ url('pembayaran-va-cetak') }}"
                var vinput = "";
                $("#form-filter input, #form-filter select").each(function() {
                    var input = $(this); // This is the jquery object of the input, do what you will
                    if (input.val() != '') {
                        vinput += input.attr('name') + '=' + input.val() + '&'
                    }

                });
                window.open(url_ + '?' + vinput, "_blank");
            })
        })
    </script>
@endsection
