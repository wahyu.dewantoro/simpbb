<html>

<head>

</head>

<body>
    <table class="table table-sm text-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Billing</th>
                <th>NOP</th>
                <th>Nama</th>
                <th>Keterangan</th>
                <th>Nomor VA</th>
                <th>Pokok</th>
                <th>Denda</th>
                <th>Total</th>
                <th>Status</th>
                <td>Reffno</td>
                <td>Tanggal Bayar</td>
            </tr>
        </thead>
        <tbody>

            @foreach ($data as $index => $item)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>&nbsp;{{ $item->kobil ?? '' }}</td>
                    <td>
                        {{ formatnop($item->nop) }}</td>
                    <td>{{ $item->nama_wp ?? '' }}</td>
                    <td>&nbsp;{{ $item->keterangan }} </td>
                    <td>
                        &nbsp;{{ $item->nomor_va }}
                    </td>
                    <td>
                        @php
                            $pokok = $item->pokok;
                            $denda = $item->denda;
                            $total = $item->total;
                        @endphp
                        {{ $pokok }}
                    </td>
                    <td>{{ $denda }}</td>
                    <td>{{ $total }}</td>
                    <td>
                        @if ($item->amount != '')
                            <span class="text-success"><i class="fas fa-check"></i> Lunas</span>
                        @else
                            @php
                                $a = strtotime(date('Ymdhis'));
                                $b = strtotime($item->tanggal_exp);
                            @endphp
                            @if ($b > $a)
                                <span class="text-info"><i class="fas fa-exclamation-triangle"></i> Belum
                                    lunas</span>
                            @else
                                <span class="text-danger"><i class="far fa-window-close"></i> Expired</span>
                            @endif
                        @endif
                    </td>
                    <td>
                        &nbsp;{{ $item->reference }}
                    </td>
                    <td>
                        {{ $item->tanggal_bayar }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
