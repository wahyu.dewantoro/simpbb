<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <link href="https://fonts.googleapis.com/css2?family=Lobster&family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            padding: 10px;
            background-color: #f9f9f9;
        }

        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 15px;
            border: 1px solid #eee;
            background-color: #fff;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }

        .invoice-box h1 {
            font-family: 'Lobster', cursive;
            font-size: 36px;
            margin-bottom: 10px;
            text-align: center;
            color: #2c3e50;
        }

        .invoice-box h2 {
            font-size: 20px;
            margin: 5px 0;
        }

        .invoice-box table {
            width: 100%;
            line-height: 1.4;
            text-align: left;
            margin-bottom: 15px;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr.information td {
            padding-bottom: 15px;
            border-bottom: 1px solid #ddd;
        }

        .invoice-box table tr.heading td {
            background-color: #f2f2f2;
            border-bottom: 2px solid #ddd;
            font-weight: bold;
            text-align: center;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.total td {
            border-top: 2px solid #eee;
            font-weight: bold;
            text-align: right;
        }

        .payment-method {
            margin-top: 20px;
            font-size: 14px;
            display: flex; /* Using flexbox to align items horizontally */
            justify-content: space-between; /* Space between the items */
        }

        .payment-section {
            width: 48%; /* Set width for each section */
        }

        .payment-method strong {
            display: block;
            margin: 5px 0;
        }

        .lead {
            font-weight: bold;
            margin: 10px 0;
        }

        /* Button to copy VA */
        .copy-btn {
            background-color: #2c3e50;
            color: white;
            border: none;
            padding: 5px 10px;
            cursor: pointer;
            border-radius: 5px;
            font-size: 14px;
            margin-left: 10px;
        }

        /* Styles for print */
        @media print {
            body {
                padding: 0;
                background-color: #fff;
            }

            .invoice-box {
                box-shadow: none;
                border: none;
                padding: 10px; /* Reduce padding for print */
            }

            .invoice-box h1,
            .invoice-box h2 {
                page-break-after: avoid;
            }

            .payment-method {
                page-break-inside: avoid;
                display: block; /* Stack items for print */
            }

            table {
                page-break-inside: avoid;
                margin-bottom: 10px; /* Reduce margin for print */
            }

            .invoice-box table tr.total td {
                border-top: 2px solid #000;
            }

            .invoice-box table tr.heading td {
                background-color: #eaeaea;
            }

            @page {
                size: A4;
                margin: 10mm;
            }
        }
    </style>
    <script>
        function copyToClipboard() {
            const vaNumber = "{{ $data->VirtualAccount->nomor_va }}"; // Ambil nomor VA dari server
            const tempInput = document.createElement("textarea"); // Buat elemen textarea sementara
            tempInput.value = vaNumber; // Set nilai textarea dengan nomor VA
            document.body.appendChild(tempInput); // Tambahkan textarea ke body
            tempInput.select(); // Pilih teks di textarea
            document.execCommand("copy"); // Jalankan perintah untuk menyalin
            document.body.removeChild(tempInput); // Hapus textarea setelah menyalin
            alert("Nomor Virtual Account berhasil disalin: " + vaNumber);
        }
    </script>
</head>

<body>
    <div class="invoice-box">
        <h1>Invoice</h1>

        <div class="information">
            <table>
                <tr>
                    <td>
                        <strong>{{ $data->nama_wp }}</strong>
                    </td>
                    <td>
                        <strong>Tanggal: {{ tglindo(date('Ymd')) }}</strong>
                    </td>
                </tr>
            </table>
        </div>

        <table>
            <tr class="heading">
                <td>NOP</td>
                <td>TAHUN</td>
                <td>POKOK</td>
                <td>DENDA</td>
                <td>TOTAL</td>
            </tr>
            @foreach ($data->billing_kolektif()->get() as $row)
                <tr class="item">
                    <td>{{ $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '.' . $row->no_urut . '.' . $row->kd_jns_op }}</td>
                    <td style="text-align: center">{{ $row->tahun_pajak }}</td>
                    <td>{{ number_format($row->pokok, 0, ',', '.') }}</td>
                    <td>{{ number_format($row->denda, 0, ',', '.') }}</td>
                    <td>{{ number_format($row->total, 0, ',', '.') }}</td>
                </tr>
            @endforeach
        </table>

        <table>
            <tr class="total">
                <td style="text-align: left">Total:</td>
                <td>{{ number_format($data->VirtualAccount->total_tagihan, 0, ',', '.') }}</td>
            </tr>
            <tr>
                <td style="text-align: left">Virtual Account:</td>
                <td>
                    <span id="va-number">{{ $data->VirtualAccount->nomor_va }}</span>
                    <button class="copy-btn" onclick="copyToClipboard()">Salin</button>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">Masa Berlaku:</td>
                <td>{{ $data->expired_at }}</td>
            </tr>
        </table>

        <div class="payment-method">
            <div class="payment-section">
                <p class="lead">Cara Pembayaran:</p>
                <strong>Melalui Mobile Banking Bank Jatim:</strong>
                <ol>
                    <li>Login ke aplikasi mobile banking JConnect Mobile</li>
                    <li>Pilih menu Bayar</li>
                    <li>Pilih menu Virtual Account</li>
                    <li>Pilih Virtual Account</li>
                    <li>Masukkan Nomor Virtual Account <b>"{{ $data->VirtualAccount->nomor_va }}"</b></li>
                    <li>Masukkan PIN anda</li>
                    <li>Transaksi selesai</li>
                </ol>
            </div>

            <div class="payment-section">
                <strong>Pembayaran Melalui Mobile Banking Lainnya:</strong>
                <ol>
                    <li>Login ke aplikasi mobile banking</li>
                    <li>Pilih menu Transfer</li>
                    <li>Pilih Transfer ke Bank Lainnya</li>
                    <li>Pilih Bank Tujuan Bank Jatim</li>
                    <li>Masukkan Nomor Virtual Account <b>"{{ $data->VirtualAccount->nomor_va }}"</b> sebagai nomor rekening tujuan</li>
                    <li>Masukkan Nominal Transfer sesuai dengan tagihan</li>
                    <li>Pilih metode transfer Realtime/Transfer Online</li>
                    <li>Masukkan PIN anda</li>
                    <li>Transaksi selesai</li>
                </ol>
            </div>
        </div>
    </div>
</body>

</html>
