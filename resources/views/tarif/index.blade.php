@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Master {{$title}}</b></h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item">
                                    <a href="javascript:;" class="btn btn-block btn-primary btn-sm" 
                                        data-modal-target="#modal-tarif"> <i class="fas fa-plus-square"></i> Tambah {{$title}} </a>
                                </li>
                            </ul>

                        </div>
                        <div class="card-body p-1">
                            <div class="card-body no-padding p-1">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>KD PROPINSI</th>
                                            <th>KD DATI2</th>
                                            <th>TAHUN AWAL</th>
                                            <th>TAHUN AKHIR</th>
                                            <th>NJOP MIN</th>
                                            <th>NJOP MAX</th>
                                            <th>NILAI TARIF</th>
                                            <th>-</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('tarif/form',['url'=>url('refrensi/tarif_create'),'now'=>$nowYear])
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'id':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('refrensi/tarif_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'kd_propinsi'},
                    { data: 'kd_dati2'},
                    { data: 'thn_awal'},
                    { data: 'thn_akhir'},
                    { data: 'njop_min'},
                    { data: 'njop_max'},
                    { data: 'nilai_tarif'},
                    { data: 'raw_tag'},
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let timer=60000;
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            setInterval( function () {
                resLoad();
            }, timer );

            $(document).on("submit", ".form-tarif", function (evt) {
                evt.preventDefault();
                let e=$(this);
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            return Swal.fire({ icon: 'warning', html: response.msg});
                        }else{
                            Swal.close();
                            resLoad();
                        }
                    },
                    error: function (x, y) {
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            })
            $(document).on("click", "[data-modal-target]", function (evt) {
                let e=$(this);
                $(e.attr('data-modal-target')).modal({
                    keyboard: false,
                    backdrop:'static',
                    show:true
                });
            });
            $(document).on("click", "[data-delete]", function (evt) {
                let e=$(this);
                let send=e.attr('data-delete');
                let reload=e.attr('data-reload');
                Swal.fire({ 
                    icon: 'warning',
                    html: '<p>Hapus Data</p>',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        asyncData("{{url('refrensi/tarif_delete')}}",{
                            id:send
                        }).then((response) => {
                            if(!response.status){
                                Swal.fire({ icon: 'error', html: response.msg});
                            };
                            eventReload(reload);
                        })
                    }
                });
            })
        });
    </script>
@endsection