<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QRIS QR Code</title>
    <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            background-color: #f5f5f5;
        }

        .qris-card {
            width: 300px;
            border: 1px solid #ccc;
            border-radius: 8px;
            overflow: hidden;
            background-color: white;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            text-align: center;
        }

        .qris-header {
            background-color: #f44336;
            padding: 10px;
            color: white;
        }

        .qris-header h1 {
            font-size: 18px;
            margin: 0;
        }

        .qris-header img {
            width: 100%;
        }

        .qris-merchant {
            padding: 10px;
            font-size: 16px;
            font-weight: bold;
        }

        .qris-code {
            padding: 10px;
        }

        .qris-footer {
            padding: 10px;
            font-size: 12px;
            color: #777;
        }

        .qris-footer span {
            display: block;
        }

        #download {
            margin: 10px;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
            border-radius: 5px;
        }

        #download:hover {
            background-color: #45a049;
        }
    </style>
</head>

<body>

    <div class="qris-card" id="qrisCard">
        <div class="qris-header">
            <img src="{{ url('QRIS_logo.png') }}" alt="QRIS Logo">
            {{-- <h1>QRIS Payment</h1> --}}
        </div>
        <div class="qris-merchant">
            {{ $qris->merchant_name }} <br>
            NMID: {{ $qris->nmid }}
        </div>

        <div class="qris-code">
            <img id="qrcodeImage"
                src="data:image/png;base64,{{ base64_encode(QrCode::format('png')->size(200)->generate($qr_value)) }}"
                alt="QR Code">
        </div>

        <div class="qris-footer">
            <span>Dicetak oleh: {{ $qris->merchant_pan }}</span>
            <span>GPN</span>
        </div>

        <!-- Download Button -->
        <button id="download" onclick="downloadQRCode()">Download QR Code</button>
    </div>

    <script>
        function downloadQRCode() {
            const qrisCard = document.getElementById("qrisCard");

            html2canvas(qrisCard).then((canvas) => {
                const link = document.createElement("a");
                link.href = canvas.toDataURL("image/png");
                link.download = "{{ $qris->bill_number }}.png";
                link.click();
            });
        }
    </script>

</body>

</html>
