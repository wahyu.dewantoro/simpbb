<table class="table table-sm table-bordered text-sm">
    <thead>
        <tr>
            <th class="text-center">No</th>
            <th class="text-center">Billnumber</th>
            <th> NOP</th>
            <th>Nama</th>
            <th class="text-center">Keterangan</th>

            <th class="text-center">Pokok</th>
            <th class="text-center">Denda</th>
            <th class="text-center">Total</th>
            <th class="text-center">Status</th>
            <th>PJSP</th>
            <th>Tgl Bayar</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $index => $item)
            <tr>
                <td class="text-center">{{ $index + 1 }}</td>

                <td>{{ $item->bill_number }}</td>
                <td>{{ formatnop($item->nop) }} </td>
                <td> {{ $item->nama_wp ?? '' }}
                </td>
                <td>
                    {{ $item->keterangan }}
                </td>
                <td class="text-right">{{ $item->pokok }}</td>
                <td class="text-right">{{ $item->denda }}</td>
                <td class="text-right">{{ $item->total }}</td>

                <td>
                    @if ($item->amount_pay != '')
                        <span class="text-success"><i class="fas fa-check"></i> Lunas</span>
                        {{-- <br>
                        <span class="text-info">{{ $item->pjsp }}<br>
                            {{ $item->transactiondate }}
                        </span> --}}
                    @else
                        @php
                            $a = strtotime(date('Ymdhis'));
                            $b = strtotime($item->expired_date);
                        @endphp
                        @if ($b > $a)
                            <span class="text-info"><i class="fas fa-exclamation-triangle"></i> Belum
                                lunas</span>
                        @else
                            <span class="text-danger"><i class="far fa-window-close"></i> Expired</span>
                        @endif
                    @endif
                </td>
                <td>{{ $item->pjsp }}</td>
                <td>
                    {{ $item->transactiondate }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
