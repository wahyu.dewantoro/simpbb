@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Realisasi Target APBD</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        {{-- @can('add_users') --}}
                        <a href="{{ route('realisasi.targetrealisasi.create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-plus"></i> Tambah
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2" width="5%" style="text-align: center">No</th>
                                        <th rowspan="2" style="text-align: center">Buku</th>
                                        <th rowspan="2" style="text-align: center">Sektor</th>
                                        <th class="text-center" colspan="3">Target</th>
                                        <th rowspan="2"></th>
                                    </tr>
                                    <tr>
                                        <th width="18%" style="text-align: center"> Awal</th>
                                        <th width="18%" style="text-align: center">PAK 1</th>
                                        <th width="18%" style="text-align: center">PAK 2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @if(count($data)>0)
                                    @foreach ($data as $index => $row)
                                        <tr>
                                            <td width="5px" align="center">{{ $no }}</td>
                                            <td>{{ $row->buku }}</td> 
                                            <td>{{ $row->sektor }}</td>
                                            <td style="text-align: right">{{ $row->awal<>'' ?number_format($row->awal,0,'','.'):'' }}</td>
                                            <td style="text-align: right">{{ $row->perubahan_1<>'' ?number_format($row->perubahan_1,0,'','.'):'' }}</td>
                                            <td style="text-align: right">{{ $row->perubahan_2<>'' ?number_format($row->perubahan_2,0,'','.'):'' }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('realisasi.targetrealisasi.edit',trim($row->tahun).'.'.trim($row->kd_buku).'.'.trim($row->kd_sektor)) }}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="6">Tidak ada data</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
