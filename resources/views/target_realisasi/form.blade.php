@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">

            <h1>{{ $data['title'] }}</h1>

        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form action="{{ $data['action'] }}" method="post" class="form-horizontal">
                            <div class="card-body">
                                @csrf
                                @method($data['method'])
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <label for="tahun" class="col-md-6 col-form-label float-right">Tahun</label>
                                            <div class="col-md-6">
                                                <input type="number" name="tahun" id="tahun" class="form-control form-control-sm" maxlength="4" value="{{ $data['target']->tahun??'' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <label for="jenis" class="col-md-3 col-form-label float-right">Buku</label>
                                            <div class="col-md-9">
                                                <select name="kd_buku" id="kd_buku" required
                                                    class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @php
                                                        $srb=$data['target']->kd_buku??'';
                                                    @endphp
                                                    @foreach ($data['buku'] as $irb => $rb)
                                                        <option @if($srb==$irb) selected @endif value="{{ $irb }}">{{ $rb }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <label for="jenis" class="col-md-6 col-form-label">Sektor</label>
                                            <div class="col-md-6">
                                                <select name="kd_sektor" id="kd_sektor" required
                                                    class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @php
                                                        $ss=$data['target']->kd_sektor??'';
                                                    @endphp
                                                    @foreach ($data['sektor'] as $irs => $rs)
                                                        <option @if($irs==$ss) selected @endif value="{{ $irs }}">{{ $rs }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <label for="awal" class="col-md-3 col-form-label">Awal</label>
                                            <div class="col-md-5">
                                                <input type="text" name="awal" id="awal"
                                                    class="form-control form-control-sm angka" required value="{{ $data['target']->awal??'' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="perubahan_1" class="col-md-3 col-form-label">Perubahan 1</label>
                                            <div class="col-md-5">
                                                <input type="text" name="perubahan_1" id="perubahan_1"
                                                    class="form-control form-control-sm angka" value="{{ $data['target']->perubahan_1??'' }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="perubahan_2" class="col-md-3 col-form-label">Perubahan 2</label>
                                            <div class="col-md-5">
                                                <input type="text" name="perubahan_2" id="perubahan_2"
                                                    class="form-control form-control-sm angka" value="{{ $data['target']->perubahan_2??'' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                {{-- <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" id="awal" name="jenis" value="1" checked="">
                                                <label for="customRadio1" class="custom-control-label">Awal</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" id="customRadio2" name="jenis" value="2">
                                                <label for="customRadio2" class="custom-control-label">Perubahan 1</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" id="customRadio3" name="jenis" value="3">
                                                <label for="customRadio3" class="custom-control-label">Perubahan 2</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="exampleInputFile"><b>Buku 1 dan 2</b></label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label col-md-4">Perdesaan</label>
                                            <div class="col-md-12">
                                                <input type="text" name="perdesaan_12" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label col-md-4">Perkotaan</label>
                                            <div class="col-md-12">
                                                <input type="text" name="perkotaan_12" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="exampleInputFile"><b>Buku 3,4, dan 5</b></label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label col-md-4">Perdesaan</label>
                                            <div class="col-md-12">
                                                <input type="text" name="perdesaan_345" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="control-label col-md-4">Perkotaan</label>
                                            <div class="col-md-12">
                                                <input type="text" name="perkotaan_345" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                {{-- <input type="hidden" name="tahun" value="{{ $data['tahun'] }}"> --}}
                                <!-- <div class="form-group">
                                                                                        <button class="btn btn-primary">Simpan</button>
                                                                                    </div> -->
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <button class="btn btn-primary btn-sm">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.angka').keyup();

            $('.angka').on('keyup', function() {
                var this_ = $(this).val();
                console.log(this_)
                var res = formatangka(this_);
                $(this).val(res);
            });

        });

        function formatangka(objek) {
            a = objek;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + "." + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                }
            }
            // objek.value = c;
            return c;
        };
    </script>
@endsection
