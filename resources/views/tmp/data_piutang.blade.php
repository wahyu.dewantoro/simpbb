<table>
    <thead>
        <tr>
            <th>NOP</th>
            <th>Status</th>
            <th>Kecamatan</th>
            <th>Kelurahan</th>
            <th>Alamat Objek</th>
            <th>Tahun</th>
            <th>Wajib Pajak</th>
            <th>Alamat WP</th>
            <th>PBB</th>
            <th>BAYAR</th>
            <th>KURANG BAYAR</th>
            <th>Ket</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{{ $row->nop }}</td>
                <td>{{ $row->is_aktif=='1'?'Aktif':'Non Aktif' }}</td>
                <td>{{ $row->nm_kecamatan }}</td>
                <td>{{ $row->nm_kelurahan }}</td>
                <td>{{ $row->alamat_op }}</td>
                <td>{{ $row->tahun_pajak }}</td>
                <td>{{ $row->nm_wp_sppt }}</td>
                <td>{{ $row->alamat_wp }}</td>
                <td>{{ $row->pbb }}</td>
                <td>{{ $row->bayar }}</td>
                <td>{{ $row->kurang_bayar }}</td>
                <td>@if($row->status_pembayaran_sppt=='3') ** @endif</td>
            </tr>
        @endforeach
    </tbody>
</table>

