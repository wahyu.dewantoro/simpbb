<table>
    <thead>
        <tr>
            <th>nop</th>
            <th>tahun</th>
            <th>pbb</th>
            <th>tanggal</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{{ formatnop($row->nop) }}</td>
                <td>{{ $row->tahun }}</td>
                <td>{{ $row->pbb }}</td>
                <td>{{ $row->tanggal }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
