@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Penelitian</h1>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fas fa-exclamation-triangle"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?= $pesan; ?><br>
                            <a href="{{ url('penelitian/kantor') }}"><i class="fas fa-angle-double-left"></i> Kembali</a>
                        </span>
                        
                    </div>
                </div>


            </div>
        </div>


        {{-- <div class="error-page">
            <h2 class="headline text-warning"> 404</h2>
            <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops!</h3>
                <h3>
                    @php
                        echo $pesan;
                    @endphp
                    <br>
                    <a href="{{ url('penelitian/kantor') }}">Kembali</a>
                </h3>
            </div>
        </div> --}}
    </section>
@endsection
