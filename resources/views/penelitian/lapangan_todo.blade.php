@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Penelitian Lapangan</h1>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-header p-0 pt-1">
                    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true"> <i class="fas fa-list-ol"></i> Penelitian</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false"> <i class="fas fa-book"></i> Hasil Penelitian</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body p-0">
                    <div class="tab-content" id="custom-tabs-two-tabContent">
                        <div class="tab-pane fade show active p-0" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                            <table class="table table-sm table-bordered table-hover table-checkable" id="table-objek" style="width:100%">
                                <thead class="bg-success">
                                    <tr>
                                        <th style="width:1px !important;">No</th>
                                        <th class="text-center">Nopel</th>
                                        <th class="text-center">Ajuan</th>
                                        <th class="text-center">NOP</th>
                                        <th class="text-center">Wajib Pajak</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Aksi</th>

                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane fade p-0" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                            <table class="table table-striped table-sm table-bordered table-hover table-checkable" id="table-hasil" style="width:100%">
                                <thead class="bg-danger">
                                    <tr>
                                        <th style="width:1px !important;">No</th>
                                        <th>Penelitian</th>
                                        <th>Ajaun</th>
                                        <th>NOP</th>
                                        <th>Wajib Pajak</th>
                                        <th>Alamat WP</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Hasil Penelitian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>
<style>
    table {
        text-transform: uppercase;
    }

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableObjek, tableHasil


        tableObjek = $('#table-objek').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('penelitian/lapangan') }}?tab=task"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }

                , {
                    data: 'nomor_layanan'
                    , orderable: false
                    , name: 'nomor_layanan'
                },

                {
                    data: 'jenis_layanan_nama'
                    , orderable: false
                    , name: 'nomor_layanan'
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , name: 'nama_nop'
                }
                , {
                    data: 'nama_wp'
                    , orderable: false
                    , name: 'nama_nop'

                }
                , {
                    data: 'keterangan'
                    , orderable: false
                    , name: 'alamat_op'
                    , searchable: false
                }

                , {
                    data: 'pilih'
                    , orderable: false
                    , name: 'pilih'
                    , searchable: false
                    , class: "text-center"
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , iDisplayLength: 10
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , rowCallback: function(row, data, index) {}
        });

        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('penelitian/lapangan') }}?tab=hasil"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false

                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'noformulir'
                    , orderable: false
                    , name: 'a.no_formulir'
                    , render: function(data, type, row) {
                        return data + '<br><small class="text-warning">' + arrayTransaksi[row.jns_transaksi] + '</small>'
                    }
                }
                , {
                    data: 'nama_layanan'
                    , orderable: false
                    , render: function(data, type, row) {
                        return data + '<br><small class="text-warning">' + row.nomorlayanan + '</small>'
                    }
                }
                , {
                    data: 'nopproses'
                    , orderable: false
                    , name: 'nop'
                }
                , {
                    data: 'nm_wp'
                    , orderable: false
                    , name: 'a.nm_wp'
                }
                , {
                    data: 'kelurahan_wp'
                    , orderable: false
                    , name: 'a.kelurahan_wp'
                    , render: function(data, type, row) {
                        return data + '<br><small class="text-warning">' + row.kota_wp + '</small>'
                    }
                }
                , {
                    data: 'pilih'
                    , orderable: false
                    , name: 'pilih'
                    , searchable: false
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });

        $('#table-hasil').on('click', '.cetak-tagihan', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            url = "{{ url('penelitian/nota-perhitungan-pdf') }}/" + id;
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return true;
        })

        $('#table-hasil').on('click', '.detail-objek', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            $('.modal-body').html('')
            openloading();
            $.ajax({
                url: "{{ url('penelitian/lapangan') }}/" + id
                , success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-xl").modal('show');
                }
                , error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('.modal-body').html('')
                }
            });
        })
    })

</script>
@endsection
