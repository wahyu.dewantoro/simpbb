<div class="card-body table-responsive-sm p-1">

    <table class="table table-sm table-bordered">
        <thead class="bg-info">
            <tr>
                <th style="width: 30px">No</th>
                <th>NOP</th>
                <th>Wajib Pajak</th>
                <th>Status</th>
                <th style="width: 25px">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @php
            $all=$data->count();
            $entri=0;
            @endphp
            @foreach ($data as $idx=> $row)
            <tr>
                <td class="text-center">{{ $idx+1 }}</td>
                <td class="text-left">
                    @if($row->nop_proses<>'')
                        {{ formatnop($row->nop_proses)}}
                        @else
                        {{
                        $row->kd_propinsi.'.'.
                        $row->kd_dati2.'.'.
                        $row->kd_kecamatan.'.'.
                        $row->kd_kelurahan.'.'.
                        $row->kd_blok.'-'.
                        $row->no_urut.'.'.
                        $row->kd_jns_op
                    }}
                        @endif
                        <br>
                        @if($row->nop_gabung=='')
                        <small class="text-danger">Induk</small>
                        @else
                        <small class="text-warning">Pecah</small>
                        @endif
                </td>
                <td>{{ strtoupper($row->nama_wp) }}</td>
                <td>
                    @if ($row->ido<>'')
                        @php
                        $entri+=1;
                        @endphp
                        <span class="text-success">Hasil penelitian telah di isi.</span>
                        @else
                        <span class="text-warning">Belum di isi.</span>
                        @endif
                </td>
                <td class="text-center">
                    <a data-id_a="{{ $id }}" data-induk="{{ $induk }}" data-id="{{ $row->id }}" data-jns_penelitian="{{ $penelitian }}" role="button" class="penelitian" tabindex="0"><i class="text-success fas fa-edit"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="float-right">
        <button data-id="{{ $id }}" data-induk="{{ $induk }}" @if($entri<>$all) disabled @endif id="proses" class="btn btn-sm btn-info"><i class="far fa-check-circle"></i> Proses</button>
    </div>
</div>
<script>
    $('#proses').on('click', function(ev) {
        ev.preventDefault()
        id_ = $(this).data('id')
        induk_ = $(this).data('induk')
        openloading()
        $.ajax({
            url: "{{ url('penelitian/kantor-pecah-proses') }}"
            , data: {
                id: id_
                , induk: induk_
            }
            , success: function(res) {

                console.log(res)
                if (res.status == '1') {
                    closeloading()
                    window.location.href = "{{ url('penelitian/hasil-penelitian') }}/" + id_
                } else {
                    Swal.fire({
                        icon: "error"
                        , title: "Peringatan"
                        , text: res.pesan
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    });
                }

            }
            , error: function(er) {
                // closeloading()
                Swal.fire({
                    icon: "error"
                    , title: "Peringatan"
                    , text: "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem."
                    , allowOutsideClick: false
                    , allowEscapeKey: false
                });

                console.log(er)
            }
        })
    })


    $('.penelitian').on('click', function(e) {
        e.preventDefault()
        $('#konten-penelitian').html('');
        // data-pecah
        openloading()
        id = $(this).data('id')
        penelitian = $(this).data('jns_penelitian')
        $.ajax({
            url: "{{ url('penelitian/kantor-form-pecah') }}"
            , data: {
                'id': id
                , 'penelitian': penelitian
                , 'id_a': $(this).data('id_a')
                , 'induk': $(this).data('induk')
            , }
            , success: function(res) {
                closeloading()
                $('#data-pecah').hide()
                $('#konten-penelitian').html(res);
            }
            , error: function(er) {
                closeloading()
                console.log(er)
            }
        })
    })

</script>
