<head>
    @include('layouts.style_pdf')
</head>

<body style="font-size:12px">
    @include('layouts.kop_pdf')
    <div id="watermark"><img src="{{ public_path('kabmalang_black.png') }}"></div>
    <h3 class="text-tengah"><strong><u>NOTA PERHITUNGAN PBB P2</u></strong><br>Nomor :{{ $nota->nomor }}</h3>

    <p>Dasar :</p>
    <ol>
        <li>Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan Pemerintahan Daerah; </li>
        <li>Peraturan Pemerintah Nomor 35 Tahun 2023 tentang Ketentuan Umum Pajak Daerah dan Retribusi Daerah;</li>
        <li>Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010 tentang Pajak Daerah; </li>    
    </ol>
    <table width="500px">
        <tbody>
            <tr style="vertical-align: top">
                <td width="70px">Nama</td>
                <td width="1px">:</td>
                <td>{{ $nota->nm_wp }}</td>
            </tr>
            <tr style="vertical-align: top">
                <td>Alamat</td>
                <td>:</td>
                <td>
                    <?=
                     $nota->jalan_wp.' '.
                     $nota->blok_kav_no_wp .' RT '.($nota->rt_wp!=''?$nota->rt_wp:'000').' /  RW '.($nota->rw_wp!=''?$nota->rw_wp:'00').' '.
                     str_replace('-',' ',$nota->kelurahan_wp).' '.  str_replace('-',' ',$nota->kota_wp) 
                ?>
                </td>
            </tr>
            <tr style="vertical-align: top">
                <td>NOP</td>
                <td>:</td>
                <td>
                    {{ formatnop($nota->nop)}}
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <table width="100%" class="table-sm table table-bordered">
        <thead class="bg-info">
            <tr>
                <th class="text-tengah">No</th>
                {{-- <th class="text-tengah">Bumi</th>
                <th class="text-tengah">Bangunan</th> --}}
                {{-- <th class="text-tengah">NOP</th> --}}
                <th class="text-tengah">Tahun </th>
                <th class="text-tengah">Pokok</th>
                <th class="text-tengah">Denda</th>
                <th class="text-tengah">Total</th>
            </tr>
        </thead>
        <tbody>
            @php
            $pokok=0;
            $denda=0;
            $total=0;
            @endphp
            @foreach ($tagihan as $ri=> $item)
            <tr>
                <td class="text-tengah">{{ $ri+1 }}</td>
                {{-- <td class="text-tengah">{{  }}</td>
                <td class="text-tengah">{{ $item->luas_bng_sppt }}</td> --}}
                {{-- <td>
                    {{ $item->kd_propinsi }}.{{ $item->kd_dati2 }}.{{ $item->kd_kecamatan }}.{{ $item->kd_kelurahan }}.{{ $item->kd_blok }}-{{ $item->no_urut }}.{{ $item->kd_jns_op }}

                </td> --}}
                <td class="text-tengah">{{ $item->thn_pajak_sppt }}</td>
                <td class="text-kanan">{{ angka($item->pokok) }}</td>
                <td class="text-kanan">{{ angka($item->denda) }}</td>
                <td class="text-kanan">{{ angka($item->total) }}</td>
            </tr>
            @php
            $pokok+=$item->pokok;
            $denda+=$item->denda;
            $total+=$item->total;
            @endphp
            @endforeach
        </tbody>
        <tfoot>
            <td class="text-tengah" colspan="2"><b>Total</b></td>
            <td class="text-kanan">{{ angka($pokok) }}</td>
            <td class="text-kanan">{{ angka($denda) }}</td>
            <td class="text-kanan">{{ angka($total) }}</td>
        </tfoot>
    </table>
    <table width="500px">
        <tbody>
            <tr>
                <td width="170px"><b>Kode Billing</b></td>
                <td width="1px">:</td>
                <td><strong>{{ $nota->kobil }}</strong></td>
            </tr>
            <tr>
                <td><b>Tahun Pajak</b></td>
                <td>:</td>
                <td><b>{{ $nota->tahun_pajak }}</b></td>
            </tr>
            <tr>
                <td>Tanggal Kadaluarsa</td>
                <td>:</td>
                <td>{{ tglIndo($nota->expired_at) }}</td>
            </tr>
        </tbody>
    </table>
    <p>Segera lakukan pembayaran melalui <strong>{{ strtoupper($tp->nama_tempat) }}</strong> dengan menggunakan <b>KODE BILLING</b> sebelum tanggal kadaluarsa.</p>
    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%">
                    @if ($nota->tgl_bayar<>'')
                        <img width="150px" src="{{ public_path('lunas.png') }}">
                        @endif

                </td>
                <td></td>
                <td width="40%" align="center">
                    <p>{{ strtoupper($nota->kota) }}, {{ tglIndo($nota->tanggal) }}<br><b>a/n KEPALA BADAN PENDAPATAN DAERAH</b><br>Kepala Bidang PBB P2<br>
                        <br>
                        <img src="data:image/png;base64,{!!$url!!}" class="qrcode" width="70px">
                        <br><u><b>{{ $nota->pegawai}}</b></u><br>
                        PEMBINA <br>
                        NIP.{{ $nota->nip}}
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <footer>
    </footer>
</body>
</html>
