@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Hasil Penelitian</h1>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="pencariandata form-control form-control-sm" name="tahun" id="tahun">
                                        @for($tahun = date('Y'); $tahun >= 2022; $tahun--)
                                        <option value="{{ $tahun}}">{{ $tahun }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select class="pencariandata form-control form-control-sm" name="jenis_layanan_id" id="jenis_layanan_id">
                                        <option value="">Layanan</option>
                                        @foreach ($jnslayanan as $jl)
                                        <option value="{{ $jl->id }}">{{ $jl->nama_layanan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="float-right">
                                <input type="text" placeholder="Pencarian" name="keyword" id="keyword" class="form-control form-control-sm pencariandata">
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-sm table-bordered table-hover text-sm" id="table-hasil" style="width:100%">
                        <thead class="bg-danger">
                            <tr>
                                <th style="width:1px !important;">No</th>
                                {{-- <th>Penelitian</th> --}}
                                <th>Layanan</th>
                                <th>NOP</th>
                                <th>Wajib Pajak</th>
                                <th>Alamat WP</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl" style="max-width: 98%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Hasil Penelitian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>
<style>
    
    .dataTables_filter,
    .dataTables_length,
    .dataTables_info {
        display: none;
    }

</style>
@endsection
@php $search=''; @endphp
@if(isset($nomor_layanan)&&$nomor_layanan)
@php $search='?nomor_layanan='.$nomor_layanan['nomor_layanan']; @endphp
@endif
@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableObjek, tableHasil
        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
                // , ajax: "{{  url('penelitian/hasil-penelitian'.$search) }}"
            , ajax: {
                url: "{{  url('penelitian/hasil-penelitian'.$search) }}"
                , data: function(d) {
                    d.tahun = $('#tahun').val();
                    d.jenis_layanan_id = $('#jenis_layanan_id').val()
                    d.keyword = $('#keyword').val()
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false

                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                /* , {
                    data: 'nomorlayanan'
                    , orderable: false
                    , name: 'nomorlayanan'
                } */
                , {
                    data: 'nama_layanan'
                    , orderable: false
                    , name: 'nama_layanan'
                    , render: function(data, type, row) {
                        return data + '<br><small class="text-info">' + row.nomorlayanan + '</small>'
                    }
                }
                , {
                    data: 'nopproses'
                    , orderable: false
                    , name: 'nop'
                }
                , {
                    data: 'nm_wp'
                    , orderable: false
                    , name: 'a.nm_wp'
                }
                , {
                    data: 'kelurahan_wp'
                    , orderable: false
                    , name: 'a.kelurahan_wp'
                    , render: function(data, type, row) {
                        return data + '<br><small class="text-warning">' + row.kota_wp + '</small>'
                    }
                }
                , {
                    data: 'pilih'
                    , orderable: false
                    , name: 'pilih'
                    , searchable: false
                    , class: "text-sm text-center"
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });

        function pencarian() {
            tableHasil.draw();
        }

        $(document).on('change keyup', '.pencariandata', function() {
            pencarian();
        });

        $('#table-hasil').on('click', '.cetak-tagihan', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            url = "{{ url('penelitian/nota-perhitungan-pdf') }}/" + id;
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return true;
        })


        // 

        $('#table-hasil').on('click', '.cetak-sk', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            url = $(this).data('url')
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return true;
        })

        $('#table-hasil').on('click', '.cancel_penelitian', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            url = "{{ url('penelitian/hasil-penelitian-cancel') }}/" + id

            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "penelitian  yang dibatalkan tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    document.location.href = url;
                }
            })

        })

        $('#table-hasil').on('click', '.detail-objek', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            $('.modal-body').html('')
            openloading();
            $.ajax({
                url: "{{ url('penelitian/kantor') }}/" + id
                , success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-xl").modal('show');
                }
                , error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('.modal-body').html('')
                }
            });
        })
    })

</script>
@endsection
