@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Hasil Penelitian</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a href="{{ $urlback }}" class="btn btn-sm btn-info"><i class="fas fa-angle-double-left"></i> Back todolist</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            @if (count($tagihan)>0)
            <div class="col-6">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h3 class="card-title">Nota Perhitungan PBB</h3>
                        <div class="card-tools">
                            <button id="cetak-tagihan" class="btn btn-sm btn-success"><i class="fas fa-print"></i> Cetak</button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div id="table-tagihan">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th>No</th>
                                   
                                        <th>Tahun </th>
                                        <th>Pokok</th>
                                        <th>Denda</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($tagihan as $ri=> $item)
                                    <tr>
                                        <td class="text-center">{{ $ri+1 }}</td>
                                      
                                        <td class="text-center">{{ $item->thn_pajak_sppt }}</td>
                                        <td class="text-right">{{ angka($item->pokok) }}</td>
                                        <td class="text-right">{{ angka($item->denda) }}</td>
                                        <td class="text-right">{{ angka($item->total) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-outline" id="data-pecah">
                    <div class="card-body p-1">
                        <div id="konten-nop"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var id = "{{ $id }}";

        function loadLhp(id) {
            openloading()
            $.ajax({
                url: "{{ url('penelitian/kantor') }}/" + id
                , success: function(res) {
                    $('#konten-nop').html(res)
                    closeloading()
                }
                , error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('#konten-nop').html('')
                }
            });
        }

        loadLhp(id)

        $('#cetak-tagihan').on('click', function(e) {
            e.preventDefault()
            url = "{{ url('penelitian/nota-perhitungan-pdf',acak($id)) }}";
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return true;
        })

    })

</script>
@endsection
