<head>
    @include('layouts.style_pdf')
</head>
<body>
    @include('layouts.kop_pdf')
    <div id="watermark"><img src="{{ public_path('kabmalang_black.png') }}"></div>
    <p class="text-tengah"> <strong><u>NOTA PERHITUNGAN PBB-P2</u></strong></p>
    <p class="text-tengah">{{ $jenis  }}, {{ $jenis_objek }} , {{ $bayar }}</p>

    @include('penelitian.nota-perhitungan-print-konten',['data'=>$data,'excel'=>0])
    <footer>
    </footer>
</body>
</html>
