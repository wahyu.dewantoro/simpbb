<div class="row">
    <div class="col-12">
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <th class="bg-warning">Data Objek</th>
                </tr>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-6">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>Nomor Layanan</td>
                                            <td width="1px">:</td>
                                            <td>{{ isset($objek->layananObjek->nomor_layanan)?formatNomor($objek->layananObjek->nomor_layanan):null }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ajuan</td>
                                            <td width="1px">:</td>
                                            <td>{{ strtoupper(optional($objek->jenisLayanan)->nama_layanan) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Transaksi</td>
                                            <td width="1px">:</td>
                                            <td>{{ jenisTransaksiTanah($objek->jns_transaksi) }} 
                                                @if ($objek->keterangan_pembatalan!='')
                                                <br>[<span class="text-danger"> {{$objek->keterangan_pembatalan}}</span>]
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>NOP Proses</td>
                                            <td width="1px">:</td>
                                            <td>{{ formatnop($objek->nop_proses) }}</td>
                                        </tr>
                                        @if ($objek->nop_bersama<>'')
                                            <tr>
                                                <td>NOP Bersama</td>
                                                <td width="1px">:</td>
                                                <td>{{ formatnop($objek->nop_bersama) }}</td>
                                            </tr>
                                            @endif
                                            @if($objek->nop_asal<>'')
                                                <tr>
                                                    <td>NOP Asal</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ formatnop($objek->nop_asal) }}</td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td>Nomor Persil</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $objek->no_persil }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat OP</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }} <br>
                                                        RT: {{ $objek->rt_op }} RW : {{ $objek->rw_op }}

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Status WP</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ statusWP($objek->kd_status_wp) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>ZNT</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $objek->kd_znt }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Luas</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ angka($objek->luas_bumi) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Tanah</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ jenisBumi($objek->jns_bumi) }}</td>
                                                </tr>
                                    </tbody>
                                </table>
                                <b>Berkas Terampir saat permohonan:</b>
                                <ol>
                                    @foreach ($berkasajuan as $ik=> $berkas)
                                    <li>{{ $berkas }} [ {{$ik}} ]</li>
                                    @endforeach
                                </ol>
                            </div>
                            <div class="col-6">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>NIK</td>
                                            <td width="1px">:</td>
                                            <td>{{ $objek->subjek_pajak_id }}</td>
                                        </tr>
                                        <tr>
                                            <td>Wajib Pajak</td>
                                            <td width="1px">:</td>
                                            <td>{{ $objek->nm_wp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td width="1px">:</td>
                                            <td>{{ $objek->jalan_wp }} {{ $objek->blok_kav_no_wp }} <br>
                                                RT: {{ $objek->rt_wp }} RW : {{ $objek->rw_wp }}<br>
                                                {{ $objek->kelurahan_wp }} <br>
                                                {{ $objek->kota_wp }} {{ $objek->kd_pos_wp }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Telepon</td>
                                            <td width="1px">:</td>
                                            <td>{{ $objek->telp_wp }}</td>
                                        </tr>
                                        <tr>
                                            <td>NPWP</td>
                                            <td width="1px">:</td>
                                            <td>{{ $objek->npwp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan</td>
                                            <td width="1px">:</td>
                                            <td>{{ jenisPekerjaan($objek->status_pekerjaan_wp) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Perekaman</td>
                                            <td width="1px">:</td>
                                            <td>{{ tglIndo($objek->created_at) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Diteliti Oleh</td>
                                            <td width="1px">:</td>
                                            <td>{{ $objek->user->nama }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                @if ($objek->jns_transaksi<>3 && !empty($lhp))
                    <tr>
                        <th class="bg-warning">Data NJOP</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-sm table-bordered ">
                                        <thead>
                                            <tr style="vertical-align: middle">
                                                <th class="text-center text-small" colspan="7">Sebelum</th>
                                                <th class="text-center text-small" colspan="9">Sesudah</th>
                                            </tr>
                                            <tr style="vertical-align: middle">
                                                <th class="text-center text-small" rowspan="2">NOP</th>
                                                <th class="text-center text-small" colspan="3">Bumi</th>
                                                <th class="text-center text-small" colspan="3">Bangunan</th>
                                                <th class="text-center text-small" rowspan="2">NOP</th>
                                                <th class="text-center text-small" colspan="4">Bumi</th>
                                                <th class="text-center text-small" colspan="4">Bangunan</th>
                                            </tr>
                                            <tr style="vertical-align: middle">
                                                <th class="text-center text-small">Luas</th>
                                                <th class="text-center text-small">NJOP/M<sup>2</sup></th>
                                                <th class="text-center text-small">NJOP</th>
                                                <th class="text-center text-small">Luas</th>
                                                <th class="text-center text-small">NJOP/M<sup>2</sup></th>
                                                <th class="text-center text-small">NJOP</th>
                                                <th class="text-center text-small">Luas</th>
                                                <th class="text-center text-small">NJOP/M<sup>2</sup></th>
                                                <th class="text-center text-small">NJOP</th>
                                                <th class="text-center text-small">%</th>
                                                <th class="text-center text-small">Luas</th>
                                                <th class="text-center text-small">NJOP/M<sup>2</sup></th>
                                                <th class="text-center text-small">NJOP</th>
                                                <th class="text-center text-small">%</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                            @php
                                            $jumr=$lhp['last']->count();
                                            $hasil=$lhp['hasil'];
                                            @endphp
                                            @foreach ($lhp['last'] as $i=> $last)
                                            @php
                                            $last_nj_bm= $last->total_luas_bumi>0? $last->njop_bumi / $last->total_luas_bumi:0;
                                            $last_bj_bng=$last->total_luas_bng>0?$last->njop_bng/$last->total_luas_bng:0;

                                            $hasil_nj_bm= $hasil->total_luas_bumi>0? $hasil->njop_bumi / $hasil->total_luas_bumi:0;
                                            $hasil_bj_bng=$hasil->total_luas_bng>0?$hasil->njop_bng/$hasil->total_luas_bng:0;

                                            $persen_bumi=$last_nj_bm > 0 ? angka(($hasil_nj_bm-$last_nj_bm)/$last_nj_bm *100,2):'-';
                                            $persen_bng=$last_bj_bng>0?angka(($hasil_bj_bng-$last_bj_bng)/$last_bj_bng *100,2):'-';
                                            @endphp

                                            <tr>
                                                <td>
                                                    {{ $last->kd_propinsi}}.{{ $last->kd_dati2}}.{{ $last->kd_kecamatan}}.{{ $last->kd_kelurahan}}.{{ $last->kd_blok}}-{{ $last->no_urut}}.{{ $last->kd_jns_op}}
                                                </td>
                                                <td class="text-center">{{ angka($last->total_luas_bumi) }}</td>
                                                <td class="text-right">{{ angka($last_nj_bm)   }}</td>
                                                <td class="text-right">{{ angka($last->njop_bumi) }}</td>
                                                <td class="text-center">{{ angka($last->total_luas_bng) }}</td>
                                                <td class="text-right">{{ angka($last_bj_bng) }}</td>
                                                <td class="text-right">{{ angka($last->njop_bng) }}</td>
                                                @if($i==0)
                                                <td rowspan="{{ $jumr }}">
                                                    {{ $hasil->kd_propinsi}}.{{ $hasil->kd_dati2}}.{{ $hasil->kd_kecamatan}}.{{ $hasil->kd_kelurahan}}.{{ $hasil->kd_blok}}-{{ $hasil->no_urut}}.{{ $hasil->kd_jns_op}}
                                                </td>
                                                <td class="text-center" rowspan="{{ $jumr }}">{{ angka($hasil->total_luas_bumi) }}</td>
                                                <td class="text-right" rowspan="{{ $jumr }}">{{ angka($hasil_nj_bm) }}</td>
                                                <td class="text-right" rowspan="{{ $jumr }}">{{ angka($hasil->njop_bumi) }}</td>
                                                <td class="text-center text-small" rowspan="{{ $jumr }}">{{ $persen_bumi}} %</td>
                                                <td class="text-center" rowspan="{{ $jumr }}">{{ angka($hasil->total_luas_bng) }}</td>
                                                <td class="text-right" rowspan="{{ $jumr }}">{{ angka($hasil_bj_bng) }}</td>
                                                <td class="text-right" rowspan="{{ $jumr }}">{{ angka($hasil->njop_bng) }}</td>
                                                <td class="text-center text-small" rowspan="{{ $jumr }}">{{ $persen_bng}} %</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endif
            </tbody>
        </table>
        <table class="table table-sm table-bordered">
            @if(count((array)$bng)>0)
            @foreach($bng as $key => $rbng)
            <tr>
                <th class="bg-warning">Data Bangunan ke {{ $rbng->no_bng }}</th>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-sm table-borderless">
                                <tbody>

                                    <tr>
                                        <td>Transaksi</td>
                                        <td width="1px">:</td>
                                        <td>{{ jenisTransaksiBangunan($rbng->jns_transaksi) }}</td>
                                    </tr>
                                    <tr>
                                        <td>NOP</td>
                                        <td width="1px">:</td>
                                        <td>{{ formatnop($rbng->nop) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Fungsi Bangunan</td>
                                        <td width="1px">:</td>
                                        <td>{{ penggunaanBangunan($rbng->kd_jpb) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Dibangun</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->thn_dibangun_bng }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Direnovasi</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->thn_renovasi_bng }}</td>
                                    </tr>
                                    <tr>
                                        <td>Luas</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->luas_bng) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Lantai</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->jml_lantai_bng }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Kondisi</td>
                                        <td width="1px">:</td>
                                        <td>{{ kondisi($rbng->kondisi_bng) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Konstruksi</td>
                                        <td width="1px">:</td>
                                        <td>{{ kontruksi($rbng->jns_konstruksi_bng) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Atap</td>
                                        <td width="1px">:</td>
                                        <td>{{ atap($rbng->jns_atap_bng) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Dinding</td>
                                        <td width="1px">:</td>
                                        <td>{{ dinding($rbng->kd_dinding) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Lantai</td>
                                        <td width="1px">:</td>
                                        <td>{{ lantai($rbng->kd_lantai) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Langit - langit</td>
                                        <td width="1px">:</td>
                                        <td>{{ langit($rbng->kd_langit_langit) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Daya Listrik</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->daya_listrik) }} Watt</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">AC</td>
                                    </tr>
                                    <tr>
                                        <td>AC Split</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->acsplit<>''?$rbng->acsplit:0 }} Biji</td>
                                    </tr>
                                    <tr>
                                        <td>AC Window</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->acwindow<>''?$rbng->acwindow:0 }} Biji</td>
                                    </tr>
                                    <tr>
                                        <td>AC Central</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->acsentral=='1'?'Ada':'Tidak Ada' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Kolam Renang</td>
                                    </tr>
                                    <tr>
                                        <td>Luas Kolam</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->luas_kolam) }} M<sup>2</sup></td>
                                    </tr>
                                    <tr>
                                        <td>Finishing Kolam</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->finishing_kolam=='1'?'Di Plester':'Dengan Pelapis' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Perkerasan Halaman</td>
                                    </tr>
                                    <tr>
                                        <td> Ringan</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->luas_perkerasan_ringan) }} M<sup>2</sup></td>
                                    </tr>
                                    <tr>
                                        <td> Sedang</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->luas_perkerasan_sedang) }} M<sup>2</sup></td>
                                    </tr>
                                    <tr>
                                        <td> Berat</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->luas_perkerasan_berat) }} M<sup>2</sup></td>
                                    </tr>
                                    <tr>
                                        <td> Dengan Penutup</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->luas_perkerasan_dg_tutup) }} M<sup>2</sup></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Lapangan Tenis</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="table table-sm table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>Dengan Lampu</td>
                                                        <td>Tanpa Lampu</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="table table-sm table-borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Beton</td>
                                                                        <td width="1px">:</td>
                                                                        <td>{{ angka($rbng->lap_tenis_lampu_beton) }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Aspal</td>
                                                                        <td width="1px">:</td>
                                                                        <td>{{ angka($rbng->lap_tenis_lampu_aspal) }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Rumput</td>
                                                                        <td width="1px">:</td>
                                                                        <td>{{ angka($rbng->lap_tenis_lampu_rumput) }}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table class="table table-sm table-borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Beton</td>
                                                                        <td width="1px">:</td>
                                                                        <td>{{ angka($rbng->lap_tenis_beton) }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Aspal</td>
                                                                        <td width="1px">:</td>
                                                                        <td>{{ angka($rbng->lap_tenis_aspal) }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Rumput</td>
                                                                        <td width="1px">:</td>
                                                                        <td>{{ angka($rbng->lap_tenis_rumput) }}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Lift</td>
                                    </tr>
                                    <tr>
                                        <td>Penumpang</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->lift_penumpang) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kapsul</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->lift_kapsul) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Barang</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->lift_barang) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Tangga Berjalan</td>
                                    </tr>
                                    <tr>
                                        <td>Lebar < 0,80 M</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->tgg_berjalan_a) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Lebar > 0,80 M</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->tgg_berjalan_b) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Pagar</td>
                                    </tr>
                                    <tr>
                                        <td>Panjang</td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->pjg_pagar) }} M</td>
                                    </tr>
                                    <tr>
                                        <td>Bahan</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->bhn_pagar=='1'?'Baja/Besi':($rbng->bhn_pagar=='2'?'Batu bata/ Batako':'') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Pemadan Kebakaran</td>
                                    </tr>
                                    <tr>
                                        <td>Hydrant</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->hydrant=='1'?'Ada':'Tidak ada' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Sprinkler</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->sprinkler=='1'?'Ada':'Tidak ada' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Fire Alarm</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->fire_alarm=='1'?'Ada':'Tidak ada' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td colspan="3" class="bg-info">Saluran</td>
                                    </tr>
                                    <tr>
                                        <td>PABX</td>
                                        <td width="1px">:</td>
                                        <td>{{ $rbng->jml_pabx }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kedalam Sumur Artesis </td>
                                        <td width="1px">:</td>
                                        <td>{{ angka($rbng->sumur_artesis) }} M</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
            @endif
        </table>
    </div>
    <div class="col-12">
        {{-- data pecahan --}}
        @foreach ($pecahobjek as $pch=> $pecahobjek)
        <table class="table table-sm table-bordered">
            <tbody>
                <tr>
                    <th class="bg-info">Data Pecah Objek Ke {{ $pch+1 }}</th>
                </tr>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-6">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>Nomor Formulir</td>
                                            <td width="1px">:</td>
                                            <td>{{ formatNomor($pecahobjek->no_formulir) }}</td>
                                        </tr>

                                        <tr>
                                            <td>Ajuan</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->jenisLayanan->nama_layanan }}</td>
                                        </tr>
                                        <tr>
                                            <td>Transaksi</td>
                                            <td width="1px">:</td>
                                            <td>{{ jenisTransaksiTanah($pecahobjek->jns_transaksi) }}</td>
                                        </tr>
                                        <tr>
                                            <td>NOP Proses</td>
                                            <td width="1px">:</td>
                                            <td>{{ formatnop($pecahobjek->nop_proses) }}</td>
                                        </tr>
                                        @if ($pecahobjek->nop_bersama<>'')


                                            <tr>
                                                <td>NOP Bersama</td>
                                                <td width="1px">:</td>
                                                <td>{{ formatnop($pecahobjek->nop_bersama) }}</td>
                                            </tr>
                                            @endif
                                            @if($pecahobjek->nop_asal<>'')
                                                <tr>
                                                    <td>NOP Asal</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ formatnop($pecahobjek->nop_asal) }}</td>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <td>Nomor Persil</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $pecahobjek->no_persil }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat OP</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $pecahobjek->jalan_op }} {{ $pecahobjek->blok_kav_no_op }} <br>
                                                        RT: {{ $pecahobjek->rt_op }} RW : {{ $pecahobjek->rw_op }}

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Status WP</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ statusWP($pecahobjek->kd_status_wp) }}</td>
                                                </tr>

                                                <tr>
                                                    <td>ZNT</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $pecahobjek->kd_znt }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Luas</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ angka($pecahobjek->luas_bumi) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Tanah</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ jenisBumi($pecahobjek->jns_bumi) }}</td>
                                                </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-6">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>NIK</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->subjek_pajak_id }}</td>
                                        </tr>
                                        <tr>
                                            <td>Wajib Pajak</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->nm_wp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->jalan_wp }} {{ $pecahobjek->blok_kav_no_wp }} <br>
                                                RT: {{ $pecahobjek->rt_wp }} RW : {{ $pecahobjek->rw_wp }}<br>
                                                {{ $pecahobjek->kelurahan_wp }} <br>
                                                {{ $pecahobjek->kota_wp }} {{ $pecahobjek->kd_pos_wp }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Telepon</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->telp_wp }}</td>
                                        </tr>
                                        <tr>
                                            <td>NPWP</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->npwp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan</td>
                                            <td width="1px">:</td>
                                            <td>{{ jenisPekerjaan($pecahobjek->status_pekerjaan_wp) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Perekaman</td>
                                            <td width="1px">:</td>
                                            <td>{{ tglIndo($pecahobjek->created_at) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Diteliti Oleh</td>
                                            <td width="1px">:</td>
                                            <td>{{ $pecahobjek->user->nama }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        @endforeach
    </div>
</div>
