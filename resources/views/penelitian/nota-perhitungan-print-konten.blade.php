<table width="100%" class="table-sm table table-bordered">
    <thead class="bg-info">
        <tr>
            <TH>NO</TH>
            <TH>NOPEL</TH>
            <TH>NOMOR</TH>
            <TH>KOBIL</TH>
            <TH>NOP</TH>
            <TH>NOMINAL</TH>
            <TH>KETERANGAN</TH>
        </tr>
    </thead>
    <tbody>
        @php
        $no=1;
        $total=0;
        @endphp
        @foreach ($data as $row)

        <tr>
            <td class="text-tengah">{{ $no }}</td>
            <td>{{$row->nomor_layanan }}</td>
            <td>{{ $row->nomor }}</td>
            <td>{{ formatnop($row->kobil) }}</td>
            <td>{{ formatnop($row->nop) }}</td>
            <td class="text-kanan">{{ $excel=='0'?angka($row->nominal??0):($row->nominal??0) }}</td>
            <td>{{ $row->ket }}</td>
        </tr>
        @php
        $total +=$row->nominal??0;
        $no++;
        @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5">Total</th>
            <td class="text-kanan">{{ angka($total) }}</td>
            <td></td>
        </tr>
    </tfoot>
</table>
