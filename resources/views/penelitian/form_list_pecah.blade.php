@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Penelitian Mutasi Pecah</h1>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            <div class="card card-outline" id="data-pecah">
                <div id="konten-nop"></div>
            </div>
        </div>
        <div class="col-12">
            <div id="konten-penelitian">
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        // konten-nop
        function loadNop() {
            openloading()
            $.ajax({
                url: "{{ url('penelitian/kantor-pecah-list') }}"
                , data: {
                    id: "{{ $id }}"
                    , induk: "{{ $induk }}"
                }
                , success: function(res) {
                    closeloading()
                    $("#konten-nop").html(res);
                }
                , error: function(er) {
                    console.log(er)
                    closeloading()
                }
            })
        }
        loadNop();
    })

</script>
@endsection
