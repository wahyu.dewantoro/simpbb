@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if ($is_admin == true)
                    <div class="col-lg-3 col-6">

                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ $verifikasi['rayon'] ?? 0 }}</h3>
                                <p>Verifikasi Rayon</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-secret"></i>
                            </div>
                            <a href="{{ url('kelola-nop-verifikasi') }}?jenis=rayon" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-6">

                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{ $verifikasi['developer'] }}</h3>
                                <p>Verifikasi Developer</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-house-user"></i>
                            </div>
                            <a href="{{ url('kelola-nop-verifikasi') }}?jenis=developer" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">

                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3>{{ $verifikasi['wajib_pajak'] }}</h3>
                                <p>Verifikasi Wajib Pajak</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user"></i>
                            </div>
                            <a href="{{ url('kelola-nop-verifikasi') }}?jenis=wp" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $verifikasi['objek'] }}</h3>
                                <p>Usulan Objek</p>
                            </div>
                            <div class="icon">
                                {{-- <i class="fas fa-user-alt"></i> --}}
                                <i class="fas fa-globe-asia"></i>
                            </div>
                            <a href="{{ url('kelola-objek-verifikasi') }}" class="small-box-footer">More info <i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
