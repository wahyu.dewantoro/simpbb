@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Mutasi Pembayaran</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form action="{{ $data['action'] }}" method="post" id="quickForm">
                @csrf
                @method($data['method'])
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">

                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="nop_asal" class="col-sm-3 col-form-label">NOP Asal<span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" id="nop_asal" name="nop_asal"
                                            value="{{ $data['mutasi']->nop_asal ?? '' }}"
                                            class="form-control form-control-sm nop">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nop_tujuan" class="col-sm-3 col-form-label">NOP Tujuan<span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" id="nop_tujuan" name="nop_tujuan"
                                            value="{{ $data['mutasi']->nop_tujuan ?? '' }}"
                                            class="form-control form-control-sm nop">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nop_tujuan" class="col-sm-3 col-form-label">Pembayaran<span
                                            class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <div id="hasil"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <button class="btn btn-sm btn-primary"><i class="far fa-save"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/localization/messages_id.min.js"></script>
    <script>
        $(document).ready(function() {



            $('#quickForm').validate({
                rules: {
                    nop_asal: {
                        required: true,

                    },
                    nop_tujuan: {
                        required: true,

                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

            $('.nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $('#nop_asal').on('change keyup', function() {
                var nop_asal = $('#nop_asal').val()
                var psmb_id = "{{ $data['mutasi']->id ?? '' }}"
                $('#hasil').html('')
                if (nop_asal.length == 24) {
                    Swal.fire({
                        title: 'Sedang mencari ...',
                        html: '<span class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></span>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                    $.ajax({
                        url: "{{ url('pembayaran-mutasi/create') }}",
                        data: {
                            nop_asal,
                            psmb_id
                        },
                        success: function(res) {
                            Swal.close()
                            $('#hasil').html(res)

                        },
                        error: function(e) {
                            Swal.close()
                            $('#hasil').html('Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.')
                        }
                    })
                }
            })

            $('#nop_asal,#nop_tujuan').trigger('keyup')
        })
    </script>
@endsection
