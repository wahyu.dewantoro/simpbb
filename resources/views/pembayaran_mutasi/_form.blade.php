<table class="table table-sm table-bordered">
    <thead class="bg-info">
        <tr>
            <th class="text-center">
                <input type="checkbox" id="checkAll" class="form-control form-control-sm">
            </th>
            <th class="text-center">Tahun</th>
            <th class="text-center">Pokok</th>
            <th class="text-center">Denda</th>
            <th class="text-center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
            <tr>
                <td>
                    <div class="form-cdheck">
                        <input type="checkbox" name="thn_pajak_sppt[]" value="{{ $item->thn_pajak_sppt }}"
                            @if (in_array($item->thn_pajak_sppt, $selected)) checked @endif class="form-control form-control-sm tahun"
                            id="exampleCheck1{{ $item->thn_pajak_sppt }}">
                    </div>
                </td>
                <td class="text-center">{{ $item->thn_pajak_sppt }}</td>
                <td class="text-right">{{ angka($item->pokok) }}</td>
                <td class="text-right">{{ angka($item->denda) }}</td>
                <td class="text-right">{{ angka($item->jumlah) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<script>
    $(document).ready(function() {
        jQuery.validator.addClassRules("tahun", {
            required: true,
            // minlength: 2
        });
        $("#checkAll").click(function() {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    })
</script>
