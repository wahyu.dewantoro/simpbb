@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Mutasi Pembayaran</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a href="{{ route('pembayaran-mutasi.create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-folder-plus"></i> Tambah
                        </a>
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="card-title">Data</h3>
                    <div class="card-tools">
                        <form action="{{ url()->current() }}">
                            <div class="input-group input-group-sm" style="width: 250px;">
                                <input type="text" name="cari" class="form-control float-right" placeholder="Search"
                                    value="{{ request()->get('cari') }}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered">
                        <thead class="bg bg-info">
                            <tr>
                                <th>No</th>
                                <th>Asal</th>
                                <th>Tujuan</th>
                                <th>Petugas</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $row)
                                <tr>
                                    <td align="center">{{ $index + $data->firstItem() }}</td>
                                    <td>{{ formatnop($row->nop_asal) }}</td>
                                    <td>{{ formatnop($row->nop_tujuan) }}</td>
                                    <td>
                                        {{ $row->created_at }}
                                    </td>
                                    <td class="text-center"> <a href="{{ route('pembayaran-mutasi.edit', $row->id) }}"><i
                                                class="fas fa-edit text-info"></i>
                                        </a>
                                        <a data-id="{{ $row->id }}" href="{{ url('pembayaran-mutasi', $row->id) }}"
                                            class='hapusRole'><i class="fas fa-trash-alt text-danger"></i>
                                        </a>
                                        <form method="POST" id="delete-form-{{ $row->id }}"
                                            action="{{ route('pembayaran-mutasi.destroy', [$row->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer p-0">
                    <div class="float-sm-right">
                        {{ $data->appends(['cari' => request()->get('cari')]) }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.hapusRole').on('click', function(e) {
                id = $(this).data('id')
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "data yang dihapus tidak dapat di kembalikan.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        document.getElementById('delete-form-' + id).submit()
                        // document.location.href = $(this).data('href');
                    }
                })

            });
        })
    </script>
@endsection
