@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Permissions</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        @can(' add_permissions')
                            <a href="{{ route('permissions.create') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-folder-plus"></i> Create
                            </a>
                        @endcan
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data</h3>
                    <div class="card-tools">
                        <form action="{{ url()->current() }}">
                            <div class="input-group input-group-sm" style="width: 250px;">
                                <input type="text" name="cari" class="form-control float-right" placeholder="Search"
                                    value="{{ request()->get('cari') }}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th width="5px">No</th>
                                <th>Permissions</th>
                                <th>Role</th>
                                @can('edit_permissions', 'delete_permissions')
                                    <th>Actions</th>
                                @endcan

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($permissions as $index => $row)
                                <tr>
                                    <td align="center">{{ $index + $permissions->firstItem() }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ implode(',', $row->roles->pluck('name')->toarray()) }}
                                    </td>
                                    @can('edit_permissions', 'delete_permissions')
                                        <td class="text-center">

                                            @can('edit_permissions')
                                                <a href="{{ route('permissions.edit', $row->id) }}"><i
                                                        class="fas fa-edit text-info"></i> </a>
                                            @endcan
                                            @if ($row->name != 'Super User')
                                                @can('delete_permissions')
                                                    <a href="{{ url('permissions', $row->id) }}" onclick="
                                                                    var result = confirm('Are you sure you want to delete this record?');
                                                                    if(result){
                                                                        event.preventDefault();
                                                                        document.getElementById('delete-form-{{ $row->id }}').submit();
                                                                    }"><i class="fas fa-trash-alt text-danger"></i> </a>
                                                    <form method="POST" id="delete-form-{{ $row->id }}"
                                                        action="{{ route('permissions.destroy', [$row->id]) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                @endcan
                                            @endcan
                                        @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
                <div class="card-footer p-0">
                    <div class="float-sm-right">
                        {{ $permissions->appends(['cari' => request()->get('cari')]) }}
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
