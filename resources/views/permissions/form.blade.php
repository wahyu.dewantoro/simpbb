@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $form->title }}</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <form action="{{ $form->action }}" method="post">
                        @csrf
                        @method($form->method)
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="role">Permission</label>
                                    <input type="text" name="permission" id="permission"
                                        value="{{ old('Permission') ?? ($form->permission->name ?? '') }}"
                                        class="form-control form-control-sm {{ $errors->has('permission') ? 'is-invalid' : '' }}">
                                    <span class="errorr invalid-feedback">{{ $errors->first('permission') }}</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="">Roles :</label>
                                <span class="errorr invalid-feedback">@if ($errors->has('roles.0')) Role harus pilih @endif</span>
                            </div>
                            @php
                                $ar = [];
                                if (old('roles')) {
                                    $ar = old('roles');
                                } else {
                                    if (isset($form->permission)) {
                                        $ar =$form->permission->roles->pluck('name')->toarray();
                                    }
                                }
                            @endphp
                            @foreach ($roles as $row)
                                <div class="col-md-3">
                                    <div class="checkbox">
                                        <label>
                                            <input @if (in_array($row->name, $ar)) checked @endif  type="checkbox" name="roles[]" value="{{ $row->name }}" id="">
                                            {{ $row->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="float-sm-right">
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
