<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo') }}/favicon.ico" />
    <title>SIMPBB P2</title>

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="SIPANJI : SIMPBB">
    <meta itemprop="description"
        content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta itemprop="image" content="{{ asset('sipanji.png') }}">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://esppt.id/simpbb">
    <meta property="og:type" content="website">
    <meta property="og:title" content="SIPANJI : SIMPBB">
    <meta property="og:description"
        content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta property="og:image" content="{{ asset('sipanji.png') }}">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="SIPANJI : SIMPBB">
    <meta name="twitter:description"
        content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta name="twitter:image" content="{{ asset('sipanji.png') }}">
    <link data-rh="true" rel="search" type="application/opensearchdescription+xml" title="SIPANJI" href="/osd.xml" />
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" {{-- href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> --}} <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('lte') }}/dist/css/adminlte.min.css">
    {{-- toast --}}
    <link href="{{ asset('lte') }}/plugins/toastr/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.14.0/themes/base/jquery-ui.min.css"
        integrity="sha512-F8mgNaoH6SSws+tuDTveIu+hx6JkVcuLqTQ/S/KJaHJjGc8eUxIrBawMnasq2FDlfo7FYsD8buQXVwD+0upbcA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .hero-image {
            background-image: url("background.png");
            background-color: #cccccc;
            height: auto;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }

        fieldset {
            border: solid 1px #000;
            padding: 10px;
            display: block;
            clear: both;
            margin: 5px 0px;
        }

        legend {
            width: inherit;
            font-size: 13px;
            font-weight: bold;
            /* Or auto */
            padding: 0 10px;
            /* To give a bit of padding on the left and right */
            border-bottom: none;
        }
    </style>
</head>

<body class="hold-transition register-page hero-image">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-sm-12 form-alert">
                <div class="register-boxx">
                    <div class="login-logo">
                        <a href="#"><b>{{ config('app.name', 'Laravel') }}</b></a>
                    </div>


                    <!-- /.login-logo -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title text-bold">FORM REGISTRASI</h3>
                        </div>
                        <div class="card-body">

                            @if (session('pesan'))
                                <?= session('pesan') ?>
                            @endif
                            <form method="POST" id="form-registrasi" action="{{ route('register') }}"
                                enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-1">
                                            <label for="nik" class="col-form-label">NIK <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="text" id="nik" name="nik"
                                                placeholder="Masukan nik"
                                                class="form-control form-control-sm angka  @error('nik') is-invalid @enderror "
                                                value="{{ old('nik') }}">
                                            @error('nik')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group m-1">
                                            <label for="nama" class="col-form-label">Nama Lengkap <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="text" id="nama" name="nama"
                                                placeholder="Masukan nama lengkap"
                                                class="form-control form-control-sm  @error('nama') is-invalid @enderror "
                                                value="{{ old('nama') }}">
                                            @error('nama')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>



                                        <div class="form-group m-1">
                                            <label for="telepon" class="col-form-label">No Whatsapp <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="text" id="telepon" name="telepon"
                                                placeholder="Masukan nomor whatsapp aktif"
                                                class="form-control form-control-sm angka @error('telepon') is-invalid @enderror "
                                                value="{{ old('telepon') }}">
                                            @error('telepon')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group m-1">
                                            <label for="email" class="col-form-label">Email <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="text" id="email" name="email"
                                                placeholder="Masukan email"
                                                class="form-control form-control-sm  @error('email') is-invalid @enderror "
                                                value="{{ old('email') }}">
                                            @error('email')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-1">
                                            <label for="alamat_user" class="col-form-label">Alamat <sup
                                                    class="text-danger">*</sup></label>
                                            <textarea id="alamat_user" name="alamat_user" placeholder="Masukan alamat" rows="4"
                                                class="form-control form-control-sm  @error('alamat_user') is-invalid @enderror ">{{ old('alamat_user') }}</textarea>
                                            @error('alamat_user')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group m-1">
                                                    <label for="kelurahan_user" class="col-form-label">Desa /
                                                        Kelurahan <sup class="text-danger">*</sup></label>
                                                    <input type="text" id="kelurahan_user" name="kelurahan_user"
                                                        placeholder="Masukan  desa/ kelurahan"
                                                        class="form-control form-control-sm  @error('kelurahan_user') is-invalid @enderror "
                                                        value="{{ old('kelurahan_user') }}">
                                                    @error('kelurahan_user')
                                                        <span class="invalid-feedback">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group m-1">
                                                    <label for="kecamatan_user" class="col-form-label">Kecamatan <sup
                                                            class="text-danger">*</sup></label>
                                                    <input type="text" id="kecamatan_user" name="kecamatan_user"
                                                        placeholder="Masukan  kecamatan"
                                                        class="form-control form-control-sm  @error('kecamatan_user') is-invalid @enderror "
                                                        value="{{ old('kecamatan_user') }}">
                                                    @error('kecamatan_user')
                                                        <span class="invalid-feedback">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group m-1">
                                                    <label for="dati2_user" class="col-form-label">Kabupaten/ Kota
                                                        <sup class="text-danger">*</sup></label>
                                                    <input type="text" id="dati2_user" name="dati2_user"
                                                        placeholder="Masukan kabupaten/kota"
                                                        class="form-control form-control-sm  @error('dati2_user') is-invalid @enderror "
                                                        value="{{ old('dati2_user') }}">
                                                    @error('dati2_user')
                                                        <span class="invalid-feedback">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group m-1">
                                                    <label for="propinsi_user" class="col-form-label">Propinsi <sup
                                                            class="text-danger">*</sup></label>
                                                    <input type="text" id="propinsi_user" name="propinsi_user"
                                                        placeholder="Masukan propinsi"
                                                        class="form-control form-control-sm  @error('propinsi_user') is-invalid @enderror "
                                                        value="{{ old('propinsi_user') }}">
                                                    @error('propinsi_user')
                                                        <span class="invalid-feedback">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-1">
                                            <label for="role" class="col-form-label">Tipe WP</label>
                                            <select name="role" id="role"
                                                class="form-control form-control-sm @error('role') is-invalid @enderror">
                                                <option value="">Piih</option>
                                                @php
                                                    $swp = old('role') ?? '';
                                                @endphp
                                                @foreach ($roles as $item)
                                                    <option @if ($swp == $item) selected @endif
                                                        value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="form-developer">
                                            <div class="form-group m-1">
                                                <label for="nama_pengembang" class="col-form-label">Nama PT /
                                                    CV</label>
                                                <input type="text" name="nama_pengembang" id="nama_pengembang"
                                                    class="form-control form-control-sm  @error('nama_pengembang') is-invalid @enderror "
                                                    value="{{ old('nama_pengembang') ?? '' }}"
                                                    placeholder="Masukan nama badan hukum developer">
                                                @error('nama_pengembang')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div id="form-rayon">
                                            <div class="form-group m-1">
                                                <label for="kd_kecamatan" class="col-form-label">Kecamatan</label>
                                                <select name="kd_kecamatan" id="kd_kecamatan"
                                                    class="form-control form-control-sm @error('kd_kecamatan') is-invalid @enderror ">
                                                    <option value="">Pilih</option>
                                                    @foreach ($kecamatan as $i => $item)
                                                        <option value="{{ $i }}">{{ $item }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('kd_kecamatan')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group m-1">
                                                <label for="kd_kelurahan" class="col-form-label">Kelurahan</label>
                                                <select name="kd_kelurahan" id="kd_kelurahan"
                                                    class="form-control form-control-sm @error('kd_kelurahan') is-invalid @enderror ">
                                                    <option value="">Pilih</option>
                                                </select>
                                                @error('kd_kelurahan')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div id="form-kuasa">
                                            <div class="form-group m-1">
                                                <label for="file_rekom" class="col-form-label">Dokumen Pendukung <sup
                                                        class="text-danger">*</sup></label>
                                                <input type="file" name="file_rekom"
                                                    class="form-control form-control-sm @error('file_rekom') is-invalid @enderror"
                                                    required accept="image/*">
                                                @error('file_rekom')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="tgl_surat" class="col-form-label">Tanggal
                                                            Dokumen</label>
                                                        <input type="text"
                                                            class="form-control form-control-sm @error('tgl_surat') is-invalid  @enderror"
                                                            name="tgl_surat" id="tgl_surat"
                                                            value="{{ old('tgl_surat') ?? '' }}">
                                                        @error('tgl_surat')
                                                            <span class="invalid-feedback">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="no_surat" class="col-form-label">Nomor
                                                            Dokumen</label>
                                                        <input type="text"
                                                            class="form-control form-control-sm @error('no_surat') is-invalid  @enderror"
                                                            name="no_surat" id="no_surat"
                                                            value="{{ old('no_surat') ?? '' }}">
                                                        @error('no_surat')
                                                            <span class="invalid-feedback">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group m-1">
                                            <label for="file_ktp" class="col-form-label">Scan KTP Asli <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="file" name="file_ktp"
                                                class="form-control form-control-sm @error('file_ktp') is-invalid @enderror"
                                                required accept="image/*">
                                            @error('file_ktp')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group m-1">
                                            <label for="file_selfi" class="col-form-label">Selfie <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="file" name="file_selfi"
                                                class="form-control form-control-sm @error('file_selfi') is-invalid @enderror"
                                                required accept="image/*">
                                            @error('file_selfi')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group m-1">
                                            <label for="username" class="col-form-label">Username <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="text" id="username" name="username"
                                                class="form-control form-control-sm  @error('username') is-invalid @enderror "
                                                value="{{ old('username') }}">
                                            @error('username')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group m-1">
                                            <label for="password" class="col-form-label">Password <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="password" id="password" name="password"
                                                class="form-control form-control-sm  @error('password') is-invalid @enderror "
                                                value="{{ old('password') }}">
                                            @error('password')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group mt-1">
                                            <label for="password" class="col-form-label">Confirm Password <sup
                                                    class="text-danger">*</sup></label>
                                            <input type="password" name="password_confirmation"
                                                id="password_confirmation"
                                                class="form-control form-control-sm  @error('password_confirmation') is-invalid @enderror "
                                                value="{{ old('password_confirmation') ?? '' }}"
                                                placeholder="Masukan kembali password">
                                            @error('password_confirmation')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-lg-8">
                                        <a href="#" data-toggle="modal" data-target="#exampleModal">Syarat
                                            dan ketentuan</a>

                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input"
                                                id="customCheckDisabled1">
                                            <label class="custom-control-label" for="customCheckDisabled1">saya
                                                menyetujui syarat dan kentuan</label>
                                        </div>

                                    </div>
                                    <div class="col-lg-4">

                                        <div class="float-right">
                                            <button type="submit" class="btn btn-primary" id="submitButton">
                                                <i class="far fa-check-square"></i>
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span id="msg-success" data-msg="{!! session('success') !!}"></span>
    <span id="msg-error" data-msg="{!! session('error') !!}"></span>
    <span id="msg-warning" data-msg="{!! session('warning') !!}"></span>
    <!-- /.login-box -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Syarat & Ketentuan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="syarat_wp">
                        <b>WAJIB PAJAK ONLINE</b>

                        <b>KETENTUAN</b>
                        <ol>
                            <li>Wajib pajak adalah orang atau badan yang memiliki tanah dan atau bangunan di wilayah
                                Kabupaten Malang.</li>
                            <li>Pemohon adalah orang yang bertanggung jawab terhadap informasi objek pajak.</li>
                            <li>Kesalahan pada form registrasi merupakan sepenuhnya tanggung jawab pemohon.</li>
                            <li>Segala bentuk penyalahgunaan informasi dapat diproses secara hukum berdasarkan
                                undang-undang
                                yang berlaku.</li>
                        </ol>

                        <b>SYARAT REGISTRASI</b>
                        <ol>
                            <li>Nama dalam sertifikat harus sesuai dengan E-KTP.</li>
                            <li>Lampiran yang diupload adalah E-KTP, sertifikat, foto selfie dengan memegang KTP, dan
                                SPPT
                                tahun terakhir.</li>
                            <li>Nama di sertifikat yang tidak sama dengan E-KTP harus diajukan pembetulan terlebih
                                dahulu
                                dengan membawa KTP asli dan sertifikat ke kantor Badan Pendapatan atau kantor UPT
                                terdekat.
                            </li>
                        </ol>
                    </div>
                    <div id="syarat_developer">
                        <b>SYARAT DAN KETENTUAN DEVELOPER</b>

                        <b>KETENTUAN</b>
                        <ol>
                            <li>Pemohon adalah orang yang ditunjuk oleh PT. (Developer) secara resmi yang selanjutnya
                                disebut sebagai PENGELOLA.</li>
                            <li>Nomor Objek Pajak yang dimohonkan adalah benar masuk dalam perumahan.</li>
                            <li>Bertanggung jawab terhadap segala informasi.</li>
                            <li>Segala bentuk penyalahgunaan informasi dapat diproses secara hukum berdasarkan
                                undang-undang
                                yang berlaku.</li>
                            <li>Bertanggung jawab atas ketertiban pembayaran.</li>
                            <li>ESPPT yang tidak sesuai dengan bukti kepemilikan wajib dilaporkan ke kantor Badan
                                Pendapatan
                                Daerah Kabupaten Malang untuk diajukan pembetulan.</li>
                            <li>Jika orang yang ditunjuk oleh PT. (Developer) sudah tidak lagi menjadi pengurus, wajib
                                dilaporkan ke Badan Pendapatan Daerah Kabupaten Malang dan mengajukan nama sebagai
                                pengganti.</li>
                            <li>Nomor Objek Pajak yang sudah tidak dikelola oleh PT. (Developer) wajib dilaporkan ke
                                kantor
                                Badan Pendapatan Daerah untuk dihapus dari daftar.</li>
                            <li>User dan password harus dirahasiakan.</li>
                        </ol>

                        <b>SYARAT REGISTRASI</b>
                        <ol>
                            <li>Surat penunjukan dari PT. (Developer) berkop dan stempel yang ditandatangani oleh
                                direktur
                                utama.</li>
                            <li>Fotokopi akta PT. dan NIB.</li>
                            <li>Fotokopi KTP pengelola.</li>
                            <li>Melampirkan daftar NOP yang dikelola.</li>
                        </ol>
                    </div>

                    <div id="syarat_rayon">
                        <b>SYARAT DAN KETENTUAN RAYON</b>

                        <b>KETENTUAN</b>
                        <ol>
                            <li>Pemohon adalah warga desa/kelurahan yang ditunjuk oleh Kepala Desa/Lurah setempat yang
                                kemudian disebut sebagai PENGELOLA.</li>
                            <li>Membuat surat permohonan sebagai rayon dengan melampirkan NOP di desa yang akan
                                dikelola.
                            </li>
                            <li>Dalam satu desa dapat mengajukan lebih dari satu pengelola.</li>
                            <li>NOP yang telah dikelola oleh rayon satu tidak dapat dikelola oleh rayon berikutnya.</li>
                            <li>Segala bentuk penyalahgunaan informasi dapat diproses secara hukum berdasarkan
                                undang-undang
                                yang berlaku.</li>
                            <li>Bertanggung jawab atas ketertiban pembayaran.</li>
                            <li>ESPPT yang tidak sesuai dengan bukti kepemilikan wajib dilaporkan ke kantor Badan
                                Pendapatan
                                Daerah Kabupaten Malang untuk diajukan pembetulan.</li>
                            <li>User dan password harus dirahasiakan.</li>
                            <li>Pengelola yang sudah tidak mengelola NOP di desa dapat mengajukan penggantian nama.</li>
                        </ol>

                        <b>SYARAT REGISTRASI</b>
                        <ol>
                            <li>Surat penunjukan dari Kepala Desa/Lurah berkop dan stempel yang ditandatangani oleh
                                Kepala
                                Desa atau Lurah.</li>
                            <li>Fotokopi KTP pengelola.</li>
                            <li>Daftar NOP yang akan dikelola.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- jQuery -->
    <script src="{{ asset('lte') }}/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap 4 -->
    <script src="{{ asset('lte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('lte') }}/dist/js/adminlte.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/toastr/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.14.0/jquery-ui.min.js"
        integrity="sha512-MlEyuwT6VkRXExjj8CdBKNgd+e2H+aYZOCUaCrt9KRk6MlZDOs91V1yK22rwm8aCIsb5Ec1euL8f0g58RKT/Pg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $(document).ready(function() {
            $('#submitButton').prop('disabled', true);

            function syarat() {
                var role = $('#role').val()

                if (role == 'Wajib Pajak') {
                    $('#syarat_wp').show()
                } else {
                    $('#syarat_wp').hide()
                }
                if (role == 'DEVELOPER/PENGEMBANG') {
                    $('#syarat_developer').show()
                } else {
                    $('#syarat_developer').hide()
                }
                if (role == 'Rayon') {
                    $('#syarat_rayon').show()
                } else {
                    $('#syarat_rayon').hide()
                }
            }

            $('#customCheckDisabled1').on('change', function() {
                // Periksa apakah ada checkbox yang dicentang
                if ($('#customCheckDisabled1:checked').length > 0) {
                    $('#submitButton').prop('disabled', false); // Aktifkan tombol
                } else {
                    $('#submitButton').prop('disabled', true); // Nonaktifkan tombol
                }
            });


            $(window).keydown(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });


            $('#tgl_surat').daterangepicker({
                "singleDatePicker": true,
                "minYear": 2024,
                "autoApply": true,

            });


            var ac_kecamatan, ac_kelurahan;
            ac_kecamatan = <?= $ac_kecamatan ?>;

            $("#kecamatan_user").autocomplete({
                source: ac_kecamatan
            });

            ac_kelurahan = <?= $ac_kelurahan ?>;
            $("#kelurahan_user").autocomplete({
                source: ac_kelurahan
            });



            toastr.options = {
                "closeButton": true, // Tambahkan tombol close untuk kontrol lebih
                "debug": false, // Nonaktifkan debug untuk produksi
                "newestOnTop": true, // Tampilkan notifikasi terbaru di atas
                "progressBar": true, // Progress bar untuk menunjukkan durasi
                "positionClass": "toast-top-right", // Posisi di sudut kanan atas
                "preventDuplicates": true, // Hindari notifikasi duplikat
                "onclick": null, // Aksi tambahan dapat disesuaikan di sini
                "showDuration": "300", // Durasi animasi tampil lebih cepat
                "hideDuration": "500", // Durasi animasi hilang lebih halus
                "timeOut": "10000", // Tampilkan notifikasi selama sekian detik
                "extendedTimeOut": "2000", // Tambahan waktu saat pengguna hover
                "showEasing": "easeOutBounce", // Animasi muncul yang lebih menarik
                "hideEasing": "easeInQuad", // Animasi hilang yang lebih elegan
                "showMethod": "slideDown", // Gunakan slide untuk efek muncul
                "hideMethod": "fadeOut" // Fade out untuk efek hilang
            };

            var success = $('#msg-success').data('msg');
            var error = $('#msg-error').data('msg');
            var warning = $('#msg-warning').data('msg');

            // Display a success toast, with a title
            if (success != '') {
                toastr.success(success);
            }

            // Display an error toast, with a title
            if (error != '') {
                toastr.error(error);
            }

            // Display a success toast, with a title
            if (warning != '') {
                toastr.warning(warning);
            }

            $('.angka').keyup(function(event) {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault(); //stop character from entering input
                }
                var input = $(this).val().trim()
                $(this).val(input)
            });


            jQuery.extend(jQuery.validator.messages, {
                required: "Wajib di isi.",
                email: "Silakan isi alamat email yang valid",
                number: "Silakan masukkan nomor yang valid.",
                equalTo: "Silakan masukkan nilai yang sama lagi.",
                maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                minlength: jQuery.validator.format("Please enter at least {0} characters."),
                rangelength: jQuery.validator.format(
                    "Please enter a value between {0} and {1} characters long."),
                range: jQuery.validator.format("Please enter a value between {0} and {1}."),
                max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
                min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
                extension: "Harus dengan format file .pdf, .png, .jpg",
            });

            $('#form-registrasi').validate({
                rules: {
                    customCheckDisabled1: {
                        required: true
                    },
                    nik: {
                        required: true,
                        maxlength: 16,
                        minlength: 16,
                        number: true
                    },
                    nama: {
                        required: true,
                    },
                    alamat_user: {
                        required: true,
                    },
                    kelurahan_user: {
                        required: true,
                    },
                    kecamatan_user: {
                        required: true,
                    },
                    dati2_user: {
                        required: true,
                    },
                    propinsi_user: {
                        required: true,
                    },
                    telepon: {
                        required: true,
                        number: true
                    },
                    role: {
                        required: true,
                    },
                    email: {
                        email: true,
                        required: true
                    },
                    username: {
                        required: true,
                        minlength: 5
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password",
                        minlength: 5
                    },
                    file_ktp: {
                        required: true,
                        accept: "image/*,application/pdf"
                    },
                    file_selfi: {
                        required: true,
                        accept: "image/*,application/pdf"
                    },
                    file_rekom: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon" || $("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        },
                        accept: "image/*,application/pdf"
                    },
                    no_surat: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon" || $("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        }
                    },
                    tgl_surat: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon" || $("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        }
                    },
                    nama_pengembang: {
                        required: function(element) {
                            return ($("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        }
                    }

                    // nama_pengembang

                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

            $(document).on('change', '#role', function() {
                syarat()

                var role = $('#role').val()
                // form-kuasa
                if (role == 'Wajib Pajak' || role == '') {
                    $('#form-kuasa').addClass('d-none')
                } else {
                    $('#form-kuasa').removeClass('d-none')
                }

                // form-developer
                if (role == 'DEVELOPER/PENGEMBANG') {
                    $('#form-developer').removeClass('d-none')

                } else {
                    $('#form-developer').addClass('d-none')
                }


                if (role == 'Rayon') {
                    $('#form-rayon').removeClass('d-none')

                } else {
                    $('#form-rayon').addClass('d-none')
                }

                $('#kd_kecamatan').val('')
                $('#kd_kecamatan').trigger('change')
            })

            $('#role').val('{{ old('role') ?? '' }}').trigger('change')

            $(document).on('change', '#kd_kecamatan', function() {
                var kd_kecamatan = $(this).val()
                $.ajax({
                    url: "{{ url('api/desa') }}",
                    data: {
                        kd_kecamatan
                    },
                    success: function(res) {
                        var option = "<option value=''>Pilih</option>";
                        $.each(res, function(k, v) {
                            /// do stuff
                            option += "<option value='" + k + "'>" + k + " - " + v +
                                "</option>";
                        });

                        $('#kd_kelurahan').html(option)
                    }
                })

            })


        });
    </script>
</body>

</html>
