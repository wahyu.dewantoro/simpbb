<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah">
        <strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong><br>
        <strong>{{ $sk->nomor }}</strong>
        <br>
        <br>
        <strong>TENTANG<br>
            PEMBATALAN SURAT PEMBERITAHUAN PAJAK TERUTANG <br>
            PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
            (SPPT PBB P2)

        </strong>

    </p>
    <table width="100%">
        <tr style="vertical-align: top">
            <td width="15%">Menimbang</td>
            <td width="1px">:</td>
            <td style="text-align: justify">Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu
                ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Pembatalan Surat Pemberitahuan Pajak
                Terutang Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (SPPT PBB P2).</td>
        </tr>
        <tr style="vertical-align: top">
            <td>Mengingat</td>
            <td>:</td>
            <td style="text-align: justify">
                <ol>
                    <li> Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan
                        Pemerintahan Daerah;</li>
                    <li> Peraturan Pemerintah Nomor 35 Tahun 2023 tentang Ketentuan Umum Pajak Daerah dan Retribusi
                        Daerah;</li>
                    <li> Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010
                        tentang Pajak Daerah;</li>
                    <li> Peraturan Bupati Malang Nomor 7 Tahun 2019 tentang Perubahan Kedua Atas Peraturan Bupati Malang
                        Nomor 49 Tahun 2013 Tentang Tata Cara Pembatalan Ketetapan Pajak Bumi dan Bangunan Perdesaan dan
                        Perkotaan Yang Tidak Benar.</li>
                </ol>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td>Memperhatikan</td>
            <td>:</td>
            <td>Surat permohonan Pembatalan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh Pemohon
                <br>
                a. Wajib Pajak/Kuasa Wajib Pajak : {{ $sk->TblSpop->nm_wp }} <br>
                b. Nomor Pelayanan : {{ $sk->TblSpop->layananObjek->nomor_layanan }}
            </td>
        </tr>
    </table>
    <p class="text-tengah"><strong>MEMUTUSKAN </strong></p>
    <table width="100%">
        <tr style="vertical-align: top">
            <td style="text-align: justify">
                Menerima permohonan Pembatalan SPPT PBB P2 yang diajukan oleh Pemohon dengan NOP PBB sebagai berikut
                :<br>
                <table width="100%" border="0">

                    <tr style="vertical-align: top">
                        <td width="25%">NOP</td>
                        <td width="1px">:</td>
                        <td>{{ formatnop($sk->TblSpop->nop_proses) }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Keterangan Pembatalan</td>
                        <td>:</td>
                        <td>
                            <?= $keterangan ?>
                        </td>
                    </tr>

                </table>

            </td>
        </tr>
    </table>

    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%" rowspan="3"></td>
                <td rowspan="3"></td>
                <td>Ditetapkan di : {{ $sk->kota }}</td>
            </tr>
            <tr>
                <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
            </tr>
            <tr>
                <td width="40%" align="center">
                    <p>
                        <span class=""><strong>{{ $sk->jabatan }}<br>KABUPATEN MALANG</strong> </span><br>
                        {!! QrCode::size(100)->generate($url) !!}
                        <br>
                        <span><b><u>{{ $sk->pegawai }}</u></b>
                            <br>{{ $sk->pangkat }}
                            <br>NIP : {{ $sk->nip }}</span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
