<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    <div class="page">
        @include('layouts.kop_pdf')
        <h3 class="text-tengah"><strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong></h3>
        <p class="text-tengah">
            <strong>{{ $sk->nomor}}</strong>
            <br>
            <br>
            <strong>TENTANG<br>
                PEMBETULAN SURAT PEMBERITAHUAN PAJAK TERUTANG <br>
                PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
                (SPPT PBB P2)
            </strong>
        </p>
        <table width="100%">
            <tr style="vertical-align: top">
                <td width="15%">Menimbang</td>
                <td width="1px">:</td>
                <td style="text-align: justify">Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Pembetulan Surat Pemberitahuan Pajak Terutang Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (SPPT PBB P2) terhadap kesalahan tulis, kesalahan hitung dan/atas kekeliruan penerapan ketentuan tertentu dalam pelaksanaan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.</td>
            </tr>
            <tr style="vertical-align: top">
                <td>Mengingat</td>
                <td>:</td>
                <td style="text-align: justify">
                    <ol>
                        <li>Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan Pemerintahan Daerah;</li>
                        <li>Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010 tentang Pajak Daerah;</li>
                        <li>Peraturan Bupati Malang Nomor 57 Tahun 2017 tentang Perubahan atas Peraturan Bupati Malang Nomor 53 Tahun 2013 tentang Tata Cara Pembetulan Kesalahan Tulis, Kesalahan Hitung dan/atau Kekeliruan Penerapan Ketentuan Tertentu Dalam Pelaksanaan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.</li>
                    </ol>

                </td>
            </tr>
            <tr style="vertical-align: top">
                <td>Memperhatikan</td>
                <td>:</td>
                <td>Surat permohonan Pembetulan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh Pemohon <br>
                    a. Wajib Pajak/Kuasa Wajib Pajak : {{ $sk->TblSpop->nm_wp }} <br>
                    b. Nomor Pelayanan : {{ $sk->TblSpop->layananObjek->nomor_layanan }}<br>
                    c. NOP : {{ formatnop($sk->TblSpop->nop_proses)}}
                </td>
            </tr>
        </table>
        <p class="text-tengah"><strong>MEMUTUSKAN </strong></p>
        <table width="100%">
            <tr style="vertical-align: top">
                <td style="text-align: justify">
                    Menerima permohonan Pembetulan SPPT PBB P2 yang diajukan oleh Pemohon sebagai berikut :<br>
                    <b>TERLAMPIR</b>
                </td>
            </tr>
        </table>

        <table width="100%" border="0">
            <tbody>
                <tr>
                    <td width="30%" rowspan="3"></td>
                    <td rowspan="3"></td>
                    <td>Ditetapkan di : {{ $sk->kota }}</td>
                </tr>
                <tr>
                    <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
                </tr>
                <tr>
                    <td width="35%" align="center">
                        <p>
                            <span class=""><strong>{{ $sk->jabatan}}</strong> </span><br>
                            {!! QrCode::size(100)->generate($url); !!}
                            <br>
                            <span><b>{{ $sk->pegawai }}</b><br>NIP : {{ $sk->nip }}</span>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- <div class="page"> --}}
    <h4 class="text-tengah">LAMPIRAN KEPUTUSAN KEPALA BADAN PENDATAN KABUPATEN MALANG</h4>
    <hr style="border-color:black">
    <p  class="text-tengah"><strong>Nomor : {{ $sk->nomor }}</strong></p>
    <table width="100%" border="0" style="font-size: 10px;">
        <tr style="vertical-align: top">
            <td colspan="3"><strong>Objek :</strong></td>
        </tr>
        <tr style="vertical-align: top">
            <td width="15%">NOP</td>
            <td width="1%">:</td>
            <td>{{ formatnop($sk->TblSpop->nop_proses)}}</td>
        </tr>

        <tr style="vertical-align: top">
            <td width="15%">Letak Objek</td>
            <td width="1%">:</td>
            <td>{{ $sk->hasil->alamat_op }}</td>
        </tr>
        <tr style="vertical-align: top">
            <td>Nama WP</td>
            <td width="1px">:</td>
            <td>{{ $sk->hasil->nm_wp }}</td>
        </tr>
        <tr style="vertical-align: top">
            <td>Alamat WP</td>
            <td>:</td>
            <td>
                <?= $sk->hasil->alamat_wp?>
            </td>
        </tr>
    </table>
    <p style="font-weight: bold;font-size: 10px;">Mutasi gabung dari:</p>
    <table width="100%" style="border:1px solid black; border-collapse: collapse; font-size: 10px;">
        <thead>
            <tr>
                <th width="4%" style="border:1px solid black; border-collapse: collapse" rowspan="2">NO</th>
                <th style="border:1px solid black; border-collapse: collapse" colspan="2">OBJEK</th>
                <th style="border:1px solid black; border-collapse: collapse" colspan="2">SUBJEK</th>
            </tr>
            <tr>
                <th width="24%" style="border:1px solid black; border-collapse: collapse">NOP</th>
                <th width="24%" style="border:1px solid black; border-collapse: collapse">ALAMAT</th>
                <th width="24%" style="border:1px solid black; border-collapse: collapse">WAJIB PAJAK</th>
                <th width="24%" style="border:1px solid black; border-collapse: collapse">ALAMAT</th>
            </tr>
        </thead>
        <tbody>
            @php
            $no=1;
            @endphp
            @foreach ($last as $idx=> $last)
            <tr>
                <td style="border:1px solid black; border-collapse: collapse; text-align: center">{{ $no }}</td>
                <td style="border:1px solid black; border-collapse: collapse">{{ $last->kd_propinsi.'.'. $last->kd_dati2.'.'. $last->kd_kecamatan.'.'. $last->kd_kelurahan.'.'.$last->kd_blok.'-'.$last->no_urut.'.'.$last->kd_jns_op }}</td>
                <td style="border:1px solid black; border-collapse: collapse">{{ $last->alamat_op }}</td>
                <td style="border:1px solid black; border-collapse: collapse">{{ $last->nm_wp??'' }}</td>
                <td style="border:1px solid black; border-collapse: collapse">{{ $last->alamat_wp }}</td>
            </tr>
            @php
            $no++;
            @endphp
            @endforeach
        </tbody>
    </table>


    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%" rowspan="3"></td>
                <td rowspan="3"></td>
                <td>Ditetapkan di : {{ $sk->kota }}</td>
            </tr>
            <tr>
                <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
            </tr>
            <tr>
                <td width="40%" align="center">
                    <p>
                        <span class=""><strong>{{ $sk->jabatan}}</strong> </span><br>
                        {!! QrCode::size(100)->generate($url); !!}
                        <br>
                        <span><b>{{ $sk->pegawai }}</b><br>NIP : {{ $sk->nip }}</span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>
