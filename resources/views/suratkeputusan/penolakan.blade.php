<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah">
        <strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong><br>
        <strong>{{ $sk->nomor }}</strong>
        <br>
        <br>
        <strong>TENTANG<br>
            KEBERATAN NILAI JUAL OBJEK PAJAK BUMI DAN<br> BANGUNAN PERDESAAN DAN PERKOTAAN 

        </strong>

    </p>
    <table width="100%">
        <tr style="vertical-align: top">
            <td width="15%">Menimbang</td>
            <td width="1px">:</td>
            <td style="text-align: justify">Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Keberatan Nilai Jual Objek Pajak Bumi dan/atau Bangunan dalam pelaksanaan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan..</td>
        </tr>
        <tr style="vertical-align: top">
            <td>Mengingat</td>
            <td>:</td>
            <td style="text-align: justify">
                <ol>
                    <li> Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah                       Pusat dan Pemerintahan Daerah;</li>
                    <li> Peraturan Pemerintah Nomor 35 Tahun 2023 tentang Ketentuan Umum Pajak Daerah dan Retribusi Daerah;</li>
                    <li> Peraturan Menteri Keuangan Nomor 208/PMK.07/2018 tentang Pedoman Penilaian Pajak Bumi dan Bangunan Perdesaan dan Perkotaan;</li>
                    <li> Peraturan Daerah Nomor 7 Tahun 2023 tentang Pajak Daerah dan Retribusi Daerah.</li>
                </ol>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td>Memperhatikan</td>
            <td>:</td>
            <td>Surat permohonan Pembatalan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh Pemohon
                <br>
                a. Wajib Pajak/Kuasa Wajib Pajak : {{ $sppt->nm_wp_sppt }} <br>
                b. Nomor Pelayanan : {{ $sk->layananObjek->nomor_layanan }}
            </td>
        </tr>
    </table>
    <p class="text-tengah"><strong>MEMUTUSKAN </strong></p>
    <table width="100%">
        <tr style="vertical-align: top">
            <td style="text-align: justify">
                Menolak permohonan Keberatan NJOP PBB P2 yang diajukan oleh Pemohon sebagai berikut :
                :<br>
                <table width="100%" border="0">

                    <tr style="vertical-align: top">
                        <td width="25%">NOP</td>
                        <td width="1px">:</td>
                        <td>{{
                                $sppt->kd_propinsi.'.'.
                                $sppt->kd_dati2.'.'.
                                $sppt->kd_kecamatan.'.'.
                                $sppt->kd_kelurahan.'.'.
                                $sppt->kd_blok.'.'.
                                $sppt->no_urut.'.'.
                                $sppt->kd_jns_op
                                
                            
                            }}</td>
                    </tr>
                    
                    <tr style="vertical-align: top">
                        <td width="25%">Letak Objek</td>
                        <td width="1px">:</td>
                        <td>{{ $alamat_op  
                            }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td width="25%">Wajib Pajak</td>
                        <td width="1px">:</td>
                        <td>{{ $sppt->nm_wp_sppt  
                            }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td width="25%">Alamat WP</td>
                        <td width="1px">:</td>
                        <td><?= $alamat_wp ?></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table class="table table-bordered" style="font-size: 14px">
                                <thead>
                                    <tr>
                                        <th>Objek</th>
                                        <th>Luas</th>
                                        <th>NJOP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Bumi</td>
                                        <td style="text-align: center">{{ angka($sppt->luas_bumi_sppt) }}</td>
                                        <td style="text-align: right">{{ angka($sppt->njop_bumi_sppt/ $sppt->luas_bumi_sppt) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Bangunan</td>
                                        <td style="text-align: center">{{ angka($sppt->luas_bng_sppt) }}</td>
                                        <td style="text-align: right">{{ angka($sppt->luas_bng_sppt>0 ? $sppt->njop_bng_sppt/ $sppt->luas_bng_sppt:0) }}</td>
                                    </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Keterangan Pembatalan</td>
                        <td>:</td>
                        <td>
                            Lokasi objek telah sesuai peta Zona Nilai Tanah PBB P2 dan NJOP per meter telah mencerminkan nilai rata-rata transaksi
                        </td>
                    </tr>

                </table>

            </td>
        </tr>
    </table>

    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%" rowspan="3"></td>
                <td rowspan="3"></td>
                <td>Ditetapkan di : {{ $sk->kota }}</td>
            </tr>
            <tr>
                <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
            </tr>
            <tr>
                <td width="40%" align="center">
                    <p>
                        <span class=""><strong>{{ $sk->jabatan }}<br>KABUPATEN MALANG</strong> </span><br>
                        {!! QrCode::size(100)->generate($url) !!}
                        <br>
                        <span><b><u>{{ $sk->pegawai }}</u></b>
                            <br>{{ $sk->pangkat }}
                            <br>NIP : {{ $sk->nip }}</span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
