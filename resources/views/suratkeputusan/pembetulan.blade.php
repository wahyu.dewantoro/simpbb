<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah" style="font-size: 14px">
        <strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong><br>
        <strong>{{ $sk->nomor }}</strong>
        <br>
        <br>
        <strong style="font-size: 14px">TENTANG<br>
            PEMBETULAN SURAT PEMBERITAHUAN PAJAK TERUTANG <br>
            PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
            (SPPT PBB P2)
        </strong>

    </p>
    <table width="100%" style="font-size: 14px">
        <tr style="vertical-align: top">
            <td width="15%">Menimbang</td>
            <td width="1px">:</td>
            <td style="text-align: justify">Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu
                ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Pembetulan Surat Pemberitahuan Pajak
                Terutang Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (SPPT PBB P2) terhadap kesalahan tulis,
                kesalahan hitung dan/atas kekeliruan penerapan ketentuan tertentu dalam pelaksanaan Pajak Bumi dan
                Bangunan Perdesaan dan Perkotaan.</td>
        </tr>
        <tr style="vertical-align: top">
            <td>Mengingat</td>
            <td>:</td>
            <td style="text-align: justify">
                <ol>
                    <li>Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan
                        Pemerintahan Daerah; </li>
                    <li>Peraturan Pemerintah Nomor 35 Tahun 2023 tentang Ketentuan Umum Pajak Daerah dan Retribusi
                        Daerah;</li>
                    {{-- <li>Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010 tentang Pajak Daerah; </li> --}}
                    <li>Peraturan Daerah Nomor 7 Tahun 2023 tentang Pajak Daerah dan Retribusi Daerah;</li>
                    <li>Peraturan Bupati Malang Nomor 57 Tahun 2017 tentang Perubahan atas Peraturan Bupati Malang Nomor
                        53 Tahun 2013 tentang Tata Cara Pembetulan Kesalahan Tulis, Kesalahan Hitung dan/atau Kekeliruan
                        Penerapan Ketentuan Tertentu Dalam Pelaksanaan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.
                    </li>

                </ol>

            </td>
        </tr>
        <tr style="vertical-align: top">
            <td>Memperhatikan</td>
            <td>:</td>
            <td>Surat permohonan Pembetulan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh Pemohon
                <br>
                a. Wajib Pajak/Kuasa Wajib Pajak : {{ $sk->TblSpop->nm_wp }} <br>
                b. Nomor Pelayanan : {{ $sk->TblSpop->layananObjek->nomor_layanan }}
            </td>
        </tr>
    </table>
    <p class="text-tengah" style="font-size: 14px"><strong>MEMUTUSKAN </strong></p>
    <table width="100%" style="font-size: 14px">
        <tr style="vertical-align: top">
            <td style="text-align: justify">
                Menerima permohonan Pembetulan SPPT PBB P2 yang diajukan oleh Pemohon sebagai berikut :<br>
                <table width="100%" border="0" style="font-size: 14px">
                    <tr style="vertical-align: top">
                        <td colspan="3"><strong>Awal</strong></td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>NOP</td>
                        <td>:</td>
                        <td>{{ $last->kd_propinsi . '.' . $last->kd_dati2 . '.' . $last->kd_kecamatan . '.' . $last->kd_kelurahan . '.' . $last->kd_blok . '-' . $last->no_urut . '.' . $last->kd_jns_op }}
                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Letak Objek</td>
                        <td>:</td>
                        <td>{{ $last->alamat_op ?? '' }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td width="20%">Nama WP</td>
                        <td width="1px">:</td>
                        <td>{{ $last->nm_wp ?? '' }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Alamat WP</td>
                        <td>:</td>
                        <td>
                            <?= $last->alamat_wp ?? '' ?>
                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td colspan="3"><strong>Menjadi</strong></td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Letak Objek</td>
                        <td>:</td>
                        <td>{{ optional($sk->hasil)->alamat_op }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td width="20%">Nama WP</td>
                        <td width="1px">:</td>
                        <td>{{ $sk->hasil->nm_wp }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Alamat WP</td>
                        <td>:</td>
                        <td>
                            <?= $sk->hasil->alamat_wp ?>
                        </td>
                    </tr>
                </table>
                <b style="font-size: 14px">NOP : {{ formatnop($sk->TblSpop->nop_proses) }}</b>
                <table class="table table-bordered" style="font-size: 14px">
                    <thead>
                        <tr>
                            <th rowspan="2">Objek</th>
                            <th colspan="2">Luas</th>
                            <th colspan="2">NJOP</th>
                        </tr>
                        <tr>
                            <th>Awal</th>
                            <th>Akhir</th>
                            <th>Awal</th>
                            <th>Akhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>Bumi</b></td>
                            <td style="text-align: center">{{ $last->luas_bumi ?? '' }}</td>
                            <td style="text-align: center">{{ $sk->hasil->luas_bumi }}</td>
                            <td style="text-align: right">
                                {{ $last->njop_bumi > 0 ? angka($last->njop_bumi / $last->luas_bumi, 0) : '-' }}</td>
                            <td style="text-align: right">
                                {{ $sk->hasil->luas_bumi > 0 ? angka($sk->hasil->njop_bumi / $sk->hasil->luas_bumi) : 0 }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Bangunan</b></td>
                            <td style="text-align: center">{{ $last->luas_bng ?? '' }}</td>
                            <td style="text-align: center">{{ $sk->hasil->luas_bng }}</td>
                            <td style="text-align: right">
                                {{ $last->njop_bng > 0 ? angka($last->njop_bng / $last->luas_bng) : '-' }}</td>
                            <td style="text-align: right">
                                {{ $sk->hasil->luas_bng > 0 ? angka($sk->hasil->njop_bng / $sk->hasil->luas_bng) : '-' }}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" style="font-size: 14px">
        <tbody>
            <tr>
                <td width="30%" rowspan="3"></td>
                <td rowspan="3"></td>
                <td>Ditetapkan di : {{ $sk->kota }}</td>
            </tr>
            <tr>
                <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
            </tr>
            <tr>
                <td width="40%" align="center">
                    <p>
                        <span class=""><strong>{{ $sk->jabatan }}<br>KABUPATEN MALANG</strong> </span><br>
                        {!! QrCode::size(100)->generate($url) !!}
                        <br>
                        <span><b><u>{{ $sk->pegawai }}</u></b>
                            <br>{{ $sk->pangkat }}
                            <br>NIP : {{ $sk->nip }}</span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
