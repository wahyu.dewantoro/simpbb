<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah" style="font-size: 14px">
        <strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong><br>
        <strong>{{ $sk->nomor }}</strong>
        <br>
        <br>
        <strong style="font-size: 14px">TENTANG<br>
            PENDAFTARAN OBYEK BARU SURAT PEMBERITAHUAN PAJAK TERUTANG <br>
            PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
            (SPPT PBB P2)
        </strong>

    </p>
    <table width="100%" style="font-size: 14px">
        <tr style="vertical-align: top">
            <td width="15%">Menimbang</td>
            <td width="1px">:</td>
            <td style="text-align: justify">Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu
                ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Pendaftaran Obyek Baru Surat Pemberitahuan
                Pajak Terutang Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (SPPT PBB P2).</td>
        </tr>
        <tr style="vertical-align: top">
            <td>Mengingat</td>
            <td>:</td>
            <td style="text-align: justify">
                <ol>
                    <li> Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan
                        Pemerintahan Daerah;</li>
                    <li> Peraturan Pemerintah Nomor 35 Tahun 2023 tentang Ketentuan Umum Pajak Daerah dan Retribusi
                        Daerah;</li>
                    {{-- <li> Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010
                        tentang Pajak Daerah;</li> --}}
                    <li>Peraturan Daerah Nomor 7 Tahun 2023 tentang Pajak Daerah dan Retribusi Daerah;</li>
                    <li> Peraturan Bupati Malang Nomor 32 Tahun 2019 Tentang Petunjuk Pelaksanaan Pemungutan Pajak Bumi
                        Dan Bangunan Perdesaan Dan Perkotaan.</li>

                </ol>

            </td>
        </tr>
        <tr style="vertical-align: top">
            <td>Memperhatikan</td>
            <td>:</td>
            <td>Surat permohonan Pendaftaran Obyek Baru SPPT Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang
                diajukan oleh Pemohon
                <br>
                a. Wajib Pajak/Kuasa Wajib Pajak : {{ $sk->TblSpop->nm_wp }} <br>
                b. Nomor Pelayanan : {{ $sk->TblSpop->layananObjek->nomor_layanan }}
            </td>
        </tr>
    </table>
    <p class="text-tengah" style="font-size: 14px"><strong>MEMUTUSKAN </strong></p>
    <table width="100%" style="font-size: 14px">
        <tr style="vertical-align: top">
            <td style="text-align: justify">
                Menerima permohonan Pendaftaran Obyek Baru SPPT PBB P2 yang diajukan oleh Pemohon sebagai berikut :
                :<br>
                <table width="100%" border="0" style="font-size: 14px">


                    <tr style="vertical-align: top">
                        <td width="20%">Nama WP</td>
                        <td width="1px">:</td>
                        <td>{{ $sk->hasil->nm_wp }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Alamat WP</td>
                        <td>:</td>
                        <td>
                            <?= $sk->hasil->alamat_wp ?>
                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Alamat Objek</td>
                        <td>:</td>
                        <td>{{ optional($sk->hasil)->alamat_op }}</td>
                    </tr>
                </table>
                <b style="font-size: 14px">NOP : {{ formatnop($sk->TblSpop->nop_proses) }}</b>
                <table class="table table-bordered" style="font-size: 14px">
                    <thead>
                        <tr>
                            <th>Objek</th>
                            <th>Luas</th>
                            <th>NJOP</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><b>Bumi</b></td>
                            <td style="text-align: center">{{ $sk->hasil->luas_bumi }}</td>
                            <td style="text-align: right">
                                {{ $sk->hasil->luas_bumi > 0 ? angka($sk->hasil->njop_bumi / $sk->hasil->luas_bumi) : 0 }}
                            </td>
                        </tr>
                        <tr>
                            <td><b>Bangunan</b></td>
                            <td style="text-align: center">{{ $sk->hasil->luas_bng }}</td>
                            <td style="text-align: right">
                                {{ $sk->hasil->luas_bng > 0 ? angka($sk->hasil->njop_bng / $sk->hasil->luas_bng) : '-' }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" style="font-size: 14px">
        <tbody>
            <tr>
                <td width="30%" rowspan="3"></td>
                <td rowspan="3"></td>
                <td>Ditetapkan di : {{ $sk->kota }}</td>
            </tr>
            <tr>
                <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
            </tr>
            <tr>
                <td width="40%" align="center">
                    <p>
                        <span class=""><strong>{{ $sk->jabatan }}<br>KABUPATEN MALANG</strong> </span><br>
                        {!! QrCode::size(100)->generate($url) !!}
                        <br>
                        <span><b><u>{{ $sk->pegawai }}</u></b>
                            <br>{{ $sk->pangkat }}
                            <br>NIP : {{ $sk->nip }}</span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
