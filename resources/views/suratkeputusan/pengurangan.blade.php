<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah">
        <strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong><br>
        <strong>{{ $sk->nomor }}</strong>
        <br>
        <br>
        <strong>TENTANG<br>
            PENGURANGAN PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN<br>
            (PBB P2)
        </strong>

    </p>
    <table width="100%">
        <tr style="vertical-align: top">
            <td width="15%" style="vertical-align: top">MENIMBANG</td>
            <td width="4%">:</td>
            <td style="text-align: justify">Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu
                ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Pengurangan Pajak Bumi dan Bangunan
                Perdesaan dan Perkotaan (PBB P2).</td>
        </tr>
        <tr style="vertical-align: top">
            <td>MENGINGAT</td>
            <td>:</td>
            <td style="text-align: justify">
                <ol>
                    <li>Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan
                        Pemerintahan Daerah; </li>
                    <li>Peraturan Pemerintah Nomor 35 Tahun 2023 tentang Ketentuan Umum Pajak Daerah dan Retribusi
                        Daerah;</li>
                    <li>Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010
                        tentang Pajak Daerah; </li>
                    <li>Peraturan Bupati Malang Nomor 51 Tahun 2017 tentang Perubahan atas Peraturan Bupati Malang Nomor
                        43 Tahun 2013 tentang Tata Cara Pengajuan dan Penyelesaian Pengurangan Pajak Bumi dan Bangunan
                        Perdesaan dan Perkotaan.</li>
                </ol>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td>MEMPERHATIKAN</td>
            <td>:</td>
            <td>Surat permohonan Pengurangan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh Pemohon
                <br>
                a. Wajib Pajak/Kuasa Wajib Pajak : {{ $sk->TblSpop->nm_wp }} <br>
                b. Nomor Pelayanan : {{ $sk->TblSpop->layananObjek->nomor_layanan }}
            </td>
        </tr>
    </table>
    <p class="text-tengah"><strong>MEMUTUSKAN </strong></p>
    <table width="100%">
        <tr style="vertical-align: top">
            <td width="15%" style="vertical-align: top">KESATU</td>
            <td width="4%">:</td>
            <td style="text-align: justify">Mengabulkan sebagian atau seluruhnya permohonan pengurangan PBB P2 dengan
                rincian :<br>
                <table width="100%">
                    <tr>
                        <td width="350px">a. Nama Wajib Pajak</td>
                        <td width="1px">:</td>
                        <td>{{ $spo->nm_wp_sppt }}</td>
                    </tr>
                    <tr>
                        <td>b. NOP</td>
                        <td width="1px">:</td>
                        <td>
                            {{ formatnop($sk->TblSpop->nop_proses) }}
                        </td>
                    </tr>

                    <tr>
                        <td>c. PBB Terhutang</td>
                        <td width="1px">:</td>
                        <td>Rp. {{ angka($spo->pbb_yg_harus_dibayar_sppt - $spo->insentif) }}</td>
                    </tr>
                    <tr>
                        <td>d. Prosentase pengurangan</td>
                        <td width="1px">:</td>
                        <td>

                            {{ angka($spo->persentase) }} %
                        </td>
                    </tr>
                    <tr>
                        <td>e. Besarnya Pengurangan (c x d)</td>
                        <td width="1px">:</td>
                        <td>Rp. {{ angka($spo->nilai_potongan) }}</td>
                    </tr>
                    <tr>
                        <td>f. Jumlah pajak terutang setelah pengurangan (c-e)</td>
                        <td>:</td>
                        <td>Rp. {{ angka($spo->pbb_yg_harus_dibayar_sppt - $spo->insentif - $spo->nilai_potongan) }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>KEDUA</td>
            <td>:</td>
            <td>Apabila di kemudian hari ternyata diketahui terdapat kekeliruan dalam Keputusan ini, maka akan
                dibetulkan sebagaimana mestinya.</td>
        </tr>
        <tr>
            <td>KETIGA</td>
            <td>:</td>
            <td>Keputusan Kepala Badan Pendapatan Daerah ini mulai berlaku pada tanggal ditetapkan.</td>
        </tr>
    </table>

    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%" rowspan="3"></td>
                <td rowspan="3"></td>
                <td>Ditetapkan di : {{ $sk->kota }}</td>
            </tr>
            <tr>
                <td>Pada Tanggal : {{ tglIndo($sk->tanggal) }}</td>
            </tr>
            <tr>
                <td width="40%" align="center">
                    <p>
                        <span class=""><strong>{{ $sk->jabatan }}<br>KABUPATEN MALANG</strong> </span><br>
                        {!! QrCode::size(100)->generate($url) !!}
                        <br>
                        <span><b><u>{{ $sk->pegawai }}</u></b>
                            <br>{{ $sk->pangkat }}
                            <br>NIP : {{ $sk->nip }}</span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
