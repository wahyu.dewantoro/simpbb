<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN DAFTAR SK</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{$subjek['kota_wp']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{$subjek['kelurahan_wp']}}</td>
        
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>Penelitian</th>
            <th>Ajuan</th>
            <th>NOP</th>
            <th>Wajib Pajak</th>
            <th>Alamat WP</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @php 
            $pbb_asli_total=0; 
            $pbb_bayar_total=0;    
        @endphp
        @foreach ($result as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td >{{ $row->no_formulir }}</td>
                <td >{{ $row->nama_layanan }}</td>
                <td >{{ formatnop($row->nop_proses)}}</td>
                <td >{{ $row->nm_wp}}</td>
                <td >{{ $row->kelurahan_wp }} {{ $row->kota_wp }}</td>
            </tr>
            @php 
                $no++; 
            @endphp
        @endforeach
    </tbody>
</table>
