@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Surat Keputusan</h1>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-12 ">
                <div class="card card-primary card-tabs">
                    <div class="card-body p-1">
                        <form action="#" id="form-cek" class="mb-1">
                            @csrf
                            <div class="row">

                                <div class="form-group col-md-2 m-0">
                                    <div class="input-group input-group-sm">
                                        <select id="kecamatan" name="kecamatan"
                                            class="form-control-sm form-control select-bottom pencarian_data"
                                            data-placeholder="Pilih Kecamatan" data-change="#desa"
                                            data-target-name='kelurahan'>
                                            @if (count($kecamatan) > 1)
                                                <option value="-">-- Kecamatan --</option>
                                            @endif
                                            @foreach ($kecamatan as $rowkec)
                                                <option value="{{ $rowkec->kd_kecamatan }}">
                                                    {{ $rowkec->nm_kecamatan }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3 m-0">
                                    <div class="input-group input-group-sm">
                                        <select id="kelurahan" name="kelurahan"
                                            class="form-control-sm form-control select-bottom select-reset change-next pencarian_data"
                                            data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                                            @if (count($kelurahan) > 1)
                                                <option value="-">-- Kelurahan --</option>
                                            @endif
                                            @foreach ($kelurahan as $rowkel)
                                                <option value="{{ $rowkel->kd_kelurahan }}">
                                                    {{ $rowkel->nm_kelurahan }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3 m-0" >
                                    <div id="reportrange"
                                        style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>
                                    <input type="hidden" id="tanggal_mulai" name="tanggal_mulai">
                                    <input type="hidden" id="tanggal_akhir" name="tanggal_akhir">
                                </div>
                                <div class="form-group col-md-3  m-0">
                                    <div class="input-group  input-group-sm">
                                        <select id="jenis_sk" name="jenis_sk" required
                                            class="form-control  pencarian_data" data-placeholder="Pilih Layanan">
                                            <option value="-">-- Jenis SK--</option>
                                            @foreach ($Jenis_layanan as $layanan)
                                                <option value="{{ $layanan->id }}">
                                                    {{ $layanan->nama_layanan }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <span class="input-group-append">
                                            <button type="button" id="cetak"
                                                class="btn btn-success btn-flat">Excell</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil">
                            <thead class="bg-danger">
                                <tr>
                                    <th style="width:1px !important;">No</th>
                                    <th>Penelitian</th>
                                    <th>Ajaun</th>
                                    <th>NOP</th>
                                    <th>Wajib Pajak</th>
                                    <th>Alamat WP</th>
                                    <th>Cetak</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl" style="max-width: 98%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail Hasil Penelitian</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0 m-0">
                </div>
            </div>
        </div>
    </div>
    <style>
        .upper {
            text-transform: uppercase;
        }

        .selectappend::after {
            content: "";
            position: absolute;
            z-index: 100;
            background: rgba(221, 221, 221, .8);
            top: 0px;
            bottom: 0px;
            width: 100%;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('js') }}/wilayah.js"></script>
    @php $search=''; @endphp
    @if (isset($nomor_layanan) && $nomor_layanan)
        @php $search='?nomor_layanan='.$nomor_layanan['nomor_layanan']; @endphp
    @endif
    <script>
        $(document).ready(function() {
            $('.tanggall').val('')
            $('.tanggall').daterangepicker({
                singleDatePicker: true
                    // ,autoUpdateInput: false
                    ,
                showDropdowns: true,
                autoApply: true,
                locale: {
                    format: 'DD MMM YYYY',
                    daysOfWeek: [
                        "Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
                    ],
                    monthNames: [
                        "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus",
                        "September", "Oktober", "November", "Desember"
                    ]
                }
            });

            var tableObjek, tableHasil;
            const changeData = function(e, async_status = true) {
                let uri = e.attr("data-change").split("#"),
                    target = e.closest('form').find("[name='" + e.attr("data-target-name") + "']");
                target.appendData("{{ url('') }}/" + uri[1], {
                    kd_kecamatan: e.val()
                }, {
                    obj: '-- Semua Kelurahan --',
                    key: '-'
                }, async_status, true);
                return target;
            };
            $(document).on("change", "[data-change]", function(evt) {
                let e = $(this);
                changeData(e);
            });
            $.fn.dataTable.ext.errMode = 'none' ?? 'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            tableHasil = $('#table-hasil').DataTable({
                processing: true,
                serverSide: true,
                orderable: false

                    ,
                ajax: {
                    url: "{{ url('penelitian/sk' . $search) }}",
                    data: function(d) {
                        d.jenis_sk = $('#jenis_sk').val()
                        d.kecamatan = $('#kecamatan').val()
                        d.kelurahan = $('#kelurahan').val()
                        d.tanggal = $('#tanggal').val()
                        d.tanggal_mulai = $('#tanggal_mulai').val()
                        d.tanggal_akhir = $('#tanggal_akhir').val()
                    }
                },
                columns: [{
                    data: null,
                    class: 'text-center',
                    orderable: false

                        ,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    searchable: false
                }, {
                    data: 'nomorlayanan',
                    orderable: false,
                    name: 'nomorlayanan'
                }, {
                    data: 'nama_layanan',
                    orderable: false
                }, {
                    data: 'nopproses',
                    orderable: false,
                    name: 'nop'
                }, {
                    data: 'nm_wp',
                    orderable: false,
                    name: 'a.nm_wp'
                }, {
                    data: 'kelurahan_wp',
                    orderable: false,
                    name: 'a.kelurahan_wp',
                    render: function(data, type, row) {
                        return data + '<br><small class="text-warning">' + row.kota_wp +
                            '</small>'
                    }
                }, {
                    data: 'pilih',
                    orderable: false,
                    name: 'pilih',
                    class: 'text-center',
                    searchable: false
                }],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });

            tableHasil.on('error.dt', (e, settings, techNote, message) => {
                Swal.fire({
                    icon: 'warning',
                    html: defaultError
                });
            });


            const toString = function(obj) {
                let str = [];
                obj.map((key) => {
                    if (key.name != '_token') {
                        str.push(key.name + "=" + key.value);
                    }
                });
                return str.join("&");
            }

            var start = moment().subtract(365, 'days');
            var end = moment();

            function cb(start, end) {

                $('#reportrange span').html(start.format('DD MMM YYYY') + ' - ' + end.format('DD MMM YYYY'));
                $('#tanggal_mulai').val(start.format('DD MMM YYYY'))
                $('#tanggal_akhir').val(end.format('DD MMM YYYY'))
                pencarian()
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            cb(start, end);


            function pencarian() {
                tableHasil.draw();
            }
            $(document).on('change', '.pencarian_data', function(e) {
                pencarian()
            })



            $(document).on("click", "#cetak", function(evt) {
                jenis_sk = $('#jenis_sk').val()
                kecamatan = $('#kecamatan').val()
                kelurahan = $('#kelurahan').val()
                tanggal_mulai = $('#tanggal_mulai').val()
                tanggal_akhir = $('#tanggal_akhir').val()


                // let e = $(this),
                // form = e.closest('form'),
                url = "{{ url('penelitian/sk_cetak') }}?jenis_sk=" + jenis_sk + "&kecamatan=" + kecamatan +
                    "&kelurahan=" + kelurahan + "&tanggal_mulai=" + tanggal_mulai + "&tanggal_akhir=" +
                    tanggal_akhir
                window.open(url, '_blank');
            });



            $('#table-hasil').on('click', '.cetak-sk', function(e) {
                e.preventDefault()
                id = $(this).data('id')
                url = $(this).data('url')
                window.open(url, 'newwindow', 'width=700,height=1000');
                return true;
            })


        })
    </script>
@endsection
