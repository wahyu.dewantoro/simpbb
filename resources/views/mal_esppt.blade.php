<html>
<head>
    <style>
        body {
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            size: 180mm 188mm;
            margin: 0;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif
        }

        #watermark {
            position: fixed;
            opacity: 0.2;
            /* margin: 0; */
        }

        @page {
            size: 180mm 188mm;
            margin: 0;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif
        }

        .konten {
            /* margin-left: 3mm;
            margin-right: 3mm; */
            margin-top: 8mm;
            /* ;
            
            
            margin-bottom: 4mm; */
        }

    </style>

</head>
<body>
    {{-- <div id="watermark"><img style="width: 180mm" src="{{ public_path('esppt.jpeg') }}"></div> --}}
    <div class="konten">
        <p style="text-align: center; font-weight: bold; font-size: 13px; margin-bottom:1px; ">SURAT PEMBERITAHUAN PAJAK TERHUTANG<BR>PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN</p>
        <span style="font-weight: bold; font-size: 12px ">NOP : 35.07.150.010.001-0002.0 </span>
        <table style=" table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='5' border="0">
            <tr style="vertical-align: top">
                <td style="width: 38%">
                    <p style=" margin-top:0; margin-bottom: 2px; font-weight: bold; font-size: 12px;">LETAK OBJEK PAJAK</p>
                    <p style="font-size: 9pt">Dsn Pagutan RT 011 RW 04<br>
                        Girimulyo<br>
                        Gedangan<br>
                        Kabupaten Malang
                    </p>
                </td>
                <td style="width: 38%">
                    <p style=" margin-top:0; margin-bottom: 2px; font-weight: bold; font-size: 12px; ">NAMA DAN ALAMAT WAJIB PAJAK</p>
                    <p style="font-size: 9pt">ACH SUGENG<br>
                        Dsn Pagutan RT 011 RW 04<br>
                        Girimulyo<br>
                        Kabupaten Malang</p>

                </td>
                <td style="width: 25%; text-align: center; vertical-align: middle ">
                    <p style="font-size: 25px; font-weight: bold">2022</p>
                </td>
            </tr>
        </table>
        <style>
            .table-bordered>tbody>tr>td,
            .table-bordered>thead>tr>td,
            .table-bordered {
                border: none;
                border-collapse: collapse;
                border-right: solid 1px black;
                border-left: solid 1px black;
            }

        </style>
        <table style="font-size: 12px;  table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1' class="table-bordered">
            <thead>
                <tr style="text-align: center; vertical-align: top;font-weight: bold; ">
                    <td style="border: solid 1px black;">OBJEK PAJAK</td>
                    <td style="border: solid 1px black;">LUAS M<sup>2</sup></td>
                    <td style="border: solid 1px black;">KELAS</td>
                    <td style="border: solid 1px black;">NJOP PER M<sup>2</sup> (Rp)</td>
                    <td style="border: solid 1px black;">TOTAL NJOP (Rp)</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style=" border-bottom: 0;">BUMI</td>
                    <td style="text-align: right; border-bottom: 0;">1.050</td>
                    <td style="text-align: center;  border-bottom: 0;; border-bottom-color:white; ">076</td>
                    <td style="text-align: right;  border-bottom: 0;; border-bottom-color:white; ">200.000</td>
                    <td style="text-align: right;  border-bottom: 0;; border-bottom-color:white; ">210.000.000</td>
                </tr>
                <tr>
                    <td style="border-bottom: solid 1px black;">BANGUNAN</td>
                    <td style="border-bottom: solid 1px black;  text-align: right">50</td>
                    <td style="border-bottom: solid 1px black;  text-align: center">023</td>
                    <td style="border-bottom: solid 1px black;  text-align: right">823.000</td>
                    <td style="border-bottom: solid 1px black;  text-align: right">41.150.000</td>
                </tr>
            </tbody>
        </table>
        <table style="font-size: 12px;  border-left: solid 1px black; border-top: 0; border-right: solid 1px black; border-bottom: solid 1px black; table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1'>
            <tr>
                <td>NJOP Sebagai dasar pengenaan PBB P2</td>
                <td style="text-align: left" width="10px">=</td>
                <td style="text-align: right">251.150.000</td>
            </tr>
            <tr>
                <td>NJOPTKP (NJOP Tidak Kena Pajak)</td>
                <td style="text-align: left" width="10px">=</td>
                <td style="text-align: right">10.000.000</td>
            </tr>
            <tr>
                <td>NJOP untuk perhitungan PBB P2</td>
                <td style="text-align: left" width="10px">=</td>
                <td style="text-align: right">241.150.000</td>
            </tr>
            <tr>
                <td>PBB P2 yang Terhutang</td>
                <td style="text-align: left" width="10px">=</td>
                <td style="text-align: right">0.1 % x 241.150.000 = 241.150 </td>
            </tr>
            <tr>
                <td>Potongan</td>
                <td style="text-align: left" width="10px">=</td>
                <td style="text-align: right">0 </td>
            </tr>
            <tr style="border-top: solid 1px black">
                <td colspan="2">PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN YANG HARUS DIBAYAR </td>
                <td style="text-align: right">(Rp) 241.150</td>
            </tr>
            <tr>
                <td colspan="3"><i style="font-weight: bold">DUA RATUS EMPAT PULUH SATU RIBU SERATUS LIMA PULUH RUPIAH </i></td>
            </tr>
            <tr style="border-top: solid 1px black">
                <td colspan="3" style="font-weight: bold">TUNGGAKAN</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp; &nbsp; </td>
            </tr>
        </table>
        <table style="font-size: 12px;  border-left: solid 1px black; border-top: 0; border-right: solid 1px black; border-bottom: solid 1px black; table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1'>
            <tr style="vertical-align: top">
                <td style="width:50%;">
                    TGL. JATUH TEMPO : 30 SEP 2022<br>
                    TEMPAT PEMBAYARAN :<br>
                    Bank Jatim, PT POS, Indomaret, Alfamart ,Shopee, Blibli

                </td>
                <td style="width:50%; text-align: center; vertical-align: top; border-left: solid 1px black;">
                    <p style="margin: 0">
                        MALANG, 03 JAN 2022<br>
                        KEPALA BADAN PENDAPATAN DAERAH<br><br><br><br><br><br>

                        MADE ARYA WEDANTHARA
                    </p>
                </td>
            </tr>
        </table>
        <br><br><br><br><br>
        {{-- border-left : solid 1px black;   border-top:solid 1px black; border-right: solid 1px black; border-bottom: solid 1px black; --}}
        <table style="font-size: 12px;   table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1'>
            <tr>
                <td>
                    NAMA WP :<br>
                    LetakObjek Pajak : Kecamatan SUMBERPUCUNG<br>
                    Desa/Kelurahan JATIGURI<br>
                    NOP : 35.07.050.008.000-4976.7<br>
                    SPPT Tahun/Rp. : 2022 / 241.150
                </td>
                <td style="width:45%; text-align: left; vertical-align: top; ">
                    Diterima Tgl &nbsp;&nbsp;&nbsp; :<br>
                    Tanda Tangan :<br>

                    <p style="text-align: center">………………………………………………………………<br>
                        Nama Terang</p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
