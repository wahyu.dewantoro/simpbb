@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tempat Pembayaran SPPT</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Form Tempat Pembayaran</h3>
                        </div>
                        <div class="card-body p-2">

                            <form action="{{ $data['url'] }}" method="POST">
                                @csrf
                                @method($data['method'])
                                <div class="form-group">
                                    <label for="">Nama Tempat</label>
                                    <input type="text" name="nama_tempat" class="form-control" required
                                        value="{{ $data['tempat']->nama_tempat ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <div class="float-right">
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i>
                                            Simpan</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
