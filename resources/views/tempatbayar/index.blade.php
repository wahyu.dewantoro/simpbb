@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tempat Pembayaran SPPT</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a href="{{ url('refrensi/tempat-bayar/create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-folder-plus"></i> Tambah
                        </a>
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data</h3>
                    <div class="card-tools">
                         
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th width="10px">No</th>
                                            <th>Nama Tempat</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $index => $row)
                                            <tr>
                                                <td align="center">{{ $index + $data->firstItem() }}</td>
                                                <td>{{ $row->nama_tempat }}</td>

                                                <td class="text-center">


                                                    <a href="{{ route('refrensi.tempat-bayar.edit', $row->id) }}"><i
                                                            class="fas fa-edit text-info"></i> </a>


                                                    <a href="{{ url('refrensi/tempat-bayar', $row->id) }}" onclick="
                                                                                                var result = confirm('Are you sure you want to delete this record?');
                                                                                                if(result){
                                                                                                    event.preventDefault();
                                                                                                    document.getElementById('delete-form-{{ $row->id }}').submit();
                                                                                                }"><i
                                                            class="fas fa-trash-alt text-danger"></i>
                                                    </a>
                                                    <form method="POST" id="delete-form-{{ $row->id }}"
                                                        action="{{ route('refrensi.tempat-bayar.destroy', [$row->id]) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer p-0">
                    <div class="float-sm-right">
                        {{ $data->appends(['cari' => request()->get('cari')]) }}
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
