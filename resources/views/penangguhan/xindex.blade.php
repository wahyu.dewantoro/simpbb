@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Dafar Penangguhan NOP</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <form action="#penangguhan_nop_search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control form-control-sm nop_full"  name="nop" placeholder="NOP" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group-sm">
                                            <button type='submit' class='btn btn-sm btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group col-lg-2">
                                        <div class="input-group input-group-sm">
                                            <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Cetak </button>
                                        </div>
                                    </div> -->
                                </div>
                            </form>
                            <div class="card-body no-padding p-1">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>NIK</th>
                                            <th>Nama</th>
                                            <th>NOP</th>
                                            <th>Alamat</th>
                                            <th>Luas Bumi</th>
                                            <th>Luas Bangunan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'data':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('pendataan/penangguhan_nop_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'nik_wp',class:'w-15'},
                    { data: 'nama_wp'},
                    { data: 'nop'},
                    { data: 'alamat_op'},
                    { data: 'luas_bumi'},
                    { data: 'luas_bng'},
                    { data: 'action'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            // let timer=60000;
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            // setInterval( function () {
            //     resLoad();
            // }, timer );
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    if(key.name!='_token'){
                        str.push(key.name+"="+key.value);
                    }
                });
                return str.join("&");
            }
            // $(document).on("click", ".btn-export", function (evt) {
            //     let e=$(this),
            //         form=e.closest('form'),
            //         url="{{url('pendataan/penangguhan_nop_cetak')}}?"+toString(form.serializeArray());
            //         window.open(url, '_blank');
            // });
            $(document).on("click", "[data-penangguhan]", function (evt) {
                let e=$(this),
                    send=e.attr('data-penangguhan');
                Swal.fire({ 
                    icon: 'warning',
                    // text: 'Penangguhan NOP?',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    html:'<div class="p-0 input-group input-group-sm text-left">'+
                            '<div class="col-sm-12 m-0 p-0 text-center">Penangguhan NOP?</div>'+
                            '<div class="col-md-12 row">'+
                                '<select name="alasan" class="form-control-sm form-control">'+
                                    '<option>WP Bandel</option>'+
                                    '<option>WP tidak diketahui</option>'+
                                    '<option>Fasum</option>'+
                                    '<option>Objek tidak ada</option>'+
                                    '<option>SPPT Ganda</option>'+
                                    '<option>Sengketa</option>'+
                                '</select>'+
                            '</div>' +
                            '<div class="col-md-12 row">'+
                                '<textarea name="keterangan" class="form-control-sm form-control triggerHeight trigger-h105" placeholder="Keterangan"></textarea>'+
                            '</div>' +
                        '</div>',
                    focusConfirm:false,
                    preConfirm:function(){
                        return [ $("input[name='alasan']").val(), $("textarea[name='keterangan']").val()];
                    }
                    // input: 'textarea',
                    // inputLabel: 'Keterangan',
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses Persiapan Penagguhan NOP',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        asyncData("{{ url('pendataan/penangguhan_nop_proses') }}",
                            {nop:send,keterangan:willsend.value}).then((response) => {
                            if(!response.status){
                                return Swal.fire({ icon: 'error', html: response.msg}); 
                            }
                            Swal.fire({
                                title: 'Info',
                                html:response.msg,
                                showConfirmButton: false,
                            });
                            resLoad();
                        }).catch((error)=>{
                            Swal.fire({ icon: 'error', text: defaultError});
                        });
                    }
                });
            })
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian Data.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    swal.close();
                });
            });
        });
    </script>
@endsection