@extends('layouts.app')
@section('css')
    <style>
        .has-search .form-control {
            padding-left: 2.375rem;
        }

        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
        }

        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Adjustment</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">

                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'Koreksi Piutang' ? 'active' : '' }}"
                                id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile"
                                role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Koreksi
                                Piutang</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'Blokir/Iventarisasi' ? 'active' : '' }}"
                                id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-messages"
                                role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Blokir /
                                Iventarisasi</a>
                        </li>

                    </ul>
                </div>
                <div class="card-body p-1">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade {{ $tab == 'Koreksi Piutang' ? ' show active' : '' }} p-0"
                            id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="search" id="search"
                                            class="form-control form-control-sm" placeholder="Pencarian">
                                        <span class="input-group-append">
                                            <button id="cari" type="button" class="btn btn-info btn-flat"><i
                                                    class="fas fa-search"></i></button>

                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <a class="btn btn-primary btn-flat float-right"
                                        href="{{ url('pendataan/penangguhan-form-koreksi') }}"><i
                                            class="far fa-plus-square"></i> Tambah KP</a>
                                    <a class="btn btn-success btn-flat float-right"
                                        href="{{ url('pendataan/pembatalan-sistep') }}"><i class="far fa-plus-square"></i>
                                        Pembatalan Sistep</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="table"
                                    class="table table-bordered table-striped text-sm table-sm display nowrap"
                                    style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Transaksi</th>
                                            <th>NOP</th>
                                        
                                            <th>Surat</th>
                                            
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>
                        <div class="tab-pane fade {{ $tab == 'Blokir/Iventarisasi' ? ' show active' : '' }}"
                            id="custom-tabs-four-messages" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select name="jns_koreksi" id="jn_koreksi_blokir"
                                                class="form-control form-control-sm" required>
                                                <option value="">Jenis</option>
                                                @foreach (KategoriIventarisasi() as $i => $item)
                                                    <option value="{{ $i }}">{{ $i }} -
                                                        {{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div class="input-group input-group-sm">
                                                <input type="text" name="search" id="search_empat"
                                                    class="form-control form-control-sm" placeholder="Pencarian">
                                                <span class="input-group-append">
                                                    <button id="cari_empat" type="button" class="btn btn-info btn-flat"><i
                                                            class="fas fa-search"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <a class="btn btn-primary btn-flat float-right"
                                        href="{{ url('pendataan/penangguhan-form-iventarisasi') }}"><i
                                            class="far fa-plus-square"></i> Tambah</a>
                                            <a class="btn btn-success btn-flat float-right"
                                            href="{{ url('pendataan/penangguhan-upload-iventarisasi') }}"><i
                                                class="fas fa-file-upload"></i> Upload</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="table_empat"
                                    class="table table-bordered table-striped text-sm table-sm display nowrap"
                                    style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Transaksi</th>
                                            <th>NOP</th>
                                            <th>Surat</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            /*$(document).ajaxSend(function(event, jqxhr, settings) {
             $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });
*/
            function cekNopIb() {
                var nop = $('#nop_ib').val()
                var a = nop.toString();
                var b = a.replace(/[^\d]/g, "");
                var thn_pajak_sppt = $('#thn_pajak_sppt_tiga').val()
                if (b.length == 18 && thn_pajak_sppt.length == 4) {
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan-cek-nop-kp') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "nop": b,
                            "thn_pajak_sppt": thn_pajak_sppt
                        },
                        success: function(res) {
                            console.log(res)
                            if (res.status == '1') {
                                $('#nilai_koreksi_tiga').val(res.pbb)
                            } else {
                                // masalah
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Peringatan',
                                    text: res.keterangan,
                                })
                                $('#nilai_koreksi_tiga').val('')
                                $('#nop_ib').val('')
                                $('#thn_pajak_sppt_tiga').val('')
                            }
                        },
                        error: function(er) {
                            // $.LoadingOverlay("hide");
                        }
                    })
                }
            }


            $('#table').on('click', '.buka', function(e) {
                e.preventDefault();
                var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,
                    thn_pajak_sppt;
                kd_propinsi = $(this).data('kd_propinsi')
                kd_dati2 = $(this).data('kd_dati2')
                kd_kecamatan = $(this).data('kd_kecamatan')
                kd_kelurahan = $(this).data('kd_kelurahan')
                kd_blok = $(this).data('kd_blok')
                no_urut = $(this).data('no_urut')
                kd_jns_op = $(this).data('kd_jns_op')
                thn_pajak_sppt = $(this).data('thn_pajak_sppt')
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "NOP akan masuk kedalam piutang DHKP",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        openloading()
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('pembayaran.blokir.buka') }}",
                            data: {
                                kd_propinsi,
                                kd_dati2,
                                kd_kecamatan,
                                kd_kelurahan,
                                kd_blok,
                                no_urut,
                                kd_jns_op,
                                thn_pajak_sppt,
                                '_token': '{{ csrf_token() }}',
                            },
                            success: function(data) {
                                closeloading()
                                toastr.success(data.msg);
                                tableHasil.draw();
                            },
                            error: function() {
                                closeloading()
                                toastr.error('Gagal melakukan proses buka blokir');
                            }
                        });
                    }
                })
            })


            $('#table_empat').on('click', '.buka_empat', function(e) {
                e.preventDefault();
                var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,
                    thn_pajak_sppt;
                kd_propinsi = $(this).data('kd_propinsi')
                kd_dati2 = $(this).data('kd_dati2')
                kd_kecamatan = $(this).data('kd_kecamatan')
                kd_kelurahan = $(this).data('kd_kelurahan')
                kd_blok = $(this).data('kd_blok')
                no_urut = $(this).data('no_urut')
                kd_jns_op = $(this).data('kd_jns_op')
                thn_pajak_sppt = $(this).data('thn_pajak_sppt')
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "NOP akan masuk kedalam piutang DHKP",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        openloading()
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('pembayaran.blokir.buka') }}",
                            data: {
                                kd_propinsi,
                                kd_dati2,
                                kd_kecamatan,
                                kd_kelurahan,
                                kd_blok,
                                no_urut,
                                kd_jns_op,
                                thn_pajak_sppt,
                                '_token': '{{ csrf_token() }}',
                            },
                            success: function(data) {
                                closeloading()
                                toastr.success(data.msg);
                                tableempat.draw();
                            },
                            error: function() {
                                closeloading()
                                toastr.error('Gagal melakukan proses buka blokir');
                            }
                        });
                    }
                })
            })

            tableHasil = $('#table').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('pembayaran.blokir.index') }}",
                    data: function(d) {
                        d.jenis = 'KP'
                        d.search = $('#search').val()
                    }
                },
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        searchable: false
                    }, {
                        data: 'no_transaksi',
                        name: 'no_transaksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            let ket = row.nm_kategori;
                            return data + '<br>' + ket
                        }
                    }, {
                        data: 'nop',
                        name: 'nop',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            return data + "<br>" + row.thn_pajak_sppt
                        }
                    },

                    {
                        data: 'surat',
                        name: 'surat',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false
                    }
                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });
            let tableempat = $('#table_empat').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('pembayaran.blokir.index') }}",
                    data: function(d) {
                        d.jenis = '2'
                        d.search = $('#search_empat').val()
                        d.kd_kategori = $('#jn_koreksi_blokir').val()
                    }
                },
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        searchable: false
                    }, {
                        data: 'no_transaksi',
                        name: 'no_transaksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            let ket = row.nm_kategori;
                            return data + '<br>' + ket
                        }
                    }, {
                        data: 'nop',
                        name: 'nop',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            return data + "<br>" + row.thn_pajak_sppt
                        }
                    },

                    {
                        data: 'surat',
                        name: 'surat',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false
                    }
                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });
            $('#cari').on('click', function(e) {
                e.preventDefault()
                tableHasil.draw();
            })
            $('#cari_empat').on('click', function(e) {
                e.preventDefault()
                tableempat.draw();
            })
        })
    </script>
@endsection
