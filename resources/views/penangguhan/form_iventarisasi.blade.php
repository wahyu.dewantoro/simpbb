@extends('layouts.app')
@section('css')
    <style>
        .dataTables_filter,
        .dataTables_length {
            ater display: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Iventarisasi / Blokir</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-flat btn-primary btn-sm" href="{{ url('pendataan/penangguhan') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{ route('pembayaran.blokir.store') }}" method="POST"
                                enctype="multipart/form-data" class="form-horizontal" id="formbynopblokir">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Jenis</label>
                                    <div class="col-sm-8">
                                        <select name="jns_koreksi" id="jns_koreksi_blokir"
                                            class="form-control form-control-sm" required>
                                            <option value="">Pilih</option>
                                            @foreach (KategoriIventarisasi() as $i => $item)
                                                <option value="{{ $i }}">{{ $i }} -
                                                    {{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal / No Surat</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="tgl_surat" class="form-control form-control-sm tanggal"
                                            placeholder="Tanggal " required>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" name="no_surat" class="form-control form-control-sm "
                                            placeholder="Nomor surat " required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">NOP</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nop" id="nop"
                                            class="form-control form-control-sm nop" placeholder="Nomor objek pajak"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea name="keterangan" id="keterangan" class="form-control"  placeholder="Masukan keterangan" cols="30" rows="4"></textarea>
                                        {{-- <input type="text" name="nop" id="nop"
                                            class="form-control form-control-sm nop" placeholder="Nomor objek pajak"
                                            required> --}}
                                    </div>
                                </div>
                                <div id="nop_penyesuaian"></div>
                                <div class="form-group">

                                    <button type="submit" id="submit_tiga" class="btn btn-primary float-right"><i
                                            class="far fa-save"></i>
                                        Submit</button>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <div id="tunggakan"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });


            $('.nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });


            $('#nop').on('keyup', function(e) {

                // jn_koreksi_blokir


                var a = $('#nop').val()
                var b = a.replace(/[^\d]/g, "");
                var c = "";
                var panjang = b.length;
                $('#nop_penyesuaian').html('')
                if (panjang == '18') {
                    $('#tunggakan').html('')
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan_nop') }}",
                        // method: 'GET',
                        data: {
                            nop: b
                        },
                        success: function(res) {
                            $('#tunggakan').html(res)
                        },
                        errorr: function(e) {
                            $('#tunggakan').html('')
                            Swal.fire({
                                icon: 'error',
                                title: 'Opps',
                                text: "Terjadi kesalahan pada sistem!",
                                allowOutsideClick: false,
                            })
                        }
                    })

                    var jns_koreksi_blokir = $('#jns_koreksi_blokir').val()
                    // nop_penyesuaian
                    if (jns_koreksi_blokir == '07') {
                        $.ajax({
                            url: "{{ url('pendataan/penangguhan-list-tunggakan') }}",
                            // method: 'GET',
                            data: {
                                nop: b
                            },
                            success: function(res) {
                                $('#nop_penyesuaian').html(res)
                            },
                            errorr: function(e) {
                                $('#nop_penyesuaian').html('')
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Opps',
                                    text: "Terjadi kesalahan pada sistem!",
                                    allowOutsideClick: false,
                                })
                            }
                        })
                    }
                }
            })


            $('#jns_koreksi_blokir').on('change', function(e) {
                var jns_koreksi_blokir = $('#jns_koreksi_blokir').val()
                var a = $('#nop').val()
                var b = a.replace(/[^\d]/g, "");
                var c = "";
                var panjang = b.length;
                if (panjang == '18' && jns_koreksi_blokir == '07') {
                    $('#nop').trigger('keyup')
                }
            })

        })
    </script>
@endsection
