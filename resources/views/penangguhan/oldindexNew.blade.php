@extends('layouts.app')
@section('css')
    <style>
        .has-search .form-control {
            padding-left: 2.375rem;
        }

        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
        }

        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Adjustment</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">

                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                        {{-- <li class="nav-item">
                            <a class="nav-link {{ $tab == 'Penonaktifan Objek' ? 'active' : '' }}"
                                id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home"
                                role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Penonaktifan
                                Objek</a>
                        </li> --}}
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'Koreksi Piutang' ? 'active' : '' }}"
                                id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile"
                                role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Koreksi
                                Piutang</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'Blokir / Iventarisasi' ? 'active' : '' }}"
                                id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-messages"
                                role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Blokir /
                                Iventarisasi</a>
                        </li>

                    </ul>
                </div>
                <div class="card-body p-0">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade  {{ $tab == 'Penonaktifan Objek' ? ' show active' : '' }} p-2"
                            id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">NOP Aktif</label>
                                    <div class="form-group has-search">
                                        <span class="fa fa-search form-control-feedback"></span>
                                        <input type="text" class="form-control nop" placeholder="Masukan NOP"
                                            id="nop">
                                    </div>
                                </div>
                            </div>
                            <div id="hasil"></div>
                        </div>
                        <div class="tab-pane fade {{ $tab == 'Koreksi Piutang' ? ' show active' : '' }} p-0"
                            id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                            <div class="card card-info card-tabs">
                                <div class="card-header p-0 pt-1">
                                    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                                        <li class="pt-2 px-3">
                                            <h3 class="card-title">Koreksi</h3>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" id="tab_by_nop-tab" data-toggle="pill"
                                                href="#tab_by_nop" role="tab" aria-controls="tab_by_nop"
                                                aria-selected="true">By Nop</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="tab_by_upoad-tab" data-toggle="pill"
                                                href="#tab_by_upoad" role="tab" aria-controls="tab_by_upoad"
                                                aria-selected="false">Upload</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="tab_by_data-tab" data-toggle="pill" href="#tab_by_data"
                                                role="tab" aria-controls="tab_by_data" aria-selected="false">Data</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body p-2">
                                    <div class="tab-content" id="custom-tabs-two-tabContent">
                                        <div class="tab-pane fade show active" id="tab_by_nop" role="tabpanel"
                                            aria-labelledby="tab_by_nop-tab">
                                            <form action="{{ route('pembayaran.blokir.store') }}" method="post"
                                                id="formbynop">
                                                @method('post')
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="nop">NOP</label>
                                                                    <input required type="text" name="nop"
                                                                        id="nop_kp" autofocus="true"
                                                                        class="form-control form-control-sm nop"
                                                                        placeholder="NOP">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="thn_pajak_sppt">Masa Pajak</label>
                                                                <input type="text"
                                                                    class="form-control form-control-sm angka"
                                                                    name="thn_pajak_sppt" id="thn_pajak_sppt" required>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="thn_pajak_sppt">Nilai</label>
                                                                <input type="text"
                                                                    class="form-control form-control-sm angka"
                                                                    name="nilai_koreksi" id="nilai_koreksi" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label for="no_surat">Nomor Surat</label>
                                                                    <input required type="text" name="no_surat"
                                                                        id="no_surat"
                                                                        class="form-control form-control-sm"
                                                                        placeholder="Nomor surat">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="tgl_surat">Tanggal Surat</label>
                                                                    <input required type="text"
                                                                        class="form-control form-control-sm tanggal"
                                                                        name="tgl_surat" id="tgl_surat">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="jns_koreksi" value="3">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="keterangan">Keterangan</label>
                                                                <textarea class="form-control" name="keterangan" id="keterangan" rows="3"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="float-right">
                                                            <button type="submit" id="submit"
                                                                class="btn btn-primary"><i class="far fa-save"></i>
                                                                submit</button>
                                                        </div>


                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab_by_upoad" role="tabpanel"
                                            aria-labelledby="tab_by_upoad-tab">
                                            <h6>Upload Koreksi</h6>[<a href="{{ url('tmp_upload_blokir.xlsx') }}"><i
                                                    class="far fa-file-alt"></i> Template Upload</a>]
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="tgl_surat">File</label>
                                                        <input type="file" name="file" id="file"
                                                            class="form-control" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="no_surat">Nomor Surat</label>
                                                        <input required type="text" name="no_surat_dua"
                                                            id="no_surat_dua" class="form-control form-control-sm"
                                                            placeholder="Nomor surat">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tgl_surat">Tanggal Surat</label>
                                                        <input required type="text"
                                                            class="form-control form-control-sm tanggal"
                                                            name="tgl_surat_dua" id="tgl_surat_dua">
                                                        <input type="hidden" name="jns_koreksi_dua" value="3">
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="keterangan_dua">Keterangan</label>
                                                        <textarea class="form-control" name="keterangan_dua" id="keterangan_dua" required rows="2"></textarea>
                                                    </div>
                                                    <div class="form-grup">
                                                        <div class="float-right">
                                                            <button type="button" id="upload"
                                                                class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Upload</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab_by_data" role="tabpanel"
                                            aria-labelledby="tab_by_data-tab">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <div class="input-group input-group-sm">
                                                        <input type="text" name="search" id="search"
                                                            class="form-control form-control-sm" placeholder="Pencarian">
                                                        <span class="input-group-append">
                                                            <button id="cari" type="button"
                                                                class="btn btn-info btn-flat"><i
                                                                    class="fas fa-search"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="table"
                                                    class="table table-bordered table-striped text-sm table-sm display nowrap"
                                                    style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Batch</th>
                                                            <th>NOP</th>
                                                            <th>Tahun Pajak</th>
                                                            <th>Keterangan</th>
                                                            <th>Created By</th>
                                                            <th>Verifikator</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane fade {{ $tab == 'Blokir / Iventarisasi' ? ' show active' : '' }}"
                            id="custom-tabs-four-messages" role="tabpanel"
                            aria-labelledby="custom-tabs-four-messages-tab">
                            <div class="card card-info card-tabs">
                                <div class="card-header p-0">
                                    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                                        <li class="pt-2 px-3">
                                            <h3 class="card-title">Blokir / Iventarisasi</h3>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" id="tab_by_nop_blokir-tab" data-toggle="pill"
                                                href="#tab_by_nop_blokir" role="tab"
                                                aria-controls="tab_by_nop_blokir" aria-selected="true">By Nop</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="tab_by_upoad_blokir-tab" data-toggle="pill"
                                                href="#tab_by_upoad_blokir" role="tab"
                                                aria-controls="tab_by_upoad_blokir" aria-selected="false">Upload</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="tab_by_data_blokir-tab" data-toggle="pill"
                                                href="#tab_by_data_blokir" role="tab"
                                                aria-controls="tab_by_data_blokir" aria-selected="false">Data</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body p-2">
                                    <div class="tab-content" id="custom-tabs-two-tabContent">
                                        <div class="tab-pane fade show active" id="tab_by_nop_blokir" role="tabpanel"
                                            aria-labelledby="tab_by_nop_blokir-tab">
                                            <form action="{{ route('pembayaran.blokir.store') }}" method="post"
                                                id="formbynopblokir">
                                                @method('post')
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="nop">NOP</label>
                                                                    <input required type="text" name="nop"
                                                                        id="nop_ib" autofocus="true"
                                                                        class="form-control form-control-sm nop"
                                                                        placeholder="NOP">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="thn_pajak_sppt">Masa Pajak</label>
                                                                <input type="text"
                                                                    class="form-control form-control-sm angka"
                                                                    name="thn_pajak_sppt" id="thn_pajak_sppt_tiga"
                                                                    required>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="thn_pajak_sppt">Nilai</label>
                                                                <input type="text"
                                                                    class="form-control form-control-sm angka"
                                                                    name="nilai_koreksi" id="nilai_koreksi_tiga" required>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="">Jenis</label>
                                                                    <select name="blokir" id="blokir"
                                                                        class="form-control form-control-sm">
                                                                        <option value="1">Blokir Usulan KP</option>
                                                                        <option value="2">Iventarisasi</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="no_surat">Nomor Surat</label>
                                                                    <input required type="text" name="no_surat"
                                                                        id="no_surat_tiga"
                                                                        class="form-control form-control-sm"
                                                                        placeholder="Nomor surat">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="tgl_surat">Tanggal Surat</label>
                                                                    <input required type="text"
                                                                        class="form-control form-control-sm tanggal"
                                                                        name="tgl_surat" id="tgl_surat_tiga">
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="jns_koreksi" value="3">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="keterangan">Keterangan</label>
                                                                <textarea class="form-control" name="keterangan" id="keterangan_tiga" rows="3"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="float-right">
                                                            <button type="submit" id="submit_tiga"
                                                                class="btn btn-primary"><i class="far fa-save"></i>
                                                                submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab_by_upoad_blokir" role="tabpanel"
                                            aria-labelledby="tab_by_upoad_blokir-tab">
                                            <h6>Upload Blokir</h6>[<a href="{{ url('tmp_upload_blokir.xlsx') }}"><i
                                                    class="far fa-file-alt"></i> Template Upload</a>]
                                            <div class="row">
                                                <div  class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="tgl_surat">File</label>
                                                                <input type="file" name="file" id="file_empat"
                                                                    class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">Jenis</label>
                                                                <select name="blokir_empat" id="blokir_empat"
                                                                    class="form-control form-control-sm">
                                                                    <option value="1">Blokir Usulan KP</option>
                                                                    <option value="2">Iventarisasi</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="no_surat">Nomor Surat</label>
                                                                <input required type="text" name="no_surat_empat"
                                                                    id="no_surat_empat" class="form-control form-control-sm"
                                                                    placeholder="Nomor surat">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="tgl_surat">Tanggal Surat</label>
                                                                <input required type="text"
                                                                    class="form-control form-control-sm tanggal"
                                                                    name="tgl_surat_empat" id="tgl_surat_empat">
                                                                <input type="hidden" name="jns_koreksi_empat" value="3">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="keterangan_empat">Keterangan</label>
                                                        <textarea class="form-control" name="keterangan_empat" id="keterangan_empat" required rows="2"></textarea>
                                                    </div>
                                                    <div class="form-grup">
                                                        <div class="float-right">
                                                            <button type="button" id="upload_empat"
                                                                class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Upload</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab_by_data_blokir" role="tabpanel"
                                            aria-labelledby="tab_by_data_blokir-tab">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <div class="input-group input-group-sm">
                                                        <input type="text" name="search" id="search_empat"
                                                            class="form-control form-control-sm" placeholder="Pencarian">
                                                        <span class="input-group-append">
                                                            <button id="cari_empat" type="button"
                                                                class="btn btn-info btn-flat"><i
                                                                    class="fas fa-search"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="table_empat"
                                                    class="table table-bordered table-striped text-sm table-sm display nowrap"
                                                    style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Batch</th>
                                                            <th>NOP</th>
                                                            <th>Tahun Pajak</th>
                                                            <th>Keterangan</th>
                                                            <th>Created By</th>
                                                            <th>Verifikator</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });


            $('.nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            function cekNopKp() {
                var nop = $('#nop_kp').val()
                var a = nop.toString();
                var b = a.replace(/[^\d]/g, "");
                var thn_pajak_sppt = $('#thn_pajak_sppt').val()
                if (b.length == 18 && thn_pajak_sppt.length == 4) {
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan-cek-nop-kp') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "nop": b,
                            "thn_pajak_sppt": thn_pajak_sppt
                        },
                        success: function(res) {
                            console.log(res)
                            if (res.status == '1') {
                                $('#nilai_koreksi').val(res.pbb)
                            } else {
                                // masalah
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Peringatan',
                                    text: res.keterangan,
                                })
                                $('#nilai_koreksi').val('')
                                $('#nop_kp').val('')
                                $('#thn_pajak_sppt').val('')
                            }

                        },
                        error: function(er) {
                            // $.LoadingOverlay("hide");
                        }
                    })
                }
            }

            function cekNopIb() {
                var nop = $('#nop_ib').val()
                var a = nop.toString();
                var b = a.replace(/[^\d]/g, "");
                var thn_pajak_sppt = $('#thn_pajak_sppt_tiga').val()
                if (b.length == 18 && thn_pajak_sppt.length == 4) {
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan-cek-nop-kp') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "nop": b,
                            "thn_pajak_sppt": thn_pajak_sppt
                        },
                        success: function(res) {
                            console.log(res)
                            if (res.status == '1') {
                                $('#nilai_koreksi_tiga').val(res.pbb)
                            } else {
                                // masalah
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Peringatan',
                                    text: res.keterangan,
                                })
                                $('#nilai_koreksi_tiga').val('')
                                $('#nop_ib').val('')
                                $('#thn_pajak_sppt_tiga').val('')
                            }

                        },
                        error: function(er) {
                            // $.LoadingOverlay("hide");
                        }
                    })
                }
            }

            $('#nop_kp,#thn_pajak_sppt').on('keyup change', function(e) {
                cekNopKp()
            })

            $('#nop_ib,#thn_pajak_sppt_tiga').on('keyup change', function(e) {
                cekNopIb()
            })


            $("#formbynop").on("submit", function(e) {
                // openloading();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('pembayaran.blokir.store') }}",
                    type: 'post',
                    data: $(this).serialize(),
                    success: function(data) {

                        // toastr.success(data.msg);



                        $('#nop_kp').val('')
                        $('#thn_pajak_sppt').val('')
                        $('#nilai_koreksi').val('')

                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: data.msg,
                        })
                    },
                    error: function(res) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan',
                            text: "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.",
                        })
                    }
                })
            });

            $("#formbynopblokir").on("submit", function(e) {
                // openloading();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('pembayaran.blokir.store') }}",
                    type: 'post',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('#nop_ib').val('')
                        $('#thn_pajak_sppt_tiga').val('')
                        $('#nilai_koreksi_tiga').val('')
                        $('#no_surat_tiga').val('')
                        $('#tgl_surat_tiga').val('')
                        $('#keterangan_tiga').val('')
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: data.msg,
                        })
                    },
                    error: function(res) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan',
                            text: "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.",
                        })
                    }
                })
            });

            $('#table').on('click', '.buka', function(e) {
                e.preventDefault();
                var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,
                    thn_pajak_sppt;
                kd_propinsi = $(this).data('kd_propinsi')
                kd_dati2 = $(this).data('kd_dati2')
                kd_kecamatan = $(this).data('kd_kecamatan')
                kd_kelurahan = $(this).data('kd_kelurahan')
                kd_blok = $(this).data('kd_blok')
                no_urut = $(this).data('no_urut')
                kd_jns_op = $(this).data('kd_jns_op')
                thn_pajak_sppt = $(this).data('thn_pajak_sppt')

                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "NOP akan masuk kedalam piutang DHKP",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        openloading()
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('pembayaran.blokir.buka') }}",
                            data: {
                                kd_propinsi,
                                kd_dati2,
                                kd_kecamatan,
                                kd_kelurahan,
                                kd_blok,
                                no_urut,
                                kd_jns_op,
                                thn_pajak_sppt,
                                '_token': '{{ csrf_token() }}',
                            },
                            success: function(data) {
                                closeloading()
                                toastr.success(data.msg);
                                tableHasil.draw();
                            },
                            error: function() {
                                closeloading()
                                toastr.error('Gagal melakukan proses buka blokir');
                            }
                        });
                    }
                })
            })

            $('#table_empat').on('click', '.buka_empat', function(e) {
                e.preventDefault();
                var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,
                    thn_pajak_sppt;
                kd_propinsi = $(this).data('kd_propinsi')
                kd_dati2 = $(this).data('kd_dati2')
                kd_kecamatan = $(this).data('kd_kecamatan')
                kd_kelurahan = $(this).data('kd_kelurahan')
                kd_blok = $(this).data('kd_blok')
                no_urut = $(this).data('no_urut')
                kd_jns_op = $(this).data('kd_jns_op')
                thn_pajak_sppt = $(this).data('thn_pajak_sppt')

                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "NOP akan masuk kedalam piutang DHKP",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        openloading()
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('pembayaran.blokir.buka') }}",
                            data: {
                                kd_propinsi,
                                kd_dati2,
                                kd_kecamatan,
                                kd_kelurahan,
                                kd_blok,
                                no_urut,
                                kd_jns_op,
                                thn_pajak_sppt,
                                '_token': '{{ csrf_token() }}',
                            },
                            success: function(data) {
                                closeloading()
                                toastr.success(data.msg);
                                tableempat.draw();
                            },
                            error: function() {
                                closeloading()
                                toastr.error('Gagal melakukan proses buka blokir');
                            }
                        });
                    }
                })
            })
            // table_empat

            $('#upload').on('click', function(e) {
                e.preventDefault()
                $('#modal-upload').modal('toggle');
                let keterangan = $('#keterangan_dua').val()
                var files = $('#file')[0].files;

                var no_surat_dua = $('#no_surat_dua').val()
                var tgl_surat_dua = $('#tgl_surat_dua').val()
                var fd = new FormData();
                if (files.length > 0 && keterangan != '') {
                    var filename = $('#file').val().split('\\').pop();
                    var la = filename.split('.');
                    let vexe = ['xlsx', 'xls'];
                    let exe = la[la.length - 1];
                    status = '0';
                    for (var i = 0; i < vexe.length; i++) {
                        var name = vexe[i];
                        if (name == exe.toLowerCase()) {
                            status = '1';
                            break;
                        }
                    }
                    if (status == '0') {

                        $(this).val('');
                        swal.fire({
                            title: "Oppssss",
                            text: "File ." + exe +
                                " tidak di perbolehkan, harus format .xls atau .xlsx",
                            icon: "warning",
                            // timer: 2000,
                        });
                    } else {
                        fd.append('file', files[0])
                        fd.append('keterangan', keterangan)
                        fd.append('no_surat', no_surat_dua)
                        fd.append('tgl_surat', tgl_surat_dua)
                        fd.append('_token', "{{ csrf_token() }}")

                        /*   */

                        $.ajax({
                            url: "{{ route('pembayaran.blokir.upload') }}",
                            type: 'post',
                            data: fd,
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                $('#file').val('')
                                $('#keterangan_dua').val('')

                                $('#no_surat_dua').val('')
                                $('#tgl_surat_dua').val('')
                                swal.fire({
                                    title: "Success",
                                    text: response.msg,
                                    icon: "success",
                                });
                            },
                            error: function(res) {
                                $('#file').val('')
                                $('#keterangan_dua').val('')
                                swal.fire({
                                    title: "Error",
                                    text: "Terjadi kesalahan",
                                    icon: "warning",
                                    timer: 2000,
                                });
                            }
                        });
                    }
                } else {
                    swal.fire({
                        title: "Peringatan",
                        text: "Silahkan pilih file dan keterangan harus isi.",
                        icon: "warning",
                        timer: 2000,
                    });

                }

            })

            $('#upload_empat').on('click', function(e) {
                e.preventDefault()
                // $('#modal-upload').modal('toggle');
                let keterangan = $('#keterangan_empat').val()
                var files = $('#file_empat')[0].files;
                var blokir=$('#blokir_empat').val()
                var no_surat_empat = $('#no_surat_empat').val()
                var tgl_surat_empat = $('#tgl_surat_empat').val()
                var fd = new FormData();
                if (files.length > 0 && keterangan != '') {
                    var filename = $('#file_empat').val().split('\\').pop();
                    var la = filename.split('.');
                    let vexe = ['xlsx', 'xls'];
                    let exe = la[la.length - 1];
                    status = '0';
                    for (var i = 0; i < vexe.length; i++) {
                        var name = vexe[i];
                        if (name == exe.toLowerCase()) {
                            status = '1';
                            break;
                        }
                    }
                    if (status == '0') {

                        $(this).val('');
                        swal.fire({
                            title: "Oppssss",
                            text: "File ." + exe +
                                " tidak di perbolehkan, harus format .xls atau .xlsx",
                            icon: "warning",
                            // timer: 2000,
                        });
                    } else {
                        fd.append('file', files[0])
                        fd.append('keterangan', keterangan)
                        fd.append('blokir', blokir)
                        fd.append('no_surat', no_surat_empat)
                        fd.append('tgl_surat', tgl_surat_empat)
                        fd.append('_token', "{{ csrf_token() }}")

                        /*   */

                        $.ajax({
                            url: "{{ route('pembayaran.blokir.upload') }}",
                            type: 'post',
                            data: fd,
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                $('#file_empat').val('')
                                $('#keterangan_empat').val('')

                                $('#no_surat_empat').val('')
                                $('#tgl_surat_empat').val('')
                                swal.fire({
                                    title: "Success",
                                    text: response.msg,
                                    icon: "success",
                                });
                                tableempat.draw();
                            },
                            error: function(res) {
                                $('#file_empat').val('')
                                $('#keterangan_empat').val('')
                                swal.fire({
                                    title: "Error",
                                    text: "Terjadi kesalahan",
                                    icon: "warning",
                                    timer: 2000,
                                });
                            }
                        });
                    }
                } else {
                    swal.fire({
                        title: "Peringatan",
                        text: "Silahkan pilih file dan keterangan harus isi.",
                        icon: "warning",
                        timer: 2000,
                    });

                }

            })

            $('#nop').keydown(function(e) {
                if (e.keyCode == 13) {
                    let nop = $('#nop').val()
                    $('#hasil').html('')
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan_nop') }}",
                        method: 'GET',
                        data: {
                            nop: nop
                        },
                        success: function(res) {
                            $('#hasil').html(res)
                        },
                        errorr: function(e) {
                            $('#hasil').html('')
                            Swal.fire({
                                icon: 'error',
                                title: 'Opps',
                                text: "Terjadi kesalahan pada sistem!",
                                allowOutsideClick: false,
                            })
                        }
                    })
                }
            })

            tableHasil = $('#table').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('pembayaran.blokir.index') }}",
                    data: function(d) {
                        d.jenis = 'KP'
                        d.search = $('#search').val()
                    }
                },
                columns: [{
                    data: null,
                    class: 'text-center',
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    searchable: false
                }, {
                    data: 'no_transaksi',
                    name: 'no_transaksi',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'nop',
                    name: 'nop',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'thn_pajak_sppt',
                    name: 'thn_pajak_sppt',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'keterangan',
                    name: 'keterangan',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return data + "<br><small class='text-success'>" + row
                            .verifikasi_keterangan + "</small>"
                    }

                }, {
                    data: 'nama',
                    name: 'nama',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return data + "<br><small class='text-success'>" + row.created_at +
                            "</small>"
                    }

                }, {
                    data: 'verifikator',
                    name: 'verifikator',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });


            let tableempat = $('#table_empat').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('pembayaran.blokir.index') }}",
                    data: function(d) {
                        d.jenis = 'IBIV'
                        d.search = $('#search_empat').val()
                    }
                },
                columns: [{
                    data: null,
                    class: 'text-center',
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    searchable: false
                }, {
                    data: 'no_transaksi',
                    name: 'no_transaksi',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'nop',
                    name: 'nop',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'thn_pajak_sppt',
                    name: 'thn_pajak_sppt',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'keterangan',
                    name: 'keterangan',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return data + "<br><small class='text-success'>" + row
                            .verifikasi_keterangan + "</small>"
                    }

                }, {
                    data: 'nama',
                    name: 'nama',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return data + "<br><small class='text-success'>" + row.created_at +
                            "</small>"
                    }

                }, {
                    data: 'verifikator',
                    name: 'verifikator',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });

            $('#cari').on('click', function(e) {
                e.preventDefault()
                tableHasil.draw();
            })



            $('#cari_empat').on('click', function(e) {
                e.preventDefault()
                tableempat.draw();
            })
        })
    </script>
@endsection
