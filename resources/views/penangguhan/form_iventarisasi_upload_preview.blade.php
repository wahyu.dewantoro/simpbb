@extends('layouts.app')
@section('css')
    <style>
        /*   .has-search .form-control {
                                                                                            padding-left: 2.375rem;
                                                                                        }

                                                                                        .has-search .form-control-feedback {
                                                                                            position: absolute;
                                                                                            z-index: 2;
                                                                                            display: block;
                                                                                            width: 2.375rem;
                                                                                            height: 2.375rem;
                                                                                            line-height: 2.375rem;
                                                                                            text-align: center;
                                                                                            pointer-events: none;
                                                                                            color: #aaa;
                                                                                        } */

        .dataTables_filter,
        .dataTables_length {
            ater display: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Upload Iventarisasi / Blokir</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-flat btn-primary btn-sm"
                            href="{{ url('/pendataan/penangguhan-upload-iventarisasi') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <form action="{{ url('pendataan/penangguhan-upload-iventarisasi-store') }}" method="POST"
                    enctype="multipart/form-data" class="form-horizontal" id="formbynopblokir">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Jenis</label>
                                    <div class="col-sm-8">
                                        @php
                                            $data = KategoriIventarisasi();
                                            $jenis = $data[$jns_koreksi];
                                        @endphp
                                        <input type="hidden" name="jns_koreksi" value="{{ $jns_koreksi }}">
                                        <input type="text" value="{{ $jenis }}" id="jns_koreksi"
                                            class="form-control form-control-sm" readonly>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal / No Surat</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="tgl_surat" class="form-control form-control-sm"
                                            value="{{ $tgl_surat }}" placeholder="Tanggal " readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" name="no_surat" class="form-control form-control-sm "
                                            placeholder="Nomor surat " readonly value="{{ $no_surat }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-sm table-bordered text-sm">
                                    <thead>
                                        <tr>
                                            <td>No</td>
                                            <td>NOP</td>
                                            <td>Wajib Pajak</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($objek as $row)
                                            <input type="hidden" name="kd_propinsi[]" value="{{ $row->kd_propinsi }}">
                                            <input type="hidden" name="kd_dati2[]" value="{{ $row->kd_dati2 }}">
                                            <input type="hidden" name="kd_kecamatan[]" value="{{ $row->kd_kecamatan }}">
                                            <input type="hidden" name="kd_kelurahan[]" value="{{ $row->kd_kelurahan }}">
                                            <input type="hidden" name="kd_blok[]" value="{{ $row->kd_blok }}">
                                            <input type="hidden" name="no_urut[]" value="{{ $row->no_urut }}">
                                            <input type="hidden" name="kd_jns_op[]" value="{{ $row->kd_jns_op }}">

                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>
                                                    {{ $row->kd_propinsi .
                                                        '.' .
                                                        $row->kd_dati2 .
                                                        '.' .
                                                        $row->kd_kecamatan .
                                                        '.' .
                                                        $row->kd_kelurahan .
                                                        '.' .
                                                        $row->kd_blok .
                                                        '.' .
                                                        $row->no_urut .
                                                        '.' .
                                                        $row->kd_jns_op }}
                                                </td>
                                                <td>{{ $row->nm_wp }}</td>
                                            </tr>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button type="submit" id="submit_tiga" class="btn btn-primary float-right"><i
                                    class="far fa-save"></i>
                                Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });


            $('.nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });


            $('#nop').on('keyup', function(e) {

                // jn_koreksi_blokir


                var a = $('#nop').val()
                var b = a.replace(/[^\d]/g, "");
                var c = "";
                var panjang = b.length;
                $('#nop_penyesuaian').html('')
                if (panjang == '18') {
                    $('#tunggakan').html('')
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan_nop') }}",
                        // method: 'GET',
                        data: {
                            nop: b
                        },
                        success: function(res) {
                            $('#tunggakan').html(res)
                        },
                        errorr: function(e) {
                            $('#tunggakan').html('')
                            Swal.fire({
                                icon: 'error',
                                title: 'Opps',
                                text: "Terjadi kesalahan pada sistem!",
                                allowOutsideClick: false,
                            })
                        }
                    })

                    var jns_koreksi_blokir = $('#jns_koreksi_blokir').val()
                    // nop_penyesuaian
                    if (jns_koreksi_blokir == '07') {
                        $.ajax({
                            url: "{{ url('pendataan/penangguhan-list-tunggakan') }}",
                            // method: 'GET',
                            data: {
                                nop: b
                            },
                            success: function(res) {
                                $('#nop_penyesuaian').html(res)
                            },
                            errorr: function(e) {
                                $('#nop_penyesuaian').html('')
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Opps',
                                    text: "Terjadi kesalahan pada sistem!",
                                    allowOutsideClick: false,
                                })
                            }
                        })
                    }
                }
            })


            $('#jns_koreksi_blokir').on('change', function(e) {
                var jns_koreksi_blokir = $('#jns_koreksi_blokir').val()
                var a = $('#nop').val()
                var b = a.replace(/[^\d]/g, "");
                var c = "";
                var panjang = b.length;
                if (panjang == '18' && jns_koreksi_blokir == '07') {
                    $('#nop').trigger('keyup')
                }
            })

        })
    </script>
@endsection
