<div class="form-group row">
    {{-- <label class="col-sm-4 col-form-label">Tunggakan</label> --}}
    <div class="col-sm-8 offset-4">
        {{-- <input type="text" name="nop" id="nop" class="form-control form-control-sm nop"
            placeholder="Nomor objek pajak" required> --}}
        <table class="table table-responsive table-bordered text-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tahun Piutang</th>
                    <th>Masa Pajak</th>
                    <th>PBB</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($data as $item)
                    <tr>
                        <td class="text-center" width="3px"><input type="checkbox" name="tahun[]"  value="{{ $item->thn_pajak_sppt }}"></td>
                        <td class="text-center">{{ $item->tahun_piutang }}</td>
                        <td class="text-center">{{ $item->thn_pajak_sppt }}</td>
                        <td class="text-right">{{ angka($item->pbb) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
