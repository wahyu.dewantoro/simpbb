<div class="card">
    <div class="card-header">
        <h5 class="card-title">Preview data objek</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-borderless table-sm">
                    <tbody>
                        <tr>
                            <td width="120px">NOP</td>
                            <td width="1px">:</td>
                            <td>{{ $objek->kd_propinsi . '.' . $objek->kd_dati2 . '.' . $objek->kd_kecamatan . '.' . $objek->kd_kelurahan . '.' . $objek->kd_blok . '-' . $objek->no_urut . '.' . $objek->kd_jns_op }}<br>
                                <small class="text-danger">{{ jenisBumi($objek->jns_bumi) }}</small>
                            </td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>
                                {{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }}<br>
                                @if ($objek->rt_op)
                                    RT {{ $objek->rt_op }}
                                @endif
                                @if ($objek->rw_op)
                                    RW {{ $objek->rw_op }}
                                @endif
                                <br>
                                {{ $kelurahan }} , {{ $kecamatan }}
                            </td>
                        </tr>
                        <tr>
                            <td>Luas Bumi</td>
                            <td>:</td>
                            <td>{{ $objek->total_luas_bumi }}</td>
                        </tr>
                        <tr>
                            <td>Luas Bangunan</td>
                            <td>:</td>
                            <td>{{ $objek->total_luas_bng }}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-borderless table-sm">
                    <tbody>
                        <tr>
                            <td width="120px">Subjek Pajak</td>
                            <td width="1px">:</td>
                            <td>{{ $objek->nm_wp }} </td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>
                                {{ $objek->jalan_wp }} {{ $objek->blok_kav_no_wp }}<br>
                                @if ($objek->rt_wp)
                                    RT {{ $objek->rt_wp }}
                                @endif
                                @if ($objek->rw_wp)
                                    RT {{ $objek->rw_wp }}
                                @endif
                                <br>
                                {{ $objek->kelurahan_wp }} <br>
                                {{ $objek->kota_wp }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                {{-- <table class="table table-striped table-sm ">
            <thead>
                <tr style="vertical-align: top">
                    <th style="font-weight: bold; text-align:center">Tahun</th>
                    <th style="font-weight: bold; text-align:center">PBB</th>
                    <th style="font-weight: bold; text-align:center">Tanggal Bayar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pembayaran as $item)
                    @php
                        $item = (object) $item;
                    @endphp
                    <tr style="vertical-align: top">
                        <td class="fw-semibold text-gray-900 text-center">{{ $item->thn_pajak_sppt }}</td>
                        <td class="fw-semibold text-gray-900 text-right">
                            {{ $item->pbb_akhir > 0 ? angka($item->pbb_akhir) : 'NIHIL' }}</td>
                        <td class="fw-semibold text-gray-900 text-center">
                            <?= $item->no_koreksi != '' ? "<span class='text-danger'>" . $item->no_koreksi . '</span>' : tglIndo($item->tgl_bayar) ?>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table> --}}
            </div>
        </div>



    </div>
</div>
