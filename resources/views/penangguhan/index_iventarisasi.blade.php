@extends('layouts.app')
@section('css')
    <style>
        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Iventarisasi</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-body p-1">
                            <div class="row">
                                <div class="col-md-2">
                                    <select name="tahun" id="tahun" class="form-control form-control-sm pencarian">
                                        <option value="">Tahun Pajak</option>
                                        @for ($i = date('Y'); $i >= 2003; $i--)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="tahun_proses" id="tahun_proses"
                                        class="form-control form-control-sm pencarian">
                                        <option value="">Tahun Proses</option>
                                        @for ($i = date('Y'); $i >= 2003; $i--)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="jns_koreksi" id="jn_koreksi_blokir"
                                        class="form-control form-control-sm pencarian" required>
                                        <option value="">Jenis</option>
                                        <option value="00">00 - Koreksi Piutang</option>
                                        @foreach (KategoriIventarisasi() as $i => $item)
                                            <option value="{{ $i }}">{{ $i }} -
                                                {{ $item }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input class="form-control form-control-sm border-right-0 " name="search"
                                            id="search_empat" placeholder="Pencarian . . .">
                                        <span class="input-group-append bg-white border-left-0">
                                            <span class="input-group-text bg-transparent">
                                                <i class="fas fa-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button data-id="csv" class=" cetak btn btn-sm btn-info btn-flat float-right"><i
                                            class="far fa-file-alt"></i> CSV</button>
                                    <button data-id="excel" class="cetak btn btn-sm btn-success btn-flat float-right"><i
                                            class="far fa-file-alt"></i> Excel</button>
                                </div>
                            </div>
                            <table id="table_empat"
                                class="table table-bordered table-striped text-sm table-sm display nowrap"
                                style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No Transaksi</th>
                                        <th>NOP</th>
                                        <th>Surat</th>
                                        {{-- <th></th> --}}
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });


            let tableempat = $('#table_empat').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('pembayaran.blokir.index') }}",
                    data: function(d) {
                        d.jenis = '2'
                        d.search = $('#search_empat').val()
                        d.kd_kategori = $('#jn_koreksi_blokir').val()
                        d.tahun = $('#tahun').val()
                        d.tahun_proses = $('#tahun_proses').val()
                    }
                },
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        searchable: false
                    }, {
                        data: 'no_transaksi',
                        name: 'no_transaksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            let ket = row.nm_kategori;
                            return data + '<br>' + ket
                        }
                    }, {
                        data: 'nop',
                        name: 'nop',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            return data + "<br>" + row.thn_pajak_sppt
                        }
                    },

                    {
                        data: 'surat',
                        name: 'surat',
                        orderable: false,
                        searchable: false
                    }
                    /* , {
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false
                    } */
                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });

            function pencarian() {
                tableempat.draw();
            }

            $('.pencarian').on('change', function(e) {
                e.preventDefault()
                pencarian()
            })

            $('#search_empat').keydown(function(e) {
                if (e.keyCode == 13) {
                    pencarian()
                }
            });

            $('.cetak').on("click", function(e) {
                e.preventDefault()
                var jenis = $(this).data('id')
                var search = $('#search_empat').val()
                var kd_kategori = $('#jn_koreksi_blokir').val()
                var tahun = $('#tahun').val()
                var tahun_proses = $('#tahun_proses').val()

                let url = "{{ url('pendataan/penangguhaniventarisasiexport') }}?jenis=" + jenis +
                    "&search=" + search + "&kd_kategori=" + kd_kategori + "&tahun=" + tahun +
                    '&tahun_proses=' + tahun_proses
                window.open(url)
            })


        });
    </script>
@endsection
