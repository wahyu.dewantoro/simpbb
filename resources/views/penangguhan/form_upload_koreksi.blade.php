@extends('layouts.app')
@section('css')
    <style>
        .has-search .form-control {
            padding-left: 2.375rem;
        }

        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
        }

        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Koreksi</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-flat btn-primary btn-sm" href="{{ url('pendataan/penangguhan') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">

                    <form action="{{ url('pendataan/penangguhan-preview-koreksi') }}" method="post" id="formbynop"
                        enctype="multipart/form-data">
                        @method('post')
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="jenis" id="jenis" value="0">
                                <input type="hidden" name="jns_koreksi" value="3">
                                <div class="form-group">
                                    <label for="no_surat">Nomor Surat</label>
                                    <input required type="text" name="no_surat" id="no_surat"
                                        class="form-control form-control-sm" placeholder="Nomor surat">
                                </div>
                                <div class="form-group">
                                    <label for="tgl_surat">Tanggal Surat</label>
                                    <input required type="text" class="form-control form-control-sm tanggal"
                                        name="tgl_surat" id="tgl_surat">
                                </div>
                                <div class="form-group">
                                    <label for="">File</label>
                                    <input type="file" name="file" class="form-control form-control-sm " required>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="col-md-12">
                                    <label for="keterangan">Keterangan</label>
                                    <textarea class="form-control" name="keterangan" id="keterangan" rows="3"></textarea>
                                </div>



                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="float-right">
                                    <button type="submit" id="submit" class="btn btn-primary"><i
                                            class="far fa-save"></i>
                                        Preview</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });


            $('.nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            function cekNopKp() {
                var nop = $('#nop_kp').val()
                var a = nop.toString();
                var b = a.replace(/[^\d]/g, "");
                var thn_pajak_sppt = $('#thn_pajak_sppt').val()
                if (b.length == 18 && thn_pajak_sppt.length == 4) {
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan-cek-nop-kp') }}",
                        method: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "nop": b,
                            "thn_pajak_sppt": thn_pajak_sppt
                        },
                        success: function(res) {
                            console.log(res)
                            if (res.status == '1') {
                                $('#nilai_koreksi').val(res.pbb)
                            } else {
                                // masalah
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Peringatan',
                                    text: res.keterangan,
                                })
                                $('#nilai_koreksi').val('')
                                $('#nop_kp').val('')
                                $('#thn_pajak_sppt').val('')
                            }

                        },
                        error: function(er) {
                            // $.LoadingOverlay("hide");
                        }
                    })
                }
            }

            $('#nop_kp,#thn_pajak_sppt').on('keyup change', function(e) {
                cekNopKp()
            })
        })
    </script>
@endsection
