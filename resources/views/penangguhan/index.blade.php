@extends('layouts.app')
@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css"> --}}
    <style>
        .has-search .form-control {
            padding-left: 2.375rem;
        }

        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
        }
    </style>
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Penangguhan NOP</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group has-search">
                                        <span class="fa fa-search form-control-feedback"></span>
                                        <input type="text" class="form-control" placeholder="Masukan NOP" id="nop">
                                    </div>

                                </div>
                            </div>
                            <div id="hasil"></div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });


            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $('#nop').keydown(function(e) {
                if (e.keyCode == 13) {
                    let nop = $('#nop').val()
                    $('#hasil').html('')
                    $.ajax({
                        url: "{{ url('pendataan/penangguhan_nop') }}",
                        method: 'GET',
                        data: {
                            nop:nop
                        },
                        success: function(res) {
                            $('#hasil').html(res)
                        },
                        errorr: function(e) {
                            $('#hasil').html('')
                            Swal.fire({
                                icon: 'error',
                                title: 'Opps',
                                text: "Terjadi kesalahan pada sistem!",
                                allowOutsideClick: false,
                            })
                        }
                    })
                }
            })
        });
    </script>
@endsection
