<table>
    <thead>
        <tr>
            <th>KD_PROPINSI</th>
            <th>KD_DATI2</th>
            <th>KD_KECAMATAN</th>
            <th>KD_KELURAHAN</th>
            <th>KD_BLOK</th>
            <th>NO_URUT</th>
            <th>KD_JNS_OP</th>
            <th>TAHUN_PAJAK</th>
            <th>TAHUN_TERBIT</th>
            <th>NM_WP_SPPT</th>
            <th>PBB</th>
            <th>STIMULUS</th>
            <th>KOREKSI</th>
            <th>BAYAR</th>
            <th>SALDO_SISA</th>
        </tr>
    </thead>
    @foreach ($data as $item)
        <tr>
            <td>{{ $item->kd_propinsi }}</td>
            <td>{{ $item->kd_dati2 }}</td>
            <td>{{ $item->kd_kecamatan }}</td>
            <td>{{ $item->kd_kelurahan }}</td>
            <td>{{ $item->kd_blok }}</td>
            <td>{{ $item->no_urut }}</td>
            <td>{{ $item->kd_jns_op }}</td>
            <td>{{ $item->tahun_pajak }}</td>
            <td>{{ $item->tahun_terbit }}</td>
            <td>{{ $item->nm_wp_sppt }}</td>
            <td>{{ $item->pbb }}</td>
            <td>{{ $item->potongan }}</td>
            <td>{{ $item->koreksi }}</td>
            <td>{{ $item->bayar }}</td>
            <td>{{ $item->saldo_sisa }}</td>
        </tr>
    @endforeach

</table>
