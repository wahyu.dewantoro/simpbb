@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Pegawai Struktural</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body p-2">
                            <form action="{{ $data['action'] }}" method="POST">
                                @csrf
                                @method($data['method'])
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">KODE</label>
                                            <div class="row">
                                                <div class="col-4">
                                                    <input type="text" name="kode" id="kode"
                                                        value="{{ old('kode') ?? ($data['struktural']->kode ?? '') }}"
                                                        class="form-control {{ $errors->has('kode') ? 'is-invalid' : '' }}">
                                                        <span class="errorr invalid-feedback">{{ $errors->first('kode') }}</span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="jabatan">Jabatan</label>
                                            <div class="row">
                                                <div class="col-10">
                                                    <input type="text" name="jabatan" id="jabatan"
                                                        value="{{ old('jabatan') ?? ($data['struktural']->jabatan ?? '') }}"
                                                        class="form-control {{ $errors->has('jabatan') ? 'is-invalid' : '' }}">
                                                    <span
                                                        class="errorr invalid-feedback">{{ $errors->first('nama') }}</span>
                                                </div>
                                                <div class="col-2">
                                                    @php
                                                        $sel = old('is_plt') ?? ($data['struktural']->is_plt ?? '');
                                                    @endphp
                                                    <div class="form-check">

                                                        <input type="checkbox" @if ($sel == 1) checked @endif
                                                            class="form-check-input" name="is_plt" value="1" id="is_plt">
                                                        <label class="form-check-label" for="is_plt">PLT

                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Nama</label>
                                            <input type="text" id="nama" name="nama"
                                                value="{{ old('nama') ?? ($data['struktural']->nama ?? '') }}"
                                                class="form-control {{ $errors->has('nama') ? 'is-invalid' : '' }}">
                                            <span class="error invalid-feedback">{{ $errors->first('nama') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="nip">NIP</label>
                                            <input type="text" id="nip" name="nip"
                                                value="{{ old('nip') ?? ($data['struktural']->nip ?? '') }}"
                                                class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}">
                                            <span class="error invalid-feedback">{{ $errors->first('nip') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="nip">NIP</label>
                                            <input type="text" id="nip" name="nip"
                                                value="{{ old('nip') ?? ($data['struktural']->nip ?? '') }}"
                                                class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}">
                                            <span class="error invalid-feedback">{{ $errors->first('nip') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="nip">Pangkat</label>
                                            <input type="text" id="pangkat" name="pangkat"
                                                value="{{ old('pangkat') ?? ($data['struktural']->pangkat ?? '') }}"
                                                class="form-control {{ $errors->has('pangkat') ? 'is-invalid' : '' }}">
                                            <span class="error invalid-feedback">{{ $errors->first('pangkat') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-right">
                                                <button class="btn btn-sm btn-info"> <i class="fas fa-save"></i>
                                                    Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
