@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pegawai Struktural</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        {{-- @can('add_users') --}}
                        <a href="{{ url('refrensi/struktural/create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-user-plus"></i> Create
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="card card-solid">
            <div class="card-body pb-0">
                <div class="row d-flex align-items-stretch">
                    @foreach ($pegawai as $row)
                        <div class="col-12 col-sm-6 col-md-3 d-flex align-items-stretch">
                            <div class="card bg-light">
                                <div class="card-header text-muted border-bottom-0">

                                    {{ $row->nama }}
                                </div>
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-7">
                                            
                                            <p> {{ $row->pangkat }} </p>
                                            <p> <b>NIP: </b> {{ $row->nip }} </p>
                                            <p class="text-muted">
                                                <b>{{ $row->is_plt != '' ? 'Plt' : '' }} {{ $row->jabatan }}</b>

                                            </p>

                                        </div>
                                        <div class="col-5 text-center">
                                           
                                            <i class="fas fa-user-tie" style="font-size: 48px;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        <a href="{{ route('refrensi.struktural.edit', $row->id) }}"
                                            class="btn btn-sm btn-flat btn-outline-success">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('refrensi.struktural.destroy', $row->id) }}"
                                            onclick="
                                            var result = confirm('Are you sure you want to delete this record?');
                                            if(result){
                                                event.preventDefault();
                                                document.getElementById('delete-form-{{ $row->id }}').submit();
                                            }"
                                            title="Delete Jabatan" class="btn btn-sm btn-flat btn-outline-danger">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        <form method="POST" id="delete-form-{{ $row->id }}"
                                            action="{{ route('refrensi.struktural.destroy', [$row->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
       
    </section>
@endsection
