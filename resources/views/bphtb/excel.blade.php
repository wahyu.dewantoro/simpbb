<html>

<body>
    <table>
        <thead>
            <tr>
                <td colspan="8" style="text-align: center">
                    <b>Ajuan BPHTB {{ $tahun }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center">
                    <b>Kecamatan {{ $kecamatan->nm_kecamatan }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center">
                    <small> Ditarik pada {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small>
                </td>
            </tr>
        </thead>
    </table>
    <table class="table table-sm   table-bordered table-hover">
        <thead>
            <tr>

                <th colspan="6" style="text-align:center; font-weight:bold;">PBB</th>
                <th colspan="5" style="text-align:center; font-weight:bold;">BPHTB</th>
            </tr>
            <tr>
                <th style="text-align:center; font-weight:bold;">NOP</th>
                <th style="text-align:center; font-weight:bold;">Alamat</th>
                <th style="text-align:center; font-weight:bold;">Nama SPPT</th>
                <th style="text-align:center; font-weight:bold;">Alamat WP SPPT</th>
                <th style="text-align:center; font-weight:bold;">Bumi M<sup>2</sup></th>
                <th style="text-align:center; font-weight:bold;">Bangunan M<sup>2</sup></th>
                <th style="text-align:center; font-weight:bold;">Permohonan</th>
                <th style="text-align:center; font-weight:bold;">Nama Pemohon</th>
                <th style="text-align:center; font-weight:bold;">Alamat Pemohon</th>
                <th style="text-align:center; font-weight:bold;">Bumi M<sup>2</sup></th>
                <th style="text-align:center; font-weight:bold;">Bangunan M<sup>2</sup></th>
            </tr>
        </thead>
        <tbody>
            
                @foreach ($data as $k => $item)
                    <tr>
                        
                            <td> {{ formatnop($item->nop) }}</td>
                            <td> {{ $item->nama_kel_op }} - {{ $item->nama_kec_op }}
                            </td>
                            <td> {{ $item->nm_wp_sppt }}</td>
                            <td>

                                {{ $item->jln_wp_sppt }}
                                {{ $item->blok_kav_no_wp_sppt }}
                                {{ $item->kelurahan_wp_sppt }}
                                {{ $item->kota_wp_sppt }}

                            </td>
                            <td style="text-align:center; font-weight:bold;">

                                {{ number_format($item->luas_bumi_sppt, 2, ',', '.') }}

                            </td>
                            <td style="text-align:center; font-weight:bold;">

                                {{ number_format($item->luas_bng_sppt, 2, ',', '.') }}

                            </td>
                        
                        <td>{{ tglIndo($item->tanggal_permohonan) }}</td>
                        <td>{{$item->nama_wp }}</td>
                        <td>{{$item->nama_kel }} {{ $item->nama_kec }} {{  $item->nama_kab }}</td>
                        <td style="text-align:center; font-weight:bold;">{{ number_format($item->luas_tanah, 2, ',', '.') }}
                        </td>
                        <td style="text-align:center; font-weight:bold;">
                            {{ number_format($item->luas_bangunan, 2, ',', '.') }}</td>

                    </tr>
                
                
            @endforeach
        </tbody>
    </table>
</body>

</html>
