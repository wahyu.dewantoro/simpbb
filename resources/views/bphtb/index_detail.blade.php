@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data BPHTB</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ $kecamatan->nm_kecamatan }}
                            </h3>
                            <div class="card-tools">
                                <a href="{{ url('informasi/bphtb-excel', [$kd_kecamatan, $tahun]) }}"
                                    class="btn btn-success btn-flat btn-sm"> <i class="far fa-file-excel"></i> Excel</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-sm   table-bordered table-hover">
                                <thead>
                                    <tr>
                                        {{-- <th rowspan="2" width="10px">No</th> --}}
                                        <th colspan="4" class="text-center">PBB</th>
                                        <th colspan="3" class="text-center">BPHTB</th>
                                        <th rowspan="2" width="10px">Detail</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">NOP</th>
                                        <th class="text-center">Alamat Objek</th>

                                        <th class="text-center">Bumi M<sup>2</sup></th>
                                        <th class="text-center">Bangunan M<sup>2</sup></th>
                                        <th class="text-center">Permohonan</th>
                                        <th class="text-center">Bumi M<sup>2</sup></th>
                                        <th class="text-center">Bangunan M<sup>2</sup></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $tmp = '';
                                        
                                        $arr = [];
                                        foreach ($data as $row) {
                                            $arr[$row->nop][] = $row;
                                        }
                                    @endphp

                                    {{-- foreach ($arr as $key => $val) --}}
                                    @foreach ($arr as $key => $val)
                                        @foreach ($val as $k => $item)
                                            <tr>
                                                @if ($k == 0)
                                                    <td rowspan="{{ count($val) }}"> {{ formatnop($item->nop) }} </td>
                                                    <td rowspan="{{ count($val) }}"> {{ $item->nama_kel_op }} -
                                                        {{ $item->nama_kec_op }} </td>
                                                    <td rowspan="{{ count($val) }}" class="text-center">
                                                        {{ number_format($item->luas_bumi_sppt, 2, ',', '.') }}
                                                    </td>
                                                    <td rowspan="{{ count($val) }}" class="text-center">
                                                        {{ number_format($item->luas_bng_sppt, 2, ',', '.') }}
                                                    </td>
                                                @endif
                                                <td class="text-left"> {{ tglIndo($item->tanggal_permohonan) }}</td>
                                                <td class="text-center">
                                                    {{ number_format($item->luas_tanah, 2, ',', '.') }}
                                                </td>
                                                <td class="text-center">
                                                    {{ number_format($item->luas_bangunan, 2, ',', '.') }}</td>
                                                @if ($k == 0)
                                                    <td class="text-center" rowspan="{{ count($val) }}">
                                                        <a data-nop='{{ $item->nop }}' class="text-success detail"
                                                            href="#">
                                                            <i class="fas fa-search-location"></i> </a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer p-0">
                            <div class="row">
                                <div class="col-6">
                                    [Halaman : {{ $data->currentPage() }} ]
                                </div>
                                <div class="col-6">
                                    <div class="float-right">
                                        {{ $data->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Informasi Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="konten-detail">
                    <p>One fine body&hellip;</p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection
@section('script')
    <script>
        $('.detail').on('click', function(e) {
            e.preventDefault()
            var nop = $(this).data('nop');
            Swal.fire({
                title: 'Sedang memproses ...',
                html: '<span class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></span>',
                showConfirmButton: false,
                allowOutsideClick: false,
            });
            $.ajax({
                url: "{{ url('informasi/bphtb-detail') }}",
                data: {
                    'nop': nop
                },
                success: function(res) {
                    Swal.close()
                    $('#konten-detail').html(res)
                    $('#modal-lg').modal('show')
                },
                error: function(err) {
                    Swal.fire({
                        title: 'Terjadi kesalahan jaringan',
                        html: '<span class="fa-3x pd-5"><i class="fa fa-close"></i></span>',
                        /* showConfirmButton: false,
                        allowOutsideClick: false, */
                    });
                }
            })

        })
    </script>
@endsection
