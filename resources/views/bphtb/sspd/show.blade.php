@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail SSPD BPHTB</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a class="btn btn-sm btn-info btn-flat" href="{{ route('bphtb.sspd.index') }}"><i class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-sm table-borderless text-sm">
                                <tbody>
                                    <tr>
                                        <td width="200px">No Registrasi</td>
                                        <td>:</td>
                                        <td>{{ $data['no_registrasi'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>NOP</td>
                                        <td>:</td>
                                        <td>{{ formatnop($data['nop']) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Transaksi (BPHTB)</td>
                                        <td>:</td>
                                        <td>{{ $data['jenis_perolehan'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Bayar</td>
                                        <td>:</td>
                                        <td>{{ $data['tanggal_bayar'] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-sm table-borderless text-sm">
                                <tbody>
                                    <tr>
                                        <td width="200px">Penjual / Pemberi Hak</td>
                                        <td>:</td>
                                        <td>{{ $data['penjual'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pembeli / Penerima Hak</td>
                                        <td>:</td>
                                        <td>
                                            <ul class="list-unstyled">
                                                @foreach ($data['pembeli'] as $pembeli)
                                                    <li> {{ $pembeli['nama'] }} ( {{ $pembeli['nik'] }} )<br>
                                                        {{ $pembeli['alamat'] }} RT {{ $pembeli['rt'] }}
                                                        RW{{ $pembeli['rw'] }} <br>
                                                        {{ $pembeli['desa'] }} - {{ $pembeli['kecamatan'] }}<br>
                                                        {{ $pembeli['kabupaten'] }}- {{ $pembeli['propinsi'] }}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-sm table-bordered text-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center fw-bold">Objek</th>
                                        <th class="text-center fw-bold">Luas</th>
                                        <th class="text-center fw-bold">NPOP/M<sup>2</sup></th>
                                        <th class="text-center fw-bold">Jumlah </th>
                                    </tr>
                                </thead>
                                <tbody class="table-group-divider">
                                    <tr>
                                        <td>Bumi</td>
                                        <td class="text-center">{{ $data['luas_bumi'] }}</td>
                                        <td class="text-right">{{ angka($data['npop_bumi_meter']) }}</td>
                                        <td class="text-right">{{ angka($data['npop_bumi']) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Bangunan</td>
                                        <td class="text-center">{{ $data['luas_bng'] }}</td>
                                        <td class="text-right">{{ angka($data['npop_bng_meter']) }}</td>
                                        <td class="text-right">{{ angka($data['npop_bng']) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="text-right fw-bold">{{ angka($data['npop_bng'] + $data['npop_bumi']) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Berkas Terlampir</p>
                        </div>
                        @foreach ($data['berkas'] as $berkas)
                            <div class="col-lg-4">
                                <a href="#" class="openWindow" data-url="{{ $berkas['url'] }}" target="_blank"><i
                                        class="fas fa-file-pdf"></i> {{ $berkas['dokumen'] }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.openWindow').on('click', function(e) {
                e.preventDefault()
                const url = $(this).data('url'); // Ambil URL dari atribut data-url
                // window.open(url, '_blank', 'width=1000,height=800');
                openTab(url)
            });
        })
    </script>
@endsection
