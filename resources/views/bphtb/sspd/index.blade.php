@extends('layouts.app')
@section('style')
    <style>
        #loading-indicator {
            background: rgba(255, 255, 255, 0.7);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            z-index: 10;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data SSPD BPHTB</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        {{-- <div id="reportrange"
                            style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div id="loading-indicator"
                style="display: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 10;">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <input type="hidden" id="tanggal_filter">
                        <table id="table-sspd" class="table table-sm text-sm table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 60px" rowspan="2">No</th>
                                    <th class="text-center" style="width: 100px" rowspan="2">No Registrasi</th>
                                    <th class="text-center" rowspan="2" style="width: 100px">NOP</th>
                                    <th class="text-center" colspan="2">Bumi</th>
                                    <th class="text-center" colspan="2">Bangunan</th>
                                    <th class="text-center" rowspan="2"></th>
                                </tr>
                                <tr>
                                    <th class="text-center" style="width: 150px">Luas</th>
                                    <th class="text-center" style="width: 150px">NPOP/M<sup>2</sup></th>
                                    <th class="text-center" style="width: 150px">Luas</th>
                                    <th class="text-center" style="width: 150px">NPOP/M<sup>2</sup></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {




            var table = $('#table-sspd').DataTable({
                ordering: false,
                dom: '<"d-flex justify-content-between"f<"#daterange-container">>t<"d-flex justify-content-between"lpi>',
                ajax: {
                    url: '{{ route('bphtb.sspd.index') }}',
                    dataSrc: 'data',
                    data: function(d) {
                        // Tambahkan start_date dan end_date ke parameter AJAX
                        const daterange = $('#tanggal_filter').val();
                        if (daterange) {
                            const dates = daterange.split('#');
                            d.start_date = dates[0]; // Tanggal mulai
                            d.end_date = dates[1]; // Tanggal akhir
                        }
                    },
                    beforeSend: function() {
                        $('#loading-indicator').show(); // Tampilkan loading indikator
                    },
                    complete: function() {
                        $('#loading-indicator').hide(); // Sembunyikan loading indikator
                    }
                },
                columns: [{
                        data: null, // Kolom untuk nomor urut
                        class: 'text-center',
                        render: function(data, type, row, meta) {
                            const nomor = meta.row + 1; // Nomor urut dimulai dari 1

                            return nomor;
                            // return formatRupiah(nomor)
                        }
                    },
                    {
                        data: 'no_registrasi',
                    },
                    {
                        data: 'nop',
                        render: function(data, type, row) {
                            return formatnop(data);
                        }
                    },
                    {
                        data: 'luas_bumi',
                        class: 'text-center'
                    },
                    {
                        data: 'npop_bumi_meter',
                        class: 'text-right',
                        render: function(data, type, row) {
                            return formatRupiah(data)
                        }
                    },
                    {
                        data: 'luas_bng',
                        class: 'text-center',

                    },
                    {
                        data: 'npop_bng_meter',
                        class: 'text-right',
                        render: function(data, type, row) {
                            return formatRupiah(data)
                        }
                    },

                    {
                        data: 'no_registrasi',
                        class: 'text-center',
                        render: function(data, type, row) {
                            let url = "{{ url('sspd-bphtb') }}/" + data
                            return `<a href="${url}"><i class="fas fa-binoculars"></i></a>`
                        }
                    }
                ]
            });

            $('#daterange-container').html(`<div id="reportrange"
                                                    style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto;">
                                                    <i class="fa fa-calendar"></i>&nbsp;
                                                    <span></span> <i class="fa fa-caret-down"></i>
                                                </div>
                                            `);


            var start = moment().subtract(29, 'days');
            var end = moment();

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                const tanggal_input = start.format('YYYY-MM-DD') + '#' + end.format('YYYY-MM-DD')
                $('#tanggal_filter').val(tanggal_input)
                table.ajax.reload(); // Reload tabel dengan parameter baru
            }

            // Hide spinner setelah DataTables selesai memuat data
            table.on('draw', function() {
                $('#loading-indicator').hide();
            });

            cb(start, end);

        });
    </script>
@endsection
