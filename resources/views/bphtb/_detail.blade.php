<div class="row">
    <div class="col-12">
        <b>Data PBB</b>
        <div class="row">
            <div class="col-md-6">
                <table class="table table-borderless table-sm">
                    <tr>
                        <td width="150px">NOP</td>
                        <td width="1px">:</td>
                        <td>{{ formatNop($sspd[0]->nop) }}</td>
                    </tr>
                    <tr>
                        <td>Alamat Objek</td>
                        <td>:</td>
                        <td>
                            {{ $sspd[0]->nama_kel_op }} - {{ $sspd[0]->nama_kec_op }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            <table>
                                <tbody>
                                    <tr>
                                        <td width="50%">L. Bumi :
                                            {{ number_format($sspd[0]->luas_bumi_sppt, 2, ',', '.') }}
                                            M<sup>2</sup></td>
                                        <td>L. Bng : {{ number_format($sspd[0]->luas_bng_sppt, 2, ',', '.') }}
                                            M<sup>2</sup></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
            <div class="col-md-6">
                <table class="table table-borderless table-sm">
                    <tr>
                        <td width="150px">Subjek Pajak</td>
                        <td width="1px">:</td>
                        <td>{{ $sspd[0]->nm_wp_sppt }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>
                            {{ $sspd[0]->jln_wp_sppt }}
                            {{ $sspd[0]->blok_kav_no_wp_sppt }}
                            {{ $sspd[0]->kelurahan_wp_sppt }}
                            {{ $sspd[0]->kota_wp_sppt }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <b>Riwayat BPHTB</b>
                <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="text-align:center; font-weight:bold;">Permohonan</th>
                            <th style="text-align:center; font-weight:bold;">Nama Pemohon</th>
                            <th style="text-align:center; font-weight:bold;">Alamat Pemohon</th>
                            <th style="text-align:center; font-weight:bold;">Bumi M<sup>2</sup></th>
                            <th style="text-align:center; font-weight:bold;">Bangunan M<sup>2</sup></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sspd as $item)
                            <tr>
                                <td>{{ tglIndo($item->tanggal_permohonan) }}</td>
                                <td>{{ $item->nama_wp }}</td>
                                <td>{{ $item->nama_kel }} {{ $item->nama_kec }} {{ $item->nama_kab }}</td>
                                <td>
                                    {{ number_format($item->luas_tanah, 2, ',', '.') }}
                                </td>
                                <td>
                                    {{ number_format($item->luas_bangunan, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
