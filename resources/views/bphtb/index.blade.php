@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data BPHTB</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Tahun Pajak {{ date('Y') }}</h3>
                            {{-- <div class="card-tools">
                                <a href="{{ url('inform/desalunas-pdf') }}" class="btn btn-warning btn-sm"> <i
                                        class="far fa-file-pdf"></i> PDF</a>

                            </div> --}}
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th width="10px">No</th>
                                        <th class="text-left">Kecamatan</th>
                                        <th class="text-center">Ajuan</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $index => $item)
                                        <tr>
                                            <td>{{ $data->firstItem() + $index }}</td>
                                            <td>{{ $item->nm_kecamatan }}</td>
                                            <td width="200px" align="center">
                                                {{ number_format($item->jumlah, 0, '', '.') }}</td>
                                            <td width="100px" class="text-right">
                                                <a href="{{ route('informasi.bphtb.show', [$item->kd_kecamatan, $tahun]) }}"
                                                    class="btn btn-xs btn-flat btn-outline-warning">
                                                    <i class="fas fa-search"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer p-0">
                            <div class="row">
                                <div class="col-6">
                                    [Halaman : {{ $data->currentPage() }} ]
                                </div>
                                <div class="col-6">
                                    <div class="float-right">
                                        {{ $data->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>

    </script>
@endsection
