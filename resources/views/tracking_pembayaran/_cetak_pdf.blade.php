<html>

<head>
    @include('layouts.style_pdf')
    <style>
        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            background-color: grey;
            color: white;
            text-align: center;
            line-height: 0.5cm;
        }

     

        .inti {
            border: 1px solid #C6C6C6;
            margin: auto;
            font-size: 0.9em;
        }

        .inti td {
            border-right: 1px solid #C6C6C6;
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
        }

        .inti th {
            border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;
        }

        #watermark {
            position: fixed;
            top: 50mm;
            width: 100%;
            height: 200px;
            opacity: 0.05;
            text-align: center;
            vertical-align: middle
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 11pt !important
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 11pt !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }
    </style>
</head>

<body>
    @php
        $nota = [];
        if (isset($tahun_nota)) {
            $nota = $tahun_nota;
        }
    @endphp
    <div id="watermark"><img src="{{ public_path('kabmalang.png') }}"></div>
    @include('layouts.kop_pdf')
    <p style="text-align: center"><strong>Riwayat Pembayaran</strong></p>
    <p>
        <strong> NOP : </strong>{{ $nop }}
    </p>
    {{-- <table class="inti" width="100%" cellpadding="3" cellspacing="0"> --}}
    <table class="table table-sm table-bordered table-hover">
        <thead>
            <tr>
                <th rowspan="2" style="vertical-align: middle; text-align: center">No</th>
                <th rowspan="2" style="vertical-align: middle; text-align: center">Tahun</th>
                <th rowspan="2" style="vertical-align: middle; text-align: center">Nama</th>
                <th rowspan="2" style="vertical-align: middle; text-align: center">PBB</th>
                <th colspan="4" style="vertical-align: middle; text-align: center">Pembayaran</th>
                <th rowspan="2" style="vertical-align: middle; text-align: center" width="200px">Keterangan</th>
            </tr>
            <tr>
                <th style="vertical-align: middle; text-align: center">Pokok</th>
                <th style="vertical-align: middle; text-align: center">Denda</th>
                <th style="vertical-align: middle; text-align: center">Total</th>
                <th style="vertical-align: middle; text-align: center" width="125px">Tanggal Bayar</th>
            </tr>

        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            {{-- @if (count([$data]) > 0) --}}
            @if (isset($data))
                @foreach ($data as $index => $row)
                    {{-- @if ($row->thn_pajak_sppt != '2024') --}}
                    <tr>
                        <td width="5px" align="center">{{ $no }}</td>
                        <td style="text-align: center">{{ $row->thn_pajak_sppt }}</td>
                        <td>{{ $row->nm_wp_sppt }}</td>
                        <td style="text-align: right">{{ angka(str_replace(',', '.', $row->pbb_akhir)) }}</td>
                        <td style="text-align: right">
                            {{ $row->tgl_bayar != null ? angka(str_replace(',', '.', $row->jumlah_bayar - $row->denda_bayar)) : angka($row->pbb_akhir) }}
                        </td>
                        <td style="text-align: right">
                            {{ $row->tgl_bayar != null ? angka(str_replace(',', '.', $row->denda_bayar)) : angka($row->denda) }}
                        </td>
                        <td style="text-align: right">
                            {{ $row->tgl_bayar != null ? angka(str_replace(',', '.', $row->jumlah_bayar)) : angka(((int) str_replace(',', '', $row->denda)) + ((int) str_replace(',', '', $row->pbb_akhir))) }}
                        </td>
                        <td>
                            @if ($row->tgl_bayar != '')
                                @php
                                    $et = explode(',', $row->tgl_bayar);
                                    $tgl = '';
                                    $ct = '';
                                    foreach ($et as $item) {
                                        if ($ct != $item) {
                                            $tgl .= tglIndo($item) . ', ';
                                        }
                                        $ct = $item;
                                    }
                                    echo substr($tgl, 0, -2);
                                @endphp
                            @endif

                        </td>
                        <td><small>
                                @php
                                    $ket = '';
                                    if ($row->status_pembayaran_sppt == '3' && $row->tgl_bayar == '') {
                                        $ket = 'blokir ';
                                    }

                                    // if ($row->no_koreksi != '' && $row->status_pembayaran_sppt == '3')
                                    if (($row->no_koreksi != '' && $row->status_pembayaran_sppt == '3') || $row->jns_koreksi == '3') {
                                        $ket .= '  ' . $row->no_koreksi;
                                    }

                                    // if ($row->tgl_surat != '' && $row->status_pembayaran_sppt == '3') {
                                    if (($row->no_koreksi != '' && $row->status_pembayaran_sppt == '3') || $row->jns_koreksi == '3') {
                                        $ket .= ' - ' . date('d/m/Y', strtotime($row->tgl_surat));
                                    }

                                @endphp
                                <?= $ket ?>
                            </small>
                        </td>


                    </tr>
                    {{-- @endif --}}
                    @php
                        $no++;
                    @endphp
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="10">Data tidak ditemukan</td>
                </tr>
            @endif
        </tbody>
    </table>


    <table width="100%">
        <tr>
            <td width="70%"></td>
            <td style="text-align: center">
                KEPANJEN, {{ tglIndo(date('Y-m-d')) }}<br><br>
                @if (Auth()->user())
                    @if (Auth()->user()->hasRole('Wajib Pajak') == false)
                        {{ Auth()->user()->nama }}
                    @endif
                @endif
            </td>
        </tr>
    </table>

    <footer>
        Copyright &copy; sipanji.id PBB P2 {{ date('Y') }}
    </footer>
</body>

</html>
