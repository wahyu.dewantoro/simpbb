@if (count([$data]) > 0)
    <a onclick="window.open('{{ url('informasi/riwayat-cetak') }}?nop={{ $nop }}', 
    'newwindow', 
    'width=700,height=1000'); 
       return false;"
        class="float-right text-success" href="{{ url('informasi/riwayat-cetak') }}?nop={{ $nop }}">[<i
            class="fas fa-print"></i> Cetak ]</a>
@endif

<table class="table table-sm table-bordered" id="table-data">
    <thead>
        <tr>
            <th rowspan="2" style="vertical-align: middle; text-align: center">No</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">NOP</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Tahun</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Nama</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">PBB</th>
            <th colspan="4" style="vertical-align: middle; text-align: center">Pembayaran</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Keterangan</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">E SPPT/SSPD</th>
        </tr>
        <tr>
            <th style="vertical-align: middle; text-align: center">Pokok</th>
            <th style="vertical-align: middle; text-align: center">Denda</th>
            <th style="vertical-align: middle; text-align: center">Total</th>
            <th style="vertical-align: middle; text-align: center">Tanggal Bayar</th>
        </tr>

    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @if (count([$data]) > 0)
            @foreach ($data as $index => $row)
                @php
                    $pp = $row->jumlah_bayar - $row->denda_bayar;

                    if ($pp > $row->pbb_akhir) {
                        $pp = $row->pbb_akhir;
                    }

                @endphp
                <tr>
                    <td width="5px" align="center">{{ $no }}</td>
                    <td>{{ $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op }}
                    </td>
                    <td style="text-align: center">{{ $row->thn_pajak_sppt }}</td>
                    <td>{{ $row->nm_wp_sppt }}</td>
                    <td style="text-align: right">{{ angka($row->pbb_akhir) }}</td>
                    <td style="text-align: right">
                        {{ $row->tgl_bayar != null ? angka($pp) : angka($row->pbb_akhir) }}
                    </td>
                    <td style="text-align: right">
                        {{ $row->tgl_bayar != null ? angka($row->denda_bayar) : angka($row->denda) }}</td>
                    <td style="text-align: right">
                        {{ $row->tgl_bayar != null ? angka($row->denda_bayar + $pp) : angka(((int) str_replace(',', '', $row->denda)) + ((int) str_replace(',', '', $row->pbb_akhir))) }}
                    </td>
                    <td>
                        @if ($row->tgl_bayar != '')
                            @php
                                $et = explode(',', $row->tgl_bayar);
                                $tgl = '';
                                $ct = '';
                                foreach ($et as $item) {
                                    if ($item != $ct) {
                                        $tgl .= tglIndo($item) . ', ';
                                    }
                                    $ct = $item;
                                }
                                echo substr($tgl, 0, -2);
                            @endphp
                        @endif
                        @if (($row->no_koreksi != '' && $row->status_pembayaran_sppt == '3') || $row->jns_koreksi == '3')
                            {{ tglIndo($row->tgl_surat) }}
                        @endif
                    </td>
                    <td>
                        @if ($row->no_koreksi == '' || $row->tgl_bayar != '')
                            <small>
                                @if ($pp == 0 && $row->pbb_akhir != 0)
                                    <span class="text-info"> Belum Lunas</span>
                                @elseif($pp == $row->pbb_akhir && $row->pbb_akhir > 0)
                                    <span class="text-success"> Lunas</span>
                                @elseif($pp > $row->pbb_akhir)
                                    <span class="text-warning"> Lebih Bayar</span>
                                @elseif($row->pbb_akhir == 0)
                                @else
                                    <span class="text-info"> Kurang Bayar</span>
                                @endif
                            </small><br>
                        @endif
                        <small>
                            @if ($row->pbb_akhir == 0)
                                NIHIL
                            @endif
                            @if ($row->kobil != '' && $row->pbb_akhir > 0)
                                <span class="text-danger">


                                    @php
                                        $ck = substr(trim($row->kobil), -1);
                                    @endphp
                                    {{ jenisKobil($ck) }}
                                    @if (!in_array($ck, [6, 7]))
                                        : {{ $row->kobil }}
                                    @endif
                                </span><br>
                            @endif
                            @php
                                $ket = '';
                                if ($row->tgl_bayar == '') {
                                    if (
                                        ($row->no_koreksi != '' && $row->status_pembayaran_sppt == '3') ||
                                        $row->jns_koreksi == '3'
                                    ) {
                                        $ket .= ' ' . $row->no_koreksi;
                                    }
                                    if (
                                        ($row->no_koreksi != '' && $row->status_pembayaran_sppt == '3') ||
                                        $row->jns_koreksi == '3'
                                    ) {
                                        $ket .= ' - ' . tglIndo($row->tgl_surat);
                                    }
                                }
                            @endphp
                            <?= $ket ?>
                        </small>
                    </td>

                    <td class="text-center">


                        @if (
                            $row->pbb_akhir == 0 or
                                ($row->kobil == '' or $row->kobil != '' && $row->tgl_bayar != '') &&
                                    $row->status_pembayaran_sppt != '3' &&
                                    $row->tgl_terbit_sppt != '')
                            @php
                                $var = str_replace('.', '', $nop) . $row->thn_pajak_sppt;
                            @endphp

                            @if (auth()->user()->hasRole('Wajib Pajak') == false)
                                @if ($row->tgl_bayar != '')
                                    <a onclick="window.open('{{ url('informasi/sspd/cetak', $var) }}', 
                        'newwindow', 
                        'width=700,height=1000'); 
                           return false;"
                                        href="{{ url('informasi/sspd/cetak', $var) }}" target="_blank"><i
                                            class="nav-icon fas fa-file-invoice-dollar"></i></a>
                                @else
                                    <a onclick="window.open('{{ url('informasi/sppt/cetak', $var) }}',
                           'newwindow',
                           'width=700,height=1000');
                           return false;"
                                        href="{{ url('informasi/sppt/cetak', $var) }}" target="_blank"><i
                                            class="far fa-file-alt"></i></a>
                                @endif
                            @else
                                <a onclick="window.open('{{ url('informasi/sppt/cetak', $var) }}',
                            'newwindow',
                            'width=700,height=1000');
                            return false;"
                                    href="{{ url('informasi/sppt/cetak', $var) }}" target="_blank"><i
                                        class="far fa-file-alt"></i></a>
                            @endif
                        @endif

                    </td>
                </tr>
                @php
                    $no++;
                @endphp
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="10">Data tidak ditemukan</td>
            </tr>
        @endif
    </tbody>
</table>
