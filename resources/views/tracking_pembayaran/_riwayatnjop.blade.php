<table class="table table-sm table-bordered" id="table-data">
    <thead>
        <tr>
            <th rowspan="2" style="vertical-align: middle; text-align: center">No</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Tahun</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Nama</th>
            <th colspan="3" style="vertical-align: middle; text-align: center">Bumi</th>
            <th colspan="3" style="vertical-align: middle; text-align: center">Bangunan</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">PBB</th>
            @if (isset($tahun_nota))
            <th rowspan="2" style="vertical-align: middle; text-align: center">E - SPPT</th>
            @endif
        </tr>
        <tr>
            <th style="vertical-align: middle; text-align: center">Luas</th>
            <th style="vertical-align: middle; text-align: center">NJOP / M<sup>2</sup></th>
            <th style="vertical-align: middle; text-align: center">NJOP</th>
            <th style="vertical-align: middle; text-align: center">Luas</th>
            <th style="vertical-align: middle; text-align: center">NJOP / M<sup>2</sup></th>
            <th style="vertical-align: middle; text-align: center">NJOP</th>
        </tr>


    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @if (count([$data]) > 0)
        @foreach ($data as $index => $row)
        <tr>
            <td width="5px" align="center">{{ $no }}</td>

            <td style="text-align: center">{{ $row->thn_pajak_sppt }}</td>
            <td>{{ $row->nm_wp_sppt }}</td>
            <td style="text-align: center">{{ angka($row->luas_bumi_sppt) }}</td>
            <td style="text-align: right">{{ $row->luas_bumi_sppt>0? angka($row->njop_bumi_sppt/$row->luas_bumi_sppt):0 }}</td>
            <td style="text-align: right">{{ angka($row->njop_bumi_sppt) }}</td>
            <td style="text-align: center">{{ angka($row->luas_bng_sppt) }}</td>
            <td style="text-align: right">{{ $row->luas_bng_sppt>0? angka($row->njop_bng_sppt/$row->luas_bng_sppt):0 }}</td>
            <td style="text-align: right">{{ $row->njop_bng_sppt>0?angka($row->njop_bng_sppt):0 }}</td>
            <td style="text-align: right">{{ angka(onlynumber($row->pbb)) }}</td>
            @if (isset($tahun_nota))


            <td style="text-align: center">
                @if (!in_array($row->thn_pajak_sppt,$tahun_nota))
                @php
                $var=onlyNumber($nop).$row->thn_pajak_sppt;
                @endphp


                @if($show_sppt==true)
                <button type="button" class="btn btn-primary btn-flat btn-sm dropdown-toggle" data-toggle="dropdown">
                    <i class="fas fa-print"></i>
                </button>
                <ul class="dropdown-menu">

                    <li class="dropdown-item"><a onclick="window.open('{{ url('informasi/sppt/cetak',$var) }}', 
                        'newwindow', 
                        'width=700,height=1000'); 
                           return false;" href="{{ url('informasi/sppt/cetak',$var) }}" target="_blank">Full Page</a></li>
                    <li class="dropdown-item"><a onclick="window.open('{{ url('informasi/sppt/cetak',$var) }}?jenis=0', 
                        'newwindow', 
                        'width=700,height=1000'); 
                           return false;" href="{{ url('informasi/sppt/cetak',$var) }}?jenis=0" target="_blank">Blangko</a></li>

                </ul>


                @endif
                @endif
            </td>
            @endif
        </tr>
        @php
        $no++;
        @endphp
        @endforeach

        @endif
    </tbody>
</table>
