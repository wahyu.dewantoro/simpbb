@if (count([$data]) > 0)
<a onclick="window.open('{{ url('informasi/riwayat-cetak') }}?nop={{ $nop }}', 
    'newwindow', 
    'width=700,height=1000'); 
       return false;" class="float-right text-success" href="{{ url('informasi/riwayat-cetak') }}?nop={{ $nop }}">[<i class="fas fa-print"></i> Cetak ]</a>
@endif

@php
$nota=[];
if(isset($tahun_nota)){
$nota=$tahun_nota;
}
@endphp

<table class="table table-sm table-bordered" id="table-data">
    <thead>
        <tr>
            <th rowspan="2" style="vertical-align: middle; text-align: center">No</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">NOP</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Tahun</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Nama</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">PBB</th>
            <th colspan="4" style="vertical-align: middle; text-align: center">Pembayaran</th>
            <th rowspan="2" style="vertical-align: middle; text-align: center">Keterangan</th>
            @can('view_sppt')
            <th rowspan="2" style="vertical-align: middle; text-align: center"></th>
            @endcan
        </tr>
        <tr>
            <th style="vertical-align: middle; text-align: center">Pokok</th>
            <th style="vertical-align: middle; text-align: center">Denda</th>
            <th style="vertical-align: middle; text-align: center">Total</th>
            <th style="vertical-align: middle; text-align: center">Tanggal Bayar</th>
        </tr>

    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @if(count([$data]) > 0)
        @foreach ($data as $index => $row)
        <tr>
            <td width="5px" align="center">{{ $no }}</td>
            <td>{{ $row->kd_propinsi .'.' .$row->kd_dati2 .'.' .$row->kd_kecamatan .'.' .$row->kd_kelurahan .'.' .$row->kd_blok .'-' .$row->no_urut .'.' .$row->kd_jns_op }}
            </td>
            <td style="text-align: center">{{ $row->thn_pajak_sppt }}</td>
            <td>{{ $row->nm_wp_sppt }}</td>
            <td style="text-align: right">{{ $row->pbb }}</td>
            <td style="text-align: right">
                {{ $row->pokok_bayar != null ? $row->pokok_bayar : null }}</td>
            <td style="text-align: right">
                {{ $row->denda_bayar != null ? $row->denda_bayar : null }}</td>
            <td style="text-align: right">
                {{ $row->total_bayar != 0? $row->total_bayar: angka(str_replace('.', '', $row->denda_bayar) + str_replace('.', '', $row->pokok_bayar)) }}
            </td>
            <td>{{ $row->tgl_pembayaran_sppt != '' ? tglindo($row->tgl_pembayaran_sppt) : '' }}</td>
            <td><small>
                    @if (in_array($row->thn_pajak_sppt,$nota))
                        <span class="text-danger"> NOTA PERHITUNGAN </span><br>
                    @endif


                    @if($row->status_pembayaran_sppt=='3')
                    Pembayaran di blokir <br>
                    @endif
                    <?= str_replace(',','<br>',$row->unflag_desc) ?>
                </small></td>
            {{-- @can('view_sppt') --}}
            <td class="text-center">
                @if (!in_array($row->thn_pajak_sppt,$nota))
                    @php
                    $var=str_replace('.','',$nop).$row->thn_pajak_sppt;
                    @endphp
                <a onclick="window.open('{{ url('informasi/sppt/cetak',$var) }}', 
                                'newwindow', 
                                'width=700,height=1000'); 
                                   return false;" href="{{ url('informasi/sppt/cetak',$var) }}" target="_blank" class="btn btn-default btn-sm">
                                   <i class="nav-icon fas fa-file-invoice-dollar"></i> E-SPPT</a>
                @endif
                @if($row->status_pembayaran_sppt=='1')
                <a onclick="window.open('{{ url('informasi/sppt/cetak_tanda_terima',$var) }}', 
                                'newwindow', 
                                'width=700,height=1000'); 
                                   return false;" href="{{ url('informasi/sppt/cetak_tanda_terima',$var) }}" target="_blank" class="btn btn-default btn-sm">
                                   <i class="nav-icon fas fa-file-invoice-dollar"></i> SSPD</a>
                @endif                                   
            </td>
            {{-- @endcan --}}
        </tr>
        @php
        $no++;
        @endphp
        @endforeach
        @else
        <tr>
            <td class="text-center" colspan="10">Data tidak ditemukan</td>
        </tr>
        @endif
    </tbody>
</table>
