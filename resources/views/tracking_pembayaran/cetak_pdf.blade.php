<html>

<head>
   @include('layouts.style_pdf')
</head>
<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">Desa Lunas PBB P2 Tahun Pajak {{ $tahun }} <br>
        <small>Ditarik pada {{ tglIndo(date('Y-m-d')) }} {{ date('H:i:s') }}</small>
    </h4>
    
    <table class="table table-sm table-bordered" id="table-data">
        <thead>
            <tr>
                <th style="text-align: center">No</th>
                <th style="text-align: center">Desa/Kelurahan</th>
                <th style="text-align: center">Kecamatan</th>
                <th style="text-align: center">Ketetapan PBB</th>
                <th style="text-align: center">Tanggal</th>
                
            </tr>
        </thead>
        <tbody>
            @php
                $no=1;
            @endphp
            @foreach ($data as $index => $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ $row->nm_kelurahan }}</td>
                <td>{{ $row->nm_kecamatan }}</td>
                <td align="right">
                    {{ number_format($row->nilai_baku, 0, '', '.') }}
                </td>
                <td align="center">{{ tglIndo($row->tanggal_lunas) }}</td>
                
            </tr>
            @php
                $no++;
            @endphp 
        @endforeach 
        </tbody>
    </table>
</body>

</html>
