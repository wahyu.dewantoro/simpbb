<style type="text/css">
    @page {
        margin: 5mm
    }

    body {
        margin: 5mm;
        font-family: "Calibri (Body)";
        font-size: 11pt
    }

    #watermark {
        position: fixed;
        top: 50mm;
        width: 100%;
        height: 200px;
        opacity: 0.1;
        text-align: center;
        vertical-align: middle
    }

    .table-bordered,
    .border {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
        background-color: transparent
    }

    .table-bordered,
    .table-bordered th,
    .table-bordered td {
        border-collapse: collapse;
        border: 1px solid #212529;
        padding: 3px;
        font-size: 12px !important
    }

    .border,
    .border th,
    .border td {
        border-collapse: collapse;
        border: 1px solid #212529;
        font-size: 11px !important;
        padding: 3px
    }

    .table-bordered th,
    .border th {
        vertical-align: middle
    }

    .table-bordered td,
    .border td {
        vertical-align: top
    }

    .text-kanan {
        text-align: right
    }

    .text-tengah {
        text-align: center
    }

    .text-kecil {
        font-size: 9 px !important
    }

    .text-sedang {
        font-size: 11px !important
    }

    #header,
    #footer {
        position: fixed;
        left: 0;
        right: 0;
        color: #aaa;
        font-size: .9em
    }

    #header {
        top: 0;
        border-bottom: .1pt solid #aaa
    }

    #footer {
        bottom: 0;
        border-top: .1pt solid #aaa
    }

    .page-number:before {
        content: "Page "counter(page)
    }


    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 0.5cm;

        /** Extra personal styles **/
        /* background-color:grey; */
        color: black;
        text-align: center;
        line-height: 0.5cm;
        font-size: small;
    }

    div.page {
        page-break-after: always;
        page-break-inside: avoid;
    }

    thead {
        display: table-header-group
    }

    tfoot {
        display: table-row-group
    }

    tr {
        page-break-inside: avoid
    }

</style>
