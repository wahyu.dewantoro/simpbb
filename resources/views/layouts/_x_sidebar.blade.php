<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column text-sm nav-compact nav-flat nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="{{ url('home') }}" class="nav-link  ">
                <i class="nav-icon fas fa-home"></i>
                <p>
                    Home
                </p>
            </a>
        </li>
        @if (hasPermissions(['layanan.sknjop', 'layanan.permohonan','permohonan online','kolektif_online']))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon  fas fa-calculator"></i>
                <p>
                    Permohonan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @if (hasPermissions(['layanan.permohonan']))
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p> Daftar Form</p>
                        <i class="right fas fa-angle-left"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item  ">
                            <a href="{{ url('layanan') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Form Layanan
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('pembetulan') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Pembetulan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('mutasi_gabung') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Mutasi Gabung</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('mutasi_pecah') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Mutasi Pecah</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                @if (hasPermissions(['kolektif_online']))
                <li class="nav-item">
                    <a href="{{ url('kolektif_online') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Kolektif Online
                        </p>
                    </a>
                </li>
                @endif
                @if (hasPermissions(['permohonan online']))
                <li class="nav-item">
                    <a href="{{ url('layanan/pembetulan-online') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Pembetulan Online
                        </p>
                    </a>
                </li>
                @endif
                @if (hasPermissions(['view_layanan']))
                <li class="nav-item">
                    <a href="{{ url('laporan_input') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Data Permohonan
                        </p>
                    </a>
                </li>
                @endif
                @if (hasPermissions(['layanan.sknjop']))
                <li class="nav-item">
                    <a href="{{ url('sknjop') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            SK NJOP
                        </p>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        @if (hasPermissions(['surat tugas verlap', 'penelitian lapangan', 'penelitian kantor']))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-file-signature"></i>
                <p>
                    Penelitian
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @if (hasPermissions(['surat tugas verlap']))
                <li class="nav-item">
                    <a href="{{ url('penelitian/surat-tugas') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Surat Tugas</p>
                    </a>
                </li>
                @endif
                @if (hasPermissions(['penelitian kantor']))
                <li class="nav-item">
                    <a href="{{ url('penelitian/kantor') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Penelitian Kantor
                        </p>
                    </a>
                </li>
                @endif
                @if (hasPermissions(['penelitian lapangan']))
                <li class="nav-item">
                    <a href="{{ url('penelitian/lapangan') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Penelitian Khusus
                        </p>
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a href="{{ url('penelitian/hasil-penelitian') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Hasil Penelitian
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('penelitian/nota-perhitungan') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Nota Perhitungan
                        </p>
                    </a>
                </li>

                {{-- hasil-penelitian --}}
            </ul>
        </li>
        @endif
        @if (hasPermissions(['view_billing_kolektif', 'add_billing_kolektif']))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-mail-bulk"></i>
                <p>
                    Collective Billing
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @if (hasPermissions(['add_billing_kolektif']))
                <li class="nav-item">
                    <a href="{{ url('dafnom') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Form Daftar Nominatif
                        </p>
                    </a>
                </li>
                @endif

                <li class="nav-item">
                    <a href="{{ url('data_dafnom') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Data Daftar Nominatif
                        </p>
                    </a>
                </li>
                {{-- <li class="nav-item">
                        <a href="{{ url('daftar_nop') }}" class="nav-link">
                <i class="nav-icon far fa-circle nav-icon"></i>
                <p>
                    Daftar NOP
                </p>
                </a>
        </li> --}}
        @if (hasPermissions(['view_import_kobil']))
        <li class="nav-item">
            <a href="{{ url('dafnom/riwayat') }}" class="nav-link">
                <i class="nav-icon far fa-circle nav-icon"></i>
                <p>
                    Riwayat Import
                </p>
            </a>
        </li>
        @endif
    </ul>
    {{-- </li> --}}
    </li>
    @endif
    @if (hasPermissions(['view_sppt', 'list objek', 'view_riwayat_pembayaran', 'data.bphtb', 'data.piutang', 'Penilaian Objek', 'Penetapan Objek']))
    <li class="nav-item">
        <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-file-alt"></i>
            <p>
                Data & Informasi
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @if (hasPermissions(['view_sppt', 'list objek']))
            <li class="nav-item">
                <a href="{{ url('informasi/objek-pajak') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        {{-- SPPT --}}
                        Data Objek
                    </p>
                </a>
            </li>
            @endif
            @can('view_riwayat_pembayaran')
            <li class="nav-item">
                <a href="{{ url('informasi/riwayat') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Riwayat Pembayaran
                    </p>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endif
    {{-- @php
        dd(hasPermissions(['laporan_permohonan']));
    @endphp --}}
    @if (hasPermissions(['pembayaran sppt', 'view_pembayaran_kobil', 'view_realisasi_detail_kecamatan', 'view_dhkp', 'view_dhkp_desa', 'view_dhkp_perbandingan', 'view_dhkp_perbandingan_desa', 'view_kecamatan_lunas', 'view_realisasi_target_apbd', 'view_realisasi_baku', 'view_realisasi_harian', 'view_realisasi_bulanan', 'view_rangking_kecamatan', 'realisasi tunggakan','laporan_permohonan']))
    <li class="nav-item">
        <a href="#" class="nav-link  ">
            <i class="nav-icon far fa-file-pdf"></i>
            <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">

            @if (hasPermissions(['laporan_permohonan']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Input Pelayanan
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ url('permohonan/entry_user') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Entry User
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('permohonan/entry_kelurahan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Entry Kelurahan
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('permohonan/entry_bulan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Entry Bulan
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            @if (hasPermissions(['view_dhkp', 'view_dhkp_desa', 'view_dhkp_perbandingan', 'view_dhkp_perbandingan_desa']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        DHKP
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @if (hasPermissions(['view_ringkasan_baku']))
                    <li class="nav-item">
                        <a href="{{ url('dhkp/ringkasan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Ringkasan
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_dhkp']))
                    <li class="nav-item">
                        <a href="{{ url('dhkp/wepe') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Wajib Pajak
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_dhkp_desa']))
                    <li class="nav-item">
                        <a href="{{ url('dhkp/desa') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Rekap Desa
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_dhkp_perbandingan']))
                    <li class="nav-item">
                        <a href="{{ url('dhkp/perbandingan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Perbandingan WP
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_dhkp_perbandingan_desa']))
                    <li class="nav-item">
                        <a href="{{ url('dhkp/rekapbandingdesa') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Perbandingan Desa
                            </p>
                        </a>
                    </li>
                    @endif

                    @if (hasPermissions(['view_dhkp']) &&
                    Auth()->user()->hasRole('Desa') == false)
                    <li class="nav-item">
                        <a href="{{ url('dhkp/dokumen') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Dokumen
                            </p>
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            @if (hasPermissions(['view_kecamatan_lunas', 'view_realisasi_target_apbd', 'view_realisasi_baku', 'view_realisasi_harian', 'view_realisasi_bulanan', 'view_rangking_kecamatan', 'realisasi tunggakan', 'rekonsiliasi pembayaran']))
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Realisasi
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">

                    @if (hasPermissions(['view_realisasi_target_apbd']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/targetapbd') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Target APBD
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_rangking_kecamatan']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/rangkingkecamatan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Rangking Kecamatan
                            </p>
                        </a>
                    </li>
                    @endif

                    @if (hasPermissions(['view_realisasi_baku']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/baku') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Baku
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_realisasi_harian']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/harian') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Harian
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (hasPermissions(['view_realisasi_bulanan']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/bulanan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Bulanan
                            </p>
                        </a>
                    </li>
                    @endif

                    @if (hasPermissions(['view_kecamatan_lunas']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/kecamatanlunas') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Kecamatan Lunas
                            </p>
                        </a>
                    </li>
                    @endif

                    @if (hasPermissions(['realisasi tunggakan']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/tunggakan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Tunggakan
                            </p>
                        </a>
                    </li>
                    @endif

                    @if (hasPermissions(['desa lunas']))
                    <li class="nav-item">
                        <a href="{{ url('realisasi/desalunas') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Desa Lunas
                            </p>
                        </a>
                    </li>
                    @endif

                </ul>
            </li>
            @endif


            @if (hasPermissions(['data.piutang']))
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Piutang
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">

                    <li class="nav-item">
                        <a href="{{ url('informasi/piutang-kecamatan') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Kecamatan
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
            {{-- <li class="nav-item">
                            <a href="{{ url('informasi/piutang') }}" class="nav-link">
            <i class="nav-icon far fa-circle nav-icon"></i>
            <p>
                Piutang
            </p>
            </a>
    </li> --}}
    @endif
    @if (hasPermissions(['pembayaran sppt', 'view_pembayaran_kobil', 'view_realisasi_detail_kecamatan', 'rekonsiliasi pembayaran']))
    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon far fa-circle nav-icon"></i>
            <p>
                Pembayaran
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @if (hasPermissions(['besembilan']))
            <li class="nav-item">
                <a href="{{ url('besembilan') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        API Besembilan
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['rekonsiliasi pembayaran']))
            <li class="nav-item  ">
                <a href="{{ url('realisasi/rekon') }}" class="nav-link  ">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Rekonsiliasi
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('realisasi/unflag') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Flag / Unflag
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['pembayaran sppt']))
            <li class="nav-item">
                <a href="{{ url('realisasi/pembayaran') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Pembayaran SPPT
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['view_pembayaran_kobil']))
            <li class="nav-item">
                <a href="{{ url('realisasi/kobil') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Pembayaran via kobil
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['view_realisasi_detail_kecamatan']))
            <li class="nav-item">
                <a href="{{ url('realisasi/detailkecamatan') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Kecamatan
                    </p>
                </a>
            </li>
            @endif
        </ul>
    </li>
    @endif
    </ul>
    </li>
    @endif
    @if (hasPermissions(['view_setting_target_apbd', 'view_struktural', 'setting_jatuh_tempo_sppt', 'tempat bayar']))
    <li class="nav-item">
        <a href="#" class="nav-link  ">
            <i class="nav-icon fas fa-folder-open"></i>
            <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">

            @if (hasPermissions(['tempat bayar']))
            <li class="nav-item">
                <a href="{{ url('refrensi/tempat-bayar') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Tempat Pembayaran
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['nomor formulir']))
            <li class="nav-item">
                <a href="{{ url('refrensi/formulir') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Nomor Formulir
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['view_setting_target_apbd']))
            <li class="nav-item">
                <a href="{{ url('realisasi/targetrealisasi') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Target APBD
                    </p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['view_struktural']))
            <li class="nav-item">
                <a href="{{ url('refrensi/struktural') }}" class="nav-link  ">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Struktural</p>
                </a>
            </li>
            @endif
            @can('setting_jatuh_tempo_sppt')
            <li class="nav-item">
                <a href="{{ url('refrensi/jatuhtempo') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Jatuh Tempo SPPT
                    </p>
                </a>
            </li>
            @endcan
            @can('view_kelompok_objek')
            <li class="nav-item">
                <a href="{{ url('refrensi/kelompokobjek') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Kelompok Objek
                    </p>
                </a>
            </li>
            @endcan
            @can('view_lokasi_objek')
            <li class="nav-item">
                <a href="{{ url('refrensi/lokasiobjek') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Lokasi Objek
                    </p>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endif
    @if (hasPermissions(['view_permissions', 'view_users', 'view_roles']))
    <li class="nav-item">
        <a href="#" class="nav-link  ">

            <i class="nav-icon fas fa-users-cog"></i>
            <p>
                System Settings
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ url('whatsapp') }}" class="nav-link  ">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Whatsapp</p>
                </a>
            </li>

            @if (hasPermissions(['view_roles']))
            <li class="nav-item">
                <a href="{{ url('roles') }}" class="nav-link  ">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Role</p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['view_permissions']))
            <li class="nav-item">
                <a href="{{ url('permissions') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Permissions</p>
                </a>
            </li>
            @endif
            @if (hasPermissions(['view_users']))
            <li class="nav-item">
                <a href="{{ url('users') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Users</p>
                </a>
            </li>
            @endif
            @if (Auth()->user()->hasRole('Super User'))
            <li class="nav-item">
                <a target="_blank" href="{{ url('logs') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Logs</p>
                </a>
            </li>
            <li class="nav-item">
                <a target="_blank" href="{{ url('monitor-queue') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>Queue</p>
                </a>
            </li>
            @endif
        </ul>
    </li>
    @endif
    </ul>
</nav>
