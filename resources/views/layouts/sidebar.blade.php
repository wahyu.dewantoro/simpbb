<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column text-sm nav-compact nav-flat nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">

        @foreach(showMenu(0) as $menu)
        @php
        $count =$menu->childs;
        @endphp
        <li class="nav-item">
            <a class="nav-link" <?= $count ? 'role="button" tabindex="0"' :'href="'.url($menu->url).'"' ?>>
                <i class="nav-icon  {{ $menu->icon<>''?$menu->icon:'fas fa-chevron-circle-right fas-sm'}}"></i>
                <p>{{ $menu->title }} <?= $count >0? '<i class="right fas fa-angle-left"></i>' :'' ?> </p>
            </a>
            @if($count>0)
            <ul class="nav nav-treeview">
                @include('menu.menusubdua',['childs' => showMenu($menu->id)])
            </ul>
            @endif
        </li>
        @endforeach
        {{-- @if (Auth()->user()->hasRole('Super User')) --}}
        @if(in_array('Super User',hasRole()))
        {{-- <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Master Data
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ url('refrensi/znt') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            ZNT & NIR
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('refrensi/tempat-bayar') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Tempat Pembayaran
                        </p>
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{ url('realisasi/targetrealisasi') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Target APBD
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('refrensi/struktural') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Struktural</p>
                    </a>
                </li>

                 <li class="nav-item">
                    <a href="{{ url('refrensi/jatuhtempo') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Jatuh Tempo SPPT
                        </p>
                    </a>
                </li> 

                <li class="nav-item">
                    <a href="{{ url('refrensi/kelompokobjek') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Kelompok Objek
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('refrensi/lokasiobjek') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Lokasi Objek
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('refrensi/jenispengurangan') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Jenis Pengurangan
                        </p>
                    </a>
                </li>
            </ul>
        </li> --}}

        <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-users-cog"></i>
                <p>
                    System Settings
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ url('whatsapp') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Whatsapp</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('roles') }}" class="nav-link  ">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Role</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('users') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Users</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a target="_blank" href="{{ url('logs') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Logs</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a target="_blank" href="{{ url('monitor-queue') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Queue</p>
                    </a>
                </li>
            </ul>
        </li>
        @endif
    </ul>
</nav>
