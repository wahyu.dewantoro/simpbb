<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>@yield('title', 'SIMPBB')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo') }}/favicon.ico" />

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="SIMPBB: Sistem Pelayanan PBB P2 BAPENDA Kabupaten Malang">
    <meta itemprop="description"
        content="SIMPBB adalah sistem pelayanan PBB P2 yang dikembangkan oleh BAPENDA Kabupaten Malang untuk mempermudah masyarakat dalam mengelola Pajak Bumi dan Bangunan.">
    <meta itemprop="image" content="{{ asset('sipanji.png') }}">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://sipanji.id/simpbb">
    <meta property="og:type" content="website">
    <meta property="og:title" content="SIMPBB: Sistem Pelayanan PBB P2 BAPENDA Kabupaten Malang">
    <meta property="og:description"
        content="SIMPBB adalah layanan digital untuk membantu masyarakat Kabupaten Malang mengelola PBB dengan mudah dan efisien. Dikembangkan oleh BAPENDA Kabupaten Malang.">
    <meta property="og:image" content="{{ asset('sipanji.png') }}">
    <meta property="og:site_name" content="SIPANJI - SIMPBB">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="SIMPBB: Sistem Pelayanan PBB P2 BAPENDA Kabupaten Malang">
    <meta name="twitter:description"
        content="SIMPBB, sistem inovatif dari BAPENDA Kabupaten Malang, mempermudah masyarakat dalam pengurusan Pajak Bumi dan Bangunan (PBB) secara digital.">
    <meta name="twitter:image" content="{{ asset('sipanji.png') }}">

    <!-- Additional SEO Meta Tags -->
    <meta name="description"
        content="SIMPBB memudahkan masyarakat Kabupaten Malang dalam mengelola Pajak Bumi dan Bangunan dengan dukungan digital dari BAPENDA Kabupaten Malang.">
    <meta name="keywords" content="SIMPBB, PBB P2, pelayanan pajak, BAPENDA, Kabupaten Malang, sistem pajak digital">
    <meta name="robots" content="index, follow">
    <meta name="author" content="BAPENDA Kabupaten Malang">

    {{-- Font Awesome Icons untuk ikon tambahan di halaman  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/fontawesome-free/css/all.min.css">

    {{-- Toastr untuk menampilkan notifikasi pop-up yang sementara  --}}
    <link href="{{ asset('lte') }}/plugins/toastr/toastr.min.css" rel="stylesheet" />

    {{-- jQuery UI Stylesheet untuk elemen UI seperti date picker, dialog, dll.  --}}
    <link href="{{ asset('lte') }}/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />

    {{-- DataTables Bootstrap 4 Stylesheet untuk styling tabel interaktif  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    {{-- DataTables Responsive Stylesheet agar tabel menyesuaikan dengan layar kecil  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    {{-- DataTables Buttons Stylesheet untuk tombol tambahan pada DataTables  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

    {{-- AdminLTE CSS untuk gaya dan tampilan utama dari template AdminLTE  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/dist/css/adminlte.min.css">

    {{-- Daterangepicker Stylesheet untuk input tanggal dengan rentang tanggal  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/daterangepicker/daterangepicker.css">

    {{-- Bootstrap Datepicker Stylesheet untuk memilih tanggal dengan tampilan kalender  --}}
    <link rel="stylesheet" href="{{ asset('bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.min.css') }}">

    {{-- Select2 Stylesheet untuk komponen dropdown yang dapat mencari data  --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/select2/css/select2.min.css">


    <style>
        .modalloading {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, .8) url('{{ url('FhHRx.gif') }}') 100% 100% no-repeat;
        }

        body.loading .mmodalloadingodal {
            overflow: hidden;
        }

        body.loading .modalloading {
            display: block;
        }

        .hero-image {}

        .sorting,
        .sorting_asc,
        .sorting_desc {
            background: none;
        }
    </style>
    @yield('css')
    <!-- </head> -->
</head>

<body class="hold-transition sidebar-mini text-sm layout-fixed  layout-navbar-fixed layout-footer-fixed">
    {{-- sidebar-collapse --}}
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-dark navbar-navy text-sm">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Navbar Search -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search"
                                    aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>



                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset('lte') }}/dist/img/user2-160x160.jpg"
                            class="user-image img-circle elevation-2" alt="User Image">
                        <span class="d-none d-md-inline"> {{ auth()->user()->nama_pendek ?? '-' }}
                        </span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <li class="user-header bg-light-navy">
                            <img src="{{ asset('lte') }}/dist/img/user2-160x160.jpg" class="img-circle elevation-2"
                                alt="User Image">
                            <p>
                                {{ auth()->user()->nama ?? '-' }}
                                <small>{{ implode(', ', hasRole()) }}</small>
                                @impersonating($guard = null)
                                    <span><small>Impersonate
                                            <a class="text-warning" href="{{ route('impersonate.leave') }}">[ Leave
                                                ]</a>
                                        </small></span>
                                @endImpersonating
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <a class="btn  btn-info btn-flat" href="{{ url('users', Auth()->user()->id) }}"><i
                                    class="nav-icon fas fa-user"></i> Profile</a>
                            <a href="{{ url('logout') }}" class="btn  btn-danger btn-flat float-right"><i
                                    class="fas fa-sign-out-alt"></i> Sign out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar elevation-1 sidebar-light-navy">
            <a href="{{ url('home') }}" style="background: white" class="brand-link logo-switch">
                <img src="{{ asset('tax.jpg') }}" alt="AdminLTE Docs Logo Small"
                    class="brand-image-xl logo-xs img-rounded">
                <img src="{{ asset('sipanji.png') }}" alt="SIPANJI LARGE" class="brand-image-xs logo-xl"
                    style="left: 12px">
            </a>
            <!-- Sidebar -->
            <div
                class="sidebar os-host os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition os-theme-dark">
                @include('layouts.sidebar')
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper hero-image">
            <!-- Content Header (Page header) -->
            @yield('content')
            <!-- /.content -->
        </div>
        <!-- Main Footer -->
        <footer class="main-footer"
            style="padding: 1rem; background-color: #f4f6f9; color: #333; text-align: center;">
            <div class="container">
                <strong>&copy; {{ date('Y') }} BAPENDA Kabupaten Malang.</strong> All rights reserved.
                <div class="float-right d-none d-sm-inline">
                    SIMPBB - Bagian dari <a href="http://sipanji.id" target="_blank"
                        rel="noopener noreferrer">SIPANJI</a>
                </div>
            </div>
        </footer>

    </div>
    <!-- ./wrapper -->
    <span id="msg-success" data-msg="{!! session('success') !!}"></span>
    <span id="msg-error" data-msg="{!! session('error') !!}"></span>
    <span id="msg-warning" data-msg="{!! session('warning') !!}"></span>
    <div class="modalloading"></div>
    <div id="user_id" data-id="{{ auth()->user()->id }}"></div>

    {{-- jQuery Library untuk berbagai fungsi JavaScript --}}
    <script src="{{ asset('lte') }}/plugins/jquery/jquery.min.js"></script>

    {{-- jQuery UI Library untuk elemen-elemen UI tambahan --}}
    <script src="{{ asset('lte') }}/plugins/jquery-ui/jquery-ui.min.js"></script>

    {{-- Bootstrap Bundle untuk komponen UI seperti modal, dropdown, dan lainnya --}}
    <script src="{{ asset('lte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    {{-- AdminLTE App untuk fitur dan gaya dari template AdminLTE --}}
    <script src="{{ asset('lte') }}/dist/js/adminlte.min.js"></script>

    {{-- Toastr Library untuk menampilkan notifikasi yang muncul sementara --}}
    <script src="{{ asset('lte') }}/plugins/toastr/toastr.min.js"></script>

    {{-- Moment.js untuk manipulasi dan format tanggal dan waktu --}}
    <script src="{{ asset('lte') }}/plugins/moment/moment.min.js"></script>

    {{-- Daterangepicker Library untuk memilih rentang tanggal --}}
    <script src="{{ asset('lte') }}/plugins/daterangepicker/daterangepicker.js"></script>

    {{-- Bootstrap Datepicker untuk memilih tanggal dengan tampilan kalender --}}
    <script src="{{ asset('bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js') }}"></script>

    {{-- SweetAlert2 untuk menampilkan popup notifikasi dengan gaya yang menarik --}}
    <script src="{{ asset('sweetalert2') }}/dist/sweetalert2.all.min.js"></script>

    {{-- jQuery Form Plugin untuk mempermudah manipulasi form --}}
    <script src="{{ asset('plugins') }}/jquery.form.min.js"></script>

    {{-- jQuery MaskMoney untuk input angka dengan format mata uang --}}
    <script src="{{ asset('plugins') }}/jquery.maskMoney.min.js"></script>

    {{-- Inputmask Library untuk menambahkan mask pada inputan seperti format telepon atau angka --}}
    <script src="{{ asset('plugins') }}/inputmask/dist/jquery.inputmask.min.js"></script>

    {{-- Select2 untuk membuat dropdown yang mendukung pencarian --}}
    <script src="{{ asset('lte') }}/plugins/select2/js/select2.full.min.js"></script>

    {{-- DataTables Library untuk tabel interaktif --}}
    <script src="{{ asset('lte') }}/plugins/datatables/jquery.dataTables.js"></script>

    {{-- DataTables Bootstrap 4 untuk menambahkan gaya Bootstrap ke DataTables --}}
    <script src="{{ asset('lte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

    {{-- DataTables Responsive untuk tampilan tabel yang responsif --}}
    <script src="{{ asset('lte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    {{-- DataTables Buttons untuk menambahkan tombol aksi di DataTables --}}
    <script src="{{ asset('lte') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>

    {{-- Custom JavaScript Component untuk kebutuhan aplikasi --}}
    <script src="{{ asset('js') }}/appcomponent.js"></script>

    {{-- Loading Overlay untuk menampilkan overlay loading saat halaman diproses --}}
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js">
    </script>

    <script>
        $body = $("body");

        function openloading() {
            Swal.fire({
                // icon: 'error',
                title: '<i class="fas fa-sync fa-spin"></i>',
                text: "Sistem sedang berjalan, mohon ditunggu!",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false
            })
        }

        function closeloading() {
            swal.close();
        }

        function openTab(url) {
            window.open(url, 'newwindow', 'width=700,height=1000');
            return false;
        }


        function formatnop(a) {
            // a = objek.value;
            var a = a.toString();
            var b = a.replace(/[^\d]/g, "");
            var c = "";
            var panjang = b.length;

            if (panjang <= 2) {
                // 35 -> 0,2
                c = b;
            } else if (panjang > 2 && panjang <= 4) {
                // 07. -> 2,2
                c = b.substr(0, 2) + '.' + b.substr(2, 2);
            } else if (panjang > 4 && panjang <= 7) {
                // 123 -> 4,3
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
            } else if (panjang > 7 && panjang <= 10) {
                // .123. ->
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
            } else if (panjang > 10 && panjang <= 13) {
                // 123.
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10,
                    3);
            } else if (panjang > 13 && panjang <= 17) {
                // 1234
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10,
                    3) + '.' + b.substr(13, 4);
            } else {
                // .0
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10,
                    3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
                // alert(panjang);
            }
            // objek.value = c;
            return c;
        }


        function formatRupiah(angka) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        }

        $(document).ready(function() {

            $('[data-toggle="tooltip"]').tooltip()

            $.ajaxSetup({
                statusCode: {
                    401: function() {
                        location.reload();
                    }
                }
            });
            $(document).on('keypress', '.angka', function() {
                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault(); //stop character from entering input
                }
            });

            $('#bulan').datepicker({
                format: "mm-yyyy",
                viewMode: "months",
                minViewMode: "months"
            });




            $('.tanggal').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                locale: {
                    format: 'DD MMM YYYY',
                    daysOfWeek: [
                        "Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
                    ],
                    monthNames: [
                        "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus",
                        "September", "Oktober", "November", "Desember"
                    ]
                }
            });

            toastr.options = {
                "closeButton": true, // Tambahkan tombol close untuk kontrol lebih
                "debug": false, // Nonaktifkan debug untuk produksi
                "newestOnTop": true, // Tampilkan notifikasi terbaru di atas
                "progressBar": true, // Progress bar untuk menunjukkan durasi
                "positionClass": "toast-top-right", // Posisi di sudut kanan atas
                "preventDuplicates": true, // Hindari notifikasi duplikat
                "onclick": null, // Aksi tambahan dapat disesuaikan di sini
                "showDuration": "300", // Durasi animasi tampil lebih cepat
                "hideDuration": "500", // Durasi animasi hilang lebih halus
                "timeOut": "7000", // Tampilkan notifikasi selama 7 detik
                "extendedTimeOut": "2000", // Tambahan waktu saat pengguna hover
                "showEasing": "easeOutBounce", // Animasi muncul yang lebih menarik
                "hideEasing": "easeInQuad", // Animasi hilang yang lebih elegan
                "showMethod": "slideDown", // Gunakan slide untuk efek muncul
                "hideMethod": "fadeOut" // Fade out untuk efek hilang
            };


            var success = $('#msg-success').data('msg');
            var error = $('#msg-error').data('msg');
            var warning = $('#msg-warning').data('msg');

            // Display a success toast, with a title
            if (success != '') {
                toastr.success(success);
            }

            // Display an error toast, with a title
            if (error != '') {
                toastr.error(error);
            }

            // Display a success toast, with a title
            if (warning != '') {
                toastr.warning(warning);
            }

            var url = window.location;
            // for sidebar menu entirely but not cover treeview
            $('ul.nav-sidebar a').filter(function() {
                if (this.href) {
                    return this.href == url || url.href.indexOf(this.href) == 0;
                }
            }).addClass('active');

            // for the treeview
            $('ul.nav-treeview a').filter(function() {
                if (this.href) {
                    return this.href == url || url.href.indexOf(this.href) == 0;
                }
            }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');


            // user_id
            // get notifikasi
            function getNotifikasi() {
                var user_id = $('#user_id').data('id')
                $.ajax({
                    url: "{{ url('notifikasi-show') }}",
                    data: {
                        'user_id': user_id
                    },
                    success: function(res) {
                        $.each(res, function(k, v) {
                            toastr.info(v.deskripsi, v.judul);
                        });

                    },
                    error: function(res) {

                    }
                });
            }

        });



        var imageAddr = "https://hackthestuff.com/images/test.jpg";
        var downloadSize = 13055440;

        function ShowProgressMessage(msg) {
            document.getElementById('speed').innerHTML = msg;
        }

        function InitiateSpeedDetection() {
            ShowProgressMessage("Checking speed, please wait...");
            window.setTimeout(MeasureConnectionSpeed, 0);
        };

        function MeasureConnectionSpeed() {
            var startTime, endTime;
            var download = new Image();
            download.onload = function() {
                endTime = (new Date()).getTime();
                showResults();
            }
            download.onerror = function(err, msg) {
                ShowProgressMessage("Invalid image, or error downloading");
            }
            startTime = (new Date()).getTime();
            var cacheBuster = "?nnn=" + startTime;
            download.src = imageAddr + cacheBuster;

            function showResults() {
                var duration = (endTime - startTime) / 1000;
                var bitsLoaded = downloadSize * 8;
                var speedBps = (bitsLoaded / duration).toFixed(2);
                var speedKbps = (speedBps / 1024).toFixed(2);
                var speedMbps = (speedKbps / 1024).toFixed(2);

                if (speedMbps > 1) {
                    ShowProgressMessage("Your connection speed is " + speedMbps + " Mbps");
                } else if (speedKbps > 1) {
                    ShowProgressMessage("Your connection speed is " + speedKbps + " kbps");
                } else {
                    ShowProgressMessage("Your connection speed is " + speedBps + " bps");
                }
            }
        }


        // InitiateSpeedDetection();
    </script>

    @yield('script')

</body>

</html>
