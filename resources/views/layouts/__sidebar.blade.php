<nav class="mt-2">
    {{-- <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-compact nav-legacy" data-widget="treeview" role="menu" data-accordion="false"> --}}
    <ul class="nav nav-pills nav-sidebar flex-column text-sm nav-compact nav-flat nav-child-indent"
        data-widget="treeview" role="menu" data-accordion="false">

        @if (hasPermissions(['layanan.sknjop', 'layanan.permohonan ']))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                {{-- <i class="nav-icon fab fa-servicestack"></i> --}}
                <i class="fas fa-chalkboard-teacher nav-icon "></i>
                <p>
                    Layanan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            bill
        </li>
        @endif
        @if (Auth()->user()->hasRole('Super User'))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                
                <i class="nav-icon fas fa-file-signature"></i>
                <p>
                    Penelitian
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ url('penelitian/kantor') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Kantor</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('penelitian/lapangan') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Lapangan</p>
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if (hasPermissions(['view_penagihan', 'add_penagihan']))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Penagihan
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ url('penagihan_form') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Form Penagihan</p>
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a href="{{ url('penagihan_daftar') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Daftar Penagihan</p>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a href="{{ url('realisasi_penagihan_piutang') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>Realisasi Piutang</p>
                    </a>
                </li>
            </ul>
        </li>
        @endif
        @if (hasPermissions(['view_billing_kolektif', 'add_billing_kolektif']))
        <li class="nav-item">
            <a href="#" class="nav-link  ">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Billing Collective
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @if (hasPermissions(['add_billing_kolektif']))
                <li class="nav-item">
                    <a href="{{ url('dafnom') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Pembuatan Dafnom
                        </p>
                    </a>
                </li>
                @endif
                @if (hasPermissions(['view_import_kobil']))
                <li class="nav-item">
                    <a href="{{ url('dafnom/riwayat') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Riwayat Import
                        </p>
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a href="{{ url('data_dafnom') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Data Dafnom
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('daftar_nop') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Daftar NOP
                        </p>
                    </a>
                </li>
                <!-- <li class="nav-item">
                        <a href="{{ url('dafnom_realisasi') }}" class="nav-link">
                            <i class="nav-icon far fa-circle nav-icon"></i>
                            <p>
                                Realisasi
                            </p>
                        </a>
                    </li> -->
                </ul>
            </li>
        @endif
        @if (hasPermissions(['view_sppt', 'view_riwayat_pembayaran', 'data.bphtb', 'data.piutang', 'Penilaian Objek', 'Penetapan Objek']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    {{-- <i class=" fas fa-file-medical-alt"></i> --}}
                    <i class="fas fa-qrcode nav-icon"></i>
                    <p>
                        Data & Informasi
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">

                    {{-- @can('Penilaian Objek')
                <li class="nav-item">
                    <a href="{{ url('penilaian') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Penilaian Objek
                        </p>
                    </a>
                </li>
                @endcan --}}
                    @can('Penetapan Objek')
                        {{-- <li class="nav-item">
                    <a href="{{ url('penetapan') }}" class="nav-link">
                        <i class="nav-icon far fa-circle nav-icon"></i>
                        <p>
                            Penetapan Objek
                        </p>
                    </a>
                </li> --}}
                    @endcan
                    @can('view_sppt')
                        <li class="nav-item">
                            <a href="{{ url('informasi/objek-pajak') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    {{-- SPPT --}}
                                    Data Objek
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('view_riwayat_pembayaran')
                        <li class="nav-item">
                            <a href="{{ url('informasi/riwayat') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Riwayat Pembayaran
                                </p>
                            </a>
                        </li>
                    @endcan
                    @if (hasPermissions(['unflag']))
                        <li class="nav-item">
                            <a href="{{ url('informasi/unflag') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Unflag
                                </p>
                            </a>
                        </li>
                    @endif
                    {{-- @if (hasPermissions(['data.bphtb']))
                        <li class="nav-item">
                            <a href="{{ url('informasi/bphtb') }}" class="nav-link">
                <i class="nav-icon far fa-circle nav-icon"></i>
                <p>
                    BPHTB
                </p>
                </a>
        </li>
        @endif --}}
                    @if (hasPermissions(['data.piutang']))
                        <li class="nav-item">
                            <a href="{{ url('informasi/piutang') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Piutang
                                </p>
                            </a>
                        </li>
                    @endif
                    <!-- <li class="nav-item">
            <a href="{{ url('informasi/masal') }}" class="nav-link">
                <i class="nav-icon far fa-circle nav-icon"></i>
                <p>
                    Penetapan Masal
                </p>
            </a>
        </li> -->

                </ul>
            </li>
        @endif
        @if (hasPermissions(['view_pembayaran_kobil', 'view_kecamatan_lunas', 'view_realisasi_target_apbd', 'view_realisasi_baku', 'view_realisasi_harian', 'view_realisasi_bulanan', 'view_realisasi_detail_kecamatan', 'view_rangking_kecamatan', 'pembayaran sppt','rekonsiliasi pembayaran','realisasi tunggakan','besembilan']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    <i class="fas fa-chart-line nav-icon "></i>
                    <p>
                        Laporan Keuangan
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    {{-- @if (hasPermissions(['besembilan']))
                        <li class="nav-item  ">
                            <a href="{{ url('besembilan') }}" class="nav-link  ">
                                <i class="nav-icon  fas fa-bolt"></i>
                                <p>
                                    Besembilan 
                                </p>
                            </a>
                        </li>
                    @endif --}}
                    @if (hasPermissions(['rekonsiliasi pembayaran']))
                        <li class="nav-item  ">
                            <a href="{{ url('realisasi/rekon') }}" class="nav-link  ">
                                <i class="nav-icon  fas fa-compress-arrows-alt"></i>
                                <p>
                                    Rekonsiliasi
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_pembayaran_kobil', 'view_realisasi_detail_kecamatan', 'pembayaran sppt']))
                        <li class="nav-item  ">
                            <a href="#" class="nav-link  ">
                                <i class="nav-icon fas fa-file-invoice-dollar"></i>
                                <p>
                                    Pembayaran
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                @if (hasPermissions(['pembayaran sppt']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/pembayaran') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Pembayaran SPPT
                                            </p>
                                        </a>
                                    </li>
                                @endif
                                @if (hasPermissions(['view_pembayaran_kobil']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/kobil') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Pembayaran via kobil
                                            </p>
                                        </a>
                                    </li>
                                @endif
                                @if (hasPermissions(['view_realisasi_detail_kecamatan']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/detailkecamatan') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Kecamatan
                                            </p>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if (hasPermissions(['view_kecamatan_lunas', 'view_realisasi_target_apbd', 'view_realisasi_baku', 'view_realisasi_harian', 'view_realisasi_bulanan', 'view_rangking_kecamatan','realisasi tunggakan']))
                        <li class="nav-item  ">
                            <a href="#" class="nav-link  ">
                                <i class="nav-icon fas fa-file-invoice"></i>
                                <p>
                                    Realisasi
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>

                            <ul class="nav nav-treeview">

                                @if (hasPermissions(['view_realisasi_target_apbd']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/targetapbd') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Target APBD
                                            </p>
                                        </a>
                                    </li>
                                @endif
                                @if (hasPermissions(['view_rangking_kecamatan']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/rangkingkecamatan') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Rangking Kecamatan
                                            </p>
                                        </a>
                                    </li>
                                @endif

                                @if (hasPermissions(['view_realisasi_baku']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/baku') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Baku
                                            </p>
                                        </a>
                                    </li>
                                @endif
                                @if (hasPermissions(['view_realisasi_harian']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/harian') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Harian
                                            </p>
                                        </a>
                                    </li>
                                @endif
                                @if (hasPermissions(['view_realisasi_bulanan']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/bulanan') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Bulanan
                                            </p>
                                        </a>
                                    </li>
                                @endif

                                @if (hasPermissions(['view_kecamatan_lunas']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/kecamatanlunas') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Kecamatan Lunas
                                            </p>
                                        </a>
                                    </li>
                                @endif

                                @if (hasPermissions(['realisasi tunggakan']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/tunggakan') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Tunggakan
                                            </p>
                                        </a>
                                    </li>
                                @endif

                                @if (hasPermissions(['desa lunas']))
                                    <li class="nav-item">
                                        <a href="{{ url('realisasi/desalunas') }}" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>
                                                Desa Lunas
                                            </p>
                                        </a>
                                    </li>
                                @endif



                                {{-- <li class="nav-item">
                        <a href="{{ url('realisasi/bakukecamatan') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Baku Kecamatan
                    </p>
                    </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('realisasi/bakudesa') }}" class="nav-link">
                    <i class="nav-icon far fa-circle nav-icon"></i>
                    <p>
                        Baku Desa
                    </p>
                </a>
            </li> --}}

                            </ul>
                        </li>
                    @endif
                </ul>
            </li>
        @endif


        @if (hasPermissions(['view_dhkp', 'view_dhkp_desa', 'view_dhkp_perbandingan', 'view_dhkp_perbandingan_desa']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    <i class="fas fa-boxes nav-icon"></i>
                    <p>
                        DHKP
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @if (hasPermissions(['view_ringkasan_baku']))
                        <li class="nav-item">
                            <a href="{{ url('dhkp/ringkasan') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Ringkasan
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_dhkp']))
                        <li class="nav-item">
                            <a href="{{ url('dhkp/wepe') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Wajib Pajak
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_dhkp_desa']))
                        <li class="nav-item">
                            <a href="{{ url('dhkp/desa') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Rekap Desa
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_dhkp_perbandingan']))
                        <li class="nav-item">
                            <a href="{{ url('dhkp/perbandingan') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Perbandingan WP
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_dhkp_perbandingan_desa']))
                        <li class="nav-item">
                            <a href="{{ url('dhkp/rekapbandingdesa') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Perbandingan Desa
                                </p>
                            </a>
                        </li>
                    @endif

                    @if (hasPermissions(['view_dhkp']) && Auth()->user()->hasRole('Desa') == false)
                        <li class="nav-item">
                            <a href="{{ url('dhkp/dokumen') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Dokumen
                                </p>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif
        @if (hasPermissions(['view_setting_target_apbd', 'view_struktural', 'setting_jatuh_tempo_sppt', 'tempat bayar']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    <i class="fas fa-archive nav-icon"></i>
                    <p>
                        Refrensi
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">

                    @if (hasPermissions(['tempat bayar']))
                        <li class="nav-item">
                            <a href="{{ url('refrensi/tempat-bayar') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Tempat Pembayaran
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['nomor formulir']))
                        <li class="nav-item">
                            <a href="{{ url('refrensi/formulir') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Nomor Formulir
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_setting_target_apbd']))
                        <li class="nav-item">
                            <a href="{{ url('realisasi/targetrealisasi') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Target APBD
                                </p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_struktural']))
                        <li class="nav-item">
                            <a href="{{ url('refrensi/struktural') }}" class="nav-link  ">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Struktural</p>
                            </a>
                        </li>
                    @endif
                    @can('setting_jatuh_tempo_sppt')
                        <li class="nav-item">
                            <a href="{{ url('refrensi/jatuhtempo') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Jatuh Tempo SPPT
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('view_kelompok_objek')
                        <li class="nav-item">
                            <a href="{{ url('refrensi/kelompokobjek') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Kelompok Objek
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('view_lokasi_objek')
                        <li class="nav-item">
                            <a href="{{ url('refrensi/lokasiobjek') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>
                                    Lokasi Objek
                                </p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endif
        @if (hasPermissions(['view_permissions', 'view_users', 'view_roles']))
            <li class="nav-item  ">
                <a href="#" class="nav-link  ">
                    <i class="nav-icon fas fa-user-cog"></i>
                    <p>
                        Sistem
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @if (hasPermissions(['view_roles']))
                        <li class="nav-item">
                            <a href="{{ url('roles') }}" class="nav-link  ">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Role</p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_permissions']))
                        <li class="nav-item">
                            <a href="{{ url('permissions') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Permissions</p>
                            </a>
                        </li>
                    @endif
                    @if (hasPermissions(['view_users']))
                        <li class="nav-item">
                            <a href="{{ url('users') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                    @endif

                    @if (Auth()->user()->hasRole('Super User'))
                        {{-- <li class="nav-item">
                            <a href="{{ url('impersonate') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Impersonate</p>
                            </a>
                        </li> --}}
                        <li class="nav-item">
                            <a target="_blank" href="{{ url('logs') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Logs</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a target="_blank" href="{{ url('monitor-queue') }}" class="nav-link">
                                <i class="nav-icon far fa-circle nav-icon"></i>
                                <p>Monitor Queue</p>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif

        {{-- @if (Auth()->user()->hasRole('Super User'))
            <li class="nav-item">
                <a href="{{ url('monitor-queue') }}" class="nav-link">
    <i class="nav-icon fab fa-watchman-monitoring"></i>
    <p>
        Monitor Queue
    </p>
    </a>
    </li>
    @endif --}}

    </ul>
</nav>
