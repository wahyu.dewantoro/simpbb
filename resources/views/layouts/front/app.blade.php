<!DOCTYPE html>


<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="Portal Pembayaran Pajak PBB Online | SIPANJI Bapenda Kabupaten Malang">
    <meta name="description"
        content="Bayar Pajak PBB Online lebih mudah dan cepat dengan SIPANJI, layanan resmi Bapenda Kabupaten Malang. Akses info pajak, cek tagihan, dan nikmati kemudahan pembayaran dari mana saja.">
    <meta name="keywords" content="pajak pbb, sipanji, bapenda kabupaten malang, bayar pajak online, portal pajak online">
    <meta name="author" content="Bapenda Kabupaten Malang">
    <meta name="robots" content="index, follow">
    <meta property="og:title" content="Portal Pembayaran Pajak PBB Online | SIPANJI Bapenda Kabupaten Malang">
    <meta property="og:description"
        content="Bayar Pajak PBB Online lebih mudah dan cepat dengan SIPANJI, layanan resmi Bapenda Kabupaten Malang. Akses info pajak, cek tagihan, dan nikmati kemudahan pembayaran dari mana saja.">
    <meta property="og:image" content="{{ asset('sipanji.png') }}">
    <meta property="og:url" content="{{ url('pembayaran-online') }}">
    <meta property="og:type" content="website">
    <title>Portal Pembayaran Pajak PBB Online | SIPANJI Bapenda Kabupaten Malang</title>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('lte') }}/dist/css/adminlte.min.css">
</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">


        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <a href="#" class="navbar-brand">
                    <img src="{{ asset('sipanji.png') }}" alt="AdminLTE Logo" class="brand-image " style="opacity: .8">
                </a>


            </div>
        </nav>



        <div class="content-wrapper">

            <div class="content-header">
                <div class="container">
                    <div class="row mb-2">
                        <div class="col-sm-12">
                            <h1 class="m-0 text-center">Pembayaran Online</h1>
                            <div class="text-center"> <p class="m-0">Melalui Virtual Account (VA) atau
                                    QRIS</p></div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>

                                    <p class="card-text">
                                        Some quick example text to build on the card title and make up the bulk of the
                                        card's
                                        content.
                                    </p>

                                    <a href="#" class="card-link">Card link</a>
                                    <a href="#" class="card-link">Another link</a>
                                </div>
                            </div>

                            <div class="card card-primary card-outline">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>

                                    <p class="card-text">
                                        Some quick example text to build on the card title and make up the bulk of the
                                        card's
                                        content.
                                    </p>
                                    <a href="#" class="card-link">Card link</a>
                                    <a href="#" class="card-link">Another link</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title m-0">Featured</h5>
                                </div>
                                <div class="card-body">
                                    <h6 class="card-title">Special title treatment</h6>

                                    <p class="card-text">With supporting text below as a natural lead-in to additional
                                        content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>

                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h5 class="card-title m-0">Featured</h5>
                                </div>
                                <div class="card-body">
                                    <h6 class="card-title">Special title treatment</h6>

                                    <p class="card-text">With supporting text below as a natural lead-in to additional
                                        content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>



        <aside class="control-sidebar control-sidebar-dark">

        </aside>



        <footer class="main-footer">

            <div class="float-right d-none d-sm-inline">
                Pembayaran Online
            </div>

            <strong>Copyright &copy; <a href="https://sipanji.id">SIPANJI</a>.</strong> All rights
            reserved.
        </footer>
    </div>


    <script src="{{ asset('lte') }}/plugins/jquery/jquery.min.js"></script>

    <script src="{{ asset('lte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="{{ asset('lte') }}/dist/js/adminlte.min.js"></script>


</body>

</html>
