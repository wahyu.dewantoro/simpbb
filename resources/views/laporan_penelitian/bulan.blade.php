@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Laporan Penelitian per Bulan</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <form action="#bulan_search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <select name="layanan"  required class="form-control select"  data-placeholder="Pilih Layanan">
                                                <option value="-">-- Semua Layanan --</option>
                                                @foreach ($Jenis_layanan as $layanan)
                                                    <option value="{{ $layanan->id }}">
                                                        {{ $layanan->nama_layanan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="input-group input-group-sm">
                                            <select name="kecamatan"  class="form-control-sm form-control select-bottom"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                                                @if(count($kecamatan)>1) <option value="-">-- Kecamatan --</option> @endif
                                                @foreach ($kecamatan as $rowkec)
                                                    <option value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="input-group input-group-sm">
                                            <select name="kelurahan"  class="form-control-sm form-control select-bottom select-reset change-next"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu)." >
                                                @if(count($kelurahan)>1) <option value="-">-- Kelurahan --</option> @endif
                                                @foreach ($kelurahan as $rowkel)
                                                    <option value="{{ $rowkel->kd_kelurahan }}">
                                                        {{ $rowkel->nm_kelurahan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group-sm">
                                            <select name="tahun"  class="form-control-sm form-control"  data-placeholder="Tahun." >
                                                @foreach ($tahun_pajak as $rowtahun)
                                                    <option value="{{ $rowtahun }}">
                                                        {{ $rowtahun}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group input-group-sm">
                                            <select name="bulan"  class="form-control-sm form-control select"  data-placeholder="Bulan." >
                                                <option value="-">-- Semua --</option>
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group">
                                            <button type='submit' class='btn btn-sm btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group input-group-sm">
                                            <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Cetak  </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-body no-padding p-1">
                                <table  class="pull-right table-bordered table-striped table-sm mb-1" id="total-table">
                                    <thead>
                                        <tr>
                                            <th>Total Penelitian</th>
                                            <th>Total Luas Bumi</th>
                                            <th>Total Luas Bangunan</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm text-right'>
                                        <tr>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-body no-padding p-1">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>kecamatan</th>
                                            <th>Kelurahan</th>
                                            <th>Jenis Layanan</th>
                                            <th>Total</th>
                                            <th>Luas Bumi</th>
                                            <th>Luas Bangunan</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            $(".year").inputmask({ alias: "datetime", inputFormat: "yyyy"});
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'id':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('laporan_penelitian/bulan_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'nm_kecamatan'},
                    { data: 'nm_kelurahan'},
                    { data: 'jenis_layanan_nama',class:'w-20'},
                    { data: 'total'},
                    { data: 'luas_bumi'},
                    { data: 'luas_bng'},
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let timer=60000;
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            setInterval( function () {
                resLoad();
            }, timer );
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    if(key.name!='_token'){
                        str.push(key.name+"="+key.value);
                    }
                });
                return str.join("&");
            }
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian Data.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    asyncData("{{ url('laporan_penelitian/bulan_total') }}"+"?"+toString(e.serializeArray())).then((response) => {
                        $("#total-table>tbody").html(response.data);
                        swal.close();
                    }).catch((error)=>{
                        Swal.fire({ icon: 'error', text: defaultError});
                    });
                    swal.close();
                });
            });
            let eForm=$("#form-cek");
            asyncData("{{ url('laporan_penelitian/bulan_total') }}"+"?"+toString(eForm.serializeArray())).then((response) => {
                $("#total-table>tbody").html(response.data);
                swal.close();
            }).catch((error)=>{
                Swal.fire({ icon: 'error', text: defaultError});
            });
            const changeData=function(e,async_status=true){
                let uri=e.attr("data-change").split("#"),
                    target=e.closest('form').find("[name='"+e.attr("data-target-name")+"']");
                    target.appendData("{{url('')}}/"+uri[1],{kd_kecamatan:e.val()},{obj:'-- Semua Kelurahan --',key:'-'},async_status,true);
                return target;  
            };
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this);
                changeData(e);
            });
            $(document).on("click", ".btn-export", function (evt) {
                let e=$(this),
                    form=e.closest('form'),
                    url="{{url('laporan_penelitian/bulan_cetak')}}?"+toString(form.serializeArray());
                    window.open(url, '_blank');
            });
        });
    </script>
@endsection