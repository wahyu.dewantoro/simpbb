<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>Nomor Layanan</th>
            <th>Layanan</th>
            <th>Objek</th>
            <th>No SK</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
            <th>Keterangan</th>

            <th>User Input</th>
            <th>Penerima Berkas</th>
            <th>Peneliti</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @foreach ($data as $item)
            @php
                $nop =
                    $item->kd_propinsi .
                    '.' .
                    $item->kd_dati2 .
                    '.' .
                    $item->kd_kecamatan .
                    '.' .
                    $item->kd_kelurahan .
                    '.' .
                    $item->kd_blok .
                    '-' .
                    $item->no_urut .
                    '.' .
                    $item->kd_jns_op;
            @endphp
            @if ($item->nop_gabung == '' || $item->nop_gabung == null)
                @if ($item->jenis_layanan_id == '7')
                    @php $nop="-NOP GABUNG- "; @endphp
                @endif
            @else
                @if ($item->jenis_layanan_id == '6')
                    @php $nop="-NOP PECAH-"; @endphp
                @endif
            @endif
            @if ($item->jenis_layanan_id == '1')
                @php $nop="-NOP BARU-"; @endphp
            @endif
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $item->nomor_layanan }}</td>
                <td>{{ $item->jenis_layanan_nama }}
                </td>
                <td>{{ $nop }}</td>
                <td>
                    @if ($item->nomor_sk != '')
                        {{ $item->nomor_sk }}
                    @endif
                </td>
                <td>
                    {{ $item->nama_wp }}</td>
                <td>{{ $item->alamat_op }}</td>
                {{-- <td>{{ $item->jenis_layanan_nama }}</td> --}}
                <td>{{ $item->luas_bumi }}</td>
                <td>{{ $item->luas_bng }}</td>
                <td>{{ $item->keterangan_tolak }}

                </td>

                <td>{{ $item->usernameinput }}</td>
                <td>{{ $item->petugas }}</td>
                <td>{{ $item->petugaspeneliti }}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
