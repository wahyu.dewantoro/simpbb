<html>

<head>
    <title>LAPORAN NOTA PERHITUNGAN</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>LAPORAN NOTA PERHITUNGAN</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
<div style="width:75%; float:left">
    Nama User : {{$result['nama']}}
</div>
<div style="width:25%; float:left">
    Tanggal : {{$result['tanggal']}}
</div>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>Tanggal</th>
            <th>Nomor Layanan</th>
            <th>NOP</th>
            <th>Nama</th>
            <th>Kelurahan/Kecamatan</th>
            <th>Jenis Layanan</th>
            <th>User</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
       @foreach($result['data'] as $item)
            @php 
                $nop=$item->kd_propinsi.".".$item->kd_dati2.".".$item->kd_kecamatan.".".$item->kd_kelurahan.".".$item->kd_blok."-".$item->no_urut.".".$item->kd_jns_op;
            @endphp
            @if($item->nop_gabung==""||$item->nop_gabung==null)
                    @if($item->jenis_layanan_id=='7')
                        @php $nop="-NOP GABUNG- "; @endphp
                    @endif
                @else
                    @if($item->jenis_layanan_id=='6')
                        @php $nop="-NOP PECAH-"; @endphp
                    @endif
                @endif
                @if($item->jenis_layanan_id=='1')
                    @php $nop="-NOP BARU-"; @endphp
                @endif
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->deleted_at}}</td>
                <td>{{$item->nomor_layanan}}</td>
                <td>{{$nop}}</td>
                <td>{{$item->nama_wp}}</td>
                <td>{{$item->nm_kelurahan}}/{{$item->nm_kecamatan}}</td>
                <td>{{$item->jenis_layanan_nama}}</td>
                <td>{{$item->userdel}}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
</body>
</html>
