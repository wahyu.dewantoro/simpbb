<html>

<head>
    <title>LAPORAN BELUM BAYAR</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>LAPORAN BELUM BAYAR</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
<div style="width:25%; float:left">
    Kecamatan : {{$result['kecamatan']}}
</div>
<div style="width:25%; float:left">
    Kelurahan : {{$result['kelurahan']}}
</div>
<div style="width:25%; float:left">
    Jenis Layanan : {{$result['jenis_layanan']}}
</div>
<div style="width:25%; float:left">
    Tanggal : {{$result['tanggal']}}
</div>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>Nomor Layanan</th>
            <th>NOP</th>
            <th>Nama</th>
            <th>Alamat Lengkap</th>
            <th>No. Telp</th>
            <th>Tgl. Permohonan</th>
            <th>Jenis Layanan</th>
            <th>PBB</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; $total=0;@endphp
       @foreach($result['data'] as $item)
            @php 
                $nop=$item->kd_propinsi.".".$item->kd_dati2.".".$item->kd_kecamatan.".".$item->kd_kelurahan.".".$item->kd_blok."-".$item->no_urut.".".$item->kd_jns_op;
            @endphp
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->nomor_layanan}}</td>
                <td>{{$nop}}</td>
                <td>{{$item->nama_wp}}</td>
                <td>{{$item->alamat_op}}</td>
                <td>{{$item->telp_wp}}</td>
                <td>{{$item->tanggal_layanan}}</td>
                <td>{{$item->jenis_layanan_nama}}</td>
                <td>{{angka($item->nominal)}}</td>
            </tr>
            @php $no++; $total=$total+$item->nominal;@endphp
        @endforeach
        <tr>
                <td colspan="8">Total</td>
                <td>{{angka($total)}}</td>
            </tr>
    </tbody>
</table>
</body>
</html>
