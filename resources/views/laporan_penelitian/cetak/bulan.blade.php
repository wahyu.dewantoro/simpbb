<html>

<head>
    <title>LAPORAN PENELITIAN per BULANAN</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>LAPORAN PENELITIAN per BULANAN</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
<div style="width:75%; float:left">
    <div>Kecamatan : {{$result['kecamatan']}}</div>
    <div>Kelurahan : {{$result['kelurahan']}}</div>
</div>
<div style="width:25%; float:left">
    <div>Bulan : {{$result['bulan']}}</div>
    <div>tahun : {{$result['tahun']}}</div>
</div>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>Kecamatan</th>
            <th>Kelurahan</th>
            <th>Jenis Layanan</th>
            <th>Total</th>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
       @foreach($result['data'] as $item)
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->nm_kecamatan}}</td>
                <td>{{$item->nm_kelurahan}}</td>
                <td>{{$item->jenis_layanan_nama}}</td>
                <td>{{$item->total}}</td>
                <td>{{$item->luas_bumi}}</td>
                <td>{{$item->luas_bng}}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
</body>
</html>
