<html>

<head>
    <title>LAPORAN PERBANDINGAN NJOP</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>LAPORAN PERMOHONAN TANPA NOTA</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
<div style="width:25%; float:left">
    Tanggal : {{$result['tanggal']}}
</div>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number' rowspan='2'>No</th>
            <th  rowspan='2'>Nomor Layanan</th>
            <th  rowspan='2'>NOP</th>
            <th  colspan='2'>NJOP AWAL</th>
            <th  colspan='2'>NJOP AKHIR</th>
            <th  colspan='2'>PROSENTASE</th>
            <th  rowspan='2'>Peneliti</th>
        </tr>
        <tr>
            <th>Tanah</th>
            <th>Bangunan</th>
            <th>Tanah</th>
            <th>Bangunan</th>
            <th>Tanah</th>
            <th>Bangunan</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
       @foreach($result['data'] as $item)
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->nomor_layanan}}</td>
                <td>{{$item->nop}}</td>
                <td>{{$item->njop_bumi_meter_last}}</td>
                <td>{{$item->njop_bng_meter_last}}</td>
                <td>{{$item->njop_bumi_meter}}</td>
                <td>{{$item->njop_bng_meter}}</td>
                <td>{{$item->persen_bumi}}</td>
                <td>{{$item->persen_bng}}</td>
                <td>{{$item->pemutakhiran_nama}}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
</body>
</html>
