@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Laporan Nota perhitungan</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <form action="#nota_perhitungan_search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div class="input-group input-group-sm">
                                            <select name="list_user"  required class="form-control form-control-sm select"  data-placeholder="Pilih User">
                                            @if(count($listUser)>1) <option value="-">-- Semua User --</option> @endif
                                                @foreach ($listUser as $user)
                                                    <option value="{{ $user->id }}">
                                                        {{ $user->nama }} [ {{ $user->nik }} ]
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group-sm">
                                            <input type="checkbox" class="form-control form-control-sm"  name="all" value='all'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    Semua
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group input-group-sm">
                                            <input id="daterangepicker" type="text" class="form-control form-control-sm"  name="tgl" placeholder="Tanggal Pembuatan">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group-sm">
                                            <button type='submit' class='btn btn-sm btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group input-group-sm">
                                            <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Cetak </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-body no-padding p-1">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Tanggal</th>
                                            <th>Nomor Layanan</th>
                                            <th>NOP</th>
                                            <th>Nama</th>
                                            <th>Kelurahan/Kecamatan</th>
                                            <th>Jenis Layanan</th>
                                            <th>User</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'id':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('laporan_penelitian/nota_perhitungan_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'deleted_at'},
                    { data: 'nomor_layanan',class:'w-15'},
                    { data: 'nop'},
                    { data: 'nama_wp'},
                    { data: 'alamat'},
                    { data: 'jenis_layanan_nama',class:'w-20'},
                    { data: 'userdel'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let timer=60000;
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            setInterval( function () {
                resLoad();
            }, timer );
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    if(key.name!='_token'){
                        str.push(key.name+"="+key.value);
                    }
                });
                return str.join("&");
            }
            $(document).on("click", ".btn-export", function (evt) {
                let e=$(this),
                    form=e.closest('form'),
                    url="{{url('laporan_penelitian/nota_perhitungan_cetak')}}?"+toString(form.serializeArray());
                    window.open(url, '_blank');
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian Data.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    swal.close();
                });
            });
        });
    </script>
@endsection