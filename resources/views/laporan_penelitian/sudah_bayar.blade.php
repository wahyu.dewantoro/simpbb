@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Hasil Penelitian Sudah Bayar</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-12 ">
                <div class="card  ">
                    <div class="card-header">
                        <form action="#" class="form-inline" id="filter-form">
                            <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan"
                                required id="kd_kecamatan">
                                @if (count($kecamatan) > 1)
                                    <option value="">-- Kecamatan --</option>
                                @endif
                                @foreach ($kecamatan as $rowkec)
                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                        value="{{ $rowkec->kd_kecamatan }}">
                                        {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                    </option>
                                @endforeach
                            </select>
                            <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan"
                                id="kd_kelurahan">
                                <option value="">-- Kelurahan / Desa -- </option>
                            </select>

                            <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="permohonan"
                                required id="permohonan">
                                <option value="">-- Pelayanan --</option>
                                <option value="1">Individu</option>
                                <option value="3">Kolektif</option>
                            </select>
                            <input type="hidden" name="jenis" id="jenis" value="1">
                            <input type="hidden" name="bayar" required id="bayar" value="1">
                            <div class="float-right">
                                <div class="btn-group btn-group-sm  mb-2 mr-sm-2">
                                    <button id="cetak" class="btn btn-sm btn-warning"><i class="fas fa-print"></i>
                                        PDF</button>
                                    <button id="cetak_excel" class="btn btn-sm btn-success"><i class="fas fa-print"></i>
                                        Excel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body p-0 table-responsive-sm ">

                        <table class="table table-striped table-sm table-bordered table-hover table-checkable"
                            id="table-hasil" style="width:100%">
                            <thead class="bg-info">
                                <tr>
                                    <th style="width:1px !important;">No</th>
                                    <th style="text-align:center">NOPEL</th>
                                    <th style="text-align:center">nomor</th>
                                    <th style="text-align:center">kobil</th>
                                    <th style="text-align:center">NOP</th>
                                    {{-- <th style="text-align:center">nominal</th> --}}
                                    <th style="text-align:center">keterangan</th>
                                    {{-- <th></th> --}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail Hasil Penelitian</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0 m-0">
                </div>
            </div>
        </div>
    </div>
    <style>
        table {
            text-transform: uppercase;
        }
    </style>
@endsection

@section('script')
    <script src="{{ asset('js') }}/wilayah.js"></script>
    <script>
        $(document).ready(function() {

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })



            $('#kd_kecamatan').trigger('change')

            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                $('#kd_kelurahan').html(html);
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }


            let tableHasil = $('#table-hasil').DataTable({
                processing: true,
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                autoWidth: false,
                responsive: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ url('penelitian/nota-perhitungan') }}",
                    data: function(d) {
                        d.kd_kecamatan = $('#kd_kecamatan').val()
                        d.kd_kelurahan = $('#kd_kelurahan').val()
                        d.jenis = $('#jenis').val()
                        d.bayar = $('#bayar').val()
                        d.jenis_objek = $('#permohonan').val()
                    }
                },
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }, {
                        data: 'nomorlayanan',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'nomor',
                        orderable: false,
                        name: 'pencarian_data'
                    }, {
                        data: 'kobil',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'nop',
                        orderable: false,
                        searchable: false
                    }

                    , {
                        data: 'keterangan',
                        orderable: false,
                        searchable: false
                    }
                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10

            });

            $('#table-hasil').on('click', '.cetak-tagihan', function(e) {
                e.preventDefault()
                id = $(this).data('id')
                url = "{{ url('penelitian/nota-perhitungan-pdf') }}/" + id;
                window.open(url, 'newwindow', 'width=700,height=1000');
                return true;
            })

            $('#table-hasil').on('click', '.cetak-sk', function(e) {
                e.preventDefault()
                id = $(this).data('id')
                url = $(this).data('url')
                window.open(url, 'newwindow', 'width=700,height=1000');
                return true;
            })

            $('#table-hasil').on('click', '.cetak-sppt', function(e) {
                e.preventDefault()
                url = $(this).data('url')
                window.open(url, 'newwindow', 'width=700,height=1000');
                return true;
            })

            $('#table-hasil').on('click', '.hapus', function(e) {
                e.preventDefault()
                id = $(this).data('id')
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "data yang dihapus tidak dapat di kembalikan.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        document.location.href = "{{ url('penelitian/nota-perhitungan-hapus') }}/" +
                            id
                    }
                })
            })

            $('#cetak').on('click', function() {
                let jenis = $('#jenis').val()
                let bayar = $('#bayar').val()
                let jenis_objek = $('#permohonan').val()
                let kd_kecamatan = $('#kd_kecamatan').val()
                let kd_kelurahan = $('#kd_kelurahan').val()
                url_ = "{{ url('penelitian/nota-perhitungan-print') }}";
                url = url_ + "?jenis=" + jenis + "&bayar=" + bayar + "&jenis_objek=" + jenis_objek +
                    "&kd_kecamatan=" + kd_kecamatan + "&kd_kelurahan=" + kd_kelurahan;
                window.open(url, '_blank');
            });


            $('#cetak_excel').on('click', function() {
                let jenis = $('#jenis').val()
                let bayar = $('#bayar').val()
                let jenis_objek = $('#permohonan').val()
                let kd_kecamatan = $('#kd_kecamatan').val()
                let kd_kelurahan = $('#kd_kelurahan').val()
                url_ = "{{ url('penelitian/nota-perhitungan-excel') }}";
                url = url_ + "?jenis=" + jenis + "&bayar=" + bayar + "&jenis_objek=" + jenis_objek +
                    "&kd_kecamatan=" + kd_kecamatan + "&kd_kelurahan=" + kd_kelurahan;
                window.open(url, '_blank');
            });

            $(document).on('change', '.pencariandata', function() {
                pencarian();
            });

            function pencarian() {
                tableHasil.draw();
            }


        })
    </script>
@endsection
