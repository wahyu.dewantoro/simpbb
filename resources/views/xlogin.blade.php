<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo/favicon.ico') }}" />

    <!-- Title -->
    <title>SIMPBB P2</title>

    <!-- Meta Tags for SEO -->
    <meta name="description"
        content="SIMPBB adalah sistem pelayanan PBB P2 dari BAPENDA Kabupaten Malang, dirancang untuk memudahkan masyarakat dalam mengurus Pajak Bumi dan Bangunan.">
    <meta name="keywords" content="SIMPBB, PBB P2, pelayanan PBB, BAPENDA, Kabupaten Malang, sistem informasi pajak">
    <meta name="robots" content="index, follow">
    <meta name="author" content="BAPENDA Kabupaten Malang">

    <!-- Open Graph Tags -->
    <meta property="og:url" content="https://esppt.id/simpbb">
    <meta property="og:type" content="website">
    <meta property="og:title" content="SIMPBB P2 - Sistem Informasi Pelayanan PBB P2 Kabupaten Malang">
    <meta property="og:description"
        content="SIMPBB memudahkan masyarakat Kabupaten Malang dalam mengurus Pajak Bumi dan Bangunan.">
    <meta property="og:image" content="{{ asset('sipanji.png') }}">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="SIMPBB P2 - Sistem Informasi Pelayanan PBB P2 Kabupaten Malang">
    <meta name="twitter:description"
        content="SIMPBB memudahkan masyarakat Kabupaten Malang dalam mengurus Pajak Bumi dan Bangunan.">
    <meta name="twitter:image" content="{{ asset('sipanji.png') }}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <link href="{{ asset('lte/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Inline Styles -->
    <style>
        html,
        body {
            height: 100%;
        }

        .login-box {
            width: 460px;
            margin-top: -260px;
        }

        .hero-image {
            background: url("background.png") center no-repeat;
            background-size: cover;
            background-color: #cccccc;
        }
    </style>

    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "WebSite",
      "url": "https://esppt.id/simpbb",
      "name": "SIMPBB - Sistem Pelayanan Pajak Bumi dan Bangunan Kabupaten Malang",
      "description": "SIMPBB adalah layanan online resmi untuk mengurus Pajak Bumi dan Bangunan (PBB) P2 di Kabupaten Malang, cepat, mudah, dan aman.",
      "publisher": {
        "@type": "Organization",
        "name": "BAPENDA Kabupaten Malang",
        "logo": {
          "@type": "ImageObject",
          "url": "{{ asset('sipanji.png') }}"
        }
      },
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://esppt.id/simpbb?q={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
    </script>
</head>

<body class="hold-transition login-page hero-image">
    <!-- Flash Messages -->
    <span id="msg-success" data-msg="{!! session('success') !!}"></span>
    <span id="msg-error" data-msg="{!! session('error') !!}"></span>
    <span id="msg-warning" data-msg="{!! session('warning') !!}"></span>

    <!-- Login Box -->
    <div class="login-box">
        <div class="login-logo" style="margin-top: 80px">
            <img src="{{ asset('sipanji.png') }}" alt="SIMPBB Logo" width="50%">
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <form method="POST" action="{{ route('login') }}" autocomplete="off" id="form-login">
                    @csrf
                    <div class="input-group mb-3">
                        <input id="username" type="text" name="username" value="{{ old('username') }}"
                            class="form-control @error('username') is-invalid @enderror" placeholder="Masukkan username"
                            autofocus>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="fas fa-user-astronaut"></i>
                            </div>
                        </div>
                        @error('username')
                            <span class="text-danger text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="input-group mb-3">
                        <input id="password" type="password" name="password"
                            class="form-control @error('password') is-invalid @enderror"
                            placeholder="Masukkan password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="fas fa-key"></i>
                            </div>
                        </div>
                        @error('password')
                            <span class="text-danger text-sm">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-8">
                            @if (Route::has('register'))
                                <a href="{{ url('register') }}" class="text-center"><i class="fas fa-user-plus"></i>
                                    Register</a>
                            @endif
                        </div>
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-sm"><i
                                    class="fas fa-sign-in-alt"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <div class="text-center mt-4" style="color: #666;">
        <p style="font-weight: bold; margin: 0;">&copy; {{ date('Y') }} - <a href="https://sipanji.id/">SIPANJI</a>
            | BAPENDA KAB MALANG</p>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#form-login')[0].reset();

            toastr.options = {
                closeButton: false,
                progressBar: true,
                positionClass: "toast-top-right",
                timeOut: 5000,
                extendedTimeOut: 1000
            };

            const messages = {
                success: $('#msg-success').data('msg'),
                error: $('#msg-error').data('msg'),
                warning: $('#msg-warning').data('msg')
            };

            if (messages.success) toastr.success(messages.success);
            if (messages.error) toastr.error(messages.error);
            if (messages.warning) toastr.warning(messages.warning);
        });

        // Tawk.to Chat Script
        (function() {
            var Tawk_API = Tawk_API || {},
                Tawk_LoadStart = new Date();
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5f7e739a4704467e89f598db/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();

        // WhatsApp Button
        (function() {
            const options = {
                whatsapp: "+6281259357700",
                call_to_action: "Hubungi Kami",
                position: "left"
            };
            const proto = document.location.protocol,
                host = "getbutton.io",
                url = proto + "//static." + host;
            const s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            const x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
</body>

</html>
