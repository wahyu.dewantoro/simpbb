@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Program Pemutihan Denda Pajak</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools">
                                {{-- @can('add_kelompok_objek') --}}
                                <a href="{{ route('refrensi.pemutihan.create') }}" class="btn btn-primary btn-sm">
                                    <i class="fas fa-plus"></i> Tambah
                                </a>
                                {{-- @endcan --}}
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-hover table-sm dataTable no-footer">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center"  width="10px">No</th>
                                        <th class="text-left" >Program</th>
                                        <th class="text-left"  width="200px">Durasi</th>
                                        <th class="text-center"  width="50px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($data as $row)
                                        <tr>
                                            <td class="text-center">{{ $no }}</td>
                                            <td>{{ $row->nama_program }}
                                                <br>
                                                <small class="text-info">{{ $row->deskripsi }}</small>
                                            </td>
                                            <td>
                                                @php
                                                    $tgl_mulai = tglindo($row->tgl_mulai);
                                                    $tgl_selesai = tglindo($row->tgl_selesai);

                                                    $bulan_mulai = date('m', strtotime($row->tgl_mulai));
                                                    $bulan_selesai = date('m', strtotime($row->tgl_selesai));

                                                    $tahun_mulai = date('Y', strtotime($row->tgl_mulai));
                                                    $tahun_selesai = date('Y', strtotime($row->tgl_selesai));
                                                @endphp

                                                @if ($bulan_mulai === $bulan_selesai && $tahun_mulai === $tahun_selesai)
                                                    {{ date('d', strtotime($row->tgl_mulai)) }} -
                                                    {{ date('d', strtotime($row->tgl_selesai)) }}
                                                    {{ date('F Y', strtotime($row->tgl_mulai)) }}
                                                @else
                                                    {{ $tgl_mulai }} - {{ $tgl_selesai }}
                                                @endif
                                            </td>

                                            <td class="text-center">
                                                <a href="{{ route('refrensi.pemutihan.edit', $row->id) }}"><i
                                                        class="fas fa-edit text-info" title="Edit Data"></i> </a>
                                                <a href="{{ url('refrensi.pemutihan.destroy', $row->id) }}"
                                                    onclick="
                                                                    var result = confirm('Are you sure you want to delete this record?');
                                                                    if(result){
                                                                        event.preventDefault();
                                                                        document.getElementById('delete-form-{{ $row->id }}').submit();
                                                                    }"
                                                    title="Delete Data"><i class="fas fa-trash-alt text-danger"></i>
                                                </a>
                                                <form method="POST" id="delete-form-{{ $row->id }}"
                                                    action="{{ route('refrensi.pemutihan.destroy', [$row->id]) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </td>
                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
