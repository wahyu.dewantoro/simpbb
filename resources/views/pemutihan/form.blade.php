@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-10">
                <h1>{{ $title??'Form Pemutihan Denda Pajak' }}</h1>
            </div>
            <div class="col-sm-2">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body p-2">
                        <form action="{{ $data['action'] }}" method="post">
                            @csrf
                            @method($data['method'])

                            <div class="form-group">
                                <label for="nama_program">Nama Program</label>
                                <input type="text" name="nama_program" id="nama_program" class="form-control form-control-sm" value="{{ $data['pemutihan']->nama_program??'' }}" required>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <input type="text" name="deskripsi" id="deskripsi" class="form-control form-control-sm" value="{{ $data['pemutihan']->deskripsi??'' }}" required>
                            </div>
                            <div class="form-group">
                                <label for="tgl_mulai">Mulai</label>
                                <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control form-control-sm tanggal" value="{{ isset($data['pemutihan']->tgl_mulai)?date('d M Y',strtotime($data['pemutihan']->tgl_mulai)) :'' }}" required>
                            </div>
                            <div class="form-group">
                                <label for="tgl_selesai">Selesai</label>
                                
                                <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control form-control-sm tanggal" value="{{ isset($data['pemutihan']->tgl_selesai)?date('d M Y',strtotime($data['pemutihan']->tgl_selesai)) :''  }}" required>
                            </div>

                            <div class="float-right">
                                <button class="btn btn-float btn-info"><i class="fas fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
