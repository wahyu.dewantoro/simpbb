<form action="{{ route('tagihan-sppt.register.pembayaran') }}" method="POST">
    @csrf
    <table class="table table-sm table-bordered text-sm">
        <thead class="bg-info">
            <tr>
                <th>No</th>
                <th>Tahun Pajak</th>
                <th>Pokok</th>
                <th>Denda</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($data_tagihan as $item)
                @php
                    $item = (object) $item;
                    $total = 0;
                @endphp
                <input type="hidden" name="nop[]"
                    value="{{ $item->kd_propinsi .
                        $item->kd_dati2 .
                        $item->kd_kecamatan .
                        $item->kd_kelurahan .
                        $item->kd_blok .
                        $item->no_urut .
                        $item->kd_jns_op }}">
                <input type="hidden" name="tahun_pajak[]" value="{{ $item->tahun_pajak }}">
                <input type="hidden" name="nm_wp[]" value="{{ $item->nm_wp }}">
                <input type="hidden" name="pokok[]" value="{{ $item->pokok }}">
                <input type="hidden" name="denda[]" value="{{ $item->denda }}">
                <input type="hidden" name="total[]" value="{{ $item->total }}">
                <td class="text-center">{{ $no }}</td>
                <td>{{ $item->tahun_pajak }}</td>
                <td class="text-right">{{ angka($item->pokok) }}</td>
                <td class="text-right">{{ angka($item->denda) }}</td>
                <td class="text-right">{{ angka($item->total) }}</td>
                <tr>

                </tr>
                @php
                    $total += $item->total;
                    $no++;
                @endphp
            @endforeach
            @if (count($data_tagihan) == 0)
                <tr>
                    <td colspan="5" class="text-center">Tidak ada tagihan !</td>
                </tr>
            @endif
        </tbody>
    </table>
    <div class="float-right">
        @if (count($data_tagihan) > 0)
            <button class="btn btn-sm btn-flat btn-primary">Proses</button>
        @endif
    </div>
</form>
