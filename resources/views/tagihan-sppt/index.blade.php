@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cek Tagihan SPPT</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right"></div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline card-primary">
                        <div class="card-body p-1">
                            <!-- Tab navigation -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="form-search-tab" data-toggle="tab" href="#form-search"
                                        role="tab" aria-controls="form-search" aria-selected="true"><i
                                            class="fas fa-search"></i> Cek </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab"
                                        aria-controls="history" aria-selected="false"><i class="fas fa-history"></i>
                                        History</a>
                                </li>

                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="form-search" role="tabpanel"
                                    aria-labelledby="form-search-tab">
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <div class="input-group input-group-sm">
                                                @if (auth()->user()->is_wp() == false)
                                                    <input autofocus="true" required type="text" name="nop"
                                                        id="nop" autofocus="true"
                                                        value="{{ request()->get('nop') ?? '' }}"
                                                        class="form-control form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}"
                                                        placeholder="Masukan nomor objek pajak (NOP)">
                                                @else
                                                    <select name="nop" id="nop"
                                                        class="form-control select-nop form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}">
                                                        <option value=""></option>
                                                    </select>
                                                @endif
                                                <span class="input-group-append">
                                                    <button id="cek" type="button" class="btn btn-sm btn-success"> <i
                                                            class="fas fa-search"></i> </button>
                                                </span>
                                            </div>
                                            {{-- 
                                            <div class="input-group input-group-sm">


                                                <input required type="text" name="nop" id="nop"
                                                    value="{{ request()->get('nop') ?? '' }}"
                                                    class="form-control form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}"
                                                    placeholder="Masukan nomor objek pajak (NOP)">
                                                <span class="input-group-append">
                                                    <button id="cek" type="button" class="btn btn-sm btn-success">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </span>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div id="response" class="mt-2">
                                        <div class="alert alert-info alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true">&times;</button>
                                            <h5><i class="icon fas fa-info"></i> Info!</h5>
                                            Silakan memasukkan NOP untuk mengecek total tagihan PBB P2.
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">
                                    <div class="card-body p-0">
                                        <table class="table table-sm table-bordered text-sm mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width: 25px">No</th>
                                                    <th class="text-center" style="width: 150px">Nop</th>
                                                    <th class="text-center" style="width: 150px">Virtual Account</th>
                                                    <th class="text-center" style="width: 150px">Bill Number Qris</th>
                                                    <th class="text-center" style="width: 100px">Total</th>
                                                    <th class="text-center">Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $no = 1;
                                                @endphp
                                                @foreach ($data as $array)
                                                    <tr>
                                                        <td class="text-center">{{ $no }}</td>
                                                        <td>
                                                            {{ $array['nop'] }}
                                                            <small class="d-block">{{ $array['kobil'] }}</small>
                                                            <small class="d-block">{{ $array['nm_wp'] }}</small>
                                                        </td>
                                                        <td class="text-center">{{ $array['nomor_va'] }}</td>
                                                        <td class="text-center">{{ $array['bill_number'] }}</td>

                                                        <td class="text-right">{{ angka($array['total']) }}</td>

                                                        <td>
                                                            <small>{{ $array['status'] }} ({{ $array['jenis'] }})</small>
                                                            <small class="d-block">Masa berlaku:
                                                                {{ $array['expired_at'] }}</small>
                                                            @if ($array['jenis'] == 'Qris' && $array['status'] == 'Belum Lunas')
                                                                <a class="open-link"
                                                                    href="{{ route('pembayaran-qris.show', $array['qr_value']) }}">
                                                                    <i class="fas fa-money-check"></i> Bayar
                                                                </a>
                                                            @endif
                                                            @if ($array['jenis'] == 'Virtual Account' && $array['status'] == 'Belum Lunas')
                                                                <a class="open-link"
                                                                    href="{{ route('pembayaran-va.show', encrypt($array['data_billing_id'])) }}">
                                                                    <i class="fas fa-money-check"></i> Bayar
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @php $no++; @endphp
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('js') }}/wilayah.js"></script>
    <script>
        $(document).ready(function() {
            function openWindow(url) {
                var windowHeight = window.screen.height;
                var windowWidth = 600;
                window.open(url, 'newWindow', 'width=' + windowWidth + ',height=' + windowHeight +
                    ',top=0,left=100,scrollbars=yes');
            }

            $(document).on('click', '.open-link', function(event) {
                event.preventDefault();
                var hrefValue = $(this).attr('href');
                openWindow(hrefValue);
            });

            $('.select-nop').select2({
                placeholder: 'Cari nop . . .',
                allowClear: true,
                language: {
                    searching: function() {
                        return "Sedang mencari..."; // Ubah teks pencarian di sini
                    }
                },
                ajax: {
                    url: '{{ url('daftar-objek-wp') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term // parameter query untuk pencarian
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                templateResult: function(data) {
                    if (!data.id) {
                        return data.text;
                    }
                    // Tampilkan ID di depan nama di dropdown
                    return $('<span>' + formatnop(data.id) + ' - ' + data.text + '</span>');
                },
                templateSelection: function(data) {
                    if (!data.id) {
                        return data.text;
                    }
                    // Tampilkan ID di depan nama saat item dipilih
                    return formatnop(data.id) + ' - ' + data.text;
                }
            });

            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                imageColor: "#8080c0"
            });

            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });

            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });

            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $("#cek").click(function(event) {
                event.preventDefault();

                $.ajax({
                    url: window.location.href,
                    method: 'GET',
                    data: {
                        nop: $('#nop').val()
                    },
                    success: function(response) {
                        $("#response").html(response);
                    },
                    error: function(xhr, status, error) {
                        console.error("Error: " + error);
                        $("#response").html("Terjadi kesalahan: " + error);
                    }
                });
            });
        });
    </script>
@endsection
