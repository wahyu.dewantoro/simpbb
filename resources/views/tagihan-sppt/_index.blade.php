 <hr>
 @if ($hasil['Status']['ResponseCode'] != '00')
     <blockquote class="quote-danger"
         style="border-left: 5px solid #dc3545; background-color: #f8d7da; padding: 15px; border-radius: 5px; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);">
         <h5 style="color: #721c24; font-weight: bold;">
             <i class="fas fa-exclamation-triangle"></i> Warning
         </h5>
         <p style="color: #721c24;">
             NOP ini telah dikonversi menjadi nomor VA/QRIS/kode billing atau tidak ada tunggakan.
         </p>
     </blockquote>
 @else
     {{-- {{ route('tagihan.bayar') }} --}}
     <form action="{{ route('tagihan-sppt.register.pembayaran') }}" method="post">
         @csrf
         @method('post')
         <div class="row">
             <div class="col-md-12">
                 <div class="row">
                     <div class="col-md-12">
                         <input type="hidden" name="nama" value="{{ $hasil['Nama'] }}">

                         <table class="table-sm table-borderless">
                             <tbody>
                                 <tr>
                                     <td>NOP</td>
                                     <td>: {{ formatnop($hasil['Nop']) }}</td>
                                 </tr>
                                 <tr>
                                     <td>Wajib Pajak</td>
                                     <td>: {{ $hasil['Nama'] }}</td>
                                 </tr>
                             </tbody>
                         </table>

                     </div>
                     <div class="col-md-12">
                         <table class="table table-bordered table-sm text-sm">
                             <thead class="bg-success">
                                 <tr>
                                     <th class="text-center" width="10x"></th>
                                     <th class="text-center">Tahun</th>
                                     <th class="text-center">Pokok</th>
                                     <th class="text-center">Denda</th>
                                     <th class="text-center">Total</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 @php
                                     $no = 1;
                                     $pokok = 0;
                                     $denda = 0;
                                     $total = 0;
                                 @endphp

                                 @foreach ($hasil['Tagihan'] as $item)
                                     @php
                                         $item = (object) $item;

                                         $pokok += $item->Pokok;
                                         $denda += $item->Denda;
                                         $total += $item->Total;
                                     @endphp


                                     <input type="hidden" name="nop[]" value="{{ $hasil['Nop'] }}">
                                     <input type="hidden" name="tahun[{{ $item->Tahun }}]"
                                         value="{{ $item->Tahun }}">
                                     <input type="hidden" name="pokok[{{ $item->Tahun }}]"
                                         value="{{ $item->Pokok }}">
                                     <input type="hidden" name="denda[{{ $item->Tahun }}]"
                                         value="{{ $item->Denda }}">
                                     <input type="hidden" name="total[{{ $item->Tahun }}]"
                                         value="{{ $item->Total }}">
                                     <tr>
                                         <td class="text-center">

                                             <div class="form-check">
                                                 <input type="checkbox" disabled checked class="form-check-input">
                                                 <input
                                                     @if ($no == 1) style="display: none" readonly checked @endif
                                                     checked type="checkbox" id="id_{{ $no }}"
                                                     class="form-check-input ambil" name="gettahun[]"
                                                     value="{{ $item->Tahun }}" data-pokok="{{ $item->Pokok }}"
                                                     data-denda="{{ $item->Denda }}">
                                             </div>
                                         </td>
                                         <td class="text-center">{{ $item->Tahun }}</td>
                                         <td class="text-right">{{ number_format($item->Pokok, 0, ',', '.') }}
                                         </td>
                                         <td class="text-right">{{ number_format($item->Denda, 0, ',', '.') }}
                                         </td>
                                         <td class="text-right">{{ number_format($item->Total, 0, ',', '.') }}
                                         </td>
                                     </tr>
                                     @php
                                         $no++;
                                     @endphp
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
             <div class="col-md-12">
                 <i class="fas fa-exclamation text-warning"></i> Tagihan anda
                 <table class="table table-sm table-borderless">
                     <tbody>
                         <tr>
                             <th width="20%">Pokok</th>
                             <td width="1%">:</td>
                             <td class="text-right"><input type="text" readonly id="total_pokok"
                                     value="{{ $pokok }}" class="form-control form-control-sm"></td>
                         </tr>
                         <tr>
                             <th>Denda</th>
                             <td>:</td>
                             <td class="text-right"><input type="text" readonly class="form-control form-control-sm"
                                     value="{{ $denda }}" id="total_denda"></td>
                         </tr>

                         <tr>
                             <th>Total</th>
                             <td>:</td>
                             <td class="text-right">
                                 <input type="text" readonly class="form-control form-control-sm"
                                     value="{{ $total }}" id="total_all">

                             </td>
                         </tr>
                     </tbody>
                 </table>
                 <div class="form-group row">
                     <label for="metode" class="col-form-control col-md-6">Metode Pembayaran</label>
                     <div class="col-md-6">
                         <select name="metode" id="metode" class="form-control form-control-sm" required>
                             <option value="">Pilih</pilih>
                             <option value="1">VA BANK JATIM</option>
                             <option @if ($total > 10000000) disabled @endif id="qris" value="2">
                                 Qris
                             </option>
                         </select>
                     </div>
                 </div>
                 <div class="form-group">
                     <div class="float-right">
                         <button class="btn btn-sm btn-flat btn-success"><i class="fas fa-money-check"></i>
                             Bayar</button>
                     </div>
                 </div>
             </div>
         </div>
     </form>
 @endif

 <script>
     $(document).ready(function() {

         function formatRibuan(angka) {
             var angka = angka.toString(); // Konversi angka ke string
             var number_string = angka.replace(/[^,\d]/g, '').toString(),
                 split = number_string.split(','),
                 sisa = split[0].length % 3,
                 rupiah = split[0].substr(0, sisa),
                 ribuan = split[0].substr(sisa).match(/\d{3}/gi);

             // tambahkan titik jika yang di input sudah menjadi angka ribuan
             if (ribuan) {
                 separator = sisa ? '.' : '';
                 rupiah += separator + ribuan.join('.');
             }
             return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
         }


         // console.log("ready!");
         let v_pokok = parseInt('{{ $pokok ?? 0 }}')
         let v_denda = parseInt('{{ $denda ?? 0 }}')
         let v_total = v_pokok + v_denda

         $('#total_pokok').val(formatRibuan(v_pokok))
         $('#total_denda').val(formatRibuan(v_denda))
         $('#total_all').val(formatRibuan(v_total))

         cekQris(v_total)
         // console.log(v_pokok)

         $('.ambil').on('change', function(e) {
             let pokok = $(this).data('pokok')
             let denda = $(this).data('denda')
             if ($(this).is(':checked')) {
                 v_pokok = v_pokok + pokok;
                 v_denda = v_denda + denda;

             } else {
                 v_pokok = v_pokok - pokok;
                 v_denda = v_denda - denda;

             }


             let total = v_pokok + v_denda
             cekQris(total)
             $('#total_pokok').val(formatRibuan(v_pokok))
             $('#total_denda').val(formatRibuan(v_denda))
             $('#total_all').val(formatRibuan(total))
         })

         function cekQris(total) {
             if (total < 10000000) {
                 $('#qris').attr('disabled', false);
             } else {
                 $('#qris').attr('disabled', true);
             }
         }



     });
 </script>
