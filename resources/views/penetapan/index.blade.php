@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <h1>PENETAPAN MASAL SPPT</h1>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <form id="form-penetapan" action="{{ url('penetapan') }}" method="post">
                        <div class="card-body">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-grop">
                                        <label for="">Tahun</label>
                                        <select name="tahun_pajak" id="tahun_pajak" class="form-control form-control-sm">
                                            @for($i=date('Y'); $i<=date('Y')+1; $i++)
                                            <option value="{{ $i}}">{{ $i }}</option>
                                            @endfor
                                        </select>
 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Tanggal Terbit</label>
                                        <input type="text" class="form-control  form-control-sm" name="tgl_terbit" id="tgl_terbit" placeholder="Tanggal Terbit" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Tanggal Jatuh Tempo</label>
                                        <input type="text" class="form-control tanggal form-control-sm" name="tgl_jatuh_tempo" id="tgl_jatuh_tempo" placeholder="Tanggal Jatuh Tempo" value="{{ $jatuhtempo }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Kecamatan</label>
                                        <select class="form-control form-control-sm select2" name="kd_kecamatan" required id="kd_kecamatan" required data-placeholder="Kecamatan">
                                            <option value=""></option>
                                            @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->kd_kecamatan }} - {{ $rowkec->nm_kecamatan }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Kelurahan</label>
                                        <select class="form-control form-control-sm select2" name="kd_kelurahan" id="kd_kelurahan" required data-placeholder="Desa / Kelurahan">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Blok</label>
                                        <select name="kd_blok" id="kd_blok" class="form-control form-control-sm ">
                                            <option value="">-- blok --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No Urut</label>
                                        <input type="text" maxlength="4" name="no_urut" id="no_urut" class="form-control form-control-sm angka">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-sm table-counter">
                                        <thead>
                                            <tr>
                                                <th>Objek</th>
                                                <th>Sudah</th>
                                                <th>Belum</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><span id="count_objek" class="info-box-number">0</span></td>
                                                <td><span id="count_sudah" class="info-box-number">0</span></td>
                                                <td><span id="count_belum" class="info-box-number">0</span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="reset" class="btn btn-warning btn-sm btn-flat"><i class="fas fa-retweet"></i> Reset</button>
                            <button type="submit" disabled id="submit" class="btn btn-primary btn-sm btn-flat float-right"><i class="far fa-paper-plane"></i>
                                Proses</button>
                        </div>
                    </form>
                </div>

            </div>
            <div class="col-md-8">
                <div class="row">

                    {{-- <div class="col-md-3">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-globe-asia"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Objek</span>
                                
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-12">
                        <div id="checklist"></div>
                    </div>
                    {{-- <div id="hasilpencarian"></div> --}}
                </div>
            </div>

        </div>
</section>
@endsection
@section('css')
<style>
    .body-scroll {
        max-height: 400px;
        overflow-y: scroll;
        padding: 0px;
    }
</style>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $(".select2").select2({
            closeOnSelect: true,
            allowClear: true
        })

        $("#form-penetapan").on("submit", function(event) {
            openloading();
            event.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data akan mereplace data penetapan sebelumnya, baik yang belum lunas atau sudah lunas.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Yakin!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        url: "{{ route('penetapan.store') }}",
                        type: 'post',
                        data: $(this).serialize(),
                        success: function(res) {
                            // closeloading();
                            countobjek();
                            toastr.success(res.msg, 'PENETAPAN MASAL');


                        },
                        error: function(res) {
                            closeloading();
                            toastr.error('Sistem error', 'PENETAPAN MASAL');
                        }
                    })
                }
            })
        });

        $('#kd_blok').on('change', function() {
            $('#no_urut').val('');
        })


        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            $('#kd_blok').val('');
            $('#no_urut').val('');
            $('#kd_kelurahan').val('');

            getKelurahan(kk);
        })


        $('#kd_kelurahan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            var kel = $('#kd_kelurahan').val();
            $('#kd_blok').val('');
            $('#no_urut').val('');
            if (kk != '' && kel != '') {
                getblok(kk, kel);
            }

        })



        function getKelurahan(kk) {
            $('#kd_kelurahan').html('<option value=""></option>');
            if (kk != '') {
                $.ajax({
                    url: "{{ url('desa') }}",
                    data: {
                        'kd_kecamatan': kk
                    },
                    success: function(data) {
                        $.each(data, function(key, value) {
                            var newOption = new Option(key + '-' + value, key, true, true);
                            $('#kd_kelurahan').append(newOption)
                        });

                        $('#kd_kelurahan').val(null).trigger('change');
                    },
                    error: function(res) {

                    }
                });
            }



            // openloading();
            /* var html = '<option value="">-- pilih --</option>';
            if (kk != '') {
                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan').val(k);
                            }
                        });
                        // console.log(res);
                        $('#kd_kelurahan').html(html);
                        
                        // closeloading();
                    }
                    , error: function(res) {
                        $('#kd_kelurahan').html(html);
                        // closeloading();
                    }
                });
            } else {
                $('#kd_kelurahan').html(html);
                // closeloading();
            } */
        }

        function getblok(kd_kec, kd_kel) {

            var html = '<option value="">-- blok --</option>';
            if (kd_kec != '' && kd_kel != '') {
                // openloading();
                $.ajax({
                    url: "{{ url('desa-blok') }}",
                    data: {
                        'kd_kecamatan': kd_kec,
                        'kd_kelurahan': kd_kel
                    },
                    success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + v +
                                '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_blok').val(k);
                            }
                        });

                        $('#kd_blok').html(html);

                        // $('#no_urut').val('');
                    },
                    error: function(res) {
                        $('#kd_blok').html(html);
                        // closeloading();
                    }
                });
            } else {
                $('#kd_blok').html(html);
                closeloading();
            }
        }

        function countobjek() {
            $('#count_objek').html('0')
            $('#count_sudah').html('0')
            $('#count_belum').html('0')
            kd_kecamatan = $('#kd_kecamatan').val()
            kd_kelurahan = $('#kd_kelurahan').val()
            kd_blok = $('#kd_blok').val()
            no_urut = $('#no_urut').val()
            tahun = $('#tahun_pajak').val()
            // openloading()
            $('#submit').prop('disabled', true);
            Swal.fire({
                // icon: 'error',
                title: '<i class="fas fa-sync fa-spin"></i>',
                text: "Sedang menghitung objek",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false
            })
            $.ajax({
                url: "{{ url('count-objek') }}",
                data: {
                    'kd_kecamatan': kd_kecamatan,
                    'kd_kelurahan': kd_kelurahan,
                    'kd_blok': kd_blok,
                    'no_urut': no_urut,
                    'tahun': tahun
                },
                success: function(res) {
                    $('#count_objek').html(res.jumlah)
                    $('#count_sudah').html(res.sudah)
                    $('#count_belum').html(res.belum)

                    if (res.jumlah == '0') {
                        $('#submit').prop('disabled', true);
                    } else {
                        $('#submit').prop('disabled', false);
                    }


                    closeloading();
                },
                error: function(res) {
                    $('#count_objek').html('0')
                    $('#count_sudah').html('0')
                    $('#count_belum').html('0')
                    $('#submit').prop('disabled', true);
                    closeloading();

                }
            });

        }

        $('#tahun_pajak').on('change', function(e) {
            e.preventDefault();
            if ($('#tahun_pajak').val().length == 4) {
                $.ajax({
                    url: "{{ route('penetapan.index') }}?ubah_tahun=1",
                    data: {
                        'tahun': $('#tahun_pajak').val()
                    },
                    success: function(res) {

                        // $('#tgl_terbit').val(res.terbit)
                        $('#tgl_jatuh_tempo').val(res.jatuhtempo)
                    },
                    error: function(res) {
                        $('#tahun_pajak').val('')
                        // $('#tgl_terbit').val('')
                        $('#tgl_jatuh_tempo').val('')
                    }
                });

                $.ajax({
                    url: "{{ route('penetapan.index') }}?checklist=1",
                    data: {
                        'tahun': $('#tahun_pajak').val()
                    },
                    success: function(res) {
                        $('#checklist').html(res)
                    },
                    error: function(res) {
                        $('#checklist').html('');
                    }
                });

                // 

            } else {
                // $('#tgl_terbit').val('')
                $('#tgl_jatuh_tempo').val('')
            }
        })

        const d = new Date();
        let year = d.getFullYear();

        $('#tgl_terbit').daterangepicker({
            "singleDatePicker": true,
            autoApply: true,
            minDate: "01/01/" + year,
        });


        $('#kd_kecamatan, #kd_kelurahan, #kd_blok').on('change', function() {
            var kec = $('#kd_kecamatan').val()
            var kel = $('#kd_kelurahan').val()
            if (kec != '' && kel != '') {
                countobjek()
            }

        })


        $('#no_urut').on('keyup', function() {
            countobjek()
        })
        // count_objek


        $('#tahun_pajak').trigger('change')

    });
</script>
@endsection