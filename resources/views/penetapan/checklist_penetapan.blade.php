<style>
    ul.demo {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

</style>
<div class="card card-cyan">
    <div class="card-header">
        <div class="card-title">Checklist Penetapan</div>
    </div>
    <div class="card-body">
        <div class="row">
            @php
                $no=1;
            @endphp
            @foreach ($data as $row)
            <div class="col-md-3">
                <ul class="demo">
                    @foreach ($row as $item)
                    <li>{{ $no }}. {{ $item->jenis}} @if($item->value>0)<i class="fas fa-check text-sm text-success"></i> @else <i class="fas fa-times text-sm text-danger"></i> @endif </li>
                    @php
                    $no++;
                @endphp
                    @endforeach
                </ul>
            </div>
            @endforeach
        </div>
    </div>
</div>
