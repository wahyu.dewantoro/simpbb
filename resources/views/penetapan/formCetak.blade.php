@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <h1>CETAK MASAL SPPT</h1>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <form id="form-cetak" action="{{ route('cetak.generate') }}" method="post">
                        <div class="card-body">

                            @csrf
                            @method('post')

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-grop">
                                        <label for="">Tahun</label>
                                        <input type="text" class="form-control form-control-sm angka" maxlength="4" name="tahun_pajak" id="tahun_pajak" placeholder="Tahun Pajak" value="" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="buku">Buku</label>
                                        <select name="buku" id="buku" class="form-control form-control-sm">
                                            <option value="">Semua Buku</option>
                                            <option value="1">Buku 1 & 2</option>
                                            <option value="2">Buku 3,4 & 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Kecamatan</label>
                                        <select class="form-control form-control-sm" name="kd_kecamatan" required id="kd_kecamatan" required>
                                            <option value="">-- Pilih --</option>
                                            @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->kd_kecamatan }} - {{ $rowkec->nm_kecamatan }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Kelurahan</label>
                                        <select class="form-control form-control-sm" name="kd_kelurahan" id="kd_kelurahan" required>
                                            <option value="">-- Kelurahan / Desa -- </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Blok</label>
                                        {{-- <input type="text" maxlength="3" > --}}
                                        <select name="kd_blok" id="kd_blok" class="form-control form-control-sm ">
                                            <option value="">-- blok --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No Urut</label>
                                        <input type="text" maxlength="10" name="no_urut" id="no_urut" class="form-control form-control-sm angka_dua">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <span class="text-danger">
                                        Catatan : Untuk mencetak beberapa lembar SPPT pada blok yang sama bisa di lakukan dengan memasukan tanda "-", contoh : 1-100
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="reset" class="btn btn-warning btn-sm btn-flat"><i class="fas fa-retweet"></i> Reset</button>
                            <button type="submit" id="submit" class="btn btn-primary btn-sm btn-flat float-right"><i class="far fa-paper-plane"></i> Proses</button>
                        </div>
                    </form>
                </div>

            </div>
            <div class="col-md-8">
                <div id="preview_hasil"></div>

                {{-- <iframe  height="100%" width="100%"  src="{{ $resfile }}" frameborder="0"></iframe> --}}
            </div>

        </div>
</section>
@endsection
@section('css')
<style>
    .body-scroll {
        max-height: 400px;
        overflow-y: scroll;
        padding: 0px;
    }

</style>
@endsection
@section('script')
<script>
    $(document).ready(function() {

        $('.angka_dua').keypress(function(evt) {
            return (/^[0-9]*\-?[0-9]*$/).test($(this).val() + evt.key);
        });

        $("#form-cetak").on("submit", function(event) {
            openloading();
            event.preventDefault();
            $.ajax({
                url: "{{ route('cetak.generate') }}"
                , type: 'post'
                , data: $(this).serialize()
                , success: function(res) {
                    closeloading();
                    toastr.success('Generate file e sppt telah selesai', 'GENERATE E SPPT');
                    $('#preview_hasil').html(res)
                }
                , error: function(res) {
                    closeloading();
                    toastr.error('Gagal generate file', 'GENERATE E SPPT');
                    $('#preview_hasil').html("")
                }
            })
        });



        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            getKelurahan(kk);
        })


        $('#kd_kelurahan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            var kel = $('#kd_kelurahan').val();
            getblok(kk, kel);
        })

        var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
        if (kd_kecamatan == '') {
            var kd_kecamatan = $('#kd_kecamatan').val()
        }
        getKelurahan(kd_kecamatan);

        function getKelurahan(kk) {
            openloading();
            var html = '<option value="">-- pilih --</option>';
            if (kk != '') {
                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan').val(k);
                            }
                        });
                        // console.log(res);
                        $('#kd_kelurahan').html(html);
                        /* if (count != 1) {
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        } */
                        closeloading();
                    }
                    , error: function(res) {
                        $('#kd_kelurahan').html(html);
                        closeloading();
                    }
                });
            } else {
                $('#kd_kelurahan').html(html);
                closeloading();
            }
        }

        function getblok(kd_kec, kd_kel) {

            var html = '<option value="">-- blok --</option>';
            if (kd_kec != '' && kd_kel != '') {
                openloading();
                $.ajax({
                    url: "{{ url('desa-blok-sppt') }}"
                    , data: {
                        'thn_pajak_sppt': $('#tahun_pajak').val()
                        , 'kd_kecamatan': kd_kec
                        , 'kd_kelurahan': kd_kel
                    }
                    , success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + v +
                                '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_blok').val(k);
                            }
                        });

                        $('#kd_blok').html(html);

                        closeloading();
                    }
                    , error: function(res) {
                        $('#kd_blok').html(html);
                        closeloading();
                    }
                });
            } else {
                $('#kd_blok').html(html);
                closeloading();
            }
        }

        function countobjek() {
            $('#count_objek').html('0')
            kd_kecamatan = $('#kd_kecamatan').val()
            kd_kelurahan = $('#kd_kelurahan').val()
            kd_blok = $('#kd_blok').val()
            no_urut = $('#no_urut').val()

            $.ajax({
                url: "{{ url('count-objek') }}"
                , data: {
                    'kd_kecamatan': kd_kecamatan
                    , 'kd_kelurahan': kd_kelurahan
                    , 'kd_blok': kd_blok
                    , 'no_urut': no_urut
                }
                , success: function(res) {
                    $('#count_objek').html(res)
                    // closeloading();
                }
                , error: function(res) {
                    $('#count_objek').html('0')
                    // closeloading();
                }
            });

        }


        $('#kd_kecamatan, #kd_kelurahan, #kd_blok').on('change', function() {
            // countobjek()
        })


        $('#no_urut').on('keyup', function() {
            // countobjek()
        })

        $('#tahun_pajak').val('')


    });

</script>
@endsection
