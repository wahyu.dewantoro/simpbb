@extends('layouts.app')
@section('css')
    <style>
        .toolbar {
            float: left;
        }
    </style>
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.0/css/buttons.dataTables.min.css"> --}}
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Piutang</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card ">
                        {{-- <div class="card-header">
                            <div class="row">
                                <div class="col-4 col-md-2">
                                    <select name="thn_pajak_sppt" id="thn_pajak_sppt"
                                        class="form-control form-control-sm pencariandata" required>
                                        @foreach ($tahun as $rt)
                                            <option value="{{ $rt }}">{{ $rt }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 col-md-2">
                                    <select name="status_objek" id="status_objek"
                                        class="form-control form-control-sm pencariandata" required>
                                        <option value="">Status Objek</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Non Aktif</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select name="kd_kecamatan" id="kd_kecamatan"
                                        class="form-control form-control-sm pencariandata" required>
                                        @if (count($kecamatan) > 1)
                                            <option value="">-- Kecamatan --</option>
                                        @endif
                                        @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select name="kd_kelurahan" id="kd_kelurahan"
                                        class="form-control form-control-sm pencariandata" required>
                                        <option value="">-- Kelurahan / Desa --</option>
                                    </select>
                                </div>

                            </div>
                        </div> --}}

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="text" id="tanggal" class="form-control pencariandata form-control-sm">
                                </div>
                            </div>
                            <table id="table" class="table table-sm text-sm">
                                <thead>
                                    <tr>
                                        <th>Tahun</th>
                                        <th>Ketetapan PBB</th>
                                        <th>Insentif</th>
                                        <th>Terbayar</th>
                                        <th>Koreksi</th>
                                        <th>Salso</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function(e) {
            $('#tanggal').daterangepicker({
                "singleDatePicker": true,
                "showDropdowns": true,
                "autoApply": true,
                "linkedCalendars": false,
                "showCustomRangeLabel": false,
                "minDate": "12/31/2022"
            });

            var table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                searchable: false,
                ajax: {
                    url: "{{ url('informasi/piutang') }}",
                    data: function(d) {
                        d.tanggal = $('#tanggal').val()
                        /*   d.kd_kecamatan = $('#kd_kecamatan').val()
                          d.kd_kelurahan = $('#kd_kelurahan').val()
                          d.thn_pajak_sppt = $('#thn_pajak_sppt').val()
                          d.status_objek = $('#status_objek').val() */
                    }
                },
                columns: [{
                        data: 'tahun_piutang',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'pbb',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'potongan',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'bayar',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'koreksi',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'saldo',
                        orderable: false,
                        searchable: false
                    },
                    //  {
                    //         data: 'nop',
                    //         name: 'nop',
                    //         orderable: false,
                    //     },
                    //     {
                    //         data: 'is_aktif',
                    //         name: 'is_aktif',
                    //         orderable: false,
                    //     },
                    //     {
                    //         data: 'nm_kecamatan',
                    //         name: 'nm_kecamatan',
                    //         orderable: false,
                    //     },
                    //     {
                    //         data: 'nm_kelurahan',
                    //         name: 'nm_kelurahan',
                    //         orderable: false,
                    //     },
                    //     // {
                    //     //     data: 'alamat_op',
                    //     //     name: 'alamat_op',
                    //     //     orderable: false,
                    //     // },
                    //     {
                    //         data: 'tahun_pajak',
                    //         name: 'tahun_pajak',
                    //         orderable: false,
                    //     },
                    //     {
                    //         data: 'nm_wp_sppt',
                    //         name: 'nm_wp_sppt',
                    //         orderable: false,
                    //     },
                    //     // {
                    //     //     data: 'alamat_wp',
                    //     //     name: 'alamat_wp',
                    //     //     orderable: false,
                    //     // },
                    //     {
                    //         data: 'pbb',
                    //         name: 'pbb',
                    //         orderable: false,
                    //     },
                    //     {
                    //         data: 'bayar',
                    //         name: 'bayar',
                    //         orderable: false,
                    //     },
                    //     {
                    //         data: 'kurang_bayar',
                    //         name: 'kurang_bayar',
                    //         orderable: false,
                    //     },

                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                // dom: '<"toolbar">frtip',
                // initComplete: function() {
                //     $("div.toolbar")
                //         .html(
                //             '<button class="btn btn-sm btn-success" type="button" id="cetak"><i class="far fa-file-alt"></i> Excell</button>'
                //         );
                // },
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });
            $(document).on('change', '.pencariandata', function() {
                pencarian();
            });

            function pencarian() {
                table.draw();
            }


            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })



            $('#kd_kecamatan').trigger('change')




            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                $('#kd_kelurahan').html(html);
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            $('#kd_kelurahan').trigger('change')
                            /* if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            } */
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }


        })
    </script>
@endsection
