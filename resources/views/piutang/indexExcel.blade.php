<table class="table table-sm table-bordered table-hover">
    <thead style="vertical-align: middle; text-align:center">
        <tr>
            
            <th align="center" rowspan="2"><b>Kecamatan</b></th>
            <th align="center" colspan="2"><b>{{ $tahun }}</b></th>
            <th align="center" colspan="2"><b>{{ $tahun - 1 }}</b></th>
            <th align="center" colspan="2"><b>{{ $tahun - 2 }}</b></th>
            <th align="center" colspan="2"><b>{{ $tahun - 3 }}</b></th>
            <th align="center" colspan="2"><b>{{ $tahun - 4 }}</b></th>
        </tr>
        <tr>
            <th align="center"><b>Objek</b></th>
            <th align="center"><b>Piutang</b></th>
            <th align="center"><b>Objek</b></th>
            <th align="center"><b>Piutang</b></th>
            <th align="center"><b>Objek</b></th>
            <th align="center"><b>Piutang</b></th>
            <th align="center"><b>Objek</b></th>
            <th align="center"><b>Piutang</b></th>
            <th align="center"><b>Objek</b></th>
            <th align="center"><b>Piutang</b></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($piutang as $item)
            <tr>
                <td>{{ $item->nm_kecamatan }}</td>
                <td align="center">{{ number_format($item->obj_a, 0, '', '') }}</td>
                <td align="right">{{ number_format($item->nilai_a, 0, '', '') }}
                </td>
                <td align="center">{{ number_format($item->obj_b, 0, '', '') }}</td>
                <td align="right">{{ number_format($item->nilai_b, 0, '', '') }}
                </td>
                <td align="center">{{ number_format($item->obj_c, 0, '', '') }}</td>
                <td align="right">{{ number_format($item->nilai_c, 0, '', '') }}
                </td>
                <td align="center">{{ number_format($item->obj_d, 0, '', '') }}</td>
                <td align="right">{{ number_format($item->nilai_d, 0, '', '') }}
                </td>
                <td align="center">{{ number_format($item->obj_e, 0, '', '') }}</td>
                <td align="right">{{ number_format($item->nilai_e, 0, '', '') }}
                </td>
            </tr>

        @endforeach
    </tbody>
</table>
