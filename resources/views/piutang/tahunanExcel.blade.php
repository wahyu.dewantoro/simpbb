<table>
    <thead>
        <tr>
         <th>NOP</th>
         <th>Alamat Objek</th>
         <th>Tahun</th>
         <th>Wajib Pajak</th>
         <th>Alamat</th>
         <th>PBB</th>
         <th>Buku</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
          <tr>
              <td>
                {{ $row->kd_propinsi}}.{{ $row->kd_dati2}}.{{ $row->kd_kecamatan}}.{{ $row->kd_kelurahan}}.{{ $row->kd_blok}}-{{ $row->no_urut}}.{{ $row->kd_jns_op}}
              </td>
              <td>
              {{ $row->jalan_op }} {{ $row->blok_kav_no_op }} {{ $row->nm_kelurahan }} {{ $row->nm_kecamatan }} 
              </td>
              <td>{{ $row->thn_pajak_sppt }}</td>
              <td>{{ $row->nm_wp_sppt }}</td>
              <td>
                {{ $row->jln_wp_sppt }} {{ $row->blok_kav_no_wp_sppt }}  {{ $row->kelurahan_wp_sppt }}  {{ $row->kota_wp_sppt }} 
              </td>
              <td align="right">{{ $row->pbb }}</td>
              <td>{{ $row->buku }}</td>
          </tr>
        @endforeach
    </tbody>
</table>