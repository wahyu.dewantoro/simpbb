


<table class="table table-sm table-bordered table-hover">
    <thead style="vertical-align: middle; text-align:center">
        <tr>
            <th  colspan="11">Data Piutang {{ $kelurahan != '' ? $kelurahan->nm_kelurahan : '' }} {{ $kecamatan != '' ? $kecamatan->nm_kecamatan : '' }}</th>
        </tr>
        
        <tr>
            <th align="center" rowspan="2">NOP</th>
            <th align="center" rowspan="2">Alamat Objek</th>
            <th align="center" rowspan="2">Kelurahan</th>
            <th align="center" rowspan="2">Kecamatan</th>
            <th align="center" colspan="4">{{ $tahun }}</th>
            <th align="center" colspan="4">{{ $tahun - 1 }}</th>
            <th align="center" colspan="4">{{ $tahun - 2 }}</th>
            <th align="center" colspan="4">{{ $tahun - 3 }}</th>
            <th align="center" colspan="4">{{ $tahun - 4 }}</th>
        </tr>
        <tr>
            <th align="center">Wajib Pajak</th>
            <th align="center">Alamat WP</th>
            <th align="center">Buku</th>
            <th align="center">Piutang</th>
            <th align="center">Wajib Pajak</th>
            <th align="center">Alamat WP</th>
            <th align="center">Buku</th>
            <th align="center">Piutang</th>
            <th align="center">Wajib Pajak</th>
            <th align="center">Alamat WP</th>
            <th align="center">Buku</th>
            <th align="center">Piutang</th>
            <th align="center">Wajib Pajak</th>
            <th align="center">Alamat WP</th>
            <th align="center">Buku</th>
            <th align="center">Piutang</th>
            <th align="center">Wajib Pajak</th>
            <th align="center">Alamat WP</th>
            <th align="center">Buku</th>
            <th align="center">Piutang</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($piutang as $item)
            <tr>
                <td>{{ formatNop($item->kd_propinsi . $item->kd_dati2 . $item->kd_kecamatan . $item->kd_kelurahan . $item->kd_blok . $item->no_urut . $item->kd_jns_op) }}
                </td>
                <td>{{ $item->alamat_op }}</td>
                <td>{{ $item->nm_kelurahan }}</td>
                <td>{{ $item->nm_kecamatan }}</td>
                <td >{{ $item->nama_a }} </td>
                <td>{{ $item->alamat_a }}</td>
                <td>{{ $item->buku_a }}</td>
                <td align="right">{{ $item->a != '' ? number_format($item->a, 0, '', '') : null }}
                </td>
                <td >{{ $item->nama_b }} </td>
                <td>{{ $item->alamat_b }}</td>
                <td>{{ $item->buku_b }}</td>
                <td align="right">{{ $item->b != '' ? number_format($item->b, 0, '', '') : null }}
                </td>
                <td >{{ $item->nama_c }} </td>
                <td>{{ $item->alamat_c }}</td>
                <td>{{ $item->buku_c }}</td>
                <td align="right">{{ $item->c != '' ? number_format($item->c, 0, '', '') : null }}
                </td>
                <td >{{ $item->nama_d }} </td>
                <td>{{ $item->alamat_d }}</td>
                <td>{{ $item->buku_d }}</td>
                <td align="right">{{ $item->d != '' ? number_format($item->d, 0, '', '') : null }}
                </td>
                <td >{{ $item->nama_e }} </td>
                <td>{{ $item->alamat_e }}</td>
                <td>{{ $item->buku_e }}</td>
                <td align="right">{{ $item->e != '' ? number_format($item->e, 0, '', '') : null }}
                </td>

            </tr>

        @endforeach
    </tbody>
</table>
