@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Piutang</h1>
            </div>

        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <form action="{{ url('informasi/piutang-tiga-empat-lima') }}">
                            <div class="row">
                                <div class="col-4 col-md-1">
                                    <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="thn_awal" required id="thn_awal">
                                        <option value="">.: Tahun Awal :.</option>
                                        @foreach ($list_tahun as $rt)
                                        <option @if($thn_awal==$rt) selected @endif value="{{ $rt}}">{{ $rt }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 col-md-1">
                                    <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="thn_akhir" required id="thn_akhir">
                                        <option value="">.: Tahun Akhir :.</option>
                                        @foreach ($list_tahun as $ra)
                                        <option @if($thn_akhir==$ra) selected @endif value="{{ $ra}}">{{ $ra }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 col-md-1">
                                    <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_buku" id="kd_buku">
                                        <option value="">.: Buku :.</option>
                                        <option @if($kd_buku=='1' ) selected @endif value="1">1 & 2</option>
                                        <option @if($kd_buku=='2' ) selected @endif value="2">3,4 & 5</option>
                                    </select>
                                </div>
                                <div class="col-4 col-md-3">
                                    <button class="btn btn-sm btn-primary">Cari</button>
                                    <button type="button" id="excell" class="btn btn-sm btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i> excell</button>
                                    {{-- <a href="{{ route('informasi.piutang.345.excel') }}?tahun={{ $tahun }}" class="btn btn-sm btn-flat btn-success"> <i class="fa fa-file-excel-o" aria-hidden="true"></i> excell</a> --}}
                                    {{-- <div class="float-sm-right"> --}}

                                    {{-- </div> --}}
                                </div>

                            </div>
                        </form>
                        <table class="table table-sm table-bordered text-sm" id="table">
                            <thead>
                                <tr>
                                    <th>NOP</th>
                                    @for($thn=$thn_awal;$thn<=$thn_akhir;$thn++) <th class="text-center">{{ $thn }}</th>
                                        @endfor
                                        <th class="text-center">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                @php
                                $arr=(array)$row;
                                $jml=0;
                                @endphp
                                <tr>
                                    <td>
                                        {{ $row->kd_propinsi }}.{{ $row->kd_dati2 }}.{{ $row->kd_kecamatan }}.{{ $row->kd_kelurahan }}.{{ $row->kd_blok }}.{{ $row->no_urut }}.{{ $row->kd_jns_op }}
                                    </td>
                                    @for($thn=$thn_awal;$thn<=$thn_akhir;$thn++) <td class="text-right">
                                        @php
                                        $jml +=$arr['pbb_'.$thn]!=''?$arr['pbb_'.$thn]:0;
                                        @endphp
                                        {{ $arr['pbb_'.$thn]!=''?angka($arr['pbb_'.$thn]):null }}</td>
                                        @endfor
                                        <td class="text-right">
                                            {{ angka($jml) }}
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- Halaman : {{ $pegawai->currentPage() }} --}}
                        {{-- Jumlah Data : {{ $pegawai->total() }} <br /> --}}
                        {{-- Data Per Halaman : {{ $pegawai->perPage() }} <br /> --}}


                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#excell').on('click', function(e) {
            e.preventDefault()
            var thn_awal = $('#thn_awal').val()
            var thn_akhir = $('#thn_akhir').val()
            var kd_buku = $('#kd_buku  ').val()
            var url = "{{ route('informasi.piutang.345.excel') }}?thn_awal=" + thn_awal + "&thn_akhir=" + thn_akhir + "&kd_buku=" + kd_buku

            window.open(url, '_blank');
        })
    })

</script>
@endsection
