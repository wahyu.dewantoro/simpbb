<table>
    <thead>
        <tr>
            <th>NOP</th>
            @for($thn=$thn_awal;$thn<=$thn_akhir;$thn++) <th class="text-center">PBB {{ $thn }}</th>
                                        @endfor
                                        <th class="text-center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
        @php
        $arr=(array)$row;
        $jml=0;
        @endphp
        <tr>
            <td>
                {{ $row->kd_propinsi }}.{{ $row->kd_dati2 }}.{{ $row->kd_kecamatan }}.{{ $row->kd_kelurahan }}.{{ $row->kd_blok }}.{{ $row->no_urut }}.{{ $row->kd_jns_op }}
            </td>
            @for($thn=$thn_awal;$thn<=$thn_akhir;$thn++) <td class="text-right">
                @php
                $jml +=$arr['pbb_'.$thn]!=''?$arr['pbb_'.$thn]:0;
                @endphp
                {{ $arr['pbb_'.$thn]!=''?($arr['pbb_'.$thn]):null }}</td>
                @endfor
                <td class="text-right">
                    {{ ($jml) }}
                </td>
        </tr>
        @endforeach
    </tbody>
</table>
