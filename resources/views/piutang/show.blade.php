@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Piutang {{ $kecamatan->nm_kecamatan }} Buku {{ $buku }}</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">

                            <div class="card-tools">
                                <a href="{{ url('informasi/piutang-cetak-detail') }}?kecamatan={{ $kecamatan->kd_kecamatan }}&turut={{ $turut }}&buku={{ $buku }}"
                                    class="btn btn-sm btn-flat btn-success"> <i class="fas fa-file-excel"></i> Excel</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-sm table-bordered table-hover">
                                <thead style="vertical-align: middle; text-align:center">
                                    <tr>
                                        <th rowspan="2">Nop</th>
                                        <th colspan="2">{{ $tahun }}</th>
                                        <th colspan="2">{{ $tahun - 1 }}</th>
                                        <th colspan="2">{{ $tahun - 2 }}</th>
                                        <th colspan="2">{{ $tahun - 3 }}</th>
                                        <th colspan="2">{{ $tahun - 4 }}</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">WP</th>
                                        <th class="text-center">Piutang</th>
                                        <th class="text-center">WP</th>
                                        <th class="text-center">Piutang</th>
                                        <th class="text-center">WP</th>
                                        <th class="text-center">Piutang</th>
                                        <th class="text-center">WP</th>
                                        <th class="text-center">Piutang</th>
                                        <th class="text-center">WP</th>
                                        <th class="text-center">Piutang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($piutang as $item)
                                        <tr>
                                            <td>{{ formatNop($item->kd_propinsi . $item->kd_dati2 . $item->kd_kecamatan . $item->kd_kelurahan . $item->kd_blok . $item->no_urut . $item->kd_jns_op) }}
                                            </td>
                                            <td class="text-left">{{ $item->nama_a }} </td>
                                            <td class="text-right">
                                                {{ $item->a != '' ? number_format($item->a, 0, '', '.') : null }}
                                            </td>
                                            <td class="text-left">{{ $item->nama_b }} </td>
                                            <td class="text-right">
                                                {{ $item->b != '' ? number_format($item->b, 0, '', '.') : null }}
                                            </td>
                                            <td class="text-left">{{ $item->nama_c }} </td>
                                            <td class="text-right">
                                                {{ $item->c != '' ? number_format($item->c, 0, '', '.') : null }}
                                            </td>
                                            <td class="text-left">{{ $item->nama_d }} </td>
                                            <td class="text-right">
                                                {{ $item->d != '' ? number_format($item->d, 0, '', '.') : null }}
                                            </td>
                                            <td class="text-left">{{ $item->nama_e }} </td>
                                            <td class="text-right">
                                                {{ $item->e != '' ? number_format($item->e, 0, '', '.') : null }}
                                            </td>

                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="card-footer p-0">
                            <div class="row">
                                <div class="col-6">
                                    [Halaman : {{ $piutang->currentPage() }} ]
                                </div>
                                <div class="col-6">
                                    <div class="float-right">
                                        {{ $piutang->appends(['turut' => $turut]) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>

    </script>
@endsection
