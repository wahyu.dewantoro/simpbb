@extends('layouts.app')
@section('css')
    <style>
        .toolbar {
            float: left;
        }
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.0/css/buttons.dataTables.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Rekap Piutang</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card ">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-4 col-md-2">
                                    <select name="thn_pajak_sppt" id="thn_pajak_sppt"
                                        class="form-control form-control-sm pencariandata" required>
                                        @foreach ($tahun as $rt)
                                            <option value="{{ $rt }}">{{ $rt }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select name="kd_kecamatan" id="kd_kecamatan"
                                        class="form-control form-control-sm pencariandata" required>
                                        @if (count($kecamatan) > 1)
                                            <option value="">-- Kecamatan --</option>
                                        @endif
                                        @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="table" class="table table-sm text-sm">
                                <thead>
                                    <tr>
                                        <th>Kecamatan</th>
                                        <th>Ketetapan</th>
                                        <th>Pengurang</th>
                                        <th>Terbayar</th>
                                        <th>Koreksi</th>
                                        <th>Sisa</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function(e) {


            var table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                // dom: 'B<"toolbar">frtip',
                buttons: [{
                    text: '<i class="far fa-file-alt"></i> Excell',
                    className: 'btn btn-sm btn-success',
                    action: function(e, dt, node, config) {
                        // alert('Button activated');
                        var kd_kecamatan = $('#kd_kecamatan').val()
                        var kd_kelurahan = $('#kd_kelurahan').val()
                        var thn_pajak_sppt = $('#thn_pajak_sppt').val()
                        var url = "{{ url('informasi/piutang-excel') }}?kd_kecamatan=" +
                            kd_kecamatan +
                            "&kd_kelurahan=" + kd_kelurahan + "&thn_pajak_sppt=" +
                            thn_pajak_sppt
                        window.location.href = url
                    }
                }],
                ajax: {
                    url: "{{ url('informasi/rekap-piutang') }}",
                    data: function(d) {
                        d.kd_kecamatan = $('#kd_kecamatan').val()
                        d.kd_kelurahan = $('#kd_kelurahan').val()
                        d.thn_pajak_sppt = $('#thn_pajak_sppt').val()
                    }
                },
                columns: [{
                        data: 'nm_kecamatan',
                        name: 'nm_kecamatan',
                        orderable: false,
                    },
                    {
                        data: 'ketetapan',
                        name: 'ketetapan',
                        orderable: false,
                        class: 'text-right'
                    },
                    {
                        data: 'pengurang',
                        name: 'pengurang',
                        orderable: false,
                        class: 'text-right'
                    },
                    {
                        data: 'pokok_terbayar',
                        name: 'pokok_terbayar',
                        orderable: false,
                        class: 'text-right'
                    },
                    {
                        data: 'koreksi_piutang',
                        name: 'koreksi_piutang',
                        orderable: false,
                        class: 'text-right'
                    },
                    {
                        data: 'sisa',
                        name: 'sisa',
                        orderable: false,
                        class: 'text-right'
                    },

                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });
            $(document).on('change', '.pencariandata', function() {
                pencarian();
            });

            function pencarian() {
                table.draw();
            }


            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                // getKelurahan(kk);
            })



            // $('#kd_kecamatan').trigger('change')




            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                $('#kd_kelurahan').html(html);
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            $('#kd_kelurahan').trigger('change')
                            /* if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            } */
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }


        })
    </script>
@endsection
