@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Lurah</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-default">
                        <i class="fas fa-file"></i> Tambah
                    </button>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h3 class="card-title">Tahun Pajak {{ date('Y') }}</h3> --}}
                        <div class="card-tools">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-sm table-bordered text-sm" id="table-data">
                            <thead>
                                <tr>
                                    <th style="text-align: center">No</th>
                                    <th style="text-align: center">Desa/Kelurahan</th>
                                    <th style="text-align: center">Kecamatan</th>
                                    <th style="text-align: center">Lurah</th>
                                    <th style="text-align: center">Mulai</th>
                                    <th style="text-align: center">Sampai</th>

                                    <th style="text-align: center">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-cetak" action="{{ route('refrensi.lurah.store') }}" method="post">
                @method('post')
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Form Lurah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Kecamatan</label>
                        <div class="col-md-8">
                            {{-- <input type="text" > --}}
                            <select name="kd_kecamatan" id="kd_kecamatan" class="form-control form-control-sm" required>
                                <option value="">Pilih</option>
                                @foreach ($ref_kecamatan as $rk)
                                <option value="{{  $rk->kd_kecamatan }}">{{ $rk->kd_kecamatan }} - {{ $rk->nm_kecamatan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Desa/Kelurahan</label>
                        <div class="col-md-8">
                            <select name="kd_kelurahan" id="kd_kelurahan" class="form-control form-control-sm" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Nama</label>
                        <div class="col-md-8">
                            <input type="text" name="nm_lurah" id="nm_lurah" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Periode Jabatan</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-6">
                                    <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control form-control-sm tanggal">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control form-control-sm tanggal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-edit" action="#" method="post">
                @method('post')
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Form Lurah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" name="id" id="id_edit">
                        <label class="col-form-label col-md-4">Kecamatan</label>
                        <div class="col-md-8">
                            {{-- <input type="text" > --}}
                            <select name="kd_kecamatan_edit" id="kd_kecamatan_edit" class="form-control form-control-sm" required>
                                <option value="">Pilih</option>
                                @foreach ($ref_kecamatan as $rk)
                                <option value="{{  $rk->kd_kecamatan }}">{{ $rk->kd_kecamatan }} - {{ $rk->nm_kecamatan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Desa/Kelurahan</label>
                        <div class="col-md-8">
                            <select name="kd_kelurahan_edit" id="kd_kelurahan_edit" class="form-control form-control-sm" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Nama</label>
                        <div class="col-md-8">
                            <input type="text" name="nm_lurah_edit" id="nm_lurah_edit" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Periode Jabatan</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-6">
                                    <input type="text" name="tgl_mulai_edit" id="tgl_mulai_edit" class="form-control form-control-sm tanggal">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="tgl_selesai_edit" id="tgl_selesai_edit" class="form-control form-control-sm tanggal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function() {

        $(document).on('keypress', function(e) {
            if (e.which == 13) {
                // alert('You pressed enter!');
                // $('#cek').trigger('click');
                return false;
            }
        });


        $('.tanggal').val('')
        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            var kel = $('#kd_kelurahan').val();
            openloading();
            var html = '<option value="">-- pilih --</option>';
            if (kk != '') {
                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan').val(k);
                            }
                        });
                        // console.log(res);
                        $('#kd_kelurahan').html(html);
                        /* if (count != 1) {
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        } */
                        closeloading();
                    }
                    , error: function(res) {
                        $('#kd_kelurahan').html(html);
                        closeloading();
                    }
                });
            } else {
                $('#kd_kelurahan').html(html);
                closeloading();
            }
        })





        let table = $('#table-data').DataTable({
            processing: true
            , serverSide: true
            , ajax: {
                url: "{!! route('refrensi.lurah.index') !!}"
                , data: function(d) {
                    d.thn_pajak = $('#thn_pajak').val()
                }
            }

            , columns: [{
                    data: null
                    , searchable: false
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                }
                , {
                    data: 'nm_kelurahan'
                    , name: 'ref_kelurahan.nm_kelurahan'
                }
                , {
                    data: 'nm_kecamatan'
                    , name: 'ref_kecamatan.nm_kecamatan'
                }
                , {
                    data: 'nm_lurah'
                    , name: 'nm_lurah'
                }
                , {
                    data: 'tgl_mulai'
                    , name: 'tgl_mulai'
                    , class: 'text-right'
                    , searchable: false
                }
                , {
                    data: 'tgl_selesai'
                    , name: 'tgl_selesai'
                    , class: 'text-center'
                    , searchable: false
                }, {
                    data: 'aksi'
                    , name: 'aksi'
                    , class: 'text-center'
                    , searchable: false
                }

            ]
        });

        $("#form-cetak").on("submit", function(event) {
            openloading();
            event.preventDefault();
            $.ajax({
                url: "{{ route('refrensi.lurah.store') }}"
                , type: 'post'
                , data: $(this).serialize()
                , success: function(res) {
                    closeloading();
                    toastr.success(res, 'Data Lurah');
                    $('#modal-default').modal('toggle');
                    table.draw();
                    kosongkan()
                }
                , error: function(res) {
                    closeloading();
                    toastr.error('Gagal menambah data', 'Data Lurah');
                    table.draw();
                }
            })
        });

        function kosongkan() {
            $('#kd_kecamatan').val('')
            $('#kd_kelurahan').val('')
            $('#nm_lurah').val('')
            $('#tgl_mulai').val('')
            $('#tgl_selesai').val('')
        }

        $('#table-data').on('click', '.hapus', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        url: "{!! url('refrensi/lurah-hapus') !!}/" + id
                        , data: {
                            id: id
                        }
                        , success: function(res) {
                            closeloading();
                            toastr.success(res, 'Data Camat');
                            table.draw();
                        }
                        , error: function(res) {
                            closeloading();
                            toastr.error('Gagal menambah data', 'Data Camat');
                            // table.draw();
                        }
                    })
                }
            })
        })

        $('#table-data').on('click', '.edit', function(e) {
            e.preventDefault()
            let id = $(this).data('id')
            let kd_kecamatan = $(this).data('kd_kecamatan')
            let kd_kelurahan = $(this).data('kd_kelurahan')
            let nm_lurah = $(this).data('nm_lurah')
            let tgl_mulai = $(this).data('tgl_mulai')
            let tgl_selesai = $(this).data('tgl_selesai')

            $('#kd_kecamatan_edit').val(kd_kecamatan)
            $('#kd_kelurahan_edit').val(kd_kelurahan)
            $('#id_edit').val(id)
            $('#nm_lurah_edit').val(nm_lurah)
            $('#tgl_mulai_edit').val(tgl_mulai)
            $('#tgl_selesai_edit').val(tgl_selesai)

            $('#tgl_mulai_edit').trigger('keyup')
            $('#tgl_selesai_edit').trigger('keyup')

            $('#modal-edit').modal('toggle');

            $('#kd_kecamatan_edit').trigger('change');
        })

        $('#kd_kecamatan_edit').on('change', function() {
            var kk = $('#kd_kecamatan_edit').val();
            var kel = $('#kd_kelurahan_edit').val();
            console.log(kel)
            openloading();
            var html = '<option value="">-- pilih --</option>';
            if (kk != '') {
                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan_edit').val(k);
                            }
                        });
                        // console.log(res);
                        $('#kd_kelurahan_edit').html(html);
                        $('#kd_kelurahan_edit').val(kel);
                        closeloading();
                    }
                    , error: function(res) {
                        $('#kd_kelurahan_edit').html(html);
                        closeloading();
                    }
                });
            } else {
                $('#kd_kelurahan_edit').html(html);
                closeloading();
            }
        })


        $("#form-edit").on("submit", function(event) {
            openloading();
            event.preventDefault();
            $.ajax({
                url: "{{ route('refrensi.lurah.update') }}"
                , type: 'post'
                , data: $(this).serialize()
                , success: function(res) {
                    closeloading();
                    toastr.success(res, 'Data Lurah');
                    $('#modal-edit').modal('toggle');
                    table.draw();
                    kosongkan()
                }
                , error: function(res) {
                    closeloading();
                    toastr.error('Gagal edit data', 'Data Lurah');
                    table.draw();
                }
            })
        });

    })

</script>
@endsection
