@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Potongan Individu</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                    <a href="{{ route('master-potongan.index') }}" class="btn btn-info btn-flat btn-sm">
                        <i class="fas fa-angle-double-left"></i>
                        Kembali
                    </a>

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header p-0 border-bottom-0">
                        <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">By NOP</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">UPLOAD</a>
                            </li>

                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-four-tabContent">
                            <div class="tab-pane fade show active" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                                {{-- By form --}}
                                <form action="{{ $data['action'] }}" method="post">
                                    @csrf
                                    @method($data['method'])
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="form-group">
                                                <label for="nop">NOP</label>
                                                @php
                                                $nop='';
                                                if(!empty($data['potongan'])){
                                                $nop=$data['potongan']->kd_propinsi . '.' . $data['potongan']->kd_dati2 . '.' . $data['potongan']->kd_kecamatan . '.' . $data['potongan']->kd_kelurahan . '.' . $data['potongan']->kd_blok . '-' . $data['potongan']->no_urut . '.' . $data['potongan']->kd_jns_op;
                                                }
                                                @endphp
                                                <input type="text" name="nop" id="nop" class="form-control form-control-sm" required placeholder="Nomor Objek Pajak" value="{{  $nop }}">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <div class="form-group">
                                                <label for="thn_pajak_sppt">Tahun Pajak</label>
                                                <input type="number" name="thn_pajak_sppt" id="thn_pajak_sppt" class="form-control form-control-sm" required placeholder="Tahun Pajak" value="{{ $data['potongan']->thn_pajak_sppt??'' }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-md-8">
                                            <div class="form-group">
                                                <label for="nilai_potongan">Nilai Potongan</label>
                                                <input type="number" name="nilai_potongan" id="nilai_potongan" class="form-control form-control-sm" required placeholder="Nilai Potongan" value="{{ $data['potongan']->nilai_potongan??'' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 col-md-4">
                                            <div class="form-group ">
                                                <label for="">Mulai</label>
                                                <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control form-control-sm tanggal"  value="{{ $data['potongan']->tgl_mulai??'' }}">
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div class="form-group ">
                                                <label for="">Selesai</label>
                                                <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control form-control-sm tanggal"  value="{{ $data['potongan']->tgl_selesai??'' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="float-right">

                                            <button class="btn btn-flat btn-primary"><i class="far fa-save"></i> submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                <a href="{{ url('potongan_individu.xlsx') }}"><i class="fas fa-file-download text-success"></i> Template </a>
                                <br><br>
                                <form action="{{ url('master-potongan-individu') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('post')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group ">
                                                        <label for="">Mulai</label>
                                                        <input type="text" name="tgl_mulai_dua" id="tgl_mulai_dua" class="form-control form-control-sm tanggal" required value="{{ $data['potongan']->tgl_mulai??'' }}">
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group ">
                                                        <label for="">Selesai</label>
                                                        <input type="text" name="tgl_selesai_dua" id="tgl_selesai_dua" class="form-control form-control-sm tanggal" required value="{{ $data['potongan']->tgl_selesai??'' }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-sm">
                                                <input type="file" name="file" class="form-control">
                                                <span class="input-group-append">
                                                    <button class="btn btn-primary"><i class="fas fa-file-upload"></i> Upload</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).on({
        ajaxStart: function() {
            openloading();
        }
        , ajaxStop: function() {
            closeloading();
        }
    });

    $(document).ready(function() {

        if ($("input[name=_method]").val() == 'post') {
            $('.tanggal').val('')
        }

        /* $(document).on('keypress', function(e) {
            if (e.which == 13) {
                // alert('You pressed enter!');
                $('#cek').trigger('click');
            }
        }); 
        */

        // $('#nop').trigger('keyup');

        $('#nop').on('keyup', function() {
            var nop = $(this).val();
            var convert = formatnop(nop);
            $(this).val(convert);
        });


    });

</script>
@endsection
