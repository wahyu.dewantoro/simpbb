@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Insentif Objek </h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group row text-sm">
                                    <label for="tahun_pajak" class="col-sm-4 col-form-label">Tahun</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-6">
                                                <select name="tahun_pajak" id="tahun_pajak" class="form-control form-control-sm pencariandata">
                                                    {{-- <option value="">Pilih</option> --}}
                                                    @for($i = date('Y'); $i >= 2014; $i--)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label for="jenis" class="col-sm-4 col-form-label">Jenis</label>
                                    <div class="col-sm-8">
                                        <select name="jenis" id="jenis" class="form-control form-control-sm pencariandata">
                                            <option value="">All</option>
                                            <option value="1">Persentase</option>
                                            <option value="2">Selisih</option>
                                            <option value="3">Inidividu</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <button type='button' class='btn btn-sm   btn-success btn-export'> <i class="fas fa-file-alt"></i> Export </button>
                            </div>
                        </div>
                    </form>

                    <table class="table table-striped text-sm table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                        <thead class="bg-info">
                            <tr>
                                <th>No</th>
                                <th style="text-align: center">NOP</th>
                                {{-- <th style="text-align: center">Alamat</th> --}}
                                <th style="text-align: center">Wajib Pajak</th>
                                <th style="text-align: center">Nilai Potongan</th>
                                <th style="text-align: center">Jenis</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection

@section('script')
{{-- <script src="{{ asset('js') }}/wilayah.js"></script> --}}
<script>
    $(document).ready(function() {
        var tableHasil
        var now = $('#tahun_pajak').val()

        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ route('potongan.index') }}"
                , data: function(d) {
                    d.tahun_pajak = $('#tahun_pajak').val();
                    d.jenis = $('#jenis').val();
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , name: 'nop'
                    , orderable: false
                    , searchable: false
                }
              /*   , {
                    data: 'alamat'
                    , name: 'nop'
                    , orderable: false
                    , searchable: false
                    , render: function(data, type, row) {
                        return data + "<br><small>" + row.nm_kelurahan + ' - ' + row.nm_kecamatan + "</small>"
                    }
                } */
                , {
                    data: 'nm_wp_sppt'
                    , name: 'nm_wp_sppt'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'nilai_potongan'
                    , name: 'nilai_potongan'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)
                    }
                }
                , {
                    data: 'jenis'
                    , name: 'jenis'
                    , orderable: false
                    , searchable: false
                    , render: function(data, type, row) {
                        let ket;
                        if (data == '1') {
                            ket = 'Persentase ' + row.persentase
                        } else if (data == '2') {
                            ket = 'Selisih'
                        } else {
                            ket = 'Individu'
                        }
                        return ket
                    }
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        $(document).on('change', '.pencariandata', function() {
            pencarian();
        });


        $(document).on("click", ".btn-export", function(evt) {
            tahun = $('#tahun_pajak').val();
            jenis = $('#jenis').val();


            let url = "{{ route('potongan.excel') }}?tahun_pajak=" + tahun + "&jenis=" + jenis
            window.open(url, '_blank');
        });


        function pencarian() {
            tableHasil.draw();
        }



    })

</script>
@endsection
