@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Insentif Objek </h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <select name="tahun" id="tahun" class="form-control">
                        <option value="">Tahun</option>
                            @for($taun=date('Y')+1;$taun>=2022;$taun--)
                        <option @if($tahun==$taun) selected @endif value="{{ $taun }}">{{ $taun }}</option>
                        @endfor
                        </option>
                    </select>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 col-md-6 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">

                    <table class="table table-striped text-sm table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                        <thead class="bg-info">
                            <tr>
                                <th style="text-align: center">Jenis</th>
                                <th style="text-align: center">Nilai Potongan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $total=0;
                            @endphp
                            @foreach ($data as $row)
                            @php
                            $total+=$row->nilai;
                            @endphp
                            <tr>
                                <td>{{ $row->jenis }}</td>
                                <td class="text-right">{{ angka($row->nilai) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td class="text-right">{{ angka($total) }}</td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection

@section('script')
{{-- <script src="{{ asset('js') }}/wilayah.js"></script> --}}
<script>
    $(document).ready(function() {
        $('#tahun').on('change', function() {
            var tahun=$('#tahun').val()
            console.log(tahun)
            
            window.location.href="{{ url('potongan/rekap') }}?tahun="+tahun
        })
    })

</script>
@endsection
