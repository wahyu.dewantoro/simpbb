<table>
    <thead>
        <tr>
            <th>No</th>
            <th>NOP</th>
            <th>Alamat</th>
            <th>Wajib Pajak</th>
            <th>Nilai Potongan</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no=1;
        @endphp
        @foreach ($data as $row)
        <tr>
            <td>{{ $no }}</td>
            <td>{{ $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op }}</td>
            <td>{{ $row->jalan_op }} {{ $row->blok_kav_no_op }} {{ $row->nm_kelurahan }} {{ $row->nm_kecamatan }}</td>
            <td>{{ $row->nm_wp_sppt }}</td>
            <td>{{ $row->nilai_potongan }}</td>
            <td>
                @php
                if ($row->jenis == '1') {
                $ket = 'Persentase '.$row->persentase;
                } else if ($row->jenis == '2') {
                $ket = 'Selisih';
                } else {
                $ket = 'Individu';
                }
                echo $ket;
                @endphp
            </td>
        </tr>
        @php
        $no++;
        @endphp
        @endforeach
    </tbody>
</table>
