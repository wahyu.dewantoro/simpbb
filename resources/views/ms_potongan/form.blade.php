@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Master Potongan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                    <a href="{{ route('master-potongan.index') }}" class="btn btn-info btn-flat btn-sm">
                        <i class="fas fa-angle-double-left"></i>
                        Kembali
                    </a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <form action="{{ $data['action'] }}" method="post">
            @csrf
            @method($data['method'])
            <div class="row">

                <div class="col-md-12">
                    <div class="card card-blue">
                        <div class="card-header">
                            <div class="card-title">Potongan</div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-2">
                                    <div class="form-group">
                                        <label for="">Tahun</label>
                                        <input type="text" @if($data['method']!='post' ) readonly @endif class="form-control form-control-sm" name="tahun" id="tahun" value="{{ $data['potongan']->tahun ??'' }}" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="form-group">
                                        <label for="">Buku</label>
                                        {{-- <input type="text" > --}}
                                        <select class="form-control form-control-sm" name="kd_buku" id="kd_buku" required @if($data['method']!='post' ) readonly @endif>
                                            <option value="">Pilih</option>
                                            @php
                                            $sb=$data['potongan']->kd_buku ??'';
                                            @endphp
                                            @for($i = 1; $i < 5; $i++) <option @if($sb==$i) selected @endif value="{{ $i }}">Buku {{ $i }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="form-group">
                                        <label for="">Jenis</label>
                                        {{-- <input type="text" > --}}
                                        <select class="form-control form-control-sm" name="jenis" id="jenis" required>
                                            @php
                                            $jns=$data['potongan']->jenis ??'';
                                            @endphp
                                            <option value="">Pilih</option>
                                            <option @if($jns==1) selected @endif value="1">Persentase</option>
                                            <option @if($jns==2) selected @endif value="2">Selisih dengan tahun sebelumnya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="form-group" id="div_persen">
                                        <label for="">Persentase</label>
                                        <input type="number" class="form-control form-control-sm" value="{{ $data['potongan']->persentase ??'' }}" name="persentase" id="persentase">
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="form-group ">
                                        <label for="">Mulai</label>
                                        <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control form-control-sm tanggal" required value="{{ $data['potongan']->tgl_mulai??'' }}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="form-group ">
                                        <label for="">Selesai</label>
                                        <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control form-control-sm tanggal" required value="{{ $data['potongan']->tgl_selesai??'' }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div id="divCheckAll">
                                        {{-- Check All --}}
                                        <div class="form-check">
                                            <input type="checkbox" name="checkall" id="checkall" onClick="check_uncheck_checkbox(this.checked);" class="form-check-input" />
                                            <label for="checkall">
                                                Check All
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        @php
                                        $scope=$data['scope']??[];
                                        @endphp
                                        @foreach ($data['kelurahan'] as $idx=> $item)
                                        <div class="col-md-3">
                                            <strong>{{ $idx }}</strong>
                                            <ul style="    list-style-type: none;">
                                                @foreach ($item as $kelurahan)
                                                <li>
                                                    <div class="form-check">
                                                        <input type="checkbox" name="kec_kel[]" class="form-check-input" @if(in_array($kelurahan->kd_kecamatan.'_'.$kelurahan->kd_kelurahan,$scope)) checked @endif value="{{ $kelurahan->kd_kecamatan.'_'.$kelurahan->kd_kelurahan}}" id="{{ $kelurahan->kd_kecamatan.'_'.$kelurahan->kd_kelurahan}} }}">
                                                        <label class="form-check-label" for="{{ $kelurahan->kd_kecamatan.'_'.$kelurahan->kd_kelurahan}} }}">{{ $kelurahan->nm_kelurahan }}</label>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <button class="btn btn-flat btn-sm btn-success"><i class="far fa-paper-plane"></i> Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>
</section>
@endsection
@section('script')
<script>
    function check_uncheck_checkbox(isChecked) {
        if (isChecked) {
            $('input[type="checkbox"]').each(function() {
                this.checked = true;
            });
        } else {
            $('input[type="checkbox"]').each(function() {
                this.checked = false;
            });
        }
    }


    $(document).ready(function() {

        if ($("input[name=_method]").val() == 'post') {
            $('.tanggal').val('')
        }


        function bukaTutup(jenis) {
            persen = $('#persentase')
            if (jenis == '1') {
                persen.prop('required', true);
                $('#div_persen').show()
            } else {
                persen.prop('required', false);
                $('#div_persen').hide()
            }
        }
        // bukaTutup()

        $('#jenis').on('change', function() {
            jenis = $('#jenis').val()
            bukaTutup(jenis)
        })

        $('#jenis').trigger('change')

    })

</script>
{{-- <script src="{{url('js/treeview.js')}}"></script> --}}
@endsection
