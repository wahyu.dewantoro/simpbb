@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>MASTER POTONGAN</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline card-outline-tabs">
                    <div class="card-header p-0 border-bottom-0">
                        <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link  @if($tab=='') active @endif" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">GLobal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($tab=='individu') active @endif" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Individu</a>
                            </li>

                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-four-tabContent">
                            <div class="tab-pane fade  @if($tab=='') show active @endif" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <select name="cari" id="cari" class="form-control float-right">
                                            <option value="">Tahun</option>
                                            @for($th = date('Y')+1; $th>= 2010; $th--)
                                            <option @if(request()->get('cari')==$th) selected @endif value="{{ $th }}">{{ $th }}</option>
                                            @endfor
                                        </select>
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                            <a href="{{ route('master-potongan.create') }}" class="btn btn-info btn-flat">
                                                <i class="fas fa-file-medical"></i> Tambah
                                            </a>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm text-sm">
                                        <thead class="bg-info">
                                            <tr>
                                                <th class="text-center" width="20px">No</th>
                                                <th class="text-center">Tahun</th>
                                                <th class="text-center">Buku</th>
                                                <th class="text-center">Stimulus</th>
                                                <th>Keterangan</th>
                                                <th>Masa Berlaku</th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($potongan as $index => $row)
                                            <tr>
                                                <td align="center">{{ $index + $potongan->firstItem() }}</td>
                                                <td class="text-center">
                                                    {{ $row->tahun }}</td>
                                                <td class="text-center">{{ $row->kd_buku }}
                                                </td>
                                                <td class="text-center">{{ $row->persentase }} @if($row->persentase<>'')%@endif
                                                </td>
                                                <td>
                                                    {{$row->keterangan}}
                                                </td>
                                                <td>
                                                    {{ tglIndo($row->tgl_mulai)}} s/d
                                                    {{ tglIndo($row->tgl_selesai)}}
                                                </td>
                                                <td class="text-center">
                                                    @php
                                                    $id=$row->tahun.'-'.$row->kd_buku;
                                                    @endphp
                                                    <a title='Detail Potongan' href="{{ route('master-potongan.show', $id) }}" class="text-success"> <i class="far fa-file-alt"></i> </a>
                                                    <a title="Verifikasi" href="{{ route('master-potongan.edit',$id) }}" class="text-info"> <i class="fas fa-pencil-alt"></i> </a>

                                                    <a href="#" data-id="{{ $id }}" class="hapuspotongan"><i class="fas fa-trash-alt text-danger"></i></a>

                                                    <form method="POST" id="delete-form-{{ $id }}" action="{{ route('master-potongan.destroy', [$id]) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="float-sm-right">
                                        {{ $potongan->appends(['cari' => request()->get('cari')]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade @if($tab=='individu') show active @endif" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                <div class="row">

                                    <div class="col-1">
                                        <select class="pencariandata form-control form-control-sm" name="tahun" id="tahun">
                                            @for($tahun = date('Y'); $tahun >= 2003; $tahun--)
                                            <option value="{{ $tahun}}">{{ $tahun }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- <div class="float-right"> --}}


                                        <div class="input-group input-group-sm">
                                            <input type="text" placeholder="Pencarian" name="search" id="search" class="form-control form-control-sm pencariandata">
                                            <span class="input-group-append">
                                                <a class="btn btn-info btn-sm btn-flat" href="{{ url('master-potongan-individu') }}"><i class="fas fa-file-medical"></i> Tambah</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                                    <thead class="bg-info">
                                        <tr>
                                            <th style="text-align: center" style="width:1px !important;">No</th>
                                            <th>NOP</th>
                                            <th>Wajib Pajak</th>
                                            <th>Tahun</th>
                                            <th>Potongan</th>
                                            <th>Berlaku</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .dataTables_filter,
    .dataTables_length,
    .dataTables_info {
        display: none;
    }

</style>
@endsection
@section('script')
<script>
    $(document).ready(function() {



        var tableObjek, tableHasil
        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ url('master-potongan-individu') }}"
                , data: function(d) {
                    d.tahun = $('#tahun').val()
                    d.search = $('#search').val()
                    /* d.created_by = $('#created_by').val();
                    d.tanggal = $('#tanggal').val();
                    d.status = $('#status').val();
                    d.jenis_pendataan_id = $('#jenis_pendataan_id').val(); */
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'nm_wp_sppt'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'thn_pajak_sppt'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'nilai_potongan'
                    , orderable: false
                    , searchable: false
                    , class: "text-right"
                }
                , {
                    data: 'masa_berlaku'
                    , orderable: false
                    , searchable: false
                    , class: "text-right"
                }
                
                , {
                    data: 'aksi'
                    , orderable: false
                    , searchable: false
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        $("#table-hasil").on("click", ".hapuspotongan"
            , function(e) {
                let href_ = $(this).data('href')
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?'
                    , text: "data yang dihapus tidak dapat di kembalikan."
                    , icon: 'warning'
                    , showCancelButton: true
                    , confirmButtonColor: '#3085d6'
                    , cancelButtonColor: '#d33'
                    , confirmButtonText: 'Ya, Yakin!'
                    , cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = href_
                    }
                })

            });

        function pencarian() {
            tableHasil.draw();
        }

        $(document).on('change keyup', '.pencariandata', function() {
            pencarian();
        });
    })

</script>
@endsection
