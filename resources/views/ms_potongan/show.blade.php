@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Master Potongan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                    <a href="{{ route('master-potongan.index') }}" class="btn btn-info btn-flat btn-sm">
                        <i class="fas fa-angle-double-left"></i>
                        Kembali
                    </a>

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-blue">
                    <div class="card-header">
                        <div class="card-title">Potongan</div>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td>Tahun</td>
                                    <td>:</td>
                                    <td>{{ $potongan->tahun }}</td>
                                </tr>
                                <tr>
                                    <td>Buku</td>
                                    <td>:</td>
                                    <td>{{ $potongan->kd_buku }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis</td>
                                    <td>:</td>
                                    <td>{{ $potongan->keterangan }}</td>
                                </tr>

                                <tr>
                                    <td>Persentase</td>
                                    <td>:</td>
                                    <td>{{ $potongan->persentase }} @if($potongan->persentase<>'')%@endif</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card card-blue">
                    <div class="card-header">
                        <div class="card-title">Area Potongan</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($scopePotongan as $idx=> $item)
                            <div class="col-md-4">
                                <strong>{{ $idx }}</strong>
                                <ol>
                                    @foreach ($item as $kelurahan)
                                    <li>{{ $kelurahan['nm_kelurahan'] }}</li>
                                    @endforeach
                                </ol>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
