@extends('layouts.app')
@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css"> --}}
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Koreksi Piutang Sismiop</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-sm btn-primary " href="{{ route('koreksi-sismiop.create') }}"><i
                                class="far fa-plus-square"></i> Tambah</a>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-body p-1">
                            <div class="card-body no-padding p-1">
                                <table id="table" class="table table-bordered table-striped text-sm table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Batch</th>
                                            <th>NOP</th>
                                            <th>Tahun Pajak</th>
                                            <th>Keterangan</th>
                                            <th>Created By</th>
                                            <th>Verifikator</th>
                                            <th></th>

                                            {{-- <th>No Transaksi</th>
                                        <th>No Surat</th>
                                        <th>Tgl Surat</th>
                                        <th>Jenis</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th></th> --}}
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('pembayaran.blokir.store') }}" method="post" id="formbynop">
                    <div class="modal-header">
                        <h4 class="modal-title">Form By NOP</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="nop">NOP</label>
                                    <input required type="text" name="nop" id="nop" autofocus="true"
                                        class="form-control form-control-sm" placeholder="NOP">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="thn_pajak_sppt">Tahun</label>
                                <input type="text" class="form-control form-control-sm angka" name="thn_pajak_sppt"
                                    id="thn_pajak_sppt" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="no_surat">Nomor Surat</label>
                                    <input required type="text" name="no_surat" id="no_surat"
                                        class="form-control form-control-sm" placeholder="Nomor surat">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tgl_surat">Tanggal Surat</label>
                                    <input required type="text" class="form-control form-control-sm tanggal"
                                        name="tgl_surat" id="tgl_surat">
                                </div>
                            </div>
                            <input type="hidden" name="jns_koreksi" value="3">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="keterangan">Keterangan</label>
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" id="md-close_dua" class="btn btn-default" data-dismiss="modal"><i
                                class="fas fa-times"></i> Close</button>
                        <button type="submit" id="submit" class="btn btn-primary"><i class="far fa-save"></i>
                            submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-upload">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload Blokir Pembayaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <label for="tgl_surat">File</label>
                            <input type="file" name="file" id="file" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="no_surat">Nomor Surat</label>
                                <input required type="text" name="no_surat_dua" id="no_surat_dua"
                                    class="form-control form-control-sm" placeholder="Nomor surat">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="tgl_surat">Tanggal Surat</label>
                            <input required type="text" class="form-control form-control-sm tanggal"
                                name="tgl_surat_dua" id="tgl_surat_dua">
                        </div>
                        <input type="hidden" name="jns_koreksi_dua" value="3">

                        {{-- <div class="form-group">
                        <label for="jns_koreksi_dua">Jenis Koreksi</label>
                        <select name="jns_koreksi_dua" id="jns_koreksi_dua" class="form-control form-control-sm">
                            <option value="3">Blokir Pembayaran</option>
                            <option value="4">Iventaris Piutang</option>
                        </select>
                    </div> --}}
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="keterangan_dua">Keterangan</label>
                            <textarea class="form-control" name="keterangan_dua" id="keterangan_dua" required rows="2"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" id="md-close" class="btn btn-default" data-dismiss="modal"><i
                            class="fas fa-times"></i> Close</button>
                    <button type="button" id="upload" class="btn btn-primary"><i class="fas fa-file-upload"></i>
                        Upload</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(".tanggal").val('');
            $('#nop').trigger('keyup');
            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $('#addnop').click(function(e) {
                e.preventDefault();
                $('#modal-default').modal('toggle');
            });

            $('#addupload').click(function(e) {
                e.preventDefault();
                $('#modal-upload').modal('toggle');
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            tableHasil = $('#table').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ url('koreksi-simiop') }}",
                    data: function(d) {
                        /* d.kd_kecamatan = $('#kd_kecamatan').val()
                        d.kd_kelurahan = $('#kd_kelurahan').val()
                    */
                        d.search = $('#search').val()
                    }
                },
                columns: [{
                    data: null,
                    class: 'text-center',
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    searchable: false
                }, {
                    data: 'no_transaksi',
                    name: 'no_transaksi',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'nop',
                    name: 'nop',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'thn_pajak_sppt',
                    name: 'thn_pajak_sppt',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'keterangan',
                    name: 'keterangan',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return data + "<br><small class='text-success'>" + row
                            .verifikasi_keterangan + "</small>"
                    }

                }, {
                    data: 'nama',
                    name: 'nama',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return data + "<br><small class='text-success'>" + row.created_at +
                            "</small>"
                    }

                }, {
                    data: 'verifikator',
                    name: 'verifikator',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                }],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });


            $('#cari').on('click', function(e) {
                e.preventDefault()
                tableHasil.draw();
            })


            $("#formbynop").on("submit", function(e) {
                openloading();
                e.preventDefault();
                $.ajax({
                    url: "{{ route('pembayaran.blokir.store') }}",
                    type: 'post',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('#kd_kecamatan').val(data.kd_kecamatan)
                        $('#kd_kelurahan').val(data.kd_kelurahan)
                        $('#kd_kecamatan').trigger('change');
                        closeloading()
                        toastr.success(data.msg);
                        $('#nop').val('')
                        $('#thn_pajak_sppt').val('')
                        $('#keterangan').val('')
                        $('#cari').trigger('click')
                        $('#modal-default').modal('toggle');
                    },
                    error: function(res) {
                        closeloading()
                        toastr.error('Gagal melakukan proses pemblokiran');
                    }
                })
            });

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            function getKelurahan(kk) {

                var html = '<option value="">-- Desa/Kelurahan --</option>';
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                            Swal.close()
                        },
                        error: function(res) {
                            Swal.close()
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    Swal.close()
                    $('#kd_kelurahan').html(html);
                }
            }

            $('#table').on('click', '.buka', function(e) {
                e.preventDefault();
                var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,
                    thn_pajak_sppt;
                kd_propinsi = $(this).data('kd_propinsi')
                kd_dati2 = $(this).data('kd_dati2')
                kd_kecamatan = $(this).data('kd_kecamatan')
                kd_kelurahan = $(this).data('kd_kelurahan')
                kd_blok = $(this).data('kd_blok')
                no_urut = $(this).data('no_urut')
                kd_jns_op = $(this).data('kd_jns_op')
                thn_pajak_sppt = $(this).data('thn_pajak_sppt')

                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "NOP akan masuk kedalam piutang DHKP",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        openloading()
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('pembayaran.blokir.buka') }}",
                            data: {
                                kd_propinsi,
                                kd_dati2,
                                kd_kecamatan,
                                kd_kelurahan,
                                kd_blok,
                                no_urut,
                                kd_jns_op,
                                thn_pajak_sppt,
                            },
                            success: function(data) {
                                closeloading()
                                toastr.success(data.msg);
                                tableHasil.draw();
                            },
                            error: function() {
                                closeloading()
                                toastr.error('Gagal melakukan proses buka blokir');
                            }
                        });
                    }
                })
            })


            $('#upload').on('click', function(e) {
                e.preventDefault()
                $('#modal-upload').modal('toggle');
                let keterangan = $('#keterangan_dua').val()
                var files = $('#file')[0].files;

                var no_surat_dua = $('#no_surat_dua').val()
                var tgl_surat_dua = $('#tgl_surat_dua').val()
                var fd = new FormData();
                if (files.length > 0 && keterangan != '') {
                    var filename = $('#file').val().split('\\').pop();
                    var la = filename.split('.');
                    let vexe = ['xlsx', 'xls'];
                    let exe = la[la.length - 1];
                    status = '0';
                    for (var i = 0; i < vexe.length; i++) {
                        var name = vexe[i];
                        if (name == exe.toLowerCase()) {
                            status = '1';
                            break;
                        }
                    }
                    if (status == '0') {

                        $(this).val('');
                        swal.fire({
                            title: "Oppssss",
                            text: "File ." + exe +
                                " tidak di perbolehkan, harus format .xls atau .xlsx",
                            icon: "warning",
                            // timer: 2000,
                        });
                    } else {
                        fd.append('file', files[0])
                        fd.append('keterangan', keterangan)
                        fd.append('no_surat', no_surat_dua)
                        fd.append('tgl_surat', tgl_surat_dua)
                        Swal.fire({
                            // icon: 'error',
                            title: '<i class="fas fa-sync fa-spin"></i>',
                            text: "Sistem sedang berjalan, mohon ditunggu!",
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            showConfirmButton: false
                        })

                        $.ajax({
                            url: "{{ route('pembayaran.blokir.upload') }}",
                            type: 'post',
                            data: fd,
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                $('#file').val('')
                                $('#keterangan_dua').val('')
                                $('#cari').trigger('click')
                                swal.fire({
                                    title: "Notifikasi",
                                    text: response.msg,
                                    icon: "success",
                                });

                            },
                            error: function(res) {
                                $('#file').val('')
                                $('#keterangan_dua').val('')
                                swal.fire({
                                    title: "Error",
                                    text: "Terjadi kesalahan",
                                    icon: "warning",
                                    timer: 2000,
                                });
                            }
                        });
                    }
                } else {
                    swal.fire({
                        title: "Peringatan",
                        text: "Silahkan pilih file dan keterangan harus isi.",
                        icon: "warning",
                        timer: 2000,
                    });

                }

            })

        })
    </script>
@endsection
