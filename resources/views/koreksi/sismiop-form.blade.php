@extends('layouts.app')
@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css"> --}}
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Koreksi Piutang Sismiop</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-sm btn-primary " href="{{ route('koreksi-sismiop.index') }}">
                            <i class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-body p-1">
                            <div class="card-body no-padding p-1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Kecamatan</label>
                                            <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan"
                                                required id="kd_kecamatan">
                                                @if (count($kecamatan) > 1)
                                                    <option value="">-- Kecamatan --</option>
                                                @endif
                                                @foreach ($kecamatan as $rowkec)
                                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                        value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Kelurahan</label>
                                            <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan"
                                                id="kd_kelurahan">
                                                <option value="">-- Kelurahan / Desa -- </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Preview Piutang</label>
                                            <table class="table table-sm table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center bg-info">Tahun</th>
                                                        <th class="text-center bg-info">Objek</th>
                                                        <th class="text-center bg-info">Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="preview">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
            if (kd_kecamatan == '') {
                var kd_kecamatan = $('#kd_kecamatan').val()
            }
            getKelurahan(kd_kecamatan);

            function getKelurahan(kk) {

                var html = '<option value="">-- Kelurahan / Desa --</option>';
                $('#kd_kelurahan').html(html);
                if (kk != '') {
                    openloading()
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                            closeloading()
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                            closeloading()
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }

            $('#kd_kelurahan').on('change', function() {
                kd_kecamatan = $('#kd_kecamatan').val()
                kd_kelurahan = $('#kd_kelurahan').val()

                $('#preview').html('')
                if (kd_kecamatan != "" && kd_kelurahan != "") {
                    openloading()
                    $.ajax({
                        url: "{{ route('koreksi-sismiop.create') }}",
                        data: {
                            kd_kecamatan,
                            kd_kelurahan
                        },
                        success: function(res) {
                            $('#preview').html(res)
                            closeloading()
                        },
                        error: function(res) {
                            $('#preview').html('')
                            closeloading()
                        }

                    })
                }

            })

        })
    </script>
@endsection
