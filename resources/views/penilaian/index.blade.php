@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <h1>Penilaian Objek</h1>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">

                        </div>
                        <div class="card-footer p-0">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
