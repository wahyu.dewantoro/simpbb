@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body p-2">
                            <form action="{{ $data['action'] }}" method="post">
                                @csrf
                                @method($data['method'])
                                <div class="form-group">
                                    <label for="">Kelompok</label>
                                    <input type="text" value="{{ $data['objek']->nama_kelompok ?? '' }}"
                                        name="nama_kelompok" id="nama_kelompok" class="form-control" required>
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-float btn-info"><i class="fas fa-save"></i> Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
