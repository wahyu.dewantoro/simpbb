@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Kelompok Objek</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools">
                                @can('add_kelompok_objek')
                                    <a href="{{ route('refrensi.kelompokobjek.create') }}" class="btn btn-primary btn-sm">
                                        <i class="fas fa-plus"></i> Tambah
                                    </a>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Deskripsi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($data as $row)
                                        <tr>
                                            <td class="text-center">{{ $no }}</td>
                                            <td>{{ $row->nama_kelompok }}</td>
                                            <td class="text-center">
                                                @can('edit_kelompok_objek')
                                                    <a href="{{ route('refrensi.kelompokobjek.edit', $row->id) }}"><i
                                                            class="fas fa-edit text-info" title="Edit User"></i> </a>
                                                @endcan
                                                @can('delete_kelompok_objek')
                                                    <a href="{{ url('users', $row->id) }}" onclick="
                                                                    var result = confirm('Are you sure you want to delete this record?');
                                                                    if(result){
                                                                        event.preventDefault();
                                                                        document.getElementById('delete-form-{{ $row->id }}').submit();
                                                                    }" title="Delete User"><i
                                                            class="fas fa-trash-alt text-danger"></i>
                                                    </a>
                                                    <form method="POST" id="delete-form-{{ $row->id }}"
                                                        action="{{ route('refrensi.kelompokobjek.destroy', [$row->id]) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                @endcan


                                            </td>
                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
