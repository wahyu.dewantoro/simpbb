<style>
    ul.no-bullets {
        list-style-type: none;
        /* Remove bullets */
        padding: 0;
        /* Remove padding */
        margin: 0;
        /* Remove margins */
    }

</style>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive-sm">
            <table class="table table-sm table-bordered">
                <thead class="bg-info">
                    <tr>
                        <th>ZNT</th>
                        <th>BLOK PETA ZNT</th>
                        <th width="200px">NIR [ {{ date('Y')}} ] </th>
                        <th>Keterangan</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th width="70px"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $i=> $row)
                    <tr>
                        {{-- <td>{{ $i+1 }}</td> --}}
                        <td>{{ $row->kd_znt }}</td>

                        <td>
                            {{ $row->blok}}
                        </td>
                        <td class="text-center">
                            {{ angka($row->nir,2)}}
                        </td>
                        <td>{{ $row->nama_lokasi }}</td>
                        <td>{{ $row->created_by }}</td>
                        <td>{{ $row->updated_by }}</td>
                        <td class="text-left">
                            @php
                            $id=$row->kd_kecamatan.'_'.$row->kd_kelurahan.'_'.$row->kd_znt;
                            @endphp
                            <a href="{{ route('refrensi.znt.show',$id) }}">
                                <i class="fas fa-search text-info"></i>
                            </a>
                            <a class="edit" data-kd_kecamatan="{{ $row->kd_kecamatan }}" data-kd_kelurahan="{{ $row->kd_kelurahan }}" data-kd_znt="{{ $row->kd_znt }}" data-lokasi_objek_id="{{ $row->lokasi_objek_id }}" data-kepakai="{{ $row->kepakai }}">
                                <i class="fas fa-edit text-primary"></i>
                            </a>
                            @if($row->cs=='')
                            @if($row->kepakai=='0')
                            <a data-href="{{ route('refrensi.znt.hapus',$id) }}" class="hapus">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                            @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit ZNT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table>
                    <tbody>
                        <tr>
                            <td>Kecamatan</td>
                            <td>: <span id="edit_text_kecamatan"></span></td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>: <span id="edit_text_kelurahan"></span></td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" id="val_edit_kd_kecamatan">
                <input type="hidden" id="val_edit_kd_kelurahan">
                <div class="form-group row">
                    <label class="col-form-label col-md-3" for="">Kode ZNT</label>
                    <div class="col-md-4">
                        <input type="text" name="edit_kd_znt" id="edit_kd_znt" class="form-control-sm form-control" maxlength="2">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-3" for="">Lokasi</label>
                    <div class="col-md-8">
                        <select name="edit_lokasi_objek_id" id="edit_lokasi_objek_id" class="form-control form-control-sm">
                            <option value=""> Pilih </option>
                            @foreach ($lokasi as $i=> $item)
                            <option value="{{ $i }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal"><i class="far fa-window-close"></i> Close</button>
                <button id="edit_znt" type="button" class="btn btn-primary btn-sm btn-flat"><i class="far fa-save"></i> simpan</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $('.edit').on('click', function(e) {
            e.preventDefault();
            $('#edit_text_kecamatan').html($("#kd_kecamatan option:selected").text())
            $('#edit_text_kelurahan').html($("#kd_kelurahan option:selected").text())
            $('#val_edit_kd_kecamatan').val($(this).data('kd_kecamatan'))
            $('#val_edit_kd_kelurahan').val($(this).data('kd_kelurahan'))
            $('#edit_kd_znt').val($(this).data('kd_znt'))
            $('#edit_lokasi_objek_id').val($(this).data('lokasi_objek_id'))
            $('#modal-edit').modal('show');
        });

        $('#edit_znt').on('click', function(e) {
            e.preventDefault();
            val_kd_kecamatan = $('#val_edit_kd_kecamatan').val()
            val_kd_kelurahan = $('#val_edit_kd_kelurahan').val()
            kd_znt = $('#edit_kd_znt').val()
            lokasi_objek_id = $('#edit_lokasi_objek_id').val()
            openloading()
            $.ajax({
                type: 'post'
                , url: "{{ route('refrensi.znt.update') }}"
                , data: {
                    kd_znt: kd_znt
                    , kd_kecamatan: val_kd_kecamatan
                    , kd_kelurahan: val_kd_kelurahan
                    , lokasi_objek_id: lokasi_objek_id
                }
                , success: function(data) {
                    closeloading()
                    toastr.info(data.msg, 'EDIT ZNT');
                    if (data.status == 1) {
                        $('#modal-default').modal('toggle');
                        loadData(val_kd_kecamatan, val_kd_kelurahan);
                    }

                }
            });

        })


        $('.hapus').on('click', function(e) {
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "Data Peta Blok ZNT dan data NIR juga akan di hapus."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    // document.getElementById('delete-form-' + id).submit()
                    document.location.href = $(this).data('href');
                }
            })

        });
    });

</script>
