@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Master ZNT dan NIR</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan" required id="kd_kecamatan">
                            @if (count($kecamatan) > 1)
                            <option value="">-- Kecamatan --</option>
                            @endif
                            @foreach ($kecamatan as $rowkec)
                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                {{ $rowkec->kd_kecamatan.' - '. $rowkec->nm_kecamatan }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan" id="kd_kelurahan">
                            <option value="">-- Kelurahan / Desa -- </option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button id="tambah_znt" type="button" class="btn btn-sm btn-flat btn-info" data-toggle="modal" data-target="#modal-default">
                            <i class="fas fa-puzzle-piece"></i> Tambah ZNT
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div id="load-body"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah ZNT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table>
                    <tbody>
                        <tr>
                            <td>Kecamatan</td>
                            <td>: <span id="text_kecamatan"></span></td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>: <span id="text_kelurahan"></span></td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" id="val_kd_kecamatan">
                <input type="hidden" id="val_kd_kelurahan">
                <div class="form-group row">
                    <label class="col-form-label col-md-3" for="">Kode ZNT</label>
                    <div class="col-md-4">
                        <input type="text" name="kd_znt" id="kd_znt" class="form-control-sm form-control" maxlength="2">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-3" for="">Lokasi</label>
                    <div class="col-md-8">
                        <select name="lokasi_objek_id" id="lokasi_objek_id" class="form-control form-control-sm"> 
                            <option value=""> Pilih </option>
                            @foreach ($lokasi as $i=> $item)
                            <option value="{{ $i }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="simpanznt" type="button" class="btn btn-primary">Save</button> --}}
                <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal"><i class="far fa-window-close"></i> Close</button>
                <button id="simpanznt" type="button" class="btn btn-primary btn-sm btn-flat"><i class="far fa-save"></i> simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            getKelurahan(kk);
        })

        enableTambah();

        $('#simpanznt').on('click', function(e) {
            e.preventDefault();
            val_kd_kecamatan = $('#val_kd_kecamatan').val()
            val_kd_kelurahan = $('#val_kd_kelurahan').val()
            kd_znt = $('#kd_znt').val()
            lokasi_objek_id = $('#lokasi_objek_id').val()
            openloading()
            $.ajax({
                type: 'POST'
                , url: "{{ route('refrensi.znt.simpan') }}"
                , data: {
                    kd_znt: kd_znt
                    , kd_kecamatan: val_kd_kecamatan
                    , kd_kelurahan: val_kd_kelurahan
                    , lokasi_objek_id: lokasi_objek_id
                }
                , success: function(data) {
                    closeloading()
                    toastr.info(data.msg, 'TAMBAH ZNT');
                    if (data.status == 1) {
                        $('#modal-default').modal('toggle');
                        loadData(val_kd_kecamatan, val_kd_kelurahan);
                    }

                }
            });

        })


        var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
        if (kd_kecamatan == '') {
            var kd_kecamatan = $('#kd_kecamatan').val()
        }
        getKelurahan(kd_kecamatan);

        function getKelurahan(kk) {
            var html = '<option value="">-- pilih --</option>';
            if (kk != '') {
                openloading()
                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        closeloading()
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v + '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan').val(k);
                            }
                        });
                        // console.log(res);
                        $('#kd_kelurahan').html(html);
                        if (count != 1) {
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}").trigger('change');
                        }

                    }
                    , error: function(res) {
                        $('#kd_kelurahan').html(html);
                    }
                });
            } else {
                $('#kd_kelurahan').html(html);
            }

        }

        function enableTambah() {
            var kec = $('#kd_kecamatan').val();
            var kel = $('#kd_kelurahan').val();
            if (kec != '' && kel != '') {
                $('#tambah_znt').prop('disabled', false);
            } else {
                $('#tambah_znt').prop('disabled', true);
            }


        }

        $('#kd_kecamatan,#kd_kelurahan').on('change', function() {
            $('#load-body').html('')
            var kec = $('#kd_kecamatan').val();
            var kel = $('#kd_kelurahan').val();
            if (kec != '' && kel != '') {
                loadData(kec, kel);
                enableTambah()
                $('#val_kd_kecamatan').val(kec)
                $('#val_kd_kelurahan').val(kel)

                $('#text_kecamatan').html($("#kd_kecamatan option:selected").text())
                $('#text_kelurahan').html($("#kd_kelurahan option:selected").text())
                $('#kd_znt').val('')
            }
        })

        function loadData(kd_kecamatan, kd_kelurahan) {
            openloading()
            $.ajax({
                url: "{{ url('refrensi/znt') }}"
                , data: {
                    'kd_kecamatan': kd_kecamatan
                    , 'kd_kelurahan': kd_kelurahan
                }
                , success: function(res) {
                    closeloading()
                    $('#load-body').html(res)
                }
                , error: function(res) {
                    closeloading()
                }
            });
        }
    });

</script>
@endsection
