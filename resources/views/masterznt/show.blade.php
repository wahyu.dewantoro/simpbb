@extends('layouts.app')

@section('content')
    <style>
        ul.no-bullets {
            list-style-type: none;
            /* Remove bullets */
            padding: 0;
            /* Remove padding */
            margin: 0;
            /* Remove margins */
        }
    </style>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail ZNT dan NIR</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a href="{{ route('refrensi.znt.index') }}?kd_kecamatan={{ $data->kd_kecamatan }}&kd_kelurahan={{ $data->kd_kelurahan }}"
                            class="btn btn-primary btn-sm">
                            <i class="fas fa-angle-double-left"></i>
                            Kembali
                        </a>
                    </div>

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>: {{ $data->kecamatan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td>: {{ $data->kelurahan }}</td>
                                    </tr>
                                    <tr>
                                        <td>ZNT</td>
                                        <td>: {{ $data->kd_znt }}</td>
                                    </tr>
                                    <tr>
                                        <td>Created By</td>
                                        <td>: {{ $data->created_by }} <br> <i>{{ $data->created_at }}</i> </td>
                                    </tr>
                                    <tr>
                                        <td>Updated By</td>
                                        <td>: {{ $data->updated_by }} <br> <i>{{ $data->updated_at }}</i> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table class="table table-sm table-bordered  table-striped ">
                                <thead class="bg-success">
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">BLOK PETA</th>
                                        <th class="text-center">

                                            <button id="tambah_blok" type="button" class="btn btn-sm btn-flat btn-warning"
                                                data-toggle="modal" data-target="#modal-blok">
                                                <i class="fas fa-plus-square"></i>
                                            </button>

                                        </th>
                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach ($blokpeta as $i => $item)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $item->kd_blok }}</td>
                                            <td class="text-center">
                                                @if ($item->op == 0)
                                                    @php
                                                        $id = $item->kd_kecamatan . '_' . $item->kd_kelurahan . '_' . $item->kd_znt . '_' . trim($item->kd_blok);
                                                    @endphp
                                                    <a data-href="{{ route('refrensi.znt.hapus-blok', $id) }}"
                                                        class="hapus-blok">
                                                        <i class="fas fa-trash text-danger"></i>
                                                    </a>
                                                @else
                                                    <small class="text-warning"><i
                                                            class="fas fa-exclamation-triangle"></i>Terpakai</small>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-sm table-bordered  table-striped ">
                            <thead class="bg-success">
                                <tr>
                                    <th style="text-align: center">TAHUN</TH>
                                    <th style="text-align: center">NIR</TH>
                                    <th style="text-align: center">KELAS</TH>
                                    <th style="text-align: center">NJOP/M<SUP>2</SUP></TH>
                                    <th>HKPD</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th style="text-align: center" width="200px">
                                        <button id="tambah_nir" type="button" class="btn btn-sm btn-flat btn-warning">
                                            <i class="fas fa-plus-square"></i>
                                        </button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($nir as $item)
                                    <tr>
                                        <td class="text-center">{{ $item->thn_nir_znt }}</td>
                                        <td class="text-right">{{ angka($item->nir) }}</td>
                                        <td class="text-center">{{ $item->kd_kls_tanah }}</td>
                                        <td class="text-right">{{ angka($item->nilai_per_m2_tanah) }}</td>
                                        <td class="text-center">{{ $item->nilai_hkpd }} %</td>
                                        <td>
                                            {{ $item->created_by }}<br>
                                            <small>{{ $item->created_at }}</small>
                                        </td>
                                        <td>
                                            {{ $item->updated_by }}<br>
                                            <small>{{ $item->updated_at }}</small>
                                        </td>
                                        <td class="text-center">
                                            @php
                                                $id = $data->kd_kecamatan . '_' . $data->kd_kelurahan . '_' . $data->kd_znt . '_' . $item->thn_nir_znt;
                                            @endphp
                                            {{-- ||  $item->thn_nir_znt =='2024' --}}
                                            @if ($item->pakai == 0)
                                                <a href="#" class="edit_nir" data-nir="{{ $item->nir }}"
                                                    data-tahun="{{ $item->thn_nir_znt }}"
                                                    data-nilai_hkpd="{{ $item->nilai_hkpd }}"><i
                                                        class="fas fa-edit text-info"></i></a>
                                                <a data-href="{{ route('refrensi.znt.hapus-nir', $id) }}" class="hapus">
                                                    <i class="fas fa-trash text-danger"></i>
                                                </a>
                                            @else
                                                <small class="text-warning"><i
                                                        class="fas fa-exclamation-triangle"></i>Terpakai</small>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="title-form-nir">Tambah NIR</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table>
                        <tbody>
                            <tr>
                                <td>Kecamatan</td>
                                <td>: {{ $data->kecamatan }}</td>
                            </tr>
                            <tr>
                                <td>Kelurahan</td>
                                <td>: {{ $data->kelurahan }}</td>
                            </tr>
                            <tr>
                                <td>ZNT</td>
                                <td>: {{ $data->kd_znt }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" id="val_kd_kecamatan" value="{{ $data->kd_kecamatan }}">
                    <input type="hidden" id="val_kd_kelurahan" value="{{ $data->kd_kelurahan }}">
                    <input type="hidden" id="val_kd_znt" value="{{ $data->kd_znt }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label" for="">TAHUN</label>
                                <input type="hidden" id="is_edit_nir" value="">
                                <div class="col-md-6">
                                    <input type="text" name="thn_nir_znt" id="thn_nir_znt"
                                        class="form-control-sm form-control" maxlength="4">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label" for="">NIR</label>
                                <div class="col-md-6">
                                    <input type="text" name="nir" id="nir"
                                        class="form-control-sm form-control angka">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label" for="">HKPD (%)</label>
                                <div class="col-md-6">
                                    <input type="text" name="nilai_hkpd" id="nilai_hkpd"
                                        class="form-control-sm form-control angka" maxlength="3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>
                        Close</button>
                    <button id="simpan_nir" type="button" class="btn btn-primary"><i class="fas fa-save"></i>
                        Simpan</button>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="modal-blok">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Blok Peta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table>
                        <tbody>
                            <tr>
                                <td>Kecamatan</td>
                                <td>: {{ $data->kecamatan }}</td>
                            </tr>
                            <tr>
                                <td>Kelurahan</td>
                                <td>: {{ $data->kelurahan }}</td>
                            </tr>
                            <tr>
                                <td>ZNT</td>
                                <td>: {{ $data->kd_znt }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" id="val_kd_kecamatan" value="{{ $data->kd_kecamatan }}">
                    <input type="hidden" id="val_kd_kelurahan" value="{{ $data->kd_kelurahan }}">
                    <input type="hidden" id="val_kd_znt" value="{{ $data->kd_znt }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Blok</label>

                                <select name="kd_blok" id="kd_blok" class="form-control">
                                    @foreach ($blok_arr as $rk)
                                        <option value="{{ $rk }}">{{ $rk }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="simpan_blok" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.hapus').on('click', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Menghapus data NIR",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        document.location.href = $(this).data('href');
                    }
                })
            });


            $('.hapus-blok').on('click', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Menghapus data BLOK ZNT ",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        document.location.href = $(this).data('href');
                    }
                })
            });

            $('#simpan_blok').on('click', function(e) {
                e.preventDefault();
                val_kd_kecamatan = $('#val_kd_kecamatan').val()
                val_kd_kelurahan = $('#val_kd_kelurahan').val()
                val_kd_znt = $('#val_kd_znt').val()
                kd_blok = $('#kd_blok').val()
                openloading()
                $.ajax({
                    type: 'POST',
                    url: "{{ route('refrensi.znt.simpan-blok') }}",
                    data: {
                        kd_znt: val_kd_znt,
                        kd_kecamatan: val_kd_kecamatan,
                        kd_kelurahan: val_kd_kelurahan,
                        kd_blok: kd_blok
                    },
                    success: function(data) {
                        closeloading()
                        alert(data.msg)
                        if (data.status == 1) {
                            $('#modal-blok').modal('toggle');
                            window.location.reload();
                        }
                    },
                    error: function(er) {
                        closeloading()
                    }
                });
            })

            $('#simpan_nir').on('click', function(e) {
                e.preventDefault();
                val_kd_kecamatan = $('#val_kd_kecamatan').val()
                val_kd_kelurahan = $('#val_kd_kelurahan').val()
                val_kd_znt = $('#val_kd_znt').val()
                thn_nir_znt = $('#thn_nir_znt').val()
                nir = $('#nir').val()
                nilai_hkpd = $('#nilai_hkpd').val()
                is_edit = $('#is_edit_nir').val()
                openloading()
                $.ajax({
                    type: 'POST',
                    url: "{{ route('refrensi.znt.simpan-nir') }}",
                    data: {
                        kd_znt: val_kd_znt,
                        kd_kecamatan: val_kd_kecamatan,
                        kd_kelurahan: val_kd_kelurahan,
                        nir: nir,
                        thn_nir_znt: thn_nir_znt,
                        is_edit: is_edit,
                        nilai_hkpd: nilai_hkpd
                    },
                    success: function(data) {
                        closeloading()
                        alert(data.msg)
                        if (data.status == 1) {
                            $('#modal-default').modal('toggle');
                            window.location.reload();
                        }
                    },
                    error: function(er) {
                        closeloading()
                    }
                });
            })

            $('#tambah_nir').on('click', function(e) {
                $('#thn_nir_znt').val('')
                $('#nir').val('')
                $('#is_edit_nir').val(0);
                $('#title-form-nir').html('Tambah NIR')
                $("#thn_nir_znt").attr("readonly", false);
                $('#modal-default').modal('show')
            })


            $('.edit_nir').on('click', function(e) {
                thn_nir_znt = $(this).data('tahun')
                nir = $(this).data('nir')
                nilai_hkpd = $(this).data('nilai_hkpd')
                $('#thn_nir_znt').val(thn_nir_znt)
                $('#nir').val(nir)
                $('#nilai_hkpd').val(nilai_hkpd)
                $('#is_edit_nir').val(1);
                $("#thn_nir_znt").attr("readonly", true);
                $('#title-form-nir').html('Edit NIR')
                $('#modal-default').modal('show')
            })
        });
    </script>
@endsection
