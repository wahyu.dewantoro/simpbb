@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>SPPT Masal</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        {{-- @can('add_users') --}}
                        {{-- <a href="{{ url('refrensi/struktural/create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-user-plus"></i> Create
                        </a> --}}
                        {{-- @endcan --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="card card-primary card-outline">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-5 col-sm-3">
                        <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                            aria-orientation="vertical">
                            <a class="nav-link active" id="pbbmin" data-toggle="pill" href="#vert-tabs-home" role="tab"
                                aria-controls="vert-tabs-home" aria-selected="true">PBB Minimal</a>
                            <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill"
                                href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile"
                                aria-selected="false">NJOP Minimal</a>
                            <a class="nav-link" id="vert-tabs-settings-tab" data-toggle="pill"
                                href="#vert-tabs-settings" role="tab" aria-controls="vert-tabs-settings"
                                aria-selected="false">Copy SPPT</a>
                            <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill"
                                href="#vert-tabs-messages" role="tab" aria-controls="vert-tabs-messages"
                                aria-selected="false">Penilaian & Penetapan</a>

                        </div>
                    </div>
                    <div class="col-7 col-sm-9">
                        <div class="tab-content" id="vert-tabs-tabContent">
                            <div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel"
                                aria-labelledby="pbbmin">
                                <form action="#" method="post">
                                    @csrf
                                    @method('post')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="thn_pbb_minimal">Tahun Pajak</label>
                                                <input type="text" id="thn_pbb_minimal"
                                                    class="form-control form-control-sm {{ $errors->has('thn_pbb_minimal') ? 'is-invalid' : '' }} angka">
                                                <span
                                                    class="errorr invalid-feedback">{{ $errorrs->first('thn_pbb_minimal') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="no_sk_pbb_minimal">Nomor SK</label>
                                                <input type="text" id="no_sk_pbb_minimal"
                                                    class="form-control form-control-sm {{ $errors->has('no_sk_pbb_minimal') ? 'is-invalid' : '' }} ">
                                                <span
                                                    class="errorr invalid-feedback">{{ $errorrs->first('no_sk_pbb_minimal') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="tgl_sk_pbb_minimal">Tanggal SK</label>
                                                <input type="text" id="tgl_sk_pbb_minimal"
                                                    class="form-control form-control-sm {{ $errors->has('tgl_sk_pbb_minimal') ? 'is-invalid' : '' }} tanggal" >
                                                <span
                                                    class="errorr invalid-feedback">{{ $errorrs->first('tgl_sk_pbb_minimal') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="nilai_pbb_minimal">PBB Minimal</label>
                                                <input type="text" id="nilai_pbb_minimal"
                                                    class="form-control form-control-sm {{ $errors->has('nilai_pbb_minimal') ? 'is-invalid' : '' }} angka">
                                                <span
                                                    class="errorr invalid-feedback">{{ $errorrs->first('nilai_pbb_minimal') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </form>





                            </div>
                            <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel"
                                aria-labelledby="vert-tabs-profile-tab">
                                Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut
                                ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                                cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis
                                posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere
                                nec nunc. Nunc euismod pellentesque diam.
                            </div>
                            <div class="tab-pane fade" id="vert-tabs-messages" role="tabpanel"
                                aria-labelledby="vert-tabs-messages-tab">
                                Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat
                                augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit
                                sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut
                                velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus
                                tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet
                                sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum
                                gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt
                                eleifend ac ornare magna.
                            </div>
                            <div class="tab-pane fade" id="vert-tabs-settings" role="tabpanel"
                                aria-labelledby="vert-tabs-settings-tab">
                                Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus
                                turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis
                                vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum
                                pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet
                                urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse
                                platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
