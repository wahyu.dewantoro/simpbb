@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="text-danger">Kesalahan Server</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="error-page">
                <h2 class="headline text-danger" style="font-size: 100px;">500</h2>
                <div class="error-content text-center">
                    <h3 class="mt-4">
                        <i class="fas fa-exclamation-triangle text-danger" style="font-size: 60px;"></i>
                        Ups! Terjadi kesalahan.
                    </h3>
                    <p class="mt-3">
                        Kami mengalami kesalahan yang tidak terduga. Silakan coba lagi nanti.
                    </p>
                    <p>
                        Anda dapat <a href="{{ url('/') }}" class="">kembali ke dashboard</a> atau menghubungi dukungan jika masalah berlanjut.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
