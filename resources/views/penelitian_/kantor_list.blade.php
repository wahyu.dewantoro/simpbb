@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3"><b>Penelitian Kantor</b></h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <li class="nav-item">
                                <a class="nav-link rounded-0 active" href="#tab-ajuan" data-toggle="tab">
                                    <i class="fas fa-file-signature"></i> Ajuan Penelitian
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link rounded-0" href="#tab-penelitian" data-toggle="tab">
                                    <i class="fas fa-file-signature"></i> Data Penelitian
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('penelitian/kantor-form') }}" class="nav-link rounded-0" target="_BLANK">
                                    <i class="fas fa-folder-plus"></i> Form Penelitian (Non Ajuan)
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body p-1 ">
                    <div class="tab-content">
                            <div id="tab-penelitian" class="tab-pane ">
                                <div class="row">
                                    <div class="col-md-12 pr-1">
                                        <div class="card rounded-0 mb-0">
                                            <div class="card-header">
                                                <h3 class="card-title">Hasil Penelitian</h3>
                                            </div>
                                            <div class="card-body p-1 table-responsive-sm">
                                                <table class="table table-bordered table-hover table-sm table-striped" id="penelitian-table">
                                                    <thead>
                                                        <tr classs="text-center">
                                                            <th class='number'>No</th>
                                                            <th>NOP</th>
                                                            <th>Nama</th>
                                                            <th>Jenis</th>
                                                            <th>Created</th>
                                                            <th>Pemutakhiran</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class='table-sm'></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-ajuan" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-12 pl-1">
                                        <div class="card rounded-0 mb-0">
                                            <div class="card-header">
                                                <h3 class="card-title">Todo List</h3>
                                            </div>
                                            <div class="card-body p-1 table-responsive-sm">
                                                <table class="table table-bordered table-hover table-sm table-striped" id="ajuan-table">
                                                    <thead>
                                                        <tr classs="text-center">
                                                            <th class='number'>No</th>
                                                            <th>No. Pelayanan</th>
                                                            <th>Ajuan</th>
                                                            <th>Created</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class='table-sm'></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
    <script>                                                                                          
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data penelitian kantor.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'id':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#ajuan-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('penelitian/kantor_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'nomor_layanan',class:'w-10'},
                    { data: 'raw_tag'},
                    { data: 'created_at',class:'text-center w-15'},
                    { data: 'option',class:'text-center w-10'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let datatablepenelitian=$("#penelitian-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('penelitian/kantor_penelitian_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'nop',class:'w-10'},
                    { data: 'nama_wp',class:'w-20'},
                    { data: 'action'},
                    // { data: 'kd_status',class:'text-center'},
                    { data: 'created_at',class:'text-center'},
                    { data: 'pemutakhiran_at',class:'text-center'},
                    { data: 'option',class:'text-center'}
                ]
            });
            datatablepenelitian.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
        });
    </script>
@endsection
