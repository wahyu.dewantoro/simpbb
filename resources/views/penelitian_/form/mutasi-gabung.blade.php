<div class="row">
    <div class="col-md-6 pr-1">
        <div class="card mb-1 rounded-0">
            <div class="card-header d-flex">
                <h3 class="card-title">Data Subjek</h3>
            </div>
            <div class="card-body p-1 form-group-sm-mb">
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">NIK*</label>
                            <div class="col-md-9 p-0 input-group">
                                <input type="text" name="nik_wp" id="nik_wp" class="form-control form-control-sm" value="{{ $data['layanan']->nik_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">NAMA*</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="nama_wp" id="nama_wp"
                                class="form-control form-control-sm" required
                                value="{{ $data['layanan']->nama_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">ALAMAT*</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="alamat_wp" id="alamat_wp"
                                class="form-control form-control-sm" required
                                value="{{ $data['layanan']->alamat_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">NO TELP.*</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="telp_wp" id="telp_wp" maxlength="20"
                                class="form-control form-control-sm numeric" required
                                value="{{ $data['layanan']->nomor_telepon }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-6 col-form-label">RT* (Max 3 digit)</label>
                            <div class="col-md-6 p-0">
                                <input type="text" name="rt_wp"  maxlength="3" class="form-control form-control-sm numeric" required  value="{{ $data['layanan']->rt_wp }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-6 col-form-label">RW* (Max 2 digit)</label>
                            <div class="col-md-6 p-0">
                                <input type="text" name="rw_wp"  maxlength="2"  class="form-control form-control-sm "  required  value="{{ $data['layanan']->rw_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">KELURAHAN*</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="kelurahan_wp" maxlength="30" class="form-control form-control-sm "  required  value="{{ $data['layanan']->kelurahan_wp }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">KECAMATAN*</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="kecamatan_wp" maxlength="30" class="form-control form-control-sm " required value="{{ $data['layanan']->kecamatan_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">KOTA/KABUPATEN*</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="kabupaten_wp" maxlength="30" class="form-control form-control-sm "  required value="{{ $data['layanan']->dati2_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row hidden'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">PROPINSI*</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="propinsi_wp" maxlength="30" class="form-control form-control-sm " value="{{ $data['layanan']->propinsi_wp }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 pl-1">
        <div class="card mb-1 rounded-0">
            <div class="card-header d-flex">
                <h3 class="card-title">Data Objek Induk</h3>
            </div>
            <div class="card-body p-1 form-group-sm-mb" >
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">NOP*</label>
                            <input type="text" name="nop_induk" class="form-control  numeric nop_full" placeholder="NOP" required value="{{ $data['layanan']->nop }}">
                            @if($data['ceknop'])
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info" data-nop='nop' aria-expanded="false">
                                        <i class='fas fa-plus-square'></i> Cek NOP
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">ALAMAT*</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="alamat_op_induk" maxlength="30" class="form-control form-control-sm " required  value="{{ $data['layanan']->alamat_op }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-6 col-form-label">RT* (Max 3 digit)</label>
                            <div class="col-md-6 p-0">
                                <input type="text" name="rt_op_induk"  maxlength="3"  class="form-control form-control-sm numeric" value="{{ $data['layanan']->rt_op }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-6 col-form-label">RW* (Max 2 digit)</label>
                            <div class="col-md-6 p-0">
                                <input type="text" name="rw_op_induk"  maxlength="2"  class="form-control form-control-sm numeric" value="{{ $data['layanan']->rw_op }}"required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">KELURAHAN</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="kelurahan_op_induk" maxlength="30" class="form-control form-control-sm " value="{{ $data['layanan']->nm_kelurahan }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">KECAMATAN</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="kecamatan_op_induk"  maxlength="30" class="form-control form-control-sm " value="{{ $data['layanan']->nm_kecamatan }}" readonly> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">KOTA/KABUPATEN</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="kabupaten_op_induk" maxlength="30" class="form-control form-control-sm " value="{{ $data['layanan']->kd_dati2 }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row hidden'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-3 col-form-label">PROPINSI</label>
                            <div class="col-md-9 p-0">
                                <input type="text" name="propinsi_op_induk" maxlength="30" class="form-control form-control-sm " value="{{ $data['layanan']->kd_propinsi }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">Luas Bumi*</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="luas_bumi_induk" class="form-control form-control-sm numeric" data-calc="jenis_layanan_id" value="{{ $data['layanan']->luas_bumi }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">NJOP Bumi</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="njop_bumi_induk" class="form-control form-control-sm numeric" placeholder="NJOP Bumi" value="{{ $data['layanan']->njop_bumi }}">
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">Luas Bangunan*</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="luas_bng_induk" class="form-control form-control-sm numeric" value="{{ $data['layanan']->luas_bng }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class='input-group'>
                            <label class="col-md-5 col-form-label">NJOP Bangunan</label>
                            <div class="col-md-7 p-0">
                                <input type="text" name="njop_bng_induk" class="form-control form-control-sm numeric" value="{{ $data['layanan']->njop_bng }}" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div> 
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Objek Gabung</h3>
            </div>
            <div class="card-body">
                <div class='row'>
                    <table data-table="objek-gabung" class="table table-bordered table-striped table-counter table-sm">
                        <thead class='text-center'>
                            <tr>
                                <th class='number'>No</th>
                                <th class="w-15">NOP</th>
                                <th class="w-15">Luas Bumi</th>
                                <th class="w-15">Luas Bangunan</th>
                                <th>Alamat</th>
                                <th class="w-10">RT/RW</th>
                                <th class="w-15">Kelurahan</th>
                                <th class="w-15">Kecamatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total=0;
                            @endphp
                            @if(isset($data['nopgabung']))
                                @foreach($data['nopgabung'] as $nopg)
                                    <tr>
                                        <td>
                                            <input type="hidden" name="nop_gabung[]" value="{{ $nopg['objek_id'] }}">
                                        </td>
                                        <td>{{$nopg['nop']}}</td>
                                        <td>{{$nopg['luas_bumi']}}</td>
                                        <td>{{$nopg['luas_bng']}}</td>
                                        <td>{{$nopg['alamat_op']}}</td>
                                        <td>{{$nopg['rtrw']}}</td>
                                        <td>{{$nopg['kelurahan']}}</td>
                                        <td>{{$nopg['kecamatan']}}</td>
                                    </tr>
                                    @php
                                        $total=$total+$nopg['luas_bumi'];
                                    @endphp
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer p-1">
                <div class="card-widget bold">
                    <ul>
                        <li>Total Luas Bumi   : <span name="total_luas_bumi">{{$total}}</span></li>
                        <li>Hasil Luas Bumi : <span name="sisa_luas_bumi">{{ ($data['layanan']->luas_bumi)?$data['layanan']->luas_bumi+$total:0 }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> 
    <div class="col-12">
        <textarea class="notes" name="uraian">{{ $data['layanan']->uraian }}</textarea>
    </div>
    <!-- @ include('penelitian.form.dokumen') -->
</div>
<div class="card-footer p-1">
    <div class="row">
        <div class="col-md-6 pr-1"><a href="{{ url($data['back'])}}" class="btn btn-block btn-default">Batal</a></div>
        <div class="col-md-6 pl-1"> <button type="submit" class="btn btn-block btn-primary">Simpan</button> </div>
    </div>
</div>