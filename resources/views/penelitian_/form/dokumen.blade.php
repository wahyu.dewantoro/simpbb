@if($data['dokumen'])
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Dokumen Pendukung</h3>
        </div>
        <div class="card-body">
            <div class='row'>
                <table data-table="dokumen-table" class="table table-bordered table-striped table-counter table-sm">
                    <thead class='text-center'>
                        <tr>
                            <th class='number'>No</th>
                            <th>Dokumen</th>
                            <th>Jenis Dokumen</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['dokumen'] as $dokumen_list)
                            <tr>
                                <td>{{$dokumen_list[0]}}</td>
                                <td>{{$dokumen_list[1]}}</td>
                                <td>{{$dokumen_list[2]}}</td>
                                <td>{{$dokumen_list[3]}}</td>
                                <td>
                                    <a href="{{$dokumen_list[4]}}" target="_BLANK" class="btn btn-info btn-block">Preview</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif