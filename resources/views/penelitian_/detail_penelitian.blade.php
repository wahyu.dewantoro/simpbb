@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/summernote/summernote.min.css">
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline card-tabs no-radius no-margin rounded-0" data-card='main'>
                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3"><b>{{ $data['title'] }}</b></h3>
                    </div>
                    <form action="{{ $data['action'] }}" method="post">
                        <div class="card-body p-2">
                            @csrf
                            @method($data['method'])
                            <div class="row mb-2">
                                <div class="col-md-6 pr-1">
                                    <p class="bg-warning color-palette p-2 mb-0">
                                        <strong>Nomor Permohonan :</strong> {{ $data['layanan']->nomor_layanan }}
                                    </p>
                                    
                                </div>
                                <div class="col-md-6 pl-1">
                                    <p class="bg-warning color-palette p-2 mb-0">
                                        <strong>Permohonan :</strong> {{ $data['layanan']->jenis_layanan_nama }}
                                    </p>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{ $data['layanan']->id }}">
                            <input type="hidden" name="nomor_layanan" value="{{ $data['layanan']->nomor_layanan }}">
                            <input type="hidden" name="layanan_objek_id" value="{{ $data['layanan']->layanan_objek_id }}">
                            <input type="hidden" name="jenis_layanan_id" value="{{ $data['layanan']->jenis_layanan_id }}">
                                @include('penelitian.form.'.$data['page'])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
    
@endsection
@section('script')
    {{-- <script src=""></script> --}}
    <script src="{{ asset('lte') }}/plugins/summernote/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.notes').summernote({
                placeholder: 'Uraian penelitian',
                height: 100
            });
            $(".nik").inputmask('9999999999999999');
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
        });
    </script>
@endsection
