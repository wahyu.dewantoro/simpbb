<div class="row">
    <div class="col-md-6">
        <div class="card card-info card-outline">
            <div class="card-header">
                <p class="card-title">
                    Subjek Pajak
                </p>
            </div>
            <div class="card-body p-1">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nik_wp">NIK</label>
                            <input type="text" name="nik_wp" id="nik_wp"
                                class="form-control form-control-sm"
                                value="{{ $data['objek']->nik_wp }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Wajib Pajak</label>
                            <input type="text" name="nama_wp" id="nama_wp"
                                class="form-control form-control-sm" required
                                value="{{ $data['objek']->nama_wp }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="telp_wp">Telp</label>
                            <input type="text" name="telp_wp" id="telp_wp"
                                class="form-control form-control-sm angka">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="alamat_wp">Alamat</label>
                            <input type="text" name="alamat_wp" id="alamat_wp"
                                class="form-control form-control-sm" required
                                value={{ $data['objek']->alamat_wp }}>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="rt_wp">RT</label>
                                    <input type="text" name="rt_wp" id="rt_wp"
                                        class="form-control form-control-sm" required
                                        value="{{ $data['objek']->rt_wp }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="rw_wp">RW </label>
                                    <input type="text" name="rw_wp" id="rw_wp"
                                        class="form-control form-control-sm" required
                                        value="{{ $data['objek']->rw_wp }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kelurahan_wp">Kelurahan</label>
                            <input type="text" name="kelurahan_wp" id="kelurahan_wp"
                                class="form-control form-control-sm" required
                                value="{{ $data['objek']->kelurahan_wp }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="kecamatan_wp">Kecamatan</label>
                            <input type="text" name="kecamatan_wp" id="kecamatan_wp"
                                class="form-control form-control-sm" required
                                value="{{ $data['objek']->kecamatan_wp }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="kode_pos_wp">Kode Pos</label>
                            <input type="text" name="kode_pos_wp" id="kode_pos_wp"
                                class="form-control form-control-sm" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="kabupaten_wp">Kabupaten</label>
                            <input type="text" name="kabupaten_wp" id="kabupaten_wp"
                                class="form-control form-control-sm" required
                                value="{{ $data['objek']->dati2_wp }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <p class="card-title">Objek Pajak</p>
            </div>
            <div class="card-body p-1">
               {{--  <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nop">NOP</label>
                            <input type="text" name="nop" id="nop"
                                class="form-control form-control-sm angka" required
                                value="{{ $data['objek']->kd_propinsi .$data['objek']->kd_dati2 .$data['objek']->kd_kecamatan .$data['objek']->kd_kelurahan .$data['objek']->kd_blok .$data['objek']->no_urut .$data['objek']->kd_jns_op }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="luas_tanah">Luas Tanah</label>
                            <input type="text" name="luas_tanah" id="luas_tanah"
                                class="form-control form-control-sm angka" required
                                value="{{ $data['objek']->luas_bumi }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="luas_bangunan">Luas Bangunan</label>
                            <input type="text" name="luas_bangunan" id="luas_bangunan"
                                class="form-control form-control-sm angka" required
                                value="{{ $data['objek']->luas_bng }}">
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="lokasi_op">Alamat OP</label>
                            <input type="text" name="lokasi_op" id="lokasi_op"
                                value="{{ $data['alamat_op']->alamat_op }}"
                                class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="rt_op">RT</label>
                                    <input type="text" name="rt_op" id="rt_op"
                                        value="{{ $data['alamat_op']->rt_op }}"
                                        class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="rw_op">RW</label>
                                    <input type="text" name="rw_op" id="rw_op"
                                        value="{{ $data['alamat_op']->rw_op }}"
                                        class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kelurahan_op">Kelurahan</label>
                            <input type="text" name="kelurahan_op" id="kelurahan_op"
                                value="{{ $data['alamat_op']->nm_kelurahan }}"
                                class="form-control form-control-sm" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kecamatan_op">Kecamatan</label>
                            <input type="text" name="kecamatan_op" id="kecamatan_op"
                                value="{{ $data['alamat_op']->nm_kecamatan }}"
                                class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="kabupaten_op">Kabupaten</label>
                            <input type="text" name="kabupaten_op" id=""
                                class="form-control form-control-sm" required
                                value="Malang">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    
    <div class="col-12">
        <div class="float-right">
            <button class="btn btn-sm btn-success"><i class="far fa-save"></i> Simpan</button>
            <button class="btn btn-sm btn-warning"><i class="far fa-window-close"></i> Batal</button>
        </div>
    </div>
</div>