@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/summernote/summernote.min.css">
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline card-tabs no-radius no-margin rounded-0" data-card='main'>
                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3"><b>Form Penelitian Kantor</b></h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            @php
                                $first='active';
                            @endphp
                            @foreach($jenisLayanan as $item)
                            <li class="nav-item">
                                <a class="nav-link rounded-0 {{$first}}" href="#tab-{{$item['tag']}}" data-toggle="tab">
                                    <i class="fas fa-folder-plus"></i> {{$item['nama']}}
                                </a>
                            </li>
                            @php
                                $first='';
                            @endphp
                            @endforeach
                        </ul>
                    </div>
                    <div class="card-body p-1 ">
                        <div class="tab-content">
                            @php
                                $first='active';
                            @endphp
                            @foreach($jenisLayanan as $item)
                                <div id="tab-{{$item['tag']}}" class="tab-pane {{ $first }}">
                                 @include('penelitian/form/'.$item['tag'])
                                </div>
                            @php
                                $first='';
                            @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
    {{-- <script src=""></script> --}}
    <script src="{{ asset('lte') }}/plugins/summernote/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.notes').summernote({
                placeholder: 'Uraian penelitian',
                height: 100
            });
            $(".nik").inputmask('9999999999999999');
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
        });
    </script>
@endsection
