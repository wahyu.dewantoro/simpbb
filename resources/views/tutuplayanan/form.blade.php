@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-10">
                <h1>{{ $title??'FORM TUTUP LAYANAN' }}</h1>
            </div>
            <div class="col-sm-2">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body p-2">
                        <form action="{{ $data['action'] }}" method="post">
                            @csrf
                            @method($data['method'])


                            <div class="form-group">
                                <label for="tgl_mulai">Mulai</label>
                                <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control form-control-sm tanggal" value="{{ isset($data['tl']->tgl_mulai)?date('d M Y',strtotime($data['tl']->tgl_mulai)) :'' }}" required>
                            </div>
                            <div class="form-group">
                                <label for="tgl_selesai">Selesai</label>

                                <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control form-control-sm tanggal" value="{{ isset($data['tl']->tgl_selesai)?date('d M Y',strtotime($data['tl']->tgl_selesai)) :''  }}" required>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <input type="text" name="deskripsi" id="deskripsi" class="form-control form-control-sm" value="{{ $data['tl']->deskripsi??'' }}" required>
                            </div>
                            <div class="float-right">
                                <button class="btn btn-float btn-info"><i class="fas fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
