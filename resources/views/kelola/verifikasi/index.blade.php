@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Verifikasi Akun Wajib Pajak</h1>
                </div>
                <div class="col-sm-2s">
                    <div class="float-sm-right">
                        {{-- @if (Auth()->user()->is_pengelola == '1')
                            <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-nop/create') }}"><i
                                    class="fas fa-file-alt"></i> Buat Usulan</a>
                        @endif --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <form action="{{ url()->current() }}" id="formSearch">
                                <div class="row">
                                    @php
                                        $array = [
                                            'all' => 'Semua',
                                            'rayon' => 'Rayon',
                                            'developer' => 'Developer',
                                            'wp' => 'Wajib Pajak',
                                        ];
                                    @endphp

                                    <div class="col-lg-4">
                                        <select name="jenis" id="jenis" class="form-control">
                                            @php
                                                $sl = request()->get('jenis');
                                            @endphp
                                            @foreach ($array as $i => $item)
                                                <option @if ($i == $sl) selected @endif
                                                    value="{{ $i }}">{{ $item }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="col-lg-4   ">

                                        <div class="input-group">
                                            <input class="form-control py-2 border-right-0 border" type="search"
                                                value="{{ request()->get('search') }}" id="search" name='search'
                                                placeholder="Pencarian">
                                            <span class="input-group-append">
                                                <div class="input-group-text bg-transparent"><i class="fa fa-search"></i>
                                                </div>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center" width="3px">No</th>
                                        <th>Rayon/Developer</th>
                                        <th>No Surat</th>
                                        <th>Tanggal</th>
                                        {{-- <th class="text-center">Pemohon</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Objek</th>
                                        <th class="text-center">Status</th> --}}
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $i => $item)
                                        <tr>
                                            <td class="text-center" width="3px">{{ $i = $data->firstItem() + $i }}</td>
                                            <td>{{ $item->nama_pengembang }}</td>
                                            <td>{{ $item->no_surat }}</td>
                                            <td>{{ tglIndo($item->tgl_surat) }}</td>

                                            <td class="text-center">
                                                <a
                                                    href="{{ route('kelola-nop-verifikasi.edit', $item->id) }}?model={{ encrypt($item->model) }}">
                                                    <i class="fas fa-check-circle text-primary"></i> Verifikasi
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    Total : {{ $data->total() }}
                                </div>
                                <div class="col-6">
                                    {{-- {!! $data->links() !!} --}}
                                    {{ $data->appends(['search' => request('search'), 'jenis' => request('jenis')])->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // Event onchange untuk selectbox
            $('#jenis').on('change', function() {
                // Submit form  saat selectbox berubah
                $('#formSearch').submit();
            });
        })
    </script>
@endsection
