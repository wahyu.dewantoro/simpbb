@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Verifikasi User</h1>
                </div>
                <div class="col-sm-2s">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline">
                        <form action="{{ $data['action'] }}" method="POST">
                            <div class="card-body">
                                @csrf
                                @method($data['method'])
                                <input type="hidden" name="model" value="{{ $data['model'] }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <table class="table-borderless table-sm text-sm">
                                            <tbody>

                                                <tr>
                                                    <td width=150px">Nama</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $data['data']->user->nama ?? ($data['data']->nama ?? '') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->user->alamat_user ?? ($data['data']->alamat_user ?? '') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Kelurahan/Desa</td>
                                                    <td>:</td>

                                                    <td>
                                                        {{ $data['data']->user->kelurahan_user ?? ($data['data']->kelurahan_user ?? '') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Kecamatan</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->user->kecamatan_user ?? ($data['data']->kecamatan_user ?? '') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Kab/Kota</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->user->dati2_user ?? ($data['data']->dati2_user ?? '') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Propinsi</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->user->propinsi_user ?? ($data['data']->propinsi_user ?? '') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Telepon/Whatsapp</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->user->telepon ?? ($data['data']->telepon ?? '') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->user->email ?? ($data['data']->email ?? '') }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-6">
                                        <table class="table table-sm table-borderless text-sm">
                                            @if ($data['model'] == 'UsulanPengelola')
                                                <tr>
                                                    <td width=150px">PT/CV Developer </td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $data['data']->nama_pengembang }}</td>
                                                </tr>
                                            @endif
                                            @if ($data['model'] == 'UserRayon')
                                                <tr>
                                                    <td width=150px">Rayon </td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $data['data']->nmkelurahan }} -
                                                        {{ $data['data']->nmkecamatan }}
                                                    </td>
                                                </tr>
                                            @endif
                                            @if ($data['data']->no_surat != '')
                                                <tr>
                                                    <td>No Surat</td>
                                                    <td>:</td>
                                                    <td>{{ $data['data']->no_surat }}</td>
                                                </tr>
                                            @endif
                                            @if ($data['data']->tgl_surat != '')
                                                <tr>
                                                    <td>Tanggal Surat</td>
                                                    <td>:</td>
                                                    <td>{{ tglindo($data['data']->tgl_surat) }}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>File Pendukung</td>
                                                <td>:</td>
                                                <td>
                                                    @isset($data['data']->Dokumen->url)
                                                        <a class="my-link" href="{{ url($data['data']->Dokumen->url) }}"><i
                                                                class="fas fa-file-pdf"></i>
                                                            {{ $data['data']->Dokumen->nama_dokumen }}</a> <br>
                                                    @endisset

                                                    @foreach ($data['dokumen'] as $item)
                                                        <a class="my-link" href="{{ url($item->url) }}"><i
                                                                class="fas fa-file-pdf"></i>
                                                            {{ $item->nama_dokumen }}</a> <br>
                                                    @endforeach

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Verifikasi</td>
                                                <td>:</td>
                                                <td>
                                                    <select class="form-control form-control-sm" name="verifikasi_kode"
                                                        id="verifikasi_kode" required>
                                                        <option value="">Pilih</option>
                                                        <option value="1">Disetujui</option>
                                                        <option value="0">Ditolak</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <td>:</td>
                                                <td>
                                                    <textarea class="form-control" placeholder="Masukan keterangan verifikasi ......." name="verifikasi_keterangan"
                                                        id="verifikasi_keterangan" rows="3"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <button class="btn btn-sm btn-flat btn-outline-primary"><i class="far fa-save"></i>
                                        Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            function openWindow(url) {

                var windowHeight = window.screen.height; // Mengambil tinggi layar
                var windowWidth = 600; // Lebar tetap
                window.open(url, 'newWindow', 'width=' + windowWidth + ',height=' + windowHeight +
                    ',top=0,left=100,scrollbars=yes');
            }

            $('.my-link').on('click', function(event) {
                event.preventDefault(); // Mencegah tindakan default link (membuka URL)
                var hrefValue = $(this).attr('href'); // Mengambil nilai href
                openWindow(hrefValue);
            });



            function validasiTextarea(status) {
                if (status == '0') {
                    $('#verifikasi_keterangan').attr('required', 'required');
                } else {
                    $('#verifikasi_keterangan').removeAttr('required');
                }
            }

            $('#verifikasi_kode').on('change', function() {
                var status = $("#verifikasi_kode").val();
                validasiTextarea(status)
            });

            $('#verifikasi_kode').trigger('change')

        })
    </script>
@endsection
