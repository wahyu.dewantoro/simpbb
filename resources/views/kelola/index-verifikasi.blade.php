@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Verifikasi Usulan</h1>
                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">

                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fas fa-envelope text-info"></i> Verifikasi Pengelola /
                                Developer</h4>
                        </div>
                        <div class="card-body p-1">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center" width="3px">No</th>
                                        <th class="text-center">PT / CV / Badan</th>

                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($usulandeveloper) > 0)
                                        @foreach ($usulandeveloper as $i => $item)
                                            <tr>
                                                <td class="text-center" width="3px">{{ $i + 1 }}</td>
                                                <td class="text-left">{{ $item->nama_pengembang }}
                                                    <br>{{ $item->no_surat }}
                                                    <br>
                                                    {{ tglindo($item->tgl_surat) }}
                                                </td>
                                                <td>{{ $item->nama }}</td>
                                                <td class="text-left">
                                                    {{ $item->status }}
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{ url('kelola-developer-verifikasi', $item->id) }}"
                                                        class="btn btn-xs btn-flat btn-primary">
                                                        <i class="fas fa-user-check"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">Tidak ada data</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            Usulan NOP
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center" width="3px">No</th>
                                        <th class="text-center">Pemohon</th>
                                        <th>Developer</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Objek</th>

                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($usulannop) > 0)
                                        @foreach ($usulannop as $i => $item)
                                            <tr>
                                                <td class="text-center" width="3px">{{ $i + 1 }}</td>
                                                <td class="text-left">{{ $item->nama }}<br>
                                                    <span class="text-info">{{ $item->nik }}</span>
                                                </td>
                                                <td>
                                                    @php
                                                        $peng = $item->user->UserPengelola->first();
                                                        $nm_pengembang = $peng->nama_pengembang ?? '-';
                                                    @endphp
                                                    {{ $nm_pengembang }}
                                                </td>
                                                <td class="text-left">{{ tglIndo($item->tanggal_usulan) }}</td>
                                                <td class="text-center">{{ $item->JumlahNop }}</td>

                                                <td class="text-center">
                                                    <a href="{{ url('kelola-nop-verifikasi', $item->id) }}">
                                                        <i class="fas fa-user-check"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="5">Tidak ada data</td>
                                        </tr>
                                    @endif
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
