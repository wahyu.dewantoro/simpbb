@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Pengelolaan Nomor Objek Pajak (NOP)</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-nop') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <div class="card card-info card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Pemohon</h3>
                        </div>
                        <div class="card-body p-1">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="120px">Nama</td>
                                        <td width="1px">:</td>
                                        <td>{{ $data->user->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td>:</td>
                                        <td>{{ $data->user->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>{{ $data->user->telepon }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{ $data->user->alamat_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td>:</td>
                                        <td>{{ $data->user->kelurahan_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{{ $data->user->kecamatan_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kab / Kota</td>
                                        <td>:</td>
                                        <td>{{ $data->user->dati2_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Propinsi</td>
                                        <td>:</td>
                                        <td>{{ $data->user->propinsi_user }}</td>
                                    </tr>
                                    @if ($data->UrlDokumen != '')
                                        <tr>
                                            <td>Pendukung</td>
                                            <td>:</td>
                                            <td><a target="_blank" href="{{ $data->UrlDokumen }}"> <i
                                                        class="fas fa-file-pdf"></i> {{ $data->namadokumen }}</a></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>


                        </div>
                    </div>
                    @if ($data->user->UserPengelola()->count() > 0)
                        {{-- $data->user->UserPengelola()->get() --}}
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Developer</h3>
                            </div>
                            <div class="card-body p-1">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                        <tr>
                                            <td width="120px"> PT / CV / Badan</td>
                                            <td width="1px">:</td>
                                            <td>{{ $perusahaan->nama_pengembang }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Surat</td>
                                            <td>:</td>
                                            <td>{{ $perusahaan->no_surat }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Surat</td>
                                            <td>:</td>
                                            <td>{{ tglIndo($perusahaan->tgl_surat) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Dokumen</td>
                                            <td>:</td>
                                            <td><a target="_blank" href="{{ $perusahaan->urldokumen }}"><i
                                                        class="fas fa-file-alt"></i> {{ $perusahaan->namadokumen }}</a>
                                            </td>
                                        </tr>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endif

                </div>
                <div class="col-sm-8">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">NOP Yang di usulkan</h3>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th width="3px">No</th>
                                        <th>NOP</th>
                                        <th>Wajib Pajak</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data->KelolaUsulanObjek()->get() as $i => $item)
                                        <tr>
                                            <td widtd="3px">{{ $i + 1 }}</td>
                                            <td>{{ $item->nop }}</td>
                                            <td>{{ $item->wajibpajak }}</td>
                                            <td>{{ $item->status }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <p class="font-italic p-3">
                                {{ $data->keterangan_verifikasi }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
