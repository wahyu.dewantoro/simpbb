@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Pengelolaan Nomor Objek Pajak (NOP)</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        @if (Auth()->user()->is_pengelola == '1')
                            <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-nop/create') }}"><i
                                    class="fas fa-file-alt"></i> Buat Usulan</a>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools">
                                {{-- <form action="{{ url()->current() }}">
                                    <div class="input-group">
                                        <input class="form-control py-2 border-right-0 border" type="search"
                                            value="{{ request()->get('search') }}" id="search" name='search'
                                            placeholder="Pencarian">
                                        <span class="input-group-append">
                                            <div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div>
                                        </span>
                                    </div>
                                </form> --}}
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center" width="3px">No</th>
                                        <th class="text-center">NOP</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Alamat OP</th>
                                        <th class="text-center">Bumi</th>
                                        <th class="text-center">Bng</th>
                                        <th>PBB {{ date('Y') }}</th>
                                        <th>Pembayaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $i => $item)
                                        <tr>
                                            <td class="text-center" width="3px">{{ $i = $data->firstItem() + $i }}</td>
                                            <td class="text-left">
                                                {{ formatNop(
                                                    $item->kd_propinsi .
                                                        $item->kd_dati2 .
                                                        $item->kd_kecamatan .
                                                        $item->kd_kelurahan .
                                                        $item->kd_blok .
                                                        $item->no_urut .
                                                        $item->kd_jns_op,
                                                ) }}
                                            </td>
                                            <td>{{ $item->nm_wp }}</td>
                                            <td>
                                                {{ $item->jalan_op }}
                                                {{ $item->blok_kav_no_op }}
                                                {{ $item->rt_op != '' ? 'RT ' . $item->rt_op : '' }}
                                                {{ $item->rw_op != '' ? 'RW ' . $item->rw_op : '' }}

                                            </td>
                                            <td class="text-center">{{ $item->total_luas_bumi }}</td>
                                            <td class="text-center">{{ $item->total_luas_bng }}</td>
                                            <td class="text-right">{{ angka($item->pbb) }}</td>
                                            <td>
                                                @php
                                                    $st = $item->status_pembayaran_sppt;
                                                    switch ($st) {
                                                        case '1':
                                                            # code...
                                                            $text = 'Lunas';
                                                            break;

                                                        case '0':
                                                            # code...
                                                            $text = 'Belum Lunas';
                                                            break;

                                                        default:
                                                            # code...
                                                            $text = '-';
                                                            break;
                                                    }

                                                @endphp
                                                {{ $text }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    Total : {{ $data->total() }}
                                </div>
                                <div class="col-6">
                                    {!! $data->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
