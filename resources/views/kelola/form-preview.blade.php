@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Form Usulan Pengelolaan Nomor Objek Pajak (NOP)</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-nop') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form action="{{ $dataForm['action'] }}" method="post" enctype="multipart/form-data" id="form-usulan">
                <div class="card card-outline">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row p-0 m-0">
                                    <label for="nik" class="col-form-label col-sm-4">NIK</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nik" id="nik"
                                            value="{{ Auth()->user()->nik }}" class="form-control form-control-sm "
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row p-0 m-0">
                                    <label for="nama" class="col-form-label col-sm-4">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nama" id="nama"
                                            value="{{ Auth()->user()->nama }}" class="form-control form-control-sm "
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row p-0 m-0">
                                    <label for="alamat" class="col-form-label col-sm-4">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="alamat" id="alamat"
                                            value="{{ Auth()->user()->alamat_user }}" class="form-control form-control-sm "
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row p-0 m-0">
                                    <label for="kelurahan" class="col-form-label col-sm-4">Kelurahan</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="kelurahan" id="kelurahan"
                                            value="{{ Auth()->user()->kelurahan_user }}"
                                            class="form-control form-control-sm " readonly>
                                    </div>
                                </div>
                                <div class="form-group row p-0 m-0">
                                    <label for="kecamatan" class="col-form-label col-sm-4">Kecamatan</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="kecamatan" id="kecamatan"
                                            value="{{ Auth()->user()->kecamatan_user }}"
                                            class="form-control form-control-sm " readonly>
                                    </div>
                                </div>
                                <div class="form-group row p-0 m-0">
                                    <label for="dati2_user" class="col-form-label col-sm-4">Kab/Kota</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="dati2_user" id="dati2_user"
                                            value="{{ Auth()->user()->dati2_user }}" class="form-control form-control-sm "
                                            readonly>
                                    </div>
                                </div>
                                <div class="form-group row p-0 m-0">
                                    <label for="propinsi_user" class="col-form-label col-sm-4">Kab/Kota</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="propinsi_user" id="propinsi_user"
                                            value="{{ Auth()->user()->propinsi_user }}"
                                            class="form-control form-control-sm " readonly>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-8">
                                <table class="table table-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <th width='3px'>No</th>
                                            <th>NOP</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $bisa = 0;
                                        @endphp
                                        @foreach ($dataForm['nop'] as $i => $item)
                                            @if ($item['status'] == '1')
                                                @php
                                                    $bisa += 1;
                                                @endphp
                                                <input type="hidden" name="nop[]" value="{{ $item['nop'] }}">
                                            @endif
                                            <tr>
                                                <td class="text-center">{{ $i + 1 }}</td>
                                                <td>{{ $item['nop'] }}
                                                    <br>
                                                    <small class="text-priamry">{{ $item['nm_wp']??'' }}</small>

                                                </td>
                                                <td class="{{ $item['status'] == '1' ? 'text-success' : 'text-danger' }}">
                                                    {{ $item['keterangan'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button @if ($bisa == 0) disabled @endif
                                class="btn btn-info btn-sm btn-flat"><i class="fas fa-file-export"></i>
                                Submit</button>
                        </div>
                    </div>
                </div>
                @csrf
                @method($dataForm['method'])
            </form>
        </div>
    </section>
@endsection
@section('css')
    <style>
        fieldset {
            border: solid 1px #000;
            padding: 10px;
            display: block;
            clear: both;
            margin: 5px 0px;
        }

        legend {
            width: inherit;
            font-size: 13px;
            font-weight: bold;
            /* Or auto */
            padding: 0 10px;
            /* To give a bit of padding on the left and right */
            border-bottom: none;
        }
    </style>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {

            $("#add").click(function(e) {
                e.preventDefault()
                var lastField = $("#buildyourform div:last");
                var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
                var fieldWrapper = $(
                    "<div class=\"fieldwrapper input-group input-group-sm m-1 form-alert\" id=\"field" +
                    intId + "\"/>");
                fieldWrapper.data("idx", intId);
                var fName = $(
                    "<input type=\"text\" name=\"nop[" + intId +
                    "]\" required placeholder=\"Masukan NOP\"  class=\"fieldname nop form-control form-control-sm\" />"
                );

                var removeButton = $(
                    "<span class=\"input-group-append\"><button type=\"button\" class=\"btn btn-danger btn-flat remove \"><i class=\"fas fa-trash-alt\"></i></button></span>"
                );
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper.append(fName);
                fieldWrapper.append(removeButton);
                $("#buildyourform").append(fieldWrapper);
                $('.nop').on('keyup', function() {
                    var nop = $(this).val();
                    var convert = formatnop(nop);
                    $(this).val(convert);
                });


            });

            // $('#add').trigger('click')

            jQuery.extend(jQuery.validator.messages, {
                required: "Harus di isi.",
                extension: "Harus dengan format file .pdf, .png, .jpg",

            });
            /* $('#form-usulan').validate({
                rules: {
                    file: {
                        required: true,
                        extension: "xlsx"
                    },

                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-alert').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            }); */



        });
    </script>
@endsection
