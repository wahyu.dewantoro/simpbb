@extends('layouts.app')
@section('title')
    Verifikasi Usulan NOP
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1> Verifikasi Usulan NOP</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-nop') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Verifikasi Data</h4>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                            <p class="text-primary"><i class="far fa-list-alt"></i> Daftar NOP</p>
                            <div class="callout callout-danger">
                                <h5>Perhatian !</h5>
                                <p>Objek yang tidak di centang sama dengan <b>Ditolak</b></p>
                            </div>
                            <form action="{{ route('kelola-objek-verifikasi.update', $data->id) }}" method="post">
                                @csrf
                                @method('PATCH')
                                <table class="table table-sm table-bordered">
                                    <thead class="bg-info">
                                        <tr>
                                            <th width="3px">No</th>
                                            <th>NOP</th>
                                            <th>Wajib Pajak</th>
                                            <th><input type="checkbox" id="checkAll">Check All</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data->KelolaUsulanObjek()->get() as $i => $item)
                                            <tr>
                                                <td widtd="3px">{{ $i + 1 }}</td>
                                                <td>{{ $item->nop }}</td>
                                                <td>{{ $item->wajibpajak }}</td>
                                                <td>
                                                    <input type="checkbox" id="id_{{ $item->id }}" value="1"
                                                        name="id_nop[{{ $item->id }}]"> Disetujui
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    <button class="btn btn-flat btn-info btn-sm"><i class="fas fa-save"></i>
                                        Verifikasi</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                            {{-- <p class="text-primary"><i class="fas fa-user-tie"></i> </p> --}}

                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="120px">Nama</td>
                                        <td width="1px">:</td>
                                        <td>{{ $data->user->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td>:</td>
                                        <td>{{ $data->user->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>{{ $data->user->telepon }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{ $data->user->alamat_user }} <br>
                                            {{ $data->user->kelurahan_user }} - {{ $data->user->kecamatan_user }} <br>
                                            {{ $data->user->dati2_user }} - {{ $data->user->propinsi_user }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>NOP yang di kelola</td>
                                        <td>:</td>
                                        <td>{{ $jum_op }}</td>
                                    </tr>
                                    @if ($data->UrlDokumen != '')
                                        <tr>
                                            <td>Pendukung</td>
                                            <td>:</td>
                                            <td><a target="_blank" href="{{ $data->UrlDokumen }}"> <i
                                                        class="fas fa-file-pdf"></i> {{ $data->namadokumen }}</a></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            @if ($data->Pemohon->UserPengelola()->count() > 0)
                                <p class="text-info"><i class="fas fa-building"></i> Developer </p>
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                        <tr>
                                            <td width="120px"> PT / CV / Badan</td>
                                            <td width="1px">:</td>
                                            <td>{{ $perusahaan->nama_pengembang }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Surat</td>
                                            <td>:</td>
                                            <td>{{ $perusahaan->no_surat }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Surat</td>
                                            <td>:</td>
                                            <td>{{ tglindo($perusahaan->tgl_surat) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Dokumen</td>
                                            <td>:</td>
                                            <td><a target="_blank" href="{{ $perusahaan->urldokumen }}"><i
                                                        class="fas fa-file-alt"></i> {{ $perusahaan->namadokumen }}</a>
                                            </td>
                                        </tr>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif

                            @if ($data->Pemohon->UserRayon()->count() > 0)
                                <p class="text-info"><i class="fas fa-building"></i> Rayon </p>
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                        <tr>
                                            <td width="120px"> Rayon</td>
                                            <td width="1px">:</td>
                                            <td>{{ $rayon->nmkelurahan . ' - ' . $rayon->nm_kecamatan }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>No Surat</td>
                                            <td>:</td>
                                            <td>{{ $rayon->no_surat }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Surat</td>
                                            <td>:</td>
                                            <td>{{ tglIndo($rayon->tgl_surat) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Dokumen</td>
                                            <td>:</td>
                                            <td><a target="_blank" href="{{ $rayon->Dokumen->url }}"><i
                                                        class="fas fa-file-alt"></i>
                                                    {{ $rayon->Dokumen->nama_dokumen }}</a>
                                            </td>
                                        </tr>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // $('#status_verifikasi').on('change', function(e) {
            //     var st_ = $('#status_verifikasi').val()

            //     if (st_ == '0') {
            //         $('#keterangan').prop('required', true);
            //     } else {
            //         $('#keterangan').prop('required', false);
            //     }
            // })
            // $('#status_verifikasi').trigger('change')

            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

        })
    </script>
@endsection
