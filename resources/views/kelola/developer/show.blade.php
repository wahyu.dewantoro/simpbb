@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Data Developer</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-developer') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">Detail Data</h3>
                        </div>
                        <div class="card-body p-1">
                            <div class="row">

                                <div class="col-sm-4">
                                    <table class="table table-sm table-borderless">
                                        <tbody>
                                            <tr>
                                                <td width="150px">NIK</td>
                                                <td width="1px">:</td>
                                                <td>{{ $data->user->nik ?? '' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1px">:</td>
                                                <td>{{ $data->user->nama ?? '' }}</td>
                                            </tr>
                                            @if (($data->user->telepon ?? '') != '')
                                                <tr>
                                                    <td>Telepon</td>
                                                    <td>:</td>
                                                    <td>{{ $data->user->telepon }}</td>
                                                </tr>
                                            @endif
                                            @if (($data->user->email ?? '') != '')
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:</td>
                                                    <td>{{ $data->user->email }}</td>
                                                </tr>
                                            @endif
                                            @php
                                                // $uk="";

                                                if ($data->user()->exists()) {
                                                    $dk = $data->user->Unitkerja()->first();
                                                    $uk = $dk->nama_unit??"";
                                                } else {
                                                    $uk = '';
                                                }

                                            @endphp
                                            @if ($uk != '')
                                                <tr>
                                                    <td>Unit Kerja</td>
                                                    <td>:</td>
                                                    <td>{{ $uk }}</td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <td>Alamat</td>
                                                <td>:</td>
                                                <td>
                                                    @if ($data->user()->exists())
                                                        {{ $data->user->alamat_user }}
                                                        <br>
                                                        {{ $data->user->kelurahan_user }} -
                                                        {{ $data->user->kecamatan_user }}
                                                        <br>
                                                        {{ $data->user->dati2_user }} - {{ $data->user->propinsi_user }}
                                                    @endif
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-4">
                                    <table class="table table-sm table-borderless">
                                        <tbody>
                                            <tr>
                                                <td width="120px">PT / CV / Badan</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $data->nama_pengembang }}</td>
                                            </tr>
                                            <tr>
                                                <td width="120px">Tanggal</td>
                                                <td width='1px'>:</td>
                                                <td>{{ tglindo($data->tgl_surat) }}</td>
                                            </tr>
                                            <tr>
                                                <td width="120px">No Surat</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $data->no_surat }}</td>
                                            </tr>
                                            <tr>
                                                <td>Pendukung</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="{{ url($data->urldokumen) }}"><i
                                                            class="fas fa-file-pdf"></i>
                                                        {{ $data->namadokumen }}</a>
                                                    @if ($data->user()->exists())
                                                        @php

                                                            $dokumen = $data->user->Pendukung();

                                                        @endphp
                                                        @foreach ($dokumen->get() as $item)
                                                            <br><a id="my-link" " href="{{ url($item->url) }}"><i
                                                                                                                class="fas fa-file-pdf"></i>
                                                                                                            {{ $item->nama_dokumen }}</a>
     @endforeach
                                                        @endif
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    @if ($data->user()->exists())
                                        @if ($dokumen->count())
                                            {{-- <b></b> --}}
                                            <table id="my-link" class="table table-sm table-borderless">
                                                <tbody>

                                                </tbody>
                                            </table>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-sm-4">
                                    <table class="table table-borderless text-sm table-sm">
                                        <tbody>
                                            <tr>
                                                <td>Permohonan</td>
                                                <td>:</td>
                                                <td>{{ $data->created_at }}</td>
                                            </tr>
                                            <tr>
                                                <td>Verifikasi</td>
                                                <td>:</td>
                                                <td>{{ $data->verifikasi_at }}</td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td>:</td>
                                                <td>{{ $data->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {

            function openWindow(url) {

                var windowHeight = window.screen.height; // Mengambil tinggi layar
                var windowWidth = 600; // Lebar tetap
                window.open(url, 'newWindow', 'width=' + windowWidth + ',height=' + windowHeight +
                    ',top=0,left=100,scrollbars=yes');
            }

            $('.my-link').on('click', function(event) {
                event.preventDefault(); // Mencegah tindakan default link (membuka URL)
                var hrefValue = $(this).attr('href'); // Mengambil nilai href
                openWindow(hrefValue);
            });

        })
    </script>
@endsection
