@extends('layouts.app')
@section('title')
    Verifikasi Usulan Developer
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Verifikasi Usulan Developer</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group">
                                        <input class="form-control py-2 border-right-0 border" type="search"
                                            value="{{ request()->get('search') }}" id="search" name='search'
                                            placeholder="Pencarian">
                                        <span class="input-group-append">
                                            <div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center" width="3px">No</th>
                                        <th class="text-center">PT / CV / Badan</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">No Surat</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $i => $item)
                                        <tr>
                                            <td class="text-center" width="3px">{{ $i = $data->firstItem() + $i }}</td>
                                            <td class="text-left">{{ $item->nama_pengembang }}</td>
                                            <td class="text-left">{{ tglindo($item->tgl_surat) }}</td>
                                            <td class="text-left">{{ $item->no_surat }}</td>
                                            <td>{{ $item->nama }}</td>
                                            <td class="text-left">
                                                {{ $item->status }}
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('kelola-developer-verifikasi.edit', $item->id) }}">
                                                    <i class="fas fa-check-circle text-primary"></i> Verifikasi
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    Total : {{ $data->total() }}
                                </div>
                                <div class="col-6">
                                    {!! $data->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
