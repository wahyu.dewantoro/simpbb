@extends('layouts.app')
@section('title')
    Verifikasi Developer
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Verifikasi Developer</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-verifikasi-data') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Verifikasi Data</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                            <h4 class="text-info"><i class="fas fa-building"></i> Data Developer</h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <table class="table table-sm table-borderless">
                                        <tbody>
                                            <tr>
                                                <td width="120px">PT / CV / Badan</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $data->nama_pengembang }}</td>
                                            </tr>
                                            <tr>
                                                <td width="120px">Tanggal</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $data->tgl_surat }}</td>
                                            </tr>
                                            <tr>
                                                <td width="120px">No Surat</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $data->no_surat }}</td>
                                            </tr>
                                            <tr>
                                                <td>Pendukung</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="{{ url($data->urldokumen) }}"><i
                                                            class="fas fa-file-pdf"></i>
                                                        {{ $data->namadokumen }}</a></td>
                                            </tr>
                                            <tr>
                                                <td width="120px">status</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $data->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    @php
                                        $dokumen = $data->user->Pendukung();
                                    @endphp
                                    @if ($dokumen->count())
                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                @foreach ($dokumen->get() as $item)
                                                    <tr>
                                                        <td><a target="_blank" href="{{ url($item->url) }}"><i
                                                                    class="fas fa-file-pdf"></i>
                                                                {{ $item->nama_dokumen }}</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <form action="{{ route('kelola-developer-verifikasi.update', $data->id) }}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label for="verifikasi_kode" class="col-form-label">Verifikasi</label>
                                        <select name="verifikasi_kode" id="verifikasi_kode" required class="form-control ">
                                            <option value="">Pilih</option>
                                            @php
                                                $opt = [
                                                    '1' => 'Disetujui',
                                                    '0' => 'Ditolak',
                                                ];

                                            @endphp
                                            @foreach ($opt as $i => $item)
                                                <option value="{{ $i }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <Label class="col-form-label">Keterangan</Label>
                                        <textarea name="verifikasi_keterangan" id="verifikasi_keterangan" class="form-control form-control-sm">{{ old('verifikasi_keterangan') }}</textarea>
                                    </div>

                                    
                                    <div class="float-right">
                                        <button class="btn btn-sm btn-info"><i class="fas fa-check-circle"></i>
                                            Verifikasi</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                            <h4 class="text-primary"><i class="fas fa-user-tie"></i> Data WP</h4>
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                        <td>NIK</td>
                                        <td width="1px">:</td>
                                        <td>{{ $data->user->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Lengkap</td>
                                        <td width="1px">:</td>
                                        <td>{{ $data->user->nama }}</td>
                                    </tr>
                                    @if ($data->user->telepon != '')
                                        <tr>
                                            <td>Telepon</td>
                                            <td>:</td>
                                            <td>{{ $data->user->telepon }}</td>
                                        </tr>
                                    @endif
                                    @if ($data->user->email != '')
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>{{ $data->user->email }}</td>
                                        </tr>
                                    @endif
                                    @php
                                        $uk = $data->user->Unitkerja()->first()->nama_unit ?? '';
                                    @endphp
                                    @if ($uk != '')
                                        <tr>
                                            <td>Unit Kerja</td>
                                            <td>:</td>
                                            <td>{{ $uk }}</td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{ $data->user->alamat_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Desa/Kelurahan</td>
                                        <td>:</td>
                                        <td>{{ $data->user->kelurahan_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>{{ $data->user->kecamatan_user }}</td>
                                    </tr>

                                    <tr>
                                        <td>Kab / Kota</td>
                                        <td>:</td>
                                        <td>{{ $data->user->dati2_user }}</td>
                                    </tr>
                                    <tr>
                                        <td>Propinsi</td>
                                        <td>:</td>
                                        <td>{{ $data->user->propinsi_user }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#verifikasi_kode').on('change', function(e) {
                var st_ = $('#verifikasi_kode').val()

                if (st_ == '0') {
                    $('#verifikasi_keterangan').prop('required', true);
                } else {
                    $('#verifikasi_keterangan').prop('required', false);
                }
            })
            $('#verifikasi_kode').trigger('change')
        })
    </script>
@endsection
