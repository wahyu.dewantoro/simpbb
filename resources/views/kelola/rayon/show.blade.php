@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>Data Rayon</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        <a class="btn btn-flat btn-primary" href="{{ route('kelola-rayon.index') }}"><i class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <strong><i class="fas fa-user-tie"></i> Nama</strong>
                            <div class="text-muted">{{ $data->user->nama }} <br> ({{ $data->status }}) </div>
                            <strong><i class="fas fa-user-tie"></i> Rayon</strong>
                            <div class="text-muted">{{ $data->nmkelurahan . ' - ' . $data->nmkecamatan }}</div>
                            <strong><i class="far fa-address-card"></i> NIK</strong>
                            <div class="text-muted">{{ $data->user->nik }}</div>

                            <strong><i class="fas fa-phone-square-alt"></i> Nomor Whatsapp</strong>
                            <div class="text-muted">{{ $data->user->telepon }}</div>
                            <strong><i class="fas fa-envelope-open-text"></i> Email</strong>
                            <div class="text-muted">{{ $data->user->email }}</div>
                            <strong><i class="fas fa-map-marked"></i> Alamat</strong>
                            <div class="text-muted">{{ $data->user->alamat_user }}<br>
                                {{ $data->user->kelurahan_user }} - {{ $data->user->kecamatan_user }}<br>
                                {{ $data->user->dati2_user }} - {{ $data->user->propinsi_user }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
