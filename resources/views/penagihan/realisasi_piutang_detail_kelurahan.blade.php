@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Realisasi Penagihan Piutang (Kecamatan : {{$kecamatan}} - Kelurahan {{$kelurahan}})</b></h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item"><a class="btn btn-warning btn-flat pull-right" href="{{url('realisasi_penagihan_piutang/detail',[$kec])}}">Kembali</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <table id="main-table" class="table table-bordered table-striped table-hover" data-append="response-data">
                                <thead class='text-center'>
                                    <tr>
                                        <th class='number' rowspan='2'>No</th>
                                        <th rowspan='2'>NOP</th>
                                        <th rowspan='2'>WP</th>
                                        <th colspan='6'>Piutang</th>
                                        <th rowspan='2'>Detail</th>
                                    </tr>
                                    <tr>
                                        <th>2021</th>
                                        <th>2020</th>
                                        <th>2019</th>
                                        <th>2018</th>
                                        <th>2017</th>
                                        <th>2016</th>
                                    </tr>
                                </thead>
                                <tbody class='table-sm'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data penagihan.';
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:'{{ route("realisasi_penagihan_piutang.searchdetailkelurahan",[ $id]) }}',
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    { data: 'DT_RowIndex',class:'text-center'},
                    { data: 'nop'},
                    { data: 'wp'},
                    
                    { data: 'pbb2021'},
                    { data: 'pbb2020'},
                    { data: 'pbb2019'},
                    { data: 'pbb2018'},
                    { data: 'pbb2017'},
                    { data: 'pbb2016'},
                    
                    { data: 'option',class:'text-center'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
        });
    </script>
@endsection
