@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Daftar Detail Tagihan </b></h3>
                        </div>
                        <div class="card-body">
                            <table id="main-table" class="table table-bordered table-striped " data-append="response-data">
                                <thead>
                                    <tr>
                                        <th class='number'>No</th>
                                        <th>NOP</th>
                                        <th>WP</th>
                                        <th>KECAMATAN</th>
                                        <th>KELURAHAN</th>
                                        <th>ALAMAT OBJEK</th>
                                        <th>TAHUN</th>
                                        <th>BUKU</th>
                                        <th>PBB</th>
                                        <th>TAGIHAN</th>
                                        <th>KETERANGAN</th>
                                    </tr>
                                </thead>
                                <tbody class='table-sm'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data penagihan.';
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:'{{ route("penagihan_daftar.penagihandetailsearch",[$id]) }}',
                    method: 'POST'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    { data: 'DT_RowIndex',class:'text-center'},
                    { data: 'nop',class:'w-15'},
                    { data: 'wp',class:'w-20'},
                    { data: 'nm_kecamatan'},
                    { data: 'nm_kelurahan'},
                    { data: 'alamat_objek'},
                    { data: 'tahun'},
                    { data: 'buku'},
                    { data: 'pbb'},
                    { data: 'tagihan',class:'text-center w-10'},
                    { data: 'option',class:'text-center w-10'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
        });
    </script>
@endsection
