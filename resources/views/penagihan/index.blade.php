@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Form Penagihan</b></h3>
                        </div>
                        <div class="card-body">
                            @include('penagihan/form_penagihan')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil, coba ulangi proses.";
            let defaultErrorPreview="Terjadi kesalahan validasi data, pastikan data sudah sesuai dengan format.";
            let isnull="<tr class='null'><td colspan='7' class='dataTables_empty'>Data Masih Kosong</td></tr>";
            let limit=500;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        });
    </script>
@endsection
