@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Daftar Tagihan</b></h3>
                        </div>
                        <div class="card-body">
                            <table id="main-table" class="table table-bordered table-striped " data-append="response-data">
                                <thead class='text-center'>
                                    <tr>
                                        <!-- <th class='number' rowspan='2'>No</th> -->
                                        <th rowspan='2'>NO TAGIHAN</th>
                                        <th colspan='2'>2021</th>
                                        <th colspan='2'>2020</th>
                                        <th colspan='2'>2019</th>
                                        <th colspan='2'>2018</th>
                                        <th colspan='2'>2017</th>
                                        <th colspan='2'>2016</th>
                                        <th colspan='2'>TOTAL</th>
                                        <th rowspan='2'>TANGGAL</th>
                                        <th rowspan='2'>DETAIL</th>
                                    </tr>
                                    <tr>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                        <th>Tagihan</th>
                                        <th>Lunas</th>
                                    </tr>
                                </thead>
                                <tbody class='table-sm'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data penagihan.';
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:'{{ route("penagihan_daftar.penagihansearch") }}',
                    method: 'POST'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    // { data: 'DT_RowIndex',class:'text-center'},
                    { data: 'nomor_penagihan',class:'w-15'},
                    
                    { data: 'tagihan2021'},
                    { data: 'lunas2021'},
                    { data: 'tagihan2020'},
                    { data: 'lunas2020'},
                    { data: 'tagihan2019'},
                    { data: 'lunas2019'},
                    { data: 'tagihan2018'},
                    { data: 'lunas2018'},
                    { data: 'tagihan2017'},
                    { data: 'lunas2017'},
                    { data: 'tagihan2016'},
                    { data: 'lunas2016'},
                    
                    { data: 'total_tagihan'},
                    { data: 'total_lunas'},
                    { data: 'created_at'},
                    { data: 'option',class:'text-center w-10'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
        });
    </script>
@endsection
