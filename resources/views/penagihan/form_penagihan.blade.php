<form action="#dafnom/cek_pembuatan_dafnom" id="form-cek">
    @csrf   
    <div class="row">
        <div class="form-group col-md-2">
            <div class="input-group">
                <select name="tahun_pajak"  class="form-control" placeholder="PIlih tahun">
                    <option>-- Semua --</option>
                    @foreach ($tahun_pajak as $rowtahun)
                        <option @if (request()->get('tahun_pajak') == $rowtahun)  selected @endif value="{{ $rowtahun }}">
                            {{ $rowtahun}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-2">
            <div class="input-group">
                <select name="kecamatan"  required class="form-control select"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                    @if($kecamatan->count()>1) <option value="">-- Kecamatan --</option> @endif
                    @foreach ($kecamatan as $rowkec)
                        <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                            {{ $rowkec->nm_kecamatan }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-3">
            <div class="input-group">
                <select name="kelurahan"  required class="form-control select"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                    <option value="">-- Kelurahan --</option>
                    @foreach ($kelurahan as $rowkel)
                        <option @if (request()->get('kd_kelurahan') == $rowkel->kd_kelurahan)  selected @endif value="{{ $rowkel->kd_kelurahan }}">
                            {{ $rowkel->nm_kelurahan }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-3">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="NOP " name="nop" >
            </div>
        </div>
        <div class="form-group col-lg-2">
            <div class="input-group">
                <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-6" data-append='body_preview_nop_dafnom'>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title lh-35">Daftar NOP</h3>
                <div class="card-tools no-margin">
                    <div class="btn-group">
                        <a href="javascript:;" class='btn btn-warning btn-flat'><i class="fas fa-file-upload"></i> Import Excel</a>
                        <a href="javascript:;" class='btn btn-primary btn-flat' data-clone=".table-clone-list" data-target=".table-clone-submit"><i class="far fa-plus-square"></i> Tambah</a>
                    </div>
                </div>
                <div class="card-widget bold">
                    <ul>
                        <li>Jumlah NOP   : <span name='selected_jumlah_nop'>0</span></li>
                    </ul>
                </div>
            </div>
            <div class="card-body body-scroll">
                <table id="example1" class="table table-bordered table-striped table-with-checkbox table-change  table-select table-clone-list table-counter">
                    <thead>
                        <tr>
                            <th class="cursor-pointer checkall"><span><input type="checkbox" name="check" class="form-check-input check-header"> Semua</span></th>
                            <th>NOP</th>
                            <th>Wajib Pajak</th>
                            <th>Tagihan</th>
                            <th>Tahun Pajak</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="null">
                            <td colspan="5" class="dataTables_empty text-center"> Data Masih Kosong</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6" data-append='body_preview_nop_dafnom_false'>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title lh-35">Daftar Tagihan</h3>
                <div class="card-tools no-margin">
                    <a href="javascript:;" class='btn btn-primary btn-flat' data-clone=".table-clone-submit" data-target=".table-clone-list">
                    <i class="far fa-trash-alt"></i> Hapus</a>
                </div>
                <div class="card-widget bold">
                    <ul>
                        <li>Jumlah NOP   : <span name='selected_jumlah_nop'>0</span></li>
                    </ul>
                </div>
            </div>
            <div class="card-body body-scroll">
                <form action="#" id="form_input">
                    @csrf  
                    <table id="example1" class="table table-bordered table-striped table-change table-select table-clone-submit table-counter no-margin">
                        <thead>
                            <tr >
                                <th class="cursor-pointer checkall"><span><input type="checkbox" name="check" class="form-check-input check-header"> Semua</span></th>
                                <th>NOP</th>
                                <th>Wajib Pajak</th>
                                <th>Tagihan</th>
                                <th>Tahun Pajak</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="null">
                                <td colspan="6" class="dataTables_empty text-center"> Data Masih Kosong</td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body">
                <div class='row'>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <label class="col-md-2 col-form-label">Keterangan</label>
                                <textarea name="keterangan" class="form-control triggerHeight default-textarea"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
        <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Simpan</button> </div>
    </div>
</div>