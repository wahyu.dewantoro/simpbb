@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Refrensi Nomor Formulir {{ $tahun }}</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a href="{{ url('refrensi/formulir/create') }}" class="btn btn-sm btn-primary"><i
                                class="far fa-plus-square"></i> Tambah </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">
                <div class="card-body p-1">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                {{-- <th rowspan="2">No</th> --}}
                                <th rowspan="2">Penelitian</th>
                                <th rowspan="2">Uraian</th>
                                <th colspan="2">Bundel</th>
                                <th rowspan="2"></th>
                                <th rowspan="2"></th>
                            </tr>
                            <tr>
                                <th>Min</th>
                                <th>Max</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $temp = '';
                            @endphp
                            @foreach ($formulir as $i => $item)
                                <tr>
                                    {{-- <td class="text-center">{{ $i+1 }}</td> --}}
                                    <td>
                                        @if ($temp != $item->penelitian)
                                            {{ $item->penelitian == '1' ? 'Penelitian Kantor' : 'Penelitian Lapangan' }}
                                        @endif
                                    </td>
                                    <td>{{ $item->uraian }}</td>
                                    <td>{{ $item->bundel_min }}</td>
                                    <td>{{ $item->bundel_max }}</td>
                                    <td>
                                        @foreach ($item->layanan()->pluck('nama_layanan')->toArray() as $e=> $layanan)
                                             {{ $e+1 }}. {{  $layanan}}<br>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ url('refrensi/formulir', $item->id) }}/edit" class=""><i class="fas fa-edit text-success"></i> </a>
                                        <a href="{{ url('refrensi/formulir', $item->id) }}" onclick="
                                                var result = confirm('Are you sure you want to delete this record?');
                                                if(result){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $item->id }}').submit();
                                                }" title="Delete User"><i class="fas fa-trash-alt text-danger"></i> </a>
                                        <form method="POST" id="delete-form-{{ $item->id }}"
                                            action="{{ route('refrensi.formulir.destroy', [$item->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>

                                    </td>
                                </tr>
                                @php
                                    $temp = $item->penelitian;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on({
                ajaxStart: function() {
                    openloading();
                },
                ajaxStop: function() {
                    closeloading();
                }
            });

            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    $('#cek').trigger('click');
                }
            });


        });
    </script>
@endsection
