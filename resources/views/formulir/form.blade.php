@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $data['title'] }}</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-md-6">
                <form action="{{ $data['action'] }}" method="post" class="form-horizontal">
                    @csrf
                    @method($data['method'])
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <label for="tahun" class="col-sm-6 col-form-label">Tahun</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="tahun" id="tahun"
                                                class="form-control form-control-sm angka {{ $errors->has('tahun') ? 'is-invalid' : '' }}"
                                                value="{{ old('tahun') ?? ($data['formulir']->tahun ?? date('Y')) }}">
                                            <span class="errorr invalid-feedback">{{ $errors->first('tahun') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group row">
                                        <label for="penelitian" class="col-sm-4 col-form-label">Penelitian</label>
                                        <div class="col-sm-8">
                                            @php
                                                $slt = old('penelitian') ?? $data['formulir']->penelitian??'';
                                            @endphp
                                            <select name="penelitian" id="penelitian"
                                                class="form-control form-control-sm {{ $errors->has('penelitian') ? 'is-invalid' : '' }}">
                                                <option value="">Pilih</option>
                                                @foreach ($jnsPenelitian as $i => $item)
                                                    <option @if ($slt == $i) selected @endif
                                                        value="{{ $i }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                            <span
                                                class="errorr invalid-feedback">{{ $errors->first('penelitian') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="uraian" class="col-sm-3 col-form-label">Uraian</label>
                                <div class="col-sm-9">
                                    <input type="text" name="uraian" id="uraian" value="{{ old('uraian') ?? ($data['formulir']->uraian ?? '') }}"
                                        class="form-control form-control-sm {{ $errors->has('uraian') ? 'is-invalid' : '' }}">

                                    <span class="errorr invalid-feedback">{{ $errors->first('uraian') }}</span>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-6">
                                    <div class="form-group row">
                                        <label for="bundel_min" class="col-xs-6 col-form-label">Bundel Min</label>
                                        <div class="col-xs-5">
                                            <input type="text" name="bundel_min" id="bundel_min" maxlength="4" value="{{ old('bundel_min') ?? ($data['formulir']->bundel_min ?? '') }}"
                                                class="form-control form-control-sm angka {{ $errors->has('bundel_min') ? 'is-invalid' : '' }}">
                                            <span
                                                class="errorr invalid-feedback">{{ $errors->first('bundel_min') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group row">
                                        <label for="bundel_max" class="col-xs-6 col-form-label">Bundel Max</label>
                                        <div class="col-xs-5">
                                            <input type="text" name="bundel_max" id="bundel_max" maxlength="4" value="{{ old('bundel_max') ?? ($data['formulir']->bundel_max ?? '') }}"
                                                class="form-control form-control-sm angka {{ $errors->has('bundel_max') ? 'is-invalid' : '' }}">
                                            <span
                                                class="errorr invalid-feedback">{{ $errors->first('bundel_max') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                @php
                                    $ar = [];
                                    if (old('jenis_layanans')) {
                                        $ar = old('jenis_layanans');
                                    }else{
                                        $ar=[];
                                        if(isset($data['formulir'])){
                                            $ar=$data['formulir']->layanan()->pluck('jenis_layanan_id')->toArray();
                                        }
                                        
                                    }
                                    
                                @endphp
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Jenis Ajuan Pelayanan</label>
                                    </div>
                                </div>


                                @foreach ($jnsLayanan as $i => $ro)
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                <input @if (in_array($i, $ar)) checked @endif type="checkbox"
                                                    name="jenis_layanans[]" value="{{ $i }}" id="">
                                                {{ $ro }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer p-1">
                            <div class="float-right">
                                <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $(document).on({
                ajaxStart: function() {
                    openloading();
                },
                ajaxStop: function() {
                    closeloading();
                }
            });

            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    $('#cek').trigger('click');
                }
            });


        });
    </script>
@endsection
