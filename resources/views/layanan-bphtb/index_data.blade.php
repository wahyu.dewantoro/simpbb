<div class="card">
    <div class="card-body p-1">
        <table class="table table-bordered table-sm text-sm">
            <thead>
                <tr>
                    <th>No</th>

                    <th>Pemohon</th>
                    <th>NOP</th>
                    <th>Layanan PBB</th>
                    <th>Status</th>
                    <th>Nota Perhitungan / SK</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $i => $item)
                    <tr>
                        <td>{{ $data->firstItem() + $i }}</td>

                        <td>{{ $item->nama_pemohon }}
                            <br>
                            <small class="text-info">{{ $item->nomor_layanan_bphtb }}</small>
                        </td>
                        <td>
                            {{ formatnop(
                                $item->kd_propinsi .
                                    $item->kd_dati2 .
                                    $item->kd_kecamatan .
                                    $item->kd_kelurahan .
                                    $item->kd_blok .
                                    $item->no_urut .
                                    $item->kd_jns_op,
                            ) }}
                            <br>
                            <small class="text-info"> {{ $item->keterangan }}</small>
                        </td>
                        <td>
                            {{ $item->jenis_layanan_pbb }}
                        </td>
                        <td>
                            {{ $item->status_layanan_pbb }}
                        </td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach ($item->objek ?? [] as $lo)
                                    @if (optional($lo->Notaperhitungan)->layanan_objek_id != '')
                                        <li> <a onclick="window.open('{{ url('penelitian/nota-perhitungan-pdf', acak($lo->Notaperhitungan->layanan_objek_id . '-1')) }}', '_blank', 'width=800,height=600'); return false;"
                                                href="#"><i class="fas fa-file-download"></i> Nota Perhitungan</a>

                                            @if ($lo->Notaperhitungan->DataBilling->tgl_bayar != '')
                                                / <a onclick="window.open('{{ url('sk', acak($lo->Notaperhitungan->layanan_objek_id)) }}', '_blank', 'width=800,height=600'); return false;"
                                                    href="#"><i class="fas fa-file-download"></i> SK</a>
                                            @endif
                                        </li>
                                        @endif
                                    @endforeach
                            </ul>
                        </td>
                        <td class="text-center">
                            @empty($item->nomor_layanan_pbb)
                                <a href="{{ route('layanan-bphtb.edit', $item->id) }}"><i class="far fa-edit"></i></a>
                            @endempty

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <div class="float-right">
            {!! $data->links() !!}
        </div>
    </div>
</div>
