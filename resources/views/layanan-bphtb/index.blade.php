@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Permohoan dari BPHTB</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        {{-- <a href="{{ $urlback }}" class="btn btn-sm btn-info"><i class="fas fa-angle-double-left"></i> Back --}}
                        {{-- todolist</a> --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div id="data-container">
                @include('layanan-bphtb.index_data')
            </div>

        </div>
    </section>
@endsection
@section('script')
    {{-- <script src="{{ asset('js') }}/wilayah.js"></script> --}}
    <script>
        $(document).on('click', '.pagination a', function(event) {
            event.preventDefault();

            var fullUrl = $(this).attr('href');
            var baseUrl = fullUrl.split('?')[0]; // Ambil URL tanpa query
            var queryString = fullUrl.split('?')[1]; // Ambil bagian query

            fetch_data(baseUrl, queryString);
        });

        function fetch_data(url, queryString) {
            $.ajax({
                url: url + "?" + queryString, // Gabungkan kembali dengan query string
                success: function(data) {
                    $('#data-container').html(data);
                }
            });
        }
    </script>
@endsection
