@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Verifikasi Dokumen</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <b>Data Permhoonan BPHTB</b>
                                    <table class="table table-sm table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>Pemohon</td>
                                                <td width="1px">:</td>
                                                <td>{{ $data->nama_pemohon }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Whatsapp</td>
                                                <td width="1px">:</td>
                                                <td>{{ $data->nomor_pemohon }}</td>
                                            </tr>
                                            <tr>
                                                <td>NOP</td>
                                                <td width="1px">:</td>
                                                <td>{{ formatNop(
                                                    $data->kd_propinsi .
                                                        $data->kd_dati2 .
                                                        $data->kd_kecamatan .
                                                        $data->kd_kelurahan .
                                                        $data->kd_blok .
                                                        $data->no_urut .
                                                        $data->kd_jns_op,
                                                ) }}
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1px">:</td>
                                                <td>{{ $data->alamat_objek }} <br>
                                                    RT {{ $data->rt_objek }} / RW {{ $data->rw_objek }}<br>
                                                    {{ $data->nm_kelurahan }} - {{ $data->nm_kecamatan }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Transaksi</td>
                                                <td>:</td>
                                                <td>{{ $data->jenis_perolehan }}</td>
                                            </tr>

                                            <tr>
                                                <td>Luas Objek</td>
                                                <td></td>
                                                <td>
                                                    <table class="table table-bordered table-sm text-sm" border="1">
                                                        <thead>
                                                            <tr>
                                                                <th>Uraian</th>
                                                                <th>BPHTB</th>
                                                                <th>PBB</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Bumi</td>
                                                                <td>{{ $data->luas_bumi }}</td>
                                                                <td>
                                                                    {{ $pbb->total_luas_bumi }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bangunan</td>
                                                                <td>{{ $data->luas_bng }}</td>
                                                                <td>
                                                                    {{ $pbb->total_luas_bng }}
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Penjual / Pemberi Hak</td>
                                                <td width="1px">:</td>
                                                <td>{{ $penjual['nm_penjual'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Nama WP PBB</td>
                                                <td width="1px">:</td>
                                                <td>{{ $pbb->DatSubjekPajak->nm_wp }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <b>Dokumen</b>

                                    <ol>
                                        @foreach ($dokumen as $item)
                                            <li> <a onclick="window.open(this.href, '_blank', 'width=800,height=600,toolbar=no,scrollbars=no'); return false;"
                                                    href="{{ $item->url }}">{{ $item->filename }}</a></li>
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="col-md-6">
                                    <b>Keterangan Permohonan</b>
                                    <p>{{ $data->keterangan }}</p>

                                    <div class="card">
                                        <div class="card-body">
                                            <form action="{{ route('layanan-bphtb.update', $data->id) }}" method="POST">
                                                @csrf
                                                @method('PATCH')
                                                {{-- <input type="hidden" name="id" value="{{ $data->id }}"> --}}
                                                <div class="form-group">
                                                    <label for="">Mapping ke permohonan</label>
                                                    <select name="jenis_layanan_id" id="jenis_layanan_id"
                                                        class="form-control form-control-sm" required>
                                                        <option value="">Pilih</option>
                                                        @foreach ($jp as $i => $item)
                                                            <option value="{{ $i }}">{{ $item }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div id="pembetulan_luas">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">Luas bumi pembetulan</label>
                                                                <input type="text" name="luas_bumi_pembetulan"
                                                                    id="luas_bumi_pembetulan"
                                                                    class="form-control form-contol-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">Luas bng pembetulan</label>
                                                                <input type="text" name="luas_bng_pembetulan"
                                                                    id="luas_bng_pembetulan"
                                                                    class="form-control form-contol-sm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="mutasi_pecah">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">Luas bumi</label>
                                                                <input type="text" name="luas_bumi_pecah"
                                                                    id="luas_bumi_pecah"
                                                                    class="form-control form-contol-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">Luas bng</label>
                                                                <input type="text" name="luas_bng_pecah"
                                                                    id="luas_bng_pecah" class="form-control form-contol-sm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="nik_wp">NIK</label>
                                                                <input type="text" name="nik_wp_pecah" id="nik_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="nm_wp">Nama</label>
                                                                <input type="text" name="nm_wp_pecah" id="nm_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-8">

                                                            <div class="form-group">
                                                                <label for="alamat_wp">Alamat</label>
                                                                <input type="text" name="alamat_wp_pecah"
                                                                    id="alamat_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="rt_wp">RT </label>
                                                                <input type="text" name="rt_wp_pecah" id="rt_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="rw_wp">RW</label>
                                                                <input type="text" name="rw_wp_pecah" id="rw_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="kelurahan_wp">Kelurahan / Desa</label>
                                                                <input type="text" name="kelurahan_wp_pecah"
                                                                    id="kelurahan_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="kecamatan_wp">Kecamatan</label>
                                                                <input type="text" name="kecamatan_wp_pecah"
                                                                    id="kecamatan_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="dati2_wp">Kab/Kota</label>
                                                                <input type="text" name="dati2_wp_pecah"
                                                                    id="dati2_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="propinsi_wp">Propinsi</label>
                                                                <input type="text" name="propinsi_wp_pecah"
                                                                    id="propinsi_wp_pecah"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="mutasi_penuh">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="nik_wp">NIK</label>
                                                                <input type="text" name="nik_wp_penuh"
                                                                    id="nik_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="nm_wp">Nama</label>
                                                                <input type="text" name="nm_wp_penuh" id="nm_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-8">

                                                            <div class="form-group">
                                                                <label for="alamat_wp">Alamat</label>
                                                                <input type="text" name="alamat_wp_penuh"
                                                                    id="alamat_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="rt_wp">RT </label>
                                                                <input type="text" name="rt_wp_penuh" id="rt_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="rw_wp">RW</label>
                                                                <input type="text" name="rw_wp_penuh" id="rw_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="kelurahan_wp">Kelurahan / Desa</label>
                                                                <input type="text" name="kelurahan_wp_penuh"
                                                                    id="kelurahan_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="kecamatan_wp">Kecamatan</label>
                                                                <input type="text" name="kecamatan_wp_penuh"
                                                                    id="kecamatan_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="dati2_wp">Kab/Kota</label>
                                                                <input type="text" name="dati2_wp_penuh"
                                                                    id="dati2_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="propinsi_wp">Propinsi</label>
                                                                <input type="text" name="propinsi_wp_penuh"
                                                                    id="propinsi_wp_penuh"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="float-right">
                                                    <button class="btn btn-sm btn-success"><i class="fas fa-save"></i>
                                                        Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    {{-- <script src="{{ asset('js') }}/wilayah.js"></script> --}}
    <script>
        $(document).ready(function() {
            const layananMap = {
                '3': '#pembetulan_luas',
                '9': '#mutasi_penuh',
                '6': '#mutasi_pecah'
            };

            // Sembunyikan semua elemen dan hapus required saat halaman dimuat
            Object.values(layananMap).forEach(selector => {
                $(selector).hide().find('input, select, textarea').prop('required', false);
            });

            $('#jenis_layanan_id').on('change', function() {
                let jenis_layanan_id = $(this).val();

                // Sembunyikan semua elemen dan hapus required
                Object.values(layananMap).forEach(selector => {
                    $(selector).hide().find('input, select, textarea').prop('required', false);
                });

                // Tampilkan elemen yang sesuai dan tambahkan required
                if (layananMap[jenis_layanan_id]) {
                    $(layananMap[jenis_layanan_id])
                        .show()
                        .find('input, select, textarea')
                        .prop('required', true);
                }
            });
        });
    </script>
@endsection
