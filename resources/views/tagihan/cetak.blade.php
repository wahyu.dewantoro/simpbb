<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah" style="font-size: 16px; font-weight: bold;">SURAT TAGIHAN PAJAK<br>PAJAK BUMI DAN BANGUNAN</p>
    <table width="100%" style="border: 1px solid grey;   border-collapse: collapse;">
        <tr style="vertical-align: top">
            <td width="50%" style="border: 1px solid grey;   border-collapse: collapse;">
                <table width="100%" border="0">
                    <tr>
                        <td width="70px">Nomor</td>
                        <td width="3px">:</td>
                        <td>{{ $st->surat_nomor }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{ tglIndo($st->surat_tanggal) }}</td>
                    </tr>
                </table>
                <Strong>DATA OBJEK PAJAK</Strong>
                <table width="100%" border="0">
                    <tr style="vertical-align: top">
                        <td width="60px">NOP</td>
                        <td width="3px">:</td>
                        <td> <strong>
                                {{ $st->kd_propinsi . '.' . $st->kd_dati2 . '.' . $st->kd_kecamatan . '.' . $st->kd_kelurahan . '.' . $st->kd_blok . '-' . $st->no_urut . '.' . $st->kd_jns_op }}
                            </strong>
                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $st->jalan_op }}</td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Desa/Kelurahan</td>
                        <td>:</td>
                        <td>{{ $st->nm_kelurahan }}</td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>:</td>
                        <td>{{ $st->nm_kecamatan }}</td>
                    </tr>
                </table>
            </td>
            <td width="50%">
                <!-- <strong>DATA DOKUMEN</strong>
                <table width="100%" border="0">
                    <tr style="vertical-align: top">
                        <td width="100px">Nomor SPPT</td>
                        <td width="3px">:</td>
                        <td>{{ $st->kd_propinsi . '.' . $st->kd_dati2 . '.' . $st->kd_kecamatan . '.' . $st->kd_kelurahan . '.' . $st->kd_blok . '-' . $st->no_urut . '.' . $st->kd_jns_op }}
                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td>Tahun SPPT</td>
                        <td>:</td>
                        <td>{{ $st->tahun_tagihan }}</td>
                    </tr>
                </table> -->
                <strong>WAJIB PAJAK</strong>
                <table width="100%" border="0">
                    <table width="100%" border="0">
                        <tr style="vertical-align: top">
                            <td width="70px">NIK</td>
                            <td width="3px">:</td>
                            <td>{{ $st->nik }}
                            </td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td width="70px">NAMA</td>
                            <td width="3px">:</td>
                            <td>{{ $st->nm_wp }}
                            </td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td>Alamat</td>
                            <td>:</td>
                            <td>{{ $st->jalan_wp }}<br>
                                {{ $st->nm_kelurahan_wp }} - {{ $st->nm_kecamatan_wp }} <br>
                                {{ $st->nm_dati2_wp }} - {{ $st->nm_propinsi_wp }}
                            </td>
                        </tr>
                    </table>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr style="vertical-align: top">
            <td width="50%">
                <p style="text-align: justify">PBB terutang menurut database bapenda sampai dengan penerbitan surat tagihan pajak, dengan rincian
                    sebagai berikut: </p>
                <table class="table table-bordered" style="font-size: 11px">
                    <thead>
                        <tr>
                            <th width="10px">No</th>
                            <th width="100px">Tahun</th>
                            <th>PBB</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no = 1;
                        $total = 0;
                        @endphp
                        @foreach ($sppt as $row)
                        @php
                        $total += $row->pbb;
                        @endphp

                        <tr>
                            <td align="center">{{ $no }}</td>
                            <td align="center">{{ $row->thn_pajak_sppt }}</td>
                            <td align="right">{{ angka($row->pbb) }}</td>
                        </tr>
                        @php
                        $no++;
                        @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td align="right">{{ angka($total) }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <strong>TERBILANG :{{ strtoupper(terbilang($total)) }}</strong>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <p style="text-align: left ;"><b>Tempat Pembayaran </b>: <br>
                    <small> {{ $tp->tp }}</small>
                </p>
                <p style="text-align: left ; font-weight: bold;">Catatan :</p>
                <ol>
                    <li>Apabila Anda mengalami kesulitan saat pembayaran silahkan hubungi ke nomor layanan PBB Bapenda <strong>082230086060</strong></li>
                    <li>Segera lakukan pembayaran untuk menghindari pemblokiran serta denda dan tindakan penagihan sesuai ketentuan </li>
                </ol>
            </td>
            <td>
                <p class="text-tengah"><strong><u>PERHATIAN</u></strong></p>
                <ol>
                    <li style="text-align: justify">Surat Tagihan Pajak Daerah (STPD) ini harus dilunasi paling lambat 1 (satu) Bulan sejak tanggal diterimanya
                    </li>
                    <li style="text-align: justify">Surat Tagihan Pajak Daerah (STPD) tersebut diatas belum termasuk perhitungan sanksi administrasi berupa denda sebesar 1% perbulan maksimal 24 bulan </li>
                    <li style="text-align: justify">Apabila setelah lewat tanggal jatuh tempo hutang pajak belum dilunasi, maka akan dilakukan tindakan Penagihan berupa Surat Teguran, Surat Paksa, Pelaksanaan Sita dan Lelang</li>
                    
                </ol>
                <p style="padding-left: 15px;"><strong> Dasar Hukum :</strong></p>
                <ol>
                    <li style="text-align: justify">Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan Pemerintahan Daerah</li>
                    <li style="text-align: justify">Peraturan Pemerintah Nomor 35 Tahun 2023 Tentang Ketentuan Umum Pajak Daerah dan
                        Retribusi Daerah</li>
                    <li style="text-align: justify">Peraturan Daerah Nomor 7 Tahun 2023 </li>
                    {{-- Tentang Perubahan atas Peraturan Daerah Nomor 8
                        Tahun 2010 Tentang Pajak Daerah  --}}

                </ol>
                <p class="text-tengah">{{ $st->surat_kota }}, {{ tglIndo($st->surat_tanggal) }}<br>
                    {{ $st->surat_jabatan }}<br><br>
                    {!! QrCode::size(100)->generate($url) !!}
                    <br>
                    <strong><u>{{ $st->surat_pegawai }}</u></strong><br>

                    Pembina Utama Muda <br>
                    NIP. {{ $st->surat_nip }}

                </p>
            </td>
        </tr>
    </table>
    <br>
    <p class="text-tengah"> ---------------------------------------------------- GUNTING DISINI
        ----------------------------------------------------</p>
    <table border="0" width="100%">
        <tr>
            <td width="60%">
                <table>
                <tr>
                        <td  >Nomor</td>
                        <td  >:</td>
                        <td>{{ $st->surat_nomor }}</td>
                    </tr>
                    <tr>
                        <td width="100px">Wajib Pajak</td>
                        <td width="1px">:</td>
                        <td>{{ $st->nm_wp }}</td>
                    </tr>
                    <tr>
                        <td>Lokasi Objek</td>
                        <td>:</td>
                        <td>{{ $st->jalan_op }} , {{ $st->nm_kelurahan }} - {{ $st->nm_kecamatan }}</td>
                    </tr>
                    <tr>
                        <td>NOP</td>
                        <td>:</td>
                        <td>{{ $st->kd_propinsi . '.' . $st->kd_dati2 . '.' . $st->kd_kecamatan . '.' . $st->kd_kelurahan . '.' . $st->kd_blok . '-' . $st->no_urut . '.' . $st->kd_jns_op }}
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td width="150px">Diterima Tanggal</td>
                        <td width="1px">:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Penerima </td>
                        <td>:</td>
                        <td></td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>

</body>

</html>