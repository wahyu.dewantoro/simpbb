@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Kode BIlling untuk STPD</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-left">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <table class="table table-sm text-sm table-borderless">
                                <tr>
                                    <td width="70px"><strong>Nomor</strong></td>
                                    <td width="3px">:</td>
                                    <td>{{ $st->surat_nomor }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Tanggal</strong></td>
                                    <td>:</td>
                                    <td>{{ tglIndo($st->surat_tanggal) }}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td width="70px"><strong>NOP</strong></td>
                                    <td width="3px">:</td>
                                    <td> <strong>
                                            {{ $st->kd_propinsi . '.' . $st->kd_dati2 . '.' . $st->kd_kecamatan . '.' . $st->kd_kelurahan . '.' . $st->kd_blok . '-' . $st->no_urut . '.' . $st->kd_jns_op }}
                                        </strong>
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>Alamat</strong></td>
                                    <td>:</td>
                                    <td>{{ $st->jalan_op }}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>Desa/Kelurahan</strong></td>
                                    <td>:</td>
                                    <td>{{ $st->nm_kelurahan }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Kecamatan</strong></td>
                                    <td>:</td>
                                    <td>{{ $st->nm_kecamatan }}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td width="70px">NIK</td>
                                    <td width="3px">:</td>
                                    <td>{{ $st->nik }}
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td width="70px"><strong>NAMA</strong></td>
                                    <td width="3px">:</td>
                                    <td>{{ $st->nm_wp }}
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>Alamat</strong></td>
                                    <td>:</td>
                                    <td>{{ $st->jalan_wp }}<br>
                                        {{ $st->nm_kelurahan_wp }} - {{ $st->nm_kecamatan_wp }} <br>
                                        {{ $st->nm_dati2_wp }} - {{ $st->nm_propinsi_wp }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-7">
                            @if ($st->id_kobil == '')
                                <form action="{{ url('surat-tagihan-kobil', $st->id) }}" method="post">
                                    @csrf
                                    @method('post')
                                    <table class="table table-bordered table-sm text-sm">
                                        <thead>
                                            <tr>
                                                <th width="10px"><input type="checkbox" id="select_all" /></th>
                                                <th width="100px">Tahun</th>
                                                <th>Pokok</th>
                                                <th>Stimulus</th>
                                                <th>Denda</th>
                                                <th>Jumlah</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($sppt as $row)
                                                <tr>
                                                    <td align="center">
                                                        @if ($row->nota == '' && $row->lunas == '')
                                                            <input type="checkbox"
                                                                name="thn_pajak_sppt[{{ $row->thn_pajak_sppt }}]"
                                                                class="checkbox" value="{{ $row->thn_pajak_sppt }}" />
                                                        @endif
                                                    </td>
                                                    <td align="center">{{ $row->thn_pajak_sppt }}</td>
                                                    <td align="right">{{ angka($row->pbb) }}</td>
                                                    <td align="right">{{ angka($row->potongan) }}</td>
                                                    <td align="right">{{ angka($row->denda) }}</td>
                                                    <td align="right">
                                                        {{ angka($row->pbb - $row->potongan + $row->denda) }}</td>
                                                    <td>
                                                        @if ($row->lunas == '1')
                                                            Lunas
                                                            @endif &nbsp; @if ($row->nota == '1')
                                                                Nota Perhitungan/Kobil
                                                            @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                    <div class="float-right">
                                        <button class="btn btn-flat btn-primary" disabled id="submit"><i
                                                class="fas fa-save"></i> submit</button>
                                    </div>
                                </form>
                            @else
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-sm text-sm table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td><strong>Kode Billing</strong></td>
                                                    <td>:</td>
                                                    <td>{{ $kobil[0]->kobil }} </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Expired</strong></td>
                                                    <td>:</td>
                                                    <td>{{ $kobil[0]->expired_at }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Status</strong></td>
                                                    <td>:</td>
                                                    <td>{{ $kobil[0]->kd_status == '1' ? 'Lunas' : 'Belum lunas' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-sm text-sm table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td><strong>Pokok</strong></td>
                                                    <td>:</td>
                                                    <td>{{ angka($jumlah_pokok) }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Denda</strong></td>
                                                    <td>:</td>
                                                    <td>{{ angka($jumlah_denda) }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Jumlah</strong></td>
                                                    <td>:</td>
                                                    <td>{{ angka($jumlah_total) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-12">
                                        <a target="_blank" class="btn btn-sm btn-info"
                                            href="{{ url('data_dafnom/pdf_cetak', $st->id_kobil) }}"><i
                                                class="fas fa-print"></i></a>
                                        <table class="table table-sm table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tahun</th>
                                                    <th class="text-right">Pokok</th>
                                                    <th class="text-right">Denda</th>
                                                    <th class="text-right">Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($kobil as $row)
                                                    <tr>
                                                        <td class="text-center">{{ $row->thn_pajak_sppt }}</td>
                                                        <td class="text-right">{{ angka($row->pokok) }}</td>
                                                        <td class="text-right">{{ angka($row->denda) }}</td>
                                                        <td class="text-right">{{ angka($row->total) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script>
        $(document).ready(function() {


            var cc = 0;
            $('#select_all').on('click', function() {
                if (this.checked) {
                    $('.checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $('.checkbox').each(function() {
                        this.checked = false;
                    });
                }

                cc = $('.checkbox:checked').length
                if (cc > 0) {
                    $('#submit').attr("disabled", false);
                } else {
                    $('#submit').attr("disabled", true);
                }
            });



            $('.checkbox').on('click', function() {
                cc = $('.checkbox:checked').length
                if (cc > 0) {
                    $('#submit').attr("disabled", false);
                } else {
                    $('#submit').attr("disabled", true);
                }

                if ($('.checkbox:checked').length == $('.checkbox').length) {
                    $('#select_all').prop('checked', true);
                } else {
                    $('#select_all').prop('checked', false);
                }
            });


        })
    </script>
@endsection
