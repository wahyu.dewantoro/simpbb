@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Surat Tagihan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    <a href="{{  route('surat-tagihan.create')}}" class="btn btn-sm btn-primary btn-flat"><i class="fas fa-user-plus"></i> Tambah</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-1">

                    <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil">
                        <thead class="bg-danger">
                            <tr>
                                <th style="width:1px !important;">No</th>
                                <th>Nomor Surat</th>
                                <th>NOP</th>
                                <th>Wajib Pajak</th>
                                <th>action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .upper {
        text-transform: uppercase;
    }

    .selectappend::after {
        content: "";
        position: absolute;
        z-index: 100;
        background: rgba(221, 221, 221, .8);
        top: 0px;
        bottom: 0px;
        width: 100%;
    }

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>

<script>
    $(document).ready(function() {
        var tableObjek, tableHasil;
        const changeData = function(e, async_status = true) {
            let uri = e.attr("data-change").split("#")
                , target = e.closest('form').find("[name='" + e.attr("data-target-name") + "']");
            target.appendData("{{url('')}}/" + uri[1], {
                kd_kecamatan: e.val()
            }, {
                obj: '-- Semua Kelurahan --'
                , key: '-'
            }, async_status, true);
            return target;
        };
        $(document).on("change", "[data-change]", function(evt) {
            let e = $(this);
            changeData(e);
        });
        // $.fn.dataTable.ext.errMode = 'none' ? ? 'throw';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{   route('surat-tagihan.index')}}"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false

                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'surat_nomor'
                    , orderable: false
                    , name: 'surat_nomor'
                    , render: function(data, type, row) {
                        return data + "<br><small>" + row.surat_tanggal + "</small>"
                    }
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , name: 'nop'
                    // , searchable: false
                }
                , {
                    data: 'nm_wp'
                    , orderable: false
                    , name: 'nm_wp'
                }
                , {
                    data: 'aksi'
                    , orderable: false
                    , name: 'aksi'
                    , class: 'text-center'
                    , searchable: false
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });
        tableHasil.on('error.dt', (e, settings, techNote, message) => {
            Swal.fire({
                icon: 'warning'
                , html: defaultError
            });
        });
        const toString = function(obj) {
            let str = [];
            obj.map((key) => {
                if (key.name != '_token') {
                    str.push(key.name + "=" + key.value);
                }
            });
            return str.join("&");
        }

        $('#table-hasil').on('click', '.cetak-sk', function(e) {
            e.preventDefault()
            url = $(this).data('href')
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return true;
        })

        $('#table-hasil').on('click', '.hapus', function(e) {
            e.preventDefault()
            url = $(this).data('url')
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    // document.getElementById('delete-form-' + id).submit()
                    document.location.href = url
                }
            })
        })


    })

</script>
@endsection
