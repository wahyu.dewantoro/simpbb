@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Form Surat Tagihan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    {{-- <a href="{{  route('surat-tagihan.create')}}" class="btn btn-sm btn-primary btn-flat"><i class="fas fa-user-plus"></i> Tambah</a> --}}
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-1">
                    <form action="{{ $data['action']}}" method="post">
                        @csrf
                        @method($data['method'])
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="nop">NOP</label>
                                    <div class="col-md-6">
                                        <input type="text" name="nop" id="nop" class="form-control form-control-sm" required>

                                        <input type="hidden" name="kd_propinsi" id="kd_propinsi">
                                        <input type="hidden" name="kd_dati2" id="kd_dati2">
                                        <input type="hidden" name="kd_kecamatan" id="kd_kecamatan">
                                        <input type="hidden" name="kd_kelurahan" id="kd_kelurahan">
                                        <input type="hidden" name="kd_blok" id="kd_blok">
                                        <input type="hidden" name="no_urut" id="no_urut">
                                        <input type="hidden" name="kd_jns_op" id="kd_jns_op">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="nik">NIK</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nik" id="nik" class="form-control form-control-sm" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="nm_wp">Nama</label>
                                            <div class="col-md-9">
                                                <input type="text" name="nm_wp" id="nm_wp" class="form-control form-control-sm" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="jalan_wp">Jalan</label>
                                    <div class="col-md-10">
                                        <input type="text" name="jalan_wp" id="jalan_wp" class="form-control form-control-sm" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="rt_wp">RT</label>
                                            <div class="col-md-4">
                                                <input type="text" name="rt_wp" id="rt_wp" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="rw_wp">RW</label>
                                            <div class="col-md-4">
                                                <input type="text" name="rw_wp" id="rw_wp" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="nm_kelurahan_wp">Desa/Kel</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nm_kelurahan_wp" id="nm_kelurahan_wp" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="nm_kecamatan_wp">Kecamatan</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nm_kecamatan_wp" id="nm_kecamatan_wp" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="nm_dati2_wp">Dati2</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nm_dati2_wp" id="nm_dati2_wp" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label" for="nm_propinsi_wp">Propinsi</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nm_propinsi_wp" id="nm_propinsi_wp" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div id="tagihan"></div>
                                {{-- <div id="preview_tagihan"></div> --}}
                                <table class="table table-sm text-sm table-striped">
                                    <tbody id="preview_tagihan"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="float-right">
                                    <button id="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        $.LoadingOverlaySetup({
            background: "rgba(0, 0, 0, 0.5)",
            image: '',
            fontawesome: 'far fa-hourglass fa-spin',
            // imageAnimation: "1.5s fadein",
            // text        : "Sedang mencari...",
            imageColor: "#8080c0"
        });
        // $.LoadingOverlay("show");
        $(document).ajaxSend(function(event, jqxhr, settings) {
            $.LoadingOverlay("show");
        });
        $(document).ajaxComplete(function(event, jqxhr, settings) {
            $.LoadingOverlay("hide");
        });


        $('#submit').prop('disabled', true);
        $(window).keydown(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });

        $(document).on('keypress', function(e) {
            if (e.which == 13) {
                // alert('You pressed enter!');
                // $('#cek').trigger('click');
            }
        });

        $('#nop').trigger('keyup');

        function kosongKan() {
            $('#kd_propinsi').val('')
            $('#kd_dati2').val('')
            $('#kd_kecamatan').val('')
            $('#kd_kelurahan').val('')
            $('#kd_blok').val('')
            $('#no_urut').val('')
            $('#kd_jns_op').val('')
            $('#nik').val('')
            $('#nm_wp').val('')
            $('#jalan_wp').val('')
            $('#rt_wp').val('')
            $('#rw_wp').val('')
            $('#nm_kelurahan_wp').val('')
            $('#nm_kecamatan_wp').val('')
            $('#nm_dati2_wp').val('')
            $('#nm_propinsi_wp').val('')
            $('#submit').prop('disabled', true);
            $('#tagihan').html("")
            $('#preview_tagihan').html("")
        }

        $('#nop').on('keyup', function() {
            var nop = $(this).val();
            var convert = formatnop(nop);
            $(this).val(convert);
            if (convert.length == 24) {
                kosongKan()
                // openloading()
                $.ajax({
                    url: "{{ route('surat-tagihan.create') }}",
                    data: {
                        nop: convert
                    },
                    success: function(res) {
                        // closeloading()
                        $('#kd_propinsi').val(res.kd_propinsi)
                        $('#kd_dati2').val(res.kd_dati2)
                        $('#kd_kecamatan').val(res.kd_kecamatan)
                        $('#kd_kelurahan').val(res.kd_kelurahan)
                        $('#kd_blok').val(res.kd_blok)
                        $('#no_urut').val(res.no_urut)
                        $('#kd_jns_op').val(res.kd_jns_op)
                        $('#nik').val(res.nik)
                        $('#nm_wp').val(res.nm_wp)
                        $('#jalan_wp').val(res.jalan_wp)
                        $('#rt_wp').val(res.rt_wp)
                        $('#rw_wp').val(res.rw_wp)
                        $('#nm_kelurahan_wp').val(res.nm_kelurahan_wp)
                        $('#nm_kecamatan_wp').val(res.nm_kecamatan_wp)
                        $('#nm_dati2_wp').val(res.nm_dati2_wp)
                        $('#nm_propinsi_wp').val(res.nm_propinsi_wp)

                        let tagihan = "";
                        let preview_tagihan = "";
                        $.each(res.tagihan, function(tahun, pbb) {
                            tagihan += "<input type='hidden' name='thn_pajak_sppt[]' value='" + tahun + "'>"
                            preview_tagihan += "<tr><td>" + tahun + "</td><td>:</td><td class='text-right'>" + pbb + "</td></tr>";
                        });

                        $('#tagihan').html(tagihan)
                        $('#preview_tagihan').html(preview_tagihan)

                        console.log(res.stpd)
                        if (res.stpd!='') {
                            $('#submit').prop('disabled', true);
                            Swal.fire({
                                title: '<i class="fas fa-exclamation-triangle text-danger"></i>',
                                text: "Objek sudah pernah di terbitkan STPD dengan nomor "+res.stpd,
                                allowOutsideClick: false,
                                allowEscapeKey: false
                                // , showConfirmButton: false
                            })
                        } else
                        if (preview_tagihan == '') {
                            $('#submit').prop('disabled', true);
                            Swal.fire({
                                title: '<i class="fas fa-exclamation-triangle text-danger"></i>',
                                text: "Tidak memilikki tunggakan",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                                
                            })
                        } else {
                            $('#submit').prop('disabled', false);

                        }
                    },
                    errorr: function(er) {
                        kosongKan()
                    }
                })
            } else {
                kosongKan()
            }
        });
    })
</script>
@endsection