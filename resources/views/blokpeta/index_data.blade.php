<style>
    ul.no-bullets {
        list-style-type: none;
        /* Remove bullets */
        padding: 0;
        /* Remove padding */
        margin: 0;
        /* Remove margins */
    }

</style>
<div class="row">
    <div class="col-md-8">
        <div class="table-responsive-sm">
            <table class="table table-sm table-bordered">
                <thead class="bg-info">
                    <tr>
                        <th style="text-align: center" width="100px">No</th>
                        <th style="text-align: center">BLOK</th>
                        <th style="text-align: center">STATUS</th>
                        <th style="text-align: center" width="100px"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $i=> $row)
                    <tr>
                        <td class="text-center">{{ $i+1 }}</td>
                        <td>
                            {{ $row->kd_blok}}
                        </td>
                        <td>
                            {{ $row->status_peta }}
                        </td>
                        <td class="text-center">
                            @php
                            $id=$row->kd_kecamatan.'_'.$row->kd_kelurahan.'_'.$row->kd_blok;
                            @endphp
                            <button class="edit-blok btn btn-m btn-flat" data-kd_kecamatan="{{ $row->kd_kecamatan }}" data-status_peta_blok="{{ $row->status_peta_blok }}" data-kd_kelurahan="{{ $row->kd_kelurahan }}" data-kd_blok="{{ $row->kd_blok }}">
                                <i class="fas fa-edit text-info"></i>
                            </button>
                            @if($row->pakai==0)
                            <a data-href="{{ route('refrensi.blok-peta.hapus',$id) }}" class="hapus">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-default-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit BLOK</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table>
                    <tbody>
                        <tr>
                            <td>Kecamatan</td>
                            <td>: <span id="text_kecamatan_edit"></span></td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>: <span id="text_kelurahan_edit"></span></td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" id="val_kd_kecamatan_edit">
                <input type="hidden" id="val_kd_kelurahan_edit">
                <input type="hidden" id="val_kd_blok_old">
                <div class="form-group row">
                    <label class="col-form-label col-md-3" for="">Kode BLOK</label>
                    <div class="col-md-3">
                        <select name="kd_blok" id="kd_blok_edit" class="form-control form-control-sm">
                            <option value="">pilih</option>
                            @for ($i=0;$i<999; $i++) <option value="{{ padding($i,'0',3) }}">{{ padding($i,'0',3) }}</option>
                                @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-md-3" for="">Status</label>
                    <div class="col-md-3">
                        <select name="status_peta_blok" id="status_peta_blok" class="form-control form-control-sm">
                            <option value="0">SISTEP</option>
                            <option value="1">SISMIOP</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="editblok" type="button" class="btn btn-primary">Save</button> --}}
                <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal"><i class="far fa-window-close"></i> Close</button>
                <button id="editblok" type="button" class="btn btn-primary btn-sm btn-flat"><i class="far fa-save"></i> simpan</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $('.edit-blok').on('click', function(e) {
            $('#text_kecamatan_edit').html($("#kd_kecamatan option:selected").text())
            $('#text_kelurahan_edit').html($("#kd_kelurahan option:selected").text())

            $('#val_kd_kecamatan_edit').val($(this).data('kd_kecamatan'));
            $('#val_kd_kelurahan_edit').val($(this).data('kd_kelurahan'));
            $('#kd_blok_edit').val($(this).data('kd_blok'))

            $('#val_kd_blok_old').val($(this).data('kd_blok'))

            $('#status_peta_blok').val($(this).data('status_peta_blok'))
            $('#modal-default-edit').modal('show')
        })


        $('#editblok').on('click', function(e) {
            e.preventDefault();
            val_kd_kecamatan = $('#val_kd_kecamatan_edit').val()
            val_kd_kelurahan = $('#val_kd_kelurahan_edit').val()
            kd_blok = $('#kd_blok_edit').val()

            status_peta_blok = $('#status_peta_blok').val()
            bo = $('#val_kd_blok_old').val()
            url = "{{ url('refrensi/blok-peta') }}/" + bo
            openloading()
            $.ajax({
                type: 'PATCH'
                , url: url
                , data: {
                    kd_blok: kd_blok
                    , kd_kecamatan: val_kd_kecamatan
                    , kd_kelurahan: val_kd_kelurahan
                    , status_peta_blok: status_peta_blok
                }
                , success: function(data) {
                    $('#modal-default').modal('toggle');
                    toastr.info(data.msg, 'PROSES TAMBAH BLOK');
                    if (data.status == 1) {
                    
                        loadData(val_kd_kecamatan, val_kd_kelurahan);
                    }
                    closeloading()
                }
            });

        })



        $('.hapus').on('click', function(e) {
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "Data Peta Blok akan di hapus."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    // document.getElementById('delete-form-' + id).submit()
                    document.location.href = $(this).data('href');
                }
            })

        });

        function loadData(kd_kecamatan, kd_kelurahan) {
            openloading()
            $.ajax({
                url: "{{ url('refrensi/blok-peta') }}"
                , data: {
                    'kd_kecamatan': kd_kecamatan
                    , 'kd_kelurahan': kd_kelurahan
                }
                , success: function(res) {
                    closeloading()
                    $('#load-body').html(res)
                }
                , error: function(res) {
                    closeloading()
                }
            });
        }
    });

</script>
