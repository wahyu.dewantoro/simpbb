@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Form Surat Tugas</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Objek</h3>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="table-objek" style="width:100%">
                            <thead>
                                <tr>
                                    <TH STYLE="WIDTH:1PX !IMPORTANT;">

                                        {{-- <input type="checkbox" onClick="check_uncheck_checkbox(this.checked);"  name="checkall" id="checkall" class="form-check-input" /> --}}

                                    </TH>
                                    <TH>Ajuan</TH>
                                    <TH>NOP</TH>
                                    <TH>Alamat</TH>

                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-card-outline card-info">
                    <form action="{{ $data['action'] }}" method="post" class="form-horizontal" id="form">
                        @csrf
                        {{ method_field($data['method']) }}
                        <div class="card-header">
                            <h3 class="card-title">Surat Tugas Penelitian</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="tanggal_mulai" class="col-md-4 col-form-label">Mulai</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control form-control-sm tanggal" value="{{ isset($data['surat']->tanggal_mulai)?date('d M Y',strtotime($data['surat']->tanggal_mulai)) :'' }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="tanggal_selesai" class="col-md-4 col-form-label">Sampai</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="tanggal_selesai" id="tanggal_selesai" class="form-control form-control-sm tanggal" value="{{ isset($data['surat']->tanggal_selesai)?date('d M Y',strtotime($data['surat']->tanggal_selesai)) :'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label for="" class="col-md-6 col-form-label">Pegawai yang ditugaskan</label>
                                        <div class="col-md-2">
                                            @php
                                            $jum=1;
                                            if(isset($data['surat'])){
                                            $jum=$data['surat']->pegawai()->get()->count();
                                            }

                                            @endphp
                                            <input type="text" name="jml" id="jml" class="form-control form-control-sm" value='{{ $jum }}'>
                                        </div>
                                    </div>
                                    <div id="form-pegawai">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Daftar Objek</label>
                                        <textarea data-error="#errNm1" class="form-control d-none" name="list_nop" id="list_nop" cols="30" rows="10"></textarea>
                                    </div>
                                    <table class="table table-bordered table-sm" id="tableObjekLayanan">
                                        <thead>
                                            <tr>
                                                <th width="30px" class="text-center">No</th>
                                                <th class="text-center">NOP</th>
                                                <th class="text-center" width="30px">
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <div class="errorTxt">
                                        <span id="errNm1"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <button class="btn btn-sm btn-info"><i class="far fa-save"></i> Simpan</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Objek Penelitian</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0">
                    
                </div>

            </div>

        </div>

    </div> --}}
</section>
@endsection


@section('style')
<style>
    .errorTxt {
        border: 1px solid red;
        min-height: 20px;
    }

</style>
@endsection
@section('script')
<script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.js"></script>
<script src="{{ asset('lte') }}/plugins/jquery-validation/additional-methods.min.js"></script>
<script>
    $(document).ready(function() {
        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        var base_url = "{{ url('/') }}"

        function getFormPegawai(jumlah) {
            $("#form-pegawai").html("")
            if (jumlah > 3) {
                jumlah = 3
            }

            var html = ""

            var optionlist = "";
            @foreach($data['staff'] as $key => $st)
            id = "{{ $key }}";
            vv = "{{ $st }}";
            optionlist += '<option value="' + id + '_' + vv + '">' + vv + '</option>'
            @endforeach


            for (let index = 0; index < jumlah; index++) {
                html += '<div class="form-group row">\
                            <label for="" class="col-md-4 col-form-label">PEGAWAI KE ' + (index + 1) + '</label>\
                            <div class="col-md-8">\
                                <select id="nama_' + index + '" name="nama[]" class="form-control form-control-sm select2"><option value="">Pilih</option>' + optionlist + '</select>\
                            </div>\
                        </div>\
                        <div class="form-group row">\
                            <label for="" class="col-md-4 col-form-label">NIP</label>\
                            <div class="col-md-8">\
                                <input id="nip_' + index + '" type="text" name="nip[]" class="form-control form-control-sm" placeholder="NIP">\
                            </div>\
                        </div>\
                        <div class="form-group row">\
                            <label for="" class="col-md-4 col-form-label">JABATAN</label>\
                            <div class="col-md-8">\
                                <input id="jabatan_' + index + '" type="text" name="jabatan[]" class="form-control form-control-sm" value="Staff PBB P2" placeholder="Jabatan">\
                            </div>\
                        </div><hr>'
            }
            $("#form-pegawai").html(html)
        }

        $("#jml").on('keyup', function(e) {
            jml = $(this).val()
            getFormPegawai(jml)
        })

        $('#jml').trigger('keyup')


        @if(isset($data['surat']))
        @foreach($data['surat']->pegawai()->get() as $ip => $peg)
        idxx = "{{ $ip }}"
        var nm_peg = "{{ $peg->users_id }}_{{ $peg->nama }}"
        var nip_peg = "{{ $peg->nip }}"
        var jab_peg = "{{ $peg->jabatan }}"
        $('#nip_' + idxx).val(nip_peg)
        $('#jabatan_' + idxx).val(jab_peg)
        $('#nama_' + idxx).val(nm_peg)
        @endforeach
        @endif


        var arrayObjek = []
            , listObjek = []
            , paramAllNop = []
        $(document).on("click", '.pilihnop', function() {
            countNop = $('#tableObjekLayanan tbody tr').length;
            countNop = countNop + 1;
            var nopcheck = $(this).data('id') + '#' + $(this).data('nop') + '#' + $(this).data('alamat_op') + '#' + $(this).data('nm_kelurahan') + '#' + $(this).data('nm_kecamatan')

            var tableListNop = "<tr id='row" + countNop + "' class='" + $(this).data('id') + " row_objek'>\
                            <td class='text-center'>" + countNop + "</td>\
                            <td>" + formatnop($(this).data('nop')) + "<br><small class='text-warning'>" + $(this).data('nm_kelurahan') + " - " + $(this).data('nm_kecamatan') + "</small></td>\
                            <td class='text-center'><a role='button' tabindex='0' class= 'text-danger remove flat' data-key='nop'  data-nm_kecamatan='" + $(this).data('nm_kecamatan') + "' data-nm_kelurahan='" + $(this).data('nm_kelurahan') + "' data-alamat_op='" + $(this).data('alamat_op') + "'  data-nop='" + $(this).data('nop') + "' data-id='" + $(this).data('id') + "'  title='Hapus'>\
                                    <i class='fa fa-trash'></i>\
                                </a>\
                            </td>\
                        </tr>";
            if ($(this).is(':checked')) {
                arrayObjek.push(nopcheck)
                listObjek = arrayObjek.join(',')
                $('#list_nop').val(listObjek)
                $("#tableObjekLayanan tbody").append(tableListNop)
                $(this).closest('tr').find('.pilih_nop').attr('disabled', true)
                // paramAllNop = listObjek

            } else {
                var remove_Item = nopcheck
                arrayObjek = $.grep(arrayObjek, function(value) {
                    return value != remove_Item;
                });
                // console.log(arrayObjek)
                listObjek = arrayObjek.join(',')
                $('#list_nop').val(listObjek)
                $("#tableObjekLayanan tbody").find('.' + $(this).data('id')).remove()

                $(this).closest('tr').css('background-color', 'transparent');
            }
            paramAllNop = arrayObjek
        });


        var tableObjek, idsurat, appendurl

        appendurl = ""

        @if(isset($data['surat']))
        appendurl = "?id={{ $data['surat']->id }}"
        @endif


        tableObjek = $('#table-objek').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('penelitian/objek-penelitian') }}" + appendurl
            , columns: [{
                    data: 'pilih'
                    , name: 'pilih'
                    , searchable: false
                    , orderable: false
                }
                , {
                    data: 'nomor_layanan'
                    , orderable: false
                    , name: 'nomor_layanan'
                    , render: function(data, type, row) {
                        data += '<br>' + row.jenis_layanan_nama
                        // console.log(row.keterangan)
                        if (row.keterangan) {
                            data += "<br><span class='text-danger'>" + row.keterangan + "</span>";
                        }

                        return data
                    }
                }, {
                    data: 'nop'
                    , orderable: false
                    , name: 'nama_nop'
                }
                , {
                    
                    data: 'alamat_op'
                    , orderable: false
                    , name: 'alamat_op'
                    , render: function(data, type, row) {
                        data += '<br>' + row.nm_kelurahan + ' ' + row.nm_kecamatan
                        return '<small>' + data + '</small>'
                    }
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "columnDefs": [{
                "orderable": false
                , "targets": '_all'
            }]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {
                var alamat = data.alamat_op
                if (data.alamat_op == null) {
                    alamat = ''
                }
                var inrow = data.id + '#' +
                    data.kd_propinsi +
                    data.kd_dati2 +
                    data.kd_kecamatan +
                    data.kd_kelurahan +
                    data.kd_blok +
                    data.no_urut +
                    data.kd_jns_op +
                    '#' + alamat + '#' + data.nm_kelurahan + '#' + data.nm_kecamatan;
                var cek = jQuery.inArray(inrow, paramAllNop)

                if (cek !== -1) {
                    $('td', row).find('.pilihnop').prop('checked', true);
                }
            }
        });

        $(document).on('click', '.remove', function() {
            var remove_Item = $(this).data('id') + '#' + $(this).data('nop') + '#' + $(this).data('alamat_op') + '#' + $(this).data('nm_kelurahan') + '#' + $(this).data('nm_kecamatan')
            arrayObjek = $.grep(arrayObjek, function(value) {
                return value != remove_Item;
            });
            listObjek = arrayObjek.join(',')
            $('#list_nop').val(listObjek)
            paramAllNop = arrayObjek
            // $('#table-objek') closest('tr').css('background-color', 'transparent')
            $('#table-objek').find('.pilihnop').prop('checked', false)

            $("#tableObjekLayanan tbody").find('.' + $(this).data('id')).remove()


        });

        $('#tanggal_mulai').on('change', function() {
            console.log($(this).val())
            $('#tanggal_selesai').val($(this).val())
        })


        // define objek form edit
        // $(this).data('id') + '#' + $(this).data('nop') + '#' + $(this).data('alamat_op') + '#' + $(this).data('nm_kelurahan') + '#' + $(this).data('nm_kecamatan')
        @if(isset($data['surat']))
        @foreach($data['surat']->objek()->get() as $obj)


        countNop = $('#tableObjekLayanan tbody tr').length;
        countNop = countNop + 1;
        var nopcheck = '{{ $obj->layanan_objek_id}}#{{ $obj->kd_propinsi. $obj->kd_dati2. $obj->kd_kecamatan. $obj->kd_kelurahan. $obj->kd_blok. $obj->no_urut. $obj->kd_jns_op }}#{{ $obj->alamat }}#{{ $obj->nm_kelurahan }}#{{ $obj->nm_kecamatan }}'
        var tableListNop = "<tr id='row" + countNop + "' class='{{ $obj->layanan_objek_id}} row_objek'>\
                            <td class='text-center'>" + countNop + "</td>\
                            <td>" + formatnop("{{ $obj->kd_propinsi. $obj->kd_dati2. $obj->kd_kecamatan. $obj->kd_kelurahan. $obj->kd_blok. $obj->no_urut. $obj->kd_jns_op }}") + "<br><small class='text-warning'>{{ $obj->nm_kelurahan }} - {{ $obj->nm_kecamatan }}</small></td>\
                            <td class='text-center'><a role='button' tabindex='0' class= 'text-danger remove flat' data-key='nop'  data-nm_kecamatan='{{ $obj->nm_kecamatan }}' data-nm_kelurahan='{{ $obj->nm_kelurahan }}' data-alamat_op='{{ $obj->alamat }}'  data-nop='{{ $obj->kd_propinsi. $obj->kd_dati2. $obj->kd_kecamatan. $obj->kd_kelurahan. $obj->kd_blok. $obj->no_urut. $obj->kd_jns_op }}' data-id='{{ $obj->layanan_objek_id}}'  title='Hapus'>\
                                    <i class='fa fa-trash'></i>\
                                </a>\
                            </td>\
                        </tr>";
        $("#tableObjekLayanan tbody").append(tableListNop)

        arrayObjek.push(nopcheck)
        @endforeach
        paramAllNop = arrayObjek
        listObjek = arrayObjek.join(',')
        $('#list_nop').val(listObjek)

        console.log(paramAllNop);
        @endif




        $("#form").submit(function() {
            var form = $("#form");
            form.validate({
                errorElement: "span"
                , errorPlacement: function(error, element) {
                    error.addClass("invalid-feedback");
                    element.closest(".form-group").append(error);
                }
                , highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid");
                }
                , unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                }
                , rules: {
                    'jml': {
                        required: true
                        , min: 1
                        , max: 3
                    }
                    , 'tanggal_mulai': {
                        required: true
                    }
                    , 'tanggal_selesai': {
                        required: true
                    }
                    , 'list_nop': {
                        required: true
                    }
                    , 'nama[]': {
                        required: true
                    }
                    , 'nip[]': {
                        required: true
                    }
                    , 'jabatan[]': {
                        required: true
                    }
                , }
            });


            if (form.valid() === false) {
                return false
            } else {

                obj = $('#list_nop').val()
                if (obj == '') {
                    Swal.fire({
                        icon: "error"
                        , title: "Peringatan"
                        , text: "Daftar objek penelitian belum di isi "
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    });

                    return false
                }

            }

        });

        $(document).on("keypress", "input,select", function(e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(":focusable");
                var index = $canfocus.index(document.activeElement) + 1;
                if (index >= $canfocus.length) index = 0;
                $canfocus.eq(index).focus();
            }
        });

       /*  function check_uncheck_checkbox(isChecked) {
            if (isChecked) {
                $('input[type="checkbox"]').each(function() {
                    this.checked = true;
                });
            } else {
                $('input[type="checkbox"]').each(function() {
                    this.checked = false;
                });
            }
        } */

        $('#table-objek').on('click', '.pelimpahan', function(e) {
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "melimpahkan berkas ini ke penelitian Kantor"
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    document.location.href = $(this).data('href');
                }
            })

        });

    })

    window.addEventListener("beforeunload", function(e) {
            openloading()            
        });
</script>
@endsection
