@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Surat Tugas Penelitian</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    <a href="{{ url('penelitian/surat-tugas/create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-folder-plus"></i> Tambah
                    </a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">

        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link   {{  $tab=='list'?'active':'' }} " id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">List</a>
                    </li>
                    @if ($verifikasi=='1')
                    <li class="nav-item">
                        <a class="nav-link {{  $tab=='verifikasi'?'active':'' }} " id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Verifikasi <span id='count' class="badge badge-info"></span></a>
                    </li>
                    @endif
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                    <div class="tab-pane fade  {{  $tab=='list'?'show active':'' }}  " id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                        <table id="table-surat" class="table table-sm table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Batch</th>
                                    <th>Tanggal</th>
                                    <th>Pegawai</th>
                                    <th>Lokasi</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    @if ($verifikasi=='1')
                    <div class="tab-pane fade {{  $tab=='verifikasi'?'show active':'' }}" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                        <table id="table-verifikasi" class="table table-sm table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Batch</th>
                                    <th>Tanggal</th>
                                    <th>Pegawai</th>
                                    <th>Lokasi</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-verifikasi">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Verifikasi Surat Tugas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-1 m-1">
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        var tableObjek, tableVerifikasi
        // table-objek

        $('#count').html('0')


        $('#table-verifikasi').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('penelitian/surat-tugas-verifikasilist') }}"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }, {
                    data: 'batch'
                    , name: 'batch'
                    , orderable: false
                }
                , {
                    data: 'tanggal_peneitian'
                    , name: 'tanggal_peneitian'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'pegawai'
                    , name: 'pegawai'
                    , orderable: false
                }
                , {
                    data: 'lokasi'
                    , name: 'lokasi'
                    , orderable: false
                }
                , {
                    data: 'status'
                    , name: 'status'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'aksi'
                    , name: 'aksi'
                    , searchable: false
                    , orderable: false
                    , class: 'text-center'
                }
            ]
            , fnDrawCallback: function() {

                // console.log(this.api().page.info().recordsTotal);
                $('#count').html(this.api().page.info().recordsTotal);
            }
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {

            }
        });

        tableObjek = $('#table-surat').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('penelitian/surat-tugas') }}"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }, {
                    data: 'batch'
                    , name: 'batch'
                    , orderable: false
                }
                , {
                    data: 'tanggal_peneitian'
                    , name: 'tanggal_peneitian'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'pegawai'
                    , name: 'pegawai'
                    , orderable: false
                }
                , {
                    data: 'lokasi'
                    , name: 'lokasi'
                    , orderable: false
                }
                , {
                    data: 'status'
                    , name: 'status'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'aksi'
                    , searchable: false
                    , orderable: false
                    , class: 'text-center'
                }
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {

            }
        });

        $('#table-surat').on('click', '.hapus', function(e) {
            id = $(this).data('id')
            e.preventDefault();
            var token = '{{ csrf_token() }}'
            console.log('cliked hapus')

            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        url: "{{ url('penelitian/surat-tugas') }}/" + id
                        , method: "POST"
                        , data: {
                            surat_tugas: id,
                            // set a parameter named _method with value 'delete'
                            _method: 'delete',
                            // set a parameter named _token with the token value
                            _token: token
                        }
                        , success: function(data) {
                            closeloading()
                            if (data.status == 1) {
                                toastr.success(data.msg);
                            } else {
                                toastr.error(data.msg);
                            }
                            newUrl = "{{  url('penelitian/surat-tugas') }}"
                            $('#table-surat').DataTable().ajax.url(newUrl).load()
                        }
                        , error: function(data) {
                            Swal.fire({
                                title: 'warning'
                                , text: "ada kesalahan dalam sistem"
                                , icon: 'warning'
                            })
                        }
                    })
                }
            })
        })

        // verifikasi show
        // 
        $('#table-verifikasi').on('click', '.show-verifikasi', function(e) {
            $('.modal-body').html('')
            openloading()
            $.ajax({
                url: "{{ url('penelitian/surat-tugas-verifikasi') }}"
                , data: {
                    id: $(this).data('id')
                }
                , success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-verifikasi").modal('show')
                }
                , error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('.modal-body').html('')
                }
            });
        })
    })

</script>
@endsection
