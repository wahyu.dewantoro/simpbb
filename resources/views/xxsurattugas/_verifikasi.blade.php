<div class="row">
    <div class="col-md-6">
        <table class="table table-sm table-borderless">
            <tr>
                <td>batch</td>
                <td width="1px">:</td>
                <td>{{ $data->batch }}</td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td width="1px">:</td>
                <td>
                    @php
                    $tanggal =tglindo($data->tanggal_mulai);
                    $ts =tglindo ($data->tanggal_selesai);
                    if($tanggal<>$ts){
                        $tanggal.=' S/d. '.$ts;
                        }

                        @endphp
                        {{ $tanggal }}
                </td>
            </tr>
            <tr>
                <td>Pegawai</td>
                <td width="1px">:</td>
                <td>
                    @php
                    $res = $data->pegawai()->get()->pluck('nama')->toarray();
                    echo implode(', ', $res);

                    @endphp
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td width="1px">:</td>
                <td>{{ $data->lokasi }}</td>
            </tr>

        </table>
    </div>
    <div class="col-md-4">
        <form action="{{ url('penelitian/surat-tugas-proses-verifikasi') }}" method="post">
            @csrf
            @method('post')
            <input type="hidden" name="id" value="{{ $data->id }}">
            <div class="form-group">
                <label for="">Verifikasi</label>
                <select name="kd_status" id="kd_status" class="form-control form-control-sm" required>
                    <option value="">Pilih</option>
                    <option value="1">Setuju</option>
                    <option value="0">Tolak</option>
                </select>
            </div>
            <button class="btn btn-sm btn-info"><i class="far fa-save"></i> Simpan</button>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <p><strong>Daftar Objek Penelitian</strong></p>
        <table class="table table-sm table-bordered">
            <thead>
                <tr>
                    <th width="5px">No</th>
                    <th>NOP</th>
                    <th>Alamat</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->objek()->get() as $i=> $item)
                <tr>
                    <td class="text-center">
                        {{$i+1}}
                    </td>
                    <td>{{
                        formatnop($item->kd_propinsi.$item->kd_dati2.$item->kd_kecamatan.$item->kd_kelurahan.$item->kd_blok.$item->no_urut.$item->kd_jns_op)
                        
                    }}</td>
                    <td>
                        {{$item->alamat}}<br>
                        <small>
                            {{ $item->nm_kelurahan.' - '.$item->nm_kecamatan}}
                        </small>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
