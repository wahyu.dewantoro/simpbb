<html>

<head>
    <style>
        @page {
            margin: 5mm
        }

        body {
            margin: 5mm;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12pt
        }

        #watermark {
            position: fixed;
            top: 50mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 12pt !important
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 13pt !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 9 px !important
        }

        .text-sedang {
            font-size: 11px !important
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: .9em
        }

        #header {
            top: 0;
            border-bottom: .1pt solid #aaa
        }

        #footer {
            bottom: 0;
            border-top: .1pt solid #aaa
        }

        .page-number:before {
            content: "Page "counter(page)
        }

        p{
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13pt
        }
    </style>

</head>

<body>
    <table width="100%" style="font-size: .875rem !important ">
        <tr>
            <th width="70px"><img style="-webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
                filter: grayscale(100%);" width="60px" height="70px" src="{{ public_path('kabmalang.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>
                    <span style="font-size: 18pt">BADAN PENDAPATAN DAERAH</span>
                        <br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            <th width="100px"></th>
        </tr>
    </table>
    <hr style="border-color: black">

    <p style="font-size: 12pt" class="text-tengah"><u><strong>SURAT PERINTAH TUGAS</strong></u><br>
        Nomor : {{ $st->nomor_surat }}</p>
    <table style="width: 100%; border:1px;">
        <tbody>
            <tr style="vertical-align: top;">
                <td width="100px">Dasar</td>
                <td width='1px'>:</td>
                <td>
                    <ol>
                        <li>Peraturan Daerah Kabupaten Malang Nomor 6 Tahun 2021 tentang Anggaran Pendapatan dan Belanja Daerah Tahun Anggaran 2022;</li>
                        <li>Peraturan Bupati Malang Nomor 196 tahun 2021 tentang Penjabaran Anggaran Pendapatan dan Belanja Daerah Tahun Anggaran 2022;</li>
                        <li>Dokumen Pelaksanaan Anggaran (DPA) Badan Pendapatan Daerah Kabupaten Malang Tahun Anggaran 2022;
                            No. DPA/A.1/5.02.0.00.0.0.0.29.0000/001/2022 Tanggal 4 Januari 2022, dengan ini :
                        </li>
                    </ol>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="text-tengah"><u><strong>MEMERNTAHKAN</strong></u></p>
    <table style="width: 100%; border:1px;">
        <tbody>
            <tr style="vertical-align: top;">
                <td width="100px">Pegawai</td>
                <td width='1px'>:</td>
                <td>
                    <table style="width: 100%; border:1px;">
                        <tbody>

                            @foreach ($st->pegawai()->get() as $no=> $pegawai)
                            <tr style="vertical-align: top">
                                <td width="5px">{{ $no+1 }}.</td>
                                <td width="50px">Nama</td>
                                <td width='1px'>:</td>
                                <td>{{ $pegawai->nama }}</td>
                            </tr>
                            <tr style="vertical-align: top">
                                <td></td>
                                <td width="50px">NIP</td>
                                <td width='1px'>:</td>
                                <td>{{ $pegawai->nip }}</td>
                            </tr>
                            <tr style="vertical-align: top">
                                <td></td>
                                <td width="50px">Jabatan</td>
                                <td width='1px'>:</td>
                                <td>{{ $pegawai->jabatan }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="vertical-align: top">
                <td>Untuk</td>
                <td>:</td>
                <td>
                    @php
                    $hari=tglHari($st->tanggal_mulai);
                    $harib=tglHari($st->tanggal_selesai);
                    if($harib<>$hari){
                        $hari.=' - '.$harib;
                        }

                        $tanggal=tglIndo($st->tanggal_mulai);
                        $tanggalb=tglIndo($st->tanggal_selesai);
                        $res=$tanggal;
                        if($tanggal<>$tanggalb){
                            if(date('m',strtotime($tanggal)) <> date('m',strtotime($tanggalb))){
                                $res=$tanggal.' - '.$tanggalb;
                                }else{
                                $res=date('d',strtotime($tanggal)).' - '.$tanggalb;
                                }
                                }
                                @endphp
                                <p>Dalam rangka melaksanakan kegiatan Penilaian Individual Objek Pajak Perumahan di Kecamatan {{$st->lokasi}} pada hari {{$hari}} tanggal {{ $res }}.</p>
                </td>

            </tr>
        </tbody>
    </table>
    <p style="text-indent: 50px;">Sesuai prosedur, setelah melaksanakan kegiatan dimaksud agar melaporkan hasilnya kepada Pimpinan.</p>
    <p style="text-indent: 50px;">Demikian untuk dilaksanakan dengan penuh tanggung jawab.</p>
    <table border="0" width="100%">
        <tr style="vertical-align: top">
            <td rowspan="2" width="600px" >&nbsp;</td>
            <td rowspan='2' width="100px">&nbsp; </td>
            <td width="400px">
                <table>
                    <tr>
                        <td>Dikelurakan di</td>
                        <td>: {{ $st->surat_kota }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>: {{ tglIndo($st->surat_tanggal)  }}</td>
                    </tr>
                </table>
                <hr style="border-color: black">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <p class="text-center" style="font-size: 12pt">
                    a.n. KEPALA BADAN PENDAPATAN DAERAH <br>KABUPATEN MALANG<br>
                        Sekretaris<br>
                        u.b.<br>
                    <b>{{ $st->surat_jabatan }}</b>
                </p>
                <br><br>
                <p><strong><u>{{ $st->surat_pegawai }}</u></strong><br>{{ $st->surat_pangkat }}<br>
                    NIP. {{ $st->surat_nip }}
                    </p>
            </td>
        </tr>
    </table>
    
</body>

</html>
