@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Camat</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                    {{-- <a href="{{ route('refrensi.camat.create') }}" class="btn btn-primary btn-sm">
                    <i class="fas fa-file"></i> Tambah
                    </a> --}}

                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-default">
                        <i class="fas fa-file"></i> Tambah
                    </button>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h3 class="card-title">Tahun Pajak {{ date('Y') }}</h3> --}}
                        <div class="card-tools">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-sm table-bordered text-sm" id="table-data">
                            <thead>
                                <tr>
                                    <th style="text-align: center">No</th>
                                    <th style="text-align: center">Kecamatan</th>
                                    <th style="text-align: center">Camat</th>
                                    <th style="text-align: center">Mulai</th>
                                    <th style="text-align: center">Sampai</th>

                                    <th style="text-align: center">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-cetak" action="{{ route('refrensi.camat.store') }}" method="post">
                @method('post')
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Form Camat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Kecamatan</label>
                        <div class="col-md-8">
                            {{-- <input type="text" > --}}
                            <select name="kd_kecamatan" id="kd_kecamatan" class="form-control form-control-sm" required>
                                <option value="">Pilih</option>
                                @foreach ($ref_kecamatan as $rk)
                                <option value="{{  $rk->kd_kecamatan }}">{{ $rk->kd_kecamatan }} - {{ $rk->nm_kecamatan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Nama</label>
                        <div class="col-md-8">
                            <input type="text" name="nm_camat" id="nm_camat" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">NIP</label>
                        <div class="col-md-8">
                            <input type="text" name="nip_camat" id="nip_camat" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Periode Jabatan</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-6">
                                    <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control form-control-sm tanggal">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control form-control-sm tanggal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form-edit" action="#" method="post">
                @method('post')
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Form Camat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" name="id" id="id_edit">
                        <label class="col-form-label col-md-4">Kecamatan</label>
                        <div class="col-md-8">
                            {{-- <input type="text" > --}}
                            <select name="kd_kecamatan_edit" id="kd_kecamatan_edit" class="form-control form-control-sm" required>
                                <option value="">Pilih</option>
                                @foreach ($ref_kecamatan as $rk)
                                <option value="{{  $rk->kd_kecamatan }}">{{ $rk->kd_kecamatan }} - {{ $rk->nm_kecamatan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Nama</label>
                        <div class="col-md-8">
                            <input type="text" name="nm_camat_edit" id="nm_camat_edit" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">NIP</label>
                        <div class="col-md-8">
                            <input type="text" name="nip_camat_edit" id="nip_camat_edit" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4">Periode Jabatan</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-6">
                                    <input type="text" name="tgl_mulai_edit" id="tgl_mulai_edit" class="form-control form-control-sm tanggal">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="tgl_selesai_edit" id="tgl_selesai_edit" class="form-control form-control-sm tanggal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function() {

        $(document).on('keypress', function(e) {
            if (e.which == 13) {
                // alert('You pressed enter!');
                // $('#cek').trigger('click');
                return false;
            }
        });

        $('.tanggal').val('')


        let table = $('#table-data').DataTable({
            processing: true
            , serverSide: true
            , ajax: {
                url: "{!! route('refrensi.camat.index') !!}"
                , data: function(d) {
                    d.thn_pajak = $('#thn_pajak').val()
                }
            }

            , columns: [{
                    data: null
                    , searchable: false
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                }
                , {
                    data: 'nm_kecamatan'
                    , name: 'ref_kecamatan.nm_kecamatan'
                }
                , {
                    data: 'nm_camat'
                    , name: 'nm_camat'
                    , render: function(data, type, row, meta) {
                        return data + '<br><small>NIP :' + row.nip_camat + '</small>'
                    }
                }
                , {
                    data: 'tgl_mulai'
                    , name: 'tgl_mulai'
                    , class: 'text-right'
                    , searchable: false
                }
                , {
                    data: 'tgl_selesai'
                    , name: 'tgl_selesai'
                    , class: 'text-center'
                    , searchable: false
                }, {
                    data: 'aksi'
                    , name: 'aksi'
                    , class: 'text-center'
                    , searchable: false
                }

            ]
        });

        $("#form-cetak").on("submit", function(event) {
            openloading();
            event.preventDefault();
            $.ajax({
                url: "{{ route('refrensi.camat.store') }}"
                , type: 'post'
                , data: $(this).serialize()
                , success: function(res) {
                    closeloading();
                    toastr.success(res, 'Data Camat');
                    $('#modal-default').modal('toggle');
                    table.draw();
                    kosongkan()
                }
                , error: function(res) {
                    closeloading();
                    toastr.error('Gagal menambah data', 'Data Camat');
                    table.draw();
                }
            })
        });

        function kosongkan() {
            $('#kd_kecamatan').val('')
            $('#nm_camat').val('')
            $('#nip_camat').val('')
            $('#tgl_mulai').val('')
            $('#tgl_selesai').val('')
        }

        $('#table-data').on('click', '.hapus', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        url: "{!! url('refrensi/camat-hapus') !!}/" + id
                        , data: {
                            id: id
                        }
                        , success: function(res) {
                            closeloading();
                            toastr.success(res, 'Data Camat');
                            table.draw();
                        }
                        , error: function(res) {
                            closeloading();
                            toastr.error('Gagal menambah data', 'Data Camat');
                            // table.draw();
                        }
                    })
                }
            })
        })

        $('#table-data').on('click', '.edit', function(e) {
            e.preventDefault()
            let id = $(this).data('id')
            let kd_kecamatan = $(this).data('kd_kecamatan')
            let nm_camat = $(this).data('nm_camat')
            let nip_camat = $(this).data('nip_camat')
            let tgl_mulai = $(this).data('tgl_mulai')
            let tgl_selesai = $(this).data('tgl_selesai')

            $('#kd_kecamatan_edit').val(kd_kecamatan)
            $('#id_edit').val(id)
            $('#nm_camat_edit').val(nm_camat)
            $('#nip_camat_edit').val(nip_camat)
            $('#tgl_mulai_edit').val(tgl_mulai)
            $('#tgl_selesai_edit').val(tgl_selesai)

            $('#tgl_mulai_edit').trigger('keyup')
            $('#tgl_selesai_edit').trigger('keyup')

            $('#modal-edit').modal('toggle');
        })

        $("#form-edit").on("submit", function(event) {
            openloading();
            event.preventDefault();
            $.ajax({
                url: "{{ route('refrensi.camat.update') }}"
                , type: 'post'
                , data: $(this).serialize()
                , success: function(res) {
                    closeloading();
                    toastr.success(res, 'Data Camat');
                    $('#modal-edit').modal('toggle');
                    table.draw();
                    kosongkan()
                }
                , error: function(res) {
                    closeloading();
                    toastr.error('Gagal edit data', 'Data Camat');
                    table.draw();
                }
            })
        });

    })

</script>
@endsection
