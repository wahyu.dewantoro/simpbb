<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN KOMPENSASI LEBIH BAYAR</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{$subjek['kota_wp']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{$subjek['kelurahan_wp']}}</td>
        
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>NOP</th>
            <th>Tahun</th>
            <th>Nama WP</th>
            <th>Alamat WP</th>
            <th>Alamat OP</th>
            <th>Tanggal bayar</th>
            <th>PBB Bayar</th>
            <th>PBB</th>
            <th>Lebih Bayar</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @php 
            $pbb_asli_total=0; 
            $pbb_bayar_total=0;    
        @endphp
        @foreach ($result as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td >
                    {{ $row['kd_propinsi'] }}.
                    {{ $row['kd_dati2'] }}.
                    {{ $row['kd_kecamatan'] }}.
                    {{ $row['kd_kelurahan'] }}.
                    {{ $row['kd_blok'] }}-
                    {{ $row['no_urut'] }}.
                    {{ $row['kd_jns_op'] }}
                </td>
                <td >{{ $row['thn_pajak_sppt'] }}</td>
                <td >{{ $row['nama_wp']}}</td>
                <td >{{ $row['alamat_wp']}}</td>
                <td >{{ $row['alamat_op'] }}</td>
                <td >{{ $row['tgl_bayar']??'' }}</td>
                <td >{{ $row['pbb_bayar']??'0' }}</td>
                <td >{{ $row['pbb_asli']??'0'}}</td>
                <td >{{ $row['selisih']??'0' }}</td>
            </tr>
            @php 
                $no++; 
                $pbb_asli_total=$pbb_asli_total+$row['pbb_asli']; 
                $pbb_bayar_total=$pbb_bayar_total+$row['pbb_bayar']; 
            @endphp
        @endforeach
        <tr>
            @php 
                $selisih_total=$pbb_bayar_total-$pbb_asli_total;
            @endphp
            <td colspan="7" align="center">TOTAL</td>
            <td >{{ $pbb_asli_total??'0'}}</td>
            <td >{{ $pbb_bayar_total??'0' }}</td>
            <td >{{ $selisih_total??'0' }}</td>
        </tr>
    </tbody>
</table>
