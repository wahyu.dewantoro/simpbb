@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"> Form Kompensasi Bayar </h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                <li class="nav-item">
                                    <a class="nav-link active" href="{{url('kompensasi_lebih_bayar/daftar')}}"><i class="fas fa-arrow-left"></i> Daftar Kompensasi Lebih Bayar </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body no-padding">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="card">
                                        <div class="card-body  p-1">
                                             @include('kompensasi_bayar/form_list')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let textnull='<tr class="null"> <td colspan="9" class="dataTables_empty text-center"> Data Masih Kosong</td> </tr>';
            let defaultError='Proses Kompensasi gagal.';
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: value??{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
            $(document).on("keyup", "[name='nop']", function (evt) {
                let e=$(this);
                if(e.inputmask('unmaskedvalue').length==18){
                    e.closest('.form-group').find('[data-nop]').trigger('click');
                }
            });
            $(document).on("click", "[data-nop]", function (evt) {
                let e=$(this),
                card=e.closest(".card"),
                form=e.closest('form'),
                name=e.attr('data-nop'),
                tInput=card.find('[name="'+name+'"]'),
                v=tInput.val(),
                vmask=tInput.inputmask('unmaskedvalue'),
                uri='{{ url("kompensasi_lebih_bayar/ceknop") }}';
                if(vmask.length==18){
                    asyncData(uri,{nop:v}).then((response) => {
                        if(!response.status){
                            Swal.fire({ icon: 'error', html: response.msg});
                        };
                        $.each(response.data,(x,y)=>{
                            let setdata=card.find('[name="'+x+'"]');
                            setdata.val(y);
                        });
                        $.each(response.tag,(x,y)=>{
                            let settag=form.find('[data-name="'+x+'"]');
                            if(settag){
                                settag.html(y);
                            };
                        });
                    });
                };
            });
            let resetForm=function(e){
                e[0].reset();
                e.find("[data-name='riwayat_pembayaran']").html(textnull);
            };
            $(document).on("submit", "#form-kompensasi", function (evt) {
                evt.preventDefault();
                let e=$(this);
                if(!e.find('.check-filled:checked').length){
                    return  Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Tahun Pembayaran Harap di pilih!!!'
                    });
                }
                Swal.fire({
                    title: e.attr('data-loader'),
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            return Swal.fire({ icon: 'warning', html:response.msg??defaultError });
                        }
                        resetForm(e);
                        if(response.curl){
                            Swal.fire({
                                title: 'Info',
                                html:response.msg,
                                showConfirmButton: false,
                            });
                        }else{
                            Swal.fire({ icon: 'success', text: response.msg});
                        }
                    },
                    error: function (x, y) {
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            });
        });
    </script>
@endsection