@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Daftar Kompensasi Lebih Bayar</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <form action="#" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <div class="input-group input-group-sm">
                                            <select name="kecamatan"  class="form-control-sm form-control select-bottom"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                                                @if(count($kecamatan)>1) <option value="-">-- Kecamatan --</option> @endif
                                                @foreach ($kecamatan as $rowkec)
                                                    <option value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group input-group-sm">
                                            <select name="kelurahan"  class="form-control-sm form-control select-bottom select-reset change-next"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu)." >
                                                @if(count($kelurahan)>1) <option value="-">-- Kelurahan --</option> @endif
                                                @foreach ($kelurahan as $rowkel)
                                                    <option value="{{ $rowkel->kd_kelurahan }}">
                                                        {{ $rowkel->nm_kelurahan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control form-control-sm nop_full"  name="nop" placeholder="35.07.***.***.***-****.*">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group-sm">
                                            <button type='submit' class='btn btn-sm btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group input-group-sm">
                                            <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Export </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-body no-padding p-1">
                                <table  class="pull-right table-bordered table-striped table-sm mb-1" id="total-table">
                                    <thead>
                                        <tr>
                                            <th>PBB Total</th>
                                            <th>PBB Bayar Total</th>
                                            <th>Lebih Bayar Total</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm text-right'>
                                        <tr>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <a class='btn btn-sm btn-info pull-right' href="{{url('kompensasi_lebih_bayar')}}"> 
                                <i class="fas fa-plus"></i> Buat Kompensasi Lebih Bayar </a>
                            <div class="card-body no-padding p-1">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>NOP</th>
                                            <th>Tahun</th>
                                            <th>Nama WP</th>
                                            <th>Alamat WP</th>
                                            <th>Alamat OP</th>
                                            <th>Tanggal bayar</th>
                                            <th>PBB Bayar</th>
                                            <th>PBB</th>
                                            <th>Lebih Bayar</th>
                                            <th>---</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'data':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('kompensasi_lebih_bayar/search') }}",
                    method: 'GET'
                },
                lengthMenu: [20],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'nop',class:'w-15'},
                    { data: 'thn_pajak_sppt'},
                    { data: 'nama_wp'},
                    { data: 'alamat_wp'},
                    { data: 'alamat_op'},
                    { data: 'tgl_bayar'},
                    { data: 'pbb_bayar'},
                    { data: 'pbb_asli'},
                    { data: 'selisih'},
                    { data: 'raw_tag'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    if(key.name!='_token'){
                        str.push(key.name+"="+key.value);
                    }
                });
                return str.join("&");
            }
            $(document).on("click", ".btn-export", function (evt) {
                let e=$(this),
                    form=e.closest('form'),
                    url="{{url('kompensasi_lebih_bayar/cetak')}}?"+toString(form.serializeArray());
                    window.open(url, '_blank');
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let cekValue=$('.nop_full[name="nop"]');
                if(cekValue.inputmask('unmaskedvalue').length!=18&&cekValue.inputmask('unmaskedvalue').length!='0') return;
                let e=$(this),
                    uri="{{ url('kompensasi_lebih_bayar/search') }}";
                Swal.fire({
                    title: 'Pencarian Data.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri+"?"+toString(e.serializeArray())).load((response)=>{
                    asyncData("{{ url('kompensasi_lebih_bayar/total') }}"+"?"+toString(e.serializeArray())).then((response) => {
                        $("#total-table>tbody").html(response.data);
                        swal.close();
                    }).catch((error)=>{
                        Swal.fire({ icon: 'error', text: defaultError});
                    });
                    swal.close();
                });
            });
            const changeData=function(e,async_status=true){
                let uri=e.attr("data-change").split("#"),
                    target=e.closest('form').find("[name='"+e.attr("data-target-name")+"']");
                    target.appendData("{{url('')}}/"+uri[1],{kd_kecamatan:e.val()},{obj:'-- Semua Kelurahan --',key:'-'},async_status,true);
                return target;  
            };
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this);
                changeData(e);
            });
            let eForm=$("#form-cek");
            asyncData("{{ url('kompensasi_lebih_bayar/total') }}"+"?"+toString(eForm.serializeArray())).then((response) => {
                $("#total-table>tbody").html(response.data);
                swal.close();
            }).catch((error)=>{
                Swal.fire({ icon: 'error', text: defaultError});
            });
            $(document).on("click", "[data-verifikasi]", function (evt) {
                let e=$(this),
                id=e.attr('data-verifikasi');
                Swal.fire({ 
                    icon: 'warning',
                    text: 'Verifikasi Kompensasi Bayar?',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses Verifikasi Kompensasi Bayar',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        asyncData("{{ url('kompensasi_lebih_bayar/verifikasi') }}",{nomor_klb:id}).then((response) => {
                            resLoad();
                            if(!response.status){
                                return Swal.fire({ icon: 'error', html: response.msg}); 
                            }
                            Swal.close()
                        }).catch((error)=>{
                            Swal.fire({ icon: 'error', text: defaultError});
                        });
                    }
                });
            })
        });
    </script>
@endsection