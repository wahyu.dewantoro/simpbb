
<form action="{{url('kompensasi_lebih_bayar/store')}}" enctype="multipart/form-data" method="post" class="form-horizontal" data-loader="Proses Kompensasi Bayar" id="form-kompensasi">
    @csrf
    <div class="row">
        <div class="col-md-4 pr-1">
            <div class="card mb-1">
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-4 col-form-label col-form-label-sm mb-0">NOP*</label>
                                <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm" data-nop='nop' aria-expanded="false">
                                        <i class='fas fa-search'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1">
                        <b>Subjek</b>
                    </p>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NIK </span>
                                </div>
                                <input type="text" name="nik_wp" class="form-control-sm form-control " placeholder="NIK WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nama </span>
                                </div>
                                <input type="text" name="nama_wp" class="form-control  form-control-sm" placeholder="Nama WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Alamat WP</span>
                                </div>
                                <input type="text" name="alamat_wp" class="form-control form-control-sm" placeholder="Alamat WP" readonly>
                            </div>
                        </div>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1">
                        <b>Objek</b>
                    </p>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Alamat</span>
                                </div>
                                <input type="text" name="nop_alamat" class="form-control form-control-sm" placeholder="Alamat Objek" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1 pr-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RT</span>
                                </div>
                                <input type="text" name="nop_rt" maxlength="3" class="form-control form-control-sm numeric" placeholder="RT Objek" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1 pl-0">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RW</span>
                                </div>
                                <input type="text" name="nop_rw"  maxlength="2" class="form-control form-control-sm numeric" placeholder="RW Objek" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kelurahan</span>
                                </div>
                                <input type="text" name="nop_kelurahan"  maxlength="2" class="form-control form-control-sm" placeholder="Kelurahan" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kecamatan</span>
                                </div>
                                <input type="text" name="nop_kecamatan"  maxlength="2" class="form-control form-control-sm" placeholder="Kecamatan" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1 pr-1" >
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Luas Tanah</span>
                                </div>
                                <input type="text" name="luas_bumi"  maxlength="10" class="form-control form-control-sm numeric" placeholder="Luas Tanah" required readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1 pl-0" >
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Luas Bangunan</span>
                                </div>
                                <input type="text" name="luas_bng" class="form-control form-control-sm numeric"  maxlength="10" placeholder="Luas Bangunan" required readonly>
                            </div>
                        </div>
                    </div>
                    
                </div>  
            </div>
        </div>  
        <div class="col-md-8 pl-1">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"><b>Riwayat Pembayaran</b></h3>
                </div>
                <div class="card-body py-2 px-3">
                    <div class="row">
                        <div class="col-sm-12  body-scroll">
                            <table class="table table-bordered table-striped table-sm">
                                <thead class='text-center'>
                                    <tr>
                                        <!-- <th>NOP</th>
                                        <th>Nama WP</th> -->
                                        <th>---</th>
                                        <th>Tahun</th>
                                        <th>PBB Bayar</th>
                                        <th>PBB</th>
                                        <th>Selisih</th>
                                        <th>Tanggal Bayar</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody data-name="riwayat_pembayaran" class="tag-html">
                                    <tr class="null">
                                        <td colspan="9" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
            <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Proses Kompensasi Bayar</button> </div>
        </div>
    </div>
</form>