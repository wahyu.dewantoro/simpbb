<html>
<head>
   @include('layouts.style_pdf')
</head>
<body >
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">
        KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH <br> 
        <small>973/{{$data['no_sk']}}/LB.PBB/{{$data['nop_kelurahan']}}/{{$data['tahunpajak']}}</small><br><br>
        TENTANG<br> KOMPENSASI LEBIH BAYAR PAJAK BUMI DAN BANGUNAN <br>PERDESAAN DAN PERKOTAAN (PBB P2)<br>
    </h4>
    <br>
    <br>
    <div>
        <div style='width:19%;float:left'>Menimbang</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Bahwa dengan berlakunya SK Bupati Malang Nomor: 188.45/333/KEP/35.07.013/2022 tentang Pengurangan Pokok Ketetapan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan Tahun 2022, maka terhadap pokok ketetapan PBB P2 tahun 2022 diberikan pengurangan.</div>
    </div> 
    <div> 
        <br>
        <br>
        <br>
    </div>
    <div>
        <div style='width:19%;float:left'>Mengingat</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>
            <ol style='margin:0px; padding:0px 20px;'>
                <li>Undang-undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan Pemerintah Daerah.</li>
                <li>Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010 tentang Pajak Daerah.</li>
                <li>Peraturan Bupati Malang Nomor 43 Tahun 2013 Tentang Tata Cara Pengajuan dan Penyelesaian Pengurangan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.</li>
                <li>Surat Keputusan Bupati Malang Nomor : 188.45/333/KEP/35.07.013/2022 tentang Pengurangan Pokok Ketetapan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan Tahun 2022.</li>
            </ol>    
        </div>
    </div>
    <div style='width:100%;float:left'> 
        <br>
    </div>
    <div>
        <div style='width:19%;float:left'>Memperhatikan</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Surat Setoran Pajak Daerah (SSPD) PBB P2 Tahun 2022.</div>
    </div> 
    <div style='width:100%;float:left'> 
        <br>
        <h4 class="text-tengah"> MEMUTUSKAN </h4>
    </div> 
    <div style='width:100%;float:left'> 
        <div style='width:19%;float:left'>KESATU</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>
            Kelebihan pembayaran atas PBB P2 tahun 2022 dengan rincian :
            <table style='width:100%;float:left'> 
                <tbody>
                    <tr>
                        <td>a</td>
                        <td>NOP</td>
                        <td>:</td>
                        <td>{{$data['nop']}}</td>
                    </tr>
                    <tr>
                        <td>b</td>
                        <td>Nama Wajib Pajak</td>
                        <td>:</td>
                        <td>{{$data['nama_wp']}}</td>
                    </tr>
                    <tr>
                        <td>c</td>
                        <td>Alamat Wajib Pajak</td>
                        <td>:</td>
                        <td>{{$data['alamat_wp']}}</td>
                    </tr>
                    <tr>
                        <td>d</td>
                        <td>Alamat Objek Pajak</td>
                        <td>:</td>
                        <td>{{$data['alamat_op']}}</td>
                    </tr>
                    <tr>
                        <td>e</td>
                        <td>Tanggal Pembayaran PBB P2</td>
                        <td>:</td>
                        <td>{{tglIndo($data['tgl_bayar'])}}</td>
                    </tr>
                    <tr>
                        <td>f</td>
                        <td>Pembayaran PBB P2 Tahun 2022</td>
                        <td>:</td>
                        <td>Rp. {{$data['pbb_bayar']}}</td>
                    </tr>
                    <tr>
                        <td>g</td>
                        <td>PBB P2 yang seharusnya dibayar</td>
                        <td>:</td>
                        <td>Rp. {{$data['pbb_asli']}}</td>
                    </tr>
                    <tr>
                        <td>h</td>
                        <td>Selisih Lebih Bayar PBB P2 Tahun 2022</td>
                        <td>:</td>
                        <td>Rp. {{$data['selisih']}}</td>
                    </tr>
                </tbody>
            </table>  
        </div>
    </div>
    <div style='width:100%;float:left'> 
        <br>
    </div>
    <div style='width:100%;float:left'> 
        <div style='width:19%;float:left'>KEDUA</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Jumlah kelebihan bayar sebagaimana tersebut pada diktum kesatu, akan dikompensasikan untuk PBB P2 terhutang pada tahun berikutnya. </div>
    </div>
    <div style='width:100%;float:left'> 
        <br>
    </div>
    <div style='width:100%;float:left'> 
        <div style='width:19%;float:left'>KETIGA</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Keputusan Kepala Badan Pendapatan Daerah ini mulai berlaku pada tanggal  {{ $data['tanggalsk'] }} .</div>
    </div>
    <div style='width:100%;float:left'> 
        <br>
        <br>
    </div>
   <div>
        <div style='width:60%;float:left'><br></div>
        <div style='width:40%;float:left'>
            <p>
                Ditetapkan di :  <b>Kepanjen</b> <br>
                Pada Tanggal :   {{ $data['tanggalsk'] }}  <br>
                Kepala Badan Pendapatan Daerah,
                Kabupaten Malang
            </p>
            <p>{!! QrCode::size(70)->errorCorrection('M')->generate($url) !!}</p>
            <p>
                {{ $kaban->nama }} <br>
                Pembina Utama Muda <br>
                NIP. {{ $kaban->nip }}
            </p>
        </div>
    </div>

</body>
</html>
