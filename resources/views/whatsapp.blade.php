@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <iframe src="http://192.168.1.205:8000" height="400px"></iframe>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
    }
</script>
@endsection
