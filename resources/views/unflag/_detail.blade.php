<br>
<table class="table table-sm table-borderless" id="table-data">
    <thead class="bg-gray-dark">
        <tr>
            <th width="5%" style="text-align: center">No</th>
            <th style="text-align: center">NOP</th>
            <th style="text-align: center">Merchant</th>
            <th style="text-align: center">Tanggal</th>
            <th style="text-align: center">Total</th>
            <th style="text-align: center">Pengesahan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody >
        @php
            $no = 1;
        @endphp
        @if (count($pembayaran) > 0)
            @foreach ($pembayaran as $index => $row)
                <tr class="bg-info color-palette">
                    <td width="5px" align="center">{{ $no }}</td>
                    <td style="text-align: center">{{ formatNop($row->nop) }}</td>
                    <td class="text-center">{{ $row->merchant }}</td>
                    <td class="text-center">{{ tglIndo($row->datetime) }}
                        {{ date('H:i:s', strtotime($row->datetime)) }}</td>
                    <td class="text-right">{{ number_format($row->totalbayar, 0, ',', '.') }}</td>
                    <td class="text-center">{{ $row->kodepengesahan }}</td>
                    <td class="text-center">
                        <a class="text-danger" href="{{ route('informasi.unflag.edit',$row->kodepengesahan) }}" onclick="return confirm('Apakah anda yakin ?')"><i class="fas fa-trash"></i> </a>
                    </td>
                </tr>
                @if (count($row->tahun) > 0)
                <tr class="">
                        <td></td>
                        <td colspan="6">

                            <table width="100%" style="border: none;">
                                <thead> 
                                    <thead>
                                        <tr>
                                            <th colspan="2">Detail Pembayaran</th>
                                        </tr>
                                    </thead>
                                </thead>
                                <tbody>
                                    @php
                                        $ur = 1;
                                    @endphp
                                    @foreach ($row->tahun as $item)
                                        <tr>
                                            <td width="2px" align="center">{{ $ur }}.</td>
                                            <td>
                                                <b>{{ formatNop($item->nop) }} - {{ $item->tahun_pajak }} </b><br>
                                                Pokok: {{ number_format($item->pokok,0,',','.') }} ,
                                                Denda: {{ number_format($item->denda,0,',','.') }}, 
                                                Total : {{ number_format($item->total,0,',','.') }}
                                            </td>
                                        </tr>
                                        @php
                                            $ur++;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>


                        </td>
                    </tr>
                @endif
                @php
                    $no++;
                @endphp
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="7">Tidak ada data</td>
            </tr>
        @endif
    </tbody>
</table>
{{-- @section('script')
    <script>
        $(document).ready(function() {
            $('#table-data').DataTable();
        });
    </script>
@endsection --}}
