@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Flag / Unflag</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-left">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-tabs">
                    <div class="card-header p-0 pt-1">
                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link   @if($tab=='flag')  active @endif" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Flag</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if($tab=='unflag')  active @endif" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Unflag</a>
                            </li>

                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-one-tabContent">
                            <div class="tab-pane fade @if($tab=='flag') show active @endif " id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                <form action="{{ url('realisasi/unflag') }}" method="post" id="form-unflag">
                                    @method('post')
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="hidden" name="tipe" value='1'>
                                                <label for="kd_bank">Tempat Pembayaran</label>
                                                <select name="kd_bank" id="kd_bank" class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @foreach ($akunws as $row)
                                                    <option value="{{ $row->username.'|'.$row->password_md5 }}">{{ $row->username }}</option>
                                                    @endforeach
                                               
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="tanggal_bayar">Tanggal</label>
                                                <input type="text" name="tanggal_bayar" id="tanggal_bayar" class="form-control form-control-sm tanggal" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="nop">NOP</label>
                                                <input type="text" name="nop" id="nop" class="form-control form-control-sm" reuired>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="thn_pajak">Tahun Pajak</label>
                                                <input type="text" name="thn_pajak" id="thn_pajak" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="pokok">Pokok</label>
                                                <input type="text" name="pokok" id="pokok" class="form-control form-control-sm" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="denda">Denda</label>
                                                <input type="text" name="denda" id="denda" class="form-control form-control-sm" readonly>
                                            </div>
                                            <div class="float-right">
                                                <button type="submit" id="submitflag" disabled class="btn btn-sm btn-info"><i class="far fa-save"></i> Flag</button>
                                            </div>

                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade @if($tab=='unflag') show active @endif " id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                <form action="{{ url('realisasi/unflag') }}" method="post" id="form-flag">
                                    @method('post')
                                    @csrf
                                    <input type="hidden" name="tipe" value="0">
                                    
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label for="kd_bank">Tempat Pembayaran</label>
                                                <select name="kd_bank" id="kd_bank" class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @foreach ($akunws as $row)
                                                    <option value="{{ $row->username.'|'.$row->password_md5 }}">{{ $row->username }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                                    </div>
                                                </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="nop">NOP</label>
                                                <input type="text" name="nop" id="nop_unflag" class="form-control form-control-sm" reuired>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="thn_pajak">Tahun Pajak</label>
                                                <input type="text" name="thn_pajak" id="thn_pajak_unflag" class="form-control form-control-sm" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                      
                                            <div class="float-right">
                                                <button type="submit" id="submitunflag"   class="btn btn-sm btn-info"><i class="far fa-save"></i> Unflag</button>
                                            </div>

                                        </div>

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

@endsection
@section('script')

<script>
    $(document).ready(function() {
        $('#nop,#nop_unflag').trigger('keyup');

        $('#nop,#nop_unflag').on('keyup', function() {
            var nop = $(this).val();
            var convert = formatnop(nop);
            $(this).val(convert);
        });

        function inquiry(nop, tahun, kd_bank,tanggal_bayar) {
            openloading()
            $('#submitflag').prop('disabled', true)
            $.ajax({
                url: "{{ url('realisasi/unflag') }}"
                , data: {
                    jenis: 'inquiry'
                    , nop: nop
                    , tahun: tahun
                    , kd_bank: kd_bank,
                    tanggal_bayar:tanggal_bayar
                }
                , success: function(res) {
                    console.log(res)
                    closeloading()
                    if (res.data == 1 && res.status == 1) {
                        $('#submitflag').prop('disabled', false)
                    } else {
                        $('#submitflag').prop('disabled', true)
                    }
                    $('#pokok').val(res.pokok)
                    $('#denda').val(res.denda)
                }
                , error: function(e) {
                    closeloading()
                    console.log(e);
                }
            });
        }

        function reversal(nop, tahun) {
            openloading()
            $('#submitunflag').prop('disabled', true)
            $.ajax({
                url: "{{ url('realisasi/unflag') }}"
                , data: {
                    jenis: 'reversal'
                    , nop: nop
                    , tahun: tahun
                }
                , success: function(res) {
                    closeloading()
                    if (res.data == 1 && res.status == 1) {
                        $('#submitunflag').prop('disabled', false)
                    } else {
                        $('#submitunflag').prop('disabled', true)
                    }
                    $('#pokok_unflag').val(res.pokok)
                    $('#denda_unflag').val(res.denda)
                }
                , error: function(e) {
                    closeloading()
                    console.log(e);
                }
            });
        }

       /*  $('#thn_pajak_unflag,#nop_unflag').on('keyup', function(e) {
            var nop = $('#nop_unflag').val()
            var tahun = $('#thn_pajak_unflag').val()
            var kd_bank = $('#kd_bank').val()
            if (nop.length == 24 && tahun.length == 4) {
                reversal(nop, tahun)
            } else {
                $('#pokok_unflag').val(0)
                $('#denda_unflag').val(0)
            }

        }) */

        $('#thn_pajak,#nop,#tanggal_bayar').on('keyup change', function(e) {
            var nop = $('#nop').val()
            var tahun = $('#thn_pajak').val()
            var kd_bank = $('#kd_bank').val()
            var tanggal_bayar=$('#tanggal_bayar').val()
            tanggal_bayar
            if (nop.length == 24 && tahun.length == 4) {
                inquiry(nop, tahun, kd_bank,tanggal_bayar)
            } else {
                $('#pokok').val(0)
                $('#denda').val(0)
            }
        })

        /* $('#cek').on('click', function(e) {
            e.preventDefault();
            Swal.fire({
                title: 'Sedang mencari ...'
                , html: '<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>'
                , showConfirmButton: false
                , allowOutsideClick: false
            , });

        }); */
    });

</script>
@endsection
