<form class="form-tarif" role="form" action="{{ $url }}" >
@csrf   
<div class="modal fade" id="modal-tarif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-l" role="document">
    <div class="modal-content">
      <div class="modal-header p-1">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-swatchbook"></i> Tambah PBB Minimal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-1">
        <div class="row m-0 col-md-12">
            <div class="form-group col-md-6 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">Propinsi</label>
                    <input type="text" name="kd_propinsi" class="form-control-sm form-control" placeholder="Propinsi" value="35" readonly required>
                </div>
            </div>
            <div class="form-group col-md-6 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">Dati2</label>
                    <input type="text" name="kd_dati2" class="form-control-sm form-control" placeholder="Dati2" value="07" readonly required>
                </div>
            </div>
            <div class="form-group col-md-12 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">Tahun PBB Minimal</label>
                    <select name="thn_pbb_minimal" class="form-control-sm form-control" data-placeholder="Pilih Tahun Pajak" required >
                        <option value="">-- Pilih Tahun Pajak --</option>
                        @foreach ($listYear as $y)
                            <option value="{{$y}}"> {{$y}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">NO SK</label>
                    <input type="text" name="no_sk_pbb_minimal" class="form-control-sm form-control" placeholder="NO SK" required maxlength="30">
                </div>
            </div>
            <div class="form-group col-md-12 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">TGL SK</label>
                    <input type="text" name="tgl_sk_pbb_minimal" class="form-control-sm form-control tgl_sk" placeholder="YYYY-MM-DD">
                </div>
            </div>
            <div class="form-group col-md-12 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">NILAI PBB</label>
                    <input type="text" name="nilai_pbb_minimal" class="form-control-sm form-control numeric" placeholder="NJOP Max" required value="0" maxlength="8">
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer p-1">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
      </div>
    </div>
  </div>
</div>
</form>