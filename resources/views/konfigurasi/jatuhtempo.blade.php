@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tanggal Jatuh Tempo SPPT</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class='col-md-12'>
                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill"
                                        href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home"
                                        aria-selected="true">By NOP</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill"
                                        href="#custom-tabs-three-profile" role="tab"
                                        aria-controls="custom-tabs-three-profile" aria-selected="false">Upload</a>
                                </li>

                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel"
                                    aria-labelledby="custom-tabs-three-home-tab">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form action="{{ route('refrensi.jatuh.tempo-proses') }}" method="post">
                                                @csrf
                                                @method('post')
                                                <div class="form-group">
                                                    <label for="NOP">NOP</label>
                                                    <input required type="text" name="nop" id="nop"
                                                        autofocus="true" class="form-control  form-control-sm"
                                                        placeholder="Masukan nomor objek pajak (NOP)">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tahun Pajak</label>
                                                    {{-- <input value="{{ date('Y') }}" type="text" name="thn_pajak_sppt" id="thn_pajak_sppt" class="form-control form-control-sm"> --}}
                                                    <select name="thn_pajak_sppt[]" id="thn_pajak_sppt"
                                                        class="form-control select2" required multiple>
                                                        @for ($thn = date('Y'); $thn >= 2003; $thn--)
                                                            <option value="{{ $thn }}">{{ $thn }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jatuh Tempo</label>
                                                    <input value="" type="text" name="tg_jatuh_tempo_sppt"
                                                        id="tg_jatuh_tempo_sppt"
                                                        class="form-control form-control-sm tanggal">
                                                </div>

                                                <div class="float-sm-right">
                                                    <button class="btn btn-primary"><i class="fas fa-save"></i>
                                                        Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"
                                    aria-labelledby="custom-tabs-three-profile-tab">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <form id="form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="">Tanggal Jatuh Tempo</label>
                                                    <input type="text" name="tanggal" id="tanggal"
                                                        class="form-control form-control-sm tanggal">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">File Excel</label>
                                                    <input type="file" name="file" id="file"
                                                        class="form-control form-control-sm ">
                                                </div>
                                                <div class="float-sm-right">
                                                    <a href="{{ url('template_jatuh_tempo.xlsx') }}"
                                                        class="btn btn-sm btn-success"><i class="fa fa-download"></i>
                                                        Template</a>

                                                    <button id="preview" type="button" class="btn btn-sm btn-info"><i
                                                            class="fas fa-book-reader"></i>
                                                        Preview</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-9">
                                            <div id="hasil_preview"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).on({
            ajaxStart: function() {
                openloading();
            },
            ajaxStop: function() {
                closeloading();
            }
        });

        $(document).ready(function() {

            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    // $('#cek').trigger('click');
                }
            });

            $('#nop').trigger('keyup');

            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });


            $("#preview").click(function() {
                $('#hasil_preview').html("");
                const fileupload = $('#file').prop('files')[0];

                let formData = new FormData();
                formData.append('file', fileupload);
                formData.append('tanggal', $('#tanggal').val());
                formData.append('_token', '{{ csrf_token() }}');

                $.ajax({
                    type: 'POST',
                    url: "{{ route('refrensi.jatuh.tempo-preview') }}",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(res) {
                        $('#hasil_preview').html(res);
                    },
                    error: function() {
                        $('#hasil_preview').html("");
                        alert("Data Gagal Diupload");
                    }
                });
           });
        });
    </script>
@endsection
