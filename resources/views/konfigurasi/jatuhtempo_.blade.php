<style>
    .body-scroll {
        max-height: 370px;
        overflow-y: scroll;
        padding: 0px;
        scrollbar-width: thin;
        /* "auto" or "thin" */
        scrollbar-color: blue red;
        /* scroll thumb and track */
    }
</style>

<form id="form-penetapan" method="post" action="{{ url('refrensi/jatuhtempo-masal') }}">
    @csrf
    <div class="card card-outline card-primary">
        <div class="card-header">
            Preview Data
        </div>
        <div class="card-body p-0 body-scroll">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NOP</th>
                        <th>Tahun</th>
                        <th>Tanggal Jatuh Tempo</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $count = 0;
                        $kd_propinsi = '';
                        $kd_dati2 = '';
                        $kd_kecamatan = '';
                        $kd_kelurahan = '';
                        $kd_blok = '';
                        $no_urut = '';
                        $kd_jns_op = '';
                        $thn_pajak_sppt = '';
                        
                        $tgl_jatuh_tempo_sppt = '';
                        
                    @endphp
                    @foreach ($data as $a => $ra)
                        @if ($ra['status'] == '1')
                            @php
                                $count += 1;
                                
                                $kd_propinsi .= $ra['kd_propinsi'] . '|';
                                $kd_dati2 .= $ra['kd_dati2'] . '|';
                                $kd_kecamatan .= $ra['kd_kecamatan'] . '|';
                                $kd_kelurahan .= $ra['kd_kelurahan'] . '|';
                                $kd_blok .= $ra['kd_blok'] . '|';
                                $no_urut .= $ra['no_urut'] . '|';
                                $kd_jns_op .= $ra['kd_jns_op'] . '|';
                                $thn_pajak_sppt .= $ra['thn_pajak_sppt'] . '|';
                                $tgl_jatuh_tempo_sppt .= $ra['tgl_jatuh_tempo_sppt'] . '|';
                                
                            @endphp
                        @endif
                        <tr>
                            <td class="text-center">{{ $a + 1 }}</td>
                            <td>
                                {{ $ra['kd_propinsi'] }}.{{ $ra['kd_dati2'] }}.{{ $ra['kd_kecamatan'] }}.{{ $ra['kd_kelurahan'] }}.{{ $ra['kd_blok'] }}-{{ $ra['no_urut'] }}.{{ $ra['kd_jns_op'] }}
                            </td>
                            <td>
                                {{ $ra['thn_pajak_sppt'] }}
                            </td>
                            <td>
                                {{ tglIndo($ra['tgl_jatuh_tempo_sppt']) }}
                            </td>
                            <td>
                                <span class="{{ $ra['status'] == '1' ? 'text-success' : 'text-danger' }}">
                                    @if ($ra['status'] == '1')
                                        <i class="fas fa-check"></i>
                                    @else
                                        <i class="far fa-times-circle"></i>
                                    @endif {{ $ra['keterangan'] }}
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @php
                $kd_propinsi = substr($kd_propinsi, 0, -1);
                $kd_dati2 = substr($kd_dati2, 0, -1);
                $kd_kecamatan = substr($kd_kecamatan, 0, -1);
                $kd_kelurahan = substr($kd_kelurahan, 0, -1);
                $kd_blok = substr($kd_blok, 0, -1);
                $no_urut = substr($no_urut, 0, -1);
                $kd_jns_op = substr($kd_jns_op, 0, -1);
                $thn_pajak_sppt = substr($thn_pajak_sppt, 0, -1);
                $tgl_jatuh_tempo_sppt = substr($tgl_jatuh_tempo_sppt, 0, -1);
                
            @endphp

            <textarea style="display:none" name="kd_propinsi" id="kd_propinsi">{{ $kd_propinsi }}</textarea>
            <textarea style="display:none" name="kd_dati2" id="kd_dati2">{{ $kd_dati2 }}</textarea>
            <textarea style="display:none" name="kd_kecamatan" id="kd_kecamatan">{{ $kd_kecamatan }}</textarea>
            <textarea style="display:none" name="kd_kelurahan" id="kd_kelurahan">{{ $kd_kelurahan }}</textarea>
            <textarea style="display:none" name="kd_blok" id="kd_blok">{{ $kd_blok }}</textarea>
            <textarea style="display:none" name="no_urut" id="no_urut">{{ $no_urut }}</textarea>
            <textarea style="display:none" name="kd_jns_op" id="kd_jns_op">{{ $kd_jns_op }}</textarea>
            <textarea style="display:none" name="thn_pajak_sppt" id="thn_pajak_sppt">{{ $thn_pajak_sppt }}</textarea>
            <textarea style="display:none" name="tgl_jatuh_tempo_sppt" id="tgl_jatuh_tempo_sppt">{{ $tgl_jatuh_tempo_sppt }}</textarea>

        </div>

        <div class="card-footer">
            <div class="float-right">
                <div class="pull-right"><button id="prosesfim" type="submit" class="btn btn-sm btn-success"><i
                            class="fas fa-file-signature"></i> Proses</button></div>
            </div>
        </div>

    </div>
</form>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#form-penetapan").on("submit", function(event) {

            event.preventDefault();
            openloading()
            $.ajax({
                url: "{{ url('refrensi/jatuhtempo-masal') }}",
                type: 'post',
                data: $(this).serialize(),
                success: function(res) {
                    $('#hasil_preview').html('');
                    document.getElementById("form-data").reset();
                    if (res.status == true) {
                        toastr.success(res.msg);
                    } else {
                        toastr.error(res.msg);
                    }

                },
                error: function(res) {
                    $('#hasil_preview').html('');
                    toastr.error('Something went wrong!');
                }
            })
        });


        /*  $('#prosesfim').on('click', function(e) {
             e.preventDefault();
             
                        
             var kd_propinsi = $('#kd_propinsi').val();
             var kd_dati2 = $('#kd_dati2').val();
             var kd_kecamatan = $('#kd_kecamatan').val();
             var kd_kelurahan = $('#kd_kelurahan').val();
             var kd_blok = $('#kd_blok').val();
             var no_urut = $('#no_urut').val();
             var kd_jns_op = $('#kd_jns_op').val();
             var thn_pajak_sppt = $('#thn_pajak_sppt').val();
             
             var tgl_jatuh_tempo_sppt = $('#tgl_jatuh_tempo_sppt').val();

             $.ajax({
                 url: "{{ url('refrensi/jatuhtempo-masal') }}"
                 , type: 'POST'
                 , data: {
                     , kd_propinsi: kd_propinsi
                     , kd_dati2: kd_dati2
                     , kd_kecamatan: kd_kecamatan
                     , kd_kelurahan: kd_kelurahan
                     , kd_blok: kd_blok
                     , no_urut: no_urut
                     , kd_jns_op: kd_jns_op
                     , thn_pajak_sppt: thn_pajak_sppt
                     , tgl_jatuh_tempo_sppt: tgl_jatuh_tempo_sppt
                 }
                 , success: function(res) {
                     $('#hasil_preview').html('');

                     document.getElementById("form-data").reset();

                     if (res.status == true) {
                         toastr.success(res.msg);
                     } else {
                         toastr.error(res.msg);
                     }
                 }
                 , error: function() {
                     $('#hasil_preview').html('');
                     toastr.error('Something went wrong!');
                 }
             })
         }); */
    });
</script>
