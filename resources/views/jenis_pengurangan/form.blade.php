@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body p-2">
                            <form action="{{ $data['action'] }}" method="post">
                                @csrf
                                @method($data['method'])
                                <div class="form-group">
                                    <label for="">Nama Pengurangan</label>
                                    <input type="text" value="{{ $data['objek']->nama_pengurangan ?? '' }}"
                                        name="nama_pengurangan" id="nama_pengurangan" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Jenis</label>
                                    <select name="jenis"  required class="form-control">
                                        @php 
                                        $listJenis=[
                                            ['key'=>'0','value'=>'Selesai depan'],
                                            ['key'=>'1','value'=>'Penelitian']
                                        ];
                                        @endphp
                                        @foreach ($listJenis as $val)
                                            @php $selected="";@endphp
                                                @if(isset($data['objek']->jenis))
                                                @if ($data['objek']->jenis == $val['key'])  
                                                    @php $selected="selected";@endphp
                                                @endif
                                            @endif
                                            <option value="{{ $val['key'] }}" {{$selected}}>
                                                {{ $val['value'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Pengurangan</label>
                                    <input type="text" value="{{ $data['objek']->pengurangan ?? '' }}"
                                        name="pengurangan" id="pengurangan" class="form-control" required>
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-float btn-info"><i class="fas fa-save"></i> Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
