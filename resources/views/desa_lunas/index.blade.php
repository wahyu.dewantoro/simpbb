@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Desa Lunas</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    @if($is_kelurahan==false && $is_kecamatan==false)
                    <a href="{{ route('realisasi.desalunas.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-file"></i> Tambah
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h3 class="card-title">Tahun Pajak {{ date('Y') }}</h3> --}}
                        <div class="card-tools">



                            <div class="input-group input-group-sm">
                                <select name="thn_pajak" id="thn_pajak" class="form-control form-control-sm">
                                    @php
                                    $sel= $data['lunas']->tahun_pajak ?? date('Y');
                                    @endphp
                                    @for($i = date('Y'); $i >=2014; $i--)
                                    <option @if($i==$sel) selected @endif value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                                <span class="input-group-append">
                                    {{-- <button type="button" class="btn btn-info btn-flat">Go!</button> --}}
                                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                        Export
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-item"><a id="cetak_pdf" href="#">PDF</a></li>
                                        <li class="dropdown-item"><a id="cetak_excel" href="#">Excel</a></li>

                                    </ul>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-sm table-bordered" id="table-data">
                            <thead>
                                <tr>
                                    <th style="text-align: center">No</th>
                                    <th style="text-align: center">Desa/Kelurahan</th>
                                    <th style="text-align: center">Kecamatan</th>
                                    <th style="text-align: center">Ketetapan PBB</th>
                                    <th style="text-align: center">Tanggal</th>
                                    @if($is_kelurahan==false && $is_kecamatan==false)
                                    <th style="text-align: center">Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {
        let table = $('#table-data').DataTable({
            processing: true
            , serverSide: true
                // , ajax: "{!! url('realisasi/desalunas') !!}"
            , ajax: {
                url: "{!! url('realisasi/desalunas') !!}"
                , data: function(d) {
                    d.thn_pajak = $('#thn_pajak').val()
                }
            }

            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                }
                , {
                    data: 'nm_kelurahan'
                    , name: 'nm_kelurahan'
                }
                , {
                    data: 'nm_kecamatan'
                    , name: 'nm_kecamatan'
                }
                , {
                    data: 'baku'
                    , name: 'baku'
                    , class: 'text-right'
                }
                , {
                    data: 'lunas'
                    , name: 'lunas'
                    , class: 'text-center'
                }
                , @if($is_kelurahan == false && $is_kecamatan == false) {
                    data: 'aksi'
                    , name: 'aksi'
                    , orderable: false
                    , class: 'text-center'

                }
                @endif
            ]
        });

        $('#thn_pajak').on('change', function(e) {
            e.preventDefault()
            table.draw();
        })


        $('#cetak_pdf').on("click", function(e) {
            e.preventDefault();
            var url = "{{ url('realisasi/desalunas-pdf') }}?thn_pajak=" + $('#thn_pajak').val()
            window.open(
                url, '_blank' // <- This is what makes it open in a new window.
            );
        })
        $('#cetak_excel').on("click", function(e) {
            e.preventDefault();
            var url = "{{ url('realisasi/desalunas-excel') }}?thn_pajak=" + $('#thn_pajak').val()
            window.open(
                url, '_blank' // <- This is what makes it open in a new window.
            );
        })

        // {{-- <a href="{{ url('realisasi/desalunas-pdf') }}" class="btn btn-warning btn-sm"> <i class="far fa-file-pdf"></i> PDF</a>
        //                     <a href="{{ url('realisasi/desalunas-excel') }}" class="btn btn-success btn-sm"> <i class="far fa-file-pdf"></i> Excel</a> --}}
    });

</script>
@endsection
