@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">

        <h1>{{ $data['title'] }}</h1>

    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body p-1">
                        <form action="{{ $data['action'] }}" method="post">
                            @csrf
                            @method($data['method'])
                            <div class="form-group">
                                <label for="">Tahun Pajak</label>
                                {{-- <input type="text" name="tahun_pajak" class="form-control" readonly
                                        value="{{ $data['lunas']->tahun_pajak ?? date('Y') }}"> --}}
                                <select name="tahun_pajak" id="tahun_pajak" class="form-control " required>
                                    <option value="">Pilih</option>
                                    @php
                                    $sel= $data['lunas']->tahun_pajak ?? date('Y');
                                    @endphp
                                    @for($i = date('Y'); $i >=2014; $i--)
                                    <option @if($i==$sel) selected @endif value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="">Kecamatan</label>
                                @php
                                $selected=$data['lunas']->kd_kecamatan??'';
                                @endphp
                                <select name="kd_kecamatan" id="kd_kecamatan" class="form-control select2" date-placeholder="Pilih" required>
                                    <option value="">-- pilih --</option>

                                    @foreach ($kecamatan as $rk)

                                    <option @if($selected==$rk->kd_kecamatan) selected @endif value="{{ $rk->kd_kecamatan }}">{{ $rk->kd_kecamatan }} {{ $rk->nm_kecamatan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Desa</label>
                                @php
                                $selected=$data['lunas']->kd_kelurahan??'';
                                @endphp
                                <select name="kd_kelurahan" id="kd_kelurahan" class="form-control" date-placeholder="Pilih" required>
                                    <option value="">-- Desa --</option>
                                    @if ($kelurahan != '-')
                                    @foreach ($kelurahan as $rk)
                                    <option @if($selected==$rk->kd_kelurahan) selected @endif value="{{ $rk->kd_kelurahan }}">{{ $rk->kd_kelurahan }} {{ $rk->nm_kelurahan }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <input type="hidden" id="temp_kd_kelurahan" value="<?= $selected ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Lunas</label>
                                <input type="text" name="tanggal_lunas" id="tanggal_lunas" class="form-control tanggal" required value="{{ isset($data['lunas']->tanggal_lunas) ? date('d M Y', strtotime($data['lunas']->tanggal_lunas)) : date('d F Y') }}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).ready(function() {

        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            getKelurahan(kk);
        })

        var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
        if (kd_kecamatan == '') {
            var kd_kecamatan = $('#kd_kecamatan').val()
        }

        var temp_kode_kel = $('#temp_kd_kelurahan').val();

        if (temp_kode_kel == '') {
            getKelurahan(kd_kecamatan);
        }


        function getKelurahan(kk) {
            var html = '<option value="">-- pilih --</option>';
            if (kk != '') {

                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        $.each(res, function(k, v) {
                            // console.log(k)

                            html += '<option value="' + k + '">' + v + '</option>';
                        });
                        // console.log(res);

                        $('#kd_kelurahan').html(html);
                        $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                    }
                    , error: function(res) {
                        $('#kd_kelurahan').html(html);
                    }
                });
            } else {
                $('#kd_kelurahan').html(html);
            }

        }
    });

</script>
@endsection
