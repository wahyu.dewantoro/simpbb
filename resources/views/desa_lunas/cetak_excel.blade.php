<html>


<body>


    <table class="table table-sm table-bordered" id="table-data">
        <thead>
            <tr>
                <th>No</th>
                <th>Desa/Kelurahan</th>
                <th>Kecamatan</th>
                <th>Ketetapan PBB</th>
                <th>Tanggal</th>

            </tr>
        </thead>
        <tbody>
            @php
            $no=1;
            @endphp
            @foreach ($data as $index => $row)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $row->nm_kelurahan }}</td>
                <td>{{ $row->nm_kecamatan }}</td>
                <td>
                    {{ angka(bakuDesa($row->kd_kecamatan, $row->kd_kelurahan))}}
                </td>
                <td>{{ tglIndo($row->tanggal_lunas) }}</td>

            </tr>
            @php
            $no++;
            @endphp
            @endforeach
        </tbody>
    </table>
</body>

</html>
