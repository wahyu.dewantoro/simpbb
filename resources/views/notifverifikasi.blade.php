@extends('layouts.app')
@section('style')
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                <div class="card card-warning">
                    {{-- <div class="card-header">
                        <h3 class="card-title">Akun dalam Proses Verifikasi</h3>
                    </div> --}}
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="alert alert-warning alert-dismissible">
                            <h5><i class="icon fas fa-exclamation-triangle"></i> Verifikasi Akun</h5>
                            Akun Anda saat ini sedang dalam proses verifikasi kelengkapan dokumen yang telah
                            diunggah. Tim kami akan segera melakukan pengecekan atas dokumen yang Anda unggah.
                            <br><br>
                            <strong>Harap tunggu beberapa saat hingga proses verifikasi selesai.</strong>
                            <p class="mt-2">
                                Jika ada pertanyaan atau dokumen tambahan yang diperlukan, tim kami akan menghubungi
                                Anda melalui informasi kontak yang terdaftar.
                            </p>
                        </div>
                        <div class="text-center mt-4">
                            {{-- <a href="{{ url('/') }}" class="btn btn-primary">
                                <i class="fas fa-home"></i> Kembali ke Beranda
                            </a> --}}
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')
@endsection
