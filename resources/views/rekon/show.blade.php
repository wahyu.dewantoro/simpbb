<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title">Detail Rekon Tanggal {{ tglIndo($tanggal) }}</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-1">
        <table class="table table-sm table-hover table-bordered">
            <thead class="bg-info">
                <tr>
                    <th style="text-align: center">No</th>
                    <th style="text-align: center">NOP</th>
                    <th style="text-align: center">Tahun</th>
                    <th style="text-align: center">Flag / Unflag</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($history as $n => $rk)
                    <tr>
                        <td class="text-center">{{ $n + 1 }}</td>
                        <td>
                            {{ formatnop($rk->kd_propinsi . $rk->kd_dati2 . $rk->kd_kecamatan . $rk->kd_kelurahan . $rk->kd_blok . $rk->no_urut . $rk->kd_jns_op) }}
                        </td>
                        <td class="text-center">{{ $rk->thn_pajak_sppt }}</td>
                        <td class="text-center">{{ $rk->flag == 1 ? 'Flag' : 'Unflag' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
