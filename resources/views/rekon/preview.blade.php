<style>
    .body-scroll {
        max-height: 370px;
        overflow-y: scroll;
        padding: 0px;
        scrollbar-width: thin;
        /* "auto" or "thin" */
        scrollbar-color: blue red;
        /* scroll thumb and track */
    }
</style>

<form id="form-rekon">
    @csrf
    <input type="hidden" id="kode_bank" name='kode_bank' value="{{ $kode_tp }}">
    <input type="hidden" id="tanggal_batch" name="tanggal_batch" value="{{ $postdate }}">
    <div class="card card-outline card-primary">
        <div class="card-header">
            Preview Data
        </div>
        <div class="card-body p-0 body-scroll">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NOP</th>
                        <th>Tahun</th>
                        <th>Pokok</th>
                        <th>Denda</th>

                        <th>Nominal</th>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $count = 0;
                        $kd_propinsi = '';
                        $kd_dati2 = '';
                        $kd_kecamatan = '';
                        $kd_kelurahan = '';
                        $kd_blok = '';
                        $no_urut = '';
                        $kd_jns_op = '';
                        $thn_pajak_sppt = '';
                        $pokok = '';
                        $denda = '';
                        $jumlah = '';
                        $tanggal_pembayaran = '';

                    @endphp
                    @foreach ($data as $a => $ra)
                        @if ($ra['status'] == '1')
                            @php
                                $count += 1;

                                $kd_propinsi .= $ra['kd_propinsi'] . '|';
                                $kd_dati2 .= $ra['kd_dati2'] . '|';
                                $kd_kecamatan .= $ra['kd_kecamatan'] . '|';
                                $kd_kelurahan .= $ra['kd_kelurahan'] . '|';
                                $kd_blok .= $ra['kd_blok'] . '|';
                                $no_urut .= $ra['no_urut'] . '|';
                                $kd_jns_op .= $ra['kd_jns_op'] . '|';
                                $thn_pajak_sppt .= $ra['thn_pajak_sppt'] . '|';
                                $pokok .= $ra['pokok'] . '|';
                                $denda .= $ra['denda'] . '|';
                                $jumlah .= $ra['jumlah'] . '|';
                                $tanggal_pembayaran .= $ra['tanggal_pembayaran'] . '|';
                            @endphp
                        @endif
                        <tr>
                            <td class="text-center">{{ $a + 1 }}</td>
                            <td>
                                {{ $ra['kd_propinsi'] }}.{{ $ra['kd_dati2'] }}.{{ $ra['kd_kecamatan'] }}.{{ $ra['kd_kelurahan'] }}.{{ $ra['kd_blok'] }}-{{ $ra['no_urut'] }}.{{ $ra['kd_jns_op'] }}
                            </td>
                            <td>
                                {{ $ra['thn_pajak_sppt'] }}
                            </td>
                            <td class="text-right">
                                {{ angka($ra['pokok']) }}
                            </td>
                            <td class="text-right">
                                {{ angka($ra['denda']) }}
                            </td>
                            <td class="text-right">
                                {{ angka($ra['jumlah']) }}
                            </td>
                            <td>
                                {{ tglIndo($ra['tanggal_pembayaran']) }}
                            </td>
                            <td>
                                <span class="{{ $ra['status'] == '1' ? 'text-success' : 'text-danger' }}">
                                    @if ($ra['status'] == '1')
                                        <i class="fas fa-check"></i>
                                    @else
                                        <i class="far fa-times-circle"></i>
                                    @endif {{ $ra['keterangan'] }}
                                </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @php
                $kd_propinsi = substr($kd_propinsi, 0, -1);
                $kd_dati2 = substr($kd_dati2, 0, -1);
                $kd_kecamatan = substr($kd_kecamatan, 0, -1);
                $kd_kelurahan = substr($kd_kelurahan, 0, -1);
                $kd_blok = substr($kd_blok, 0, -1);
                $no_urut = substr($no_urut, 0, -1);
                $kd_jns_op = substr($kd_jns_op, 0, -1);
                $thn_pajak_sppt = substr($thn_pajak_sppt, 0, -1);
                $pokok = substr($pokok, 0, -1);
                $denda = substr($denda, 0, -1);
                $jumlah = substr($jumlah, 0, -1);
                $tanggal_pembayaran = substr($tanggal_pembayaran, 0, -1);

            @endphp

            <textarea style="display:none" name="kd_propinsi" id="kd_propinsi">{{ $kd_propinsi }}</textarea>
            <textarea style="display:none" name="kd_dati2" id="kd_dati2">{{ $kd_dati2 }}</textarea>
            <textarea style="display:none" name="kd_kecamatan" id="kd_kecamatan">{{ $kd_kecamatan }}</textarea>
            <textarea style="display:none" name="kd_kelurahan" id="kd_kelurahan">{{ $kd_kelurahan }}</textarea>
            <textarea style="display:none" name="kd_blok" id="kd_blok">{{ $kd_blok }}</textarea>
            <textarea style="display:none" name="no_urut" id="no_urut">{{ $no_urut }}</textarea>
            <textarea style="display:none" name="kd_jns_op" id="kd_jns_op">{{ $kd_jns_op }}</textarea>
            <textarea style="display:none" name="thn_pajak_sppt" id="thn_pajak_sppt">{{ $thn_pajak_sppt }}</textarea>
            <textarea style="display:none" name="pokok" id="pokok">{{ $pokok }}</textarea>
            <textarea style="display:none" name="denda" id="denda">{{ $denda }}</textarea>
            <textarea style="display:none" name="jumlah" id="jumlah">{{ $jumlah }}</textarea>

            <textarea style="display:none" name="tanggal_pembayaran" id="tanggal_pembayaran">{{ $tanggal_pembayaran }}</textarea>

        </div>
        @if ($count > 0)
            <div class="card-footer">
                <div class="float-right">
                    <div class="pull-right"><button id="rekon" class="btn btn-sm btn-success"><i
                                class="fas fa-file-signature"></i> Proses Rekon</button></div>
                </div>
            </div>
        @endif
    </div>
</form>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#rekon').on('click', function(e) {
            e.preventDefault();
            var kode_bank = $('#kode_bank').val();
            var tanggal_batch = $('#tanggal_batch').val();
            var kd_propinsi = $('#kd_propinsi').val();
            var kd_dati2 = $('#kd_dati2').val();
            var kd_kecamatan = $('#kd_kecamatan').val();
            var kd_kelurahan = $('#kd_kelurahan').val();
            var kd_blok = $('#kd_blok').val();
            var no_urut = $('#no_urut').val();
            var kd_jns_op = $('#kd_jns_op').val();
            var thn_pajak_sppt = $('#thn_pajak_sppt').val();
            var pokok = $('#pokok').val();
            var denda = $('#denda').val();
            var jumlah = $('#jumlah').val();
            var tanggal_pembayaran = $('#tanggal_pembayaran').val();

            $.ajax({
                url: "{{ url('realisasi/rekon') }}",
                type: 'post',
                data: {
                    kode_bank: kode_bank,
                    tanggal_batch: tanggal_batch,
                    kd_propinsi: kd_propinsi,
                    kd_dati2: kd_dati2,
                    kd_kecamatan: kd_kecamatan,
                    kd_kelurahan: kd_kelurahan,
                    kd_blok: kd_blok,
                    no_urut: no_urut,
                    kd_jns_op: kd_jns_op,
                    thn_pajak_sppt: thn_pajak_sppt,
                    pokok: pokok,
                    denda: denda,
                    jumlah: jumlah,
                    tanggal_pembayaran: tanggal_pembayaran
                },
                success: function(res) {
                    $('#hasil_preview').html('');
                    document.getElementById("form-data").reset();
                    if (res.status == true) {
                        toastr.success(res.msg);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function() {
                    $('#hasil_preview').html('');
                    toastr.error('Something went wrong!');
                }
            })
        });
    });
</script>
