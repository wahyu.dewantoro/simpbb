@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Rekonsiliasi</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Riwayat Rekon</h3>
                        <div class="card-tools">
                            <a href="{{ url('realisasi/rekon/create') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-folder-plus"></i> Tambah
                            </a>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <table class="table table-sm table-striped table-bordered">
                            <thead class="bg-info">
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Pembayaran</th>
                                    <th>Tempat Bayar</th>
                                    <th>NOP</th>
                                    <th>flag</th>
                                    <th>unflag</th>
                                    <th>Upload</th>
                                    <th>Oleh</th>
                                </tr>
                            </thead>
                            <thead>
                                @foreach ($batch as $i => $item)
                                <tr>
                                    <td class="text-center">{{ $i + 1 }}</td>
                                    <td>{{ tglIndo($item->tanggal_pembayaran) }}</td>
                                    <td class="text-center">{{ $item->kode_bank }}</td>
                                    <td class="text-center">
                                        {{ $item->Nop()->count()}}
                                    </td>
                                    <td class="text-center">
                                        {{ $item->proses()->where('flag',1)->count()}}
                                    </td>
                                    <td class="text-center">
                                        {{ $item->proses()->where('flag',0)->count()}}
                                    </td>
                                    <td>{{ tglIndo($item->created_at) }}, {{ date('H:i:s',strtotime($item->created_at)) }}</td>
                                    <td >{{ $item->user->nama }}</td>
                                </tr>
                                @endforeach
                            </thead>
                        </table>
                    </div>

                    <div class="card-footer p-2">
                        <div class="text-center">
                            {{ $batch->appends(['cari' => request()->get('cari')]) }}
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
@endsection
@section('script')
<script>
    $(document).on({
        ajaxStart: function() {
            openloading();
        }
        , ajaxStop: function() {
            closeloading();
        }
    });
    $(document).ready(function() {



        $('.detail').on('click', function() {
            $('#preview').html('');
            var tanggal = $(this).data('tanggal');
            $.ajax({
                url: "{{ url('realisasi/rekon') }}/" + tanggal
                , type: 'get'
                , success: function(res) {
                    // alert(res)
                    $('#preview').html(res);
                }
                , error: function(res) {
                    $('#preview').html('');
                }
            })
        })

        /* $("button").click(function() {
            $("p").slideToggle();
        }); */
    });

</script>
@endsection
