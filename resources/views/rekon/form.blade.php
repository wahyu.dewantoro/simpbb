@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Upload Rekonsiliasi</h1>
            </div>
            <div class="col-sm-6">
                {{-- <div class="float-sm-right">
                        <a href="{{ url('realisasi/rekon/create') }}" class="btn btn-primary btn-sm">
                <i class="fas fa-folder-plus"></i> Tambah
                </a>
            </div> --}}
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-md-3">
                        <form id="form-data">
                            @csrf
                            <div class="form-group">
                                <label for="">Tempat Pembayaran</label>
                                <select name="kode_tp" id="kode_tp" class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    @foreach ($tp as $tp)
                                    <option value="{{ $tp->kode_bank }}">{{ $tp->nama_bank }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Pembayaran</label>
                                <input type="text" name="tanggal" id="tanggal" class="form-control form-control-sm tanggal">
                            </div>
                            <div class="form-group">
                                <label for="">File Excel</label>
                                <input type="file" name="file" id="file" class="form-control form-control-sm ">
                            </div>
                            <div class="float-sm-right">
                                <a href="{{ url('templaterekon_baru.xlsx') }}" class="btn btn-sm btn-success"><i class="fa fa-download"></i> Template Rekon</a>

                                <button id="preview" type="button" class="btn btn-sm btn-info"><i class="fas fa-book-reader"></i>
                                    Preview</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-9">
                        <div id="hasil_preview"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    /*  */

    $(document).ready(function() {
        $(document).on({
            ajaxStart: function() {
                openloading();
            }
            , ajaxStop: function() {
                closeloading();
            }
        });

        $("#preview").click(function() {
            $('#hasil_preview').html("");
            const fileupload = $('#file').prop('files')[0];

            let formData = new FormData();
            formData.append('file', fileupload);
            formData.append('kode_tp', $('#kode_tp').val());
            formData.append('tanggal', $('#tanggal').val());
            formData.append('_token', '{{ csrf_token() }}');

            $.ajax({
                type: 'POST'
                , url: "{{ url('realisasi/rekon-upload') }}"
                , data: formData
                , cache: false
                , processData: false
                , contentType: false
                , success: function(res) {
                    $('#hasil_preview').html(res);
                }
                , error: function() {
                    $('#hasil_preview').html("");
                    alert("Data Gagal Diupload");
                }
            });
        });



    });

</script>
@endsection
