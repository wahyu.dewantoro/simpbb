<div class="table-responsive">
    <table class="table table-bordered table-sm">
        <thead class="bg-info">
            <tr>
                <th style="text-align: center">No</th>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">NIR</th>
                <th style="text-align: center">Kelas</th>
                <th style="text-align: center">NJOP/m<sup>2</sup></th>
            </tr>
        </thead>
        <tbody>
            @php
            $no=1;
            @endphp
            @foreach ($data as $item)
            <tr>
                <td align="center">{{ $no }}</td>
                <td align="center">{{ $item->thn_nir_znt }}</td>
                <td align="right">{{ angka($item->nir) }}</td>
                <td align="center">{{ $item->kd_kls_tanah }}</td>
                <td align="right">{{ angka($item->nilai_per_m2_tanah) }}</td>
            </tr>
            @php
            $no++;
            @endphp
            @endforeach
        </tbody>
    </table>
</div>
