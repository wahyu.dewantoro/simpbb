<html>

<head>
    @include('layouts.style_pdf')
    <style>
        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            background-color: grey;
            color: white;
            text-align: center;
            line-height: 0.5cm;
        }

        /* body {
   font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;
   font-size: 10px;
   line-height: 1.42857143;
   color: #333;
   background-color: #fff;
  } */

        .inti {
            border: 1px solid #C6C6C6;
            margin: auto;
            font-size: 0.9em;
        }

        .inti td {
            border-right: 1px solid #C6C6C6;
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
        }

        .inti th {
            border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;
        }

        #watermark {
            position: fixed;
            top: 50mm;
            width: 100%;
            height: 200px;
            opacity: 0.05;
            text-align: center;
            vertical-align: middle
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 11pt !important
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 11pt !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }
    </style>
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">
        DATA NIR
    </h4>
    <br>
    <table class="table table-sm table-bordered table-hover">
        <thead>
            <tr>
                <th style="text-align: center; vertical-align: middle">No</th>
                <th style="text-align: center; vertical-align: middle">Kecamatan</th>
                <th style="text-align: center; vertical-align: middle">Kelurahan</th>
                <th style="text-align: center; vertical-align: middle">ZNT</th>
                <th style="text-align: center; vertical-align: middle">Lokasi</th>
                <th style="text-align: center; vertical-align: middle">Jum OP</th>
                <th style="text-align: center; vertical-align: middle">Tahun</th>
                <th style="text-align: center; vertical-align: middle">Nir</th>
                <th style="text-align: center; vertical-align: middle">HKPD</th>
                <th style="text-align: center; vertical-align: middle">Creted</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dataArray as $item)
                <tr>
                    <td style="text-align:center">{{ $item['no'] }}</td>
                    <td>{{ $item['nm_kecamatan'] }}</td>
                    <td>{{ $item['nm_kelurahan'] }}</td>
                    <td>{{ $item['kd_znt'] }}</td>
                    <td>{{ $item['lokasi'] }}</td>
                    <td style="text-align: center">{{ angka($item['jum_op']) }}</td>
                    <td style="text-align: center">{{ $item['tahun'] }}</td>
                    <td style="text-align: right">{{ angka($item['nir']) }}</td>
                    <td style="text-align:center">{{ angka($item['hkpd']) }} %</td>
                    <td style="text-align: center"><?= $item['created'] ?></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
