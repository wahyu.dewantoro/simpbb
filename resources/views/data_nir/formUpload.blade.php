@extends('layouts.app')
@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css"> --}}
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Upload NIR</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-sm btn-success" href="{{ route('refrensi.data_nir.index') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-body p-1">
                            {{-- <form action="{{ route('refrensi.data_nir.upload-preview') }}" method="POST"
                                enctype="multipart/form-data" id="form-upload">
                                @csrf
                                @method('post') --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Tahun</label>
                                        <select class="loadnir form-control form-control-sm" name="tahun" id="tahun">
                                            <option value="">Pilih</option>
                                            @for ($tahun = date('Y') + 1; $tahun >= 2003; $tahun--)
                                                <option value="{{ $tahun }}">{{ $tahun }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Kecamatan</label>
                                        <select class="form-control form-control-sm loadnir" name="kd_kecamatan" required
                                            id="kd_kecamatan" required>
                                            <option value="">-- Pilih --</option>
                                            @foreach ($kecamatan as $rowkec)
                                                <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                    value="{{ $rowkec->kd_kecamatan }}">
                                                    {{ $rowkec->kd_kecamatan }} - {{ $rowkec->nm_kecamatan }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Kelurahan</label>
                                        <select class="form-control form-control-sm loadnir" name="kd_kelurahan"
                                            id="kd_kelurahan" required>
                                            <option value="">-- Kelurahan / Desa -- </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">File Template</label>
                                        <input type="file" id="file" name="file"
                                            class="form-control form-control-sm" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="float-left">

                                            <button type="button" id="template" class="btn btn-success btn-flat"><i
                                                    class="far fa-file-alt"></i> Template</button>
                                        </div>

                                        <div class="float-right">
                                            <button type="button" id="preview" class="btn btn-primary btn-flat"><i
                                                    class="fas fa-save"></i> Preview</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div id="hasil_preview"> </div>
                                </div>
                            </div>
                            {{-- </form> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('script')
    <script>
        $(document).on("keypress", function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });


        $(document).ready(function() {
            // konten_nir
            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });

            $("#preview").click(function() {
                $('#hasil_preview').html("");
                const fileupload = $('#file').prop('files')[0];

                let formData = new FormData()
                formData.append('file', fileupload)
                formData.append('_token', '{{ csrf_token() }}')
                formData.append('kd_kecamatan', $('#kd_kecamatan').val())
                formData.append('kd_kelurahan', $('#kd_kelurahan').val())
                formData.append('tahun', $('#tahun').val())
                // formData.append('kode_tp', $('#kode_tp').val());

                $.ajax({
                    type: 'POST',
                    url: "{{ route('refrensi.data_nir.upload-preview') }}",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(res) {
                        $('#hasil_preview').html(res);
                    },
                    error: function() {
                        $('#hasil_preview').html("");
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan !',
                            text: "Gagal upload data",

                        })
                    }
                });
            });


            /* function loadNir() {
                            let tahun = $('#tahun').val()
                            let kd_kecamatan = $('#kd_kecamatan').val()
                            let kd_kelurahan = $('#kd_kelurahan').val()
                            $('#konten_nir').html('');
                            if (tahun != '' && kd_kecamatan != '' && kd_kelurahan != '') {

                                $.ajax({
                                    url: "{{ route('refrensi.data_nir.create') }}",
                                    data: {
                                        tahun,
                                        kd_kecamatan,
                                        kd_kelurahan
                                    },
                                    success: function(res) {
                                        $('#konten_nir').html(res);
                                    },
                                    error: function(er) {
                                        $('#konten_nir').html('');
                                    }
                                })
                            }

                        }

                        $('.loadnir').on('change', function() {
                            loadNir()
                        })
             */
            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })


            getKelurahan($('#kd_kecamatan').val())

            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                $('#kd_kelurahan').html(html);
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }

            $('#template').on("click", function(e) {
                e.preventDefault()
                let kd_kecamatan = $('#kd_kecamatan').val()
                let kd_kelurahan = $('#kd_kelurahan').val()

                if (kd_kecamatan != '' && kd_kelurahan != "") {
                    let url = "{{ url('refrensi/data_nir/template-import') }}?kd_kecamatan=" +
                        kd_kecamatan + "&kd_kelurahan=" + kd_kelurahan
                    window.open(url);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Peringatan !',
                        text: "Kecamatan dan Desa/kelurahan tidak boleh kosong",

                    })
                }

            })
        });
    </script>
@endsection
