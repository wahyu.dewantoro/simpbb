@extends('layouts.app')
@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css"> --}}
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data NIR</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-sm btn-success" href="{{ route('refrensi.data_nir.create') }}"><i
                                class="fas fa-plus-square"></i> Tambah </a>
                        <a class="btn btn-sm btn-info" href="{{ route('refrensi.data_nir.upload') }}"><i
                                class="fas fa-upload"></i> Upload</a>

                        <a href="#" class="btn-export btn btn-sm btn-primary"><i class="fas fa-file-alt"></i>
                            Unduh</a>
                       
 
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-body p-1">
                            <form action="#data_nir_search" id="form-cek">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <div class="input-group input-group-sm">
                                            <select id="kecamatan" name="kecamatan"
                                                class="form-control-sm form-control select-bottom pencariandata"
                                                data-placeholder="Pilih Kecamatan" data-change="#desa"
                                                data-target-name='kelurahan'>
                                                @if (count($kecamatan) > 1)
                                                    <option value=""></option>
                                                @endif
                                                @foreach ($kecamatan as $rowkec)
                                                    <option value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->kd_kecamatan }} - {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group input-group-sm">
                                            <select name="kelurahan" id="kelurahan"
                                                class="form-control-sm form-control select-bottom select-reset change-next pencariandata"
                                                data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                                                @if (count($kelurahan) > 1)
                                                    <option value=""></option>
                                                @endif
                                                @foreach ($kelurahan as $rowkel)
                                                    <option value="{{ $rowkel->kd_kelurahan }}">
                                                        {{ $rowkel->kd_kelurahan }} - {{ $rowkel->nm_kelurahan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <div class="input-group input-group-sm">
                                            <select class="pencariandata form-control form-control-sm" name="tahun"
                                                id="tahun">
                                                @for ($tahun = date('Y') + 1; $tahun >= 2003; $tahun--)
                                                    <option value="{{ $tahun }}">{{ $tahun }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            <div class="table-responsive">
                                <table id="main-table" class="table table-bordered table-striped table-sm"
                                    data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Desa / Kecamatan</th>
                                            <th>ZNT / Lokasi</th>
                                            <th>TAHUN</th>
                                            <th>NIR</th>
                                            <th>HKPD</th>
                                            <th>Objek</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('data_nir/form', [
            'url' => url('refrensi/data_nir_create'),
            'listYear' => $listYear,
            'listYearFrom' => $listYearFrom,
        ])
    </section>
    <style>
        .dataTables_filter,
        .dataTables_length,
        .dataTables_info {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError = "Proses tidak berhasil.";
            let txtNull = 'Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull = "<tr class='null'><td colspan='9'>" + txtNull + "</td></tr>";
            async function asyncData(uri, value) {
                let getData;
                try {
                    getData = await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value) ? {
                            "_token": "{{ csrf_token() }}",
                            'id': value
                        } : {},
                        dataType: "json",
                    });
                    return getData;
                } catch (error) {
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none' ?? 'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable = $("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('refrensi/data_nir_search') }}",
                    method: 'GET',
                    data: function(d) {
                        d.tahun = $('#tahun').val();
                        d.kecamatan = $('#kecamatan').val();
                        d.kelurahan = $('#kelurahan').val();
                    }
                },
                lengthMenu: [20, 40, 60, 80, 100],
                ordering: false,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false
                    },
                    // { data: 'kode'},
                    {
                        data: 'nm_kecamatan',
                        render: function(data, type, row) {
                            return row.nm_kelurahan + "<br><span class='text-success'>" + data +
                                "</span>"
                        }
                    },

                    {
                        data: 'kd_znt',
                        render: function(data, type, row) {
                            return data + '<br><span class="text-info">' + row.nama_lokasi +
                                '</span>'
                        }
                    },
                    {
                        data: 'thn_nir_znt'
                    },
                    // {
                    //     data: 'no_dokumen'
                    // },
                    {
                        data: 'nir'
                    },
                    {
                        data: 'nilai_hkpd'
                    },
                    {
                        data: 'jum_op'
                    } ,
                    {
                        data: 'created'
                    }, 
                ]
            });
            datatable.on('error.dt', (e, settings, techNote, message) => {
                Swal.fire({
                    icon: 'warning',
                    html: defaultError
                });
            });
            /* let timer=60000;
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            setInterval( function () {
                resLoad();
            }, timer ); */

            $(document).on("submit", ".form-nir", function(evt) {
                evt.preventDefault();
                let e = $(this);
                Swal.fire({
                    title: 'Proses Salin Data NIR',
                    html: '<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function(response) {
                        if (!response.status) {
                            return Swal.fire({
                                icon: 'warning',
                                html: response.msg
                            });
                        } else {
                            Swal.close();
                            resLoad();
                        }
                    },
                    error: function(x, y) {
                        Swal.fire({
                            icon: 'error',
                            text: defaultError
                        });
                    }
                });
            })
            const toString = function(obj) {
                let str = [];
                obj.map((key) => {
                    if (key.name != '_token') {
                        str.push(key.name + "=" + key.value);
                    }
                });
                return str.join("&");
            }
            $(document).on("click", ".btn-export", function(evt) {
                evt.preventDefault()
                // let e = $(this),
                // form = e.closest('form'),

                let tahun = $('#tahun').val();
                let kecamatan = $('#kecamatan').val();
                let kelurahan = $('#kelurahan').val();
                let url = "{{ url('refrensi/data_nir_cetak') }}?tahun=" + tahun + "&kecamatan=" +
                    kecamatan + "&kelurahan=" + kelurahan;
                window.open(url, '_blank');
            });
            $(document).on("click", "[data-modal-target]", function(evt) {
                let e = $(this);
                $(e.attr('data-modal-target')).modal({
                    keyboard: false,
                    backdrop: 'static',
                    show: true
                });
            });

            function pencarian() {
                datatable.draw();
            }

            $(document).on('change keyup', '.pencariandata', function() {
                pencarian();
            });
            const changeData = function(e, async_status = true) {
                let uri = e.attr("data-change").split("#"),
                    target = e.closest('form').find("[name='" + e.attr("data-target-name") + "']");
                target.appendData("{{ url('') }}/" + uri[1], {
                    kd_kecamatan: e.val()
                }, async_status, true);
                return target;
            };
            $(document).on("change", "[data-change]", function(evt) {
                let e = $(this);
                changeData(e);
            });
        });
    </script>
@endsection
