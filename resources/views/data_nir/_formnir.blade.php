<div class="form-group">
    <label for="">NIR ZNT</label>
    <table class="table table-borderless table-hover">
        <thead>
            <tr>
                <th width="300px">ZNT</th>
                <th>Nilai NIR</th>
                <th width="100px">HKPD (%)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datanir as $item)
                <tr>
                    <td>{{ $item->kd_znt }}
                        <span class="text-info"><?= $item->nama_lokasi!=""?"<br>".$item->nama_lokasi:"" ;?></span>
                    </td>
                    <td><input type="text" required name="nir[{{ $item->kd_znt }}]" value="{{ $item->nilai_nir }}"
                            class="form-control form-control-sm angka" @if($is_sistep==true) readonly @endif> </td>
                    <td><input type="text" required name="nilai_hkpd[{{ $item->kd_znt }}]"
                            value="{{ $item->nilai_hkpd }}" class="form-control form-control-sm angka" @if($is_sistep==true) readonly @endif> </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <button class="btn btn-sm btn-info btn-flat float-right"> Submit</button>
</div>
<script>
    $(document).on("keypress",  function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });
</script>
