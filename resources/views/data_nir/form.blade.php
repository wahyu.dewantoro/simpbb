<form class="form-nir" role="form" action="{{ $url }}" >
@csrf   
<div class="modal fade" id="modal-nir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-l" role="document">
    <div class="modal-content">
      <div class="modal-header p-1">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-swatchbook"></i> Salin Data NIR</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-1">
        <div class="row m-0 col-md-12">
            <div class="form-group col-md-12 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">Tahun Pajak</label>
                    <select name="tahun_pajak" class="form-control-sm form-control" data-placeholder="Pilih Tahun Pajak" required >
                        <option value="">-- Pilih Tahun Pajak --</option>
                        @foreach ($listYear as $y)
                            <option value="{{$y}}"> {{$y}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12 m-0 pl-0">
                <div class='input-group'>
                    <label class="col-md-4 col-form-label">Salin dari</label>
                    <select name="dari_tahun" class="form-control-sm form-control" data-placeholder="Pilih Tahun Pajak" required >
                        <option value="">-- Pilih Tahun Pajak --</option>
                        @foreach ($listYearFrom as $yF)
                            <option value="{{$yF}}"> {{$yF}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer p-1">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
      </div>
    </div>
  </div>
</div>
</form>