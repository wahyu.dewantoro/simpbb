<div class="row">
    <div class="col-12">


        @if ($is_sistep == true)
            <div class="alert alert-danger alert-dismissible">
                <h5><i class="icon fas fa-exclamation-triangle"></i> Peringatan</h5>
                Objek masih sistep
            </div>
        @elseif ($kd_kecamatan == $array[0]['kd_kecamatan'] && $kd_kelurahan == $array[0]['kd_kelurahan'] && $is_sistep == false)
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('refrensi/data_nir') }}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $tahun }}" name="tahun">
                        <input type="hidden" value="{{ $kd_kecamatan }}" name="kd_kecamatan">
                        <input type="hidden" value="{{ $kd_kelurahan }}" name="kd_kelurahan">
                        <div class="form-group">
                            {{-- <label for="">NIR ZNT</label> --}}
                            <table class="table table-borderless table-sm text-sm table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th width="300px">ZNT</th>
                                        {{-- <th></th> --}}
                                        <th>Nilai NIR</th>
                                        <th width="100px">HKPD (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($array as $row)
                                        @if ($row['kd_znt'] != '')
                                            <tr>
                                                <td class="text-center">{{ $no }}</td>
                                                <td>{{ $row['kd_znt'] }}
                                                    <br> <span
                                                        class="text-danger">{{ getNamaLokasiZnt($row['kd_kecamatan'], $row['kd_kelurahan'], $row['kd_znt']) }}</span>

                                                </td>
                                                <td>
                                                    {{ $row['nir'] }}
                                                    <input type="hidden" required name="nir[{{ $row['kd_znt'] }}]"
                                                        value="{{ $row['nir'] }}"
                                                        class="form-control form-control-sm angka" readonly>
                                                </td>
                                                <td>{{ $row['hkpd'] }} % <input type="hidden" required
                                                        name="nilai_hkpd[{{ $row['kd_znt'] }}]"
                                                        value="{{ $row['hkpd'] }}"
                                                        class="form-control form-control-sm angka" readonly> </td>
                                            </tr>

                                            @php
                                                $no++;
                                            @endphp
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <button class="btn btn-sm btn-info btn-flat float-right"> <i class="fas fa-save"></i>
                                Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        @else
            <div class="alert alert-danger alert-dismissible">
                <h5><i class="icon fas fa-exclamation-triangle"></i> Peringatan</h5>
                Data yang tidak di upload tidak sesuai.
            </div>
        @endif


    </div>
</div>
