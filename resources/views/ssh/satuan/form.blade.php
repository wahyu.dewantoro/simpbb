@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Resource</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-2">
                        <form action="{{ route('ssh.harga-resource.store') }}" method="POST">
                            @csrf
                            @method('post')
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group row">
                                        <label for="tahun" class="col-sm-6 col-form-label">Tahun</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control angka" name="tahun" id="tahun" value="{{ date('Y') }}" placeholder="Tahun" maxlength="4" autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div id="konten"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {
        // $('#tahun').trigger('change')

        loaddata($('#tahun').val())

        function loaddata(thn) {
            if (thn.length == 4) {
                openloading()
                $.ajax({
                    url: "{{ route('ssh.harga-resource.create') }}"
                    , data: {
                        'tahun': thn
                    }
                    , success: function(res) {
                        closeloading()
                        $('#konten').html(res)
                    }
                })
            } else {
                $('#konten').html('')
            }
        }


        $('#tahun').on('keyup change', function(e) {
            thn = $('#tahun').val()
            loaddata(thn)
        })
    });

</script>
@endsection
