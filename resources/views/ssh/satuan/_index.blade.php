<div class=" table-responsive-sm">
    <table class="table table-condensed table-sm table-sm table-hover ">
        <thead class="bg-info">
            <tr>
                <th class="text-center">Pekerjaan</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-right">Harga</th>
            </tr>
        </thead>
        <tbody>
            @php
            $i=1;
            @endphp
            @foreach ($grouped as $key=> $item)
            <tr>
                <th colspan="3">{{ $i.'. '.$key }}</th>
            </tr>
            @php
            $ii=1;
            @endphp
            @foreach ($item as $list)

            <tr>
                <td width="15px"></td>
                <td>{{ $i.'.'.$ii }}. {{ $list->nm_kegiatan }} </td>
                <td class="text-right">{{ angka($list->hrg_satuan*1000) }}</td>
            </tr>
            @php
            $ii++;
            @endphp

            @endforeach
            @php
            $i++;
            @endphp
            @endforeach
        </tbody>
    </table>
</div>
