<div class=" table-responsive-sm">
    <table class="table table-condensed table-sm table-sm table-hover ">
        <thead class="bg-info">
            <tr>
                <th class="text-center" colspan="2">Resource</th>
                <th class="text-center">Satuan</th>
                <th class="text-center" colspan="2">Harga Thn {{ $tahun-1 }}</th>
                <th class="text-left">Harga {{ $tahun }}</th>
            </tr>
        </thead>
        <tbody>
            @php
            $i=1;
            @endphp
            @foreach ($grouped as $key=> $item)
            <tr>
                <th colspan="6">{{ $i.'. '.$key }}</th>
            </tr>
            @php
            $ii=1;
            @endphp
            @foreach ($item as $list)
            @php
            switch ($list->satuan_resource) {
            case 'M2':
            # code...
            $satuan="M<sup>2</sup>";
            break;
            case 'M3':
            # code...
            $satuan="M<sup>3</sup>";
            break;
            default:
            # code...
            $satuan=$list->satuan_resource;
            break;
            }
            @endphp
            <tr>
                <td width="15px"></td>
                <td>{{ $i.'.'.$ii }}. {{ $list->nm_resource }} </td>
                <td class="text-center"><?= $satuan ?></td>
                <td class="text-right">{{ angka($list->hrg_tahun_lalu*1000) }}</td>
                <td width="50px"></td>
                <td class="text-right">
                    <input type="hidden" name="kd_propinsi[]" value="35">
                    <input type="hidden" name="kd_kanwil[]" value="01">
                    <input type="hidden" name="kd_kantor[]" value="01">
                    <input type="hidden" name="kd_dati2[]" value="07">
                    <input type="hidden" name="no_dokumen[]" value="{{ date('Ymd') }}002">
                    <input type="hidden" name="thn_hrg_resource[]" value='{{ $tahun }}'>
                    <input type="hidden" name="kd_group_resource[]" value="{{ $list->kd_group_resource }}">
                    <input type="hidden" name="kd_resource[]" value="{{ $list->kd_resource }}">
                    @php
                    $hrg_skrg=$list->hrg_tahun_skrg<>''?angka($list->hrg_tahun_skrg*1000):'';
                        if($hrg_skrg=='' && $list->hrg_tahun_lalu!=''){
                        $hrg_skrg=$list->hrg_tahun_lalu<>''?angka($list->hrg_tahun_lalu*1000):'';
                            }
                            @endphp

                            <input type="text" name="hrg_resource[]" class="form-control form-control-sm angka rupiah" value="{{  $hrg_skrg }}">
                </td>
            </tr>
            @php
            $ii++;
            @endphp

            @endforeach
            @php
            $i++;
            @endphp
            @endforeach
        </tbody>
    </table>
</div>
<div class="float-right">
    <button class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i> Simpan</button>
    <a href="{{ route('ssh.harga-resource.index') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>
</div>
<script>
    $(function() {
        $(".rupiah").on("keyup", function() {
            var angka = $(this).val();
            var convert = formatRupiah(angka);
            $(this).val(convert);
        });
    })

</script>
