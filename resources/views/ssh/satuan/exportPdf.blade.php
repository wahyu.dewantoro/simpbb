<html>
<head>
    @include('layouts.style_pdf')
</head>
<body>
    @include('layouts.kop_pdf')
    <p class="text-tengah"><b>HARGA Kegiatan {{ $tahun }}</b></p>
    @include('ssh.satuan._index', compact('grouped'))
</body>
</html>
