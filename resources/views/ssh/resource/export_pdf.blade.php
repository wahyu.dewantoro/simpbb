<html>

<head>
    @include('layouts.style_pdf')
    <style>
        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            background-color: grey;
            color: white;
            text-align: center;
            line-height: 0.5cm;
        }

        /* body {
			font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace;
			font-size: 10px;
			line-height: 1.42857143;
			color: #333;
			background-color: #fff;
		} */

        .inti {
            border: 1px solid #C6C6C6;
            margin: auto;
            font-size: 0.9em;
        }

        .inti td {
            border-right: 1px solid #C6C6C6;
            padding-left: 3px;
            padding-right: 3px;
            padding-bottom: 1px;
        }

        .inti th {
            border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;
        }

        #watermark {
            position: fixed;
            top: 50mm;
            width: 100%;
            height: 200px;
            opacity: 0.05;
            text-align: center;
            vertical-align: middle
        }

    </style>
</head>

<body>

    <div id="watermark"><img src="{{ public_path('kabmalang.png') }}"></div>
    @include('layouts.kop_pdf')
    <p style="text-align: center"><strong>Harga Resource {{ $tahun }}</strong></p>
    <p><br>

        @include('ssh.resource._index',['grouped'=>$grouped])
        
</body>

</html>
