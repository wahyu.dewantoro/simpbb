<div class=" table-responsive-sm">
    <table class="table table-condensed table-sm table-sm table-hover ">
        <thead class="bg-info">
            <tr>
                <th class="text-center" colspan="2">RESOURCE</th>
                <th class="text-center">SATUAN</th>
                <th class="text-right">HARGA</th>
            </tr>
        </thead>
        <tbody>
            @php
            $i=1;
            @endphp
            @foreach ($grouped as $key=> $item)
            <tr>
                <th style="text-align: left" colspan="4">{{ $i.'. '.$key }}</th>
            </tr>
            @php
            $ii=1;
            @endphp
            @foreach ($item as $list)
            @php
            switch ($list->satuan_resource) {
            case 'M2':
            # code...
            $satuan="M<sup>2</sup>";
            break;
            case 'M3':
            # code...
            $satuan="M<sup>3</sup>";
            break;
            default:
            # code...
            $satuan=$list->satuan_resource;
            break;
            }
            @endphp
            <tr>
                <td width="15px"></td>
                <td>{{ $i.'.'.$ii }}. {{ $list->nm_resource }} </td>
                <td class="text-center"><?= $satuan ?></td>
                @php
                $is_excel=false;
                if(isset($excel)){
                $is_excel=true;
                }
                @endphp
                <td class="text-right">{{ $is_excel==true?$list->hrg_resource*1000: angka($list->hrg_resource*1000) }}</td>
            </tr>
            @php
            $ii++;
            @endphp

            @endforeach
            @php
            $i++;
            @endphp
            @endforeach
        </tbody>
    </table>
</div>
