@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Role & Permissions</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                            @can(' add_roles')
                            <a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-folder-plus"></i> Create
                            </a>
                            @endcan
                    </div>
                    
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                    <h3>Roles</h3>
                </div>
                <div class="col-md-7 page-action text-right">
                    {{-- @can('add_roles') --}}
                    <a href="#" class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#roleModal"> <i
                            class="glyphicon glyphicon-plus"></i> New</a>
                    {{-- @endcan --}}
                </div>
            </div>


            @forelse ($roles as $role)
                <form action="{{ url('roles', $role->id) }}" method="post" class="m-b">
                    @csrf
                    @method('patch')
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ $role->name . ' Permissions' }}</h3>
                        </div>
                        <div class="card-body">
                            {{-- @if ($role->name === 'Super User')
                                @include('shared._permissions'])
                            @else
                                @include('shared._permissions')
                                <button class="btn btn-primary" type="submit">Save</button>
                            @endif --}}
                            {{-- @include('shared._permissions') --}}
                            <div class="row">
                                @foreach ($permissions as $perm)
                                    <?php
                                    $per_found = null;
                                    if (isset($role)) {
                                        $per_found = $role->hasPermissionTo($perm->name);
                                    }
                                    if (isset($user)) {
                                        $per_found = $user->hasDirectPermission($perm->name);
                                    }
                                    ?>
                                    <div class="col-md-3">
                                        <div class="checkbox">
                                            <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">
                                                <input type="checkbox" name="permissions[]" {{ $per_found==true?"checked":"" }}
                                                    value="{{ $perm->name }}" id="">
                                                {{ $perm->name }}
                                                {{-- $perm->name --}}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>



                </form>



            @empty
                <p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
            @endforelse
 
        </div>
        </div>
    </section>
@endsection
