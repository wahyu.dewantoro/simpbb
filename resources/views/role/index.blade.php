@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Role & Permissions</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    @can(' add_roles')
                    <a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-folder-plus"></i> Create
                    </a>
                    @endcan
                </div>

            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data</h3>
                <div class="card-tools">
                    <form action="{{ url()->current() }}">
                        <div class="input-group input-group-sm" style="width: 250px;">
                            <input type="text" name="cari" class="form-control float-right" placeholder="Search" value="{{ request()->get('cari') }}">

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm" id="table">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Role</th>
                                <th>Menus</th>
                                @can('edit_roles', 'delete_roles')
                                <th>Actions</th>
                                @endcan

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $index => $row)
                            <tr>
                                <td align="center">{{ $index + $roles->firstItem() }}</td>
                                <td>{{ $row->name }}</td>
                                <td><?= 'Have <b>'.count($row->menu->pluck('id')->toarray()) .'</b> Menus' ?>
                                </td>
                                @can('edit_roles', 'delete_roles')
                                <td class="text-center">

                                    @can('edit_roles')
                                    <a href="{{ route('roles.edit', $row->id) }}"><i class="fas fa-edit text-info"></i> </a>
                                    @endcan
                                    @if ($row->name != 'Super User')
                                    @can('delete_roles')
                                    <a data-id="{{$row->id  }}" href="{{ url('roles', $row->id) }}" class='hapusRole'><i class="fas fa-trash-alt text-danger"></i>
                                    </a>
                                    <form method="POST" id="delete-form-{{ $row->id }}" action="{{ route('roles.destroy', [$row->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    @endcan
                                    @endif
                                    @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer p-0">
                <div class="float-sm-right">
                    {{ $roles->appends(['cari' => request()->get('cari')]) }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('.hapusRole').on('click', function(e) {
            id = $(this).data('id')
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form-' + id).submit()
                    // document.location.href = $(this).data('href');
                }
            })

        });
    })

</script>
@endsection
