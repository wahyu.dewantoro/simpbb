@extends('layouts.app')
@section('css')
<link href="{{ url('css/treeview.css') }}" rel="stylesheet">
@endsection
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $form->title }}</h1>
            </div>

        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <form action="{{ $form->action }}" method="post">
                    @csrf
                    @method($form->method)
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="role">Role</label>
                                <input type="text" name="role" id="role" @if(($form->role->name ?? '')=='Super User') readonly @endif
                                value="{{ old('role') ?? ($form->role->name ?? '') }}"
                                class="form-control form-control-sm {{ $errors->has('role') ? 'is-invalid' : '' }}">
                                <span class="errorr invalid-feedback">{{ $errors->first('role') }}</span>
                            </div>
                        </div>
                        {{-- <div class="col-md-12">
                            <label for="">Permissions :</label>
                            <span class="errorr invalid-feedback">@if ($errors->has('permissions.0')) Permissions harus pilih @endif</span>
                        </div> --}}
                        {{-- @php
                        $ar = [];
                        if (old('permissions')) {
                        $ar = old('permissions');
                        } else {
                        if (isset($form->role)) {
                        $ar =$form->role->permissions->pluck('name')->toarray();
                        }
                        } --}}
                        @endphp
                        {{-- @foreach ($permissions as $row)
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input @if (in_array($row->name, $ar)) checked @endif type="checkbox" name="permissions[]" value="{{ $row->name }}" id="">
                        {{ $row->name }}
                        </label>
                    </div>
            </div>
            @endforeach --}}
            <div class="col-md-8">
                {{-- <h5 class="text-center mb-4 bg-info text-white">Menu List</h5> --}}
                <label for="">Menu Aktif :</label>
                <span class="errorr invalid-feedback">@if ($errors->has('menu.0')) Menu aktif harus pilih @endif</span>
                <div id="divCheckAll">
                    {{-- Check All --}}
                    <div class="form-check">
                        <input type="checkbox" name="checkall" id="checkall" onClick="check_uncheck_checkbox(this.checked);" class="form-check-input" />
                        <label for="checkall">
                            Check All menu
                        </label>
                    </div>
                </div>
                <div id="divCheckboxList">
                    <ul id="tree1">
                        @php
                        $active=$form->active??[];
                        @endphp
                        @foreach($menus as $menu)
                        <li>
                            <div class="form-check">
                                <input @if( in_array($menu->id,$active)) checked @endif type="checkbox" name="menu[]" class="form-check-input parent" value="{{ $menu->id }}" id="{{ $menu->id }}" onchange="fnTest(this);">
                                <label for="{{ $menu->id }}">
                                    {{ $menu->title }}
                                </label>
                            </div>
                            @if(count($menu->childs))
                            @include('menu.manageChild',['childs' => $menu->childs,'active'=>$active])
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="float-sm-right">
            <button class="btn btn-primary btn-sm"> <i class="fas fa-save"></i> Simpan</button>
        </div>
        </form>
    </div>
    </div>
    </div>
</section>
@endsection
@section('script')
<script>
    function check_uncheck_checkbox(isChecked) {
        if (isChecked) {
            $('input[type="checkbox"]').each(function() {
                this.checked = true;
            });
        } else {
            $('input[type="checkbox"]').each(function() {
                this.checked = false;
            });
        }
    }

</script>
<script src="{{url('js/treeview.js')}}"></script>
@endsection
