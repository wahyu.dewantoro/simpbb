<html>

<head>

</head>

<body>
    Laporan Target dan Realisasi PBB P2 Tahun {{ $tahun }}
    <table id="table-data" class="table table-bordered table-sm">
        <thead>
            <tr style="vertical-align: middle">
                <th style="text-align: center" rowspan="3">Buku</th>
                <th style="text-align: center" rowspan="2" colspan="2">Ketetapan PBB</th>
                <th style="text-align: center" colspan="10">Realisasi</th>
            </tr>
            <tr style="vertical-align: middle">

                <th style="text-align: center" colspan="5"> Tahun Pajak {{ $tahun }}</th>
                <th style="text-align: center" colspan="5"> Tahun Pajak {{ $awal }} - {{ $akhir }}</th>
            </tr>
            <tr style="vertical-align: middle">
                <th style="text-align: center">Tahun Pajak {{ $tahun }}</th>
                <th style="text-align: center">Tahun Pajak {{ $awal }} - {{ $akhir }}</th>
                <th style="text-align: center">Pokok</th>
                <th style="text-align: center">Denda</th>
                <th style="text-align: center">Total</th>
                <th style="text-align: center">%</th>
                <th style="text-align: center">Sisa</th>
                <th style="text-align: center">Pokok</th>
                <th style="text-align: center">Denda</th>
                <th style="text-align: center">Total</th>
                <th style="text-align: center">%</th>
                <th style="text-align: center">Sisa</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($realisasi as $item)
                <tr>
                    {{-- <td>Buku {{ romawi($item->buku) }}</td> --}}
                    <td><?= strlen($item->buku)=='1'?'Buku '.romawi($item->buku):'<b>'.$item->buku.'</b>' ?></td>
                    <td style="text-align: right">{{ angka($item->baku_berjalan) }}</td>
                    <td style="text-align: right">{{ angka($item->baku_tunggakan) }}</td>
                    <td style="text-align: right">{{ angka($item->real_pokok_berjalan) }}</td>
                    <td style="text-align: right">{{ angka($item->real_denda_berjalan) }}</td>
                    <td style="text-align: right">{{ angka($item->real_jumlah_berjalan) }}</td>
                    <td style="text-align: right">
                        {{ $item->real_jumlah_berjalan > 0 ? angka(($item->real_jumlah_berjalan / $item->baku_berjalan) * 100, 2) : angka(0,2) }}
                    </td>
                    <td style="text-align: right">{{ angka($item->baku_berjalan - $item->real_jumlah_berjalan) }}
                    </td>
                    <td style="text-align: right">{{ angka($item->real_pokok_tunggakan) }}</td>
                    <td style="text-align: right">{{ angka($item->real_denda_tunggakan) }}</td>
                    <td style="text-align: right">{{ angka($item->real_jumlah_tunggakan) }}</td>
                    <td style="text-align: right">
                        {{ $item->baku_tunggakan > 0 ? angka(($item->real_jumlah_tunggakan / $item->baku_tunggakan) * 100, 2) : angka(0,2) }}
                    </td>
                    <td style="text-align: right">{{ angka($item->baku_tunggakan - $item->real_jumlah_tunggakan) }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
