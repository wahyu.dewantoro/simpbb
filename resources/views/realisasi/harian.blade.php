@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Realiasi Harian</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}" class="form-inline">

                                    <input type="text" name="tanggal" id="tanggal"
                                        class="form-control form-control-sm tanggal mb-2 mr-sm-2" required
                                        value="{{ $tanggal }}">
    
                                    {{-- <select class="form-control form-control-sm  mb-2 mr-sm-2" name="kd_kecamatan"
                                        id="kd_kecamatan">
                                        <option value="">-- Kecamatan --</option>
                                        @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <select class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan"
                                        id="kd_kelurahan">
                                        <option value="">-- Desa -- </option>
                                    </select> --}}
                                    <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i>
                                        Cari</button>
                                    {{-- @can('excel_realisasi_harian')
                                        <a href="{{ url('realisasi/harian-excel') }}?{{ $param }}" class="btn btn-success mb-2"><i class="far fa-file-excel"></i> </a>
                                    @endcan --}}
                                    @can('pdf_realisasi_harian')
                                        <a href="{{ url('realisasi/harian-pdf') }}?{{ $param }}" class="btn btn-warning mb-2"><i class="far fa-file-pdf"></i> </a>
                                    @endcan
    
                                </form>
                            </div>
                        </div>
                        <div class="card-body p-1 table-responsive">
                            
                            @include('realisasi.harian_table',['data'=>$data])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
            if (kd_kecamatan == '') {
                var kd_kecamatan = $('#kd_kecamatan').val()
            }
            getKelurahan(kd_kecamatan);


            function getKelurahan(kk) {
                var html = '<option value="">-- pilih --</option>';
                if (kk != '') {

                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            $.each(res, function(k, v) {
                                // console.log(k)
                                html += '<option value="' + k + '">' + v + '</option>';
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }
        });
    </script>
@endsection
