<table class="table table-bordered table-sm" id="table-data">
    <thead>
        <tr>
            <th>No</th>
            <th>Kobil</th>
            <th>NOP</th>
            <th>Tahun Pajak</th>
            <th>Wajib Pajak</th>
            <th>Pokok</th>
            <th>Denda</th>
            <th>Total</th>
            <th>Tanggal Bayar</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach ($data as $row)
            <tr>
                <td style="text-align: center">{{ $no }}</td>
                <td>{{ formatnop($row->kobil) }}</td>
                <td>{{ $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '-' . $row->no_urut . '.' . $row->kd_jns_op }}
                </td>
                <td>{{ $row->tahun_pajak }}</td>
                <td>{{ $row->nm_wp }}</td>
                <td style="text-align: right">{{ $row->pokok }}</td>
                <td style="text-align: right">{{ $row->denda }}</td>
                <td style="text-align: right">{{ $row->total }}</td>
                <td style="text-align: right">{{ $row->tgl_bayar }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>
