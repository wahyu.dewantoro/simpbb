<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h3 class="text-tengah">Detail Kecamatan  Tanggal {{ tglIndo($tanggal) }}</h3>
    @include('realisasi.detail_kecamatan_table_cetak',['data'=>$data])
</body>

</html>
