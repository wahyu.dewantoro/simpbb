<html>
<head>
</head>
<body>
    <table>
        <tr>
            <th style="text-align: center" colspan="10"><b>Rangking Kelurahan / Desa Buku {{ implode(', ', $buku) }} Tahun Pajak {{ $tahun }}</b></th>
        </tr>
        <tr>
            <th style="text-align: center" colspan="10"><b>Cut Off {{ tglindo($tanggal) }}</b></th>
        </tr>
    </table>
    @include('realisasi.rangking_kelurahan_table',['realisasi'=>$realisasi,'excel'=>1])
</body>
</html>
