<table class="table  table-sm table-bordered">
    <thead>
        <tr>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Kecamatan</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Buku</th>
            <th style="text-align: center; vertical-align:middle;" colspan="3">Realisasi Tahun
                Berjalan</th>
            <th style="text-align: center; vertical-align:middle;" colspan="3">
                Realisasi Tunggakan </th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Total</th>
        </tr>
        <tr>
            <th style="text-align: center; vertical-align:middle;">Pokok</th>
            <th style="text-align: center; vertical-align:middle;">Denda</th>
            <th style="text-align: center; vertical-align:middle;">Total</th>
            <th style="text-align: center; vertical-align:middle;">Pokok</th>
            <th style="text-align: center; vertical-align:middle;">Denda</th>
            <th style="text-align: center; vertical-align:middle;">Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr>
                @if ($row->kecamatan == 'Subtotal' or $row->kecamatan == 'Total')
                    <td colspan="2"><b>{{ $row->kecamatan }}</b></td>
                @else
                    <td>{{ $row->kecamatan }}</td>
                    <td align="center">{{ romawi($row->buku) }}</td>
                @endif
                <td align="right">{{ number_format($row->pokok_berjalan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->denda_berjalan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->jumlah_berjalan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->pokok_tunggakan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->denda_tunggakan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->jumlah_tunggakan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->total_keseluruhan, 0, '', '.') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
