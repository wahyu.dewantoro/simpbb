<table class="table  table-sm table-bordered">
    <thead>
        <tr style="vertical-align: middle">
            <th rowspan="2" style="text-align: center">No</th>
            <th rowspan="2" style="text-align: center">Kecamatan</th>
            <th rowspan="2" style="text-align: center">Kelurahan / Desa</th>
            <th rowspan="2" style="text-align: center">Objek</th>
            <th rowspan="2" style="text-align: center">Ketetapan PBB</th>
            <th colspan="3" style="text-align: center">Realisasi</th>
            <th rowspan="2" style="text-align: center">Sisa</th>
            <th rowspan="2" style="text-align: center; width: 90px">%</th>
        </tr>
        <tr style="vertical-align: middle">
            <th style="text-align:center">Pokok</th>
            <th style="text-align:center">Denda</th>
            <th style="text-align:center">Total</th>
        </tr>
    </thead>
    <tbody>
        @if (!empty($realisasi))
        @php
        $op = 0;
        $baku = 0;
        $pokok = 0;
        $denda = 0;
        $vrealisasi = 0;
        $vsisa=0;
        @endphp
        @foreach ($realisasi as $i => $row)
        @php
        $op += $row->op;
        $baku += $row->baku;
        $pokok += $row->pokok;
        $denda += $row->denda;
        $vrealisasi += $row->realisasi;
        $vsisa += $row->baku - $row->pokok;
        @endphp

        <tr>
            <td align="center">{{ $i + 1 }}</td>
            <td>{{ $row->nm_kecamatan }}</td>
            <td>{{ $row->nm_kelurahan }}</td>
            <td @if(isset($pdf)) width="5px" @endif align="center">{{  $excel==0?angka($row->op):$row->op }}</td>
            <td @if(isset($pdf)) width="120px" @endif align="right">{{  $excel==0?angka($row->baku):$row->baku }}</td>
            <td @if(isset($pdf)) width="120px" @endif align="right">{{  $excel==0?angka($row->pokok):$row->pokok }}</td>
            <td @if(isset($pdf)) width="120px" @endif align="right">{{  $excel==0?angka($row->denda):$row->denda }}</td>
            <td @if(isset($pdf)) width="120px" @endif align="right">{{  $excel==0?angka($row->realisasi):$row->realisasi }}</td>
            <td @if(isset($pdf)) width="120px" @endif align="right">  @if($row->baku - $row->pokok >0) {{   $excel==0?angka($row->baku - $row->pokok,3):($row->baku - $row->pokok) }} @else 0 @endif</td>
            <td align="center">{{ angka($row->persen_realisasi,3) }} %
            </td>

        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3">Jumlah</td>
            <td align="center">{{   $excel==0?angka($op):$op }}</td>
            <td align="right">{{   $excel==0?angka($baku):$baku }}</td>
            <td align="right">{{   $excel==0?angka($pokok):$pokok }}</td>
            <td align="right">{{   $excel==0?angka($denda):$denda }}</td>
            <td align="right">{{   $excel==0?angka($vrealisasi):$vrealisasi }}</td>
            <td align="right">{{   $excel==0?angka($vsisa):$vsisa }}</td>
            <td align="center">{{   $excel==0?angka($pokok/$baku *100,3):($pokok/$baku *100)}} %</td>
        </tr>
    </tfoot>
    @endif
</table>
