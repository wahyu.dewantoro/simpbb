@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Realiasi Baku</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="tanggal" class="form-control float-right tanggal"
                                            placeholder="Search" value="{{ $tanggal }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                        <a target="_blank"
                                            href="{{ url('realisasi/baku-excel') }}?tanggal={{ $tanggal }}"
                                            class="btn btn-sm btn-success"><i class="far fa-file-excel"></i></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th class="text-center">Buku</th>
                                        <th class="text-center">Pokok</th>
                                        <th class="text-center">Denda</th>
                                        <th class="text-center">Jumlah</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $pokok = 0;
                                        $denda = 0;
                                        $jumlah = 0;
                                    @endphp
                                    @foreach ($realisasi as $item)
                                        @php
                                            $pokok += $item->pokok;
                                            $denda += $item->denda;
                                            $jumlah += $item->jumlah;
                                        @endphp
                                        <tr>
                                            <td>Buku {{ $item->buku }}</td>
                                            <td class="text-right">{{ angka($item->pokok) }}</td>
                                            <td class="text-right">{{ angka($item->denda) }}</td>
                                            <td class="text-right">{{ angka($item->jumlah) }}</td>
                                            <td class="text-center">
                                                <a href="{{ url('realisasi/baku-buku') . '?' . $filter . '&buku=' . $item->buku }}"
                                                    target="_blank"><i class="fas fa-file-download"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="text-bold">Total</td>
                                        <td class="text-right text-bold">{{ angka($pokok) }}</td>
                                        <td class="text-right text-bold">{{ angka($denda) }}</td>
                                        <td class="text-right text-bold">{{ angka($jumlah) }}</td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
