@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Rangking Kelurahan / Desa</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}" class="form-inline" id="form-filter">
                                    <input type="text" class="form-control form-control-sm mb-2 mr-sm-2 tanggal"
                                        id="tanggal" name="tanggal" required>
                                    <select name="kd_buku" id="kd_buku" class="form-control form-control-sm mb-2 mr-sm-2">
                                        @foreach ($lb as $i => $item)
                                            <option @if ($i == '1') selected @endif
                                                value="{{ $i }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                    <button class="btn btn-sm btn-info mb-2 mr-sm-2"><i class="fa fa-search"></i>
                                        Tampilkan</button>
                                    <a href="#" data-id="excel" class="btn btn-sm btn-success mb-2 mr-sm-2 cetak"><i
                                            class="far fa-file-excel"></i></a>

                                    <a href="#" data-id="pdf" class="btn btn-sm btn-warning mb-2 mr-sm-2 cetak"><i
                                            class="far fa-file-pdf"></i></a>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <div id="preview-data"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });

            $("#form-filter").on("submit", function(event) {
                ;
                $('#preview-data').html('')
                event.preventDefault();

                $.ajax({
                    url: "{{ url()->current() }}",
                    data: $(this).serialize(),
                    success: function(res) {
                        // closeloading();
                        $('#preview-data').html(res)
                    },
                    error: function(res) {
                        // closeloading();
                        toastr.error('Error', 'Gagal menampilkan data');
                        $('#preview-data').html('')
                    }
                })

            });

            $('#form-filter').trigger('submit')

            $('.cetak').on('click', function(e) {
                e.preventDefault()
                let tipe = $(this).data('id')
                let tanggal = $('#tanggal').val()
                let kd_buku = $('#kd_buku').val()

                let url_cetak
                if (tipe == 'excel') {
                    url_cetak = "{{ url('realisasi/rangkingkelurahan-excel') }}"
                } else {
                    url_cetak = "{{ url('realisasi/rangkingkelurahan-pdf') }}"
                }

                url_cetak += "?kd_buku=" + kd_buku + "&tanggal=" + tanggal
                window.open(url_cetak)
            })
        })
    </script>
@endsection
