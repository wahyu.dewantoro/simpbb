@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Realiasi Tunggakan</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="tanggal" class="form-control float-right tanggal"
                                            placeholder="Search" value="{{ $tanggal }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                        <a href="{{ url('realisasi/tunggakan-excel') }}?tanggal={{ $tanggal }}"
                                            class="btn btn-sm btn-success"><i class="far fa-file-excel"></i></a>
                                        <a href="{{ url('realisasi/tunggakan-pdf') }}?tanggal={{ $tanggal }}"
                                            class="btn btn-sm btn-warning"><i class="far fa-file-pdf"></i></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr style="vertical-align: middle">
                                        <th style="text-align: center;width:200px" rowspan="3">Buku</th>
                                        <th style="text-align: center" rowspan="2" colspan="2">Ketetapan PBB</th>
                                        <th style="text-align: center" colspan="10">Realisasi</th>
                                    </tr>
                                    <tr style="vertical-align: middle">

                                        <th style="text-align: center" colspan="5"> {{ $tahun }}</th>
                                        <th style="text-align: center" colspan="5"> {{ $awal }} -
                                            {{ $akhir }}</th>
                                    </tr>
                                    <tr style="vertical-align: middle">
                                        <th style="text-align: center"> {{ $tahun }}</th>
                                        <th style="text-align: center"> {{ $awal }} - {{ $akhir }}</th>
                                        <th style="text-align: center">Pokok</th>
                                        <th style="text-align: center">Denda</th>
                                        <th style="text-align: center">Total</th>
                                        <th style="text-align: center">%</th>
                                        <th style="text-align: center">Sisa</th>
                                        <th style="text-align: center">Pokok</th>
                                        <th style="text-align: center">Denda</th>
                                        <th style="text-align: center">Total</th>
                                        <th style="text-align: center">%</th>
                                        <th style="text-align: center">Sisa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($realisasi as $item)
                                        <tr>
                                            {{-- <td>Buku {{ romawi($item->buku) }}</td> --}}
                                            <td><?= strlen($item->buku) == '1' ? 'Buku ' . romawi($item->buku) : '<b>' . $item->buku . '</b>' ?>
                                            </td>
                                            <td style="text-align: right">{{ angka($item->baku_berjalan) }}</td>
                                            <td style="text-align: right">{{ angka($item->baku_tunggakan) }}</td>
                                            <td style="text-align: right">{{ angka($item->real_pokok_berjalan) }}</td>
                                            <td style="text-align: right">{{ angka($item->real_denda_berjalan) }}</td>
                                            <td style="text-align: right">{{ angka($item->real_jumlah_berjalan) }}</td>
                                            <td style="text-align: right">
                                                {{ $item->real_jumlah_berjalan > 0 ? angka(($item->real_jumlah_berjalan / $item->baku_berjalan) * 100, 2) : angka(0, 2) }}
                                            </td>
                                            <td style="text-align: right">
                                                {{ angka($item->baku_berjalan - $item->real_jumlah_berjalan) }}</td>
                                            <td style="text-align: right">{{ angka($item->real_pokok_tunggakan) }}</td>
                                            <td style="text-align: right">{{ angka($item->real_denda_tunggakan) }}</td>
                                            <td style="text-align: right">{{ angka($item->real_jumlah_tunggakan) }}</td>
                                            <td style="text-align: right">
                                                {{ $item->baku_tunggakan > 0 ? angka(($item->real_jumlah_tunggakan / $item->baku_tunggakan) * 100, 2) : angka(0, 2) }}
                                            </td>
                                            <td style="text-align: right">
                                                {{ angka($item->baku_tunggakan - $item->real_jumlah_tunggakan) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
