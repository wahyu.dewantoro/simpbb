<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h5 class="text-tengah">Realisasi Bulanan
        <small>Bulan {{ date('F Y',strtotime('01-'.$bulan))  }} {{ $kecamatan<>''?'Kecamatan '.$kecamatan:'' }} {{ $kelurahan<>''?'Kel/Desa '.$kelurahan:'' }}</small>
        <br><small> Ditarik pada  {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small>
    </h5>
    
    @include('realisasi.bulanan_table',['data'=>$data])
</body>

</html>
