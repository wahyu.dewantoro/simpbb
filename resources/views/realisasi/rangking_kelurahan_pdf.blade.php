<html>

<head>
    <style>
        body {
            margin-top: 1mm;
            margin-bottom: 1mm;
            margin-left: 1mm;
            margin-right: 1mm;
            font-family: "Calibri (Body)";
            font-size: 0.9em;
        }

        table {
            font-weight: normal;
            font-family: "Calibri (Body)";
            font-size: 0.9em;
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 12px !important
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 11px !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 9 px !important
        }

        .text-sedang {
            font-size: 11px !important
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: .9em
        }

        #header {
            top: 0;
            border-bottom: .1pt solid #aaa
        }

        #footer {
            bottom: 0;
            border-top: .1pt solid #aaa
        }
    </style>
</head>

<body>
    {{-- <div id="watermark"><img src="watermark.jpg"></div> --}}
    <table width="100%" style="font-size: .875rem !important ">
        <tr>
            <th width="70px"><img width="60px" height="70px" src="{{ public_path('kabmalang_black.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            <th width="100px"></th>
        </tr>
    </table>
    <hr>
    <h4 class="text-tengah">Rangking Kelurahan/Desa <br>
        Cut off {{ tglIndo($tanggal) }}</h4>
    @include('realisasi.rangking_kelurahan_table_dua', [
        'realisasi' => $realisasi,
        'excel' => '0',
        'pdf' => true,
    ])
</body>

</html>
