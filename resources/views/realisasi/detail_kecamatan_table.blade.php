<table class="table  table-sm table-sm table-bordered">
    <thead>
        <tr>
            <th style="text-align: center; vertical-align:middle" rowspan="2">No</th>
            <th style="text-align: center; vertical-align:middle" rowspan="2">NOP</th>
            <th style="text-align: center; vertical-align:middle" rowspan="2">Nama WP</th>
            <th style="text-align: center; vertical-align:middle" colspan="3">Realisasi
                Berjalan</th>
            <th style="text-align: center; vertical-align:middle" colspan="3">Realisasi
                Tunggakan</th>
        </tr>
        <tr>
            <th style="text-align: center; vertical-align:middle">Pokok</th>
            <th style="text-align: center; vertical-align:middle">Denda</th>
            <th style="text-align: center; vertical-align:middle">Total</th>
            <th style="text-align: center; vertical-align:middle">Pokok</th>
            <th style="text-align: center; vertical-align:middle">Denda</th>
            <th style="text-align: center; vertical-align:middle">Total</th>
        </tr>

    </thead>
    <tbody>
        @php
            $no = 0;
        @endphp
        @foreach ($data as $row)
            <tr>
                <td align="center">{{ $data->firstItem() + $no }}</td>
                <td>{{ $row->kd_propinsi . '.' . $row->kd_dati2 . '.' . $row->kd_kecamatan . '.' . $row->kd_kelurahan . '.' . $row->kd_blok . '.' . $row->no_urut . '.' . $row->kd_jns_op }}
                    <br><small><span class="text-danger">
                        {{ $row->jalan_op }} &nbsp;
                        {{ $row->blok_kav_no_op }} &nbsp;
                        {{ $row->rt_op<>''?'RT '.$row->rt_op:'' }} &nbsp;
                        {{ $row->rw_op<>''?'RW '.$row->rw_op:'' }} &nbsp;
                        {{ $row->nm_kelurahan_op }} &nbsp;
                        {{ $row->nm_kecamatan_op }}
                    </span></small>
                </td>
                <td>{{ $row->nm_wp_sppt }}
                    <br><small><span class="text-danger">
                            {{ $row->jln_wp_sppt }} &nbsp;
                            {{ $row->blok_kav_no_wp_sppt }} &nbsp;
                            {{ $row->rt_wp_sppt<>''?'RT '.$row->rt_wp_sppt:'' }} &nbsp;
                            {{ $row->rw_wp_sppt<>''?'RW '.$row->rw_wp_sppt:'' }} &nbsp;
                            {{ $row->kelurahan_wp_sppt }} &nbsp;
                            {{ $row->kota_wp_sppt }} &nbsp;
                        </span></small>
                </td>
                <td align="right">{{ number_format($row->pokok_berjalan, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->denda_berjalan, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->total_berjalan, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->pokok_tunggakan, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->denda_tunggakan, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->total_tunggakan, 0, '', '.') }}
                </td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>