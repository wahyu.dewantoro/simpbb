<table class="table  table-sm table-bordered">
    <thead>
        <tr style="vertical-align: middle">
            <th style="text-align: center">No</th>
            <th style="text-align: center">Kecamatan</th>
            <th style="text-align: center">Kelurahan / Desa</th>
            <th style="text-align: center">Objek</th>
            <th style="text-align: center">Ketetapan PBB</th>
            <th style="text-align: center">Stimulus</th>
            <th style="text-align: center">Bayar</th>
            <th style="text-align: center">Koreksi</th>
            <th style="text-align: center">Sisa</th>
            <th style="text-align: center; width: 90px">%</th>
        </tr>
    </thead>
    <tbody>
        @if (!empty($realisasi))
            @php
                $op = 0;
                $baku = 0;
                $potongan = 0;
                $bayar = 0;
                $koreksi = 0;
                $sisa = 0;
            @endphp
            @foreach ($realisasi as $i => $row)
                @php
                    $op += $row->jumlah_op;
                    $baku += $row->baku;
                    $potongan += $row->potongan;
                    $bayar += $row->bayar;
                    $koreksi += $row->koreksi;
                    $sisa += $row->sisa;
                @endphp

                <tr>
                    <td align="center">{{ $i + 1 }}</td>
                    <td>{{ $row->nm_kecamatan }}</td>
                    <td>{{ $row->nm_kelurahan }}</td>
                    <td @if (isset($pdf)) width="5px" @endif align="center">
                        {{ $excel == 0 ? angka($row->jumlah_op) : $row->jumlah_op }}</td>
                    <td @if (isset($pdf)) width="120px" @endif align="right">
                        {{ $excel == 0 ? angka($row->baku) : $row->baku }}</td>
                    <td @if (isset($pdf)) width="120px" @endif align="right">
                        {{ $excel == 0 ? angka($row->potongan) : $row->potongan }}</td>
                    <td @if (isset($pdf)) width="120px" @endif align="right">
                        {{ $excel == 0 ? angka($row->bayar) : $row->bayar }}</td>
                    <td @if (isset($pdf)) width="120px" @endif align="right">
                        {{ $excel == 0 ? angka($row->koreksi) : $row->koreksi }}</td>
                    <td @if (isset($pdf)) width="120px" @endif align="right">
                        {{ $excel == 0 ? angka($row->sisa) : $row->sisa }}</td>
                    <td align="center">{{ angka($row->persen) }} %
                    </td>
                </tr>
            @endforeach
    </tbody>
    <tfoot>
        {{-- <tr>
            <td colspan="3">Jumlah</td>
            <td align="center">{{ $excel == 0 ? angka($op) : $op }}</td>
            <td align="right">{{ $excel == 0 ? angka($baku) : $baku }}</td>
            <td align="right">{{ $excel == 0 ? angka($pokok) : $pokok }}</td>
            <td align="right">{{ $excel == 0 ? angka($denda) : $denda }}</td>
            <td align="right">{{ $excel == 0 ? angka($vrealisasi) : $vrealisasi }}</td>
            <td align="right">{{ $excel == 0 ? angka($vsisa) : $vsisa }}</td>
            <td align="center">{{ $excel == 0 ? angka(($pokok / $baku) * 100, 3) : ($pokok / $baku) * 100 }} %</td>
        </tr> --}}
    </tfoot>
    @endif
</table>
