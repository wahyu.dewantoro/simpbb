@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Pembayaran Via Kode Billing</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm">

                                
                                <input type="text" name="tanggal" id="tanggal"
                                    class="form-control form-control-sm tanggal   mr-sm-2" required
                                    value="{{ $tanggal }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-flat btn-sm btn-success" id="excel"><i class="fas fa-file-excel"></i> Excel</button>
                                    </span>
                                </div>
                                    
                            </div>
                        </div>
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-bordered table-sm" id="table-data">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kobil</th>
                                        <th>NOP</th>
                                        <th>Tahun Pajak</th>
                                        <th>Wajib Pajak</th>
                                        <th>Pokok</th>
                                        <th>Denda</th>
                                        <th>Total</th>
                                        <th>Tanggal Bayar</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // $("#tanggal").on('change', function() {
            $('#tanggal').on('change', function() {
                console.log($("#tanggal").val());
                var url = '{!! url('realisasi/kobil') !!}?tanggal=' + $("#tanggal").val();
                $('#table-data').DataTable().ajax.url(url).load();
                
            });
            
            $('#excel').on('click', function() {
                var url = "{!! url('realisasi/kobil-excel') !!}?tanggal="+ $('#tanggal').val();
                console.log(url);
                window.open(url,'_blank');
            });


            $('#table-data').DataTable({
                processing: true,
                serverSide: true,
                // searching: false,
                ajax: '{!! url('realisasi/kobil') !!}',
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'kobil',
                        name: 'kobil'
                    },
                    {
                        data: 'nop',
                        name: 'nop'
                    },
                    {
                        data: 'tahun_pajak',
                        name: 'tahun_pajak',
                        // class: 'text-right'
                    },
                    {
                        data: 'nm_wp',
                        name: 'nm_wp',
                        // class: 'text-center'
                    },
                    {
                        data: 'pokok',
                        name: 'pokok',
                        // class: 'text-center'
                    },
                    {
                        data: 'denda',
                        name: 'denda',
                        // class: 'text-center'
                    },
                    {
                        data: 'total',
                        name: 'total',
                        // class: 'text-center'
                    },
                    {
                        data: 'tanggal_bayar',
                        name: 'tanggal_bayar',
                        // class: 'text-center'
                    },
                ]
            });
        });
    </script>
@endsection
