<table class="table  table-sm table-bordered">
    <thead>
        <tr style="vertical-align: middle">
            <th style="text-align: center">No</th>
            <th style="text-align: center">Kecamatan</th>

            {{-- <th style="text-align: center">Objek</th> --}}
            <th style="text-align: center">Ketetapan PBB</th>
            <th style="text-align: center">Stimulus</th>
            <th style="text-align: center">Bayar</th>
            <th style="text-align: center">Koreksi</th>
            <th style="text-align: center">Sisa</th>
            <th style="text-align: center; width: 90px">%</th>
        </tr>
    </thead>
    <tbody>

        @php
            // $op = 0;
            $baku = 0;
            $potongan = 0;
            $bayar = 0;
            $koreksi = 0;
            $sisa = 0;
        @endphp
        @foreach ($realisasi as $i => $row)
            @php
                // $op += $row->jumlah_op;
                $baku += $row->baku;
                $potongan += $row->potongan;
                $bayar += $row->bayar;
                $koreksi += $row->koreksi;
                $sisa += $row->baku - $row->potongan - $row->bayar - $row->koreksi;
            @endphp

            <tr>
                <td align="center">{{ $i + 1 }}</td>
                <td>{{ $row->nm_kecamatan }}</td>
                {{-- <td>{{ $row->nm_kelurahan }}</td> --}}
                {{-- <td @if (isset($pdf)) width="5px" @endif align="center">
                    {{ $excel == 0 ? angka($row->jumlah_op) : $row->jumlah_op }}</td> --}}
                <td @if (isset($pdf)) width="120px" @endif align="right">
                    {{ $excel == 0 ? angka($row->baku) : $row->baku }}</td>
                <td @if (isset($pdf)) width="120px" @endif align="right">
                    {{ $excel == 0 ? angka($row->potongan) : $row->potongan }}</td>
                <td @if (isset($pdf)) width="120px" @endif align="right">
                    {{ $excel == 0 ? angka($row->bayar) : $row->bayar }}</td>
                <td @if (isset($pdf)) width="120px" @endif align="right">
                    {{ $excel == 0 ? angka($row->koreksi) : $row->koreksi }}</td>
                <td @if (isset($pdf)) width="120px" @endif align="right">
                    {{ $excel == 0 ? angka($row->baku - $row->potongan - $row->bayar - $row->koreksi) : $row->baku - $row->potongan - $row->bayar - $row->koreksi }}
                </td>
                <td align="center">{{ angka($row->persen) }} %
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
