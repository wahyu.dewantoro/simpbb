<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">LAPORAN REALISASI BAKU PAJAK BUMI DAN BANGUNAN TAHUN PAJAK {{ $tahun }}
        <br><small> Cut Off  {{ tglIndo($tanggal) }} </small></h4>
        <br><small> Ditarik pada  {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small></h4>
    @include('realisasi.baku_table',['realisasi'=>$realisasi])
</body>

</html>
