<html>
<body>
    <table>
        <thead>
            <tr>
                <td colspan="8" style="text-align: center">
                    <b>LAPORAN REALISASI BAKU PAJAK BUMI DAN BANGUNAN TAHUN PAJAK  {{ $tahun }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center">
                    <small> Cut off  {{ tglIndo($tanggal) }}</small>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center">
                    <small> Ditarik pada  {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small>
                </td>
            </tr>
        </thead>
    </table>
    @include('realisasi.baku_table',['realisasi'=>$realisasi])
</body>

</html>
