@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>Pembayaran SPPT</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Data</h3>
                        <div class="card-tools">
                            <div class="btn-group">
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span></span> <i class="fa fa-caret-down"></i>
                                </div>
                                <input type="hidden" name="tgl_start" id="tgl_start">
                                <input type="hidden" name="tgl_end" id="tgl_end">
                                <span class="input-group-append">
                                    {{-- @can('pembayaran sppt excel') --}}
                                    <a class="btn btn-sm btn-flat btn-success" href="#" id="excel"><i class="far fa-file-excel"></i></a>
                                    {{-- @endcan --}}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <div class="table-responsive">
                            <table class="table  table-sm table-sm table-bordered" id="table-hasil">
                                <thead>

                                    <tr>
                                        <th style="text-align: center; vertical-align:middle">No</th>
                                        <th style="text-align: center; vertical-align:middle">NOP</th>
                                        <th style="text-align: center; vertical-align:middle">Tanggal</th>
                                        <th style="text-align: center; vertical-align:middle">Pokok</th>
                                        <th style="text-align: center; vertical-align:middle">Denda</th>
                                        <th style="text-align: center; vertical-align:middle">Total</th>
                                        <th style="text-align: center; vertical-align:middle">keterangan</th>
                                    </tr>

                                </thead>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
@section('script')
<script>
    $(document).ready(function() {


        // .subtract(29, 'days');
        var start = moment();
        var end = moment();

        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ url('realisasi/pembayaran') }}"
                , data: function(d) {
                    d.tgl_start = $('#tgl_start').val()
                    d.tgl_end = $('#tgl_end').val()
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , searchable: false
                    , render: function(data, type, row) {
                        return data + "<br><small class='text-info'>" + row.thn_pajak_sppt + "</small>"
                    }
                }

                , {
                    data: 'tgl_pembayaran_sppt'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'pokok'
                    ,class:"text-right"
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'denda'
                    ,class:"text-right"
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'total'
                    ,class:"text-right"
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'keterangan'
                    , orderable: false
                    , searchable: false
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        function cb(start, end) {
            vstart = start.format('D MMMM  YYYY')
            vend = end.format('D MMMM  YYYY')

            if (vstart == vend) {
                txt = vstart
            } else {
                txt = vstart + ' - ' + vend
            }

            $('#reportrange span').html(txt);
            $('#tgl_start').val(vstart)
            $('#tgl_end').val(vend)
            tableHasil.draw();
        }

        $('#reportrange').daterangepicker({
            startDate: start
            , endDate: end
            , ranges: {
                'Hari ini': [moment(), moment()]
                , 'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')]
                , '7 Hari terakhir': [moment().subtract(6, 'days'), moment()]
            }
        }, cb);

        cb(start, end);


        $('#excel').on('click', function(e) {
            e.preventDefault()
            let tgl_start = $('#tgl_start').val()
            let tgl_end = $('#tgl_end').val()
            let url_ = "{{ url('realisasi/pembayaran-excel') }}?start=" + tgl_start + "&end=" + tgl_end
            window.open(url_);
        })

    });

</script>
@endsection
