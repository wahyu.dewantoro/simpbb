<html>
<head>
</head>
<body>
    <table>
        <tr>
            <th style="text-align: center" colspan="8"><b>Rangking Kecamatan Buku {{ implode(', ', $buku) }} Tahun Pajak {{ $tahun }}</b></th>
        </tr>
        <tr>
            <th style="text-align: center" colspan="8"><b>Cut Off {{ tglindo($tanggal) }}</b></th>
        </tr>
    </table>
    @include('realisasi.rangking_kecamatan_table',['realisasi'=>$realisasi])
</body>
</html>
