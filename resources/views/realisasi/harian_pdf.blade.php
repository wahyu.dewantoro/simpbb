<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">Realisasi Harian
        <br><small>Tanggal {{ tglIndo($tanggal) }} {{ $kecamatan<>''?'Kecamatan '.$kecamatan:'' }} {{ $kelurahan<>''?'Kel/Desa '.$kelurahan:'' }}</small>
        <br><small> Ditarik pada  {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small>
    </h4>    
    @include('realisasi.harian_table',['data'=>$data])
</body>

</html>
