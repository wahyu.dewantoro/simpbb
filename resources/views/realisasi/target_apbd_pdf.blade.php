<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">LAPORAN TARGET APBD DAN REALISASI PAJAK BUMI DAN BANGUNAN TAHUN PAJAK {{ $tahun }}
        <br><small> Cut off  {{ tglIndo($tanggal) }} </small>
        <br><small> Ditarik pada  {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small>
    </h>

    @include('realisasi.target_apbd_table',['realisasi'=>$realisasi,'tahun'=>$tahun,'pdf'=>true])
</body>

</html>
