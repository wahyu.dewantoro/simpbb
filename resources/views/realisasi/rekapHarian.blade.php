@extends('layouts.app')

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>Rekap Harian Pembayaran</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h3 class="card-title">List Data</h3> --}}
                        <div class="card-tools">
                            <div class="card-tools">
                                <div class="card-tools">
                                    <div class="btn-group">
                                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <input type="hidden" name="tgl_start" id="tgl_start">
                                        <input type="hidden" name="tgl_end" id="tgl_end">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                        <div id="hasil"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')

<script>
    $(document).ready(function() {
        $(document).on({
            ajaxStart: function() {
                openloading();
            }
            , ajaxStop: function() {
                closeloading();
            }
        });
        /*  $('#cetak').on('click', function() {
             tanggala = $('#tanggala').val()
             tanggalb = $('#tanggalb').val()
             url_ = "{{ url('realisasi/rekap-harian-pembayaran-excel') }}";
             url = url_ + "?tanggala=" + tanggala + "&tanggalb=" + tanggalb
             console.log(url);
             window.open(url, '_blank');
         }); */

        var start = moment().subtract(6, 'days');
        var end = moment();

        function getData(start, end) {
            $.ajax({
                url: "{{ url('realisasi/rekap-harian-pembayaran') }}"
                , data: {
                    start
                    , end
                }
                , success: function(res) {
                    $('#hasil').html(res)
                }
                , error: function(e) {
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false,
                        // showConfirmButton: false
                    })
                    $('#hasil').html('')
                }
            });
        }

        function cb(start, end) {
            vstart = start.format('D MMMM  YYYY')
            vend = end.format('D MMMM  YYYY')

            if (vstart == vend) {
                txt = vstart
            } else {
                txt = vstart + ' - ' + vend
            }
            $('#reportrange span').html(txt);
            getData(vstart, vend)
        }

        $('#reportrange').daterangepicker({
            showCustomRangeLabel: true
            , startDate: start
            , endDate: end
            , ranges: {

                'Last 7 Days': [moment().subtract(6, 'days'), moment()]
                , 'Last 30 Days': [moment().subtract(29, 'days'), moment()]
                , 'This Month': [moment().startOf('month'), moment().endOf('month')]
                , 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            , }
        }, cb);

        cb(start, end);

    });

</script>
@endsection
