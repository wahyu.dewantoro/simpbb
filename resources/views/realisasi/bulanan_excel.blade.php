<html>

<head>
    
</head>

<body>
    
    <table>
        <thead>
            <tr>
                <th colspan="8" style="text-align: center">
                    Realisasi Bulanan
                </th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: center">
                    <small>Bulan {{ date('F Y',strtotime('01-'.$bulan))  }} {{ $kecamatan<>''?'Kecamatan '.$kecamatan:'' }} {{ $kelurahan<>''?'Kel/Desa '.$kelurahan:'' }}</small>
                </th>
            </tr>
        </thead>
    </table>
    @include('realisasi.bulanan_table',['data'=>$data])
</body>

</html>
