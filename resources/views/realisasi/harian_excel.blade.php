<html>

<head>
    
</head>

<body>
    <table>
        <thead>
            <tr>
                <th colspan="8" style="text-align: center">
                    Realisasi Harian
                </th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: center">
                    <small>Tanggal {{ tglIndo($tanggal) }} {{ $kecamatan<>''?'Kecamatan '.$kecamatan:'' }} {{ $kelurahan<>''?'Kel/Desa '.$kelurahan:'' }}</small>
                </th>
            </tr>
        </thead>
    </table>
    @include('realisasi.harian_table',['data'=>$data])
</body>

</html>
