<html>
    <body>
        <table>
            <thead>
                <tr>
                    <th style="text-align: center" colspan="10">
                        LAPORAN TARGET APBD DAN REALISASI PAJAK BUMI DAN BANGUNAN TAHUN PAJAK {{ $tahun }}
                    </th>
                </tr>
                
                <tr>
                    <th style="text-align: center" colspan="10">
                        <small> Cut Off {{ tglIndo($cutoff) }}</small>
                    </th>
                </tr>
                <tr>
                    <th style="text-align: center" colspan="10">
                        <small> Ditarik pada  {{ tglIndo(date('Ymd')) }} , {{ date('H:i:s') }}</small>
                    </th>
                </tr>
            </thead>
        </table>
        @include('realisasi.target_apbd_table',['realisasi'=>$realisasi,'tahun'=>$tahun])
    </body>
</html>
