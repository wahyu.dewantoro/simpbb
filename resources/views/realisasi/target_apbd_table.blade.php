<table class="table  table-sm table-bordered">
    <thead>
        <tr>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Buku</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Target</th>
            <th style="text-align: center; vertical-align:middle;" colspan="3">Realisasi Tahun
                {{ $tahun }}</th>
            <th style="text-align: center; vertical-align:middle;" colspan="3">Realisasi
                Tunggakan</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Total</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">%</th>
        </tr>
        <tr>
            <th style="text-align: center; vertical-align:middle;">Pokok</th>
            <th style="text-align: center; vertical-align:middle;">Denda</th>
            <th style="text-align: center; vertical-align:middle;">Total</th>
            <th style="text-align: center; vertical-align:middle;">Pokok</th>
            <th style="text-align: center; vertical-align:middle;">Denda</th>
            <th style="text-align: center; vertical-align:middle;">Total</th>
        </tr>
    </thead>
    <tbody>
        @php
            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $e = 0;
            $f = 0;
            $g = 0;
            $h = 0;
        @endphp
        @foreach ($realisasi as $row)
            @php
                
                $a += $row->target;
                $b += $row->pokok_berjalan;
                $c += $row->denda_berjalan;
                $d += $row->jumlah_berjalan;
                $e += $row->pokok_tunggakan;
                $f += $row->denda_tunggakan;
                $g += $row->jumlah_tunggakan;
                $h += $row->total_keseluruhan;
            @endphp

            <tr>
                <td @if (isset($pdf)) width="180px" @endif>{{ $row->nm_buku }}</td>
                <td align="right">{{ number_format($row->target, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->pokok_berjalan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->denda_berjalan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->jumlah_berjalan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->pokok_tunggakan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->denda_tunggakan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->jumlah_tunggakan, 0, '', '.') }}</td>
                <td align="right">{{ number_format($row->total_keseluruhan, 0, '', '.') }} </td>
                <td align="center">{{ number_format($row->persen_realisasi, 2, ',', '.') }} %</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>Jumlah</td>
            <td style="text-align: right">{{ angka($a) }}</td>
            <td style="text-align: right">{{ angka($b) }}</td>
            <td style="text-align: right">{{ angka($c) }}</td>
            <td style="text-align: right">{{ angka($d) }}</td>
            <td style="text-align: right">{{ angka($e) }}</td>
            <td style="text-align: right">{{ angka($f) }}</td> 
            <td style="text-align: right">{{ angka($g) }}</td>
            <td style="text-align: right">{{ angka($h) }}</td>
            <td style="text-align: center">{{  $h>0 ?number_format($h/$a*100, 2, ',', '.') :0}} %</td>
        </tr>
    </tfoot>
</table>
