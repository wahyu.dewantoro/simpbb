<table class="table  table-sm table-sm table-bordered">
    <thead>

        <tr>
            <th style="text-align: center; vertical-align:middle">No</th>
            <th style="text-align: center; vertical-align:middle">NOP</th>
            <th style="text-align: center; vertical-align:middle">Tahun Pajak</th>
            <th style="text-align: center; vertical-align:middle">Wajib Pajak</th>
            <th style="text-align: center; vertical-align:middle">Tanggal</th>
            <th style="text-align: center; vertical-align:middle">Pokok</th>
            <th style="text-align: center; vertical-align:middle">Denda</th>
            <th style="text-align: center; vertical-align:middle">Total</th>
            <th style="text-align: center; vertical-align:middle">Pengesahan</th>
            <th style="text-align: center; vertical-align:middle">TP</th>
            <th style="text-align: center; vertical-align:middle">Kobil / Tahun</th>
        </tr>

    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach ($data as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ formatNop($row->nop) }}</td>
                <td>{{ $row->thn_pajak_sppt }}</td>
                <td>{{ $row->nm_wp_sppt }}</td>
                <td>{{ tglIndo($row->tgl_pembayaran_sppt) }}</td>
                <td style="text-align: right">{{ number_format($row->pokok,0,'','') }}</td>
                <td style="text-align: right">{{ number_format($row->denda,0,'','') }}</td>
                <td style="text-align: right">{{ number_format($row->total,0,'','') }}</td>
                <td>{{ $row->pengesahan }}</td>
                <td>{{ $row->nama_bank }}</td>
                <td>{{ $row->kobil_tahun }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>
