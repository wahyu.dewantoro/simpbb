@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Realiasi Baku</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="tanggal" class="form-control float-right tanggal"
                                            placeholder="Search" value="{{ $tanggal }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>

                                        @can('excel_realisasi_baku')
                                            <a href="{{ url('realisasi/baku-excel') }}?tanggal={{ $tanggal }}" class="btn btn-sm btn-success"><i class="far fa-file-excel"></i></a>
                                        @endcan
                                        @can('pdf_realisasi_baku')
                                            <a href="{{ url('realisasi/baku-pdf') }}?tanggal={{ $tanggal }}" class="btn btn-sm btn-warning"><i class="far fa-file-pdf"></i></a>
                                        @endcan
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            @include('realisasi.baku_table',['realisasi'=>$realisasi])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
