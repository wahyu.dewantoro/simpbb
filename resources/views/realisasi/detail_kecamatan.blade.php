@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Realiasi Detail Kecamatan</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <div class="card-body table-responsive p-1">
                            <form action="{{ url()->current() }}" class="form-inline">

                                <input type="text" name="tanggal" id="tanggal"
                                    class="form-control form-control-sm tanggal mb-2 mr-sm-2" required
                                    value="{{ $tanggal }}">
                                @php
                                    $cb = request()->get('buku') ?? ['1', '2'];
                                @endphp
                                @for ($i = 1; $i <= 5; $i++)
                                    <div class="form-check form-check-inline">
                                        <input @if (in_array($i, $cb)) checked @endif class="form-check-input" name="buku[]"
                                            type="checkbox" id="inlineCheckbox1{{ $i }}"
                                            value="{{ $i }}">
                                        <label class="form-check-label" for="inlineCheckbox1{{ $i }}">Buku
                                            {{ $i }}</label>
                                    </div>
                                @endfor

                                <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i>
                                    Cari</button>

                                    @can('excel_realisasi_detail_kecamatan')
                                    <a class="btn mb-2 btn-success" href="{{ url('realisasi/detailkecamatan-excel') }}?{{ $param }}"><i class="far fa-file-excel"></i></a>
                                    @endcan
                                    @can('pdf_realisasi_detail_kecamatan')
                                    <a class="btn mb-2 btn-warning" href="{{ url('realisasi/detailkecamatan-pdf') }}?{{ $param }}"><i class="far fa-file-pdf"></i></a>
                                    @endcan
                            </form>
                            
                               @include('realisasi.detail_kecamatan_table',['data'=>$data])
                            
                            <div class="float-right d-none d-sm-inline">
                            {{ $data->withPath('detailkecamatan')->withQueryString()->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
            if (kd_kecamatan == '') {
                var kd_kecamatan = $('#kd_kecamatan').val()
            }
            getKelurahan(kd_kecamatan);


            function getKelurahan(kk) {
                var html = '<option value="">-- Desa --</option>';
                if (kk != '') {

                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            $.each(res, function(k, v) {
                                // console.log(k)
                                html += '<option value="' + k + '">' + v + '</option>';
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }
        });
    </script>
@endsection
