 <table class="table table-sm  text-sm table-striped">
     <thead>

         <tr style="vertical-align: middle">
             <th style="text-align: left">Tanggal</th>
             <th style="text-align: right">BJ</th>
             <th style="text-align: right">POS</th>
             <th style="text-align: right">Total</th>

         </tr>
     </thead>
     <tbody>
         @foreach ($data as $item)
         <tr>
             <td style="text-align: left">{{ tglindo($item->tanggal) }}</td>
             <td style="text-align: right">{{ angka($item->bj) }}</td>
             <td style="text-align: right">{{ angka($item->pos) }}</td>
             <td style="text-align: right">{{ angka($item->jumlah) }}</td>
         </tr>
         @endforeach
     </tbody>
 </table>
