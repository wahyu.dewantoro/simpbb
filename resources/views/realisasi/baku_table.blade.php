<table class="table  table-sm table-bordered">
    <thead>
        <tr>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Buku</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Jumlah OP</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Jumlah Baku</th>
            <th style="text-align: center; vertical-align:middle;" colspan="3">Realisasi </th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">%</th>
            <th style="text-align: center; vertical-align:middle;" rowspan="2">Sisa</th>
        </tr>
        <tr>
            <th style="text-align: center; vertical-align:middle;">Pokok</th>
            <th style="text-align: center; vertical-align:middle;">Denda</th>
            <th style="text-align: center; vertical-align:middle;">Total</th>

        </tr>
    </thead>
    <tbody>
        @php
            $a=0;
            $b=0;
            $c=0;
            $d=0;
            $e=0;
            $f=0;
            $g=0;
        @endphp
        @foreach ($realisasi as $row)
        @php
            $a+=$row->jumlah_op;
            $b+=$row->baku;
            $c+=$row->pokok;
            $d+=$row->denda;
            $e+=$row->jumlah;
            $f=0;
            $g+=$row->sisa_baku;
        @endphp

            <tr>
                <td><?= strlen($row->buku)=='1'?'Buku '.romawi($row->buku):'<b>'.$row->buku.'</b>' ?></td>
                <td align="right">{{ number_format($row->jumlah_op,0,'','.') }}</td>
                <td align="right">{{ number_format($row->baku,0,'','.') }}</td>
                <td align="right">{{ number_format($row->pokok,0,'','.') }}</td>
                <td align="right">{{ number_format($row->denda,0,'','.') }}</td>
                <td align="right">{{ number_format($row->jumlah,0,'','.') }}</td>
                <td align="center">{{ number_format($row->persen_realisasi,2,',','.') }} %</td>
                <td align="right">{{ number_format($row->sisa_baku,0,'','.') }}</td>
            </tr>
        @endforeach
    </tbody>
    {{-- <tfoot>
        <tr>
            <td>Total</td>
            <td align="right">{{ angka($a) }}</td>
            <td align="right">{{ angka($b) }}</td>
            <td align="right">{{ angka($c) }}</td>
            <td align="right">{{ angka($d) }}</td>
            <td align="right">{{ angka($e) }}</td>
            <td align="center">{{ angka($e/$b*100,2) }} %</td>
            <td align="right">{{ angka($g) }}</td>
        </tr>
    </tfoot> --}}
</table>