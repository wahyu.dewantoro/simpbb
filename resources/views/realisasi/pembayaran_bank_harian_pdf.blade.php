<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h3 class="text-tengah">Pembayaran Tanggal {{ tglIndo($tanggal) }}</h3>
    @include('realisasi.pembayaran_bank_harian_cetak',['data'=>$data])
</body>

</html>
