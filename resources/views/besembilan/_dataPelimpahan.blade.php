<style>
    /* .dataTables_filter, */
    /* .dataTables_length,
    .dataTables_info {
        display: none;
    } */
</style>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="besembilan-table">
        <thead class="bg-info">
            <tr>
                <th>Source</th>
                <th>Datetime</th>
                <th>Description</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td>{{ $item->source }} </td>
                    <td>{{ $item->datetime }} </td>
                    <td>{{ $item->description }} </td>
                    <td class="text-right">{{ number_format($item->amount) }} </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {
        $('#besembilan-table').DataTable({
            "ordering": false,
            dom: 'Bfrtip',
            buttons: [
                'pdf'
            ]
        })
    })
</script>
