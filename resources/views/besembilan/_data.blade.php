<style>
    /* .dataTables_filter, */
    /* .dataTables_length,
    .dataTables_info {
        display: none;
    } */
</style>
<div class="table-responsive">
    <table class="table table-sm table-bordered" id="besembilan-table">
        <thead class="bg-info">
            <tr>
                <th>dateTime</th>
                <th>description</th>
                <th>Debit</th>
                <th>Kredit</th>
                {{-- <th>ccy</th> --}}
                <th>reffno</th>
                <th>source</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td>{{ $item['dateTime'] }}</td>
                    <td>{{ $item['description'] }}</td>
                    <td class="text-right">
                        @if ($item['flag'] == 'D')
                            {{ $item['amount'] }}
                        @endif
                    </td>
                    <td class="text-right">
                        @if ($item['flag'] == 'C')
                            {{ $item['amount'] }}
                        @endif
                    </td>
                    <td>{{ $item['reffno'] }}</td>
                    <td>{{ $item['source'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {
        $('#besembilan-table').DataTable({
            "ordering": false,
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel', 'pdf'
            ]
        })
    })
</script>
