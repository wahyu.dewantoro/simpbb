@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.6/css/buttons.dataTables.min.css">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1>Pelimpahan Ke RKUD</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools">
                                <div class="card-tools">
                                    <div class="btn-group">
                                        <div id="reportrange"
                                            style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <span></span> <i class="fa fa-caret-down"></i>
                                        </div>
                                        <input type="hidden" name="tgl_start" id="tgl_start">
                                        <input type="hidden" name="tgl_end" id="tgl_end">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div id="hasil"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
            $(document).on({
                ajaxStart: function() {
                    openloading();
                },
                ajaxStop: function() {
                    closeloading();
                }
            });


            var start = moment().startOf('month');
            var end = moment();

            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    $('#cek').trigger('click');
                }
            });

            /* $('#cek').on('click', function(e) {
                e.preventDefault();
                
            }); */

            function getData(start, end) {
                $.ajax({
                    url: "{{ url('pelimpahan') }}",
                    data: {
                        start,
                        end
                    },
                    success: function(res) {
                        $('#hasil').html(res)
                    },
                    error: function(e) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan',
                            text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            // showConfirmButton: false
                        })
                        $('#hasil').html('')
                    }
                });
            }

            function cb(start, end) {
                vstart = start.format('D MMMM  YYYY')
                vend = end.format('D MMMM  YYYY')

                if (vstart == vend) {
                    txt = vstart
                } else {
                    txt = vstart + ' - ' + vend
                }
                $('#reportrange span').html(txt);
                getData(vstart, vend)
            }

            $('#reportrange').daterangepicker({
                showCustomRangeLabel: false,
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
        });
    </script>
@endsection
