@for ($index = 1; $index <= $count; $index++) <div class="row  hapus" id="TextBoxDiv{{ $index }}">
    @php
    $lspop=[];
    if(!empty($bangunan)){
    $lspop=$bangunan[$index-1];
    }

    @endphp

    <div class="col-12">
        <table class="table table-bordered">
            <thead class="bg-warning">
                <tr>
                    <th class="text-center">Bangunan Ke {{ $index }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-grop row">
                                    <label class="col-md-2 col-form-label text-right">Jenis Transaksi</label>
                                    <div class="col-md-6 ">
                                        <select name="jns_transaksi_lspop[]" class="form-control form-control-sm jns_transaksi_lspop" data-index="{{ $index }}" id="jns_transaksi_lspop_{{ $index }}">

                                        </select>
                                        {{-- <div class="row">
                                            <div class="col-md-2">
                                                <input readonly type="text" name="jns_transaksi_lspop[]" class="form-control form-control-sm jns_transaksi_lspop" data-index="{{ $index }}" id="jns_transaksi_lspop_{{ $index }}">
                                            </div>
                                            <div class="col-md-6">
                                                <span class="form-control form-control-sm" readonly id="jns_transaksi_lspop_keterangan_{{ $index }}"></span>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-grop row">
                                    <label class="col-md-4 col-form-label text-right">Penggunaan Bangunan</label>
                                    <div class="col-md-8">
                                        <select name="kd_jpb[]" class="form-control form-control-sm kd_jpb" value="{{  $lspop->kd_jpb??$jpb }}" data-index="{{ $index }}" id="kd_jpb_{{ $index }}" required>
                                        </select>
                                        {{-- <div class="row">
                                            <div class="col-md-3">
                                                <input type="text" name="kd_jpb[]" class="form-control form-control-sm kd_jpb" value="{{  $lspop->kd_jpb??$jpb }}" data-index="{{ $index }}" id="kd_jpb_{{ $index }}">
                                    </div>
                                    <div class="col-md-9">
                                        <span readonly class="form-control form-control-sm" id="kd_jpb_keterangan_{{ $index }}"></span>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <div class="form-grop row">
                    <label class="col-md-6 col-form-label text-right">Tahun
                        Dibangun</label>
                    <div class="col-md-4">
                        <input type="text" name="thn_dibangun_bng[]" value="{{ $lspop->thn_dibangun_bng??date('Y') }}" class="form-control form-control-sm">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-grop row">
                    <label class="col-md-6 col-form-label text-right">Tahun
                        Renovasi</label>
                    <div class="col-md-4">
                        <input type="text" name="thn_renovasi_bng[]" value="{{ $lspop->thn_renovasi_bng??'' }}" class="form-control form-control-sm">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-grop row">
                <label class="col-md-4 col-form-label text-right">Luas Bangunan</label>
                <div class="col-md-2">
                    <input type="text" name="luas_bng[]" class="form-control form-control-sm" value="{{ $lspop->luas_bng??$luas_bng }}">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-grop row">
                <label class="col-md-3 col-form-label text-right">Jumlah Lantai</label>
                <div class="col-md-2">
                    <input type="text" name="jml_lantai_bng[]" class="form-control form-control-sm" value="{{ $lspop->jml_lantai_bng??$jml_lantai }}">
                </div>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-md-6">
            <div class="form-grop row">
                <label class="col-md-4 col-form-label text-right">Kondisi</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="kondisi_bng[]" value="{{ $lspop->kondisi_bng??'' }}" class="form-control form-control-sm kondisi_bng" data-index="{{ $index }}" id="kondisi_bng_{{ $index }}">
                        </div>
                        <div class="col-md-10">
                            <span readonly class="form-control form-control-sm" id="kondisi_bng_keterangan_{{ $index }}"></span>

                        </div>
                    </div>

                </div>
            </div>

            <div class="form-grop row">
                <label class="col-md-4 col-form-label text-right">Konstruksi</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="jns_konstruksi_bng[]" value="{{ $lspop->jns_konstruksi_bng??'' }}" class="form-control form-control-sm jns_konstruksi_bng" data-index="{{ $index }}" id="jns_konstruksi_bng_{{ $index }}">
                        </div>
                        <div class="col-md-10">
                            <span readonly class="form-control form-control-sm" id="jns_konstruksi_bng_keterangan_{{ $index }}"></span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-grop row">
                <label class="col-md-4 col-form-label text-right">Atap </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="jns_atap_bng[]" value="{{ $lspop->jns_atap_bng??'' }}" class="form-control form-control-sm jns_atap_bng" data-index="{{ $index }}" id="jns_atap_bng_{{ $index }}">
                        </div>
                        <div class="col-md-10">
                            <span readonly class="form-control form-control-sm" id="jns_atap_bng_keterangan_{{ $index }}"></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-grop row">
                <label class="col-md-3 col-form-label text-right">Dinding</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="kd_dinding[]" value="{{ $lspop->kd_dinding??'' }}" class="form-control form-control-sm kd_dinding" data-index="{{ $index }}" id="kd_dinding_{{ $index }}">
                        </div>
                        <div class="col-md-10">
                            <span readonly class="form-control form-control-sm" id="kd_dinding_keterangan_{{ $index }}"></span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-grop row">
                <label class="col-md-3 col-form-label text-right">Lantai</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="kd_lantai[]" value="{{ $lspop->kd_lantai??'' }}" class="form-control form-control-sm kd_lantai" data-index="{{ $index }}" id="kd_lantai_{{ $index }}">
                        </div>
                        <div class="col-md-10">
                            <span readonly class="form-control form-control-sm" id="kd_lantai_keterangan_{{ $index }}"></span>

                        </div>
                    </div>
                </div>
            </div>

            <div class="form-grop row">
                <label class="col-md-3 col-form-label text-right">Langit - Langit</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="kd_langit_langit[]" value="{{ $lspop->kd_langit_langit??'' }}" class="form-control form-control-sm kd_langit_langit" data-index="{{ $index }}" id="kd_langit_langit_{{ $index }}">
                        </div>
                        <div class="col-md-10">
                            <span readonly class="form-control form-control-sm" id="kd_langit_langit_keterangan_{{ $index }}"></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-grop row">
                <label class="col-md-8 col-form-label text-right">Daya Listrik</label>
                <div class="col-md-4">
                    <input type="text" name="daya_listrik[]" value="{{ $lspop->daya_listrik??'' }}" id="daya_listrik_{{ $index }}" class="form-control form-control-sm">
                </div>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <td width="50%">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-grop row">
                            <label class="col-md-8 col-form-label text-right">AC Split</label>
                            <div class="col-md-4">
                                <input type="text" name="acsplit[]" value="{{ $lspop->acsplit??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-grop row">
                            <label class="col-md-8 col-form-label text-right">AC
                                Window</label>
                            <div class="col-md-4">
                                <input type="text" name="acwindow[]" value="{{ $lspop->acwindow??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td width="50%">
                <div class="form-grop row">
                    <label class="col-md-4 col-form-label text-right">AC Sentral</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2">
                                <input type="text" name="acsentral[]" value="{{ $lspop->acsentral??'' }}" class="form-control form-control-sm acsentral" data-index="{{ $index }}" id="acsentral_{{ $index }}">
                            </div>
                            <div class="col-md-10">
                                <span readonly class="form-control form-control-sm" id="acsentral_keterangan_{{ $index }}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <div class="row">
                    <div class="col-md-8 offset-4">
                        <strong>~Kolam Renang~</strong>
                    </div>
                    <div class="col-md-8">
                        <div class="form-grop row">
                            <label class="col-md-4 col-form-label text-right">Luas</label>
                            <div class="col-md-3">
                                <input type="text" name="luas_kolam[]" value="{{ $lspop->luas_kolam??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-4 col-form-label text-right">Finishing</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" name="finishing_kolam[]" value="{{ $lspop->finishing_kolam??'' }}" class="form-control form-control-sm finishing_kolam" data-index="{{ $index }}" id="finishing_kolam_{{ $index }}">
                                    </div>
                                    <div class="col-md-9">
                                        <span readonly class="form-control form-control-sm" id="finishing_kolam_keterangan_{{ $index }}"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td width="50%">
                <div class="row">
                    <div class="col-md-8 offset-4">
                        <strong>~Luas Perkerasan Halaman~</strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Ringan</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_perkerasan_ringan[]" value="{{ $lspop->luas_perkerasan_ringan??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Sedang</label>
                            <div class="col-md-6">
                                <input type="text" name="luas_perkerasan_sedang[]" value="{{ $lspop->luas_perkerasan_sedang??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-grop row">
                            <label class="col-md-8 col-form-label text-right">Berat</label>
                            <div class="col-md-4">
                                <input type="text" name="luas_perkerasan_berat[]" value="{{ $lspop->luas_perkerasan_berat??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-8 col-form-label text-right">Dgn penutup
                                lantai</label>
                            <div class="col-md-4">
                                <input type="text" name="luas_perkerasan_dg_tutup[]" value="{{ $lspop->luas_perkerasan_dg_tutup??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td width="30%">
                <div class="row">
                    <div class="col-md-8 offset-4">
                        <strong>~Lapangan Tenis~</strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <b class="text-center"> Dengan Lampu</b>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Beton</label>
                            <div class="col-md-4">
                                <input type="text" name="lap_tenis_lampu_beton[]" value="{{ $lspop->lap_tenis_lampu_beton??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Aspal</label>
                            <div class="col-md-4">
                                <input type="text" name="lap_tenis_lampu_aspal[]" value="{{ $lspop->lap_tenis_lampu_aspal??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Rumput</label>
                            <div class="col-md-4">
                                <input type="text" name="lap_tenis_lampu_rumput[]" value="{{ $lspop->lap_tenis_lampu_rumput??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <b class="text-center">Tanpa Lampu</b>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Beton</label>
                            <div class="col-md-4">
                                <input type="text" name="lap_tenis_beton[]" value="{{ $lspop->lap_tenis_beton??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Aspal</label>
                            <div class="col-md-4">
                                <input type="text" name="lap_tenis_aspal[]" value="{{ $lspop->lap_tenis_aspal??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Rumput</label>
                            <div class="col-md-4">
                                <input type="text" name="lap_tenis_rumput[]" value="{{ $lspop->lap_tenis_rumput??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td width="20%">
                <div class="row">
                    <div class="col-md-8 offset-4">
                        <strong>~Lift~</strong>
                    </div>
                    <div class="col-12">
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Penumpang</label>
                            <div class="col-md-4">
                                <input type="text" name="lift_penumpang[]" value="{{ $lspop->lift_penumpang??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Kapsul</label>
                            <div class="col-md-4">
                                <input type="text" name="lift_kapsul[]" value="{{ $lspop->lift_kapsul??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Barang</label>
                            <div class="col-md-4">
                                <input type="text" name="lift_barang[]" value="{{ $lspop->lift_barang??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td width="25%">
                <div class="row">
                    <div class="col-md-8 offset-4">
                        <strong>~Tangga Berjalan~</strong>
                    </div>
                    <div class="col-md-12">
                        <div class="form-grop row">
                            <label class="col-md-5 col-form-label text-right">Lebar < 0.80 M</label>
                                    <div class="col-md-4">
                                        <input type="text" name="tgg_berjalan_a[]" value="{{ $lspop->tgg_berjalan_a??'' }}" class="form-control form-control-sm">
                                    </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-5 col-form-label text-right">Lebar > 0.80
                                M</label>
                            <div class="col-md-4">
                                <input type="text" name="tgg_berjalan_b[]" value="{{ $lspop->tgg_berjalan_b??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-5 col-form-label text-right">Panjang
                                Pagar</label>
                            <div class="col-md-6">
                                <input type="text" name="pjg_pagar[]" value="{{ $lspop->pjg_pagar??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-5 col-form-label text-right">Bahan
                                Pagar</label>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" name="bhn_pagar[]" value="{{ $lspop->bhn_pagar??'' }}" class="form-control form-control-sm bhn_pagar" data-index="{{ $index }}" id="bhn_pagar_{{ $index }}">
                                    </div>
                                    <div class="col-md-9">
                                        <span readonly class="form-control form-control-sm" id="bhn_pagar_keterangan_{{ $index }}"></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <table class="table table-bordered">
        <tr>
            <td class="text-center" colspan="3"><b>Pemadam Kebakaran</b></td>
        </tr>
        <tr>
            <td width="50%">
                <div class="form-grop row">
                    <label class="col-md-4 col-form-label text-right">Hydrant</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" name="hydrant[]" class="form-control form-control-sm hydrant" data-index="{{ $index }}" id="hydrant_{{ $index }}" value="{{ $lspop->hydrant??'' }}">
                            </div>
                            <div class="col-md-9">
                                <span readonly class="form-control form-control-sm" id="hydrant_keterangan_{{ $index }}"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-grop row">
                    <label class="col-md-4 col-form-label text-right">Sprinkler</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" name="sprinkler[]" value="{{ $lspop->sprinkler??'' }}" class="form-control form-control-sm sprinkler" data-index="{{ $index }}" id="sprinkler_{{ $index }}">
                            </div>
                            <div class="col-md-9">
                                <span readonly class="form-control form-control-sm" id="sprinkler_keterangan_{{ $index }}"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-grop row">
                    <label class="col-md-4 col-form-label text-right">Fire Alarm</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" name="fire_alarm[]" value="{{ $lspop->fire_alarm??'' }}" class="form-control form-control-sm fire_alarm" data-index="{{ $index }}" id="fire_alarm_{{ $index }}">
                            </div>
                            <div class="col-md-9">
                                <span readonly class="form-control form-control-sm" id="fire_alarm_keterangan_{{ $index }}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="form-grop row">
                    <label class="col-md-6 col-form-label text-right">PABX</label>
                    <div class="col-md-4">
                        <input type="text" name="jml_pabx[]" value="{{ $lspop->jml_pabx??'' }}" class="form-control form-control-sm">
                    </div>
                </div>
                <div class="form-grop row">
                    <label class="col-md-6 col-form-label text-right">Sumur
                        Artesis</label>
                    <div class="col-md-4">
                        <input type="text" name="sumur_artesis[]" value="{{ $lspop->sumur_artesis??'' }}" class="form-control form-control-sm">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div id="jpb_3_8_{{ $index }}" class="jpb_3_8">
        <br>
        <table class="table table-bordered">
            <thead class="bg-info">
                <tr>
                    <th class="text-center" colspan="3">Data Tambahan utuk JPB 3/8</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="30%">
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Tinggi
                                Kolom</label>
                            <div class="col-md-4">
                                <input type="text" name="jpb3_8_tinggi_kolom[]" value="{{ $lspop->jpb3_8_tinggi_kolom??"" }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Lebar
                                Bentang</label>
                            <div class="col-md-4">
                                <input type="text" name="jpb3_8_lebar_bentang[]" value="{{ $lspop->jpb3_8_lebar_bentang??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </td>
                    <td width="30%">

                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Daya dukung
                                Lantai</label>
                            <div class="col-md-4">
                                <input type="text" name="jpb3_8_dd_lantai[]" value="{{ $lspop->jpb3_8_dd_lantai??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Keliling
                                Dinding</label>
                            <div class="col-md-4">
                                <input type="text" name="jpb3_8_kel_dinding[]" value="{{ $lspop->jpb3_8_kel_dinding??''  }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </td>
                    <td width="30%">
                        <div class="form-grop row">
                            <label class="col-md-6 col-form-label text-right">Mezzanine</label>
                            <div class="col-md-4">
                                <input type="text" name="jpb3_8_mezzanine[]" value="{{ $lspop->jpb3_8_mezzanine??'' }}" class="form-control form-control-sm">
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div id="non_standard_{{ $index }}" class="non_standard">
        <br>
        <table class="table table-borderless">
            <thead class="bg-info">
                <tr>
                    <td class="text-center">
                        <b>Bangunan Non Standard</b>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr id="non_5_13_{{ $index }}">
                    <td>
                        {{-- != (5& 13) --}}
                        <div class="form-grop row">
                            <label class="col-md-2 col-form-label text-right">Kelas Bangunan
                            </label>
                            <div class="col-md-3">
                                <select name="jpb_lain_kls_bng[]" id="jpb_lain_kls_bng_{{ $index }}" class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    @php
                                    $sjpb_lain_kls_bng=$lspop->jpb_lain_kls_bng??'';
                                    @endphp
                                    @for ($i = 1; $i <= 5; $i++) <option @if($sjpb_lain_kls_bng==$i) selected @endif value="{{ $i }}">Kelas
                                        {{ $i }}</option>
                                        @endfor
                                </select>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="jpb_5_{{ $index }}">
                    <td>
                        {{-- JPB5 --}}
                        <div class="form-grop row">
                            <label class="col-md-2 col-form-label text-right">Kelas Bangunan
                            </label>
                            <div class="col-md-3">
                                <select name="jpb5_kls_bng[]" id="jpb5_kls_bng_{{ $index }}" class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    @php
                                    $sjpb5_kls_bng=$lspop->jpb5_kls_bng??'';
                                    @endphp
                                    @for ($i = 1; $i <= 5; $i++) <option @if($sjpb5_kls_bng==$i) selected @endif value="{{ $i }}">Kelas
                                        {{ $i }}</option>
                                        @endfor
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-grop row">
                                    <label class="col-md-4 col-form-label text-right">Luas
                                        kamar</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb5_luas_kamar[]" value="{{ $lspop->jpb5_luas_kamar??'' }}" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-grop row">
                                    <label class="col-md-4 col-form-label text-right">Luas
                                        ruang
                                        lain</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb5_luas_rng_lain[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="jpb_7_{{ $index }}">
                    <td>
                        <div class="form-grop row">
                            <label class="col-md-2 col-form-label text-right">Jenis
                                Hotel</label>
                            <div class="col-md-4">
                                <select name="jpb7_jns_hotel[]" id="jpb7_jns_hotel_{{ $index }}" class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    <option value="1">Non Resort</option>
                                    <option value="2">Resort</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-grop row">
                            <label class="col-md-2 col-form-label text-right">Bintang</label>
                            <div class="col-md-4">
                                <select name="jpb7_bintang[]" id="jpb7_bintang_{{ $index }}" class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    @for ($i = 1; $i <= 5; $i++) <option value="{{ $i }}">
                                        {{ bintangHotel($i) }}</option>
                                        @endfor
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Jumlah
                                        Kamar</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb7_jml_kamar[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Luas
                                        Kamar</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb7_luas_kamar[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Luas
                                        Ruang
                                        Lain</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb7_luas_rng_lain[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="jpb_13_{{ $index }}">
                    <td>
                        {{-- apartemen / 13 --}}
                        <div class="form-grop row">
                            <label class="col-md-2 col-form-label text-right">Kelas
                                bangunan</label>
                            <div class="col-md-4">
                                <select name="jpb13_kls_bng[]" id="jpb13_kls_bng_{{ $index }}" class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    @for ($i = 1; $i <= 5; $i++) <option value="{{ $i }}">Kelas
                                        {{ $i }}</option>
                                        @endfor
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Jumlah
                                        Apartemen</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb13_jml[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Luas
                                        Kamar</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb13_luas_kamar[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Luas
                                        ruang
                                        lain</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb13_luas_rng_lain[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr id="jpb_15_{{ $index }}">
                    <td>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-grop row">
                                    <label class="col-md-6 col-form-label text-right">Kapasitas
                                        Tangki</label>
                                    <div class="col-md-4">
                                        <input type="text" name="jpb15_kapasitas_tangki[]" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-grop row">
                                    <label class="col-md-4 col-form-label text-right">Letak
                                        Tangki</label>
                                    <div class="col-md-4">
                                        <select name="jpb15_letak_tangki[]" id="jpb15_letak_tangki_{{ $index }}" class="form-control form-control-sm">
                                            <option value="">Pilih</option>
                                            <option value="1">Di atas tanah</option>
                                            <option value="2">Di bawah tanah</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    @endfor
    <style>
        .jpb_3_8,
        .non_standard {
            display: none
        }

    </style>
    <script>
        $(document).ready(function() {
            var arrayLspop = {
                '1': 'PEREKAMAN DATA'
                , '2': 'PEMUTAKHIRAN DATA'
                , '3': 'PENGHAPUSAN DATA'
                , '4': 'PENILAIAN INDIVIDU'
            }

            var arrayJpb = {
                "1": "PERUMAHAN"
                , "2": "PERKANTORAN SWASTA"
                , "3": "PABRIK"
                , "4": "TOKO/APOTIK/PASAR/RUKO"
                , "5": "RUMAH SAKIT/KLINIK"
                , "6": "OLAH RAGA/REKREASI"
                , "7": "HOTEL/WISMA"
                , "8": "BENGKEL/GUDANG/PERTANIAN"
                , "9": "GEDUNG PEMERINTAH"
                , "10": "LAIN-LAIN"
                , "11": "BANGUNAN TIDAK KENA PAJAK"
                , "12": "BANGUNAN PARKIR"
                , "13": "APARTEMEN"
                , "14": "POMPA BENSIN"
                , "15": "TANGKI MINYAK"
                , "16": "GEDUNG SEKOLAH"
            , }

            var arrayKondisi = {
                '1': 'SANGAT BAIK'
                , '2': 'BAIK'
                , '3': 'SEDANG'
                , '4': 'JELEK'
            , }


            var arrayKonsturksi = {
                '1': 'BAJA'
                , '2': 'BETON'
                , '3': 'BATU BATA'
                , '4': 'KAYU'
            , }

            var arrayAtap = {
                '1': 'DECRABON / BETON / GTG GLAZUR'
                , '2': 'GTG BETON / ALUMUNIUM'
                , '3': 'GTG BIASA / SIRAP'
                , '4': 'ASBES'
                , '5': 'SENG'
            }

            var arrayDinding = {
                '1': 'KACA / ALUMINIUM'
                , '2': 'BETON'
                , '3': 'BATU BATA / CONBLOK'
                , '4': 'KAYU'
                , '5': 'SENG'
            }

            var arrayLantai = {
                '1': 'MARMER'
                , '2': 'KERAMIK'
                , '3': 'TEROSO'
                , '4': 'UBIN PC / PAPANA'
                , '5': 'SEMEN'
            }

            var arrayLangit = {
                '1': 'AKUSTIK / JATI'
                , '2': 'TRIPLEK / ASBES BAMBU'
                , '3': 'TIDAK ADA '
            }

            var arrayNormal = {
                '1': 'ADA'
                , '2': 'TIDAK ADA'
            }

            var arrayKolam = {
                '1': 'DI PLESTER'
                , '2': 'DENGAN PELAPIS'
            , }

            var arrayPagar = {
                '1': 'Besi / Baja'
                , '2': 'Batu Bata / Batako'
            }

            // option list JPB




            $('.bhn_pagar').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                if (typeof arrayPagar[isi] === 'undefined') {
                    hasil = ""
                    $(this).val('')
                } else {
                    hasil = arrayPagar[isi]
                }
                $('#bhn_pagar_keterangan_' + index).html(hasil);
            })



            // console.log('jml jpb'+arrayJpb.length)
            // console.log(arrayJpb)





            $('.hydrant').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                if (typeof arrayNormal[isi] === 'undefined') {
                    hasil = ""
                    $(this).val('')
                } else {
                    hasil = arrayNormal[isi]
                }
                $('#hydrant_keterangan_' + index).html(hasil);
            })

            $('.sprinkler').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                if (typeof arrayNormal[isi] === 'undefined') {
                    hasil = ""
                    $(this).val('')
                } else {
                    hasil = arrayNormal[isi]
                }
                $('#sprinkler_keterangan_' + index).html(hasil);

            })
            $('.fire_alarm').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                if (typeof arrayNormal[isi] === 'undefined') {
                    hasil = ""
                    $(this).val('')
                } else {
                    hasil = arrayNormal[isi]
                }
                $('#fire_alarm_keterangan_' + index).html(hasil);

            })

            //  
            $('.finishing_kolam').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                if (typeof arrayKolam[isi] === 'undefined') {
                    hasil = ""
                    $(this).val('')
                } else {
                    hasil = arrayKolam[isi]
                }
                $('#finishing_kolam_keterangan_' + index).html(hasil);
            })

            $('.acsentral').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                if (typeof arrayNormal[isi] === 'undefined') {
                    hasil = ""
                    $(this).val('')
                } else {
                    hasil = arrayNormal[isi]
                }
                $('#acsentral_keterangan_' + index).html(hasil);
            })






            function keyupTransaksiLspop(isi, index, arrayLspop) {
                if (typeof arrayLspop[isi] === 'undefined') {
                    hasil = ""
                    $('#jns_transaksi_lspop_' + index).val('')
                } else {
                    hasil = arrayLspop[isi]
                }

                $('#jns_transaksi_lspop_keterangan_' + index).html(hasil);
            }


            $('.jns_transaksi_lspop').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                keyupTransaksiLspop(isi, index, arrayLspop);
            })

            function keyupJpb(isi, index, arrayJpb) {
                if (typeof arrayJpb[isi] === 'undefined') {
                    hasil = ""
                    $('#kd_jpb_' + index).val('')
                } else {
                    hasil = arrayJpb[isi]
                }
                $('#kd_jpb_keterangan_' + index).html(hasil)
            }




            function keyupKondisiBng(isi, index, arrayKondisi) {
                if (typeof arrayKondisi[isi] === 'undefined') {
                    hasil = ""
                    $('#kondisi_bng_' + index).val('')
                } else {
                    hasil = arrayKondisi[isi]
                }
                $('#kondisi_bng_keterangan_' + index).html(hasil)
            }

            $('.kondisi_bng').on('keyup', function() {
                isi = $(this).val()
                index = $(this).data('index')
                keyupKondisiBng(isi, index, arrayKondisi)
            })

            function keyupKonstruksi(isi, index, arrayKonsturksi) {
                if (typeof arrayKonsturksi[isi] === 'undefined') {
                    hasil = ""
                    $('#jns_konstruksi_bng_' + index).val('')
                } else {
                    hasil = arrayKonsturksi[isi]
                }
                $('#jns_konstruksi_bng_keterangan_' + index).html(hasil)
            }

            $('.jns_konstruksi_bng').on('keyup', function() {
                isi = $(this).val()
                index = $(this).data('index')
                keyupKonstruksi(isi, index, arrayKonsturksi)

            })

            function keyupAtap(isi, index, arrayAtap) {
                if (typeof arrayAtap[isi] === 'undefined') {
                    hasil = ""
                    $('#jns_atap_bng_' + 1).val('')
                } else {
                    hasil = arrayAtap[isi]
                }
                $('#jns_atap_bng_keterangan_' + index).html(hasil)
            }

            $('.jns_atap_bng').on('keyup', function() {
                isi = $(this).val()
                index = $(this).data('index')
                keyupAtap(isi, index, arrayAtap)
            })

            function keyupDinding(isi, index, arrayDinding) {
                if (typeof arrayDinding[isi] === 'undefined') {
                    hasil = ""
                    $('#kd_dinding_' + index).val('')
                } else {
                    hasil = arrayDinding[isi]
                }
                $('#kd_dinding_keterangan_' + index).html(hasil)
            }


            $('.kd_dinding').on('keyup', function() {
                isi = $(this).val()
                index = $(this).data('index')
                keyupDinding(isi, index, arrayDinding)
            })

            function keyupLantai(isi, index, arrayLantai) {
                if (typeof arrayLantai[isi] === 'undefined') {
                    hasil = ""
                    $('#kd_lantai_' + index).val('')
                } else {
                    hasil = arrayLantai[isi]
                }
                $('#kd_lantai_keterangan_' + index).html(hasil)
            }

            $('.kd_lantai').on('keyup', function() {
                isi = $(this).val()
                index = $(this).data('index')
                keyupLantai(isi, index, arrayLantai)
            })

            function keyupLangit(isi, index, arrayLangit) {
                if (typeof arrayLangit[isi] === 'undefined') {
                    hasil = ""
                    $('#kd_langit_langit_' + index).val('')
                } else {
                    hasil = arrayLangit[isi]
                }
                $('#kd_langit_langit_keterangan_' + index).html(hasil)
            }

            $('.kd_langit_langit').on('keyup', function() {
                isi = $(this).val()
                index = $(this).data('index')
                keyupLangit(isi, index, arrayLangit)
            })

            var idx = "{{ $index }}";
            var $non_standard = $('#non_standard_' + idx);

            $('#jpb_3_8_' + idx).hide();

            $non_standard.hide();

            // child Non standart
            $('#non_5_13_' + idx).hide();
            $('#jpb_5_' + idx).hide();
            $('#jpb_7_' + idx).hide();
            $('#jpb_13_' + idx).hide();
            $('#jpb_15_' + idx).hide();

            function inArray(needle, haystack) {
                var length = haystack.length;
                for (var i = 0; i < length; i++) {
                    if (haystack[i] == needle) return true;
                }
                return false;
            }


            let count = "{{ $count }}"
            for (let i = 1; i <= count; i++) {
                index = i
                
                optionJpb="<option value='' >Pilih</option>";
                var jpb = "{{ trim($jpb) }}"
                $.each(arrayJpb, function(i, r) {
                    var select=""
                    if(i==jpb){
                        select="selected"
                    }
                    optionJpb += "<option "+select+" value='" + i + "'>"+i+" - " + r + "</option>";  
                })
                $('#kd_jpb_' + i).append(optionJpb)

                

                optionLspop="";
                $.each(arrayLspop, function(i, r) { 
                    var select=""
                    if(i==2){
                        select="selected"
                    }
                    optionLspop += "<option "+select+" value='" + i + "'>"+i+" - " + r + "</option>";
                    
                })
                $('#jns_transaksi_lspop_' + i).append(optionLspop)

                // jns_transaksi_lspop


                /*    isi_ = $('#kd_jpb_' + i).val()
                   if (isi_ == '1') {
                       
                       keyupJpb(1, i, arrayJpb)
                       var jpb = "{{ $jpb }}"
                       var jenis_ajuan = "{{ $jenis_ajuan }}"

                       if (jpb == '1' && jenis_ajuan == '3') {
                           $('#kondisi_bng_' + i).val(2).trigger('keyup')
                           //  keyupKondisiBng(2, i, arrayKondisi)
                           $('#jns_konstruksi_bng_' + i).val(3).trigger('keyup')
                           //  keyupKonstruksi(3, i, arrayKonsturksi)
                           $('#jns_atap_bng_' + i).val(3).trigger('keyup')
                           //  keyupAtap(3, i, arrayAtap)
                           $('#kd_dinding_' + i).val(3).trigger('keyup')
                           //  keyupDinding(3, i, arrayDinding)
                           $('#kd_lantai_' + i).val(2).trigger('keyup')
                           //  keyupLantai(2, i, arrayLantai)
                           $('#kd_langit_langit_' + i).val(2).trigger('keyup')
                           //  keyupLangit(2, i, arrayLangit)
                       }
                   }
                   $('#jns_transaksi_lspop_' + i).val('2').trigger('keyup')
                   $('#kd_jpb_' + i).trigger('keyup')
                   
                   
                   $('#jns_transaksi_lspop_' + i).val('2').trigger('keyup') */
                //  perubahan form 
            }

            $('.kd_jpb').on('keyup', function() {
                isi = $(this).val();
                index = $(this).data('index')
                keyupJpb(isi, index, arrayJpb)
                // console.log('keyup jpb '+isi+' '+index)
                // console.log(isi)

                if (isi == '1') {
                    $('#kondisi_bng_' + index).val(2).trigger('keyup')
                    $('#jns_konstruksi_bng_' + index).val(3).trigger('keyup')
                    $('#jns_atap_bng_' + index).val(3).trigger('keyup')
                    $('#kd_dinding_' + index).val(3).trigger('keyup')
                    $('#kd_lantai_' + index).val(2).trigger('keyup')
                    $('#kd_langit_langit_' + index).val(2).trigger('keyup')
                } else {
                    $('#kondisi_bng_' + index).val('').trigger('keyup')
                    $('#jns_konstruksi_bng_' + index).val('').trigger('keyup')
                    $('#jns_atap_bng_' + index).val('').trigger('keyup')
                    $('#kd_dinding_' + index).val('').trigger('keyup')
                    $('#kd_lantai_' + index).val('').trigger('keyup')
                    $('#kd_langit_langit_' + index).val('').trigger('keyup')
                }

                //  
                var idx = index;
                var $non_standard = $('#non_standard_' + idx);

                console.log($non_standard)

                var jpb_ = parseInt($(this).val());
                //  tambahan untuk JPB 3 / 8
                if (jpb_ == '3' || jpb_ == '8') {
                    $('#jpb_3_8_' + idx).show();

                } else {
                    $('#jpb_3_8_' + idx).hide();
                }

                //  untuk non standard
                if (jpb_ == '2' ||
                    jpb_ == '9' ||
                    jpb_ == '4' ||
                    jpb_ == '5' ||
                    jpb_ == '6' ||
                    jpb_ == '7' ||
                    jpb_ == '13' ||
                    jpb_ == '15' ||
                    jpb_ == '16') {
                    $non_standard.show();

                    if (jpb_ == '6' ||
                        jpb_ == '16' ||
                        jpb_ == '4' ||
                        jpb_ == '2' ||
                        jpb_ == '9') {
                        $('#non_5_13_' + idx).show();
                    } else {
                        $('#non_5_13_' + idx).hide();
                    }

                    if (jpb_ == '5') {
                        $('#jpb_5_' + idx).show()
                    } else {
                        $('#jpb_5_' + idx).hide()
                    }
                    if (jpb_ == '7') {
                        $('#jpb_7_' + idx).show()
                    } else {
                        $('#jpb_7_' + idx).hide()
                    }
                    if (jpb_ == '13') {
                        $('#jpb_13_' + idx).show()
                    } else {
                        $('#jpb_13_' + idx).hide()
                    }
                    if (jpb_ == '15') {
                        $('#jpb_15_' + idx).show()
                    } else {
                        $('#jpb_15_' + idx).hide()
                    }

                } else {
                    $non_standard.hide();
                    $('#non_5_13_' + idx).hide();
                    $('#jpb_5_' + idx).hide();
                    $('#jpb_7_' + idx).hide();
                    $('#jpb_13_' + idx).hide();
                    $('#jpb_15_' + idx).hide();
                }


            })

            $('.kd_jpb').on('change', function() {
                //  var idx = "{{ $index }}";
                //  isi = $(this).val();
                idx = $(this).data('index')
                var $non_standard = $('#non_standard_' + idx);

                var jpb_ = parseInt($(this).val());
                //  tambahan untuk JPB 3 / 8
                if (jpb_ == '3' || jpb_ == '8') {
                    $('#jpb_3_8_' + idx).show();

                } else {
                    $('#jpb_3_8_' + idx).hide();
                }

                //  untuk non standard
                if (jpb_ == '2' ||
                    jpb_ == '9' ||
                    jpb_ == '4' ||
                    jpb_ == '5' ||
                    jpb_ == '6' ||
                    jpb_ == '7' ||
                    jpb_ == '13' ||
                    jpb_ == '15' ||
                    jpb_ == '16') {
                    $non_standard.show();

                    if (jpb_ == '6' ||
                        jpb_ == '16' ||
                        jpb_ == '4' ||
                        jpb_ == '2' ||
                        jpb_ == '9') {
                        $('#non_5_13_' + idx).show();
                    } else {
                        $('#non_5_13_' + idx).hide();
                    }

                    if (jpb_ == '5') {
                        $('#jpb_5_' + idx).show()
                    } else {
                        $('#jpb_5_' + idx).hide()
                    }
                    if (jpb_ == '7') {
                        $('#jpb_7_' + idx).show()
                    } else {
                        $('#jpb_7_' + idx).hide()
                    }
                    if (jpb_ == '13') {
                        $('#jpb_13_' + idx).show()
                    } else {
                        $('#jpb_13_' + idx).hide()
                    }
                    if (jpb_ == '15') {
                        $('#jpb_15_' + idx).show()
                    } else {
                        $('#jpb_15_' + idx).hide()
                    }

                } else {
                    $non_standard.hide();
                    $('#non_5_13_' + idx).hide();
                    $('#jpb_5_' + idx).hide();
                    $('#jpb_7_' + idx).hide();
                    $('#jpb_13_' + idx).hide();
                    $('#jpb_15_' + idx).hide();
                }

            })

            $('.kd_jpb').trigger('change')
            // $("input[type=text]").trigger('keyup')
        })

    </script>
