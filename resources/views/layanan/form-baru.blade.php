@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <ul class="nav nav-pills p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"> <i class="fas fa-book"></i> Form Layanan Pribadi </a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"> <i class="fas fa-book"></i> Form Layanan Badan</a></li>
                                <!-- <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab"> <i class="fas fa-book"></i> Form Layanan Kolektif</a></li> -->
                            </ul>
                        </div>
                        <div class="card-body no-padding">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="card">
                                        <div class="card-body  p-1">
                                            @include('layanan/form_input_pribadi')
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="card">
                                        <div class="card-body p-1">
                                            @include('layanan/form_input_badan')
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- <div class="tab-pane" id="tab_3">
                                    <div class="card">
                                        <div class="card-body">
                                            @include('layanan/form_input_kolektif')
                                        </div>
                                    </div>
                                    
                                </div> -->
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            // Proses Pribadi/ Badan
            const changeData=function(e,async_status=true){
                let uri=e.attr("data-change").split("#"),
                    target=e.closest('form').find("[name='"+e.attr("data-target-name")+"']");
                    target.appendData(uri[1],{kd_kecamatan:e.val()},false,async_status,true);
                return target;  
            };
            const calc=function(e){
                let form=e.closest('form'),
                source=e.attr('data-calc'),
                result=0,
                a=form.find('[name="luas_bumi"]').val(),
                b=form.find('[name="hasil_pecah_hasil_gabung"]').val();
                if(form.find('[name="'+source+'"]').val()=='6'){
                    if(a-b>0){
                        result=a-b;
                    };
                };
                if(form.find('[name="'+source+'"]').val()=='7'){
                    result=parseInt(a)+parseInt(b);
                }
                form.find('[name="sisa_pecah_total_gabung"]').val(result);
            };
            $(document).on("keyup", "[data-calc]", function (evt) {
                let e=$(this);
                calc(e);
            });
            $(document).on("change", ".data-hide-list", function (evt) {
                let typeform={
                    '1':'A',
                    '2':'G',
                    '3':'B',
                    '4':'H',
                    '5':'I',
                    '6':'C',
                    '7':'D',
                    '8':'E',
                    '9':'F',
                    '10':'G'
                };  
                let e=$(this),
                    val=e.val(),
                    form=e.closest("form"),
                    target=form.find("[data-append-show='"+e.attr('name')+"'][data-when~='"+typeform[val]+"']"),
                    unTarget=form.find("[data-append-show='"+e.attr('name')+"']");
                unTarget.addClass('hidden');
                target.removeClass('hidden');
                form.find('[data-required]').removeAttr('required');
                form.find('[data-required~="'+typeform[val]+'"]').attr('required','required');
                if(typeform[val]=='F'||typeform[val]=='G'||typeform[val]=='H'){
                    form.find('[data-readonly~="'+typeform[val]+'"] input').attr('readonly','readonly');
                    form.find('[data-unreadonly~="'+typeform[val]+'"] input').removeAttr('readonly');
                    form.find('[data-norequiredhide="'+typeform[val]+'"]').addClass('hidden');
                }else{
                    form.find('[data-readonly] input').removeAttr('readonly');
                    form.find('[data-norequiredhide').removeClass('hidden');
                }
                if(typeform[val]!='A'){
                    form.find('.focusList').focus();
                }
            });
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this);
                changeData(e);
            });
            $('.change-next').on('select2:select', function() {
                $(this).closest('.card-body').find('.change-next-append').focus();
            });
            $(".inputdate").inputmask({ alias: "datetime", inputFormat: "dd - mm - yyyy"});
            $(".nik").inputmask('9999999999999999');
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
            $(".email").inputmask({   alias: "email"  });
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: value??{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $(document).on("keyup", "[name='nop']", function (evt) {
                let e=$(this);
                if(e.inputmask('unmaskedvalue').length==18){
                    e.closest('.form-group').find('[data-nop]').trigger('click');
                }
            });
            $(document).on("click", "[data-nop]", function (evt) {
                let e=$(this),
                card=e.closest(".card"),
                form=e.closest('form'),
                name=e.attr('data-nop'),
                tInput=card.find('[name="'+name+'"]'),
                v=tInput.val(),
                vmask=tInput.inputmask('unmaskedvalue'),
                jenis=$("[name='jenis_layanan_id']").val(),
                extra=(jenis)?"&jenis="+jenis:"",
                uri='api/ceknop?nop='+v+extra;
                if(vmask.length==18){
                    asyncData(uri,{}).then((response) => {
                        if(!response.status){
                            Swal.fire({ icon: 'error', html: response.msg}).then(()=>{
                                card.find('[name="'+name+'"]').focus();
                            }); 
                        };
                        $.each(response.data,(x,y)=>{
                            let setdata=card.find('[name="'+x+'"]');
                            let duplicate=setdata.attr('data-usedto');
                            if(setdata){
                                let jenis=$('[name="jenis_layanan_id"]').val();
                                if(!setdata.hasClass('no_replace')||jenis!='9'){
                                    setdata.val(y);
                                }
                                //setdata.attr('readonly','readonly');
                            };
                            if(duplicate!=undefined){
                               form.find('[name="'+duplicate+'"]').val(y);
                            };
                        });
                        $.each(response.tag,(x,y)=>{
                            let settag=form.find('[data-name="'+x+'"]');
                            if(settag){
                                settag.html(y);
                            };
                        });
                    });
                    $(".thirdFocus").focus();
                }else{
                    tInput.focus();
                }
            });
            $(document).ready(function() {
                $(".onFocus").first().focus();
            });
            jQuery.extend(jQuery.expr[":"], {
                focusable: function(el, index, selector) {
                    return $(el).is("a:not(:hidden), button:not(:hidden), :input:not(:hidden), [tabindex]:not(:hidden)");
                }
            });
            $(document).on("keypress", "input:not(:hidden),select:not(:hidden),textarea:not(:hidden)", function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    // console.log($(this).val());
                    if(($(this).val()!=null&&$(this).val()!="")||$(this).hasClass('no-required')){
                        var $canfocus = $(":focusable");
                        var index = $canfocus.index(document.activeElement) + 1;
                        if (index >= $canfocus.length) index = 0;
                        $canfocus.eq(index).focus();
                    }
                }
            });
            $(document).on("click", "[data-nik]", function (evt) {
                let e=$(this),
                    form=e.closest("form"),
                    tInput=e.closest('.input-group').find('input'),
                    v=tInput.inputmask('unmaskedvalue'),
                    type=e.attr('data-nik'),
                    uri='api/ceknik?nik='+v;
                let listlayanan={
                    'nik':'nik',
                    'kec_name':'kecamatan',
                    'nama_lgkp':'nama',
                    'alamat':'alamat',
                    'tgl_lhr':'tanggal_lahir_wp',
                    'kel_name':'kelurahan',
                    'kab_name':'dati2',
                    'prop_name':'propinsi',
                    'agama_wp':'agama_wp',
                    'blok_kav_no_wp':'blok_kav_no_wp',
                    'email':'email',
                    'rt_wp_baru':'rt_wp_baru',
                    'rw_wp_baru':'rw_wp_baru',
                    //'kec_name':'kecamatan_wp',
                    //'nama_lgkp':'nama_wp',
                    //'alamat':'alamat_wp',
                    'nomor_telepon':'nomor_telepon',
                    'no_rt':'rt_wp',
                    'no_rw':'rw_wp',
                    //'kel_name':'kelurahan_wp',
                    //'kab_name':'dati2_wp',
                    //'prop_name':'propinsi_wp',
                };
                if(v.length==16){
                    // Swal.fire({
                    //     title: 'Cek NIK',
                    //     html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    //     showConfirmButton: false,
                    //     allowOutsideClick: false,
                    // });
                    asyncData(uri,{}).then((response) => {
                        if(response.kode){
                            $.each(listlayanan,(x,y)=>{
                                if(response.raw[x]){
                                    let setdata=form.find('[name="'+y+'"]');
                                    let setdatawp=form.find('[name="'+y+'_wp"]');
                                    let val=response.raw[x];
                                    if(setdata){
                                        if(setdata.hasClass('inputdate')){
                                            val=new Date(val);
                                        }
                                        setdata.val(val);
                                        setdatawp.val(val);
                                        // console.log('[name="'+y+'"] =>'+val);
                                    };
                                };
                            });
                            
                        }
                        // swal.close();
                        // $(".secondFocus").focus();
                    }).catch((error)=>{
                        // Swal.fire({ icon: 'error', html: response.keterangan}).then(()=>{
                        //     $(".secondFocus").focus();
                        // });
                    });
                    // swal.close();
                    $(".secondFocus").focus();
                }else{
                    tInput.focus();
                };
            });
            $(document).on("click", ".btn-used", function (evt) {
                let e=$(this),
                    form=e.closest("form"),
                    listtarget=form.find("[data-usedto]");
                if(listtarget.length){
                    listtarget.map((index, e) =>{
                        let source=$(e),
                            sourceval=source.val();
                        if(sourceval){
                            let target=form.find("[name='"+$(e).attr("data-usedto")+"']");
                            if(target.length){
                                if(target.hasClass("select")){
                                    if(target.hasClass('data-duplicate')){
                                        target.html(source.html());
                                    };
                                    target.val(sourceval);
                                    target.trigger('change.select2');
                                    target.closest(".selectappend").removeClass("selectappend");
                                }else{
                                    target.val(sourceval);
                                };
                            };
                        };
                    });
                };
            });
            $(document).on("click", "[btn-delete]", function (evt) {
                let e=$(this);
                e.closest('tr').remove();
            });
            $(document).on("click", "#tambah-dokumen", function (evt) {
                let source=$(this).closest('.data-source');
                let sourcelist=source.find('[data-name]');
                let result=true,resultdata=[];
                const d = new Date();
                let time = d.getTime();
                let btndel="<button type='button' class='btn btn-warning btn-block' btn-delete='#dokumen-table' ><i class='far fa-trash-alt'></i> Hapus</buton>";
                let cloneFile;
                sourcelist.map((index,e)=>{
                    let source=$(e);
                    let tag;
                    if(source.val()){
                        let textval=source.val();
                        if(source.hasClass('file')){
                            cloneFile=source.clone().attr('name','dokumen['+time+']['+source.attr('data-name')+']').addClass('hidden');
                            tag=textval.replace(/.*(\/|\\)/, '')+'<span data-id="'+time+'"></span>';
                        }else{
                            tag='<input name="dokumen['+time+']['+source.attr('data-name')+']" value="'+source.val()+'" type="hidden"> '+textval;
                        }
                        resultdata.push(tag);
                    }else{
                        result=false;
                    };
                });
                if(result){
                    $("#dokumen-table tbody").append("<tr><td></td><td>"+resultdata.join('</td><td>')+"</td><td class='p-in-4'>"+btndel+"</td></tr>");
                    $("#dokumen-table tbody [data-id='"+time+"']").html(cloneFile);
                }
                $('.reFocus').focus();
                source.find('.change-next-append').val('');
            });
            let resetForm=function(e){
                e[0].reset();
                e.find('#dokumen-table tbody').html('');
                e.find('.select-reset').html('');
                e.find('.select').trigger('change.select2');
                e.find('[data-hide]').trigger('change');
                e.find('.data-hide-list').trigger('change');
            };
            $(document).on("keyup", "[data-show='luas_bng']", function (evt) {
                let e = $(this),
                    target=$("[data-when~='luas_bng']");
                    if(e.val()>0){
                        target.removeClass('hidden');
                    }else{
                        target.addClass('hidden');
                    }
            })
            const openData= function (verb, url, data, target) {
                var form = document.createElement("form");
                form.action = url;
                form.method = verb;
                form.target = target || "_self";
                if (data) {
                    for (var key in data) {
                        var input = document.createElement("textarea");
                        input.name = key;
                        input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
                        form.appendChild(input);
                    }
                };
                form.style.display = 'none';
                document.body.appendChild(form);
                form.submit();
            };
            $(document).on("submit", "#form-badan", function (evt) {
                evt.preventDefault();
                let e=$(this);
                if(!e.find('#dokumen-table tbody>tr').length){
                    return  Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Dokumen Pendukung Harap di isi!!!'
                    });
                }
                Swal.fire({
                    title: e.attr('data-loader'),
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            //response.msg
                            return Swal.fire({ icon: 'warning', text:response.msg??'layanan registrasi gagal.' });
                        }
                        resetForm(e);
                        if(response.curl){
                            Swal.fire({
                                title: 'Info',
                                // html:'<p>'+response.msg+', Download SK Pembetulan</p> <a href="{{ url("laporan_input/cetak_pdf?nomor_layanan=") }}'+response.curl+
                                html:response.msg,
                                showConfirmButton: false,
                            }).then(()=>{
                                $(".onFocus").first().focus();
                            });
                            //openData('get','laporan_input/cetak_pdf',{'nomor_layanan':response.curl}, '_blank');
                        }else{
                            Swal.fire({ icon: 'success', text: response.msg});
                        }
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            });
            $(document).on("submit", "#form-pribadi", function (evt) {
                evt.preventDefault();
                let e=$(this);
                if(!e.find('#dokumen-table tbody>tr').length){
                    return  Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Dokumen Pendukung Harap di isi!!!'
                    });
                }
                Swal.fire({
                    title: e.attr('data-loader'),
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            //response.msg
                            return Swal.fire({ icon: 'warning', text:response.msg??'layanan registrasi gagal.' });
                        }
                        resetForm(e);
                        if(response.curl){
                            Swal.fire({
                                title: 'Info',
                                // html:'<p>'+response.msg+', Download SK Pembetulan</p> <a href="{{ url("laporan_input/cetak_pdf?nomor_layanan=") }}'+response.curl+
                                html:response.msg,
                                showConfirmButton: false,
                            }).then(()=>{
                                $(".onFocus").first().focus();
                            });
                            //openData('get','laporan_input/cetak_pdf',{'nomor_layanan':response.curl}, '_blank');
                        }else{
                            Swal.fire({ icon: 'success', text: response.msg});
                        }
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            });

            // Proses Kolektif 
            $(document).on("change", "[name='import_excel']", function (evt) {
                if(!$(this).hasExtension(['.xls', '.xlsx', '.csv'])){
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Format file tidak sesuai!!!'
                    });
                };
                let e=$(this),
                    form=e.closest("form"),
                    uri=form.attr("action").split("#"),
                    name=e.val()??'Import Excel';
                let namefile=evt.target.files[0].name;
                $("label[for='"+e.attr("name")+"']").html(namefile);
                Swal.fire({
                    title: 'Proses menampilkan dan validasi data dari Excel',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                form.ajaxSubmit({
                    type: "post",
                    url: uri[1] ,
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                           return Swal.fire({ icon: 'error', text: response.msg});
                        }
                        pageAppend('preview_kolektif',
                                response.preview_kolektif,
                                response.jumlah_nop,
                                response.preview_kolektif_invalid,
                                response.jumlah_nop_invalid);
                        swal.close();
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultErrorPreview});
                    }
                });
            });
            let pageAppend=function(Appendroot,tbody,jumlah_nop,tbody_false,jumlah_nop_false){
                $.each(tbody,(x,y)=>{
                    $("[data-append='"+Appendroot+"'] #table-kolektif-"+x+">tbody").html(y.table);
                    $("[data-append='"+Appendroot+"'] #tab-count-"+x+"").html(y.count);
                    //
                });
                if(tbody_false){
                    $("[data-append='"+Appendroot+"_invalid'] tbody").html(tbody_false);
                };
                $("[name='jumlah_nop']").html(jumlah_nop);
                $("[name='jumlah_nop_invalid']").html(jumlah_nop_false);
            };
            $(document).on("click", "[data-uri]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-uri").split("#"),
                    form=$(e.attr("data-form")),
                    length=form.find("[data-name='nik_wp']").length;
                if(!length){
                    return Swal.fire({icon: 'warning',title: 'Oops...',text: 'Data Masih Kosong'});
                }
                Swal.fire({ 
                    icon: 'warning',
                    html: '<p>Pastikan seluruh data layanan kolektif sudah benar.</p><p>Simpan daftar layanan kolektif?</p>',
                    showCancelButton: true,
                    confirmButtonText: "Simpan",
                    cancelButtonText: "Batal",
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses pembuatan data Layanan kolektif',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        let empty='<tr class="null"><td colspan="26" class="dataTables_empty text-center"> Data Masih Kosong</td></tr>',
                            emptyO='<tr class="null"><td colspan="26" class="dataTables_empty text-center">',
                            emptyC='</td></tr>';
                        form.ajaxSubmit({
                            type: "post",
                            url: uri[1] ,
                            dataType: "json",
                            success: function (response) {
                                if(!response.status){
                                    return Swal.fire({ icon: 'error', text: response.msg});
                                }
                                Swal.fire({ icon: 'success', text: response.msg}).then((reload)=>{ 
                                   pageAppend('preview_kolektif',{
                                       '1':{'table':emptyO+' Data '+$('#1-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '2':{'table':emptyO+' Data '+$('#2-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '3':{'table':emptyO+' Data '+$('#3-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '4':{'table':emptyO+' Data '+$('#4-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '5':{'table':emptyO+' Data '+$('#5-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '6':{'table':emptyO+' Data '+$('#6-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '7':{'table':emptyO+' Data '+$('#7-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '8':{'table':emptyO+' Data '+$('#8-tab>span').html()+' Masih Kosong '+emptyC,'count':0},
                                       '9':{'table':emptyO+' Data '+$('#9-tab>span').html()+' Masih Kosong '+emptyC,'count':0}
                                   },0,empty,0);
                                });
                            },
                            error: function (x, y) {
                                //x.responseJSON.message??
                                Swal.fire({ icon: 'error', text: defaultError});
                            }
                        });
                    }
                });
            });
            $(document).on("change", "[data-name='file']", function (evt) {
                let e=$(this);
                if(!e.hasExtension(['.pdf','jpg','png'])){
                    e.val('');
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Format file tidak sesuai!!!'
                    });
                };
            });
            $(document).on("click", "[data-layanan]", function (evt) {
                Swal.fire({
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let e=$(this),
                    value=e.attr('data-layanan'),
                    uri="{{ url('layanan/detail')}}",
                    modal=$('#modal-detail-kolektif');
                asyncData(uri,{id:value}).then((response) => {
                    if(!response.status){
                        return Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            text: response.msg
                        });
                    }
                    $.each(response.data,(x,y)=>{
                        let setdata=modal.find('[name="'+x+'"]');
                        if(setdata.length){
                            setdata.val(y);
                        };
                    });
                    swal.close();
                    modal.modal('show')
                });
            });
        });
    </script>
@endsection