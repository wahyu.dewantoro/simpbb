<div class="card card-primary card-outline card-tabs no-radius no-margin card-reponse" data-card='side'>
  <div class="card-header p-0">
    <h3 class="card-title p-3"><b>Detail Data Layanan</b></h3>
    <div class="card-tools p-1">
      <button type="button" class="btn btn-primary" data-card="remove"><i class="fas fa-times"></i> Tutup</button>     
    </div>
  </div>
  <div class="card-body p-0 clone-hide" data-remote='clone'>
    <div class="row">
        <div class="col-md-4 pr-0">
            <div class="card rounded-0 m-0">
                <div class="card-header">
                    <h3 class="card-title bold-text" data-name='jenis_layanan'></h3>
                </div>
                <div class="card-body p-1">
                    <table data-name='data-layanan' class='table table-borderless table-sm  no-margin'></table>
                </div>
            </div>
        </div>
        <div class="col-md-8 pl-0">
            <div class="card rounded-0 m-0">
                <div class="card-header d-flex">
                    <h3 class="card-title bold-text">Detail NOP (Subjek & Objek)</h3>
                </div>
                <div class="card-body p-1">
                    <div class="row p-0 m-0">
                        <div class="col-md-5 pl-0">
                            <p class="bg-warning color-palette p-1 m-0">
                                <b>Subjek</b>
                            </p>
                            <table data-name='data-subjek' class='table table-borderless table-sm no-margin'></table>
                        </div>
                        <div class="col-md-7 pl-0">
                            <p class="bg-warning color-palette p-1 m-0">
                                <b>Objek</b>
                            </p>
                            <table data-name='data-objek' class='table table-borderless table-sm no-margin'></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-12">
            <div class="card rounded-0 m-0">
                <div class="card-header d-flex">
                    <h3 class="card-title bold-text">Dokumen Pendukung </h3>
                </div>
                <div class="card-body">
                <form action="layanan/update" enctype="multipart/form-data" method="post" class="form-horizontal">
                    @csrf
                    <textarea class="hidden" name="nomor_layanan" data-name="nomor_layanan"></textarea>
                    <div class="callout callout-warning col-sm-12">
                        <ol class="m-0">
                        <li>Pastikan Format Dokumen berjenis <b>[*.pdf,*.png,*.jpg]</b></li>
                        </ol>
                    </div>
                    <div class="col-md-12 p-0 row data-source">
                        <div class="form-group col-md-3">
                            <div class='input-group'>
                                <input type="file" class="form-control file" data-name='file'>
                                <div class="input-group-append">
                                    <span class="input-group-text">Dokumen </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class='input-group'>
                                <select class="form-control"  data-placeholder="Jenis Dokumen." data-name='nama_dokumen'>
                                    @foreach ($jenisDokumen as $key=>$dokumen)
                                        <option value="{{ $key }}"> {{ $dokumen }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class='input-group'>
                                <input type="text" class="form-control" placeholder="Keterangan Dokumen" data-name="keterangan">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class='input-group'>
                                <button class="tambah-dokumen btn btn-info btn-block" type="button"><i class="fas fa-plus-square"></i> Tambah</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card-split-row"></div>
                    </div>
                    <table class='table table-bordered table-striped  no-margin dokumen-table table-counter'>
                        <thead>
                            <tr>
                                <td>NO</td>
                                <td>Dokumen</td>
                                <td>Jenis Dokumen</td>
                                <td>keterangan</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody data-name='data-dokumen'></tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12"> <button type="submit" class="btn btn-block btn-primary">Update Berkas</button> </div>
                    </div>
                </div>
                </form>
            </div>
        </div> -->
    </div>
  </div>
  <!-- auto addd -->
</div>