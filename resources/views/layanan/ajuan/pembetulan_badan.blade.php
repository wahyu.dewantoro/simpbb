
<form action="{{url('pembetulan/store_pembetulan')}}" enctype="multipart/form-data" method="post" class="form-horizontal" data-loader="Proses layanan (Pribadi)" id="form-badan">
    @csrf
    <input type="hidden" name="jenisObjek" value="2">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label p-1">Jenis*</label>
                                <div class="col-md-9 p-0">
                                    <select name="jenis_layanan_id" class="form-control-sm form-control data-hide-list onFocus" required placeholder="Pilih Jenis Pembetulan">
                                        @foreach ($Jenis_layanan as $layanan)
                                            <option value="{{ $layanan->id }}">
                                                {{ $layanan->nama_layanan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 pr-1">
            <div class="card mb-1 ">
                <div class="card-header d-flex">
                    <h3 class="card-title">Detail NOP</h3>
                </div>
                <div class="card-body p-1">
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-4 col-form-label col-form-label-sm mb-0">NOP*</label>
                                <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP" required>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm" data-nop-badan='nop' aria-expanded="false">
                                        <i class='fas fa-plus-square'></i> Cek NOP
                                    </button>
                                    <button type="button" class="btn-used hidden">
                                        <i class='fas fa-plus-square'></i> Gunakan Data NOP
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1"> <b>Data Subjek Pajak</b> </p>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NIB/No Akta</span>
                                </div>
                                <input type="text" name="nik_badan" class="form-control-sm form-control nik" placeholder="NIB/No Akta" readonly  data-usedto="nik_badan_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nama Badan</span>
                                </div>
                                <input type="text" name="nama_badan" class="form-control-sm form-control " placeholder="Nama Badan" required readonly  data-usedto="nama_badan_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">No. SBU</span>
                                </div>
                                <input type="text" name="nomor_sbu_badan_badan" class="form-control-sm form-control " placeholder="No. SBU" required readonly  data-usedto="nomor_sbu_badan_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">No. Kemenhum</span>
                                </div>
                                <input type="text" name="nomor_kemenhum_badan" class="form-control-sm form-control " placeholder="No. Kemenhum" required readonly  data-usedto="nomor_kemenhum_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">ALAMAT</span>
                                </div>
                                <textarea type="text" name="alamat_wp" class="form-control-sm form-control no-min" placeholder="Alamat WP Badan" required readonly  data-usedto="alamat_wp_pembetulan" > </textarea>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">TELP</span>
                                </div>
                                <input type="text" name="telp_wp" class="form-control-sm form-control " placeholder="Telp WP Badan" required readonly  data-usedto="telp_wp_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RT</span>
                                </div>
                                <input type="text" name="rt_wp" class="form-control-sm form-control numeric " placeholder="RT WP Badan" required readonly  data-usedto="rt_wp_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RW</span>
                                </div>
                                <input type="text" name="rw_wp" class="form-control-sm form-control numeric" placeholder="RW WP Badan" required readonly  data-usedto="rw_wp_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KELURAHAN</span>
                                </div>
                                <input type="text" name="kelurahan_wp" class="form-control-sm form-control "placeholder="Kelurahan WP Badan" required readonly  data-usedto="kelurahan_wp_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KECAMATAN</span>
                                </div>
                                <input type="text" name="kecamatan_wp" class="form-control-sm form-control "placeholder="Kecamatan WP Badan" required readonly  data-usedto="kecamatan_wp_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KOTA/KABUPATEN </span>
                                </div>
                                <input type="text" name="dati2_wp" class="form-control-sm form-control" placeholder="Kota/Kabupaten WP Badan" required readonly  data-usedto="dati2_wp_pembetulan" >
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">PROPINSI</span>
                                </div>
                                <input type="text" name="propinsi_wp" class="form-control-sm form-control" placeholder="Propinsi WP Badan" required readonly  data-usedto="propinsi_wp_pembetulan" >
                            </div>
                        </div>
                    </div>
                    <div class="card-split-row mb-1"></div>
                    <p class="bg-warning color-palette p-1 mb-1"> <b>Data Objek Pajak</b> </p>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">ALAMAT</span>
                                </div>
                                <textarea type="text" name="nop_alamat" class="form-control-sm form-control no-min" placeholder="Alamat Lengkap OP" readonly data-usedto="nop_alamat_pembetulan" > </textarea>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RT</span>
                                </div>
                                <input type="text" name="nop_rt" class="form-control-sm form-control numeric" placeholder="RT OP" readonly data-usedto="nop_rt_pembetulan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RW</span>
                                </div>
                                <input type="text" name="nop_rw" class="form-control-sm form-control numeric" placeholder="RW OP" readonly data-usedto="nop_rw_pembetulan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KELURAHAN</span>
                                </div>
                                <input type="text" name="nop_kelurahan" class="form-control-sm form-control "placeholder="Kelurahan OP" readonly data-usedto="nop_kelurahan_pembetulan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KECAMATAN</span>
                                </div>
                                <input type="text" name="nop_kecamatan" class="form-control-sm form-control "placeholder="Kecamatan OP" readonly data-usedto="nop_kecamatan_pembetulan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KOTA/KABUPATEN </span>
                                </div>
                                <input type="text" name="nop_dati2" class="form-control-sm form-control" placeholder="Kota/Kabupaten OP" readonly data-usedto="nop_dati2_pembetulan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">PROPINSI</span>
                                </div>
                                <input type="text" name="nop_propinsi" class="form-control-sm form-control" placeholder="Propinsi OP" readonly data-usedto="nop_propinsi_pembetulan">
                            </div>
                        </div>
                    </div>
                    <div class="card-split-row hidden mb-1" data-append-show='jenis_layanan_id'  data-when='B'></div>
                    <div class='row hidden'  data-append-show='jenis_layanan_id'  data-when='B'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KELAS TANAH</span>
                                </div>
                                <input type="text" name="kd_kls_tanah" class="form-control-sm form-control "  placeholder="Kelas Tanah" readonly data-usedto="kd_kls_tanah_pembetulan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">KELAS BNG</span>
                                </div>
                                <input type="text" name="kd_kls_bng" class="form-control-sm form-control "  placeholder="Kelas Bangunan" readonly data-usedto="kd_kls_bng_pembetulan">
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='B' data-norequiredhide="E">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">LUAS BUMI</span>
                                </div>
                                <input type="text" name="luas_bumi" class="form-control-sm form-control numeric"  maxlength="10" data-calc="jenis_layanan_id" placeholder="Luas Bumi" readonly data-usedto="luas_bumi_pembetulan">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row hidden"data-append-show='jenis_layanan_id'  data-when='B' data-norequiredhide="E">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NJOP BUMI</span>
                                </div>
                                <input type="text" name="njop_bumi" class="form-control-sm form-control numeric" placeholder="NJOP Bumi" readonly data-usedto="njop_bumi_pembetulan">
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='B' data-norequiredhide="E">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">LUAS BANGUNAN</span>
                                </div>
                                <input type="text" name="luas_bng" class="form-control-sm form-control numeric" placeholder="Luas Bangunan"  maxlength="10" readonly data-usedto="luas_bng_pembetulan">
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='B' data-norequiredhide="E">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NJOP BANGUNAN</span>
                                </div>
                                <input type="text" name="njop_bng" class="form-control-sm form-control numeric" placeholder="NJOP Bangunan" readonly data-usedto="njop_bng_pembetulan" >
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div class="col-md-7 pl-1">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title">Data Pembetulan</h3>
                </div>
                <div class="card-body p-1">
                    <div class="callout callout-warning col-sm-12 p-1 mb-1" data-append="callout">
                        <span>Tanda <b>*</b> pada inputan  Harus di isi.</span>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1"> <b>Data Subjek Pajak</b> </p>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">NIB/No Akta*</label>
                                <div class="col-md-9 p-0 input-group">
                                    <input type="text"  name="nik_badan_pembetulan" class="form-control-sm form-control thirdFocus nik"placeholder="NIB/No Akta" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Nama Badan*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"  name="nama_badan_pembetulan" class="form-control-sm form-control " maxlength="30" placeholder="Nama Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">No. SBU*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"  name="nomor_sbu_badan_pembetulan" class="form-control-sm form-control "placeholder="No. SBU" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">No. Kemenhum*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"  name="nomor_kemenhum_pembetulan" class="form-control-sm form-control " placeholder="No. Kemenhum" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">ALAMAT*</label>
                                <div class="col-md-9 p-0">
                                   <textarea type="text"   name="alamat_wp_pembetulan" class="form-control-sm form-control no-min" maxlength="30" placeholder="Alamat WP Badan" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">NO TELP.*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"   name="telp_wp_pembetulan" class="form-control-sm form-control numeric " maxlength="20" placeholder="No Telp. WP Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">RT* (Max 3 digit)</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"  name="rt_wp_pembetulan"  maxlength="3" class="form-control-sm form-control numeric"placeholder="RT WP Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">RW* (Max 2 digit)</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"   name="rw_wp_pembetulan"  maxlength="2"  class="form-control-sm form-control "placeholder="RW WP Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KELURAHAN*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"  name="kelurahan_wp_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Kelurahan WP Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KECAMATAN*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text"  name="kecamatan_wp_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Kecamatan WP Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KOTA/KABUPATEN*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="dati2_wp_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Kota/Kabupaten WP Badan"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">PROPINSI*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="propinsi_wp_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Propinsi WP Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-split-row mb-1"></div>
                    <p class="bg-warning color-palette p-1 mb-1"> <b>Data Objek Pajak</b> </p>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">ALAMAT*</label>
                                <div class="col-md-9 p-0" data-readonly="E">
                                   <textarea type="text"  readonly name="nop_alamat_pembetulan" maxlength="30" class="form-control-sm form-control no-min" placeholder="Alamat OP" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">RT* (Max 3 digit)</label>
                                <div class="col-md-9 p-0" data-readonly="E">
                                    <input type="text"  readonly name="nop_rt_pembetulan"  maxlength="3"  class="form-control-sm form-control numeric"placeholder="RT OP"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">RW* (Max 2 digit)</label>
                                <div class="col-md-9 p-0" data-readonly="E">
                                    <input type="text" readonly name="nop_rw_pembetulan"  maxlength="2"  class="form-control-sm form-control numeric"placeholder="RW OP" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KELURAHAN</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nop_kelurahan_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Kelurahan OP" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KECAMATAN</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nop_kecamatan_pembetulan"  maxlength="30" class="form-control-sm form-control "placeholder="Kecamatan OP" readonly> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KOTA/KABUPATEN</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nop_dati2_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Kota/Kabupaten OP" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">PROPINSI</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nop_propinsi_pembetulan" maxlength="30" class="form-control-sm form-control "placeholder="Propinsi OP" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-split-row hidden" data-append-show='jenis_layanan_id'  data-when='B'></div>
                    <div class='row hidden'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KELAS TANAH</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="kd_kls_tanah_pembetulan" class="form-control-sm form-control "placeholder="Kelas Tanah" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row hidden'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">KELAS BNG</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="kd_kls_bng_pembetulan" class="form-control-sm form-control "placeholder="Kelas Bangunan" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='B' data-norequiredhide="E">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">LUAS BUMI*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="luas_bumi_pembetulan" class="form-control-sm form-control numeric" maxlength="10" data-calc="jenis_layanan_id" placeholder="Luas Bumi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" >
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">NJOP Bumi</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="njop_bumi_pembetulan" class="form-control-sm form-control numeric"  placeholder="NJOP Bumi" readonly>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='B' data-norequiredhide="E">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">LUAS BANGUNAN*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="luas_bng_pembetulan" class="form-control-sm form-control numeric"  maxlength="10" placeholder="Luas Bangunan">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">NJOP Bangunan</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="njop_bng_pembetulan" class="form-control-sm form-control numeric" placeholder="NJOP Bangunan" readonly >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-12 mb-1">
            <div class="card p-1 mb-0">
                <div class="card-header">
                    <h3 class="card-title">Riwayat Pembayaran Belum lunas</h3>
                </div>
                <div class="card-body p-1">
                    <table class="table table-bordered table-striped table-counter table-sm">
                        <thead class='text-center'>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">NOP</th>
                                <th rowspan="2">Tahun</th>
                                <th rowspan="2">Nama</th>
                                <th rowspan="2">PBB</th>
                                <th colspan="3">Pembayaran</th>
                                <th rowspan="2">Keterangan</th>
                            </tr>
                            <tr>
                                <th>Pokok</th>
                                <th>Denda</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody data-name="riwayat_pembayaran" class="tag-html"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-1">
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-2 col-form-label">Keterangan</label>
                                    <textarea name="keterangan" class="form-control-sm form-control triggerHeight ">KETERANGAN</textarea>
                                    <!-- <input name="keterangan" class="form-control-sm form-control numeric"> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layanan/part_dokumen')
    </div>
    <div class="card-footer p-1">
        <div class="row">
            <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
            <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Simpan</button> </div>
        </div>
    </div>
</form>