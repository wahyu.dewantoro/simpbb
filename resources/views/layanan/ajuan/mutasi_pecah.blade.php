
<form action="{{url('layanan/store')}}" enctype="multipart/form-data" method="post" class="form-horizontal" data-loader="Proses layanan (Pribadi)" id="form-pribadi">
    @csrf
    <input type="hidden" name="jenisObjek" value="1">
    <input type="hidden" name="jenis_layanan_id" value="{{$Jenis_layanan->id}}">
    <div class="row">
        <div class="col-md-7 pr-1">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"><b>Data Pemohon</b></h3>
                </div>
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">NIK*</label>
                                <input type="text" name="nik" class="form-control-sm form-control form-control form-control onFocus nik"placeholder="Nomor Induk Kependudukan" data-usedto="nik_wp" required>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm" data-nik="layanan" aria-expanded="false">
                                        <i class='fas fa-search'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Nama*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nama" class="form-control form-control-sm secondFocus"placeholder="Nama" data-usedto="nama_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <!-- <label class="col-md-3 col-form-label col-form-label-sm mb-0">Agama*</label>
                                <div class="col-md-3 p-0">
                                    <select name="agama_wp" class="form-control form-control-sm" data-placeholder="Pilih Agama" required >
                                        <option value="">-- Pilih Agama --</option>
                                        @ foreach ($ jenisAgama as $ key=>$ val)
                                            <option value=" { { $ key }}">
                                                { { $ val }}
                                            </option>
                                        @ endforeach
                                    </select>
                                </div> text-right  -->
                                <label class="col-md-3 col-form-labelcol-form-label-sm mb-0">No. Telp*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_telepon" class="form-control form-control-sm numeric "  maxlength="15" placeholder="No Telp." required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Alamat*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="alamat" class="form-control form-control-sm no-min" placeholder="Alamat" data-usedto="alamat_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1" >
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Blok/Kav/No</label>
                                <div class="col-md-9 p-0">
                                <!-- data-required='A' -->
                                    <input type="text" class="form-control-sm form-control no-min" name="blok_kav_no" placeholder="Blok/Kav/No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kelurahan*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="kelurahan" class="form-control form-control-sm"placeholder="Kelurahan" data-usedto="kelurahan_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kecamatan*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="kecamatan" class="form-control form-control-sm"placeholder="Kecamatan" data-usedto="kecamatan_wp" required>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kota/Kabupaten*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="dati2" class="form-control form-control-sm"placeholder="Kota/Kabupaten" data-usedto="dati2_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Propinsi*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="propinsi" class="form-control form-control-sm"placeholder="Propinsi" data-usedto="propinsi_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">Email*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="email" class="form-control-sm form-control no-required email" placeholder="Email" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">pengurusan*</label>
                                <div class="col-md-9 p-0">
                                    <select name="pengurus"  required class="form-control form-control-sm" data-placeholder="Pilih Pengurusan">
                                        <option value="1">Sendiri</option>
                                        <option value="2">di kuasakan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="text" name="tanggal_lahir_wp" class="form-control inputdate hidden">
                </div>
            </div>
        </div>
        <div class="col-md-5 pl-1">
            <div class="card mb-1">
                <div class="card-header d-flex">
                    <h3 class="card-title"><b>NOP Induk</b></h3>
                </div>
                <div class="card-body p-1">
                    <div class='row hidden ' > <!---->
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <input type="text" name="nik_wp" class="form-control nik" placeholder="Nomor Induk Kependudukan">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class='input-group'>
                                <input type="text" name="rt_wp" maxlength="3" class="form-control numeric " placeholder="RT OP">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class='input-group'>
                                <input type="text" name="rw_wp"  maxlength="2" class="form-control numeric" placeholder="RW OP">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class='input-group'>
                                <input type="text" name="kelurahan_wp" class="form-control "placeholder="Kelurahan">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class='input-group'>
                                <input type="text" name="kecamatan_wp" class="form-control "placeholder="Kecamatan" >
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class='input-group'>
                                <input type="text" name="dati2_wp" class="form-control" placeholder="Kota/Kabupaten">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class='input-group'>
                                <input type="text" name="propinsi_wp" class="form-control" placeholder="Propinsi">
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-4 col-form-label col-form-label-sm mb-0">NOP*</label>
                                <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm" data-nop='nop' aria-expanded="false">
                                        <i class='fas fa-search'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1">
                        <b>Subjek</b>
                    </p>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NIK </span>
                                </div>
                                <input type="text" name="nik_wp" class="form-control-sm form-control " placeholder="NIK WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nama </span>
                                </div>
                                <input type="text" name="nama_wp" class="form-control  form-control-sm" placeholder="Nama WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1 hidden">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Alamat WP</span>
                                </div>
                                <input type="text" name="alamat_wp" class="form-control form-control-sm" placeholder="Alamat WP" >
                            </div>
                        </div>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1">
                        <b>Objek</b>
                    </p>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Alamat</span>
                                </div>
                                <input type="text" name="nop_alamat" class="form-control form-control-sm" placeholder="Alamat Objek">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Blok/Kav/No</span>
                                </div>
                                <input type="text" name="blok_kav_no_op" class="form-control form-control-sm" placeholder="Blok/Kav/No" >
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RT</span>
                                </div>
                                <input type="text" name="nop_rt" maxlength="3" class="form-control form-control-sm numeric" placeholder="RT Objek">
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RW</span>
                                </div>
                                <input type="text" name="nop_rw"  maxlength="2" class="form-control form-control-sm numeric" placeholder="RW Objek">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kelurahan</span>
                                </div>
                                <input type="text" name="nop_kelurahan"  maxlength="2" class="form-control form-control-sm" placeholder="Kelurahan">
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kecamatan</span>
                                </div>
                                <input type="text" name="nop_kecamatan"  maxlength="2" class="form-control form-control-sm" placeholder="Kecamatan">
                            </div>
                        </div>
                        <div class="form-group col-md-6 hidden">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kelas Tanah</span>
                                </div>
                                <input type="text" name="kd_kls_tanah" class="form-control form-control-sm" placeholder="Kelas Tanah">
                            </div>
                        </div>
                        <div class="form-group col-md-6 hidden">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kelas Bangunan</span>
                                </div>
                                <input type="text" name="kd_kls_bng" class="form-control form-control-sm" placeholder="Kelas Bangunan">
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1" >
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Luas Tanah</span>
                                </div>
                                <input type="text" name="luas_bumi"  maxlength="10" class="form-control form-control-sm numeric" data-calc="jenis_layanan_id" placeholder="Luas Tanah" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1" >
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Luas Bangunan</span>
                                </div>
                                <input type="text" name="luas_bng" class="form-control form-control-sm numeric"  maxlength="10" placeholder="Luas Bangunan" required data-show="luas_bng">
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-when="luas_bng">
                        <!-- data-norequiredhide="E" -->
                        <div class="form-group col-md-12">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label">JPB*</label>
                                <div class="col-md-8 p-0">
                                    <select name="kelompok_objek_id"   class="form-control form-control-sm select"  data-placeholder="Jenis Penggunaan Bangunan"  >
                                        <option value="">-- Jenis --</option>
                                        @foreach ($KelompokObjek as $kelompok)
                                            <option @if (request()->get('kelompok') == $kelompok->id)  selected @endif value="{{ $kelompok->id }}">
                                                {{ $kelompok->nama_kelompok }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden">
                        <div class="form-group col-md-12">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label">Lokasi Objek*</label>
                                <div class="col-md-8 p-0">
                                <select name="lokasi_objek_id"   class="form-control form-control-sm select"  data-placeholder="Pilih Lokasi Objek." >
                                        <option value="">-- Lokasi --</option>
                                        @foreach ($LokasiObjek as $lokasi)
                                            <option @if (request()->get('lokasi') == $lokasi->id)  selected @endif value="{{ $lokasi->id }}">
                                                {{ $lokasi->nama_lokasi }}
                                            </option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>  
        <div class="col-md-12">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"><b>Data Pecah</b></h3>
                </div>
                <div class="card-body p-1">
                    <div class='row' data-table-add="noppecah">
                        <div class="col-md-6 pr-1">
                            <p class="bg-warning color-palette p-1 mb-1 pl-2 "> <b> Subjek</b> </p>
                            <div class="row">
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">NIK</label>
                                        <input type="text" name="nikadd" class="form-control form-control-sm nik thirdFocus rePecah" data-input="nik_wp_pecah" data-label="NIK">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-info btn-sm" data-nikadd="layanan" aria-expanded="false">
                                            <i class='fas fa-search'></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Nama </label>
                                        <input type="text" name="nama_wpadd" class="form-control form-control-sm secondFocusAdd" data-input="nama_wp_pecah" data-label="Nama">
                                    </div>
                                </div>
                                <!-- <div class="form-group col-md-6 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-6 col-form-label col-form-label-sm mb-0">Agama</label>
                                        <select name="agama_wpadd" class="form-control form-control-sm option" data-input="agama_wp_pecah" data-label="Agama"> >
                                            <option value="">-- Pilih Agama --</option>
                                            @ foreach ($ jenisAgama as $ key=>$ val)
                                                <option value="{ { $ key }}">
                                                    { { $ val }}
                                                </option>
                                            @ endforeach
                                        </select>
                                    </div>
                                </div> -->
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">No Telp</label>
                                        <input type="text" name="notelp_wpadd"  class="form-control form-control-sm numeric" data-input="notelp_wp_pecah" maxlength="15" data-label="No. Telp">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Alamat </label>
                                        <input type="text" name="alamat_wpadd" class="form-control form-control-sm no-min" data-input="alamat_wp_pecah" data-label="Alamat">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Blok/Kav/No </label>
                                        <input type="text" name="blok_kav_no_wpadd" class="form-control form-control-sm no-min no-required" data-input="blok_kav_no_wp_pecah" data-label="Blok/Kav/No ">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-6 col-form-label col-form-label-sm mb-0">RT</label>
                                        <input type="text" name="rt_wpadd" class="form-control form-control-sm rt numeric" data-input="rt_wp_pecah" maxlength="3" data-label="RT">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">RW</label>
                                        <input type="text" name="rw_wpadd" class="form-control form-control-sm rw numeric" data-input="rw_wp_pecah" maxlength="2" data-label="RW">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kelurahan</label>
                                        <input type="text" name="kelurahan_wpadd" class="form-control form-control-sm " data-input="kelurahan_wp_pecah" data-label="Kelurahan">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kecamatan</label>
                                        <input type="text" name="kecamatan_wpadd" class="form-control form-control-sm " data-input="kecamatan_wp_pecah" data-label="Kecamatan">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kota/Kabupaten</label>
                                        <input type="text" name="dati2_wpadd" class="form-control form-control-sm " data-input="dati2_wp_pecah" data-label="Kota / Kabupaten">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-3 col-form-label col-form-label-sm mb-0">Propinsi</label>
                                        <input type="text" name="propinsi_wpadd" class="form-control form-control-sm " data-input="propinsi_wp_pecah" data-label="Propinsi">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <p class="bg-warning color-palette p-1 mb-1 pl-2 "> <b> Objek</b> </p>
                            <div class="row">
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm'>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Kecamatan</span>
                                        </div>
                                        <input type="text" name="nop_kecamatanadd" class="form-control form-control-sm no-back change" readonly data-input="kecamatan_pecah" data-label="Kecamatan">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm'>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Kelurahan</span>
                                        </div>
                                        <input type="text" name="nop_kelurahanadd" class="form-control form-control-sm no-back change" readonly data-input="kelurahan_pecah" data-label="Kelurahan">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-4 col-form-label col-form-label-sm mb-0">Alamat</label>
                                        <input type="text" name="nop_alamatadd" class="form-control form-control-sm no-back change no-min" data-input="alamat_pecah" data-label="Alamat">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-4 col-form-label col-form-label-sm mb-0">Blok/Kav/No</label>
                                        <input type="text" name="blok_kav_no_opadd" class="form-control form-control-sm no-back change no-min no-required no-required" data-input="blok_kav_no_pecah" data-label="Blok/Kav/No">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-6 col-form-label col-form-label-sm mb-0">RT</label>
                                        <input type="text" name="nop_rtadd" class="form-control form-control-sm no-back numeric change rt" maxlength="3" data-input="rt_pecah" data-label="RT">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-6 col-form-label col-form-label-sm mb-0">RW</label>
                                        <input type="text" name="nop_rwadd" class="form-control form-control-sm no-back numeric change rw" maxlength="2" data-input="rw_pecah" data-label="RW">
                                    </div>
                                </div>

                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-4 col-form-label col-form-label-sm mb-0">Luas Bumi</label>
                                        <input type="text" name="luas_bumiadd" class="form-control form-control-sm numeric "  maxlength="10" data-input="luas_bumi_pecah" data-label="Luas Bumi">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-4 col-form-label col-form-label-sm mb-0">Luas Bangunan</label>
                                        <input type="text" name="luas_bngadd" class="form-control form-control-sm  numeric"  maxlength="10" data-input="luas_bng_pecah" value="0" data-label="Luas Bangungan">
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-4 col-form-label col-form-label-sm mb-0">JPB</label>
                                        <select name="kelompok_objek_idadd"   class="form-control-sm form-control no-required option" data-input="kelompok_objek_id_pecah" data-placeholder="Jenis Penggunaan Bangunan" data-label="Jenis Penggunaan Bangunan">
                                            <option value="">-- Jenis --</option>
                                            @foreach ($KelompokObjek as $kelompok)
                                                <option @if (request()->get('kelompok') == $kelompok->id)  selected @endif value="{{ $kelompok->id }}">
                                                    {{ $kelompok->nama_kelompok }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 mb-1">
                                    <div class='input-group input-group-sm p-0'>
                                        <label class="col-md-4 col-form-label col-form-label-sm mb-0">Lokasi Objek</label>
                                        <select name="lokasi_objek_idadd"   class="form-control-sm form-control no-required option" data-input="lokasi_objek_idpecah" data-placeholder="Pilih Lokasi Objek." data-label="Lokasi Objek">
                                            <option value="">-- Lokasi --</option>
                                            @foreach ($LokasiObjek as $lokasi)
                                                <option @if (request()->get('lokasi') == $lokasi->id)  selected @endif value="{{ $lokasi->id }}">
                                                    {{ $lokasi->nama_lokasi }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12"><div class="card-split-row mb-1"></div></div>
                        <div class="form-group col-md-4 pull-right mb-1 ml-auto">
                            <div class='input-group input-group-sm'>
                                <button type="button" class="btn btn-info btn-block btn-sm" data-objek-add='noppecah' aria-expanded="false">
                                    <i class='fas fa-plus-square'></i> Tambah
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class='row body-scroll m-0'>
                        <table class="table table-bordered table-striped table-counter mb-0 table-sm">
                            <thead class='text-center'>
                                <tr>
                                    <th class="number">No</th>
                                    <th>Objek Pajak</th>
                                    <th>Wajib Pajak</th>
                                    <th>Domisili WP</th>
                                    <th class="option">-</th>
                                </tr>
                            </thead>
                            <tbody data-name="list_pecah" class="tag-html">
                                <tr class="null">
                                    <td colspan="5" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card mb-1">
                <div class="card-body p-2">
                    <div class='row'>
                        <div class="form-group col-md-5">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label">Luas Hasil Pecah*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="hasil_pecah_hasil_gabung" data-calc="jenis_layanan_id" class="form-control form-control-sm numeric" placeholder="0" readonly>
                                </div>  
                            </div>
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label">Sisa Hasil Pecah*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="sisa_pecah_total_gabung" class="form-control form-control-sm numeric" data-calc-result="jenis_layanan_id" placeholder="0"  readonly>
                                </div>  
                            </div>
                        </div>
                        <div class="form-group col-md-7">
                            <div class='input-group'>
                                <label class="col-md-2 col-form-label">Keterangan</label>
                                    <textarea name="keterangan" class="form-control-sm form-control triggerHeight  ">KETERANGAN</textarea>
                                    <!-- <input name="keterangan" class="form-control-sm form-control numeric"> -->
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Status NOP</label>
                                <select name="status_nop" class="form-control form-control-sm">
                                    <option value="1">Non Aktif</option>
                                    <option value="2">NOP Fasum</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="col-md-12">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"><b>Riwayat Pembayaran Belum lunas</b></h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12  body-scroll">
                            <table class="table table-bordered table-striped table-counter table-sm">
                                <thead class='text-center'>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">NOP</th>
                                        <th rowspan="2">Tahun</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">PBB</th>
                                        <th colspan="3">Pembayaran</th>
                                        <th rowspan="2">Keterangan</th>
                                    </tr>
                                    <tr>
                                        <th>Pokok</th>
                                        <th>Denda</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody data-name="riwayat_pembayaran" class="tag-html">
                                    <tr class="null">
                                        <td colspan="9" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layanan/part_dokumen')
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
            <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Simpan</button> </div>
        </div>
    </div>
</form>