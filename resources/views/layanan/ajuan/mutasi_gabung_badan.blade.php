
<form action="{{url('layanan/store')}}" enctype="multipart/form-data" method="post" class="form-horizontal" data-loader="Proses layanan (Pribadi)" id="form-form-badan">
    @csrf
    <input type="hidden" name="jenisObjek" value="2">
    <input type="hidden" name="jenis_layanan_id" value="{{$Jenis_layanan->id}}">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"> <b>Daftar NOP yang di gabung</b> </h3>
                </div>
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-4 col-form-label col-form-label-sm mb-0">NOP </label>
                                <input type="text" name="nopadd" class="form-control-sm form-control numeric nop_full thirdFocus" placeholder="NOP Gabung">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm" data-nop-add='nopadd' aria-expanded="false">
                                        <i class='fas fa-search'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-sm table-striped table-counter">
                        <thead class='text-center'>
                            <tr>
                                <th rowspan='2'>No</th>
                                <th rowspan='2'>NIB/No. Akta</th>
                                <th rowspan='2'>NAMA Badan</th>
                                <th rowspan='2'>NOP</th>
                                <th colspan='4'>Lokasi Objek</th>
                                <th colspan='2'>Luas</th>
                                <th rowspan='2'>-</th>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <th>RT/RW</th>
                                <th>Kelurahan</th>
                                <th>Kecamatan</th>
                                <th>Bumi</th>
                                <th>Bangunan</th>
                            </tr>
                        </thead>
                        <tbody data-name="list_nop" class="tag-html">
                            <tr class="null">
                                <td colspan="11" class="dataTables_empty text-center"> Data Masih Kosong</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1 pt-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label p-1">Luas Hasil Gabung*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="hasil_pecah_hasil_gabung" data-calc="jenis_layanan_id" class="form-control-sm form-control numeric" placeholder="0" >
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row hidden">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label p-1">Total Hasil Gabung*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="sisa_pecah_total_gabung" class="form-control-sm form-control numeric" data-calc-result="jenis_layanan_id" placeholder="0"  readonly>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="col-md-12">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"><b>Riwayat Pembayaran Belum lunas</b></h3>
                </div>
                <div class="card-body p-1 " >
                    <div class="row">
                        <div class="col-sm-3 br-0">
                            <ul class="nav flex-column nav-tabs nav-tabs-right h-100" data-target="tab" >
                                <!-- <li class="nav-item mb-1 main-reset"><a  class="main-tab nav-link tabs-h active" href="#tab_1" data-toggle="tab"> <i class="fas fa-book"></i> Nop Awal </a></li> -->
                            </ul>
                        </div>
                        <div class="col-sm-9 pl-0">
                            <div class="tab-content" data-target="table">
                                <!-- <div class="main-tab tab-pane active main-reset" id="tab_1">
                                    <div class="card">
                                        <div class="col-sm-12  body-scroll">
                                            <table class="table table-bordered table-striped table-counter table-sm">
                                                <thead class='text-center'>
                                                    <tr>
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">NOP</th>
                                                        <th rowspan="2">Tahun</th>
                                                        <th rowspan="2">Nama</th>
                                                        <th rowspan="2">PBB</th>
                                                        <th colspan="3">Pembayaran</th>
                                                        <th rowspan="2">Keterangan</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Pokok</th>
                                                        <th>Denda</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-name="riwayat_pembayaran" class="tag-html">
                                                    <tr class="null">
                                                        <td colspan="9" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 pr-1">
            <div class="card mb-1">
                <div class="card-header">
                    <h3 class="card-title"><b>Data Hasil Gabung</b></h3>
                </div>
                <div class="card-body p-1">
                    <p class="bg-warning color-palette p-1 mb-1">
                        <b>Subjek</b>
                    </p>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1 col-form-label-sm mb-0">NIB/No. Akta*</label>
                                <input type="text" name="nik" class="form-control-sm form-control form-control form-control onFocus nik" maxlenght="16" placeholder="NIB/No. Akta" data-usedto="nik_wp" required>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm" data-nik="layanan" aria-expanded="false">
                                        <i class='fas fa-search'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Nama Badan*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nama" class="form-control-sm form-control secondFocus "placeholder="Nama" data-usedto="nama_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <!-- <label class="col-md-3 col-form-label p-1">Agama*</label>
                                <div class="col-md-3 p-0">
                                    <select name="agama_wp" class="form-control-sm form-control" data-placeholder="Pilih Agama" required >
                                        <option value="">-- Pilih Agama --</option>
                                        @ foreach ($ jenisAgama as $ key=>$ val)
                                            <option value="{ { $ key }}">
                                                { { $ val }}
                                            </option>
                                        @ endforeach
                                    </select>
                                </div> -->
                                <label class="col-md-3 col-form-label p-1">No. Telp*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_telepon" class="form-control-sm form-control numeric "  maxlength="20" placeholder="No Telp." required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Alamat*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="alamat" class="form-control-sm form-control no-min" placeholder="Alamat" data-usedto="alamat_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Blok/Kav/No</label>
                                <div class="col-md-9 p-0">
                                <!-- data-required='A' -->
                                    <input type="text" class="form-control-sm form-control no-min" name="blok_kav_no" placeholder="Blok/Kav/No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">RT *</label>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="rt_wp_baru"  maxlength="3" class="form-control-sm form-control rt numeric" placeholder="RT">
                                </div>
                                <label class="col-md-2 col-form-label p-1 text-right">RW *</label>
                                <div class="col-md-4 p-0">
                                    <input type="text" name="rw_wp_baru"  maxlength="2" class="form-control-sm form-control rw numeric" placeholder="RW">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Kelurahan*</label>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="kelurahan" class="form-control-sm form-control "placeholder="Kelurahan" data-usedto="kelurahan_wp" required>
                                    <!-- <select name="kelurahan"  required class="form-control-sm form-control select select-reset" data-usedto="kelurahan_wp"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                                        <option value="">-- Kelurahan --</option>
                                    </select> -->
                                </div>
                                <label class="col-md-2 col-form-label p-1 text-right">Kecamatan*</label>
                                <div class="col-md-4 p-0">
                                    <input type="text" name="kecamatan" class="form-control-sm form-control "placeholder="Kecamatan" data-usedto="kecamatan_wp" required>
                                    <!-- <select name="kecamatan"  required class="form-control-sm form-control select" data-usedto="kecamatan_wp"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                                        <option value="">-- Kecamatan --</option>
                                        @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Kota/Kabupaten*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="dati2" class="form-control-sm form-control "placeholder="Kota/Kabupaten" data-usedto="dati2_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Propinsi*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="propinsi" class="form-control-sm form-control "placeholder="Propinsi" data-usedto="propinsi_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Email*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="email" class="form-control-sm form-control no-required email" placeholder="Email" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-3 col-form-label p-1">Pengurusan*</label>
                                <div class="col-md-9 p-0">
                                    <select name="pengurus"  required class="form-control-sm form-control" data-placeholder="Pilih Pengurusan">
                                        <option value="1">Sendiri</option>
                                        <option value="2">di kuasakan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="text" name="tanggal_lahir_wp" class="form-control-sm form-control inputdate hidden">
                </div>
            </div>
        </div>
        <div class="col-md-5 pl-1">
            <div class="card mb-1">
                <div class="card-header d-flex">
                    <h3 class="card-title"><b>-</b></h3>
                </div>
                <div class="card-body p-1">
                    <div class='row hidden'>
                        <div class="form-group col-md-12 mb-1">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-4 col-form-label col-form-label-sm mb-0">NOP*</label>
                                <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP">
                                <!-- <div class="input-group-append hidden">
                                    <button type="button" class="btn btn-info btn-sm hidden" data-nop='nop' aria-expanded="false">
                                        <i class='fas fa-plus-square'></i> Cek NOP
                                    </button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- <p class="bg-warning color-palette p-1 mb-1">
                        <b>Subjek</b>
                    </p> -->
                    <div class="row hidden">
                        <div class="form-group col-md-12 mb-1 ">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NIK </span>
                                </div>
                                <input type="text" name="nik_wp" class="form-control-sm form-control " placeholder="NIK WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1 ">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nama </span>
                                </div>
                                <input type="text" name="nama_wp" class="form-control-sm form-control " placeholder="Nama WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1 ">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Alamat </span>
                                </div>
                                <input type="text" name="alamat_wp" class="form-control-sm form-control " placeholder="Alamat WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1 ">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RT </span>
                                </div>
                                <input type="text" name="rt_wp" class="form-control-sm form-control " placeholder="RT WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1 ">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RW </span>
                                </div>
                                <input type="text" name="rw_wp" class="form-control-sm form-control " placeholder="RW WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1 ">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kelurahan </span>
                                </div>
                                <input type="text" name="kelurahan_wp" class="form-control-sm form-control " placeholder="Kelurahan WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kecamatan </span>
                                </div>
                                <input type="text" name="kecamatan_wp" class="form-control-sm form-control " placeholder="Kecamatan WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kota/Kabupaten </span>
                                </div>
                                <input type="text" name="dati2_wp" class="form-control-sm form-control " placeholder="Kota/Kabupaten WP" readonly>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Propinsi </span>
                                </div>
                                <input type="text" name="propinsi_wp" class="form-control-sm form-control " placeholder="Propinsi WP" readonly>
                            </div>
                        </div>
                    </div>
                    <p class="bg-warning color-palette p-1 mb-1">
                        <b>Objek</b>
                    </p>
                    <div class="row">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label p-1">Kecamatan</label>
                                <div class="col-md-8 p-0">
                                    <select name="nop_kecamatan"  class="form-control-sm form-control select-bottom"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='nop_kelurahan'>
                                        <option value="">-- Kecamatan --</option>
                                        @foreach ($kecamatan as $rowkec)
                                            <option value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label p-1">Kelurahan</label>
                                <div class="col-md-8 p-0">
                                    <select name="nop_kelurahan"  class="form-control-sm form-control select-bottom"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu)." >
                                        <option value="">-- Kelurahan --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text ">Alamat </span>
                                </div>
                                <input type="text" name="nop_alamat" class="form-control-sm form-control " placeholder="Alamat Objek" required>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Blok/Kav/No </span>
                                </div>
                                <input type="text" name="blok_kav_no_op" class="form-control-sm form-control " placeholder="Blok/Kav/No.">
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RT </span>
                                </div>
                                <input type="text" name="nop_rt" class="form-control-sm form-control rt numeric" maxlength="3" placeholder="RT Objek" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1">
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RW </span>
                                </div>
                                <input type="text" name="nop_rw" class="form-control-sm form-control rw numeric" maxlength="2" placeholder="RW Objek" required>
                            </div>
                        </div>
                        
                        
                        <div class="form-group col-md-6 mb-1" >
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Luas Tanah</span>
                                </div>
                                <input type="text" name="luas_bumi" class="form-control-sm form-control numeric"  maxlength="10" data-calc="jenis_layanan_id" placeholder="Luas Tanah" required >
                            </div>
                        </div>
                        <div class="form-group col-md-6 mb-1" >
                            <div class='input-group input-group-sm'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Luas Bangunan</span>
                                </div>
                                <input type="text" name="luas_bng" class="form-control-sm form-control numeric"  maxlength="10" placeholder="Luas Bangunan" required data-show="luas_bng" >
                            </div>
                        </div>
                    </div>
                    <div class="row " data-when="luas_bng">
                        <!-- data-norequiredhide="E" -->
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label mb-0 p-1">JPB*</label>
                                <div class="col-md-8 p-0">
                                    <select name="kelompok_objek_id"   class="form-control-sm form-control select"  data-placeholder="Jenis Penggunaan Bangunan"  >
                                        <option value="">-- Jenis --</option>
                                        @foreach ($KelompokObjek as $kelompok)
                                            <option @if (request()->get('kelompok') == $kelompok->id)  selected @endif value="{{ $kelompok->id }}">
                                                {{ $kelompok->nama_kelompok }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="form-group col-md-12 mb-1">
                            <div class='input-group input-group-sm'>
                                <label class="col-md-4 col-form-label mb-0 p-1">Lokasi Objek*</label>
                                <div class="col-md-8 p-0">
                                <select name="lokasi_objek_id"   class="form-control-sm form-control select"  data-placeholder="Pilih Lokasi Objek." >
                                        <option value="">-- Lokasi --</option>
                                        @foreach ($LokasiObjek as $lokasi)
                                            <option @if (request()->get('lokasi') == $lokasi->id)  selected @endif value="{{ $lokasi->id }}">
                                                {{ $lokasi->nama_lokasi }}
                                            </option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>  
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-1">
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-2 col-form-label p-1">Keterangan</label>
                                <textarea name="keterangan" class="form-control-sm form-control triggerHeight ">KETERANGAN</textarea>
                                <!-- <input name="keterangan" class="form-control-sm form-control numeric"> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layanan/part_dokumen')
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
            <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Simpan</button> </div>
        </div>
    </div>
</form>