<html>
<head>
   @include('layouts.style_pdf')
</head>
<body >
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">
        KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH <br> KABUPATEN MALANG <br>
        <small>Nomor : {{ $data['nopel'] }}</small><br><br>
        TENTANG<br> PEMBATALAN PAJAK BUMI DAN BANGUNAN <br>PERDESAAN DAN PERKOTAAN ATAS SPPT<br>
        TAHUN {{ $data['tahunpajak'] }}
    </h4>
    <br>
    <br>
    <div>
        <div style='width:19%;float:left'>Membaca</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Surat Permohonan Pembatalan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh {{ $data['nm_wp'] }},
         dengan nomor pelayanan {{ $data['nopel'] }}; </div>
    </div>
    <br>
    <br>
    <div>
        <div style='width:19%;float:left'>Menimbang</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Bahwa berdasarkan hasil penelitian sebagaimana dituangkan dalam laporan hasil penelitian Lapangan Nomor …….., 
        sehingga terdapat/<u>tidak terdapat*</u>) cukup alasan untuk menerima Pembatalan Ketetapan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang tidak benar; </div>
    </div> 
    <div> 
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
    <div>
        <div style='width:19%;float:left'>Mengingat</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>
            <ol style='margin:0px; padding:0px 20px;'>
                <li>Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah.</li>
                <li>Peraturan Daerah Nomor  8 Tahun 2010 tentang Pajak Daerah.</li>
                <li>Peraturan Bupati Malang Nomor 49 Tahun 2013 tentang Tata Cara Pembatalan Ketetapan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang tidak benar.</li>
            </ol>    
        </div>
    </div>
    <div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <h4 class="text-tengah"> MEMUTUSKAN </h4>
        <br>
    </div> 
    <div>
        <div style='width:19%;float:left'>Menetapkan</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'></div>
        <br>
    </div>
    <div>
        <div style='width:19%;float:left'>PERTAMA</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Menerima seluruhnya/<u>menerima sebagian/menolak*</u>) berkas permohonan Pembatalan PBB yang diajukan oleh {{ $data['nm_wp'] }}, 
        yang diajukan sebagaimana ditetapkan dalam lampiran Keputusan ini, yang merupakan bagian yang tidak terpisahkan;  </div>
        <br>
        <br>
        <br>
    </div>
    <div>
        <div style='width:19%;float:left'>KEDUA</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Apabila di kemudian hari ternyata diketahui terdapat kekeliruan dalam keputusan ini, akan dibetulkan sesuai ketentuan yang berlaku; </div>
        <br>
        <br>
    </div>
    <div>
        <div style='width:19%;float:left'>KETIGA</div>
        <div style='width:1%;float:left'>:</div>
        <div style='width:80%;float:left'>Keputusan ini mulai berlaku sejak tanggal ditetapkan.</div>
    </div>
   <div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div style='width:60%;float:left'><br></div>
        <div style='width:40%;float:left'>
                Ditetapkan di :  <b>Malang</b> <br>
                Pada Tanggal :   {{ $data['tanggalsk'] }}  <br>
                Kepala Badan Pendapatan Daerah,
                Kabupaten Malang
                <br>
                <br>
                <br>
                <br>
                Ir. Didik Budi Muljono, MT <br>
                Pembina Utama Muda <br>
                NIP. 19600504 198811 1 001
        </div>
    </div>

</body>
</html>
