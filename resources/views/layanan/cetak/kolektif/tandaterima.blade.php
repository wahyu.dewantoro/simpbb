<html>

<head>
    <title>Tanda terima</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }

        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }

        table {
            font-weight: bold;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
        }

    </style>
</head>

<body>
   
    <table width="100%" border="0">
        <tr>
            <td>
                <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    JL. RADEN PANJI NOMOR 158 KEPANJEN </P>
                <hr>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="45%"></td>
            <td>
                @php $no=1; @endphp
                <table cellpadding="3" width="100%">
                    <tbody>
                        <tr style="vertical-align: top">
                            <td width="5%">@php echo $no.'.'; $no++; @endphp</td>
                            <td width="46%">NOMOR PELAYANAN</td>
                            <td width="1%">:</td>
                            <td>{{ $permohonan['nomor_layanan'] }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td>@php echo $no.'.'; $no++; @endphp</td>
                            <td>Tanggal Penerimaan Berkas</td>
                            <td>:</td>
                            <td>{{ $permohonan['tanggal_permohonan'] }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td>@php echo $no.'.'; $no++; @endphp</td>
                            <td>Tanggal SELESAI</td>
                            <td>:</td>
                            <td>{{ $permohonan['tanggal_selesai'] }}</td>
                        </tr>
                    </tbody>
                </table>

            </td>
        </tr>
    </table>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td width="3%">@php echo $no.'.'; $no++; @endphp</td>
            <td width="25%">JENIS PELAYANAN</td>
            <td width="3%">:</td>
            <td>{{ $permohonan['jenis_layanan'] }}</td>
        </tr>
        <tr>
            <td>@php echo $no.'.'; $no++; @endphp</td>
            <td>Jumlah Pemohon</td>
            <td>:</td>
            <td>{{ count($getObjek) }} Data Permohonan</td>
        </tr>
    </table>
    <hr>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center">
                A. DATA WAJIB / OBJEK PAJAK
            </td>
        </tr>
    </table>
    <hr>
    <br><br>
    <table cellpadding="3" width="100%">
        <tbody>
            <tr>
                <td width="3%">@php echo $no.'.'; $no++; @endphp</td>
                <td width="25%">NAMA PEMOHON / NIK </td>
                <td width="3%">:</td>
                <td> {{ $permohonan['nama_pemohon'] }} / {{ $permohonan['nik_pemohon'] }} </td>
            </tr>
            <tr style="vertical-align: top">
                <td></td>
                <td>ALAMAT PEMOHON</td>
                <td>:</td>
                <td>{{ $permohonan['alamat_pemohon'] }} DS/KEL. {{ $permohonan['kelurahan_pemohon'] }} KEC
                    {{ $permohonan['kecamatan_pemohon'] }} {{ $permohonan['dati2_pemohon'] }}
                    {{ $permohonan['propinsi_pemohon'] }}</td>
            </tr>
            <tr>
                <td></td>
                <td>NO TELP</td>
                <td>:</td>
                <td>{{ $permohonan['nomor_hp'] }}</td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr style="vertical-align: top">
                <td>@php echo $no.'.'; $no++; @endphp</td>
                <td>LETAK OBJEK PAJAK</td>
                <td>:</td>
                <td>{{ $permohonan['alamat_op'] }}</td>
            </tr>

            <tr>
                <td>@php echo $no.'.'; $no++; @endphp</td>
                <td>KETERANGAN</td>
                <td>:</td>
                <td>{{ $permohonan['keterangan'] }}</td>
            </tr>
        </tbody>
    </table>
    <hr>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center">
                B. PENERIMAAN BERKAS
            </td>
        </tr>
    </table>
    <hr>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td>@php echo $no.'.'; $no++; @endphp DOKUMEN DILAMPIRKAN :</td>

        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr>
            <td width="2%">&nbsp;&nbsp;</td>
            <td width="30%" valign="top">
                <table cellpadding="3" width="100%">
                @foreach ($permohonan['table_dokumen'] as $idx => $kolek)
                    <tr>
                        <td  width="2%">{{$kolek['no']}}.</td>
                        <td  width="40%">{{$kolek['nama_dokumen']}}</td>
                        <td>: {{$kolek['keterangan']}}</td>
                    </tr>
                    @if($kolek['no']%7==0)
                    </table>
                    </td>
                    <td width="30%" valign="top">
                    <table cellpadding="3" width="100%">
                    @endif
                @endforeach
                </table>
            </td>
        </tr>
    </table>
    
    <br><hr><br>
    <table cellpadding="3" width="100%">
        <tr>
            <td>@php echo $no.'.'; $no++; @endphp Catatan<br></td>
        </tr>
        <tr>
            <td>{{ $permohonan['keterangan']}}</td>
        </tr>
    </table>
    <br>
    <br>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td colspan='2'>PETUGAS PENERIMA BERKAS: {{ $permohonan['petugas_penerima_berkas'] }}</td>
        </tr>
        <tr>
            <td>NIP: -</td>
            <td align="right">Wajib Pajak: {{ $permohonan['nama_pemohon'] }}</td>
        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center" colspan='2'><br>------------------------------------ Gunting
                Disini------------------------------------ <br></td>
        </tr>
        <tr>
            <td align="center" colspan='2'>
                <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    JL. RADEN PANJI NOMOR 158 KEPANJEN </P>
            </td>
        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr valign="top">
            <td width="50%">
                <table>
                    @if($permohonan['jenis_layanan_id']!='1')
                    <tr>
                        <td wdth="3px">@php echo $no.'.'; $no++; @endphp</td>
                        <td>Jumlah Pemohon</td>
                        <td width="1px">:</td>
                        <td>{{ count($getObjek) }} Data Permohonan</td>
                    </tr>
                    @endif
                    <tr>
                        <td>@php echo $no.'.'; $no++; @endphp</td>
                        <td>NOMOR PELAYANAN</td>
                        <td>:</td>
                        <td>{{ $permohonan['nomor_layanan'] }}</td>
                    </tr>
                    <tr>
                        <td width="3px">@php echo $no.'.'; $no++; @endphp</td>
                        <td>TANGGAL Penerimaan Berkas</td>
                        <td width="1px">:</td>
                        <td>{{ $permohonan['tanggal_permohonan'] }}</td>
                    </tr>
                </table>
            </td>
            <td>
                <table cellpadding="3" width="100%" border="0">

                    <tr>
                        <td>@php echo $no.'.'; $no++; @endphp</td>
                        <td>TANGGAL SELESAI </td>
                        <td>:</td>
                        <td>{{ $permohonan['tanggal_selesai'] }}</td>
                    </tr>

                    <tr>
                        <td>@php echo $no.'.'; $no++; @endphp</td>
                        <td>PETUGAS PENERIMAN BERKAS</td>
                        <td>:</td>
                        <td>{{ $permohonan['petugas_penerima_berkas'] }}</td>
                    </tr>

                </table>

            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">

                <hr>
                TANDA PENDAFTARAN PELAYANAN
                <hr>

            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <table width="100%" >
                    <tr>
                        <td>@php echo $no.'.'; $no++; @endphp</td>
                        <td>URUSAN</td>
                        <td>:</td>
                        <td>{{ $permohonan['jenis_layanan'] }}</td>
                    </tr>
                    <tr>
                        <td>@php echo $no.'.'; $no++; @endphp</td>
                        <td>CATATAN</td>
                        <td>:</td>
                        <td>{{ $permohonan['keterangan']}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td colspan='2'>PETUGAS PENERIMA BERKAS: {{ $permohonan['petugas_penerima_berkas'] }}</td>
        </tr>
        <tr>
            <td>NIP: -</td>
            <td align="right">Wajib Pajak: {{ $permohonan['nama_pemohon'] }}</td>
        </tr>
    </table>
    @if (count($getObjek) > 1)
        <div class="page-break"></div>

        <style>
            td.lampiran {
                border-right: 1px solid #C6C6C6;
            }

            lampiran.th {
                border-right: 1px solid #C6C6C6;
                border-bottom: 1px solid #C6C6C6;
            }

        </style>
     @if($permohonan['jenis_layanan_id']=='7')
        <h3 style="font-size: 12px; font-weight:bold">
            LAMPIRAN TANDA TERIMA PELAYANAN MUTASI GABUNG</h3>
    @endif
    @if($permohonan['jenis_objek']!='1')
        <h3 style="font-size: 12px; font-weight:bold">
            LAMPIRAN TANDA TERIMA PELAYANAN KOLEKTIF</h3>
    @endif
        <table style="border: 1px solid #C6C6C6; font-size:12px;" cellpadding="5" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">No</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">NOP</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">Nama WP</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">Tahun</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">Jenis Pelayanan</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1; @endphp
            @foreach ($getObjek as $idx => $kolek)
                <tr>
                    <td style="border-right: 1px solid #C6C6C6; text-align:center">{{ $no  }}</td>
                    <td style=" border-right: 1px solid #C6C6C6; text-align:center">
                    @if($permohonan['jenis_objek']=='3'&&$kolek['nop_gabung']=='')
                        {{ formatnop($kolek['kd_propinsi'] . $kolek['kd_dati2'] . $kolek['kd_kecamatan'] . $kolek['kd_kelurahan'] . $kolek['kd_blok'] . $kolek['no_urut'] . $kolek['kd_jns_op']) }}
                    @else
                        -
                    @endif
                    </td>
                    <td style=" border-right: 1px solid #C6C6C6;">{{ $kolek['nama_wp'] }}</td>
                    <td style=" border-right: 1px solid #C6C6C6; text-align:center">
                        {{ date('Y', strtotime($permohonan['tgl'])) }}</td>
                    <td style=" border-right: 1px solid #C6C6C6;text-align:center">{{ $permohonan['jenis_layanan']  }}</td>
                </tr>
                @php $no++; @endphp
            @endforeach
            </tbody>
        </table>


    @endif 

</body>

</html>
