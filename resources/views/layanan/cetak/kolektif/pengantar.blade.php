<table width="70%" border="0" class="font-no-bold" style="float:left">
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td style="width:10%">Nomor</td>
        <td>: .........../.........../.........../{{ substr($permohonanKolektif['nomor_layanan'],0,4)}}</td>
    </tr>
    <tr>
        <td>Sifat</td>
        <td>: Segera</td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>: 1 (satu) berkas</td>
    </tr>
    <tr>
        <td>Perihal</td>
        <td>: Permohonan Ajuan Mutasi Penuh SPPT PBB P2  Kolektif</td>
    </tr>
</table>
<table width="30%" border="0" style="float:left" class="font-no-bold">
    <tr>
        <td width="20%">{{$permohonanKolektif['kelurahan']}},</td>
        <td>{{$permohonanKolektif['tgl']}}</td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td style="vertical-align: baseline;">Yth. Sdr.</td>
        <td>
            <p>K e p a d a</p>
            <p>Kepala Badan Pendapatan Daerah</p>
            <p>Kabupaten Malang</p>
            <br>
            <p>di</p>
            <p class="fontUp">KEPANJEN</p>
        </td>
    </tr>
</table>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<table width="100%" border="0" class="font-no-bold" >
    <tr>
        <td style="width:10%"></td>
        <td>Bersama ini disampaikan permohonan ajuan data baru SPPT PBB P2 kolektif  dengan rincian per jenis ajuan perbaikan SPPT PBB P2 Tahun Pajak {{ substr($permohonanKolektif['nomor_layanan'],0,4)}} sebagaimana isian format masing-masing terlampir sebanyak  :</td>
    </tr>
    <tr>
        <td></td>
        <td>{{count($permohonanKolektif['data'])}} Pemohon</td>
    </tr>
    <tr>
        <td></td>
        <td>Menyatakan data dan informasi yang saya berikan ke petugas adalah benar dan apabila dikemudian hari terdapat kekeliruan Kepala Desa / Lurah bertanggung jawab sepenuhnya.<br>
            Demikian atas perhatian dan proses permohonannya disampaikan terima kasih.</td>
    </tr>
</table>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<table width="30%" border="0" style="float:right" class="font-no-bold text-center">
    <tr>
        <td>KEPALA DESA/LURAH</td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td>NAMA LENGKAP/TTD/STEMPEL</td>
    </tr>
</table>