<html>

<head>
    <title>REKAP KOLEKTIF</title>
    <style type="text/css">
        .fontUp{
            font-size: 1.2em;
            font-weight: bold;
        }
        .font-no-bold{
            font-weight: normal;
        }
        .float-left{
            float: left;
            width: 100%;
        }
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<br>
<br>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <p></P>
            <hr>
        </td>
    </tr>
</table>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
@include('layanan.cetak.kolektif.pengantar')
{{-- <table width="70%" border="0" class="font-no-bold" style="float:left">
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td style="width:10%">Nomor</td>
        <td>: .........../.........../.........../2022</td>
    </tr>
    <tr>
        <td>Sifat</td>
        <td>: Segera</td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>: 1 (satu) berkas</td>
    </tr>
    <tr>
        <td>Perihal</td>
        <td>: Permohonan Ajuan Data Baru SPPT PBB P2  Kolektif</td>
    </tr>
</table>
<table width="30%" border="0" style="float:left" class="font-no-bold">
    <tr>
        <td width="20%">{{$permohonanKolektif['kelurahan']}},</td>
        <td>{{$permohonanKolektif['tgl']}}</td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td style="vertical-align: baseline;">Yth. Sdr.</td>
        <td>
            <p>K e p a d a</p>
            <p>Kepala Badan Pendapatan Daerah</p>
            <p>Kabupaten Malang</p>
            <br>
            <p>di</p>
            <p class="fontUp">KEPANJEN</p>
        </td>
    </tr>
</table>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<table width="100%" border="0" class="font-no-bold" >
    <tr>
        <td style="width:10%"></td>
        <td>Bersama ini disampaikan permohonan ajuan data baru SPPT PBB P2 kolektif  dengan rincian per jenis ajuan perbaikan SPPT PBB P2 Tahun Pajak 2022 sebagaimana isian format masing-masing terlampir sebanyak  :</td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>{{count($permohonanKolektif['data'])}} Pemohon</td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Demikian atas perhatian dan proses permohonannya disampaikan terima kasih.</td>
    </tr>
</table>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<div class="float-left"><br></div>
<table width="30%" border="0" style="float:right" class="font-no-bold text-center">
    <tr>
        <td>KEPALA DESA/LURAH</td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td><br></td>
    </tr>
    <tr>
        <td>NAMA LENGKAP/TTD/STEMPEL</td>
    </tr>
</table> --}}
<br>
<br>
<div class="page-break"></div>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <p>REKAP AJUAN KOLEKTIF {{$permohonanKolektif['nama_layanan']}}</P>
            <hr>
        </td>
    </tr>
</table>
<br>
<table border="0">
    <tr>
        <td>Jenis Ajuan</td><td>:</td><td>{{$permohonanKolektif['nama_layanan']}}</td>
    </tr>
    <tr>
        <td>Desa/Kelurahan</td><td>:</td><td>{{$permohonanKolektif['kelurahan']}}</td>
    </tr>
    <tr>
        <td>Kecamatan</td><td>:</td><td>{{$permohonanKolektif['kecamatan']}}</td>
    </tr>
    <tr>
        <td>NOMOR LAYANAN</td><td>:</td><td>{{$permohonanKolektif['nomor_layanan']}}</td>
    </tr>
</table>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th rowspan="2">No</th>
            <th colspan="8">(DATA YANG SEHARUSNYA)</th>
            <th rowspan="2">NIK WP</th>
            <th rowspan="2">NO. TELP WP (HP)</th>
        </tr>
        <tr>
            <th>NAMA WP</th>
            <th>ALAMAT WP</th>
            <th>RT/RW </th>
            <th>LOKASI </th>
            <th>Alamat OP</th>
            <th>RT/RW</th>
            <!-- <th>KELURAHAN</th>
            <th>KECAMATAN</th> -->
            <th>LT (M2)</th>
            <th>LB (M2)</th>
        </tr>
    </thead>
    <tbody>
        @foreach($permohonanKolektif['data'] as $item)
            <tr>
                <td>{{$item['no']}}</td>
                <td>{{$item['nama_wp']}}</td>
                <td>{{$item['alamat_wp']}}</td>
                <td>{{$item['rt_rw_wp']}}</td>
                <td>{{$item['kelurahan_wp']}}, {{$item['kecamatan_wp']}}, {{$item['dati2_wp']}}, {{$item['propinsi_wp']}}</td>
                <td>{{$item['alamat_op']}}</td>
                <td>{{$item['rt_rw_op']}}</td>
                <!-- <td>{ {$item['kelurahan_op']}}</td>
                <td>{ {$item['kecamatan_op']}}</td> -->
                <td>{{$item['luas_bumi']}}</td>
                <td>{{$item['luas_bng']}}</td>
                <td>{{$item['nik_wp']}}</td>
                <td>{{$item['notelp_wp']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<br>
<br>
<table width="20%" border="0" class="text-center" style="float:right;">
    <tr>
        <td>
            <p>Kepala desa / Lurah</P>
        </td>
    </tr>
    <tr><td><br></td> </tr>
    <tr> <td><br></td> </tr>
    <tr> <td><br></td> </tr>
    <tr> <td><br></td> </tr>
    <tr> <td><br></td> </tr>
    <tr>
        <td><hr></td>
    </tr>
</table>
</body>
</html>
