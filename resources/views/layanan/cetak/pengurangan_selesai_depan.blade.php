<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    <table width="100%" class="">
        <tr>
            <th width="70px"><img width="60px" height="70px" src="{{ public_path('kabmalang.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            {{-- <th width="100px"> --}}
                
                {{-- <img width="60px" src="{{ url('qrsppt') }}?nop={{ $sppt->kd_propinsi }}{{ $sppt->kd_dati2 }}{{ $sppt->kd_kecamatan }}{{ $sppt->kd_kelurahan }}{{ $sppt->kd_blok }}{{ $sppt->no_urut }}{{ $sppt->kd_jns_op }}{{ $sppt->thn_pajak_sppt }}" alt=""> --}}
            {{-- </th> --}}
        </tr>
    </table>
    <hr>
    <p style="text-align: center ">
        <b><u>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</u></b>
        <br>
        <b> Nomor : {{ $permohonan['nomor_layanan'] }} / PENGURANGAN.PBB/35.07/2022</b>
    </p>
    <p style="text-align: center ">
        <b>TENTANG PENGURANGAN PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN (PBB P2)</b>
    </p>
    <table width="100%">
        <tbody style="">
            <tr>
                <td style="vertical-align: baseline;">MENIMBANG</td>
                <td style="vertical-align: baseline;">:</td>
                <td>
                    <p>Bahwa berdasarkan hasil penelitian kantor dan atau lapangan maka perlu ditetapkan Keputusan Kepala Badan Pendapatan Daerah tentang Pengurangan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB P2) </p>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: baseline;">MENGINGAT</td>
                <td style="vertical-align: baseline;">:</td>
                <td>
                    <ol style="margin:0px;">
                        <li>Undang-Undang Nomor 1 Tahun 2022 tentang Hubungan Keuangan Antara Pemerintah Pusat dan Pemerintah Daerah.</li>
                        <li>Peraturan Daerah Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 Tahun 2010 tentang Pajak Daerah.</li>
                        <li>Peraturan Bupati Malang Nomor 51 Tahun 2017 tentang Perubahan Atas Peraturan Bupati Malang Nomor 43 Tahun 2013 tentang Tata Cara Pengajuan dan Penyelesaian Pengurangan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan.</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: baseline;">MEMPERHATIKAN</td>
                <td style="vertical-align: baseline;">:</td>
                <td>
                    <p>Surat permohonan Pengurangan Pajak Bumi dan Bangunan Perdesaan dan Perkotaan yang diajukan oleh Pemohon dengan Nomor Pelayanan : {{ $permohonan['nomor_layanan'] }}</p>
                </td>
            </tr>
            <tr>
                <td colspan='3' style="text-align:center"><b>MEMUTUSKAN</b></td>
            </tr>
            <tr>
                <td style="vertical-align: baseline;">MENETAPKAN</td>
                <td style="vertical-align: baseline;">:</td>
                <td> </td>
            </tr>
            <tr>
                <td style="vertical-align: baseline;">KESATU</td>
                <td style="vertical-align: baseline;">:</td>
                <td>
                    <p>Mengabulkan sebagian atau seluruhnya permohonan pengurangan PBB P2 dengan rincian :</p>
                </td>
            </tr>
            <tr>
                <td colspan='3'>
                    <table width="90%" style="margin:auto">
                        <tr>
                            <td>a.</td>
                            <td>Nama Wajib Pajak</td>
                            <td>:</td>
                            <td>{{ $permohonan['nama_wp'] }}</td>
                        </tr>
                        <tr>
                            <td>b.</td>
                            <td>NOP</td>
                            <td>:</td>
                            <td>{{ $permohonan['nop'] }}</td>
                        </tr>
                        <tr>
                            <td>c.</td>
                            <td>PBB Terutang</td>
                            <td>:</td>
                            <td>{{ $permohonan['pbb'] }}</td>
                        </tr>
                        <tr>
                            <td>d.</td>
                            <td>Prosentase pengurangan (%)</td>
                            <td>:</td>
                            <td>{{ $permohonan['pengurangan'] }} %</td>
                        </tr>
                        <tr>
                            <td>e.</td>
                            <td>Besarnya Pengurangan (c x d/100)</td>
                            <td>:</td>
                            <td>{{ $permohonan['nilai_pengurangan'] }}</td>
                        </tr>
                        <tr>
                            <td>f.</td>
                            <td>Jumlah pajak terutang setelah pengurangan (c-e)</td>
                            <td>:</td>
                            <td>{{ $permohonan['hasil_pbb'] }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: baseline;">KEDUA</td>
                <td style="vertical-align: baseline;">:</td>
                <td>
                    <p>Apabila di kemudian hari ternyata diketahui terdapat kekeliruan dalam Keputusan ini, maka akan dibetulkan sebagaimana mestinya.</p>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: baseline;">KETIGA</td>
                <td style="vertical-align: baseline;">:</td>
                <td>
                    <p>Keputusan Kepala Badan Pendapatan Daerah ini mulai berlaku pada tanggal ditetapkan.</p>
                </td>
            </tr>
        </tbody>
    </table>
   
<p></p>
<p></p>
<table width="100%" border="0">
    <tbody>
        <tr>
            <td width="30%"></td>
            <td></td>
            <td width="40%" align="center">
                <p>Ditetapkan di: <b>KEPANJEN</b></p>
                <p><u>Pada Tanggal  :{{ tglIndo($permohonan['tanggal_sk']) }}</u></p>
                <p>KEPALA BADAN PENDAPATAN DAERAH KABUPATEN MALANG</p>
                <br>
                <br>
                <br>
                <br>
                <br>
                <p><u><b>MADE ARYA WEDANTHARA, SH, M.Si</b></u>
                </br>
                Pembina Utama Muda
                </br>NIP. 19690811 199503 1 002</p>
            </td>
        </tr>
    </tbody>
</table>
</html>

