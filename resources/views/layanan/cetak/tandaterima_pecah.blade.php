<html>

<head>
    <title>Tanda terima</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }

        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }

        table {
            font-weight: bold;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
        }

    </style>
</head>

<body>
    @php 
        $nopecah=1;
    @endphp
   @foreach($permohonan['pecah'] as $item)
    <table width="100%" border="0">
        <tr>
            <td>
                <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    JL. RADEN PANJI NOMOR 158 KEPANJEN </P>
                <hr>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="45%"></td>
            <td>
                @php $no=1; @endphp
                <table cellpadding="3" width="100%">
                    <tbody>
                        <tr style="vertical-align: top">
                            <!-- <td width="5%">@php echo $no.'.'; $no++; @endphp</td> -->
                            <td width="46%">NOMOR PELAYANAN</td>
                            <td width="1%">:</td>
                            <td>{{ $permohonan['nomor_layanan'] }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                            <td>TANGGAL PELAYANAN</td>
                            <td>:</td>
                            <td>{{ $permohonan['tanggal_permohonan'] }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                            <td>TGL SELESAI (PERKIRAAN)</td>
                            <td>:</td>
                            <td>{{ $permohonan['tanggal_selesai'] }}</td>
                        </tr>
                    </tbody>
                </table>

            </td>
        </tr>
    </table>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <!-- <td width="3%">@php echo $no.'.'; $no++; @endphp</td> -->
            <td width="25%">JENIS PELAYANAN</td>
            <td width="3%">:</td>
            <td>{{ $permohonan['jenis_layanan'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>NOP INDUK</td>
            <td>:</td>
            <td>{{ $permohonan['nop'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>LUAS TANAH / BANGUNAN</td>
            <td>:</td>
            <td>{{ $permohonan['luas_bumi'] }}/{{ $permohonan['luas_bng'] }}</td>
        </tr>
        <tr><td></td></tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td colspan='3'><u>PEMOHON</u></td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>NIK</td>
            <td>:</td>
            <td>{{ $permohonan['nik_wp_pemohon'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>NAMA </td>
            <td>:</td>
            <td>{{ $permohonan['nama_pemohon'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>No. Telp</td>
            <td>:</td>
            <td>{{ $permohonan['nomor_telepon_pemohon'] }}</td>
        </tr>
    </table>
    <hr>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center">
                DATA PECAH {{$nopecah}}
            </td>
        </tr>
    </table>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <!-- <td width="3%">@php echo $no.'.'; $no++; @endphp</td> -->
            <td width="25%">NIK</td>
            <td width="3%">:</td>
            <td>{{ $item['nik_wp'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>NAMA</td>
            <td>:</td>
            <td>{{ $item['nama_wp'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>ALAMAT</td>
            <td>:</td>
            <td>{{ $item['alamat_wp'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>NO. TELP</td>
            <td>:</td>
            <td>{{ $item['telp_wp'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>LETAK OBJEK</td>
            <td>:</td>
            <td>{{ $item['lokasi_objek_nama'] }}</td>
        </tr>
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
            <td>TANAH</td>
            <td>:</td>
            <td>{{ $item['luas_bumi'] }}</td>
            <td>BANGUNAN</td>
            <td>:</td>
            <td>{{ $item['luas_bng'] }}</td>
        </tr>
    </table>
    <hr>
    <table cellpadding="3" width="100%">
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp LAMPIRAN BERKAS :</td> -->
            <td> LAMPIRAN BERKAS :</td>

        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr>
            <td width="2%">&nbsp;&nbsp;</td>
            <td width="30%" valign="top">
                <table cellpadding="3" width="100%">
                @php $noBerkas=1; @endphp  
                @if($permohonan['berkas'])
                    @foreach ($permohonan['berkas'] as $idx => $kolek)
                        <tr>
                            <td  width="2%">{{$noBerkas}}.</td>
                            <td  width="30%">{{$kolek['nama_dokumen']}}</td>
                            <td>: No. {{$kolek['keterangan']}}</td>
                            <td>: Ref  : {{$permohonan['nop']}}</td>
                        </tr>
                        @php $noBerkas++; @endphp 
                    @endforeach
                @endif
                @if($item['berkas'])
                    @foreach ($item['berkas'] as $idx => $kolek)
                        <tr>
                            <td  width="2%">{{$noBerkas}}.</td>
                            <td  width="30%">{{$kolek['nama_dokumen']}}</td>
                            <td>: No. {{$kolek['keterangan']}}</td>
                            <td></td>
                        </tr>
                        @php $noBerkas++; @endphp 
                    @endforeach
                @endif
                </table>
            </td>
        </tr>
    </table>
    
    <hr>
    <table cellpadding="3" width="100%">
        <tr>
            <!-- <td>@php echo $no.'.'; $no++; @endphp Catatan<br></td> -->
            <td>CATATAN<br></td>
        </tr>
        <tr>
            <td>{{ $permohonan['keterangan']}}</td>
        </tr>
    </table>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td colspan='2'>PETUGAS PENERIMA BERKAS: {{ $permohonan['petugas_penerima_berkas'] }}</td>
        </tr>
        <tr>
            <td>NIP: -</td>
            <td align="right">Wajib Pajak: {{ $permohonan['nama_pemohon'] }}</td>
        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center" colspan='2'><br>------------------------------------ Gunting
                Disini------------------------------------ <br></td>
        </tr>
        <tr>
            <td align="center" colspan='2'>
                <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    JL. RADEN PANJI NOMOR 158 KEPANJEN </P>
            </td>
        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr valign="top">
            <td width="50%">
                <table>
                    <tr>
                        <!-- <td wdth="3px">@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>N O P</td>
                        <td width="1px">:</td>
                        <td>{{ $permohonan['nop'] }}</td>
                    </tr>
                    <tr>
                        <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>NOMOR PELAYANAN</td>
                        <td>:</td>
                        <td>{{ $permohonan['nomor_layanan'] }}</td>
                    </tr>
                    <tr>
                        <!-- <td width="3px">@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>TANGGAL PELAYANAN</td>
                        <td width="1px">:</td>
                        <td>{{ $permohonan['tanggal_permohonan'] }}</td>
                    </tr>
                </table>
            </td>
            <td>
                <table cellpadding="3" width="100%" border="0">

                    <tr>
                        <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>TGL SELESAI (PERKIRAAN)</td>
                        <td>:</td>
                        <td>{{ $permohonan['tanggal_selesai'] }}</td>
                    </tr>

                    <tr>
                        <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>PETUGAS PENERIMAN BERKAS</td>
                        <td>:</td>
                        <td>{{ $permohonan['petugas_penerima_berkas'] }}</td>
                    </tr>

                </table>

            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">

                <hr>
                TANDA PENDAFTARAN PELAYANAN
                <hr>

            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <table width="100%" >
                    <tr>
                        <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>JENIS PERMOHONAN</td>
                        <td>:</td>
                        <td>{{ $permohonan['jenis_layanan'] }}</td>
                    </tr>
                    <tr>
                        <!-- <td>@php echo $no.'.'; $no++; @endphp</td> -->
                        <td>PERNYATAAN PEMOHON</td>
                        <td>:</td>
                        <td>SAYA WAJIB PAJAK ATAU PENERIMA KUASA MENYATAKAN DATA DAN INFORMASI YANG SAYA BERIKAN KE PETUGAS ADALAH BENAR DAN APABILA DIKEMUDIAN HARI TERDAPAT KEKELIRUAN SAYA BERTANGGUNG JAWAB SEPENUHNYA</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table cellpadding="3" width="100%">
        <tr>
            <td colspan='2'>PETUGAS PENERIMA BERKAS: {{ $permohonan['petugas_penerima_berkas'] }}</td>
        </tr>
        <tr>
            <td>NIP: -</td>
            <td align="right">Wajib Pajak: {{ $permohonan['nama_pemohon'] }}</td>
        </tr>
    </table>
    @php $nopecah++; @endphp
    @if(count($permohonan['pecah'])>=$nopecah)
    <div class="page-break"></div>
    @endif
    @endforeach
</body>
</html>
