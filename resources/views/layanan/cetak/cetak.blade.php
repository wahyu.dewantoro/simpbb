<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">Daftar Permohonan</h3>
        </th>
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>Nomor Layanan</th>
            <th>Jenis Layanan</th>
            <th>Nama</th>
            <th>Tgl Layanan</th>
            <th>Status</th>
            <th>-</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no=1; 
            $status=[
                '0'=>"Penelitian",
                '1'=>"Selesai",
                '2'=>"Berkas belum Masuk",
                '3'=>"Berkas ditolak",
            ];
        @endphp
        @foreach ($result as $row)
            @php $countData=$row['objek_count']; @endphp
            @if($row['jenis_layanan_id']=='6')
                @php $countData=$row['pecah_count']; @endphp
            @endif
            @if($row['jenis_layanan_id']=='7')
                @php $countData=$row['gabung_count']; @endphp
            @endif
                @if($countData<=$row['pemutakhiran_count']&&$row['kd_status']!='1'&&$row['jenis_layanan_id']!='8')
                    @php $row['kd_status']='1'; @endphp
                @endif
            <tr>
                <td align="center">{{ $no }}</td>
                <td >{{ $row['nomor_layanan'] }}</td>
                <td >{{ $row['jenis_layanan_nama'] }}</td>
                <td >{{ $row['nama']}}</td>
                <td >{{ $row['created_at']}}</td>
                <td>
                    @if(isset($status[$row['kd_status']])) 
                        @if($row['tolak_count']>0)
                            @if($row['jenis_objek']=='3')
                                {{ $status[$row['kd_status']] }}
                                {{"[".$row['tolak_count']."] Ajuan di tolak"}}
                            @else
                                {{" Ajuan di tolak"}}
                            @endif
                        @else
                            {{ $status[$row['kd_status']] }}
                        @endif
                    @else - @endif
                </td>
                <td></td>
            </tr>
            @php 
                $no++; 
            @endphp
        @endforeach
    </tbody>
</table>
