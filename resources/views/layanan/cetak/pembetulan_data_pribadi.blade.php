<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    <table width="100%" class="">
        <tr>
            <th width="70px"><img width="60px" height="70px" src="{{ public_path('kabmalang.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            {{-- <th width="100px"> --}}
                
                {{-- <img width="60px" src="{{ url('qrsppt') }}?nop={{ $sppt->kd_propinsi }}{{ $sppt->kd_dati2 }}{{ $sppt->kd_kecamatan }}{{ $sppt->kd_kelurahan }}{{ $sppt->kd_blok }}{{ $sppt->no_urut }}{{ $sppt->kd_jns_op }}{{ $sppt->thn_pajak_sppt }}" alt=""> --}}
            {{-- </th> --}}
        </tr>
    </table>
    <hr>
    <p style="text-align: center "><b><u>SURAT KETERANGAN PAJAK BUMI BANGUNAN</u></b><br> Nomor : {{ $permohonan['nomor_layanan'] }}</p>
    <p>Yang bertanda tangan di bawah ini :</p>
    <table>
        <tbody>
            <tr>
                <td width="100px">Nama</td>
                <td>:</td>
                <td>{{ $peg['nama'] }}</td>
            </tr>
            <tr>
                <td width="100px">Jabatan</td>
                <td>:</td>
                <td>Kepala Bidang PBB P2</td>
            </tr>
        </tbody>
    </table>
    <p style="text-indent: 45px; text-align: justify;">Sesuai dengan ketentuan Pasal 79 ayat (1) Undang - Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi
Daerah dan Pasal 90 Ayat (1) Peraturan Daerah Kab. Malang Nomor 8 Tahun 2010 tentang Pajak Daerah, Sebagaimana telah
diubah dengan peraturan daerah Kabupaten malang nomor 1 tahun 2019 tentang perubahan atas Peraturan Daerah nomor 8
tahun 2010 tentang Pajak Daerah.</p>
<p style="text-indent: 45px; text-align: justify;">Sesuai dengan pengajuan pembetulan yang di mohon oleh wajib pajak tertanggal 1 Januari 2022 dan setelah melalui
pemeriksaan dan penelitian maka Nomor objek pajak di bawah ini disetujui untuk dibetulkan</p>
    <table>
        <tbody>
            <tr>
                <td width="100px">Nomor Objek</td>
                <td>:</td>
                <td>{{ $permohonan['nop'] }}
                </td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table width="100%">
        <tbody>
            <tr>
                <td width="200px">Nama Pemilik</td>
                <td width="1px">:</td>
                <td>{{ $permohonan['nama_wp_before'] }}</sup></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $permohonan['alamat_wp_before'] }}</sup></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>No Telp</td>
                <td>:</td>
                <td>{{ $permohonan['nomor_telepon_before'] }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Alamat Objek</td>
                <td>:</td>
                <td>{{ $permohonan['alamat_op_before'] }}</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Luas Tanah</td>
                <td>:</td>
                <td>{{ $permohonan['luas_bumi'] }}</td>
                <td>Luas Bangunan</td>
                <td>:</td>
                <td>{{ $permohonan['luas_bng'] }}</td>
            </tr>
        </tbody>
    </table>
    <p><b> <u>Dirubah menjadi</u></b></p>
    
    <table width="100%">
        <tbody>
            <tr>
                <td width="200px">Nama WP</td>
                <td width="1px">:</td>
                <td>{{ $permohonan['nama_wp'] }}</sup></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $permohonan['alamat_wp'] }}</sup></td>
            </tr>
            <tr>
                <td>No Telp</td>
                <td>:</td>
                <td>{{ $permohonan['nomor_telepon'] }}</td>
            </tr>
            <tr>
                <td>Alamat Objek</td>
                <td>:</td>
                <td>{{ $permohonan['alamat_op'] }}</td>
            </tr>
        </tbody>
    </table>
    <p></p>
    <table style="border: 1px solid #C6C6C6;" cellpadding="5" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="   border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;">No</th>
                <th style="   border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;">Keterangan</th>
                <th style="   border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;">Luas</th>
             <th style="   border-right: 1px solid #C6C6C6;
            border-bottom: 1px solid #C6C6C6;">NJOP</th>
            </tr>
        </thead>
        <tr>
            <td style="border-right: 1px solid #C6C6C6; text-align:center">1</td>
            <td style=" border-right: 1px solid #C6C6C6;">Tanah</td>
            <td style=" border-right: 1px solid #C6C6C6;">{{ $permohonan['luas_bumi'] }}</td>
            <td style=" border-right: 1px solid #C6C6C6;">{{ $permohonan['njop_bumi'] }}</td>
        </tr>
        <tr>
            <td style="border-right: 1px solid #C6C6C6; text-align:center">2</td>
            <td style=" border-right: 1px solid #C6C6C6;">Bangunan</td>
            <td style=" border-right: 1px solid #C6C6C6;">{{ $permohonan['luas_bng'] }}</td>
            <td style=" border-right: 1px solid #C6C6C6;">{{ $permohonan['njop_bng'] }}</td>
        </tr>
    </table>
</body>
<p style="text-indent: 45px; text-align: justify;">Demikian surat keterangan ini dibuat sebagai penganti SPPT (surat pemberitahuan pajak terhutang daerah) tahun 2022
untuk dapat dipergunakan seperlunya. apabila di kemudian hari terdapat kekeliruan akan dibetulkan dan ditindaklanjuti sesuai
dengan ketentuan yang berlaku.</p>
<p></p>
<p></p>
<table width="100%" border="0">
    <tbody>
        <tr>
            <td width="30%"></td>
            <td></td>
            <td width="40%" align="center">
                <p>Kepanjen, {{ tglIndo($permohonan['tanggal_sk']) }}<br>A.N KEPALA BADAN PENDAPATAN DAERAH<br>Sekretaris<br>u.b.<br>Kepala Bidang PBB P2<br>
                    
                    {!! QrCode::size(70)->errorCorrection('M')->generate($url) !!}<br>
                    {{ $peg['nama']}}<br>NIP.{{ $peg['nip']}}
                </p>
            </td>
        </tr>
    </tbody>
</table>
</html>

