<html>

<head>
    <title>LAPORAN INDUK PECAH</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>LAPORAN INDUK PECAH</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
<div style="width:75%; float:left">
    <div>Kecamatan : {{$result['kecamatan']}}</div>
    <div>Kelurahan : {{$result['kelurahan']}}</div>
    
</div>
<div style="width:25%; float:left">
    <div> Tanggal : {{$result['tanggal']}}</div>
    <div> jenisInduk : {{$result['jenisInduk']}}</div>
</div>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>Nomor Layanan</th>
            <th>NOP</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Status</th>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
            <th>User Input/Penerima Berkas</th>
        </tr>
    </thead>
    <tbody>
            @php 
                $status=false; 
                $no=1;  
                $total_luas=0;
                $total_sisa=0;
            @endphp
       @foreach($result['data'] as $item)
            @php 
                $nop=$item->kd_propinsi.".".$item->kd_dati2.".".$item->kd_kecamatan.".".$item->kd_kelurahan.".".$item->kd_blok."-".$item->no_urut.".".$item->kd_jns_op;
            @endphp
                @if($item->nop_gabung==""||$item->nop_gabung==null)
                    @if($item->jenis_layanan_id=='7')
                        @php $nop="-NOP GABUNG- "; @endphp
                    @endif
                @else
                    @if($item->jenis_layanan_id=='6')
                        @php $nop="-NOP PECAH-"; @endphp
                    @endif
                @endif
                @if($item->jenis_layanan_id=='1')
                    @php $nop="-NOP BARU-"; @endphp
                @endif
                @php  $petugas="";@endphp
                @if($item->petugas!=$item->usernameinput)
                    @php $petugas=$item->petugas;@endphp
                @endif
                @php $luas_sisa=$item->hasil_pecah_hasil_gabung; @endphp

                @if($item->sisa_pecah_total_gabung>0)
                    @php $luas_sisa=$item->luas_bumi-$item->sisa_pecah_total_gabung; @endphp
                @endif
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->nomor_layanan}}</td>
                <td>{{$nop}}</td>
                <td>{{$item->nama_wp}}</td>
                <td>{{$item->alamat_op}}</td>
                <td>{{$item->raw_tag}}</td>
                <td>{{$item->luas_bumi}}</td>
                <td>{{$luas_sisa}}</td>
                <td>{{$item->usernameinput}}/{{$petugas}}</td>
            </tr>
            @php 
                $no++; 
                $total_luas=$item->luas_bumi+$total_luas;
                $total_sisa=$total_sisa+$luas_sisa;
            @endphp
        @endforeach
        <tr>
            <td colspan="6">Total</td>
            <td>{{$total_luas}}</td>
            <td>{{$total_sisa}}</td>
            <td></td>
        </tr>
    </tbody>
</table>
</body>
</html>
