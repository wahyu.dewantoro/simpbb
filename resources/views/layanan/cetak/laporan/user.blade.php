<html>

<head>
    <title>LAPORAN ENTRY USER</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 10px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>LAPORAN ENTRY USER</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
<div style="width:75%; float:left">
    Nama User : {{$result['nama']}}
</div>
<div style="width:25%; float:left">
    Tanggal : {{$result['tanggal']}}
</div>
<div>Jenis Penelitian : Penelitian Kantor</div>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>Nomor Layanan</th>
            <th>NOP</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jenis Layanan</th>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
            <th>Status</th>
            <th>petugas</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no=1; 
            $status=true;
            $total_luas=0;
            $total_bng=0;
            $statusLabel=[
                '0'=>"Penelitian",
                '1'=>"Selesai",
                '2'=>"Berkas belum Masuk",
            ];
        @endphp
       @foreach($result['data'] as $item)
            @if($item->penelitian=='2'&&$status)
                    <tr>
                        <td colspan="6">Total</td>
                        <td>{{$total_luas}}</td>
                        <td>{{$total_bng}}</td>
                        <td></td>
                    </tr>
                </tbody>
                </table>
                <br>
                <div>Jenis Penelitian : Penelitian Khusus</div>
                <br>
                <table width="100%" border="1" cellpadding="5" cellspacing="0" >
                    <thead class="text-center">
                        <tr>
                            <th class='number'>No</th>
                            <th>Nomor Layanan</th>
                            <th>NOP</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Jenis Layanan</th>
                            <th>Luas Bumi</th>
                            <th>Luas Bangunan</th>
                            <th>Status</th>
                            <th>Petugas</th>
                        </tr>
                    </thead>
                <tbody>
                @php 
                    $status=false; 
                    $no=1;  
                    $total_luas=0;
                    $total_bng=0;
                @endphp
            @endif
            @php 
                $nop=$item->kd_propinsi.".".$item->kd_dati2.".".$item->kd_kecamatan.".".$item->kd_kelurahan.".".$item->kd_blok."-".$item->no_urut.".".$item->kd_jns_op;
            @endphp
                @if($item->nop_gabung==""||$item->nop_gabung==null)
                    @if($item->jenis_layanan_id=='7')
                        @php $nop="-NOP GABUNG- "; @endphp
                    @endif
                @else
                    @if($item->jenis_layanan_id=='6')
                        @php $nop="-NOP PECAH-"; @endphp
                    @endif
                @endif
                @if($item->jenis_layanan_id=='1')
                    @php $nop="-NOP BARU-"; @endphp
                @endif
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->nomor_layanan}}</td>
                <td>{{$nop}}</td>
                <td>{{$item->nama_wp}}</td>
                <td>{{$item->alamat_op}}</td>
                <td>{{$item->jenis_layanan_nama}}</td>
                <td>{{$item->luas_bumi}}</td>
                <td>{{$item->luas_bng}}</td>
                <td>
                    @php
                        $setstatus="--";
                    @endphp
                    @if($item->pemutakhiran_at!=null&&$item->pemutakhiran_at!=""&&$item->kd_status!='1'&&$item->jenis_layanan_id!='8')
                        @php
                            $item->kd_status='1';
                        @endphp
                    @endif
                    
                    @if(in_array($item->kd_status,array_keys($statusLabel)))
                        @php $setstatus=$statusLabel[$item->kd_status]; @endphp
                    @endif
                    {{$setstatus}}
                </td>
                <td>{{ $item->petugas }}</td>
            </tr>
            @php 
                $no++; 
                $total_luas=$item->luas_bumi+$total_luas;
                $total_bng=$item->luas_bng+$total_bng;
            @endphp
        @endforeach
        <tr>
            <td colspan="6">Total</td>
            <td>{{$total_luas}}</td>
            <td>{{$total_bng}}</td>
            <td></td>
        </tr>
    </tbody>
</table>
</body>
</html>
