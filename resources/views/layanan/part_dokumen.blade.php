<div class="col-md-12">
    <div class="card mb-1">
        <div class="card-header">
            <h3 class="card-title"><b>Berkas Pendukung</b></h3>
        </div>
        <div class="card-body p-2">
                <!-- <div class="callout callout-warning col-sm-12">
                    <ol class="m-0">
                    <li>Pastikan Format Dokumen berjenis <b>[*.pdf,*.png,*.jpg]</b></li>
                    </ol>
                </div> -->
            <div class="col-md-12 p-0  data-source">
                <div class="row">
                <!-- <div class="form-group col-md-3">
                    <div class='input-group'>
                        <input type="file" class="form-control-sm form-control file" data-name='file'>
                        <div class="input-group-append">
                            <span class="input-group-text">Dokumen </span>
                        </div>
                    </div>
                </div> -->
                @php
                    $width='4';
                @endphp
                @if(isset($Jenis_layanan->id))
                @if($Jenis_layanan->id=='6'||$Jenis_layanan->id=='7')
                    @php
                        $width='3';
                    @endphp
                @endif
                @endif
                <div class="form-group col-md-{{$width}} mb-1">
                    <div class='input-group'>
                        <select class="form-control-sm form-control select change-next reFocus"  data-placeholder="Jenis Berkas." data-name='nama_dokumen'>
                            <option value=""></option>
                            @foreach ($jenisDokumen as $key=>$dokumen)
                                <option value="{{ $key }}"> {{ $dokumen }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-{{$width}} mb-1">
                    <div class='input-group'>
                        <input type="text" class="form-control-sm form-control change-next-append" maxlength="80" placeholder="Nomor Berkas" data-name="keterangan">
                    </div>
                </div>
                @if(isset($Jenis_layanan->id))
                @if($Jenis_layanan->id=='6')
                    <div class="form-group col-md-3 mb-1">
                        <div class='input-group'>
                            <select class="form-control-sm form-control selected"  data-placeholder="Jenis" data-name='jenis'>
                                <option value="0">Lampiran Induk</option>
                            </select>
                        </div>
                    </div>
                @endif
                @if($Jenis_layanan->id=='7')
                    <div class="form-group col-md-3 mb-1">
                        <div class='input-group'>
                            <select class="form-control-sm form-control selected"  data-placeholder="Jenis" data-name='jenis'>
                                <option value="0">Hasil Gabung</option>
                                <!-- <option value="1">Lampiran Nop Gabung</option> -->
                            </select>
                        </div>
                    </div>
                @endif
                @endif
                <div class="form-group col-md-{{$width}} mb-1">
                    <div class='input-group'>
                        <button id="tambah-dokumen" class='btn btn-info btn-block btn-sm' type="button"><i class="fas fa-plus-square"></i> Tambah</button>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card-split-row mb-1"></div>
            </div>
            <table id="dokumen-table" class="table table-bordered table-striped table-counter mb-0 table-sm">
                <thead class='text-center'>
                    <tr>
                        <th class='number'>No</th>
                        <!-- <th>Dokumen</th> -->
                        <th>Jenis Berkas</th>
                        <th>Nomor Berkas</th>
                        @if(isset($Jenis_layanan->id))
                        @if($Jenis_layanan->id=='6')
                            <th>-</th>
                        @endif
                        @if($Jenis_layanan->id=='7')
                            <th>Berkas dari NOP</th>
                        @endif
                        @endif
                        <th>-</th>
                    </tr>
                </thead>
                <tbody> </tbody>
            </table>
        </div>
    </div>
</div>