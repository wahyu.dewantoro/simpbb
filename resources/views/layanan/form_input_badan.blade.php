<form action="layanan/store" enctype="multipart/form-data" method="post" class="form-horizontal" data-loader="Proses layanan (Badan)" id="form-badan">
    @csrf
    <input type="hidden" name="jenisObjek" value="2">
    <div class="row">
        <div class="col-sm-12">
            <div class="card mb-2">
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Layanan*</label>
                                <div class="col-md-9 p-0">
                                    <select name="jenis_layanan_id" class="form-control-sm form-control form-control data-hide-list onFocus text-bold" data-placeholder="Pilih Jenis Layanan" required >
                                        <option value="">-- Pilih Jenis Layanan --</option>
                                        @foreach ($Jenis_layanan as $layanan)
                                            <option value="{{ $layanan->id }}">
                                                {{ $layanan->nama_layanan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 pr-1">
            <div class="card mb-2">
                <div class="card-header d-flex">
                    <h3 class="card-title">Data Objek Badan</h3>
                </div>
                <div class="card-body p-1">
                    <!-- <div class="card-split-row"></div> -->
                    <div class="row hidden"  data-append-show='jenis_layanan_id'  data-when='A B C D E F G H I'>
                        <div class="form-group col-md-12 mb-0" data-append-show='jenis_layanan_id' data-when='A'>
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Kecamatan*</label>
                                <div class="col-md-8 p-0">
                                    <select name="pre_kecamatan"  class="form-control-sm form-control select-bottom"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='pre_kelurahan'>
                                        <option value="">-- Kecamatan --</option>
                                        @foreach ($kecamatan as $rowkec)
                                            <option value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-0" data-append-show='jenis_layanan_id' data-when='A'>
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Kelurahan*</label>
                                <div class="col-md-8 p-0">
                                    <select name="pre_kelurahan"  class="form-control-sm form-control select-bottom select-reset change-next"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu)." >
                                        <option value="">-- Kelurahan --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-0" data-append-show='jenis_layanan_id' data-when='A'>
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Alamat*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" class="form-control-sm form-control change-next-append no-min" name="alamat_op" placeholder="Alamat." data-required='A'>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-0 hidden" data-append-show='jenis_layanan_id' data-when='A'>
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Blok/Kav/No</label>
                                <div class="col-md-8 p-0">
                                    <!-- data-required='A' -->
                                    <input type="text" class="form-control-sm form-control no-min" name="blok_kav_no" placeholder="Blok/Kav/No." >
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-0" data-append-show='jenis_layanan_id'  data-when='A'>
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">RT *</label>
                                <div class="col-md-2 p-0">
                                    <input type="text" name="rt_op"  maxlength="3" class="form-control-sm form-control numeric" placeholder="RT OP"  data-required='A'>
                                </div>
                                <label class="col-md-3 col-form-label">RW *</label>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="rw_op"  maxlength="2" class="form-control-sm form-control numeric" placeholder="RW OP"  data-required='A'>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-1 hidden" data-append-show='jenis_layanan_id'  data-when='B C D E F G H I' >
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-4 col-form-label col-form-label-sm mb-0">NOP*</label>
                                <input type="text" name="nop" class="form-control-sm form-control numeric nop_full focusList" placeholder="NOP">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-info btn-sm " data-nop='nop' aria-expanded="false">
                                        <i class='fas fa-share'></i> 
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 hidden" data-append-show='jenis_layanan_id'  data-when='B C D E F G H I' data-readonly="F G C H I">
                            <div class='input-group'>
                                <input type="text" name="nop_alamat" class="form-control-sm form-control no-min" placeholder="Alamat Lengkap NOP" >
                            </div>
                        </div>
                        <div class="form-group col-md-6 hidden" data-append-show='jenis_layanan_id'  data-when='B C D E F G H I' data-readonly="F G C H I">
                            <div class='input-group'>
                                <input type="text" name="kd_kls_tanah" class="form-control-sm form-control "  placeholder="Kelas Tanah">
                            </div>
                        </div>
                        <div class="form-group col-md-6 hidden" data-append-show='jenis_layanan_id'  data-when='B C D E F G H I' data-readonly="F G C H I">
                            <div class='input-group'>
                                <input type="text" name="kd_kls_bng" class="form-control-sm form-control "  placeholder="Kelas Bangunan">
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-readonly="G" data-unreadonly="F H I" data-append-show='jenis_layanan_id'  data-when='A B C D E F G H I'>
                        <div class="form-group col-md-12 mb-0" >
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label" data-append-show='jenis_layanan_id'  data-when='A B E F G H I'>Luas Bumi*</label>
                                <label class="col-md-4 col-form-label hidden" data-append-show='jenis_layanan_id'  data-when='C D'>Luas Bumi Asal*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="luas_bumi" class="form-control-sm form-control numeric"  maxlength="10" data-calc="jenis_layanan_id" placeholder="Luas Bumi" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='C D'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label" data-append-show='jenis_layanan_id'  data-when='C'>Luas Hasil Pecah*</label>
                                <label class="col-md-4 col-form-label" data-append-show='jenis_layanan_id'  data-when='D'>Luas Hasil Gagbung*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="hasil_pecah_hasil_gabung" data-calc="jenis_layanan_id" class="form-control-sm form-control numeric" placeholder="...." >
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='C D'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label" data-append-show='jenis_layanan_id'  data-when='C'>Sisa Hasil Pecah*</label>
                                <label class="col-md-4 col-form-label" data-append-show='jenis_layanan_id'  data-when='D'>Total Hasil Gabung*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="sisa_pecah_total_gabung" class="form-control-sm form-control numeric" data-calc-result="jenis_layanan_id" placeholder="...."  readonly>
                                </div>  
                            </div>
                        </div>
                    </div>

                    <div class="row hidden">
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">NJOP Bumi*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="njop_bumi" class="form-control-sm form-control numeric" placeholder="NJOP Bumi">
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-readonly="G" data-unreadonly="F H I"  data-append-show='jenis_layanan_id'  data-when='A B C D E F G H I'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Luas Bangunan*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="luas_bng" class="form-control-sm form-control numeric"  maxlength="10" placeholder="Luas Bangunan" required data-show="luas_bng">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden">
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">NJOP Bangunan*</label>
                                <div class="col-md-8 p-0">
                                    <input type="text" name="njop_bng" class="form-control-sm form-control numeric" placeholder="NJOP Bangunan">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row hidden" data-readonly="F G" data-unreadonly="H"  data-append-show='jenis_layanan_id'  data-when='A B C D E F G H'> -->
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='A'>
                        <!-- data-norequiredhide="E" -->
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">JPB</label>
                                <div class="col-md-8 p-0">
                                <!-- data-append-show='jenis_layanan_id'  data-when='A B E F H'  -->
                                    <select name="kelompok_objek_id"   class="form-control-sm form-control no-required"  data-placeholder="Jenis Penggunaan Bangunan" >
                                        <option value="">-- Jenis --</option>
                                        @foreach ($KelompokObjek as $kelompok)
                                            <option @if (request()->get('kelompok') == $kelompok->id)  selected @endif value="{{ $kelompok->id }}">
                                                {{ $kelompok->nama_kelompok }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" >
                        <!-- hidden--> 
                        <div class="form-group col-md-12 m-0">
                            <div class='input-group-sm input-group'>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">NIK</span>
                                </div>
                                <input type="text" name="nik_wp" class="form-control-sm form-control nik no_replace" placeholder="Nomor Induk Kependudukan"  data-usedto="nik">
                            </div>
                        </div>
                        <div class="form-group col-md-12 m-0">
                            <div class='input-group'>
                                <input type="text" name="nama_wp" class="form-control-sm form-control no_replace" placeholder="Nama" data-usedto="nama">
                            </div>
                        </div>
                        <div class="form-group col-md-12 m-0 hidden">
                            <div class='input-group'>
                                <input type="text" name="telp_wp" class="form-control-sm form-control no_replace" placeholder="Telp WP" data-usedto="nomor_telepon">
                            </div>
                        </div>
                        <div class="form-group col-md-6 m-0 pr-0">
                            <div class='input-group'>
                                <input type="text" name="alamat_wp" class="form-control-sm form-control no_replace no-min" placeholder="Alamat" data-usedto="alamat">
                            </div>
                        </div>
                        <div class="form-group col-md-3 m-0 pl-0 pr-0">
                            <div class='input-group'>
                                <input type="text" name="rt_wp" maxlength="3" class="form-control-sm form-control numeric no_replace" placeholder="RT" data-usedto="rt_wp_baru">
                            </div>
                        </div>
                        <div class="form-group col-md-3 m-0 pl-0">
                            <div class='input-group'>
                                <input type="text" name="rw_wp"  maxlength="2" class="form-control-sm form-control numeric no_replace" placeholder="RW" data-usedto="rw_wp_baru">
                            </div>
                        </div>
                        <div class="form-group col-md-6 m-0 pr-0">
                            <div class='input-group'>
                                <input type="text" name="kelurahan_wp" class="form-control-sm form-control no_replace" placeholder="Kelurahan" data-usedto="kelurahan">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-6 m-0 pl-0">
                            <div class='input-group'>
                                <input type="text" name="kecamatan_wp" class="form-control-sm form-control no_replace" placeholder="Kecamatan" data-usedto="kecamatan">
                                
                            </div>
                        </div>
                        <div class="form-group col-md-6 m-0 pr-0">
                            <div class='input-group'>
                                <input type="text" name="dati2_wp" class="form-control-sm form-control no_replace" placeholder="Kota/Kabupaten"  data-usedto="dati2" >
                            </div>
                        </div>
                        <div class="form-group col-md-6 m-0 pl-0">
                            <div class='input-group'>
                                <input type="text" name="propinsi_wp" class="form-control-sm form-control no_replace" placeholder="Propinsi" data-usedto="propinsi">
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='A'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Lokasi Objek*</label>
                                <div class="col-md-8 p-0">
                                <select name="lokasi_objek_id"   class="form-control-sm form-control"  data-placeholder="Pilih Lokasi Objek." >
                                        <option value="">-- Lokasi --</option>
                                        @foreach ($LokasiObjek as $lokasi)
                                            <option @if (request()->get('lokasi') == $lokasi->id)  selected @endif value="{{ $lokasi->id }}">
                                                {{ $lokasi->nama_lokasi }}
                                            </option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='A B C D E F G H I'>
                        <div class="form-group col-md-12 m-0 p-0">
                            <div class="card-split-row m-0 mb-2 p-2"></div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-4 col-form-label">Alasan</label>
                                    <textarea name="keterangan" class="form-control-sm form-control triggerHeight trigger-h105 thirdFocus">KETERANGAN</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 pl-1">
            <div class="card mb-2">
                <div class="card-header">
                    <h3 class="card-title">Data Subjek Pajak Badan</h3>
                </div>
                <div class="card-body p-1">
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='p-0 input-group input-group-sm'>
                                <label class="col-md-3 col-form-label col-form-label-sm mb-0">No. Akta*</label>
                                <input type="text" name="nik" class="form-control-sm form-control form-control form-control nik" placeholder="NO. Akta" required>
                                <div class="input-group-append ">
                                    <button type="button" class="btn btn-info btn-sm keypress" data-nik="layanan">
                                        <i class='fas fa-share'></i> 
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Nama Badan*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nama" class="form-control-sm form-control form-control secondFocus" placeholder="Nama Badan"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. SBU </label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_sbu_badan" class="form-control-sm form-control form-control secondFocus" placeholder="No. SBU "  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. kemenhum</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_kemenhum" class="form-control-sm form-control form-control secondFocus" placeholder="No. kemenhum"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. Telp*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_telepon" class="form-control-sm form-control form-control numeric " maxlength="15" minlength="6" placeholder="No Telp." required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Alamat*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="alamat" class="form-control-sm form-control form-control no-min" placeholder="Alamat"  required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12 mb-0 hidden" data-append-show='jenis_layanan_id' data-when='A F'>
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Blok/Kav/No</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" class="form-control-sm form-control no-min" name="blok_kav_no_wp" placeholder="Blok/Kav/No.">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='A B C D F'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">RT *</label>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="rt_wp_baru"  maxlength="3" class="form-control-sm form-control numeric" placeholder="RT WP">
                                </div>
                                <label class="col-md-3 col-form-label">RW *</label>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="rw_wp_baru"  maxlength="2" class="form-control-sm form-control numeric" placeholder="RW WP">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Kelurahan*</label>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="kelurahan" class="form-control-sm form-control form-control "placeholder="Kelurahan"  required>
                                </div>
                                <label class="col-md-2 col-form-label text-right">Kecamatan*</label>
                                <div class="col-md-4 p-0">
                                    <input type="text" name="kecamatan" class="form-control-sm form-control form-control "placeholder="Kecamatan"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Kota/Kabupaten*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="dati2" class="form-control-sm form-control form-control "placeholder="Kota/Kabupaten" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Propinsi*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="propinsi" class="form-control-sm form-control form-control "placeholder="Propinsi"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Email*</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="email" class="form-control-sm form-control no-required email" placeholder="Email" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">pengurusan*</label>
                                <div class="col-md-9 p-0">
                                    <select name="pengurus"  required class="form-control-sm form-control" data-placeholder="Pilih Pengurusan">
                                        <option value="1">Sendiri</option>
                                        <option value="2">di kuasakan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='A F'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Status Subjek Pajak</label>
                                <div class="col-md-9 p-0">
                                <select name="status_subjek_pajak"   class="form-control-sm form-control"  data-placeholder="Pilih Status Subjek Pajak." >
                                        <option value="">-- Lokasi --</option>
                                        @foreach ($SSubjekPajak as $key=>$val)
                                            <option value="{{ $key }}"> {{ $val }} </option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hidden" data-append-show='jenis_layanan_id'  data-when='I'>
                        <div class="form-group col-md-12 mb-0">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Jenis Pengurangan</label>
                                <div class="col-md-9 p-0">
                                <select name="jenis_pengurangan"   class="form-control-sm form-control"  data-placeholder="Pilih Jenis Pengurangan." >
                                        <option value="">-- Kriteria --</option>
                                        @foreach ($jenisPengurangan as $itemJ)
                                            <option value="{{ $itemJ->id }}"> {{ $itemJ->nama_pengurangan }} </option>
                                        @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="text" name="tanggal_lahir_wp" class="form-control-sm form-control inputdate hidden">
                </div>
            </div>
        </div>
        <div class="col-md-12 hidden" data-append-show='jenis_layanan_id'  data-when='B C D E G H I'>
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Riwayat NOP Belum lunas</h3>
                </div>
                <div class="card-body" >
                    <table class="table table-bordered table-striped table-counter table-sm">
                        <thead class='text-center'>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">NOP</th>
                                <th rowspan="2">Tahun</th>
                                <th rowspan="2">Nama</th>
                                <th rowspan="2">PBB</th>
                                <th colspan="3">Pembayaran</th>
                                <th rowspan="2">Keterangan</th>
                            </tr>
                            <tr>
                                <th>Pokok</th>
                                <th>Denda</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody data-name="riwayat_pembayaran" class="tag-html"></tbody>
                    </table>
                </div>
            </div>
        </div>
        @include('layanan/part_dokumen')
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
            <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Simpan</button> </div>
        </div>
    </div>
</form>