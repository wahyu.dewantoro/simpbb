@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Data Input Layanan</b></h3>
                        </div>
                        <div class="card-body p-2">
                            <form action="#laporan_input/search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-3 mb-1">
                                        <div class="input-group">
                                            <select name="layanan"  required class="form-control select"  data-placeholder="Pilih Layanan">
                                                <option value="-">-- Semua Layanan --</option>
                                                @foreach ($Jenis_layanan as $layanan)
                                                    <option value="{{ $layanan->id }}">
                                                        {{ $layanan->nama_layanan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 mb-1">
                                        <div class="input-group">
                                            <select name="jenis_objek"  required class="form-control select"  data-placeholder="Pilih Jenis Data">
                                                <option value="-">-- Semua Jenis data--</option>
                                                <option value="1">Pribadi</option>
                                                <option value="2">Badan</option>
                                                <option value="3">Kolektif</option>
                                                <option value="5">Permohonan BPHTB</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 mb-1">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="input-group input-group-sm">
                                                    <input type="checkbox" class="form-control form-control-sm"  name="all" value='all'>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            Semua
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="input-group  input-group-sm">
                                                    <input id="daterangepicker" type="text" class="form-control form-control-sm"  name="tgl" placeholder="Tanggal Pembuatan">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3 mb-1">
                                        <div class="input-group">
                                            <select name="status"  required class="form-control select"  data-placeholder="Pilih Status">
                                                <option value="-">-- Semua Status input--</option>
                                                <option value="0">Penelitian</option>
                                                <option value="1">Selesai [di setujui]</option>
                                                <option value="x">Selesai [Ajuan di tolak/sebagian]</option>
                                                <option value="2">Berkas Belum Masuk</option>
                                                <option value="3">Berkas di tolak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2 mb-1">
                                        <span class="input-group-text p-1 pl-2 ">
                                        NOP 
                                        </span>
                                    </div>
                                    <div class="form-group col-md-2 mb-1">
                                        <div class="input-group">
                                            <input type="text" name="nop" class="form-control form-control-sm nop_full" placeholder="NOP">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2 mb-1">
                                        <span class="input-group-text p-1 pl-2 ">
                                        NIK WP / NAMA WP 
                                        </span>
                                    </div>
                                    <div class="form-group col-md-3 mb-1">
                                        <div class="input-group">
                                            <input type="text" name="nik_nama_wp" class="form-control form-control-sm" placeholder="NIK WP / NAMA WP">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2 mb-1">
                                        <div class="input-group">
                                            <button type='submit' class='btn btn-block btn-primary btn-sm '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1 mb-1">
                                        <div class="input-group input-group-sm">
                                            <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Cetak </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="callout callout-warning col-sm-12 mb-2 p-2" data-append="callout">
                                <ul class=" m-0">
                                    <li>Untuk melakukan pencarian data pada 1 hari tertentu, tekan 2x tanngal tersebut pada pilihan tanngal pencarian</li>
                                </ul>
                            </div>
                            <div class="card-body no-padding">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Nomor Layanan</th>
                                            <th>Jenis Layanan</th>
                                            <th>Nama</th>
                                            <th>Tgl Layanan</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            
                    @include('layanan/detail_layanan')
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'list':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('laporan_input/search') }}",
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'nomor_layanan',class:'w-15'},
                    { data: 'jenis_layanan_nama',class:'w-20'},
                    { data: 'nama'},
                    { data: 'created_at',class:'w-15 text-center'},
                    { data: 'kd_status',class:'text-center w-10'},
                    { data: 'option',class:'text-center w-10'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let timer=60000;//1s = 1000
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            setInterval( function () {
                resLoad();
            }, timer );
            
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    if(key.name!='_token'){
                        str.push(key.name+"="+key.value);
                    }
                });
                return str.join("&");
            }
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian daftar input layanan.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    swal.close();
                });
            });
            $(document).on("click", "[data-card='remove']", function (evt) {
                let side=$("[data-card='side']");
                let main =$("[data-card='main']");
                $("[data-remote].active").removeClass("active");
                side.hide();
                main.show();
            });
            $(document).on("click", "[data-update]", function (evt) {
                let e=$(this),
                id=e.attr('data-update');
                Swal.fire({ 
                    icon: 'warning',
                    text: 'update Status Berkas Permohonan?',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    input: 'textarea',
                    inputLabel: 'Alamat Berkas GDrive',
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses update Status Berkas Permohonan',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        asyncData("kolektif_online/updateberkas",{nomor_layanan:id,berkas:willsend.value}).then((response) => {
                            resLoad();
                            if(!response.status){
                                return Swal.fire({ icon: 'error', html: response.msg}); 
                            }
                            Swal.close()
                        }).catch((error)=>{
                            Swal.fire({ icon: 'error', text: defaultError});
                        });
                    }
                });
            })
            $(document).on("click", "[data-tolak]", function (evt) {
                let e=$(this),
                id=e.attr('data-tolak');
                Swal.fire({ 
                    icon: 'warning',
                    text: 'Tolak Berkas Permohonan?',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    input: 'textarea',
                    inputLabel: 'Alasan Penolakan',
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses Penolakan Berkas Permohonan',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        asyncData("kolektif_online/tolakberkas",{nomor_layanan:id,keterangan:willsend.value}).then((response) => {
                            resLoad();
                            if(!response.status){
                                return Swal.fire({ icon: 'error', html: response.msg}); 
                            }
                            Swal.close()
                        }).catch((error)=>{
                            Swal.fire({ icon: 'error', text: defaultError});
                        });
                    }
                });
            })
            $(document).on("click", "[data-delete]", function (evt) {
                let e=$(this),
                    id=e.attr('data-delete');
                Swal.fire({ 
                    icon: 'warning',
                    text: 'Hapus data Ajuan?',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                    input: 'textarea',
                    inputLabel: 'Keterangan Penghapusan Permohonan',
                    inputPlaceholder: 'Masukkan data Pemohon penghapusan Ajuan beserta alasan penghapusan.',
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses Hapus Ajuan Layanan',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        asyncData("laporan_input/delete",{nomor_layanan:id,keterangan:willsend.value}).then((response) => {
                            resLoad();
                            if(!response.status){
                                return Swal.fire({ icon: 'error', html: response.msg}); 
                            }
                            Swal.close()
                        }).catch((error)=>{
                            Swal.fire({ icon: 'error', text: defaultError});
                        });
                    }
                });
            })
            $(document).on("change", "[data-name='file']", function (evt) {
                let e=$(this);
                if(!e.hasExtension(['.pdf','jpg','png'])){
                    e.val('');
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Format file tidak sesuai!!!'
                    });
                };
            });
            $(document).on("click", ".tambah-dokumen", function (evt) {
                let sourcelist=$(this).closest('.data-source').find('[data-name]');
                let e=$(this);
                let form =e.closest('form');
                let result=true,resultdata=[];
                const d = new Date();
                let time = d.getTime();
                let btndel="<button type='button' class='btn btn-warning btn-block' btn-delete='.dokumen-table' ><i class='far fa-trash-alt'></i> Hapus</buton>";
                let cloneFile;
                sourcelist.map((index,e)=>{
                    let source=$(e);
                    let tag;
                    if(source.val()){
                        let textval=source.val();
                        if(source.hasClass('file')){
                            cloneFile=source.clone().attr('name','dokumen['+time+']['+source.attr('data-name')+']').addClass('hidden');
                            tag=textval.replace(/.*(\/|\\)/, '')+'<span data-id="'+time+'"></span>';
                        }else{
                            tag='<input name="dokumen['+time+']['+source.attr('data-name')+']" value="'+source.val()+'" type="hidden"> '+textval;
                        }
                        resultdata.push(tag);
                    }else{
                        result=false;
                    };
                });
                if(result){
                    form.find(".dokumen-table tbody").append("<tr><td></td><td>"+resultdata.join('</td><td>')+"</td><td class='p-in-4'>"+btndel+"</td></tr>");
                    form.find(".dokumen-table tbody [data-id='"+time+"']").html(cloneFile);
                    form.find(".dokumen-table tbody>.null").remove();
                }
            });
            $(document).on("click", "[data-detail]", function (evt) {
                let e=$(this),
                    id=e.attr('data-detail'),
                    side=$("[data-card='side']"),
                    main =$("[data-card='main']"),
                    target=$("[data-remote='"+id+"']");
                if(target.length){
                    target.addClass('active');
                    main.hide();
                    side.show();
                    return;
                };
                Swal.fire({
                    title: 'Detail data dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let clone=$("[data-remote='clone']").clone();
                clone.attr('data-remote',id);
                clone.addClass('active');
                asyncData("laporan_input/detail",{nomor_layanan:id}).then((response) => {
                    if(!response.status){
                        clone.remove();
                        return Swal.fire({ icon: 'error', html: response.msg}); 
                    }
                    $.when.apply($, $.each(response.data, (x, y) => {
                        clone.find('[data-name="'+x+'"]').html(y);
                    })).then(()=>side.append(clone)).then(() => {
                        swal.close();
                        main.hide();
                        side.show();
                    });
                }).catch((error)=>{
                    clone.remove();
                    Swal.fire({ icon: 'error', text: defaultError});
                });
            });
            $(document).on("click", ".btn-export", function (evt) {
                let e=$(this),
                    form=e.closest('form'),
                    url="{{url('laporan_input/export')}}?"+toString(form.serializeArray());
                    window.open(url, '_blank');
            });
        });
    </script>
@endsection