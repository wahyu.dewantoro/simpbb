<div class="col-md-6">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="card-title">Data Badan</h3>
                </div>
                <div class="card-body">
                  
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. Akta</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_akta_badan" class="form-control nik" placeholder="No. Akta"required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Nama Badan</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nama_badan" class="form-control "placeholder="Nama Badan" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. SBU </label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_sbu_badan" class="form-control " placeholder="No. SBU "  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. kemenhum</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_kemenhum" class="form-control " placeholder="No. kemenhum" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Kelurahan</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="kelurahan" class="form-control " placeholder="Kelurahan" data-usedto="kelurahan_wp" required>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Kecamatan</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="kecamatan" class="form-control " placeholder="Kecamatan" data-usedto="kecamatan_wp" required>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Kota/Kabupaten</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="dati2" class="form-control "placeholder="Kota/Kabupaten" data-usedto="dati2_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Propinsi</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="propinsi" class="form-control "placeholder="Propinsi" data-usedto="propinsi_wp" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">No. Telp</label>
                                <div class="col-md-9 p-0">
                                    <input type="text" name="nomor_telepon" class="form-control numeric "placeholder="No Telepon yang bisa di hubungi" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-12">
                            <div class='input-group'>
                                <label class="col-md-3 col-form-label">Keterangan</label>
                                <div class="col-md-9 p-0">
                                    <textarea name="keterangan" class="form-control default-textarea triggerHeight"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>