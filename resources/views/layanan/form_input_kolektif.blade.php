<form action="#layanan/preview" id="form-import">
    @csrf   
    <div class="row">
        <div class="form-group col-sm-8">
            <div class="input-group">
                <div class="custome-file">
                    <input type="file" class="custom-file-input" name='import_excel'>
                    <label class="custom-file-label" for="import_excel">Import Excel</label>  
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <a href="{{URL::to('/public/format_kolektif_layanan.xls')}}" class="btn btn-success btn-block">Download Format Excel</a>
        </div>
        <div class="col-sm-2">
            <a href="{{URL::to('/public/Tutorial_pengisian_layanan_kolektif.pdf')}}" class="btn btn-success btn-block">Panduan Kolektif</a>
        </div>
        <div class="callout callout-warning col-sm-12">
            <ol class='m-0'>
                <li>Pastikan file excel yg di import berjenis *.xls/xlsx</li>
                <li>Format file Excel harus sama dengan format yang telah di sediakan</li>
                <li>Pastikan format data merupakan data asli (Bukan mengambil dari data lain sperti Vlookup,dst)</li>
                <li>Maksimal data yg di proses dalam satu kali proses import Excel tidak lebih dari 500 data</li>
            </ol>
        </div>
        
    </div>
    
</form>
<div class="row">
    <div class="col-12 col-sm-12">
        <div class="card">
            <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                    <li class="nav-item"><a class="nav-link active" href="#tab_31" data-toggle="tab"> <i class="fas fa-bars"></i> Data Layanan Kolektif ( <span name='jumlah_nop'>0</span> ) </a></li>
                    <li class="nav-item"><a class="nav-link" href="#tab_32" data-toggle="tab"> <i class="fas fa-book"></i> Data tidak valid ( <span name='jumlah_nop_invalid'>0</span> ) </a></li>
                </ul>
            </div>
            <div class="card-body p-0">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_31" data-append='preview_kolektif'>
                        <div class="row">
                            <div class="col-sm-3 br-1">
                                <div class="nav flex-column nav-tabs nav-tabs-right h-100" id="vert-tabs-right-tab" role="tablist" aria-orientation="vertical">
                                @php $active='active'; @endphp
                                @foreach ($layanan_all as $layanan)
                                    <a class="nav-link {{ $active }} m-2 tabs-h" 
                                        id="{{ $layanan->id }}-tab" 
                                        data-toggle="pill" 
                                        href="#content-{{ $layanan->id }}" 
                                        role="tab" 
                                        aria-controls="content-{{ $layanan->id }}" 
                                        aria-selected="true">
                                        <span>{{ $layanan->nama_layanan }}</span> <b class="pr">[ <span id="tab-count-{{ $layanan->id }}">0</span> ]</b>
                                    </a>
                                     @php $active=''; @endphp
                                @endforeach
                                </div>
                            </div>
                            <div class="col-sm-9 pl-0">
                                <form action="#" id="form_import">
                                    @csrf 
                                    <div class="tab-content" id="vert-tabs-right-tabContent">
                                        @php $active='active'; @endphp
                                        @foreach ($layanan_all as $layanan)
                                            <div class="tab-pane fade show {{ $active }}" id="content-{{ $layanan->id }}" role="tabpanel" aria-labelledby="{{ $layanan->id }}-tab">
                                                <div class="card">
                                                    <div class="card-body body-scroll">
                                                        <table id="table-kolektif-{{ $layanan->id }}" class="table table-bordered table-striped table-counter">
                                                            <thead>
                                                                <tr>
                                                                    <th>NO</th>
                                                                    <th>NIK</th>
                                                                    <th>Nama WP</th>
                                                                    <th>Kota/Kabupaten</th>
                                                                    <th>Propinsi</th>
                                                                    <th>NOP</th>
                                                                    <th>Option</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="null">
                                                                    <td colspan="7" class="dataTables_empty text-center"> Data {{ $layanan->nama_layanan }} Masih Kosong.</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            @php $active=''; @endphp
                                        @endforeach
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_32" data-append='preview_kolektif_invalid'>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title lh-35">Data tidak valid untuk masuk layanan kolektif</h3>
                                <div class="card-widget bold">
                                    <ul>  <li>Jumlah Layanan   : <span name='jumlah_nop_invalid'>0</span></li> </ul>
                                </div>
                            </div>
                            <div class="card-body body-scroll">
                                <table id="example2" class="table table-bordered table-striped table-counter">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Layanan</th>
                                            <th>NIK</th>
                                            <th>Nama WP</th>
                                            <th>No Telp</th>
                                            <th>Alamat</th>
                                            <th>RT/RW</th>
                                            <th>Kelurahan</th>
                                            <th>Kecamatan</th>
                                            <th>Kabupaten/Kota</th>
                                            <th>Propinsi</th>
                                            <th>Keterangan</th>
                                            <th>NOP</th>
                                            <th>NOP Induk</th>
                                            <th>Alamat Objek</th>
                                            <th>RT/RW Objek</th>
                                            <th>Kelurahan Objek</th>
                                            <th>Kecamatan Objek</th>
                                            <th>Luas Bumi</th>
                                            <th>Hasil Pecah</th>
                                            <th>Sisa Pecah</th>
                                            <th>Hasil Gabung</th>
                                            <th>Total Gabung</th>
                                            <th>Luas Bangungan</th>
                                            <th>Jenis Penggunaan Bangunan</th>
                                            <th>Lokasi Objek</th>
                                            <th>status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="null">
                                            <td colspan="26" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-md-6"><a href="layanan" class="btn btn-block btn-default">Batal</a></div>
        <div class="col-md-6"><a href="javascript:;" class="btn btn-block btn-primary"  data-uri='#layanan/store_kolektif' data-form='#form_import'>Simpan Layanan Kolektif</a></div>
    </div>
</div>

<div class="modal fade" id="modal-detail-kolektif">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Layanan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <div class="input-group-prepend">
                                <span class="input-group-text">Jenis Layanan</span>
                            </div>
                            <input type="text" name="nik_wp" class="form-control" placeholder="Jenis Layanan" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="bg-warning color-palette p-2"> <b>Data Subjek Pajak</b> </p>
                        <div class='row'>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">NIK</span>
                                    </div>
                                    <input type="text" name="nik_wp" class="form-control" placeholder="NIK WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Nama</span>
                                    </div>
                                    <input type="text" name="nama_wp" class="form-control" placeholder="Nama WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">No Telp</span>
                                    </div>
                                    <input type="text" name="telp_wp" class="form-control" placeholder="No Telp WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">ALamat</span>
                                    </div>
                                    <textarea type="text" name="alamat_wp" class="form-control" placeholder="Alamat WP" readonly> </textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">RT/RW</span>
                                    </div>
                                    <input type="text" name="rtrw_wp" class="form-control" placeholder="RT/RW WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Kelurahan</span>
                                    </div>
                                    <input type="text" name="kelurahan_wp" class="form-control" placeholder="Kelurahan WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Kecamatan</span>
                                    </div>
                                    <input type="text" name="kecamatan_wp" class="form-control" placeholder="Kecamatan WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Kabupaten/Kota</span>
                                    </div>
                                    <input type="text" name="dati2_wp" class="form-control" placeholder="Kabupaten/Kota WP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Propinsi</span>
                                    </div>
                                    <input type="text" name="propinsi_wp" class="form-control" placeholder="Propinsi WP" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="bg-warning color-palette p-2"> <b>Data Objek Pajak</b> </p>
                        <div class='row'>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">NOP</span>
                                    </div>
                                    <input type="text" name="nop" class="form-control" placeholder="NOP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Alamat OP</span>
                                    </div>
                                    <textarea type="text" name="alamat_op" class="form-control" placeholder="Alamat OP" readonly></textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">RT/RW OP</span>
                                    </div>
                                    <input type="text" name="rtrw_op" class="form-control" placeholder="RT/RW OP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Kelurahan</span>
                                    </div>
                                    <input type="text" name="kelurahan_op" class="form-control" placeholder="Kelurahan OP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Kecamatan</span>
                                    </div>
                                    <input type="text" name="kecamatan_op" class="form-control" placeholder="Kecamatan OP" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Luas Bumi</span>
                                    </div>
                                    <input type="text" name="luas_bumi" class="form-control" placeholder="Luas Bumi" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Luas Bangunan</span>
                                    </div>
                                    <input type="text" name="luas_bng" class="form-control" placeholder="Luas Bangunan" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-6 hidden">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Luas Bumi C</span>
                                    </div>
                                    <input type="text" name="nik_wp" class="form-control" placeholder="Luas Bumi" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-6 hidden">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Luas Bangunan C</span>
                                    </div>
                                    <input type="text" name="nik_wp" class="form-control" placeholder="Luas Bangunan" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Jenis Penggunaan Bangunan</span>
                                    </div>
                                    <input type="text" name="kelompok_objek_nama" class="form-control" placeholder="Jenis Penggunaan Bangunan" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class='input-group'>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Lokasi Objek</span>
                                    </div>
                                    <input type="text" name="lokasi_objek_nama" class="form-control" placeholder="Lokasi_objek" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <div class="input-group-prepend">
                                <span class="input-group-text">Keterangan</span>
                            </div>
                            <textarea type="text" name="keterangan" class="form-control" placeholder="Keterangan Layanan" readonly></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
