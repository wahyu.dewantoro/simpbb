@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <ul class="nav nav-pills p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"> <i class="fas fa-book"></i> Form Mutasi Pecah Pribadi </a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"> <i class="fas fa-book"></i> Form Mutasi Pecah Badan</a></li>
                                <!-- <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab"> <i class="fas fa-book"></i> Form Layanan Kolektif</a></li> -->
                            </ul>
                        </div>
                        <div class="card-body no-padding">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="card">
                                        <div class="card-body  p-1">
                                        @include('layanan/ajuan/mutasi_pecah')
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <div class="card">
                                        <div class="card-body p-1">
                                            @include('layanan/ajuan/mutasi_pecah_badan')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
         $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            const changeData=function(e,async_status=true){
                let uri=e.attr("data-change").split("#"),
                    target=e.closest('form').find("[name='"+e.attr("data-target-name")+"']");
                    target.appendData(uri[1],{kd_kecamatan:e.val()},false,async_status);
                return target;  
            };
            const calc=function(e){
                let form=e.closest('form'),
                source=e.attr('data-calc'),
                result=0,
                a=form.find('[name="luas_bumi"]').val(),
                b=form.find('[name="hasil_pecah_hasil_gabung"]').val();
                a=parseInt(a);
                b=parseInt(b);
                if(!isNaN(a)&&!isNaN(b)){
                    let jenis=form.find('[name="'+source+'"]').val();
                    if(jenis=='6'){
                        result=(a-b>0)?a-b:0;
                    };
                    if(jenis=='7'){
                        result=a+b;
                    }
                }
                form.find('[name="sisa_pecah_total_gabung"]').val(result);
            };
            $(document).on("keyup", "[data-calc]", function (evt) {
                let e=$(this);
                calc(e);
            });
            // $(".rt").inputmask('999');
            // $(".rw").inputmask('99');
            $(document).on("change", ".data-hide-list", function (evt) {
                let typeform={
                    '1':'A',
                    '2':'B',
                    '3':'B',
                    '4':'B',
                    '5':'B',
                    '6':'C',
                    '7':'D',
                    '8':'E'
                };  
                let e=$(this),
                    val=e.val(),
                    form=e.closest("form"),
                    target=form.find("[data-append-show='"+e.attr('name')+"'][data-when~='"+typeform[val]+"']"),
                    unTarget=form.find("[data-append-show='"+e.attr('name')+"']");
                unTarget.addClass('hidden');
                target.removeClass('hidden');
                if(val=='8'){
                    form.find('[data-readonly="'+typeform[val]+'"] input').attr('readonly','readonly');
                    form.find('[data-norequiredhide="'+typeform[val]+'"] [name="kelompok_objek_id"]').removeAttr('required');
                    form.find('[data-norequiredhide="'+typeform[val]+'"] [name="lokasi_objek_id"]').removeAttr('required');
                    form.find('[data-norequiredhide="'+typeform[val]+'"]').addClass('hidden');
                }else{
                    form.find('[data-readonly] input').removeAttr('readonly');
                    form.find('[data-norequiredhide] [name="kelompok_objek_id"]').attr('required','required');
                    form.find('[data-norequiredhide] [name="lokasi_objek_id"]').attr('required','required');
                    form.find('[data-norequiredhide').removeClass('hidden');
                }
            });
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this);
                changeData(e);
            });
            $(".inputdate").inputmask({ alias: "datetime", inputFormat: "dd - mm - yyyy"});
            $(".nik").inputmask('9999999999999999');
            $(".nop_full").inputmask('99.99.999.999.999-9999.9');
            $(".email").inputmask({   alias: "email"  });
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: value??{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $(document).on("click", "[data-objek-add]", function (evt) {
                let e=$(this),
                    form=e.closest("form"),
                    target=form.find('[data-table-add="'+e.attr('data-objek-add')+'"] [name]'),
                    targetReset=form.find('[data-table-add="'+e.attr('data-objek-add')+'"] [name]:not(.no-back)'),
                    tag="<td></td>",
                    tagInput="",
                    ret=true,
                    tableAppend=form.find('[data-name="list_pecah"]');
                    const d = new Date();
                    let time = d.getTime();
                let nik=0;
                let total=0;
                let cluster={
                    nop_kecamatanadd:'op',
                    nop_kelurahanadd:'op',
                    nop_alamatadd:'op',
                    blok_kav_no_opadd:'op',
                    nop_rtadd:'op',
                    nop_rwadd:'op',
                    luas_bumiadd:'op',
                    luas_bngadd:'op',
                    kelompok_objek_idadd:'op',
                    lokasi_objek_idadd:'op',
                    nikadd:'wp',
                    nama_wpadd:'wp',
                    agama_wpadd:'wp',
                    notelp_wpadd:'wp',
                    alamat_wpadd:'dw',
                    blok_kav_no_wpadd:'dw',
                    rt_wpadd:'wp',
                    rw_wpadd:'wp',
                    kelurahan_wpadd:'dw',
                    kecamatan_wpadd:'dw',
                    dati2_wpadd:'dw',
                    propinsi_wpadd:'dw'
                };
                let op='';
                let wp='';
                let dw='';
                let labelVal;
                $.each(target,(x,y)=>{
                    if(!$(y).val()&&!$(y).hasClass('no-required')){
                        ret=false;
                        $(y).parent().append('<label class="error">Harus di isi.</label>');
                    }
                    if($(y).attr('name') in cluster){
                        tagInput=' <input type="hidden" data-tinput="'+$(y).attr('data-input')+'" name="list_pecah['+time+']['+$(y).attr('data-input')+']" value="'+$(y).val()+'">';
                        if($(y).attr('data-input')=="luas_bumi_pecah"){
                            tagInput=" <input type='hidden' data-tinput='"+$(y).attr('data-input')+"' name='list_pecah["+time+"]["+$(y).attr('data-input')+"]' data-count='pecah' value='"+$(y).val()+"'>";
                            // total=parseInt($(y).val());
                        }
                        if($(y).attr('data-input')=="nik_wp_pecah"){
                            nik=$(y).val();
                        }
                        labelVal=$(y).val();
                        if($(y).hasClass('option')){
                            labelVal=(labelVal)?$(y).find('option:selected').text():'';
                        }
                        if(cluster[$(y).attr('name')]=='op'){
                            op=op+"<li><b>"+$(y).attr('data-label')+"</b> : <i>"+labelVal+"</i>"+tagInput+"</li>";
                        }
                        if(cluster[$(y).attr('name')]=='wp'){
                            wp=wp+"<li><b>"+$(y).attr('data-label')+"</b> : <i>"+labelVal+"</i>"+tagInput+"</li>";
                        }
                        if(cluster[$(y).attr('name')]=='dw'){
                            dw=dw+"<li><b>"+$(y).attr('data-label')+"</b> : <i>"+labelVal+"</i>"+tagInput+"</li>";
                        }
                    }
                    
                })
                tag=tag+"<td><ul class='m-0'>"+op+"</ul></td><td><ul class='m-0'>"+wp+"</ul></td><td><ul class='m-0'>"+dw+"</ul></td>";
                if(!ret){
                    return Swal.fire({ icon: 'warning', html: 'Data Pecah harap di isi.'});
                }
                let del='<span class="btn btn-danger btn-flat un" data-list-id="'+nik+'" title="Hapus Data " ><i class="fas fa-trash"></i></span>';
                let edit='<span class="btn btn-warning btn-flat un" data-edit-id="'+nik+'" title="Edit Data " ><i class="fas fa-file-signature"></i></span>';
                let button='<div clas="btn-group">'+del+edit+'</div>';
                // if(tableAppend.find('tr[data-nik]').length=='0'){
                let luasbumi=(parseInt($('[name="luas_bumi"]').val()))?$('[name="luas_bumi"]').val():0;
                
                tableAppend.find('tr.null').remove();
                tableAppend.append('<tr data-parent="'+nik+'">'+tag+'<td>'+button+'</td></tr>');

                // let dV=$("[data-name='jenis']").html();
                $.each(tableAppend.find('[data-count="pecah"]'),(x,y)=>{
                    total=parseInt(total)+parseInt($(y).val());
                });
                // if(parseInt(luasbumi)<total){
                //     tableAppend.find('[data-parent="'+nik+'"]').remove();
                //     return Swal.fire({ icon: 'warning', html: 'Data Pecah Luas bumi melebihi batas luas induk.'});
                // }
                //dV=dV+"<option value='"+time+"'>NOP Pecah "+pecahNo+"</option>";
                form.find("[data-name='jenis']").append("<option value='"+nik+"'>Lampiran Pecah ["+nik+"]</option>");
                form.find('[name="hasil_pecah_hasil_gabung"]').val(total);
                let totalHasil=parseInt(luasbumi)-parseInt(total);
                form.find('[name="sisa_pecah_total_gabung"]').val((totalHasil>0)?totalHasil:'0');
                targetReset.val('');
                form.find('.rePecah').focus();
                targetReset.parent().find('.error').remove();
                // }
            })
            $('.change-next').on('select2:select', function() {
                $(this).closest('.card-body').find('.change-next-append').focus();
            });
            // $(document).on("click", ".data-delete", function (evt) {
            //     let e=$(this),
            //     target=e.closest('tr'),
            //     table=e.closest('[data-name="list_pecah"]');
            //     target.remove();
            //     if(!table.find('tr').length){
            //         table.html('<tr class="null"> <td colspan="11" class="dataTables_empty text-center"> Data Masih Kosong</td></tr>');
            //     }
            //     let total=0;
            //     $.each(table.find('[data-count="pecah"]'),(x,y)=>{
            //         total=parseInt(total)+parseInt($(y).val());
            //     });
            //     let luasbumi=(parseInt($('[name="luas_bumi"]').val()))?$('[name="luas_bumi"]').val():0;
            //     $('[name="hasil_pecah_hasil_gabung"]').val(total);
            //     $('[name="sisa_pecah_total_gabung"]').val(parseInt(luasbumi)-parseInt(total));
            //     $('[data-name="jenis"] option[value="'+e.attr('data-triger')+'"]').remove();
            //     $('[data-parent="'+e.attr('data-triger')+'"]').closest('tr').remove();
            // })
            $(document).on("click", "[data-nop-add]", function (evt) {
                let e=$(this),
                card=e.closest(".card"),
                form=e.closest('form'),
                name=e.attr('data-nop-add'),
                v=card.find('[name="'+name+'"]').val(),
                uri='{{ url("mutasi_gabung/ceknop_tambah") }}';
                if(v){
                    Swal.fire({
                        title: 'Cek NOP',
                        html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                    let tbody=form.find('[data-name="list_nop"]');
                    asyncData(uri,{nop:v}).then((response) => {
                        if(!response.status){
                            return Swal.fire({ icon: 'error', html: response.msg}); 
                        };
                        tbody.find('.null').remove();
                        if(!tbody.find('[data-list-id="'+response.data_list_id+'"]').length){
                            tbody.append(response.data);
                        }
                        swal.close();
                    }).then((response) =>{
                        calcListGabung(tbody,form);
                    });
                }
            });
            $(document).on("click", "[data-list-id]", function (evt) {
                let e=$(this),
                form=e.closest('form');
                let tbody=form.find('[data-name="list_nop"]');
                e.closest('tr').remove();
                form.find('[data-name="jenis"] option[value="'+e.attr('data-list-id')+'"]').remove();
                calcListGabung(tbody,form);
            });
            $(document).on("click", "[data-edit-id]", function (evt) {
                let e=$(this),
                        tbody=e.closest("tbody"),
                        form=e.closest('form'),
                        table=form.find('[data-table-add="noppecah"]'),
                        cloneData=e.closest('tr').find('input[data-tinput]');
                Swal.fire({ 
                    icon: 'warning',
                    html: '<p>Edit Data Pecah?</p>',
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "tidak",
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        $.each(cloneData,(x,y)=>{
                            table.find('[data-input="'+$(y).attr('data-tinput')+'"]').val($(y).val());
                        });
                        e.closest('tr').remove();
                        form.find('[data-name="jenis"] option[value="'+e.attr('data-edit-id')+'"]').remove();
                        calcListGabung(tbody,form);
                    }
                });
            });
            let calcListGabung=function(tbody,form){
                let listlb=tbody.find('[data-name="luas_bumi"]');
                let total=0;
                if(!listlb.length){
                    tbody.append('<tr class="null"><td colspan="4" class="dataTables_empty text-center"> Data Masih Kosong</td></tr>');
                }
                $.each(listlb,(x,y)=>{
                    let v=parseInt($(y).html());
                    if(!isNaN(v)){
                        total=parseInt(total)+v;
                    }
                });
                form.find('[name="hasil_pecah_hasil_gabung"]').val(total);
                form.find('[name="hasil_pecah_hasil_gabung"]').trigger('keyup');
            }
            $(document).on("keyup", "[name='nop']", function (evt) {
                let e=$(this);
                if(e.inputmask('unmaskedvalue').length==18){
                    e.closest('.form-group').find('[data-nop]').trigger('click');
                }
            });
            $(document).on("click", "[data-nop]", function (evt) {
                let e=$(this),
                card=e.closest(".card"),
                form=e.closest('form'),
                name=e.attr('data-nop'),
                tInput=card.find('[name="'+name+'"]'),
                v=tInput.val(),
                vmask=tInput.inputmask('unmaskedvalue'),
                extra="&jenis=6",
                uri='api/ceknop?nop='+v+extra;
                if(vmask.length==18){
                    asyncData(uri,{}).then((response) => {
                        if(!response.status){
                            Swal.fire({ icon: 'error', html: response.msg}).then(()=>{
                                card.find('[name="'+name+'"]').focus();
                            }); 
                        };
                        $.each(response.data,(x,y)=>{
                            let setdata=card.find('[name="'+x+'"]');
                            let setdataadd=form.find('[name="'+x+'add"]');
                            if(!setdata.hasClass('no-change')){
                                setdata.val(y);
                                if(setdataadd.length&&setdataadd.hasClass('change')){
                                    setdataadd.val(y);
                                }
                                //setdata.attr('readonly','readonly');
                            };
                        });
                        $.each(response.tag,(x,y)=>{
                            let settag=form.find('[data-name="'+x+'"]');
                            if(settag){
                                settag.html(y);
                            };
                        });
                    }).then(()=>{
                        $('.btn-used').trigger('click');
                    });
                    $(".thirdFocus").focus();
                }else{
                    tInput.focus();
                }
            });
            $(document).on("keyup", "[name='nikadd']", function (evt) {
                let e=$(this);
                if(e.inputmask('unmaskedvalue').length==16){
                    e.closest('.form-group').find('[data-nikadd]').trigger('click');
                }
            });
            $(document).on("click", "[data-nikadd]", function (evt) {
                let e=$(this),
                    card=e.closest(".card"),
                    tInput=e.closest('.input-group').find('input'),
                    v=tInput.inputmask('unmaskedvalue'),
                    type=e.attr('data-nik'),
                    uri='api/ceknik?nik='+v;
                let listlayanan={
                    // 'nik':'nik',
                    'kec_name':'kecamatan_wpadd',
                    'nama_lgkp':'nama_wpadd',
                    'alamat':'alamat_wpadd',
                    // 'tgl_lhr':'tanggal_lahir_wp',
                    'kel_name':'kelurahan_wpadd',
                    'kab_name':'dati2_wpadd',
                    'prop_name':'propinsi_wpadd',
                    'blok_kav_no_wp':'blok_kav_no_wpadd',
                    'agama_wp':'agama_wpadd',
                    // 'email':'email',
                    // 'rt_wp_baru':'rt_wp_baru',
                    // 'rw_wp_baru':'rw_wp_baru',
                    'nomor_telepon':'notelp_wpadd',
                    'no_rt':'rt_wpadd',
                    'no_rw':'rw_wpadd',
                };
                if(v.length==16){
                    asyncData(uri,{}).then((response) => {
                        if(response.kode){
                            $.each(listlayanan,(x,y)=>{
                                if(response.raw[x]){
                                    let setdata=card.find('[name="'+y+'"]');
                                    let val=response.raw[x];
                                    if(setdata){
                                        setdata.val(val);
                                        card.find('.error').remove();
                                    };
                                };
                            });
                        }
                    });
                    $(".secondFocusAdd").focus();
                }else{
                    tInput.focus();
                };
            });
            $(document).on("keyup", "[name='nik']", function (evt) {
                let e=$(this);
                if(e.inputmask('unmaskedvalue').length==16){
                    e.closest('.form-group').find('[data-nik]').trigger('click');
                }
            });
            $(document).on("click", "[data-nik]", function (evt) {
                let e=$(this),
                    form=e.closest("form"),
                    tInput=e.closest('.input-group').find('input'),
                    v=tInput.inputmask('unmaskedvalue'),
                    type=e.attr('data-nik'),
                    uri='api/ceknik?nik='+v;
                let listlayanan={
                    'nik':'nik',
                    'kec_name':'kecamatan',
                    'nama_lgkp':'nama',
                    'alamat':'alamat',
                    'tgl_lhr':'tanggal_lahir_wp',
                    'kel_name':'kelurahan',
                    'kab_name':'dati2',
                    'prop_name':'propinsi',
                    'blok_kav_no_wp':'blok_kav_no',
                    'agama_wp':'agama_wp',
                    'email':'email',
                    'rt_wp_baru':'rt_wp_baru',
                    'rw_wp_baru':'rw_wp_baru',
                    //'kec_name':'kecamatan_wp',
                    //'nama_lgkp':'nama_wp',
                    //'alamat':'alamat_wp',
                    'nomor_telepon':'nomor_telepon',
                    'no_rt':'rt_wp',
                    'no_rw':'rw_wp',
                    //'kel_name':'kelurahan_wp',
                    //'kab_name':'dati2_wp',
                    //'prop_name':'propinsi_wp',
                };
                if(v.length==16){
                    // Swal.fire({
                    //     title: 'Cek NIK',
                    //     html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    //     showConfirmButton: false,
                    //     allowOutsideClick: false,
                    // });
                    asyncData(uri,{}).then((response) => {
                        if(response.kode){
                            $.each(listlayanan,(x,y)=>{
                                if(response.raw[x]){
                                    let setdata=form.find('[name="'+y+'"]');
                                    // let setdatawp=form.find('[name="'+y+'_wp"]');
                                    let val=response.raw[x];
                                    if(setdata){
                                        // if(setdata.hasClass('inputdate')){
                                        //     val=new Date(val);
                                        // }
                                        setdata.val(val);
                                        // setdatawp.val(val);
                                    };
                                };
                            });
                            
                        }
                        // swal.close();
                        // $(".secondFocus").focus();
                    }).catch((error)=>{
                        // Swal.fire({ icon: 'error', html: response.keterangan}).then(()=>{
                        //     $(".secondFocus").focus();
                        // });
                    });
                    // swal.close();
                    form.find(".secondFocus").focus();
                }else{
                    tInput.focus();
                };
            });
            $(document).on("click", ".btn-used", function (evt) {
                let e=$(this),
                    form=e.closest("form"),
                    listtarget=form.find("[data-usedto]");
                if(listtarget.length){
                    listtarget.map((index, e) =>{
                        let source=$(e),
                            sourceval=source.val();
                        if(sourceval){
                            let target=form.find("[name='"+$(e).attr("data-usedto")+"']");
                            if(target.length){
                                if(target.hasClass("select")){
                                    if(target.hasClass('data-duplicate')){
                                        target.html(source.html());
                                    };
                                    target.val(sourceval);
                                    target.trigger('change.select2');
                                    target.closest(".selectappend").removeClass("selectappend");
                                }else{
                                    target.val(sourceval);
                                };
                            };
                        };
                    });
                };
            });
            $(document).on("click", "[btn-delete]", function (evt) {
                let e=$(this);
                e.closest('tr').remove();
            });
            $(document).on("click", "#tambah-dokumen", function (evt) {
                let e=$(this),
                    form=e.closest("form"),
                    source=e.closest('.data-source');
                let sourcelist=source.find('[data-name]');
                let result=true,resultdata=[];
                const d = new Date();
                let time = d.getTime();
                let cloneFile;
                let selectId;
                sourcelist.map((index,e)=>{
                    let source=$(e);
                    let tag;
                    if(source.val()){
                        let textval=source.val();
                        // if(source.hasClass('file')){
                        //     cloneFile=source.clone().attr('name','dokumen['+time+']['+source.attr('data-name')+']').addClass('hidden');
                        //     tag=textval.replace(/.*(\/|\\)/, '')+'<span data-id="'+time+'"></span>';
                        // }
                        if(source.hasClass('selected')){
                            textval=source.find('option:selected').text();
                            selectId=source.val();
                        }
                        tag='<input name="dokumen['+time+']['+source.attr('data-name')+']" value="'+source.val()+'" type="hidden"> '+textval;
                        resultdata.push(tag);
                    }else{
                        result=false;
                    };
                });
                let btndel="<button type='button' class='btn btn-warning btn-block' btn-delete='#dokumen-table' data-parent='"+selectId+"' ><i class='far fa-trash-alt'></i> Hapus</buton>";
                if(result){
                    form.find("#dokumen-table tbody").append("<tr><td></td><td>"+resultdata.join('</td><td>')+"</td><td class='p-in-4'>"+btndel+"</td></tr>");
                    // $("#dokumen-table tbody [data-id='"+time+"']").html(cloneFile);
                }
                form.find('.reFocus').focus();
                source.find('.change-next-append').val('');
            });
            let resetForm=function(e){
                e[0].reset();
                e.find('#dokumen-table tbody').html('');
                e.find('.select-reset').html('');
                e.find('[data-name="riwayat_pembayaran"]').html('');
                e.find('.select').trigger('change.select2');
                e.find('[data-hide]').trigger('change');
                e.find('.data-hide-list').trigger('change');
                $('[data-name="list_pecah"]').html('<tr class="null"><td colspan="4" class="dataTables_empty text-center"> Data Masih Kosong</td></tr>');
                $('[data-name="jenis"]').html('<option value="0">Lampiran Induk</option>');
            };
            $(document).on("change", "[name='import_excel']", function (evt) {
                if(!$(this).hasExtension(['.xls', '.xlsx', '.csv'])){
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Format file tidak sesuai!!!'
                    });
                };
                let e=$(this),
                    form=e.closest("form"),
                    uri=form.attr("action").split("#"),
                    name=e.val()??'Import Excel';
                let namefile=evt.target.files[0].name;
                $("label[for='"+e.attr("name")+"']").html(namefile);
                Swal.fire({
                    title: 'Proses menampilkan dan validasi data dari Excel',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                form.ajaxSubmit({
                    type: "post",
                    url: uri[1] ,
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                           return Swal.fire({ icon: 'error', text: response.msg});
                        }
                        swal.close();
                        pageAppend('preview_kolektif',
                                response.preview_kolektif,
                                response.jumlah_nop,
                                response.preview_kolektif_invalid,
                                response.jumlah_nop_invalid);
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultErrorPreview});
                    }
                });
            });
            let pageAppend=function(Appendroot,tbody,jumlah_nop,tbody_false,jumlah_nop_false){
                $("[data-append='"+Appendroot+"'] tbody").html(tbody);
                if(tbody_false){
                    $("[data-append='"+Appendroot+"_invalid'] tbody").html(tbody_false);
                };
                $("[name='jumlah_nop']").html(jumlah_nop);
                $("[name='jumlah_nop_invalid']").html(jumlah_nop_false);
            };
            $(document).on("click", "[data-uri]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-uri").split("#"),
                    form=$(e.attr("data-form")),
                    length=form.find("[data-name='nik']").length;
                if(!length){
                    return Swal.fire({icon: 'warning',title: 'Oops...',text: 'Data Masih Kosong'});
                }
                Swal.fire({ 
                    icon: 'warning',
                    text: 'Pastikan Seluruh data Layanan Kolektif sudah benar.\n Simpan Daftar Layanan Kolektif?',
                    showCancelButton: true,
                    confirmButtonText: "Simpan",
                    cancelButtonText: "Batal",
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses pembuatan data Layanan kolektif',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        let empty='<td colspan="20" class="dataTables_empty text-center"> Data Masih Kosong</td>';
                        form.ajaxSubmit({
                            type: "post",
                            url: uri[1] ,
                            dataType: "json",
                            success: function (response) {
                                if(!response.status){
                                    return Swal.fire({ icon: 'error', text: response.msg});
                                }
                                Swal.fire({ icon: 'success', text: response.msg}).then((reload)=>{ 
                                    pageAppend('preview_kolektif',empty,0,empty,0);
                                });
                            },
                            error: function (x, y) {
                                //x.responseJSON.message??
                                Swal.fire({ icon: 'error', text: defaultError});
                            }
                        });
                    }
                });
            });
            $(document).on("change", "[data-name='file']", function (evt) {
                let e=$(this);
                if(!e.hasExtension(['.pdf','jpg','png'])){
                    e.val('');
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Format file tidak sesuai!!!'
                    });
                };
            });
            const openData= function (verb, url, data, target) {
                var form = document.createElement("form");
                form.action = url;
                form.method = verb;
                form.target = target || "_self";
                if (data) {
                    for (var key in data) {
                        var input = document.createElement("textarea");
                        input.name = key;
                        input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
                        form.appendChild(input);
                    }
                };
                form.style.display = 'none';
                document.body.appendChild(form);
                form.submit();
            };
            $(document).ready(function() {
                $(".onFocus").first().focus();
            });
            jQuery.extend(jQuery.expr[":"], {
                focusable: function(el, index, selector) {
                    return $(el).is("a:not(:hidden), button:not(:hidden):not(.un), :input:not(:hidden):not([readonly]), [tabindex]:not(:hidden)");
                }
            });
            $(document).on("keypress", "input:not(:hidden),select:not(:hidden),textarea:not(:hidden)", function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    // console.log($(this).val());
                    if(($(this).val()!=null&&$(this).val()!="")||$(this).hasClass('no-required')){
                        var $canfocus = $(":focusable");
                        var index = $canfocus.index(document.activeElement) + 1;
                        if (index >= $canfocus.length) index = 0;
                        $canfocus.eq(index).focus();
                    }
                }
            });
            $(document).on("submit", "#form-pribadi", function (evt) {
                evt.preventDefault();
                let e=$(this);
                if(!e.find('#dokumen-table tbody>tr').length){
                    return  Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Dokumen Pendukung Harap di isi!!!'
                    });
                }
                if(!e.find('[data-name="list_pecah"] input').length){
                    return  Swal.fire({ icon: 'warning', html: 'Data Pecah harap di isi.'});
                }
                Swal.fire({
                    title: e.attr('data-loader'),
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            //response.msg
                            return Swal.fire({ icon: 'warning', text:response.msg??'layanan registrasi gagal.' });
                        }
                        resetForm(e);
                        if(response.curl){
                            Swal.fire({
                                title: 'Info',
                                // html:'<p>'+response.msg+', Download SK Pembetulan</p> <a href="{{ url("laporan_input/cetak_pdf?nomor_layanan=") }}'+response.curl+
                                html:response.msg,
                                showConfirmButton: false,
                            });
                            //openData('get','laporan_input/cetak_pdf',{'nomor_layanan':response.curl}, '_blank');
                        }else{
                            Swal.fire({ icon: 'success', text: response.msg});
                        }
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            });
            $(document).on("submit", "#form-badan", function (evt) {
                evt.preventDefault();
                let e=$(this);
                if(!e.find('#dokumen-table tbody>tr').length){
                    return  Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Dokumen Pendukung Harap di isi!!!'
                    });
                }
                if(!e.find('[data-name="list_pecah"] input').length){
                    return  Swal.fire({ icon: 'warning', html: 'Data Pecah harap di isi.'});
                }
                Swal.fire({
                    title: e.attr('data-loader'),
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            //response.msg
                            return Swal.fire({ icon: 'warning', text:response.msg??'layanan registrasi gagal.' });
                        }
                        resetForm(e);
                        if(response.curl){
                            Swal.fire({
                                title: 'Info',
                                // html:'<p>'+response.msg+', Download SK Pembetulan</p> <a href="{{ url("laporan_input/cetak_pdf?nomor_layanan=") }}'+response.curl+
                                html:response.msg,
                                showConfirmButton: false,
                            });
                            //openData('get','laporan_input/cetak_pdf',{'nomor_layanan':response.curl}, '_blank');
                        }else{
                            Swal.fire({ icon: 'success', text: response.msg});
                        }
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            });
        });
    </script>
@endsection