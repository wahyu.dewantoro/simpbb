@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Verifikasi Permohonan (Pembetulan) Online </h1>
            </div>
            <div class="col-sm-6">
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-body p-1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-outline card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Data Objek</h3>
                                    </div>
                                    <div class="card-body p-0">
                                        <table class="table table-sm table-borderless">
                                            <tbody>
                                                <tr style="vertical-align: top">
                                                    <td width="120px">NOP</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $objek->kd_propinsi .'.' .$objek->kd_dati2 .'.' .$objek->kd_kecamatan .'.' .$objek->kd_kelurahan .'.' .$objek->kd_blok .'-' .$objek->no_urut .'.' .$objek->kd_jns_op }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>Alamat</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }}
                                                        <br>
                                                        RW : {{ $objek->rw_op }} / RT :{{ $objek->rt_op }}
                                                        <br>
                                                        {{ $objek->nm_kelurahan }} -
                                                        {{ $objek->nm_kecamatan }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>Cabang</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $objek->kd_status_cabang == '0' ? 'Tidak' : 'Ya' }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td width="120px">Luas Bumi</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ angka($objek->total_luas_bumi) }} M<sup>2</sup>
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>Luas Bng</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ angka($objek->total_luas_bng) }} M<sup>2</sup>
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>NJOP Bumi</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        Rp. {{ angka($objek->njop_bumi) }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>NJOP Bng</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        Rp. {{ angka($objek->njop_bng) }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td width="70px">Status WP</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ statusWP($objek->kd_status_wp) }}
                                                    </td>
                                                </tr>

                                                <tr style="vertical-align: top">
                                                    <td>ZNT</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $objek->kd_znt }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>Jenis Tanah</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ jenisBumi($objek->jns_bumi) }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-outline card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Data Subjek Sebelumnya</h3>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-sm  table-borderless">
                                            <tbody>
                                                <tr style="vertical-align: top">
                                                    <td width="30%">Nama</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $subjek->nm_wp }}</td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td width="30%">NIK</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $subjek->subjek_pajak_id }}</td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>Alamat</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $subjek->jalan_wp }} {{ $subjek->blok_kav_no_wp }}<br>
                                                        RW : {{ $subjek->rw_wp }} RT : {{ $subjek->rt_wp }}<br>
                                                        {{ $subjek->kelurahan_wp }} <br>
                                                        {{ $subjek->kota_wp }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td width="30%">Kode Pos</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $subjek->kd_pos_wp }}</td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>No Telp</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ $subjek->telp_wp }}
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>NPWP</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $subjek->npwp }}</td>
                                                </tr>
                                                <tr style="vertical-align: top">
                                                    <td>Pekerjaan</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ jenisPekerjaan($subjek->status_pekerjaan_wp) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-outline card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Data Pembetulan Subjek Pajak</h3>
                                    </div>
                                    <form action="{{ url('layanan/pembetulan-online',$permohonan->id) }}" method="post" id='myform' enctype="multipart/form-data">
                                        <div class="card-body">
                                            @csrf
                                            @method('patch')
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="subjek_pajak_id" class="col-form-label col-md-4  ">NIK</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="subjek_pajak_id" id="subjek_pajak_id" class="form-control form-control-sm" value="{{ $permohonan->nik_wp }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="nm_wp" class="col-form-label col-md-4  ">Nama</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="nm_wp" id="nm_wp" class="form-control form-control-sm" value="{{ $permohonan->nama_wp }}">
                                                        </div>
                                                    </div>

                                                    @php
                                                    $vos=$permohonan->alamat_wp;

                                                    if ($vos != '') {
                                                    $vos=str_replace('|','-',$vos);
                                                    $eos = explode('-', $vos);
                                                    $aos = isset($eos[0]) ? trim($eos[0]) : '';
                                                    $aobs = isset($eos[1]) ? trim($eos[1]) : '';
                                                    } else {
                                                    $aos="";
                                                    $aobs="";
                                                    }

                                                    @endphp

                                                    <div class="form-group row">
                                                        <label for="jalan_wp" class="col-form-label col-md-4  ">Jalan</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="jalan_wp" id="jalan_wp" class="form-control form-control-sm" value="{{ $aos }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="blok_kav_no_wp" class="col-form-label col-md-4  ">Blok/
                                                            Kav/
                                                            No</label>
                                                        <div class="col-md-4">
                                                            <input type="text" name="blok_kav_no_wp" id="blok_kav_no_wp" class="form-control form-control-sm" value="{{ $aobs }}">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="rw_wp" class="col-form-label col-md-8 ">RW</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="rw_wp" id="rw_wp" class="form-control form-control-sm" value="{{ $permohonan->rw_wp ?? '' }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="rt_wp" class="col-form-label col-md-4 ">RT</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="rt_wp" id="rt_wp" class="form-control form-control-sm" value="{{ $permohonan->rt_wp ?? '' }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="kelurahan_wp" class="col-form-label col-md-4 ">Kelurahan</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="kelurahan_wp" id="kelurahan_wp" class="form-control form-control-sm" value="{{ $permohonan->kelurahan_wp ?? '' }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="kecamatan_wp" class="col-form-label col-md-4 ">Kecamatan</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="kecamatan_wp" id="kecamatan_wp" class="form-control form-control-sm" value="{{ $permohonan->kecamatan_wp ?? '' }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="kota_wp" class="col-form-label col-md-4 ">Dati
                                                            II</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="kota_wp" id="kota_wp" class="form-control form-control-sm" value="{{ $permohonan->dati2_wp ?? '' }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="propinsi_wp" class="col-form-label col-md-4 ">Propinsi</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="propinsi_wp" id="propinsi_wp" class="form-control form-control-sm" value="{{ $permohonan->propinsi_wp ?? '' }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="kd_pos_wp" class="col-form-label col-md-6 ">Kode
                                                            POS</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="kd_pos_wp" id="kd_pos_wp" class="form-control form-control-sm" value="{{ $permohonan->kode_pos_wp ?? '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label for="status_pekerjaan_wp" class="col-form-label col-md-4 ">Pekerjaan</label>
                                                        <div class="col-md-8">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <input type="text" name="status_pekerjaan_wp" id="status_pekerjaan_wp" class="form-control form-control-sm">
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <span class="form-control form-control-sm" readonly id="status_pekerjaan_wp_keterangan"></span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="kd_status_wp" class="col-form-label col-md-4  ">Status</label>
                                                        <div class="col-md-8 ">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    @php
                                                                    $sw=$permohonan->status_subjek_pajak??'4';
                                                                    @endphp
                                                                    <input type="text" name="kd_status_wp" id="kd_status_wp" class="form-control form-control-sm" value="{{ $sw }}">
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <span class="form-control form-control-sm" readonly id="kd_status_wp_keterangan"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="npwp" class="col-form-label col-md-4 ">NPWP</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="npwp" id="npwp" class="form-control form-control-sm" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="telp_wp" class="col-form-label col-md-4 ">Telepon</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="telp_wp" id="telp_wp" class="form-control form-control-sm" value="{{ $permohonan->telp_wp ?? '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <hr>
                                                    <p><strong>Dokumen Pendukung</strong></p>
                                                    <ol>
                                                        @foreach ($lampiran as $dok)
                                                        <li><a target="_blank" href="{{ url('preview-dok') }}/{{ $dok->disk}}/{{ acak($dok->filename) }}">{{ $dok->nama_dokumen }}</a></li>
                                                        @endforeach
                                                    </ol>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="float-right">
                                                <button id="btn-tolak" type="button" data-href="{{ url('layanan/pembetulan-online/'.$permohonan->nomor_layanan.'/reject') }}" class="btn btn-sm btn-danger"><i class="far fa-window-close"></i> Tolak</button>
                                                <button class="btn btn-sm btn-success"><i class="far fa-save"></i> Proses</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{ asset('lte') }}/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {

        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }


        var base_url = "{{ url('/') }}";

        $('input').each(function() {
            $(this).val($(this).val().toUpperCase())
        })

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }



        JenisBumi(arrayJnsBumi)
        statusWepe(arrayStatusWp)
        // function statusWepe(array)

        function JenisBumi(arrayJnsBumi) {
            idxpkj = $("#jns_bumi").val();
            if (typeof arrayJnsBumi[idxpkj] === "undefined") {
                hasil = "";
                $("#jns_bumi").val("");
            } else {
                hasil = arrayJnsBumi[idxpkj];
            }
            $("#jns_bumi_keterangan").html(hasil);
        }

        function statusWepe(arrayStatusWp) {
            idxpkj = $("#kd_status_wp").val();
            if (typeof arrayStatusWp[idxpkj] === "undefined") {
                hasil = "";
                $('#kd_status_wp').val("");
            } else {
                hasil = arrayStatusWp[idxpkj];
            }

            $("#kd_status_wp_keterangan").html(hasil);
        }



        $("#status_pekerjaan_wp").on("keyup", function() {
            idxpkj = $("#status_pekerjaan_wp").val();
            if (typeof arrayPekerjaan[idxpkj] === "undefined") {
                hasil = "";
                $(this).val("");
            } else {
                hasil = arrayPekerjaan[idxpkj];
            }
            $("#status_pekerjaan_wp_keterangan").html(hasil);
        });
        $("#status_pekerjaan_wp").val('5').trigger('keyup')

        $("#kd_status_wp").on("keyup", function() {
            statusWepe(arrayStatusWp)
        });

        $.validator.addMethod(
            "angkaRegex"
            , function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
            }
            , "Harus di isi dengan angka."
        );

        jQuery.validator.addMethod("exactlength", function(value, element, param) {
            return this.optional(element) || value.length == param;
        }, $.validator.format("Please enter exactly {0} characters."));



        // $("#submit").click(function() {
        $("#myform").submit(function() {
            var form = $("#myform");
            form.validate({
                errorElement: "span"
                , errorPlacement: function(error, element) {
                    error.addClass("invalid-feedback");
                    element.closest(".form-group").append(error);
                }
                , highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid");
                }
                , unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                }
                , rules: {
                    subjek_pajak_id: {
                        required: true
                        , digits: true
                        , angkaRegex: true
                        , minlength: 16
                        , maxlength: 16
                    }
                    , nm_wp: {
                        required: true
                        , maxlength: 30
                    }
                    , jalan_wp: {
                        required: true
                        , maxlength: 30
                    }
                    , blok_kav_no_wp: {
                        required: false
                    }
                    , rt_wp: {
                        digits: true
                        , angkaRegex: false
                        , required: true
                        , maxlength: 3
                    }
                    , rw_wp: {
                        angkaRegex: true
                        , required: true
                        , maxlength: 2
                        , digits: true
                    }
                    , kd_pos_wp: {
                        required: false
                        , digits: true
                        , angkaRegex: true
                        , maxlength: 5
                    }
                    , kelurahan_wp: {
                        required: true
                    }
                    , kecamatan_wp: {
                        required: true
                    }
                    , kota_wp: {
                        required: true
                    }
                    , propinsi_wp: {
                        required: true
                    }
                    , telp_wp: {
                        digits: true
                        , angkaRegex: true
                        , required: false
                        , minlength: 10
                    }
                    , npwp: {
                        digits: true
                        , angkaRegex: true
                        , required: false
                    }
                    , status_pekerjaan_wp: {
                        required: true
                        , digits: true
                        , maxlength: 1
                    }
                }
                , messages: {
                    subjek_pajak_id: {
                        required: "Harus di isi"
                        , digits: "Di isi dengan angka"
                        , minlength: "minimal {0} karakter"
                        , maxlength: "Tidak boleh lebih dari {0} karakter"
                    }
                    , nm_wp: {
                        required: 'Harus di isi'
                    }
                    , jalan_wp: {
                        required: 'Harus di isi'
                    },

                    rt_wp: {
                        required: 'RT harus isi'
                        , digits: 'Di isi angka'
                        , minlength: "minimal {0} karakter"
                        , maxlength: "Tidak boleh lebih dari {0} karakter"
                    }
                    , rw_wp: {
                        required: "RW harus isi"
                        , digits: 'Di isi angka'
                        , minlength: "minimal {0} karakter"
                        , maxlength: "Tidak boleh lebih dari {0} karakter"
                    }
                    , kd_pos_wp: {
                        digits: 'Di isi dengan angka'
                        , maxlength: "Tidak boleh lebih dari {0} karakter"
                    }
                    , kelurahan_wp: {
                        required: "Harus di isi"
                    }
                    , kecamatan_wp: {
                        required: "Harus di isi"
                    }
                    , kota_wp: {
                        required: "Harus di isi"
                    }
                    , propinsi_wp: {
                        required: "Harus di isi"
                    }
                    , telp_wp: {
                        digits: "Di isi dengana angka"
                        , minlength: "Minimal {0} karakter"
                    }
                    , npwp: {
                        digits: "Di isi dengan angka"
                        , required: false
                    },

                },

            });


            // return false;
            if (form.valid() === false) {
                return false;
            }
        });





        // register jQuery extension
        jQuery.extend(jQuery.expr[":"], {
            focusable: function(el, index, selector) {
                return $(el).is("a, button, :input, [tabindex]");
            }
        });

        $(".form-control").keyup(function() {
            if (this.value.length == this.maxLength) {
                $(this).next('.form-control').focus();
            }
        });

        $(document).on("keypress", "input,select", function(e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(":focusable");
                var index = $canfocus.index(document.activeElement) + 1;
                if (index >= $canfocus.length) index = 0;
                $canfocus.eq(index).focus();
            }
        });
        $("input[type=text]").keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });

        // tolak
        // btn-tolak
        $('#btn-tolak').on('click', function(e) {
            // id = $(this).data('id')
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin menolak permohonan ?'
                    // , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    // document.getElementById('delete-form-' + id).submit()
                    document.location.href = $(this).data('href');
                }
            })
        });
    });

</script>
@endsection
