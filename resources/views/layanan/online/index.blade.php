@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Daftar Permohonan (Pembetulan) Online </h1>
            </div>
            <div class="col-sm-6">


            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-outline card-info">
                    <div class="card-body p-1">
                        <table class="table table-sm table-bordered table-hover table-checkable" id="table-objek" style="width:100%">
                            <thead class="bg-success">
                                <tr>
                                    <th style="width:1px !important;">No</th>
                                    <th class="text-center">Nopel</th>
                                    <th class="text-center">Ajuan</th>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Wajib Pajak</th>
                                    <th class="text-center">Keterangan</th>
                                    <th class="text-center">Aksi</th>

                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-penolakan">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Penolakan Permohonan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <input type="hidden" name="id" id="id_tolak">
                    <textarea required name="keterangan_tolak" id="keterangan_tolak" class="form-control" placeholder="Tulis keteranan alasan penolakan penelitian" cols="30" rows="5"></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-sm btn-flat btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn_tolak" class="btn btn-sm btn-flat btn-success">Submit</button>
            </div>

        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableObjek


        tableObjek = $('#table-objek').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('layanan/pembetulan-online') }}"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }

                , {
                    data: 'nomor_layanan'
                    , orderable: false
                    , name: 'nomor_layanan'
                },

                {
                    data: 'jenis_layanan_nama'
                    , orderable: false
                    , name: 'nomor_layanan'
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , name: 'nama_nop'
                }
                , {
                    data: 'nama_wp'
                    , orderable: false
                    , name: 'nama_nop'

                }
                , {
                    data: 'keterangan'
                    , orderable: false
                    , name: 'alamat_op'
                    , searchable: false
                }
                , {
                    data: 'pilih'
                    , orderable: false
                    , name: 'pilih'
                    , searchable: false
                    , class: "text-center"
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , iDisplayLength: 10
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , rowCallback: function(row, data, index) {}
        });

        // $('.tolak').on('click', function(e) {
        $(document).on("click",'.tolak',function (e) {
            e.preventDefault()
            var id = $(this).data('id')
            $('#id_tolak').val(id)
            $('#modal-penolakan').modal('toggle');
        })


        $('#btn_tolak').on('click', function(e) {
            $('#modal-penolakan').modal('toggle');
            e.preventDefault()
            var id_tolak = $('#id_tolak').val()
            var keterangan_tolak = $('#keterangan_tolak').val()
            var url_ = "{{ url('penelitian/kantor-tolak')}}/" + id_tolak
            openloading()
            $.ajax({
                url: url_
                , type: 'POST'
                , data: {
                    "_token": "{{ csrf_token() }}"
                    , keterangan_tolak: keterangan_tolak
                }
                , success: function(res) {
                    tableObjek.draw();
                    Swal.fire({
                        icon: "success"
                        , title: "Berhasil"
                        , text: "Data telah di tolak"
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    });
                }
                , error: function(er) {
                    // console.log(er)

                    Swal.fire({
                        icon: "error"
                        , title: "Peringatan"
                        , text: "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem."
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    });
                }
            })
        })

    });

</script>
@endsection
