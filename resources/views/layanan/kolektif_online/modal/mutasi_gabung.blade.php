<form class="form-kolektif-temp" role="form" action="{{ $url }}" >
@csrf   
<input type="hidden" name='jenis_layanan_id' value="7">
<input type="hidden" name='id' value="">
<input type="hidden" name='kd_kelurahan' value="">
<input type="hidden" name='kd_kecamatan' value="">
<div class="modal fade" id="{{$type}}-mutasi-gabung" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header p-1">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-swatchbook"></i> {{$titlePage}} Mutasi Gabung</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-1">
          <div class="row">
          <div class="col-sm-12">
                <div class="card-split-row mb-1 m-0"></div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NOP Gabung</label>
                        <div class="col-md-8 p-0 input-group"> 
                            <input type="text" name="nop_gabung_label" class="form-control-sm form-control numeric unform nop_full" placeholder="NOP">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info btn-sm" data-nop-add='nop_gabung_label' aria-expanded="false">
                                <i class='fas fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="card mb-1">
                    <div class="card-body body-scroll">
                        <table class="table table-bordered table-sm table-striped table-counter">
                            <thead class='text-center'>
                                <tr>
                                    <th rowspan='2'>No</th>
                                    <th rowspan='2'>NIK</th>
                                    <th rowspan='2'>NAMA</th>
                                    <th rowspan='2'>NOP</th>
                                    <th colspan='4'>Lokasi Objek</th>
                                    <th colspan='2'>Luas</th>
                                    <th rowspan='2'>-</th>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <th>RT/RW</th>
                                    <th>Kelurahan</th>
                                    <th>Kecamatan</th>
                                    <th>Bumi</th>
                                    <th>Bangunan</th>
                                </tr>
                            </thead>
                            <tbody data-name="list_nop" class="tag-html">
                                <tr class="null">
                                    <td colspan="11" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NOP Hasil Gabung</label>
                        <!-- <div class="col-md-8 p-0 input-group"> 
                            <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info btn-sm" data-nop='nop' aria-expanded="false">
                                <i class='fas fa-search'></i>
                                </button>
                            </div>
                        </div> -->
                    </div>
                </div>
              </div>
              <div class="col-sm-6 pr-1">
                <p class="bg-warning color-palette p-1 mb-1"> <b>Objek Pajak</b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat Objek</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_op" class="form-control-sm form-control  no-min" placeholder="Alamat Objek" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_op" class="form-control-sm form-control rt_rw " placeholder="RT/RW" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bumi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bumi" class="form-control-sm form-control numeric " placeholder="Luas Bumi" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bangunan</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="luas_bng" class="form-control-sm form-control numeric " placeholder="Luas Bangunan" value="0" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">JPB</label>
                        <div class="col-md-8 p-0"> 
                            <select name="kelompok_objek_id"   class="form-control select unform"  data-placeholder="Jenis Penggunaan Bangunan"  >
                                <option value="">-- Jenis --</option>
                                @foreach ($KelompokObjek as $kelompok)
                                    <option @if (request()->get('kelompok') == $kelompok->id)  selected @endif value="{{ $kelompok->id }}">
                                        {{ $kelompok->nama_kelompok }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Lokasi Objek</label>
                        <div class="col-md-8 p-0"> 
                            <select name="lokasi_objek_id"   class="form-control select unform"  data-placeholder="Lokasi Objek.">
                                <option value="">-- Lokasi --</option>
                                @foreach ($LokasiObjek as $lokasi)
                                    <option @if (request()->get('lokasi') == $lokasi->id)  selected @endif value="{{ $lokasi->id }}">
                                        {{ $lokasi->nama_lokasi }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

              </div>
              <div class="col-sm-6 pl-1">
              <p class="bg-warning color-palette p-1 mb-1"> <b>Subjek Pajak</b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NIK</label>
                        <div class="col-md-8 p-0 input-group"> 
                            <input type="text" name="nik_wp" class="form-control-sm form-control numeric nik nik_lookup" maxlength="16" placeholder="NIK">
                            <div class="input-group-append ">
                                <button type="button" class="btn btn-info btn-sm keypress" data-nik="layanan">
                                    <i class='fas fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Nama</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nama_wp" class="form-control-sm form-control " placeholder="NAMA" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">No Telp</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="telp_wp" class="form-control-sm form-control " placeholder="No Telp"  maxlength="16">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_wp" class="form-control-sm form-control  no-min" placeholder="Alamat" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_wp" class="form-control-sm form-control rt_rw " placeholder="RT/RW" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kelurahan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kelurahan_wp" class="form-control-sm form-control " placeholder="Kelurahan" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kecamatan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kecamatan_wp" class="form-control-sm form-control " placeholder="Kecamatan" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kota/Kabupaten</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="dati2_wp" class="form-control-sm form-control " placeholder="Kota/Kabupaten" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Propinsi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="propinsi_wp" class="form-control-sm form-control " placeholder="Propinsi" >
                        </div>
                    </div>
                </div>
            
              </div>
              
          </div>
      </div>
      <div class="modal-footer p-1">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-sm">{{$button}} ke daftar Kolektif</button>
      </div>
    </div>
  </div>
</div>
</form>