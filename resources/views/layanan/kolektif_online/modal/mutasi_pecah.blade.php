<form class="form-kolektif-temp" role="form" action="{{ $url }}" >
@csrf   
<input type="hidden" name='jenis_layanan_id' value="6">
<input type="hidden" name='id' value="">
<input type="hidden" name='kd_kelurahan' value="">
<input type="hidden" name='kd_kecamatan' value="">
<div class="modal fade" id="{{$type}}-mutasi-pecah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header p-1">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-swatchbook"></i> {{$titlePage}} Mutasi Pecah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-1">
          <div class="row">
              <div class="col-sm-12">
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NOP Induk</label>
                        <div class="col-md-8 p-0 input-group"> 
                            <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info btn-sm" data-nop='nop' aria-expanded="false">
                                <i class='fas fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-6 pr-1">
                <p class="bg-warning color-palette p-1 mb-1"> <b>Objek Pajak</b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat Objek</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_op" class="form-control-sm form-control no-min" placeholder="Alamat Objek">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_op" class="form-control-sm form-control rt_rw " placeholder="RT/RW" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bumi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bumi" class="form-control-sm form-control numeric " placeholder="Luas Bumi">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bangunan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bng" class="form-control-sm form-control numeric " placeholder="Luas Bangunan" value="0">
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-6 pl-1">
              <p class="bg-warning color-palette p-1 mb-1"> <b>Subjek Pajak</b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NIK</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nik_wp" class="form-control-sm form-control numeric nik " maxlength="16" placeholder="NIK" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Nama</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nama_wp" class="form-control-sm form-control " placeholder="NAMA" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">No Telp</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="telp_wp" class="form-control-sm form-control " placeholder="No Telp"  maxlength="16">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_wp" class="form-control-sm form-control no-min" placeholder="Alamat">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_wp" class="form-control-sm form-control rt_rw " placeholder="RT/RW">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1 ">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kelurahan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kelurahan_wp" class="form-control-sm form-control " placeholder="Kelurahan">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1 ">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kecamatan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kecamatan_wp" class="form-control-sm form-control " placeholder="Kecamatan">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1 ">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kota/Kabupaten</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="dati2_wp" class="form-control-sm form-control " placeholder="Kota/Kabupaten">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1 ">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Propinsi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="propinsi_wp" class="form-control-sm form-control " placeholder="Propinsi">
                        </div>
                    </div>
                </div>
            
              </div>
              <div class="col-sm-12">
                <div class="card-split-row mb-1 m-0"></div>
                    <div class="card mb-1">
                        <div class="card-header p-1">
                            <div class="card-title">
                            <b>Data Pecah</b>
                            </div>
                        </div>
                        <div class="card-body p-0 mb-1">
                        <div class='row' data-table-add="noppecah">
                            <div class="col-md-6 pr-1">
                                <p class="bg-warning color-palette p-1 mb-1 pl-2 "> <b> Subjek</b> </p>
                                <div class="row">
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">NIK</label>
                                            <input type="text" name="nikadd" class="form-control form-control-sm nik unform thirdFocus rePecah" data-input="nik_wp_pecah" data-label="NIK">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-info btn-sm" data-nikadd="layanan" aria-expanded="false">
                                                    <i class='fas fa-search'></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">Nama </label>
                                            <input type="text" name="nama_wpadd" class="form-control form-control-sm secondFocusAdd unform" data-input="nama_wp_pecah" data-label="Nama">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">No Telp</label>
                                            <input type="text" name="notelp_wpadd"  class="form-control form-control-sm numeric unform" data-input="notelp_wp_pecah" maxlength="15" data-label="No. Telp">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">Alamat </label>
                                            <input type="text" name="alamat_wpadd" class="form-control form-control-sm no-min unform" data-input="alamat_wp_pecah" data-label="Alamat">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">RT/RW </label>
                                            <input type="text" name="rt_rw_wpadd" class="form-control-sm form-control rt_rw unform" placeholder="RT/RW"  data-label="RT/RW" data-input="rt_rw_wp_pecah">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kelurahan</label>
                                            <input type="text" name="kelurahan_wpadd" class="form-control form-control-sm unform" data-input="kelurahan_wp_pecah" data-label="Kelurahan">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kecamatan</label>
                                            <input type="text" name="kecamatan_wpadd" class="form-control form-control-sm unform" data-input="kecamatan_wp_pecah" data-label="Kecamatan">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">Kota/Kabupaten</label>
                                            <input type="text" name="dati2_wpadd" class="form-control form-control-sm unform" data-input="dati2_wp_pecah" data-label="Kota / Kabupaten">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-3 col-form-label col-form-label-sm mb-0">Propinsi</label>
                                            <input type="text" name="propinsi_wpadd" class="form-control form-control-sm unform" data-input="propinsi_wp_pecah" data-label="Propinsi">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pl-1">
                                <p class="bg-warning color-palette p-1 mb-1 pl-2 "> <b> Objek</b> </p>
                                <div class="row">
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-4 col-form-label col-form-label-sm mb-0">Alamat</label>
                                            <input type="text" name="nop_alamatadd" class="form-control form-control-sm change unform no-min" data-input="alamat_pecah" data-label="Alamat">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-4 col-form-label col-form-label-sm mb-0">RT/RW </label>
                                            <input type="text" name="nop_rt_rw_add" class="form-control-sm form-control rt_rw unform" placeholder="RT/RW" data-label="RT/RW" data-input="nop_rt_rw_pecah">
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-4 col-form-label col-form-label-sm mb-0">Luas Bumi</label>
                                            <input type="text" name="luas_bumiadd" class="form-control form-control-sm numeric unform" data-input="luas_bumi_pecah" data-label="Luas Bumi">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-4 col-form-label col-form-label-sm mb-0">Luas Bangunan</label>
                                            <input type="text" name="luas_bngadd" class="form-control form-control-sm  numeric unform" data-input="luas_bng_pecah" value="0" data-label="Luas Bangungan">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-4 col-form-label col-form-label-sm mb-0">JPB</label>
                                            <select name="kelompok_objek_idadd"   class="form-control-sm form-control no-required option unform" data-input="kelompok_objek_id_pecah" data-placeholder="Jenis Penggunaan Bangunan" data-label="Jenis Penggunaan Bangunan">
                                                <option value="">-- Jenis --</option>
                                                @foreach ($KelompokObjek as $kelompok)
                                                    <option @if (request()->get('kelompok') == $kelompok->id)  selected @endif value="{{ $kelompok->id }}">
                                                        {{ $kelompok->nama_kelompok }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 mb-1">
                                        <div class='input-group input-group-sm p-0'>
                                            <label class="col-md-4 col-form-label col-form-label-sm mb-0">Lokasi Objek</label>
                                            <select name="lokasi_objek_idadd"   class="form-control-sm form-control no-required option unform" data-input="lokasi_objek_idpecah" data-placeholder="Pilih Lokasi Objek." data-label="Lokasi Objek">
                                                <option value="">-- Lokasi --</option>
                                                @foreach ($LokasiObjek as $lokasi)
                                                    <option @if (request()->get('lokasi') == $lokasi->id)  selected @endif value="{{ $lokasi->id }}">
                                                        {{ $lokasi->nama_lokasi }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group col-md-4 pull-right mb-1 ml-auto">
                                <div class='input-group input-group-sm'>
                                    <button type="button" class="btn btn-info btn-block btn-sm" data-objek-add='noppecah' aria-expanded="false">
                                        <i class='fas fa-plus-square'></i> Tambah
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-split-row mb-1 m-0"></div>
                            <div class='row body-scroll m-0'>
                                <table class="table table-bordered table-striped table-counter mb-0 table-sm">
                                    <thead class='text-center'>
                                        <tr>
                                            <th class="number">No</th>
                                            <th>Objek Pajak</th>
                                            <th>Wajib Pajak</th>
                                            <th>Domisili WP</th>
                                            <th class="option">-</th>
                                        </tr>
                                    </thead>
                                    <tbody data-name="list_pecah" class="tag-html">
                                        <tr class="null">
                                            <td colspan="5" class="dataTables_empty text-center"> Data Masih Kosong</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
              <div class="col-sm-12">
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form- p-1 mb-0">Luas Hasil Pecah*</label>
                        <div class="col-md-8 p-0">
                            <input type="text" name="hasil_pecah_hasil_gabung" data-calc="jenis_layanan_id" class="form-control form-control-sm numeric" placeholder="0" readonly>
                        </div>  
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1 mb-0">Sisa Hasil Pecah*</label>
                        <div class="col-md-8 p-0">
                            <input type="text" name="sisa_pecah_total_gabung" class="form-control form-control-sm numeric" data-calc-result="jenis_layanan_id" placeholder="0"  readonly>
                        </div>  
                    </div>
                    </div>
              </div>
          </div>
      </div>
      <div class="modal-footer p-1">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-sm">{{$button}} ke daftar Kolektif</button>
      </div>
    </div>
  </div>
</div>
</form>