<form class="form-kolektif-temp" role="form" action="{{ $url }}" >
@csrf   
<input type="hidden" name='jenis_layanan_id' value="3">
<input type="hidden" name='id' value="">
<input type="hidden" name='kd_kelurahan' value="">
<input type="hidden" name='kd_kecamatan' value="">
<div class="modal fade" id="{{$type}}-pembetulan-luas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header p-1">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-swatchbook"></i> {{$titlePage}} Pembetulan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-1">
          <div class="row">
              <div class="col-sm-12">
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NOP</label>
                        <div class="col-md-8 p-0 input-group"> 
                            <input type="text" name="nop" class="form-control-sm form-control numeric nop_full" placeholder="NOP">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-info btn-sm" data-nop='nop' aria-expanded="false">
                                    <i class='fas fa-search'></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-6 pr-1">
                <p class="bg-warning color-palette p-1 mb-1"> <b>Objek Pajak</b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat Objek</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_op_label" class="form-control-sm form-control unform no-min" placeholder="Alamat Objek" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_op_label" class="form-control-sm form-control rt_rw unform" placeholder="RT/RW" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bumi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bumi_label" class="form-control-sm form-control numeric unform" placeholder="Luas Bumi" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bangunan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bng_label" class="form-control-sm form-control numeric unform" placeholder="Luas Bangunan" value="0" readonly>
                        </div>
                    </div>
                </div>
                <p class="bg-warning color-palette p-1 mb-1"> <b>Subjek Pajak</b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NIK</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nik_wp_label" class="form-control-sm form-control numeric nik unform" maxlength="16" placeholder="NIK" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Nama</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nama_wp_label" class="form-control-sm form-control unform" placeholder="NAMA" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_wp_label" class="form-control-sm form-control unform no-min" placeholder="Alamat" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_wp_label" class="form-control-sm form-control rt_rw unform" placeholder="RT/RW" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kelurahan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kelurahan_wp_label" class="form-control-sm form-control unform" placeholder="Kelurahan" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kecamatan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kecamatan_wp_label" class="form-control-sm form-control unform" placeholder="Kecamatan" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kota/Kabupaten</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="dati2_wp_label" class="form-control-sm form-control unform" placeholder="Kota/Kabupaten" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Propinsi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="propinsi_wp_label" class="form-control-sm form-control unform" placeholder="Propinsi" readonly>
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-6 pl-1">
              <p class="bg-warning color-palette p-1 mb-1"> <b>Objek Pajak (Pembetulan) </b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat Objek</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_op" class="form-control-sm form-control no-min unform" placeholder="Alamat Objek" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_op" class="form-control-sm form-control rt_rw unform" placeholder="RT/RW" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bumi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bumi" class="form-control-sm form-control numeric unform" placeholder="Luas Bumi">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Luas Bangunan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="luas_bng" class="form-control-sm form-control numeric unform" placeholder="Luas Bangunan" value="0" >
                        </div>
                    </div>
                </div>
                <p class="bg-warning color-palette p-1 mb-1"> <b>Subjek Pajak (Pembetulan) </b> </p>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">NIK</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nik_wp" class="form-control-sm form-control numeric nik unform" maxlength="16" placeholder="NIK" >
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Nama</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="nama_wp" class="form-control-sm form-control unform" placeholder="NAMA">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">No Telp</label>
                        <div class="col-md-8 p-0"> 
                            <input type="text" name="telp_wp" class="form-control-sm form-control unform" placeholder="No Telp" maxlength="16">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Alamat</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="alamat_wp" class="form-control-sm form-control unform no-min" placeholder="Alamat">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">RT/RW</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="rt_rw_wp" class="form-control-sm form-control rt_rw unform" placeholder="RT/RW">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kelurahan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kelurahan_wp" class="form-control-sm form-control unform" placeholder="Kelurahan">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kecamatan</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="kecamatan_wp" class="form-control-sm form-control unform" placeholder="Kecamatan">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Kota/Kabupaten</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="dati2_wp" class="form-control-sm form-control unform" placeholder="Kota/Kabupaten">
                        </div>
                    </div>
                </div>
                <div class="form-group mb-1">
                    <div class='input-group input-group-sm'>
                        <label class="col-md-4 col-form-label p-1">Propinsi</label>
                        <div class="col-md-8 p-0"> 
                        <input type="text" name="propinsi_wp" class="form-control-sm form-control unform" placeholder="Propinsi">
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
      <div class="modal-footer p-1">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary btn-sm">{{$button}} ke daftar Kolektif</button>
      </div>
    </div>
  </div>
</div>
</form>