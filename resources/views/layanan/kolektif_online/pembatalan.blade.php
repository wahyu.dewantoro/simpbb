
<table id="table-kolektif-{{ $layanan->id }}" class="table table-bordered table-striped table-sm">
    <thead class="text-center">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">NOP</th>
            <th colspan="2">Objek Pajak</th>
            <th colspan="3">Subjek Pajak</th>
            <th rowspan="2">Alasan</th>
            <th rowspan="2">-</th>
        </tr>
        <tr>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
            <th>NIK</th>
            <th>NAMA</th>
            <th>No Telp</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="9" class="dataTables_empty text-center"> Data {{ $layanan->nama_layanan }} Masih Kosong</td>
        </tr>
    </tbody>
</table>
