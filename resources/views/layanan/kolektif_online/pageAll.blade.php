<form id="form-kolektif">
    @csrf   
    <div class="row p-1">
        <div class="form-group col-md-4 mb-0">
            <div class="input-group">
                <select name="kecamatan"  class="form-control select"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                    @if($kecamatan->count()>1) <option value="">-- Kecamatan --</option> @endif
                    @foreach ($kecamatan as $rowkec)
                        <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                            {{ $rowkec->nm_kecamatan }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-4 mb-0">
            <div class="input-group">
                <select name="kelurahan" class="form-control select"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                    @if(count($kelurahan)>1) <option value="">-- Kelurahan --</option> @endif
                    @foreach ($kelurahan as $rowkel)
                        <option @if (request()->get('kd_kelurahan') == $rowkel->kd_kelurahan)  selected @endif value="{{ $rowkel->kd_kelurahan }}">
                            {{ $rowkel->nm_kelurahan }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</form>
<div class="col-sm-12"> <div class="card-split-row mb-1"></div> </div>
<form action="{{url('kolektif_online')}}" class="form-kolektif-save" method="post">
@csrf   
<div class="row">
    <div class="col-sm-12 br-1 pr-1">
        <ul class="nav nav-pills p-0 tabs-h border-primary">    
        @php $active='active'; @endphp
        @foreach ($layanan_all as $layanan)
            <li class="nav-item">
                <a class="nav-link {{ $active }} no-radius" 
                    id="{{ $layanan->id }}-tab" 
                    data-toggle="tab" 
                    href="#content-{{ $layanan->id }}" >
                    <span>{{ $layanan->nama_layanan }} </span> 
                    <!-- <b class="pr">[ <span id="tab-count-{ { $layanan->id }}">0</span> ]</b> -->
                </a>
            </li>
            @php $active=''; @endphp
        @endforeach
        </ul>
    </div>
    <div class="col-sm-12 pl-0">
        <div class="tab-content" id="vert-tabs-right-tabContent">
            @php $active='active'; @endphp
            @foreach ($layanan_all as $layanan)
                <div class="tab-pane fade show {{ $active }}" id="content-{{ $layanan->id }}" role="tabpanel" aria-labelledby="{{ $layanan->id }}-tab">
                    <div class="card mb-1">
                        <div class="card-header p-1 d-flex">
                            <ul class="nav nav-pills ml-auto" >
                                <li class="nav-item">
                                    <a href="javascript:;" class="btn btn-block btn-primary btn-sm" data-modal-target="#modal-{{ $layanan->page }}"> <i class="fas fa-plus-square"></i> Ajuan Kolektif </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body body-scroll mmh-5  pl-2 pr-1 pt-1">
                            @include('layanan/kolektif_online/'.$layanan->page)
                        </div>
                    </div>
                </div>
                @php $active=''; @endphp
            @endforeach
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-md-6"><a href="javascript:;" data-href="{{url('kolektif_online/refresh')}}" class="btn btn-block btn-default btn-reload">Batal</a></div>
        <div class="col-md-6"> <button type="submit" class="btn btn-block btn-primary">Simpan & Buat Permohonan Kolektif</button> </div>
    </div>
</div>
</form>
<!-- Form -->
@include('layanan/kolektif_online/modal/pendaftaran_baru',['url'=>url('kolektif_online/store'),'type'=>'modal','titlePage'=>'Form','button'=>'Simpan'])
@include('layanan/kolektif_online/modal/pembatalan',['url'=>url('kolektif_online/store'),'type'=>'modal','titlePage'=>'Form','button'=>'Simpan'])
@include('layanan/kolektif_online/modal/pembetulan_luas',['url'=>url('kolektif_online/store'),'type'=>'modal','titlePage'=>'Form','button'=>'Simpan'])
@include('layanan/kolektif_online/modal/mutasi_penuh',['url'=>url('kolektif_online/store'),'type'=>'modal','titlePage'=>'Form','button'=>'Simpan'])
@include('layanan/kolektif_online/modal/mutasi_pecah',['url'=>url('kolektif_online/store'),'type'=>'modal','titlePage'=>'Form','button'=>'Simpan'])
@include('layanan/kolektif_online/modal/mutasi_gabung',['url'=>url('kolektif_online/store'),'type'=>'modal','titlePage'=>'Form','button'=>'Simpan'])
<!-- Form Update -->
@include('layanan/kolektif_online/modal/pendaftaran_baru',['url'=>url('kolektif_online/update'),'type'=>'edit','titlePage'=>'Edit','button'=>'Update'])
@include('layanan/kolektif_online/modal/pembatalan',['url'=>url('kolektif_online/update'),'type'=>'edit','titlePage'=>'Edit','button'=>'Update'])
@include('layanan/kolektif_online/modal/pembetulan_luas',['url'=>url('kolektif_online/update'),'type'=>'edit','titlePage'=>'Edit','button'=>'Update'])
@include('layanan/kolektif_online/modal/mutasi_penuh',['url'=>url('kolektif_online/update'),'type'=>'edit','titlePage'=>'Edit','button'=>'Update'])
@include('layanan/kolektif_online/modal/mutasi_pecah',['url'=>url('kolektif_online/update'),'type'=>'edit','titlePage'=>'Edit','button'=>'Update'])
@include('layanan/kolektif_online/modal/mutasi_gabung',['url'=>url('kolektif_online/update'),'type'=>'edit','titlePage'=>'Edit','button'=>'Update'])