
<table id="table-kolektif-{{ $layanan->id }}" class="table table-bordered table-striped table-sm">
    <thead class="text-center">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">NOP</th>
            <th colspan="4">Objek Pajak</th>
            <th colspan="9">Subjek Pajak</th>
            <th rowspan="2">-</th>
        </tr>
        <tr>
            <th>Alamat Objek</th>
            <th>RT/RW</th>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
            <th>NIK</th>
            <th>NAMA</th>
            <th>No Telp</th>
            <th>Alamat</th>
            <th>RT/RW</th>
            <th>Kelurahan</th>
            <th>Kecamatan</th>
            <th>Kota/Kabupaten</th>
            <th>Propinsi</th>
            
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="18" class="dataTables_empty text-center"> Data {{ $layanan->nama_layanan }} Masih Kosong</td>
        </tr>
    </tbody>
</table>