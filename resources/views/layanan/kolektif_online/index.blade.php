@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-2"><b>Form Layanan Kolektif Online</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <div class="callout callout-warning col-sm-12 m-0 p-1" >
                                <ul class="pl-3 m-0">
                                    <li>
                                        Semua Ajuan
                                        <ul class="pl-3">
                                            <li >Maksimal data entry 15  NOP. dalam satu nomor pelayanan </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Mutasi Gabung
                                        <ul class="pl-3">
                                            <li>Semua Data Mutasi Gabung harus jadi satu dalam satu Nomor pelayanan </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Mutasi Pecah
                                        <ul class="pl-3">
                                            <li>Data Mutasi pecah ketika sudah mencapai batas maksimal 15 Pecah wajib di simpan</li>
                                            <li>sisa pecah yang masih tersisa di input pada nomor pelayanan berikutnya dengan tetap induknya dijadikan dasar pecahnya</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        @include('layanan/kolektif_online/pageAll')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
<script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script>
    let defaultError="Proses tidak berhasil.";
    $(document).ready(function() {
        const formEvent = function () {
            if ($("[role^='form']").length > 0) {
                var elForm = $("[role^='form']");
                if (elForm.length > 0) {
                    elForm.each(function () {
                        var f = $(this),
                        dataForm = f.serializeArray(),
                        counter = 0,
                        rulesDefault = {},
                        mssgesDefault = {},
                        textEr = " harus di isi!!",
                        textErNumber = " harus Angka";
                        for (property in dataForm) {
                            var attr = $("[name='" + dataForm[counter].name + "']", f),
                            names = attr.closest(".form-group").find(".col-form-label").html(),
                            status = (attr.hasClass("unform")) ? false : true,
                            numeric = (attr.hasClass("number")) ? true : false;
                            var text_require = (attr.attr("data-hold")) ? attr.attr("data-hold") : names + textEr,
                            number_require = (attr.attr("data-hold")) ? attr.attr("data-hold") : names + textErNumber;
                            rulesDefault[dataForm[counter].name] = {
                                required: status,
                                number: numeric
                            };
                            mssgesDefault[dataForm[counter].name] = {
                                required: text_require,
                                number: number_require
                            };
                            counter++;
                        };
                        $(this).validate({
                            rules: rulesDefault,
                            messages: mssgesDefault,
                            submitHandler: function (form) {
                                hasSubmitEven($(form));
                            }
                        });
                        dataForm = null;
                    });
                };
            };
        },hasSubmitEven = function (form) {
            if (form.length > 0) {
                let uri_new = form.attr("action");
                let modalPage=form.find('.modal');
                let jenis=form.find('[name="jenis_layanan_id"]').val();;
                form.ajaxSubmit({
                    url: uri_new,
                    type: "post",
                    dataType: "json",
                    data:$("#form-kolektif").serializeArray(),
                    success: function (response) {
                        if(!response.status){
                            return Swal.fire({ icon: 'warning', html: response.msg});
                        };
                        modalPage.modal('hide');
                        form.resetForm();
                        eventReload(jenis);
                        form.find(".tag-html").html("<tr class='null'> <td colspan='11' class='dataTables_empty text-center'> Data Masih Kosong</td></tr>");
                    },
                    error: function (x, y) {
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            };
        },eventReload=function(jenis){
            switch(jenis) {
                case "1":
                    datatable1.ajax.reload( null, false ); 
                break;
                case "2":
                    datatable2.ajax.reload( null, false ); 
                break;
                case "3":
                    datatable3.ajax.reload( null, false ); 
                break;
                case "6":
                    datatable6.ajax.reload( null, false ); 
                break;
                case "7":
                    datatable7.ajax.reload( null, false ); 
                break;
                case "9":
                    datatable9.ajax.reload( null, false ); 
                break;
                case "all":
                    datatable1.ajax.reload( null, false ); 
                    datatable2.ajax.reload( null, false ); 
                    datatable3.ajax.reload( null, false ); 
                    datatable6.ajax.reload( null, false ); 
                    datatable7.ajax.reload( null, false ); 
                    datatable9.ajax.reload( null, false ); 
                break;
                default:
                    console.log(jenis);
            }
        },asyncData=async function (uri,value){
            let getData;
            try {
                getData=await $.ajax({
                    type: "get",
                    url: uri,
                    data: value??{},
                    dataType: "json",
                });
                return getData;
            }catch(error){
                return error;
            }
        };
        Swal.fire({ 
            icon: 'warning',
            html: '<p><b>PERHATIAN</b><p>'+
                '<p>Pastikan bahwa <b>Permohonan Pembetulan</b> ketika nota perhitungan telah terbit.</p><p> Maksimal 30 hari dari tanggal terbitnya nota  harus dibayar!!!</p><p>Cek di menu NOTA PERHITUNGAN dan segera melakukan Pembayaran !!!</p>',
            confirmButtonText: "Ya",
            allowOutsideClick: false,
        });
        $(".rt_rw").inputmask('999/99');
        $(".nop_full").inputmask('99.99.999.999.999-9999.9');
        $(".nik").inputmask('9999999999999999');
        $(document).on("keyup", "[name='nop_gabung_label']", function (evt) {
            let e=$(this);
            if(e.inputmask('unmaskedvalue').length==18){
                e.closest('.form-group').find('[data-nop-add]').trigger('click');
            }
        });
        $(document).on("keyup", "[name='nop']", function (evt) {
            let e=$(this);
            if(e.inputmask('unmaskedvalue').length==18){
                e.closest('.form-group').find('[data-nop]').trigger('click');
            }
        });
        $(document).on("keyup", ".nik_lookup", function (evt) {
            let e=$(this);
            if(e.inputmask('unmaskedvalue').length==16){
                e.closest('.form-group').find('[data-nik]').trigger('click');
            }
        });
        $(document).on("click", "[data-nik]", function (evt) {
            let e=$(this),
                form=e.closest("form"),
                tInput=e.closest('.input-group').find('input'),
                v=tInput.inputmask('unmaskedvalue');
            if(v.length==16){
                let listlayanan={
                    'nik':'nik',
                    'nama_lgkp':'nama_wp',
                    'alamat':'alamat_wp',
                    'nomor_telepon':'telp_wp',
                    'kel_name':'kelurahan_wp',
                    'kec_name':'kecamatan_wp',
                    'kab_name':'dati2_wp',
                    'prop_name':'propinsi_wp',
                    'rt_rw_wp':'rt_rw_wp',
                };
                asyncData("{{url('api/ceknik')}}",{
                    nik:v
                }).then((response) => {
                    if(response.kode){
                        $.each(listlayanan,(x,y)=>{
                            if(response.raw[x]){
                                let setdata=form.find('[name="'+y+'"]');
                                if(setdata){
                                    setdata.val(response.raw[x]);
                                };
                            };
                        });
                        
                    }
                });
            }else{
                tInput.focus();
            };
        });
        $(document).on("click", ".btn-reload", function (evt) {
            evt.preventDefault();
            let e=$(this);
            Swal.fire({ 
                icon: 'warning',
                html: '<p>Hapus seluruh draft permohonan?</p>',
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "tidak",
            }).then((willsend)=>{ 
                if(willsend.isConfirmed){
                    window.location.replace(e.attr("data-href"));
                }
            });
        });
        $(document).on("submit", ".form-kolektif-save", function (evt) {
            evt.preventDefault();
            let e=$(this);
            Swal.fire({ 
                icon: 'warning',
                html: '<p>Pastikan seluruh data layanan kolektif sudah benar.</p><p>Simpan daftar layanan kolektif Online?</p>',
                showCancelButton: true,
                confirmButtonText: "Simpan",
                cancelButtonText: "Batal",
            }).then((willsend)=>{ 
                if(willsend.isConfirmed){
                    Swal.fire({
                        title: 'Proses pembuatan data Layanan kolektif Online',
                        html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                    e.ajaxSubmit({
                        type: "post",
                        dataType: "json",
                        success: function (response) {
                            if(!response.status){
                                return Swal.fire({ icon: 'warning', html: response.msg});
                            }
                            eventReload('all');
                            if(response.curl){
                                Swal.fire({
                                    title: 'Info',
                                    html:response.msg,
                                    showConfirmButton: false,
                                });
                            }else{
                                Swal.fire({ icon: 'success', text: response.msg});
                            }
                        },
                        error: function (x, y) {
                            Swal.fire({ icon: 'error', text: defaultError});
                        }
                    });
                }
            });
        })
        let calcListGabung=function(tbody,form){
            let listlb=tbody.find('[data-name="luas_bumi"]');
            let total=0;
            $.each(listlb,(x,y)=>{
                let v=parseInt($(y).html());
                if(!isNaN(v)){
                    total=parseInt(total)+v;
                }
            });
            let listlbng=tbody.find('[data-name="luas_bangunan"]');
            let totalbng=0;
            $.each(listlbng,(x,y)=>{
                let v=parseInt($(y).html());
                if(!isNaN(v)){
                    totalbng=parseInt(totalbng)+v;
                }
            });
            form.find('[name="luas_bumi"]').val(total);
            form.find('[name="luas_bangunan"]').val(totalbng);
        }
        
        $(document).on("click", "[data-nop-add]", function (evt) {
            let e=$(this),
            card=e.closest(".card"),
            form=e.closest('form'),
            name=e.attr('data-nop-add'),
            tInput=form.find('[name="'+name+'"]'),
            v=tInput.val(),
            vmask=tInput.inputmask('unmaskedvalue'),
            jenisId=form.find("[name='jenis_layanan_id']").val();
            
            let kecamatan=$("#form-kolektif [name='kecamatan']").val();
            let kelurahan=$("#form-kolektif [name='kelurahan']").val();
            if(kecamatan+"."+kelurahan!=v.substr(6,7)){
                return Swal.fire({ icon: 'error', html: 'Kelurahan yang di pilih tidak sama dengan lokasi NOP'}).then(()=>{
                    card.find('[name="'+name+'"]').focus();
                }); 
            }

            if(vmask.length==18){
                let tbody=form.find('[data-name="list_nop"]');
                asyncData("{{url('kolektif_online/ceknop_tambah')}}",{
                    nop:v,
                    jenis:jenisId
                }).then((response) => {
                    if(!response.status){
                        Swal.fire({ icon: 'error', html: response.msg}).then(()=>{
                            card.find('[name="'+name+'"]').focus();
                        }); 
                    }else{
                        tbody.find('.null').remove();
                    }
                    if(!tbody.find('[data-list-id="'+response.data_list_id+'"]').length){
                        tbody.append(response.data);
                    }
                }).then((e)=>{
                    calcListGabung(tbody,form);
                });
            }else{
                tInput.focus();
            }
        });
        $(document).on("keyup", "[name='nikadd']", function (evt) {
            let e=$(this);
            if(e.inputmask('unmaskedvalue').length==16){
                e.closest('.form-group').find('[data-nikadd]').trigger('click');
            }
        });
        $(document).on("click", "[data-nikadd]", function (evt) {
            let e=$(this),
                card=e.closest(".card"),
                tInput=e.closest('.input-group').find('input'),
                v=tInput.inputmask('unmaskedvalue'),
                type=e.attr('data-nik');
            let listlayanan={
                'kec_name':'kecamatan_wpadd',
                'nama_lgkp':'nama_wpadd',
                'alamat':'alamat_wpadd',
                'kel_name':'kelurahan_wpadd',
                'kab_name':'dati2_wpadd',
                'prop_name':'propinsi_wpadd',
                // 'blok_kav_no_wp':'blok_kav_no_wpadd',
                'nomor_telepon':'notelp_wpadd',
                'rt_rw_wp':'rt_rw_wpadd',
            };
            if(v.length==16){
                asyncData("{{url('api/ceknik')}}",{
                    nik:v
                }).then((response) => {
                    if(response.kode){
                        $.each(listlayanan,(x,y)=>{
                            if(response.raw[x]){
                                let setdata=card.find('[name="'+y+'"]');
                                let val=response.raw[x];
                                if(setdata){
                                    setdata.val(val);
                                };
                            };
                        });
                    }
                });
                $(".secondFocusAdd").focus();
            }else{
                tInput.focus();
            };
        });
        $(document).on("click", "[data-list-id]", function (evt) {
            let e=$(this),
            tbody=e.closest("tbody");
            e.closest('tr').remove();
            if(!tbody.find("[data-list-id]").length>0){
                tbody.html('<tr class="null"><td colspan="11" class="dataTables_empty text-center"> Data Masih Kosong</td></tr>');
            }
        });
        $(document).on("click", "[data-edit-id]", function (evt) {
            let e=$(this),
                    tbody=e.closest("tbody"),
                    form=e.closest('form').find('[data-table-add="noppecah"]'),
                    cloneData=e.closest('tr').find('input[data-tinput]');
            Swal.fire({ 
                icon: 'warning',
                html: '<p>Edit Data Pecah?</p>',
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "tidak",
            }).then((willsend)=>{ 
                if(willsend.isConfirmed){
                    $.each(cloneData,(x,y)=>{
                        form.find('[data-input="'+$(y).attr('data-tinput')+'"]').val($(y).val());
                    });
                    e.closest('tr').remove();
                    if(!tbody.find("[data-list-id]").length>0){
                        tbody.html('<tr class="null"><td colspan="11" class="dataTables_empty text-center"> Data Masih Kosong</td></tr>');
                    }
                }
            });
        });
        $(document).on("click", "[data-objek-add]", function (evt) {
            let e=$(this),
                form=e.closest('form'),
                target=form.find('[data-table-add="'+e.attr('data-objek-add')+'"] [name]'),
                targetReset=form.find('[data-table-add="'+e.attr('data-objek-add')+'"] [name]:not(.no-back)'),
                tag="<td></td>",
                tagInput="",
                ret=true,
                tableAppend=form.find('[data-name="list_pecah"]');
                const d = new Date();
                let time = d.getTime();
            let nik=0;
            let total=0;
            let cluster={
                nop_alamatadd:'op',
                nop_rt_rw_add:'op',
                luas_bumiadd:'op',
                luas_bngadd:'op',
                kelompok_objek_idadd:'op',
                lokasi_objek_idadd:'op',
                nikadd:'wp',
                nama_wpadd:'wp',
                notelp_wpadd:'wp',
                alamat_wpadd:'dw',
                rt_rw_wpadd:'wp',
                kelurahan_wpadd:'dw',
                kecamatan_wpadd:'dw',
                dati2_wpadd:'dw',
                propinsi_wpadd:'dw'
            };
            let op='';
            let wp='';
            let dw='';
            let labelVal;
            $.each(target,(x,y)=>{
                $(y).parent().find(".error").remove();
                if(!$(y).val()&&!$(y).hasClass('no-required')){
                    ret=false;
                    $(y).parent().append("<label class='error'> Harap di isi.</label>");
                }
                if($(y).attr('name') in cluster){
                    tagInput=' <input type="hidden" data-tinput="'+$(y).attr('data-input')+'" name="list_pecah['+time+']['+$(y).attr('data-input')+']" value="'+$(y).val()+'">';
                    if($(y).attr('data-input')=="luas_bumi_pecah"){
                        tagInput=" <input type='hidden' data-tinput='"+$(y).attr('data-input')+"' name='list_pecah["+time+"]["+$(y).attr('data-input')+"]' data-count='pecah' value='"+$(y).val()+"'>";
                        // total=parseInt($(y).val());
                    }
                    if($(y).attr('data-input')=="nik_wp_pecah"){
                        nik=$(y).val();
                    }
                    labelVal=$(y).val();
                    if($(y).hasClass('option')){
                        labelVal=(labelVal)?$(y).find('option:selected').text():'';
                    }
                    if(cluster[$(y).attr('name')]=='op'){
                        op=op+"<li><b>"+$(y).attr('data-label')+"</b> : <i>"+labelVal+"</i>"+tagInput+"</li>";
                    }
                    if(cluster[$(y).attr('name')]=='wp'){
                        wp=wp+"<li><b>"+$(y).attr('data-label')+"</b> : <i>"+labelVal+"</i>"+tagInput+"</li>";
                    }
                    if(cluster[$(y).attr('name')]=='dw'){
                        dw=dw+"<li><b>"+$(y).attr('data-label')+"</b> : <i>"+labelVal+"</i>"+tagInput+"</li>";
                    }
                }
                
            })
            if(ret){
                tag=tag+"<td><ul class='m-0'>"+op+"</ul></td><td><ul class='m-0'>"+wp+"</ul></td><td><ul class='m-0'>"+dw+"</ul></td>";
                let del='<span class="btn btn-danger btn-flat data-delete un" data-list-id="'+nik+'" title="Hapus Data " ><i class="fas fa-trash"></i></span>';
                let edit='<span class="btn btn-warning btn-flat un" data-edit-id="'+nik+'" title="Edit Data " ><i class="fas fa-file-signature"></i></span>';
                let button='<div clas="btn-group">'+del+edit+'</div>';
                tableAppend.find('tr.null').remove();
                tableAppend.append('<tr data-parent="'+nik+'">'+tag+'<td>'+button+'</td></tr>');
                let luasbumi=(parseInt(form.find('[name="luas_bumi"]').val()))?form.find('[name="luas_bumi"]').val():0;
                $.each(tableAppend.find('[data-count="pecah"]'),(x,y)=>{
                    total=parseInt(total)+parseInt($(y).val());
                });
                // if(parseInt(luasbumi)<total){
                //     tableAppend.find('[data-parent="'+nik+'"]').remove();
                //     return Swal.fire({ icon: 'warning', html: 'Data Pecah Luas bumi melebihi batas luas induk.'});
                // }
                form.find('[name="hasil_pecah_hasil_gabung"]').val(total);
                let totalHasil=parseInt(luasbumi)-parseInt(total);
                form.find('[name="sisa_pecah_total_gabung"]').val((totalHasil>0)?totalHasil:'0');    
                targetReset.val('');
                form.find('.rePecah').focus();
            }
        })
        $(document).on("click", "[data-nop]", function (evt) {
            let e=$(this),
            card=e.closest(".card"),
            form=e.closest('form'),
            name=e.attr('data-nop'),
            tInput=form.find('[name="'+name+'"]'),
            v=tInput.val(),
            vmask=tInput.inputmask('unmaskedvalue'),
            jenisId=form.find("[name='jenis_layanan_id']").val();
            let kecamatan=$("#form-kolektif [name='kecamatan']").val();
            let kelurahan=$("#form-kolektif [name='kelurahan']").val();
            if(kecamatan+"."+kelurahan!=v.substr(6,7)){
                return Swal.fire({ icon: 'error', html: 'Kelurahan yang di pilih tidak sama dengan lokasi NOP'}).then(()=>{
                    card.find('[name="'+name+'"]').focus();
                }); 
            }
            if(vmask.length==18){
                asyncData("{{url('api/ceknop')}}",{
                    nop:v,
                    jenis:jenisId
                }).then((response) => {
                    if(!response.status){
                        Swal.fire({ icon: 'error', html: response.msg}).then(()=>{
                            card.find('[name="'+name+'"]').focus();
                        }); 
                    };
                    $.each(response.data,(x,y)=>{
                        let setdataadd=form.find('[name="'+x+'"]');
                        let setdataaddlabel=form.find('[name="'+x+'_label"]');
                        if(!setdataadd.hasClass('no-change')){
                            setdataadd.val(y);
                            setdataaddlabel.val(y);
                        };
                    });
                })
            }else{
                tInput.focus();
            }
        });
        $(document).on("change", "[data-change]", function (evt) {
            let e=$(this),
                uri=e.attr("data-change").split("#"),
                target=$("[name='"+e.attr("data-target-name")+"']");
            target.appendData(uri[1],{kd_kecamatan:e.val()});
        });
        $(document).on("click", "[data-edit]", function (evt) {
            let e=$(this);
            let send=e.attr('data-edit');
            let reload=e.attr('data-reload');
            Swal.fire({ 
                icon: 'warning',
                html: '<p>Edit Data</p>',
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            }).then((willsend)=>{ 
                if(willsend.isConfirmed){
                    let modal=$(e.attr('data-target')),
                        form=modal.closest('form');
                    Swal.fire({
                        title: 'Proses',
                        html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                    asyncData("{{url('kolektif_online/edit')}}",{
                        id:send
                    }).then((response) => {
                        if(!response.status){
                            Swal.fire({ icon: 'error', html: response.msg});
                        };
                        $.when.apply($,$.each(response.data,(x,y)=>{
                            let setdataadd=form.find('[name="'+x+'"]');
                            if(!setdataadd.hasClass('no-change')){
                                setdataadd.val(y);
                                if(setdataadd.hasClass("select")){
                                    setdataadd.trigger('change.select2');
                                }
                            };
                        })).then(()=>{
                            $.each(response.tag,(x,y)=>{
                                form.find('[data-name="'+x+'"]').html(y);
                            })
                        }).then(()=>{
                            Swal.close();
                            modal.modal({
                                keyboard: false,
                                backdrop:'static',
                                show:true
                            });
                        });
                    })
                }
            });
        })
        $(document).on("click", "[data-delete]", function (evt) {
            let e=$(this);
            let send=e.attr('data-delete');
            let reload=e.attr('data-reload');
            Swal.fire({ 
                icon: 'warning',
                html: '<p>Hapus Data</p>',
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            }).then((willsend)=>{ 
                if(willsend.isConfirmed){
                    asyncData("{{url('kolektif_online/delete')}}",{
                        id:send
                    }).then((response) => {
                        if(!response.status){
                            Swal.fire({ icon: 'error', html: response.msg});
                        };
                        eventReload(reload);
                    })
                }
            });
        })
        $(document).on("click", "[data-modal-target]", function (evt) {
            let e=$(this);
            let cekData=$("#form-kolektif").serializeArray();
            let status=true;
            $.when.apply($, $.each(cekData, (x, y) => {
                if(y.value==null||y.value==""){
                    status=false;
                }
            }))
            if(!status){
                return Swal.fire({
                    icon: 'warning',
                    title: 'Oops...',
                    text: 'Kecamatan dan Kelurahan harus di isi.'
                });
            }
            $(e.attr('data-modal-target')).modal({
                keyboard: false,
                backdrop:'static',
                show:true
            });
        });
        formEvent();
        
        // databtable page 
        $.fn.dataTable.ext.errMode = 'none'??'throw';
        const datatable1=$("#table-kolektif-1").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('kolektif_online/search?jenis=1') }}",
                method: 'GET'
            },
            lengthMenu: [[-1],['Semua']],
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'alamat_wp',class:'w-15'},
                { data: 'rt_rw_op',class:'w-20'},
                { data: 'luas_bumi'},
                { data: 'luas_bng'},
                { data: 'kelompok_objek_nama'},
                { data: 'lokasi_objek_nama'},
                { data: 'nik_wp'},
                { data: 'nama_wp'},
                { data: 'telp_wp'},
                { data: 'alamat_wp'},
                { data: 'rt_rw_wp'},
                { data: 'kelurahan_wp'},
                { data: 'kecamatan_wp'},
                { data: 'dati2_wp'},
                { data: 'propinsi_wp'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        datatable1.on('error.dt',(e, settings, techNote, message)=>{
            //message??
            Swal.fire({ icon: 'warning', html: defaultError});
        });
        const datatable2=$("#table-kolektif-2").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('kolektif_online/search?jenis=2') }}",
                method: 'GET'
            },
            lengthMenu: [[-1],['Semua']],
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'nop',class:'w-15'},
                { data: 'luas_bumi'},
                { data: 'luas_bng'},
                { data: 'nik_wp'},
                { data: 'nama_wp'},
                { data: 'telp_wp'},
                { data: 'alasan'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        datatable2.on('error.dt',(e, settings, techNote, message)=>{
            //message??
            Swal.fire({ icon: 'warning', html: defaultError});
        });
        const datatable3=$("#table-kolektif-3").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('kolektif_online/search?jenis=3') }}",
                method: 'GET'
            },
            lengthMenu: [[-1],['Semua']],
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'nop',class:'w-15'},
                { data: 'alamat_op'},
                { data: 'rt_rw_op'},
                { data: 'luas_bumi'},
                { data: 'luas_bng'},
                { data: 'nik_wp'},
                { data: 'nama_wp'},
                { data: 'telp_wp'},
                { data: 'alamat_wp'},
                { data: 'rt_rw_wp'},
                { data: 'kelurahan_wp'},
                { data: 'kecamatan_wp'},
                { data: 'dati2_wp'},
                { data: 'propinsi_wp'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        datatable3.on('error.dt',(e, settings, techNote, message)=>{
            //message??
            Swal.fire({ icon: 'warning', html: defaultError});
        });
        const datatable9=$("#table-kolektif-9").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('kolektif_online/search?jenis=9') }}",
                method: 'GET'
            },
            lengthMenu: [[-1],['Semua']],
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'nop',class:'w-15'},
                { data: 'alamat_op'},
                { data: 'rt_rw_op'},
                { data: 'luas_bumi'},
                { data: 'luas_bng'},
                { data: 'nik_wp'},
                { data: 'nama_wp'},
                { data: 'telp_wp'},
                { data: 'alamat_wp'},
                { data: 'rt_rw_wp'},
                { data: 'kelurahan_wp'},
                { data: 'kecamatan_wp'},
                { data: 'dati2_wp'},
                { data: 'propinsi_wp'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        datatable9.on('error.dt',(e, settings, techNote, message)=>{
            //message??
            Swal.fire({ icon: 'warning', html: defaultError});
        });
        const datatable7=$("#table-kolektif-7").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('kolektif_online/search?jenis=7') }}",
                method: 'GET'
            },
            lengthMenu: [[-1],['Semua']],
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'nop',class:'w-15'},
                { data: 'alamat_op'},
                { data: 'rt_rw_op'},
                { data: 'luas_bumi'},
                { data: 'luas_bng'},
                { data: 'nik_wp'},
                { data: 'nama_wp'},
                { data: 'telp_wp'},
                { data: 'alamat_wp'},
                { data: 'rt_rw_wp'},
                { data: 'kelurahan_wp'},
                { data: 'kecamatan_wp'},
                { data: 'dati2_wp'},
                { data: 'propinsi_wp'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        datatable7.on('error.dt',(e, settings, techNote, message)=>{
            //message??
            Swal.fire({ icon: 'warning', html: defaultError});
        });
        const datatable6=$("#table-kolektif-6").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:"{{ url('kolektif_online/search?jenis=6') }}",
                method: 'GET'
            },
            lengthMenu: [[-1],['Semua']],
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'nop',class:'w-15'},
                { data: 'alamat_op'},
                { data: 'rt_rw_op'},
                { data: 'luas_bumi'},
                { data: 'luas_bng'},
                { data: 'nik_wp'},
                { data: 'nama_wp'},
                { data: 'telp_wp'},
                { data: 'alamat_wp'},
                { data: 'rt_rw_wp'},
                { data: 'kelurahan_wp'},
                { data: 'kecamatan_wp'},
                { data: 'dati2_wp'},
                { data: 'propinsi_wp'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        datatable6.on('error.dt',(e, settings, techNote, message)=>{
            //message??
            Swal.fire({ icon: 'warning', html: defaultError});
        });
    })
</script>
@endsection