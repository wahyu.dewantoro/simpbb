@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-5 col-sm-5">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Rekap per jenis layanan</b></h3>
                        </div>
                        <div class="card-body">
                            <form action="#rekap_jenis_layanan/search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div class="input-group">
                                            <label class="col-md-4 col-form-label">Tanggal</label>
                                            <div class="col-md-8 p-0">
                                                <input id="daterangepicker" type="text" class="form-control"  name="tgl" placeholder="Tanggal Pembuatan">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="input-group">
                                            <select name="status"  required class="form-control select"  data-placeholder="Pilih Status">
                                                <option value="-">-- Semua Status input--</option>
                                                <option value="0">Proses</option>
                                                <option value="1">Selesai</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group">
                                            <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-body no-padding">
                                <table id="main-table" class="table table-bordered table-striped " data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Jenis Layanan</th>
                                            <!-- <th>Jumlah Pribadi</th>
                                            <th>Jumlah Badan</th>
                                            <th>Jumlah Kolektif</th> -->
                                            <th>Total</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'>
                                        <!-- <tr class="null"><td colspan="9" > Lakukan Pencarian data untuk menampilkan Rekap Data.</td></tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7 col-sm-7">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Detail jenis layanan</b></h3>
                        </div>
                        <div class="card-body">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan Rekap Data.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                language: { "emptyTable": txtNull },
                lengthMenu: [[20], [20]],
                ordering: false,
                columns: [
                    { data: 'no',class:'text-center'},
                    { data: 'nama_layanan'},
                    // { data: 'pribadi'},
                    // { data: 'badan'},
                    // { data: 'kolektif'},
                    { data: 'total',class:'w-20'},
                    { data: 'aksi',class:'text-center w-10'}
                ] 
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    str.push(key.name+"="+key.value);
                });
                return str.join("&");
            }
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian daftar input layanan.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    if(response.extra){
                        $.each(response.extra,(x,y)=>{
                            $("[data-name='"+x+"']").html(y);
                        });
                    }
                    if(!response.status){
                        return Swal.fire({ icon: 'warning', html: response.msg});
                    }
                    //Swal.fire({ icon: 'success', html: response.msg});
                    swal.close();
                });
            });
            $(document).on("click", "[data-detail]", function (evt) {
                // let e=$(this),
                //     id=e.attr('data-detail'),
                //     side=$("[data-card='side']"),
                //     main =$("[data-card='main']"),
                //     target=$("[data-remote='"+id+"']");
                // if(target.length){
                //     target.addClass('active');
                //     main.hide();
                //     side.show();
                //     return;
                // };
                // Swal.fire({
                //     title: 'Detail data dafnom.',
                //     html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                //     showConfirmButton: false,
                //     allowOutsideClick: false,
                // });
                // let clone=$("[data-remote='clone']").clone();
                // clone.attr('data-remote',id);
                // clone.addClass('active');
                // asyncData("laporan_input/detail",id).then((response) => {
                //     if(!response.status){
                //         clone.remove();
                //         return Swal.fire({ icon: 'error', html: response.msg}); 
                //     }
                //     $.when.apply($, $.each(response.data, (x, y) => {
                //         clone.find('[data-name="'+x+'"]').html(y);
                //     })).then(()=>side.append(clone)).then(() => {
                //         swal.close();
                //         main.hide();
                //         side.show();
                //     });
                // }).catch((error)=>{
                //     clone.remove();
                //     Swal.fire({ icon: 'error', text: defaultError});
                // });
            });
        });
    </script>
@endsection