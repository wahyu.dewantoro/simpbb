@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Verifikasi Layanan</b></h3>
                        </div>
                        <div class="card-body">
                            <form action="#laporan_input/search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <div class="input-group">
                                            <select name="jenis_objek"  required class="form-control select"  data-placeholder="Pilih Jenis Data">
                                                <option value="-">-- Semua Jenis data--</option>
                                                <option value="1">Pribadi</option>
                                                <option value="2">Badan</option>
                                                <option value="3">Kolektif</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                        <input id="daterangepicker" type="text" class="form-control"  name="tgl" placeholder="Tanggal Pembuatan">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <select name="status"  required class="form-control select"  data-placeholder="Pilih Status">
                                                <option value="-">-- Semua Status input--</option>
                                                <option value="0">Proses</option>
                                                <option value="1">Selesai</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <div class="input-group">
                                            <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="callout callout-warning col-sm-12" data-append="callout">
                                <ul class=" m-0">
                                    <li>Untuk melakukan pencarian data pada 1 hari tertentu, tekan 2x tanngal tersebut pada pilihan tanngal pencarian</li>
                                </ul>
                            </div>
                            <div class="card-body no-padding">
                                <table id="main-table" class="table table-bordered table-striped " data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Nomor Layanan</th>
                                            <th>Jenis Layanan</th>
                                            <th>Nama</th>
                                            <th>Tgl Layanan</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            
                    @include('layanan/detail_layanan')
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'id':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:'{{ route("verifikasi_layanan.search") }}',
                    method: 'GET'
                },
                lengthMenu: [20, 40, 60, 80, 100 ],
                ordering: false,
                columns: [
                    { data: 'DT_RowIndex',class:'text-center'},
                    { data: 'nomor_layanan',class:'w-15'},
                    { data: 'jenis_layanan_nama',class:'w-20'},
                    { data: 'nama'},
                    { data: 'created_at',class:'w-15 text-center'},
                    { data: 'kd_status',class:'text-center w-10'},
                    { data: 'option',class:'text-center w-10'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    str.push(key.name+"="+key.value);
                });
                return str.join("&");
            }

            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian daftar input layanan.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    if(response.extra){
                        $.each(response.extra,(x,y)=>{
                            $("[data-name='"+x+"']").html(y);
                        });
                    }
                    if(!response.status){
                        return Swal.fire({ icon: 'warning', html: response.msg});
                    }
                    //Swal.fire({ icon: 'success', html: response.msg});
                    swal.close();
                });
            });
            $(document).on("click", "[data-card='remove']", function (evt) {
                let side=$("[data-card='side']");
                let main =$("[data-card='main']");
                $("[data-remote].active").removeClass("active");
                side.hide();
                main.show();
            });
            $(document).on("click", "[data-detail]", function (evt) {
                let e=$(this),
                    id=e.attr('data-detail'),
                    side=$("[data-card='side']"),
                    main =$("[data-card='main']"),
                    target=$("[data-remote='"+id+"']");
                if(target.length){
                    target.addClass('active');
                    main.hide();
                    side.show();
                    return;
                };
                Swal.fire({
                    title: 'Detail data dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let clone=$("[data-remote='clone']").clone();
                clone.attr('data-remote',id);
                clone.addClass('active');
                asyncData("laporan_input/detail",id).then((response) => {
                    if(!response.status){
                        clone.remove();
                        return Swal.fire({ icon: 'error', html: response.msg}); 
                    }
                    $.when.apply($, $.each(response.data, (x, y) => {
                        clone.find('[data-name="'+x+'"]').html(y);
                    })).then(()=>side.append(clone)).then(() => {
                        swal.close();
                        main.hide();
                        side.show();
                    });
                }).catch((error)=>{
                    clone.remove();
                    Swal.fire({ icon: 'error', text: defaultError});
                });
            });
        });
    </script>
@endsection