@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">

        <h1>{{ $data['title'] }}</h1>

    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body p-1">
                        <form action="{{ $data['action'] }}" method="post">
                            @csrf
                            @method($data['method'])
                            <div class="form-group">
                                <label for="">Tahun Pajak</label>
                                {{-- <input type="text" name="tahun_pajak" class="form-control" readonly
                                        value="{{ $data['lunas']->tahun_pajak ?? date('Y') }}"> --}}
                                <select name="tahun_pajak" id="tahun_pajak" class="form-control " required>
                                    <option value="">Pilih</option>
                                    @php
                                    $sel= $data['lunas']->tahun_pajak ?? date('Y');
                                    @endphp
                                    @for($i = date('Y'); $i >=2014; $i--)
                                    <option @if($i==$sel) selected @endif value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Kecamatan</label>
                                @php
                                $selected=$data['lunas']->kd_kecamatan??'';
                                @endphp
                                <select name="kd_kecamatan" id="kd_kecamatan" class="form-control select2" date-placeholder="Pilih" required>
                                    <option value="">-- pilih --</option>

                                    @foreach ($kecamatan as $rk)

                                    <option @if($selected==$rk->kd_kecamatan) selected @endif value="{{ $rk->kd_kecamatan }}">{{ $rk->kd_kecamatan }} {{ $rk->nm_kecamatan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Lunas</label>
                                <input type="text" name="tanggal_lunas" id="tanggal_lunas" class="form-control tanggal" required value="{{ isset($data['lunas']->tanggal_lunas) ? date('d M Y', strtotime($data['lunas']->tanggal_lunas)) : date('d F Y') }}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
