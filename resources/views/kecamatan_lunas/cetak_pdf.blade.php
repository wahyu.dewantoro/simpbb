<html>

<head>
   @include('layouts.style_pdf')
</head>
<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">Kecamatan Lunas PBB P2 Tahun Pajak {{ $tahun }} <br>
        <small>Ditarik pada {{ tglIndo(date('Y-m-d')) }} {{ date('H:i:s') }}</small>
    </h4>
    
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <TH STYLE="TEXT-ALIGN: CENTER">NO</TH>
                <TH STYLE="TEXT-ALIGN: CENTER">KECAMATAN</TH>
                <TH STYLE="TEXT-ALIGN: CENTER">Ketetapan PBB</TH>
                <TH STYLE="TEXT-ALIGN: CENTER">TANGGAL</TH>
                
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @if(count($data)>0)
            @foreach ($data as $index => $row)
                <tr>
                    <td  style="text-align: center">{{ $no }}</td>
                    <td>{{ $row->kecamatan->nm_kecamatan }}</td>
                    <td align="right">{{ number_format(bakuKecamatan($row->tahun_pajak,$row->kd_kecamatan),0,'','.') }}</td>
                    <td class="text-center">{{ tglIndo($row->tanggal_lunas) }}</td>
                    
                </tr>
                @php
                    $no++;
                @endphp
            @endforeach
            @else
                <tr>
                    <td style="text-align: center"colspan="4">Tidak ada data</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>

</html>
