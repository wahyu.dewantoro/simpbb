@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Kecamatan Lunas</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    {{-- @can('add_users') --}}
                    <a href="{{ route('realisasi.kecamatanlunas.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-file"></i> Tambah
                    </a>
                    {{-- @endcan --}}
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tahun Pajak</h3>
                        <div class="card-tools">

                            <div class="input-group input-group-sm">
                                <select name="thn_pajak" id="thn_pajak" class="form-control form-control-sm">
                                    @php
                                    // $sel= $tahun ?? date('Y');
                                    @endphp
                                    @for($i = date('Y'); $i >=2014; $i--)
                                    <option @if($i==$tahun) selected @endif value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                                <span class="input-group-append">
                                    {{-- <button type="button" class="btn btn-info btn-flat">Go!</button> --}}
                                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                        Export
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-item"><a id="cetak_pdf" href="#">PDF</a></li>
                                        {{-- <li class="dropdown-item"><a id="cetak_excel" href="#">Excel</a></li> --}}
                                    </ul>
                                </span>
                            </div>

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align: center">No</th>
                                    <th style="text-align: center">Kecamatan</th>
                                    <th style="text-align: center">Ketetapan PBB</th>
                                    <th style="text-align: center">Tanggal</th>
                                    <th style="text-align: center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no = 1;
                                @endphp
                                @if (count($data) > 0)
                                @foreach ($data as $index => $row)
                                <tr>
                                    <td class="text-center">{{ $index + $data->firstItem() }}</td>
                                    <td>{{ $row->kecamatan->nm_kecamatan }}</td>
                                    <td align="right">
                                        {{ number_format(bakuKecamatan($row->tahun_pajak, $row->kd_kecamatan), 0, '', '.') }}
                                    </td>
                                    <td class="text-center">{{ tglIndo($row->tanggal_lunas) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('realisasi.kecamatanlunas.edit', [$row->kd_kecamatan . '_' . $row->tahun_pajak]) }}"><i class="fas fa-edit text-info"></i> </a>
                                        <a href="{{ route('realisasi.kecamatanlunas.show', [$row->kd_kecamatan . '_' . $row->tahun_pajak]) }}" onclick="
                                                                var result = confirm('Are you sure you want to delete this record?');
                                                                if(result){
                                                                    event.preventDefault();
                                                                    document.getElementById('delete-form-{{ $row->kd_kecamatan . $row->tahun_pajak }}').submit();
                                                                }"><i class="fas fa-trash-alt text-danger"></i> </a>
                                        <form method="POST" id="delete-form-{{ $row->kd_kecamatan . $row->tahun_pajak }}" action="{{ route('realisasi.kecamatanlunas.destroy', [$row->kd_kecamatan . '_' . $row->tahun_pajak]) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @php
                                $no++;
                                @endphp
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-center" colspan="5">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer p-0">
                        <div class="float-sm-right">
                            {{ $data->appends(['cari' => request()->get('cari')]) }}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- thn_pajak --}}
@section('script')
<script>
    $(function() {
        $('#thn_pajak').on("change", function(e) {
            e.preventDefault();
            var url = "{{ url('realisasi/kecamatanlunas') }}?thn_pajak=" + $('#thn_pajak').val()
            window.location.href = url
            // window.open(
            //     url, '_blank' // <- This is what makes it open in a new window.
            // );
        })

        $('#cetak_pdf').on('click', function(e) {
            e.preventDefault();
            var url = "{{ url('realisasi/kecamatanlunas-pdf') }}?thn_pajak=" + $('#thn_pajak').val()
            // window.location.href = url
            window.open(
                url, '_blank' // <- This is what makes it open in a new window.
            );
        })
    });

</script>
@endsection
