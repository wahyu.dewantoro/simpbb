<table>
    <thead>
        <tr>
            <th>NOP</th>
            <th>TAHUN PAJAK</th>
            <th>PBB</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{{ $row->nop }}</td>
                <td>{{ $row->tahun_pajak }}</td>
                <td>{{ $row->pbb }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
