@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3"><b>Laporan Daftar Himpunan Ketetapan (DHK)</b></h3>
                    </div>
                    <div class="card-body p-1">
                        <form action="#dhk_search" id="form-cek">
                            @csrf
                            <div class="row">
                                <div class="form-group col-lg-1">
                                    <div class="input-group input-group-sm">
                                        <select name="tahun" class="form-control-sm form-control" data-placeholder="Pilih Tahun">
                                            @foreach ($tahunList as $tahun)
                                            <option value="{{ $tahun}}">
                                                {{ $tahun }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group input-group-sm">
                                        <select name="kecamatan" class="form-control-sm form-control select-bottom" data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan' required>
                                            @if(count($kecamatan)>1) <option value="-">-- Kecamatan --</option> @endif
                                            @foreach ($kecamatan as $rowkec)
                                            <option value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->nm_kecamatan }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="input-group input-group-sm">
                                        <select name="kelurahan" class="form-control-sm form-control select-bottom select-reset change-next" data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                                            @if(count($kelurahan)>1) <option value="-">-- Kelurahan --</option> @endif
                                            @foreach ($kelurahan as $rowkel)
                                            <option value="{{ $rowkel->kd_kelurahan }}">
                                                {{ $rowkel->nm_kelurahan }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="form-group col-md-3">
                                    
                                    <div class="input-group input-group-sm">
                                        <button type='submit' class='btn btn-sm  btn-primary '> <i class="fas fa-search"></i> Cari </button>    
                                        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
                                            <i class="fas fa-file-download"></i> Cetak
                                        </button>
                                        <div class="dropdown-menu">
                                            <a data-qr="yes" class="dropdown-item export" href="#">Dengan QR CODE</a>
                                            <a data-qr="no" class="dropdown-item export" href="#">Tanpa QR CODE</a>
                                            <a data-qr="excell" class="dropdown-item export" href="#">Excell</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="card-body no-padding p-1">
                            <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                <thead>
                                    <tr>
                                        <th class='number'>No</th>
                                        <th>Kelurahan</th>
                                        <th>Blok</th>
                                        <th>ZNT</th>
                                        <th>HKPD/NJKP(%)</th>
                                        <th>Jumlah NOP</th>
                                        <th>Luas Bumi</th>
                                        <th>NIR</th>
                                    </tr>
                                </thead>
                                <tbody class='table-sm'></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        let defaultError = "Proses tidak berhasil.";
        let txtNull = 'Lakukan Pencarian data untuk menampilkan data layanan input.';
        let isnull = "<tr class='null'><td colspan='9'>" + txtNull + "</td></tr>";
        $(".nop_full").inputmask('99.99.999.999.999-9999.9');
        async function asyncData(uri, value) {
            let getData;
            try {
                getData = await $.ajax({
                    type: "get"
                    , url: uri
                    , data: (value) ? {
                        "_token": "{{ csrf_token() }}"
                        , 'data': value
                    } : {}
                    , dataType: "json"
                , });
                return getData;
            } catch (error) {
                return error;
            }
        };
        $.fn.dataTable.ext.errMode = 'none' ?? 'throw';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        let datatable = $("#main-table").DataTable({
            processing: true
            , serverSide: true
            , ajax: {
                url: "{{ url('analisa/dhk_search') }}"
                , method: 'GET'
            }
            , lengthMenu: [20]
            , ordering: false
            , columns: [{
                    data: 'DT_RowIndex'
                    , name: 'DT_RowIndex'
                    , searchable: false
                }
                , {
                    data: 'nm_kelurahan'
                }
                , {
                    data: 'kd_blok'
                    , 'class': "text-center"
                }
                , {
                    data: 'kd_znt'
                    , 'class': "text-center"
                }
                , {
                    data: 'hkpd'
                    , 'class': "text-center"
                }
                , {
                    data: 'totalnop'
                    , 'class': "text-right"
                }
                , {
                    data: 'luas_bumi_sppt'
                    , 'class': "text-right"
                }
                , {
                    data: 'nir'
                    , 'class': "text-right"
                },
                // { data: 'njop_bumi_sppt'},
                // { data: 'njop_bng_sppt'}
            ]
        });
        datatable.on('error.dt', (e, settings, techNote, message) => {
            Swal.fire({
                icon: 'warning'
                , html: defaultError
            });
        });
        let timer = 600000;
        let resLoad = () => {
            datatable.ajax.reload(null, false);
        };
        setInterval(function() {
            resLoad();
        }, timer);
        const toString = function(obj) {
            let str = [];
            obj.map((key) => {
                if (key.name != '_token') {
                    str.push(key.name + "=" + key.value);
                }
            });
            return str.join("&");
        }

        $(document).on("click", ".export", function(evt) {

            var qr = $(this).data('qr')
            let e = $(this)
                , form = e.closest('form')
                , url = "{{url('analisa/dhk_cetak')}}?qr=" + qr + '&' + toString(form.serializeArray());
                window.open(url, '_blank');
        });


        $(document).on("submit", "#form-cek", function(evt) {
            evt.preventDefault();
            let e = $(this)
                , uri = e.attr("action").split("#");
            Swal.fire({
                title: 'Pencarian Data.'
                , html: '<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>'
                , showConfirmButton: false
                , allowOutsideClick: false
            , });
            let response = datatable.ajax.url(uri[1] + "?" + toString(e.serializeArray())).load((response) => {
                swal.close();
            });
        });
        const changeData = function(e, async_status = true) {
            let uri = e.attr("data-change").split("#")
                , target = e.closest('form').find("[name='" + e.attr("data-target-name") + "']");
            target.appendData("{{url('')}}/" + uri[1], {
                kd_kecamatan: e.val()
            }, {
                obj: '-- Semua Kelurahan --'
                , key: '-'
            }, async_status, true);
            return target;
        };
        $(document).on("change", "[data-change]", function(evt) {
            let e = $(this);
            changeData(e);
        });
    });

</script>
@endsection
