<div class="row">
    <div class="col-12">
        <p class="text-info"><strong>Informasi</strong></p>
        <table class="table table-borderless table-sm text-sm">
            <tbody>
                <tr>
                    <td width="100px">NOP</td>
                    <td width="1px">:</td>
                    <td> {{ $wp->kd_propinsi .
                        '.' .
                        $wp->kd_dati2 .
                        '.' .
                        $wp->kd_kecamatan .
                        '.' .
                        $wp->kd_kelurahan .
                        '.' .
                        $wp->kd_blok .
                        '-' .
                        $wp->no_urut .
                        '.' .
                        $wp->kd_jns_op }}
                    </td>
                </tr>
                <tr>
                    <td>Alamat Objek</td>
                    <td>:</td>
                    <td>
                        @if ($wp->no_persil != '')
                            Persil {{ $wp->no_persil }}
                        @endif
                        @if ($wp->jalan_op != '')
                            {{ $wp->jalan_op }}
                        @endif
                        @if ($wp->blok_kav_no_op != '')
                            {{ $wp->blok_kav_no_op }}
                        @endif
                        @if ($wp->rt_op != '')
                            RT {{ $wp->rt_op }}
                        @endif
                        @if ($wp->rw_op != '')
                            RW {{ $wp->rw_op }}
                        @endif
                        <br>
                        {{ $wp->NmKelurahan }} ,
                        {{ $wp->NmKecamatan }}
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>:</td>
                    <td class="text-danger">{{ jenisBumi($wp->DatOpBumi->jns_bumi) }}</td>
                </tr>
                <tr>
                    <td>Luas</td>
                    <td>:</td>
                    <td>
                        Bumi : {{ angka($wp->total_luas_bumi) }}<br>
                        Bng : {{ angka($wp->total_luas_bng) }}
                    </td>
                </tr>
                <tr>
                    <td>Wajib Pajak</td>
                    <td>:</td>
                    <td> {{ $wp->nm_wp }}
                        <br>
                        <i class="text-danger">{{ statusWP($wp->kd_status_wp) }}</i>
                    </td>
                </tr>
                <tr>
                    <td>Alamat WP</td>
                    <td>:</td>
                    <td>
                        @if ($wp->DatSubjekPajak->jalan_wp != '')
                            {{ $wp->DatSubjekPajak->jalan_wp }}
                        @endif
                        @if ($wp->DatSubjekPajak->blok_kav_no_wp != '')
                            {{ $wp->DatSubjekPajak->blok_kav_no_wp }}
                        @endif
                        @if ($wp->DatSubjekPajak->rt_wp != '')
                            RT {{ $wp->DatSubjekPajak->rt_wp }}
                        @endif
                        @if ($wp->DatSubjekPajak->rw_wp != '')
                            RW {{ $wp->DatSubjekPajak->rw_wp }}
                        @endif
                        @if ($wp->DatSubjekPajak->kelurahan_wp != '')
                            {{ $wp->DatSubjekPajak->kelurahan_wp }}
                        @endif
                        @if ($wp->DatSubjekPajak->kota_wp != '')
                            {{ $wp->DatSubjekPajak->kota_wp }}
                        @endif

                    </td>
                </tr>
            </tbody>
        </table>
        <form action="{{ url('analisa/nop_non_aktif') }}" method="POST" id="form-pengaktifan">
            @csrf
            @method('post')
            <input type="hidden" name="kd_propinsi" id="kd_propinsi" value="{{ $wp->kd_propinsi }}">
            <input type="hidden" name="kd_dati2" id="kd_dati2" value="{{ $wp->kd_dati2 }}">
            <input type="hidden" name="kd_kecamatan" id="kd_kecamatan" value="{{ $wp->kd_kecamatan }}">
            <input type="hidden" name="kd_kelurahan" id="kd_kelurahan" value="{{ $wp->kd_kelurahan }}">
            <input type="hidden" name="kd_blok" id="kd_blok" value="{{ $wp->kd_blok }}">
            <input type="hidden" name="no_urut" id="no_urut" value="{{ $wp->no_urut }}">
            <input type="hidden" name="kd_jns_op" id="kd_jns_op" value="{{ $wp->kd_jns_op }}">
            @foreach ($tagihan as $item)
                @if ($item->sisa == '')
                    <input type="hidden" name="tahun[]" value="{{ $item->tahun }}">
                @endif
            @endforeach
            <div class="form-group">
                <div class="float-right">
                    <button id="submit" class=" btn btn-sm btn-info"><i class="fas fa-save"></i> Submit</button>
                </div>
            </div>

        </form>
    </div>
    {{-- <div class="col-12">

        <hr>
        <p class="text-info"><strong>SPPT</strong></p>
        <table class="table table-sm text-sm table-bordered">
            <thead>
                <tr>
                    <th>Tahun</th>
                    <th>PBB</th>
                    <th>Stimulus</th>
                    <th>Terbayar</th>
                    <th>Koreksi</th>
                    <th>Sisa</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                @endphp
                @foreach ($tagihan as $item)
                    @php
                        // $jumlah = $item->pbb + $item->denda;
                        // $total += $jumlah;
                    @endphp
                    <tr>
                        <td class="text-center">{{ $item->tahun }}</td>
                        <td class="text-right">{{ $item->pbb }}</td>
                        <td class="text-right">{{ $item->potongan }}</td>
                        <td class="text-right">{{ $item->terbayar }}</td>
                        <td class="text-right">{{ $item->koreksi }}</td>
                        <td class="text-right">{{ $item->sisa }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div> --}}
</div>
<script>
    $("#form-pengaktifan").on("submit", function(event) {
        openloading();
        event.preventDefault();
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "dengan mengaktifan objek akan menerbitkan nota perhitungan dari tunggakan dengan masa aktif 1 hari, Objek akan aktif setelah nota perhitungan di lunasi",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Yakin!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                openloading()
                $.ajax({
                    url: "{{ url('analisa/nop_non_aktif') }}",
                    type: 'post',
                    data: $(this).serialize(),
                    success: function(res) {
                        closeloading();
                        if (res.status == '1') {
                            toastr.success('Berhasil', res.pesan);
                        } else {
                            toastr.warning('Gagal', res.pesan);
                        }

                        if (res.url != '') {
                            window.open(res.url);
                        }
                        $('#exampleModal').modal('hide')
                        $('#main-table').DataTable().ajax.reload();
                    },
                    error: function(res) {
                        closeloading();
                        toastr.error('Error', 'Terjadi kesalahan sistem');
                        $('#exampleModal').modal('hide')
                        $('#main-table').DataTable().ajax.reload();
                    }
                })
            }
        })
    });
</script>
