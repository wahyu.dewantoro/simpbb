@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline "">
                    <div class=" card-header p-2">
                    <h3 class="card-title p-3"><b>Daftar NOP di blokir </b></h3>

                </div>


                <div class="card-body no-padding p-1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="nop" id="nop" autofocus="true" class="form-control form-control-sm" placeholder="Masukan nop">
                                    </div>
                                </div>
                            </div>
                            <table id="table" class="table table-bordered table-striped text-sm table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NOP</th>
                                        <th>Surat Keputusan</th>
                                        <th>Wajib Pajak</th>
                                        <th>Tahun</th>
                                        <th>Piutang</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection
@section('script')

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



        tableHasil = $('#table').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ route('pembayaran.blokir.nop')}}"
                , data: function(d) {
                    d.nop = $('#nop').val()
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , name: 'nop'
                    , orderable: false
                    , searchable: false
                }, {
                    data: 'data_sk'
                    , name: 'data_sk'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'wajib_pajak'
                    , name: 'wajib_pajak'
                    , orderable: false
                    , searchable: false
                }
                , {
                    class: 'text-center'
                    , data: 'thn_pajak_sppt'
                    , name: 'thn_pajak_sppt'
                    , orderable: false
                    , searchable: false

                }
                , {
                    data: 'pbb_akhir'
                    , name: 'pbb_akhir'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'

                }
                , {
                    class: 'text-center'
                    , data: 'aksi'
                    , name: 'aksi'
                    , orderable: false
                    , searchable: false
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        $('#search').on('keyup', function(e) {
            e.preventDefault()
            tableHasil.draw();
        })

        $('#nop').on('keyup', function(e) {
            var nop = $(this).val();
            var convert = formatnop(nop);
            $(this).val(convert);
            if (convert.length == 24) {
                tableHasil.draw();
            }
        });


        $('#table').on('click', '.buka', function(e) {
            e.preventDefault();
            var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt;
            kd_propinsi = $(this).data('kd_propinsi')
            kd_dati2 = $(this).data('kd_dati2')
            kd_kecamatan = $(this).data('kd_kecamatan')
            kd_kelurahan = $(this).data('kd_kelurahan')
            kd_blok = $(this).data('kd_blok')
            no_urut = $(this).data('no_urut')
            kd_jns_op = $(this).data('kd_jns_op')
            thn_pajak_sppt = $(this).data('thn_pajak_sppt')

            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "NOP akan masuk kedalam piutang DHKP"
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        type: 'POST'
                        , url: "{{ route('pembayaran.blokir.buka') }}"
                        , data: {
                            kd_propinsi
                            , kd_dati2
                            , kd_kecamatan
                            , kd_kelurahan
                            , kd_blok
                            , no_urut
                            , kd_jns_op
                            , thn_pajak_sppt
                        , }
                        , success: function(data) {
                            closeloading()
                            toastr.success(data.msg);
                            tableHasil.draw();
                        }
                        , error: function() {
                            closeloading()
                            toastr.error('Gagal melakukan proses buka blokir');
                        }
                    });
                }
            })
        })


    })

</script>
@endsection
