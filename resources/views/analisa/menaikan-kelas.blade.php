@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.6/css/buttons.dataTables.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Simulasi Penerapan HKPD</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-12 ">
                <div class="card card-primary card-tabs">
                    <div class="card-body p-1">
                        <div class="row">

                            {{-- 
                            <div class="col-4 col-md-2">

                                <button class="btn btn-info btn-flat btn-sm" id="btn-refresh" type="button"><i
                                        class="fas fa-sync-alt"></i></button>
                                <button class="btn btn-success btn-flat btn-sm" id="btn-export" type="button"><i
                                        class="fas fa-file-download"></i></button>
                            </div> --}}


                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Kecamatan</label>
                                    <div class="col-sm-8">
                                        <select name="kd_kecamatan" id="kd_kecamatan" class="form-control form-control-sm">
                                            <option value="">Kecamatan</option>
                                            @foreach ($kecamatan as $kd_kecamatan => $kec)
                                                <option value="{{ $kd_kecamatan }}">{{ $kd_kecamatan . ' - ' . $kec }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Kelurahan</label>
                                    <div class="col-sm-8">
                                        <select class="form-control form-control-sm " name="kd_kelurahan" id="kd_kelurahan">
                                            <option value="">-- Kelurahan / Desa -- </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">ZNT </label>
                                    <div class="col-sm-8">
                                        <select class="form-control form-control-sm " name="kd_znt" id="kd_znt">
                                            <option value="">-- ZNT / kawasan -- </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Tahun</label>
                                    <div class="col-sm-8">
                                        <select name="tahun" id="tahun" class="form-control form-control-sm">
                                            <div class="col-md-4">
                                                <option value="">Tahun</option>
                                                @foreach ($tahun as $rtahun)
                                                    <option value="{{ $rtahun }}">{{ $rtahun }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">HKPD (%)</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="hkpd" id="hkpd" max="100" min="1"
                                            class="form-control form-control-sm" value='80'>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Naik Kelas</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="naik_kelas" id="naik_kelas"
                                            class="form-control form-control-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-8 offset-sm-4 ">
                                        <button class="btn btn-info btn-flat btn-sm" id="btn-refresh" type="button"><i
                                                class="fas fa-sync-alt"></i> Proses</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div id="review"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.6/js/buttons.print.min.js"></script>


    <script>
        $(document).ready(function() {


            $(document).on("click", "#btn-export", function(evt) {
                tahun = $('#tahun').val()
                tahun_last = $('#tahun_last').val()
                kd_kecamatan = $('#kd_kecamatan').val()
                let url = "{{ route('dhkp.rekap-perbandingan-excel') }}?tahun=" + tahun + "&tahun_last=" +
                    tahun_last + "&kd_kecamatan=" + kd_kecamatan
                window.open(url, '_blank');
            });



            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })


            $('#kd_kelurahan').on('change', function() {
                kd_kecamatan = $('#kd_kecamatan').val()
                kd_kelurahan = $('#kd_kelurahan').val()

                getkawasan(kd_kecamatan, kd_kelurahan)
            })


            function getkawasan(kd_kecamatan, kd_kelurahan) {
                var html = '<option value="">-- ZNT / kawasan -- </option>';
                if (kd_kecamatan != '' && kd_kelurahan != '') {
                    $.ajax({
                        url: "{{ url('analisa/menaikan-kelas') }}",
                        data: {
                            kd_kecamatan,
                            kd_kelurahan,
                            'jenis': 'kawasan'
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {

                                var kaw = " [kawasan belumdi setup]";
                                if (v != null) {
                                    kaw = ' - ' + v;
                                }

                                var apd = '<option value="' + k + '">' + k + kaw +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_znt').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_znt').html(html);
                        },
                        error: function(res) {
                            $('#kd_znt').html(html);
                        }
                    });
                } else {
                    $('#kd_znt').html(html);
                }
            }

            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan/Desa --</option>';
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }

           
            $(document).on('click', '#btn-refresh', function() {
                
                var kd_kecamatan = $('#kd_kecamatan').val()
                var kd_kelurahan = $('#kd_kelurahan').val()
                var tahun = $('#tahun').val()
                var hkpd = $('#hkpd').val()
                var kd_znt = $('#kd_znt').val()
                var naik_kelas=$('#naik_kelas').val()
                openloading()
                $.ajax({
                    url: "{{ url('analisa/menaikan-kelas') }}",
                    data: {
                        kd_kecamatan,
                        kd_kelurahan,
                        tahun,
                        hkpd,
                        kd_znt,
                        naik_kelas
                    },
                    success: function(res) {
                        closeloading()
                        $('#review').html(res);
                    },
                    error: function(res) {
                        closeloading()
                        $('#review').html('');
                    }
                });

            });

            // $('#tahun').trigger('change')
        })
    </script>
@endsection
