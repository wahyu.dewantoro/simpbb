@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline "">
                    <div class=" card-header p-2">
                    <h3 class="card-title p-3"><b>Verifikasi Koreksi Piutang</b></h3>
                    <div class="card-tools">
                        <a class="btn btn-sm btn-info" href="{{ route('pembayaran.blokir.verifikasi') }}"><i class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>


                <div class="card-body ">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-striped">
                                        <thead class="bg-info">
                                            <tr>
                                                <th>Tahun</th>
                                                <th class="text-right">Piutang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($rekap as $item)
                                            <tr>
                                                <td>{{ $item->thn_pajak_sppt }}</td>
                                                <td class="text-right">{{ angka($item->pbb_akhir) }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <form action="{{ route('pembayaran.blokir.verifikasi-form.store',$trx) }}" method="post">
                                        @csrf
                                        @method('post')


                                        <div class="form-group">
                                            <label for="verifikasi_kode">Status</label>
                                            <select name="verifikasi_kode" id="verifikasi_kode" class="form-control form-control-sm" required>
                                                <option value="">Pilih</option>
                                                <option value="1">Setuju</option>
                                                <option value="0">Di tolak</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="verifikasi_keterangan">Keterangan</label>
                                            <textarea class="form-control" name="verifikasi_keterangan" id="verifikasi_keterangan" rows='2' required></textarea>
                                        </div>
                                        <button class="btn btn-primary btn-sm" type="submit"><i class="far fa-save"></i> submit </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-12  col-md-4 order-1 order-md-2">
                            <div class="text-muted">
                                <p class="text-sm "><b class="d-blok">No Transaksi</b><br>
                                    {{ $data->no_transaksi }}
                                </p>
                                <p class="text-sm "><b class="d-blok">No Surat Keputusan</b><br>
                                    {{ $data->no_surat }}
                                </p>
                                <p class="text-sm "><b class="d-blok">Tanggal Surat Keputusan</b><br>
                                    {{ tglIndo($data->tgl_surat) }}
                                </p>
                                <p class="text-sm "><b class="d-blok">Jenis</b><br>
                                    @php
                                    switch ($data->jns_koreksi) {
                                    case '4':
                                    # code...
                                    $jns='Iventaris';
                                    break;

                                    default:
                                    # code...
                                    $jns='Blokir';
                                    break;
                                    }
                                    echo $jns;
                                    @endphp
                                </p>
                                <p class="text-sm "><b class="d-blok">Keterangan</b><br>
                                    {{ $data->keterangan }}
                                </p>
                                <p class="text-sm "><b class="d-blok">Verifikasi</b><br>

                                    @php

                                    switch ($data->verifikasi_kode) {
                                    case '1':
                                    # code...
                                    $res = "Telah di setujui";

                                    break;
                                    case '0':
                                    # code...
                                    $res = "Tidak di setujui";
                                    break;
                                    default:
                                    # code...
                                    $res = "Belum di verifikasi";
                                    break;
                                    }

                                    echo $res;
                                    @endphp

                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection
@section('script')

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    })

</script>
@endsection
