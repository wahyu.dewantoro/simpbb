<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    <table width="100%" class="">
        <tr>
            <th width="70px"><img width="60px" height="70px" src="{{ public_path('kabmalang.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
        </tr>
    </table>
    <hr>
    <table style="font-size: .875rem !important" width="100%">
        <tr>
            <th colspan="10">
                <h3 style="text-align: center ">DAFTAR NOP KOREKSI</h3>
            </th>
        </tr>
    </table>
    <table style="font-size: .875rem !important" width="100%">
        <tr>
            <td colspan="2"> NO Transaksi</td>
            <td>:{{$data->no_transaksi}}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"> No Surat</td>
            <td>:{{$data->no_surat}}</td>
            <td></td>
            <td colspan="2">Tanggal Surat</td>
            <td>:{{tglIndo($data->tgl_surat)}}</td>

        </tr>
    </table>
    <table class="border" width="100%">
        <thead>
            <tr>
                <th class='number'>No</th>
                <th>NOP</th>
                <th>Subjek</th>
                <th>Tahun</th>
                <th>PBB</th>
            </tr>
        </thead>
        <tbody>
            @php
            $no=1;
            @endphp
            @foreach ($nop as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>
                    {{ $row->kd_propinsi}}.
                    {{ $row->kd_dati2}}.
                    {{ $row->kd_kecamatan}}.
                    {{ $row->kd_kelurahan}}.
                    {{ $row->kd_blok}}.
                    {{ $row->no_urut}}.
                    {{ $row->kd_jns_op}}
                </td>
                <td>{{ $row->nm_wp_sppt }}</td>
                <td align="center">{{ $row->thn_pajak_sppt }}</td>
                <td align="right">{{ angka($row->pbb_akhir) }}</td>
            </tr>
            @php $no++; @endphp
            @endforeach
        </tbody>
    </table>
    {{-- <p></p>
    <p></p>
    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%"></td>
                <td></td>
                <td width="40%" align="center">
                    <p>Malang, {{ tglIndo($keckel['tgl_now']) }}<br>
                        Pemerintah Daerah Kabupaten Malang<br>
                        Bupati<br>
                        <br>
                        <br>
                        <br>
                        ---<br>
                        Sekertaris Daerah Kabupaten Malang
                    </p>
                </td>
            </tr>
        </tbody>
    </table> --}}
</body>
</html>
