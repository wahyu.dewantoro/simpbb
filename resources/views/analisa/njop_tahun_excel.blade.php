<table class="table table-striped text-sm table-sm table-bordered table-hover " id="table-hasil" border="1">
    <thead class="bg-info">
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">NOP</th>
            <th colspan="6">NJOP BUMI</th>
            <th colspan="6">NJOP BNG</th>
            <th colspan="4">PBB</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th class="tahun_last">Luas {{ $last }}</th>
            <th class="tahun_last">Kelas {{ $last }}</th>
            <th class="tahun_last">NJOP {{ $last }}</th>
            <th class="tahun_last">Luas {{$now }}</th>
            <th class="tahun_last">Kelas {{$now }}</th>
            <th class="tahun_now">NJOP {{ $now }}</th>
            <th class="tahun_last">Luas {{ $last }}</th>
            <th class="tahun_last">Kelas {{ $last }}</th>
            <th class="tahun_last">NJOP {{ $last }}</th>
            <th class="tahun_now">Luas {{ $now }}</th>
            <th class="tahun_now">Kelas {{ $now }}</th>
            <th class="tahun_last">NJOP {{ $now }}</th>
            <th class="tahun_last">Tarif {{ $last }}</th>
            <th class="tahun_last">PBB {{ $last }}</th>
            <th class="tahun_last">Tarif {{ $now }}</th>
            <th class="tahun_last">PBB {{ $now }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
        <tr>
            <td></td>
            <td>{{ $item->kd_propinsi.'.'.$item->kd_dati2.'.'.$item->kd_kecamatan.'.'.$item->kd_kelurahan.'.'.$item->kd_blok.'-'.$item->no_urut.'.'.$item->kd_jns_op}}</td>
            <td>{{ $item->luas_bumi_last }}</td>
            <td>{{ $item->kd_kls_tanah_last }}</td>
            <td>{{ $item->njop_bumi_last }}</td>
            <td>{{ $item->luas_bumi_now }}</td>
            <td>{{ $item->kd_kls_tanah_now }}</td>
            <td>{{ $item->njop_bumi_now }}</td>
            <td>{{ $item->luas_bng_last }}</td>
            <td>{{ $item->kd_kls_bng_last }}</td>
            <td>{{ $item->njop_bng_last }}</td>
            <td>{{ $item->luas_bng_now }}</td>
            <td>{{ $item->kd_kls_bng_now }}</td>
            <td>{{ $item->njop_bng_now }}</td>
            <td>{{ $item->tarif_last }}</td>
            <td>{{ $item->pbb_last }}</td>
            <td>{{ $item->tarif_now }}</td>
            <td>{{ $item->pbb_now }}</td>
            @php
            $deskripsi = "";
            if ($item->njop_bng_last > $item->njop_bng_now) {
               $deskripsi .= "NJOP bangunan turun ,";
            }

            if ($item->njop_bumi_last > $item->njop_bumi_now) {
            $deskripsi .= "NJOP bumi turun ,";
            }

            if ($item->njop_bng_last < $item->njop_bng_now) {
                $deskripsi .= "NJOP bangunan naik ,";
                }

                if ($item->njop_bumi_last < $item->njop_bumi_now) {
                    $deskripsi .= "NJOP bumi naik ,";
                    }

                    if ($deskripsi <> '') {
                        $deskripsi = substr($deskripsi, 0, -1);
                        }

                        // return $deskripsi;
                        @endphp
                        <td>{{ $deskripsi }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
