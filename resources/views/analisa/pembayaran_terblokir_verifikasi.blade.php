@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3"><b>Verifikasi Koreksi Piutang</b></h3>
                    </div>
                    <div class="card-body p-1">
                        <form action="#pembayaran_terblokir_search" id="form-cek">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="search" id="search" class="form-control form-control-sm" placeholder="Pencarian">
                                        <span class="input-group-append">
                                            <button id="cari" type="button" class="btn btn-info btn-flat"><i class="fas fa-search"></i></button>
                                          
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </form>
                        <div class="card-body no-padding p-1">
                            <table id="table" class="table table-bordered table-striped text-sm table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No Transaksi</th>
                                        <th>No Surat</th>
                                        <th>Tgl Surat</th>
                                        <th>Keterangan</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection
@section('script')

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        tableHasil = $('#table').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ route('pembayaran.blokir.verifikasi') }}"
                , data: function(d) {

                    d.search = $('#search').val()
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'no_transaksi'
                    , name: 'no_transaksi'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'no_surat'
                    , name: 'no_surat'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'tgl_surat'
                    , name: 'tgl_surat'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'keterangan'
                    , name: 'keterangan'
                    , orderable: false
                    , searchable: false

                }
                , {
                    data: 'verifikasi_kode'
                    , name: 'verifikasi_kode'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'aksi'
                    , name: 'aksi'
                    , orderable: false
                    , searchable: false
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        $('#cari').on('click', function(e) {
            e.preventDefault()
            tableHasil.draw();
        })
 


        $('#table').on('click', '.buka', function(e) {
            e.preventDefault();
            var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt;
            kd_propinsi = $(this).data('kd_propinsi')
            kd_dati2 = $(this).data('kd_dati2')
            kd_kecamatan = $(this).data('kd_kecamatan')
            kd_kelurahan = $(this).data('kd_kelurahan')
            kd_blok = $(this).data('kd_blok')
            no_urut = $(this).data('no_urut')
            kd_jns_op = $(this).data('kd_jns_op')
            thn_pajak_sppt = $(this).data('thn_pajak_sppt')

            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "dengan membuka tagihan , akan masuk kedalam piutang DHKP"
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        type: 'POST'
                        , url: "{{ route('pembayaran.blokir.buka') }}"
                        , data: {
                            kd_propinsi
                            , kd_dati2
                            , kd_kecamatan
                            , kd_kelurahan
                            , kd_blok
                            , no_urut
                            , kd_jns_op
                            , thn_pajak_sppt
                        , }
                        , success: function(data) {
                            closeloading()
                            toastr.success(data.msg);

                            // $('#kd_kecamatan').val(kd_kecamatan)
                            // $('#kd_kelurahan').val(kd_kelurahan)
                            // $('#thn_pajak_sppt').val(thn_pajak_sppt)
                            $('#cari').trigger('click')
                        }
                        , error: function() {
                            closeloading()
                            toastr.error('Gagal melakukan proses buka blokir');
                        }
                    });
                }
            })
        })
 

    })

</script>
@endsection
