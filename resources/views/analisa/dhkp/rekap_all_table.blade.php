@php
$is_excel=$excel??'0';
@endphp
<table class="table table-sm table-bordered text-sm  ">
    <thead class="bg-info">
        <tr>
            <th rowspan="2" class="text-center">Kecamatan</th>
            <th rowspan="2" class="text-center">Kelurahan</th>
            <th rowspan="2" class="text-center">Objek</th>
            <th class="text-center" colspan="4">Penetapan</th>

        </tr>
        <tr>
            <th class="text-center">SPPT</th>
            <th class="text-center">PBB</th>
            <th class="text-center">Stimulus</th>
            <th class="text-center">PBB Akhir</th>
        </tr>
    </thead>
    <tbody>
        @php
        $op=0;
        $jumlah_objek=0;
        $pbb=0;
        $potongan=0;
        $pbb_akhir=0;
        @endphp
        @foreach ($data as $row)

        @php
        $op +=$row->op;
        $jumlah_objek +=$row->jumlah_objek;
        $pbb +=$row->pbb;
        $potongan +=$row->potongan;
        $pbb_akhir +=$row->pbb_akhir;
        @endphp

        <tr @if($row->jumlah_objek=='') class='bg-red' @endif   @if($row->jumlah_objek!=$row->op && $row->jumlah_objek!='') class='bg-warning' @endif>
            <td class=" ">
                {{ $row->kd_kecamatan }} - {{ $row->nm_kecamatan }}
            </td>
            <td class=" ">
                {{ $row->kd_kelurahan }} - {{ $row->nm_kelurahan }}
            </td>
            <td class="  text-center">{{ $is_excel=='0'? angka($row->op) :$row->op }}</td>
            <td class="  text-center">{{ $is_excel=='0'? angka($row->jumlah_objek) :$row->jumlah_objek }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->pbb) :$row->pbb }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->potongan) :$row->potongan }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->pbb_akhir) :$row->pbb_akhir }}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td class="text-center text-bold" colspan="2">TOTAL</td>
            <td class="text-center text-bold">{{ $is_excel=='0'? angka($op) :$op }}</td>
            <td class="text-center text-bold">{{ $is_excel=='0'? angka($jumlah_objek) :$jumlah_objek }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($pbb) :$pbb }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($potongan) :$potongan }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($pbb_akhir) :$pbb_akhir }}</td>
        </tr>
    </tfoot>
</table>
