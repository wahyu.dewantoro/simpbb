@php
$is_excel=$excel??'0';
@endphp
<table class="table table-sm table-bordered text-sm  ">
    <thead class="bg-info">
        <tr>
            <th rowspan="2" class="text-center">Kecamatan</th>
            <th rowspan="2" class="text-center">Kelurahan</th>
            <th class="text-center" colspan="4">{{ $tahun }}</th>
            <th class="text-center" colspan="4">{{ $tahun_last }}</th>
        </tr>
        <tr>
            <th class="text-center">SPPT</th>
            <th class="text-center">PBB</th>
            <th class="text-center">Stimulus</th>
            <th class="text-center">PBB Akhir</th>
            <th class="text-center">SPPT</th>
            <th class="text-center">PBB</th>
            <th class="text-center">Stimulus</th>
            <th class="text-center">PBB Akhir</th>
        </tr>
    </thead>
    <tbody>
        @php
        $op=0;
        $jumlah_objek=0;
        $pbb=0;
        $potongan=0;
        $pbb_akhir=0;


        $jumlah_objek_last=0;
        $pbb_last=0;
        $potongan_last=0;
        $pbb_akhir_last=0;
        @endphp
        @foreach ($data as $row)

        @php
        $jumlah_objek +=$row->jumlah_objek_now;
        $pbb +=$row->pbb_now;
        $potongan +=$row->potongan_now;
        $pbb_akhir +=$row->pbb_akhir_now;

        $jumlah_objek_last +=$row->jumlah_objek_last;
        $pbb_last +=$row->pbb_last;
        $potongan_last +=$row->potongan_last;
        $pbb_akhir_last +=$row->pbb_akhir_last;
        @endphp

        <tr>
            <td class=" ">
                {{ $row->kd_kecamatan }} - {{ $row->nm_kecamatan }}
            </td>
            <td class=" ">
                {{ $row->kd_kelurahan }} - {{ $row->nm_kelurahan }}
            </td>

            <td class="  text-center">{{ $is_excel=='0'? angka($row->jumlah_objek_now) :$row->jumlah_objek_now }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->pbb_now) :$row->pbb_now }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->potongan_now) :$row->potongan_now }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->pbb_akhir_now) :$row->pbb_akhir_now }}</td>

            <td class="  text-center">{{ $is_excel=='0'? angka($row->jumlah_objek_last) :$row->jumlah_objek_last }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->pbb_last) :$row->pbb_last }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->potongan_last) :$row->potongan_last }}</td>
            <td class="  text-right">{{ $is_excel=='0'? angka($row->pbb_akhir_last) :$row->pbb_akhir_last }}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td class="text-center text-bold" colspan="2">TOTAL</td>

            <td class="text-center text-bold">{{ $is_excel=='0'? angka($jumlah_objek) :$jumlah_objek }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($pbb) :$pbb }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($potongan) :$potongan }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($pbb_akhir) :$pbb_akhir }}</td>

            <td class="text-center text-bold">{{ $is_excel=='0'? angka($jumlah_objek_last) :$jumlah_objek_last }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($pbb_last) :$pbb_last }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($potongan_last) :$potongan_last }}</td>
            <td class="text-right text-bold">{{ $is_excel=='0'? angka($pbb_akhir_last) :$pbb_akhir_last }}</td>
        </tr>
    </tfoot>
</table>
