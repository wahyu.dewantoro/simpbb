@extends('layouts.app')
@section('css')
<style>


</style>
@endsection
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>REKAPITULASI DHKP</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-1">
                    <div class="row">
                        <div class="col-3 col-md-1">
                            <select name="tahun" id="tahun" class="form-control form-control-sm">
                                <option value="">Tahun</option>
                                @foreach ($tahun as $tahun)
                                <option @if ($ts==$tahun) selected @endif value="{{ $tahun }}">{{ $tahun }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4 col-md-2">
                            <select name="kd_kecamatan" id="kd_kecamatan" class="form-control form-control-sm">
                                <option value="">Kecamatan</option>
                                @foreach ($kecamatan as $kd_kecamatan=> $kec)
                                <option value="{{ $kd_kecamatan }}">{{ $kd_kecamatan.' - '.$kec }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4 col-md-2">

                            <button class="btn btn-info btn-flat btn-sm" id="btn-refresh" type="button"><i class="fas fa-sync-alt"></i></button>
                            <button class="btn btn-success btn-flat btn-sm" id="btn-export" type="button"><i class="fas fa-file-download"></i></button>
                        </div>
                        <div class="col-md-12 table-responsive">
                            <div id="review"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- <style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style> --}}
@endsection

@section('script')
{{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> --}}

<script>
    $(document).ready(function() {


        $(document).on("click", "#btn-export", function(evt) {
            tahun = $('#tahun').val()
            kd_kecamatan = $('#kd_kecamatan').val()
            let url = "{{ url('dhkp/desa/cetak') }}?tahun=" + tahun + "&kd_kecamatan=" + kd_kecamatan
            window.open(url, '_blank');
        });


        function pencarian(tahun, kd_kecamatan) {
            openloading()
            $.ajax({
                url: "{{ route('analisa.analisa.rekap-all') }}"
                , data: {
                    'tahun': tahun
                    , 'kd_kecamatan': kd_kecamatan
                }
                , success: function(res) {
                    closeloading()
                    $('#review').html(res);
                }
                , error: function(res) {
                    closeloading()
                    $('#review').html('');
                }
            });
        }


        $(document).on('change', '#tahun,#kd_kecamatan', function() {
            tahun = $('#tahun').val()
            kd_kecamatan = $('#kd_kecamatan').val()
            pencarian(tahun, kd_kecamatan)
        });


        // $
        $(document).on('click', '#btn-refresh', function() {
            tahun = $('#tahun').val()
            kd_kecamatan = $('#kd_kecamatan').val()
            pencarian(tahun, kd_kecamatan)
        });

        $('#tahun').trigger('change')
    })

</script>
@endsection
