<div class="table-responsive">
    <table class="table table-sm text-sm table-hover table-bordered" id="table">
        <thead>
            <tr>
                <th>NOP</th>
                <th>ZNT</th>
                <th>NIR</th>
                <th>KELAS TANAH</th>
                <th>LUAS BUMI</th>
                <th>NJOP/M<sup>2</sup> TANAH</th>
                <th>NJOP TANAH</th>
                <th>LUAS BNG</th>
                <th>NJOP/M<sup>2</sup> BNG</th>
                <th>NJOP BNG</th>
                <th>NJOPTKP</th>
                <th>HKPD LAMA</th>
                <th>NILAI NJKP TANAH LAMA</th>
                <th>NILAI NJKP LAMA</th>
                <th>TARIF PBB TANAH LAMA</th>
                <th>TARIF PBB LAMA</th>
                <th>PBB TANAH LAMA</th>
                <th>PBB LAMA</th>
                <th>HKPD BARU</th>
                <th>KENAIKAN KELAS</th>
                <th>KELAS TANAH BARU</th>
                <th>NJOP M<sup>2</sup> TANAH BARU</th>
                <th>NJOP TANAH BARU</th>
                <th>NILAI NJKP TANAH BARU</th>
                <th>NILAI NJKP BARU</th>
                <th>TARIF PBB TANAH BARU</th>
                <th>TARIF PBB BARU</th>
                <th>PBB TANAH BARU</th>
                <th>PBB BARU</th>
                <th>KETERANGAN</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td>{{ $item->nop }}</td>
                    <td>{{ $item->kd_znt }}</td>
                    <td>{{ $item->nir }}</td>
                    <td>{{ $item->kelas_tanah }}</td>
                    <td>{{ angka($item->luas_bumi) }}</td>
                    <td>{{ angka($item->njop_m2_tanah) }}</td>
                    <td>{{ angka($item->njop_tanah) }}</td>
                    <td>{{ angka($item->luas_bng) }}</td>
                    <td>{{ angka($item->njop_m2_bng) }}</td>
                    <td>{{ angka($item->njop_bng) }}</td>
                    <td>{{ angka($item->njoptkp) }}</td>
                    <td>{{ angka($item->hkpd_lama, 2) }}</td>
                    <td>{{ angka($item->nilai_njkp_lama) }}</td>
                    <td>{{ angka($item->nilai_njkp_all_lama) }}</td>
                    <td>{{ angka($item->tarif_pbb_lama, 3) }}%</td>
                    <td>{{ angka($item->tarif_pbb_all_lama, 3) }}%</td>
                    <td>{{ angka($item->pbb_lama) }}</td>
                    <td>{{ angka($item->pbb_all_lama) }}</td>
                    <td>{{ angka($item->hkpd_baru, 2) }}</td>
                    <td>{{ angka($item->kenaikan_kelas) }}</td>
                    <td>{{ angka($item->kelas_tanah_baru) }}</td>
                    <td>{{ angka($item->njop_m2_tanah_baru) }}</td>
                    <td>{{ angka($item->njop_tanah_baru) }}</td>
                    <td>{{ angka($item->nilai_njkp_baru) }}</td>
                    <td>{{ angka($item->nilai_njkp_all_baru) }}</td>
                    <td>{{ angka($item->tarif_pbb_baru, 3) }}%</td>
                    <td>{{ angka($item->tarif_pbb_all_baru, 3) }}%</td>
                    <td>{{ angka($item->pbb_baru) }}</td>
                    <td>{{ angka($item->pbb_all_baru) }}</td>
                    <td>
                        @php
                            $selisih = (int) $item->pbb_lama - (int) $item->pbb_baru;
                            
                            $persen = angka(($selisih / $item->pbb_lama) * 100, 2);
                            
                            switch ($selisih) {
                                case $selisih < 0:
                                    # code...
                                    $ket = 'Naik';
                                    break;
                                case $selisih > 0:
                                    # code...
                                    $ket = 'Turun';
                                    break;
                                default:
                                    # code...
                                    $ket = 'Tetap';
                                    break;
                            }
                            
                            echo $ket ;
                            // . ' ' . abs($persen) . '% dari PBB tanah sebelumnya';
                            
                        @endphp
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<style>
    .toolbar {
        float: left;
    }
</style>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            dom: '<"toolbar">frtip',
            initComplete: function() {
                $("div.toolbar")
                    .html(
                        '<button class="btn btn-sm btn-success" type="button" id="any_button"><i class="far fa-file-alt"></i> Excell</button>'
                    );
            }
        })

        $('#any_button').on("click", function(e) {
            e.preventDefault()
            var kd_kecamatan = $('#kd_kecamatan').val()
            var kd_kelurahan = $('#kd_kelurahan').val()
            var tahun = $('#tahun').val()
            var hkpd = $('#hkpd').val()
            var kd_znt = $('#kd_znt').val()
            var naik_kelas = $('#naik_kelas').val()
            // alert('oke');

            var url = "{{ url('analisa/menaikan-kelas-excell') }}?kd_kecamatan=" + kd_kecamatan +
                "&kd_kelurahan=" + kd_kelurahan + "&tahun=" + tahun + "&hkpd=" + hkpd + "&kd_znt=" +
                kd_znt + "&naik_kelas=" + naik_kelas
            window.location.href = url

        })
    });
</script>
