@extends('layouts.app')
@section('css')
    <style>
        .dataTables_filter,
        .dataTables_length {
            display: none;
        }


        .has-search .form-control {
            padding-left: 2.375rem;
        }

        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pengaktifan Objek</h1>
                </div>
                <div class="col-sm-6">
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home"
                                aria-selected="true">Non AKtif & Fasum</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill"
                                href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile"
                                aria-selected="false">Kode Billing Pengaktifan</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade show active" id="custom-tabs-four-home" role="tabpanel"
                            aria-labelledby="custom-tabs-four-home-tab">
                            <div class="input-group col-md-6">
                                <div class="form-group has-search">
                                    <span class="fa fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" placeholder="Search" id="pencarian">

                                </div>
                            </div>
                            <table id="main-table" class="table table-bordered table-striped table-sm"
                                data-append="response-data" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">NOP</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Bumi</th>
                                        <th class="text-center">Bng</th>
                                        <th class="text-center">Wajib Pajak</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody class='table-sm'></tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel"
                            aria-labelledby="custom-tabs-four-profile-tab">
                            <div class="input-group col-md-6">
                                <div class="form-group has-search">
                                    <span class="fa fa-search form-control-feedback"></span>
                                    <input type="text" class="form-control" placeholder="Search" id="pencarian_dua">

                                </div>
                            </div>
                            <table id="main-table-dua" class="table table-bordered table-striped "
                                data-append="response-data" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>NOP Baru</th>
                                        <th>NOP Lama</th>
                                        <th>Tanggal</th>
                                        <th>Petugas</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pengaktifan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="kontenmodal">
                    ...
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError =
                "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.";
            async function asyncData(uri, value) {
                let getData;
                try {
                    getData = await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value) ? {
                            "_token": "{{ csrf_token() }}",
                            'data': value
                        } : {},
                        dataType: "json",
                    });
                    return getData;
                } catch (error) {
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none' ?? 'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable = $("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('analisa/nop_non_aktif') }}",
                    method: 'GET',
                    data: function(d) {
                        d.pencarian = $('#pencarian').val();
                    }
                },
                ordering: false,
                columns: [{
                        data: 'nop',
                        class: 'w-15'
                    },
                    {
                        data: 'alamat',
                        class: 'w-20'
                    },
                    {
                        data: 'luas_bumi',
                        class: 'w-5 text-center'
                    },
                    {
                        data: 'luas_bng',
                        class: 'w-5 text-center'
                    },
                    {
                        data: 'nm_wp',
                        class: 'w-20'
                    },
                    {
                        data: 'action',
                        class: 'w-5 text-center'
                    }
                ]
            });
            datatable.on('error.dt', (e, settings, techNote, message) => {
                Swal.fire({
                    icon: 'warning',
                    html: defaultError
                });
            });

            let datatable_dua = $("#main-table-dua").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('analisa/nop-reaktif') }}",
                    method: 'GET',
                    data: function(d) {
                        d.kd_jns_op = '4'
                        d.pencarian = $('#pencarian_dua').val();
                    }
                },
                ordering: false,
                columns: [

                    {
                        data: 'nop',
                        searchable: false
                    },
                    {
                        data: 'nop_asal',
                        searchable: false
                    },
                    {
                        data: 'tanggal',
                        searchable: false
                    },
                    {
                        data: 'petugas',
                        searchable: false
                    },

                    {
                        data: 'action',
                        searchable: false
                    },

                ]
            });
            datatable_dua.on('error.dt', (e, settings, techNote, message) => {
                //message??
                Swal.fire({
                    icon: 'warning',
                    html: defaultError
                });
            });


            function refresh() {
                datatable.draw()
            }


            function refresh_dua() {
                datatable_dua.draw()
            }


            $('#pencarian').on('keyup', function(e) {
                refresh()
            })

            $('#pencarian_dua').on('keyup', function(e) {
                refresh_dua()
            })

            $(document).on("click", ".reactive", function(e) {
                e.preventDefault()
                let kd_propinsi = $(this).data('kd_propinsi')
                let kd_dati2 = $(this).data('kd_dati2')
                let kd_kecamatan = $(this).data('kd_kecamatan')
                let kd_kelurahan = $(this).data('kd_kelurahan')
                let kd_blok = $(this).data('kd_blok')
                let no_urut = $(this).data('no_urut')
                let kd_jns_op = $(this).data('kd_jns_op')
                openloading()
                $.ajax({
                    url: "{{ url('analisa/nop_non_aktif/load-tunggakan') }}",
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'kd_propinsi': kd_propinsi,
                        'kd_dati2': kd_dati2,
                        'kd_kecamatan': kd_kecamatan,
                        'kd_kelurahan': kd_kelurahan,
                        'kd_blok': kd_blok,
                        'no_urut': no_urut,
                        'kd_jns_op': kd_jns_op,
                    },
                    success: function(res) {
                        $('#exampleModal').modal('show')
                        $('#kontenmodal').html(res)
                        closeloading()
                    },
                    error: function(er) {
                        closeloading()
                        Swal.fire({
                            icon: 'error',
                            title: 'Oppss',
                            text: "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem."
                        })
                    }
                })
            })
        });
    </script>
@endsection
