@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
<section class="content content-cloud">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card card-primary card-outline "">
                    <div class=" card-header p-2">
                    <h3 class="card-title p-3"><b>Koreksi Piutang</b></h3>
                    <div class="card-tools">
                        <a target="_blank" href="{{ route('pembayaran.blokir.pdf',$trx) }}" class="btn btn-sm btn-success"><i class="fas fa-file-download"></i></a>
                        <a class="btn btn-sm btn-info" href="{{ route('pembayaran.blokir.index') }}"><i class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>


                <div class="card-body no-padding p-1">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="search" id="search" class="form-control form-control-sm" placeholder="Pencarian">
                                    </div>
                                </div>
                            </div>
                            <table id="table" class="table table-bordered table-striped text-sm table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NOP</th>
                                        <th>Wajib Pajak</th>
                                        <th>Tahun</th>
                                        <th>Piutang</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-12  col-md-4 order-1 order-md-2">
                            <div class="text-muted">
                                <p class="text-sm "><b class="d-blok">No Transaksi</b><br>
                                    {{ $data->no_transaksi }}
                                </p>
                                <p class="text-sm "><b class="d-blok">No Surat Keputusan</b><br>
                                    {{ $data->no_surat }}
                                </p>
                                <p class="text-sm "><b class="d-blok">Tanggal Surat Keputusan</b><br>
                                    {{ tglIndo($data->tgl_surat) }}
                                </p>
                                <p class="text-sm "><b class="d-blok">Jenis</b><br>
                                    @php
                                    switch ($data->jns_koreksi) {
                                    case '4':
                                    # code...
                                    $jns='Iventaris';
                                    break;

                                    default:
                                    # code...
                                    $jns='Blokir';
                                    break;
                                    }
                                    echo $jns;
                                    @endphp
                                </p>
                                <p class="text-sm "><b class="d-blok">Keterangan</b><br>
                                    {{ $data->keterangan!=''?$data->keterangan:'-' }}
                                </p>
                                <p class="text-sm "><b class="d-blok">Verifikasi</b><br>

                                    @php

                                    switch ($data->verifikasi_kode) {
                                    case '1':
                                    # code...
                                    $res = "Telah di setujui";

                                    break;
                                    case '0':
                                    # code...
                                    $res = "Tidak di setujui";
                                    break;
                                    default:
                                    # code...
                                    $res = "Belum di verifikasi";
                                    break;
                                    }

                                    echo $res;
                                    @endphp
                                    @if ($data->verifikasi_keterangan!='')
                                    <br><small class="text-danger">{{ $data->verifikasi_keterangan }}</small>

                                    @endif

                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection
@section('script')

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        tableHasil = $('#table').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ route('pembayaran.blokir.show',$trx)}}"
                , data: function(d) {
                    d.search = $('#search').val()
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , name: 'nop'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'wajib_pajak'
                    , name: 'wajib_pajak'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'thn_pajak_sppt'
                    , name: 'thn_pajak_sppt'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'pbb_akhir'
                    , name: 'pbb_akhir'
                    , orderable: false
                    , searchable: false

                }
                , {
                    data: 'aksi'
                    , name: 'aksi'
                    , orderable: false
                    , searchable: false
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        $('#search').on('keyup', function(e) {
            e.preventDefault()
            tableHasil.draw();
        })


        $('#table').on('click', '.buka', function(e) {
            e.preventDefault();
            var kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op, thn_pajak_sppt;
            kd_propinsi = $(this).data('kd_propinsi')
            kd_dati2 = $(this).data('kd_dati2')
            kd_kecamatan = $(this).data('kd_kecamatan')
            kd_kelurahan = $(this).data('kd_kelurahan')
            kd_blok = $(this).data('kd_blok')
            no_urut = $(this).data('no_urut')
            kd_jns_op = $(this).data('kd_jns_op')
            thn_pajak_sppt = $(this).data('thn_pajak_sppt')

            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "dengan membuka tagihan , akan masuk kedalam piutang DHKP"
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        type: 'POST'
                        , url: "{{ route('pembayaran.blokir.buka') }}"
                        , data: {
                            kd_propinsi
                            , kd_dati2
                            , kd_kecamatan
                            , kd_kelurahan
                            , kd_blok
                            , no_urut
                            , kd_jns_op
                            , thn_pajak_sppt
                        , }
                        , success: function(data) {
                            closeloading()
                            toastr.success(data.msg);
                            tableHasil.draw();
                        }
                        , error: function() {
                            closeloading()
                            toastr.error('Gagal melakukan proses buka blokir');
                        }
                    });
                }
            })
        })


    })

</script>
@endsection
