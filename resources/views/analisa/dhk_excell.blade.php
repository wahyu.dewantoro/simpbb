 <table>
     <tr>
         <td>Kecamatan</td>
         <td>Kelurahan</td>
         <td>BLOK</td>
         <td>ZNT</td>
         <td>HKPD/NJKP</td>
         <td>NIR</td>
         <td>Luas Bumi</td>
         <td>Jumlah Objek</td>
     </tr>
     @foreach ($data as $rk)
     <tr>
         <td>{{ $rk->nm_kecamatan}}</td>
         <td>{{ $rk->nm_kelurahan}}</td>
         <td>{{ $rk->kd_blok}}</td>
         <td>{{ $rk->kd_znt}}</td>
         <td>{{ $rk->hkpd}}</td>
         <td>{{ $rk->nir}}</td>
         <td>{{ $rk->luas_bumi_sppt}}</td>
         <td>{{ $rk->totalnop}}</td>
     </tr>
     @endforeach
 </table>
