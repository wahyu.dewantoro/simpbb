
    <table class="table table-sm text-sm table-hover table-bordered" id="table">
        <thead>
            <tr>
                <th rowspan="2">NOP</th>
                <th rowspan="2">Alamat</th>
                <th rowspan="2">ZNT</th>
                <th rowspan="2">Lokasi</th>
                <th rowspan="2">NIR</th>
                <th rowspan="2">KELAS TANAH</th>
                <th rowspan="2">LUAS BUMI</th>
                <th rowspan="2">NJOP/M<sup>2</sup> TANAH</th>
                <th rowspan="2">NJOP TANAH</th>
                <th rowspan="2">LUAS BNG</th>
                <th rowspan="2">NJOP/M<sup>2</sup> BNG</th>
                <th rowspan="2">NJOP BNG</th>
                <th rowspan="2">NJOPTKP</th>
                <th rowspan="2">HKPD LAMA</th>
                <th rowspan="2">NILAI NJKP TANAH LAMA</th>
                <th rowspan="2">NILAI NJKP LAMA</th>
                <th rowspan="2">TARIF PBB TANAH LAMA</th>
                <th rowspan="2">TARIF PBB LAMA</th>
                <th rowspan="2">PBB TANAH LAMA</th>
                <th rowspan="2">PBB LAMA</th>
                <th rowspan="2">HKPD BARU</th>
                <th rowspan="2">KENAIKAN KELAS</th>
                <th rowspan="2">KELAS TANAH BARU</th>
                <th rowspan="2">NJOP M<sup>2</sup> TANAH BARU</th>
                <th rowspan="2">NJOP TANAH BARU</th>
                <th rowspan="2">NILAI NJKP TANAH BARU</th>
                <th rowspan="2">NILAI NJKP BARU</th>
                <th rowspan="2">TARIF PBB TANAH BARU</th>
                <th rowspan="2">TARIF PBB BARU</th>
                <th rowspan="2">PBB TANAH BARU</th>
                <th rowspan="2">PBB BARU</th>
                <th colspan="2">POTONGAN</th>
                <th rowspan="2">NJOP SPPT</th>
                <th rowspan="2">KETERANGAN</th>
            </tr>
            <tr>
                <th>Persen</th>
                <th>Upload</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
                <tr>
                    <td>{{ $item->nop }}</td>
                    <td>{{ $item->alamat_objek }}</td>
                    <td>{{ $item->kd_znt }}</td>
                    <td>{{ $item->kategori_lokasi }}</td>
                    <td>{{ $item->nir }}</td>
                    <td>{{ $item->kelas_tanah }}</td>
                    <td>{{ ($item->luas_bumi) }}</td>
                    <td>{{ ($item->njop_m2_tanah) }}</td>
                    <td>{{ ($item->njop_tanah) }}</td>
                    <td>{{ ($item->luas_bng) }}</td>
                    <td>{{ ($item->njop_m2_bng) }}</td>
                    <td>{{ ($item->njop_bng) }}</td>
                    <td>{{ ($item->njoptkp) }}</td>
                    <td>{{ ($item->hkpd_lama*100)  }}%</td>
                    <td>{{ ($item->nilai_njkp_lama) }}</td>
                    <td>{{ ($item->nilai_njkp_all_lama) }}</td>
                    <td>{{ angka($item->tarif_pbb_lama, 3) }}%</td>
                    <td>{{ angka($item->tarif_pbb_all_lama, 3) }}%</td>
                    <td>{{ ($item->pbb_lama) }}</td>
                    <td>{{ ($item->pbb_all_lama) }}</td>
                    <td>{{ ($item->hkpd_baru) }}%</td>
                    <td>{{ ($item->kenaikan_kelas) }}</td>
                    <td>{{ ($item->kelas_tanah_baru) }}</td>
                    <td>{{ ($item->njop_m2_tanah_baru) }}</td>
                    <td>{{ ($item->njop_tanah_baru) }}</td>
                    <td>{{ ($item->nilai_njkp_baru) }}</td>
                    <td>{{ ($item->nilai_njkp_all_baru) }}</td>
                    <td>{{ angka($item->tarif_pbb_baru, 3) }}%</td>
                    <td>{{ angka($item->tarif_pbb_all_baru, 3) }}%</td>
                    <td>{{ ($item->pbb_baru) }}</td>
                    <td>{{ ($item->pbb_all_baru) }}</td>
                    <td>{{ $item->potongan_individu==0?($item->persentase_potongan*100):0 }}%</td>
                    
                    <td>{{ ($item->potongan_individu) }}</td>
                    <td>{{ ($item->njop_sppt) }}</td>
                    <td>
                        @php
                            $selisih = (int) $item->pbb_lama - (int) $item->pbb_baru;
                            
                            $persen = angka(($selisih / $item->pbb_lama) * 100, 2);
                            
                            switch ($selisih) {
                                case $selisih < 0:
                                    # code...
                                    $ket = 'Naik';
                                    break;
                                case $selisih > 0:
                                    # code...
                                    $ket = 'Turun';
                                    break;
                                default:
                                    # code...
                                    $ket = 'Tetap';
                                    break;
                            }
                            
                            echo $ket ;
                            // . ' ' . abs($persen) . '% dari PBB tanah sebelumnya';
                            
                        @endphp
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
