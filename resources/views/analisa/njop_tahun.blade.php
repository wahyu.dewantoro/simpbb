@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>NJOP NAIK/TURUN LINTAS TAHUN PAJAK</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group row text-sm">
                                    <label for="tahun_pajak_a" class="col-sm-4 col-form-label">Tahun Pajak</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-6">
                                                <select name="tahun_pajak_a" id="tahun_pajak_a" class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @for($i = date('Y')+1; $i >= 2014; $i--)
                                                    <option @if($i==$now) selected @endif value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-6">
                                                <select name="tahun_pajak_b" id="tahun_pajak_b" class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @for($i = date('Y'); $i >= 2014; $i--)
                                                    <option @if($i==$last) selected @endif value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label for="jenis" class="col-sm-4 col-form-label">NJOP</label>
                                    <div class="col-sm-8">
                                        <select name="jenis" id="jenis" class="form-control form-control-sm pencariandata">
                                            <option value="">All</option>
                                            <option value="1">Bumi</option>
                                            <option value="2">Bangunan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label for="status" class="col-sm-4 col-form-label">STATUS</label>
                                    <div class="col-sm-8">
                                        <select name="status" id="status" class="form-control form-control-sm pencariandata">
                                            <option value="">All</option>
                                            <option value="1">Naik</option>
                                            <option value="2">Turun</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Export </button>
                            </div>
                        </div>
                    </form>

                    <table class="table table-striped text-sm table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                        <thead class="bg-info">
                            <tr>
                                <th rowspan="2" style="text-align: center" style="width: 10px">No</th>
                                <th style="text-align: center" rowspan="2">NOP</th>
                                <th style="text-align: center" colspan="4">NJOP BUMI</th>
                                <th style="text-align: center" colspan="4">NJOP BNG</th>
                                <th colspan="4" style="text-align: center" >PBB</th>
                                <th rowspan="2" style="text-align: center" style="width: 10px">Keterangan</th>
                            </tr>
                            <tr>
                                <th style="text-align: center" class="tahun_last">LUAS {{ $last }}</th>
                                <th style="text-align: center" class="tahun_last">NJOP {{ $last }}</th>
                                <th style="text-align: center" class="tahun_last">LUAS {{$now }}</th>
                                <th style="text-align: center" class="tahun_now">NJOP {{ $now }}</th>
                                <th style="text-align: center" class="tahun_last">LUAS {{ $last }}</th>
                                <th style="text-align: center" class="tahun_last">NJOP {{ $last }}</th>
                                <th style="text-align: center" class="tahun_now">LUAS {{ $now }}</th>
                                <th style="text-align: center" class="tahun_last">NJOP {{ $now }}</th>
                                <th style="text-align: center" class="tahun_last">TRF {{ $last }}</th>
                                <th style="text-align: center" class="tahun_last">PBB {{ $last }}</th>
                                <th style="text-align: center" class="tahun_now">TRF {{ $now }}</th>
                                <th style="text-align: center" class="tahun_now">PBB {{ $now }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .dataTables_filter,
    .dataTables_length {
        display: none;
    }

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableHasil
        var now = $('#tahun_pajak_a').val()
        var last = $('#tahun_pajak_b').val()

        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ url('analisa/njop-tahun') }}?now=" + now + "&last=" + last
                , data: function(d) {
                    d.tahun_pajak_a = $('#tahun_pajak_a').val();
                    d.tahun_pajak_b = $('#tahun_pajak_b').val();
                    d.jenis = $('#jenis').val();
                    d.status = $('#status').val();
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , name: 'nop'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'luas_bumi_last'
                    , name: 'luas_bumi_last'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                }
                , {
                    data: 'njop_bumi_last'
                    , name: 'njop_bumi_last'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)+"<br><small>Kelas :"+row.kd_kls_tanah_last+"</small>"
                    }
                }
                , {
                    data: 'luas_bumi_now'
                    , name: 'luas_bumi_now'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                }
                , {
                    data: 'njop_bumi_now'
                    , name: 'njop_bumi_now'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)+"<br><small>Kelas :"+row.kd_kls_tanah_now+"</small>"
                    }
                }
                , {
                    data: 'luas_bng_last'
                    , name: 'luas_bng_last'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                }
                , {
                    data: 'njop_bng_last'
                    , name: 'njop_bng_last'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)+"<br><small>Kelas :"+row.kd_kls_bng_last+"</small>"
                    }
                }
                , {
                    data: 'luas_bng_now'
                    , name: 'luas_bng_now'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                }
                , {
                    data: 'njop_bng_now'
                    , name: 'njop_bng_now'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)+"<br><small>Kelas :"+row.kd_kls_bng_now+"</small>"
                    }
                }
                , {
                    data: 'tarif_last'
                    , name: 'tarif_last'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                }
                , {
                    data: 'pbb_last'
                    , name: 'pbb_last'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)
                    }
                }
                , {
                    data: 'tarif_now'
                    , name: 'tarif_now'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    
                }
                , {
                    data: 'pbb_now'
                    , name: 'pbb_now'
                    , orderable: false
                    , searchable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data)
                    }
                }

                , {
                    data: 'keterangan'
                    , name: 'keterangan'
                    , orderable: false
                    , searchable: false
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });


        $(document).on('change', '.pencariandata', function() {
            pencarian();
        });


        $(document).on("click", ".btn-export", function(evt) {
            now = $('#tahun_pajak_a').val();
            last = $('#tahun_pajak_b').val();
            jenis = $('#jenis').val();
            status = $('#status').val();

            let url = "{{url('analisa/njop-tahun-excel')}}?now=" + now + "&last=" + last + "&jenis=" + jenis + "&status=" + status
            window.open(url, '_blank');
        });


        function pencarian() {
            tableHasil.draw();
        }


        $(document).on('change', '#tahun_pajak_a,#tahun_pajak_b', function() {
            var now = $('#tahun_pajak_a').val()
            var last = $('#tahun_pajak_b').val()
            $('.tahun_now').html(now)
            $('.tahun_last').html(last)

            var newUrl = "{{ url('analisa/njop-tahun') }}?now=" + now + "&last=" + last;
            $('#table-hasil').DataTable().ajax.url(newUrl).load()

        });

        /**/
    })

</script>
@endsection
