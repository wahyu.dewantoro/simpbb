@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
    <style>
        .dataTables_filter,
        .dataTables_length {
            display: none;
        }
    </style>
@endsection
@section('content')
<section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Personal Confidential Information (PCI)</b></h3>
                        </div>
                        <div class="card-body p-1">
                            <form action="#pci_search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control form-control-sm "  name="nik" placeholder="NIK / Nama">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group input-group-sm">
                                            <button type='submit' class='btn btn-sm btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group input-group-sm">
                                            <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i class="fas fa-file-alt"></i> Cetak </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-body no-padding p-1">
                                <table id="main-table" class="table table-bordered table-striped table-sm" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Subjek</th>
                                            <th>NOP</th>
                                            <th>Alamat</th>
                                            <th>Kelurahan</th>
                                            <th>Kecamatan</th>
                                            <th>NJOP T.</th>
                                            <th>NJOP B.</th>
                                            <th>PBB</th>
                                            <th>Status</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody class='table-sm'></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data layanan input.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            $(".nik_full").inputmask('999999999999999999');
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'data':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url:"{{ url('analisa/pci_search') }}",
                    method: 'GET'
                },
                lengthMenu: [20],
                ordering: false,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                    { data: 'raw_tag',class:'w-15'},
                    { data: 'nop',class:'w-15'},
                    { data: 'jalan_op'},
                    { data: 'nm_kelurahan'},
                    { data: 'nm_kecamatan'},
                    { data: 'njop_bumi_sppt'},
                    { data: 'njop_bng_sppt'},
                    { data: 'pbb'},
                    { data: 'status'},
                    { data: 'keterangan'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            let timer=600000;
            let resLoad=()=>{
                datatable.ajax.reload( null, false ); 
            };
            setInterval( function () {
                resLoad();
            }, timer );
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    if(key.name!='_token'){
                        str.push(key.name+"="+key.value);
                    }
                });
                return str.join("&");
            }
            $(document).on("click", ".btn-export", function (evt) {
                let e=$(this),
                    form=e.closest('form'),
                    url="{{url('analisa/pci_cetak')}}?"+toString(form.serializeArray());
                    window.open(url, '_blank');
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian Data.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    swal.close();
                });
            });
        });
    </script>
@endsection