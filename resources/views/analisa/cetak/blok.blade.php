<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN BLOK</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{ $keckel['nm_kecamatan'] }}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{ $keckel['nm_kelurahan'] }}</td>

    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number' rowspan="2">No</th>
            <th rowspan="2">NOP</th>
            <th rowspan="2">Alamat Objek</th>
            <th rowspan="2">Nama</th>
            <th rowspan="2">NIK</th>
            <th rowspan="2">Telep</th>
            <th rowspan="2">Alamat WP</th>
            <th rowspan="2">Blok</th>
            <th rowspan="2">Tahun</th>
            <th rowspan="2">Status</th>
            <th rowspan="2">ZNT</th>
            <th rowspan="2">NIR</th>
            <th rowspan="2">Kelas</th>
            <th colspan="2">NJOP </th>
            <th colspan="2">Luas</th>
            <th rowspan="2">PBB</th>
        </tr>
        <tr>
            <th>Tanah</th>
            <th>Bangunan</th>
            <th>Tanah</th>
            <th>Bangunan</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach ($result as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>
                    {{ $row->kd_propinsi }}.
                    {{ $row->kd_dati2 }}.
                    {{ $row->kd_kecamatan }}.
                    {{ $row->kd_kelurahan }}.
                    {{ $row->kd_blok }}-
                    {{ $row->no_urut }}.
                    {{ $row->kd_jns_op }}
                </td>
                <td>
                    {{ $row->jalan_op }}
                    {{ $row->blok_kav_no_op }}
                    RT {{ $row->rt_op }}
                    RW {{ $row->rw_op }}
                </td>
                <td>{{ $row->nm_wp }}</td>
                <th>&nbsp;{{ $row->subjek_pajak_id }}</th>
                <th>{{ $row->telp_wp }}</th>
                <td>
                    {{ $row->jalan_wp }}
                    {{ $row->blok_kav_no_wp }}
                    RT {{ $row->rt_wp }}
                    RW {{ $row->rw_wp }}
                </td>
                <td>{{ $row->kd_blok }}</td>
                <td>{{ date('Y') }}</td>
                <td>{{ $row->status_op }}</td>
                <td>{{ $row->kd_znt }}</td>
                <td>{{ $row->nir }}</td>
                <td>{{ $row->kd_kls_tanah }}</td>
                <td>{{ $row->njop_bumi }}</td>
                <td>{{ $row->njop_bng }}</td>
                <td>{{ $row->total_luas_bumi }}</td>
                <td>{{ $row->total_luas_bng }}</td>
                <td>{{ $row->pbb_terhutang_sppt }}</td>

            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
