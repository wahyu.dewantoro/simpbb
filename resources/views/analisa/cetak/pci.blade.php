<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN PERSONAL CONFIDENTIAL INFORMATION (PCI)</h3>
        </th>
    </tr>
</table>
@php
    $subject = current($result);
@endphp
{{-- <table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> NIK</td>
        <td>:{{$subject['subjek_pajak_id']}}</td>
    </tr>
    <tr>
        <td colspan="2">NAMA WP</td>
        <td>:{{$subject['nm_wp']}}</td>
    </tr>
    <tr>
        <td colspan="2"> ALAMAT</td>
        <td colspan="8">:{{$subject['jalan_wp']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{$subject['kota_wp']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{$subject['kelurahan_wp']}}</td>
        
    </tr>
</table> --}}
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>NOP</th>
            <th>Nama WP</th>
            <th>Alamat WP</th>
            <th>Alamat Objek</th>
            {{-- <th>Kelurahan</th>
            <th>Kecamatan</th> --}}
            <th>NJOP T.</th>
            <th>NJOP B.</th>
            <th>PBB</th>
            <th>Status</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @foreach ($result as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ $row['nop'] }}</td>
                <td>{{ $row['nm_wp'] }}</td>
                <td>{{ $row['jalan_wp'] }} RT {{ $row['rt_wp'] }} RW {{ $row['rw_wp'] }}, {{ $row['kelurahan_wp'] }},
                    {{ $row['kota_wp'] }}</td>
                <td>{{ $row['jalan_op'] }} ,{{ $row['nm_kelurahan'] ?? '-' }}, {{ $row['nm_kecamatan'] ?? '-' }}</td>
                <td>{{ $row['njop_bumi_sppt'] ?? '0' }}</td>
                <td>{{ $row['njop_bng_sppt'] ?? '0' }}</td>
                <td>{{ $row['pbb'] ?? '0' }}</td>
                <td>{{ $row['status'] ?? '' }}</td>
                <td>{{ $row['keterangan'] ?? '' }}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
