<html>

<head>
    <title>Laporan Pengaktifan NOP</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }
        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
            text-transform: uppercase;
        }
        table {
            /* font-weight: bold; */
            font-family: 'Courier New', Courier, monospace;
            font-size: 11px;
        }
        .text-center{
            text-align: center;
        }
        thead{
            display:table-header-group;
        }
        tfoot{
            display: table-row-group;
        }
        tr{
            page-break-inside: avoid;
        }
    </style>
</head>
<body>
<table width="100%" border="0" class="text-center">
    <tr>
        <td>
            <h1>Laporan Pengaktifan NOP</h1>
            <hr>
        </td>
    </tr>
</table>
<br>
{{--<div style="width:75%; float:left">
    Nama User : {{$result['nama']}}
</div>
<div style="width:25%; float:left">
    Tanggal : {{$result['tanggal']}}
</div>--}}
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0" >
    <thead class="text-center">
        <tr>
            <th class='number'>No</th>
            <th>NIK</th>
            <th>Nama</th>
            <th>NOP</th>
            <th>Alamat</th>
            <th>Luas Bumi</th>
            <th>Luas Bangunan</th>
            <th>Tanggal</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
       @foreach($result['data'] as $item)
            <tr>
                <td>{{$no}}</td>
                <td>{{$item->subjek_pajak_id}}</td>
                <td>{{$item->nm_wp}}</td>
                <td>{{$item->kd_propinsi}}.
                    {{$item->kd_dati2}}.
                    {{$item->kd_kecamatan}}.
                    {{$item->kd_kelurahan}}.
                    {{$item->kd_blok}}-
                    {{$item->no_urut}}.
                    {{$item->kd_jns_op}}
                </td>
                <td>{{$item->jalan_op}}</td>
                
                <td>{{$item->total_luas_bumi}}</td>
                <td>{{$item->total_luas_bng}}</td>
                <td>{{tglIndo($item->unflag_time)}}</td>
                <td>{{$item->keterangan}}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
</body>
</html>
