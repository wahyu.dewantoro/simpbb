<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN PEMBAYARAN TERBLOKIR</h3>
        </th>
    </tr>
</table>
@php
    $subject=current($result);
@endphp
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{$keckel['nm_kecamatan']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{$keckel['nm_kelurahan']}}</td>
        
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>Subjek</th>
            <th>NOP</th>
            <th>Kelurahan</th>
            <th>Kecamatan</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @foreach ($result as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td >{{ $row->nm_wp_sppt }}</td>
                <td >{{ $row->kd_propinsi.".".
                        $row->kd_dati2.".".
                        $row->kd_kecamatan.".".
                        $row->kd_kelurahan.".".
                        $row->kd_blok."-".
                        $row->no_urut.".".
                        $row->kd_jns_op
                }}</td>
                <td >{{ $row->nm_kelurahan??'-' }}</td>
                <td >{{ $row->nm_kecamatan??'-' }}</td>
                <td >{{ $row->unflag_desc??'' }}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
