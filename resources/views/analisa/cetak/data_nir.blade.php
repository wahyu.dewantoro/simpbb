<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN REKAP DATA NIR</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{$keckel['nm_kecamatan']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{$keckel['nm_kelurahan']}}</td>
        
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>KECAMATAN</th>
            <th>KELURAHAN</th>
            <th>KD ZNT</th>
            <th>TAHUN</th>
            <th>NO DOKUMEN</th>
            <th>NIR</th>
            <TH>HKPD</TH>
        </tr>
    </thead>
    <tbody>
        @php  $no=1;  @endphp
        @foreach ($result as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td >{{ $row['nm_kecamatan'] }}</td>
                <td >{{ $row['nm_kelurahan'] }}</td>
                <td >{{ $row['kd_znt'] }}</td>
                <td >{{ $row['thn_nir_znt'] }}</td>
                <td >{{ $row['no_dokumen'] }}</td>
                <td >{{ $row['nir'] }}</td>
                <td >{{ $row['nilai_hkpd'] }}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>

