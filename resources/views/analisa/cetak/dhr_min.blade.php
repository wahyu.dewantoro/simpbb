<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    <table width="100%" class="">
        <tr>
            <th width="70px"><img width="60px" height="70px" src="{{ public_path('kabmalang.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
        </tr>
    </table>
    <hr>
    <table style="font-size: 16px !important" width="100%">
        <tr>
            <th colspan="10">
                <p style="text-align: center;margin:0 ">LAPORAN REKAP DAFTAR HIMPUNAN KETETAPAN</p>
                <h4 style="text-align: center; margin: 0 ">Ds/kel {{ $nm_kelurahan }} ,Kecamatan {{ $nm_kecamatan }}
                </h4>
                <p style="text-align: center;margin: 0 ">TAHUN PAJAK {{ $tahun }}</p>
            </th>
        </tr>
    </table>

    <table class="border" width="100%">
        <thead>
            <tr>
                <th class='number'>No</th>
                <th>Kelurahan</th>
                <th style="text-align: center;">Blok</th>
                <th style="text-align: center;">ZNT</th>
                <th style="text-align: center;">HKPD/NJKP (%)</th>
                <th style="text-align: center;">Jumlah NOP</th>
                <th style="text-align: right;">Luas Bumi</th>
                <th style="text-align: right;">NIR</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($data as $row)
                <tr>
                    <td align="center">{{ $no }}</td>
                    <td> {{ $row->nm_kelurahan }} </td>
                    <td style="text-align: center;">{{ $row->kd_blok }}</td>
                    <td style="text-align: center;">{{ $row->kd_znt }}</td>
                    <td style="text-align: center;">{{ $row->hkpd }}</td>
                    <td style="text-align: center;">{{ angka($row->totalnop) }}</td>
                    <td style="text-align: right;">{{ angka($row->luas_bumi_sppt) }}</td>
                    <td style="text-align: right;">{{ angka($row->nir) }}</td>
                </tr>
                @php $no++; @endphp
            @endforeach
        </tbody>
    </table>
    <p></p>
    <p></p>
    <table width="100%" border="0">
        <tbody>
            <tr>
                <td width="30%"></td>
                <td></td>
                <td width="40%" align="center">
                    <p style="margin: 0">
                        {{-- {{ $qrcode }} --}}
                        MALANG, {{ tglIndo(date('Y')) }}<br>
                        {{ $kaban->is_plt != '' ? 'PLT' : '' }} {{ $kaban->jabatan }}<br>
                        @if ($qrcode != '')
                            {!! $qrcode !!}<br>
                        @else
                            <br>
                            <br>
                            <br>
                            <br>
                        @endif
                        <b>{{ $kaban->nama }}</b><br>NIP : {{ $kaban->nip }}
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
