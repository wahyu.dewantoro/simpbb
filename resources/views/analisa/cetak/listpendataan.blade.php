<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN DAFTAR PENDATAAN</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Jenis</td>
        <td>:{{$search['jenis']}}</td>
    </tr>
    <tr>
        <td colspan="2">Status</td>
        <td>:{{$search['status']}}</td>
    </tr>
    <tr>
        <td colspan="2">Pegawai</td>
        <td colspan="8">:{{$search['pegawai']}}</td>
    </tr>
    <tr> 
        <td colspan="2">Tanggal</td>
        <td>:{{$search['tanggal']}}</td>
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>Jenis</th>
            <th>Nomor</th>
            <th>Tanggal</th>
            <th>Status</th>
            <th>Objek</th>
            <th>Pegawai</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no=1; 
            $listKode=['1'=>'Disetujui','2'=>'Verifikasi PETA','0'=>'Tidak Disetujui'];
        @endphp
        @foreach ($result as $row)
            @php 
                $status="Belum di verifikasi";
            @endphp
            @if(in_array($row['verifikasi_kode'],array_keys($listKode)))
                $status=$listKode[$row['verifikasi_kode']];
            @endif
            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ $row['nama_pendataan'] }}</td>
                <td>{{ $row['nomor_batch'] }}</td>
                <td>{{ tglIndo($row['tanggal']) }}</td>
                <td>{{ $status }}</td>
                <td>{{ $row['objek'] }}</td>
                <td>{{ $row['pegawai_pendata'] }}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
