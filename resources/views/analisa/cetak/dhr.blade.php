<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">LAPORAN DHR (PCI)</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{$keckel['nm_kecamatan']}}</td>
    </tr>
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{$keckel['nm_kelurahan']}}</td>
        
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th class='number'>No</th>
            <th>NOP</th>
            <th>Nama</th>
            <th>Blok</th>
            <th>Tahun</th>
            <th>Status</th>
            <th>Kelas</th>
            <th>NIR</th>
            <th>Luas Tanah</th>
            <th>PBB Tanah</th>
            <th>ZNT</th>
            <th>Luas Bangunan</th>
            <th>NJOP Bangunan</th>
            <th>PBB Bangungan</th>
            <th>Jenis Tanah</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no=1; 
            $listJenis=['1'=>'TANAH + BANGUNAN',
                            '2' => 'KAVLING',
                            '3' => 'TANAH KOSONG',
                            '4' => 'FASILITAS UMUM',
                            '5' => 'NON AKTIF'];
        @endphp
        @foreach ($result as $row)
            @php $result='-'; @endphp
            @php $status='Aktif'; @endphp
            @if(in_array($row['jns_bumi'],array_keys($listJenis))){
                @php $result=$listJenis[$row['jns_bumi']]; @endphp
            @endif
            @if($row['jns_transaksi_op']=='3'||in_array($row['jns_bumi'],['4','5']))
                @php $status='non Aktif'; @endphp
            @endif
            <tr>
                <td align="center">{{ $no }}</td>
                <td >
                    {{$row['kd_propinsi']}}.
                    {{$row['kd_dati2']}}.
                    {{$row['kd_kecamatan']}}.
                    {{$row['kd_kelurahan']}}.
                    {{$row['kd_blok']}}-
                    {{$row['no_urut']}}.
                    {{$row['kd_jns_op']}}
                </td>
                <td >{{ $row['nm_wp_sppt'] }}</td>
                <td >{{ $row['kd_blok'] }}</td>
                <td >{{ $row['thn_pajak_sppt'] }}</td>
                <td >{{ $status}}</td>
                <td >{{ $row['kd_kls_tanah'] }}</td>
                <td >{{ $row['nir'] }}</td>
                <td >{{ $row['luas_bumi_sppt'] }}</td>
                <td >{{ $row['pbb_tanah'] }}</td>
                <td >{{ $row['kd_znt'] }}</td>
                <td >{{ $row['luas_bng_sppt'] }}</td>
                <td >{{ $row['njop_bng_sppt'] }}</td>
                <td >{{ $row['pbb_bangunan'] }}</td>
                <td >{{$result}}</td>
            </tr>
            @php $no++; @endphp
        @endforeach
    </tbody>
</table>
