@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Daftar NOP Billing Kolektif</b></h3>
                        </div>
                        <div class="card-body">
                            <form action="#daftar_nop/search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <select name="kecamatan"  required class="form-control select"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                                                @if($kecamatan->count()>1) <option value="">-- Kecamatan --</option> @endif
                                                @foreach ($kecamatan as $rowkec)
                                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <select name="kelurahan"  required class="form-control select"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                                                @if(count($kelurahan)>1) <option value="">-- Kelurahan --</option> @endif
                                                @foreach ($kelurahan as $rowkel)
                                                    <option @if (request()->get('kd_kelurahan') == $rowkel->kd_kelurahan)  selected @endif value="{{ $rowkel->kd_kelurahan }}">
                                                        {{ $rowkel->nm_kelurahan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <select name="tahun_pajak"  class="form-control select">
                                                @foreach ($tahun_pajak as $rowtahun)
                                                    <option @if (request()->get('tahun_pajak') == $rowtahun)  selected @endif value="{{ $rowtahun }}">
                                                        {{ $rowtahun}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <select name="status"  class="form-control select">
                                                <option value='-'>Semua</option>
                                                <option value='1'>Lunas</option>
                                                <option value='0'>Belum Lunas</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group">
                                            <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group">
                                            <button type='button' class='btn btn-block btn-primary form-option'> <i class="fas fa-bars"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 input-option">
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Kode Billing Kolektif </span>
                                                    </div>
                                                    <input type="text" class="form-control"  placeholder="Kode Billing" name="kode_billing">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">NOP </span>
                                                    </div>
                                                    <input type="text" class="form-control" name="nop" placeholder="NOP">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Nama Wajib Pajak </span>
                                                    </div>
                                                    <input type="text" class="form-control"  name="nm_wp" placeholder="Nama WP">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-9">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Tanggal Pembuatan Kobil </span>
                                                    </div>
                                                    <input id="daterangepicker" type="text" class="form-control"  name="tgl_kobil" placeholder="Tanggal Kobil">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">PBB Mulai dari 0 - </span>
                                                    </div>
                                                    <input type="text" value='500.000' class="form-control hasdatarange numericMask numeric" name="pbb" placeholder="PBB">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-body no-padding">
                                <table id="main-table" class="table table-bordered table-striped" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Billing</th>
                                            <th>NOP</th>
                                            <th>NAMA WP</th>
                                            <th>PBB</th>
                                            <th>Kecamatan</th>
                                            <th>Kelurahan</th>
                                            <th>Tgl Kobil</th>
                                            <th>Tgl Bayar</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody> </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Terjadi kesalahan kesalahan jaringan.";
            let txtNull="Data Masih Kosong/ Data yang di cari tidak ditemukan.";
            let isnull="<tr class='null'><td colspan='10'>"+txtNull+"</td></tr>";
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                language: { "emptyTable": txtNull },
                lengthMenu: [[20], [20]],
                ordering: false,
                columns: [
                    { data: 'no'},
                    { data: 'kobil'},
                    { data: 'nop'},
                    { data: 'nama_wp'},
                    { data: 'pbb'},
                    { data: 'nama_kecamatan'},
                    { data: 'nama_kelurahan'},
                    { data: 'created_at'},
                    { data: 'tgl_bayar'},
                    { data: 'kd_status',class:'text-center'},
                    { data: 'aksi',class:'text-center'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    str.push(key.name+"="+key.value);
                });
                return str.join("&");
            }

            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-change").split("#"),
                    target=$("[name='"+e.attr("data-target-name")+"']");
                target.appendData(uri[1],{kd_kecamatan:e.val()},{});
            });
            $(document).on("click", ".form-option", function (evt) {
                $(".input-option").toggleClass("active");
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian daftar dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    if(!response.status){
                        return Swal.fire({ icon: 'warning', html: response.msg});
                    }
                    Swal.fire({ icon: 'success', html: response.msg});
                });
            });

        });
    </script>
@endsection
