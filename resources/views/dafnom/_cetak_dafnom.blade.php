<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Pdf Dafnom</title>
    <style>
        .table-borderd th, .table-borderd td {
            border-width: 1px 0px 0px 1px; 
            border-style:solid;
            padding: 4px;
        }
        .table-borderd>tbody>tr:last-child>td{
            border-bottom-width:1px;
        }
    </style>
</head>
<body>
    @php
        $kobil='';
    @endphp
    @if($data_billing)
        @php
            $kobil=$data_billing->kd_propinsi.".".
                            $data_billing->kd_dati2.".".
                            $data_billing->kd_kecamatan.".".
                            $data_billing->kd_kelurahan.".".
                            $data_billing->kd_blok."-".
                            $data_billing->no_urut.".".
                            $data_billing->kd_jns_op;
        @endphp
    @endif
    <div class="print-page">
        <div class='container text-center'>
            <div class="header" style="text-align:center">
                <h3>DAFTAR NOMINATIVE BILLING COLLECTIVE</h3>
            </div>
        </div>
        <hr>
        <div class="container border-bottom" width="100%" >
            <table class="table" width="100%" style="font-size: 9pt;">
                <tr>
                    <td>KODE BILLING KOLEKTIF</td>
                    <td>:</td>
                    <td>{{$kobil}}</td>
                </tr>
                <tr>
                    <td>TAHUN PAJAK BILLING KOLEKTIF</td>
                    <td>:</td>
                    <td>{{$data_billing->tahun_pajak}}</td>
                </tr>
                <tr>
                    <td>KETERANGAN</td>
                    <td>:</td>
                    <td>{{$data_billing->nama_wp}}</td>
                </tr>
                <tr>
                    <td>JUMLAH NOP</td>
                    <td>:</td>
                    <td>{{count($billing_kolektif)}}</td>
                </tr>
                <tr>
                    <td>TOTAL POKOK</td>
                    <td>:</td>
                    <td>Rp. {{number_format($data_billing->pbb,0,',','.')}}</td>
                </tr>
                <tr>
                    <td>TOTAL DENDA</td>
                    <td>:</td>
                    <td>Rp. {{number_format($data_billing->denda,0,',','.')}}</td>
                </tr>
                <tr>
                    <td>TOTAL </td>
                    <td>:</td>
                    <td>Rp. {{number_format($data_billing->pbb+$data_billing->denda,0,',','.')}}</td>
                </tr>
                <tr>
                    <td>KECAMATAN</td>
                    <td>:</td>
                    <td>{{$data_billing->nama_kecamatan}}</td>
                </tr>
                <tr>
                    <td>KELURAHAN</td>
                    <td>:</td>
                    <td>{{$data_billing->nama_kelurahan}}</td>
                </tr>
                <tr>
                    <td>TANGGAL REKAM</td>
                    <td>:</td>
                    <td>{{$data_billing->created_at}}</td>
                </tr>
                <tr>
                    <td>EXPIRED</td>
                    <td>:</td>
                    <td>{{$data_billing->expired_at}}</td>
                </tr>
            </table>
            <br>
            <table class="table table-borderd" width="100%" style="font-size: 9pt; border-collapse: collapse" >
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NOP</th>
                        <th>Nama WP</th>
                        <th>Tahun Pajak</th>
                        <th>Pokok</th>
                        <th>Denda</th>
                        <th style="border-right-width:1px">Total</th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $no=1;
                    $totalpokok=0;
                    $totaldenda=0;
                    $totalAll=0;
                @endphp
                @foreach($billing_kolektif as $item)
                    @php
                    $nop_list=$item->kd_propinsi.".".
                            $item->kd_dati2.".".
                            $item->kd_kecamatan.".".
                            $item->kd_kelurahan.".".
                            $item->kd_blok."-".
                            $item->no_urut.".".
                            $item->kd_jns_op;
                    @endphp
                    <tr>
                        <td align="center">{{$no}}</td>
                        <td>{{$nop_list}}</td>
                        <td>{{$item->nm_wp}}</td>
                        <td align="center">{{$item->tahun_pajak}}</td>
                        <td align="right">{{number_format($item->pokok,0,',','.')}} </td>
                        <td align="right">{{number_format($item->denda,0,',','.')}}</td>
                        <td align="right" style="border-right-width:1px">{{number_format($item->pokok+$item->denda,0,',','.')}} </td>
                    </tr>
                    @php
                        $totalpokok=$totalpokok+$item->pokok;
                        $totaldenda=$totaldenda+$item->denda;
                        $no++;
                    @endphp
                @endforeach
                <tr>
                    <td colspan="4">Total</td>
                    <td align="right">{{number_format($totalpokok,0,',','.')}} </td>
                    <td align="right">{{number_format($totaldenda,0,',','.')}}</td>
                    <td align="right" style="border-right-width:1px">{{number_format($totalpokok+$totaldenda,0,',','.')}} </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br>
        <div class="container" width="100%" >
            <!-- footer -->
        </div>
    </div>
    <br>
</body>
</html>

