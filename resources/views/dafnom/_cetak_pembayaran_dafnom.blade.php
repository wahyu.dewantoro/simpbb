<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Pdf Pembayaran Dafnom</title>
</head>
<body>
    @php
    $no=1;
    $page=1;
    $total_page=(count($billing_kolektif)>1)?ceil(count($billing_kolektif)/2):1;
    if($noPage){
        $page=$noPage->page;
        $total_page=$noPage->total;
    }
    @endphp
    @foreach($billing_kolektif as $item)
        @php
            $kobil=$data_billing->kd_propinsi.".".
                                $data_billing->kd_dati2.".".
                                $data_billing->kd_kecamatan.".".
                                $data_billing->kd_kelurahan.".".
                                $data_billing->kd_blok."-".
                                $data_billing->no_urut.".".
                                $data_billing->kd_jns_op;
            $nop=$item->kd_propinsi.".".
                                $item->kd_dati2.".".
                                $item->kd_kecamatan.".".
                                $item->kd_kelurahan.".".
                                $item->kd_blok."-".
                                $item->no_urut.".".
                                $item->kd_jns_op;
            $nopF=$item->kd_propinsi.$item->kd_dati2.$item->kd_kecamatan.$item->kd_kelurahan.$item->kd_blok.$item->no_urut.$item->kd_jns_op;
            $pengesahan=$item->pengesahan??$data_billing->pengesahan;
            $qrcode=$uri.'?ntpd=' . $pengesahan. '&kobil=' . $data_billing->kobil . '&nop=' . $nopF;
            $qrcode=base64_encode(QrCode::format('svg')->generate($qrcode));
        @endphp
        <div class="print-page">
            @if($no%2==0)
                <div style="border-bottom:3px #333 dashed"></div>
                <br>
            @else
                <div style="text-align: left"><span style="font-size: 8pt; "><b>[Halaman {{$page}} dari {{$total_page}}]</b></span></div>
                @php
                    $page++;
                @endphp
            @endif
            <div class='container text-center'>
                <table width="100%">
                    <tr>
                        <td width="20%"></td>
                        <td width="60%" align="center" style="font-size: 10pt;">
                            <div class="header">
                                <h3>SURAT SETORAN PAJAK DAERAH (SSPD)</h3>
                                <h3>PAJAK BUMI DAN BANGUNAN</h3>
                            </div>
                        </td>
                        <td width="20%" align='right'><img src="data:image/png;base64,{!!$qrcode!!}" class="qrcode" width="80px"></td>
                    </tr>
                </table>
            </div>
            <hr>
            <div class="container border-bottom" width="100%" >
                <table class="table" width="100%" style="font-size: 9pt;">
                    <tr>
                        <td>NTPD</td>
                        <td>:</td>
                        <td>{{$pengesahan}}</td>
                    </tr>
                    <tr>
                        <td>BANK BAYAR</td>
                        <td>:</td>
                        <td>{{$item->nama_bank}}</td>
                    </tr>
                    <tr>
                        <td>NOP</td>
                        <td>:</td>
                        <td>{{$nop}}</td>
                    </tr>
                    <tr>
                        <td>TAHUN PAJAK</td>
                        <td>:</td>
                        <td>{{$item->tahun_pajak}}</td>
                    </tr>
                    <tr>
                        <td>NAMA WP</td>
                        <td>:</td>
                        <td>{{$item->nm_wp}}</td>
                    </tr>
                    <tr>
                        <td>ALAMAT</td>
                        <td>:</td>
                        <td>{{$item->jln_wp_sppt}}</td>
                    <tr>
                        <td>KELURAHAN</td>
                        <td>:</td>
                        <td>{{$data_billing->nama_kelurahan}}</td>
                    </tr>
                    <tr>
                        <td>KECAMATAN</td>
                        <td>:</td>
                        <td>{{$data_billing->nama_kecamatan}}</td>
                    </tr>
                    <tr>
                        <td>TGL PEMBAYARAN</td>
                        <td>:</td>
                        <td>{{tglIndo($data_billing->tgl_bayar)}}</td>
                    </tr>
                    <tr>
                        <td>JUMLAH</td>
                        <td>:</td>
                        <td>{{number_format($item->pokok,0,',','.')}}</td>
                    </tr>
                    <tr>
                        <td>DENDA</td>
                        <td>:</td>
                        <td>{{number_format($item->denda,0,',','.')}}</td>
                    </tr>
                    <tr>
                        <td>TOTAL</td>
                        <td>:</td>
                        <td>{{number_format($item->total,0,',','.')}}</td>
                    </tr>
                    <tr>
                        <td>TERBILANG</td>
                        <td>:</td>
                        <td>{{terbilang_cap($item->total)}} Rupiah</td>
                    </tr>
                    <tr>
                        <td colspan="3"><hr></td>
                    </tr>
                    <tr>
                        <td>KODE BILLING KOLEKTIF</td>
                        <td>:</td>
                        <td>{{$kobil}}</td>
                    </tr>
                    <tr>
                        <td>KETERANGAN</td>
                        <td>:</td>
                        <td>{{$data_billing->nama_wp}}</td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="container" width="100%" >
                 <span style="font-size: 9pt;"><u><b>CATATAN :</b></u></span>
                <ol width="100%" style="font-size: 9pt; padding:0px 0px 0px 20px;margin:0px">
                    <li>Dokumen ini merupakan pembayaran yang SAH PBB P2 Bapenda Kab. Malang</li>
                    <li>Untuk pengecekan, silahkan scan QRCODE diatas</li>
                </ol>
            </div>
        </div>
        <br>
        @php
            $no++;
        @endphp
    @endforeach
</body>
</html>

