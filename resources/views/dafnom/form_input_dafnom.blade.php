<form action="#dafnom/cek_pembuatan_dafnom" id="form-cek">
    @csrf   
    <div class="row">
        <div class="form-group col-md-1">
            <div class="input-group">
                <select name="tahun_pajak"  class="form-control select">
                    @foreach ($tahun_pajak as $rowtahun)
                        <option @if (request()->get('tahun_pajak') == $rowtahun)  selected @endif value="{{ $rowtahun }}">
                            {{ $rowtahun}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-2">
            <div class="input-group">
                <select name="kecamatan"  required class="form-control select"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                    @if($kecamatan->count()>1) <option value="">-- Kecamatan --</option> @endif
                    @foreach ($kecamatan as $rowkec)
                        <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                            {{ $rowkec->nm_kecamatan }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-3">
            <div class="input-group">
                <select name="kelurahan"  required class="form-control select"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                    @if(count($kelurahan)>1) <option value="">-- Kelurahan --</option> @endif
                    @foreach ($kelurahan as $rowkel)
                        <option @if (request()->get('kd_kelurahan') == $rowkel->kd_kelurahan)  selected @endif value="{{ $rowkel->kd_kelurahan }}">
                            {{ $rowkel->nm_kelurahan }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group col-md-1">
            <div class="input-group">
                <input type="text" class="form-control numeric" placeholder="Blok" name="blok" >
            </div>
        </div>
        <div class="form-group col-md-2">
            <div class="input-group">
                <input type="text" class="form-control numeric numericMask" required placeholder="Rencana Bayar" name="rencana_bayar">
            </div>
        </div>
        <div class="form-group col-md-1">
            <div class="input-group">
                <select name="option"  class="form-control">
                    <option value="1">Total</option>
                    <option value="2">Spesifik</option>
                </select>
            </div>
        </div>
        <div class="form-group col-lg-2">
            <div class="input-group">
                <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-6" data-append='body_preview_nop_dafnom'>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title lh-35">List NOP</h3>
                <div class="card-tools no-margin">
                    <a href="javascript:;" class='btn btn-primary' data-clone=".table-clone-list" data-target=".table-clone-submit">
                    <i class="far fa-plus-square"></i> Tambah</a>
                </div>
                <div class="card-widget bold">
                    <ul>
                        <li>Jumlah NOP   : <span name='selected_jumlah_nop'>0</span></li>
                        <li>Akumulasi   : Rp. <span name='selected_akumulasi'>0</span></li>
                    </ul>
                </div>
            </div>
            <div class="card-body body-scroll">
                <table id="example1" class="table table-bordered table-striped table-with-checkbox table-change  table-select table-clone-list table-counter">
                    <thead>
                        <tr>
                            <th class="cursor-pointer checkall"><span><input type="checkbox" name="check" class="form-check-input check-header"> Semua</span></th>
                            <th>NOP</th>
                            <th>Wajib Pajak</th>
                            <th>Nominal</th>
                            <th>Denda</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="null">
                            <td colspan="6" class="dataTables_empty text-center"> Data Masih Kosong</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="card-widget bold">
                    <ul>
                        <li>Total NOP   : <span name='jumlah_nop'>0</span></li>
                        <li>Total PBB   : Rp. <span name='akumulasi'>0</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" data-append='body_preview_nop_dafnom_false'>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title lh-35">Daftar Nominative</h3>
                <div class="card-tools no-margin">
                    <a href="javascript:;" class='btn btn-primary' data-clone=".table-clone-submit" data-target=".table-clone-list">
                    <i class="far fa-trash-alt"></i> Hapus</a>
                </div>
                <div class="card-widget bold">
                    <ul>
                        <li>Jumlah NOP   : <span name='selected_jumlah_nop'>0</span></li>
                        <li>Akumulasi   : Rp. <span name='selected_akumulasi'>0</span></li>
                    </ul>
                </div>
            </div>
            <div class="card-body body-scroll">
                <form action="#" id="form_input">
                    @csrf  
                    <table id="example1" class="table table-bordered table-striped table-change table-select table-clone-submit table-counter no-margin">
                        <thead>
                            <tr >
                            <th class="cursor-pointer checkall"><span><input type="checkbox" name="check" class="form-check-input check-header"> Semua</span></th>
                                <th>NOP</th>
                                <th>Wajib Pajak</th>
                                <th>Nominal</th>
                                <th>Denda</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="null">
                                <td colspan="6" class="dataTables_empty text-center"> Data Masih Kosong</td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="card-footer">
                <div class="card-widget bold">
                    <ul>
                        <li>Total NOP   : <span name='jumlah_nop'>0</span></li>
                        <li>Total PBB   : Rp. <span name='akumulasi'>0</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="callout callout-warning col-sm-12">
    <ol>
        <li>Maksimal data yg di proses dalam satu kali proses <b>'pembuatan dafnom'</b> tidak lebih dari 500 data</li>
        <li>Form inputan <b>'blok'</b> bisa tidak diisi untuk menampilkan seluruh blok yang ada.</li>
        <li>Data NOP yang sudah melakukan pembayaran baik kolektif maupun pribadi tidak akan muncul dalam daftar.</li>
    </ol>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-md-6"><a href="javascript:;" class="btn btn-block btn-default" data-reset="#form-cek">Batal</a></div>
        <div class="col-md-6"><a href="javascript:;" class="btn btn-block btn-primary"data-uri='#dafnom/store' data-form='#form_input'>Simpan</a></div>
    </div>
</div>