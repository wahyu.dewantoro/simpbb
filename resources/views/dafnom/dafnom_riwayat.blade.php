@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Daftar Riwayat Import Billing Kolektif</b></h3>
                        </div>
                        <div class="card-body  col-rel">
                            <div class="col-md-7">
                                <table id="main-table" class="table table-bordered table-striped table-counter" data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama File</th>
                                            <th>Tanggal Import Excel</th>
                                            <th>Size</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody>@php echo $setdata['data'] @endphp</tbody>
                                </table>
                            </div>
                            <div class="col-md-5 card card-outline card-right no-radius no-margin">
                                Detail File
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Terjadi kesalahan kesalahan jaringan.";
            let txtNull="File Masih Kosong/ File yang di cari tidak ditemukan.";
            let isnull="<tr class='null'><td colspan='10'>"+txtNull+"</td></tr>";
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            let datatable=$("#main-table").DataTable({
                language: { "emptyTable": txtNull },
                order: [[ 2, "desc" ]],
                columns: [
                    { data: 'no'},
                    { data: 'nama_file'},
                    { data: 'modified'},
                    { data: 'size'},
                    { data: 'option',class:'text-center'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });

        });
    </script>
@endsection
