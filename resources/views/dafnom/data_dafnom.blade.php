@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin" data-card='main'>
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Data Dafnom</b></h3>
                        </div>
                        <div class="card-body">
                            <form action="#data_dafnom/search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <div class="input-group">
                                            <select name="kecamatan"  required class="form-control select"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                                                @if($kecamatan->count()>1) <option value="">-- Kecamatan --</option> @endif
                                                @foreach ($kecamatan as $rowkec)
                                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <select name="kelurahan"  required class="form-control select"  data-placeholder="Kelurahan (Pilih Kecamatan dahulu).">
                                                @if(count($kelurahan)>1) <option value="">-- Kelurahan --</option> @endif
                                                @foreach ($kelurahan as $rowkel)
                                                    <option @if (request()->get('kd_kelurahan') == $rowkel->kd_kelurahan)  selected @endif value="{{ $rowkel->kd_kelurahan }}">
                                                        {{ $rowkel->nm_kelurahan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <select name="tahun_pajak"  class="form-control select">
                                                @foreach ($tahun_pajak as $rowtahun)
                                                    <option @if (request()->get('tahun_pajak') == $rowtahun)  selected @endif value="{{ $rowtahun }}">
                                                        {{ $rowtahun}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <select name="kd_jns_op"  class="form-control select">
                                                <option value="1">Kolektif</option>
                                                <option value="2">Individu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group">
                                            <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group col-lg-1">
                                        <div class="input-group">
                                            <button type='button' class='btn btn-block btn-primary '> <i class="fas fa-bars"></i></button>
                                        </div>
                                    </div> -->
                                </div>
                            </form>
                            <div class="callout callout-warning col-sm-12" data-append="callout">
                                <ul class=" m-0">
                                    <li>Total NOP Kolektif : <b>[ <span data-name="nopkolektif">0</span> ] NOP</b> </li>
                                    <li>Total NOP lunas (Non Kolektif) : <b>[ <span data-name="nopnonkolektif">0</span> ] NOP</b> </li>
                                </ul>
                            </div>
                            <div class="card-body no-padding">
                                <table id="main-table" class="table table-bordered table-striped " data-append="response-data">
                                    <thead>
                                        <tr>
                                            <th class='number'>No</th>
                                            <th>Kode Billing</th>
                                            <th>Kecamatan</th>
                                            <th>Kelurahan</th>
                                            <th>Keterangan</th>
                                            <th>PBB</th>
                                            <th>Tgl Buat</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr class="null"><td colspan="9" > Lakukan Pencarian data untuk menampilkan data dafnom.</td></tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            
                    @include('dafnom/detail_dafnom')
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Terjadi kesalahan kesalahan jaringan.";
            let txtNull='Lakukan Pencarian data untuk menampilkan data dafnom.';
            let isnull="<tr class='null'><td colspan='9'>"+txtNull+"</td></tr>";
            async function asyncData(uri,value){
                let getData;
                try {
                    getData=await $.ajax({
                        type: "get",
                        url: uri,
                        data: (value)?{"_token": "{{ csrf_token() }}",'id':value}:{},
                        dataType: "json",
                    });
                    return getData;
                }catch(error){
                    return error;
                }
            };
            $.fn.dataTable.ext.errMode = 'none'??'throw';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let datatable=$("#main-table").DataTable({
                language: { "emptyTable": txtNull },
                lengthMenu: [[20], [20]],
                ordering: false,
                columns: [
                    { data: null,
                        searchable: false,
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        class: 'text-center'
                    },
                    { data: 'kobil'},
                    { data: 'nama_kecamatan'},
                    { data: 'nama_kelurahan'},
                    { data: 'nama_wp'},
                    { data: 'pbb'},
                    { data: 'created_at'},
                    { data: 'kd_status',class:'text-center'},
                    { data: 'aksi',class:'text-center'}
                ]
            });
            datatable.on('error.dt',(e, settings, techNote, message)=>{
                //message??
                Swal.fire({ icon: 'warning', html: defaultError});
            });
            const toString = function(obj) {
                let str=[];
                obj.map((key)=>{
                    str.push(key.name+"="+key.value);
                });
                return str.join("&");
            }
            $(document).on("click", "[data-card='remove']", function (evt) {
                let side=$("[data-card='side']");
                let main =$("[data-card='main']");
                $("[data-remote].active").removeClass("active");
                side.hide();
                main.show();
            });
            
            $(document).on("click", "[data-detail]", function (evt) {
                let e=$(this),
                    id=e.attr('data-detail'),
                    side=$("[data-card='side']"),
                    main =$("[data-card='main']"),
                    target=$("[data-remote='"+id+"']");
                if(target.length){
                    target.addClass('active');
                    main.hide();
                    side.show();
                    return;
                };
                Swal.fire({
                    title: 'Detail data dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let clone=$("[data-remote='clone']").clone();
                clone.attr('data-remote',id);
                clone.addClass('active');
                asyncData("data_dafnom/detail",id).then((response) => {
                    if(!response.status){
                        clone.remove();
                        return Swal.fire({ icon: 'error', html: response.msg}); 
                    }
                    $.when.apply($, $.each(response.data, (x, y) => {
                        clone.find('[data-name="'+x+'"]').html(y);
                    })).then(()=>side.append(clone)).then(() => {
                        side.find('[data-remote="'+id+'"] table.table-response').DataTable();
                        swal.close();
                        main.hide();
                        side.show();
                    });
                }).catch((error)=>{
                    clone.remove();
                    Swal.fire({ icon: 'error', text: defaultError});
                });
            });
            
            $(document).on("click", "[data-delete]", function (evt) {
                let msg="Yakin Menghapus data Billing kolektif ini?",
                    e=$(this),
                    value=e.attr("data-delete");
                Swal.fire({ 
                    icon: 'warning',
                    text: msg
                }).then((willdelete)=>{ 
                    if(willdelete.isConfirmed){
                        Swal.fire({
                            title: 'Proses hapus data.',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        $.ajax({
                            type: "post",
                            url: "data_dafnom/delete",
                            data: {"_token": "{{ csrf_token() }}",id:value},
                            dataType: "json",
                            success: function (response) {
                                e.closest('tr').remove();
                                $("[data-remote='"+value+"']").remove();
                                Swal.fire({ icon: 'success', html: response.msg});
                            },
                            error: function (x, y) {
                                Swal.fire({ icon: 'error', text: x.responseJSON.message??defaultError});
                            }
                        });
                    };
                });
            });
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-change").split("#"),
                    target=$("[name='"+e.attr("data-target-name")+"']");
                target.appendData(uri[1],{kd_kecamatan:e.val()},{obj:'SEMUA KELURAHAN',key:'-'});
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian daftar dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                let response=datatable.ajax.url(uri[1]+"?"+toString(e.serializeArray())).load((response)=>{
                    if(response.extra){
                        $.each(response.extra,(x,y)=>{
                            $("[data-name='"+x+"']").html(y);
                        });
                    }
                    if(!response.status){
                        console.log('zzz')
                        return Swal.fire({ icon: 'warning', html: response.msg});
                    }
                    swal.close();
                    //Swal.fire({ icon: 'success', html: response.msg});
                });
            });

        });
    </script>
@endsection
