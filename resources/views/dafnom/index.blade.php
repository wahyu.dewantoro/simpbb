@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3"><b>Pembuatan Dafnom</b></h3>
                            <ul class="nav nav-pills ml-auto p-2">
                                @if($userkelurahan)
                                    <li class="nav-item"><a class="nav-link " href="#tab_1" data-toggle="tab"> <i class="fas fa-bars"></i> Form </a></li>
                                @endif
                                <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab"> <i class="fas fa-book"></i> Import Excel</a></li>
                            </ul>
                         </div>
                        <div class="card-body">
                            <div class="tab-content">
                                @if($userkelurahan)
                                    <div class="tab-pane " id="tab_1">
                                        @include('dafnom/form_input_dafnom')
                                    </div>
                                @endif
                                <div class="tab-pane active" id="tab_2">
                                @include('dafnom/import_excel')
                                </div>
                            </div>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script>
        $(document).ready(function() {
            let defaultError="Proses tidak berhasil, coba ulangi proses.";
            let defaultErrorPreview="Terjadi kesalahan validasi data, pastikan data sudah sesuai dengan format.";
            let isnull="<tr class='null'><td colspan='7' class='dataTables_empty'>Data Masih Kosong</td></tr>";
            let limit=500;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.fn.evenSelect = function(exts) {
                let e=$(this),
                    countSelect=e.find("tr.selected"),
                    sumPbb=0;
                countSelect.map((index, e) =>{
                    sumPbb=sumPbb+parseInt($(e).find("[data-name='total']>input").val());
                });
                appendSelected(e,countSelect.length,sumPbb.format(0, 3, '.', ''));
            }
            $(document).on("click", "[data-clone]", function (evt) {
                let e=$(this),
                    source=$(e.attr("data-clone")),
                    target=$(e.attr("data-target"));
                if(!source.find(".selected").length){
                    return Swal.fire({icon: 'warning',title: 'Oops...',text: 'Data belum dipilih.'});
                };
                let selected=source.find(".selected");
                if(selected.length>limit){
                    return Swal.fire({icon: 'warning',title: 'Oops...',text: 'Jumlah Data melebihi batas. Max = '+limit});
                };
                let clone=selected.clone();
                selected.remove();
                target.find("tbody tr.null").remove();
                target.find("tbody").append(clone);
                target.find(".selected").removeClass("selected");
                appendSelected(source,0,0);
                appendSelected(target,0,0);
                if(!source.find("tbody tr").length){
                    source.find("tbody").html(isnull);
                };
                calculateDataTable(source);
                calculateDataTable(target);
            });
            $(document).on("click", ".checkall", function (evt) {
                let e=$(this),checkBoxes=e.find("input[type='checkbox']"),checked=(!checkBoxes.prop("checked"));
                checkBoxes.prop("checked", checked);
                if(checked){
                    e.closest(".table").find("tbody>tr:not(.null)").addClass("selected");
                }else{
                    e.closest(".table").find("tbody>tr:not(.null)").removeClass("selected");
                }
                if(e.closest(".table").hasClass("table-change")){
                    e.closest("table").evenSelect();
                };
            });
            $(document).on("click", ".table-select tbody tr:not(.null)", function (evt) {
                let e=$(this);
                e.toggleClass("selected");
                if(e.closest("table").hasClass("table-change")){
                    e.closest("table").evenSelect();
                };
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian data nop dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                pageAppend('body_preview_nop_dafnom',isnull,0,0,false,0,0);
                appendSelected($(".table-clone-list"),0,0);
                appendSelected($(".table-clone-submit"),0,0);
                calculateDataTable($(".table-clone-list"));
                calculateDataTable($(".table-clone-submit"));
                e.ajaxSubmit({
                    type: "post",
                    url: uri[1] ,
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            return Swal.fire({ icon: 'warning', html: response.msg});
                        }
                        swal.close();
                        pageAppend('body_preview_nop_dafnom',
                                response.body_preview_nop_dafnom,
                                response.jumlah_nop,
                                response.akumulasi,
                                false,
                                response.jumlah_nop_false,
                                response.akumulasi_false);
                    },
                    error: function (x, y) {
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultError});
                    }
                });
            });
            $(document).on("click", "[data-reset]", function (evt) {
                pageAppend('body_preview_nop_dafnom',isnull,0,0,isnull,0,0);
                appendSelected($(".table-clone-list"),0,0);
                appendSelected($(".table-clone-submit"),0,0);
                calculateDataTable($(".table-clone-list"));
                calculateDataTable($(".table-clone-submit"));
            });
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-change").split("#"),
                    target=$("[name='"+e.attr("data-target-name")+"']");
                target.appendData(uri[1],{kd_kecamatan:e.val()});
            });
            let pageAppend=function(Appendroot,tbody,jumlah_nop,akumulasi,tbody_false,jumlah_nop_false,akumulasi_false){
                $("[data-append='"+Appendroot+"'] tbody").html(tbody);
                $("[data-append='"+Appendroot+"'] [name='jumlah_nop']").html(jumlah_nop);
                $("[data-append='"+Appendroot+"'] [name='akumulasi']").html(akumulasi);
                if(tbody_false){
                    $("[data-append='"+Appendroot+"_false'] tbody").html(tbody_false);
                };
                $("[data-append='"+Appendroot+"_false'] [name='jumlah_nop_false']").html(jumlah_nop_false);
                $("[data-append='"+Appendroot+"_false'] [name='akumulasi_false']").html(akumulasi_false);
            };
            let calculateDataTable=function(e){
                let countSelect=e.find("tr:not(.null)>td[data-name='total']"),
                    sumPbb=0,
                    rElement=e.closest("[data-append]");
                countSelect.map((index, e) =>{
                    sumPbb=sumPbb+parseInt($(e).find("input").val());
                });
                rElement.find("[name='jumlah_nop']").html(countSelect.length);
                rElement.find("[name='akumulasi']").html(sumPbb.format(0, 3, '.', ''));
            }
            let appendSelected=function(e,count,currency){
                e.closest("[data-append]").find("[name='selected_jumlah_nop']").html(count);
                e.closest("[data-append]").find("[name='selected_akumulasi']").html(currency);
            }
            $(document).on("click", "[data-uri]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-uri").split("#"),
                    form=$(e.attr("data-form")),
                    length=form.find("[data-name='nop']").length;
                if(!length){
                    return Swal.fire({icon: 'warning',title: 'Oops...',text: 'Data Masih Kosong'});
                }
                Swal.fire({ 
                    icon: 'warning',
                    text: 'Pastikan data Kobil sudah benar (termasuk Nama WP, Nominal, Denda dan Total).\n Simpan Daftar Kobil?',
                    showCancelButton: true,
                    confirmButtonText: "Simpan",
                    cancelButtonText: "Batal",
                }).then((willsend)=>{ 
                    if(willsend.isConfirmed){
                        Swal.fire({
                            title: 'Proses pembuatan data billing kolektif',
                            html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                            showConfirmButton: false,
                            allowOutsideClick: false,
                        });
                        form.ajaxSubmit({
                            type: "post",
                            url: uri[1] ,
                            dataType: "json",
                            success: function (response) {
                                if(!response.status){
                                    return Swal.fire({ icon: 'error', text: response.msg});
                                }
                                Swal.fire({ icon: 'success', text: response.msg}).then((reload)=>{ 
                                    let hasReset=e.closest('.tab-pane').find('[data-reset]');
                                    if(hasReset.length){
                                        return hasReset.trigger("click");
                                    };
                                    pageAppend('body_preview_dafnom',isnull,0,0,isnull,0,0);
                                });
                            },
                            error: function (x, y) {
                                //x.responseJSON.message??
                                Swal.fire({ icon: 'error', text: defaultError});
                            }
                        });
                    };
                });
            });
            $(document).on("change", "[name='import_excel']", function (evt) {
                if(!$(this).hasExtension(['.xls', '.xlsx', '.csv'])){
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Format file tidak sesuai!!!'
                    });
                };
                let e=$(this),
                    form=e.closest("form"),
                    uri=form.attr("action").split("#"),
                    name=e.val()??'Import Excel';
                let namefile=evt.target.files[0].name;
                $("label[for='"+e.attr("name")+"']").html(namefile);
                pageAppend('body_preview_dafnom',isnull,0,0,isnull,0,0);
                Swal.fire({
                    title: 'Proses menampilkan dan validasi data dari Excel',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                form.ajaxSubmit({
                    type: "post",
                    url: uri[1] ,
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                           return Swal.fire({ icon: 'error', text: response.msg});
                        }
                        swal.close();
                        pageAppend('body_preview_dafnom',
                                response.body_preview_dafnom,
                                response.jumlah_nop,
                                response.akumulasi,
                                response.body_preview_dafnom_false,
                                response.jumlah_nop_false,
                                response.akumulasi_false);
                    },
                    error: function (x, y) {
                        // console.log(x.responseJSON.message)
                        //x.responseJSON.message??
                        Swal.fire({ icon: 'error', text: defaultErrorPreview});
                    }
                });
            });
        });
    </script>
@endsection
