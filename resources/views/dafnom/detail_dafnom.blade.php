<div class="card card-primary card-outline card-tabs no-radius no-margin card-reponse" data-card='side'>
  <div class="card-header p-0">
    <h3 class="card-title p-3"><b>Detail Daftar Nominative</b></h3>
    <div class="card-tools p-1">
      <button type="button" class="btn btn-primary" data-card="remove"><i class="fas fa-times"></i> Tutup</button>     
    </div>
  </div>
  <div class="card-body clone-hide" data-remote='clone'>
    <div class="callout callout-warning col-sm-12 no-margin">
      <table class='table table-bordered table-striped bold-text no-margin'>
        <thead>
          <tr>
            <th>Kode Billing</th>
            <th>Tgl Billing Kolektif</th>
            <th>Kecamatan</th>
            <th>Kelurahan</th>
            <th>Jumlah NOP</th>
            <th>Jumlah PBB</th>
            <th>Jumlah Denda</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td data-name='kode_billing'></td>
            <td data-name='tgl_kobil'></td>
            <td data-name='nama_kecamatan'></td>
            <td data-name='nama_kelurahan'></td>
            <td data-name="jumlah_nop"></td>
            <td>Rp. <span data-name="total_pbb"></span></td>
            <td>Rp. <span data-name="total_denda"></span></td>
            <td data-name="status"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="card-header text-center">
      <div class="btn-group" data-name='option-tools'></div>
    </div>
    <div class="card-body no-padding">
      <table class="table table-bordered table-striped table-counter table-response" data-append="response-data-detail">
        <thead>
            <tr>
                <th>No</th>
                <th>NOP</th>
                <th>NAMA WP</th>
                <th>PBB</th>
                <th>Denda</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody data-name='append-data'></tbody>
      </table>
    </div> 
  </div>
  <!-- auto addd -->
</div>