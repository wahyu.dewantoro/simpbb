<form action="#dafnom/preview" id="form-import">
    @csrf   
    <div class="row">
        <div class="form-group col-sm-8">
            <div class="input-group">
                <div class="custome-file">
                    <input type="file" class="custom-file-input" name='import_excel'>
                    <label class="custom-file-label" for="import_excel">Import Excel</label>  
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <a href="{{URL::to('/public/Format_Import_Daftar_Nominative.xlsx')}}" class="btn btn-success btn-block">Download Format Excel</a>
        </div>
        <div class="callout callout-warning col-sm-12">
            <ol>
                <li>Pastikan file excel yg di import berjenis *.xlsx</li>
                <li>Format file Excel harus sama dengan format yang telah di sediakan <b>[NO , NOP , NAMA WP, ALAMAT , TAHUN PAJAK, PBB TERHUTANG]</b></li>
                <li>Pastikan format data merupakan data asli (Bukan mengambil dari data lain sperti Vlookup,dst)</li>
                <li>Maksimal data yg di proses dalam satu kali proses import Excel tidak lebih dari 500 data</li>
            </ol>
        </div>
        
    </div>
    
</form>
<div class="row">
    <div class="col-md-12" data-append='body_preview_dafnom'>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title lh-35">List NOP</h3>
                <div class="card-widget bold">
                    <ul>
                        <li>Jumlah NOP   : <span name='jumlah_nop'>0</span> </li>
                        <li>Akumulasi   : Rp. <span name='akumulasi'>0</span> </li>
                    </ul>
                </div>
            </div>
            <div class="card-body body-scroll">
                <form action="#" id="form_import">
                    @csrf  
                    <table id="example1" class="table table-bordered table-striped no-margin">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NOP</th>
                                <th>Nama WP</th>
                                <th>Tahun Pajak</th>
                                <th>Nominal</th>
                                <th>Denda</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class='null'>
                                <td colspan="7" class="dataTables_empty"> Data Masih Kosong</td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12" data-append='body_preview_dafnom_false'>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title lh-35">Data tidak valid untuk masuk Dafnom</h3>
                <div class="card-widget bold">
                    <ul>
                        <li>Jumlah NOP   : <span name='jumlah_nop_false'>0</span></li>
                        <li>Akumulasi   : Rp. <span name='akumulasi_false'>0</span></li>
                    </ul>
                </div>
            </div>
            <div class="card-body body-scroll">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NOP</th>
                            <th>Nama WP</th>
                            <th>Alamat</th>
                            <th>Tahun Pajak</th>
                            <th>Nominal</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="null">
                            <td colspan="7" class="dataTables_empty"> Data Masih Kosong</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="callout callout-warning col-sm-12">
        <ol>
            <li>Pastikan data Kobil sudah benar (termasuk <b>NOP</b>, Nama WP, Nominal, Denda dan Total)</li>
        </ol>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="col-md-12"><a href="javascript:;" class="btn btn-block btn-primary"  data-uri='#dafnom/store' data-form='#form_import'>Simpan Data Billing Kolektif</a></div>
    </div>
</div>