@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css') }}/stylesheet.css">
@endsection
@section('content')
    <section class="content content-cloud">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-tabs no-radius no-margin">
                        <div class="card-header d-flex p-0">
                            <h3 class="card-title p-3">Realisasi</h3>
                        </div>
                        <div class="card-body">
                            <form action="#data_dafnom/search" id="form-cek">
                                @csrf   
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <div class="input-group">
                                            <select name="kecamatan"  required class="form-control select"  data-placeholder="Pilih Kecamatan" data-change="#desa" data-target-name='kelurahan'>
                                                <option value="">-- Kecamatan --</option>
                                                @foreach ($kecamatan as $rowkec)
                                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group">
                                            <select name="kelurahan"  required class="form-control select"  data-placeholder="Pilih Kelurahan">
                                                <option value="">-- Kelurahan --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <div class="input-group">
                                            <select name="tahun_pajak"  class="form-control select">
                                                @foreach ($tahun_pajak as $rowtahun)
                                                    <option @if (request()->get('tahun_pajak') == $rowtahun)  selected @endif value="{{ $rowtahun }}">
                                                        {{ $rowtahun}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-2">
                                        <div class="input-group">
                                            <button type='submit' class='btn btn-block btn-primary '> <i class="fas fa-search"></i> Cari </button>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <div class="input-group">
                                            <button type='button' class='btn btn-block btn-primary '> <i class="fas fa-bars"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <table id="dataTable" class="table table-bordered table-striped table-counter" data-append="response-data">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Billing</th>
                                        <th>Billing kolektif</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>Tgl Buat</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="null"><td colspan="8" > Lakukan Pencarian data untuk menampilkan data dafnom.</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            let defaultError="Terjadi kesalahan kesalahan jaringan.";
            let isnull="<tr class='null'><td colspan='8'>Data Masih Kosong</td></tr>";
            $(document).on("change", "[data-change]", function (evt) {
                let e=$(this),
                    uri=e.attr("data-change").split("#"),
                    target=$("[name='"+e.attr("data-target-name")+"']");
                target.appendData(uri[1],{kd_kecamatan:e.val()},{obj:'SEMUA KELURAHAN',key:'-'});
            });
            $(document).on("submit", "#form-cek", function (evt) {
                evt.preventDefault();
                let e=$(this),
                    uri=e.attr("action").split("#");
                Swal.fire({
                    title: 'Pencarian daftar dafnom.',
                    html:'<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                e.ajaxSubmit({
                    type: "get",
                    url: uri[1] ,
                    dataType: "json",
                    success: function (response) {
                        if(!response.status){
                            return Swal.fire({ icon: 'warning', html: response.msg});
                        }
                        Swal.fire({ icon: 'success', html: response.msg});
                        $("[data-append='response-data'] tbody").html(response.data);
                    },
                    error: function (x, y) {
                        Swal.fire({ icon: 'error', text: x.responseJSON.message??defaultError});
                    }
                });
            });

        });
    </script>
@endsection
