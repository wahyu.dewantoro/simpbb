<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo') }}/favicon.ico" />
    <title>SIMPBB P2</title>

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="SIPANJI : SIMPBB">
    <meta itemprop="description"
        content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta itemprop="image" content="{{ asset('sipanji.png') }}">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://esppt.id/simpbb">
    <meta property="og:type" content="website">
    <meta property="og:title" content="SIPANJI : SIMPBB">
    <meta property="og:description"
        content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta property="og:image" content="{{ asset('sipanji.png') }}">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="SIPANJI : SIMPBB">
    <meta name="twitter:description"
        content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta name="twitter:image" content="{{ asset('sipanji.png') }}">



    <link data-rh="true" rel="search" type="application/opensearchdescription+xml" title="SIPANJI" href="/osd.xml" />
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" {{-- href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> --}} <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('lte') }}/dist/css/adminlte.min.css">
    <style>
        .hero-image {
            background-image: url("background.png");
            background-color: #cccccc;
            height: auto;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }
    </style>
</head>

<body class="hold-transition register-page hero-image">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-sm-10">
                <div class="register-boxx">
                    <div class="login-logo">
                        <a href="#"><b>{{ config('app.name', 'Laravel') }}</b></a>
                    </div>

                    <!-- /.login-logo -->
                    <div class="card card-outline card-info">
                        <div class="card-header">Verifikasi Akun</div>
                        <div class="card-body">
                            @if (session('pesan'))
                                <?= session('pesan') ?>
                            @endif
                            <div class="row">

                                <div class="col-sm-6">
                                    <table class="table table-sm table-borderless">
                                        <tbody>
                                            <tr>
                                                <td width="150px">nama</td>
                                                <td>:</td>
                                                <td>{{ $user->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td width="150px">nik</td>
                                                <td>:</td>
                                                <td>{{ $user->nik }}</td>
                                            </tr>
                                            <tr>
                                                <td width="150px">username</td>
                                                <td>:</td>
                                                <td>{{ $user->username }}</td>
                                            </tr>
                                            <tr>
                                                <td width="150px">telepon</td>
                                                <td>:</td>
                                                <td>{{ $user->telepon }}</td>
                                            </tr>
                                            <tr>
                                                <td width="150px">email</td>
                                                <td>:</td>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-sm-6">

                                    <form method="POST" id="form" action="{{ url('register-verifikasi') }}">
                                        @csrf
                                        @method('POST')
                                        <input type="hidden" name="id" value="{{ encrypt($user->id) }}">
                                        <div class="form-group row">
                                            <label for="kode_otp"class="col-sm-6 col-form-label text-right">Kode OTP
                                                <sup class="text-danger">*</sup></label>
                                            <div class="col-sm-6">
                                                <input type="text" maxlength="6" id="kode_otp" autofocus
                                                    class="form-control angka" name="kode_otp">
                                                <div id="ulang"><span class='text-small text-info'>Tidak menerima
                                                        kode OTP ? <a href='#' id='resend'>[Kirim
                                                            Ulang]</a></span></div>
                                                <div class="countdown"></div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{ asset('lte') }}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('lte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('lte') }}/dist/js/adminlte.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#kode_otp').val('')
            var interval = null;




            function hitungMUndur() {
                var timer2 = "2:00";
                var cc = '120';
                interval = setInterval(function() {

                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var minutes = parseInt(timer[0], 10);
                    var seconds = parseInt(timer[1], 10);

                    --cc;
                    --seconds;

                    minutes = (seconds < 0) ? --minutes : minutes;
                    if (minutes < 0) clearInterval(interval);
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    //minutes = (minutes < 10) ?  minutes : minutes;
                    if (cc > 0) {
                        $('.countdown').html(minutes + ':' + seconds);
                        $("#kode_otp").prop('disabled', false);
                        $("#ulang").hide()
                    } else {
                        $("#kode_otp").prop('disabled', true);
                        $('.countdown').html("");
                        $("#ulang").show()
                        clearInterval(interval)
                    }
                    timer2 = minutes + ':' + seconds;
                    console.log(cc);
                }, 1000);
            }

            @if (session('pesan'))
            
            @else
                hitungMUndur();
            @endif
            $('#kode_otp').on('keyup change', function() {
                var otp = $(this).val()
                // otp=otp
                var panjang = otp.length;
                if (panjang == 6) {
                    $('#form').trigger('submit');
                }


            })

            $('#resend').on("click", function(e) {
                // alert("oke")
                clearInterval(interval)
                $("#ulang").hide()
                e.preventDefault()
                $.ajax({
                    method: "GET",
                    url: "{{ url('resend-otp', encrypt($user->id)) }}",
                }).done(function(msg) {
                    console.log(msg)
                    if (msg.st == 'true') {
                        hitungMUndur()
                    }
                });
            })


            $('.angka').keypress(function(event) {
                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault(); //stop character from entering input
                }

            });
        });
    </script>
</body>

</html>
