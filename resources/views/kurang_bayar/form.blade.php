@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Fom Kurang Bayar</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-md-6">
            <div class="card card-primary card-tabs">
                <div class="card-body p-2">
                    {{-- menampilkan error validasi --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form action="{{ $data['action'] }}" method="post">
                        @csrf
                        @method($data['method'])
                        <div class="col-12">

                            @php
                            // dd($data['ln']);
                            $snop='';
                            if(isset($data['kb'])){
                            $snop=
                            $data['kb']->kd_propinsi.'.'.
                            $data['kb']->kd_dati2.'.'.
                            $data['kb']->kd_kecamatan.'.'.
                            $data['kb']->kd_kelurahan.'.'.
                            $data['kb']->kd_blok.'.'.
                            $data['kb']->no_urut.'.'.
                            $data['kb']->kd_jns_op.'.'.
                            $data['kb']->thn_pajak_sppt;
                            }
                            @endphp
                            <div class="form-group row">
                                <label class="col-form-label col-md-4" for="">NOP</label>
                                <div class="col-md-8">
                                    <select name="nop" id="nop" class="form-control select">
                                        <option value=""></option>
                                        @foreach ($data['ln'] as $i=> $rn)
                                        @php
                                        $val=$rn->kd_propinsi.'.'.
                                        $rn->kd_dati2.'.'.
                                        $rn->kd_kecamatan.'.'.
                                        $rn->kd_kelurahan.'.'.
                                        $rn->kd_blok.'.'.
                                        $rn->no_urut.'.'.
                                        $rn->kd_jns_op.'.'.
                                        $rn->thn_pajak_sppt;
                                        @endphp
                                        <option @if($snop==$val) selected @endif value="{{ 
                                                $val                                               
                                                }}">{{
                                            $rn->kd_propinsi.'.'.
                                            $rn->kd_dati2.'.'.
                                            $rn->kd_kecamatan.'.'.
                                            $rn->kd_kelurahan.'.'.
                                            $rn->kd_blok.'.'.
                                            $rn->no_urut.'.'.
                                            $rn->kd_jns_op.' - '.
                                            $rn->thn_pajak_sppt                                               
                                            }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-4" for="nm_wp">Wajib Pajak</label>
                                <div class="col-md-8">
                                    <input type="text" name="nm_wp" id="nm_wp" value="" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-4" for="nm_kelurahan">Kelurahan</label>
                                <div class="col-md-8">
                                    <input type="text" name="nm_kelurahan" id="nm_kelurahan" value="" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-4" for="nm_kecamatan">Kecamatan</label>
                                <div class="col-md-8">
                                    <input type="text" name="nm_kecamatan" id="nm_kecamatan" value="" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4" for="nilai">Nilai Kurang Bayar</label>
                                <div class="col-md-8">
                                    <input type="text" name="nilai" id="nilai" class="form-control rupiah" required value="{{ angka($data['kb']->nilai??'0') }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="float-right">
                            <button class="btn btn-flat btn-sm btn-success" type="submit" id="simpanpenelitian"><i class="far fa-save"></i>
                                Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        //  $("#nop").on("change", function() {

        //     // var nop = $(this).val();
        //     // var convert = formatnop(nop);
        //     // $(this).val(convert);
        // });



        $(".select").select2({
            placeholder: "Pilih NOP"
            , allowClear: true
        });


        $(".rupiah").on("keyup", function() {
            var angka = $(this).val();
            var convert = formatRupiah(angka);
            $(this).val(convert);
        });

        function kosongKan() {
            $('#kd_propinsi').val('')
            $('#kd_dati2').val('')
            $('#kd_kecamatan').val('')
            $('#kd_kelurahan').val('')
            $('#kd_blok').val('')
            $('#no_urut').val('')
            $('#kd_jns_op').val('')
            $('#nm_wp').val('')
            $('#nm_kelurahan').val('')
            $('#nm_kecamatan').val('')
            $('#nilai').val('')
        }


        $("#nop").on("change", function() {
            var obj = $('#nop').val().replace(/[^\d]/g, "");

            console.log(obj)
            if (obj.length >= 18) {
                openloading()
                var kec = obj.substr(4, 3);
                var kel = obj.substr(7, 3);
                var blok = obj.substr(10, 3);
                var no_urut = obj.substr(13, 4);
                var jns_op = obj.substr(17, 1);
                var thn_pajak_sppt = obj.substr(18, 4);
                $.ajax({
                    url: "{{ url('kurang-bayar/create') }}"
                    , type: "get"
                    , data: {
                        kd_propinsi: '35'
                        , kd_dati2: '07'
                        , kd_kecamatan: kec
                        , kd_kelurahan: kel
                        , kd_blok: blok
                        , no_urut: no_urut
                        , kd_jns_op: jns_op
                        , thn_pajak_sppt: thn_pajak_sppt
                    }
                    , success: function(res) {
                        closeloading()
                        res = $.parseJSON(res)
                        if (res.bayar > 0) {

                            $('#kd_propinsi').val(res.kd_propinsi);
                            $('#kd_dati2').val(res.kd_dati2);
                            $('#kd_kecamatan').val(res.kd_kecamatan);
                            $('#kd_kelurahan').val(res.kd_kelurahan);
                            $('#kd_blok').val(res.kd_blok);
                            $('#no_urut').val(res.no_urut);
                            $('#kd_jns_op').val(res.kd_jns_op);
                            $('#nm_wp').val(res.nm_wp);
                            $('#nm_kelurahan').val(res.nm_kelurahan);
                            $('#nm_kecamatan').val(res.nm_kecamatan);
                            $('#nilai').val(res.pbb - res.bayar)
                            $('#nilai').trigger('keyup')
                        } else {
                            kosongKan()
                            Swal.fire({
                                icon: 'error'
                                , title: 'WARNING'
                                , text: "NOP belum melakukan pembayaran"
                                , allowOutsideClick: false
                                , allowEscapeKey: false
                                // , showConfirmButton: true
                            })
                        }
                    }
                    , error: function(e) {
                        closeloading()
                        kosongKan()
                    }
                })

            } else {
                kosongKan()
            }
        });




        // if ($("#nop").val() != '') {
        //     $("#nop").trigger('keyup')
        // }
        $('#nop').trigger('change');

    })

</script>
@endsection
