@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Kurang Bayar</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    <a href="{{ route('kurang-bayar.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-plus-square"></i> Tambah
                    </a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-2">
                        <table class="table table-bordered table-sm">
                            <thead class="bg-info">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">NOP</th>
                                    <th class="text-center">Wajib Pajak</th>
                                    <th class="text-center">Tahun Pajak</th>
                                    <th class="text-center">Kurang Bayar</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                // $i = ($data->page - 1) * $data->per_page + 1;
                                $i=1;
                                @endphp
                                @foreach ($data as $key=>$item)
                                <tr>
                                    <td class="text-center">{{ $data->firstItem() + $key }}</td>
                                    <td>
                                        {{ $item->kd_propinsi.'.'.$item->kd_dati2.'.'.$item->kd_kecamatan.'.'.$item->kd_kelurahan.'.'.$item->kd_blok.'-'.$item->no_urut.'.'.$item->kd_jns_op }}
                                    </td>
                                    <td>{{ $item->nm_wp_sppt }}</td>
                                    <td>{{ $item->thn_pajak_sppt }}</td>
                                    <td class="text-right">{{ angka($item->nilai) }}</td>

                                    <td class="text-center">
                                        @php
                                            $id=$item->kd_propinsi.$item->kd_dati2.$item->kd_kecamatan.$item->kd_kelurahan.$item->kd_blok.$item->no_urut.$item->kd_jns_op.$item->thn_pajak_sppt;
                                        @endphp
                                        <a href="{{ route('kurang-bayar.edit', $id) }}"><i class="fas fa-edit text-info"></i> </a>
                                        <a href="{{ route('kurang-bayar.destroy', $id) }}" onclick="
                                                                                    var result = confirm('Are you sure you want to delete this record?');
                                                                                    if(result){
                                                                                        event.preventDefault();
                                                                                        document.getElementById('delete-form-{{ $id }}').submit();
                                                                                    }"><i class="fas fa-trash-alt text-danger"></i>
                                        </a>
                                        <form method="POST" id="delete-form-{{ $id }}" action="{{ route('kurang-bayar.destroy', [$id]) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @php
                                $i++;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(function() {

    });

</script>
@endsection
