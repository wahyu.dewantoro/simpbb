@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Permohonan</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a href="{{ url('mandiri/layanan/create') }}" class="btn btn-sm btn-info"><i
                                class="fas fa-folder-plus"></i> Tambah</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <form action="{{ url()->current() }}">
                            <div class="input-group">
                                <input class="form-control py-2 border-right-0 border" type="search"
                                    value="{{ request()->get('search') }}" id="search" name='search'
                                    placeholder="Pencarian">
                                <span class="input-group-append">
                                    <div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div>
                                </span>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-hover text-sm table-striped-columns table-sm" id="table">
                        <thead>
                            <tr>

                                <th>No</th>
                                <th>Nopel</th>
                                <th>Tanggal</th>
                                <th>Layanan</th>
                                <th>Pemohon</th>
                                <th>NOP</th>

                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $item)
                                <tr>
                                    <td>{{ $index + $data->firstItem() }}</td>
                                    <td>{{ $item->nomor_layanan }}<br>
                                        <small class="text-info">{{ $item->status_permohonan }}</small>
                                    </td>
                                    <td>{{ $item->tanggal_layanan }}</td>
                                    <td>{{ $item->jenis_layanan_nama }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>
                                        {{ $item->kd_propinsi }}.{{ $item->kd_dati2 }}.{{ $item->kd_kecamatan }}.{{ $item->kd_kelurahan }}.{{ $item->kd_blok }}-{{ $item->no_urut }}.{{ $item->kd_jns_op }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('mandiri.layanan.show', $item->nomor_layanan) }}"><i
                                                class="fas fa-binoculars text-info"></i></a>

                                        @if ($item->status_permohonan == 'Belum diproses' && Auth()->user()->hasRole('Wajib Pajak') == false)
                                            <a class="hapus"
                                                data-url="{{ route('mandiri.layanan.destroy', $item->nomor_layanan) }}"
                                                data-id="{{ $item->nomor_layanan }}" href="#"><i
                                                    class="text-danger fas fa-trash"></i></a>
                                        @endif
                                        @if ($item->status_permohonan == 'Belum diproses')
                                        {{-- <small>Untuk </small> --}}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer p-1 ">
                    <div class="row">
                        <div class="col-6">
                            Total : {{ $data->total() }}
                        </div>
                        <div class="col-6">
                            {!! $data->links() !!}
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#table').on('click', '.hapus', function(e) {
                e.preventDefault()
                let id = $(this).data('id');
                let url_ = $(this).data('url');
                let CSRF_TOKEN = $("meta[name='csrf-token']").attr("content");
                Swal.fire({
                    title: 'Apakah anda Yakin?',
                    text: "ingin menghapus data ini!",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'TIDAK',
                    confirmButtonText: 'YA, HAPUS!'
                }).then(function(e) {
                    if (e.value === true) {
                        $.ajax({
                            type: 'DELETE',
                            url: url_,
                            data: {
                                _token: CSRF_TOKEN
                            },
                            dataType: 'JSON',
                            success: function(results) {
                                window.location.href = "{{ url()->current() }}";
                            }
                        });
                    }
                })
            })
        })
    </script>
@endsection
