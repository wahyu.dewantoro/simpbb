@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Permohonan</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-sm btn-info" href="{{ url('mandiri/layanan') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form action="{{ $data['action'] }}" method="POST" enctype="multipart/form-data">
                @method($data['method'])
                @csrf
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group row">
                                    <label for="jenis_layanan_id" class="col-sm-4 col-form-label">Layanan</label>
                                    <div class="col-sm-8">
                                        <select name="jenis_layanan_id" id="jenis_layanan_id"
                                            class="form-control form-control-sm @error('jenis_layanan_id')  is-invalid @enderror">
                                            <option value="">Pilih</option>
                                            @php
                                                $sl = old('jenis_layanan_id') ?? '';
                                            @endphp
                                            @foreach ($data['jenis_layanan'] as $i => $item)
                                                <option @if ($sl == $i) selected @endif
                                                    value="{{ $i }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('jenis_layanan_id')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="nop" class="col-sm-4 col-form-label">NOP</label>
                                    <div class="col-sm-8">
                                        <select
                                            class="form-control form-control-sm @error('nop') invalid-feedback @enderror"
                                            name="nop" id="nop">
                                            @php
                                                $snp = old('nop') ?? '';
                                            @endphp
                                            <option value="">Pilih</option>
                                            @foreach ($data['nop'] as $item)
                                                <option @if ($snp == $item) selected @endif
                                                    value="{{ $item }}">{{ formatnop($item) }}</option>
                                            @endforeach
                                        </select>

                                        {{-- <input type="text"
                                            value=""> --}}
                                        @error('nop')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_subjek_pajak_id">
                                    <label for="subjek_pajak_id" class="col-form-label col-sm-4">NIK / No KTP</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="subjek_pajak_id" name="subjek_pajak_id"
                                            class="form-control form-control-sm @error('subjek_pajak_id') is-invalid  @enderror">
                                        @error('subjek_pajak_id')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_nama_subjek">
                                    <label for="nama_subjek" class="col-form-label col-sm-4">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="nama_subjek" name="nama_subjek"
                                            class="form-control form-control-sm @error('nama_subjek') is-invalid @enderror">
                                        @error('nama_subjek')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_alamat">
                                    <label for="alamat" class="col-form-label col-sm-4">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control form-control-sm @error('alamat') is-invalid @enderror"
                                            name="alamat" id="alamat" value="{{ old('alamat') ?? '' }}">
                                        @error('alamat')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_kelurahan">
                                    <label for="kelurahan" class="col-form-label col-sm-4">Kelurahan</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control form-control-sm @error('kelurahan') is-invalid @enderror"
                                            name="kelurahan" id="kelurahan" value="{{ old('kelurahan') ?? '' }}">
                                        @error('kelurahan')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_kecamatan">
                                    <label for="kecamatan" class="col-form-label col-sm-4">Kecamatan</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control form-control-sm @error('kecamatan') is-invalid @enderror"
                                            name="kecamatan" id="kecamatan" value="{{ old('kecamatan') ?? '' }}">
                                        @error('kecamatan')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_dati2">
                                    <label for="dati2" class="col-form-label col-sm-4">Dati 2</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control form-control-sm @error('dati2') is-invalid @enderror"
                                            name="dati2" id="dati2" value="{{ old('dati2') ?? '' }}">
                                        @error('dati2')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_propinsi">
                                    <label for="propinsi" class="col-form-label col-sm-4">Propinsi</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control form-control-sm @error('propinsi') is-invalid @enderror"
                                            name="propinsi" id="propinsi" value="{{ old('propinsi') ?? '' }}">
                                        @error('propinsi')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row" id="label_nomor_telepon">
                                    <label for="nomor_telepon" class="col-form-label col-sm-4">No Wa</label>
                                    <div class="col-sm-8">
                                        <input type="text"
                                            class="form-control form-control-sm @error('nomor_telepon') is-invalid @enderror"
                                            name="nomor_telepon" id="nomor_telepon"
                                            value="{{ old('nomor_telepon') ?? Auth()->user()->telepon }}">
                                    </div>
                                </div>


                                <div class="form-group row" id="input_luas_bumi">
                                    <label for="luas_bumi" class="col-form-label col-sm-4">Luas Bumi</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="luas_bumi" id="luas_bumi"
                                            class="form-control form-control-sm">
                                        <input type="hidden" id="njop_bumi" name="njop_bumi">
                                    </div>
                                </div>
                                <div class="form-group row" id="input_luas_bng">
                                    <label for="luas_bng" class="col-form-label col-sm-4">Luas Bng</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="luas_bng" id="luas_bng"
                                            class="form-control form-control-sm">
                                        <input type="hidden" id="njop_bng" name="njop_bng">
                                    </div>
                                </div>
                                <div class="form-group row" id="label_pengurangan">
                                    <label for="jenis_pengurangan_id" class="col-sm-4 col-form-label">Pengurangan</label>
                                    <div class="col-sm-8">
                                        <select name="jenis_pengurangan_id" id="jenis_pengurangan_id"
                                            class="form-control form-control-sm">
                                            <option value="">Pilih</option>
                                            @foreach ($data['jenis_pengurangan'] as $i => $item)
                                                <option value="{{ $i }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" id="label_keterangan">
                                    <label for="keterangan" class="col-sm-4 col-form-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea name="keterangan" id="keterangan" rows="2" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="bukti_kepemilikan">Dok Kepemilikan <small
                                            class="text-danger">.pdf</small></label>
                                    <div class="col-sm-8">
                                        <input type="file"
                                            class="form-control form-control-sm   @error('ktp') is-invalid @enderror"
                                            accept="application/pdf" id="bukti_kepemilikan" name="bukti_kepemilikan"
                                            required>
                                        <small>(SHM ,C letter , AJB)</small>
                                        @error('bukti_kepemilikan')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="ktp">KTP <small
                                            class="text-danger">.pdf</small></label>
                                    <div class="col-sm-8">
                                        <input type="file"
                                            class="form-control form-control-sm  @error('ktp') is-invalid @enderror"
                                            accept="application/pdf" id="ktp" name="ktp" required>
                                        @error('ktp')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <table class="table table-sm text-sm table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td width='120px'>NOP</td>
                                                    <td with='1px'>:</td>
                                                    <td><span id="label_nop"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat Objek</td>
                                                    <td>:</td>
                                                    <td><span id="label_alamat_op"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Subjek Pajak</td>
                                                    <td>:</td>
                                                    <td><span id="label_subjek_pajak"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-6">
                                        <table class="table table-sm table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center text-bold">Objek</th>
                                                    <th class="text-center text-bold">Luas</th>
                                                    <th class="text-center text-bold">NJOP/M<sup>2</sup></th>
                                                    <th class="text-center text-bold">NPOP</th>
                                                </tr>
                                                <tr>
                                                    <td>Bumi</td>
                                                    <td class="tex-center"><span id="label_luas_bumi"></span></td>
                                                    <td class="text-right"><span id="label_njop_bumi"></span></td>
                                                    <td class="text-right"><span id="label_npop_bumi"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Bng</td>
                                                    <td class="tex-center"><span id="label_luas_bng"></span></td>
                                                    <td class="text-right"><span id="label_njop_bng"></span></td>
                                                    <td class="text-right"><span id="label_npop_bng"></span></td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button class="btn btn-sm btn-info"><i class="far fa-save"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('script')
    {{-- <script src="{{ asset('js') }}/wilayah.js"></script> --}}
    <script>
        $(document).ready(function() {
            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    // $('#cek').trigger('click');
                    e.preventDefault();
                    return false;
                }
            });

            $('#nop').on('change', function() {
                var nop = $("#nop").val();
                // var convert = formatnop(nop);
                // $("#nop").val(convert);

                // GET NUMBER ONLY 
                // var nop_asli = convert.replace(/[^\d]/g, "");
                if (nop != '') {
                    // var nop_asli = nop.replace(/[^\d]/g, "");
                    loadObjek(nop)
                } else {
                    kosongkan()
                }

            });

            function loadObjek(nop) {
                $.ajax({
                    url: "{{ url('api/data-objek-simple') }}",
                    method: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        kd_propinsi: nop.substr(0, 2),
                        kd_dati2: nop.substr(2, 2),
                        kd_kecamatan: nop.substr(4, 3),
                        kd_kelurahan: nop.substr(7, 3),
                        kd_blok: nop.substr(10, 3),
                        no_urut: nop.substr(13, 4),
                        kd_jns_op: nop.substr(17, 1)

                    },
                    success: function(res) {
                        console.log(res)
                        if (res.data != null) {
                            var label_alamat_op = res.data.jalan_op
                            if (res.data.blok_kav_no_op != null) {
                                label_alamat_op += " " + res.data.blok_kav_no_op
                            }

                            if (res.data.rt_op != null) {
                                label_alamat_op += " " + res.data.rt_op
                            }

                            if (res.data.rw_op != null) {
                                label_alamat_op += " " + res.data.rw_op
                            }



                            label_alamat_op += " " + res.data.nm_kelurahan +
                                " " + res.data.nm_kecamatan


                            var label_nop =
                                res.data.kd_propinsi + "." +
                                res.data.kd_dati2 + "." +
                                res.data.kd_kecamatan + "." +
                                res.data.kd_kelurahan + "." +
                                res.data.kd_blok + "-" +
                                res.data.no_urut + "." +
                                res.data.kd_jns_op

                            var spi = res.data.subjek_pajak_id
                            spi = spi.trim();
                            spi = spi.substr(-16, 16)
                            spi = spi.padStart(16, "0");

                            $('#subjek_pajak_id').val(spi)
                            $('#nama_subjek').val(res.data.nm_wp)
                            $('#alamat').val(res.data.jalan_op)
                            $('#kelurahan').val(res.data.nm_kelurahan)
                            $('#kecamatan').val(res.data.nm_kecamatan)
                            $('#dati2').val('MALANG')
                            $('#propinsi').val('JAWA TIMUR')

                            $('#label_nop').html(label_nop)
                            $('#label_alamat_op').html(label_alamat_op)
                            $('#label_subjek_pajak').html(res.data.nm_wp)
                            $('#label_luas_bumi').html(res.data.total_luas_bumi)
                            $('#label_njop_bumi').html(res.data.njop_bumi)
                            var npop_bumi = res.data.total_luas_bumi * res.data.njop_bumi
                            $('#label_npop_bumi').html(npop_bumi)
                            $('#njop_bumi').val(res.data.njop_bumi)
                            $('#label_luas_bng').html(res.data.total_luas_bng)
                            $('#label_njop_bng').html(res.data.njop_bng)
                            var npop_bng = res.data.total_luas_bng * res.data.njop_bng
                            $('#label_npop_bng').html(npop_bng)
                            $('#njop_bng').val(res.data.njop_bng)

                            $('#luas_bumi').val(res.data.total_luas_bumi)
                            $('#luas_bng').val(res.data.total_luas_bng)
                        }
                    }
                })
            }

            function kosongkan() {
                $('#label_nop').html('')
                $('#label_alamat_op').html('')
                $('#label_subjek_pajak').html('')
                $('#label_luas_bumi').html('')
                $('#label_njop_bumi').html('')
                $('#label_npop_bumi').html('')
                $('#label_luas_bng').html('')
                $('#label_njop_bng').html('')
                $('#label_npop_bng').html('')
                $('#luas_bumi').val('')
                $('#luas_bng').val('')

                $('#subjek_pajak_id').val('')
                $('#nama_subjek').val('')
                $('#alamat').val('')
                $('#kelurahan').val('')
                $('#kecamatan').val('')
                $('#dati2').val('')
                $('#propinsi').val('')
            }


            kosongkan()
            $('#jenis_layanan_id').on('change', function() {

                var jl = $(this).val()
                if (jl == 5) {
                    $('#label_pengurangan').show()
                    $('#label_keterangan').show()
                } else {
                    $('#label_pengurangan').hide()
                    $('#label_keterangan').hide()
                }

                if (jl == 8) {
                    $('#label_subjek_pajak_id').show()
                    $('#label_nama_subjek').show()
                    $('#label_alamat').show()
                    $('#label_kelurahan').show()
                    $('#label_kecamatan').show()
                    $('#label_dati2').show()
                    $('#label_propinsi').show()
                } else {
                    $('#label_subjek_pajak_id').hide()
                    $('#label_nama_subjek').hide()
                    $('#label_alamat').hide()
                    $('#label_kelurahan').hide()
                    $('#label_kecamatan').hide()
                    $('#label_dati2').hide()
                    $('#label_propinsi').hide()
                }

                if (jl == 3) {
                    $('#input_luas_bumi').show()
                    $('#input_luas_bng').show()
                } else {
                    $('#input_luas_bumi').hide()
                    $('#input_luas_bng').hide()
                }



            })

            $('#jenis_layanan_id').trigger('change')
            // $('#nop').trigger('keyup')

        })
    </script>
@endsection
