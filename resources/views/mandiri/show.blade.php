@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Permohonan</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a class="btn btn-sm btn-info" href="{{ url('mandiri/layanan') }}"><i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-outine">
                        <div class="card-header">
                            <h3 class="card-title text-bold">
                                <i class="fas fa-id-badge text-info"></i>      Pemohon
                            </h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless table-sm">

                                <body>
                                    <tr>
                                        <td width="120px">Nopel</td>
                                        <td width="1px">:</td>
                                        <td>{{ $layanan->nomor_layanan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>:</td>
                                        <td>{{ $layanan->tanggal_layanan }}</td>
                                    </tr>

                                    <tr>
                                        <td>Layanan</td>
                                        <td>:</td>
                                        <td>{{ $layanan->jenis_layanan_nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td>:</td>
                                        <td>{{ $layanan->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{ $layanan->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{ $layanan->alamat }}
                                            <br>{{ $layanan->kelurahan }} - {{ $layanan->kecamatan }}
                                            <br>{{ $layanan->dati2 }} - {{ $layanan->propinsi }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>{{ $layanan->nomor_telepon }}</td>
                                    </tr>

                                </body>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    @foreach ($layanan->objek()->get() as $item)
                        <div class="card card-outline">
                            <div class="card-header">
                                <h3 class="card-title text-bold"><i class="fas fa-globe-europe text-info"></i> Objek Pajak</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td width="100px">NOP</td>
                                            <td with='1px'>:</td>
                                            <td>{{ $item->kd_propinsi .
                                                '.' .
                                                $item->kd_dati2 .
                                                '.' .
                                                $item->kd_kecamatan .
                                                '.' .
                                                $item->kd_kelurahan .
                                                '.' .
                                                $item->kd_blok .
                                                '-' .
                                                $item->no_urut .
                                                '.' .
                                                $item->kd_jns_op }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>NIK</td>
                                            <td>:</td>
                                            <td>{{ $item->nik_wp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Wajib Pajak</td>
                                            <td>:</td>
                                            <td>{{ $item->nama_wp }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td>{{ $item->alamat_wp }}
                                                <br>{{ $item->kelurahan_wp }} - {{ $item->kecamatan_wp }}
                                                <br>{{ $item->dati2_wp }} - {{ $item->propinsi_wp }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Luas Bumi</td>
                                            <td>:</td>
                                            <td>{{ $item->luas_bumi }}</td>
                                        </tr>
                                        <tr>
                                            <td>NJOP Bumi</td>
                                            <td>:</td>
                                            <td>
                                                @if ($item->njop_bumi > 0)
                                                    {{ angka($item->njop_bumi / $item->luas_bumi) }}
                                                @else
                                                    0
                                                @endif
                                                M/<sup>2</sup>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Luas Bng</td>
                                            <td>:</td>
                                            <td>{{ $item->luas_bng }}</td>
                                        </tr>
                                        <tr>
                                            <td>NJOP BNG</td>
                                            <td>:</td>
                                            <td>
                                                @if ($item->njop_bng > 0)
                                                    {{ angka($item->njop_bng / $item->luas_bng) }}
                                                @else
                                                    0
                                                @endif
                                                M/<sup>2</sup>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>
                                                @if ($item->pemutakhiran_at != '')
                                                    @if ($item->is_tolak == '1')
                                                        <span class="text-danger"> Ditolak</span>
                                                        <br>
                                                        {{ $item->keterangan_tolak }}
                                                    @else
                                                        Sudah di proses
                                                    @endif
                                                @else
                                                    Belum di proses
                                                @endif

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <small class="text-bold">Dokumen Pendukung</small>
                                <ol>
                                    @foreach ($item->pendukung()->get() as $file)
                                        <li><a target="_blank" href="{{ url($file->url) }}">{{ $file->nama_dokumen }} <i
                                                    class="fas fa-external-link-alt"></i></a>
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection
