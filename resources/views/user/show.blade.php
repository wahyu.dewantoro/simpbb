@extends('layouts.app')
@section('style')
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Profile</h1>
                </div>
                <div class="col-sm-6">


                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle h-100"
                                    src="{{ asset('lte') }}/dist/img/user2-160x160.jpg" alt="User profile picture">
                            </div>
                            <h3 class="profile-username text-center">{{ $pengguna->nama_pendek }}
                                @if (Auth()->user()->hasRole('Super User') == 1)
                                    <a href="{{ url('users/' . $pengguna->id . '/edit') }}">[edit]</a>
                                @endif
                            </h3>
                            @php
                                $rl = '';
                            @endphp
                            @foreach ($pengguna->roles()->pluck('name')->toarray() as $item)
                                @php
                                    $rl .= $item . ', ';
                                @endphp
                            @endforeach
                            <p class="text-muted text-center"> {{ $rl != '' ? substr($rl, 0, -2) : '' }}</p>
                            @php
                                $dokumen = $pengguna->Pendukung();
                            @endphp
                            @if ($dokumen->count())
                                <b>Dokumen Pendukung</b>
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        @foreach ($dokumen->get() as $item)
                                            <tr>
                                                <td><a class="my-link" href="{{ url($item->url) }}"><i
                                                            class="fas fa-file-pdf"></i>
                                                        {{ $item->nama_dokumen }}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                            @if (Auth()->user()->hasrole('DEVELOPER/PENGEMBANG') == true)

                                @if ($pengguna->UserPengelola()->get()->count() > 0)
                                    <b>Data Pengelola</b>
                                    <table class="table table-sm table-borderless">
                                        @foreach ($pengguna->UserPengelola()->get() as $item)
                                            <tr>
                                                <td width='120px'>PT / CV / Badan</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->nama_pengembang }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>Tgl Surat</td>
                                                <td width='1px'>:</td>
                                                <td>{{ tglIndo($item->tgl_surat) }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>No Surat</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->no_surat }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>Keterangan</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->keterangan }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <button class="btn btn-sm btn-flat btn-info" id="register-pengelola">
                                        <i class="far fa-address-card"></i> Update data Developer
                                    </button>
                                @endif

                                @if ($pengguna->is_pengelola == '1')
                                    <b>Data Pengelola</b>
                                    <table class="table table-sm table-borderless">
                                        @foreach ($pengguna->UserPengelola()->get() as $item)
                                            <tr>
                                                <td width='120px'>PT / CV / Badan</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->nama_pengembang }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>Tgl Surat</td>
                                                <td width='1px'>:</td>
                                                <td>{{ tglIndo($item->tgl_surat) }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>No Surat</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->no_surat }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>Keterangan</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->keterangan }}</td>
                                            </tr>
                                            <tr>
                                                <td width='100px'>Status</td>
                                                <td width='1px'>:</td>
                                                <td>{{ $item->verifikasi_kode == '1' ? 'Disetujui' : 'Sedang di verifikasi' }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif

                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill"
                                        href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home"
                                        aria-selected="true">Profile</a>
                                </li>
                                @if (Auth()->user()->hasrole(['DEVELOPER/PENGEMBANG', 'Rayon']))
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill"
                                            href="#custom-tabs-one-profile" role="tab"
                                            aria-controls="custom-tabs-one-profile" aria-selected="false">NOP yang
                                            dikelola</a>
                                    </li>
                                @endif
                                @if (Auth()->user()->hasrole('Wajib Pajak') == true)
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-milik-tab" data-toggle="pill"
                                            href="#custom-tabs-one-milik" role="tab"
                                            aria-controls="custom-tabs-one-milik" aria-selected="false">NOP yang
                                            dimiliki</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="card-body p-1">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-home-tab">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <table class="table table-sm table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td>NIK</td>
                                                        <td width="1px">:</td>
                                                        <td>{{ $pengguna->nik }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama Lengkap</td>
                                                        <td width="1px">:</td>
                                                        <td>{{ $pengguna->nama }}</td>
                                                    </tr>
                                                    @if ($pengguna->telepon != '')
                                                        <tr>
                                                            <td>Telepon</td>
                                                            <td>:</td>
                                                            <td>{{ $pengguna->telepon }}</td>
                                                        </tr>
                                                    @endif
                                                    @if ($pengguna->email != '')
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>:</td>
                                                            <td>{{ $pengguna->email }}</td>
                                                        </tr>
                                                    @endif
                                                    @php
                                                        $uk = $pengguna->Unitkerja()->first()->nama_unit ?? '';
                                                    @endphp
                                                    @if ($uk != '')
                                                        <tr>
                                                            <td>Unit Kerja</td>
                                                            <td>:</td>
                                                            <td>{{ $uk }}</td>
                                                        </tr>
                                                    @endif

                                                    <tr>
                                                        <td>Alamat</td>
                                                        <td>:</td>
                                                        <td>{{ $pengguna->alamat_user }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Desa/Kelurahan</td>
                                                        <td>:</td>
                                                        <td>{{ $pengguna->kelurahan_user }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kecamatan</td>
                                                        <td>:</td>
                                                        <td>{{ $pengguna->kecamatan_user }}</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Kab / Kota</td>
                                                        <td>:</td>
                                                        <td>{{ $pengguna->dati2_user }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Propinsi</td>
                                                        <td>:</td>
                                                        <td>{{ $pengguna->propinsi_user }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-6">
                                            @if (($pengguna->UserRayon->kd_kecamatan ?? '') != '')
                                                <b>Data Rayon</b>
                                                <table class="table table-borderless table-sm">
                                                    <tbody>
                                                        <tr>
                                                            <td>Kecamatan</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $pengguna->UserRayon->nmkecamatan }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Desa/Kelurahan</td>
                                                            <td width="1px">:</td>
                                                            <td>{{ $pengguna->UserRayon->nmkelurahan }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <hr>
                                            @endif
                                            <table class="table table-borderless table-sm">
                                                <tbody>
                                                    <tr>
                                                        <td>Username</td>
                                                        <td width="1px">:</td>
                                                        <td>{{ $pengguna->username }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Password</td>
                                                        <td width="1px">:</td>
                                                        <td>**************
                                                            <br><a href="#" id="resetpass"><i
                                                                    class="fas fa-exchange-alt"></i>
                                                                Ganti Password</a>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade m-0" id="custom-tabs-one-profile" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-profile-tab">
                                    {{-- @if ($pengguna->is_pengelola == '1') --}}
                                    <a class="btn btn-sm btn-outline btn-info" href="{{ url('kelola-nop/create') }}"><i
                                            class="fas fa-file-alt"></i> Buat
                                        Usulan</a>
                                    <table class="table table-sm table-bordered">
                                        <thead class="bg-info">
                                            <tr>
                                                <th width="3px">No</th>
                                                <th>NOP</th>
                                                <th>Wajib Pajak</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($nopkelola) > 0)
                                                @foreach ($nopkelola as $i => $item)
                                                    <tr>
                                                        <td class="text-center">{{ $i + 1 }}</td>
                                                        <td>{{ $item->Nop }}</td>
                                                        <td>{{ $item->WajibPajak }}</td>
                                                        <td>{{ $item->Status }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3" class="text-center">Tidak ada nop yang di
                                                        kelola
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                    {{-- @else
                                        <div class="alert alert-warning alert-dismissible">

                                            Akun Anda belum terdaftar sebagai akun pengelola objek. Silakan melakukan
                                            registrasi sebagai pengelola.

                                        </div>

                                    @endif --}}
                                </div>
                                <div class="tab-pane fade p-0" id="custom-tabs-one-milik" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-milik-tab">
                                    <table class="table table-sm table-bordered">
                                        <thead class="bg-info">
                                            <tr>
                                                <th width="3px">No</th>
                                                <th>NOP</th>
                                                <th>Wajib Pajak</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($nopmilik) > 0)
                                                @foreach ($nopmilik as $i => $item)
                                                    <tr>
                                                        <td class="text-center">{{ $i + 1 }}</td>
                                                        <td>{{ $item->nop }}</td>
                                                        <td>{{ $item->nm_wp }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="text-center" colspan="3">Tidak memiliki NOP</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ url('update_password', $pengguna->id) }}" id="form-reset" method="post">
                    @csrf
                    @method('post')
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-user-lock text-danger"></i> Ganti
                            Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="">Password Lama</label>
                            <div class="input-group mb-3" id="show_hide_password">
                                <input type="password" name="password_lama" id="password_lama" value="" required
                                    class="form-control form-control-sm {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                    placeholder="masukan password lama">
                                <div class="input-group-append">
                                    <a href="" class="input-group-text"><i class="fa fa-eye-slash"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="">Password Baru</label>

                            <div class="input-group mb-3" id="show_hide_password_baru">
                                <input type="password" name="password_baru" id="password_baru" value="" required
                                    class="form-control form-control-sm {{ $errors->has('password_baru') ? 'is-invalid' : '' }}"
                                    placeholder="masukan password baru">
                                <div class="input-group-append">
                                    <a href="" class="input-group-text"><i class="fa fa-eye-slash"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="">Ulangi Password</label>

                            <div class="input-group mb-3" id="show_hide_password_confirm">
                                <input type="password" name="confirm_password" id="confirm_password" value=""
                                    required
                                    class="form-control form-control-sm {{ $errors->has('confirm_password') ? 'is-invalid' : '' }}"
                                    placeholder="masukan ulang password baru">
                                <div class="input-group-append">
                                    <a href="" class="input-group-text"><i class="fa fa-eye-slash"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i
                                class="far fa-window-close"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-register-pengelola">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ url('permohonan-pengelola-objek', $pengguna->id) }}" method="post"
                    enctype="multipart/form-data" id="form-pengembang">
                    @csrf
                    @method('post')
                    <div class="modal-header">
                        <h4 class="modal-title">Update Developer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_pengembang">Nama PT/CV <sup class="text-danger">*</sup></label>
                            <input type="text" name="nama_pengembang" id="nama_pengembang"
                                class="form-control form-control-sm" value="{{ old('nama_pengembang') ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="file">File Rekomendasi <sup class="text-danger">*</sup></label>
                            <input type="file" name="file" id="file" class="form-control form-control-sm"
                                value="{{ old('file') ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="tgl_surat">Tanggal Surat <sup class="text-danger">*</sup></label>
                            <input type="text" name="tgl_surat" id="tgl_surat" class="form-control form-control-sm"
                                value="{{ old('tgl_surat') ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="no_surat">No Surat <sup class="text-danger">*</sup></label>
                            <input type="text" name="no_surat" id="no_surat" class="form-control form-control-sm"
                                value="{{ old('no_surat') ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" rows="2" class="form-control"
                                placeholder="Ketengan tambahan (bila ada)"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-sm btn-flat btn-default" data-dismiss="modal"><i
                                class="far fa-window-close"></i> Cancel</button>
                        <button type="submit" class="btn btn-sm btn-flat btn-primary"><i class="fas fa-save"></i>
                            Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#register-pengelola').on('click', function(e) {
                e.preventDefault()
                $('#modal-register-pengelola').modal('show')

            })


            $('#resetpass').on('click', function(e) {
                e.preventDefault()
                $('#myModal').modal('show')
            })

            $('#tgl_surat').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 2023,
                autoApply: true,
                maxYear: parseInt(moment().format('YYYY'), 5)
            }, );

            $('#form-pengembang').validate({
                rules: {
                    nama_pengembang: {
                        required: true
                    },
                    file: {
                        required: true,
                        extension: 'pdf'
                    },
                    tgl_surat: {
                        required: true
                    },
                    no_surat: {
                        required: true
                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            })

            $('#form-reset').validate({
                rules: {
                    password_lama: {
                        required: true,
                    },
                    password_baru: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#password_baru",
                        minlength: 5
                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });

            $("#show_hide_password_baru a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password_baru input').attr("type") == "text") {
                    $('#show_hide_password_baru input').attr('type', 'password');
                    $('#show_hide_password_baru i').addClass("fa-eye-slash");
                    $('#show_hide_password_baru i').removeClass("fa-eye");
                } else if ($('#show_hide_password_baru input').attr("type") == "password") {
                    $('#show_hide_password_baru input').attr('type', 'text');
                    $('#show_hide_password_baru i').removeClass("fa-eye-slash");
                    $('#show_hide_password_baru i').addClass("fa-eye");
                }
            });


            $("#show_hide_password_confirm a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password_confirm input').attr("type") == "text") {
                    $('#show_hide_password_confirm input').attr('type', 'password');
                    $('#show_hide_password_confirm i').addClass("fa-eye-slash");
                    $('#show_hide_password_confirm i').removeClass("fa-eye");
                } else if ($('#show_hide_password_confirm input').attr("type") == "password") {
                    $('#show_hide_password_confirm input').attr('type', 'text');
                    $('#show_hide_password_confirm i').removeClass("fa-eye-slash");
                    $('#show_hide_password_confirm i').addClass("fa-eye");
                }
            });

            function openWindow(url) {

                var windowHeight = window.screen.height; // Mengambil tinggi layar
                var windowWidth = 600; // Lebar tetap
                window.open(url, 'newWindow', 'width=' + windowWidth + ',height=' + windowHeight +
                    ',top=0,left=100,scrollbars=yes');
            }

            $('.my-link').on('click', function(event) {
                event.preventDefault(); // Mencegah tindakan default link (membuka URL)
                var hrefValue = $(this).attr('href'); // Mengambil nilai href
                openWindow(hrefValue);
            });
        });
    </script>
@endsection
