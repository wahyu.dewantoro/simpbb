@extends('layouts.app')
@section('style')
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Profile</h1>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-outline">
                        <div class="card-header">
                            <h4 class="card-title"><i class="far fa-user-circle text-info"></i> Data Akun</h4>
                            <div class="float-right">



                                @if ($is_wp == false)
                                    <a href="{{ url('change_password', $pengguna->id) }}"><i class="fas fa-random"></i></i>
                                        Ganti Password</a>
                                    <a href="{{ route('users.edit', $pengguna->id) }}"><i class="fas fa-user-edit"></i>
                                        Edit</a>
                                @else
                                    @if (Auth()->user()->hasRole('Wajib Pajak') && auth()->user()->wp_verifikasi_kode=='1')
                                        <a href="{{ route('upgrade.user.form') }}"
                                            class="btn btn-sm btn-flat btn-success"><i class="fas fa-file-upload"></i>
                                            Upgrade
                                            Akun</a>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <strong><i class="fas fa-user-tie"></i> Nama</strong>
                            <div class="text-muted">{{ $pengguna->nama }}</div>
                            <strong><i class="far fa-address-card"></i> NIK</strong>
                            <div class="text-muted">{{ $pengguna->nik }}</div>
                            <strong><i class="fas fa-phone-square-alt"></i> Nomor Whatsapp</strong>
                            <div class="text-muted">{{ $pengguna->telepon }}</div>
                            <strong><i class="fas fa-envelope-open-text"></i> Email</strong>
                            <div class="text-muted">{{ $pengguna->email }}</div>
                            <strong><i class="fas fa-map-marked"></i> Alamat</strong>
                            <div class="text-muted">{{ $pengguna->alamat_user }}<br>
                                {{ $pengguna->kelurahan_user }} - {{ $pengguna->kecamatan_user }}<br>
                                {{ $pengguna->dati2_user }} - {{ $pengguna->propinsi_user }}
                            </div>
                            <strong><i class="fas fa-user-shield"></i> Username</strong>
                            <div class="text-muted">{{ $pengguna->username }}</div>

                            <strong><i class="fas fa-users"></i> Role</strong>
                            <ul style="list-style-type: none;">
                                @foreach ($pengguna->getRoleNames() as $item)
                                    <li> {{ $item }}</li>
                                @endforeach

                            </ul>
                            <ul style="list-style-type: none;">
                                @foreach ($pengguna->pendukung()->get() as $item)
                                    <li><a class="my-link" href="{{ url($item->url) }}"><i class="fas fa-file-pdf"></i>
                                            {{ $item->nama_dokumen }}</a></li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
                @if ($pengguna->hasAnyRole(['Wajib Pajak']) == true)
                    <div class="col-lg-8">

                        @if ($pengguna->wp_verifikasi_kode == '')
                            <div class="alert alert-warning alert-dismissible">
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Verifikasi Akun</h5>
                                Akun Anda saat ini sedang dalam proses verifikasi kelengkapan dokumen yang telah
                                diunggah. Tim kami akan segera melakukan pengecekan atas dokumen yang Anda unggah.
                                <br><br>
                                <strong>Harap tunggu beberapa saat hingga proses verifikasi selesai.</strong>
                                <p class="mt-2">
                                    Jika ada pertanyaan atau dokumen tambahan yang diperlukan, tim kami akan menghubungi
                                    Anda melalui informasi kontak yang terdaftar.
                                </p>
                            </div>
                        @endif

                        @if ($pengguna->wp_verifikasi_kode == '0')
                            <div class="alert alert-info alert-dismissible">
                                <h5><i class="icon fas fa-exclamation-triangle"></i> Verifikasi Akun</h5>
                                Akun Anda telah berhasil diverifikasi. Namun, setelah pemeriksaan dokumen yang Anda unggah,
                                kami
                                menemukan bahwa dokumen tersebut belum memenuhi persyaratan yang diperlukan.
                                <br><br>

                                Jika ada pertanyaan lebih lanjut atau membutuhkan bantuan, jangan ragu untuk menghubungi tim
                                kami.

                                Terima kasih atas perhatian dan kerjasamanya.
                                <br><br>
                            </div>
                        @endif
                    </div>
                @endif


                @if (count($rayon))
                    <div class="col-lg-8">
                        <div class="card card-cyan">
                            <div class="card-header">
                                History Rayon
                            </div>
                            <div class="card-body p-0">
                                <table class="table table-sm text-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Rayon</th>
                                            <th>Status</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rayon as $i => $ry)
                                            <tr>
                                                <td>{{ $i + 1 }}</td>
                                                <td>Rayon
                                                    Desa {{ $ry->nmkecamatan }}
                                                    Kec {{ $ry->nmkelurahan }}
                                                </td>
                                                <td>{{ $ry->status }}</td>
                                                <td>{{ $ry->verifikasi_keterangan }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif

                @if (count($developer))
                    <div class="col-lg-8">
                        <div class="card card-outline card-primary">
                            <div class="card-header">
                                <h3 class="card-title">History Developer</h3>
                            </div>
                            <div class="card-body p-0">
                                <table class="table table-sm table-bordered text-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Developer</th>
                                            <th>Tgl / No Surat</th>
                                            <th>Status</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($developer as $item)
                                            <tr>
                                                <td class="text-center">{{ $no }}</td>
                                                <td>{{ $item->nama_pengembang }}</td>
                                                <td>{{ tglindo($item->tgl_surat) }} / {{ $item->no_surat }}</td>
                                                <td>{{ $item->status }}</td>
                                                <td>{{ $item->verifikasi_keterangan }}</td>
                                            </tr>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('script')
    {{-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script> --}}
    <script>
        $(document).ready(function() {

            function openWindow(url) {

                var windowHeight = window.screen.height; // Mengambil tinggi layar
                var windowWidth = 600; // Lebar tetap
                window.open(url, 'newWindow', 'width=' + windowWidth + ',height=' + windowHeight +
                    ',top=0,left=100,scrollbars=yes');
            }

            $('.my-link').on('click', function(event) {
                event.preventDefault(); // Mencegah tindakan default link (membuka URL)
                var hrefValue = $(this).attr('href'); // Mengambil nilai href
                openWindow(hrefValue);
            });

        })
    </script>
@endsection
