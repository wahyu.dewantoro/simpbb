@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Impersonate</h1>
                </div>
                
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="cari" class="form-control float-right" placeholder="Search"
                                            value="{{ request()->get('cari') }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Telepon</th>
                                        <th>Role</th>
                                        <th>Username</th>
                                        <th>created_at</th>
                                        @can('edit_users', 'delete_users')
                                            <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($result as $index => $row)
                                        <tr>
                                            <td align="center">{{ $index + $result->firstItem() }}</td>
                                            <td>{{ $row->nama }}</td>
                                            <td>{{ $row->telepon }}</td>
                                            <td>
                                                @php
                                                $rl="";    
                                            @endphp
                                            @foreach ($row->roles()->pluck('name') as $item)
                                            @php
                                            $rl=$item.', ';
                                        @endphp 
                                            @endforeach  

                                            {{ $rl<>''?substr($rl,0,-2):''   }} 
                                            </td>
                                            <td>{{ $row->username }}</td>
                                            <td>{{ $row->created_at }}</td>
                                            <td class="text-center">
                                                <a href="{{ url('impersonate',$row->id) }}" class="text-primary"><i class="fas fa-person-booth"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer p-0">
                            <div class="float-sm-right">
                                {{ $result->appends(['cari'=>request()->get('cari')])}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
