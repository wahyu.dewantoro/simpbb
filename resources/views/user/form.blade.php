@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">

                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item active">{{ $title }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        @if(!empty($form->user))
        @if($form->user->update_profile=='')

        <div class="callout callout-warning">
            <h5 class="text-danger"><i class="fas fa-info"></i> Pemberitahuan</h5>
            Silahkan memperbarui data profil apabila sudah tidak relevan dengan kondisi saat ini ! jika masih relevan silahkan langsung klik tombol simpan.
        </div>
        @endif
        @endif

        <div class="card">
            <div class="card-body">

                <form method="post" action="{{ $form->action }}">
                    @csrf
                    @method($form->method)
                    <div class="row">
                        <div class="col-md-4">
                            {{-- <div class="form-group">
                                    <label for="">Nomer Identitas Kependudukan</label>
                                    <input type="text" name="nik" id="nik"
                                        value="{{ old('nik') ?? ($form->user->nik ?? '') }}"
                            class="form-control form-control-sm {{ $errors->has('nik') ? 'is-invalid' : '' }} angka"
                            placeholder="masukan nik">
                            <span class="errorr invalid-feedback">{{ $errors->first('nik') }}</span>
                        </div> --}}
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama" id="nama" value="{{ old('nama') ?? ($form->user->nama ?? '') }}" class="form-control form-control-sm {{ $errors->has('nama') ? 'is-invalid' : '' }}" placeholder="masukan nama">
                            <span class="errorr invalid-feedback">{{ $errors->first('nama') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="">Nama Pendek / Panggilan</label>
                            <input type="text" name="nama_pendek" id="nama_pendek" value="{{ old('nama_pendek') ?? ($form->user->nama_pendek ?? '') }}" class="form-control form-control-sm {{ $errors->has('nama_pendek') ? 'is-invalid' : '' }}" placeholder="masukan nama pendek / panggilan">
                            <span class="errorr invalid-feedback">{{ $errors->first('nama_pendek') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="">Unit Kerja</label>
                            @php
                            $selecteduk = old('kd_unit')??(!empty($form->user)?$form->user->Unitkerja()->first()->kd_unit : '');
                            @endphp
                            <select name="kd_unit" id="kd_unit" class="form-control form-control-sm {{ $errors->has('kd_unit') ? 'is-invalid' : '' }} select2" data-placeholder="Pilih">
                                <option value=""></option>
                                @foreach ($unitkerja as $item)
                                <option @if ($selecteduk==$item->kd_unit) selected @endif value="{{ $item->kd_unit }}">
                                    {{ $item->nama_unit }}</option>
                                @endforeach
                            </select>

                            <span class="errorr invalid-feedback">{{ $errors->first('kd_unit') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        {{-- <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" name="email" id="email"
                                        value="{{ old('email') ?? ($form->user->email ?? '') }}"
                        class="form-control form-control-sm {{ $errors->has('email') ? 'is-invalid' : '' }}"
                        placeholder="masukan email">
                        <span class="errorr invalid-feedback">{{ $errors->first('email') }}</span>
                    </div> --}}

                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" name="username" id="username" value="{{ old('username') ?? ($form->user->username ?? '') }}" class="form-control form-control-sm {{ $errors->has('username') ? 'is-invalid' : '' }}" placeholder="masukan username">
                        <span class="errorr invalid-feedback">{{ $errors->first('username') }}</span>
                    </div>
                    @if ($form->method == 'post')
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" id="password" value="{{ old('password') ?? ($form->user->password ?? '') }}" class="form-control form-control-sm {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="masukan password">
                        <span class="errorr invalid-feedback">{{ $errors->first('password') }}</span>
                    </div>
                    @endif
            </div>

            @if (Auth()->user()->hasRole('Super User'))
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Role User :</label>
                    <span class="errorr invalid-feedback">@if ($errors->has('roles.0')) Role harus pilih @endif</span>

                    <div class="row">
                        @php
                        $ar = [];
                        if (old('roles')) {
                        $ar = old('roles');
                        } else {
                        if (isset($sr)) {
                        $ar = $sr;
                        }
                        }
                        @endphp
                        @foreach ($roles as $role)
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label>
                                    <input @if (in_array($role, $ar)) checked @endif type="checkbox" name="roles[]" value="{{ $role }}" id="">
                                    {{ $role }}
                                </label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="float-sm-right">
            <button class="btn btn-primary"> <i class="fas fa-save"></i> Simpan</button>
        </div>
        </form>
    </div>
    </div>
    </div>
</section>
@endsection
@section('script')
<script>
    $(document).on("keypress", "input,select", function(e) {
        if (e.which == 13) {
            e.preventDefault();
            // Get all focusable elements on the page
            var $canfocus = $(":focusable");
            var index = $canfocus.index(document.activeElement) + 1;
            if (index >= $canfocus.length) index = 0;
            $canfocus.eq(index).focus();
        }
    });

    function walik(str) {
        var words = [];
        words = str.match(/\S+/g);
        var result = "";
        for (var i = 0; i < words.length; i++) {
            result += words[i].split('').reverse().join('');
        }
        return result
    }

    $(document).ready(function() {

        $('.select2').select2();
        $("input[type=text]").keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });
        // nama
        $('#nama').on('keyup', function() {
            var nama = $(this).val()
            if (nama != '') {
                var exp = nama.split(" ");
                var text = "";
                for (let i = 0; i < exp.length; i++) {
                    text += walik(exp[i].substring(0, 3))
                }
                // console.log(text);
                $('#username').val(text);
                $('#nama_pendek').val(exp[0])
            }
        })

        $('#nik').on('keyup', function() {
            var nik = $(this).val()

            /*   if (nik.length == 16) {
                  Swal.fire({
                      title: 'Sedang mencari ...',
                      html: '<span class="fa-2x"><i class="fa fa-spinner fa-pulse"></i></span>',
                      showConfirmButton: false,
                      allowOutsideClick: false,
                  });
                  $.ajax({
                      url: "{{ url('api/ceknik') }}",
                      type: 'GET',
                      dataType: 'JSON',
                      data: {
                          'nik': nik
                      },
                      success: function(res) {
                          if (res.kode != 0) {
                              $('#nama').val(res.raw.nama_lgkp);
                              Swal.close();
                          } else {
                              Swal.fire({
                                  title: res.keterangan,
                                  icon: 'error',
                              });
                          }
                      },
                      error: function(er) {
                          console.log(er);
                          $('#preview_pemohon').html('');
                          $('#nama').val('');

                          Swal.fire({
                              title: 'Terjadi kesalahan dalam sistem',
                              icon: 'error',
                          });
                      }
                  });
              } else {
                  $('#nama').val('');
              } */
        });
    });

</script>
@endsection
