@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Users</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        @can('add_users')
                            <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-user-plus"></i> Create
                            </a>
                        @endcan
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <h3 class="card-title">List Data</h3> --}}
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="cari" class="form-control float-right"
                                            placeholder="Pencarian" value="{{ request()->get('cari') }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-1 table-responsive">
                            <table class="table table-sm table-bordered table-sm table-hover">
                                <thead class="bg-info">
                                    <tr>
                                        <th width="10px">No</th>
                                        <th>Nama / Username</th>
                                        <th>Telepon / Email</th>
                                        <th>Roles</th>
                                        <th width="20px"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($result as $i => $row)
                                        <tr>
                                            <td class="text-center">{{ $result->firstitem() + $i }}</td>
                                            <td>{{ $row->nama }}<br>
                                                <small class="text-success">{{ $row->username }}</small>
                                            </td>
                                            <td>{{ $row->telepon }} <br>
                                                <small class="text-info">{{ $row->email }}</small>
                                            </td>
                                            <td>{{ $row->roles->pluck('name')->join(', ') }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-sm btn-secondary dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                        Actions
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        @can('edit_users', 'delete_users', 'reset_pwd_users')
                                                            @if (Auth()->user()->hasRole('Super User') && Auth()->user()->id == '361')
                                                                <a class="dropdown-item" title="Impersonate User"
                                                                    href="{{ url('impersonate', $row->id) }}">
                                                                    <i class="fas fa-person-booth"></i> Impersonate
                                                                </a>
                                                            @endif
                                                            @can('reset_pwd_users')
                                                                <a class="dropdown-item text-warning" href="#"
                                                                    onclick="
                                                                        var result = confirm('Yakin reset password ?');
                                                                        if (result) {
                                                                            event.preventDefault();
                                                                            document.getElementById('reset-form-{{ $row->id }}').submit();
                                                                        }">
                                                                    <i class="fas fa-recycle"></i> Reset Password
                                                                </a>
                                                            @endcan

                                                            <a class="dropdown-item text-success"
                                                                href="{{ url('users', $row->id) }}">
                                                                <i class="fas fa-user-tie"></i> Detail
                                                            </a>

                                                            @can('delete_users')
                                                                <a class="dropdown-item text-danger" href="#"
                                                                    onclick="confirmDelete({{ $row->id }})">
                                                                    <i class="fas fa-trash-alt"></i> Hapus
                                                                </a>
                                                                <form method="POST" id="delete-form-{{ $row->id }}"
                                                                    action="{{ route('users.destroy', [$row->id]) }}"
                                                                    style="display: none;">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                </form>
                                                            @endcan

                                                            @if (Auth()->user()->is_admin() == true)
                                                                <a class="dropdown-item text-info"
                                                                    href="{{ url('users/' . $row->id . '/edit') }}"><i
                                                                        class="fas fa-user-edit"></i> Edit</a>
                                                            @endif

                                                            <form method="GET" id="reset-form-{{ $row->id }}"
                                                                action="{{ route('users.reset_password', [$row->id]) }}">
                                                                @csrf
                                                                @method('GET')
                                                            </form>
                                                        @endcan
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="card-footer p-2">
                            <div class="float-right">
                                {{ $result->appends(['cari' => request()->get('cari')]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        function confirmDelete(id) {
            event.preventDefault();

            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Tindakan ini tidak dapat dibatalkan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    document.getElementById('delete-form-' + id).submit();
                }
            });

        }
    </script>
@endsection
