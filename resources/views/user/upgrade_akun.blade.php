@extends('layouts.app')
@section('style')
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Profile</h1>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-outline">
                        <div class="card-header">
                            <h4 class="card-title"><i class="far fa-user-circle text-info"></i> Data Akun</h4>
                            <div class="float-right">

                            </div>
                        </div>
                        <div class="card-body">
                            <strong><i class="fas fa-user-tie"></i> Nama</strong>
                            <div class="text-muted">{{ $pengguna->nama }}</div>
                            <strong><i class="far fa-address-card"></i> NIK</strong>
                            <div class="text-muted">{{ $pengguna->nik }}</div>
                            <strong><i class="fas fa-phone-square-alt"></i> Nomor Whatsapp</strong>
                            <div class="text-muted">{{ $pengguna->telepon }}</div>
                            <strong><i class="fas fa-envelope-open-text"></i> Email</strong>
                            <div class="text-muted">{{ $pengguna->email }}</div>
                            <strong><i class="fas fa-map-marked"></i> Alamat</strong>
                            <div class="text-muted">{{ $pengguna->alamat_user }}<br>
                                {{ $pengguna->kelurahan_user }} - {{ $pengguna->kecamatan_user }}<br>
                                {{ $pengguna->dati2_user }} - {{ $pengguna->propinsi_user }}
                            </div>
                            <strong><i class="fas fa-user-shield"></i> Username</strong>
                            <div class="text-muted">{{ $pengguna->username }}</div>

                            <strong><i class="fas fa-users"></i> Role</strong>
                            <ul style="list-style-type: none;">
                                @foreach ($pengguna->getRoleNames() as $item)
                                    <li> {{ $item }}</li>
                                @endforeach

                            </ul>
                            <ul style="list-style-type: none;">
                                @foreach ($pengguna->pendukung()->get() as $item)
                                    <li><a class="my-link" href="{{ url($item->url) }}"><i class="fas fa-file-pdf"></i>
                                            {{ $item->nama_dokumen }}</a></li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">

                    <form id="form-upgrade" action="{{ route('upgrade.user.store') }}" method="POST"
                        enctype="multipart/form-data">
                        <div class="card card-outline card-primary">
                            <div class="card-body">

                                @csrf
                                @method('POST')
                                <div class="form-group m-1">
                                    <label for="role" class="col-form-label">Upgrade Ke</label>
                                    <select name="role" id="role"
                                        class="form-control form-control-sm @error('role') is-invalid @enderror">
                                        {{-- <option value="">Piih</option> --}}
                                        @php
                                            $swp = old('role') ?? 'Rayon';
                                        @endphp
                                        @foreach ($roles as $item)
                                            <option @if ($swp == $item) selected @endif
                                                value="{{ $item }}">
                                                {{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="form-developer">
                                    <div class="form-group m-1">
                                        <label for="nama_pengembang" class="col-form-label">Nama PT /
                                            CV</label>
                                        <input type="text" name="nama_pengembang" id="nama_pengembang"
                                            class="form-control form-control-sm  @error('nama_pengembang') is-invalid @enderror "
                                            value="{{ old('nama_pengembang') ?? '' }}"
                                            placeholder="Masukan nama badan hukum developer">
                                        @error('nama_pengembang')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="form-rayon">
                                    <div class="form-group m-1">
                                        <label for="kd_kecamatan" class="col-form-label">Kecamatan</label>
                                        <select name="kd_kecamatan" id="kd_kecamatan"
                                            class="form-control form-control-sm @error('kd_kecamatan') is-invalid @enderror ">
                                            <option value="">Pilih</option>
                                            @foreach ($kecamatan as $i => $item)
                                                <option value="{{ $i }}">{{ $item }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('kd_kecamatan')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group m-1">
                                        <label for="kd_kelurahan" class="col-form-label">Kelurahan</label>
                                        <select name="kd_kelurahan" id="kd_kelurahan"
                                            class="form-control form-control-sm @error('kd_kelurahan') is-invalid @enderror ">
                                            <option value="">Pilih</option>
                                        </select>
                                        @error('kd_kelurahan')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div id="form-kuasa">
                                    <div class="form-group m-1">
                                        <label for="file_rekom" class="col-form-label">Dokumen Pendukung <sup
                                                class="text-danger">*</sup></label>
                                        <input type="file" name="file_rekom"
                                            class="form-control form-control-sm @error('file_rekom') is-invalid @enderror"
                                            required accept="image/*">
                                        @error('file_rekom')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="tgl_surat" class="col-form-label">Tanggal
                                                    Dokumen</label>
                                                <input type="text"
                                                    class="form-control form-control-sm @error('tgl_surat') is-invalid  @enderror"
                                                    name="tgl_surat" id="tgl_surat" value="{{ old('tgl_surat') ?? '' }}">
                                                @error('tgl_surat')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="no_surat" class="col-form-label">Nomor
                                                    Dokumen</label>
                                                <input type="text"
                                                    class="form-control form-control-sm @error('no_surat') is-invalid  @enderror"
                                                    name="no_surat" id="no_surat" value="{{ old('no_surat') ?? '' }}">
                                                @error('no_surat')
                                                    <span class="invalid-feedback">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <button class="btn btn-sm btn-info"><i class="fas fa-save"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {

            function openWindow(url) {

                var windowHeight = window.screen.height; // Mengambil tinggi layar
                var windowWidth = 600; // Lebar tetap
                window.open(url, 'newWindow', 'width=' + windowWidth + ',height=' + windowHeight +
                    ',top=0,left=100,scrollbars=yes');
            }

            $('.my-link').on('click', function(event) {
                event.preventDefault(); // Mencegah tindakan default link (membuka URL)
                var hrefValue = $(this).attr('href'); // Mengambil nilai href
                openWindow(hrefValue);
            });

            $('#tgl_surat').daterangepicker({
                "singleDatePicker": true,
                "minYear": 2024,
                "autoApply": true,

            });


            var ac_kecamatan, ac_kelurahan;
            ac_kecamatan = <?= $ac_kecamatan ?>;

            $("#kecamatan_user").autocomplete({
                source: ac_kecamatan
            });

            ac_kelurahan = <?= $ac_kelurahan ?>;
            $("#kelurahan_user").autocomplete({
                source: ac_kelurahan
            });
            $(document).on('change', '#role', function() {
                var role = $('#role').val()
                // form-kuasa
                if (role == 'Wajib Pajak' || role == '') {
                    $('#form-kuasa').addClass('d-none')
                } else {
                    $('#form-kuasa').removeClass('d-none')
                }

                // form-developer
                if (role == 'DEVELOPER/PENGEMBANG') {
                    $('#form-developer').removeClass('d-none')

                } else {
                    $('#form-developer').addClass('d-none')
                }


                if (role == 'Rayon') {
                    $('#form-rayon').removeClass('d-none')

                } else {
                    $('#form-rayon').addClass('d-none')
                }

                $('#kd_kecamatan').val('')
                $('#kd_kecamatan').trigger('change')
            })

            $('#role').val('{{ old('role') ?? 'Rayon' }}').trigger('change')

            $(document).on('change', '#kd_kecamatan', function() {
                var kd_kecamatan = $(this).val()
                $.ajax({
                    url: "{{ url('api/desa') }}",
                    data: {
                        kd_kecamatan
                    },
                    success: function(res) {
                        var option = "<option value=''>Pilih</option>";
                        $.each(res, function(k, v) {
                            /// do stuff
                            option += "<option value='" + k + "'>" + k + " - " + v +
                                "</option>";
                        });

                        $('#kd_kelurahan').html(option)
                    }
                })

            })


            jQuery.extend(jQuery.validator.messages, {
                required: "Wajib di isi.",
                email: "Silakan isi alamat email yang valid",
                number: "Silakan masukkan nomor yang valid.",
                equalTo: "Silakan masukkan nilai yang sama lagi.",
                maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                minlength: jQuery.validator.format("Please enter at least {0} characters."),
                rangelength: jQuery.validator.format(
                    "Please enter a value between {0} and {1} characters long."),
                range: jQuery.validator.format("Please enter a value between {0} and {1}."),
                max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
                min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
                extension: "Harus dengan format file .pdf, .png, .jpg",
            });

            $('#form-upgrade').validate({
                rules: {
                    role: {
                        required: true,
                    },
                    file_rekom: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon" || $("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        },
                        accept: "image/*,application/pdf"
                    },
                    no_surat: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon" || $("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        }
                    },
                    tgl_surat: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon" || $("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        }
                    },
                    nama_pengembang: {
                        required: function(element) {
                            return ($("#role").val() ===
                                "DEVELOPER/PENGEMBANG"); // Wajib jika role adalah member
                        }
                    },
                    kd_kecamatan: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon");
                        }
                    },
                    kd_kelurahan: {
                        required: function(element) {
                            return ($("#role").val() === "Rayon");
                        }
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

        })
    </script>
@endsection
