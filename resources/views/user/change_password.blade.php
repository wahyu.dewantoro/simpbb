@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">

                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-4">
                <div class="card">
                <div class="card-body">

                    <form method="post" action="{{ $form->action }}">
                        @csrf
                        @method($form->method)
                            @if ($form->method == 'post')
                                    <div class="form-group">
                                        <label for="">Password Lama</label>
                                        <input type="text" name="password_lama" id="password_lama"
                                            value="" required
                                            class="form-control form-control-sm {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                            placeholder="masukan password lama">
                                        <span class="errorr invalid-feedback">{{ $errors->first('password') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Password Baru</label>
                                        <input type="text" name="password_baru" id="password_baru"
                                            value="" required
                                            class="form-control form-control-sm {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                            placeholder="masukan password baru">
                                        <span class="errorr invalid-feedback">{{ $errors->first('password') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ulangi Password</label>
                                        <input type="text" name="confirm_password" id="confirm_password"
                                            value="" required
                                            class="form-control form-control-sm {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                            placeholder="masukan ulangi password">
                                        <div style="margin-top: 2px;" id="CheckPasswordMatch"></div>
                                    </div>
                            @endif
                        <div class="float-sm-right">
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
                </div>
            </div>
            
        </div>
    </section>
@endsection
@section('script')
<script>
$(document).ready(function () {
   $("#confirm_password").on('keyup', function(){
    var password = $("#password_baru").val();
    var confirmPassword = $("#confirm_password").val();
    if (password != confirmPassword)
        $("#CheckPasswordMatch").html("Password tidak cocok !").css("color","red");
    else
        $("#CheckPasswordMatch").html("Password cocok !").css("color","green");
   });
});
</script>
@endsection