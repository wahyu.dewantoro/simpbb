@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Users</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-sm-right">
                    @can('add_users')
                    <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm">
                        <i class="fas fa-user-plus"></i> Create
                    </a>
                    @endcan
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{-- <h3 class="card-title">List Data</h3> --}}
                        <div class="card-tools">
                            <form action="{{ url()->current() }}">
                                <div class="input-group input-group-sm" style="width: 250px;">
                                    <input type="text" name="cari" class="form-control float-right" placeholder="Pencarian" value="{{ request()->get('cari') }}">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-1">
                        <div class="row">
                            @foreach ($result as $index => $row)
                            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                                <div class="card bg-light d-flex flex-fill">
                                    <div class="card-header text-muted border-bottom-0">
                                        {{ $row->nama }}
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="row">
                                            <div class="col-7">

                                                <p class="text-muted text-sm"><b>Role: </b>
                                                    @php
                                                    $rl = '';
                                                    @endphp
                                                    @foreach ($row->roles()->pluck('name') as $item)
                                                    @php
                                                    $rl .= $item . ', ';
                                                    @endphp
                                                    @endforeach

                                                    {{ $rl != '' ? substr($rl, 0, -2) : '' }}
                                                </p>
                                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Unit
                                                        Kerja:
                                                        @php
                                                        $re = '';
                                                        @endphp
                                                        @foreach ($row->unitkerja()->pluck('nama_unit') as $nu)
                                                        @php
                                                        $re = $nu . ', ';
                                                        @endphp
                                                        @endforeach

                                                        {{ $re != '' ? substr($re, 0, -2) : '' }}
                                                    </li>

                                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> Username:
                                                        {{ $row->username }}</li>
                                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Telepon #:
                                                        {{ $row->telepon }}</li>


                                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope-square "></i></span>
                                                        Email :
                                                        {{ $row->email }}</li>
                                                </ul>
                                            </div>
                                            <div class="col-5 text-center">
                                                <img src="{{ asset('lte') }}/dist/img/user2-160x160.jpg" alt="user-avatar" class="img-circle img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer clearfix p-1">
                                        <div class="text-right">
                                            @can('edit_users', 'delete_users', 'reset_pwd_users')
                                            @if (Auth()->user()->hasRole('Super User') && Auth()->user()->id=='361')
                                            <a title="Impersonate User" href="{{ url('impersonate', $row->id) }}" class=" btn btn-sm btn-primary"><i class="fas fa-person-booth"></i> Impersonate</a>
                                            @endif
                                            @can('reset_pwd_users')
                                            <a href="{{ url('users', $row->id) }}" class=" btn btn-sm btn-warning" onclick="
                                                                                    var result = confirm('Yakin reset password ?');
                                                                                    if(result){
                                                                                        event.preventDefault();
                                                                                        document.getElementById('reset-form-{{ $row->id }}').submit();
                                                                                    }" title="Reset Password"><i class="fas fa-recycle  "></i> Password </a>
                                            @endcan

                                            <a href="{{ url('users', $row->id) }}" class="btn btn-sm btn-info">
                                                <i class="fas fa-user-tie " title="Detail User"></i> Detail
                                            </a>
                                            @can('delete_users')
                                            <a href="{{ url('users', $row->id) }}" onclick="
                                                                                    var result = confirm('Are you sure you want to delete this record?');
                                                                                    if(result){
                                                                                        event.preventDefault();
                                                                                        document.getElementById('delete-form-{{ $row->id }}').submit();
                                                                                    }" title="Delete User"><i class="fas fa-trash-alt btn btn-sm btn-danger"></i> </a>
                                            <form method="POST" id="delete-form-{{ $row->id }}" action="{{ route('users.destroy', [$row->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            @endcan
                                            <form method="GET" id="reset-form-{{ $row->id }}" action="{{ route('users.reset_password', [$row->id]) }}">
                                                @csrf
                                                @method('GET')
                                            </form>
                                            @endcan
                                            {{-- <a href="#" class="btn btn-sm bg-teal">
                                                        <i class="fas fa-comments"></i>
                                                    </a>
                                                    <a href="#" class="btn btn-sm btn-primary">
                                                        <i class="fas fa-user"></i> View Profile
                                                    </a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="card-footer p-2">
                        <div class="text-center">
                            {{ $result->appends(['cari' => request()->get('cari')]) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
