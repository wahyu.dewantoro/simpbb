@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Monitoring Queue</div>
                    <div class="iframe-container">
                        <iframe width="100%" class="iframe" src="{{ url('jobs') }}" ></iframe>
                    </div>
                    {{-- <div class="">
                        <iframe class="responsive-iframe" src=""></iframe>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {

            var iFrames = $('iframe');

            function iResize() {

                for (var i = 0, j = iFrames.length; i < j; i++) {
                    iFrames[i].style.height = iFrames[i].contentWindow.document.body.offsetHeight + 'px';
                }
            }

            if ($.browser.safari || $.browser.opera) {

                iFrames.load(function() {
                    setTimeout(iResize, 0);
                });

                for (var i = 0, j = iFrames.length; i < j; i++) {
                    var iSource = iFrames[i].src;
                    iFrames[i].src = '';
                    iFrames[i].src = iSource;
                }

            } else {
                iFrames.load(function() {
                    this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
                });
            }

        });
    </script>
@endsection
