@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $data['title'] }}</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form class="form-horizontal" action="{{ $data['action'] }}" method="POST" id="myform"
                enctype="multipart/form-data">
                @csrf
                @method($data['method'])
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" id="id" value="{{ $data['objek']->id ?? '' }}">
                        <div class="card card-outline card-info" id="data_objek">
                            <div class="card-header">
                                <h3 class="card-title">Data Objek</h3>
                                <div class="card-tools">
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="jns_pendataan" class="col-md-4 col-form-label  ">Jenis</label>
                                            <div class="col-md-8">
                                                <select name="jns_pendataan" id="jns_pendataan"
                                                    class="form-control form-control-sm" required
                                                    @if (($data['objek']->id ?? '') != '') readonly @endif>
                                                    @php
                                                        $data['objek']->batch->jenis_pendataan_id ?? '';

                                                        $selected = isset($data['objek'])
                                                            ? $data['objek']->batch->jenis_pendataan_id
                                                            : '';
                                                    @endphp
                                                    <option value="">Pilih</option>
                                                    @foreach ($jenis as $i => $item)
                                                        <option @if ($item->id == $selected) selected @endif
                                                            value="{{ $item->id }}">{{ $item->nama_pendataan }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="jns_transaksi" id="jns_transaksi" value='2'>

                                        <div class="form-group row" id="label_nop_asal">
                                            <label for="nop_proses" class="col-md-4 col-form-label " id="text_nop_asal">NOP
                                                Asal</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nop_asal" id="nop_asal"
                                                    value="{{ $data['objek']->nop_asal ?? '' }}"
                                                    class="form-control form-control-sm nop"
                                                    @if (($data['objek']->id ?? '') != '') readonly @endif>
                                            </div>
                                        </div>
                                        {{-- jns_transaksi --}}
                                    </div>
                                    <div class="col-md-4">
                                        {{-- <div class="form-group"> --}}
                                        <div class="form-group row" id="label_kd_kecamatan">
                                            <label for="" class="col-md-4 col-form-label ">Kecamatan</label>
                                            <div class="col-md-8">
                                                <select class="form-control form-control-sm " name="kd_kecamatan" required
                                                    id="kd_kecamatan">
                                                    <option value="">-- Pilih --</option>
                                                    @foreach ($kecamatan as $rowkec)
                                                        <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                            value="{{ $rowkec->kd_kecamatan }}">
                                                            {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="label_kd_kelurahan">
                                            <label class="col-md-4 col-form-label ">Kelurahan</label>
                                            <div class="col-md-8">
                                                <select class="form-control form-control-sm " name="kd_kelurahan"
                                                    id="kd_kelurahan">
                                                    <option value="">-- Kelurahan / Desa -- </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="label_kd_blok">
                                        <div class="form-group row">
                                            <label for="kd_blok" class="col-md-6 col-form-label  ">Kode BLOK</label>
                                            <div class="col-md-6">
                                                {{-- <input type="text" name="kd_blok" id="kd_blok" class="form-control form-control-sm angka" value="" required> --}}
                                                <select class="form-control form-control-sm " name="kd_blok" id="kd_blok">
                                                    <option value="">-- BLOK -- </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-bordered table-sm" id="form_objek">
                                    <thead class="bg-info">
                                        <tr>
                                            <th class="text-center">Data objek pajak</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group row">
                                                            <label for="jalan_op" class="col-md-4 col-form-label  ">Nama
                                                                Jalan</label>
                                                            <div class="col-md-8">
                                                                <input type="text" name="jalan_op" id="jalan_op"
                                                                    class="form-control form-control-sm"
                                                                    value="{{ $data['objek']->jalan_op ?? '' }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="blok_kav_no_op"
                                                                class="col-md-4 col-form-label  ">Blok/Kav/No</label>
                                                            <div class="col-md-8">
                                                                <input type="text" name="blok_kav_no_op"
                                                                    id="blok_kav_no_op"
                                                                    value="{{ $data['objek']->blok_kav_no_op ?? '' }}"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="nm_kelurahan"
                                                                class="col-md-4 col-form-label ">Desa</label>
                                                            <div class="col-md-8">
                                                                <input type="text" id="nm_kelurahan"
                                                                    class="form-control form-control-sm" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="nm_kecamatan"
                                                                class="col-md-4 col-form-label ">Kecamatan</label>
                                                            <div class="col-md-8">
                                                                <input type="text" id="nm_kecamatan"
                                                                    class="form-control form-control-sm" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label for="rt_op"
                                                                        class="col-md-4 col-form-label  ">RT</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" maxlength="3"
                                                                            name="rt_op" id="rt_op"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->rt_op ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label for="rw_op"
                                                                        class="col-md-4 col-form-label  ">RW</label>
                                                                    <div class="col-md-8">

                                                                        <input type="text" name="rw_op"
                                                                            id="rw_op"
                                                                            class="form-control form-control-sm"
                                                                            maxlength="2"
                                                                            value="{{ $data['objek']->rw_op ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group row">
                                                                    <label for="no_persil"
                                                                        class="col-md-6 col-form-label  ">Persil</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="no_persil"
                                                                            id="no_persil"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->no_persil ?? '' }}">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="luas_bumi" class="col-md-3 col-form-label  ">Luas
                                                            </label>
                                                            <div class="col-md-6">
                                                                <input type="text" name="luas_bumi" id="luas_bumi"
                                                                    class="form-control form-control-sm agka"
                                                                    value="{{ $data['objek']->luas_bumi ?? '' }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group row">
                                                            <label for="kd_znt" class="col-md-3 col-form-label  ">
                                                                ZNT</label>
                                                            <div class="col-md-4">
                                                                <select name="kd_znt" id="kd_znt"
                                                                    class="form-control form-control-sm">
                                                                    <option value="">Belum tersedia</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="kelas_tanah"
                                                                class="col-md-3 col-form-label">Kelas</label>
                                                            <div class="col-md-3">
                                                                <input type="text" id="kelas_tanah"
                                                                    class="form-control form-control-sm" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="jns_bumi" class="col-md-3 col-form-label  ">Jenis
                                                                Tanah</label>
                                                            <div class="col-md-6">

                                                                <select name="jns_bumi" id="jns_bumi"
                                                                    class="form-control form-control-sm">
                                                                    <option value="">Pilih</option>
                                                                    @php
                                                                        $slt = $data['objek']->jns_bumi ?? '';
                                                                        // jenisBumijenisBumi
                                                                    @endphp
                                                                    @for ($i = 1; $i <= 3; $i++)
                                                                        <option
                                                                            @if ($slt == $i) selected @endif
                                                                            value="{{ $i }}">
                                                                            {{ jenisBumi($i) }}</option>
                                                                    @endfor

                                                                    {{-- @foreach (ArrayJenisBumi() as $i => $item)
                                                                <option  value="{{ $i }}">{{ $i }} - {{ $item }}</option>
                                                                @endforeach --}}
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="input_bng">
                                                            <label for="jml_bng" class="col-md-3 col-form-label  ">Jml
                                                                Bangunan</label>
                                                            <div class="col-md-2">
                                                                <input type="text" name="jml_bng" id="jml_bng"
                                                                    class="form-control form-control-sm angka"
                                                                    value="{{ isset($data['objek']) ? $data['objek']->bng->count() : '' }}">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div id="data-objek-pajak"></div>

                                <div id="form_bng"></div>
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table table-bordered table-sm" id="form_subjek">
                                            <thead class="bg-info">
                                                <tr>
                                                    <th class="text-center">Data Subjek Pajak</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                {{-- <div class="form-group row">
                                                                    <label for="subjek_pajak_id"
                                                                        class="col-form-label col-md-4  ">Jenis WP</label>
                                                                    <div class="col-md-8">
                                                                        <select name="jenis_wp" id="jenis_wp"
                                                                            class="form-control form-control-sm">
                                                                            <option value="1">Orang Pribadi</option>
                                                                            <option value="2">Badan Usaha</option>
                                                                        </select>
                                                                    </div>
                                                                </div> --}}
                                                                <div class="form-group row">
                                                                    <label for="subjek_pajak_id"
                                                                        class="col-form-label col-md-4  ">NIK / NIB</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="subjek_pajak_id"
                                                                            id="subjek_pajak_id"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->subjek_pajak_id ?? '' }}"
                                                                            maxlength="16">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="nm_wp"
                                                                        class="col-form-label col-md-4  ">Nama</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="nm_wp"
                                                                            id="nm_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->nm_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="jalan_wp"
                                                                        class="col-form-label col-md-4  ">Jalan</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="jalan_wp"
                                                                            id="jalan_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->jalan_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="blok_kav_no_wp"
                                                                        class="col-form-label col-md-4  ">Blok/ Kav/
                                                                        No</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" name="blok_kav_no_wp"
                                                                            id="blok_kav_no_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->blok_kav_no_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <label for="rt_wp"
                                                                                class="col-form-label col-md-4 ">RT</label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" name="rt_wp"
                                                                                    id="rt_wp"
                                                                                    class="form-control form-control-sm"
                                                                                    value="{{ $data['objek']->rt_wp ?? '' }}"
                                                                                    maxlength="3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <label for="rw_wp"
                                                                                class="col-form-label col-md-8 ">RW</label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" name="rw_wp"
                                                                                    id="rw_wp"
                                                                                    class="form-control form-control-sm"
                                                                                    value="{{ $data['objek']->rw_wp ?? '' }}"
                                                                                    maxlength="2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group row">
                                                                    <label for="kelurahan_wp"
                                                                        class="col-form-label col-md-4 ">Kelurahan</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="kelurahan_wp"
                                                                            id="kelurahan_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->kelurahan_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="kecamatan_wp"
                                                                        class="col-form-label col-md-4 ">Kecamatan</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="kecamatan_wp"
                                                                            id="kecamatan_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->kecamatan_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="kota_wp"
                                                                        class="col-form-label col-md-4 ">Dati
                                                                        II</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="kota_wp"
                                                                            id="kota_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->kota_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="propinsi_wp"
                                                                        class="col-form-label col-md-4 ">Propinsi</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="propinsi_wp"
                                                                            id="propinsi_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->propinsi_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="kd_pos_wp"
                                                                        class="col-form-label col-md-6 ">Kode POS</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="kd_pos_wp"
                                                                            id="kd_pos_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->kd_pos_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group row">
                                                                    <label for="status_pekerjaan_wp"
                                                                        class="col-form-label col-md-4 ">Pekerjaan</label>
                                                                    <div class="col-md-8">
                                                                        <select name="status_pekerjaan_wp"
                                                                            id="status_pekerjaan_wp"
                                                                            class="form-control form-control-sm" required>
                                                                            <option value="">Pilih</option>
                                                                            @php
                                                                                $spk =
                                                                                    $data['objek']
                                                                                        ->status_pekerjaan_wp ?? 5;
                                                                            @endphp
                                                                            @foreach (ArrayJenisPekerjaan() as $i => $item)
                                                                                <option
                                                                                    @if ($spk == $i) selected @endif
                                                                                    value="{{ $i }}">
                                                                                    {{ $i }} -
                                                                                    {{ $item }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="kd_status_wp"
                                                                        class="col-form-label col-md-4  ">Status</label>
                                                                    <div class="col-md-8 ">
                                                                        @php
                                                                            $sw = $data['objek']->kd_status_wp ?? 1;
                                                                        @endphp
                                                                        <select name="kd_status_wp" id="kd_status_wp"
                                                                            class="form-control form-control-sm" required>
                                                                            <option value="">Pilih</option>
                                                                            @foreach (ArrayStatusWP() as $i => $item)
                                                                                <option
                                                                                    @if ($sw == $i) selected @endif
                                                                                    value="{{ $i }}">
                                                                                    {{ $i }} -
                                                                                    {{ $item }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="npwp"
                                                                        class="col-form-label col-md-4 ">NPWP</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="npwp"
                                                                            id="npwp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->npwp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="telp_wp"
                                                                        class="col-form-label col-md-4 ">Telepon</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" name="telp_wp"
                                                                            id="telp_wp"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ $data['objek']->telp_wp ?? '' }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table class="table table-bordered table-sm " id="div_terbit_sppt">
                                            <thead class="bg-info">
                                                <tr>
                                                    <th class="text-center">Penerbitan SPPT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="row">

                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group row">
                                                                            <label for=""
                                                                                class="form-col-form-label col-md-8">Nota
                                                                                Perhitungan</label>
                                                                            <div class="col-md-4">
                                                                                <select name="is_nota" id="is_nota"
                                                                                    class="form-control">
                                                                                    <option value="0">Tidak</option>
                                                                                    {{-- <option value="1">Ya</option> --}}

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group row">
                                                                            <label for=""
                                                                                class="form-col-form-label col-md-8">Terbit
                                                                                SPPT untuk tahun </label>
                                                                            <div class="col-md-4">
                                                                                <button
                                                                                    class="btn btn-flat btn-sm btn-success"
                                                                                    id="tambah_sppt"> <i
                                                                                        class="fas fa-plus"></i>
                                                                                    Tambah</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id="daftar_penetapan"></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <table class="table table-sm table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Tahun</th>
                                                                            <th>Pembayaran</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="body_pembayaran"></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-6 offset-3">
                                        <div class="row">
                                            <div class="col-6 offset-3">
                                                <button class="btn btn-flat btn-sm btn-success" type="submit"
                                                    id="simpanpenelitian"><i class="far fa-save"></i>
                                                    Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <span id="session_id" data-user={{ user()->id }}></span>
    </section>
@endsection
@section('css')
    <style type="text/css">
        /* #btn_b, */
        /* #data_subjek, */
        #data_bangunan {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/jquery-validation/additional-methods.min.js"></script>
    <script src="{{ asset('js') }}/wilayah.js"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var method = $("input[name=_method]").val();

            if ($('#kd_kecamatan').val() != '') {
                getKelurahan($('#kd_kecamatan').val());
            }

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            $('#kd_kelurahan').on('change', function() {
                var kec = $('#kd_kecamatan').val();
                var kel = $('#kd_kelurahan').val();
                getBlok(kec, kel)
            })



            showObjek()

            $('#jns_pendataan').on('change', function() {
                showObjek()
            })

            function showObjek() {
                // jns_pendataan
                jns_pendataan = $('#jns_pendataan').val()
                $('#body_pembayaran').html('')

                id_edit = $('#id').val()
                if (id_edit == '') {
                    $('#nop_asal').val('');
                }


                if (jns_pendataan == '5') {
                    $('#form_objek').hide()
                    $('#form_subjek').hide()
                    $('#tambah_sppt,#div_terbit_sppt').hide()
                    $('#label_nop_asal').show()
                } else {
                    $('#form_objek').show()
                    $('#form_subjek').show()
                    $('#tambah_sppt,#div_terbit_sppt').show()
                }


                if (jns_pendataan == '4') {

                    $('#label_nop_asal').hide()
                    $('#label_kd_kecamatan').show()
                    $('#label_kd_kelurahan').show()
                    $('#label_kd_blok').show()
                    $('#text_nop_asal').html('NOP');

                    $('#kd_kecamatan').attr('required', true);
                    $('#kd_kelurahan').attr('required', true);
                    $('#kd_blok').attr('required', true);

                } else
                if (jns_pendataan == '2') {
                    $('#label_nop_asal').show()
                    $('#label_kd_kecamatan').show()
                    $('#label_kd_kelurahan').show()
                    $('#label_kd_blok').show()
                    $('#text_nop_asal').html('NOP ASAL');
                    $('#kd_kecamatan').val('');
                    $('#kd_kelurahan').val('');
                    $('#kd_blok').val('');
                    $('#kd_kecamatan').attr('required', false);
                    $('#kd_kelurahan').attr('required', false);
                    $('#kd_blok').attr('required', false);
                } else if (jns_pendataan == '3' || jns_pendataan == '1' || jns_pendataan == '6') {
                    $('#label_nop_asal').show()
                    $('#kd_kecamatan').val('');
                    $('#kd_kelurahan').val('');
                    $('#kd_blok').val('');

                    $('#kd_kecamatan').attr('required', false);
                    $('#kd_kelurahan').attr('required', false);
                    $('#kd_blok').attr('required', false);

                    $('#label_kd_kecamatan').hide()
                    $('#label_kd_kelurahan').hide()
                    $('#label_kd_blok').hide()
                    $('#text_nop_asal').html('NOP');

                } else {
                    if (jns_pendataan != 5) {
                        $('#label_nop_asal').hide()
                    } else {
                        $('#label_nop_asal').show()
                    }


                    $('#label_kd_kecamatan').hide()
                    $('#label_kd_kelurahan').hide()
                    $('#label_kd_blok').hide()
                    $('#text_nop_asal').html('NOP');

                    $('#kd_kecamatan').attr('required', false);
                    $('#kd_kelurahan').attr('required', false);
                    $('#kd_blok').attr('required', false);
                }
            }


            function getKelurahan(kk) {
                var html = '<option value="">-- pilih --</option>';
                if (kk != '') {
                    openloading();
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });


                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                                // cek nop asal
                                if ($('#jns_pendataan').val() == '2') {
                                    var nop = $('#nop_asal').val();
                                    var b = nop.replace(/[^\d]/g, "");
                                    var kel = b.substr(7, 3);

                                    if (kel) {
                                        $('#kd_kelurahan').val(kel)
                                    }
                                }
                            }
                            closeloading()
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                            closeloading()
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }


            function getBlok(kec, kel) {

                // kode-blok
                var html = '<option value="">-- pilih --</option>';
                if (kec != '' && kel != '') {
                    openloading();
                    $.ajax({
                        url: "{{ url('kode-blok') }}",
                        data: {
                            'kd_kecamatan': kec,
                            'kd_kelurahan': kel
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + v + '">' + v + '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_blok').val(v).trigger('change');
                                }
                            });


                            $('#kd_blok').html(html);

                            closeloading()
                        },
                        error: function(res) {
                            $('#kd_blok').html(html);
                            closeloading()
                        }
                    });
                } else {
                    $('#kd_blok').html(html);
                }

            }

            $(document).on("keypress", "input,select", function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    // Get all focusable elements on the page
                    var $canfocus = $(":focusable");
                    var index = $canfocus.index(document.activeElement) + 1;
                    if (index >= $canfocus.length) index = 0;
                    $canfocus.eq(index).focus();
                }
            });



            var base_url = "{{ url('/') }}";
            $('input').each(function() {
                $(this).val($(this).val().toUpperCase())
            })

            function pad(str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }
            var arrayZnt;
            // JenisBumi(arrayJnsBumi)
            trSpop(arrayTransaksi)
            // statusWepe(arrayStatusWp)

            function trSpop(arrayTransaksi) {
                isi = $("#jns_transaksi").val();
                if (typeof arrayTransaksi[isi] === "undefined") {
                    hasil = "";
                    $("#jns_transaksi").val("");
                } else {
                    hasil = arrayTransaksi[isi];
                }
                $("#jns_transaksi_keterangan").html(hasil);
            }


            // function get last number nop
            function getLastNop(kd_kecamatan, kd_kelurahan, kd_blok) {
                rv = '';
                Swal.fire({
                    title: '<i class="fas fa-sync fa-spin"></i>',
                    text: 'Sistem sedang berjalan, mohon ditunggu!',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showConfirmButton: false
                })

                $.ajax({
                    async: false,
                    url: base_url + "/api/nop-last-number",
                    data: {
                        kd_kecamatan: kd_kecamatan,
                        kd_kelurahan: kd_kelurahan,
                        kd_blok: kd_blok,
                        user_id: $('#session_id').data('user')
                    },
                    success: function(res) {
                        rv = res;
                        // nomor = parseInt(res) + 1
                        // rv = pad(nomor, 4)
                        closeloading()
                    },
                    error: function(er) {

                        closeloading()
                    }
                });
                return rv
            }

            // nop_proses format nop
            $(document).on('change', '#nop_asal', function(e) {
                var tombol = e.which;

                var nop = $('#nop_asal').val();
                id_edit = $('#id').val();

                var convert = formatnop(nop);
                var value = convert;
                $('#nop_asal').val(convert);
                var b = value.replace(/[^\d]/g, "");
                var kec = b.substr(4, 3);
                var kel = b.substr(7, 3);
                var blok = b.substr(10, 3);

                const not_allowed = [37, 39, 8];
                cek = not_allowed.indexOf(tombol)

                if (kec) {
                    $('#kd_kecamatan').val(kec);
                    $('#nm_kecamatan').val(arrayKecamatan[kec])
                    if ($('#jns_pendataan').val() == '2') {
                        $('#kd_kecamatan').val(kec).trigger('change')
                    }
                }

                if (kel) {
                    $('#nm_kelurahan').val(arrayKelurahan[kec + kel])
                    $('#kd_kelurahan').val(kel);
                }


                if (cek < 0) {

                    if ($('#jns_pendataan').val() == 5) {
                        if (b.length == 18) {
                            loadDataPembatalan(b)
                        }

                    } else {
                        const data_pemutakhiran = ['3', '6'];
                        idx_pendataan = data_pemutakhiran.indexOf($('#jns_pendataan').val())

                        if (id_edit == '' && idx_pendataan != -1 && b.length == '18') {
                            getZnt(kec, kel, blok);
                            loadDataObjek()
                        } else {
                            loadpembayaran()
                            if ($('#jns_pendataan').val() == '1') {
                                $('#tambah_sppt,#div_terbit_sppt').hide()
                            }
                        }

                        if (blok.length == '3' && b.length == '13') {
                            $('#kd_blok').val(blok);
                            getZnt(kec, kel, blok);
                        }
                    }
                }
            });


            $(document).on('change', '#kd_kecamatan,#kd_kelurahan,#kd_blok', function(e) {
                var kec = $('#kd_kecamatan').val()
                var kel = $('#kd_kelurahan').val()
                var blok = $('#kd_blok').val()
                // var jtr = "1"
                $('#nm_kelurahan').val(arrayKelurahan[kec + kel])
                $('#nm_kecamatan').val(arrayKecamatan[kec])

                if (kec != '' && kel != '' && blok != '') {
                    getZnt(kec, kel, blok);
                }
            });

            // cek nop penuh
            $("#nop_proses,#jns_transaksi ").on("change", function(e) {
                if ($('#nop_proses').val() != '' && $('#jns_transaksi').val() != '') {



                    var value = $('#nop_proses').val()
                    var jtr = "1"
                    if (jtr == "1") {
                        // cek nop existing
                        hasilcek = existingNop(value);
                        if (hasilcek == 'false' || hasilcek === false) {
                            $('#nop_proses').focus()
                            Swal.fire({
                                icon: "error",
                                title: "Peringatan",
                                text: "NOP " + $("#nop_proses").val() + " sudah terpakai",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            });
                        }
                    } else {
                        hasilcek = existingNop(value);
                        if (hasilcek == 'true' || hasilcek === true) {
                            $('#nop_proses').focus()
                            Swal.fire({
                                icon: "error",
                                title: "Peringatan",
                                text: "NOP " + $("#nop_proses").val() + " sudah belum terdaftar",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            });
                        }
                    }
                }
            });


            // detect transaction
            $("#jns_transaksi").on("keyup", function(e) {
                trSpop(arrayTransaksi);
                nop = $('#nop_proses').val();
                convert = formatnop(nop)
                panjang = convert.length
                if (panjang >= 17) {
                    var b = convert.replace(/[^\d]/g, "");
                    var kec = b.substr(4, 3);
                    var kel = b.substr(7, 3);
                    var blok = b.substr(10, 3);
                    // jika pendaftaran baru - 1

                    if ($('#jns_transaksi').val() == '1' && panjang != 24) {
                        no_urut = getLastNop(kec, kel, blok)
                        gabung = formatnop('3507' + kec + kel + blok + no_urut);
                        $('#nop_proses').val(gabung);
                    }
                }
            });


            $(document).on('change', '#jns_bumi', function(e) {
                id = $('#jns_bumi').val()
                if (id == 1) {
                    $("#input_bng").show();
                    $("#jml_bng").show();
                    getFormBangunan($('#jml_bng').val());
                } else {
                    getFormBangunan(0);
                    $("#input_bng").hide();
                    $("#jml_bng").val("").hide();
                }
                // tambah_sppt
                if (id == 4 || id == 5) {
                    $('#tambah_sppt,#div_terbit_sppt').hide()

                    $('#daftar_penetapan').html('')
                } else {
                    $('#tambah_sppt,#div_terbit_sppt').show()
                }

            })

            $('#jns_bumi').trigger('change')




            $.validator.addMethod(
                "angkaRegex",
                function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
                }, "Harus di isi dengan angka."
            );

            jQuery.validator.addMethod("exactlength", function(value, element, param) {
                return this.optional(element) || value.length == param;
            }, $.validator.format("Please enter exactly {0} characters."));



            $('#myform').validate({
                rules: {
                    jenis_layanan: {
                        required: true
                    },
                    jns_bumi: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 1
                    },
                    jns_transaksi: {
                        required: true
                    },
                    nop_proses: {
                        required: true,
                        exactlength: 24
                    },
                    nop_asal: {
                        required: false
                    },
                    jalan_op: {
                        required: true,
                        maxlength: 30
                    },
                    rt_op: {
                        required: false,
                        digits: true,
                        angkaRegex: true,
                        digits: true,
                        maxlength: 3,
                        minlength: 1
                    },
                    rw_op: {
                        required: false,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 2,
                        minlength: 1
                    },
                    luas_bumi: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        digits: true,
                        maxlength: 12,
                        minlength: 1
                    },
                    kd_znt: {
                        required: true
                    },
                    kd_status_wp: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 1
                    },
                    subjek_pajak_id: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        minlength: 13,
                        maxlength: 16
                    },
                    nm_wp: {
                        required: true,
                        maxlength: 30
                    },
                    jalan_wp: {
                        required: true,
                        maxlength: 30
                    },
                    blok_kav_no_wp: {
                        required: false,
                        maxlength: 15
                    },
                    rt_wp: {
                        digits: true,
                        angkaRegex: false,
                        required: true,
                        maxlength: 3
                    },
                    rw_wp: {
                        angkaRegex: true,
                        required: true,
                        maxlength: 2,
                        digits: true
                    },
                    kd_pos_wp: {
                        required: false,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 5
                    },
                    kelurahan_wp: {
                        required: true
                    },
                    kecamatan_wp: {
                        required: true
                    },
                    kota_wp: {
                        required: true
                    },
                    propinsi_wp: {
                        required: true
                    },
                    telp_wp: {
                        digits: true,
                        angkaRegex: true,
                        required: false,
                        minlength: 10
                    },
                    npwp: {
                        digits: true,
                        angkaRegex: true,
                        required: false
                    },
                    status_pekerjaan_wp: {
                        required: true,
                        digits: true,
                        maxlength: 1
                    }
                },
                messages: {
                    jns_bumi: {
                        required: 'Jenis tanah harus di isi.',
                        digits: 'Harus di isi dengan angka.',
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    jns_transaksi: {
                        required: 'Jenis transaksi harus di isi.'
                    },
                    nop_proses: {
                        required: "NOP harus di isi",
                        exactlength: "Belum terisi penuh"
                    },
                    jalan_op: {
                        required: 'Alamat harus di isi.',
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    rt_op: {
                        required: 'RT harus isi',
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    rw_op: {
                        required: "RW harus isi",
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    luas_bumi: {
                        required: "Harus di isi",
                        digits: "Di isi dengan angka",
                        minlength: "minimal {0} karakter",
                    },

                    kd_status_wp: {
                        required: "Harus di isi",
                        digits: "Di isi dengan angka",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    subjek_pajak_id: {
                        required: "Harus di isi",
                        digits: "Di isi dengan angka",
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    nm_wp: {
                        required: 'Harus di isi'
                    },
                    jalan_wp: {
                        required: 'Harus di isi'
                    },

                    rt_wp: {
                        required: 'RT harus isi',
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    rw_wp: {
                        required: "RW harus isi",
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    kd_pos_wp: {
                        digits: 'Di isi dengan angka',
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    kelurahan_wp: {
                        required: "Harus di isi"
                    },
                    kecamatan_wp: {
                        required: "Harus di isi"
                    },
                    kota_wp: {
                        required: "Harus di isi"
                    },
                    propinsi_wp: {
                        required: "Harus di isi"
                    },
                    telp_wp: {
                        digits: "Di isi dengana angka",
                        minlength: "Minimal {0} karakter"
                    },
                    npwp: {
                        digits: "Di isi dengan angka",
                        required: false
                    },

                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });


            /*
                        $("#myform").submit(function() {
                            let form = $("#myform");
                            // nik=16
                            // nib=13
                            let jenis_wp = $('#jenis_wp').val()
                            if (jenis_wp == '1') {
                                let panjang = 16
                            } else {
                                let panjang = 13
                            }

                            form.validate({
                                errorElement: "span",
                                errorPlacement: function(error, element) {
                                    error.addClass("invalid-feedback");
                                    element.closest(".form-group").append(error);
                                },
                                highlight: function(element, errorClass, validClass) {
                                    $(element).addClass("is-invalid");
                                },
                                unhighlight: function(element, errorClass, validClass) {
                                    $(element).removeClass("is-invalid");
                                },
                                rules: {
                                    jenis_layanan: {
                                        required: true
                                    },
                                    jns_bumi: {
                                        required: true,
                                        digits: true,
                                        angkaRegex: true,
                                        maxlength: 1
                                    },
                                    jns_transaksi: {
                                        required: true
                                    },
                                    nop_proses: {
                                        required: true,
                                        exactlength: 24
                                    },
                                    nop_asal: {
                                        required: false
                                    },
                                    jalan_op: {
                                        required: true,
                                        maxlength: 30
                                    },
                                    rt_op: {
                                        required: false,
                                        digits: true,
                                        angkaRegex: true,
                                        digits: true,
                                        maxlength: 3,
                                        minlength: 1
                                    },
                                    rw_op: {
                                        required: false,
                                        digits: true,
                                        angkaRegex: true,
                                        maxlength: 2,
                                        minlength: 1
                                    },
                                    luas_bumi: {
                                        required: true,
                                        digits: true,
                                        angkaRegex: true,
                                        digits: true,
                                        maxlength: 12,
                                        minlength: 1
                                    },
                                    kd_znt: {
                                        required: true
                                    },
                                    kd_status_wp: {
                                        required: true,
                                        digits: true,
                                        angkaRegex: true,
                                        maxlength: 1
                                    },
                                    subjek_pajak_id: {
                                        required: true,
                                        digits: true,
                                        angkaRegex: true,
                                        minlength: panjang,
                                        maxlength: panjang
                                    },
                                    nm_wp: {
                                        required: true,
                                        maxlength: 30
                                    },
                                    jalan_wp: {
                                        required: true,
                                        maxlength: 30
                                    },
                                    blok_kav_no_wp: {
                                        required: false,
                                        maxlength: 15
                                    },
                                    rt_wp: {
                                        digits: true,
                                        angkaRegex: false,
                                        required: true,
                                        maxlength: 3
                                    },
                                    rw_wp: {
                                        angkaRegex: true,
                                        required: true,
                                        maxlength: 2,
                                        digits: true
                                    },
                                    kd_pos_wp: {
                                        required: false,
                                        digits: true,
                                        angkaRegex: true,
                                        maxlength: 5
                                    },
                                    kelurahan_wp: {
                                        required: true
                                    },
                                    kecamatan_wp: {
                                        required: true
                                    },
                                    kota_wp: {
                                        required: true
                                    },
                                    propinsi_wp: {
                                        required: true
                                    },
                                    telp_wp: {
                                        digits: true,
                                        angkaRegex: true,
                                        required: false,
                                        minlength: 10
                                    },
                                    npwp: {
                                        digits: true,
                                        angkaRegex: true,
                                        required: false
                                    },
                                    status_pekerjaan_wp: {
                                        required: true,
                                        digits: true,
                                        maxlength: 1
                                    }
                                },
                                messages: {
                                    jns_bumi: {
                                        required: 'Jenis tanah harus di isi.',
                                        digits: 'Harus di isi dengan angka.',
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    jns_transaksi: {
                                        required: 'Jenis transaksi harus di isi.'
                                    },
                                    nop_proses: {
                                        required: "NOP harus di isi",
                                        exactlength: "Belum terisi penuh"
                                    },
                                    jalan_op: {
                                        required: 'Alamat harus di isi.',
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    rt_op: {
                                        required: 'RT harus isi',
                                        digits: 'Di isi angka',
                                        minlength: "minimal {0} karakter",
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    rw_op: {
                                        required: "RW harus isi",
                                        digits: 'Di isi angka',
                                        minlength: "minimal {0} karakter",
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    luas_bumi: {
                                        required: "Harus di isi",
                                        digits: "Di isi dengan angka",
                                        minlength: "minimal {0} karakter",
                                    },

                                    kd_status_wp: {
                                        required: "Harus di isi",
                                        digits: "Di isi dengan angka",
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    subjek_pajak_id: {
                                        required: "Harus di isi",
                                        digits: "Di isi dengan angka",
                                        minlength: "minimal {0} karakter",
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    nm_wp: {
                                        required: 'Harus di isi'
                                    },
                                    jalan_wp: {
                                        required: 'Harus di isi'
                                    },

                                    rt_wp: {
                                        required: 'RT harus isi',
                                        digits: 'Di isi angka',
                                        minlength: "minimal {0} karakter",
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    rw_wp: {
                                        required: "RW harus isi",
                                        digits: 'Di isi angka',
                                        minlength: "minimal {0} karakter",
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    kd_pos_wp: {
                                        digits: 'Di isi dengan angka',
                                        maxlength: "Tidak boleh lebih dari {0} karakter"
                                    },
                                    kelurahan_wp: {
                                        required: "Harus di isi"
                                    },
                                    kecamatan_wp: {
                                        required: "Harus di isi"
                                    },
                                    kota_wp: {
                                        required: "Harus di isi"
                                    },
                                    propinsi_wp: {
                                        required: "Harus di isi"
                                    },
                                    telp_wp: {
                                        digits: "Di isi dengana angka",
                                        minlength: "Minimal {0} karakter"
                                    },
                                    npwp: {
                                        digits: "Di isi dengan angka",
                                        required: false
                                    },

                                },

                            });


                            if (form.valid() === false) {


                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                    // footer: '<a href="">Why do I have this issue?</a>'
                                })
                                return false;
                            } else {

                                var method_ = "{{ $data['method'] }}"
                                if (method_ == 'post') {

                                    isi = "1"
                                    if (isi == "1" || isi == "7" || isi == "6") {
                                        nop_proses = $('#nop_proses').val().replace(/[^\d]/g, "");
                                        nop_asal = $('#nop_asal').val().replace(/[^\d]/g, "");

                                        if (nop_proses != nop_asal) {

                                            hasilcek = existingNop($("#nop_proses").val());
                                            if (hasilcek == 'false' || hasilcek === false) {
                                                Swal.fire({
                                                    icon: "error",
                                                    title: "Peringatan",
                                                    text: "NOP " + $("#nop_proses").val() +
                                                        " sudah terpakai ! silahkan hapus NOP ",
                                                    allowOutsideClick: false,
                                                    allowEscapeKey: false
                                                }).then((result) => {

                                                    return false

                                                })

                                                return false

                                            }
                                        }
                                    } else {

                                        hasilcek = existingNop($("#nop_proses").val());
                                        if (hasilcek == 'true' || hasilcek === true) {
                                            // return false;
                                            Swal.fire({
                                                    icon: "error",
                                                    title: "Peringatan",
                                                    text: "NOP " + $("#nop_proses").val() +
                                                        " sudah terpakai ! silahkan hapus NOP ",
                                                    allowOutsideClick: false,
                                                    allowEscapeKey: false
                                                })
                                                .then((result) => {

                                                    return false
                                                })

                                            return false;
                                        }
                                    }
                                }
                            }
                        });
            */
            $(".nop").on("keyup", function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });




            function existingNop(nop) {
                let ceknop = false;
                if (nop.length == 24) {
                    Swal.fire({
                        // icon: 'error',
                        title: '<i class="fas fa-sync fa-spin"></i>',
                        text: 'Sedang checking NOP',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false
                    })

                    var b = nop.replace(/[^\d]/g, "");
                    var kec = b.substr(4, 3);
                    var kel = b.substr(7, 3);
                    var blok = b.substr(10, 3);
                    var no_urut = b.substr(13, 4);
                    var jns_op = b.substr(17, 1);
                    $.ajax({
                        async: false,
                        url: base_url + "/api/cek-unique-nop",
                        data: {
                            kd_kecamatan: kec,
                            kd_kelurahan: kel,
                            kd_blok: blok,
                            no_urut: no_urut,
                            kd_jns_op: jns_op
                        },
                        success: function(res) {
                            ceknop = res
                            swal.close();
                        },
                        error: function(er) {

                            ceknop = false;
                            swal.close();
                        }
                    });
                    // return ceknop;
                }

                return ceknop;
                // return response;
                // return response;
            }


            arrayKelasTanah = {}

            function getZnt(kec, kel, blok) {
                arrayKelasTanah = {}
                $('#kelas_tanah').val('')
                openloading()
                $.ajax({
                    url: base_url + "/api/znt",
                    async: false,
                    data: {
                        kecamatan: kec,
                        kelurahan: kel,
                        blok: blok
                    },
                    success: function(res) {
                        var count = Object.keys(res).length;

                        html = '<option value="">Pilih</option>';
                        $.each(res, function(k, v) {
                            arrayKelasTanah[v['kd_znt']] = v['kelas_tanah']
                            var apd = '<option value="' + v['kd_znt'] + '"> ' + v['kd_znt'] +
                                " [" + formatRupiah(v['nir']) + "/M]</option>";
                            html += apd;
                        })
                        $("#kd_znt").html(html)
                        if (count == 0) {
                            Swal.fire({
                                icon: "error",
                                title: "Peringatan",
                                text: "ZNT pada blok :" + blok + " belum tersedia di sistem",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            });
                        }
                        closeloading()
                    },
                    error: function(er) {

                        $("#kd_znt").html('<option value="">Belum tersedia</option>');
                        closeloading()
                    }
                });

                var sl = "{{ !empty($data['lo']) ? $data['lo']->kd_znt_peta : '' }}"
                // var sl="";
                $('#kd_znt').val(sl).trigger('change')
                $('#kelas_tanah').val(arrayKelasTanah[sl])
            }

            $('#kd_znt').on('change', function() {
                znt = $('#kd_znt').val()
                if (znt != '') {
                    if (arrayKelasTanah[znt]) {
                        $('#kelas_tanah').val(arrayKelasTanah[znt])
                    }
                } else {
                    $('#kelas_tanah').val('')
                }
            })

            function getFormBangunan(jumlah_bng) {
                $("#form_bng").html("");
                nop_asal = $('#nop_asal').val()
                nop = nop_asal.replace(/[^\d]/g, "");
                id = $('#id').val()
                lb = "";
                jpb = "1";
                if (jumlah_bng != "" && jumlah_bng > 0) {
                    Swal.fire({
                        // icon: 'error',
                        title: '<i class="fas fa-sync fa-spin"></i>',
                        text: "Sedang memuat form bangunan",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false
                    })
                    $.ajax({
                        url: base_url + "/api/form-lspop",
                        data: {
                            index: jumlah_bng,
                            luas_bng: lb,
                            jpb: jpb,
                            jenis_ajuan: "{{ $data['penelitian']->jenis_objek ?? '' }}",
                            lhp_id: $('#lhp_id').val(),
                            nop: nop,
                            id: id
                        },
                        dataType: "html",
                        success: function(data) {
                            closeloading()
                            $("#form_bng").html(data);
                        }
                    });
                }
            }

            // tambahan untuk bangunan
            $("#jml_bng").on("keyup", function() {
                jumlah_bng = $("#jml_bng").val();
                getFormBangunan(jumlah_bng);
            });

            // register jQuery extension
            jQuery.extend(jQuery.expr[":"], {
                focusable: function(el, index, selector) {
                    return $(el).is(":input");
                }
            });

            $(".form-control").keyup(function() {
                if (this.value.length == this.maxLength) {
                    $(this).next('.form-control').focus();
                }
            });



            $("input[type=text]").keyup(function(e) {
                $(this).val($(this).val().toUpperCase());
                if ($(this).val().length == this.maxLength) {
                    e.preventDefault();
                    // Get all focusable elements on the page
                    var $canfocus = $(":focusable");
                    var index = $canfocus.index(document.activeElement) + 1;
                    if (index >= $canfocus.length) index = 0;
                    $canfocus.eq(index).focus();
                }
            });


            // $("#daftar_penetapan").html('')
            // konfigurasi tahun penetapan

            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                month = ["January", "February", "March", "April", "May", "June", "July", "August", "September",
                    "October", "November", "December"
                ]

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;


                return [d.getDate(), month[d.getMonth()], year].join(' ');
            }



            // loop tahun penetapan

            function appendSppt(counttahun, tahun, jt) {
                // jt = formatDate(parseInt(new Date().getFullYear()) + '-09-30');
                htmlpenetapan = '<div class="row_sppt row  ' + counttahun +
                    '" >\
                                                            <div class="col-md-4 ">\
                                                                <div class="form-group row">\
                                                                    <label for="mulai_tahun" class="col-form-label col-md-6 ">Tahun <a role="button" tabindex="0" class="text-danger remove flat"  data-id="' +
                    counttahun + '" data-tahun="' + counttahun +
                    '""  title="Hapus">\
                                                                                <i class="fa fa-trash"></i>\
                                                                            </a>\
                                                                        </label>\
                                                                    <div class="col-md-6">\
                                                                        <input type="text" name="mulai_tahun[]" id="mulai_tahun" class="angka form-control form-control-sm" value="' +
                    tahun +
                    '" required>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-md-4">\
                                                                <div class="form-group row">\
                                                                    <label for="tgl_jatuh_tempo" class="col-form-label col-md-5 ">J.Tempo</label>\
                                                                    <div class="col-md-7">\
                                                                        <input type="text" name="tgl_jatuh_tempo[]" id="tgl_jatuh_tempo" class="form-control form-control-sm tanggal" value="' +
                    jt + '">\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-md-4">\
                                                                <div class="form-group row">\
                                                                    <label for="hkpd_individu" class="col-form-label col-md-5 ">HKPD</label>\
                                                                    <div class="col-md-7">\
                                                                        <input type="number" name="hkpd_individu[]" id="hkpd_individu" class="form-control form-control-sm  ">\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        </div>'
                $("#daftar_penetapan").append(htmlpenetapan)
                $('#daftar_penetapan').find(".tanggal").daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    autoApply: true,
                    minYear: 2003,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'DD MMM YYYY',
                        daysOfWeek: [
                            "Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
                        ],
                        monthNames: [
                            "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus",
                            "September", "Oktober", "November", "Desember"
                        ],
                    }
                });
            }

            $(document).on('click', '#tambah_sppt', function(e) {

                e.preventDefault()
                counttahun = $('#daftar_penetapan .row_sppt').length;
                jt = formatDate(parseInt(new Date().getFullYear()) + '-09-30');
                var tahun = ""
                appendSppt(counttahun, tahun, jt)
            });



            $('#daftar_penetapan').find(".tanggal").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                minYear: 2003,
                maxYear: parseInt(moment().format('YYYY'), 10),
                locale: {
                    format: 'DD MMM YYYY',
                    daysOfWeek: [
                        "Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
                    ],
                    monthNames: [
                        "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus",
                        "September", "Oktober", "November", "Desember"
                    ],
                }
            });



            $(document).on('click', '.remove', function() {
                counttahun = $('#daftar_penetapan').length;
                $("#daftar_penetapan").find('.' + $(this).data('id')).remove()
            });




            var taunpenetapan = "{{ $data['arr_penetapan'] ?? '' }}"
            if (taunpenetapan != '') {


                const th_split = taunpenetapan.split(',')
                for (var i = 0; i < th_split.length; i++) {
                    cekrow = $('#daftar_penetapan .row_sppt').length;
                    const data_ = th_split[i].split('|')
                    var jt = formatDate(data_[1]);
                    var tahun = data_[0]
                    appendSppt(cekrow, tahun, jt)
                }
            }
            // jka pembatalan 2
            function inputEmpty() {
                $('#subjek_pajak_id').val('');
                $('#no_persil').val('');
                $('#jalan_op').val('');
                $('#blok_kav_no_op').val('');
                $('#rw_op').val('');
                $('#rt_op').val('');
                $('#kd_status_cabang').val('');
                $('#kd_status_wp').val('');
                $('#luas_bumi').val('');
                $('#status_peta_op').val('');
                $('#jns_transaksi_op').val('');
                $('#tgl_pendataan_op').val('');
                $('#nip_pendata').val('');
                $('#tgl_pemeriksaan_op').val('');
                $('#nip_pemeriksa_op').val('');
                $('#tgl_perekaman_op').val('');
                $('#nip_perekam_op').val('');
                $('#jns_bumi').val('');
                $('#jml_bng').val('');
                $('#kd_znt').val('');
                $('#nm_wp').val('');
                $('#jalan_wp').val('');
                $('#blok_kav_no_wp').val('');
                $('#rw_wp').val('');
                $('#rt_wp').val('');
                $('#kelurahan_wp').val('');
                $('#kecamatan_wp').val('');
                $('#kecamatan_wp').val('');
                $('#kota_wp').val('');
                $('#propinsi_wp').val('');
                $('#propinsi_wp').val('');
                $('#kd_pos_wp').val('');
                $('#telp_wp').val('');
                $('#npwp').val('');
                $('#status_pekerjaan_wp').val('');

                $('#jns_pendataan').trigger('change')
                $('#nop_asal').trigger('keyup')
                $('#kd_znt').val("{{ $data['objek']->kd_znt ?? '' }}")
                $('#kd_znt').trigger('change')

                $('#body_pembayaran').html("");
            }


            function loadpembayaran() {
                nop_asal = $('#nop_asal').val()
                var b = nop_asal.replace(/[^\d]/g, "");
                jns_pendataan = $('#jns_pendataan').val()
                nop_asal = $('#nop_asal').val()
                var b = nop_asal.replace(/[^\d]/g, "");

                if (b.length == '18') {
                    openloading()
                    var kd_kecamatan = b.substr(4, 3);
                    var kd_kelurahan = b.substr(7, 3);
                    var kd_blok = b.substr(10, 3);
                    var no_urut = b.substr(13, 4);
                    var kd_jns_op = b.substr(17, 1);

                    $.ajax({
                        url: "{{ url('api/data-objek') }}",
                        type: "POST",
                        data: {
                            kd_kecamatan: kd_kecamatan,
                            kd_kelurahan: kd_kelurahan,
                            kd_blok: kd_blok,
                            no_urut: no_urut,
                            kd_jns_op: kd_jns_op
                        },
                        success: function(res) {

                            if (res.pembayaran != null) {
                                bodytr = '';
                                $.each(res.pembayaran, function(i, a) {
                                    var ket = "";
                                    if (a['tgl_bayar'] != null) {
                                        ket += "Dibayar pada tanggal : " + formatDate(a[
                                            'tgl_bayar']) + '<br>';
                                    }
                                    if (a['status_pembayaran_sppt'] == '3') {
                                        ket += "Pembayaran di blokir " + a['no_koreksi'] +
                                            "<br>";
                                    }

                                    var highlight = ""
                                    if (a['status_pembayaran_sppt'] == '0') {
                                        ket += "Belum Lunas <br>";
                                        highlight = " style='color:red' "
                                    }


                                    if (a['kobil'] != null) {
                                        ket += "Nota perhitungan " + a['kobil'];
                                    }


                                    bodytr += "<tr " + highlight + ">\
                                                <td>" + a['thn_pajak_sppt'] + "</td>\
                                                <td>" + ket + "</td>\
                                                </tr>";
                                })


                                $('#body_pembayaran').html(bodytr);

                            }


                            closeloading()
                        },
                        error: function(er) {

                            closeloading()
                        }
                    });

                }
            }

            function loadDataObjek() {
                jns_pendataan = $('#jns_pendataan').val()
                nop_asal = $('#nop_asal').val()
                var b = nop_asal.replace(/[^\d]/g, "");

                const data_pemutakhiran = ['3', '6'];
                idx_pendataan = data_pemutakhiran.indexOf(jns_pendataan)

                if (idx_pendataan != -1 && b.length == '18') {
                    openloading()
                    var kd_kecamatan = b.substr(4, 3);
                    var kd_kelurahan = b.substr(7, 3);
                    var kd_blok = b.substr(10, 3);
                    var no_urut = b.substr(13, 4);
                    var kd_jns_op = b.substr(17, 1);

                    $.ajax({
                        url: "{{ url('api/data-objek') }}",
                        type: "POST",
                        data: {
                            kd_kecamatan: kd_kecamatan,
                            kd_kelurahan: kd_kelurahan,
                            kd_blok: kd_blok,
                            no_urut: no_urut,
                            kd_jns_op: kd_jns_op
                        },
                        success: function(res) {

                            if (res.pembayaran != null) {
                                bodytr = '';
                                $.each(res.pembayaran, function(i, a) {
                                    var ket = "";
                                    if (a['tgl_bayar'] != null) {
                                        ket += "Dibayar pada tanggal : " + formatDate(a[
                                            'tgl_bayar']) + '<br>';
                                    }
                                    if (a['status_pembayaran_sppt'] == '3') {
                                        ket += "Pembayaran di blokir " + a['no_koreksi'] +
                                            "<br>";
                                    }

                                    var highlight = ""
                                    if (a['status_pembayaran_sppt'] == '0') {
                                        ket += "Belum Lunas <br>";
                                        highlight = " style='color:red' "
                                    }


                                    if (a['kobil'] != null) {
                                        ket += "Nota perhitungan " + a['kobil'];
                                    }


                                    bodytr += "<tr " + highlight + ">\
                                                <td>" + a['thn_pajak_sppt'] + "</td>\
                                                <td>" + ket + "</td>\
                                                </tr>";
                                })


                                $('#body_pembayaran').html(bodytr);

                            }



                            if (res.objek != null) {
                                $('#subjek_pajak_id').val(res.objek.subjek_pajak_id.trim())
                                $('#no_persil').val(res.objek.no_persil)
                                $('#jalan_op').val(res.objek.jalan_op)
                                $('#blok_kav_no_op').val(res.objek.blok_kav_no_op)
                                $('#rw_op').val(res.objek.rw_op)
                                $('#rt_op').val(res.objek.rt_op)
                                $('#kd_status_cabang').val(res.objek.kd_status_cabang)
                                $('#kd_status_wp').val(res.objek.kd_status_wp)
                                $('#luas_bumi').val(res.objek.total_luas_bumi)
                                $('#status_peta_op').val(res.objek.status_peta_op)
                                $('#jns_transaksi_op').val(res.objek.jns_transaksi_op)
                                $('#tgl_pendataan_op').val(res.objek.tgl_pendataan_op)
                                $('#nip_pendata').val(res.objek.nip_pendata)
                                $('#tgl_pemeriksaan_op').val(res.objek.tgl_pemeriksaan_op)
                                $('#nip_pemeriksa_op').val(res.objek.nip_pemeriksa_op)
                                $('#tgl_perekaman_op').val(res.objek.tgl_perekaman_op)
                                $('#nip_perekam_op').val(res.objek.nip_perekam_op)
                                $('#jns_bumi').val(res.objek.jns_bumi)
                                $('#jml_bng').val(res.objek.jml_bng)
                                $('#kd_znt').val(res.objek.kd_znt)
                                $('#jns_bumi').trigger('change')
                                $('#kd_znt').trigger('change')
                                $('#nm_wp').val(res.objek.nm_wp)
                                $('#jalan_wp').val(res.objek.jalan_wp)
                                $('#blok_kav_no_wp').val(res.objek.blok_kav_no_wp)
                                $('#rw_wp').val(res.objek.rw_wp)
                                $('#rt_wp').val(res.objek.rt_wp)




                                if ($.trim(res.objek.kelurahan_wp)) {
                                    aa = res.objek.kelurahan_wp.split("-");
                                    $('#kelurahan_wp').val(aa[0].trim())
                                    if (typeof aa[1] === void(0)) {
                                        $('#kecamatan_wp').val(aa[1].trim())
                                    } else {
                                        $('#kecamatan_wp').val('')
                                    }
                                } else {
                                    $('#kecamatan_wp').val('')
                                }

                                if ($.trim(res.objek.kota_wp)) {
                                    ab = res.objek.kota_wp.split("-");
                                    $('#kota_wp').val(ab[0].trim())
                                    if (typeof ab[1] === void(0)) {
                                        $('#propinsi_wp').val(ab[1].trim())
                                    } else {
                                        $('#propinsi_wp').val('')
                                    }
                                } else {
                                    $('#propinsi_wp').val('')
                                }



                                $('#kd_pos_wp').val(res.objek.kd_pos_wp)
                                $('#telp_wp').val(res.objek.telp_wp)
                                $('#npwp').val(res.objek.npwp)
                                $('#status_pekerjaan_wp').val(res.objek.status_pekerjaan_wp)
                            }
                            closeloading()
                        },
                        error: function(er) {

                            inputEmpty()
                            closeloading()
                        }
                    });

                } else {
                    inputEmpty()
                }



            }

            function loadDataPembatalan(obj) {
                $('#data-objek-pajak').html('')
                openloading()
                $.ajax({
                    url: "{{ url('informasi/objek-pajak-show') }}",
                    data: {
                        nop: obj
                    },
                    success: function(res) {
                        $('#data-objek-pajak').html(res)
                        closeloading()
                    },
                    error: function(e) {
                        closeloading()
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan',
                            text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                        })
                        $('#data-objek-pajak').html('')
                    }
                });
            }

            $(document).on('keyup', '#luas_bumi', function(e) {
                lb = $(this).val();

                if (parseInt(lb) == 0 || lb == '') {
                    vb = 5;
                } else {
                    vb = 3;
                }
                $('#jns_bumi').val(vb).trigger('change');
            });


            id_edit = $('#id').val()
            if (id_edit != '') {
                vaalue = $("#nop_asal").val()
                var ba = vaalue.replace(/[^\d]/g, "");
                var keca = ba.substr(4, 3);
                var kela = ba.substr(7, 3);
                var bloka = ba.substr(10, 3);
                getZnt(keca, kela, bloka);

                $('#jns_pendataan').trigger('change')
                $('#nop_asal').trigger('keyup')
                $('#nop_asal').trigger('change')
                $('#kd_znt').val("{{ $data['objek']->kd_znt ?? '' }}")
                $('#kd_znt').trigger('change')

            }

        });




        window.addEventListener("beforeunload", function(e) {
            openloading()
        });
    </script>
@endsection
