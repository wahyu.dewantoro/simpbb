@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Upload Data Pemutakhiran</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    @csrf
                    <div class="card card-outline card-info" id="data_objek">

                        <div class="card-body p-1">
                            <div class="row" id="areaform">
                                <div class="col-md-4">
                                    <form class="form-horizontal" action="#" method="POST" id="myform"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="">Kecmatan</label>
                                            <select class="form-control form-control-sm select2" name="kd_kecamatan"
                                                required id="kd_kecamatan" data-placeholder="Pilih kecamatan">
                                                <option value=""></option>
                                                @foreach ($kecamatan as $rowkec)
                                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                        value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Kelurahan</label>
                                            <select class="form-control form-control-sm select2" name="kd_kelurahan"
                                                id="kd_kelurahan" data-placeholder="Desa / Kelurahan">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Jenis</label>
                                            <select class="form-control form-control-sm " name="jenis_pendataan_id"
                                                id="jenis_pendataan_id">
                                                <option value="">-- Pilih -- </option>
                                                @foreach ($jenis as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama_pendataan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="form-group">
                                        <label for="">NOP Induk/Asal</label>
                                        <input type="text" name="nop_asal" id="nop_asal" class="form-control form-control-sm nop">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jenis Bumi Induk/sisa</label>
                                        <select name="jns_bumi" id="jns_bumi" class="form-control form-control-sm">
                                            <option value="">Pilih</option>
                                            @foreach (ArrayJenisBumi() as $i => $item)
                                            <option value="{{ $i }}">{{ $i }} - {{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Sisa Luas Tanah</label>
                                        <input type="text" name="luas_bumi_asal" id="luas_bumi_asal" class="form-control form-control-sm angka">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Sisa Luas BNG</label>
                                        <input type="text" name="luas_bng_asal" id="luas_bng_asal" class="form-control form-control-sm angka">
                                    </div> --}}

                                        <div class="form-group">
                                            <label for="">File Excel</label>
                                            <input type="file" name="file" id="file"
                                                class="form-control form-control-sm ">
                                        </div>
                                        <div class="float-right">
                                            <button class="btn btn-flat btn-sm btn-success" type="button" id="preview"><i
                                                    class="far fa-save"></i>
                                                preview</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-3 col-sm-6 col-12">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-success"><i
                                                class="far fa-file-excel fa-2x"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text"><strong> Template</strong></span>
                                            <span class="info-box-text">Penyusunan data</span>

                                            <a href="{{ url('upload_pendataan_baru.xlsx') }}">
                                                Download <i class="fas fa-cloud-download-alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <strong> ketentuan upload exel</strong>
                                    <ol>
                                        <li>Pada kolom alamat maksimal panjang digit adalah 30</li>
                                        <li>Format exel tidak boleh di rubah nama kolomnya </li>
                                        <li>Kolom tidak boleh menggunakan fungsi exel (rumus) </li>
                                        <li>Jika proses upload terdapat data warna merah maka harus di upload ulang dengan
                                            terlebih dahulu melihat kesalahannya pada kolom status .</li>
                                        <li>Untuk angka di depan terdapat 0 di abaikan saja contoh rt 02 rw 009 maka di
                                            tulis rt.2 rw.9</li>
                                    </ol>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div id="hasil_preview"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </section>
@endsection
@section('css')
    <style type="text/css">
        /* #btn_b, */
        /* #data_subjek, */
        #data_bangunan {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/jquery-validation/additional-methods.min.js"></script>
    <script src="{{ asset('js') }}/wilayah.js"></script>
    <script>
        $(document).ready(function() {
            var headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

            $(".select2").select2({
                closeOnSelect: true,
                allowClear: true
            })

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            $(".nop").on("keyup", function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $("#jenis_pendataan_id,#jns_bumi").on("change", function() {
                jns = $('#jenis_pendataan_id').val()
                inputanPendataan(jns)

            });

            inputanPendataan($('#jenis_pendataan_id').val())

            function inputanPendataan(jns) {


                if (jns == '2') {
                    $('#nop_asal').attr('readonly', false)
                    $('#jns_bumi').attr('readonly', false)
                    $('#luas_bumi_asal').attr('readonly', false)
                    $('#luas_bng_asal').attr('readonly', false)


                } else {
                    $('#nop_asal').attr('readonly', true)
                    $('#jns_bumi').attr('readonly', true)
                    $('#luas_bumi_asal').attr('readonly', true)
                    $('#luas_bng_asal').attr('readonly', true)

                    // setupvalue
                    $('#nop_asal').val('')
                    $('#jns_bumi').val('')
                    $('#luas_bumi_asal').val('')
                    $('#luas_bng_asal').val('')
                }
            }

            function getKelurahan(kk) {
                var html = '<option value=""></option>';
                $('#kd_kelurahan').html(html)
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            $.each(res, function(key, value) {
                                var newOption = new Option(key + '-' + value, key, true, true);
                                $('#kd_kelurahan').append(newOption)
                            });

                            $('#kd_kelurahan').val(null)
                        },
                        error: function(res) {

                        }
                    });
                }

            }

            $("#preview").click(function() {

                vkd_kecamatan = $('#kd_kecamatan').val();
                vkd_kelurahan = $('#kd_kelurahan').val();
                vfile = $('#file').val();

                if (vkd_kecamatan == '' ||
                    vkd_kelurahan == '' ||
                    vfile == '') {
                    return false;
                }


                $('#hasil_preview').html("");
                const fileupload = $('#file').prop('files')[0];

                let formData = new FormData();
                formData.append('file', fileupload);
                formData.append('kd_kecamatan', $('#kd_kecamatan').val());
                formData.append('kd_kelurahan', $('#kd_kelurahan').val());
                formData.append('jenis_pendataan_id', $('#jenis_pendataan_id').val());
                // formData.append('nop_asal', $('#nop_asal').val());
                // formData.append('jns_bumi', $('#jns_bumi').val());
                // formData.append('luas_bumi_asal', $('#luas_bumi_asal').val());
                // formData.append('luas_bng_asal', $('#luas_bng_asal').val());

                formData.append('_token', '{{ csrf_token() }}');
                openloading();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pendataan.import.preview') }}",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(res) {
                        $('#hasil_preview').html(res);
                        closeloading();
                        $('#areaform').hide();
                    },
                    error: function() {
                        $('#hasil_preview').html("");
                        closeloading();
                        alert("Data Gagal Diupload");
                    }
                });
            });

        });

        /* window.addEventListener("beforeunload", function(e) {
            openloading()
        }); */
    </script>
@endsection
