<html>
<body>


    <table>
        <thead>
            <tr>
                <th colspan="16"><b>LAPORAN PENDATAAN OBJEK PAJAK</b></th>
            </tr>

            <tr>
                <th colspan="2">kecamatan</th>
                <th colspan="14">: {{ $param['nm_kec']<>''?$param['nm_kec']:'All' }}</th>
            </tr>
            <tr>
                <th colspan="2">Kelurahan</th>
                <th colspan="14">: {{ $param['nm_kel']<>''?$param['nm_kel']:'All' }}</th>
            </tr>
            <tr>
                <th colspan="2">Pendata</th>
                <th colspan="14">: {{ $param['nm_pendata']<>''?$param['nm_pendata']:'All' }}</th>
            </tr>
            <tr>
                <th colspan="2">Tanggal</th>
                <th colspan="14">: {{ $param['tanggal']<>''?tglindo($param['tanggal']):'All' }}</th>
            </tr>
            <tr>
                <th colspan="16"></th>
            </tr>

            <tr>
                <th>No</th>
                <th>Formulir</th>
                <th>NOP</th>
                <th>Alamat</th>
                <th>RT</th>
                <th>RW</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>ZNT</th>
                <th>L.Tanah</th>
                <th>NJOP Tanah</th>
                <th>L.Bng</th>
                <th>NJOP Bng</th>
                <th>Persil</th>
                <th>Pendata</th>
                <th>Tanggal</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($data as $i=>$row)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ $row->no_formulir}}</td>
                <td>{{ formatnop($row->nop_proses)}}</td>
                <td>{{ $row->jalan_op.' '.$row->blok_kav_no_op }}</td>
                <td>{{ $row->rt_op}}</td>
                <td>{{ $row->rw_op}}</td>
                <td>{{ $row->nm_kelurahan}}</td>
                <td>{{ $row->nm_kecamatan}}</td>
                <td>{{ $row->kd_znt}}</td>
                <td>{{ $row->total_luas_bumi}}</td>
                <td>{{ $row->njop_bumi}}</td>
                <td>{{ $row->total_luas_bng}}</td>
                <td>{{ $row->njop_bng}}</td>
                <td>{{ $row->no_persil}}</td>
                <td>{{ $row->created_name}}</td>
                <td>{{ tglindo($row->created_at)}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
