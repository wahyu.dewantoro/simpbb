@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pendataan / Pemutakhiran</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        <a href="{{ url('pendataan/create') }}" class="btn btn-sm btn-info btn-flat"> <i
                                class="fas fa-plus-square"></i> Tambah</a>
                        <a href="{{ url('pendataan/import') }}" class="btn btn-sm btn-success btn-flat"> <i
                                class="fas fa-file-upload"></i> Upload</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-12 ">
                <div class="card card-primary card-tabs">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-md-2">
                                <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2"
                                    name="jenis_pendataan_id" required id="jenis_pendataan_id">
                                    <option value="">.: Jenis :.</option>
                                    @foreach ($jp as $rj)
                                        <option value="{{ $rj->id }}">{{ $rj->nama_pendataan }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2">
                                <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="status"
                                    required id="status">
                                    <option value="">.: Status :.</option>
                                    <option value="x">Belum di verifikasi</option>
                                    <option value="2">Verifikasi Peta</option>
                                    <option value="1">Disetujui</option>
                                    <option value="0">Tidak Disetujui</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="created_by"
                                    required id="created_by">
                                    <option value="">.: Pegawai :.</option>
                                    @foreach ($pembuat as $rc)
                                        <option value="{{ $rc->created_by }}">{{ $rc->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control pencariandata " id="tanggal"
                                        placeholder="Tanggal">
                                    {{-- <span class="input-group-append">
                                    <button id="cetak" class="btn btn-sm btn-success"><i class="far fa-file-excel"></i> Cetak</button>
                                </span> --}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group input-group-sm">
                                    <button type='button' class='btn btn-sm btn-block btn-success btn-export'> <i
                                            class="fas fa-file-alt"></i> Export </button>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil"
                            style="width:100%">
                            <thead class="bg-info">
                                <tr>
                                    <th style="text-align: center" style="width:1px !important;">No</th>
                                    <th>Jenis</th>
                                    <th>Nomor</th>
                                    <th>Tanggal</th>
                                    <th>Status </th>
                                    <th>Objek</th>
                                    <th>Pegawai</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>

    </style>
@endsection

@section('script')
    <script src="{{ asset('js') }}/wilayah.js"></script>
    <script>
        $(document).ready(function() {
            var tableObjek, tableHasil
            tableHasil = 
            $('#table-hasil').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('pendataan.index') }}",
                    data: function(d) {
                        d.created_by = $('#created_by').val();
                        d.tanggal = $('#tanggal').val();
                        d.status = $('#status').val();
                        d.jenis_pendataan_id = $('#jenis_pendataan_id').val();
                    }
                },
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        searchable: false
                    }, {
                        data: 'nama_pendataan',
                        orderable: false,
                        name: 'pencarian_data'
                    }, {
                        data: 'nomor_batch',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'tanggal_indo',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'status',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'objek',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'pegawai_pendata',
                        orderable: false,
                        searchable: false
                    }


                    , {
                        data: 'pilih',
                        class: 'text-right',
                        orderable: false,
                        searchable: false
                    }
                ],
                aLengthMenu: [
                    [10, 20, 50, 75, -1],
                    [10, 20, 50, 75, "Semua"]
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }],
                iDisplayLength: 10,
                rowCallback: function(row, data, index) {}
            });


            $('#table-hasil').on('click', '.hapus', function(e) {
                // url = $(this).data('href')
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "data yang dihapus tidak dapat di kembalikan.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        // document.getElementById('delete-form-' + id).submit()
                        document.location.href = $(this).data('href');
                    }
                })

            });

            $(document).on('change', '.pencariandata', function() {
                pencarian();
            });

            $(document).on("click", ".btn-export", function(evt) {
                let url = "{{ url('pendataan/cetak') }}?created_by=" + $('#created_by').val() +
                    "&tanggal=" + $('#tanggal').val() + "&status=" + $('#status').val() +
                    "&jenis_pendataan_id=" + $('#jenis_pendataan_id').val();
                window.open(url, '_blank');
            });


            function pencarian() {
                tableHasil.draw();
            }

            $('#tanggal').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                todayHighlight: true
            });
        })
    </script>
@endsection
