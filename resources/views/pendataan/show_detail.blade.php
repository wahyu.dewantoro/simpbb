<div class="row">
    <div class="col-12"><b>Subjek Pajak</b></div>
    <div class="col-md-4">
        <table class="table table-sm table-borderless">
            <tr>
                <td width="100px">Status </td>
                <td width="1px">:</td>
                <td>{{ $objek->kd_status_wp ?? '' }}</td>
                {{-- <td>{{  statusWP($objek->kd_status_wp) }}</td> --}}
            </tr>
            <tr>
                <td>NIK </td>
                <td>:</td>
                <td>{{ $objek->subjek_pajak_id ?? '' }}</td>
            </tr>
            <tr>
                <td>Nama </td>
                <td>:</td>
                <td>{{ $objek->nm_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>Jalan </td>
                <td>:</td>
                <td>{{ $objek->jalan_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>Blok/Kav/No </td>
                <td>:</td>
                <td>{{ $objek->blok_kav_no_wp ?? '' }} RT {{ $objek->rt_wp ?? '' }} RW {{ $objek->rw_wp ?? '' }}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-4">
        <table class="table table-sm table-borderless">
            <tr>
                <td width="100px">Kelurahan</td>
                <td width="1px">:</td>
                <td>{{ $objek->kelurahan_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>:</td>
                <td>{{ $objek->kecamatan_wp ?? '' }}</td>
            </tr>

            <tr>
                <td>Kota </td>
                <td>:</td>
                <td>{{ $objek->kota_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>Propinsi </td>
                <td>:</td>
                <td>{{ $objek->propinsi_wp ?? '' }}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-4">
        <table class="table table-sm table-borderless">
            <tr>
                <td width="100px">Kode Pos </td>
                <td width="1px">:</td>
                <td>{{ $objek->kd_pos_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>Telp </td>
                <td>:</td>
                <td>{{ $objek->telp_wp ?? '' }}</td>
            </tr>
            <tr>
                <td>NPWP </td>
                <td>:</td>
                <td>{{ $objek->npwp ?? '' }}</td>
            </tr>
            <tr>
                <td>Pekerjaan </td>
                <td>:</td>
                <td>{{ $objek->status_pekerjaan_wp ?? '' }}</td>
            </tr>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-12"><b>Data Objek {{ $objek->ket_induk }} </b></div>
    <div class="col-md-4">
        <table class="table table-sm table-borderless">
            <tr>
                <td width="100px">Nop Asal </td>
                <td width='1px'>:</td>
                <td>{{ $objek->nop_asal }}</td>
            </tr>
            <tr>
                <td>Kecamatan </td>
                <td>:</td>
                <td>{{ $objek->nm_kecamatan }}</td>
            </tr>
            <tr>
                <td>Kelurahan </td>
                <td>:</td>
                <td>{{ $objek->nm_kelurahan }}</td>
            </tr>
            <tr>
                <td>Blok </td>
                <td>:</td>
                <td>{{ $objek->kd_blok }}</td>
            </tr>

        </table>
    </div>

    <div class="col-md-4">
        <table class="table table-sm table-borderless">
            <tr>
                <td width="100px">Jalan </td>
                <td width='1px'>:</td>
                <td>{{ $objek->jalan_op }}</td>
            </tr>
            <tr>
                <td>Blok/Kav/No </td>
                <td>:</td>
                <td>{{ $objek->blok_kav_no_op }} RT {{ $objek->rt_op }} RW {{ $objek->rw_op }}</td>
            </tr>
            <tr>
                <td>Persil </td>
                <td>:</td>
                <td>{{ $objek->no_persil }}</td>
            </tr>
            <tr>
                <td>ZNT </td>
                <td>:</td>
                <td>{{ $objek->kd_znt }}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-4">
        <table class="table table-sm table-borderless">
            <tr>
                <td width="100px">Luas Bumi </td>
                <td width="1px">:</td>
                <td>{{ $objek->luas_bumi }}</td>
            </tr>
            <tr>
                <td>NIR</td>
                <td>:</td>
                <td>{{ $objek->nir }}</td>
            </tr>
            <tr>
                <td>Jenis Bumi</td>
                <td>:</td>
                <td>{{ jenisBumi($objek->jns_bumi) }}</td>
            </tr>
        </table>
    </div>
</div>
@if ($objek->jns_bumi == 1)
    <div class="row">
        <div class="col-md-12">
            @include('pendataan._bangunan', ['bangunan' => $objek->bng()->get()])
        </div>
    </div>
@endif
