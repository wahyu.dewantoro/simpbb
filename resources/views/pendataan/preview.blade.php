<form action="{{ route('pendataan.import.store')}}" method="post" enctype="multipart/form-data">
    <input type='text' style="display:none" name="jenis_pendataan_id" value="{{ $request->jenis_pendataan_id }}">
    <div class="row">
        <div class="col-12 table-responsive">
            <table id="table-preview" class="table table-sm table-bordered ">
                <thead class="bg-info">
                    <tr>
                        <th rowspan="2">No</th>
                        <th colspan="11">Objek</th>
                        <th colspan="13">Subjek</th>
                        <th colspan="12">Bangunan</th>
                    </tr>
                    <tr>
                        <th style="width: 100px">Status</th>
                        <th style="width: 100px">NOP Asal/Induk</th>
                        <th>Jalan</th>
                        <th>Blok Kav No</th>
                        <th>Persil</th>
                        <th>RT</th>
                        <th>RW</th>
                        <th>Luas</th>
                        <th>ZNT</th>
                        <th>Sts Tanah</th>
                        <th>Kepemilikan</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Jalan</th>
                        <th>Blok Kav NO</th>
                        <th>RT</th>
                        <th>RW</th>
                        <th>Pekerjaan</th>
                        <th>Kelurahan</th>
                        <th>Kota</th>
                        <th>Propinsi</th>
                        <th>KD POS</th>
                        <th>Telp</th>
                        <th>NPWP</th>
                        <th>KD JPB</th>
                        <th>Luas</th>
                        <th>Thn Dibangun</th>
                        <th>Thn Direnovasi</th>
                        <th>Lantai</th>
                        <th>listrik</th>
                        <th>kondisi</th>
                        <th>konstruksi</th>
                        <th>atap</th>
                        <th>dinding</th>
                        <th>lantai</th>
                        <th>langit2</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $submit=0;
                    $jumlah=0;
                    @endphp
                    @foreach ($data as $nomor=> $row)
                    @php
                    $jumlah+=1;
                    @endphp

                    @if ($row['status']==1)
                    @php
                    $submit+=1;
                    @endphp
                    <input type="text" style="display: none" name="induk[]" value={{ onlynumber( $row['induk'])}}>
                    <input type="text" style="display: none" name="nop_asal[]" value={{ onlynumber( $row['nop_asal'])}}>

                    <input type='text' style="display:none" name="nop_proses[]" value="{{ $row['nop_proses'] }}">

                    <input type='text' style="display:none" name="jns_transaksi[]" value="{{ $row['jns_transaksi'] }}">
                    <input type='text' style="display:none" name="jalan_op[]" value="{{ $row['jalan_op'] }}">
                    <input type='text' style="display:none" name="blok_kav_no_op[]" value="{{ $row['blok_kav_no_op'] }}">
                    <input type='text' style="display:none" name="rt_op[]" value="{{ $row['rt_op'] }}">
                    <input type='text' style="display:none" name="rw_op[]" value="{{ $row['rw_op'] }}">
                    <input type='text' style="display:none" name="no_persil[]" value="{{ $row['no_persil'] }}">
                    <input type='text' style="display:none" name="luas_bumi[]" value="{{ $row['luas_bumi'] }}">
                    <input type='text' style="display:none" name="kd_znt[]" value="{{ $row['znt'] }}">
                    <input type='text' style="display:none" name="jns_bumi[]" value="{{ $row['status_tanah'] }}">
                    <input type='text' style="display:none" name="jml_bng[]" value="{{ $row['luas_bng']>0?1:0 }}">
                    <input type='text' style="display:none" name="subjek_pajak_id[]" value="{{ $row['subjek_pajak_id'] }}">
                    <input type='text' style="display:none" name="nm_wp[]" value="{{ $row['nm_wp'] }}">
                    <input type='text' style="display:none" name="jalan_wp[]" value="{{ $row['jalan_wp'] }}">
                    <input type='text' style="display:none" name="blok_kav_no_wp[]" value="{{ $row['blok_kav_no_wp'] }}">
                    <input type='text' style="display:none" name="rt_wp[]" value="{{ $row['rt_wp'] }}">
                    <input type='text' style="display:none" name="rw_wp[]" value="{{ $row['rw_wp'] }}">
                    <input type='text' style="display:none" name="kelurahan_wp[]" value="{{ $row['kelurahan_wp'] }}">
                    <input type='text' style="display:none" name="kecamatan_wp[]" value="{{ $row['kecamatan_wp']??'' }}">
                    <input type='text' style="display:none" name="kota_wp[]" value="{{ $row['kota_wp'] }}">
                    <input type='text' style="display:none" name="propinsi_wp[]" value="{{ $row['propinsi_wp'] }}">
                    <input type='text' style="display:none" name="kd_pos_wp[]" value="{{ $row['kd_pos_wp'] }}">
                    <input type='text' style="display:none" name="status_pekerjaan_wp[]" value="{{ $row['pekerjaan'] }}">
                    <input type='text' style="display:none" name="kd_status_wp[]" value="{{ $row['status_kepemilikan'] }}">
                    <input type='text' style="display:none" name="npwp[]" value="{{ $row['npwp'] }}">
                    <input type='text' style="display:none" name="telp_wp[]" value="{{ $row['telp_wp'] }}">
                    <input type="text" style="display: none" name="kd_jpb[]" value="{{ $row['kd_jpb'] }}">
                    <input type="text" style="display: none" name="thn_dibangun_bng[]" value="{{ $row['thn_dibangun_bng'] }}">
                    <input type="text" style="display: none" name="thn_renovasi_bng[]" value="{{ $row['thn_renovasi_bng'] }}">
                    <input type="text" style="display: none" name="luas_bng[]" value="{{ $row['luas_bng'] }}">
                    <input type="text" style="display: none" name="jml_lantai_bng[]" value="{{ $row['jml_lantai_bng'] }}">
                    <input type="text" style="display: none" name="kondisi_bng[]" value="{{ $row['kondisi_bng'] }}">
                    <input type="text" style="display: none" name="jns_konstruksi_bng[]" value="{{ $row['jns_konstruksi_bng'] }}">
                    <input type="text" style="display: none" name="jns_atap_bng[]" value="{{ $row['jns_atap_bng'] }}">
                    <input type="text" style="display: none" name="kd_dinding[]" value="{{ $row['kd_dinding'] }}">
                    <input type="text" style="display: none" name="kd_lantai[]" value="{{ $row['kd_lantai'] }}">
                    <input type="text" style="display: none" name="kd_langit_langit[]" value="{{ $row['kd_langit_langit'] }}">
                    <input type="text" style="display: none" name="daya_listrik[]" value="{{ $row['daya_listrik'] }}">

                    @endif

                    <tr @if ($row['status']=='0' ) style="color:red" @endif>
                        <td rowspan="2" class="text-center">{{ $nomor+1 }}</td>
                        <td>
                            <span class="text-success"><?= $row['induk']=='1'?'Induk':'' ?></span>
                        </td>
                        <td>{{ formatnop(onlynumber( $row['nop_asal'])) }}</td>
                        <td>{{ $row['jalan_op'] }}</td>
                        <td>{{ $row['blok_kav_no_op'] }}</td>
                        <td>{{ $row['no_persil'] }}</td>
                        <td>{{ $row['rt_op'] }}</td>
                        <td>{{ $row['rw_op'] }}</td>
                        <td>{{ $row['luas_bumi'] }}</td>
                        <td>{{ $row['znt'] }}</td>
                        <td>{{ $row['status_tanah'] }}</td>
                        <td>{{ $row['status_kepemilikan'] }}</td>
                        <td>{{ $row['subjek_pajak_id'] }}</td>
                        <td>{{ $row['nm_wp'] }}</td>
                        <td>{{ $row['jalan_wp'] }}</td>
                        <td>{{ $row['blok_kav_no_wp'] }}</td>
                        <td>{{ $row['rt_wp'] }}</td>
                        <td>{{ $row['rw_wp'] }}</td>
                        <td>{{ $row['pekerjaan'] }}</td>
                        <td>{{ $row['kelurahan_wp'] }}</td>
                        <td>{{ $row['kota_wp'] }}</td>
                        <td>{{ $row['propinsi_wp'] }}</td>
                        <td>{{ $row['kd_pos_wp'] }}</td>
                        <td>{{ $row['telp_wp'] }}</td>
                        <td>{{ $row['npwp'] }}</td>
                        <td>{{ $row['kd_jpb'] }}</td>
                        <td>{{ $row['luas_bng'] }}</td>
                        <td>{{ $row['thn_dibangun_bng'] }}</td>
                        <td>{{ $row['thn_renovasi_bng'] }}</td>
                        <td>{{ $row['jml_lantai_bng'] }}</td>
                        <td>{{ $row['daya_listrik'] }}</td>
                        <td>{{ $row['kondisi_bng'] }}</td>
                        <td>{{ $row['jns_konstruksi_bng'] }}</td>
                        <td>{{ $row['jns_atap_bng'] }}</td>
                        <td>{{ $row['kd_dinding'] }}</td>
                        <td>{{ $row['kd_lantai'] }}</td>
                        <td>{{ $row['kd_langit_langit'] }}</td>
                    </tr>
                    <tr>
                        <td colspan="36">
                            <?= $row['status']=='0'?$row['keterangan']:'Ready di import.' ?>
                        </td>
                        @for($a= 0; $a < 36; $a++) <td style="display: none">
                            </td>
                            @endfor


                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="col-8">
            <div class="float-right">
                <p><b>Valid</b> : {{ $submit }} Data , <b>Tidak valid</b> : {{ $jumlah-$submit }}</p>
            </div>
        </div>
        <div class="col-4">
            <div class="float-right">
                @if ($submit>0)
                <button class="btn btn-sm btn-flat btn-success"><i class="far fa-save"></i> Proses Import</button>
                @endif
                <button type="button" id="batal" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</button>
            </div>
        </div>
    </div>

</form>
<style>
    tbody td {
        word-break: break-all !important;
    }

</style>
<script>
    $(document).ready(function() {
        $.extend($.fn.dataTable.defaults, {
            searching: false
            , ordering: false
            , select: false
            , lengthChange: false
        });


        /* $('#table-preview').DataTable({
            bAutoWidth: false
        }); */

        $('#batal').on('click', function(e) {
            e.preventDefault();

            $('#areaform').show();
            $('#hasil_preview').html('');
        })
    });

</script>
