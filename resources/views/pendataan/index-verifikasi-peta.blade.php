@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Verifikasi Pendataan TIM PETA</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">

                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">
                    <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                        <thead class="bg-danger">
                            <tr>
                                <th style="text-align: center" style="width:1px !important;">No</th>
                                <th>Jenis</th>
                                <th>Nomor</th>
                                <th>Tanggal</th>
                                <th>Status </th>
                                <th>Objek</th>
                                <th>Pegawai</th>
                                <th>Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<style>

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableObjek, tableHasil
        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('pendataan/verifikasi-peta') }}"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'nama_pendataan'
                    , orderable: false
                    , name: 'pencarian_data'
                }
                , {
                    data: 'nomor_batch'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'tanggal_indo'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'status'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'objek'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'pegawai_pendata'
                    , orderable: false
                    , searchable: false
                }


                , {
                    data: 'pilih'
                    , class: 'text-right'
                    , orderable: false
                    , searchable: false
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });
    })
</script>
@endsection
