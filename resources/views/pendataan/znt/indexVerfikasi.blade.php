@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>VERIFIKASI MERUBAH ZNT OBJEK</h1>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    {{-- <a href="#" class='btn btn-sm btn-flat btn-success btn-export'> <i class="fas fa-file-alt"></i> Cetak </a> --}}

                    {{-- <a href="{{ route('pendataan.merubah-znt.create') }}" class="btn btn-sm btn-info btn-flat"> <i class="fas fa-plus-square"></i> Tambah</a> --}}
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 ">
                <div class="card card-primary card-tabs">
                    <div class="card-body p-0">
                        <table class="table table-striped table-sm table-bordered table-hover " id="table-verifikasi" style="width:100%">
                            <thead class="bg-info">
                                <tr>
                                    <th style="text-align: center" style="width:1px !important;">No</th>
                                    <th>Batch</th>
                                    <th>Kecamatan</th>
                                    <th>Kelurahan</th>
                                    <th>Blok</th>
                                    <th>ZNT Lama</th>
                                    <th>ZNT Baru</th>
                                    <th>Jumlah Obj</th>
                                    {{-- <th>Status</th> --}}
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="verifikator" data-id=""></div>
        </div>
    </div>
</section>

<style>

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var verifikator = $('#verifikator').data('id')


        var tableHasil, tableVerifikasi

        tableVerifikasi = $('#table-verifikasi').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            ajax: {
                url: "{{ route('pendataan.merubah-znt.index') }}?jenis=2",
                data: function(d) {
                    d.status = $('#status').val();
                }
            },
            columns: [{
                data: null,
                class: 'text-center',
                orderable: false,
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                searchable: false
            }, {
                data: 'nomor_batch',
                orderable: false,
                name: 'pencarian_data'
            }, {
                data: 'nm_kecamatan',
                orderable: false,
                searchable: false
            }, {
                data: 'nm_kelurahan',
                orderable: false,
                searchable: false
            }, {
                data: 'kd_blok',
                orderable: false,
                searchable: false
            }, {
                data: 'kd_znt_lama',
                orderable: false,
                searchable: false
            }, {
                data: 'kd_znt_baru',
                orderable: false,
                searchable: false
            }, {
                data: 'objek',
                orderable: false,
                searchable: false
            }, {
                data: 'pilih2',
                class: 'text-right',
                orderable: false,
                searchable: false
            }],
            aLengthMenu: [
                [10, 20, 50, 75, -1],
                [10, 20, 50, 75, "Semua"]
            ],
            "order": [],
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,
            }],
            iDisplayLength: 10,
            rowCallback: function(row, data, index) {}
        });


        

        $(document).on('change', '.pencariandata', function() {
            pencarian();
        });

        $(document).on("click", ".btn-export", function(evt) {
            let url = "{{url('pendataan/cetak')}}?created_by=" + $('#created_by').val() +
                "&tanggal=" + $('#tanggal').val() + "&status=" + $('#status').val() +
                "&jenis_pendataan_id=" + $('#jenis_pendataan_id').val();
            window.open(url, '_blank');
        });

        $('#cetak').on('click', function() {

        });


        function pencarian() {
            tableHasil.draw();
        }

        $('#tanggal').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
    })
</script>
@endsection