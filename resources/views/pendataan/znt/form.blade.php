@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>FORM PERUBAHAN ZNT OBJEK</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        {{-- <a href="{{ route('pendataan.merubah-znt.create') }}" class="btn btn-sm btn-info btn-flat"> <i class="fas fa-plus-square"></i> Tambah</a> --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ $data['action'] }}" method="post">
                            @csrf
                            @method($data['method'])
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4 " for="">Kecamatan</label>
                                        <div class="col-md-8">
                                            <select class="form-control form-control-sm" name="kd_kecamatan" required
                                                id="kd_kecamatan" required>
                                                <option value="">-- Pilih --</option>
                                                @foreach ($data['kecamatan'] as $rowkec)
                                                    <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                        value="{{ $rowkec->kd_kecamatan }}">
                                                        {{ $rowkec->kd_kecamatan }} - {{ $rowkec->nm_kecamatan }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4 " for="">Kelurahan</label>
                                        <div class="col-md-8">
                                            <select class="form-control form-control-sm" name="kd_kelurahan"
                                                id="kd_kelurahan" required>
                                                <option value="">-- Kelurahan / Desa -- </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4 " for="">Blok</label>
                                        <div class="col-md-8">
                                            <select name="kd_blok" id="kd_blok" class="form-control form-control-sm "
                                                required>
                                                <option value="">-- blok --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4 " for="">ZNT Lama</label>
                                        <div class="col-md-8">
                                            <select name="kd_znt_lama" id="kd_znt_lama"
                                                class="form-control form-control-sm kd_znt" required>
                                                <option value="">-- ZNT --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4 " for="">ZNT Baru</label>
                                        <div class="col-md-8">
                                            <select name="kd_znt_baru" id="kd_znt_baru"
                                                class="form-control form-control-sm kd_znt">
                                                <option value="">-- ZNT --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4 " for="">Keterangan</label>
                                        <div class="col-md-8">
                                            <textarea name="keterangan" id="keterangan" rows="1" class="form-control form-control-sm" maxlength="225" required></textarea>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-8">
                                    <div id="list_objek"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="float-right">
                                        <button id="simpanblok" type="submit" class="btn btn-primary btn-sm btn-flat"><i
                                                class="far fa-save"></i> simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>

    </style>
@endsection

@section('script')
    {{-- <script src="{{ asset('js') }}/wilayah.js"></script> --}}
    <script>
        $(document).ready(function() {
            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })


            $('#kd_kelurahan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                var kel = $('#kd_kelurahan').val();
                var blok = $('#kd_blok').val();
                getblok(kk, kel);
                // getZnt(kk, kel, blok)
            })

            $('#kd_blok').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                var kel = $('#kd_kelurahan').val();
                var blok = $('#kd_blok').val();
                getZnt(kk, kel, blok)
            })

            /*   $('#kd_znt_lama').on('change', function() {
                  var kk = $('#kd_kecamatan').val();
                  var kel = $('#kd_kelurahan').val();
                  var blok = $('#kd_blok').val();
                  var kd_znt = $('#kd_znt_lama').val();
                  listObjek(kk, kel, kd_blok, kd_znt)
              }) */

            //
            var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
            if (kd_kecamatan == '') {
                var kd_kecamatan = $('#kd_kecamatan').val()
            }
            getKelurahan(kd_kecamatan);

            function getKelurahan(kk) {
                openloading();
                var html = '<option value="">-- pilih --</option>';
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + k + ' - ' + v +
                                    '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });

                            $('#kd_kelurahan').html(html);
                            closeloading();
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                            closeloading();
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                    closeloading();
                }
            }

            function getblok(kd_kec, kd_kel) {
                var html = '<option value="">-- blok --</option>';
                if (kd_kec != '' && kd_kel != '') {
                    openloading();
                    $.ajax({
                        url: "{{ url('desa-blok') }}",
                        data: {
                            'kd_kecamatan': kd_kec,
                            'kd_kelurahan': kd_kel
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + v +
                                    '</option>';
                                html += apd;

                            });

                            $('#kd_blok').html(html);

                            closeloading();
                        },
                        error: function(res) {
                            $('#kd_blok').html(html);
                            closeloading();
                        }
                    });
                } else {
                    $('#kd_blok').html(html);
                    closeloading();
                }
            }

            function getZnt(kd_kec, kd_kel, kd_blok) {
                var html = '<option value="">-- pilih --</option>';
                if (kd_kec != '' && kd_kel != '') {
                    openloading();
                    $.ajax({
                        url: "{{ url('pendataan/merubah-znt/create') }}?tipe=znt",
                        data: {
                            'kd_kecamatan': kd_kec,
                            'kd_kelurahan': kd_kel,
                            'kd_blok': kd_blok
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            /* if (count == 1) {
                                html = '';
                            } */
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + v.kd_znt + '">' + v.kd_znt +
                                    ' - ' + formatRupiah(v.nir) + '</option>';
                                html += apd;
                                // if (count == 1) {
                                $('.kd_znt').val(k);
                                // }
                            });

                            $('.kd_znt').html(html);

                            closeloading();
                        },
                        error: function(res) {
                            $('.kd_znt').html(html);
                            closeloading();
                        }
                    });
                } else {
                    $('.kd_znt').html(html);
                    closeloading();
                }
            }


            $('#kd_znt_lama').on('change', function() {
                openloading();
                $('#list_objek').html('');
                kd_kec = $('#kd_kecamatan').val()
                kd_kel = $('#kd_kelurahan').val()
                kd_blok = $('#kd_blok').val()
                kd_znt = $('#kd_znt_lama').val();
                $.ajax({
                    url: "{{ url('pendataan/merubah-znt/create') }}?tipe=objek",
                    data: {
                        'kd_kecamatan': kd_kec,
                        'kd_kelurahan': kd_kel,
                        'kd_blok': kd_blok,
                        'kd_znt': kd_znt
                    },
                    success: function(res) {
                        $('#list_objek').html(res);
                        closeloading();
                    },
                    error: function(res) {
                        $('#list_objek').html('');
                        closeloading();
                    }
                });
            });
        })
    </script>
@endsection
