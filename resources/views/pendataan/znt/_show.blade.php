<div class="invoice p-3 mb-3">
    <div class="row">
        <div class="col-12">
            <h4>
                <i class="fas fa-globe"></i> Ds/Kel. {{ $data->nm_kelurahan }} , Kec. {{ $data->nm_kecamatan }}
                <small class="float-right">Tanggal: {{ tglIndo($data->created_at) }}</small>
            </h4>
        </div>

    </div>

    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Dari
            <address>
                <strong>{{ $data->kd_znt_lama }}</strong>
                <br>NIR : {{ angka($data->nir_lama) }}

            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            Ke
            <address>
                <strong>{{ $data->kd_znt_baru }}</strong>
                <br>NIR : {{ angka($data->nir_baru) }}
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            <b>Batch #{{ $data->nomor_batch }}</b><br>
            @php
                echo $st;
            @endphp <br>
            Pegawai : {{ $data->pegawai }}
            <br>
            Keterangan: <i>{{ $data->keterangan }}</i>
        </div>
        @if ($data->nir_baru < $data->nir_lama)
            <div class="col-sm-12">
                <div class="alert alert-danger alert-dismissible">

                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
                    Nilai NIR yang baru lebih rendah dari NIR sebelumnya.
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped table-sm table-bordered">
                <thead class="bg-info">
                    <tr>
                        <th class="text-center" rowspan="2">NOP</th>
                        <th class="text-center" colspan="2">Total NJOP</th>
                        <th class="text-center" rowspan="2">Status</th>
                    </tr>
                    <tr>
                        <th class="text-center">Lama</th>
                        <th class="text-center">Baru</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($objek as $item)
                        @php
                            $njopa = $item->luas_bumi * $data->nir_lama;
                            $njopb = $item->luas_bumi * $data->nir_baru;
                        @endphp
                        <tr>
                            <td>
                                {{ $item->kd_propinsi }}.{{ $item->kd_dati2 }}.{{ $item->kd_kecamatan }}.{{ $item->kd_kelurahan }}.{{ $item->kd_blok }}-{{ $item->no_urut }}.{{ $item->kd_jns_op }}
                            </td>
                            <td class="text-right">{{ angka($njopa) }}</td>
                            <td class="text-right">{{ angka($njopb) }}</td>
                            <td class="text-right">
                                @if($njopa == $njopb)

                                <i class="fas fa-minus" style="color: #00ff40;"></i> 
                                @elseif($njopa < $njopb)
                                    <i class="fas fa-arrow-up text-success"></i> {{ angka($njopb - $njopa) }}
                                @else
                                    <i class="fas fa-arrow-down text-danger"></i> {{ angka($njopa - $njopb) }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

</div>
