@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>VERIFIKASI PERUBAHAN ZNT OBJEK</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-right">
                        {{-- <a id="verifikasi" data-href="{{ route('pendataan.merubah-znt.verifikasi-proses',$data->id) }}" class="btn bt-sm btn-flat btn-success"> <i class="fas fa-check"></i> Verifikasi</a> --}}
                        <a href="{{ route('pendataan.merubah-znt.index') }}" class="btn bt-sm btn-flat btn-info"> <i
                                class="fas fa-angle-double-left"></i> Kembali</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">

        <div class="card card-primary">
            {{-- <div class="card-header">
            <h3 class="card-title">Verifikasi </h3>
        </div> --}}


            <form method="POST" action="{{ route('pendataan.merubah-znt.verifikasi-proses', $data->id) }}">
                @csrf
                @method('post')
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input opsi" type="radio" id="customRadio1"
                                            name="st" value="1" required>
                                        <label for="customRadio1" class="custom-control-label">Disetujui</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input opsi" type="radio" id="customRadio2"
                                            name="st" value="0">
                                        <label for="customRadio2" class="custom-control-label">Ditolak</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="penetapan" name="penetapan"
                                            value="1">
                                        <label class="form-check-label" for="penetapan">Di tetapkan ulang untuk tahun
                                            {{ date('Y') }}</label>
                                    </div>
                                </div>
                            </div>


                            <div id="area_penetapan">
                                <div class="form-group row">
                                    <label for="" class="col-form-label col-md-6">Tahun</label>
                                    <div class="col-md-6">
                                        <input type="text" name="thn_pajak" id="thn_pajak" class="form-control"
                                            value="" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-form-label col-md-6">Jatuh Tempo</label>
                                    <div class="col-md-6">
                                        <input type="text" name="tgl_jatuh_tempo" id="tgl_jatuh_tempo"
                                            class="form-control tanggal" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="" class="col-form-label col-md-4">Keterangan</label>
                                <div class="col-md-8">
                                    <textarea name="keterangan" id="keterangan" rows="3" class="form-control form-control-sm" required>{{ $data->keterangan }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="float-right">
                                    <button id="simpanblok" type="submit" class="btn btn-primary btn-sm btn-flat"><i
                                            class="far fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        @include('pendataan.znt._show', ['objek' => $objek, 'data' => $data])
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#penetapan").attr("disabled", true);
            bukaTutup(false)

            // customRadio1
            $('.opsi').on('click', function(e) {
                if ($('#customRadio1').is(':checked')) {
                    $("#penetapan").attr("disabled", false);
                } else {
                    $("#penetapan").attr("disabled", true);
                    bukaTutup(false)

                    $('#penetapan').each(function() {
                        this.checked = false;
                    });

                }
            })

            function bukaTutup(isChecked) {
                if (isChecked) {
                    $('#area_penetapan').show()
                    $('#thn_pajak').attr("disabled", false);
                    $('#thn_pajak').attr("required", true);
                    $('#thn_pajak').attr("readonly", true);
                    $('#thn_pajak').val("{{ date('Y') }}");
                    $('#tgl_jatuh_tempo').attr("disabled", false);
                    $('#tgl_jatuh_tempo').attr("required", true);
                } else {
                    $('#area_penetapan').hide()
                    $('#thn_pajak').attr("disabled", true);
                    $('#thn_pajak').attr("required", false);
                    $('#thn_pajak').val('')
                    $('#tgl_jatuh_tempo').attr("disabled", true);
                    $('#tgl_jatuh_tempo').attr("required", false);
                    $('#tgl_jatuh_tempo').val('')
                }
            }

            $('#penetapan').on('click', function(e) {
                bukaTutup($('#penetapan').is(':checked'))
            })

            function check_uncheck_checkbox(isChecked) {
                if (isChecked) {
                    $('input[type="checkbox"]').each(function() {
                        this.checked = true;
                    });
                } else {
                    $('input[type="checkbox"]').each(function() {
                        this.checked = false;
                    });
                }
            }


            $('#verifikasi').on('click', function(e) {
                // url = $(this).data('href')
                e.preventDefault();
                Swal.fire({
                    title: 'Verifikasi perubahan ZNT Objek'
                        // , text: "data yang dihapus tidak dapat di kembalikan."
                        ,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya,Setuju',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    url = $(this).data('href');
                    /* if (result.value) {
                        st = '1'
                    } else {
                        st = '0'
                    }

                    document.location.href = url + '?st=' + st */
                })

            });
        });
    </script>
@endsection
