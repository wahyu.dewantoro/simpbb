@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>PERUBAHAN ZNT OBJEK</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a href="{{ route('pendataan.merubah-znt.index') }}" class="btn bt-sm btn-flat btn-info"> <i class="fas fa-angle-double-left"></i> Kembali</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    @include('pendataan.znt._show',['objek'=>$objek,'data'=>$data])
</section>

@endsection
