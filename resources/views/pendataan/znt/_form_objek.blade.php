<table class="table table-sm table-bordered text-sm">
    <thead>
        <tr>
            <th style="text-align: center; width: 30px">
                <div class="form-check">
                    <input type="checkbox" name="checkall" id="checkall" onClick="check_uncheck_checkbox(this.checked);" class="form-check-input" />
                    <label for="checkall">
                    </label>
                </div>
            </th>
            <th style="text-align: center">NOP</th>
            <th style="text-align: center">Luas Bumi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($objek as $item)
        <tr>
            <td style="text-align: center">
                <input type="checkbox" name="objek[]" class="" value="{{ $item->kd_propinsi.$item->kd_dati2.$item->kd_kecamatan.$item->kd_kelurahan.$item->kd_blok.$item->no_urut.$item->kd_jns_op}}">
            </td>
            <td>
                {{ $item->kd_propinsi}}.{{ $item->kd_dati2}}.{{ $item->kd_kecamatan}}.{{ $item->kd_kelurahan}}.{{ $item->kd_blok}}-{{ $item->no_urut}}.{{ $item->kd_jns_op}}
            </td>
            <td class="text-right">
                {{ angka($item->luas_bumi)}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
    function check_uncheck_checkbox(isChecked) {
        if (isChecked) {
            $('input[type="checkbox"]').each(function() {
                this.checked = true;
            });
        } else {
            $('input[type="checkbox"]').each(function() {
                this.checked = false;
            });
        }
    }

</script>
