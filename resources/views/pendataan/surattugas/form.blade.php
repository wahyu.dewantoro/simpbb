@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Surat Tugas Pendataan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a href="{{ url('pendataan/surat-tugas/create') }}" class="btn btn-sm btn-info btn-flat"> <i class="fas fa-plus-square"></i> Tambah</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-2">
                    {{-- menampilkan error validasi --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form action="{{ $data['action'] }}" method="post">
                        @csrf
                        @method($data['method'])
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label for="" class="col-md-4">Tanggal</label>
                                    <div class="col-md-8">
                                        <input type="text" name="tanggal_mulai" class="form-control form-control-sm tanggalan" value="{{ old('tanggal_mulai')??( isset($data['surat']) ?date('d M Y',strtotime($data['surat']->tanggal_mulai)):''  ) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Pegawai</strong>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>NIP</th>
                                            <th>Pangkat/Gol</th>
                                            <th>Jabatan</th>
                                            <th>
                                                <button type="button" class="btn btn-flat btn-sm btn-success" id="tambah_pegawai"> <i class="fas fa-plus"></i></button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="row_pegawai">
                                        @if($data['method']=='patch')
                                        @foreach ($data['surat']->pegawai()->get() as $row)
                                        <tr class="row_pegawai   {{ $row->id}}">
                                            <td><input type="text" value="{{ $row->nama }}" name="nama[]" class="form-control-border form-control"></td>
                                            <td><input type="text" value="{{ $row->nip }}" name="nip[]" class="form-control-border form-control"></td>
                                            <td><input type="text" value="{{ $row->pangkat_golongan }}" name="pangkat_golongan[]" class="form-control-border form-control"></td>
                                            <td><input type="text" value="{{ $row->jabatan }}" name="jabatan[]" class="form-control-border form-control"></td>
                                            <td>
                                                <a role="button" tabindex="0" class="text-danger remove flat" data-id="{{ $row->id}}" data-tahun="{{ $row->id}}" title=" Hapus">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <strong>Objek</strong>
                                <table class="table table-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <th>NOP</th>
                                            <th>Alamat</th>

                                            <th>L. Bumi</th>
                                            <th>L. BNG</th>
                                            <th>ZNT</th>
                                            <th>
                                                <button type="button" data-toggle="modal" data-target="#form-objek" class="btn btn-flat btn-sm btn-success" id="tambah_objek"> <i class="fas fa-plus"></i></button>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="row_objek">
                                        @if($data['method']=='patch')
                                        @foreach ($data['surat']->objek()->get() as $row)
                                        <tr class="row_objek   obj_{{ $row->id}}">
                                            <input type="hidden" name="kd_propinsi[]" value="{{ $row->kd_propinsi }}">
                                            <input type="hidden" name="kd_dati2[]" value="{{ $row->kd_dati2 }}">
                                            <input type="hidden" name="kd_kecamatan[]" value="{{ $row->kd_kecamatan }}">
                                            <input type="hidden" name="kd_kelurahan[]" value="{{ $row->kd_kelurahan }}">
                                            <input type="hidden" name="kd_blok[]" value="{{ $row->kd_blok }}">
                                            <input type="hidden" name="no_urut[]" value="{{ $row->no_urut }}">
                                            <input type="hidden" name="kd_jns_op[]" value="{{ $row->kd_jns_op }}">
                                            <input type="hidden" name="nm_wp[]" value="{{ $row->nama_wp }}">
                                            <input type="hidden" name="jalan_op[]" value="{{ $row->jalan_op }}">
                                            <input type="hidden" name="blok_kav_no_op[]" value="{{ $row->blok_kav_no_op }}">
                                            <input type="hidden" name="rw_op[]" value="{{ $row->rw_op }}">
                                            <input type="hidden" name="rt_op[]" value="{{ $row->rt_op }}">
                                            <input type="hidden" name="nm_kelurahan[]" value="{{ $row->nm_kelurahan }}">
                                            <input type="hidden" name="nm_kecamatan[]" value="{{ $row->nm_kecamatan }}">
                                            <input type="hidden" name="total_luas_bumi[]" value="{{ $row->luas_bumi }}">
                                            <input type="hidden" name="total_luas_bng[]" value="{{ $row->luas_bng }}">
                                            <input type="hidden" name="kd_znt[]" value="{{ $row->kd_znt }}">
                                            <td>{{ $row->kd_propinsi }}.{{$row->kd_dati2 }}.{{$row->kd_kecamatan }}.{{$row->kd_kelurahan }}.{{$row->kd_blok }}-{{$row->no_urut }}.{{$row->kd_jns_op }}</td>
                                            <td>{{ $row->nm_kelurahan }} {{ $row->nm_kecamatan }}</td>
                                            <td>{{ $row->luas_bumi }}</td>
                                            <td>{{ $row->luas_bng }}</td>
                                            <td>{{ $row->kd_znt }}</td>
                                            <td>
                                                <a role="button" tabindex="0" class="text-danger hapusobj flat" data-id="{{ $row->id}}" data-tahun="{{ $row->id}}""  title=" Hapus">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="float-right">
                            <button class="btn btn-flat btn-sm btn-success" type="submit" id="simpanpenelitian"><i class="far fa-save"></i>
                                Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="form-objek">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header p-2">
                <h4 class="modal-title">Data Objek</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control nop" id="nop" placeholder="Masukan NOP">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-search-location"></i></span>
                        </div>
                    </div>
                    <span id="loader" class="loader d-none"></span>
                    <table id="list_data" class="table table-borderless d-none table-sm">
                        <tbody>
                            <tr class="d-none">
                                <td>kd_propinsi</td>
                                <td>:</td>
                                <td><span id="kd_propinsi"></span></td>
                            </tr>
                            <tr class="d-none">
                                <td>kd_dati2</td>
                                <td>:</td>
                                <td><span id="kd_dati2"></span></td>
                            </tr>
                            <tr class="d-none">
                                <td>kd_kecamatan</td>
                                <td>:</td>
                                <td><span id="kd_kecamatan"></span></td>
                            </tr>
                            <tr class="d-none">
                                <td>kd_kelurahan</td>
                                <td>:</td>
                                <td><span id="kd_kelurahan"></span></td>
                            </tr>
                            <tr class="d-none">
                                <td>kd_blok</td>
                                <td>:</td>
                                <td><span id="kd_blok"></span></td>
                            </tr>
                            <tr class="d-none">
                                <td>no_urut</td>
                                <td>:</td>
                                <td><span id="no_urut"></span></td>
                            </tr>
                            <tr class="d-none">
                                <td>kd_jns_op</td>
                                <td>:</td>
                                <td><span id="kd_jns_op"></span></td>
                            </tr>
                            <tr>
                                <td width="100px">Nama WP</td>
                                <td width='1px'>:</td>
                                <td><span id="nm_wp"></span></td>
                            </tr>
                            <tr>
                                <td>Jalan</td>
                                <td>:</td>
                                <td><span id="jalan_op"></span></td>
                            </tr>
                            <tr>
                                <td>Blok/Kav/No</td>
                                <td>:</td>
                                <td><span id="blok_kav_no_op"></span></td>
                            </tr>
                            <tr>
                                <td>RW</td>
                                <td>:</td>
                                <td><span id="rw_op"></span></td>
                            </tr>
                            <tr>
                                <td>RT</td>
                                <td>:</td>
                                <td><span id="rt_op"></span></td>
                            </tr>
                            <tr>
                                <td>Kelurahan</td>
                                <td>:</td>
                                <td><span id="nm_kelurahan"></span></td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td><span id="nm_kecamatan"></span></td>
                            </tr>
                            <tr>
                                <td>L. Bumi</td>
                                <td>:</td>
                                <td><span id="total_luas_bumi"></span></td>
                            </tr>
                            <tr>
                                <td>L. Bng</td>
                                <td>:</td>
                                <td><span id="total_luas_bng"></span></td>
                            </tr>
                            <tr>
                                <td>Kode ZNT</td>
                                <td>:</td>
                                <td><span id="kd_znt"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <span id="tombol_ok"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .loader {
        width: 500px;
        height: 500px;
        border-radius: 50%;
        position: relative;
        animation: rotate 1s linear infinite
    }

    .loader::before,
    .loader::after {
        content: "";
        box-sizing: border-box;
        position: absolute;
        inset: 0px;
        border-radius: 50%;
        border: 5px solid #FFF;
        animation: prixClipFix 2s linear infinite;
    }

    .loader::after {
        border-color: #FF3D00;
        animation: prixClipFix 2s linear infinite, rotate 0.5s linear infinite reverse;
        inset: 6px;
    }

    @keyframes rotate {
        0% {
            transform: rotate(0deg)
        }

        100% {
            transform: rotate(360deg)
        }
    }

    @keyframes prixClipFix {
        0% {
            clip-path: polygon(50% 50%, 0 0, 0 0, 0 0, 0 0, 0 0)
        }

        25% {
            clip-path: polygon(50% 50%, 0 0, 100% 0, 100% 0, 100% 0, 100% 0)
        }

        50% {
            clip-path: polygon(50% 50%, 0 0, 100% 0, 100% 100%, 100% 100%, 100% 100%)
        }

        75% {
            clip-path: polygon(50% 50%, 0 0, 100% 0, 100% 100%, 0 100%, 0 100%)
        }

        100% {
            clip-path: polygon(50% 50%, 0 0, 100% 0, 100% 100%, 0 100%, 0 0)
        }
    }

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        $(".nop").on("keyup", function() {
            var nop = $(this).val();
            var convert = formatnop(nop);
            $(this).val(convert);
        });

        $("#nop").on("keyup", function() {
            var obj = $(this).val().replace(/[^\d]/g, "");

            if (obj.length == 18) {
                $('#loader').removeClass('d-none');
                $('#list_data').addClass('d-none');
                var kec = obj.substr(4, 3);
                var kel = obj.substr(7, 3);
                var blok = obj.substr(10, 3);
                var no_urut = obj.substr(13, 4);
                var jns_op = obj.substr(17, 1);
                $.ajax({
                    url: "{{ url('api/data-objek-simple') }}"
                    , type: "post"
                    , data: {
                        kd_propinsi: '35'
                        , kd_dati2: '07'
                        , kd_kecamatan: kec
                        , kd_kelurahan: kel
                        , kd_blok: blok
                        , no_urut: no_urut
                        , kd_jns_op: jns_op
                    }
                    , success: function(res) {
                        $('#loader').addClass('d-none');
                        $('#list_data').removeClass('d-none');
                        if (res.status == true && res.data != null) {
                            $('#kd_propinsi').html(res.data.kd_propinsi);
                            $('#kd_dati2').html(res.data.kd_dati2);
                            $('#kd_kecamatan').html(res.data.kd_kecamatan);
                            $('#kd_kelurahan').html(res.data.kd_kelurahan);
                            $('#kd_blok').html(res.data.kd_blok);
                            $('#no_urut').html(res.data.no_urut);
                            $('#kd_jns_op').html(res.data.kd_jns_op);
                            $('#nm_wp').html(res.data.nm_wp);
                            $('#jalan_op').html(res.data.jalan_op);
                            $('#blok_kav_no_op').html(res.data.blok_kav_no_op);
                            $('#rw_op').html(res.data.rw_op);
                            $('#rt_op').html(res.data.rt_op);
                            $('#nm_kelurahan').html(res.data.nm_kelurahan);
                            $('#nm_kecamatan').html(res.data.nm_kecamatan);
                            $('#total_luas_bumi').html(res.data.total_luas_bumi);
                            $('#total_luas_bng').html(res.data.total_luas_bng);
                            $('#kd_znt').html(res.data.kd_znt);

                            button = '<button type="button" \
                                            data-kd_propinsi="' + res.data.kd_propinsi + '"\
                                    data-kd_dati2="' + res.data.kd_dati2 + '"\
                                    data-kd_kecamatan="' + res.data.kd_kecamatan + '"\
                                    data-kd_kelurahan="' + res.data.kd_kelurahan + '"\
                                    data-kd_blok="' + res.data.kd_blok + '"\
                                    data-no_urut="' + res.data.no_urut + '"\
                                    data-kd_jns_op="' + res.data.kd_jns_op + '"\
                                    data-nm_wp="' + res.data.nm_wp + '"\
                                    data-jalan_op="' + res.data.jalan_op + '"\
                                    data-blok_kav_no_op="' + res.data.blok_kav_no_op + '"\
                                    data-rw_op="' + res.data.rw_op + '"\
                                    data-rt_op="' + res.data.rt_op + '"\
                                    data-nm_kelurahan="' + res.data.nm_kelurahan + '"\
                                    data-nm_kecamatan="' + res.data.nm_kecamatan + '"\
                                    data-total_luas_bumi="' + res.data.total_luas_bumi + '"\
                                    data-total_luas_bng="' + res.data.total_luas_bng + '"\
                                    data-kd_znt="' + res.data.kd_znt + '"\
                                    id="tambah" class="btn btn-primary">Tambah</button>';
                            $('#tombol_ok').html(button);


                            $('#tambah').on('click', function(e) {
                                e.preventDefault()


                                countrow = $('#row_objek .row_objek').length;
                                rowtab = '<tr class="row_objek   obj_' + countrow + '" >\
                                                <input type="hidden" name="kd_propinsi[]" value="' + res.data.kd_propinsi + '">\
                                                <input type="hidden" name="kd_dati2[]" value="' + res.data.kd_dati2 + '">\
                                                <input type="hidden" name="kd_kecamatan[]" value="' + res.data.kd_kecamatan + '">\
                                                <input type="hidden" name="kd_kelurahan[]" value="' + res.data.kd_kelurahan + '">\
                                                <input type="hidden" name="kd_blok[]" value="' + res.data.kd_blok + '">\
                                                <input type="hidden" name="no_urut[]" value="' + res.data.no_urut + '">\
                                                <input type="hidden" name="kd_jns_op[]" value="' + res.data.kd_jns_op + '">\
                                                <input type="hidden" name="nm_wp[]" value="' + res.data.nm_wp + '">\
                                                <input type="hidden" name="jalan_op[]" value="' + res.data.jalan_op + '">\
                                                <input type="hidden" name="blok_kav_no_op[]" value="' + res.data.blok_kav_no_op + '">\
                                                <input type="hidden" name="rw_op[]" value="' + res.data.rw_op + '">\
                                                <input type="hidden" name="rt_op[]" value="' + res.data.rt_op + '">\
                                                <input type="hidden" name="nm_kelurahan[]" value="' + res.data.nm_kelurahan + '">\
                                                <input type="hidden" name="nm_kecamatan[]" value="' + res.data.nm_kecamatan + '">\
                                                <input type="hidden" name="total_luas_bumi[]" value="' + res.data.total_luas_bumi + '">\
                                                <input type="hidden" name="total_luas_bng[]" value="' + res.data.total_luas_bng + '">\
                                                <input type="hidden" name="kd_znt[]" value="' + res.data.kd_znt + '">\
                                                <td>' + res.data.kd_propinsi + '.' +
                                    res.data.kd_dati2 + '.' +
                                    res.data.kd_kecamatan + '.' +
                                    res.data.kd_kelurahan + '.' +
                                    res.data.kd_blok + '-' +
                                    res.data.no_urut + '.' +
                                    res.data.kd_jns_op + '</td>\
                                                <td>' + res.data.nm_kelurahan + ' ' + res.data.nm_kecamatan + '</td>\
                                                <td>' + res.data.total_luas_bumi + '</td>\
                                                <td>' + res.data.total_luas_bng + '</td>\
                                                <td>' + res.data.kd_znt + '</td>\
                                    <td>\
                                        <a role="button" tabindex="0" class="text-danger hapusobj flat"  data-id="' + countrow + '" data-tahun="' + countrow + '""  title="Hapus">\
                                                             <i class="fa fa-trash"></i>\
                                                         </a>\
                                    </td>\
                                </tr>';
                                $("#row_objek").append(rowtab)
                                $('#tombol_ok').html('');
                                $('#list_data').addClass('d-none');
                                $('#nop').val('');
                                $(document).on('click', '.hapusobj', function() {
                                    $("#row_objek").find('.obj_' + $(this).data('id')).remove()
                                });
                            })




                        } else {
                            $('#kd_propinsi').html('');
                            $('#kd_dati2').html('');
                            $('#kd_kecamatan').html('');
                            $('#kd_kelurahan').html('');
                            $('#kd_blok').html('');
                            $('#no_urut').html('');
                            $('#kd_jns_op').html('');
                            $('#nm_wp').html('');
                            $('#jalan_op').html('');
                            $('#blok_kav_no_op').html('');
                            $('#rw_op').html('');
                            $('#rt_op').html('');
                            $('#nm_kelurahan').html('');
                            $('#nm_kecamatan').html('');
                            $('#total_luas_bumi').html('');
                            $('#total_luas_bng').html('');
                            $('#kd_znt').html('');
                            $('#tombol_ok').html('');
                        }
                    }

                })

            } else {
                $('#kd_propinsi').html('');
                $('#kd_dati2').html('');
                $('#kd_kecamatan').html('');
                $('#kd_kelurahan').html('');
                $('#kd_blok').html('');
                $('#no_urut').html('');
                $('#kd_jns_op').html('');
                $('#nm_wp').html('');
                $('#jalan_op').html('');
                $('#blok_kav_no_op').html('');
                $('#rw_op').html('');
                $('#rt_op').html('');
                $('#nm_kelurahan').html('');
                $('#nm_kecamatan').html('');
                $('#total_luas_bumi').html('');
                $('#total_luas_bng').html('');
                $('#kd_znt').html('');
                $('#tombol_ok').html('');
            }
        });



        $('#tambah_pegawai').on('click', function(e) {
            e.preventDefault()
            countrow = $('#row_pegawai .row_pegawai').length;
            html = '<tr class="row_pegawai   ' + countrow + '" >\
                                            <td><input type="text" name="nama[]"  class="form-control-border form-control"></td>\
                                            <td><input type="text" name="nip[]"  class="form-control-border form-control"></td>\
                                            <td><input type="text" name="pangkat_golongan[]"  class="form-control-border form-control"></td>\
                                            <td><input type="text" name="jabatan[]"  class="form-control-border form-control"></td>\
                                            <td>\
                                                <a role="button" tabindex="0" class="text-danger remove flat"  data-id="' + countrow + '" data-tahun="' + countrow + '"  title="Hapus">\
                                                                     <i class="fa fa-trash"></i>\
                                                                 </a>\
                                            </td>\
                                        </tr>'
            $("#row_pegawai").append(html)
        })

        $(document).on('click', '.remove', function() {
            counttahun = $('#row_pegawai').length;
            $("#row_pegawai").find('.' + $(this).data('id')).remove()
        });

        $(document).on('click', '.hapusobj', function() {
            $("#row_objek").find('.obj_' + $(this).data('id')).remove()
        });


        $('.tanggalan').datepicker({
            format: "dd-mm-yyyy"
            , autoclose: true
            , todayHighlight: true
        });
    })

</script>
@endsection
