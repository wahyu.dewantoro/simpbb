@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Surat Tugas</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a href="{{ url('surat-tugas/create') }}" class="btn btn-sm btn-info btn-flat"> <i class="fas fa-plus-square"></i> Tambah</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">

                    <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                        <thead class="bg-info">
                            <tr>
                                <th style="text-align: center" style="width:1px !important;">No</th>
                                <th>Nomor</th>
                                <th>Tanggal Surat</th>
                                <th>Pelaksanaan</th>
                                <th>Lokasi</th>
                                <th>Pegawai</th>
                                <th>Objek</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<style>

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableObjek, tableHasil

        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ route('surat-tugas.index') }}"
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'surat_nomor'
                    , orderable: false
                    , name: 'pencarian_data'
                }
                , {
                    data: 'tanggal_indo'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'tanggal_pendataan'
                    , orderable: false
                    , searchable: false
                }

                , {
                    data: 'lokasi'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'pegawai'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'jum_objek'
                    , orderable: false
                    , searchable: false
                }

                , {
                    data: 'pilih'
                    , class: 'text-right'
                    , orderable: false
                    , searchable: false
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });

        $('#table-hasil').on('click', '.hapus', function(e) {
            // url = $(this).data('href')
            e.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data yang dihapus tidak dapat di kembalikan."
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya, Yakin!'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    // document.getElementById('delete-form-' + id).submit()
                    document.location.href = $(this).data('href');
                }
            })

        });

        // $(document).on('change', '.pencariandata', function() {
        //     pencarian();
        // });


        $('#cetak').on('click', function() {
            /* kd_kecamatan = $('#kd_kecamatan').val();
            kd_kelurahan = $('#kd_kelurahan').val();
            created_by = $('#created_by').val();
            tanggal = $('#tanggal').val();
            url_ = "{{ url('laporan/pendataan/objek-excel') }}";
            url = url_ + "?kd_kecamatan=" + kd_kecamatan + "&kd_kelurahan=" + kd_kelurahan + "&created_by=" + created_by + "&tanggal=" + tanggal;

            console.log(url);
            window.open(url, '_blank'); */
        });


        /*   function pencarian() {
              tableHasil.draw();
          } */

        $('#tanggal').datepicker({
            format: "dd-mm-yyyy"
            , autoclose: true
            , todayHighlight: true
        });
    })

</script>
@endsection
