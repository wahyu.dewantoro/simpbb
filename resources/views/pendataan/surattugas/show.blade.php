@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Surat Tugas Pendataan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a href="{{ url('pendataan/surat-tugas') }}" class="btn btn-sm btn-info btn-flat"> Kembali</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-2">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                    <tr>
                                    <tr>
                                        <td width="100px">Pelaksanaan</td>
                                        <td width="1px">:</td>
                                        <td>{{ tglindo($surat->tanggal_mulai) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Lokasi</td>
                                        <td>:</td>
                                        <td>{{ $surat->lokasi }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Surat</td>
                                        <td>:</td>
                                        <td>{{ tglindo($surat->surat_tanggal) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor Surat</td>
                                        <td>:</td>
                                        <td>{{ $surat->surat_nomor }}</td>
                                    </tr>
                                    </tr>
                                </tbody>
                            </table>
                            <a class="btn btn-success btn-sm btn-flat  float-right" href="{{ route('pendataan.surat-tugas.cetak',$surat->id) }}">
                                <i class="far fa-credit-card"></i> Surat Tugas
                            </a>

                            <a class="btn btn-primary btn-sm btn-flat  float-right" href="{{ route('pendataan.surat-tugas.lampiran',$surat->id) }}">
                                <i class="fas fa-download"></i> Objek
                            </a>
                            {{-- <button type="button" class="btn btn-primary btn-sm btn-flat float-right" style="margin-right: 5px;">
                                <i class="fas fa-download"></i> Objek
                            </button> --}}

                        </div>
                        <div class="col-md-6">
                            <div class="table-responsive p-0 " style="height: 200px;">
                                <table class="table table-bordered table-sm table-head-fixed text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>NIP</th>
                                            <th>Pangkat/Golongan</th>
                                            <th>Jabatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($surat->pegawai as $i=> $row)
                                        <tr>
                                            <td>{{ $i+1 }}</td>
                                            <td>{{ $row->nama }}</td>
                                            <td>{{ $row->nip }}</td>
                                            <td>{{ $row->pangkat_golongan }}</td>
                                            <td>{{ $row->jabatan }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <tr>
                                        <th>No</th>
                                        <th>NOP</th>
                                        <th>Alamat</th>
                                        <th>Kelurahan</th>
                                        <th>Kecamatan</th>
                                        <th>Wajib Pajak</th>
                                        <th>Kode ZNT</th>
                                        <th>L. BUMI</th>
                                        <th>L. BNG</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($surat->objek()->get() as $i=> $item)
                                    <tr>
                                        <td>{{ $i+1}}</td>
                                        <td>{{ $item->kd_propinsi }}.{{ $item->kd_dati2 }}.{{ $item->kd_kecamatan }}.{{ $item->kd_kelurahan }}.{{ $item->kd_blok }}-{{ $item->no_urut }}.{{ $item->kd_jns_op }}</td>
                                        <td>{{ $item->jalan_op }} {{ str_replace('null','',$item->blok_kav_no_op) }} {{ str_replace('null','',$item->rw_op) }} {{ str_replace('null','',$item->rt_op) }}</td>
                                        <td>{{ $item->nm_kelurahan }}</td>
                                        <td>{{ $item->nm_kecamatan }}</td>
                                        <td>{{ $item->nama_wp }}</td>
                                        <td>{{ $item->kd_znt }}</td>
                                        <td>{{ $item->luas_bumi }}</td>
                                        <td>{{ $item->luas_bng }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
