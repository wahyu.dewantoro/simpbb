<html>

<head>
    <style>
        @page {
            margin: 5mm
        }

        body {
            margin: 5mm;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12pt
        }

        table {

            font-family: "Calibri (Body)";
            font-size: 0.9 em;

        }


        #watermark {
            position: fixed;
            top: 50mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 12pt !important
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 13pt !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 9 px !important
        }

        .text-sedang {
            font-size: 11px !important
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: .9em
        }

        #header {
            top: 0;
            border-bottom: .1pt solid #aaa
        }

        #footer {
            bottom: 0;
            border-top: .1pt solid #aaa
        }

        .page-number:before {
            content: "Page "counter(page)
        }

        p {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13pt
        }

    </style>

</head>

<body>
    <div id="watermark"><img src="{{ public_path('kabmalang.png') }}"></div>
    <table width="100%" style="font-size: .875rem !important ">
        <tr>
            <th width="70px"><img style="-webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
                filter: grayscale(100%);" width="60px" height="70px" src="{{ public_path('kabmalang_black.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>
                    <span style="font-size: 18pt">BADAN PENDAPATAN DAERAH</span>
                    <br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            <th width="100px"></th>
        </tr>
    </table>
    <hr style="border-color: black">
    <p style="font-size: 12pt" class="text-tengah"><u><strong>LAMPIRAN SURAT PERINTAH TUGAS</strong></u><br>
        Nomor : {{ $st->surat_nomor }}</p>

    <table class="table table-bordered">
        <thead class="bg-info">
            <tr>
                <th>No</th>
                <th>NOP</th>
                <th>Alamat</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>Wajib Pajak</th>
                <th>Kode ZNT</th>
                <th>L. BUMI</th>
                <th>L. BNG</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($st->objek()->get() as $i=> $item)
            <tr>
                <td style="text-align: center">{{ $i+1}}</td>
                <td>{{ $item->kd_propinsi }}.{{ $item->kd_dati2 }}.{{ $item->kd_kecamatan }}.{{ $item->kd_kelurahan }}.{{ $item->kd_blok }}-{{ $item->no_urut }}.{{ $item->kd_jns_op }}</td>
                <td>{{ $item->jalan_op }} {{ str_replace('null','',$item->blok_kav_no_op) }} {{ str_replace('null','',$item->rw_op) }} {{ str_replace('null','',$item->rt_op) }}</td>
                <td>{{ $item->nm_kelurahan }}</td>
                <td>{{ $item->nm_kecamatan }}</td>
                <td>{{ $item->nama_wp }}</td>
                <td>{{ $item->kd_znt }}</td>
                <td style="text-align: center">{{ angka($item->luas_bumi) }}</td>
                <td style="text-align: center">{{ angka($item->luas_bng) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <table border="0" width="100%">
        <tr style="vertical-align: top">
            <td rowspan="2" width="600px">&nbsp;</td>
            <td rowspan='2' width="100px">&nbsp; </td>
            <td width="400px">
                <table>
                    <tr>
                        <td>Dikelurakan di</td>
                        <td>: {{ $st->surat_kota }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>: {{ tglIndo($st->surat_tanggal)  }}</td>
                    </tr>
                </table>
                <hr style="border-color: black">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <p class="text-center" style="font-size: 12pt">
                    a.n. KEPALA BADAN PENDAPATAN DAERAH <br>KABUPATEN MALANG<br>
                    Sekretaris<br>
                    u.b.<br>
                    <b>{{ $st->surat_jabatan }}</b>
                </p>
                <br><br>
                <p><strong><u>{{ $st->surat_pegawai }}</u></strong><br>{{ $st->surat_pangkat }}<br>
                    NIP. {{ $st->surat_nip }}
                </p>
            </td>
        </tr>
    </table>
</body>

</html>
