<html>

<head>
    <style>
        @page {
            margin: 5mm
        }

        body {
            margin: 5mm;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12pt;
            min-height: 300vh;
            position: relative;
            /* min-height: 300vh;
            position: relative;
            margin: 0; */
        }

        body:before {
            content: "";
            position: absolute;
            z-index: 9999;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background:
                url('data:image/svg+xml;utf8,<svg style="transform:rotate(-45deg)" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 50 60"><text x="0" y="25" fill="%23000">Lorem </text></svg>') 0 0/100% 100vh;
        }

        #watermark {
            position: fixed;
            top: 50mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 12pt !important
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 13pt !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 9 px !important
        }

        .text-sedang {
            font-size: 11px !important
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: .9em
        }

        #header {
            top: 0;
            border-bottom: .1pt solid #aaa
        }

        #footer {
            bottom: 0;
            border-top: .1pt solid #aaa
        }

        .page-number:before {
            content: "Page "counter(page)
        }

        p {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13pt
        }

        
    </style>

</head>

<body>
    {{-- <div cass="watermark"><img src="{{ public_path('kabmalang.png') }}"></div> --}}

    <table width="100%" style="font-size: .875rem !important ">
        <tr>
            <th width="70px"><img style="-webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
                filter: grayscale(100%);" width="60px" height="70px" src="{{ public_path('kabmalang_black.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>
                    <span style="font-size: 18pt">BADAN PENDAPATAN DAERAH</span>
                    <br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            <th width="100px"></th>
        </tr>
    </table>
    <hr style="border-color: black">
    <p style="font-size: 12pt" class="text-tengah"><u><strong>DRAFT PENDATAAN OBJEK</strong></u></p>

    <table class="table table-sm table-borderless">
        <tbody>
            <tr>
                <td width="100px">Nomor</td>
                <td width="1px">:</td>
                <td>{{ $batch->nomor_batch }}</td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>:</td>
                <td>{{ tglIndo($batch->tanggal) }}</td>
            </tr>
            <tr>
                <td>Pendata </td>
                <td>:</td>
                <td>{{ $batch->pegawai_pendata }}</td>
            </tr>
            <tr>
                <td>Pendataan</td>
                <td>:</td>
                <td>{{ $batch->nama_pendataan }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table table-sm table-bordered table-hover">
        <thead class="bg-info">
            <tr>
                <th class="text-center"></th>
                @if ($batch->verifikasi_kode=='1')
                <th class="text-center">NOP</th>
                @else
                <th class="text-center">Kecamatan</th>
                <th class="text-center">Kelurahan</th>
                <th class="text-center">Blok</th>
                @endif

                <th class="text-center">Wajib Pajak</th>
                <th class="text-center">Jalan</th>
                <th class="text-center">Blok/Kav/No</th>
                <th class="text-center">RT</th>
                <th class="text-center">RW</th>
                <th class="text-center">ZNT</th>
                <th class="text-center">Luas Bumi</th>
                <th class="text-center">NIR</th>
                <th class="text-center">Jenis Bumi</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($objek as $i=> $row)

            @php
            $ck=onlyNUmber($row->kd_propinsi.$row->kd_dati2.$row->kd_kecamatan.$row->kd_kelurahan.$row->kd_blok.$row->no_urut.$row->kd_jns_op);
            $induk=0;
            if(onlyNumber($row->nop_asal)==$ck){
            $induk=1;
            }

            @endphp

            <tr>
                <td class="text-center">
                    {{$i+1}}
                </td>
                @if ($batch->verifikasi_kode=='1')
                <td>{{ formatnop($row->spop->nop_proses) }}</td>
                @else
                <td>{{$row->nm_kecamatan }}</td>
                <td>{{$row->nm_kelurahan }}</td>
                <td>{{$row->kd_blok }}</td>
                @endif
                <td>{{$row->nm_wp }}</td>
                <td>{{$row->jalan_op }}</td>
                <td>{{$row->blok_kav_no_op }}</td>
                <td>{{$row->rt_op }}</td>
                <td>{{$row->rw_op }}</td>
                <td>{{$row->kd_znt }}</td>
                <td class="text-center">{{$row->luas_bumi }}</td>
                <td class="text-right">{{ angka($row->nir) }}</td>
                <td>{{ jenisBumi($row->jns_bumi) }}</td>
                <td>
                    @if ($induk==1)
                    Induk
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
