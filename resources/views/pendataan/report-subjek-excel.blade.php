<html>
<body>


    <table>
        <thead>
            <tr>
                <th colspan="16"><b>LAPORAN PENDATAAN SUBJEK PAJAK</b></th>
            </tr>

            <tr>
                <th colspan="2">kecamatan</th>
                <th colspan="14">: {{ $param['nm_kec']<>''?$param['nm_kec']:'All' }}</th>
            </tr>
            <tr>
                <th colspan="2">Kelurahan</th>
                <th colspan="14">: {{ $param['nm_kel']<>''?$param['nm_kel']:'All' }}</th>
            </tr>
            <tr>
                <th colspan="2">Pendata</th>
                <th colspan="14">: {{ $param['nm_pendata']<>''?$param['nm_pendata']:'All' }}</th>
            </tr>
            <tr>
                <th colspan="2">Tanggal</th>
                <th colspan="14">: {{ $param['tanggal']<>''?tglindo($param['tanggal']):'All' }}</th>
            </tr>
            <tr>
                <th colspan="16"></th>
            </tr>

            <tr>
                <th>No</th>
                <th>Formulir</th>
                <th>NOP</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>RT</th>
                <th>RW</th>
                <th>Kelurahan</th>
                <th>Kecamatan</th>
                <th>Kota / Kabupaten</th>
                <th>Propinsi</th>
                <th>Kode POS</th>
                <th>Telepon</th>
                <th>Pendata</th>
                <th>Tanggal</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $i=>$row)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ $row->no_formulir}}</td>
                <td>{{ formatnop($row->nop_proses)}}</td>
                <td>{{ $row->subjek_pajak_id}}</td>
                <td>{{ $row->nm_wp}}</td>
                <td>{{ $row->jalan_wp.' '.$row->blok_kav_no_wp }}</td>
                <td>{{ $row->rt_wp}}</td>
                <td>{{ $row->rw_wp}}</td>
                @php
                $kk=explode('-', $row->kelurahan_wp);
                $dd=explode('-', $row->kota_wp);
                @endphp
                <td>{{ $kk[0]}}</td>
                <td>{{ $kk[1]??null}}</td>
                <td>{{ $dd[0]}}</td>
                <td>{{ $dd[1]??null}}</td>>
                <td>{{ $row->kd_pos_wp}}</td>
                <td>{{ $row->telp_wp}}</td>
                <td>{{ $row->created_name}}</td>
                <td>{{ tglindo($row->created_at)}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
