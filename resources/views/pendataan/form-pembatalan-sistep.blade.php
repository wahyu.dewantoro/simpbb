@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pembatalan Objek Sistep</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" action="{{ url('pendataan/pembatalan-sistep') }}" method="POST"
                        id="myform" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-4">
                                @csrf
                                <div class="form-group">
                                    <label for="no_surat">No Surat</label>
                                    <input type="text" name="no_surat" id="no_surat"
                                        class="form-control form-control-sm" required>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_surat">Tanggal Surat</label>
                                    <input type="text" name="tgl_surat" id="tgl_surat"
                                        class="form-control form-control-sm tanggal" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Kecmatan</label>
                                    <select class="form-control form-control-sm select2" name="kd_kecamatan" required
                                        id="kd_kecamatan" data-placeholder="Pilih kecamatan">
                                        <option value=""></option>
                                        @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Kelurahan</label>
                                    <select class="form-control form-control-sm select2" name="kd_kelurahan"
                                        id="kd_kelurahan" data-placeholder="Desa / Kelurahan">
                                        <option value=""></option>
                                    </select>
                                    <input type="hidden" name="jns_pendataan_id" id="jns_pendataan_id"
                                        value="{{ $jns_pendataan_id }}">
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_objek">Jumlah Objek</label>
                                    <input type="text" readonly class="form-control form-control-sm" name="jumlah_objek"
                                        id="jumlah_objek" value='0'>
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-flat btn-sm btn-success" type="submit"><i
                                            class="far fa-save"></i>
                                        Submit</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{-- <div id="preview"></div> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </section>
@endsection
@section('css')
    <style type="text/css">
        /* #btn_b, */
        /* #data_subjek, */
        #data_bangunan {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            var headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

            $.LoadingOverlaySetup({
                background: "rgba(0, 0, 0, 0.5)",
                image: '',
                fontawesome: 'far fa-hourglass fa-spin',
                // imageAnimation: "1.5s fadein",
                // text        : "Sedang mencari...",
                imageColor: "#8080c0"
            });
            // $.LoadingOverlay("show");
            $(document).ajaxSend(function(event, jqxhr, settings) {
                $.LoadingOverlay("show");
            });
            $(document).ajaxComplete(function(event, jqxhr, settings) {
                $.LoadingOverlay("hide");
            });

            $(".select2").select2({
                closeOnSelect: true,
                allowClear: true
            })

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            function getKelurahan(kk) {
                var html = '<option value=""></option>';
                $('#kd_kelurahan').html(html)
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            $.each(res, function(key, value) {
                                var newOption = new Option(key + '-' + value, key, true, true);
                                $('#kd_kelurahan').append(newOption)
                            });

                            $('#kd_kelurahan').val(null)
                        },
                        error: function(res) {

                        }
                    });
                }

            }




            function getPreviewObjek(kd_kecamatan, kd_kelurahan) {
                $('#jumlah_objek').val('0')
                $.ajax({
                    url: "{{ url('pendataan/pembatalan-sistep') }}",
                    data: {
                        kd_kecamatan,
                        kd_kelurahan
                    },
                    success: function(res) {
                        $('#jumlah_objek').val(res)
                    }
                })
            }


            $('#kd_kecamatan,#kd_kelurahan').on('change', function() {
                kd_kecamatan = $('#kd_kecamatan').val()
                kd_kelurahan = $('#kd_kelurahan').val()

                if (kd_kecamatan != '' && kd_kelurahan != '') {
                    getPreviewObjek(kd_kecamatan, kd_kelurahan)
                }
            })

            $('#kd_kelurahan').trigger('change')
        });
    </script>
@endsection
