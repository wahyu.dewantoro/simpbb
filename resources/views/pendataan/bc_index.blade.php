@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Pendataan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a href="{{ url('pendataan/create') }}" class="btn btn-sm btn-info btn-flat"> <i class="far fa-plus-square"></i> Tambah</a>
                    <a href="{{ url('pendataan/import') }}" class="btn btn-sm btn-success btn-flat"> <i class="fas fa-file-upload"></i> Upload</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">
                    <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                        <thead class="bg-danger">
                            <tr>
                                <th style="text-align: center" rowspan="2" style="width:1px !important;">No</th>
                                <th style="text-align: center" rowspan="2">Formulir</th>
                                <th style="text-align: center" rowspan="2">NOP</th>
                                <th style="text-align: center" rowspan="2">Wajib Pajak</th>
                                <th style="text-align: center" rowspan="2">ZNT</th>
                                <th style="text-align: center" colspan="2" width="50px">Bumi</th>
                                <th style="text-align: center" colspan="2" width="50px">Bangunan</th>
                                <th style="text-align: center" rowspan="2"></th>
                            </tr>
                            <tr>
                                <th style="text-align: center">Luas</th>
                                <th style="text-align: center">NJOP</th>
                                <th style="text-align: center">Luas</th>
                                <th style="text-align: center">NJOP</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl" style="max-width: 98%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Objek Pajak</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Objek</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0 m-0">
            </div>
        </div>
    </div>
</div>
<style>
    table tbody {
        text-transform: uppercase;
    }

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {
        var tableObjek, tableHasil
        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: "{{  url('pendataan') }}"
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'noformulir'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , name: 'pencarian_data'
                    // , searchable: false
                }
                , {
                    data: 'nm_wp'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'kd_znt'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'total_luas_bumi'
                    , orderable: false
                    , class: 'text-center'
                    , render: function(data, type, row) {
                        return formatRupiah(data);
                    }
                    , searchable: false
                }
                , {
                    data: 'njop_bumi'
                    , orderable: false
                    , class: 'text-right'
                    , render: function(data, type, row) {
                        return formatRupiah(data);
                    }
                    , searchable: false
                }
                , {
                    data: 'total_luas_bng'
                    , orderable: false
                    , class: 'text-center'
                    , render: function(data, type, row) {
                        return formatRupiah(data);
                    }
                    , searchable: false
                }
                , {
                    data: 'njop_bng'
                    , class: 'text-right'
                    , orderable: false
                    , render: function(data, type, row) {
                        return formatRupiah(data);
                    }
                    , searchable: false
                }
                , {
                    data: 'pilih'
                    , class: 'text-right'
                    , orderable: false
                    , searchable: false
                }
            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });

        function nopDetail(nop) {
            // console.log(nop)
            $('.modal-body').html('')
            openloading();
            $.ajax({
                url: "{{ url('informasi/objek-pajak-show') }}"
                , data: {
                    nop: nop
                }
                , success: function(res) {
                    $('.modal-body').html(res)
                    closeloading()
                    $("#modal-xl").modal('show');
                }
                , error: function(e) {
                    closeloading()
                    Swal.fire({
                        icon: 'error'
                        , title: 'Peringatan'
                        , text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.'
                        , allowOutsideClick: false
                        , allowEscapeKey: false
                    , })
                    $('.modal-body').html('')
                }
            });
        }

        $('#table-hasil').on('click', '.cetak-sk', function(e) {
            e.preventDefault()
            id = $(this).data('id')
            url = $(this).data('url')
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return true;
        })

        $('#table-hasil').on('click', '.nop-detail', function(e) {
            e.preventDefault()
            nop=$(this).data('nop')
            nopDetail(nop)
        })
    })

</script>
@endsection
