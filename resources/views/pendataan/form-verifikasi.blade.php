@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Verifikasi Pendataan</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    {{-- <a href="{{ url('pendataan/create') }}" class="btn btn-sm btn-info btn-flat"> <i class="far fa-plus-square"></i> Tambah</a>
                    <a href="{{ url('pendataan/import') }}" class="btn btn-sm btn-success btn-flat"> <i class="fas fa-file-upload"></i> Upload</a> --}}
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="invoice p-3 mb-3">
            <form action="{{ route('pendataan.verifikasi.store') }}" method="post" id="form-verifikasi">
                <div class="row">
                    <div class="col-md-4">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td width="100px">Nomor</td>
                                    <td width="1px">:</td>
                                    <td>{{ $batch->nomor_batch }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td>:</td>
                                    <td>{{ tglIndo($batch->tanggal) }}</td>
                                </tr>
                                <tr>
                                    <td>Pendata </td>
                                    <td>:</td>
                                    <td>{{ $batch->pegawai_pendata }}</td>
                                </tr>
                                <tr>
                                    <td>Pendataan</td>
                                    <td>:</td>
                                    <td>{{ $batch->nama_pendataan }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">

                        @method('post')
                        @csrf
                        <input type="hidden" value="{{ $batch->id }}" name="id">
                        <div class="row">
                            <div class="col-6">
                                <label for="">Status Verifikasi</label>
                            </div>
                            <div class="col-6">
                                <select name="verifikasi_kode" id="verifikasi_kode" required class="form-control form-control-sm">
                                    <option value="">Pilih</option>
                                    <option value="1">Disetujui</option>
                                    <option value="0">Ditolak</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="verifikasi_deskripsi" id="verifikasi_deskripsi" class="form-control form-control-sm" rows="2" placeholder="Keterangan verifikasi"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-flat btn-sm btn-success" type="submit" id="simpanpenelitian"><i class="far fa-save"></i>
                                Save</button>

                            <a href="{{ route('pendataan.verifikasi') }}" class="btn btn-sm btn-flat btn-warning"><i class="far fa-window-close"></i> Batal</a>

                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-sm table-bordered table-hover">
                            <thead class="bg-info">
                                <tr>
                                    <th class="text-center"></th>
                                    @if ($batch->verifikasi_kode=='1' || $batch->jenis_pendataan_id=='3')
                                    <th class="text-center">NOP</th>
                                    @else
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">Blok</th>
                                    @endif
                                    <th class="text-center">Wajib Pajak</th>
                                    <th class="text-center">Jalan</th>
                                    <th class="text-center">Blok/Kav/No</th>
                                    <th class="text-center">RT</th>
                                    <th class="text-center">RW</th>
                                    <th class="text-center">ZNT</th>
                                    <th class="text-center">Luas Bumi</th>
                                    <th class="text-center">Luas BNG</th>
                                    <th class="text-center">NIR</th>
                                    <th class="text-center">Jenis Bumi</th>
                                    <th>Keterangan</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($objek as $i=> $row)

                                @php
                                $ck=onlyNUmber($row->kd_propinsi.$row->kd_dati2.$row->kd_kecamatan.$row->kd_kelurahan.$row->kd_blok.$row->no_urut.$row->kd_jns_op);
                                $induk=0;
                                if(onlyNumber($row->nop_asal)==$ck){
                                $induk=1;
                                }

                                @endphp
                                <tr>
                                    <td> <input class="marking" type="checkbox" name="mark_tolak[]" value='{{ $row->id }}'> </td>
                                    @if ($batch->verifikasi_kode=='1')
                                    <td> @if(($row->spop->nop_proses??'')!='') {{ formatnop($row->spop->nop_proses??formatnop($row->nop_asal)) }} @endif</td>
                                    @elseif($batch->jenis_pendataan_id=='3')
                                    <td>{{ formatnop($row->nop_asal) }}</td>
                                    @else
                                    <td>{{$row->nm_kecamatan }}</td>
                                    <td>{{$row->nm_kelurahan }}</td>
                                    <td>{{$row->kd_blok }}</td>
                                    @endif
                                    <td>{{$row->nm_wp }}</td>
                                    <td>{{$row->jalan_op }}</td>
                                    <td>{{$row->blok_kav_no_op }}</td>
                                    <td>{{$row->rt_op }}</td>
                                    <td>{{$row->rw_op }}</td>
                                    <td>{{$row->kd_znt }}</td>
                                    <td class="text-center">{{$row->luas_bumi }}</td>
                                    <td class="text-center">{{$row->luas_bng }}</td>
                                    <td class="text-right">{{ angka($row->nir) }}</td>
                                    <td>{{ jenisBumi($row->jns_bumi) }}</td>
                                    <td>
                                        @if ($induk==1)
                                        Induk ,
                                        @endif
                                        SPPT : {{ $row->penetapan()->selectraw(("LISTAGG(mulai_tahun||', HKPD: '||hkpd_individu||'%', ',') WITHIN GROUP (ORDER BY mulai_tahun) AS mulai_tahun"))->first()->mulai_tahun }}, Edit: {{ $row->count_edit }} x
                                    </td>
                                    <td class="text-center">
                                        {{-- <a role="button" class="detail-objek" data-id="{{ $row->id }}"><i class='fas text-success fa-binoculars'></i></a> --}}
                                        <a role="button" class="detail-objek" data-id="{{ $row->id }}" data-induk="{{ $induk }}" data-kecamatan_wp="{{ $row->kecamatan_wp }}" data-propinsi_wp="{{ $row->propinsi_wp }}" data-kd_blok="{{ $row->kd_blok }}" data-nm_wp="{{ $row->nm_wp }}" data-no_persil="{{ $row->no_persil }}" data-jalan_op="{{ $row->jalan_op }}" data-blok_kav_no_op="{{ $row->blok_kav_no_op }}" data-rw_op="{{ $row->rw_op }}" data-rt_op="{{ $row->rt_op }}" data-kd_znt="{{ $row->kd_znt }}" data-luas_bumi="{{ $row->luas_bumi }}" data-jns_bumi="{{ jenisBumi($row->jns_bumi) }}" data-nm_kecamatan="{{ $row->nm_kecamatan }}" data-nm_kelurahan="{{ $row->nm_kelurahan }}" data-nir="{{ $row->nir }}" data-nop_asal="{{ $row->nop_asal }}" data-subjek_pajak_id="{{ $row->subjek_pajak_id }}" data-jalan_wp="{{ $row->jalan_wp }}" data-blok_kav_no_wp="{{ $row->blok_kav_no_wp }}" data-rw_wp="{{ $row->rw_wp }}" data-rt_wp="{{ $row->rt_wp }}" data-kelurahan_wp="{{ $row->kelurahan_wp }}" data-kota_wp="{{ $row->kota_wp }}" data-kd_pos_wp="{{ $row->kd_pos_wp }}" data-telp_wp="{{ $row->telp_wp }}" data-npwp="{{ $row->npwp }}" data-status_pekerjaan_wp="{{ jenisPekerjaan($row->status_pekerjaan_wp) }}" data-no_persil="{{ $row->no_persil }}" data-kd_status_cabang="{{ $row->kd_status_cabang }}" data-kd_status_wp="{{ statusWP($row->kd_status_wp) }}"><i class='fas text-success fa-binoculars'></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl" style="max-width: 98%">
        <div class="modal-content">
            <div class="modal-header p-2">
                <h4 class="modal-title">Detail Objek Pajak</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    $(document).ready(function() {
        enable_cb($('#verifikasi_kode').val())
        $('#verifikasi_kode').on('change', function(e) {
            enable_cb($('#verifikasi_kode').val())
        })

        // enable_cb()
        function enable_cb(kode) {
            if (kode == '0') {
                $("#verifikasi_deskripsi").attr("required", true);
                $("input.marking").removeAttr("disabled");
            } else {
                $("input.marking").removeAttr('checked');
                $("input.marking").attr("disabled", true);
                $("#verifikasi_deskripsi").attr("required", false);
            }
        }

        $('.detail-objek').on('click', function(e) {
            openloading()
            e.preventDefault()
            id = $(this).data('id');
            $('.modal-body').html("Sistem sedang berjalan, mohon ditunggu!");
            $.ajax({
                url: "{{ url('pendataan/show-text') }}/" + id
                , success: function(res) {
                    closeloading()
                    $('.modal-body').html(res);
                    $("#modal-xl").modal('show');

                }
                , error: function(res) {
                    closeloading()
                    $('.modal-body').html('Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.');
                    $("#modal-xl").modal('show');
                }
            })
        })

        $("#form-verifikasi").on("submit", function(event) {
            openloading();
            event.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?'
                , text: "data akan segera di proses ketika di klik YA"
                , icon: 'warning'
                , showCancelButton: true
                , confirmButtonColor: '#3085d6'
                , cancelButtonColor: '#d33'
                , confirmButtonText: 'Ya'
                , cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    openloading()
                    $.ajax({
                        url: "{{ route('pendataan.verifikasi.store') }}"
                        , type: 'post'
                        , data: $(this).serialize()
                        , success: function(res) {
                            // closeloading();
                            // countobjek();
                            if (res.status == '1') {
                                toastr.success('Berhasil', res.msg);
                            } else {
                                toastr.error('Gagal', res.msg);

                            }
                            window.location.href = "{{route('pendataan.verifikasi')}}"
                        }
                        , error: function(res) {
                            closeloading();
                            toastr.error('Error', 'Gagal memverifikasi pendataan objek');
                        }
                    })
                }
            })
        });
    })

</script>
@endsection
