@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Laporan Pendataan Subjek Pajak</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="col-12 ">
            <div class="card card-primary card-tabs">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-md-3">
                            <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan" required id="kd_kecamatan">
                                @if (count($kecamatan) > 1)
                                <option value="">-- Kecamatan --</option>
                                @endif
                                @foreach ($kecamatan as $rowkec)
                                <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                    {{ $rowkec->kd_kecamatan.' - '. $rowkec->nm_kecamatan }}
                                </option>
                                @endforeach
                            </select>
                            <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="kd_kelurahan" id="kd_kelurahan">
                                <option value="">-- Kelurahan / Desa -- </option>
                            </select>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <select class="pencariandata form-control form-control-sm mb-2 mr-sm-2" name="created_by" required id="created_by">
                                <option value="">-- Pegawai --</option>
                                @foreach ($creators as $rc)
                                <option value="{{ $rc->created_by }}">{{ $rc->created_name }}</option>
                                @endforeach
                            </select>

                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control pencariandata" id="tanggal" placeholder="Tanggal pendataan">
                                <span class="input-group-append">
                                    <button id="cetak" class="btn btn-sm btn-success"><i class="far fa-file-excel"></i> Cetak</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm table-bordered table-hover " id="table-hasil" style="width:100%">
                            <thead class="bg-danger">
                                <tr>
                                    <th style="text-align: center" style="width:1px !important;">No</th>
                                    <th style="text-align: center">Formulir</th>
                                    <th style="text-align: center">NOP</th>
                                    {{-- <th style="text-align: center">NIK</th> --}}
                                    <th style="text-align: center">Nama</th>
                                    <th style="text-align: center">Alamat</th>
                                    <th style="text-align: center">RT</th>
                                    <th style="text-align: center">RW</th>
                                    <th style="text-align: center">Kelurahan</th>
                                    <th style="text-align: center">Kecamatan</th>
                                    <th style="text-align: center">Kota / Kabupaten</th>
                                    <th style="text-align: center">Propinsi</th>
                                    <th style="text-align: center">Kode POS</th>
                                    <th style="text-align: center">Telepon</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    table tbody {
        text-transform: uppercase;
    }

</style>
@endsection

@section('script')
<script src="{{ asset('js') }}/wilayah.js"></script>
<script>
    $(document).ready(function() {

        $('#kd_kecamatan').on('change', function() {
            var kk = $('#kd_kecamatan').val();
            getKelurahan(kk);
        })

        var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
        if (kd_kecamatan == '') {
            var kd_kecamatan = $('#kd_kecamatan').val()
        }
        getKelurahan(kd_kecamatan);

        function getKelurahan(kk) {
            var html = '<option value="">-- Kelurahan / Desa --</option>';
            if (kk != '') {
                $.ajax({
                    url: "{{ url('desa') }}"
                    , data: {
                        'kd_kecamatan': kk
                    }
                    , success: function(res) {
                        var count = Object.keys(res).length;
                        if (count == 1) {
                            html = '';
                        }
                        $.each(res, function(k, v) {
                            var apd = '<option value="' + k + '">' + k + ' - ' + v + '</option>';
                            html += apd;
                            if (count == 1) {
                                $('#kd_kelurahan').val(k);
                            }
                        });
                        // console.log(res);
                        $('#kd_kelurahan').html(html);
                        if (count != 1) {
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        }
                    }
                    , error: function(res) {
                        $('#kd_kelurahan').html(html);
                    }
                });
            } else {
                $('#kd_kelurahan').html(html);
            }

        }

        var tableObjek, tableHasil;
        $.extend($.fn.dataTable.defaults, {
            searching: false
            , ordering: false
            , select: false
            , lengthChange: false
        });
        tableHasil = $('#table-hasil').DataTable({
            processing: true
            , serverSide: true
            , orderable: false
            , ajax: {
                url: "{{ route('laporan.pendataan.subjek') }}"
                , data: function(d) {
                    d.kd_kecamatan = $('#kd_kecamatan').val();
                    d.kd_kelurahan = $('#kd_kelurahan').val();
                    d.created_by = $('#created_by').val();
                    d.tanggal = $('#tanggal').val();
                }
            }
            , columns: [{
                    data: null
                    , class: 'text-center'
                    , orderable: false
                    , render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                    , searchable: false
                }
                , {
                    data: 'noformulir'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'nop'
                    , orderable: false
                    , name: 'pencarian_data'
                }
                , {
                    data: 'nm_wp'
                    , orderable: false
                    , searchable: false
                    , render: function(data, type, row) {
                        return data + '<br><span class="text-danger">' + row.subjek_pajak_id + '</span>'
                    }
                }
                , {
                    data: 'jalan_wp'
                    , orderable: false
                    , searchable: false
                    , render: function(data, type, row) {
                        if (row.blok_kav_no_wp != 'NULL') {
                            data += ' ' + row.blok_kav_no_wp;
                        }

                        return data;
                    }
                }
                , {
                    data: 'rt_wp'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'rw_wp'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'kelurahan'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'kecamatan'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'kota'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'propinsi'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'kd_pos_wp'
                    , orderable: false
                    , searchable: false
                }
                , {
                    data: 'telp_wp'
                    , orderable: false
                    , searchable: false
                }

            ]
            , aLengthMenu: [
                [10, 20, 50, 75, -1]
                , [10, 20, 50, 75, "Semua"]
            ]
            , "order": []
            , "columnDefs": [{
                "targets": 'no-sort'
                , "orderable": false
            , }]
            , iDisplayLength: 10
            , rowCallback: function(row, data, index) {}
        });



        $('.pencariandata').on('change', function(e) {
            pencarian();
        });


        $('#cetak').on('click', function() {
            kd_kecamatan = $('#kd_kecamatan').val();
            kd_kelurahan = $('#kd_kelurahan').val();
            created_by = $('#created_by').val();
            tanggal = $('#tanggal').val();
            url_ = "{{ url('laporan/pendataan/subjek-excel') }}";
            url = url_ + "?kd_kecamatan=" + kd_kecamatan + "&kd_kelurahan=" + kd_kelurahan + "&created_by=" + created_by + "&tanggal=" + tanggal;

            console.log(url);
            window.open(url, '_blank');
        });


        function pencarian() {
            // er.preventDefault();
            tableHasil.draw();

        }

        $('#tanggal').datepicker({
            format: 'dd-mm-yyyy'
            , autoHide: true
        });
    })

</script>
@endsection
