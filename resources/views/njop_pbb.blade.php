<table>
    <tr>
        <th>NOP</th>
        <th>WAJIB PAJAK</th>
        <th>LUAS BUMI</th>
        <th>LUAS BNG</th>
        <th>NJOP_BUMI/METER</th>
        <th>NJOP_BNG/METER</th>
        <th>TOTAL_NJOP</th>
    </tr>
    @foreach ($data as $row)
    <tr>
        <td>{{ $row->nop }}</td>
        <td>{{ $row->nm_wp_sppt }}</td>
        <td>{{ $row->luas_bumi_sppt }}</td>
        <td>{{ $row->luas_bng_sppt }}</td>
        <td>{{ $row->njop_bumi_meter }}</td>
        <td>{{ $row->njop_bng_meter }}</td>
        <td>{{ $row->total_njop }}</td>
    </tr>
    @endforeach
</table>
