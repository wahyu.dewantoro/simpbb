@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-10">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-2">
                    <div class="float-sm-right">
                        
                        <a href="{{ route('refrensi.lokasiobjek.index') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-angle-double-left"></i> Kembali
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body p-2">
                            <form action="{{ $data['action'] }}" method="post">
                                @csrf
                                @method($data['method'])
                                <div class="form-group">
                                    <label for="">Nama Lokasi</label>
                                    <input type="text" value="{{ $data['objek']->nama_lokasi ?? '' }}"
                                        name="nama_lokasi" id="nama_lokasi" class="form-control" required>
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-float btn-info"><i class="fas fa-save"></i> Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
