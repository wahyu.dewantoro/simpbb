<div class="form-group">
    <label for="{{ $id }}">{{ $label }} <span
            class="text-danger">{{ $required ? '*' : '' }}</span></label>
    <textarea id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder ?? '' }}"
        class="form-control @error($name) is-invalid @enderror" {{ $required ? 'required' : '' }}>{{ old($name) }}</textarea>
    @error($name)
        <span class="invalid-feedback">{{ $message }}</span>
    @enderror
</div>
