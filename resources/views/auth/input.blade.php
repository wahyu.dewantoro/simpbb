<div class="form-group">
    <label for="{{ $id }}">{{ $label }} <span
            class="text-danger">{{ $required ? '*' : '' }}</span></label>
    <input type="{{ $type ?? 'text' }}" id="{{ $id }}" name="{{ $name }}"
        placeholder="{{ $placeholder ?? '' }}" class="form-control @error($name) is-invalid @enderror"
        value="{{ old($name) }}" {{ $required ? 'required' : '' }}>
    @error($name)
        <span class="invalid-feedback">{{ $message }}</span>
    @enderror
</div>
