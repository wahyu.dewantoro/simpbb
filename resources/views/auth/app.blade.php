<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo/favicon.ico') }}" />
    <title>@yield('title', 'SIMPBB P2')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lte/plugins/toastr/toastr.min.css') }}">
    <style>
        .invalid-feedback {
            display: block;
        }

        .register-page {
            background-color: #f4f6f9;
        }
    </style>
    @stack('styles')
</head>

<body class="hold-transition register-page">
    <div class="container">
        @yield('content')
    </div>

    <!-- Toast Messages -->
    <span id="msg-success" data-msg="{{ session('success') }}"></span>
    <span id="msg-error" data-msg="{{ session('error') }}"></span>

    <!-- Scripts -->
    <script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('lte/plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "timeOut": "5000"
            };

            // Display Toast Messages
            const successMsg = $('#msg-success').data('msg');
            const errorMsg = $('#msg-error').data('msg');

            if (successMsg) toastr.success(successMsg);
            if (errorMsg) toastr.error(errorMsg);
        });
    </script>
    @stack('scripts')
</body>

</html>
