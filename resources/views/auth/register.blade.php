@extends('auth.app')

@section('title', 'Form Registrasi')

@section('content')
    <div class="row justify-content-md-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-bold">Form Registrasi</h3>
                </div>
                <div class="card-body">
                    @if (session('pesan'))
                        <div class="alert alert-success">{{ session('pesan') }}</div>
                    @endif

                    <!-- Form -->
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" id="form-registrasi">
                        @csrf

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nik">NIK <span class="text-danger">*</span></label>
                                    <input type="text" id="nik" name="nik" placeholder="Masukan NIK"
                                        class="form-control @error('nik') is-invalid @enderror" value="{{ old('nik') }}">
                                    @error('nik')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama Lengkap <span class="text-danger">*</span></label>
                                    <input type="text" id="nama" name="nama" placeholder="Masukan Nama Lengkap"
                                        class="form-control @error('nama') is-invalid @enderror"
                                        value="{{ old('nama') }}">
                                    @error('nama')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="telepon">No Whatsapp <span class="text-danger">*</span></label>
                                    <input type="text" id="telepon" name="telepon" placeholder="Masukan No Whatsapp"
                                        class="form-control @error('telepon') is-invalid @enderror"
                                        value="{{ old('telepon') }}">
                                    @error('telepon')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="email" id="email" name="email" placeholder="Masukan Email"
                                        class="form-control @error('email') is-invalid @enderror"
                                        value="{{ old('email') }}">
                                    @error('email')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="alamat_user">Alamat <span class="text-danger">*</span></label>
                                    <textarea id="alamat_user" name="alamat_user" placeholder="Masukan Alamat"
                                        class="form-control @error('alamat_user') is-invalid @enderror">{{ old('alamat_user') }}</textarea>
                                    @error('alamat_user')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="role">Tipe WP <span class="text-danger">*</span></label>
                                    <select id="role" name="role"
                                        class="form-control @error('role') is-invalid @enderror">
                                        <option value="">Pilih</option>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role }}"
                                                {{ old('role') == $role ? 'selected' : '' }}>
                                                {{ $role }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('role')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
