<table>
    <tr>
        <th colspan="10">
            <h3><b>
                    Daftar Perbandingan Ketetapan Per WP
                </b></h3>
        </th>
    </tr>
    <tr>
        <th colspan="2"><b> Kelurahan/Desa</b></th>
        <th>:<b>{{ $kelurahan }}</b></th>
    </tr>
    <tr>
        <th colspan="2"><b> Kecamatan</b></th>
        <th>:<b>{{ $kecamatan }}</b></th>
    </tr>
    <tr>
        <th colspan="2"><b> Tahun Pajak</b></th>
        <th>:<b>{{ $tahun }}</b></th>
    </tr>
    <tr>
        <th colspan="2"><b> Buku</b></th>
        <th>:<b>{{ implode(',',$buku) }}</b></th>
    </tr>
</table>

@include('dhkp/perbandingan_desa_table',['data'=>$data])
