<table>
    <tr>
        <th colspan="10">
            <h3><b>DAFTAR KETETAPAN PBB P2 PER DESA</b></h3>
        </th>
    </tr>
    <tr>
        <th colspan="2"><b> Kecamatan</b></th>
        <th>:<b>{{ $kecamatan }}</b></th>
    </tr>

    @if (!empty($kelurahan))

    <tr>
        <th colspan="2"><b> Kelurahan</b></th>
        <th>:<b>{{ $kelurahan }}</b></th>
    </tr>

    @endif

    <tr>
        <th colspan="2"><b> Tahun Pajak</b></th>
        <th>:<b>{{ $tahun }}</b></th>
    </tr>
    <tr>
        <th colspan="2"><b> Buku</b></th>
        <th>:<b>{{ $buku }}</b></th>
    </tr>
    <tr>
        <th colspan="2">Ditarik pada</th>
        <th>:{{ tglIndo(date('Y-m-d')) }} {{ date('H:i:s') }}</th>
    </tr>
</table>

<table class="table table-sm table-bordered">
    <thead>
        <tr valign="middle">
            <th style="text-align: center" rowspan="2">No</th>
            <th style="text-align: center" rowspan="2">Desa</th>
            <th style="text-align: center" rowspan="2">SPPT</th>
            <th style="text-align: center" colspan="2">Luas</th>
            <th style="text-align: center" rowspan="2">Ketetapan</th>
            <th style="text-align: center" rowspan="2">Lunas</th>
            <th style="text-align: center" rowspan="2">Belum Lunas</th>
        </tr>
        <tr valign="middle">

            <th style="text-align: center">Bumi</th>
            <th style="text-align: center">Bangunan</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        $total = 0;
        $tbumi = 0;
        $tbng = 0;
        $tpajak = 0;
        $tb=0;
        $bb=0;
        @endphp
        @foreach ($data as $row)
        <tr>
            <td align="center">{{ $no }}</td>
            <td>{{ $row->nm_kelurahan }}</td>
            <td align="center">{{ number_format($row->sppt,0,'','.') }}</td>
            <td align="right">{{ number_format($row->bumi, 0, '', '.') }}
                M<sup>2</sup></td>
            <td align="right">{{ number_format($row->bangunan, 0, '', '.') }}
                M<sup>2</sup></td>
            <td align="right">Rp. {{ number_format($row->pbb, 0, '', '.') }}</td>
            <td align="right">Rp. {{ number_format($row->bayar, 0, '', '.') }}</td>
            <td align="right">Rp. {{ number_format($row->belum_bayar, 0, '', '.') }}</td>
        </tr>
        @php
        $total += $row->sppt;
        $tbumi += $row->bumi;
        $tbng += $row->bangunan;
        $tb+=$row->bayar;
        $bb+=$row->belum_bayar;
        $tpajak += $row->pbb;
        $no++;
        @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2"> Total </td>
            <td align="center">{{ number_format($total,0,'','.') }} </td>
            <td align="right">{{ number_format($tbumi, 0, '', '.') }} M<sup>2</sup></td>
            <td align="right">{{ number_format($tbng, 0, '', '.') }} M<sup>2</sup></td>
            <td align="right">Rp. {{ number_format($tpajak, 0, '', '.') }}</td>
            <td align="right">Rp. {{ number_format($tb, 0, '', '.') }}</td>
            <td align="right">Rp. {{ number_format($bb, 0, '', '.') }}</td>
        </tr>
    </tfoot>
</table>
