<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">
        Daftar Perbandingan Ketetapan Per WP
        <br>
        kelurahan {{ ucwords(strtolower($kelurahan)) }} Kecamatan {{ ucwords(strtolower($kecamatan)) }} Buku {{ implode(',', $buku) }} Tahun Pajak
        {{ $tahun }}
    </h4>
    @include('dhkp/perbandingan_desa_table',['data'=>$data])
    
</body>

</html>
