<div class="table-responsive">
    <table class="table table-hover table-sm table-bordered text-sm">
        <thead>
            <tr style="vertical-align: middle">
                <th  style="text-align: center" rowspan="2">Kecamatan</th>
                <th style="text-align: center" rowspan="2">Kel/Desa</th>
                <th  style="text-align: center" rowspan="2">Sektor</th>
                <th style="text-align: center" colspan="2">Ketetapan PBB</th>
                <th style="text-align: center" colspan="2">Lunas Bayar</th>
                <th style="text-align: center" colspan="2">Belum Bayar</th>
                <th style="text-align: center" rowspan="2">Download</th>
            </tr>
            <tr style="vertical-align: middle">
                <th style="text-align: center">Objek</th>
                <th style="text-align: center">PBB</th>
                <th style="text-align: center">Objek</th>
                <th style="text-align: center">PBB</th>
                <th style="text-align: center">Objek</th>
                <th style="text-align: center">PBB</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data->preview as $row)
            <tr>
                <td>{{ $row->kd_kecamatan.' - '.$row->kecamatan }}</td>
                <td>{{ $row->kd_kelurahan.' - '.$row->kelurahan }}</td>
                <td >{{ $row->nm_sektor }}</td>
                <td align="center">
                    {{ number_format($row->op_total, 0, '', '.') }}</td>
                <td align="right">
                    {{ number_format($row->jumlah_total, 0, '', '.') }}
                </td>
                <td align="center">
                    {{ number_format($row->op_lunas, 0, '', '.') }}</td>
                <td align="right">
                    {{ number_format($row->jumlah_lunas, 0, '', '.') }}
                </td>
                <td align="center">
                    {{ number_format($row->op_belum, 0, '', '.') }}</td>
                <td align="right">
                    {{ number_format($row->jumlah_belum, 0, '', '.') }}
                </td>
                @if (hasPermissions(['pdf_dhkp', 'excel_dhkp']))
                <td class="text-center">
                    <a class="btn btn-sm btn-primary btn-flat" href="{{ url('dhkp/wepe-excel') }}?kd_kecamatan={{ $row->kd_kecamatan }}&kd_kelurahan={{ $row->kd_kelurahan }}&tahun={{ request()->get('tahun')}}&{{ $data->param}}">
                        <i class="fas fa-download "></i>
                    </a>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
