@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>DAFTAR KETETAPAN PBB P2 PER DESA</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">Filter Data</h3>
                            <div class="card-tools">

                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ url()->current() }}" id="form-filter">
                                <input type="hidden" value="{{ date('Y') }}" class="form-control form-control-sm"
                                    name="tahun">
                                <div class="form-group">
                                    <label for="">Kecamatan</label>
                                    <select required class="form-control form-control-sm select2" name="kd_kecamatan"
                                        id="kd_kecamatan" data-placeholder="Kecamatan">
                                        @if (count($kecamatan) > 1)
                                            <option value=""></option>
                                        @endif
                                        @foreach ($kecamatan as $rowkec)
                                            <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                value="{{ $rowkec->kd_kecamatan }}">
                                                {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Kelurahan</label>
                                    <select class="form-control form-control-sm select2" name="kd_kelurahan"
                                        id="kd_kelurahan" data-placeholder="Desa / Kelurahan">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Buku</label>
                                    <select class="form-control form-control-sm " name="buku" id="buku">
                                        <option value="">Semua Buku</option>
                                        @php
                                            $bsel = request()->get('buku') ?? '1';
                                            $arb = [1 => 'Buku 1 dan 2', 'Buku 3,4, dan 5'];
                                        @endphp
                                        @for ($b = 1; $b <= 2; $b++)
                                            <option @if ($bsel == $b) selected @endif
                                                value="{{ $b }}">{{ $arb[$b] }}</option>
                                        @endfor
                                    </select>
                                    {{-- <div class="form-check">
                                        @php
                                            $cb = request()->get('buku') ?? ['1', '2'];
                                        @endphp
                                        @for ($i = 1; $i <= 5; $i++)
                                            <input @if (in_array($i, $cb)) checked @endif type="checkbox"
                                                name="buku[]" id="buku{{ $i }}"
                                                value="{{ $i }}"><label class="form-check-label"
                                                for="buku{{ $i }}"> Buku {{ $i }}</label><br>
                                        @endfor
                                    </div> --}}
                                </div>
                                <div class="float-sm-right">
                                    <button class="btn btn-sm btn-info"><i class="fa fa-search"></i> Tampilkan</button>
                                </div>

                            </form>
                        </div>
                        <!-- /.card-header -->

                    </div>
                </div>
                <div class="col-md-9">
                    <div id="preview-data"></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $("#form-filter").on("submit", function(event) {
                event.preventDefault();
                openloading();
                $('#preview-data').html('')

                $.ajax({
                    url: "{{ url()->current() }}",
                    data: $(this).serialize(),
                    success: function(res) {
                        closeloading();
                        $('#preview-data').html(res)

                    },
                    error: function(res) {
                        closeloading();
                        toastr.error('Error', 'Gagal menampilkan data');
                        $('#preview-data').html('')
                    }
                })

            });


            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            $('.select2').select2({
                'allowClear': true
            })

            function getKelurahan(kk) {
                $('#kd_kelurahan').html('<option value=""></option>');
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(data) {
                            $.each(data, function(key, value) {
                                var newOption = new Option(key + '-' + value, key, true, true);
                                $('#kd_kelurahan').append(newOption)
                            });

                            $('#kd_kelurahan').val(null).trigger('change');
                        },
                        error: function(res) {

                        }
                    });
                }
            }

        });
    </script>
@endsection
