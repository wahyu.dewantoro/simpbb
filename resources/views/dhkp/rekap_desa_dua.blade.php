<div class="card card-outline card-success">
    <div class="card-header">
        <h3 class="card-title">Preview Data</h3>
        <div class="card-tools">
            @if (hasPermissions('excel_dhkp_desa'))
                <a href="{{ url('dhkp/desa/cetak') . '?' . $filter }}" class="btn btn-sm btn-success"><i
                        class="fas fa-file-excel"></i></a>
            @endif
            @if (hasPermissions(['pdf_dhkp_desa']))
                <a href="{{ url('dhkp/desa/cetak-pdf') . '?' . $filter }}" class="btn btn-sm btn-warning"><i
                        class="fas fa-file-pdf"></i></a>
            @endif
        </div>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            @include('dhkp/rekap_desa_table_dua', [
                'data' => $rekap,
            ])
        </div>
    </div>
</div>
