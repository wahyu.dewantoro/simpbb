<table class="table table-sm table-bordered" border="1">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">NOP</th>
            <th rowspan="2">Nama WP</th>
            <th colspan='2'>ketetapan</th>
            <th rowspan='2'>Selisih</th>
        </tr>
        <tr>
            <th>{{ $tahun }}</th>
            <th>{{ $tahun - 1 }}</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach ($data as $index => $item)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ $item->nop }}</td>
                <td>{{ $item->nm_wp }}</td>
                <td align="right">{{ number_format($item->pbb_sekarang, 0, '', '.') }}</td>
                <td align="right">{{ number_format($item->pbb_kemarin, 0, '', '.') }}
                <td align="right">{{ number_format($item->pbb_selisih, 0, '', '.') }}
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>