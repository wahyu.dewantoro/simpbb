@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>REKAP PERBANDINGAN KETETAPAN PER DESA</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Preview Data</h3>
                            {{-- <div class="card-tools">
                                <a href="{{ url('dhkp-perbandingan-desa/cetak') . '?tahun=' . request()->get('tahun') . '&kd_kecamatan=' . request()->get('kd_kecamatan') }}"
                                    class="btn btn-sm btn-success"><i class="fas fa-file-download"></i></a>
                            </div> --}}
                        </div>
                        <div class="card-body p-1">
                            <form action="{{ url()->current() }}" class="form-inline">


                                <input type="number" value="{{ request()->get('tahun') ?? date('Y') }}"
                                    class="form-control form-control-sm mb-2 mr-sm-2" name="tahun">


                                <select required class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan"
                                    id="kd_kecamatan">
                                    <option value="">-- Pilih --</option>
                                    @foreach ($kecamatan as $rowkec)
                                        <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan)  selected @endif value="{{ $rowkec->kd_kecamatan }}">
                                            {{ $rowkec->nm_kecamatan }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="form-check mb-2 mr-sm-2">
                                    @php
                                        $cb = request()->get('buku') ?? ['1', '2'];
                                        $pb = '';
                                        foreach ($cb as $ib => $bb) {
                                            $pb .= 'buku[' . $ib . ']=' . $bb . '&';
                                        }
                                        
                                        $pb = $pb != '' ? substr($pb, 0, -1) : '';
                                        
                                    @endphp
                                    @for ($i = 1; $i <= 5; $i++)
                                        <input @if (in_array($i, $cb)) checked @endif type="checkbox" name="buku[]"
                                            id="buku{{ $i }}" value="{{ $i }}"><label
                                            class="form-check-label" for="buku{{ $i }}"> Buku
                                            {{ $i }} &nbsp; &nbsp;</label>
                                    @endfor
                                </div>
                                <div class="float-sm-right">
                                    <button class="btn btn-sm btn-info mb-2 mr-sm-2"><i class="fa fa-search"></i>
                                        Tampilkan</button>
                                    @can('excel_dhkp_perbandingan_desa')
                                        <a href="{{ url('dhkp/perbandingandesa/cetak') . '?tahun=' . request()->get('tahun') . '&kd_kecamatan=' . request()->get('kd_kecamatan') . '&' . $pb }}"
                                            class="btn btn-sm btn-success mb-2 mr-sm-2"><i class="far fa-file-excel"></i></a>
                                    @endcan
                                    @can('pdf_dhkp_perbandingan_desa')
                                        <a href="{{ url('dhkp/perbandingandesa-pdf') . '?tahun=' . request()->get('tahun') . '&kd_kecamatan=' . request()->get('kd_kecamatan') . '&' . $pb }}"
                                            class="btn btn-sm btn-warning mb-2 mr-sm-2"><i class="far fa-file-pdf"></i></a>
                                    @endcan
                                </div>
                            </form>
                            <div class="table-responsive">
                                    @php
                                        $tahun = request()->get('tahun')??date('Y');
                                    @endphp
                                    @include('dhkp.rekap_desa_perbandingan_table',['tahun'=>$tahun,'preview'=>$data->preview])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
