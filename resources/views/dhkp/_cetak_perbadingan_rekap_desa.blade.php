<table>
    <tr>
        <th colspan="10">
            <h3><b>
                REKAP PERBANDINGAN KETETAPAN PER DESA
                </b></h3>
        </th>
    </tr>
    
    <tr>
        <th colspan="2"><b> Kecamatan</b></th>
        <th>:<b>{{ $kecamatan }}</b></th>
    </tr>
    <tr>
        <th colspan="2"><b> Tahun Pajak</b></th>
        <th>:<b>{{ $tahun }}</b></th>
    </tr>
    <tr>
        <th colspan="2"><b> Buku</b></th>
        <th>:<b>{{ implode(',', $buku) }}</b></th>
    </tr>
</table>

<table class="table table-sm table-bordered" border="1">
    <thead>
        <tr valign="middle">
            <th rowspan="2" style="text-align: center">No</th>
            <th rowspan="2" style="text-align: center">Desa</th>
            <th colspan="3" style="text-align: center">Ketetapan</th>
            <th colspan="3" style="text-align: center">Realisasi</th>
        </tr>
        <tr valign="middle">

            <th style="text-align: center">{{ $tahun }}</th>
            <th style="text-align: center">{{ $tahun - 1 }}</th>
            <th style="text-align: center">Selisih</th>
            <th style="text-align: center">{{ $tahun }}</th>
            <th style="text-align: center">{{ $tahun - 1 }}</th>
            <th style="text-align: center">Selisih</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $e = 0;
            $f = 0;
        @endphp
        @foreach ($data as $row)
            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ $row->nm_kelurahan }}</td>
                <td align="right">
                    {{ $row->baku_sekarang }}
                </td>
                <td align="right">{{ $row->baku_kemarin }}
                </td>
                <td align="right">{{ $row->baku_selisih }}
                </td>
                <td align="right">
                    {{ $row->realisasi_sekarang }}</td>
                <td align="right">
                    {{ $row->realisasi_kemarin }}</td>
                <td align="right">
                    {{ $row->realisasi_selisih }}</td>
            </tr>
            @php
                $a += $row->baku_sekarang;
                $b += $row->baku_kemarin;
                $c += $row->baku_selisih;
                $d += $row->realisasi_sekarang;
                $e += $row->realisasi_kemarin;
                $f += $row->realisasi_selisih;
                $no++;
            @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2"> Total </td>
            <td align="center">{{ $a }} </td>
            <td align="right">{{ $b }} </td>
            <td align="right">{{ $c }} </td>
            <td align="right">{{ $d }} </td>
            <td align="right">{{ $e }} </td>
            <td align="right">{{ $f }} </td>
        </tr>
    </tfoot>
</table>
