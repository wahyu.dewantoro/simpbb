<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">
        Daftar Ketetapan PBB P2 Per Desa<br>
        Kecamatan {{ $kecamatan }}
        @if ($kelurahan != '')
            Desa {{ $kelurahan }}
        @endif

        {{ $jns_buku }} Tahun Pajak {{ $tahun }}
    </h4>
    @include('dhkp.rekap_desa_table_dua', ['data' => $rekap])
</body>

</html>
