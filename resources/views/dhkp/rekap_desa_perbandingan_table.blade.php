<table class="table table-sm table-bordered">
    <thead>
        <tr valign="middle">
            <th rowspan="2" style="text-align: center">No</th>
            <th rowspan="2" style="text-align: center">Desa</th>
            <th colspan="3" style="text-align: center">Ketetapan</th>
            <th colspan="3" style="text-align: center">Realisasi</th>
        </tr>
        <tr valign="middle">

            <th style="text-align: center">{{ $tahun }}</th>
            <th style="text-align: center">{{ $tahun - 1 }}</th>
            <th style="text-align: center">Selisih</th>
            <th style="text-align: center">{{ $tahun }}</th>
            <th style="text-align: center">{{ $tahun - 1 }}</th>
            <th style="text-align: center">Selisih</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $e = 0;
            $f = 0;
        @endphp
        @foreach ($preview as $row)


            <tr>
                <td align="center">{{ $no }}</td>
                <td>{{ ucwords(strtolower($row->nm_kelurahan)) }}</td>
                <td align="right">
                    {{ number_format($row->baku_sekarang, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->baku_kemarin, 0, '', '.') }}
                </td>
                <td align="right">{{ number_format($row->baku_selisih, 0, '', '.') }}
                </td>
                <td align="right">
                    {{ number_format($row->realisasi_sekarang, 0, '', '.') }}</td>
                <td align="right">
                    {{ number_format($row->realisasi_kemarin, 0, '', '.') }}</td>
                <td align="right">
                    {{ number_format($row->realisasi_selisih, 0, '', '.') }}</td>
            </tr>
            @php
                $a += $row->baku_sekarang;
                $b += $row->baku_kemarin;
                $c += $row->baku_selisih;
                $d += $row->realisasi_sekarang;
                $e += $row->realisasi_kemarin;
                $f += $row->realisasi_selisih;
                $no++;
            @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2"> Total </td>
            <td align="center">{{ number_format($a, 0, '', '.') }} </td>
            <td align="right">{{ number_format($b, 0, '', '.') }} </td>
            <td align="right">{{ number_format($c, 0, '', '.') }} </td>
            <td align="right">{{ number_format($d, 0, '', '.') }} </td>
            <td align="right">{{ number_format($e, 0, '', '.') }} </td>
            <td align="right">{{ number_format($f, 0, '', '.') }} </td>
        </tr>
    </tfoot>
</table>
