@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Ringkasan Baku</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <form action="{{ url('dhkp/ringkasan') }}" class="form-inline">

                                    <input type="text" name="tahun_pajak" id="tahun_pajak"
                                        class="form-control form-control-sm"
                                        value="{{ request()->get('tahun_pajak') ?? date('Y') }}">
                                    <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i>
                                        Cari</button>

                                </form>
                            </h3>
                            <div class="card-tools">

                                @can('excel_ringkasan_baku')
                                    <a target="_blank" href="{{ url('dhkp/ringkasan-excel') }}?{{ $param }}"
                                        class="btn btn-sm btn-success mb-2 mr-sm-2"><i class="far fa-file-excel"></i></a>
                                @endcan
                                @can('pdf_ringkasan_baku')
                                    <a target="_blank" href="{{ url('dhkp/ringkasan-pdf') }}?{{ $param }}"
                                        class="btn btn-sm btn-warning mb-2 mr-sm-2"><i class="far fa-file-pdf"></i></a>
                                @endcan
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive-md">
                            <table id="table-data" class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th width="3px" rowspan="2" style="text-align: center; vertical-align:middle">No
                                        </th>
                                        <th rowspan="2" style="text-align: center; vertical-align:middle">Desa / Kelurahan
                                        </th>
                                        <th rowspan="2" style="text-align: center; vertical-align:middle">Kecamatan</th>
                                        <th rowspan="2" style="text-align: center; vertical-align:middle">Buku</th>
                                        <th rowspan="2" style="text-align: center; vertical-align:middle">Objek</th>
                                        <th rowspan="2" style="text-align: center; vertical-align:middle">Ketetapan PBB</th>
                                        <th style="text-align: center; vertical-align:middle" colspan="3">Realisasi</th>
                                        <th rowspan="2" style="text-align: center; vertical-align:middle">Sisa</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: center; vertical-align:middle">Pokok</th>
                                        <th style="text-align: center; vertical-align:middle">Denda</th>
                                        <th style="text-align: center; vertical-align:middle">Total</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // console.log("ready!");

            $('#table-data').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: '{!! url('dhkp/ringkasan') !!}{{ $param != '' ? '?' . $param : '' }}',
                columns: [{
                        data: null,
                        class: 'text-center',
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'nm_kelurahan',
                        name: 'nm_kelurahan',
                        render: function(data, type, row) {
                            return row.kd_kelurahan + ' - ' + data;
                        }
                    },
                    {
                        data: 'nm_kecamatan',
                        name: 'nm_kecamatan',
                        render: function(data, type, row) {
                            return row.kd_kecamatan + ' - ' + data;
                        }
                    },
                    {
                        data: 'buku',
                        name: 'buku',
                        class: 'text-center'
                    },
                    {
                        data: 'angka_op',
                        name: 'angka_op',
                        class: 'text-center',
                        orderable: false,
                    },
                    {
                        data: 'angka_baku',
                        name: 'angka_baku',
                        class: 'text-right',
                        orderable: false,
                    },
                    {
                        data: 'angka_pokok',
                        name: 'angka_pokok',
                        class: 'text-right',
                        orderable: false,
                    },
                    {
                        data: 'angka_denda',
                        name: 'angka_denda',
                        class: 'text-right',
                        orderable: false,
                    },
                    {
                        data: 'angka_total',
                        name: 'angka_total',
                        class: 'text-right',
                        orderable: false,
                    },
                    {
                        data: 'angka_sisa',
                        name: 'angka_sisa',
                        class: 'text-right',
                        orderable: false,
                    },
                ],
                "order": [],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                // getKelurahan(kk);
            })

            var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
            // console.log(kd_kecamatan)
            // getKelurahan(kd_kecamatan);

            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                if (kk != '') {

                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            $.each(res, function(k, v) {
                                // console.log(k)
                                html += '<option value="' + k + '">' + v + '</option>';
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }
        });
    </script>
@endsection
