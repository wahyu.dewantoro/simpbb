<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    
    <table>
        <tr>
            <th colspan="9">Ringkasan BAKU   Tahun Pajak {{ $tahun }}</th>
        </tr>
    </table>
    <table id="table-data" class="table table-bordered table-sm">
        <thead>
            <tr>
                <th  rowspan="2" style="text-align: center; vertical-align:middle">No
                </th>
                <th rowspan="2" style="text-align: center; vertical-align:middle">Desa / Kelurahan</th>
                <th rowspan="2" style="text-align: center; vertical-align:middle">Kecamatan</th>
                <th rowspan="2" style="text-align: center; vertical-align:middle">Buku</th>
                <th rowspan="2" style="text-align: center; vertical-align:middle">Objek</th>
                <th rowspan="2" style="text-align: center; vertical-align:middle">Ketetapan PBB</th>
                <th style="text-align: center; vertical-align:middle" colspan="3">Realisasi</th>
                <th rowspan="2" style="text-align: center; vertical-align:middle">Sisa</th>
            </tr>
            <tr>
                <th style="text-align: center; vertical-align:middle">Pokok</th>
                <th style="text-align: center; vertical-align:middle">Denda</th>
                <th style="text-align: center; vertical-align:middle">Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no=1;
            @endphp
            @foreach ($dhkp as $row)
                <tr>
                    <td style="text-align: center">{{ $no }}</td>
                    <td>{{ $row->kd_kelurahan.' - '.$row->nm_kelurahan }}</td>
                    <td>{{ $row->kd_kecamatan.' - '.$row->nm_kecamatan }}</td>
                    <td style="text-align: center">{{ $row->kd_buku }}</td>
                    <td style="text-align: center">{{ number_format($row->jumlah_op,0,'','') }}</td>
                    <td style="text-align: right">{{ number_format($row->baku,0,'','') }}</td>
                    <td style="text-align: right">{{ number_format($row->pokok,0,'','') }}</td>
                    <td style="text-align: right">{{ number_format($row->denda,0,'','') }}</td>
                    <td style="text-align: right">{{ number_format($row->total,0,'','') }}</td>
                    <td style="text-align: right">{{ number_format($row->baku - $row->total,0,'','') }}</td>
                </tr>
            @php
                $no++;
            @endphp
            @endforeach
        </tbody>
    </table>
</body>

</html>
