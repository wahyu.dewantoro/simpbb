<div class="table-responsive">
    <table class="table table-hover table-sm table-bordered text-sm">
        <thead>
            <tr style="vertical-align: middle">
                <th style="text-align: center">Kecamatan</th>
                <th style="text-align: center">Kel/Desa</th>
                <th style="text-align: center">Objek</th>
                <th style="text-align: center">Ketetapan PBB</th>
                <th style="text-align: center">Stimulus</th>
                <th style="text-align: center">Bayar</th>
                <th style="text-align: center">Koreksi</th>
                <th style="text-align: center">Sisa</th>
                <th style="text-align: center">Download</th>
            </tr>

        </thead>
        <tbody>
            @foreach ($rekap as $row)
                <tr>
                    <td>{{ $row->kd_kecamatan . ' - ' . $row->nm_kecamatan }}</td>
                    <td>{{ $row->kd_kelurahan . ' - ' . $row->nm_kelurahan }}</td>
                    <td class="text-center">{{ angka($row->jumlah_op) }}</td>
                    <td class="text-center">{{ angka($row->baku) }}</td>
                    <td class="text-center">{{ angka($row->potongan) }}</td>
                    <td class="text-center">{{ angka($row->bayar) }}</td>
                    <td class="text-center">{{ angka($row->koreksi) }}</td>
                    <td class="text-center">{{ angka($row->sisa) }}</td>
                    @if (hasPermissions(['pdf_dhkp', 'excel_dhkp']))
                        <td class="text-center">
                            <a 
                            {{-- data-toggle="tooltip" data-placement="top" --}}
                                {{-- title="DHKP {{ $row->nm_kecamatan }} - {{ $row->nm_kelurahan }}" --}}
                                href="{{ url('dhkp/wepe-excel') }}?kd_kecamatan={{ $row->kd_kecamatan }}&kd_kelurahan={{ $row->kd_kelurahan }}&tahun={{ request()->get('tahun') }}&buku={{ request()->get('buku') }}">
                                <i class="fas fa-file-download text-success"></i></i>
                            </a>
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
