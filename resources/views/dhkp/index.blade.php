@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>DHKP Wajib Pajak</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">

                        <div class="card-body">

                            <form action="{{ url()->current() }}" class="" id="filter-form">
                                <div class="row">
                                    @if ($ordal == '0')
                                        <input type="hidden" name="tahun" id="tahun" value="{{ date('Y') }}">
                                    @else
                                        <div class="col-md-1">
                                            <select required name="tahun" id="tahun"
                                                class="form-control form-control-sm">
                                                @for ($t = date('Y')+1; $t >= 2003; $t--)
                                                    @php
                                                        $sel = request()->get('tahun') ?? date('Y');
                                                    @endphp
                                                    <option @if ($sel == $t) selected @endif
                                                        value="{{ $t }}">{{ $t }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    @endif
                                    <div class="col-md-3">
                                        <select class="form-control form-control-sm  select2" name="kd_kecamatan" required
                                            id="kd_kecamatan" data-placeholder="Kecamatan">
                                            @if (count($kecamatan) > 1)
                                                <option value=""></option>
                                            @endif
                                            @foreach ($kecamatan as $rowkec)
                                                <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                                    value="{{ $rowkec->kd_kecamatan }}">
                                                    {{ $rowkec->kd_kecamatan . ' - ' . $rowkec->nm_kecamatan }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control form-control-sm select2" name="kd_kelurahan"
                                            id="kd_kelurahan" data-placeholder="Desa / Kelurahan">
                                            @if (count($kecamatan) > 1)
                                                <option value=""></option>
                                            @endif
                                        </select>

                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control form-control-sm " name="buku" id="buku">
                                            <option value="">Semua Buku</option>
                                            @php
                                                $bsel = request()->get('buku') ?? '1';
                                                $arb = [1 => 'Buku 1 dan 2', 'Buku 3,4, dan 5'];
                                            @endphp
                                            @for ($b = 1; $b <= 2; $b++)
                                                <option @if ($bsel == $b) selected @endif
                                                    value="{{ $b }}">{{ $arb[$b] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-sm btn-info mb-2 mr-sm-2"><i class="fa fa-search"></i>
                                            Tampilkan</button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div id="preview-data"></div>
                        </div>
                        <!-- /.card-header -->
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            $('.select2').select2({
                'allowClear': true
            })

            function getKelurahan(kk) {
                let jk = "{{ count($kecamatan) }}"
                // console.log(jk)
                if (jk > 1) {
                    $('#kd_kelurahan').html('<option value=""></option>');
                } else {
                    $('#kd_kelurahan').html('');
                }
                if (kk != '') {
                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(data) {
                            $.each(data, function(key, value) {
                                var newOption = new Option(key + '-' + value, key, true, true);
                                $('#kd_kelurahan').append(newOption)
                            });
                            if (jk > 1) {
                                $('#kd_kelurahan').val(null).trigger('change');
                            }
                        },
                        error: function(res) {

                        }
                    });
                }
            }

            $('#kd_kecamatan').trigger('change')

            $("#filter-form").on("submit", function(event) {
                openloading();
                $('#preview-data').html('')
                event.preventDefault();
                openloading()
                $.ajax({
                    url: "{{ url()->current() }}",
                    data: $(this).serialize(),
                    success: function(res) {
                        closeloading();
                        $('#preview-data').html(res)

                    },
                    error: function(res) {
                        closeloading();
                        toastr.error('Error', 'Gagal menampilkan data');
                        $('#preview-data').html('')
                    }
                })

            });
        });
    </script>
@endsection
