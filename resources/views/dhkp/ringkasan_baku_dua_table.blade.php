<div class="table-responsive">
    <table class="table table-hover table-sm table-bordered text-sm">
        <thead>
            <tr style="vertical-align: middle">
                <th style="text-align: center" rowspan="2">Kecamatan</th>
                <th style="text-align: center" rowspan="2">Kel/Desa</th>
                <th style="text-align: center" rowspan="2">Buku</th>
                <th style="text-align: center" rowspan="2">Objek</th>
                <th style="text-align: center" colspan="3">Ketetapan</th>
                <th style="text-align: center" rowspan="2">Stimulus</th>
                <th style="text-align: center" rowspan="2">Bayar</th>
                <th style="text-align: center" rowspan="2">Koreksi</th>
                <th style="text-align: center" rowspan="2">Sisa</th>
                {{-- <th style="text-align: center">Download</th> --}}
            </tr>
            <tr>
                <th style="text-align: center"> Sebelumnya</th>
                <th style="text-align: center"> Perubahan</th>
                <th style="text-align: center"> Akhir</th>
            </tr>

        </thead>
        <tbody>
            @foreach ($rekap as $row)
                <tr>
                    <td>{{ $row->kd_kecamatan . ' - ' . $row->nm_kecamatan }}</td>
                    <td>{{ $row->kd_kelurahan . ' - ' . $row->nm_kelurahan }}</td>

                    <td class="text-center">{{ angka($row->buku) }}</td>
                    <td class="text-center">{{ angka($row->jumlah_op) }}</td>
                    <td class="text-center">{{ angka($row->baku_last) }}</td>
                    <td class="text-center">{{ angka($row->baku - $row->baku_last) }}</td>
                    <td class="text-center">{{ angka($row->baku) }}</td>
                    <td class="text-center">{{ angka($row->potongan) }}</td>
                    <td class="text-center">{{ angka($row->bayar) }}</td>
                    <td class="text-center">{{ angka($row->koreksi) }}</td>
                    <td class="text-center">{{ angka($row->sisa) }}</td>

                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
