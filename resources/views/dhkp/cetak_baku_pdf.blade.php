<html>

<head>
    <style type="text/css">
        .page_break {
            page-break-before: always;
        }

        body {
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            size: a4 landscape;
            margin-top: 13mm;
            margin-bottom: 11mm;
        }

        @page {
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            size: a4 landscape;
            margin-top: 13mm;
            margin-bottom: 11mm;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            /* background-color: #03a9f4; */
            color: black text-align: center;
            line-height: 30px;
        }

    </style>
</head>
<body>
    <div style="margin-top: 0">
        <table width="100%" style="border-collapse: collapse; font-size: 10px; font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif; font-weight: bold">
            <thead>
                <tr>
                    <td width="50%">
                        <table width="100%" style="border-collapse: collapse; font-size: 10px; font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif; font-weight: bold">
                            <tr>
                                <TD width="10%">PROPINSI</TD>
                                <TD>: 35 - JAWA TIMUR</TD>
                            </tr>
                            <tr>
                                <TD>DATI II</TD>
                                <TD>: 07 - KABUPATEN MALANG</TD>
                            </tr>

                        </table>

                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td width="30%" align="right">
                        <table width="100%" style="border-collapse: collapse; font-size: 10px; font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif; font-weight: bold">
                            <tr>
                                <TD width="23%">KECAMATAN</TD>
                                <td>: {{ $tempat->kd_kecamatan.' - '.$tempat->nm_kecamatan}}</td>
                            </tr>
                            <tr>
                                <TD align="right">DESA/KELURAHAN</TD>
                                <td>: {{ $tempat->kd_kelurahan.' - '.$tempat->nm_kelurahan}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </thead>
        </table>
        <hr style="border: 1px solid black">
        <table width="100%" style="border-collapse: collapse; font-size: 10px;  font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif ">
            <thead>
                <tr style="border-bottom: 2px solid grey">
                    <th style=" text-align: center; width: 5%">NO</th>
                    <th style=" text-align: center; width:15%">NOP</th>
                    <th style=" text-align: left;  width:20%">ALAMAT OBJEK</th>
                    <th style="text-align:left; width:20%">WAJIB PAJAK</th>
                    <th style=" text-align: left;">BUMI</th>
                    <th style=" text-align: left;">BNG</th>
                    <th style=" text-align: right;">PBB</th>
                    <th style=" text-align: right;">STIMULUS</th>
                    <th style=" text-align: right; width:10%">HARUS DIBAYAR</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no=$no_urut;
                $a=0;
                $b=0;
                $c=0;

                @endphp
                @foreach ($sppt as $row)
                @php
                $alamat=$row->jalan_op.' '.$row->blok_kav_no_op.' RT '.$row->rt_op.' RW '.$row->rw_op;
                @endphp
                <tr style="border-bottom: 1px solid black; vertical-align: top; ">
                    <td style="padding-bottom:3px; text-align: center">{{ angka($no) }}</td>
                    <td style="padding-bottom:3px; text-align:center">{{ $row->nop }}</td>
                    <td style="padding-bottom:3px; text-align: left">{{ Str::substr($alamat, 0, 33) }}</td>
                    <td style="padding-bottom:3px; text-align:left">{{ trim($row->nm_wp_sppt) }}</td>
                    <td style="padding-bottom:3px; ">{{ angka($row->luas_bumi_sppt) }}</td>
                    <td style="padding-bottom:3px; ">{{ angka($row->luas_bng_sppt) }}</td>
                    <td style="padding-bottom:3px; text-align:right">{{ angka($row->pbb_yg_harus_dibayar_sppt) }}</td>
                    <td style="padding-bottom:3px; text-align:right">{{ angka($row->potongan) }}</td>
                    <td style="padding-bottom:3px; text-align:right">{{ angka(($row->pbb_yg_harus_dibayar_sppt-$row->potongan)) }}</td>
                </tr>
                @php
                $a+=$row->pbb_yg_harus_dibayar_sppt ;
                $b+= $row->potongan;
                $c+= ($row->pbb_yg_harus_dibayar_sppt-$row->potongan);

                $no++;
                @endphp
                @endforeach
            </tbody>
            <tfoot>
                <tr style="border-bottom: 1px solid black; vertical-align: top; ">
                    <td colspan="6"><b>SUBTOTAL</b></td>
                    <td style="padding-bottom:3px; text-align:right; font-weight: bold">{{ angka($a) }}</td>
                    <td style="padding-bottom:3px; text-align:right; font-weight: bold">{{ angka($b) }}</td>
                    <td style="padding-bottom:3px; text-align:right; font-weight: bold">{{ angka($c) }}</td>
                </tr>
                @if($last==true)
                <tr style="border-bottom: 1px solid black; vertical-align: top; ">
                    <td colspan="6"><b>TOTAL</b></td>
                    <td style="padding-bottom:3px; text-align:right; font-weight: bold">{{ angka($pbb) }}</td>
                    <td style="padding-bottom:3px; text-align:right; font-weight: bold">{{ angka($potongan) }}</td>
                    <td style="padding-bottom:3px; text-align:right; font-weight: bold">{{ angka($pbb-$potongan) }}</td>
                </tr>
                @endif
            </tfoot>
        </table>
        @if($last==true)
        <br>
        <table width="100%" style="font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif; font-size: 10px">
            <tr>
                <td width="40%" align="center">
                    <br>
                    <b>CAMAT<br>
                        {{ $tempat->nm_kecamatan }}</b>
                    <br><br>
                    <br>
                    <br><br><br><br><br>
                    <b><u><?= strtoupper($camat->nm_camat??'<span style="color:white">................................................................</span>') ?></u></b><br>NIP : <?= $camat->nip_camat??'<span style="color:white">................................</span>' ?>
                </td>
                <td width="20%">
                    <br>
                </td>
                <td width="40%" align="center">
                    <b>
                        Malang, {{ tglIndo(date('Y-m-d')) }}<br>
                        A/N BUPATI <br>
                        {{ $kaban->is_plt != '' ? 'PLT' : '' }} {{ $kaban->jabatan }}<BR>
                        KABUPATEN MALANG</b>
                    <br>
                    <br>
                    <br><br><br><br>
                    <br>
                    <b><u>{{ $kaban->nama }}</u></b><br>NIP : {{ $kaban->nip }}

                </td>
            </tr>
        </table> @endif
        {{--
        <footer>
            <p style="text-align: center">Halaman {{ $halaman }}</p>
        </footer> --}}

    </div>
</body>
</html>
