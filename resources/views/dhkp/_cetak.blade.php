<table style="font-size: .875rem !important">
    <tr>
        <th colspan="10">
            <h3 style="text-align: center ">DAFTAR HIMPUNAN KETETAPAN PBB P2 PER WAJIB
                PAJAK</h3>
        </th>
    </tr>
</table>
<table style="font-size: .875rem !important">
    @if(isset($kecamatan))
    <tr>
        <td colspan="2"> Kecamatan</td>
        <td>:{{ $kecamatan }}</td>
    </tr>
    @endif
    @if ( isset($kelurahan))
    <tr>
        <td colspan="2"> Kelurahan/Desa</td>
        <td>:{{ $kelurahan }}</td>
    </tr>
    @endif
    <tr>
        <td colspan="2"> Tahun Pajak</td>
        <td>:{{ $tahun }}</td>
    </tr>
    <tr>
        <td colspan="2"> Buku</td>
        <td>:{{ $buku }}</td>
    </tr>
    <tr>
        <td colspan="2">Ditarik pada</td>
        <td>:{{ tglIndo(date('Y-m-d')) }} {{ date('H:i:s') }}</td>
    </tr>
</table>
<table class="border">
    <thead>
        <tr>
            <th>No</th>
            <th>NOP</th>
            <th>Alamat OP</th>
            <th>Nama WP</th>
            <th>Alamat WP</th>
            <th>L. Bumi</th>
            <th>L. Bangunan</th>
            <th>PBB Sebelumnya</th>
            <th>PBB Sesudah</th>
            <th>Pengurangan</th>
            <th>Harus di bayar</th>
            <th>Jatuh Tempo</th>
            <Th>Estimasi Denda</Th>
            {{-- <th>Status</th> --}}
            <th>Tanggal Bayar</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        $total = 0;
        @endphp
        @foreach ($dhkp as $row)
        @php
        $total += $row->pbb_yg_harus_dibayar_sppt-$row->potongan;
        @endphp
        <tr>
            <td align="center">{{ $no }}</td>
            <td>{{ $row->nop }}</td>
            <td>
                {{ ucwords(strtolower($row->jalan_op . ' ' . $row->blok_kav_no_op )).  ($row->rt_op<>''? ' RT ' . $row->rt_op : '' )   . ($row->rw_op<>''?' RW '.$row->rw_op:'')   }}
            </td>
            <td>{{ $row->nm_wp_sppt }}</td>
            <td class="kecil">
                {{ ucwords(strtolower($row->jln_wp_sppt . ' ' . $row->blok_kav_no_wp_sppt)) .  ($row->rt_wp_sppt<>''?'RT '.$row->rt_wp_sppt:' ') .' '. ($row->rw_wp_sppt<>''?' RW '.$row->rw_wp_sppt:' ')  . '  ' .ucwords(strtolower($row->kelurahan_wp_sppt . ' ' . $row->kota_wp_sppt)) }}
            </td>
            <td class="text-tengah">{{ number_format($row->luas_bumi_sppt, 0, '', '') }}</td>
            <td class="text-tengah">{{ number_format($row->luas_bng_sppt, 0, '', '') }}</td>
            <td class="text-kanan">{{ number_format(($row->last_pbb!=''?$row->last_pbb:$row->pbb_yg_harus_dibayar_sppt), 0, '', '') }}</td>
            <td class="text-kanan">{{ number_format($row->pbb_yg_harus_dibayar_sppt, 0, '', '') }}</td>
            <td class="text-kanan">{{  $row->potongan<>''?  number_format($row->potongan, 0, '', ''):0 }}</td>
            <td class="text-kanan">{{ number_format($row->pbb_yg_harus_dibayar_sppt  - $row->potongan, 0, '', '') }}</td>
            <td class="text-kecil">{{ tglIndo($row->tgl_jatuh_tempo_sppt) }}</td>
            {{-- <td class="text-kecil">
                {{ $row->status_pembayaran_sppt == '1' ? 'Lunas' : ($row->status_pembayaran_sppt == 0 ? 'Belum Bayar' : ($row->status_pembayaran_sppt == '3' ? 'Dihapus' : '')) }}
            </td> --}}
            <td class="text-kecil">{{ $row->tgl_bayar == '' && ($row->pbb_yg_harus_dibayar_sppt  - $row->potongan)>0 ? number_format($row->es_denda,0,'','') : '' }}</td>
            <td class="text-kecil">{{ $row->tgl_bayar != '' ? tglIndo($row->tgl_bayar) : '' }}</td>
        </tr>
        @php
        $no++;
        @endphp
        @endforeach
        <tr>
            <th colspan="9">Jumlah</th>
            <td class="text-kanan">{{ number_format($total, 0, '', '') }}</td>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tbody>

</table>
