@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Dokumen DHKP</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <h3 class="card-title">Filter Data</h3> --}}
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="cari" class="form-control float-right" placeholder="Search"
                                            value="{{ request()->get('cari') }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                            @can('download_all_dhkp')
                                            <a href="{{ url('dhkp/dokumen-download') }}" class="btn btn-flat btn-success"> Download all</a>
                                            @endcan
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th>Keterangan</th>
                                        <th class="text-center">Desa</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Generate</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($data as $index => $row)
                                        <tr>
                                            <td align="center">{{ $index + $data->firstItem() }}</td>
                                            <td>{{ $row->keterangan }}</td>
                                            <td>{{ $row->nm_kelurahan }}</td>
                                            <td>{{ $row->nm_kecamatan }}</td>
                                            <td>{{ tglIndo($row->created_at) }}
                                                {{ date('H:i:s', strtotime($row->created_at)) }}</td>
                                            <td class="text-center">
                                                <a href="{{ url('download') }}/{{ $row->disk }}/{{ acak($row->filename) }}" class="text-success" target="_blank"><i class="fas fa-file-download"></i> Download</a>
                                            </td>
                                        </tr>
                                        @php
                                            
                                            $no++;
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer p-0">
                            <div class="float-sm-right">
                                {{ $data->appends(['cari' => request()->get('cari')]) }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {


        });
    </script>
@endsection
