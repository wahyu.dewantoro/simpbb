<html>

<head>
    <style type="text/css">
    

        body {
            font-family: Arial;
            size: a4 landscape;
            margin-top: 20mm;
        }

        @page {
            font-family: Arial;
            size: a4 landscape;
            margin-top: 20mm;
        }

        .watermark {
            position: fixed;
            top: 10mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

    </style>
</head>
<body>
    <div class="watermark"><img src="{{ public_path('kabmalang.png') }}"></div>
    <p style="text-align: center ; font-size: 20px; margin-bottom: 5px; font-weight: bold">PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH</p>
    <p style="margin-top: 5px; font-size: 100px; font-weight: bold; text-align:center;  margin-bottom: 5px">D H K P</p>
    <P style="text-align: center; font-weight: bold; margin-bottom: 5px">DAFTAR HIMPUNAN KETETAPAN PAJAK & PEMBAYARAN BUKU {{ $jns }}<br>
        PAJAK BUMI & BANGUNAN<br>
        TAHUN : {{ $tahun_pajak }}
    </P>

    <P style="text-align: center">TANGGAL TERBIT : {{ STRTOUPPER(tglIndo(date('Y-m-d'))) }}</P>

    <table width="100%">
        <td width="35%">
            &nbsp;
        </td>
        <td>
            <table width="100%">
                <tr>
                    <td width="25%">PROPINSI</td>
                    <TD>: 35 - JAWA TIMUR</TD>
                </tr>
                <tr>
                    <td>DATI II</td>
                    <TD>: 07 - KABUPATEN MALANG</TD>
                </tr>
                <tr>
                    <TD>KECAMATAN</TD>
                    <td>: {{ $tempat->kd_kecamatan.' - '.$tempat->nm_kecamatan}}</td>
                </tr>
                <tr>
                    <TD>DESA/KELURAHAN</TD>
                    <td>: {{ $tempat->kd_kelurahan.' - '.$tempat->nm_kelurahan}}</td>
                </tr>
            </table>
        </td>
        <td width="25%">
            &nbsp;
        </td>
    </table>
</body>
</html>
