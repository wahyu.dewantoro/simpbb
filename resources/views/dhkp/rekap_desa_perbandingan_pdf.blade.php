<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    @include('layouts.kop_pdf')
    <h4 class="text-tengah">
        Rekap Perbandingan Ketetapan Per Desa<br>
        Kecamatan {{ $kecamatan }} Buku {{ implode(', ',$buku) }} Tahun Pajak {{ $tahun }}
    </h4>
    @include('dhkp.rekap_desa_perbandingan_table',['tahun'=>$tahun,'preview'=>$dhkp])
</body>

</html>
