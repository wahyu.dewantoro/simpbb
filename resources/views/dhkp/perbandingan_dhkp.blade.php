@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Daftar Perbandingan Ketetapan Per WP</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">


                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Preview Data</h3>
                        </div>
                        <div class="card-body p-1">
                            <form action="{{ url()->current() }}" class="form-inline">
                                <input type="number" value="{{ request()->get('tahun') ?? date('Y') }}"
                                    class="form-control form-control-sm mb-2 mr-sm-2" name="tahun">
                                <select required class="form-control form-control-sm mb-2 mr-sm-2" name="kd_kecamatan"
                                    id="kd_kecamatan">
                                    @if (count($kecamatan) > 1)
                                        <option value="">-- Kecamatan --</option>
                                    @endif
                                    @foreach ($kecamatan as $rowkec)
                                        <option @if (request()->get('kd_kecamatan') == $rowkec->kd_kecamatan) selected @endif
                                            value="{{ $rowkec->kd_kecamatan }}">
                                            {{ $rowkec->nm_kecamatan }}
                                        </option>
                                    @endforeach
                                </select>
                                <select class="form-control form-control-sm mb-2 mr-sm-2"
                                    @if (in_array('Desa', hasRole())) required @endif name="kd_kelurahan" id="kd_kelurahan">
                                    <option value="">-- Kelurahan / Desa -- </option>
                                </select>
                                <div class="form-check mb-2 mr-sm-2">
                                    @php
                                        $cb = request()->get('buku') ?? ['1', '2'];
                                        $pb = '';
                                        foreach ($cb as $ib => $bb) {
                                            $pb .= 'buku[' . $ib . ']=' . $bb . '&';
                                        }

                                        $pb = $pb != '' ? substr($pb, 0, -1) : '';

                                    @endphp
                                    @for ($i = 1; $i <= 5; $i++)
                                        <input @if (in_array($i, $cb)) checked @endif type="checkbox"
                                            name="buku[]" id="buku{{ $i }}" value="{{ $i }}"><label
                                            class="form-check-label" for="buku{{ $i }}"> Buku
                                            {{ $i }} &nbsp; &nbsp;</label>
                                    @endfor
                                </div>

                                <button class="btn btn-sm btn-info mb-2 mr-sm-2"><i class="fa fa-search"></i>
                                    Tampilkan</button>
                            </form>

                            <div class="table-responsive">
                                <table class="table table-sm table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center" rowspan='2'>No</th>
                                            <th style="text-align:center" rowspan='2'>Kecamatan</th>
                                            <th style="text-align:center" rowspan='2'>Kelurahan/Desa</th>
                                            <th style="text-align:center" rowspan='2'>Jumlah NOP</th>
                                            <th style="text-align:center" colspan="5">Jumlah Ketetapan</th>
                                            <th style="text-align:center" rowspan='2'>Unduh</th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center">PBB {{ request()->get('tahun') ?? date('Y') }}
                                            </th>
                                            <th style="text-align:center">Insentif
                                                {{ request()->get('tahun') ?? date('Y') }}</th>
                                            <th style="text-align:center">PBB
                                                {{ request()->get('tahun') != '' ? request()->get('tahun') - 1 : date('Y') - 1 }}
                                            <th style="text-align:center">Insentif
                                                {{ request()->get('tahun') != '' ? request()->get('tahun') - 1 : date('Y') - 1 }}
                                            </th>
                                            <th style="text-align:center">Selisih</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                            $a = 0;
                                            $b = 0;
                                            $c = 0;
                                            $d = 0;
                                            $bb = 0;
                                            $cc = 0;
                                        @endphp
                                        @foreach ($data['preview'] as $item)
                                            <tr>
                                                <td align="center">{{ $no }}</td>
                                                <td>{{ $item->nm_kecamatan }}</td>
                                                <td>{{ $item->nm_kelurahan }}</td>
                                                <td align="center">{{ number_format($item->nop, 0, '', '.') }}</td>
                                                <td align="right">{{ number_format($item->pbb_sekarang, 0, '', '.') }}
                                                </td>
                                                <td align="right">
                                                    {{ number_format($item->insentif_sekarang, 0, '', '.') }}
                                                </td>
                                                <td align="right">{{ number_format($item->pbb_kemarin, 0, '', '.') }}
                                                </td>
                                                <td align="right">{{ number_format($item->insentif_kemarin, 0, '', '.') }}
                                                </td>
                                                <td align="right">{{ number_format($item->selisih, 0, '', '.') }}</td>
                                                <td align="center">
                                                    {{-- @can('excel_dhkp_perbandingan') --}}
                                                    <a class="text-success"
                                                        href="{{ url('dhkp/perbandingan-excel') . '?' . $pb . '&tahun=' . request()->get('tahun') . '&kd_kecamatan=' . $item->kd_kecamatan . '&kd_kelurahan=' . $item->kd_kelurahan }}"><i
                                                            class="far fa-file-excel"></i></a>

                                                </td>
                                            </tr>
                                            @php
                                                $a += $item->nop;
                                                $b += $item->pbb_sekarang;
                                                $bb += $item->insentif_sekarang;
                                                $c += $item->pbb_kemarin;
                                                $cc += $item->insentif_kemarin;
                                                $d += $item->selisih;
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3">Jumlah</td>
                                            <td class="text-center"><strong>{{ angka($a) }}</strong></td>
                                            <td class="text-right"><strong>{{ angka($b) }}</strong></td>
                                            <td class="text-right"><strong>{{ angka($bb) }}</strong></td>

                                            <td class="text-right"><strong>{{ angka($c) }}</strong></td>
                                            <td class="text-right"><strong>{{ angka($cc) }}</strong></td>
                                            <td class="text-right"><strong>{{ angka($d) }}</strong></td>
                                            <td></td>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // console.log("ready!");
            $('#kd_kecamatan').on('change', function() {
                var kk = $('#kd_kecamatan').val();
                getKelurahan(kk);
            })

            var kd_kecamatan = "{{ request()->get('kd_kecamatan') }}";
            if (kd_kecamatan == '') {
                var kd_kecamatan = $('#kd_kecamatan').val()
            }
            getKelurahan(kd_kecamatan);

            function getKelurahan(kk) {
                var html = '<option value="">-- Kelurahan / Desa --</option>';
                if (kk != '') {

                    $.ajax({
                        url: "{{ url('desa') }}",
                        data: {
                            'kd_kecamatan': kk
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;
                            if (count == 1) {
                                html = '';
                            }
                            $.each(res, function(k, v) {
                                var apd = '<option value="' + k + '">' + v + '</option>';
                                html += apd;
                                if (count == 1) {
                                    $('#kd_kelurahan').val(k);
                                }
                            });
                            // console.log(res);
                            $('#kd_kelurahan').html(html);
                            if (count != 1) {
                                $('#kd_kelurahan').val("{{ request()->get('kd_kelurahan') }}")
                            }
                        },
                        error: function(res) {
                            $('#kd_kelurahan').html(html);
                        }
                    });
                } else {
                    $('#kd_kelurahan').html(html);
                }

            }
        });
    </script>
@endsection
