<html>

<head>
    @include('layouts.style_pdf')
</head>

<body>
    <table width="100%" class="">
        <tr>
            <th width="70px"><img width="60px" height="70px" src="{{ public_path('kabmalang.png') }}"></th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            {{-- <th width="100px"> --}}
                
                {{-- <img width="60px" src="{{ url('qrsppt') }}?nop={{ $sppt->kd_propinsi }}{{ $sppt->kd_dati2 }}{{ $sppt->kd_kecamatan }}{{ $sppt->kd_kelurahan }}{{ $sppt->kd_blok }}{{ $sppt->no_urut }}{{ $sppt->kd_jns_op }}{{ $sppt->thn_pajak_sppt }}" alt=""> --}}
            {{-- </th> --}}
        </tr>
    </table>
    <hr>
    <p style="text-align: center "><b>SURAT KETERANGAN NJOP</b><br> Nomor : {{ $sknjop->nomer_sk }}</p>
    <p>Yang bertanda tangan di bawah ini :</p>
    <table>
        <tbody>
            <tr>
                <td width="100px">Nama</td>
                <td>:</td>
                <td>{{ $sknjop->nama_kabid }}</td>
            </tr>
            <tr>
                <td width="100px">Jabatan</td>
                <td>:</td>
                <td>Kepala Bidang PBB P2</td>
            </tr>
        </tbody>
    </table>
    <p style="text-indent: 45px;">
        Sesuai Pasal 40 Undang-Undang Nomor 1 Tahun 2022 Tentang Hubungan Keuangan Antara Pemerintah Pusat dengan Pemerintahan Daerah dan Pasal 7 Peraturan Daerah Kabupaten Malang Nomor 7 Tahun 2023 tentang Pajak Daerah dan Retribusi Daerah
         Dengan
        ini menerangkan bahwa sesuai dengan basis data Badan Pendapatan Daerah atas objek pajak:</p>
    <table>
        <tbody>
            <tr>
                <td width="100px">Nomor Objek</td>
                <td>:</td>
                <td>{{ $sknjop->kd_propinsi . '.' . $sknjop->kd_dati2 . '.' . $sknjop->kd_kecamatan . '.' . $sknjop->kd_kelurahan . '.' . $sknjop->kd_blok . '-' . $sknjop->no_urut . '.' . $sknjop->kd_jns_op }}
                </td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ str_replace('null', ' ', $sknjop->alamat_op) }}</td>
            </tr>
        </tbody>
    </table>
    <p>Adalah sebagai berikut : </p>
    <table width="100%">
        <tbody>
            <tr>
                <td>Luas Bumi</td>
                <td width="1px">:</td>
                <td align="center">{{ $sknjop->luas_bumi }} M<sup>2</sup></td>
                <td></td>
                <td></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>Luas Bangunan</td>
                <td>:</td>
                <td align="center">{{ $sknjop->luas_bng }} M<sup>2</sup></td>
                <td></td>
                <td></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>NJOP Bumi</td>
                <td>:</td>
                <td align="center">{{ $sknjop->luas_bumi }}</td>
                <td align="center">x</td>
                <td align="right">{{ number_format($sknjop->njop_bumi / $sknjop->luas_bumi, 0, ',', '.') }}</td>
                <td align="center">=</td>
                <td align="right">{{ number_format($sknjop->njop_bumi, 0, ',', '.') }}</td>
            </tr>
            <tr>
                <td>NJOP Bangunan</td>
                <td>:</td>
                <td align="center">{{ $sknjop->luas_bng }}</td>
                <td align="center">x</td>
                <td align="right">
                    {{ $sknjop->njop_bng != 0 ? number_format($sknjop->njop_bng / $sknjop->luas_bng, 0, ',', '.') : 0 }}
                </td>
                <td align="center">=</td>
                <td style="  border-bottom: 1pt solid black;" align="right">
                    {{ number_format($sknjop->njop_bng, 0, ',', '.') }}</td>
            </tr>
            <tr>
                <td colspan="6">Nilai Jual Objek Pajak Keseluruhan</td>

                <td align="right">{{ number_format($sknjop->njop_pbb, 0, ',', '.') }}</td>
            </tr>
        </tbody>


    </table>
    <p><b>{{ ucwords(terbilang($sknjop->njop_pbb)) }} </b></p>
    <table>
        <tbody>
            <tr style="vertical-align: top">
                <td width="100px">Nama WP</td>
                <td>:</td>
                <td>{{ $sknjop->nama_wp }}
                </td>
            </tr>
            <tr style="vertical-align: top">
                <td>Alamat</td>
                <td>:</td>
                <td>{{ str_replace('null', ' ', $sknjop->alamat_wp) }}</td>
            </tr>
        </tbody>
    </table>
</body>
<p style="text-indent: 45px;">Demikian surat keterangan NJOP ini dibuat untuk dapat dipergunakan seperlunya. apabila di
    kemudian hari terdapat kekeliruan akan dibetulkan dan ditindaklanjuti sesuai dengan ketentuan yang berlaku.</p>
<table width="100%" border="0">
    <tbody>
        <tr>
            <td width="30%"></td>
            <td></td>
            <td width="35%" align="center">
                <p>Kepanjen, {{ tglIndo($sknjop->tanggal_sk) }}<br>A.N KEPALA BADAN PENDAPATAN DAERAH<br>Sekretaris<br>u.b.<br>Kepala Bidang PBB P2<br>
                    
                    {!! QrCode::size(70)->errorCorrection('M')->generate($url) !!}<br>
                    {{ $sknjop->nama_kabid}}<br>NIP.{{ $sknjop->nip_kabid}}
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p><i>NJOP Hanya untuk kepentingan PBB P2</i></p>
</html>

