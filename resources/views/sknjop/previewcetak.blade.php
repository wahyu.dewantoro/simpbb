@extends('layouts.app')

@section('content')
    <style>
        .custom1 {
            margin-left: auto !important;
            margin-right: auto !important;
            margin: 20px;
            padding: 20px;
        }

        .custom2 {
            position: relative;
            height: 0;
            overflow: hidden;
            padding-bottom: 90%;
        }

        .custom2 iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }

        #tutorial-pdf-responsive {
            max-width: 100%;
            max-height: auto;
            overflow: hidden;
        }



    </style>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        
                        <a href="{{ route('sknjop.create') }}" class="btn btn-info btn-flat btn-sm">
                            <i class="fas fa-file-medical"></i> Permohonan
                        </a>
                        
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="">
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div id="tutorial-pdf-responsive" class="custom1">
                        <div class="custom2">
                            <iframe src="{{ $filepdf }}" frameborder="0" style="overflow:hidden;height:100%;width:100%"
                                height="100%" width="100%"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
