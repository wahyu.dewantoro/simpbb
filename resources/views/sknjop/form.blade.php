@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1>Form SK NJOP</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="card card-success card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home"
                                aria-selected="true">Individu</a>
                        </li>

                    @if (Auth()->user()->hasRole('Wajib Pajak')==false)
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill"
                                href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile"
                                aria-selected="false">Kolektif</a>
                        </li>
                    @endif
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade show active" id="custom-tabs-four-home" role="tabpanel"
                            aria-labelledby="custom-tabs-four-home-tab">
                            <div class="callout callout-danger">
                                <h5>Catatan </h5>
                                <ol>
                                    <li>NIK adalah NIK wajib pajak bukan kuasa WP.</li>
                                    <li>Isian form ini harus benar dan sesuai dengan kondisi sebenarnya.</li>
                                </ol>
                              </div>
                            <form action="{{ $data['action'] }}" method="post">
                                @csrf
                                @method($data['method'])

                                <div class="">
                                    <div class="text-sm">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nik_pemohon" class="">NIK WP</label>
                                                            <div class="">
                                                                <input type="text" name="nik_pemohon" maxlength="16" id="nik_pemohon"
                                                                    class=" {{ $errors->has('nik_pemohon') ? 'is-invalid' : '' }}   form-control form-control-sm angka"
                                                                    value="{{ old('nik_pemohon') ?? ($sk->nik_pemohon ?? '') }}"
                                                                    placeholder="Masukan NIK">
                                                                <span
                                                                    class="errorr invalid-feedback">{{ $errors->first('nik_pemohon') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nomor_hp">Nomor HP</label>
                                                            <input type="text" name="nomor_hp" id="nomor_hp" required
                                                                class=" {{ $errors->has('nomor_hp') ? 'is-invalid' : '' }} form-control form-control-sm angka"
                                                                value="{{ old('nomor_hp') ?? ($sk->nomor_hp ?? '') }}"
                                                                placeholder="Nomor HP">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="nama_pemohon">Nama WP</label>
                                                            <input type="text" name="nama_pemohon" id="nama_pemohon"
                                                                class=" {{ $errors->has('nama_pemohon') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                                value="{{ old('nama_pemohon') ?? ($sk->nama_pemohon ?? '') }}"
                                                                placeholder="Nama WP">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="Alamat">Alamat</label>
                                                            <input type="text" name="alamat_pemohon" id="alamat_pemohon"
                                                                class=" {{ $errors->has('alamat_pemohon') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                                value="{{ old('alamat_pemohon') ?? ($sk->alamat_pemohon ?? '') }}"
                                                                placeholder="Alamat">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Desa / Kelurahan</label>
                                                            <input type="text" name="kelurahan_pemohon"
                                                                id="kelurahan_pemohon"
                                                                class=" {{ $errors->has('kelurahan_pemohon') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                                value="{{ old('kelurahan_pemohon') ?? ($sk->kelurahan_pemohon ?? '') }}"
                                                                placeholder="Kelurahan">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Kecamatan</label>
                                                            <input type="text" name="kecamatan_pemohon"
                                                                id="kecamatan_pemohon"
                                                                class=" {{ $errors->has('kecamatan_pemohon') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                                value="{{ old('kecamatan_pemohon') ?? ($sk->kecamatan_pemohon ?? '') }}"
                                                                placeholder="Kecamatan">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Dati II</label>
                                                            <input type="text" name="dati2_pemohon" id="dati2_pemohon"
                                                                class=" {{ $errors->has('dati2_pemohon') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                                value="{{ old('dat2_pemohon') ?? ($sk->dati2_pemohon ?? '') }}"
                                                                placeholder="Kota / Kabupaten">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Propinsi</label>
                                                            <input type="text" name="propinsi_pemohon" id="propinsi_pemohon"
                                                                class=" {{ $errors->has('propinsi_pemohon') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                                value="{{ old('propinsi_pemohon') ?? ($sk->propinsi_pemohon ?? '') }}"
                                                                placeholder="Propinsi">
                                                        </div>
                                                    </div>
                                                    <div>
                                                        {{-- <div id="preview_pemohon"></div> --}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label for="">Nomor Objek</label>
                                                            <input required type="text" name="nop" id="nop"
                                                                value="{{ old('nop') ?? '' }}"
                                                                class="form-control form-control-border form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}"
                                                                placeholder="Masukan nomor objek pajak (NOP)">
                                                            <span
                                                                class="errorr invalid-feedback">{{ $errors->first('nop') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <input type="hidden" name="alamat_op" id="alamat_op"
                                                            value="{{ old('alamat_op') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('alamat_op') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="nama_wp" id="nama_wp"
                                                            value="{{ old('nama_wp') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('nama_wp') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="alamat_wp" id="alamat_wp"
                                                            value="{{ old('alamat_wp') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('alamat_wp') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="luas_bumi" id="luas_bumi"
                                                            value="{{ old('luas_bumi') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('luas_bumi') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="njop_bumi" id="njop_bumi"
                                                            value="{{ old('njop_bumi') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('njop_bumi') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="luas_bng" id="luas_bng"
                                                            value="{{ old('luas_bng') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('luas_bng') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="njop_bng" id="njop_bng"
                                                            value="{{ old('njop_bng') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('njop_bng') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <input type="hidden" name="njop_pbb" id="njop_pbb"
                                                            value="{{ old('njop_pbb') ?? '' }}"
                                                            class="form-control form-control-sm {{ $errors->has('njop_pbb') ? 'is-invalid' : '' }}"
                                                            readonly>
                                                        <div id="preview_objek"></div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div id="preview_bayar"></div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>

                                        <div class="float-right">
                                            <button id="submit_idv" disabled class="btn btn-flat btn-primary"><i
                                                    class="fas fa-save"></i>
                                                Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel"
                            aria-labelledby="custom-tabs-four-profile-tab">
                            <form action="{{ url('sknjop-kolektif') }}" method="post">
                                @csrf
                                @method('post')
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nik_pemohon_kolektif" class="">NIK</label>
                                                    <div class="">
                                                        <input type="text" name="nik_pemohon_kolektif"
                                                            maxlength="16" id="nik_pemohon_kolektif" required
                                                            class=" {{ $errors->has('nik_pemohon_kolektif') ? 'is-invalid' : '' }}   form-control form-control-sm angka"
                                                            value="{{ old('nik_pemohon_kolektif') ?? ($sk->nik_pemohon_kolektif ?? '') }}"
                                                            placeholder="Masukan NIK">
                                                        <span
                                                            class="errorr invalid-feedback">{{ $errors->first('nik_pemohon_kolektif') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nomor_hp">Nomor HP</label>
                                                    <input type="text" name="nomor_hp_kolektif" id="nomor_hp_kolektif"
                                                        required
                                                        class=" {{ $errors->has('nomor_hp_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm angka"
                                                        value="{{ old('nomor_hp_kolektif') ?? ($sk->nomor_hp_kolektif ?? '') }}"
                                                        placeholder="Nomor HP">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nama_pemohon_kolektif">Nama</label>
                                                    <input type="text" name="nama_pemohon_kolektif" required
                                                        id="nama_pemohon_kolektif"
                                                        class=" {{ $errors->has('nama_pemohon_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                        value="{{ old('nama_pemohon_kolektif') ?? ($sk->nama_pemohon_kolektif ?? '') }}"
                                                        placeholder="Nama Pemohon">
                                                </div>
                                                <div class="form-group">
                                                    <label for="Alamat">Alamat</label>
                                                    <input type="text" name="alamat_pemohon_kolektif" required
                                                        id="alamat_pemohon_kolektif"
                                                        class=" {{ $errors->has('alamat_pemohon_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                        value="{{ old('alamat_pemohon_kolektif') ?? ($sk->alamat_pemohon_kolektif ?? '') }}"
                                                        placeholder="Alamat">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Desa / Kelurahan</label>
                                                    <input type="text" name="kelurahan_pemohon_kolektif" required
                                                        id="kelurahan_pemohon_kolektif"
                                                        class=" {{ $errors->has('kelurahan_pemohon_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                        value="{{ old('kelurahan_pemohon_kolektif') ?? ($sk->kelurahan_pemohon_kolektif ?? '') }}"
                                                        placeholder="Kelurahan">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Kecamatan</label>
                                                    <input type="text" name="kecamatan_pemohon_kolektif"
                                                        id="kecamatan_pemohon_kolektif" required
                                                        class=" {{ $errors->has('kecamatan_pemohon_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                        value="{{ old('kecamatan_pemohon_kolektif') ?? ($sk->kecamatan_pemohon_kolektif ?? '') }}"
                                                        placeholder="Kecamatan">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Dati II</label>
                                                    <input type="text" name="dati2_pemohon_kolektif"
                                                        id="dati2_pemohon_kolektif" required
                                                        class=" {{ $errors->has('dati2_pemohon_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                        value="{{ old('dat2_pemohon_kolektif') ?? ($sk->dati2_pemohon_kolektif ?? '') }}"
                                                        placeholder="Kota / Kabupaten">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Propinsi</label>
                                                    <input type="text" name="propinsi_pemohon_kolektif"
                                                        id="propinsi_pemohon_kolektif" required
                                                        class=" {{ $errors->has('propinsi_pemohon_kolektif') ? 'is-invalid' : '' }} form-control form-control-sm"
                                                        value="{{ old('propinsi_pemohon_kolektif') ?? ($sk->propinsi_pemohon_kolektif ?? '') }}"
                                                        placeholder="Propinsi">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        {{-- <fieldset id="buildyourform">
                                            <legend>Build your own form!</legend>
                                        </fieldset> --}}

                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="">Nomor Objek</label>
                                                    <input type="text" name="nop_cek" id="nop_cek"
                                                        class="form-control form-control-border form-control-sm "
                                                        placeholder="Masukan nomor objek pajak (NOP)">
                                                    <span id="info_nop_cek"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <table class="table-bordered table-sm">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">NOP</th>
                                                            <th class="text-center">Luas Bumi</th>
                                                            <th class="text-center">NJOP Bumi</th>
                                                            <th class="text-center">Luas BNG</th>
                                                            <th class="text-center">NJOP BNG</th>
                                                            <th class="text-center">NJOP</th>
                                                            <th class="text-center"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="bodykolektif">

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                                <div class="float-right">
                                    <button id="submit" class="btn btn-flat btn-primary"><i class="fas fa-save"></i>
                                        Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            @if (!empty($errors->has('nop')))
                $('#nop').trigger('keyup');
            @endif

            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);

                var b = nop.replace(/[^\d]/g, "");
                // var panjang = b.length;
                if (b.length == 18) {
                    // 
                    /* Swal.fire({
                        title: 'Sedang mencari ...',
                        html: '<span class="fa-2x"><i class="fa fa-spinner fa-pulse"></i></span>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    }); */
                    openloading()
                    // cek data
                    $.ajax({
                        url: "{{ url('api/objek') }}",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            'nop': b
                        },
                        success: function(res) {
                            if (res.kd_kecamatan != undefined) {
                                 if (res.lunas_lima == '1') {
                                    console.log(res.alamat_op)
                                    var alamat_op = res.alamat_op + ' ' + res.nm_kelurahan +
                                        ' ' +
                                        res.nm_kecamatan + ' Kab Malang';
                                    $('#alamat_op').val(alamat_op);
                                    $('#nama_wp').val(res.nm_wp_sppt);
                                    var blok_wp = '';
                                    if ($.trim(res.blok_kav_no_wp_sppt)) {
                                        blok_wp = res.blok_kav_no_wp_sppt;
                                    }
                                    var kel_wp = '';
                                    if ($.trim(res.kelurahan_wp_sppt)) {
                                        kel_wp = res.kelurahan_wp_sppt;
                                    }
                                    var kota_wp = '';
                                    if ($.trim(res.kota_wp_sppt)) {
                                        kota_wp = res.kota_wp_sppt;
                                    }

                                    var alamat_wp =
                                        res.jln_wp_sppt + ' ' +
                                        blok_wp + ' ' +
                                        kel_wp + ' ' +
                                        kota_wp;
                                    $('#alamat_wp').val(alamat_wp);

                                    $('#luas_bumi').val(res.luas_bumi_sppt);
                                    $('#njop_bumi').val(res.njop_bumi_sppt);
                                    $('#luas_bng').val(res.luas_bng_sppt);
                                    $('#njop_bng').val(res.njop_bng_sppt);
                                    var njop_pbb = parseInt(res.njop_bumi_sppt) + parseInt(res
                                        .njop_bng_sppt);
                                    $('#njop_pbb').val(parseInt(njop_pbb, 10));


                                    html = "<table class='table table-sm table-borderless'>" +
                                        "<tr><td>Alamat</td><td>:</td><td>" + alamat_op +
                                        "</td></tr>" +
                                        "<tr><td>Wajib Pajak</td><td>:</td><td>" + res
                                        .nm_wp_sppt +
                                        "</td></tr>" +
                                        "<tr><td>Alamat WP</td><td>:</td><td>" + alamat_wp +
                                        "</td></tr>" +
                                        "<tr><td>Luas Bumi</td><td>:</td><td>" + res
                                        .luas_bumi_sppt + "</td></tr>" +
                                        "<tr><td>NJOP Bumi</td><td>:</td><td>" + res
                                        .njop_bumi_sppt +
                                        "</td></tr>" +
                                        "<tr><td>Luas Bangunan</td><td>:</td><td>" + res
                                        .luas_bng_sppt + "</td></tr>" +
                                        "<tr><td>NJOP Bangunan</td><td>:</td><td>" + res
                                        .njop_bng_sppt +
                                        "</td></tr>" +
                                        "<tr><td>NJOP PBB</td><td>:</td><td>" + njop_pbb +
                                        "</td></tr>" +
                                        "</table>";
                                    $('#preview_objek').html(html);
                                    $('#submit_idv').prop('disabled', false)
                                    Swal.close();
                                 } else {
                                    $('#submit_idv').prop('disabled', true)
                                    Swal.fire({
                                        title: 'Ada tunggakan PBB yang harus di bayar',
                                        icon: 'error',
                                    });
                                } 

                            } else {
                                $('#preview_objek').html('');
                                $('#alamat_op').val('');
                                $('#nama_wp').val('');
                                $('#alamat_wp').val('');
                                $('#luas_bumi').val('');
                                $('#njop_bumi').val('');
                                $('#luas_bng').val('');
                                $('#njop_bng').val('');
                                $('#njop_pbb').val('');

                                Swal.fire({
                                    title: 'NOP tidak ditemukan',
                                    icon: 'error',
                                });
                            }
                        },
                        error: function(er) {
                            console.log(er);
                            $('#preview_objek').html('');
                            $('#alamat_op').val('');
                            $('#nama_wp').val('');
                            $('#alamat_wp').val('');
                            $('#luas_bumi').val('');
                            $('#njop_bumi').val('');
                            $('#luas_bng').val('');
                            $('#njop_bng').val('');
                            $('#njop_pbb').val('');

                            Swal.fire({
                                title: 'Terjadi kesalahan dalam sistem',
                                // html: '<span class="fa-3x pd-5"><i class="far fa-times-circle"></i></span>',
                                icon: 'error',
                            });
                        }
                    });

                    // cek bayar
                    $.ajax({
                        url: "{{ url('api/objek-cekbayar') }}",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            'nop': b
                        },
                        success: function(res) {
                            var total, lunas, table_bayar;

                            total = res.length;
                            lunas = 0;
                            table_bayar =
                                '<table class="table table-striped table-sm"><thead><tr><th>Tahun</th><th>Status</th></thead><tbody>';
                            $.each(res, function(k, v) {
                                /// do stuff
                                if (v.kd_status == 1) {
                                    lunas = lunas + 1;
                                }
                                table_bayar += '<tr><td>' + v.thn_pajak_sppt +
                                    '</td><td>' + v.status + '</td></tr>';
                            });
                            table_bayar += '</tbody></table>';


                            if (lunas == total) {
                                $('#submit').prop('disabled', false);
                                table_bayar +=
                                    '<span class="text-success"><i class="fas fa-check"></i></span> Telah memenuhi syarat pembayaran';
                            } else {
                                $('#submit').prop('disabled', false);
                                table_bayar += '';
                                // '<span class="text-danger"><i class="far fa-times-circle"></i></span>    Tidak memenuhi syarat pembayaran, silahkan melunasi tagihan sppt ';
                            }

                            $('#preview_bayar').html(table_bayar);

                            // console.log('total : ' + total + ' , Lunas :' + lunas);
                        },
                        error: function(er) {
                            console.log(er);
                            $('#submit"]').prop('disabled', true);
                            $('#preview_bayar').html('');
                            Swal.fire({
                                title: 'Terjadi kesalahan dalam sistem',
                                icon: 'error',
                            });
                        }
                    });

                } else {
                    $('#preview_objek').html('');
                    $('#alamat_op').val('');
                    $('#nama_wp').val('');
                    $('#alamat_wp').val('');
                    $('#luas_bumi').val('');
                    $('#njop_bumi').val('');
                    $('#luas_bng').val('');
                    $('#njop_bng').val('');
                    $('#njop_pbb').val('');
                }
            });

            $('#nik_pemohon_').on('keyup', function() {
                var nik = $(this).val()

                if (nik.length == 16) {
                    Swal.fire({
                        title: 'Sedang mencari ...',
                        html: '<span class="fa-2x"><i class="fa fa-spinner fa-pulse"></i></span>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                    $.ajax({
                        url: "{{ url('api/ceknik') }}",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            'nik': nik
                        },
                        success: function(res) {
                            console.log(res.raw);
                            if (res.kode == '1') {
                                var html = "<table class='table table-sm table-borderless'>" +
                                    "<tr><td>Nama</td><td>:</td><td>" + res.raw.nama_lgkp +
                                    "</td></tr>" +
                                    "<tr><td>Alamat</td><td>:</td><td>" + res.raw.alamat +
                                    "</td></tr>" +
                                    "<tr><td>Desa / Kelurahan</td><td>:</td><td>" + res.raw
                                    .kel_name + "</td></tr>" +
                                    "<tr><td>Kecamatan</td><td>:</td><td>" + res.raw.kec_name +
                                    "</td></tr>" +
                                    "<tr><td>Kabupaten / Kota </td><td>:</td><td>" + res.raw
                                    .kab_name + "</td></tr>" +
                                    "<tr><td>Propinsi</td><td>:</td><td>" + res.raw.prop_name +
                                    "</td></tr>" +
                                    "</table>";
                                $('#preview_pemohon').html(html);

                                $('#nama_pemohon').val(res.raw.nama_lgkp);
                                $('#alamat_pemohon').val(res.raw.alamat);
                                $('#kelurahan_pemohon').val(res.raw.kel_name);
                                $('#kecamatan_pemohon').val(res.raw.kec_name);
                                $('#dati2_pemohon').val(res.raw.kab_name);
                                $('#propinsi_pemohon').val(res.raw.prop_name);
                                Swal.close();
                            } else {
                                $('#preview_pemohon').html('');
                                Swal.fire({
                                    title: res.kode,
                                    text: res.keterangan,
                                    icon: 'error',
                                });
                            }
                        },
                        error: function(er) {
                            console.log(er);
                            $('#preview_pemohon').html('');
                            $('#nama_pemohon').val('');
                            $('#alamat_pemohon').val('');
                            $('#kelurahan_pemohon').val('');
                            $('#kecamatan_pemohon').val('');
                            $('#dati2_pemohon').val('');
                            $('#propinsi_pemohon').val('');

                            Swal.fire({
                                title: 'Terjadi kesalahan dalam sistem',
                                icon: 'error',
                            });
                        }
                    });


                } else {
                    $('#preview_pemohon').html('');
                    $('#nama_pemohon').val('');
                    $('#alamat_pemohon').val('');
                    $('#kelurahan_pemohon').val('');
                    $('#kecamatan_pemohon').val('');
                    $('#dati2_pemohon').val('');
                    $('#propinsi_pemohon').val('');
                }


            });

            // cek nik ajuan kolektif
            $('#nik_pemohon_kolektif').on('keyup', function() {
                var nik = $(this).val()

                if (nik.length == 16) {
                    Swal.fire({
                        title: 'Sedang mencari ...',
                        html: '<span class="fa-2x"><i class="fa fa-spinner fa-pulse"></i></span>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                    $.ajax({
                        url: "{{ url('api/ceknik') }}",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            'nik': nik
                        },
                        success: function(res) {
                            console.log(res.raw);
                            if (res.kode == '1') {
                                var html = "<table class='table table-sm table-borderless'>" +
                                    "<tr><td>Nama</td><td>:</td><td>" + res.raw.nama_lgkp +
                                    "</td></tr>" +
                                    "<tr><td>Alamat</td><td>:</td><td>" + res.raw.alamat +
                                    "</td></tr>" +
                                    "<tr><td>Desa / Kelurahan</td><td>:</td><td>" + res.raw
                                    .kel_name + "</td></tr>" +
                                    "<tr><td>Kecamatan</td><td>:</td><td>" + res.raw.kec_name +
                                    "</td></tr>" +
                                    "<tr><td>Kabupaten / Kota </td><td>:</td><td>" + res.raw
                                    .kab_name + "</td></tr>" +
                                    "<tr><td>Propinsi</td><td>:</td><td>" + res.raw.prop_name +
                                    "</td></tr>" +
                                    "</table>";
                                $('#preview_pemohon_kolektif').html(html);

                                $('#nama_pemohon_kolektif').val(res.raw.nama_lgkp);
                                $('#alamat_pemohon_kolektif').val(res.raw.alamat);
                                $('#kelurahan_pemohon_kolektif').val(res.raw.kel_name);
                                $('#kecamatan_pemohon_kolektif').val(res.raw.kec_name);
                                $('#dati2_pemohon_kolektif').val(res.raw.kab_name);
                                $('#propinsi_pemohon_kolektif').val(res.raw.prop_name);
                                Swal.close();
                            } else {
                                $('#preview_pemohon_kolektif').html('');
                                Swal.fire({
                                    title: res.kode,
                                    text: res.keterangan,
                                    icon: 'error',
                                });
                            }
                        },
                        error: function(er) {
                            console.log(er);
                            $('#preview_pemohon_kolektif').html('');
                            $('#nama_pemohon_kolektif').val('');
                            $('#alamat_pemohon_kolektif').val('');
                            $('#kelurahan_pemohon_kolektif').val('');
                            $('#kecamatan_pemohon_kolektif').val('');
                            $('#dati2_pemohon_kolektif').val('');
                            $('#propinsi_pemohon_kolektif').val('');

                            Swal.fire({
                                title: 'Terjadi kesalahan dalam sistem',
                                icon: 'error',
                            });
                        }
                    });


                } else {
                    $('#preview_pemohon_kolektif').html('');
                    $('#nama_pemohon_kolektif').val('');
                    $('#alamat_pemohon_kolektif').val('');
                    $('#kelurahan_pemohon_kolektif').val('');
                    $('#kecamatan_pemohon_kolektif').val('');
                    $('#dati2_pemohon_kolektif').val('');
                    $('#propinsi_pemohon_kolektif').val('');
                }


            });




            $('#nop_cek').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
                var b = nop.replace(/[^\d]/g, "");

                $('#info_nop_cek').html('');
                if (b.length == 18) {
                    Swal.fire({
                        title: 'Sedang mencari ...',
                        html: '<span class="fa-2x"><i class="fa fa-spinner fa-pulse"></i></span>',
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });

                    // $(info_nop_cek

                    // cek data
                    $.ajax({
                        url: "{{ url('api/objek') }}",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {
                            'nop': b
                        },
                        success: function(res) {
                            if (res.kd_kecamatan != undefined) {

                                if (res.lunas_lima == '1') {

                                    $('#nop_cek').val('');
                                    var alamat_op = res.alamat_op + ' ' + res.nm_kelurahan +
                                        ' ' +
                                        res.nm_kecamatan + ' KAB MALANG';
                                    var blok_wp = '';
                                    if ($.trim(res.blok_kav_no_wp_sppt)) {
                                        blok_wp = res.blok_kav_no_wp_sppt;
                                    }
                                    var kel_wp = '';
                                    if ($.trim(res.kelurahan_wp_sppt)) {
                                        kel_wp = res.kelurahan_wp_sppt;
                                    }
                                    var kota_wp = '';
                                    if ($.trim(res.kota_wp_sppt)) {
                                        kota_wp = res.kota_wp_sppt;
                                    }

                                    var alamat_wp =
                                        res.jln_wp_sppt + ' ' +
                                        blok_wp + ' ' +
                                        kel_wp + ' ' +
                                        kota_wp;

                                    var njop_pbb = parseInt(res.njop_bumi_sppt) + parseInt(res
                                        .njop_bng_sppt);

                                    $('#info_nop_cek').html('NOP valid dan lunas 5 tahun');

                                    var lastField = $("#bodykolektif tr:last");
                                    var intId = (lastField && lastField.length && lastField
                                        .data(
                                            "idx") + 1) || 1;
                                    var fieldWrapper = $(
                                        "<tr class=\"fieldwrapper\" id=\"field" +
                                        intId + "\"/>");
                                    fieldWrapper.data("idx", intId);


                                    var rownop = $(
                                        "<td><input type=\"hidden\" name=\"nop_kolektif[]\" value='" +
                                        convert +
                                        "' ><input type=\"hidden\" name=\"alamat_op_kolektif[]\" value='" +
                                        alamat_op +
                                        "' ><input type=\"hidden\" name=\"nama_wp_kolektif[]\" value=\"" +
                                        res.nm_wp_sppt +
                                        "\"><input type=\"hidden\" name=\"alamat_wp_kolektif[]\"  value='" +
                                        alamat_wp +
                                        "'><input type=\"hidden\" name=\"luas_bumi_kolektif[]\" value='" +
                                        res.luas_bumi_sppt +
                                        "'><input type=\"hidden\" name=\"njop_bumi_kolektif[]\" value='" +
                                        res.njop_bumi_sppt +
                                        "'><input type=\"hidden\" name=\"luas_bng_kolektif[]\" value='" +
                                        res.luas_bng_sppt +
                                        "'><input type=\"hidden\" name=\"njop_bng_kolektif[]\" value='" +
                                        res.njop_bng_sppt +
                                        "'><input type=\"hidden\" name=\"njop_pbb_kolektif[]\" value='" +
                                        njop_pbb + "'>" + convert + "</td>");
                                    var rowluas_bumi = $("<td>" + res.luas_bumi_sppt + "</td>");
                                    var rownjop_bumi = $("<td>" + res.njop_bumi_sppt + "</td>");
                                    var rowluas_bng = $("<td>" + res.luas_bng_sppt + "</td>");
                                    var rownjop_bng = $("<td>" + res.njop_bng_sppt + "</td>");
                                    var rownjop_pbb = $("<td>" + njop_pbb + "</td>");



                                    var removeButton = $(
                                        "<td>  <input type=\"button\" class=\"remove\" value=\"-\" /></td>"
                                    );
                                    removeButton.click(function() {
                                        $(this).parent().remove();
                                    });


                                    fieldWrapper.append(rownop);
                                    fieldWrapper.append(rowluas_bumi);
                                    fieldWrapper.append(rownjop_bumi);
                                    fieldWrapper.append(rowluas_bng);
                                    fieldWrapper.append(rownjop_bng);
                                    fieldWrapper.append(rownjop_pbb);
                                    fieldWrapper.append(removeButton);
                                    $("#bodykolektif").append(fieldWrapper);
                                    Swal.close();
                                } else {
                                    $('#info_nop_cek').html('NOP Belum  lunas  pembayaran 5 tahun');
                                    Swal.fire({
                                        title: 'Ada tunggakan PBB yang harus di bayar',
                                        icon: 'error',
                                    });

                                }
                                
                            } else {

                                $('#alamat_op_cek').val('');
                                $('#nama_wp_cek').val('');
                                $('#alamat_wp_cek').val('');
                                $('#luas_bumi_cek').val('');
                                $('#njop_bumi_cek').val('');
                                $('#luas_bng_cek').val('');
                                $('#njop_bng_cek').val('');
                                $('#njop_pbb_cek').val('');
                                $('#info_nop_cek').html('NOP tidak ditemukan');
                                Swal.fire({
                                    title: 'NOP tidak ditemukan',
                                    icon: 'error',
                                });
                            }
                        },
                        error: function(er) {
                            console.log(er);
                            $('#alamat_op_cek').val('');
                            $('#nama_wp_cek').val('');
                            $('#alamat_wp_cek').val('');
                            $('#luas_bumi_cek').val('');
                            $('#njop_bumi_cek').val('');
                            $('#luas_bng_cek').val('');
                            $('#njop_bng_cek').val('');
                            $('#njop_pbb_cek').val('');
                            $('#info_nop_cek').html('Terjadi kesalahan dalam sistem');
                            Swal.fire({
                                title: 'Terjadi kesalahan dalam sistem',
                                icon: 'error',
                            });
                        }
                    });



                } else {
                    $('#info_nop_cek').html('');
                    $('#alamat_op_cek').val('');
                    $('#nama_wp_cek').val('');
                    $('#alamat_wp_cek').val('');
                    $('#luas_bumi_cek').val('');
                    $('#njop_bumi_cek').val('');
                    $('#luas_bng_cek').val('');
                    $('#njop_bng_cek').val('');
                    $('#njop_pbb_cek').val('');
                }


            });

        });
    </script>
@endsection
