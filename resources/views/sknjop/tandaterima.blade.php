<html>

<head>
    <title>Tanda terima</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }

        @page {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
        }

        body {
            margin-top: 2mm;
            margin-bottom: 2mm;
            margin-left: 2mm;
            margin-right: 2mm;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
        }

        table {
            font-weight: bold;
            font-family: 'Courier New', Courier, monospace;
            font-size: 0.9em;
        }

    </style>
</head>

<body>
    @php
        $countnop = $permohonan->nop()->count();
    @endphp
    <table width="100%" border="0">
        <tr>
            <td>
                <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    JL. RADEN PANJI NOMOR 158 KEPANJEN </P>
                <hr>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="45%"></td>
            <td>
                <table cellpadding="3" width="100%">
                    <tbody>
                        <tr style="vertical-align: top">
                            <td width="5%">1.</td>
                            <td width="46%">NOMOR PELAYANAN</td>
                            <td width="1%">:</td>
                            <td>{{ $permohonan->nomor_layanan }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td>2.</td>
                            <td>TANGGAL PELAYANAN</td>
                            <td>:</td>
                            <td>{{ tglIndo($permohonan->tanggal_permohonan) }}</td>
                        </tr>
                        <tr style="vertical-align: top">
                            <td>3.</td>
                            <td>TGL SELESAI (PERKIRAAN)</td>
                            <td>:</td>
                            <td>{{ tglIndo($permohonan->tanggal_permohonan) }}</td>
                        </tr>
                    </tbody>
                </table>

            </td>
        </tr>
    </table>
    <br><br>
    <table cellpadding="3" width="100%">
        <tr>
            <td width="3%">4.</td>
            <td width="25%">JENIS PELAYANAN</td>
            <td width="3%">:</td>
            <td>SK NJOP ({{ $countnop == 1 ? 'Individu' : 'Kolektif' }})</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>N O P</td>
            <td>:</td>
            <td>
                @php
                    $np = $permohonan->nop()->first();
                    echo formatnop($np->kd_propinsi . $np->kd_dati2 . $np->kd_kecamatan . $np->kd_kelurahan . $np->kd_blok . $np->no_urut . $np->kd_jns_op);
                @endphp
            </td>
        </tr>
    </table>
    <br><br>
    <hr>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center">
                A. DATA WAJIB / OBJEK PAJAK
            </td>
        </tr>
    </table>
    <hr>
    <br><br>
    <table cellpadding="3" width="100%">
        <tbody>
            <tr>
                <td width="3%">6.</td>
                <td width="25%">NAMA PEMOHON / NIK </td>
                <td width="3%">:</td>
                <td> {{ $permohonan->nama_pemohon }} / {{ $permohonan->nik_pemohon }} </td>
            </tr>
            <tr style="vertical-align: top">
                <td></td>
                <td>ALAMAT PEMOHON</td>
                <td>:</td>
                <td>{{ $permohonan->alamat_pemohon }} DS/KEL. {{ $permohonan->kelurahan_pemohon }} KEC
                    {{ $permohonan->kecamatan_pemohon }} {{ $permohonan->dati2_pemohon }}
                    {{ $permohonan->propinsi_pemohon }}</td>
            </tr>
            <tr>
                <td></td>
                <td>NO TELP</td>
                <td>:</td>
                <td>{{ $permohonan->nomor_hp }}</td>
            </tr>
            <tr>
                <td><br></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr style="vertical-align: top">
                <td>7.</td>
                <td>LETAK OBJEK PAJAK</td>
                <td>:</td>
                <td>{{ $np->alamat_op }}</td>
            </tr>

            <tr>
                <td>8.</td>
                <td>KETERANGAN</td>
                <td>:</td>
                <td>-</td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <hr>
    <table cellpadding="3" width="100%">
        <tr>
            <td align="center">
                B. PENERIMAAN BERKAS
            </td>
        </tr>
    </table>
    <hr>
    <br><br>
    <table cellpadding="3" width="100%">
        <tr>
            <td>9. DOKUMEN DILAMPIRKAN :</td>

        </tr>
    </table>
    <br>
    <table cellpadding="3" border="0">
        <tr>
            <td width="2%">&nbsp;&nbsp;</td>
            <td width="30%" valign="top">
                <table cellpadding="3" width="100%">
                    <tr>

                        <td>1.</td>
                        <td>Pengajuan Permohonan </td>
                    </tr>
                    <tr>

                        <td>2.</td>
                        <td>Surat Kuasa </td>
                    </tr>
                    <tr>

                        <td>3.</td>
                        <td>FC KTP/SIM dan KK </td>
                    </tr>
                    <tr>

                        <td>4.</td>
                        <td>FC Sertifikat / Kepemilikan </td>
                    </tr>
                    <tr>

                        <td>5.</td>
                        <td>Asli SPPT </td>
                    </tr>
                    <tr>

                        <td>6.</td>
                        <td>FC IMB </td>
                    </tr>
                    <tr>

                        <td>7.</td>
                        <td>FC akte jual beli/hibah </td>
                    </tr>
                </table>
            </td>
            <td width="30%" valign="top">
                <table cellpadding="3" width="100%">
                    <tr>

                        <td>8.</td>
                        <td> FC SK Pensiun / Veteran </td>
                    </tr>
                    <tr>

                        <td>9.</td>
                        <td> FC SPPT / SSPD / Bukti lunas</td>
                    </tr>
                    <tr>

                        <td>10.</td>
                        <td>Asli BBPD / Bukti Lunas</td>
                    </tr>
                    <tr>

                        <td>11.</td>
                        <td>FC SK Pengurangan</td>
                    </tr>
                    <tr>

                        <td>12.</td>
                        <td>FC SK Keberatan</td>
                    </tr>
                    <tr>

                        <td>13.</td>
                        <td>FC SSPD BPHTB</td>
                    </tr>
                    <tr>

                        <td>20.</td>
                        <td>SKTM </td>
                    </tr>
                </table>
            </td>
            <td width="30%" valign="top">
                <table cellpadding="3" width="100%">
                    <tr>

                        <td>15.</td>
                        <td>Sket tanah </td>
                    </tr>
                    <tr>

                        <td>16.</td>
                        <td>Sket Lurah </td>
                    </tr>
                    <tr>

                        <td>17.</td>
                        <td>NPWP/NPWPD </td>
                    </tr>
                    <tr>

                        <td>18.</td>
                        <td>Rincian Penghasil </td>

                    </tr>
                    <tr>

                        <td>12.</td>
                        <td>SK Cagar budaya </td>

                    </tr>
                    <tr>

                        <td>20.</td>
                        <td>Lain - lain </td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br><br>
    <table cellpadding="3" width="100%">
        <tr>
            <td>10. Catatan<br></td>
        </tr>
        <tr>
            <td><br>............................................................................................................................................
            </td>
        </tr>
    </table>
    <br><br>
    <table cellpadding="3" width="100%">
        <tr>
            <td>PETUGAS PENERIMA BERKAS: {{ $permohonan->penerima->nama }}</td>
            <td align="center">NIP: -</td>
        </tr>
        <tr>
            <td align="center"><br>------------------------------------ Gunting
                Disini------------------------------------ <br></td>
        </tr>
        <tr>
            <td align="center">
                <p>FORMULIR PELAYANAN WAJIB PAJAK<br>
                    BADAN PENDAPATAN DAERAH KABUPATEN MALANG<br>
                    JL. RADEN PANJI NOMOR 158 KEPANJEN </P>
            </td>
        </tr>
    </table>
    <table cellpadding="3" width="100%">
        <tr valign="top">
            <td width="50%">
                <table>
                    <tr>
                        <td wdth="3px">11.</td>
                        <td>N O P</td>
                        <td width='1px'>:</td>
                        <td><?php echo formatnop($np->kd_propinsi . $np->kd_dati2 . $np->kd_kecamatan . $np->kd_kelurahan . $np->kd_blok . $np->no_urut . $np->kd_jns_op); ?></td>
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td>NOMOR PELAYANAN</td>
                        <td>:</td>
                        <td><?php echo $permohonan->nomor_layanan; ?></td>
                    </tr>
                    <tr>
                        <td width="3px">13.</td>
                        <td>TANGGAL PELAYANAN</td>
                        <td width="1px">:</td>
                        <td><?php echo tglIndo($permohonan->tanggal_permohonan); ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table cellpadding="3" width="100%" border="0">

                    <tr>
                        <td>14.</td>
                        <td>TGL SELESAI (PERKIRAAN)</td>
                        <td>:</td>
                        <td><?php echo tglIndo($permohonan->tanggal_permohonan); ?></td>
                    </tr>

                    <tr>
                        <td>15.</td>
                        <td>PETUGAS PENERIMAN BERKAS</td>
                        <td>:</td>
                        <td>{{ $permohonan->penerima->nama }}</td>
                    </tr>

                </table>

            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">

                <hr>
                TANDA PENDAFTARAN PELAYANAN
                <hr>

            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>16.</td>
                        <td>URUSAN</td>
                        <td>:</td>
                        <td> SK NJOP ({{ $countnop == 1 ? 'Individu' : 'Kolektif' }})</td>
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td>CATATAN</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @if ($countnop > 1)
        <div class="page-break"></div>

        <style>
            td.lampiran {
                border-right: 1px solid #C6C6C6;
            }

            lampiran.th {
                border-right: 1px solid #C6C6C6;
                border-bottom: 1px solid #C6C6C6;
            }

        </style>

        <h3 style="font-size: 12px; font-weight:bold">
            LAMPIRAN TANDA TERIMA PELAYANAN KOLEKTIF</h3>
        <table style="border: 1px solid #C6C6C6; font-size:12px;" cellpadding="5" cellspacing="0" width="80%">
            <thead>
                <tr>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">No</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">NOP</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">Nama WP</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">Tahun</th>
                    <th style="   border-right: 1px solid #C6C6C6;
             border-bottom: 1px solid #C6C6C6;">Jenis Pelayanan</th>
                </tr>
            </thead>
            @foreach ($permohonan->nop()->get() as $idx => $kolek)
                <tr>
                    <td style="border-right: 1px solid #C6C6C6; text-align:center">{{ $idx + 1 }}</td>
                    <td style=" border-right: 1px solid #C6C6C6; text-align:center">
                        {{ formatnop($kolek->kd_propinsi . $kolek->kd_dati2 . $kolek->kd_kecamatan . $kolek->kd_kelurahan . $kolek->kd_blok . $kolek->no_urut . $kolek->kd_jns_op) }}
                    </td>
                    <td style=" border-right: 1px solid #C6C6C6;">{{ $kolek->nama_wp }}</td>
                    <td style=" border-right: 1px solid #C6C6C6; text-align:center">
                        {{ date('Y', strtotime($permohonan->tanggal_permohonan)) }}</td>
                    <td style=" border-right: 1px solid #C6C6C6;text-align:center">SK NJOP</td>
                </tr>
            @endforeach
        </table>


    @endif

</body>

</html>
