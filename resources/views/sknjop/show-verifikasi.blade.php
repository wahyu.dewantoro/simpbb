@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Proses Permohonanan SK NJOP</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        @if ($permohonan->kd_status == '1')
                            <a href="{{ url('sknjop-pdf', $permohonan->id) }}" class="btn btn-flat btn-warning"> <i
                                    class="fas fa-print"></i> cetak </a>
                        @endif
                        {{-- <a href="{{ url('sknjop-tanda-cetak', $permohonan->id) }}" class="btn btn-sm btn-flat btn-info"> <i
                            class="fas fa-print"></i> tanda terima </a> --}}

                        <a href="{{ route('sknjop-verifikasi.index') }}" class="btn btn-warning btn-flat btn-sm">
                            <i class="fas fa-angle-double-left"></i> Kembali
                        </a>
                        {{-- @endcan --}}
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-info">
                        <div class="card-header ">
                            <h3 class="card-title">Data WP / Penanggung Pajak</h3>
                        </div>
                        <div class="card-body p-1">
                            <table class="table table-borderless table-sm text-sm">
                                <tbody>
                                    <tr>
                                        <td>Nomor Layanan</td>
                                        <td>:</td>
                                        <td>{{ $permohonan->nomor_layanan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>:</td>
                                        <td>{{ tglindo($permohonan->tanggal_permohonan) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nomor HP</td>
                                        <td>:</td>
                                        <td>{{ $permohonan->nomor_hp }}</td>
                                    </tr>
                                    <tr>
                                        <td width="120px">NIK WP</td>
                                        <td width="1px">:</td>
                                        <td>{{ $permohonan->nik_pemohon }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama WP</td>
                                        <td>:</td>
                                        <td>{{ $permohonan->nama_pemohon }}</td>
                                    </tr>

                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>{{ $permohonan->alamat_pemohon }}<br> DS/KEL.
                                            {{ $permohonan->kelurahan_pemohon }}
                                            KEC {{ $permohonan->kecamatan_pemohon }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kab / Kota</td>
                                        <td>:</td>
                                        <td>{{ $permohonan->dati2_pemohon }}</td>
                                    </tr>
                                    <tr>
                                        <td>Propinsi</td>
                                        <td>:</td>
                                        <td>{{ $permohonan->propinsi_pemohon }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Data Yang tercantum di SPPT</h3>                           
                        </div>
                        <div class="card-body p-1 text-sm">
                            <div class="row">
                                @php
                                    $nop_cetak = '';
                                    // $status_cetak='';
                                @endphp
                                @if ($permohonan->nop()->count() == 1)
                                    <div class="col-md-6">
                                        @php
                                            $sknjop = $permohonan->nop()->first();
                                        @endphp
                                        <table class="table table-borderless table-sm">
                                            <tbody>
                                                <tr>
                                                    <td width="100px">NOP</td>
                                                    <td width="1px">:</td>
                                                    <td>
                                                        {{ formatnop($sknjop->kd_propinsi.
                                                        $sknjop->kd_dati2.
                                                        $sknjop->kd_kecamatan.
                                                        $sknjop->kd_kelurahan.
                                                        $sknjop->kd_blok.
                                                        $sknjop->no_urut.
                                                        $sknjop->kd_jns_op) }}

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat Objek</td>
                                                    <td>:</td>
                                                    <td>{{ str_replace('null', ' ', $sknjop->alamat_op) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Wajib Pajak</td>
                                                    <td>:</td>
                                                    <td>{{ $sknjop->nama_wp }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat WP</td>
                                                    <td>:</td>
                                                    <td>{{ str_replace('null', ' ', $sknjop->alamat_wp) }}</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    {{-- <div class="col-md-6">
                                        <table class="table table-borderless table-sm">
                                            <tbody>
                                                <tr>
                                                    <td width="100px">Tanggal SK</td>
                                                    <td width="1px">:</td>
                                                    <td>{{ $sknjop->tanggal_sk != '' ? tglIndo($sknjop->tanggal_sk) : '' }}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>No SKNJOP</td>
                                                    <td>:</td>
                                                    <td>{{ $sknjop->nomer_sk }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama TTD</td>
                                                    <td>:</td>
                                                    <td>{{ $sknjop->nama_kabid }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> --}}
                                    <div class="col-md-6">
                                        <table class="table table-sm table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Objek</th>
                                                    <th class="text-center">Luas</th>
                                                    <th class="text-center">NJOP</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Bumi</td>
                                                    <td class="text-center">{{ $sknjop->luas_bumi }}</td>
                                                    <td class="text-right">
                                                        {{ number_format($sknjop->njop_bumi, '0', ',', '.') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Bangunan</td>
                                                    <td class="text-center">{{ $sknjop->luas_bng }}</td>
                                                    <td class="text-right">
                                                        {{ number_format($sknjop->njop_bng, '0', ',', '.') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td class="text-right">
                                                        {{ number_format($sknjop->njop_pbb, '0', ',', '.') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <?php
                                                        $cek = lunas_limas_tahun($sknjop->kd_kecamatan, $sknjop->kd_kelurahan, $sknjop->kd_blok, $sknjop->no_urut, $sknjop->kd_jns_op);
                                                        echo $cek == '1' ? 'Lunas, Bisa di cetak SK' : 'Belum, Tidak bisa cetak SK';
                                                        $nop_cetak .= "<input type='hidden' name='nop_cetak[]' value='3507" . $sknjop->kd_kecamatan . $sknjop->kd_kelurahan . $sknjop->kd_blok . $sknjop->no_urut . $sknjop->kd_jns_op . " '>";
                                                        if ($cek == 1) {
                                                            $nop_cetak .= "<input type='hidden' name='status_cetak[]' value='1'>";
                                                        } else {
                                                            $nop_cetak .= "<input type='hidden' name='status_cetak[]' value='0'>";
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                @else
                                    {{-- kolektif --}}
                                    <div class="col-12">
                                        <table class="table table-sm table-bordered">
                                            <thead>
                                                <tr style="vertical-align: middle">
                                                    <th style="text-align: center" rowspan="2">NOP</th>
                                                    <th style="text-align: center" colspan="2">Bumi</th>
                                                    <th style="text-align: center" colspan="2">Bangunan</th>
                                                    <th rowspan="2" style="text-align: center">NJOP PBB</th>
                                                    <th rowspan="2" style="text-align: center">
                                                        Lunas<br>( {{ date('Y') - 4 }} - {{ date('Y') - 1 }} )
                                                    </th>
                                                </tr>
                                                <tr style="vertical-align: middle">
                                                    <th class="text-ceter">Luas</th>
                                                    <th class="text-ceter">NJOP</th>
                                                    <th class="text-ceter">Luas</th>
                                                    <th class="text-ceter">NJOP</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($permohonan->nop()->get() as $item)
                                                    <tr>
                                                        <td>{{ formatnop($item->kd_propinsi . $item->kd_dati2 . $item->kd_kecamatan . $item->kd_kelurahan . $item->kd_blok . $item->no_urut . $item->kd_jns_op) }}
                                                            <br> <small>{{ $item->alamat_op }}</small>
                                                        </td>
                                                        <td class="text-right">{{ angka($item->luas_bumi) }}</td>
                                                        <td class="text-right">{{ angka($item->njop_bumi) }}</td>
                                                        <td class="text-right">{{ angka($item->luas_bng) }}</td>
                                                        <td class="text-right">{{ angka($item->njop_bng) }}</td>
                                                        <td class="text-right">{{ angka($item->njop_pbb) }}</td>
                                                        <td>

                                                            <?php
                                                            $cek = lunas_limas_tahun($item->kd_kecamatan, $item->kd_kelurahan, $item->kd_blok, $item->no_urut, $item->kd_jns_op);
                                                            echo $cek == '1' ? 'Lunas, Bisa di cetak SK' : 'Belum, Tidak bisa cetak SK';
                                                            $nop_cetak .= "<input type='hidden' name='nop_cetak[]' value='3507" . $item->kd_kecamatan . $item->kd_kelurahan . $item->kd_blok . $item->no_urut . $item->kd_jns_op . " '>";
                                                            if ($cek == 1) {
                                                                $nop_cetak .= "<input type='hidden' name='status_cetak[]' value='1'>";
                                                            } else {
                                                                $nop_cetak .= "<input type='hidden' name='status_cetak[]' value='0'>";
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>

                                        </table>

                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="card-footer p-0">
                            <form action="{{ route('sknjop-verifikasi.update', $permohonan->id) }}" method="post">
                                @csrf
                                @method('patch')
                                @php
                                    echo $nop_cetak;
                                @endphp
                                <div class="float-right">
                                    <button class="btn btn-sm btn-flat btn-success"> <i class="fas fa-file-signature"></i> Proses
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
