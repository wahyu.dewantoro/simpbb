@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>PROSES SK NJOP</h1>
                </div>                
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List Data</h3>
                            <div class="card-tools">
                                <form action="{{ url()->current() }}">
                                    <div class="input-group input-group-sm" style="width: 250px;">
                                        <input type="text" name="cari" class="form-control float-right" placeholder="Search"
                                            value="{{ request()->get('cari') }}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 table-responsive">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="20px">No</th>
                                        <th class="text-center">Nomor Layanan</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Pemohon</th>
                                        <th>Alamat</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sknjop as $index => $row)
                                        <tr>
                                            <td align="center">{{ $index + $sknjop->firstItem() }}</td>
                                            <td>
                                                {{ $row->nomor_layanan }}</td>
                                            <td>{{ tglIndo($row->tanggal_permohonan) }}
                                            </td>
                                            <td>{{ $row->nama_pemohon }}
                                            </td>
                                            <td>
                                                {{ $row->dati2_pemohon }} -
                                                {{ $row->propinsi_pemohon }}
                                            </td>
                                            <td class="text-center">
                                                <a title='Proses Permohonan SK NJOP' href="{{ route('sknjop-verifikasi.edit', $row->id) }}"
                                                    class="text-success"> <i class="fas fa-file-signature"></i> Proses</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer p-0">
                            <div class="float-sm-right">
                                {{ $sknjop->appends(['cari' => request()->get('cari')]) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
