@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pemutakhiran</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a href="{{ url('pemutakhiran/spop/create') }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-folder-plus"></i> Tambah
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">

        </div>
    </section>
@endsection
@section('script')
    <script>

    </script>
@endsection
