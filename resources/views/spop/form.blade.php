@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $data['title'] }} @if (!empty($data['penelitian']->jenis_pelayanan_nama))
                            [ {{ $data['penelitian']->jenis_pelayanan_nama }} ]
                        @endif
                    </h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form class="form-horizontal" action="{{ $data['action'] }}" method="POST" id="myform"
                enctype="multipart/form-data">
                @csrf
                @method($data['method'])
                <div class="row">
                    <div class="col-12">
                        @csrf
                        @method($data['method'])
                        <input type="hidden" value="{{ $data['penelitian']->jenis_pelayanan }}" id="jenis_pelayanan"
                            name="jenis_pelayanan">
                        <input type="hidden" value="{{ $data['jns_penelitian'] ?? '' }}" name="lhp_jns">
                        <input type="hidden" value="{{ $data['penelitian']->id ?? '' }}" id="lhp_id" name="lhp_id">
                        <div class="card card-outline card-info" id="data_objek">
                            <div class="card-header">
                                <h3 class="card-title">Data Objek</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-sm btn-danger btn-flat" data-toggle="modal"
                                        data-target="#modal-penolakan">
                                        <i class="fas fa-window-close"></i> Tolak Penelitian
                                    </button>
                                    @if ($data['urlpelimpahan'] != '')
                                        <button id="pelimpahan" class="btn btn-sm btn-success btn-flat"
                                            data-href="{{ $data['urlpelimpahan'] }}"><i class="far fa-share-square"></i>
                                            limpahkan ke penelitian
                                            {{ $data['jns_penelitian'] == '1' ? 'Khusus' : 'Kantor' }}</button>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- mutasi pecah --}}
                                        @if (!empty($data['nop_pecah']))
                                            <table class="table table-striped table-sm table-bordered">
                                                <thead class="bg-danger">
                                                    @if ($data['penelitian']->jenis_pelayanan == '6')
                                                        <tr style="vertical-align: middle">
                                                            <th style="text-align: center" rowspan="2">NOPEL</th>
                                                            <th style="text-align: center" rowspan="2">NOP</th>
                                                            <th style="text-align: center" rowspan="2">Nama Subjek</th>
                                                            <th style="text-align: center" rowspan="2">JPB</th>
                                                            <th style="text-align: center" rowspan="2">Lokasi</th>
                                                            <th style="text-align: center" colspan="2">Induk</th>
                                                            <th style="text-align: center" colspan="2">Pecah</th>
                                                        </tr>
                                                        <tr style="vertical-align: middle">
                                                            <th style="text-align: center">LT</th>
                                                            <th style="text-align: center">LB</th>
                                                            <th style="text-align: center">LT</th>
                                                            <th style="text-align: center">LB</th>
                                                        </tr>
                                                    @else
                                                        <tr style="vertical-align: middle">
                                                            <th style="text-align: center">NOPEL</th>
                                                            <th style="text-align: center">NOP</th>
                                                            <th style="text-align: center">Nama Subjek</th>
                                                            <th style="text-align: center">JPB</th>
                                                            <th style="text-align: center">Lokasi</th>
                                                            <th style="text-align: center">LT</th>
                                                            <th style="text-align: center">LB</th>
                                                        </tr>
                                                    @endif
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $lti = 0;
                                                        $lbi = 0;
                                                        $rowspan = $data['nop_pecah']->count();
                                                    @endphp
                                                    @foreach ($data['nop_pecah'] as $ipc => $pecah)
                                                        @if ($pecah->nop_gabung == '')
                                                            @php
                                                                $lti = $pecah->luas_induk;
                                                            @endphp
                                                        @endif
                                                        <tr>
                                                            <td>{{ $pecah->nomor_layanan }}</td>
                                                            <td>

                                                                {{ $pecah->kd_propinsi .
                                                                    '.' .
                                                                    $pecah->kd_dati2 .
                                                                    '.' .
                                                                    $pecah->kd_kecamatan .
                                                                    '.' .
                                                                    $pecah->kd_kelurahan .
                                                                    '.' .
                                                                    $pecah->kd_blok .
                                                                    '-' .
                                                                    $pecah->no_urut .
                                                                    '.' .
                                                                    $pecah->kd_jns_op }}
                                                                @if ($pecah->nop_gabung != '')
                                                                    <small class="text-info">Pecahan</small>
                                                                @endif
                                                            </td>
                                                            <td>{{ strtoupper($pecah->nama_wp) }}</td>
                                                            <td>
                                                                {{ $pecah->nama_kelompok }}
                                                            </td>
                                                            <td>
                                                                {{ $pecah->nama_lokasi }}
                                                            </td>
                                                            @if ($data['penelitian']->jenis_pelayanan == '6')
                                                                @if ($ipc == 0)
                                                                    <td class="text-right" rowspan="{{ $rowspan }}">
                                                                        {{ angka($lti) }}</td>
                                                                    <td class="text-right" rowspan="{{ $rowspan }}">
                                                                        {{ angka($lbi) }}</td>
                                                                @endif
                                                            @endif
                                                            <td class="text-right">{{ angka($pecah->luas_bumi) }}</td>
                                                            <td class="text-right">{{ angka($pecah->luas_bng) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>

                                            <hr>
                                        @endif

                                        {{-- mutasi gabung --}}
                                        @php
                                            $luas_bumi = $data['penelitian']->luas_bumi;
                                        @endphp
                                        @if ($data['penelitian']->jenis_pelayanan == 7)
                                            @php
                                                $luas_bumi = 0;
                                            @endphp
                                            <p><strong>Daftar NOP yang di gabung:</strong></p>
                                            <table class="table table-bordered table-sm">
                                                <thead class="bg-danger">
                                                    <tr>
                                                        <th>NOPEL</th>
                                                        <th>NOP</th>
                                                        <th>Subjek</th>
                                                        <th>LT</th>
                                                        <th>LB</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach ($data['nop_pembatalan'] as $batal)
                                                        @php
                                                            $luas_bumi += $batal['luas_bumi'];
                                                        @endphp
                                                        <input type="hidden" name="nop_pembatalan[]"
                                                            class="form-control form-control-sm"
                                                            value="{{ $batal['kd_propinsi'] . $batal['kd_dati2'] . $batal['kd_kecamatan'] . $batal['kd_kelurahan'] . $batal['kd_blok'] . $batal['no_urut'] . $batal['kd_jns_op'] }}">
                                                        <input type="hidden" name="layanan_objek_batal[]"
                                                            value="{{ $batal['id'] }}">

                                                        <tr>
                                                            <td>{{ $batal['nomor_layanan'] }}</td>
                                                            <td>
                                                                {{ formatnop(
                                                                    $batal['kd_propinsi'] .
                                                                        $batal['kd_dati2'] .
                                                                        $batal['kd_kecamatan'] .
                                                                        $batal['kd_kelurahan'] .
                                                                        $batal['kd_blok'] .
                                                                        $batal['no_urut'] .
                                                                        $batal['kd_jns_op'],
                                                                ) }}
                                                            </td>
                                                            <td>{{ $batal['nama_wp'] }}</td>
                                                            {{-- <td>{{ getNamaSubjek($batal['kd_propinsi']. $batal['kd_dati2'].
                                                    $batal['kd_kecamatan']. $batal['kd_kelurahan']. $batal['kd_blok'].
                                                    $batal['no_urut']. $batal['kd_jns_op']) }}</td> --}}
                                                            <td class="text-right">{{ angka($batal['luas_bumi']) }}</td>
                                                            <td class="text-right">{{ angka($batal['luas_bng']) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <hr>
                                        @endif
                                    </div>

                                    <div class="col-md-12">
                                        <strong>Berkas yang di lampirkan :</strong>

                                        @if ($data['penelitian']->is_online == '')
                                            <button type="button" class="btn btn-sm btn-flat btn-success"
                                                data-toggle="modal" data-target="#modal-dokumen">
                                                <i class="fas fa-file"></i> Tambah Dokumen
                                            </button>
                                        @endif
                                        <br>
                                        <div id="list_dokumen">
                                            @if (count($dokbphtb) > 0)
                                                <ol>
                                                    @foreach ($dokbphtb as $nm => $url)
                                                        <li><a href="#"
                                                                onclick="window.open('{{ $url }}', '_blank', 'width=800,height=600'); return false;">{{ $nm }}</a>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            @endif
                                            @if (isset($lampiran))
                                                <ol>
                                                    @foreach ($lampiran as $dok)
                                                        <li><a target="_blank"
                                                                href="{{ url('preview-dok') }}/{{ $dok->disk }}/{{ acak($dok->filename) }}">{{ $dok->nama_dokumen }}</a>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            @else
                                                @if (!empty($data['berkas']))
                                                    <ol>
                                                        @foreach ($data['berkas'] as $bks)
                                                            <li>
                                                                @if ($data['penelitian']->is_online == '')
                                                                    {{ $bks['nama_dokumen'] }} [{{ $bks['keterangan'] }}]


                                                                    <a role="button" tabindex="0"
                                                                        class="hapus-lampiran"
                                                                        data-id="{{ $bks['id'] }}"><i
                                                                            class="fas fa-trash-alt text-danger"></i></a>
                                                                @else
                                                                    <a target="_blank"
                                                                        href="{{ url($bks['url']) }}">{{ $bks['nama_dokumen'] }}
                                                                        <i class="fas fa-external-link-alt"></i></a>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                @else
                                                    <i>Tidak ada berkas terlampir</i>
                                                @endif
                                            @endif
                                        </div>
                                        <hr>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="nop_proses" class="col-md-4 col-form-label  ">NOP</label>
                                            <div class="col-md-8">
                                                <input type="text" name="nop_proses" id="nop_proses"
                                                    value="{{ $nop }}" class="form-control form-control-sm nop">
                                                <input type="hidden" name="nop_asal" id="nop_asal"
                                                    value="{{ $data['induk'] }}"
                                                    class="form-control form-control-sm nop">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="jns_transaksi" class="col-md-4 col-form-label  ">Jenis
                                                Transaksi</label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        @php
                                                            // dd($data['penelitian']->jts)
                                                        @endphp
                                                        <input type="text" name="jns_transaksi" id="jns_transaksi"
                                                            class="form-control form-control-sm"
                                                            value="{{ $data['penelitian']->jts }}" readonly>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <span id="jns_transaksi_keterangan"
                                                            class="form-control form-control-sm" readonly></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="display:none">
                                        <div class="form-group row">
                                            <label for="" class="col-md-4 col-form-label">Permohonan</label>
                                            <div class="col-md-8">
                                                <select name="jenis_layanan" id="jenis_layanan"
                                                    class="form-control form-control-sm">
                                                    <option value="">Pilih</option>
                                                    @foreach ($data['refAjuan'] as $rj)
                                                        <option @if ($data['penelitian']->jenis_pelayanan == $rj->jenis_layanan_id) selected @endif
                                                            value="{{ $rj->id_formulir . '_' . $rj->jenis_layanan_id }}">
                                                            {{ $rj->nama_layanan }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <table class="table table-bordered table-sm">
                                    <thead class="bg-info">
                                        <tr>
                                            <th class="text-center">Data objek pajak</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                @if (!in_array($data['penelitian']->jenis_pelayanan, [2, 5]))
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group row">
                                                                <label for="jalan_op"
                                                                    class="col-md-4 col-form-label  ">Nama
                                                                    Jalan</label>
                                                                <div class="col-md-8">
                                                                    @php
                                                                        $vop = $data['penelitian']->alamat_op ?? '';
                                                                        if ($vop != '') {
                                                                            $vop = str_replace('|', '-', $vop);

                                                                            $eop = explode('-', $vop);
                                                                            $aop = isset($eop[0]) ? trim($eop[0]) : '';
                                                                            $bop = isset($eop[1]) ? trim($eop[1]) : '';
                                                                        } else {
                                                                            $aop = isset($data['lo']->jalan_op)
                                                                                ? $data['lo']->jalan_op
                                                                                : '';
                                                                            $bop = isset($data['lo']->blok_kav_no_op)
                                                                                ? $data['lo']->blok_kav_no_op
                                                                                : '';
                                                                        }
                                                                    @endphp
                                                                    <input type="text" name="jalan_op" id="jalan_op"
                                                                        class="form-control form-control-sm"
                                                                        value="{{ $aop }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="blok_kav_no_op"
                                                                    class="col-md-4 col-form-label  ">Blok/
                                                                    Kav/
                                                                    No</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="blok_kav_no_op"
                                                                        id="blok_kav_no_op" value="{{ $bop }}"
                                                                        class="form-control form-control-sm">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nm_kelurahan"
                                                                    class="col-md-4 col-form-label ">Desa</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" id="nm_kelurahan"
                                                                        class="form-control form-control-sm" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nm_kecamatan"
                                                                    class="col-md-4 col-form-label ">Kecamatan</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" id="nm_kecamatan"
                                                                        class="form-control form-control-sm" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group row">
                                                                        <label for="rt_op"
                                                                            class="col-md-4 col-form-label  ">RT</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" maxlength="3"
                                                                                name="rt_op" id="rt_op"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $data['penelitian']->rt_op != '' ? $data['penelitian']->rt_op : (isset($data['lo']->rt_op) ? $data['lo']->rt_op : '') }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group row">
                                                                        <label for="rw_op"
                                                                            class="col-md-4 col-form-label  ">RW</label>
                                                                        <div class="col-md-8">
                                                                            @php
                                                                                $rto =
                                                                                    $data['penelitian']->rw_op != ''
                                                                                        ? $data['penelitian']->rw_op
                                                                                        : (isset($data['lo']->rw_op)
                                                                                            ? $data['lo']->rw_op
                                                                                            : '');
                                                                            @endphp
                                                                            <input type="text" name="rw_op"
                                                                                id="rw_op"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ substr($rto, -2) }}"
                                                                                maxlength="2">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    <div class="form-group row">
                                                                        <label for="no_persil"
                                                                            class="col-md-6 col-form-label  ">No
                                                                            Persil</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="no_persil"
                                                                                id="no_persil"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $data['penelitian']->no_persil ?? '' }}">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="luas_bumi"
                                                                    class="col-md-3 col-form-label  ">Luas </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" name="luas_bumi" id="luas_bumi"
                                                                        class="form-control form-control-sm agka"
                                                                        value="{{ $luas_bumi }}">
                                                                </div>
                                                            </div>
                                                            @if (($data['lo']->kd_znt_peta ?? '') != '')
                                                                <div class="position-relative p-1 bg-gray"
                                                                    style="height: 100px">
                                                                    <table class="table table-borderless table-sm">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>ZNT </td>
                                                                                <td>: {{ $data['lo']->kd_znt_peta }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>NIR </td>
                                                                                <td>: {{ angka($data['lo']->nir_peta) }}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>NJOP/M<sup>2</sup> </td>
                                                                                <td>:
                                                                                    {{ angka($data['lo']->njop_permeter) }}
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-5">
                                                            @php
                                                                $sznt = !empty($data['lo'])
                                                                    ? $data['lo']->kd_znt_peta
                                                                    : '';

                                                                $dnone_a = '';
                                                                if (
                                                                    in_array($data['penelitian']->jenis_pelayanan, [
                                                                        1,
                                                                        4,
                                                                        7,
                                                                    ]) ||
                                                                    $sznt == ''
                                                                ) {
                                                                    $dnone_a = 'd-none';
                                                                }

                                                            @endphp
                                                            <div class="form-group row">
                                                                <label for="kd_znt" class="col-md-3 col-form-label  ">
                                                                    ZNT
                                                                     {{-- {{ $data['penelitian']->jenis_pelayanan }} --}}
                                                                    </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" id="label_znt"
                                                                        class="form-control  {{ $dnone_a }}"
                                                                        readonly>

                                                                    {{-- @if (in_array($data['penelitian']->jenis_pelayanan, [1, 4, 7]) || $sznt == '') d-none @endif --}}


                                                                    {{-- @if (!in_array($data['penelitian']->jenis_pelayanan, [1, 4, 7]) || $sznt != '') d-none @endif  --}}
                                                                    <select name="kd_znt" id="kd_znt"
                                                                        @if ($sznt != '') readonly @endif
                                                                        class="form-control form-control-sm  @if( $dnone_a=='' )  d-none @endif   ">
                                                                        <option value="">Belum tersedia</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="kelas_tanah"
                                                                    class="col-md-3 col-form-label">Kelas</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" id="kelas_tanah"
                                                                        class="form-control form-control-sm" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="jns_bumi"
                                                                    class="col-md-3 col-form-label  ">Jenis
                                                                    Tanah</label>
                                                                <div class="col-md-6">
                                                                    @php
                                                                        $jbv =
                                                                            ($data['penelitian']->luas_bng ?? 0) > 0
                                                                                ? '1'
                                                                                : '3';
                                                                    @endphp
                                                                    <select name="jns_bumi" id="jns_bumi"
                                                                        class="form-control form-control-sm">
                                                                        <option value="">Pilih</option>
                                                                        @foreach (ArrayJenisBumi() as $i => $item)
                                                                            <option value="{{ $i }}"
                                                                                @if ($i == $jbv) selected @endif>
                                                                                {{ $i }} - {{ $item }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="form-group row" id="input_bng">
                                                                <label for="jml_bng"
                                                                    class="col-md-3 col-form-label  ">Jml Bangunan</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name="jml_bng" id="jml_bng"
                                                                        class="form-control form-control-sm angka">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                @else
                                                    <div id="data-objek-pajak"></div>
                                                @endif

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="form_bng"></div>



                                @if (!in_array($data['penelitian']->jenis_pelayanan, [2, 5]))
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-bordered table-sm">
                                                <thead class="bg-info">
                                                    <tr>
                                                        <th class="text-center">Data Subjek Pajak</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div id="disclaimer_subjek"></div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group row">
                                                                        <label for="subjek_pajak_id"
                                                                            class="col-form-label col-md-4  ">NIK</label>
                                                                        <div class="col-md-8">
                                                                            @php
                                                                                $nik_wp = $data['penelitian']->nik_wp;
                                                                            @endphp
                                                                            <input type="text" name="subjek_pajak_id"
                                                                                id="subjek_pajak_id"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ trim($nik_wp ?? '') }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="nm_wp"
                                                                            class="col-form-label col-md-4  ">Nama</label>
                                                                        <div class="col-md-8">
                                                                            @php
                                                                                $nama_wp = $data['penelitian']->nama_wp;
                                                                            @endphp
                                                                            <input type="text" name="nm_wp"
                                                                                id="nm_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $nama_wp ?? '' }}">
                                                                        </div>
                                                                    </div>

                                                                    @php
                                                                        $vos = $data['penelitian']->alamat_wp;
                                                                        if ($vos != '') {
                                                                            $vos = str_replace('.', '-', $vos);

                                                                            $vos = str_replace('|', '-', $vos);
                                                                            $eos = explode('-', $vos);
                                                                            $aos = isset($eos[0]) ? trim($eos[0]) : '';
                                                                            $aobs = isset($eos[1]) ? trim($eos[1]) : '';
                                                                        } else {
                                                                            $aos = '';
                                                                            $aobs = '';
                                                                        }
                                                                    @endphp

                                                                    <div class="form-group row">
                                                                        <label for="jalan_wp"
                                                                            class="col-form-label col-md-4  ">Jalan</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" name="jalan_wp"
                                                                                id="jalan_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $aos }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="blok_kav_no_wp"
                                                                            class="col-form-label col-md-4  ">Blok/
                                                                            Kav/
                                                                            No</label>
                                                                        <div class="col-md-4">
                                                                            <input type="text" name="blok_kav_no_wp"
                                                                                id="blok_kav_no_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $aobs }}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group row">
                                                                                <label for="rt_wp"
                                                                                    class="col-form-label col-md-4 ">RT</label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" name="rt_wp"
                                                                                        id="rt_wp"
                                                                                        class="form-control form-control-sm"
                                                                                        value="{{ $data['penelitian']->rt_wp ?? '' }}"
                                                                                        maxlength="3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group row">
                                                                                <label for="rw_wp"
                                                                                    class="col-form-label col-md-8 ">RW</label>
                                                                                <div class="col-md-4">
                                                                                    <input type="text" name="rw_wp"
                                                                                        id="rw_wp"
                                                                                        class="form-control form-control-sm"
                                                                                        value="{{ substr($data['penelitian']->rw_wp ?? '', -2) }}"
                                                                                        maxlength="2">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group row">
                                                                        <label for="kelurahan_wp"
                                                                            class="col-form-label col-md-4 ">Kelurahan</label>
                                                                        <div class="col-md-8">
                                                                            @php
                                                                                $voskelurahan =
                                                                                    $data['penelitian']->kelurahan_wp;
                                                                                if ($voskelurahan != '') {
                                                                                    $voskelurahan = str_replace(
                                                                                        '.',
                                                                                        '-',
                                                                                        $voskelurahan,
                                                                                    );

                                                                                    $voskelurahan = str_replace(
                                                                                        '|',
                                                                                        '-',
                                                                                        $voskelurahan,
                                                                                    );
                                                                                    $eoskelurahan = explode(
                                                                                        '-',
                                                                                        $voskelurahan,
                                                                                    );
                                                                                    $kel_wp = isset($eoskelurahan[0])
                                                                                        ? trim($eoskelurahan[0])
                                                                                        : '';
                                                                                    $kec_wp = isset($eoskelurahan[1])
                                                                                        ? trim($eoskelurahan[1])
                                                                                        : $data['penelitian']
                                                                                            ->kecamatan_wp;
                                                                                } else {
                                                                                    $kel_wp = '';
                                                                                    $kec_wp = '';
                                                                                }
                                                                            @endphp
                                                                            <input type="text" name="kelurahan_wp"
                                                                                id="kelurahan_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $kel_wp }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="kecamatan_wp"
                                                                            class="col-form-label col-md-4 ">Kecamatan</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" name="kecamatan_wp"
                                                                                id="kecamatan_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $kec_wp }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="kota_wp"
                                                                            class="col-form-label col-md-4 ">Dati
                                                                            II</label>
                                                                        <div class="col-md-8">
                                                                            @php
                                                                                $vosdati =
                                                                                    $data['penelitian']->dati2_wp;
                                                                                if ($vosdati != '') {
                                                                                    $vosdati = str_replace(
                                                                                        '.',
                                                                                        '-',
                                                                                        $vosdati,
                                                                                    );

                                                                                    $vosdati = str_replace(
                                                                                        '|',
                                                                                        '-',
                                                                                        $vosdati,
                                                                                    );
                                                                                    $eosdati = explode('-', $vosdati);
                                                                                    $dati2 = isset($eosdati[0])
                                                                                        ? trim($eosdati[0])
                                                                                        : '';
                                                                                    $propinsi = isset($eosdati[1])
                                                                                        ? trim($eosdati[1])
                                                                                        : ($data['penelitian']
                                                                                            ->propinsi_wp != ''
                                                                                            ? $data['penelitian']
                                                                                                ->propinsi_wp
                                                                                            : '');
                                                                                } else {
                                                                                    $dati2 = '';
                                                                                    $propinsi = '';
                                                                                }
                                                                            @endphp
                                                                            <input type="text" name="kota_wp"
                                                                                id="kota_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $dati2 }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="propinsi_wp"
                                                                            class="col-form-label col-md-4 ">Propinsi</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" name="propinsi_wp"
                                                                                id="propinsi_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $propinsi }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="kd_pos_wp"
                                                                            class="col-form-label col-md-6 ">Kode
                                                                            POS</label>
                                                                        <div class="col-md-6">
                                                                            <input type="text" name="kd_pos_wp"
                                                                                id="kd_pos_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $data['penelitian']->kode_pos_wp ?? '' }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group row">
                                                                        <label for="status_pekerjaan_wp"
                                                                            class="col-form-label col-md-4 ">Pekerjaan</label>
                                                                        <div class="col-md-8">
                                                                            <select name="status_pekerjaan_wp"
                                                                                id="status_pekerjaan_wp"
                                                                                class="form-control form-control-sm"
                                                                                required>
                                                                                <option value="">Pilih</option>
                                                                                @php
                                                                                    $spk = 5;
                                                                                @endphp
                                                                                @foreach (ArrayJenisPekerjaan() as $i => $item)
                                                                                    <option
                                                                                        @if ($spk == $i) selected @endif
                                                                                        value="{{ $i }}">
                                                                                        {{ $i }} -
                                                                                        {{ $item }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="kd_status_wp"
                                                                            class="col-form-label col-md-4  ">Status</label>
                                                                        <div class="col-md-8 ">
                                                                            @php
                                                                                $sw =
                                                                                    $data['penelitian']
                                                                                        ->status_subjek_pajak ?? '1';
                                                                            @endphp
                                                                            <select name="kd_status_wp" id="kd_status_wp"
                                                                                class="form-control form-control-sm"
                                                                                required>
                                                                                <option value="">Pilih</option>
                                                                                @foreach (ArrayStatusWP() as $i => $item)
                                                                                    <option
                                                                                        @if ($sw == $i) selected @endif
                                                                                        value="{{ $i }}">
                                                                                        {{ $i }} -
                                                                                        {{ $item }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="npwp"
                                                                            class="col-form-label col-md-4 ">NPWP</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" name="npwp"
                                                                                id="npwp"
                                                                                class="form-control form-control-sm"
                                                                                value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="telp_wp"
                                                                            class="col-form-label col-md-4 ">Telepon</label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" name="telp_wp"
                                                                                id="telp_wp"
                                                                                class="form-control form-control-sm"
                                                                                value="{{ $data['penelitian']->telp_wp }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table class="table table-bordered table-sm" id="div_terbit_sppt">
                                                <thead class="bg-info">
                                                    <tr>
                                                        <th class="text-center">Penerbitan SPPT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                                @php
                                                                    $crp = 0;
                                                                    if (!empty($riwayatpembayaran)) {
                                                                        $crp = $riwayatpembayaran->count();
                                                                    }
                                                                    // dd($crp);
                                                                    $arr_penetapan = '';
                                                                    if ($data['penelitian']->jenis_pelayanan == 1) {
                                                                        $tahun_penetapan = date('Y') - 5;
                                                                    } elseif (
                                                                        $data['penelitian']->jenis_pelayanan == 7
                                                                    ) {
                                                                        if (!empty($riwayatpembayaran)) {
                                                                            $tahun_penetapan = date('Y');
                                                                            $arr_penetapan .= date('Y') . ',';
                                                                        }
                                                                    } else {
                                                                        $tahun_penetapan = '';
                                                                    }
                                                                @endphp

                                                                @if ($crp > 0)
                                                                    <div class="col-md-4">
                                                                        <b>Riwayat Pembayaran </b>
                                                                        <table class="table table-sm table-striped">
                                                                            <thead class="bg-info">
                                                                                <tr>
                                                                                    {{-- <th>NOP</th> --}}
                                                                                    <th>TAHUN</th>
                                                                                    <th>TANGGAL</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach ($riwayatpembayaran as $idb => $bayar)
                                                                                    <tr>

                                                                                        <td>{{ $bayar->thn_pajak_sppt }}
                                                                                        </td>
                                                                                        <td>{{ tglIndo($bayar->tgl_pembayaran_sppt) }}
                                                                                            <span
                                                                                                class="text-danger">{{ $bayar->koreksi }}</span>
                                                                                        </td>
                                                                                    </tr>

                                                                                    @if ($bayar->tgl_pembayaran_sppt == '' && $bayar->koreksi == '')
                                                                                        @php
                                                                                            $arr_penetapan .=
                                                                                                $bayar->thn_pajak_sppt .
                                                                                                ',';

                                                                                            /* if($data['penelitian']->jenis_pelayanan==6 || $data['penelitian']->jenis_pelayanan==7){
                                                                    // $arr_penetapan=date('Y').',';

                                                                    }
                                                                    */
                                                                                            /* if($data['penelitian']->jenis_pelayanan==6 && $data['penelitian']->nop_gabung==''){
                                                                    // $arr_penetapan=date('Y').',';
                                                                    }else{
                                                                    // $arr_penetapan=date('Y').',';
                                                                    } */

                                                                                        @endphp
                                                                                    @endif
                                                                                @endforeach

                                                                                @php
                                                                                    $arr_penetapan = substr(
                                                                                        $arr_penetapan,
                                                                                        0,
                                                                                        -1,
                                                                                    );
                                                                                    // dd($arr_penetapan);
                                                                                    if ($arr_penetapan != '') {
                                                                                        $esd = explode(
                                                                                            ',',
                                                                                            $arr_penetapan,
                                                                                        );
                                                                                        $arr_penetapan = implode(
                                                                                            ',',
                                                                                            array_unique($esd),
                                                                                        );
                                                                                    }

                                                                                @endphp
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                @endif
                                                                <div class="col-md-8">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label for=""
                                                                                    class="form-col-form-label col-md-4">Terbit
                                                                                    SPPT </label>
                                                                                <div class="col-md-4">
                                                                                    @if (Auth()->user()->hasRole('Administrator'))
                                                                                        <button
                                                                                            class="btn btn-flat btn-sm btn-success"
                                                                                            id="tambah_sppt"> <i
                                                                                                class="fas fa-plus"></i>
                                                                                            Tambah</button>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="daftar_penetapan"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @php
                                        // dd($arr_penetapan=substr($arr_penetapan,0,-1));
                                    @endphp
                                @else
                                    <div class="row">
                                        <div class="col-md-6 offset-3">
                                            {{-- jns_pengurangan --}}

                                            @if ($data['penelitian']->jenis_pelayanan == '2')
                                                <div class="form-group">
                                                    <label for="jns_koreksi_blokir"
                                                        class="col-md-4 col-form-label">Jenis</label>
                                                    <div class="col-md-8">
                                                        <select name="jns_koreksi" id="jns_koreksi_blokir"
                                                            class="form-control form-control-sm" required>
                                                            <option value="">Pilih</option>
                                                            @foreach (KategoriIventarisasi() as $i => $item)
                                                                @if ($i != '07')
                                                                    <option value="{{ $i }}">
                                                                        {{ $i }} -
                                                                        {{ $item }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4 col-form-label">Alasan /
                                                        Keterangan</label>
                                                    <div class="col-md-8">
                                                        <textarea placeholder="Masukan alasan / keterangan pembatalan objek" name="alasan" id="alasan" rows="5"
                                                            class="form-control">{{ trim($data['penelitian']->alasan ?? '') }}</textarea>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <label for="">Jenis Pengurangan </label>
                                                    <select name="jns_pengurangan" id="jns_pengurangan"
                                                        class="form-control form-control-sm">
                                                        <option value="">Pilih</option>
                                                        @php
                                                            $sl = $data['penelitian']->jns_pengurangan_id ?? '';

                                                        @endphp
                                                        @foreach ($data['penelitian']->jns_pengurangan as $item)
                                                            <option persen="{{ $item->pengurangan }}"
                                                                @if ($item->id == $sl) selected @endif
                                                                value="{{ $item->id }}">{{ $item->nama_pengurangan }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nilai Pengurangan</label>
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <select class="form-control form-control-sm"
                                                                name="tipe_pengurangan" id="tipe_pengurangan">
                                                                <option value="2">Persentase(%)</option>
                                                                <option value="1">Angka</option>

                                                            </select>
                                                        </div>
                                                        <input type="text" class="form-control form-control-sm angka"
                                                            name="nilai_pengurangan" id="nilai_pengurangan"
                                                            placeholder="Nilai">
                                                    </div>
                                                </div>
                                                {{-- <div class="form-group">
                                                    <label for="">Include Insentif ?</label>
                                                    <select name="insentif" id="insentif"
                                                        class="form-control form-control-sm">
                                                        <option value="0">Tidak</option>
                                                        <option value="1">Ya</option>
                                                    </select>
                                                </div> --}}
                                                <div class="form-group">
                                                    <label for="">Keterangan</label>
                                                    {{-- <select name="jns_pengurangan" id="jns_pengurangan" class="form-control form-control-sm"> --}}
                                                    <textarea class="form-control" name="ket_pengurangan" id="ket_pengurangan" placeholder="Keterangan"></textarea>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-6 offset-3">
                                        <div class="row">
                                            <div class="col-6 offset-3">
                                                <button class="btn btn-flat btn-sm btn-success" type="submit"
                                                    id="simpanpenelitian"><i class="far fa-save"></i>
                                                    Save</button>
                                                <a href="{{ url($data['urlback']) }}" id="btn-batal"
                                                    class="btn btn-sm btn-flat btn-danger"><i
                                                        class="far fa-window-close"></i> Batal</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal fade" id="modal-dokumen">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Dokumen Terlampir</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Jenis</label>
                                <input type="hidden" id="nomor_layanan_dokumen"
                                    value="{{ $data['penelitian']->nomor_layanan }}">
                                <input type="hidden" id="id_layanan_dokumen" value="{{ $data['penelitian']->id }}">
                                <select name="nama_dokumen" id="nama_dokumen" class="form-control form-control-sm"
                                    required>
                                    <option value="">Pilih</option>
                                    @foreach ($jenisDokumen as $jd)
                                        <option value="{{ $jd }}">{{ $jd }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">No Dokumen</label>
                                <input type="text" class="form-control form-control-sm" name='keterangan_dokumen'
                                    id="keterangan_dokumen">
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-sm btn-flat btn-default"
                                data-dismiss="modal">Close</button>
                            <button type="button" id="btn_save_lampiran" class="btn btn-sm btn-flat btn-success"><i
                                    class="far fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal-penolakan">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Penolakan Penelitian</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Alasan / Keterangan</label>
                                <textarea required name="keterangan_tolak" id="keterangan_tolak" class="form-control"
                                    placeholder="Tulis keteranan alasan penolakan penelitian" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-sm btn-flat btn-default"
                                data-dismiss="modal">Close</button>
                            <button type="button" id="btn_tolak" class="btn btn-sm btn-flat btn-success"><i
                                    class="far fa-save"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span id="session_id" data-user={{ user()->id }}></span>
    </section>
@endsection
@section('css')
    <style type="text/css">
        #data_bangunan {
            display: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/jquery-validation/additional-methods.min.js"></script>
    <script src="{{ asset('js') }}/wilayah.js"></script>

    <script>
        $(document).ready(function() {

            var headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }


            $(document).on("keypress", "input,select", function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    // Get all focusable elements on the page
                    var $canfocus = $(":focusable");
                    var index = $canfocus.index(document.activeElement) + 1;
                    if (index >= $canfocus.length) index = 0;
                    $canfocus.eq(index).focus();
                }
            });


            // 
            $('#btn_tolak').on('click', function(e) {
                $('#modal-penolakan').modal('toggle');
                e.preventDefault()
                // console.log('klik')
                var keterangan_tolak = $('#keterangan_tolak').val();
                openloading()
                $.ajax({
                    url: "{{ route('penelitian.kantor.tolak', $data['penelitian']->id) }}",
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        keterangan_tolak: keterangan_tolak
                    },
                    success: function(res) {
                        // console.log(res)
                        window.location.href = "{{ url($data['urlback']) }}";
                    },
                    error: function(er) {
                        // console.log(er)

                        Swal.fire({
                            icon: "error",
                            title: "Peringatan",
                            text: "Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.",
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        });
                    }
                })
            })

            $('#btn_save_lampiran').on('click', function(e) {
                $('#modal-dokumen').modal('toggle');
                e.preventDefault()
                // console.log('klik')
                var nomor_layanan_dokumen = $('#nomor_layanan_dokumen').val()
                var id_layanan_dokumen = $('#id_layanan_dokumen').val()
                var nama_dokumen = $('#nama_dokumen').val()
                var keterangan_dokumen = $('#keterangan_dokumen').val()
                openloading()
                $.ajax({
                    url: "{{ route('layanan.simpan-lampiran.post') }}",
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        nomor_layanan: nomor_layanan_dokumen,
                        layanan_objek_id: id_layanan_dokumen,
                        nama_dokumen: nama_dokumen,
                        keterangan: keterangan_dokumen
                    },
                    success: function(res) {
                        // console.log(res)
                        closeloading()
                        $('#nama_dokumen').val('')
                        $('#keterangan_dokumen').val('')
                        html = '<ol>';
                        $.each(res.data, function(k, v) {
                            html += '<li>' + v.nama_dokumen + ' [' + v.keterangan +
                                '] <a role="button" tabindex="0"   class="hapus-lampiran" data-id="' +
                                v.id +
                                '" ><i class="fas fa-trash-alt text-danger"></i></a></li>';
                        })
                        html += '</ol>';
                        $('#list_dokumen').html(html)
                    },
                    error: function(er) {
                        closeloading()
                        $('#nama_dokumen').val('')
                        $('#keterangan_dokumen').val('')
                    }
                })
            })

            $(document).on('click', '.hapus-lampiran', function(e) {
                id = $(this).data('id')
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin menghapus dokumen?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        openloading()
                        $.ajax({
                            url: "{{ route('layanan.hapus-lampiran.get') }}",
                            data: {
                                id: id
                            },
                            success: function(res) {
                                closeloading()
                                html = '<ol>';
                                $.each(res.data, function(k, v) {
                                    html += '<li>' + v.nama_dokumen + ' [' + v
                                        .keterangan +
                                        '] <a role="button" tabindex="0" class="hapus-lampiran" data-id="' +
                                        v.id +
                                        '" ><i class="fas fa-trash-alt text-danger"></i></a></li>';
                                })
                                html += '</ol>';
                                $('#list_dokumen').html(html)
                            },
                            error: function(er) {
                                closeloading()
                            }
                        })
                    }
                })

            });

            var base_url = "{{ url('/') }}";
            $('input').each(function() {
                $(this).val($(this).val().toUpperCase())
            })

            function pad(str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }
            var arrayZnt;
            // JenisBumi(arrayJnsBumi)
            trSpop(arrayTransaksi)
            // statusWepe(arrayStatusWp)

            function trSpop(arrayTransaksi) {
                isi = $("#jns_transaksi").val();
                if (typeof arrayTransaksi[isi] === "undefined") {
                    hasil = "";
                    $("#jns_transaksi").val("");
                } else {
                    hasil = arrayTransaksi[isi];
                }
                $("#jns_transaksi_keterangan").html(hasil);
            }


            // function get last number nop
            function getLastNop(kd_kecamatan, kd_kelurahan, kd_blok) {
                rv = '';
                Swal.fire({
                    title: '<i class="fas fa-sync fa-spin"></i>',
                    text: 'Sistem sedang berjalan, mohon ditunggu!',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showConfirmButton: false
                })

                $.ajax({
                    async: false,
                    url: base_url + "/api/nop-last-number",
                    data: {
                        kd_kecamatan: kd_kecamatan,
                        kd_kelurahan: kd_kelurahan,
                        kd_blok: kd_blok,
                        user_id: $('#session_id').data('user')
                    },
                    success: function(res) {
                        rv = res;
                        closeloading()
                    },
                    error: function(er) {
                        closeloading()
                    }
                });
                return rv
            }

            // nop_proses format nop
            // $("#nop_proses").on("keyup", function(e) {

            $(document).on('keyup', '#nop_proses', function(e) {
                var _method = $("input[name=_method]").val();
                var nop = $('#nop_proses').val();
                var convert = formatnop(nop);
                var value = convert;
                var nop_asal = $('#nop_asal').val().replace(/[^\d]/g, "");
                var nop_proses = $('#nop_proses').val().replace(/[^\d]/g, "");

                $('#nop_proses').val(convert);
                var b = value.replace(/[^\d]/g, "");
                var kec = b.substr(4, 3);
                var kel = b.substr(7, 3);
                var blok = b.substr(10, 3);
                var jtr = "{{ $data['penelitian']->jenis_pelayanan }}"

                $('#nm_kelurahan').val(arrayKelurahan[kec + kel])
                $('#nm_kecamatan').val(arrayKecamatan[kec])

                if (value.length == 17 && e.keyCode != 8) {
                    getZnt(kec, kel, blok);
                    // jika pendaftaran baru - 1
                    if ((jtr == '1' || jtr == '7' || jtr == '6' || jtr == '10') && e.keyCode != 8) {
                        lastnop = getLastNop(kec, kel, blok)
                        gabung = formatnop(lastnop);
                        $(this).val(gabung);
                    }
                }

                var ganti_nop = "{{ $ganti_nop ?? '0' }}"
                if ((_method == 'POST' && jtr == '10' && ganti_nop == '1')) {
                    // alert('asdasd/')
                    lastnop = getLastNop(kec, kel, blok)
                    gabung = formatnop(lastnop);
                    $(this).val(gabung);
                }
            });

            // cek nop penuh
            $("#nop_proses,#jns_transaksi ").on("change", function(e) {
                var value = $('#nop_proses').val()
                var jtr = "{{ $data['penelitian']->jenis_pelayanan }}"
                if (jtr == "1") {
                    // cek nop existing
                    hasilcek = existingNop(value);
                    if (hasilcek == 'false' || hasilcek === false) {
                        $('#nop_proses').focus()
                        Swal.fire({
                            icon: "error",
                            title: "Peringatan",
                            text: "NOP " + $("#nop_proses").val() + " sudah terpakai",
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        });
                    }
                } else {
                    hasilcek = existingNop(value);
                    if (hasilcek == 'true' || hasilcek === true) {
                        $('#nop_proses').focus()
                        Swal.fire({
                            icon: "error",
                            title: "Peringatan",
                            text: "NOP " + $("#nop_proses").val() + " sudah belum terdaftar",
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        });
                    }
                }
            });


            // detect transaction
            $("#jns_transaksi").on("keyup", function(e) {
                trSpop(arrayTransaksi);
                nop = $('#nop_proses').val();
                convert = formatnop(nop)

                panjang = convert.length
                if (panjang >= 17) {
                    var b = convert.replace(/[^\d]/g, "");
                    var kec = b.substr(4, 3);
                    var kel = b.substr(7, 3);
                    var blok = b.substr(10, 3);
                    // jika pendaftaran baru - 1
                    if ($('#jns_transaksi').val() == '1' && panjang != 24) {

                        lastnop = formatnop(getLastNop(kec, kel, blok))
                        // console.log('nop '+lastnop)
                        $('#nop_proses').val(lastnop);
                    }
                }
            });



            // $("#input_bng").hide();
            $(document).on('change', '#jns_bumi', function(e) {
                id = $('#jns_bumi').val()
                if (id == 1) {
                    getFormBangunan(1);
                    var jbng = "{{ $data['penelitian']->bangunan ?? 1 }}"
                    $("#input_bng").show();
                    $("#jml_bng")
                        .val(jbng)
                        .show();
                } else {
                    getFormBangunan(0);
                    $("#input_bng").hide();
                    $("#jml_bng").val("").hide();
                }


                // tambah_sppt
                if (id == 4 || id == 5) {
                    $('#tambah_sppt,#div_terbit_sppt').hide()

                    $('#daftar_penetapan').html('')
                } else {
                    $('#tambah_sppt,#div_terbit_sppt').show()
                    loopTahunPenetapan()
                }

            })


            $('#jns_bumi').trigger('change')


            $.validator.addMethod(
                "angkaRegex",
                function(value, element) {
                    return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
                }, "Harus di isi dengan angka."
            );

            jQuery.validator.addMethod("exactlength", function(value, element, param) {
                return this.optional(element) || value.length == param;
            }, $.validator.format("Please enter exactly {0} characters."));



            $('#myform').validate({
                errorElement: "span",
                errorPlacement: function(error, element) {
                    error.addClass("invalid-feedback");
                    element.closest(".form-group").append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                },
                rules: {
                    jenis_layanan: {
                        required: true
                    },
                    jns_bumi: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 1
                    },
                    jns_transaksi: {
                        required: true
                    },
                    nop_proses: {
                        required: true,
                        exactlength: 24
                    },
                    nop_asal: {
                        required: false
                    },
                    jalan_op: {
                        required: true,
                        maxlength: 30
                    },
                    rt_op: {
                        required: false,
                        digits: true,
                        angkaRegex: true,
                        digits: true,
                        maxlength: 3,
                        minlength: 1
                    },
                    rw_op: {
                        required: false,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 2,
                        minlength: 1
                    },
                    luas_bumi: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        digits: true,
                        maxlength: 12,
                        minlength: 1
                    },
                    kd_znt: {
                        required: true
                    },
                    kd_status_wp: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 1
                    },
                    subjek_pajak_id: {
                        required: true,
                        digits: true,
                        angkaRegex: true,
                        minlength: 16,
                        maxlength: 16
                    },
                    nm_wp: {
                        required: true,
                        maxlength: 30
                    },
                    jalan_wp: {
                        required: true,
                        maxlength: 30
                    },
                    blok_kav_no_wp: {
                        required: false,
                        maxlength: 15
                    },
                    rt_wp: {
                        digits: true,
                        angkaRegex: false,
                        required: true,
                        maxlength: 3
                    },
                    rw_wp: {
                        angkaRegex: true,
                        required: true,
                        maxlength: 2,
                        digits: true
                    },
                    kd_pos_wp: {
                        required: false,
                        digits: true,
                        angkaRegex: true,
                        maxlength: 5
                    },
                    kelurahan_wp: {
                        required: true
                    },
                    kecamatan_wp: {
                        required: true
                    },
                    kota_wp: {
                        required: true
                    },
                    propinsi_wp: {
                        required: true
                    },
                    telp_wp: {
                        digits: true,
                        angkaRegex: true,
                        required: false,
                        minlength: 10
                    },
                    npwp: {
                        digits: true,
                        angkaRegex: true,
                        required: false
                    },
                    status_pekerjaan_wp: {
                        required: true,
                        digits: true,
                        maxlength: 1
                    }
                },
                messages: {
                    jns_bumi: {
                        required: 'Jenis tanah harus di isi.',
                        digits: 'Harus di isi dengan angka.',
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    jns_transaksi: {
                        required: 'Jenis transaksi harus di isi.'
                    },
                    nop_proses: {
                        required: "NOP harus di isi",
                        exactlength: "Belum terisi penuh"
                        /* minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter" */
                    },
                    jalan_op: {
                        required: 'Alamat harus di isi.',
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    rt_op: {
                        required: 'RT harus isi',
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    rw_op: {
                        required: "RW harus isi",
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    luas_bumi: {
                        required: "Harus di isi",
                        digits: "Di isi dengan angka",
                        minlength: "minimal {0} karakter",
                    },

                    kd_status_wp: {
                        required: "Harus di isi",
                        digits: "Di isi dengan angka",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    subjek_pajak_id: {
                        required: "Harus di isi",
                        digits: "Di isi dengan angka",
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    nm_wp: {
                        required: 'Harus di isi'
                    },
                    jalan_wp: {
                        required: 'Harus di isi'
                    },

                    rt_wp: {
                        required: 'RT harus isi',
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    rw_wp: {
                        required: "RW harus isi",
                        digits: 'Di isi angka',
                        minlength: "minimal {0} karakter",
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    kd_pos_wp: {
                        digits: 'Di isi dengan angka',
                        maxlength: "Tidak boleh lebih dari {0} karakter"
                    },
                    kelurahan_wp: {
                        required: "Harus di isi"
                    },
                    kecamatan_wp: {
                        required: "Harus di isi"
                    },
                    kota_wp: {
                        required: "Harus di isi"
                    },
                    propinsi_wp: {
                        required: "Harus di isi"
                    },
                    telp_wp: {
                        digits: "Di isi dengana angka",
                        minlength: "Minimal {0} karakter"
                    },
                    npwp: {
                        digits: "Di isi dengan angka",
                        required: false
                    },

                },

            })


            // $("#submit").click(function() {
            $("#myform").submit(function() {

                var method_ = "{{ $data['method'] }}"
                if (method_ == 'post') {

                    isi = "{{ $data['penelitian']->jenis_pelayanan }}"
                    if (isi == "1" || isi == "7" || isi == "6") {
                        let nop_proses = $('#nop_proses').val().replace(/[^\d]/g, "");
                        let nop_asal = $('#nop_asal').val().replace(/[^\d]/g, "");

                        if (nop_proses != nop_asal) {
                            // console.log('di cek')
                            hasilcek = existingNop($("#nop_proses").val());
                            if (hasilcek == 'false' || hasilcek === false) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Peringatan",
                                    text: "NOP " + $("#nop_proses").val() +
                                        " sudah terpakai ! silahkan hapus NOP ",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                }).then((result) => {
                                    return false
                                })
                                return false
                            }
                        }
                    } else {

                        let hasilcek = existingNop($("#nop_proses").val());
                        if (hasilcek == 'true' || hasilcek === true) {
                            // return false;
                            Swal.fire({
                                    icon: "error",
                                    title: "Peringatan",
                                    text: "NOP " + $("#nop_proses").val() +
                                        " sudah terpakai ! silahkan hapus NOP ",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                })
                                .then((result) => {
                                    return false
                                })
                            return false;
                        }
                    }
                }

            });

            $(".nop").on("keyup", function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });




            function existingNop(nop) {
                let ceknop = false;
                // console.log(nop)

                if (nop.length == 24) {
                    Swal.fire({
                        // icon: 'error',
                        title: '<i class="fas fa-sync fa-spin"></i>',
                        text: 'Sedang checking NOP',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false
                    })

                    var b = nop.replace(/[^\d]/g, "");
                    var kec = b.substr(4, 3);
                    var kel = b.substr(7, 3);
                    var blok = b.substr(10, 3);
                    var no_urut = b.substr(13, 4);
                    var jns_op = b.substr(17, 1);
                    $.ajax({
                        async: false,
                        url: base_url + "/api/cek-unique-nop",
                        data: {
                            kd_kecamatan: kec,
                            kd_kelurahan: kel,
                            kd_blok: blok,
                            no_urut: no_urut,
                            kd_jns_op: jns_op
                        },
                        success: function(res) {
                            // return  res;
                            // console.log('cek unique nop : ' + res);
                            ceknop = res
                            swal.close();
                        },
                        error: function(er) {
                            // console.log(er)
                            ceknop = false;
                            swal.close();
                        }
                    });
                    // return ceknop;
                }

                return ceknop;
                // return response;
                // return response;
            }




            function getZnt(kec, kel, blok) {
                arrayKelasTanah = {}
                $('#kelas_tanah').val('')
                openloading()

                let jpp = "{{ $data['penelitian']->jenis_pelayanan }}"
                if (jpp != '5') {
                    $.ajax({
                        url: base_url + "/api/znt",
                        async: false,
                        data: {
                            kecamatan: kec,
                            kelurahan: kel,
                            blok: blok
                        },
                        success: function(res) {
                            var count = Object.keys(res).length;

                            html = '<option value="">Pilih</option>';
                            $.each(res, function(k, v) {
                                arrayKelasTanah[v['kd_znt']] = v['kelas_tanah']
                                var apd = '<option value="' + v['kd_znt'] + '"> ' + v[
                                        'kd_znt'] +
                                    " [" + formatRupiah(v['nir']) + "/M]</option>";
                                html += apd;
                            })
                            $("#kd_znt").html(html)
                            if (count == 0) {
                                Swal.fire({
                                    icon: "error",
                                    title: "Peringatan",
                                    text: "ZNT pada blok :" + blok +
                                        " belum tersedia di sistem",
                                    allowOutsideClick: false,
                                    allowEscapeKey: false
                                });
                            }
                            closeloading()
                        },
                        error: function(er) {
                            // console.log(er)
                            $("#kd_znt").html('<option value="">Belum tersedia</option>');
                            closeloading()
                        }
                    });

                    var sl = "{{ !empty($data['lo']) ? $data['lo']->kd_znt_peta : '' }}"
                    // var sl="";
                    $('#kd_znt').val(sl).trigger('change')
                    $('#label_znt').val(sl)

                    $('#kelas_tanah').val(arrayKelasTanah[sl])
                }
            }

            $('#kd_znt').on('change', function() {
                znt = $('#kd_znt').val()
                if (znt != '') {
                    $('#kelas_tanah').val(arrayKelasTanah[znt])
                } else {
                    $('#kelas_tanah').val('')
                }

                $('.tahun').trigger('keyup');

            })

            function getFormBangunan(jumlah_bng) {
                $("#form_bng").html("");
                lb = "{{ $data['penelitian']->luas_bng }}";
                jpb =
                    "{{ $data['penelitian']->kelompok_objek_id == '' ? '1' : $data['penelitian']->kelompok_objek_id }}";
                if (jumlah_bng != "" && jumlah_bng > 0) {
                    Swal.fire({
                        // icon: 'error',
                        title: '<i class="fas fa-sync fa-spin"></i>',
                        text: "Sedang memuat form bangunan",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false
                    })
                    $.ajax({
                        url: base_url + "/api/form-lspop",
                        data: {
                            index: jumlah_bng,
                            luas_bng: lb,
                            jpb: jpb,
                            jenis_ajuan: "{{ $data['penelitian']->jenis_objek }}",
                            lhp_id: $('#lhp_id').val()
                        },
                        dataType: "html",
                        success: function(data) {
                            closeloading()
                            $("#form_bng").html(data);
                        },
                        error: function(e) {
                            closeloading()
                        }
                    });
                }
            }

            // tambahan untuk bangunan
            $("#jml_bng").on("keyup", function() {
                jumlah_bng = $("#jml_bng").val();
                getFormBangunan(jumlah_bng);
            });

            // register jQuery extension
            jQuery.extend(jQuery.expr[":"], {
                focusable: function(el, index, selector) {
                    return $(el).is(":input");
                }
            });

            $(".form-control").keyup(function() {
                if (this.value.length == this.maxLength) {
                    $(this).next('.form-control').focus();
                }
            });



            $("input[type=text]").keyup(function(e) {
                $(this).val($(this).val().toUpperCase());
                if ($(this).val().length == this.maxLength) {
                    e.preventDefault();
                    // Get all focusable elements on the page
                    var $canfocus = $(":focusable");
                    var index = $canfocus.index(document.activeElement) + 1;
                    if (index >= $canfocus.length) index = 0;
                    $canfocus.eq(index).focus();
                }
            });

            $('#pelimpahan').on('click', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "melimpahkan berkas ini ke penelitian {{ $data['jns_penelitian'] == '1' ? 'Khusus' : 'Kantor' }}",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Yakin!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.value) {
                        document.location.href = $(this).data('href');
                    }
                })

            });
            $("#daftar_penetapan").html('')
            // konfigurasi tahun penetapan

            function formatDate(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                month = ["January", "February", "March", "April", "May", "June", "July", "August", "September",
                    "October", "November", "December"
                ]

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;


                return [d.getDate(), month[d.getMonth()], year].join(' ');
            }




            // loop tahun penetapan
            function loopTahunPenetapan() {
                var looptahun = "{{ $arr_penetapan ?? '' }}"

                // console.log(looptahun)


                $("#daftar_penetapan").html('')

                if (looptahun != '') {
                    const thn_split = looptahun.split(',')

                    const show_trash = "{{ $is_admin }}"
                    // console.log('show trsh ' + show_trash)

                    counttahun = $('#daftar_penetapan .row_sppt').length;
                    for (var m = 0; m < thn_split.length; m++) {
                        var tahun_pajak = thn_split[m]

                        jpel = 1;
                        end = parseInt(new Date().getFullYear())
                        var tj = tahun_pajak
                        /* if (jpel == 1) {
                            tj = end
                        } */
                        jt = formatDate(tj + '-09-30');
                        counttahun = counttahun + 1;
                        // console.log(counttahun)
                        jenis_pelayanan = $('#jenis_pelayanan').val();

                        showpot = ' style="display: none;" '
                        if (jenis_pelayanan == '5') {
                            showpot = ' '
                        }

                        if (show_trash == 1) {
                            hps = '<a role="button" tabindex="0" class="text-danger remove flat"  data-id="' +
                                tahun_pajak + '" data-tahun="' + tahun_pajak +
                                '""  title="Hapus">\
                                                                                                                                                        <i class="fa fa-trash"></i>\
                                                                                                                                                    </a>';
                            var read = ''
                        } else {
                            hps = ''
                            var read = 'readonly'
                            if (jenis_pelayanan == '10') {
                                var read = ''
                            }

                        }

                        htmlpenetapan = '<div  class="row_sppt row  ' + tahun_pajak +
                            '" >\
                                                                                                                                    <div class="col-md-3 ">\
                                                                                                                                        <div class="form-group row">\
                                                                                                                                            <label for="mulai_tahun" class="col-form-label col-md-6 ">Tahun ' +
                            hps +
                            ' </label>\
                                                                                                                                            <div class="col-md-6">\
                                                                                                                                                <input type="text" name="mulai_tahun[]" id="mulai_tahun" class="angka tahun form-control form-control-sm" data-id="' +
                            tahun_pajak + '" value="' + tahun_pajak +
                            '" readonly>\
                                                                                                                                                <span id="span_' +
                            tahun_pajak +
                            '"></span>\
                                                                                                                                            </div>\
                                                                                                                                        </div>\
                                                                                                                                    </div>\
                                                                                                                                    <div class="col-md-4">\
                                                                                                                                        <div class="form-group row">\
                                                                                                                                            <label for="tgl_jatuh_tempo" class="col-form-label col-md-6 ">JTH Tempo</label>\
                                                                                                                                            <div class="col-md-6">\
                                                                                                                                                <input type="text" name="tgl_jatuh_tempo[]" id="tgl_jatuh_tempo" class="form-control form-control-sm tanggal" value="' +
                            jt + '" ' + read +
                            '>\
                                                                                                                                            </div>\
                                                                                                                                        </div>\
                                                                                                                                    </div>\
                                                                                                                                    <div class="col-md-4" ' +
                            showpot +
                            ' >\
                                                                                                                                        <div class="input-group input-group-sm">\
                                                                                                                                        <div class="input-group-prepend">\
                                                                                                                                        <select class="form-control form-control-sm" name="jns_pot[]"><option value="1">Angka</option><option value="2">Persentase</option></select>\
                                                                                                                                        </div>\
                                                                                                                                        <input type="text" class="form-control form-control-sm angka" name="potongan[]" placeholder="Nilai Potongan" >\
                                                                                                                                        </div>\
                                                                                                                                    </div>\
                                                                                                                                </div>';
                        $("#daftar_penetapan").append(htmlpenetapan)
                        //    $('.tahun').trigger('keyup');
                    }
                }
            }

            loopTahunPenetapan()

            // $('#tambah_sppt').hide()
            $('#tambah_sppt').on('click', function(e) {
                e.preventDefault()
                counttahun = $('#daftar_penetapan .row_sppt').length;

                jenis_pelayanan = $('#jenis_pelayanan').val();

                showpot = ' style="display: none;" '
                if (jenis_pelayanan == '5') {
                    showpot = ' '
                }

                jt = formatDate(parseInt(new Date().getFullYear()) + '-09-30');
                htmlpenetapan = '<div class="row_sppt row  ' + counttahun +
                    '" >\
                                                                                                                                    <div class="col-md-3 ">\
                                                                                                                                        <div class="form-group row">\
                                                                                                                                            <label for="mulai_tahun" class="col-form-label col-md-6 ">Tahun <a role="button" tabindex="0" class="text-danger remove flat"  data-id="' +
                    counttahun + '" data-tahun="' + counttahun +
                    '""  title="Hapus">\
                                                                                                                                                        <i class="fa fa-trash"></i>\
                                                                                                                                                    </a>\
                                                                                                                                                </label>\
                                                                                                                                            <div class="col-md-6">\
                                                                                                                                                <input type="text" name="mulai_tahun[]" id="mulai_tahun" class="angka tahun form-control form-control-sm" data-id="' +
                    counttahun +
                    '" value="" required>\
                                                                                                                                                <span id="span_' +
                    counttahun +
                    '"></span>\
                                                                                                                                            </div>\
                                                                                                                                        </div>\
                                                                                                                                    </div>\
                                                                                                                                    <div class="col-md-4">\
                                                                                                                                        <div class="form-group row">\
                                                                                                                                            <label for="tgl_jatuh_tempo" class="col-form-label col-md-6 ">JTH Tempo</label>\
                                                                                                                                            <div class="col-md-6">\
                                                                                                                                                <input type="text" name="tgl_jatuh_tempo[]" id="tgl_jatuh_tempo" class="form-control form-control-sm tanggal" value="' +
                    jt +
                    '">\
                                                                                                                                            </div>\
                                                                                                                                        </div>\
                                                                                                                                    </div>\
                                                                                                                                    <div class="col-md-4" ' +
                    showpot +
                    '>\
                                                                                                                                        <div class="input-group input-group-sm">\
                                                                                                                                            <div class="input-group-prepend">\
                                                                                                                                            <select class="form-control form-control-sm" name="jns_pot[]"><option value="1">Angka</option><option value="2">Persentase</option></select>\
                                                                                                                                            </div>\
                                                                                                                                            <input type="text" class="form-control form-control-sm angka" name="potongan[]" placeholder="Nilai Potongan" >\
                                                                                                                                            </div>\
                                                                                                                                    </div>\
                                                                                                                                </div>\
                                                                                                                                </div>'
                $("#daftar_penetapan").append(htmlpenetapan)
                $('#daftar_penetapan').find(".tanggal").daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    autoApply: true,
                    minYear: 2003,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'DD MMM YYYY',
                        daysOfWeek: [
                            "Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
                        ],
                        monthNames: [
                            "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
                            "Augustus", "September", "Oktober", "November", "Desember"
                        ],
                    }
                });

            })

            $('#daftar_penetapan').find(".tanggal").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                minYear: 2003,
                maxYear: parseInt(moment().format('YYYY'), 10),
                locale: {
                    format: 'DD MMM YYYY',
                    daysOfWeek: [
                        "Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
                    ],
                    monthNames: [
                        "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus",
                        "September", "Oktober", "November", "Desember"
                    ],
                }
            });

            $(document).on('click', '.remove', function() {
                counttahun = $('#daftar_penetapan').length;
                $("#daftar_penetapan").find('.' + $(this).data('id')).remove()
            });




            // jka pembatalan 2
            var vjp = "{{ $data['penelitian']->jenis_pelayanan }}"
            if (vjp == 2 || vjp == 5) {
                $('#nop_proses').prop('readonly', true);
                vnp = $('#nop_proses').val()
                loadDataPembatalan(vnp)

            }

            function loadDataPembatalan(obj) {
                $('#data-objek-pajak').html('')
                $.ajax({
                    url: "{{ url('informasi/objek-pajak-detail') }}",
                    data: {
                        nop: obj
                    },
                    success: function(res) {
                        $('#data-objek-pajak').html(res)
                        closeloading()
                    },
                    error: function(e) {
                        closeloading()
                        Swal.fire({
                            icon: 'error',
                            title: 'Peringatan',
                            text: 'Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                        })
                        $('#data-objek-pajak').html('')
                    }
                });
            }


            /* var nop = $('#nop_proses').val();
                var convert = formatnop(nop);
                var value = convert;
                $('#nop_proses').val(convert);
                var b = value.replace(/[^\d]/g, "");
                var kec = b.substr(4, 3);
                var kel = b.substr(7, 3); */

            $(document).on('keyup', '.tahun', function() {
                var tahun = $(this).val();
                var idtxt = $(this).data('id');
                count = tahun.length;
                span = $('#span_' + idtxt)
                if (count == 4) {
                    var nop = $('#nop_proses').val();
                    var convert = formatnop(nop);
                    var value = convert;
                    $('#nop_proses').val(convert);
                    var b = value.replace(/[^\d]/g, "");
                    var kec = b.substr(4, 3);
                    var kel = b.substr(7, 3);
                    var kd_znt = $('#kd_znt').val();
                    // hsl = validasiZNT(kec, kel, tahun, kd_znt);

                    // if (hsl == '0') {
                    //     span.html('ZNT ' + kd_znt + ' pada tahun ' + tahun + ' belum di setting')
                    // } else {
                    span.html('')
                    // }
                } else {
                    span.html('')
                }
                // $("#daftar_penetapan").find('.' + $(this).data('id')).remove()
            });

            function validasiZNT(kec, kel, tahun, znt) {
                $.ajax({
                    url: "{{ url('api/cekznt') }}",
                    data: {
                        kd_kecamatan: kec,
                        kd_kelurahan: kel,
                        tahun: tahun,
                        kd_znt: znt
                    },
                    success: function(res) {
                        // return res;
                        // console.log(res);
                    }
                })
            }

            const pecah_jumlah = "{{ $statuspecahan[0]->jumlah ?? 0 }}"
            const pecah_sudah = "{{ $statuspecahan[0]->sudah ?? 0 }}"


            // statuspecahan
            /* if (parseInt(pecah_jumlah) > parseInt(pecah_sudah)) {
                window.location.href = "{{ url($data['urlback']) }}";
            } */

            var vjts = "{{ $data['penelitian']->jts }}"

            // 
            if ($("#nop_proses").val() != "" && vjts != '3') {
                formatnop($("#nop_proses").val())
                vaalue = $("#nop_proses").val()
                $("#nop_proses").keyup()
                // console.log(vaalue)
                // vaalue.length
                if (vaalue.length >= 17) {
                    var ba = vaalue.replace(/[^\d]/g, "");
                    var keca = ba.substr(4, 3);
                    var kela = ba.substr(7, 3);
                    var bloka = ba.substr(10, 3);
                    getZnt(keca, kela, bloka);
                }
            }


            $("#nop_proses").trigger("keyup");



            $(document).on('change', '#subjek_pajak_id', function(e) {
                e.preventDefault()
                let nik = $(this).val()
                console.log('nik :' + nik)
                getDataNik(nik)
            })



            $(document).on('change', '#luas_bumi', function(e) {
                lb = $(this).val();

                if (parseInt(lb) == 0 || lb == '') {
                    vb = 5;
                } else {
                    vb = 3;
                }
                $('#jns_bumi').val(vb).trigger('change');
            });

            function getDataNik(nik) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $('#disclaimer_subjek').html('');
                $.ajax({
                    'url': "{{ url('api/subjek-pajak') }}",
                    'method': 'post',
                    'data': {
                        _token: CSRF_TOKEN,
                        nik
                    },
                    success: function(res) {

                        if (res.data != null) {
                            let konten_dis =
                                '<div class="callout callout-danger bg-danger">\
                                                                                                                        <h5>Disclaimer!</h5>\
                                                                                                                        <p>' +
                                res
                                .data
                                .subjek_pajak_id +
                                ' terdafatar di database PBB dengan nama ' +
                                res
                                .data.nm_wp +
                                ' </p>\
                                                                                                                        </div>';

                            $('#disclaimer_subjek').html(konten_dis)
                        }
                    },
                    error: function(er) {

                    }
                })
            }


            $('#subjek_pajak_id').trigger('change')

            $('#tipe_pengurangan').on('change', function(e) {
                let val
                if ($('#tipe_pengurangan').val() == '1') {
                    val = null;
                } else {
                    val = $('option:selected', '#jns_pengurangan').attr('persen');
                }
                $('#nilai_pengurangan').val(val)
            })



            $('#jns_pengurangan').on('change', function() {
                $('#tipe_pengurangan').trigger('change')
            })
            $('#tipe_pengurangan').trigger('change')

        });

        window.addEventListener("beforeunload", function(e) {
            openloading()
        });
    </script>
@endsection
