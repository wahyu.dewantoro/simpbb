<html>

<head>
    <style>
        body {
            font-family: "Calibri (Body)";
            font-size: 1em;
            text-transform: uppercase;
        }

        table {

            font-family: "Calibri (Body)";
            font-size: 0.9 em;
            width: 100%
        }

        #watermark {
            position: fixed;
            top: 80mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

        #pojokkananatas {
            position: fixed;
            top: 26mm;
            width: 100%;
            right: 5mm;
            text-align: right;
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            font-size: 1em !important;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 1em !important;
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 1em !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 0.7em !important;
        }

        .text-sedang {
            font-size: 1em !important;
        }

        .page-number:before {
            content: "Page "counter(page)
        }

        .inti {
            border: 1px solid black;
            margin: auto;
        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        .inti td {
            border-right: 1px solid black;
            padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;
            padding-top: 2px;
        }

        .inti th {
            border-right: 1px solid black;
            border-bottom: 1px solid black;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            /* background-color:grey; */
            color: black;
            text-align: center;
            line-height: 0.5cm;
            font-size: small;
        }

    </style>
</head>
<body>
    <table width="100%" class="table-sm table " style="border: 1px solid #212529">
        <tr>
            <td style="border: 1px solid #212529">
                <table width="100%">
                    <tr style="vertical-align: top">
                        <td width="150px">
                            <strong>TRANSAKSI</strong>
                        </td>
                        <td>
                            : {{ jenisTransaksiTanah($objek->jns_transaksi) }}
                        </td>
                    </tr>
                    <tr>
                        <td><strong>NOP PROSES</strong></td>
                        <td>: {{ formatnop($objek->nop_proses )}}</td>
                    </tr>
                    <tr>
                        <td><strong>NOP BERSAMA</strong></td>
                        <td>:</td>
                    </tr>
                    <tr>
                        <td><strong>NOP ASAL</strong></td>
                        <td>: {{ formatnop($objek->nop_asal<>''?$objek->nop_asal:$objek->nop_proses)}}</td>
                    </tr>
                    <tr>
                        <td><strong>NO SPPT LAMA</strong></td>
                        <td>:</td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #212529;  text-align:center">
                DATA LETAK OBJEK PAJAK
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #212529">
                <table class="table table-sm">
                    <tr style="vertical-align:top">
                        <td width="50%">
                            <strong>NAMA JALAN</strong> <br>
                            {{ $objek->jalan_op }}
                        </td>
                        <td>
                            <strong>BLOK / KAVLING / NOMOR</strong> <br>
                            {{ $objek->blok_kav_no_op }}
                        </td>
                    </tr>
                    <tr style="vertical-align:top">
                        <td>
                            <strong>KELURAHAN / DESA</strong><br>
                            &nbsp;
                        </td>
                        <td>
                            <strong>RT / RW</strong><br>
                            {{ $objek->rt_op<>''?$objek->rt_op:'000' }} / {{ $objek->rw_op<>''?$objek->rw_op:'00' }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #212529;  text-align:center">
                DATA SUBJEK PAJAK
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #212529; ">
                <table width="100%">
                    <tr>
                        <td width="50%">
                            <table width="100%">
                                <tr style="vertical-align: top">
                                    <td width="150px"><strong>Status</strong></td>
                                    <td>: {{ statusWP($objek->kd_status_wp) }}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>Pekerjaan</strong></td>
                                    <td>: {{ jenisPekerjaan($objek->status_pekerjaan_wp) }}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>Nama Subjek</strong></td>
                                    <td>:{{ $objek->nm_wp }}</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                <tr style="vertical-align: top">
                                    <td width="150px"><strong>ALAMAT</strong></td>
                                    <td>: {{ $objek->jalan_wp }} {{ $objek->blok_kav_no_wp }} <br>
                                        RT: {{ $objek->rt_wp }} RW : {{ $objek->rw_wp }}<br>
                                        {{ $objek->kelurahan_wp }} <br>
                                        {{ $objek->kota_wp }} {{ $objek->kd_pos_wp }} </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #212529;  text-align:center">
                DATA TANAH
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #212529">
                <table class="table table-sm">
                    <tr style="vertical-align: top">
                        <td width="150px"><strong>Luas tanah</strong></td>
                        <td>: {{ angka($objek->luas_bumi) }}</td>
                    </tr>
                    <tr>
                        <td><strong>Jenis tanah</strong></td>
                        <td>: {{ jenisBumi($objek->jns_bumi) }}</td>
                    </tr>
                    <tr>
                        <td><strong>zona nilai tanah</strong></td>
                        <td>: {{ $objek->kd_znt }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


</body>
</html>
