<!DOCTYPE html>

<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo') }}/favicon.ico" />
    <title>SIMPBB P2</title>

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="SIPANJI : SIMPBB">
    <meta itemprop="description" content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta itemprop="image" content="{{ asset('sipanji.png') }}">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="http://sipanji.id/simpbb">
    <meta property="og:type" content="website">
    <meta property="og:title" content="SIPANJI : SIMPBB">
    <meta property="og:description" content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta property="og:image" content="{{ asset('sipanji.png') }}">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="SIPANJI : SIMPBB">
    <meta name="twitter:description" content="SIMPBB Meruapakan sistem pelayanan PBB P2 dari BAPENDA KABUPATEN Malang untuk memudahkan dalam melayani masyarakat">
    <meta name="twitter:image" content="{{ asset('sipanji.png') }}">

    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/fontawesome-free/css/all.min.css">
    {{-- toast --}}
    <link href="{{ asset('lte') }}/plugins/toastr/toastr.min.css" rel="stylesheet" />
    {{-- datatables --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    {{-- <!-- Theme style --> --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/dist/css/adminlte.min.css">
    {{-- daterange --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/daterangepicker/daterangepicker.css">

    {{-- datepicker --}}
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/datepicker/css/datepicker.css">
    <link rel="stylesheet" href="{{ asset('lte') }}/plugins/select2/css/select2.min.css">
    <style>
        .hero-image {
            background-image: url("background.png");
            background-color: #cccccc;
            height: auto;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
        }

    </style>

</head>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">

        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <img src="{{ asset('sipanji.png') }}" alt="SIPANJI LARGE" class="brand-image-xs logo-xl" style="left: 12px; max-height: 30px">
                <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                    
                    <li class="nav-item">
                        {{--<a class="nav-link" href="{{ url('login') }}">
                            <i class="fas fa-sign-in-alt"></i>
                            Login
                        </a>--}}
                        <a class="nav-link" href="#"></a>
                    </li>
                </ul>
            </div>
        </nav>


        <div class="content-wrapper hero-image">
            <section class="content-header">
                <div class="container-fluid">
                    <h1 class="text-center display-4"><b>Form E SPPT</b></h1>
                </div>
            </section>
            <section class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </section>
        </div>


        <!-- ./wrapper -->
        <span id="msg-success" data-msg="{!! session('success') !!}"></span>
        <span id="msg-error" data-msg="{!! session('error') !!}"></span>
        <span id="msg-warning" data-msg="{!! session('warning') !!}"></span>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-inline">
                SIMPBB Part Of SIPANJI
            </div>
            <strong>Copyright &copy; 2021 <a href="http://sipanji.id">SIPANJI</a>.</strong> All rights
            reserved.
        </footer>
    </div>


    <!-- jQuery -->
    <script src="{{ asset('lte') }}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('lte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('lte') }}/dist/js/adminlte.min.js"></script>
    {{-- toastr --}}
    <script src="{{ asset('lte') }}/plugins/toastr/toastr.min.js"></script>
    {{-- moment --}}
    <script src="{{ asset('lte') }}/plugins/moment/moment.min.js"></script>
    {{-- date range --}}
    <script src="{{ asset('lte') }}/plugins/daterangepicker/daterangepicker.js"></script>

    {{-- datepicker --}}
    <script src="{{ asset('lte') }}/plugins/datepicker/js/bootstrap-datepicker.js"></script>

    <script src="{{ asset('sweetalert2') }}/dist/sweetalert2.all.min.js"></script>
    <script src="{{ asset('plugins') }}/jquery.form.min.js"></script>
    <script src="{{ asset('plugins') }}/jquery.maskMoney.min.js"></script>
    <script src="{{ asset('plugins') }}/inputmask/dist/jquery.inputmask.min.js"></script>
    <script src="{{ asset('lte') }}/plugins/select2/js/select2.full.min.js"></script>


    {{-- <!-- DataTables -->
    <script src="{{ asset('lte') }}/plugins/datatables/jquery.dataTables.js"></script>
    <script src="{{ asset('lte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script> --}}

    {{-- icon fontawesome --}}
    <script src="{{ asset('js') }}/appcomponent.js"></script>
    <script>
        function getBrowser() {
            // Opera 8.0+
            var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

            // Firefox 1.0+
            var isFirefox = typeof InstallTrigger !== 'undefined';

            // Safari 3.0+ "[object HTMLElementConstructor]" 
            var isSafari = /constructor/i.test(window.HTMLElement) || (function(p) {
                return p.toString() === "[object SafariRemoteNotification]";
            })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));

            // Internet Explorer 6-11
            var isIE = /*@cc_on!@*/ false || !!document.documentMode;

            // Edge 20+
            var isEdge = !isIE && !!window.StyleMedia;

            // Chrome 1 - 79
            var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

            // Edge (based on chromium) detection
            var isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);

            // Blink engine detection
            var isBlink = (isChrome || isOpera) && !!window.CSS;


            var output;
            if (isFirefox == true) {
                output = 'Firefox'
            }
            if (isChrome == true) {
                output = 'Chrome'
            }
            if (isSafari == true) {
                output = 'Safari'
            }
            if (isOpera == true) {
                output = 'Opera'
            }
            if (isIE == true) {
                output = 'IE'
            }
            if (isEdge == true) {
                output = 'Edge'
            }
            if (isEdgeChromium == true) {
                output = 'EdgeChromium'
            }
            return output
        }
        $(document).on({
            ajaxStart: function() {
                openloading();
            }
            , ajaxStop: function() {
                closeloading();
            }
        });

        $(document).on('keypress', function(e) {
            if (e.which == 13) {

            }
        });

        $(document).ready(function() {

            $('.form-control').on('keyup', function() {
                $(this).removeClass('is-invalid');
            });

            $('#otp').hide();


            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $('.angka').keypress(function(event) {

                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault(); //stop character from entering input
                }

            });

            $('#cetak').on('click', function(e) {
                e.preventDefault();
                var url = $('#cetak').data('href');
                // alert(url);
                openTab(url);
            });


            var success = $('#msg-success').data('msg');
            var error = $('#msg-error').data('msg');
            var warning = $('#msg-warning').data('msg');

            // Display a success toast, with a title
            if (success != '') {
                toastr.success(success);
            }

            // Display an error toast, with a title
            if (error != '') {
                toastr.error(error);
            }

            // Display a success toast, with a title
            if (warning != '') {
                toastr.success(warning);
            }

            var url = window.location;



            // kirim-otp
            /*     $('#kirim-otp').on('click', function(e) {
                // console.log('clicked')
                e.preventDefault()
                $('#otp_verifikasi').val('')

                var nik = $('#nik').val();
                var nama = $('#nama').val();
                var telepon = $('#telepon').val();
                var nop = $('#nop').val();
                var captcha = $("captcha").val()

                if (nama.length == 0 || nop.length < 24) {
                    if (nik.length < 16) {
                        $("#nik").addClass('is-invalid')
                    }

                    if (nama.length == 0) {
                        $("#nama").addClass('is-invalid')
                    }

                    // if (telepon.length == 0) {
                    //     $("#telepon").addClass('is-invalid')
                    // }

                    if (nop.length < 24) {
                        $("#nop").addClass('is-invalid')
                    }
                    return false;
                }

                $.ajax({
                    type: "POST"
                    , url: "{{ url('api/send-otp') }}"
                    , data: {
                        '_token': '{{ csrf_token() }}'
                        , 'browser': getBrowser()
                        , 'nomor': null
                        , 
                        , url: window.location.href
                    }
                    , success: function(res) {
                        $('#otp_verifikasi').val(res)
                        $('#otp').show();
                    }
                    , error: function(res) {}
                })


            })
 */

        });
        // $body = $("body");
        // 
        function openloading() {
            Swal.fire({
                // icon: 'error',
                title: '<i class="fas fa-sync fa-spin"></i>'
                , text: 'Sistem sedang berjalan, mohon ditunggu!'
                , allowOutsideClick: false
                , allowEscapeKey: false
                , showConfirmButton: false
            })
        }

        function closeloading() {
            swal.close();
        }

        function openTab(url) {
            window.open(url
                , 'newwindow'
                , 'width=700,height=1000');
            return false;
        }


        function formatnop(a) {
            // a = objek.value;
            var b = a.replace(/[^\d]/g, "");
            var c = "";
            var panjang = b.length;

            if (panjang <= 2) {
                // 35 -> 0,2
                c = b;
            } else if (panjang > 2 && panjang <= 4) {
                // 07. -> 2,2
                c = b.substr(0, 2) + '.' + b.substr(2, 2);
            } else if (panjang > 4 && panjang <= 7) {
                // 123 -> 4,3
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
            } else if (panjang > 7 && panjang <= 10) {
                // .123. ->
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
            } else if (panjang > 10 && panjang <= 13) {
                // 123.
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10
                    , 3);
            } else if (panjang > 13 && panjang <= 17) {
                // 1234
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10
                    , 3) + '-' + b.substr(13, 4);
            } else {
                // .0
                c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10
                    , 3) + '-' + b.substr(13, 4) + '.' + b.substr(17, 1);
            }
            return c;
        }

    </script>
    @yield('script')
</body>

</html>
