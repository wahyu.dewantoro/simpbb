<html>
<head>
    <style>
        body {
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            size: 180mm 188mm;
            margin: 0;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;

            @if($jsppt=='1') background-image: url("{{ public_path('esppt_copy.jpeg') }}");
            background-size: 182mm 190mm;
            @endif
        }

        p {
            font-family: "Calibri", sans-serif;
        }

        #watermark {
            position: fixed;
            opacity: 0.2;
            /* margin: 0; */
        }

        @page {
            size: 180mm 188mm;
            margin: 0;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif
        }

        .konten {
            margin-top: 14mm;
            margin-left: 7mm;
            margin-right: 10mm;
        }



        #tahun_pajak {
            position: fixed;
            margin-top: 39mm;
            margin-left: 148mm;
            margin-right: 10mm;
            font-weight: bold;
            font-size: 30px;
        }

        .table-bordered>tbody>tr>td,
        .table-bordered>thead>tr>td,
        .table-bordered {
            border: none;
            border-collapse: collapse;
            border-right: solid 1px black;
            border-left: solid 1px black;
        }

    </style>
    <title>
        {{ $sppt->kd_propinsi . $sppt->kd_dati2 . $sppt->kd_kecamatan . $sppt->kd_kelurahan . $sppt->kd_blok . $sppt->no_urut . $sppt->kd_jns_op.' '.$sppt->thn_pajak_sppt }}
    </title>
</head>
<body>
    <div id="tahun_pajak">{{ $sppt->thn_pajak_sppt }}</div>
    <div class="konten">
        <p style="  text-align: center; font-weight:bold; font-size: 15px; margin-bottom:3px; ">SURAT PEMBERITAHUAN PAJAK TERHUTANG<BR>PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN</p>
        <span style="font-weight: bold; 	float: left; font-size: 12px ">NOP : {{ $sppt->kd_propinsi }}.{{ $sppt->kd_dati2 }}.{{ $sppt->kd_kecamatan }}.{{ $sppt->kd_kelurahan }}.{{ $sppt->kd_blok }}-{{ $sppt->no_urut }}.{{ $sppt->kd_jns_op }} </span>
        @if($objek->kd_status_wp<>1)
            <span style="font-weight: bold;  	float: right; font-size: 12px ">STATUS : {{ statusWP($objek->kd_status_wp) }}</span>
            @endif
            <table style=" table-layout: fixed; width: 100%; margin-top: 15px; margin-bottom: 0;" cellspacing='0' cellpadding='5' border="0">
                <tr style="vertical-align: top">
                    <td style="width: 38%; padding: 0">
                        <p style=" margin-top:0; margin-bottom: 2px; font-weight: bold; font-size: 12px;">LETAK OBJEK PAJAK</p>
                        <p style="font-size: 8pt; margin-top:0; margin-bottom: 0">
                            @if (trim($objek->jalan_op)!='' || trim($objek->blok_kav_no_op)!='')
                            {{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }}<br>
                            @endif
                            @php
                            $rto=trim($objek->rt_op) != '' ? 'RT :' . $objek->rt_op : '';
                            $rwo=trim($objek->rw_op) != '' ? 'RW :' . $objek->rw_op : '';
                            @endphp
                            @if ($rwo!='' || $rto!='')
                            {{ $rto.' '.$rwo}} <br>
                            @endif
                            {{ $objek->nm_kelurahan }} <br>{{ $objek->nm_kecamatan }} <br> KABUPATEN MALANG
                        </p>
                    </td>
                    <td style="width: 38%; padding:0">
                        <p style=" margin-top:0; margin-bottom: 2px; font-weight: bold; font-size: 12px; ">NAMA DAN ALAMAT WAJIB PAJAK</p>
                        <p style="font-size: 8pt; margin-top:0; margin-bottom: 0">
                            {{ $sppt->nm_wp_sppt }}<br>
                            @if (trim($sppt->jln_wp_sppt)!='' || trim($sppt->blok_kav_no_wp_sppt)!='')
                            {{ $sppt->jln_wp_sppt }} {{ $sppt->blok_kav_no_wp_sppt }}<br>
                            @endif

                            @php
                            $rtp=trim($sppt->rt_wp_sppt) != '' ? 'RT :' . $sppt->rt_wp_sppt : '';
                            $rwp=trim($sppt->rw_wp_sppt) != '' ? 'RW :' . $sppt->rw_wp_sppt : '';
                            @endphp
                            @if ($rwp!='' || $rtp!='')
                            {{ $rtp.' '.$rwp}} <br>
                            @endif

                            @if ($sppt->kelurahan_wp_sppt != '')
                            {{ $sppt->kelurahan_wp_sppt }}<br>
                            @endif
                            @if ( $sppt->kota_wp_sppt != '')
                            {{ $sppt->kota_wp_sppt }}
                            @endif

                        </p>
                    </td>
                    <td style="width: 25%; text-align: center; vertical-align: middle ">
                        <p style="font-size: 25px; font-weight: bold; color:white"> &nbsp;</p>
                    </td>
                </tr>
            </table>
    </div>
    <table style="font-size: 10px;  table-layout: fixed; 
            width: 165mm;
                position: fixed;
            margin-top: 55mm;
            margin-left: 7mm;
            margin-right: 10mm;" cellspacing='0' border="0" cellpadding='1' class="table-bordered" padding='4'>
        <thead>
            <tr style="text-align: center; vertical-align: top;font-weight: bold; ">
                <td style="width:20%; border: solid 1px black;">OBJEK PAJAK</td>
                <td style="width:10%; border: solid 1px black;">LUAS M<sup>2</sup></td>
                <td style="width:10%; border: solid 1px black;">KELAS</td>
                <td style="width:40%; border: solid 1px black;">NJOP PER M<sup>2</sup> (Rp)</td>
                <td style="width:40%; border: solid 1px black;">TOTAL NJOP (Rp)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style=" border-bottom: 0;">BUMI</td>
                <td style="text-align: right; border-bottom: 0;">{{ angka($sppt->luas_bumi_sppt, 0, '', '.') }}</td>
                <td style="text-align: center;  border-bottom: 0; border-bottom-color:white; ">{{ $sppt->kd_kls_tanah }}</td>
                <td style="text-align: right;  border-bottom: 0; border-bottom-color:white; ">{{ $sppt->luas_bumi_sppt>0?angka($sppt->njop_bumi_sppt / $sppt->luas_bumi_sppt, 0, '', '.'):0 }}</td>
                <td style="text-align: right;  border-bottom: 0; border-bottom-color:white; ">{{ angka($sppt->njop_bumi_sppt, 0, '', '.') }}</td>
            </tr>
            <tr>
                <td style="border-bottom: solid 1px black;">BANGUNAN</td>
                <td style="border-bottom: solid 1px black;  text-align: right">{{ angka($sppt->luas_bng_sppt, 0, '', '.') }}</td>
                <td style="border-bottom: solid 1px black;  text-align: center">{{ $sppt->kd_kls_bng }}</td>
                <td style="border-bottom: solid 1px black;  text-align: right">{{ $sppt->luas_bng_sppt > 0 ? angka($sppt->njop_bng_sppt / $sppt->luas_bng_sppt, 0, '', '.') : null }}</td>
                <td style="border-bottom: solid 1px black;  text-align: right">{{ angka($sppt->njop_bng_sppt, 0, '', '.') }}</td>
            </tr>
            <tr>
                <td style="border-right: 0;" colspan="3">NJOP Sebagai dasar pengenaan PBB P2</td>
                <td style="border-left: 0; border-right: 0;">=</td>
                <td style="text-align: right; border-left: 0; ">{{ angka($sppt->njop_bumi_sppt + $sppt->njop_bng_sppt) }}</td>
            </tr>
            <tr>
                <td style="border-right: 0;" colspan="3">NJOPTKP (NJOP Tidak Kena Pajak)</td>
                <td style="border-left: 0; border-right: 0;">=</td>
                <td style="text-align: right; border-left: 0; ">{{ angka($sppt->njoptkp_sppt) }}</td>
            </tr>
            <tr>
                <td style="border-right: 0;" colspan="3">NJOP untuk perhitungan PBB P2</td>
                <td style="border-left: 0; border-right: 0;">=</td>
                <td style="text-align: right; border-left: 0; ">
                    @php
                    $njop_sppt= $sppt->njop_bumi_sppt + $sppt->njop_bng_sppt - $sppt->njoptkp_sppt;
                    @endphp
                    {{ angka($njop_sppt ) }}
                </td>
            </tr>
            <tr>
                <td style="border-right: 0;" colspan="3">PBB P2 yang Terhutang</td>
                <td style="border-left: 0; border-right: 0;">=</td>
                <td style="text-align: right; border-left: 0; ">
                    <?=  str_replace('.',',',  ($sppt->tarif)*100 )?> % x
                    {{ angka($njop_sppt  ) }} =
                    {{ angka($sppt->pbb_terhutang_sppt) }}
                </td>
            </tr>
            <tr style="border-top: solid 1px black">
                <td colspan="4" style=" border-right: 0; font-size: 10px">PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN YANG HARUS DIBAYAR </td>
                <td style="border-left: 0; text-align: right">Rp. {{ angka($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan).',-' }}</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black;" colspan="5"><i style="font-weight: bold;  font-size: 10px">{{ strtoupper(terbilang($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan)) }} </i></td>
            </tr>

        </tbody>
    </table>
    @if($sppt->baru==0)
    <table style="position: fixed; margin-left: 7mm; margin-right: 6mm;margin-top: 100mm;   font-size: 11px;  table-layout: fixed; width: 165mm;" cellspacing='0' cellpadding='1'>
        <tr>
            <td colspan="5" style="font-weight: bold">KONFIRMASI TUNGGAKAN :</td>
        </tr>
        <tr>
            @for($tahun = $sppt->thn_pajak_sppt - 1; $tahun >= $sppt->thn_pajak_sppt - 5; $tahun--) <td style="border:solid 1px black; text-align:center; font-weight:bold; width:20%"> {{ $tahun}} </td> @endfor
        </tr>
        <tr>
            @for($tahun = $sppt->thn_pajak_sppt - 1; $tahun >= $sppt->thn_pajak_sppt - 5; $tahun--) <td style="border:solid 1px black; text-align:center; width:20%">{{ $tunggakan[$tahun]??'LUNAS' }}</td>@endfor
        </tr>
        @if(count($tunggakan)>0)
        <tr>
            <td colspan="5" style="font-style: italic"><small>Catatan: belum teramasuk denda.</small></td>
        </tr>
        @endif
    </table>
    @endif

    <table style="position: fixed; margin-left: 6mm; margin-right: 6mm;margin-top: 118mm;  font-size: 11px;  table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1'>
        <tr style="vertical-align: top">
            <td style="width:50%;">
                TGL. JATUH TEMPO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ tglIndo($sppt->tgl_jatuh_tempo_sppt) }}<br>
                TEMPAT PEMBAYARAN :<br>
                {{ $tp->nama_tempat }}

            </td>
            {{-- border-left: solid 1px black; --}}
            <td style="width:50%; text-align: center; vertical-align: top; ">
                <p style="margin: 0">
                    MALANG, {{ tglIndo($sppt->tgl_terbit_sppt) }}<br>
                    {{ $kaban->is_plt != '' ? 'PLT' : '' }} {{ $kaban->jabatan }}<br>
                    <img src="data:image/png;base64, {!! $qrcode !!}">
                    <br>
                    <b>{{ $kaban->nama }}</b><br>NIP : {{ $kaban->nip }}
                </p>
            </td>
        </tr>
    </table>
    <table style="font-size: 10px; position: fixed; margin-top: 160mm; margin-left: 6mm; margin-right: 6mm; table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='3'>
        <tr>
            <td style="vertical-align: top">
                NAMA WP :<br>

                Letak Objek Pajak : Desa/Kelurahan {{ $objek->nm_kelurahan }}<br>
                Kecamatan {{ $objek->nm_kecamatan }} <br>
                NOP : {{ $sppt->kd_propinsi }}.{{ $sppt->kd_dati2 }}.{{ $sppt->kd_kecamatan }}.{{ $sppt->kd_kelurahan }}.{{ $sppt->kd_blok }}-{{ $sppt->no_urut }}.{{ $sppt->kd_jns_op }}<br>
                SPPT Tahun/Rp. : {{ $sppt->thn_pajak_sppt }} / {{ angka($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan) }}
            </td>
            <td style="width:45%; text-align: left; vertical-align: top; ">
                Diterima Tgl &nbsp;&nbsp;&nbsp; :<br>
                Tanda Tangan :<br><br>
                <p style="text-align: center">……………………………………………………<br>
                    Nama Terang</p>
            </td>
        </tr>
    </table>
</body>
</html>
