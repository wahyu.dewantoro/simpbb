<div class="row">
    <div class="col-6">
        <strong>Hasil Pencarian
        </strong>
    </div>
    <div class="col-6">
        <div class="float-right">
            @if (empty($response['keterangan']))
                <a id="cetak"
                    data-href="{{ url('esppt') }}?nop={{ $response['data']->kd_propinsi .$response['data']->kd_dati2 .$response['data']->kd_kecamatan .$response['data']->kd_kelurahan .$response['data']->kd_blok .$response['data']->no_urut .$response['data']->kd_jns_op .$response['data']->thn_pajak_sppt }}"
                    href="#"><i class="fas fa-file-download text-success"></i> Download</a>
                &nbsp;&nbsp;&nbsp;
            @endif
            <a href="#" id="kembali"><i class="fas fa-window-close text-warning"></i></a>
        </div>

    </div>
    <div class="col-12">
        <hr>
        @if ($response['objek'] == true)
            <table class="table table-sm table-borderless">
                <tbody>
                    <tr>
                        <td style="width: 30%">NOP</td>
                        <td>:
                            {{ $response['data']->kd_propinsi .'.' .$response['data']->kd_dati2 .'.***.***.***-' .$response['data']->no_urut .'.' .$response['data']->kd_jns_op }}
                        </td>
                    </tr>
                    <tr>
                        <td>Subjek Pajak</td>
                        <td>:
                            @php
                                $str = '';
                                $exp = explode(' ', $response['data']->nm_wp_sppt);
                                foreach ($exp as $exp) {
                                    $str .= sensorText($exp) . ' ';
                                }
                                echo $str;
                            @endphp
                        </td>
                    </tr>
                    <tr>
                        <td>PBB</td>
                        <td>: {{ angka($response['data']->pbb_yg_harus_dibayar_sppt) }}</td>
                    </tr>
                </tbody>
            </table>
            Keterangan:
            <br>
            @if (empty($response['keterangan']))
                E-SPPT Siap untuk di unduh/Download
            @endif
            <ol>
                @foreach ($response['keterangan'] as $ket)
                    <li>{{ $ket }}</li>
                @endforeach
            </ol>
        @else
            <i class="fas fa-exclamation-triangle text-danger"></i> NOP tidak terdaftar dalam database BAPENDA
        @endif
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#kembali').on('click', function() {
            $('#hasil').html('');
            // document.getElementById("form").reset();
            $('#form').show();
        });
        $('#cetak').on('click', function(e) {
            e.preventDefault();
            var url = $('#cetak').data('href');
            // alert(url);
            openTab(url);
        });
    })

    function openTab(url) {
        window.open(url,
            'newwindow',
            'width=700,height=1000');
        return false;
    }
</script>
