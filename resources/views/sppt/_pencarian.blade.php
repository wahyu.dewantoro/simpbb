@extends('sppt.core_pencarian')

@section('content')
<div class="row">
    <div class="col-md-10 offset-md-1">
        <div class="card card-outline card-info">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <strong>Hasil Pencarian
                        </strong>
                    </div>
                    <div class="col-6">
                        <div class="float-right">

                            <a href="{{ url('page-sppt') }}" id="kembsaali"><i class="fas fa-window-close text-warning"></i></a>
                        </div>

                    </div>
                    <div class="col-12">
                        <hr>
                        @if ($response['objek'] == true )
                        @if($response['status']==1)
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td style="width: 30%">NOP</td>
                                    <td>:
                                        {{-- $response['data']->kd_propinsi .'.' .$response['data']->kd_dati2 .'.***.***.***-' .$response['data']->no_urut .'.' .$response['data']->kd_jns_op --}}
                                        {{$response['data']->kd_propinsi .'.' .$response['data']->kd_dati2 .'.'.$response['data']->kd_kecamatan.'.'.$response['data']->kd_kelurahan.'.'.$response['data']->kd_blok.'-' .$response['data']->no_urut .'.' .$response['data']->kd_jns_op}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Subjek Pajak</td>
                                    <td>: {{$response['data']->nm_wp_sppt}}
                                        <!-- @ php
                                        $str = '';
                                        $exp = explode(' ', $response['data']->nm_wp_sppt);
                                        foreach ($exp as $exp) {
                                        $str .= sensorText($exp) . ' ';
                                        }
                                        echo $str;
                                        @ endphp -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>PBB</td>
                                    <td>: 
                                        {{-- sensorText(angka($response['data']->pbb_yg_harus_dibayar_sppt)) --}}
                                        {{angka($response['data']->pbb_yg_harus_dibayar_sppt)}}
                                        <br>
                                        @if ($response['objek'] == true && $response['status']==1 )
                                        <a id="cetak" data-href="{{ url('esppt') }}?nop={{ $response['data']->kd_propinsi .$response['data']->kd_dati2 .$response['data']->kd_kecamatan .$response['data']->kd_kelurahan .$response['data']->kd_blok .$response['data']->no_urut .$response['data']->kd_jns_op .$response['data']->thn_pajak_sppt }}" href="#"><i class="fas fa-file-download text-success"></i> Download</a>
                                        &nbsp;&nbsp;&nbsp;
                                        @endif</td>
                                </tr>
                            </tbody>
                        </table>
                        {{$response['detail']}}
                        Keterangan:
                        <br>
                            @if (empty($response['keterangan'])||$response['keterangan'][0]=='-')
                            <ol>
                                <li><strong>E- SPPTa PBB P2 bukanlah bukti kepemilikan hak. </strong></li>
                                <li>Informasi pada E-SPPT PBB P2 ini adalah kondisi objek pajak per 1 januari tahun pajak berjalan. </li>
                                <li>Nilai jual objek pajak PBB P2 di gunakan untuk tujuan perpajakan. </li>
                                <li>Formulir E-SPPT PBB P2 ini digunakan untuk keperluan penyampaian SPPT PBB P2 secara elektronik</li>
                                <li>Penyampaian E-SPPT PBB P2 disamakan dengan SPPT PBB P2 yang di cetak secara masal. </li>

                            </ol>
                            Riwayat Permohonan Online:
                            <table id="main-table" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th class='number'>No</th>
                                        <th>Nomor Layanan</th>
                                        <th>Jenis Layanan</th>
                                        <th>Nama</th>
                                        <th>Tgl Layanan</th>
                                        <th>Status</th>
                                        <!-- <th>Aksi</th> -->
                                    </tr>
                                </thead>
                                <tbody class='table-sm'>
                                    @php $no=1; @endphp
                                    @foreach($response['layanan'] as $itemLayanan)
                                        <td>{{$no}}</td>
                                        <td>{!!$itemLayanan->nomor_layanan!!}</td>
                                        <td>{{$itemLayanan->jenis_layanan_nama}}</td>
                                        <td>{{$itemLayanan->nama}}</td>
                                        <td>{!!$itemLayanan->created_at!!}</td>
                                        <td>{!!$itemLayanan->kd_status!!}</td>
                                        {{--<td>{!!$itemLayanan->option!!}</td>--}}
                                        @php $no++; @endphp
                                    @endforeach
                                </tbody>
                            </table>


                            @endif
                            @if (count($response['keterangan'])>1)
                            <ol>
                                @foreach ($response['keterangan'] as $ket)
                                <li><?=  $ket ?></li>
                                @endforeach
                            </ol>
                            @endif
                        @else
                        {{-- data yang tidak sesuai dengan NIK nya --}}
                        Riwayat Permohonan Online:
                        <table id="main-table" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th class='number'>No</th>
                                    <th>Nomor Layanan</th>
                                    <th>Jenis Layanan</th>
                                    <th>Nama</th>
                                    <th>Tgl Layanan</th>
                                    <th>Status</th>
                                    <!-- <th>Aksi</th> -->
                                </tr>
                            </thead>
                            <tbody class='table-sm'>
                                @php $no=1; @endphp
                                @foreach($response['layanan'] as $itemLayanan)
                                    <td>{{$no}}</td>
                                    <td>{!!$itemLayanan->nomor_layanan!!}</td>
                                    <td>{{$itemLayanan->jenis_layanan_nama}}</td>
                                    <td>{{$itemLayanan->nama}}</td>
                                    <td>{!!$itemLayanan->created_at!!}</td>
                                    <td>{!!$itemLayanan->kd_status!!}</td>
                                    {{--<td>{!!$itemLayanan->option!!}</td>--}}
                                    @php $no++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <p><strong>Data yang anda masukkan belum sesuai</strong>, anda bisa mendapatkan E-SPPT melalui Kantor Desa atau silahkan isi Formulir untuk mendapatkan E SPPT sesuai kondisi sebenarnya dengan ketentuan sebagai berikut :</p>
                        <ol>
                            <li>Bahwa Saya Wajib Pajak dengan sadar dan bertanggungjawab terhadap isian formulir pembetulan PBB beserta bukti pendukungnya adalah benar.</li>
                            <li>Upload KTP dan bukti kepemilikkan serta foto selfi dengan memegang KTP.</li>
                            <li>Kesalahan dalam pengisian dapat menyebabkan tidak tersampaikannya informasi SPPT PBB Elektronik.</li>
                            <li>Menyalahgunakan layanan ini adalah tindakan melawan Undang-Undang tentang IT.</li>
                        </ol>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="persetujuan">
                            <label class="form-check-label" for="persetujuan"><strong>Ya,setuju.</strong></label>
                        </div>
                        <div id="div_form">
                            <div class="card card-outline card-warning">
                                <div class="card-header">
                                    <h3 class="card-title">Form pembetulan subjek pajak</h3>
                                </div>
                                <form action="#" id="form-perbaikan" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="card-body p-1">

                                        @method('post')
                                        @csrf
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="nik">NIK</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="nik" name="nik" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="nama">Nama</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="nama" name="nama" value="{{ $response['data']->nm_wp_sppt }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="nomor_telepon">Telepon / WA</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="nomor_telepon" name="nomor_telepon">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="alamat">Alamat</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="alamat" name="alamat" value="{{ $response['data']->jln_wp_sppt }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label" for="blok_kav_no">Blok / No</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="blok_kav_no" name="blok_kav_no" value="{{ $response['data']->blok_kav_no_wp_sppt }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="kelurahan">Kelurahan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="kelurahan" name="kelurahan" value="{{ $response['data']->kelurahan_wp_sppt }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="kecamatan">Kecamatan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="kecamatan" name="kecamatan">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="dati2">Kota / Kab</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="dati2" name="dati2" value="{{ $response['data']->kota_wp_sppt }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label" for="propinsi">Propinsi</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control form-control-border border-width-2" id="propinsi" name="propinsi">
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" name="kd_propinsi" id="kd_propinsi" value="{{ $response['data']->kd_propinsi }}">
                                            <input type="hidden" name="kd_dati2" id="kd_dati2" value="{{ $response['data']->kd_dati2 }}">
                                            <input type="hidden" name="kd_kecamatan" id="kd_kecamatan" value="{{ $response['data']->kd_kecamatan }}">
                                            <input type="hidden" name="kd_kelurahan" id="kd_kelurahan" value="{{ $response['data']->kd_kelurahan }}">
                                            <input type="hidden" name="kd_blok" id="kd_blok" value="{{ $response['data']->kd_blok }}">
                                            <input type="hidden" name="no_urut" id="no_urut" value="{{ $response['data']->no_urut }}">
                                            <input type="hidden" name="kd_jns_op" id="kd_jns_op" value="{{ $response['data']->kd_jns_op }}">
                                            <input type="hidden" name="luas_bumi" id="luas_bumi" value="{{ $response['data']->luas_bumi_sppt }}">
                                            <input type="hidden" name="luas_bng" id="luas_bng" value="{{ $response['data']->luas_bng_sppt }}">

                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <p>Mengajukan Pembetulan data subjek/wajib pajak atas NOP:</p>
                                            </div>
                                            <div class="col-6">
                                                <table class="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>NOP</td>
                                                            <td>:
                                                                {{ $response['data']->kd_propinsi}}.{{ $response['data']->kd_dati2}}.{{ $response['data']->kd_kecamatan}}.{{ $response['data']->kd_kelurahan}}.{{ $response['data']->kd_blok}}-{{ $response['data']->no_urut}}.{{ $response['data']->kd_jns_op}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Luas Bumi</td>
                                                            <td>: {{ angka($response['data']->luas_bumi_sppt) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Luas Bangunan</td>
                                                            <td>: {{ angka($response['data']->luas_bng_sppt) }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-6">

                                                <div class="form-group">
                                                    <label for="scan_ktp">Scan KTP</label>

                                                    <input type="file" class="form-control form-control-file" id="scan_ktp" name="scan_ktp">

                                                </div>
                                                <div class="form-group">
                                                    <label for="bukti_kepemilikan">Bukti Kepemilikan</label>

                                                    <input type="file" class="form-control form-control-file" id="bukti_kepemilikan" name="bukti_kepemilikan">

                                                </div>
                                                <div class="form-group">
                                                    <label for="selfie_ktp">Selfie KTP</label>
                                                    <input type="file" class="form-control form-control-file" id="selfie_ktp" name="selfie_ktp">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="float-right">
                                            <button class="btn btn-sm btn-success"><i class="fas fa-save"></i> Ajukan</button>
                                        </div>
                                    </div>
                                </form>
                            </div>


                        </div>
                        @endif


                        @else
                        {{-- <i class="fas fa-exclamation-triangle text-danger"></i> NOP tidak terdaftar dalam database BAPENDA --}}
                        <ol>
                            @foreach ($response['keterangan'] as $ket)
                            <li><?=  $ket ?></li>
                            @endforeach
                        </ol>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('lte') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{ asset('lte') }}/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{ asset('js') }}/wilayah.js"></script>
<script src="{{ asset('sweetalert2') }}/dist/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {

        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        $('input').each(function() {
            $(this).val($(this).val().toUpperCase())
        })

        $('#kembali').on('click', function() {
            $('#hasil').html('');
            document.getElementById("form").reset();
            $('#form').show();
            $('#otp').hide();
        });
        $('#cetak').on('click', function(e) {
            e.preventDefault();
            var url = $('#cetak').data('href');
            // alert(url);
            openTab(url);
        });

        // persetujuan form perbaikan 
        $('#div_form').hide()
        $('input[type="checkbox"]').click(function() {
            if ($(this).is(":checked")) {
                $('#div_form').show()
                // $("#result").html("Checkbox is checked.");
            } else if ($(this).is(":not(:checked)")) {
                $('#div_form').hide()
            }
        });
        let datatable=$("#main-table").DataTable({
            ordering: false,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false },
                { data: 'nomor_layanan',class:'w-15'},
                { data: 'jenis_layanan_nama',class:'w-20'},
                { data: 'nama'},
                { data: 'created_at',class:'w-15 text-center'},
                { data: 'kd_status',class:'text-center w-10'},
                { data: 'option',class:'text-center w-10'}
            ]
        });
        $(document).on("change", "[type='file']", function(evt) {
            filename = $(this).val().split('\\').pop();
            la = filename.split('.');
            vexe = ['jpeg', 'jpg', 'png', 'pdf'];
            exe = la[la.length - 1];
            status = '0';
            for (var i = 0; i < vexe.length; i++) {
                var name = vexe[i];
                if (name == exe.toLowerCase()) {
                    status = '1';
                    break;
                }
            }
            if (status == '0') {
                $(this).val('');
                // alert("File tidak di perbolehkan");
                Swal.fire({
                    icon: "warning"
                    , title: "Peringatan"
                    , text: "File tidak di perbolehkan"
                    , type: "warning"
                    , timer: 2000
                });
            }
        });

        $.validator.addMethod(
            "angkaRegex"
            , function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
            }
            , "Harus di isi dengan angka."
        );
        $("#form-perbaikan").submit(function() {
            var form = $("#form-perbaikan");
            form.validate({
                errorElement: "span"
                , errorPlacement: function(error, element) {
                    error.addClass("invalid-feedback");
                    element.closest(".form-group").append(error);
                }
                , highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid");
                }
                , unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                }
                , rules: {
                    nik: {
                        required: true

                        , digits: true
                        , angkaRegex: true
                        , minlength: 16
                        , maxlength: 16
                    }
                    , nama: {
                        required: true
                        , maxlength: 30
                    }
                    , nomor_telepon: {
                        required: true
                        , digits: true
                        , angkaRegex: true
                    }
                    , alamat: {
                        required: true
                    }
                    , blok_kav_no: {
                        required: false
                    }
                    , kelurahan: {
                        required: true
                    }
                    , kecamatan: {
                        required: true
                    }
                    , dati2: {
                        required: true
                    }
                    , propinsi: {
                        required: true
                    }
                    , scan_ktp: {
                        required: true
                    }
                    , bukti_kepemilikan: {
                        required: true
                    }
                    , selfie_ktp: {
                        required: true
                    }

                }


            });

            console.log('Form submit :' + form.valid());

            if (form.valid() === false) {
                return false;
            } else {
                $.ajax({
                    type: "POST"
                    , url: "{{ url('mandiri/pembetulan-subjek') }}"
                    , data: new FormData(this)
                    , processData: false
                    , contentType: false
                    , success: function(res) {
                        console.log(res.status)
                        if (res.status == true) {

                            Swal.fire({
                                icon: "success"
                                , title: "Berhasil"
                                , text: "Permohonan berhasil di ajukan , selanjutnya akan di verifikasi oleh Petugas dengan estimasi 10 hari kerja."

                            }).then((result) => {
                                    document.location.href ="{{ url('page-sppt') }}"
                                
                            })

                            $('#hasil').html('');
                            document.getElementById("form").reset();
                            $('#form').show();
                            $('#otp').hide();
                        } else {
                            Swal.fire({
                                icon: "warning"
                                , title: "Gagal"
                                , text: "Gagal melakukan proses permohonan pembetulan"
                                    // , type: "warning"
                                , timer: 2000
                            }).then((result) => {
                                    document.location.href ="{{ url('page-sppt') }}"
                                
                            })
                            
                        }
                    }
                    , error: function(res) {
                        console.log(res)
                        Swal.fire({
                            icon: "warning"
                            , title: "Peringatan"
                            , text: "Sistem tidak berjalan"
                            , type: "warning"
                            , timer: 2000
                        });
                    }
                })
                return false;
                // $.post("{{ url('mandiri/pembetulan-subjek') }}", data).done(function());
            }
        });

    })

    function openTab(url) {
        window.open(url
            , 'newwindow'
            , 'width=700,height=1000');
        return false;
    }

</script>
@endsection
