<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E - SSPD PBB P2</title>
</head>

<body>
    @foreach ($pembayaran as $item)
    <div class="print-page">

        <div class='container text-center'>
            <table width="100%">
                <tr>
                    <td width="20%"></td>
                    <td width="60%" align="center" style="font-size: 10pt;">
                        <div class="header">
                            <h3>SURAT SETORAN PAJAK DAERAH (SSPD)</h3>
                            <h3>PAJAK BUMI DAN BANGUNAN</h3>
                        </div>
                    </td>
                    <td width="20%" align='right'>
                        <img src="data:image/png;base64,{!!$qrcode!!}" class="qrcode" width="80px">
                    </td>
                </tr>
            </table>
        </div>
        <hr>
        <div class="container border-bottom" width="100%">
            <table class="table" width="100%" style="font-size: 9pt;">
                <tr>
                    <td>NTPD</td>
                    <td>:</td>
                    <td>{{$item->pengesahan}}</td>
                </tr>
                <tr>
                    <td>BANK BAYAR</td>
                    <td>:</td>
                    <td>{{$item->nama_bank}}</td>
                </tr>
                <tr>
                    <td>NOP</td>
                    <td>:</td>
                    <td>{{
                            $item->nop
                        }}</td>
                </tr>
                <tr>
                    <td>TAHUN PAJAK</td>
                    <td>:</td>
                    <td>{{$item->thn_pajak_sppt}}</td>
                </tr>
                <tr>
                    <td>NAMA WP</td>
                    <td>:</td>
                    <td>{{$item->nm_wp_sppt}}</td>
                </tr>
                <tr>
                    <td>ALAMAT</td>
                    <td>:</td>
                    <td>{{$item->jalan_op }} {{ $item->blok_kav_no_op }}</td>
                <tr>
                    <td>KELURAHAN</td>
                    <td>:</td>
                    <td>{{$item->nm_kelurahan}}</td>
                </tr>
                <tr>
                    <td>KECAMATAN</td>
                    <td>:</td>
                    <td>{{$item->nm_kecamatan}}</td>
                </tr>
                <tr>
                    <td>TGL PEMBAYARAN</td>
                    <td>:</td>
                    <td>{{tglIndo($item->tgl_pembayaran_sppt)}}</td>
                </tr>
                <tr>
                    <td>JUMLAH</td>
                    <td>:</td>
                    <td>{{number_format($item->jml_sppt_yg_dibayar - $item->denda_sppt,0,',','.')}}</td>
                </tr>
                <tr>
                    <td>DENDA</td>
                    <td>:</td>
                    <td>{{number_format($item->denda_sppt,0,',','.')}}</td>
                </tr>
                <tr>
                    <td>TOTAL</td>
                    <td>:</td>
                    <td>{{number_format($item->jml_sppt_yg_dibayar,0,',','.')}}</td>
                </tr>
                <tr>
                    <td>TERBILANG</td>
                    <td>:</td>
                    <td>{{terbilang_cap($item->jml_sppt_yg_dibayar)}} Rupiah</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td>KODE BILLING KOLEKTIF</td>
                    <td>:</td>
                    <td>{{ $item->kobil }} / {{ $item->tahun_pajak}}</td>
                </tr>
                <tr>
                    <td>KETERANGAN</td>
                    <td>:</td>
                    <td>-</td>
                </tr>
            </table>
        </div>
        <br>
        <div class="container" width="100%">
            <span style="font-size: 9pt;"><u><b>CATATAN :</b></u></span>
            <ol width="100%" style="font-size: 9pt; padding:0px 0px 0px 20px;margin:0px">
                <li>Dokumen ini merupakan pembayaran yang SAH PBB P2 Bapenda Kab. Malang</li>
                <li>Untuk pengecekan, silahkan scan QRCODE diatas</li>
            </ol>
        </div>
    </div>
    @endforeach
</body>

</html>
