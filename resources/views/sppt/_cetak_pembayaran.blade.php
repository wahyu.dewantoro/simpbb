<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Tanda Terima Pembayaran</title>
</head>
<body>
    
    @foreach($riwayat as $item)
        @php
            $nop=$item->kd_propinsi.".".
                                $item->kd_dati2.".".
                                $item->kd_kecamatan.".".
                                $item->kd_kelurahan.".".
                                $item->kd_blok."-".
                                $item->no_urut.".".
                                $item->kd_jns_op;
            $nopF=$item->kd_propinsi.$item->kd_dati2.$item->kd_kecamatan.$item->kd_kelurahan.$item->kd_blok.$item->no_urut.$item->kd_jns_op;
            $qrcode=$uri.'/'.$nopF.$item->thn_pajak_sppt;
            $qrcode=base64_encode(QrCode::format('svg')->generate($qrcode));
        @endphp
        <div class="print-page">
            <div style="text-align: left"><span style="font-size: 8pt; "><b>[Halaman 1 dari 1]</b></span></div>    
            <div class='container text-center'>
                <table width="100%">
                    <tr>
                        <td width="20%"></td>
                        <td width="60%" align="center" style="font-size: 10pt;">
                            <div class="header">
                                <h3>SURAT SETORAN PAJAK DAERAH (SSPD)</h3>
                                <h3>PAJAK BUMI DAN BANGUNAN</h3>
                            </div>
                        </td>
                        <td width="20%" align='right'><img src="data:image/png;base64,{!!$qrcode!!}" class="qrcode" width="80px"></td>
                    </tr>
                </table>
            </div>
            <hr>
            <div class="container border-bottom" width="100%" >
                <table class="table" width="100%" style="font-size: 9pt;">
                    {{--<tr>
                        <td>NTPD</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>--}}
                    <tr>
                        <td>BANK BAYAR</td>
                        <td>:</td>
                        <td>BANK JATIM</td>
                    </tr>
                    <tr>
                        <td>NOP</td>
                        <td>:</td>
                        <td>{{$nop}}</td>
                    </tr>
                    <tr>
                        <td>TAHUN PAJAK</td>
                        <td>:</td>
                        <td>{{$item->thn_pajak_sppt}}</td>
                    </tr>
                    <tr>
                        <td>NAMA WP</td>
                        <td>:</td>
                        <td>{{$item->nm_wp_sppt}}</td>
                    </tr>
                    {{--<tr>
                        <td>ALAMAT</td>
                        <td>:</td>
                        <td></td>
                    <tr>--}}
                        <td>KELURAHAN</td>
                        <td>:</td>
                        <td>{{$item->nm_kelurahan}}</td>
                    </tr>
                    <tr>
                        <td>KECAMATAN</td>
                        <td>:</td>
                        <td>{{$item->nm_kecamatan}}</td>
                    </tr>
                    <tr>
                        <td>TGL PEMBAYARAN</td>
                        <td>:</td>
                        <td>{{tglIndo($item->tgl_pembayaran_sppt)}}</td>
                    </tr>
                    <tr>
                        <td>JUMLAH</td>
                        <td>:</td>
                        <td>{{$item->pokok_bayar}}</td>
                    </tr>
                    <tr>
                        <td>DENDA</td>
                        <td>:</td>
                        <td>{{$item->denda_bayar}}</td>
                    </tr>
                    <tr>
                        <td>TOTAL</td>
                        <td>:</td>
                        <td>{{$item->total_bayar}}</td>
                    </tr>
                    <tr>
                        <td>TERBILANG</td>
                        <td>:</td>
                        <td>{{terbilang_cap($item->total_bayar)}} Rupiah</td>
                    </tr>
                    {{--<tr>
                        <td colspan="3"><hr></td>
                    </tr>
                    <tr>
                        <td>KODE BILLING KOLEKTIF</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>KETERANGAN</td>
                        <td>:</td>
                        <td></td>
                    </tr>--}}
                </table>
            </div>
            <br>
            <div class="container" width="100%" >
                 <span style="font-size: 9pt;"><u><b>CATATAN :</b></u></span>
                <ol width="100%" style="font-size: 9pt; padding:0px 0px 0px 20px;margin:0px">
                    <li>Dokumen ini merupakan bukti tanda terima pembayaran Bapenda Kab. Malang</li>
                    <li>Untuk pengecekan, silahkan scan QRCODE diatas</li>
                </ol>
            </div>
        </div>
        <br>
    @endforeach
</body>
</html>

