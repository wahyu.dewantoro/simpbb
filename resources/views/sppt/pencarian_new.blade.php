@extends('sppt.core_pencarian')

@section('content')
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card card-outline card-info">
            <div class="card-body">
                <form action="{{ url('cari-sppt') }}" id="form" class="form-horizontal" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="nop" class="col-sm-3 col-form-label">NOP</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm angka" id="nop" name="nop" value="35.07" placeholder="Nomor objek pajak">
                            <span class="text-danger">{{ $errors->first('nop') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nik" class="col-sm-3 col-form-label">NIK</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm angka" maxlength="16" id="nik" name="nik" placeholder="Nomor induk kependudukan">
                            <span class="text-danger">{{ $errors->first('nik') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="Nama" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="nama" name="nama" placeholder="Nama subjek pajak">
                            <span class="text-danger">{{ $errors->first('nama') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        {{-- <label for="Nama" class="col-sm-3 col-form-label">Nama</label> --}}
                        <div class="col-sm-9 offset-sm-3">
                            @php
                            echo captcha_img();
                            @endphp
                            <input type="text" name="captcha" class="form-control form-control-sm form-control form-control-sm-sm" id="captcha" placeholder="Masukan kode di atas">
                            <span class="text-danger">{{ $errors->first('captcha') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 offset-sm-3">
                            <button class="btn btn-sm btn-flat btn-info" type="submit">Cek data</button>
                        </div>
                    </div>
                    
                </form>
                <div id="hasil"></div>
            </div>
        </div>
    </div>
</div>
@endsection
