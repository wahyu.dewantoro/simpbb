@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>SPPT</h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">

                        <div class="card-body">
                            {{-- <form action="#" method="get"> --}}
                            <div class="row">
                                <div class="col-md-1">
                                    <input value="{{ request()->get('tahun_pajak') ?? date('Y') }}" type="text"
                                        name="tahun_pajak" id="tahun_pajak"
                                        class="form-control form-control-border form-control-sm {{ $errors->has('tahun_pajak') ? 'is-invalid' : '' }}">
                                    <span class="errorr invalid-feedback">{{ $errors->first('tahun_pajak') }}</span>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="nop" id="nop" value="{{ request()->get('nop') ?? '' }}"
                                        class="form-control form-control-border form-control-sm {{ $errors->has('nop') ? 'is-invalid' : '' }}"
                                        placeholder="Masukan nomor objek pajak (NOP)" autofocus>
                                    <span class="errorr invalid-feedback">{{ $errors->first('nop') }}</span>
                                </div>
                                <div class="col-md-2">
                                    <button id="cek" type="button" class="btn btn-sm btn-primary"> <i
                                            class="fas fa-search"></i> </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="hasil"></div>
                                </div>
                            </div>
                            {{-- </form> --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).on({
            ajaxStart: function() {
                openloading();
            },
            ajaxStop: function() {
                closeloading();
            }
        });
        
        $(document).ready(function() {
            $('#nop').trigger('keyup');

            $(document).on('keypress', function(e) {
                if (e.which == 13) {
                    // alert('You pressed enter!');
                    $('#cek').trigger('click');
                }
            });

            $('#nop').on('keyup', function() {
                var nop = $(this).val();
                var convert = formatnop(nop);
                $(this).val(convert);
            });

            $('#cek').on('click', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Sedang mencari ...',
                    html: '<div class="fa-3x pd-5"><i class="fa fa-spinner fa-pulse"></i></div>',
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
                $.ajax({
                    url: "{{ url('informasi/sppt') }}",
                    data: {
                        tahun_pajak: $('#tahun_pajak').val(),
                        nop: $('#nop').val()
                    },
                    success: function(res) {
                        Swal.close()
                        $('#hasil').html(res);
                        $('#cetak').on('click', function(e) {
                            e.preventDefault();
                            var url=$('#cetak').data('href');
                            // alert(url);
                            openTab(url);
                        });
                    },
                    error: function(e) {
                        Swal.close()
                        $('#hasil').html('Maaf, ada kesalahan. Yuk, coba lagi! Kalau terus mengalami masalah, segera kontak pengelola sistem.')
                    }
                });
            });
        });
    </script>
@endsection
