<html>
<head>
    <style>
        body {
            font-size: 11.0pt;
            font-family: "Calibri", sans-serif;
            margin: 10px;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            background-image: url("{{ public_path('esppt_copy.jpeg') }}");
            background-repeat: no-repeat;
        }

        p {
            font-family: "Calibri", sans-serif;
        }

        #watermark {
            position: fixed;
            opacity: 0.2;
            /* margin: 0; */
        }

        @page {
            /* size: 180mm 188mm; */
            margin: 10px;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif
        }

        .konten {
            /* position: fixed; */
            margin-top: 15mm;
            margin-left: 7mm;
            margin-right: 18mm;
        }

        #tahun_pajak {
            position: fixed;
            margin-top: 40mm;
            margin-left: 152mm;
            font-weight: bold;
            font-size: 30px;
        }

    </style>
    <title>
        {{ $sppt->kd_propinsi . $sppt->kd_dati2 . $sppt->kd_kecamatan . $sppt->kd_kelurahan . $sppt->kd_blok . $sppt->no_urut . $sppt->kd_jns_op.' '.$sppt->thn_pajak_sppt }}
    </title>
</head>
<body>

    <div id="tahun_pajak">{{ $sppt->thn_pajak_sppt }}</div>
    <div class="konten">
        <p style="  text-align: center; margin-top: 2mm; font-weight:bold; font-size: 15px; margin-bottom:3px; ">SURAT PEMBERITAHUAN PAJAK TERHUTANG<BR>PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN</p>
        <span style="font-weight: bold; 	float: left; font-size: 12px ">NOP : {{ $sppt->kd_propinsi }}.{{ $sppt->kd_dati2 }}.{{ $sppt->kd_kecamatan }}.{{ $sppt->kd_kelurahan }}.{{ $sppt->kd_blok }}-{{ $sppt->no_urut }}.{{ $sppt->kd_jns_op }} </span>
        {{-- @if($) --}}
        @if($objek->kd_status_wp<>1)


            <span style="font-weight: bold;  	float: right; font-size: 12px ">STATUS : {{ statusWP($objek->kd_status_wp) }}</span>
            @endif
            <table style=" table-layout: fixed; width: 100%; margin-top: 15px;" cellspacing='0' cellpadding='5' border="0">
                <tr style="vertical-align: top">
                    <td style="width: 38%">
                        <p style=" margin-top:0; margin-bottom: 2px; font-weight: bold; font-size: 12px;">LETAK OBJEK PAJAK</p>
                        <p style="font-size: 9pt">
                            @if (trim($objek->jalan_op)!='' || trim($objek->blok_kav_no_op)!='')
                            {{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }}<br>
                            @endif
                            @php
                            $rto=trim($objek->rt_op) != '' ? 'RT :' . $objek->rt_op : '';
                            $rwo=trim($objek->rw_op) != '' ? 'RW :' . $objek->rw_op : '';
                            @endphp
                            @if ($rwo!='' || $rto!='')
                            {{ $rto.' '.$rwo}} <br>
                            @endif
                            {{ $objek->nm_kelurahan }} <br>{{ $objek->nm_kecamatan }} <br> KABUPATEN MALANG
                        </p>
                    </td>
                    <td style="width: 38%">
                        <p style=" margin-top:0; margin-bottom: 2px; font-weight: bold; font-size: 12px; ">NAMA DAN ALAMAT WAJIB PAJAK</p>
                        <p style="font-size: 9pt">
                            {{ $sppt->nm_wp_sppt }}<br>
                            @if (trim($objek->jalan_op)!='' || trim($objek->blok_kav_no_op)!='')
                            {{ $objek->jalan_op }} {{ $objek->blok_kav_no_op }}<br>
                            @endif

                            {{-- @if ($sppt->rw_wp_sppt != '' && $sppt->rw_wp_sppt != '')
                        {{ $sppt->rw_wp_sppt != '' ? 'RT :' . $sppt->rt_wp_sppt : '' }}
                            {{ $sppt->rt_wp_sppt != '' ? 'RW :' . $sppt->rw_wp_sppt : '' }}
                            <br>
                            @endif --}}
                            @php
                            $rtp=trim($sppt->rt_wp_sppt) != '' ? 'RT :' . $sppt->rt_wp_sppt : '';
                            $rwp=trim($sppt->rw_wp_sppt) != '' ? 'RW :' . $sppt->rw_wp_sppt : '';
                            @endphp
                            @if ($rwp!='' || $rtp!='')
                            {{ $rtp.' '.$rwp}} <br>
                            @endif

                            @if ($sppt->kelurahan_wp_sppt != '')
                            {{ $sppt->kelurahan_wp_sppt }}<br>
                            @endif
                            @if ( $sppt->kota_wp_sppt != '')
                            {{ $sppt->kota_wp_sppt }}
                            @endif
                            {{-- ACH SUGENG<br>
                        Dsn Pagutan RT 011 RW 04<br>
                        Girimulyo<br>
                        Kabupaten Malang</p> --}}

                    </td>
                    <td style="width: 25%; text-align: center; vertical-align: middle ">
                        <p style="font-size: 25px; font-weight: bold; color:white"> &nbsp;</p>
                    </td>
                </tr>
            </table>

            <style>
                .table-bordered>tbody>tr>td,
                .table-bordered>thead>tr>td,
                .table-bordered {
                    border: none;
                    border-collapse: collapse;
                    border-right: solid 1px black;
                    border-left: solid 1px black;
                }

            </style>
            <table style="font-size: 12px;  table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1' class="table-bordered" padding='3'>
                <thead>
                    <tr style="text-align: center; vertical-align: top;font-weight: bold; ">
                        <td style="border: solid 1px black;">OBJEK PAJAK</td>
                        <td style="border: solid 1px black;">LUAS M<sup>2</sup></td>
                        <td style="border: solid 1px black;">KELAS</td>
                        <td style="border: solid 1px black;">NJOP PER M<sup>2</sup> (Rp)</td>
                        <td style="border: solid 1px black;">TOTAL NJOP (Rp)</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style=" border-bottom: 0;">BUMI</td>
                        <td style="text-align: right; border-bottom: 0;">{{ number_format($sppt->luas_bumi_sppt, 0, '', '.') }}</td>
                        <td style="text-align: center;  border-bottom: 0;; border-bottom-color:white; ">{{ $sppt->kd_kls_tanah }}</td>
                        <td style="text-align: right;  border-bottom: 0;; border-bottom-color:white; ">{{ $sppt->luas_bumi_sppt>0?number_format($sppt->njop_bumi_sppt / $sppt->luas_bumi_sppt, 0, '', '.'):0 }}</td>
                        <td style="text-align: right;  border-bottom: 0;; border-bottom-color:white; ">{{ number_format($sppt->njop_bumi_sppt, 0, '', '.') }}</td>
                    </tr>
                    <tr>
                        <td style="border-bottom: solid 1px black;">BANGUNAN</td>
                        <td style="border-bottom: solid 1px black;  text-align: right">{{ number_format($sppt->luas_bng_sppt, 0, '', '.') }}</td>
                        <td style="border-bottom: solid 1px black;  text-align: center">{{ $sppt->kd_kls_bng }}</td>
                        <td style="border-bottom: solid 1px black;  text-align: right">{{ $sppt->luas_bng_sppt > 0 ? number_format($sppt->njop_bng_sppt / $sppt->luas_bng_sppt, 0, '', '.') : null }}</td>
                        <td style="border-bottom: solid 1px black;  text-align: right">{{ number_format($sppt->njop_bng_sppt, 0, '', '.') }}</td>
                    </tr>
                </tbody>
            </table>
            <table style="font-size: 12px;  border-left: solid 1px black; border-top: 0; border-right: solid 1px black; border-bottom: solid 1px black; table-layout: fixed; width: 100%;" cellspacing='0' cellpadding='1'>
                <tr>
                    <td>NJOP Sebagai dasar pengenaan PBB P2</td>
                    <td style="text-align: left" width="10px">=</td>
                    <td style="text-align: right">{{ number_format($sppt->njop_bumi_sppt + $sppt->njop_bng_sppt, 0, ',', '.') }}</td>
                    </td>
                </tr>
                <tr>
                    <td>NJOPTKP (NJOP Tidak Kena Pajak)</td>
                    <td style="text-align: left" width="10px">=</td>
                    <td style="text-align: right">{{ number_format($sppt->njoptkp_sppt, 0, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>NJOP untuk perhitungan PBB P2</td>
                    <td style="text-align: left" width="10px">=</td>
                    <td style="text-align: right">
                        @php
                        $njop_sppt= $sppt->njop_bumi_sppt + $sppt->njop_bng_sppt - $sppt->njoptkp_sppt;
                        @endphp

                        {{ number_format($njop_sppt , 0, ',', '.') }}
                    </td>
                </tr>
                <tr>
                    <td>PBB P2 yang Terhutang</td>
                    <td style="text-align: left" width="10px">=</td>
                    <td style="text-align: right"> <?= $sppt->tarif*100 ?> % x
                        {{ number_format($njop_sppt  , 0, ',', '.') }} =
                        {{ number_format($sppt->pbb_terhutang_sppt, 0, ',', '.') }}</td>
                </tr>
                @if($sppt->pbb_terhutang_sppt <$sppt->pbb_yg_harus_dibayar_sppt)
                    <tr>
                        <td>Ketetapan PBB</td>
                        <td style="text-align: left" width="10px">=</td>
                        <td style="text-align: right">
                            {{ number_format($sppt->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}

                        </td>
                    </tr>
                    @endif
                    @if( $sppt->nilai_potongan>0)
                    <tr>
                        <td>Potongan</td>
                        <td style="text-align: left" width="10px">=</td>
                        <td style="text-align: right">
                            {{ angka($sppt->nilai_potongan) }}</td>
                    </tr>
                    @endif
                    <tr style="border-top: solid 1px black">
                        <td colspan="2" style=" font-size: 10.5px">PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN YANG HARUS DIBAYAR </td>
                        <td style="text-align: right">Rp. {{ number_format($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan, 0, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><i style="font-weight: bold; font-size: 10.5px">{{ strtoupper(terbilang($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan)) }} </i></td>
                    </tr>
                    <tr style="border-top: solid 1px black">
                        <td colspan="3" style="font-weight: bold">TUNGGAKAN</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold">{{ $tunggakan<>''? $tunggakan:' - '  }}</td>
                    </tr>
            </table>


            {{-- border-left : solid 1px black;   border-top:solid 1px black; border-right: solid 1px black; border-bottom: solid 1px black; --}}

    </div>
    {{-- border-left: solid 1px black; border-top: 0; border-right: solid 1px white; border-bottom: solid 1px black; --}}
    <table style="position: fixed; 
                margin-left: 10mm;
                
            margin-top:130mm;
     font-size: 12px; " cellspacing='0' border='0' cellpadding='1'>
        <tr style="vertical-align: top">
            <td style="width: 420px">
                TGL. JATUH TEMPO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ tglIndo($sppt->tgl_jatuh_tempo_sppt) }}<br>
                TEMPAT PEMBAYARAN :<br>
                {{ $tp->nama_tempat }}

            </td>
            {{-- border-left: solid 1px black; --}}
            <td style="text-align: center; vertical-align: top; ">
                <p style="margin: 0">
                    MALANG, {{ tglIndo($sppt->tgl_terbit_sppt) }}<br>
                    {{ $kaban->is_plt != '' ? 'PLT' : '' }} {{ $kaban->jabatan }}<br>
                    <img src="data:image/png;base64, {!! $qrcode !!}">
                    <br>
                    <b>{{ $kaban->nama }}</b><br>NIP : {{ $kaban->nip }}
                </p>
            </td>
        </tr>
    </table>
    
</body>
</html>
