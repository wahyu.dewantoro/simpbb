<html>

<head>
    <title>E - SPPT</title>

    <style>
        body {
            font-family: "Calibri (Body)";
            font-size: 1em;
            /* background-color:#f6d5c1; */
        }

        table {

            font-family: "Calibri (Body)";
            font-size: 0.9 em;

        }

        .watermark {
            position: fixed;
            top: 80mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

        #pojokkananatas {
            position: fixed;
            top: 26mm;
            width: 100%;
            right: 5mm;
            text-align: right;
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            font-size: 1em !important;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 1em !important;
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 1em !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 0.7em !important;
        }

        .text-sedang {
            font-size: 1em !important;
        }

        .page-number:before {
            content: "Page "counter(page)
        }

        .inti {
            border: 1px solid black;
            margin: auto;
        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        .inti td {
            border-right: 1px solid black;
            padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;
            padding-top: 2px;
        }

        .inti th {
            border-right: 1px solid black;
            border-bottom: 1px solid black;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            /* background-color:grey; */
            color: black;
            text-align: center;
            line-height: 0.5cm;
            font-size: small;
        }

    </style>
</head>
<body>
    <div class="watermark"><img src="{{ public_path('kabmalang.png') }}"></div>
    <div id="pojokkananatas">
        <span style="font-size: 2em">{{ $sppt->thn_pajak_sppt }}</span>
    </div>
    <table width="100%" class="">
        <tr>
            <th width="70px">
                <img width="60px" height="70px" src="{{ public_path('kabmalang.png') }}">
            </th>
            <th>
                <P>PEMERINTAH KABUPATEN MALANG<br>BADAN PENDAPATAN DAERAH<br>
                    <small>Jl. Raden Panji Nomor 158 Kepanjen Telp. (0341) 3904898</small><br> K E P A N J E N - 65163
                </P>
            </th>
            <th width="100px">
                {!! QrCode::size(70)->errorCorrection('M')->generate($url) !!}
            </th>
        </tr>
    </table>
    <hr>
    <p class="text-tengah">SURAT PEMBERITAHUAN PAJAK TERUTANG ELEKTRONIK (E-SPPT)<br>
        PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN </p>
    <b>NOP :</b>
    {{ $sppt->kd_propinsi }}.{{ $sppt->kd_dati2 }}.{{ $sppt->kd_kecamatan }}.{{ $sppt->kd_kelurahan }}.{{ $sppt->kd_blok }}-{{ $sppt->no_urut }}.{{ $sppt->kd_jns_op }}
    <table width="100%" class="table-sm table table-bordered">
        <tbody>
            <tr style="vertical-align: top">
                <td style="text-align: left;width:50%;padding:15px">
                    <p class="text-tengah"><b>Alamat Objek Pajak</b></p>
                    {{ $objek->jalan_op }}
                    {{ $objek->blok_kav_no_op }} <br>
                    {{ $objek->rt_op != '' ? 'RT :' . $objek->rt_op : '' }}
                    {{ $objek->rw_op != '' ? 'RW :' . $objek->rw_op : '' }}
                    <br>
                    {{ $objek->nm_kelurahan }} <br>{{ $objek->nm_kecamatan }} <br> KABUPATEN MALANG
                </td>
                <td style="text-align: left;width:50%;padding:15px">
                    <p class="text-tengah"><b>Identitas dan Alamat Subjek Pajak</b></p>
                    <?= strlen($sppt->nik)==16?$sppt->nik.'<br>':'' ?>
                    {{ $sppt->nm_wp_sppt }}<br>
                    {{ $sppt->jln_wp_sppt }} {{ $sppt->blok_kav_no_wp_sppt }}<br>

                    @if ($sppt->rw_wp_sppt != '' && $sppt->rw_wp_sppt != '')
                    {{ $sppt->rw_wp_sppt != '' ? 'RT :' . $sppt->rt_wp_sppt : '' }}
                    {{ $sppt->rt_wp_sppt != '' ? 'RW :' . $sppt->rw_wp_sppt : '' }}
                    <br>
                    @endif
                    @if ($sppt->kelurahan_wp_sppt != '' || $sppt->kota_wp_sppt != '')
                    {{ $sppt->kelurahan_wp_sppt }}<br> {{ $sppt->kota_wp_sppt }}
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
    <table class="inti text-sedang">
        <thead>
            <tr>
                <th style="text-align: center">Objek</th>
                <th style="text-align: center">Luas (M<sup>2</sup>)</th>
                <th style="text-align: center">Kelas</th>
                <th style="text-align: center">NJOP / (M<sup>2</sup>)</th>
                <th style="text-align: center">Total</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-size: 1em">Bumi</td>
                <td style="font-size: 1em; text-align:center">{{ number_format($sppt->luas_bumi_sppt, 0, '', '.') }}
                </td>
                <td style="font-size: 1em; text-align:center">{{ $sppt->kd_kls_tanah }}</td>
                <td style="font-size: 1em; text-align:right">
                    {{ number_format($sppt->njop_bumi_sppt / $sppt->luas_bumi_sppt, 0, '', '.') }} </td>
                <td style="font-size: 1em; text-align:right"> {{ number_format($sppt->njop_bumi_sppt, 0, '', '.') }}
                </td>
            </tr>
            <tr>
                <td style="font-size: 1em">Bangunan</td>
                <td style="font-size: 1em; text-align:center">{{ number_format($sppt->luas_bng_sppt, 0, '', '.') }}
                </td>
                <td style="font-size: 1em; text-align:center">{{ $sppt->kd_kls_bng }}</td>
                <td style="font-size: 1em; text-align:right">
                    {{ $sppt->luas_bng_sppt > 0 ? number_format($sppt->njop_bng_sppt / $sppt->luas_bng_sppt, 0, '', '.') : null }}
                </td>
                <td style="font-size: 1em;text-align:right"> {{ number_format($sppt->njop_bng_sppt, 0, '', '.') }}
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table table-sm " width="100%">

        <tr>
            <td style="width:60%"><b>NJOP</b></td>
            <td style="width: 10px">=</td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;">
                {{ number_format($sppt->njop_bumi_sppt + $sppt->njop_bng_sppt, 0, ',', '.') }}</td>
        </tr>
        <tr>
            <td><b>NJOPTKP</b></td>
            <td style="width: 10px">=</td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;">{{ number_format($sppt->njoptkp_sppt, 0, ',', '.') }}</td>
        </tr>
        <tr>
            <td><b>NJOP PBB P2</b></td>
            <td style="width: 10px">=</td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;">
                {{ number_format($sppt->njop_sppt, 0, ',', '.') }}</td>
        </tr>
        <tr>
            <td><b>PBB Terutang </b></td>
            <td style="width: 10px">=</td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;"> {{ $sppt->tarif }} % x
                {{ number_format($sppt->njop_sppt, 0, ',', '.') }} =
                {{ number_format($sppt->pbb_terhutang_sppt, 0, ',', '.') }}
            </td>
        </tr>
        <tr>
            <td><b>Ketetapan PBB</b></td>
            <td style="width: 10px">=</td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;">
                {{ number_format($sppt->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}
            </td>
        </tr>
        <tr>
            <td><b style="color: red">Pengurangan </b></td>
            <td style="width: 10px">=</td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px; color:red">
                @php
                // $selisih=$sppt->pbb_yg_harus_dibayar_sppt - ($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan)
                @endphp
                {{-- {{ $selisih }} --}}

                {{ number_format(($sppt->pbb_terhutang_sppt - ($sppt->pbb_yg_harus_dibayar_sppt - $sppt->nilai_potongan))<0?0: ($sppt->pbb_yg_harus_dibayar_sppt - ($sppt->pbb_yg_harus_dibayar_sppt - $sppt->nilai_potongan)), 0, ',', '.') }}
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td colspan="2">
                <b>PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN YANG HARUS DI
                    BAYAR</b> <br>
                <i>{{ ucwords(terbilang($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan)) }} Rupiah</i>
            </td>
            <td style="text-align: right;padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;">
                <b>{{ number_format($sppt->pbb_yg_harus_dibayar_sppt-$sppt->nilai_potongan, 0, ',', '.') }}</b>
            </td>
        </tr>

    </table>

    <table width="100%" class="" style="border-collapse:collapse;border:1px solid #212529;padding:3px;">
        <tbody>
            <tr style="vertical-align: top ">
                <td width="50%" align="center" style="border-collapse:collapse;border:1px solid #212529;padding:3px;">
                    <table width="100%" style="border: 0px;">
                        <tbody>
                            <tr style="vertical-align: top">
                                <td width="32%">Tanggal Jatuh Tempo</td>
                                <td width="1px">:</td>
                                <td>{{ tglIndo($sppt->tgl_jatuh_tempo_sppt) }}</td>
                            </tr>
                            @if ($tunggakan != '')
                            <tr style="vertical-align: top">
                                <td>Tunggakan PBB P2</td>
                                <td>:</td>
                                <td>{{ $tunggakan }}<br>
                                    <strong>SEGERA LUNASI TUNGGAKAN ANDA
                                        UNTUK MENGHINDARI
                                        DENDA YANG TERUS BERTAMBAH
                                    </strong>
                                </td>
                            </tr>
                            @endif
                            <tr style="vertical-align: top">
                                <td>Tempat Pembayaran</td>
                                <td>:</td>
                                <td>{{ $tp->nama_tempat }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @if ($tunggakan == '')
                    <strong>SEGERA LAKUKAN PEMBAYARAN PBB ANDA SEBELUM JATUH TEMPO AGAR TERHINDAR DARI DENDA 2% PER
                        BULAN</strong>
                    @endif
                </td>
                <td width="50%" align="center" style="border-collapse:collapse;border:1px solid #212529;padding:3px;">
                    <span class=" "><b>MALANG, {{ tglIndo($sppt->tgl_terbit_sppt) }} </b></span><br>
                    <span class="">Dokumen ini telah di tandatangani secara elektronik oleh :</span><br>
                    <span class=" ">{{ $kaban->is_plt != '' ? 'PLT' : '' }} {{ $kaban->jabatan }}
                        <br>
                        KABUPATEN MALANG<br><br>
                        <b>{{ $kaban->nama }}</b><br>NIP : {{ $kaban->nip }}</span>
                </td>
            </tr>
        </tbody>
    </table>
    @if (count($riwayat) > 0)


    <p class="text-tengah"><strong>KONFIRMASI TUNGGAKAN PBB P2 </strong></p>
    <table class="table-bordered" width="100%" padding="3">
        <thead>
            <tr>
                {{-- <th width="10px">No</th> --}}
                <th>Tahun</th>
                <th>PBB</th>
                <th width="200px">Denda *)</th>
                <th width="200px">Keterangan</th>

            </tr>
        </thead>
        <tbody>
            @php
            $a = 0;
            $b = 0;
            $c = 0;
            @endphp
            @foreach ($riwayat as $index => $row)
            <tr>
                {{-- <td style="text-align: center">{{ $index + 1 }}</td> --}}
                <td style="text-align: center">{{ $row->thn_pajak_sppt }}</td>
                <td style="text-align: right">
                    @php
                    $a += str_replace('.', '', $row->pokok_bayar);
                    @endphp
                    {{ $row->pokok_bayar != null ? $row->pokok_bayar : null }}</td>
                <td style="text-align: center">Denda 2% / Bulan</td>
                <td style="text-align: center">Belum Lunas</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td>Total</td>
                <td style="text-align: right">{{ angka($a) }}</td>
                <td colspan="2">
                    <small>*) Denda dihitung jika dibayar setelah jatuh tempo pembayaran dengan penghitungan 2 % /
                        bulan</small>
                </td>
            </tr>
        </tfoot>
    </table>
    <p class="text-tengah"><strong>MELUNASI TUNGGAKAN PAJAK ADALAH CERMIN WARGA TAAT PAJAK</strong></p>
    @endif
    <footer>
        &copy; sipanji.id PBB P2 {{ date('Y') }} - {{ tglIndo(date('Ymd')) }} {{ date('H:i:s') }}
    </footer>
</body>

</html>
