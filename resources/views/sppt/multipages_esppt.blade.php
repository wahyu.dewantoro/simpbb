<html>
<style type="text/css" >
    
    div.page {
        page-break-after: always;
        page-break-inside: avoid;
    }

    body {
            font-family: "Calibri (Body)";
            font-size: 1em;
            /* background-color:#f6d5c1; */
        }

        table {

            font-family: "Calibri (Body)";
            font-size: 0.9 em;

        }

        .watermark {
            position: fixed;
            top: 80mm;
            width: 100%;
            height: 200px;
            opacity: 0.1;
            text-align: center;
            vertical-align: middle
        }

        #pojokkananatas {
            position: fixed;
            top: 26mm;
            width: 100%;
            right: 5mm;
            text-align: right;
        }

        .table-bordered,
        .border {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            font-size: 1em !important;
            background-color: transparent
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border-collapse: collapse;
            border: 1px solid #212529;
            padding: 3px;
            font-size: 1em !important;
        }

        .border,
        .border th,
        .border td {
            border-collapse: collapse;
            border: 1px solid #212529;
            font-size: 1em !important;
            padding: 3px
        }

        .table-bordered th,
        .border th {
            vertical-align: middle
        }

        .table-bordered td,
        .border td {
            vertical-align: top
        }

        .text-kanan {
            text-align: right
        }

        .text-tengah {
            text-align: center
        }

        .text-kecil {
            font-size: 0.7em !important;
        }

        .text-sedang {
            font-size: 1em !important;
        }

        .page-number:before {
            content: "Page "counter(page)
        }

        .inti {
            border: 1px solid black;
            margin: auto;
        }

        table {
            width: 100%;
            border-spacing: 0;
        }

        .inti td {
            border-right: 1px solid black;
            padding-left: 10px;
            padding-right: 10px;
            padding-bottom: 5px;
            padding-top: 2px;
        }

        .inti th {
            border-right: 1px solid black;
            border-bottom: 1px solid black;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.5cm;

            /** Extra personal styles **/
            /* background-color:grey; */
            color: black;
            text-align: center;
            line-height: 0.5cm;
            font-size: small;
        }
</style>
<body>
    @foreach($pages as $page)
    <div class="page">
        {{ $page }}
    </div>
    @endforeach
</body>
</html>
