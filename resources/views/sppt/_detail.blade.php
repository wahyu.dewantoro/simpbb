<div class="invoice p-3 mb-3">
    <!-- title row -->
    <div class="row">
        <div class="col-12">
            <h4>
                <i class="fas fa-globe"></i> Data SPPT
                {{-- <small class="float-right">Tanggal: {{ tglIndo(date('Ymd')) }}</small> --}}
                <div class="float-right">
                    <div id="cetak"
                        data-href="{{ url('esppt') }}?nop={{ $sppt->kd_propinsi .$sppt->kd_dati2 .$sppt->kd_kecamatan .$sppt->kd_kelurahan .$sppt->kd_blok .$sppt->no_urut .$sppt->kd_jns_op .$sppt->thn_pajak_sppt }}"
                        class="btn btn-success">
                        <i class="fas fa-print"></i> Cetak E - SPPT
                    </div>
                </div>
            </h4>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Subjek Pajak
            <address>
                <strong>{{ $sppt->nm_wp_sppt }}</strong><br>
                {{ $sppt->jln_wp_sppt }} {{ $sppt->blok_kav_no_wp_sppt }}
                {{ $sppt->rw_wp_sppt != '' ? 'RT ' . $sppt->rw_wp_sppt : '' }}
                {{ $sppt->rt_wp_sppt != '' ? 'RW ' . $sppt->rt_wp_sppt : '' }}
                <br>
                {{ $sppt->kelurahan_wp_sppt }}, {{ $sppt->kota_wp_sppt }}
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            Objek Pajak
            <address>
                <strong>
                    {{ $sppt->kd_propinsi }}.{{ $sppt->kd_dati2 }}.{{ $sppt->kd_kecamatan }}.{{ $sppt->kd_kelurahan }}.{{ $sppt->kd_blok }}-{{ $sppt->no_urut }}.{{ $sppt->kd_jns_op }}
                </strong><br>
                {{ $objek->jalan_op }}
                {{ $objek->blok_kav_no_op }}
                {{ $objek->rt_op != '' ? 'RT ' . $objek->rt_op : '' }}
                {{ $objek->rw_op != '' ? 'RW ' . $objek->rw_op : '' }}
                <br>
                {{ $objek->nm_kelurahan }} {{ $objek->nm_kecamatan }} , MALANG
            </address>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th style="text-align: center">Objek</th>
                        <th style="text-align: center">Luas (M<sup>2</sup>)</th>
                        <th style="text-align: center">Kelas</th>
                        <th style="text-align: center">NJOP / (M<sup>2</sup>)</th>
                        <th style="text-align: center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Bumi</td>
                        <td class="text-center">{{ number_format($sppt->luas_bumi_sppt, 0, '', '.') }}</td>
                        <td class="text-center">{{ $sppt->kd_kls_tanah }}</td>
                        <td class="text-right">
                            {{ number_format($sppt->njop_bumi_sppt / $sppt->luas_bumi_sppt, 0, '', '.') }} </td>
                        <td class="text-right"> {{ number_format($sppt->njop_bumi_sppt, 0, '', '.') }} </td>
                    </tr>
                    <tr>
                        <td>Bangunan</td>
                        <td class="text-center">{{ number_format($sppt->luas_bng_sppt, 0, '', '.') }}</td>
                        <td class="text-center">{{ $sppt->kd_kls_bng }}</td>
                        <td class="text-right">
                            {{ $sppt->luas_bng_sppt > 0 ? number_format($sppt->njop_bng_sppt / $sppt->luas_bng_sppt, 0, '', '.') : null }}
                        </td>
                        <td class="text-right"> {{ number_format($sppt->njop_bng_sppt, 0, '', '.') }} </td>
                    </tr>

                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-6">

 
        </div>
        <!-- /.col -->
        <div class="col-6">
            <p class="lead">Jatuh Tempo {{ tglIndo($sppt->tgl_jatuh_tempo_sppt) }}</p>

            <div class="table-responsive">
                <table class="table table-sm">
                    <tr>
                        <th style="width:60%">NJOP</th>
                        <td style="text-align: right">
                            {{-- {{ number_format($sppt->njop_sppt, 0, ',', '.') }} |  --}}
                        {{ angka($sppt->njop_bumi_sppt+$sppt->njop_bng_sppt) }}</td>
                    </tr>
                    <tr>
                        <th>NJOPTKP</th>
                        <td style="text-align: right">{{ number_format($sppt->njoptkp_sppt, 0, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <th>NJOP PBB P2</th>
                        <td style="text-align: right">
                            {{ number_format(($sppt->njop_bumi_sppt+$sppt->njop_bng_sppt) - $sppt->njoptkp_sppt, 0, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <th>PBB Terhutang </th>
                        <td style="text-align: right">{{ number_format($sppt->pbb_terhutang_sppt, 0, ',', '.') }}
                        </td>
                    </tr>
                    <tr>
                        <th>PBB yang harus di bayar</th>
                        <td style="text-align: right">
                            {{ number_format($sppt->pbb_yg_harus_dibayar_sppt, 0, ',', '.') }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</div>
<!-- /.invoice -->
