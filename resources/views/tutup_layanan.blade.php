@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-10">
                <h1>PENGUMUMAN</h1>
            </div>
            <div class="col-sm-2">
                <div class="float-sm-right">
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info alert-dismissible">
                    {{-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> --}}
                    <h5><i class="icon fas fa-info"></i> Operasional Pelayanan </h5>
                    Permohonan pelayanan berkas tutup mulai {{ tglIndo($tutup->tgl_mulai)}} sampai {{ tglIndo($tutup->tgl_selesai)}}
                </div>

            </div>
        </div>

    </div>
</section>
@endsection
